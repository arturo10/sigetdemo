﻿

Imports Siget
Imports Siget.Entity
Imports Siget.Lang

Partial Class adminl_altacursos_academico_asignaturas
    Inherits System.Web.UI.Page

#Region "LOAD"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()
    End Sub
#End Region

#Region "DATABOUND"
    Protected Sub DDLNivelAsignatura_DataBound(sender As Object, e As EventArgs) Handles DDLNivelAsignatura.DataBound
        DDLNivelAsignatura.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Nivel"), 0))
        If DDLNivelAsignatura.Items.Count = 2 Then
            DDLNivelAsignatura.SelectedIndex = 1
            GVasignaturas.DataBind()
        End If
    End Sub

    Protected Sub DDLgradoAsignatura_DataBound(sender As Object, e As EventArgs) Handles DDLgradoAsignatura.DataBound
        DDLgradoAsignatura.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Grado"), 0))
        If DDLgradoAsignatura.Items.Count = 2 Then
            DDLgradoAsignatura.SelectedIndex = 1
            GVasignaturas.DataBind()
        End If
    End Sub

    Protected Sub DDLAreaAsignaturaDataBound(sender As Object, e As EventArgs) Handles DDLAreaAsignatura.DataBound
        DDLAreaAsignatura.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_AreaConocimiento"), 0))
        If DDLAreaAsignatura.Items.Count = 2 Then
            DDLAreaAsignatura.SelectedIndex = 1
        End If
    End Sub

    Protected Sub DDLIndicadorAsignaturaDataBound(sender As Object, e As EventArgs) Handles DDLIndicadorAsignatura.DataBound
        DDLIndicadorAsignatura.Items.Insert(0, New ListItem("---Ninguno", 0))
        If DDLIndicadorAsignatura.Items.Count = 2 Then
            DDLIndicadorAsignatura.SelectedIndex = 1
        End If
    End Sub

    Protected Sub GVasignaturas_DataBound(sender As Object, e As EventArgs) Handles GVasignaturas.DataBound
        If GVasignaturas.Rows.Count = 0 Then
            btnEliminarAsignatura.Visible = False
            btnActualizarAsignatura.Visible = False
            GVasignaturas.SelectedIndex = -1
        End If
    End Sub

#End Region

#Region "UPDATE"

    Protected Sub btnActualizarAsignatura_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarAsignatura.Click
        Try
            Dim AsignaturaRepository As IRepository(Of Asignatura) = New AsignaturaRepository()
            Dim asignatura As Asignatura = AsignaturaRepository.FindById(GVasignaturas.SelectedDataKey("IdAsignatura").ToString)
            Dim horas As Double

            If Not DDLgradoAsignatura.SelectedValue = 0 Then
                asignatura.IdIndicador = CShort(DDLIndicadorAsignatura.SelectedValue)
            Else
                asignatura.IdIndicador = Nothing
            End If

            If TBHorasAsignatura.Text <> "" Then
                horas = TBHorasAsignatura.Text
            Else
                horas = String.Empty
            End If

            With asignatura
                .Descripcion = TBNombreAsignatura.Text
                .FechaModif = Date.Now.ToString
                .Modifico = User.Identity.Name
                .Horas = horas
                .Consecutivo = TBConsecutivoAsignatura.Text
                .IdArea = DDLAreaAsignatura.SelectedValue.ToString
                .IdGrado = DDLgradoAsignatura.SelectedValue.ToString
                .IdIndicador = IIf(DDLIndicadorAsignatura.SelectedValue <> "0", CShort(DDLIndicadorAsignatura.SelectedValue), Nothing)
            End With
            AsignaturaRepository.Update(asignatura)
            GVasignaturas.DataBind()
            CType(New MessageSuccess(GVasignaturas, Me.GetType(), "Se ha realizado la actualización"), MessageSuccess).show()
        Catch ex As Exception
            CType(New MessageError(GVasignaturas, Me.GetType(), "Por favor revisa de nuevo los campos que has llenado"), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try


    End Sub

#End Region

#Region "CREATE"


    Protected Sub btnCrearAsignatura_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrearAsignatura.Click
        Try
            Dim asignatura As Asignatura = New Asignatura()
            Dim AsignaturaRepository As IRepository(Of Asignatura) = New AsignaturaRepository()
            Dim horas As Double

            If TBHorasAsignatura.Text <> String.Empty Then
                horas = TBHorasAsignatura.Text
            Else
                horas = 0
            End If

            With asignatura
                .Descripcion = TBNombreAsignatura.Text
                .FechaModif = Date.Now.ToString
                .Modifico = User.Identity.Name
                .Horas = horas
                .Consecutivo = TBConsecutivoAsignatura.Text
                .IdArea = DDLAreaAsignatura.SelectedValue.ToString
                .IdGrado = DDLgradoAsignatura.SelectedValue.ToString
                .IdIndicador = IIf(DDLIndicadorAsignatura.SelectedValue <> "0", CShort(DDLIndicadorAsignatura.SelectedValue), Nothing)
            End With

            AsignaturaRepository.Add(asignatura)
            CType(New MessageSuccess(GVasignaturas, Me.GetType(), "Se ha creado la asignatura"), MessageSuccess).show()
            GVasignaturas.DataBind()
        Catch ex As Exception
            CType(New MessageError(GVasignaturas, Me.GetType(), "Por favor revisa de nuevo los campos que has llenado"), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try

    End Sub

#End Region

#Region "SELECTED INDEX CHANGED"

    Protected Sub GVasignaturas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVasignaturas.SelectedIndexChanged

        btnEliminarAsignatura.Visible = True
        btnActualizarAsignatura.Visible = True
        TBNombreAsignatura.Text = HttpUtility.HtmlDecode(GVasignaturas.SelectedDataKey("Asignatura").ToString).Trim()
        DDLAreaAsignatura.SelectedValue = CInt(GVasignaturas.SelectedDataKey("IdArea").ToString)
        TBConsecutivoAsignatura.Text = HttpUtility.HtmlDecode(GVasignaturas.SelectedDataKey("Consecutivo").ToString)
        TBHorasAsignatura.Text = GVasignaturas.SelectedDataKey("Horas").ToString

        If Not IsDBNull(GVasignaturas.SelectedDataKey("IdIndicador").ToString) Then
            DDLIndicadorAsignatura.SelectedIndex = DDLIndicadorAsignatura.Items.IndexOf(DDLIndicadorAsignatura.Items.FindByValue(GVasignaturas.SelectedDataKey("IdIndicador").ToString))
        Else
            DDLIndicadorAsignatura.SelectedValue = 0
        End If

    End Sub

#End Region

#Region "DELETE"

    Protected Sub btnEliminarAsignatura_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarAsignatura.Click
        Try
            Dim AsignaturaRepository As IRepository(Of Asignatura) = New AsignaturaRepository()
            Dim asignatura As Asignatura = AsignaturaRepository.FindById(GVasignaturas.SelectedDataKey("IdAsignatura").ToString)
            AsignaturaRepository.Delete(asignatura)
            CType(New MessageSuccess(GVasignaturas, Me.GetType(), "Se ha eliminado la asignatura"), MessageSuccess).show()
            GVasignaturas.DataBind()
        Catch ex As Exception
            CType(New MessageError(GVasignaturas, Me.GetType(), "Por favor revisa de nuevo los campos que has llenado"), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try

    End Sub

#End Region

#Region "ROW CREATED"

    Protected Sub GVasignaturas_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVasignaturas.RowCreated
        Dim cociente As Integer = (GVasignaturas.PageCount / 15)
        Dim moduloIndex As Integer = (GVasignaturas.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVasignaturas.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVasignaturas.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVasignaturas.PageIndex
                final = IIf((inicial + 15 <= GVasignaturas.PageCount), inicial + 15, GVasignaturas.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVasignaturas.PageCount, inicial + 15, GVasignaturas.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVasignaturas.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If
    End Sub

#End Region
    
#Region "ROW DATABOUND"

    Protected Sub GVasignaturass_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVasignaturas.RowDataBound


        Dim cociente As Integer = (GVasignaturas.PageCount) / 15
        Dim moduloIndex As Integer = (GVasignaturas.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVasignaturas.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVasignaturas.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVasignaturas.PageIndex
            final = IIf((inicial + 15 <= GVasignaturas.PageCount), inicial + 15, GVasignaturas.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVasignaturas.PageCount), inicial + 15, GVasignaturas.PageCount)
        End If



        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVasignaturas, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVasignaturas, "Page$" & counter.ToString)
            Next
        End If
    End Sub

#End Region

#Region "PRE RENDER"

    Protected Sub GVasignaturas_PreRender(sender As Object, e As EventArgs) Handles GVasignaturas.PreRender

        GVasignaturas.Columns(2).HeaderText = Lang_Config.Translate("general", "Asignatura")
        GVasignaturas.Columns(3).HeaderText = Lang_Config.Translate("asignaturas", "Consecutivo")
        GVasignaturas.Columns(5).HeaderText = Lang_Config.Translate("general", "AreaConocimiento")
        GVasignaturas.Columns(6).HeaderText = Lang_Config.Translate("asignaturas", "Indicador")

    End Sub


#End Region

    Protected Sub GVasignaturas_PageIndexChanged(sender As Object, e As EventArgs) Handles GVasignaturas.PageIndexChanged
        GVasignaturas.SelectedIndex = -1
        Siget.Utils.GeneralUtils.CleanAll(PanelAsignaturas.Controls)
        btnActualizarAsignatura.Visible = False
        btnEliminarAsignatura.Visible = False
    End Sub

    Protected Sub gvCalificaciones_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles GVasignaturas.SelectedIndexChanging
        If e.NewSelectedIndex = CType(sender, GridView).SelectedIndex Then
            GVasignaturas.SelectedIndex = -1
            e.Cancel = True
            Siget.Utils.GeneralUtils.CleanAll(PanelAsignaturas.Controls)
            btnActualizarAsignatura.Visible = False
            btnEliminarAsignatura.Visible = False
        End If
    End Sub
End Class
