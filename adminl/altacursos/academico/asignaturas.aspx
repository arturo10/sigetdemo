﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false" CodeFile="asignaturas.aspx.vb" 
    EnableEventValidation="false"
    Inherits="adminl_altacursos_academico_asignaturas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="col-lg-12 col-sm-12 col-xs-12 resize-mobile">
        <div class="widget">
            <div class="widget-header bg-themeprimary">
                <i class="widget-icon fa fa-arrow-right"></i>
                <span class="widget-caption"><b>    <%=Lang_Config.Translate("general", "Asignatura").ToUpper()%> </b></span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="config">
                        <i class="fa fa-cog"></i>
                    </a>
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <!--Widget Header-->
            <div class="widget-body">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:UpdatePanel ID="PanelAsignaturas" runat="server">
                            <ContentTemplate>
                                <div class="form-horizontal">
                                  
                                    <div class="panel panel-default">
                                        <div class="panel-body" id="asignaturasForm">
                                            <div class="col-lg-12" style="position:absolute;top:50%;left:50%;bottom:50%;">
                                                <div class="load-spinner"></div>
                                            </div>

                                            <div class="col-lg-7">

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">
                                                                 <%=Lang_Config.Translate("general", "Nivel")%>
                                                    </label>
                                                    <div class="col-lg-4">
                                                        <asp:DropDownList runat="server" ID="DDLNivelAsignatura"
                                                            CssClass="form-control" AutoPostBack="True"
                                                            DataSourceID="SDSniveles" DataTextField="Descripcion"
                                                            DataValueField="IdNivel">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <label class="col-lg-2 control-label">
                                                              <%=Lang_Config.Translate("general", "Grado")%>
                                                    </label>
                                                    <div class="col-lg-4">
                                                        <asp:DropDownList runat="server"
                                                            DataSourceID="SDSgrados" DataTextField="Descripcion"
                                                            DataValueField="IdGrado" AutoPostBack="true"
                                                            ID="DDLgradoAsignatura" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label" >
                                                                   <%=Lang_Config.Translate("general", "AreaConocimiento")%>
                                                    </label>
                                                    <div class="col-lg-10">
                                                        <asp:DropDownList runat="server" ID="DDLAreaAsignatura" CssClass="form-control"
                                                            AutoPostBack="True"
                                                            DataSourceID="SDSarea" DataTextField="Nombre" DataValueField="IdArea">
                                                        </asp:DropDownList>
                                                    </div>

                                                </div>
                                                <div class="form-group">

                                                    <label class="col-lg-2 control-label">
                                                                          <%=Lang_Config.Translate("general", "Asignatura")%>
                                                    </label>
                                                    <div class="col-lg-10">
                                                        <asp:TextBox runat="server" ID="TBNombreAsignatura" CssClass="form-control">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">
                                                                           <%=Lang_Config.Translate("asignaturas", "Consecutivo")%>
                                                    </label>
                                                    <div class="col-lg-4">
                                                        <asp:TextBox TextMode="Number" runat="server" ID="TBConsecutivoAsignatura" CssClass="form-control">
                                                        </asp:TextBox>
                                                    </div>
                                                    <label class="col-lg-2 control-label">
                                                                 <%=Lang_Config.Translate("asignaturas", "Duracion")%>
                                                    </label>
                                                    <div class="col-lg-4">
                                                        <asp:TextBox min="0" runat="server" ID="TBHorasAsignatura" CssClass="form-control">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label  class="col-lg-2 control-label">   <%=Lang_Config.Translate("asignaturas", "Indicador")%></label>
                                                    <div class="col-lg-10">
                                                        <asp:DropDownList runat="server" ID="DDLIndicadorAsignatura"
                                                            DataSourceID="SDSIndicadores" DataTextField="Descripcion"
                                                            DataValueField="IdIndicador" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-5 col-sm-12 col-md-12 col-xs-12">
                                                <div class="table-responsive">
                                                    <asp:GridView ID="GVasignaturas" runat="server"
                                                        AllowPaging="True" 
                                                        OnDataBound="GVasignaturas_DataBound"
                                                        AllowSorting="True"
                                                        AutoGenerateColumns="False"
                                                        DataKeyNames="IdAsignatura, IdIndicador,Horas,Asignatura,IdArea,Consecutivo,Area,Indicador"
                                                        DataSourceID="SDSdatosasignaturas"
                                                        PageSize="5"
                                                        CellPadding="3"
                                                        CssClass="table table-striped table-bordered"
                                                        Height="17px"
                                                        Style="font-size: x-small; text-align: left;">
                                                        <Columns>
                                                            <asp:BoundField DataField="Grado" HeaderText="Grado" SortExpression="Grado"
                                                                Visible="False" />
                                                            <asp:BoundField DataField="IdAsignatura" Visible="false" HeaderText="Id_Asignatura"
                                                                InsertVisible="False" ReadOnly="True" SortExpression="IdAsignatura" />
                                                            <asp:BoundField DataField="Asignatura" HeaderText="Curso"
                                                                SortExpression="Asignatura" />
                                                            <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                                                                SortExpression="Consecutivo" />
                                                            <asp:BoundField DataField="IdArea" Visible="false" HeaderText="Id-Area"
                                                                SortExpression="IdArea" />
                                                            <asp:BoundField DataField="Area" HeaderText="Area de Conocimiento"
                                                                SortExpression="Area" />
                                                            <asp:BoundField DataField="Indicador" HeaderText="Indicador"
                                                                SortExpression="Indicador" />
                                                        </Columns>
                                                        <PagerTemplate>
                                                            <ul runat="server" id="Pag" class="pagination">
                                                            </ul>
                                                        </PagerTemplate>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <SelectedRowStyle CssClass="row-selected" />
                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                    </asp:GridView>
                                                </div>

                                            </div>

                                            <div class="col-lg-12 col-xs-12">
                                                <asp:LinkButton ID="btnActualizarAsignatura" runat="server" Visible="false" type="submit"
                                                    CssClass="btn btn-lg btn-labeled btn-palegreen space-margin shiny" Style="word-wrap: break-word;">
                                                                 <i class="btn-label fa fa-refresh"></i> <%=Lang_Config.Translate("asignaturas", "Actualizar_asignatura")%> 
                                                                    
                                                </asp:LinkButton>

                                                <asp:LinkButton ID="btnEliminarAsignatura" Visible="false" runat="server" type="submit"
                                                    CssClass="btn btn-lg btn-labeled btn-darkorange space-margin">
                                                        <i class="btn-label fa fa-remove"></i>
                                                              <%=Lang_Config.Translate("asignaturas", "Eliminar_asignatura")%>
                                                </asp:LinkButton>

                                                <asp:LinkButton ID="btnCrearAsignatura" runat="server" type="submit"
                                                    CssClass=" btn btn-lg btn-labeled btn-palegreen">
                                                                <i class="btn-label fa fa-plus"></i>
                                                                      <%=Lang_Config.Translate("asignaturas", "Insertar_asignatura")%> 
                                                </asp:LinkButton>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>


                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>


    <asp:SqlDataSource ID="SDSdatosasignaturas" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT A.Horas,A.IdAsignatura, A.IdGrado, G.Descripcion Grado, A.Descripcion Asignatura, A.Consecutivo, A.IdArea, Ar.Descripcion Area, A.IdIndicador, I.Descripcion 'Indicador'
FROM Grado G, Area Ar, Asignatura A LEFT JOIN Indicador I ON A.IdIndicador = I.IdIndicador
WHERE (A.IdGrado = @IdGrado) and (G.IdGrado = A.IdGrado)
and (Ar.IdArea = A.IdArea) 
order by G.Descripcion, A.Descripcion">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLgradoAsignatura" Name="IdGrado"
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>


    <asp:SqlDataSource ID="SDSniveles" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSarea" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdArea, Descripcion + ' (' + ISNULL(Clave, '') + ')' as Nombre
from Area
where Estatus = 'Activo'
order by Descripcion"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSgrados" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLnivelAsignatura" Name="IdNivel"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>


    <asp:SqlDataSource ID="SDSIndicadores" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT [IdIndicador], [Descripcion] FROM [Indicador] ORDER BY [Descripcion]"></asp:SqlDataSource>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScriptContent" runat="Server">
</asp:Content>

