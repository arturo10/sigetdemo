﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="apoyoacademico.aspx.cs" 
    
    EnableEventValidation="false"
    
    Inherits="administrador_Academico_ApoyoAcademico" %>


<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">



    <div class="row">

        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="widget">
                <div class="widget-header bg-themeprimary">
                    <i class="widget-icon fa fa-arrow-right"></i>
                    <span class="widget-caption"><b>ÁREA APOYOS ACADÉMICOS </b></span>
                    <div class="widget-buttons">
                        <a href="#" data-toggle="config">
                            <i class="fa fa-cog"></i>
                        </a>
                        <a href="#" data-toggle="maximize">
                            <i class="fa fa-expand"></i>
                        </a>
                        <a href="#" data-toggle="collapse">
                            <i class="fa fa-minus"></i>
                        </a>
                        <a href="#" data-toggle="dispose">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                    <!--Widget Buttons-->
                </div>
                <!--Widget Header-->
                <div class="widget-body">



                    <div id="ApoyosAcademicos">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                <asp:UpdatePanel ID="PanelApoyosAcademicos" runat="server">

                                    <ContentTemplate>
                                        <div class="form-horizontal" id="ApoyoNivelForm">
                                           
                                            <div class="col-lg-6">
                                                <div class="col-lg-12" style="position: absolute; top: 50%; left: 50%; right: 50%; bottom: 50%;">
                                                    <div id="spinner-load-files" class="load-spinner"></div>
                                                </div>
                                                <div class="row">
                                                    <div class="card col-lg-11">
                                                        <div class="card-content">

                                                            <div class="form-group">
                                                                <asp:Label ID="Label1" CssClass="col-lg-2" runat="server">
                                                                    <%=Lang_Config.Translate("general","Nivel") %>
                                                                </asp:Label>
                                                                <div class="col-lg-4">
                                                                    <asp:DropDownList ID="DDLNivelApoyo" runat="server" AutoPostBack="True" OnDataBound="DDLNivelApoyo_DataBound"
                                                                        DataSourceID="SDSniveles" DataTextField="Descripcion"
                                                                        DataValueField="IdNivel" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:Label ID="Label6" CssClass="col-lg-3" runat="server" Text="Consecutivo" />
                                                                <div class="col-lg-3">
                                                                    <asp:TextBox ID="TBconsecutivo" TextMode="Number" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <asp:Label ID="Label7" CssClass="col-lg-3" runat="server" Text=" Descripción" />
                                                                <div class="col-lg-9">

                                                                    <asp:TextBox ID="TBdescripcion" ClientIDMode="Static" runat="server" CssClass="form-control"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label8" CssClass="col-lg-3" runat="server" Text="Tipo de archivo" />
                                                                <div class="col-lg-7">
                                                                    <asp:DropDownList ID="DDLtipoarchivo" OnSelectedIndexChanged="DDLtipoarchivo_SelectedIndexChanged"
                                                                         CssClass="form-control" runat="server"
                                                                        AutoPostBack="True">
                                                                        <asp:ListItem>PDF</asp:ListItem>
                                                                        <asp:ListItem>ZIP</asp:ListItem>
                                                                        <asp:ListItem>Word</asp:ListItem>
                                                                        <asp:ListItem>Excel</asp:ListItem>
                                                                        <asp:ListItem Value="PP">Power Point</asp:ListItem>
                                                                        <asp:ListItem>Imagen</asp:ListItem>
                                                                        <asp:ListItem Value="Video"></asp:ListItem>
                                                                        <asp:ListItem>Audio</asp:ListItem>
                                                                        <asp:ListItem Value="Link"></asp:ListItem>
                                                                        <asp:ListItem>FLV</asp:ListItem>
                                                                        <asp:ListItem>SWF</asp:ListItem>
                                                                        <asp:ListItem>YouTube</asp:ListItem>
                                                                    </asp:DropDownList>

                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <i class="material-icons md-48">folder</i>
                                                                </div>



                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label runat="server" CssClass="col-lg-3" Text="Cargar material "></asp:Label>

                                                                <div class="col-lg-9">

                                                                    <asp:AsyncFileUpload ID="RutaArchivo" runat="server"  OnUploadedComplete="RutaArchivo_UploadedComplete" 
                                                                        OnClientUploadStarted="ApoyoAcademicoUploadStarted" OnClientUploadComplete="ApoyoAcademicoUploadComplete"
                                                                  />
                                                                    <asp:TextBox ID="TBlink" runat="server" MaxLength="150" Visible="False"
                                                                        CssClass="form-control"></asp:TextBox>
                                                                    <asp:HyperLink runat="server" ID="LBArchivoApoyoNivel"></asp:HyperLink>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label runat="server" Text="Estatus:" CssClass="col-lg-3" />
                                                                <div class="col-lg-9">
                                                                    <asp:DropDownList ID="DDLestatus" CssClass="form-control" runat="server">
                                                                        <asp:ListItem Value="Activo"></asp:ListItem>
                                                                        <asp:ListItem Value="Suspendido">Suspendido</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:LinkButton ID="Insertar" OnClick="Insertar_Click" type="submit" runat="server"
                                                                    CssClass="ApN btn btn-labeled btn-palegreen">
                                                                             <i class="btn-label fa fa-check"></i>Insertar
                                                                </asp:LinkButton>

                                                                <asp:LinkButton ID="Actualizar" OnClick="Actualizar_Click" Visible="false" type="submit" runat="server"
                                                                    Text="Actualizar"
                                                                    CssClass="ApN btn btn-labeled btn-palegreen">
                                                                             <i class="btn-label fa fa-refresh"></i> Actualizar
                                                                </asp:LinkButton>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="table-responsive">
                                                        <asp:GridView ID="GVapoyosacademicos" runat="server" AllowPaging="True" OnDataBound="GVapoyosacademicos_DataBound"
                                                            AllowSorting="True" AutoGenerateColumns="False" OnRowCreated="GVapoyosacademicos_RowCreated"
                                                            OnRowDataBound="GVapoyosacademicos_OnRowDataBound" OnSelectedIndexChanged="GVapoyosacademicos_SelectedIndexChanged"
                                                            PageSize="5" OnPageIndexChanged="GVapoyosacademicos_PageIndexChanged" OnSelectedIndexChanging="GVapoyosacademicos_SelectedIndexChanging"
                                                            CellPadding="3"
                                                            CssClass="table table-striped table-bordered"
                                                            Height="17px"
                                                            Style="font-size: x-small; text-align: left;"
                                                            DataSourceID="SDSApoyoNivel"
                                                            DataKeyNames="IdApoyoNivel,TipoArchivoApoyo,ArchivoApoyo,Consecutivo,Descripcion,Estatus"
                                                            Caption="<h3>Apoyos académicos capturados</h3>">
                                                            <Columns>
                                                                <asp:BoundField DataField="IdApoyoNivel" HeaderText="IdApoyo" Visible="false"
                                                                    SortExpression="IdApoyoNivel" />
                                                                <asp:BoundField DataField="IdNivel" HeaderText="IdNivel"
                                                                    SortExpression="IdNivel" Visible="False" />
                                                                <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                                                                    SortExpression="Consecutivo" />
                                                                <asp:BoundField DataField="Descripcion" HeaderText="Descripción"
                                                                    SortExpression="Descripcion" />
                                                                <asp:BoundField DataField="ArchivoApoyo" HeaderText="Archivo de Apoyo"
                                                                    SortExpression="ArchivoApoyo" />
                                                                <asp:BoundField DataField="TipoArchivoApoyo" HeaderText="Tipo"
                                                                    SortExpression="TipoArchivoApoyo" />
                                                                <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                                    SortExpression="Estatus" />
                                                            </Columns>
                                                            <PagerTemplate>

                                                                <ul runat="server" id="Pag" class="pagination">
                                                                </ul>

                                                            </PagerTemplate>
                                                            <PagerStyle HorizontalAlign="Center" />
                                                            <SelectedRowStyle CssClass="row-selected" />
                                                            <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:LinkButton ID="Eliminar" OnClick="Eliminar_Click" Visible="false" type="submit" runat="server"
                                                        CssClass="ApN btn btn-labeled btn-darkorange">
                                                            <i class="btn-label fa fa-remove"></i>Eliminar
                                                    </asp:LinkButton>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">

                                                <uc1:msgSuccess runat="server" ID="msgSuccess1" />

                                                <uc1:msgInfo runat="server" ID="msgInfo1" />

                                                <uc1:msgError runat="server" ID="msgError1" />
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>


                        <asp:Label ID="Label2" runat="server"
                            Style="color: #000099; font-size: x-small; font-weight: 700; font-family: Arial, Helvetica, sans-serif;"></asp:Label>

                        <td style="font-size: x-small;">Nota: Actualizar no modifica el archivo, sólo la información del registro.
                                       

        <asp:SqlDataSource ID="SDSNiveles" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>



                            <asp:SqlDataSource ID="SDSApoyoNivel" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [ApoyoNivel] WHERE ([IdNivel] = @IdNivel) ORDER BY [Consecutivo]">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLnivelApoyo" Name="IdNivel"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                    </div>

                </div>
            </div>
        </div>
    </div>






</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScriptContent" runat="Server">
</asp:Content>

