﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" EnableEventValidation="false"
    AutoEventWireup="false" CodeFile="temas.aspx.vb" Inherits="administrador_AgruparReactivos" %>

<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgWarning.ascx" TagPrefix="uc1" TagName="msgWarning" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

     <div class="col-lg-12 col-sm-12 col-xs-12 resize-mobile">
        <div class="widget flat radius-bordered">
            <div class="widget-header bg-themeprimary">
                <span class="widget-caption"><b><%=Lang_Config.Translate("temas", "header").ToUpper()%></b>  </span>
            </div>

            <div class="widget-body">
                <div class="widget-main ">
                    <div class="tabbable">
                        <ul class="nav nav-tabs tabs-flat" id="myTab11">
                            <li id="Temas" class="active">
                                <a data-toggle="tab" href="#temas"><%=Lang_Config.Translate("temas", "Sub_header_1")%>
                                </a>
                            </li>
                            <li>
                                <a id="Subtemas" data-toggle="tab" href="#subtemas"><%=Lang_Config.Translate("temas", "Sub_header_2")%>
                                </a>
                            </li>
                         
                        </ul>
                        <div class="tab-content tabs-flat">
                            <div id="temas" class="tab-pane in active">
                                <div class="form-horizontal">
                                    <div class="panel panel-default">
                                        <asp:UpdatePanel ID="PanelTemas" runat="server">
                                            <ContentTemplate>
                                                <div class="panel-body" id="TemasForm">
                                                    <div class="col-lg-12" style="position: absolute; top: 50%; left: 50%; bottom: 50%;">
                                                        <div class="load-spinner"></div>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label"> <%=Lang_Config.Translate("general", "Nivel")%></label>
                                                            <div class="col-lg-4">
                                                                <asp:DropDownList runat="server"
                                                                    ID="DDLNivelTemas"
                                                                    CssClass="form-control"
                                                                    AutoPostBack="True"
                                                                    DataSourceID="SDSnivelesTemas"
                                                                    DataTextField="Descripcion"
                                                                    DataValueField="IdNivel">
                                                                </asp:DropDownList>

                                                            </div>
                                                            <label class="col-lg-2 control-label"> <%=Lang_Config.Translate("general","Grado") %></label>
                                                            <div class="col-lg-4">
                                                                <asp:DropDownList runat="server"
                                                                    ID="DDLGradoTemas"
                                                                    CssClass="form-control"
                                                                    AutoPostBack="True"
                                                                    DataSourceID="SDSgradosTemas"
                                                                    DataTextField="Descripcion"
                                                                    DataValueField="IdGrado">
                                                                </asp:DropDownList>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label"><%=Lang_Config.Translate("general","Asignatura") %></label>
                                                            <div class="col-lg-5">
                                                                <asp:DropDownList runat="server"
                                                                    ID="DDLAsignaturaTemas"
                                                                    CssClass="form-control"
                                                                    AutoPostBack="True"
                                                                    DataSourceID="SDSasignaturasTemas"
                                                                    DataTextField="Descripcion"
                                                                    DataValueField="IdAsignatura">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <label class="col-lg-2 control-label"><%=Lang_Config.Translate("temas", "Numero_tema")%></label>
                                                            <div class="col-lg-3">
                                                                <asp:TextBox TextMode="Number" runat="server"
                                                                    ID="TBNoTema"
                                                                    CssClass="form-control">
                                                                </asp:TextBox>
                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label"><%=Lang_Config.Translate("temas", "Descripcion")%>
                                                            </label>
                                                            <div class="col-lg-10">
                                                                <asp:TextBox runat="server" TextMode="MultiLine"
                                                                    ID="TBDescripcionTemas" Rows="3"
                                                                    CssClass="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>


                                                    </div>

                                                    <div class="col-lg-6">
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="GVtemas" runat="server" AllowPaging="True"
                                                                AllowSorting="True" AutoGenerateColumns="False" 
                                                                CellPadding="3" DataKeyNames="IdTema,IdAsignatura,NomAsignatura,Numero,NomTema"
                                                                DataSourceID="SDSdatostemas"
                                                                PageSize="5" 
                                                                CssClass="table table-striped table-bordered"
                                                                Height="17px"
                                                                Style="font-size: x-small; text-align: left;">
                                                                <Columns>
                                                                    <asp:BoundField DataField="IdAsignatura" HeaderText="Id-Asign."
                                                                        SortExpression="IdAsignatura" Visible="False" />
                                                                    <asp:BoundField DataField="NomAsignatura" HeaderText="Curso"
                                                                        SortExpression="NomAsignatura" Visible="False" />
                                                                    <asp:BoundField DataField="IdTema" HeaderText="Id_Tema" InsertVisible="False"
                                                                        ReadOnly="True" Visible="false" SortExpression="IdTema" />
                                                                    <asp:BoundField DataField="NomTema" HeaderText="Tema"
                                                                        SortExpression="NomTema" />
                                                                    <asp:BoundField DataField="Numero" HeaderText="Número"
                                                                        SortExpression="Numero" />
                                                                </Columns>
                                                                <PagerTemplate>
                                                                    <ul runat="server" id="Pag" class="pagination">
                                                                    </ul>
                                                                </PagerTemplate>
                                                                <PagerStyle HorizontalAlign="Center" />
                                                                <SelectedRowStyle CssClass="row-selected" />
                                                                <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12">
                                                        <asp:LinkButton ID="btnCrearTema" runat="server" type="submit"
                                                            CssClass="btn btn-lg btn-labeled btn-palegreen space-margin">
                                                        <i class="btn-label fa fa-plus"></i>
                                                            <%=Lang_Config.Translate("temas", "Crear_Tema")%>   
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnActualizarTema" runat="server" type="submit" Visible="false"
                                                            CssClass="btn btn-lg btn-labeled btn-palegreen space-margin shiny">
                                                        <i class="btn-label fa fa-refresh"></i>
                                                              <%=Lang_Config.Translate("temas", "Actualizar_Tema")%>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnEliminarTema" runat="server" Visible="false" type="submit"
                                                            CssClass="btn btn-lg btn-labeled btn-darkorange space-margin">
                                                        <i class="btn-label fa fa-remove"></i>
                                                              <%=Lang_Config.Translate("temas", "Eliminar_Tema")%>
                                                        </asp:LinkButton>
                                                    </div>

                                                    <div class="col-lg-12">
                                                        <uc1:msgSuccess runat="server" ID="msgSuccess" />
                                                        <uc1:msgError runat="server" ID="msgError" />
                                                    </div>



                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>

                            <div id="subtemas" class="tab-pane ">
                                <asp:UpdatePanel ID="PanelSubtemas" runat="server">
                                    <ContentTemplate>
                                        <div class="form-horizontal"  id="SubtemasForm">
                                             <div class="col-lg-12" style="position: absolute; top: 50%; left: 50%; bottom: 50%;">
                                                        <div class="load-spinner"></div>
                                                    </div>

                                            <div class="panel panel-default">

                                                <div class="panel-heading bg-themeprimary-lg">
                                                    <%=Lang_Config.Translate("temas", "Sub_header_subtema")%>:
                                                       <b> <asp:Label CssClass="lsize-st" runat="server" ID="TemaSelected"></asp:Label></b>
                                                </div>

                                                <div class="panel-body">

                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">  <%=Lang_Config.Translate("temas", "Numero_Subtema")%></label>
                                                            <div class="col-lg-3">
                                                                <asp:TextBox runat="server" 
                                                                    ID="TBNumeroSubtema"
                                                                    CssClass="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                            <label class="col-lg-3 control-label"> <%=Lang_Config.Translate("temas", "Competencia")%></label>
                                                            <div class="col-lg-4">
                                                                <asp:DropDownList runat="server" AutoPostBack="true"
                                                                    ID="DDLCompetenciaSubtema"
                                                                    DataSourceID="SDScompetencias" DataTextField="Descripcion"
                                                                    DataValueField="IdCompetencia"
                                                                    CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label"><%=Lang_Config.Translate("temas", "Descripcion_Subtema")%></label>
                                                            <div class="col-lg-10">
                                                                <asp:TextBox runat="server" TextMode="Multiline"
                                                                    ID="TBDescripcionSubtema"
                                                                    CssClass="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>


                                                    </div>

                                                    <div class="col-lg-6">
                                                        <div class="form-group">


                                                            <div class="table-responsive">
                                                                <asp:GridView ID="GVsubtemas" runat="server" AllowPaging="True"
                                                                    AllowSorting="True" AutoGenerateColumns="False"
                                                                    CellPadding="3" DataKeyNames="IdSubtema,Numero,Subtema,Competencia,IdCompetencia"
                                                                    DataSourceID="SDSdatosubt"
                                                                    PageSize="5"
                                                                    CssClass="table table-striped table-bordered"
                                                                    Height="17px"
                                                                    Style="font-size: x-small; text-align: left;"
                                                                  >
                                                                    <Columns>
                                                                          <asp:BoundField DataField="IdSubtema"  HeaderText="Id_Subtema"
                                                                             ReadOnly="True" SortExpression="IdSubtema" />
                                                                        <asp:BoundField DataField="IdCompetencia" Visible="false" HeaderText="Id_Comp."
                                                                            SortExpression="IdCompetencia" />
                                                                        <asp:BoundField DataField="Competencia" HeaderText="Competencia"
                                                                            SortExpression="Competencia" />
                                                                      
                                                                        <asp:BoundField DataField="Numero" HeaderText="Número"
                                                                            SortExpression="Numero" />
                                                                        <asp:BoundField DataField="Subtema" HeaderText="Subtema"
                                                                            SortExpression="Subtema" />
                                                                    </Columns>
                                                                    <PagerTemplate>
                                                                        <ul runat="server" id="Pag" class="pagination">
                                                                        </ul>
                                                                    </PagerTemplate>
                                                                    <PagerStyle HorizontalAlign="Center" />
                                                                    <SelectedRowStyle CssClass="row-selected" />
                                                                    <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </div>

                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-lg-6 col-lg-offset-6">
                                                                <a data-toggle="modal" id="btnApoyosSubtemas" runat="server" visible="false"
                                                                    class="btn btn-labeled btn-palegreen" data-target=".apoyoSubtemas">
                                                                    <i class="btn-label fa fa-upload"></i>
                                                                        <%=Lang_Config.Translate("temas", "Boton_upload_subtema")%>
                                                                
                                                                </a>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-lg-12">
                                                        <asp:LinkButton ID="btnCrearSubtema" runat="server" type="submit"
                                                            CssClass="btn btn-lg btn-labeled btn-palegreen space-margin">
                                                        <i class="btn-label fa fa-plus"></i>
                                                            <%=Lang_Config.Translate("temas", "Crear_SubTema")%>
                                                    
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnActualizarSubtema" runat="server" type="submit" Visible="false"
                                                            CssClass="btn btn-lg btn-labeled btn-palegreen space-margin shiny">
                                                        <i class="btn-label fa fa-refresh"></i>
                                                                  <%=Lang_Config.Translate("temas", "Actualizar_SubTema")%>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnEliminarSubtema" runat="server" type="submit" Visible="false"
                                                            CssClass="btn btn-lg btn-labeled btn-darkorange space-margin">
                                                        <i class="btn-label fa fa-remove"></i>
                                                                   <%=Lang_Config.Translate("temas", "Eliminar_SubTema")%>
                                                        </asp:LinkButton>
                                                    </div>
                                                    
                                                    <div class="col-lg-12">
                                                        <uc1:msgSuccess runat="server" ID="msgSuccess2" />
                                                        <uc1:msgError runat="server" ID="msgError2" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </div>


    <div class="modal fade apoyoSubtemas" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <asp:UpdatePanel ID="ApoyoSubtemaPanel" runat="server" UpdateMode="Always">
                
                    <ContentTemplate>
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myLargeModalLabel"><b><%=Lang_Config.Translate("temas","Header_modal_apoyos") %></h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-horizontal">
                                <div class="col-lg-6" style="position: absolute; top: 50%; left: 20%; right: 50%;bottom:30%;">
                                    <div id="spinner-load-files" class="load-spinner"></div>
                                </div>

                                <div class="form-group">
                                   <label class="col-lg-2 control-label">
                                       <%=Lang_Config.Translate("temas","Numero_Subtema") %>
                                   </label>
                                   <div class="col-lg-4">
                                       <asp:TextBox runat="server" ID="TBConsecutivoApoyoSub"
                                           CssClass="form-control" TextMode="Number">

                                       </asp:TextBox>
                                   </div>
                                    <label class="col-lg-2 control-label">
                                      <%=Lang_Config.Translate("temas", "Tipo_de_archivo")%>

                                   </label>
                                   <div class="col-lg-4">
                                       <asp:DropDownList runat="server" ID="DDLTipoArchivoApoyoSub" AutoPostBack="true"
                                           CssClass="tipoArchivoSubt form-control">
                                           <asp:ListItem Value="Imagen">Imagen</asp:ListItem>
                                            <asp:ListItem Value="PDF">PDF</asp:ListItem>
                                            <asp:ListItem Value="ZIP">ZIP</asp:ListItem>
                                            <asp:ListItem Value="Word">Word</asp:ListItem>
                                            <asp:ListItem Value="FLV">FLV</asp:ListItem>
                                            <asp:ListItem Value="SWF">SWF</asp:ListItem>
                                            <asp:ListItem  Value="Excel">Excel</asp:ListItem>
                                            <asp:ListItem Value="PP">Power Point</asp:ListItem>
                                            <asp:ListItem  Value="Video">Video</asp:ListItem>
                                            <asp:ListItem Value="Audio"> Audio</asp:ListItem>
                                            <asp:ListItem Value="Link">Link</asp:ListItem>
                                            <asp:ListItem Value="YouTube">YouTube</asp:ListItem>
                                            <asp:ListItem Value="Otro">Otro</asp:ListItem>
                                       </asp:DropDownList>
                                   </div>
                               </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">  <%=Lang_Config.Translate("temas", "Descripcion_Subtema")%></label>
                                    <div class="col-lg-10">
                                        <asp:TextBox runat="server" TextMode="MultiLine"
                                            CssClass="form-control" ID="TBDescripcionApoyoSub">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                   

                                    <div class="col-lg-10">
                                        <label class="col-lg-2 control-label">
                                             <%=Lang_Config.Translate("temas", "Archivo_apoyo")%>
                                        </label>
                                        <div class="col-lg-10"> 
                                            <asp:AsyncFileUpload runat="server" ID="FileUploadSubtema"
                                                 OnClientUploadStarted="ApoyoSubtemaUploadStarted" OnClientUploadComplete="ApoyoSubtemaUploadComplete" />
                                             <asp:TextBox ID="TBlink" runat="server" MaxLength="150" Visible="False"
                                            CssClass="form-control"></asp:TextBox>
                                            <asp:HyperLink runat="server" ID="HLArchivoCargadoSubtemas"></asp:HyperLink>
                                        </div>
                                    </div>
                                </div>  
                                

                                <div class="form-group">
                                <asp:LinkButton ID="btnCargarApoyoSub" runat="server"
                                    CssClass="btn btn-lg btn-labeled btn-palegreen space-margin">
                                                        <i class="btn-label fa fa-plus"></i>
                                             <%=Lang_Config.Translate("temas", "Cargar_apoyo")%>                 
                                   
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnActualizarApoyoSub" runat="server"
                                    CssClass="btn btn-lg btn-labeled btn-palegreen space-margin shiny">
                                                        <i class="btn-label fa fa-refresh"></i>
                                           <%=Lang_Config.Translate("temas", "Actualizar_apoyo")%>  
                                </asp:LinkButton>
                               
                            </div>
                            <div class="form-group">
                                <uc1:msgSuccess runat="server" ID="msgSuccess3" />
                                <uc1:msgError runat="server" ID="msgError3" />
                                 <uc1:msgWarning runat="server" ID="msgWarning3" />
                                     <uc1:msgInfo runat="server" ID="msgInfo" />
                            </div>

                                <div class="form-group">
                                    <div class="col-lg-6 col-lg-offset-3">
                                        <div class="table-responsive">
                                            <asp:GridView ID="GVApoyoSubtema" runat="server" AllowPaging="True"
                                                OnDataBound="GVApoyoSubtema_DataBound" 
                                                AllowSorting="True" AutoGenerateColumns="False"
                                                CellPadding="3" DataKeyNames="IdApoyoSubtema,IdSubtema,Consecutivo,ArchivoApoyo,TipoArchivoApoyo,Descripcion"
                                                DataSourceID="SDSDatosApoyosSubtemas"
                                                PageSize="5"
                                                CssClass="table table-striped table-bordered"
                                                Height="17px"
                                                Style="font-size: x-small; text-align: left;"
                                               
                                                Caption="<h3>Apoyos subtemas</h3>">
                                                <Columns>
                                                    <asp:BoundField DataField="IdApoyoSubtema" HeaderText="Id_ApoyoSubtema"
                                                        ReadOnly="True" SortExpression="IdApoyoSubtema" />
                                                    <asp:BoundField DataField="IdSubtema" Visible="false" HeaderText="IdSubtema"
                                                        SortExpression="IdSubtema" />
                                                    <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                                                        SortExpression="Consecutivo" />
                                                         <asp:BoundField DataField="Descripcion" HeaderText="Descripcion"
                                                        SortExpression="Descripcion" />
                                                       <asp:BoundField DataField="ArchivoApoyo" HeaderText="Archivo Apoyo"
                                                        SortExpression="ArchivoApoyo" />
                                                </Columns>
                                                <PagerTemplate>
                                                    <ul runat="server" id="Pag" class="pagination">
                                                    </ul>
                                                </PagerTemplate>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <SelectedRowStyle CssClass="row-selected" />
                                                <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>
                                        </div>
                                        <div class="form-group">
                                              <asp:LinkButton ID="btnEliminarApoyoSubtema" runat="server" OnClick="btnEliminarApoyoSubtema_Click"
                                    CssClass="btn btn-lg btn-labeled btn-darkorange space-margin">
                                                        <i class="btn-label fa fa-remove"></i>
                                                   <%=Lang_Config.Translate("temas", "Eliminar_apoyo")%>  
                                                            
                                </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>




    

     <asp:SqlDataSource ID="SDSDatosApoyosSubtemas" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM ApoyoSubtema WHERE ([IdSubtema] = @IdSubtema)">
        <SelectParameters>
            <asp:ControlParameter ControlID="GVsubtemas" Name="IdSubtema"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>


    <asp:SqlDataSource ID="SDSnivelesTemas" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSgradosTemas" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLNivelTemas" Name="IdNivel"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SDSasignaturasTemas" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT [IdAsignatura], [IdGrado], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) ORDER BY [Descripcion]">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLGradoTemas" Name="IdGrado"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

      <asp:SqlDataSource ID="SDSdatostemas" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT T.IdAsignatura, A.Descripcion NomAsignatura, T.IdTema, T.Descripcion NomTema, T.Numero 
FROM Tema T, Asignatura A
WHERE (T.IdAsignatura = @IdAsignatura) and (A.IdAsignatura = T.IdAsignatura)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLAsignaturaTemas" Name="IdAsignatura" 
                                PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

      <asp:SqlDataSource ID="SDSdatosubt" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT S.IdCompetencia, C.Descripcion Competencia, S.IdSubtema, S.Descripcion Subtema, S.Numero
FROM Subtema S, Competencia C
WHERE (S.IdTema = @IdTema) and (C.IdCompetencia = S.IdCompetencia)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVTemas" Name="IdTema"
                            PropertyName="SelectedDataKey(IdTema)" />
                    </SelectParameters>
                </asp:SqlDataSource>

    
                <asp:SqlDataSource ID="SDScompetencias" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Competencia] ORDER BY [Descripcion]"></asp:SqlDataSource>

     <asp:SqlDataSource ID="SDSmaterialapoyo" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [ApoyoSubtema] WHERE ([IdSubtema] = @IdSubtema) ORDER BY [Consecutivo]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVsubtemas" Name="IdSubtema"
                            PropertyName="SelectedDataKey(IdSubtema)" />
                    </SelectParameters>
                </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">
    
    <script>
        $(function () {

            $('.tipoArchivoSubt').on('change', function () {
                var tipo = $(this).val();
                if (tipo.trim() == 'Link')
                    swal("!Cuidado!",
                        "Si el enlace es fuera de la pagina incluir '// al inicio' Ejemplo://www.google.com",
                        "warning");


            });

            

        });
    </script>
</asp:Content>

