﻿
Imports Siget
Imports Siget.Entity
Imports System.IO
Imports System.Data.SqlClient

Partial Class administrador_Planteamientos
    Inherits System.Web.UI.Page

#Region "PAGE_LOAD"

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        If IsPostBack Then
            Select Case Trim(DDLtiporespuesta.SelectedValue)
                Case "Multiple"
                    CBCalificaAbierta.Attributes("style") = "display:none;"
                    CBVerificarAbiertaCalificada.Attributes("style") = "display:none;"
                    TBAnchoContenedorAbiertaCalificada.Attributes("style") = "display:none;"
                    CBComparaMayusAbiertaCalificada.Attributes("style") = "display:none;"
                    CBComparaAcentosAbiertaCalificada.Attributes("style") = "display:none;"
                    CBAlfanumericoAbiertaCalificada.Attributes("style") = "display:none;"
                    OpcionToggle.Attributes("style") = "display: block;"

                Case "Abierta"
                    CBCalificaAbierta.Attributes("style") = "display:block;"
                    CBVerificarAbiertaCalificada.Attributes("style") = "display:none;"
                    TBAnchoContenedorAbiertaCalificada.Attributes("style") = "display:none;"
                    CBComparaMayusAbiertaCalificada.Attributes("style") = "display:none;"
                    CBComparaAcentosAbiertaCalificada.Attributes("style") = "display:none;"
                    CBAlfanumericoAbiertaCalificada.Attributes("style") = "display:none;"
                    OpcionToggle.Attributes("style") = "display:none;"
                Case "Abierta Calificada"
                    CBCalificaAbierta.Attributes("style") = "display:none;"
                    CBVerificarAbiertaCalificada.Attributes("style") = "display:block;"
                    TBAnchoContenedorAbiertaCalificada.Attributes("style") = "display:block;"
                    CBComparaMayusAbiertaCalificada.Attributes("style") = "display:block;"
                    CBComparaAcentosAbiertaCalificada.Attributes("style") = "display:block;"
                    CBAlfanumericoAbiertaCalificada.Attributes("style") = "display:block;"
                    CBEliminaPuntuacion.Attributes("style") = "display:block;"
                    OpcionToggle.Attributes("style") = "display:block;"

            End Select

        End If
    End Sub
#End Region

#Region "UPLOAD_COMPLETE"

    Protected Sub ArchivApoyo_UploadedComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles ArchivoApoyo.UploadedComplete
        Beans.FileReference.Files.Insert(0, ArchivoApoyo.PostedFile)
        Beans.FileReference.nameOfFiles.Insert(0, ArchivoApoyo.FileName)
        Beans.FileReference.Activator.Insert(0, True)
    End Sub

    Protected Sub ArchivoApoyoOpcion_UploadedComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles ArchivoApoyoOpcion.UploadedComplete
        Beans.FileReference.Files.Insert(2, ArchivoApoyoOpcion.PostedFile)
        Beans.FileReference.nameOfFiles.Insert(2, ArchivoApoyoOpcion.FileName)
        Beans.FileReference.Activator.Insert(2, True)
    End Sub

    Protected Sub ArchivApoyoAdicional_UploadedComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles ArchivoAdicionalApoyo.UploadedComplete
        Beans.FileReference.Files.Insert(1, ArchivoAdicionalApoyo.PostedFile)
        Beans.FileReference.nameOfFiles.Insert(1, ArchivoAdicionalApoyo.FileName)
        Beans.FileReference.Activator.Insert(1, True)
    End Sub

#End Region

#Region "INSERT"

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrearPlanteamiento.Click

        Dim planteamiento As Planteamiento = New Planteamiento()
        Dim planteamientoRepository As IRepository(Of Planteamiento) = New PlanteamientoRepository()
        Dim opcionRepository As IRepository(Of Opcion) = New OpcionRepository()
        Dim opcion As Opcion = New Opcion()

        If Trim(TBconsecutivo.Text) = String.Empty Then TBconsecutivo.Text = "0"

        Try
            If Beans.FileReference.Activator.Count > 0 And Beans.FileReference.Activator(0) Then
                planteamiento.ArchivoApoyo = Beans.FileReference.nameOfFiles(0)
                planteamiento.TipoArchivoApoyo = DDLTipoArchivo1.SelectedValue
                Beans.FileReference.Files(0).SaveAs(Siget.Config.Global.rutaMaterial + Beans.FileReference.nameOfFiles(0))

                ArchivoApoyoLink.NavigateUrl = Siget.Config.Global.urlMaterial & HttpUtility.HtmlDecode(Beans.FileReference.nameOfFiles(0))
                ArchivoApoyoLink.Text = Beans.FileReference.nameOfFiles(0)
                Beans.FileReference.Activator(0) = False

            End If

            If TBTiempo.Text = Nothing Then
                TBTiempo.Text = 0
            End If

            If Beans.FileReference.Activator.Count > 0 And Beans.FileReference.Activator(1) Then
                planteamiento.ArchivoApoyo2 = Beans.FileReference.nameOfFiles(1)
                planteamiento.TipoArchivoApoyo2 = DDLTipoArchivo2.SelectedValue
                Beans.FileReference.Files(1).SaveAs(Siget.Config.Global.rutaMaterial + Beans.FileReference.nameOfFiles(1))
                ArchivoAdicionalLink.NavigateUrl = Siget.Config.Global.urlMaterial & HttpUtility.HtmlDecode(Beans.FileReference.nameOfFiles(1))
                ArchivoAdicionalLink.Text = Beans.FileReference.nameOfFiles(1)
                Beans.FileReference.Activator(1) = False
            End If

            With planteamiento
                .IdSubtema = DDLSubtemaPlanteamiento.SelectedValue
                .Consecutivo = TBconsecutivo.Text
                .Redaccion = HttpUtility.HtmlDecode(ckEditor.Text)
                .TipoRespuesta = DDLtiporespuesta.SelectedValue
                .Ponderacion = TBponderacion.Text
                .PorcentajeRestarResp = TBPenalizacion.Text
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
                .Ocultar = CBOcultar.Checked
                .EsconderTexto = CBEsconderTexto.Checked
                .Permite2 = CBSegundoVuelta.Checked
                .HabilitaRecorder = CBHabilitaRecorder.Checked
                .Ancho1 = TBAnchoContenedor.Text
                .Verifica = CBVerificar.Checked
                .ComparaAcentos = CBComparaMayus.Checked
                .ComparaMayusculas = CBComparaAcentos.Checked
                .ComparaSoloAlfanumerico = CBAlfaNumerico.Checked
                .EliminaPuntuacion = CBElpuntuacion.Checked
                .Tiempo = CBTiempo.Checked
                .Minutos = TBTiempo.Text

            End With

            planteamientoRepository.Add(planteamiento)

            If planteamiento.TipoRespuesta = "Abierta" Then
                With opcion
                    .IdPlanteamiento = planteamiento.IdPlanteamiento
                    .Redaccion = "Respuesta Abierta"
                    .Correcta = IIf(CBCalifica.Checked, String.Empty, "S")
                    .Consecutiva = "A"
                    .FechaModif = DateTime.Now
                    .Modifico = User.Identity.Name
                    .Esconder = False
                    .SeCalifica = CBCalifica.Checked.ToString
                End With
                opcionRepository.Add(opcion)
            ElseIf planteamiento.TipoRespuesta = "Abierta Calificada" Then
                With opcion
                    .IdPlanteamiento = planteamiento.IdPlanteamiento
                    .Redaccion = "Incorrecta"
                    .Correcta = "N"
                    .Consecutiva = "-"
                    .FechaModif = DateTime.Now
                    .Modifico = User.Identity.Name
                    .Esconder = False
                    .SeCalifica = False
                End With
                opcionRepository.Add(opcion)
            End If

            GVplanteamientos.DataBind()
            CType(New MessageSuccess(GVplanteamientos, Me.GetType, "Se insertó correctamente el planteamiento"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVplanteamientos, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try

    End Sub

    Protected Sub btnInsertarOpcion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInsertarOpcion.Click

        Try


            If Beans.FileReference.Activator.Count > 0 And Beans.FileReference.Activator(2) Then
                SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion (IdPlanteamiento,Redaccion,Correcta,Resp1,Resp2,Consecutiva,ArchivoApoyo,TipoArchivoApoyo,FechaModif,Modifico,Esconder) VALUES (" +
                    GVplanteamientos.SelectedDataKey("IdPlanteamiento").ToString + ",'" + HttpUtility.HtmlDecode(TBRedaccionOpcion.Text).Replace("'", "''").ToString() + "','" + IIf(CBCorrectaS.Checked, "S", "N") + "','" +
                TBresp1.Text + "','" + TBresp2.Text + "','" + DDLConsecutivo.SelectedValue + "','" + Beans.FileReference.nameOfFiles(2) + "','" + DDLTipoArchivoOpcion.SelectedValue.ToString() + "',getdate(),'" + User.Identity.Name.ToString + "','" + CBesconder.Checked.ToString + "')"

                Dim ruta As String
                ruta = Config.Global.rutaMaterial

                Try
                    Dim MiArchivo As FileInfo = New FileInfo(ruta & Beans.FileReference.nameOfFiles(2))
                    If MiArchivo.Exists Then
                        msgSuccess1.show("Éxito", "El archivo que está intentando cargar ya existía, revise que sea el correcto, de lo contrario, actualice el registro.")
                    Else
                        Beans.FileReference.Files(2).SaveAs(ruta & Beans.FileReference.nameOfFiles(2))
                        ArchivoApoyoOpcionLink.Text = "Archivo cargado: " & Beans.FileReference.nameOfFiles(2)
                    End If
                    Beans.FileReference.Activator(2) = False
                Catch ex As Exception
                    Utils.LogManager.ExceptionLog_InsertEntry(ex)
                    CType(New MessageError(GVplanteamientos, Me.GetType, ex.Message.ToString()), MessageError).show()
                End Try
                Beans.FileReference.Activator(2) = False
            Else
                SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion (IdPlanteamiento,Redaccion,Correcta,Resp1,Resp2,Consecutiva,FechaModif,Modifico,Esconder) VALUES (" +
                      GVplanteamientos.SelectedDataKey("IdPlanteamiento").ToString + ",'" + HttpUtility.HtmlDecode(TBRedaccionOpcion.Text).Replace("'", "''") + "','" +
                   IIf(CBCorrectaS.Checked, "S", "N") + "','" + TBresp1.Text + "','" + TBresp2.Text + "','" +
                   DDLConsecutivo.SelectedValue + "',getdate(), '" + User.Identity.Name.ToString + "','" +
                   CBesconder.Checked.ToString + "')"
            End If
            SDSopciones.Insert()
            LoadPreview()
            CType(New MessageSuccess(GVplanteamientos, Me.GetType, "El registro ha sido guardado"), MessageSuccess).show()
            GVopciones.DataBind()
        Catch ex As Exception
            CType(New MessageError(GVplanteamientos, Me.GetType, "Verifica los campos"), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try
    End Sub
#End Region

#Region "UPDATE"
    Protected Sub btnActualizarPlanteamiento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarPlanteamiento.Click

        Try
            Dim planteamientoRepository As IRepository(Of Planteamiento) = New PlanteamientoRepository()
            Dim opcionRepository As IRepository(Of Opcion) = New OpcionRepository()
            Dim opcion As Opcion
            Dim planteamiento As Planteamiento = planteamientoRepository.FindById(GVplanteamientos.SelectedDataKey("IdPlanteamiento"))


            If Beans.FileReference.Activator.Count > 0 And Beans.FileReference.Activator(0) Then
                Planteamiento.ArchivoApoyo = Beans.FileReference.nameOfFiles(0)
                Planteamiento.TipoArchivoApoyo = DDLTipoArchivo1.SelectedValue

                If File.Exists(Siget.Config.Global.rutaMaterial & GVplanteamientos.SelectedDataKey("ArchivoApoyo")) Then
                    File.Delete(Siget.Config.Global.rutaMaterial & GVplanteamientos.SelectedDataKey("ArchivoApoyo"))

                End If
                Beans.FileReference.Files(0).SaveAs(Siget.Config.Global.rutaMaterial + Beans.FileReference.nameOfFiles(0))
                ArchivoApoyoLink.NavigateUrl = Siget.Config.Global.urlMaterial & HttpUtility.HtmlDecode(Beans.FileReference.nameOfFiles(0))
                ArchivoApoyoLink.Text = Beans.FileReference.nameOfFiles(0)
                Beans.FileReference.Activator(0) = False
            Else
                If DDLTipoArchivo1.SelectedValue = "Ninguno" Then
                    If File.Exists(Siget.Config.Global.urlMaterial & HttpUtility.HtmlDecode(GVplanteamientos.SelectedDataKey("ArchivoApoyo1"))) Then
                        File.Delete(Siget.Config.Global.urlMaterial & HttpUtility.HtmlDecode(GVplanteamientos.SelectedDataKey("ArchivoApoyo1")))
                    End If
                    ArchivoApoyoLink.Text = String.Empty
                    planteamiento.ArchivoApoyo = String.Empty
                    planteamiento.TipoArchivoApoyo = String.Empty
                End If
            End If

            If Beans.FileReference.Activator.Count > 0 And Beans.FileReference.Activator(1) Then
                Planteamiento.ArchivoApoyo2 = Beans.FileReference.nameOfFiles(1)
                Planteamiento.TipoArchivoApoyo2 = DDLTipoArchivo2.SelectedValue

                If File.Exists(Siget.Config.Global.rutaMaterial & GVplanteamientos.SelectedDataKey("ArchivoApoyo2")) Then
                    File.Delete(Siget.Config.Global.rutaMaterial & GVplanteamientos.SelectedDataKey("ArchivoApoyo2"))

                End If
                Beans.FileReference.Files(1).SaveAs(Siget.Config.Global.rutaMaterial + Beans.FileReference.nameOfFiles(1))
                ArchivoAdicionalLink.NavigateUrl = Siget.Config.Global.urlMaterial & HttpUtility.HtmlDecode(Beans.FileReference.nameOfFiles(1))
                ArchivoAdicionalLink.Text = Beans.FileReference.nameOfFiles(1)

                Beans.FileReference.Activator(1) = False
            Else
                If DDLTipoArchivo2.SelectedValue = "Ninguno" Then
                    If File.Exists(Siget.Config.Global.urlMaterial & HttpUtility.HtmlDecode(GVplanteamientos.SelectedDataKey("ArchivoApoyo1"))) Then
                        File.Delete(Siget.Config.Global.urlMaterial & HttpUtility.HtmlDecode(GVplanteamientos.SelectedDataKey("ArchivoApoyo1")))
                    End If
                    ArchivoAdicionalLink.Text = String.Empty
                    planteamiento.ArchivoApoyo2 = String.Empty
                    planteamiento.TipoArchivoApoyo2 = String.Empty
                End If
            End If

            If Trim(Planteamiento.TipoRespuesta) = "Abierta" Then
                Opcion = OpcionRepository.FindById((Planteamiento.Opcions.FirstOrDefault()).IdOpcion)
                Opcion.SeCalifica = CBCalifica.Checked.ToString()
                OpcionRepository.Update(Opcion)
            End If

            If Trim(TBTiempo.Text) = Nothing Then
                TBTiempo.Text = 0
            End If


            With planteamiento
                .Consecutivo = TBconsecutivo.Text
                .Redaccion = HttpUtility.HtmlDecode(ckEditor.Text)
                .TipoRespuesta = DDLtiporespuesta.SelectedValue
                .Ponderacion = TBponderacion.Text
                .PorcentajeRestarResp = TBPenalizacion.Text
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
                .Ocultar = CBOcultar.Checked
                .EsconderTexto = CBEsconderTexto.Checked
                .Permite2 = CBSegundoVuelta.Checked
                .HabilitaRecorder = CBHabilitaRecorder.Checked
                .Ancho1 = TBAnchoContenedor.Text
                .Verifica = CBVerificar.Checked
                .ComparaAcentos = CBComparaMayus.Checked
                .ComparaMayusculas = CBComparaAcentos.Checked
                .ComparaSoloAlfanumerico = CBAlfaNumerico.Checked
                .EliminaPuntuacion = CBElpuntuacion.Checked
                .Tiempo = CBTiempo.Checked
                .Minutos = TBTiempo.Text
            End With
            planteamientoRepository.Update(planteamiento)
            GVplanteamientos.DataBind()

            CType(New MessageSuccess(btnActualizarPlanteamiento, Me.GetType, "Se actualizó correctamente el planteamiento"), MessageSuccess).show()

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(btnActualizarPlanteamiento, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try
    End Sub


    Protected Sub btnActualizarOpcion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarOpcion.Click

        Dim ruta As String
        ruta = Config.Global.rutaMaterial
        msgSuccess1.hide()
        msgError1.hide()

        Try
            If Beans.FileReference.Activator.Count > 0 And Beans.FileReference.Activator(2) Then
                SDSopciones.UpdateCommand = "SET dateformat dmy; UPDATE Opcion SET Redaccion = '" +
                   HttpUtility.HtmlDecode(TBRedaccionOpcion.Text).Replace("'", "''") + "', Correcta = '" +
                   IIf(CBCorrectaS.Checked, "S", "N") +
                    "', Resp1 = '" + TBresp1.Text + "', Resp2 = '" + TBresp2.Text +
                    "', Consecutiva = '" + DDLConsecutivo.SelectedValue + "', ArchivoApoyo = '" + Beans.FileReference.nameOfFiles(2) +
                    "', TipoArchivoApoyo = '" + DDLTipoArchivoOpcion.SelectedValue +
                    "', FechaModif = getdate(), Modifico = '" + User.Identity.Name.ToString + "', Esconder = '" +
                    CBesconder.Checked.ToString + "' WHERE IdOpcion =" + GVopciones.SelectedDataKey("IdOpcion").ToString

                Try
                    Beans.FileReference.Files(2).SaveAs(ruta & Beans.FileReference.nameOfFiles(2))
                    ArchivoApoyoOpcionLink.Text = "Archivo cargado: " & Beans.FileReference.nameOfFiles(2)
                    ArchivoApoyoOpcionLink.NavigateUrl = ruta & Beans.FileReference.nameOfFiles(2)
                Catch ex As Exception
                    Utils.LogManager.ExceptionLog_InsertEntry(ex)
                    msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
                End Try
                Beans.FileReference.Activator(2) = False
            Else
                If DDLTipoArchivoOpcion.SelectedValue = "Ninguno" Then
                    If File.Exists(ruta & GVopciones.SelectedDataKey("ArchivoApoyo").ToString()) Then
                        File.Delete(ruta & GVopciones.SelectedDataKey("ArchivoApoyo").ToString())
                    End If
                    ArchivoApoyoOpcionLink.Text = String.Empty
                End If

                SDSopciones.UpdateCommand = "SET dateformat dmy; UPDATE Opcion SET Redaccion = '" + HttpUtility.HtmlDecode(TBRedaccionOpcion.Text).Replace("'", "''") + "', Correcta = '" +
                      IIf(CBCorrectaS.Checked, "S", "N") + "',ArchivoApoyo='" + String.Empty + "', " +
                      " Resp1 = '" + TBresp1.Text + "', Resp2 = '" + TBresp2.Text + "', Consecutiva = '" +
                DDLConsecutivo.SelectedValue() + "', FechaModif = getdate(), Modifico = '" +
                User.Identity.Name.ToString + "', Esconder = '" + CBesconder.Checked.ToString +
                "' WHERE IdOpcion =" + GVopciones.SelectedDataKey("IdOpcion").ToString
            End If
            SDSopciones.Update()
            CType(New MessageSuccess(GVplanteamientos, Me.GetType, "El registro ha sido actualizado"), MessageSuccess).show()
            GVopciones.DataBind()
            LoadPreview()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVplanteamientos, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try
    End Sub
#End Region

#Region "ROW_CREATED"
    Protected Sub GVplanteamientos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVplanteamientos.RowCreated

        Dim cociente As Integer = (GVplanteamientos.PageCount / 15)
        Dim moduloIndex As Integer = (GVplanteamientos.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVplanteamientos.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVplanteamientos.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVplanteamientos.PageIndex
                final = IIf((inicial + 15 <= GVplanteamientos.PageCount), inicial + 15, GVplanteamientos.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVplanteamientos.PageCount, inicial + 15, GVplanteamientos.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVplanteamientos.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next
        End If
    End Sub
#End Region

#Region "ROW_DATA_BOUND"
    Protected Sub GVplanteamientos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVplanteamientos.RowDataBound

        Dim cociente As Integer = (GVplanteamientos.PageCount) / 15
        Dim moduloIndex As Integer = (GVplanteamientos.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVplanteamientos.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVplanteamientos.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVplanteamientos.PageIndex
            final = IIf((inicial + 15 <= GVplanteamientos.PageCount), inicial + 15, GVplanteamientos.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVplanteamientos.PageCount), inicial + 15, GVplanteamientos.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVplanteamientos, "Select$" & e.Row.RowIndex))
            Dim s As String = Regex.Replace(HttpUtility.HtmlDecode(e.Row.Cells(3).Text), "<[^>]*(>|$)", String.Empty)
            Dim l As Integer = s.Length
            Dim continua As String = ""
            If l > 100 Then
                l = 100
                continua = "...(Continúa)"
            End If
            e.Row.Cells(3).Text = s.Substring(0, l) & continua
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVplanteamientos, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVopciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVopciones.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVopciones, "Select$" & e.Row.RowIndex))
            e.Row.Cells(2).Text = Regex.Replace(HttpUtility.HtmlDecode(e.Row.Cells(2).Text), "<[^>]*(>|$)", String.Empty)

        End If
    End Sub
#End Region

#Region "SELECTED_INDEX_CHANGED"

    Protected Sub DDLtiporespuesta_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLtiporespuesta.SelectedIndexChanged

        If Trim(DDLtiporespuesta.SelectedValue) = "Abierta" Then
            PanelOpciones.Visible = False
        Else
            PanelOpciones.Visible = True
        End If

    End Sub

    Protected Sub GVplanteamientos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVplanteamientos.SelectedIndexChanged

        If Not IsDBNull(GVplanteamientos.SelectedDataKey("Redaccion")) Then
            ckEditor.Text = GVplanteamientos.SelectedDataKey("Redaccion")
        End If

        TBconsecutivo.Text = IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("Consecutivo")), GVplanteamientos.SelectedDataKey("Consecutivo"), String.Empty)
        TBPenalizacion.Text = IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("PorcentajeRestarResp")), GVplanteamientos.SelectedDataKey("PorcentajeRestarResp"), String.Empty)
        TBponderacion.Text = IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("Ponderacion")), GVplanteamientos.SelectedDataKey("Ponderacion"), String.Empty)
        DDLtiporespuesta.SelectedValue = Trim(GVplanteamientos.SelectedDataKey("TipoRespuesta"))
        If Not IsDBNull(GVplanteamientos.SelectedDataKey("Redaccion")) Then
            planteamientoSelected.Text = Regex.Replace(HttpUtility.HtmlDecode(GVplanteamientos.SelectedDataKey("Redaccion")), "<[^>]*(>|$)", String.Empty)
        Else
            planteamientoSelected.Text = String.Empty
        End If

        Select Case Trim(GVplanteamientos.SelectedDataKey("TipoRespuesta"))
            Case "Multiple"
                CBCalificaAbierta.Attributes("style") = "display:none;"
                CBVerificarAbiertaCalificada.Attributes("style") = "display:none;"
                TBAnchoContenedorAbiertaCalificada.Attributes("style") = "display:none;"
                CBComparaMayusAbiertaCalificada.Attributes("style") = "display:none;"
                CBComparaAcentosAbiertaCalificada.Attributes("style") = "display:none;"
                CBAlfanumericoAbiertaCalificada.Attributes("style") = "display:none;"
                OpcionToggle.Attributes("style") = "display: block;"
                LoadPreview()
            Case "Abierta"
                CBCalificaAbierta.Attributes("style") = "display:block;"
                CBVerificarAbiertaCalificada.Attributes("style") = "display:none;"
                TBAnchoContenedorAbiertaCalificada.Attributes("style") = "display:none;"
                CBComparaMayusAbiertaCalificada.Attributes("style") = "display:none;"
                CBComparaAcentosAbiertaCalificada.Attributes("style") = "display:none;"
                CBAlfanumericoAbiertaCalificada.Attributes("style") = "display:none;"
                OpcionToggle.Attributes("style") = "display:none;"
                Siget.Utils.GeneralUtils.CleanAllWithLabels(PanelPrevisualizar.Controls)
            Case "Abierta Calificada"
                CBCalificaAbierta.Attributes("style") = "display:none;"
                CBVerificarAbiertaCalificada.Attributes("style") = "display:block;"
                TBAnchoContenedorAbiertaCalificada.Attributes("style") = "display:block;"
                CBComparaMayusAbiertaCalificada.Attributes("style") = "display:block;"
                CBComparaAcentosAbiertaCalificada.Attributes("style") = "display:block;"
                CBAlfanumericoAbiertaCalificada.Attributes("style") = "display:block;"
                CBEliminaPuntuacion.Attributes("style") = "display:block;"
                OpcionToggle.Attributes("style") = "display:block;"
                Siget.Utils.GeneralUtils.CleanAllWithLabels(PanelPrevisualizar.Controls)

        End Select


        If Not IsDBNull(GVplanteamientos.SelectedDataKey("TipoArchivoApoyo")) Then
            DDLTipoArchivo1.SelectedValue = GVplanteamientos.SelectedDataKey("TipoArchivoApoyo")
            ArchivoApoyoLink.NavigateUrl = Siget.Config.Global.urlMaterial & HttpUtility.HtmlDecode(GVplanteamientos.SelectedDataKey("ArchivoApoyo"))
            ArchivoApoyoLink.Text = GVplanteamientos.SelectedDataKey("ArchivoApoyo")
        End If
        If Not IsDBNull(GVplanteamientos.SelectedDataKey("TipoArchivoApoyo2")) Then
            DDLTipoArchivo2.SelectedValue = GVplanteamientos.SelectedDataKey("TipoArchivoApoyo2")
            ArchivoAdicionalLink.NavigateUrl = Siget.Config.Global.urlMaterial & HttpUtility.HtmlDecode(GVplanteamientos.SelectedDataKey("ArchivoApoyo2"))
            ArchivoAdicionalLink.Text = GVplanteamientos.SelectedDataKey("ArchivoApoyo2")
        End If

        CBCalifica.Checked = IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("Verifica")), GVplanteamientos.SelectedDataKey("Verifica"), False)
        TBAnchoContenedor.Text = IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("Ancho1")), GVplanteamientos.SelectedDataKey("Ancho1"), String.Empty)
        CBComparaAcentos.Checked = IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("ComparaAcentos")), GVplanteamientos.SelectedDataKey("ComparaAcentos"), False)
        CBAlfaNumerico.Checked = IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("ComparaSoloAlfanumericos")), GVplanteamientos.SelectedDataKey("ComparaSoloAlfanumericos"), False)
        CBComparaMayus.Checked = IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("ComparaMayusculas")), GVplanteamientos.SelectedDataKey("ComparaMayusculas"), False)
        CBOcultar.Checked = IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("Ocultar")), GVplanteamientos.SelectedDataKey("Ocultar"), False)
        CBElpuntuacion.Checked = IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("EliminaPuntuacion")), GVplanteamientos.SelectedDataKey("EliminaPuntuacion"), False)
        CBEsconderTexto.Checked = IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("EsconderTexto")), GVplanteamientos.SelectedDataKey("EsconderTexto"), False)
        CBSegundoVuelta.Checked = IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("Permite2")), GVplanteamientos.SelectedDataKey("Permite2"), False)
        CBHabilitaRecorder.Checked = IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("HabilitaRecorder")), GVplanteamientos.SelectedDataKey("HabilitaRecorder"), False)
        CBTiempo.Checked = IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("Tiempo")), GVplanteamientos.SelectedDataKey("Tiempo"), False)
        If CBTiempo.Checked Then
            TBTiempo.Attributes("style") = "display:block;"
            TBTiempo.Text = GVplanteamientos.SelectedDataKey("Minutos")
        End If

        btnEliminarPlanteamiento.Visible = True
        btnActualizarPlanteamiento.Visible = True

    End Sub

    Sub LoadPreview()
        Dim repositoryPlanteamiento As IRepository(Of Planteamiento) = New PlanteamientoRepository()

        Dim IdPlant = GVplanteamientos.SelectedDataKey("IdPlanteamiento").ToString()
        Dim strConexion As String
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        Dim Redaccion As String

        miComando = objConexion.CreateCommand
        miComando.CommandText = "SELECT * FROM Planteamiento where idPlanteamiento = " + IdPlant
        Siget.Utils.GeneralUtils.CleanAllWithLabels(PanelPrevisualizar.Controls)

        If repositoryPlanteamiento.FindById(IdPlant).Opcions.Count > 0 Then


            Try
                'Abrir la conexión y leo los registros del Planteamiento
                objConexion.Open()
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read() 'Leo para poder accesarlos

                If misRegistros.Item("EsconderTexto") Then
                    LblPlanteamiento.Visible = False

                Else
                    If Not IsDBNull(misRegistros.Item("Redaccion")) Then

                        LblPlanteamiento.Visible = True
                        Redaccion = Replace(HttpUtility.HtmlDecode(misRegistros.Item("Redaccion")), "*salto*", "<BR>")
                        Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")

                        If misRegistros.Item("Consecutivo") > 0 Then
                            LblPlanteamiento.Text = CStr(misRegistros.Item("Consecutivo")) + ".- " + Redaccion
                        Else
                            'OJO: Aqui, en el despliegue para alumnos, tendria que usar un contador que enumere el planteamiento
                            LblPlanteamiento.Text = "#.- " + Redaccion
                        End If
                    Else
                        If misRegistros.Item("Consecutivo") > 0 Then
                            LblPlanteamiento.Text = CStr(misRegistros.Item("Consecutivo")) + ".- "
                        Else
                            'OJO: Aqui, en el despliegue para alumnos, tendria que usar un contador que enumere el planteamiento
                            LblPlanteamiento.Text = "#.- "
                        End If

                    End If
                End If


                'Saco la Referencia bibliográfica del reactivo (si la hay) que consta de 3 campos
                Dim Referencia As String
                Referencia = ""
                'If (Not IsDBNull(misRegistros.Item("Libro"))) Then
                'Referencia = HttpUtility.HtmlDecode(misRegistros.Item("Libro")) + ", "
                'End If
                'If (Not IsDBNull(misRegistros.Item("Autor"))) Then
                'Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Autor")) + ", "
                'End If
                'If (Not IsDBNull(misRegistros.Item("Editorial"))) Then
                'Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Editorial"))
                'End If
                '***

                If Trim(misRegistros.Item("Libro").ToString <> "") Then
                    Referencia = HttpUtility.HtmlDecode(misRegistros.Item("Libro")) + ", "
                End If
                If Trim(misRegistros.Item("Autor").ToString <> "") Then
                    Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Autor")) + ", "
                End If
                If Trim(misRegistros.Item("Editorial").ToString <> "") Then
                    Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Editorial"))
                End If
                '***
                If Trim(Referencia) <> "" Then
                    LblReferencia.Text = "(REFERENCIA: " + Referencia + ")"
                End If

                'Cargo el archivo de apoyo 1, siempre deberán estar en el directorio material
                If (Not IsDBNull(misRegistros.Item("TipoArchivoApoyo"))) And (Not IsDBNull(misRegistros.Item("ArchivoApoyo"))) Then
                    If Trim(misRegistros.Item("ArchivoApoyo")).Length > 0 Then 'Esta comparacion la hago aparte del IF superior porque marca error al querer sacar tamaño a valor NULL
                        If Trim(misRegistros.Item("TipoArchivoApoyo")) = "Imagen" Then
                            'ImPlanteamiento.Width = 300
                            'ImPlanteamiento.Height = 300
                            ImPlanteamiento.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                            ImPlanteamiento.Visible = True
                            HLapoyoplanteamiento.Visible = False
                        ElseIf Trim(misRegistros.Item("TipoArchivoApoyo")) = "Video" Or Trim(misRegistros.Item("TipoArchivoApoyo")) = "Audio" Then
                            LblVideo.Text = "<embed src=""" & Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo") + """ width=""280"" height=""227"" autostart=""true"" controller=""true"" align=""center""></embed>"
                            ImPlanteamiento.Visible = False
                            HLapoyoplanteamiento.Visible = False
                        Else
                            'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                            '********PENDIENTE***************
                            HLapoyoplanteamiento.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                            'HLapoyoplanteamiento.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                            HLapoyoplanteamiento.Text = "APOYO: " + Trim(misRegistros.Item("ArchivoApoyo").ToString)
                            HLapoyoplanteamiento.Visible = True
                            ImPlanteamiento.Visible = False
                        End If
                    Else
                        ImPlanteamiento.Visible = False
                        HLapoyoplanteamiento.Visible = False
                    End If
                End If

                'Cargo el archivo 2 de apoyo si lo hay, siempre deberán estar en el directorio material
                If (Not IsDBNull(misRegistros.Item("TipoArchivoApoyo2"))) And (Not IsDBNull(misRegistros.Item("ArchivoApoyo2"))) Then
                    If Trim(misRegistros.Item("ArchivoApoyo2")).Length > 0 Then 'Esta comparacion la hago aparte del IF superior porque marca error al querer sacar tamaño a valor NULL
                        If Trim(misRegistros.Item("TipoArchivoApoyo2")) = "Imagen" Then
                            'ImPlanteamiento2.Width = 300
                            'ImPlanteamiento2.Height = 300
                            ImPlanteamiento2.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo2")
                            ImPlanteamiento2.Visible = True
                            HLapoyoplanteamiento2.Visible = False
                        ElseIf Trim(misRegistros.Item("TipoArchivoApoyo2")) = "Video" Or Trim(misRegistros.Item("TipoArchivoApoyo2")) = "Audio" Then
                            LblVideo2.Text = "<embed src=""" & Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo2") + """ width=""280"" height=""227"" autostart=""true"" controller=""true"" align=""center""></embed>"
                            ImPlanteamiento2.Visible = False
                            HLapoyoplanteamiento2.Visible = False
                        Else
                            'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                            '********PENDIENTE***************
                            HLapoyoplanteamiento2.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo2")
                            'HLapoyoplanteamiento2.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                            HLapoyoplanteamiento2.Text = "APOYO: " + Trim(misRegistros.Item("ArchivoApoyo2").ToString)
                            HLapoyoplanteamiento2.Visible = True
                            ImPlanteamiento2.Visible = False
                        End If
                    Else
                        ImPlanteamiento2.Visible = False
                        HLapoyoplanteamiento2.Visible = False
                    End If
                End If

                'OPCIONES
                If Trim(misRegistros.Item("TipoRespuesta")) = "Multiple" Then
                    PnlOpciones.Visible = True
                    misRegistros.Close() 'Cierro la tabla de Planteamiento
                    'Algoritmo respuesta multiple
                    miComando.CommandText = "SELECT * FROM Opcion where Esconder = 'False' and idPlanteamiento = " + IdPlant + " order by Consecutiva"
                    misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                    misRegistros.Read() 'Leo para poder accesarlos

                    'DEBO USAR: (Primero colocar un PlaceHolder y llamarlo "PanelOpciones"
                    'Dim TextoOpc1 As New Label
                    'TextoOpc1.Text = misRegistros.Item("Redaccion")
                    'PanelOpciones.Controls.Add(TextoOpc1) 'ó Controls.AddAt(1, TextoOpc1)
                    'Dim TblOpciones As New Table
                    Redaccion = String.Empty
                    If Not IsDBNull(misRegistros.Item("Redaccion")) Then
                        Redaccion = Replace(HttpUtility.HtmlDecode(misRegistros.Item("Redaccion")), "*salto*", "<BR>")
                        Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                        'TextoOpc1.Text = "<font color='blue'>" + CStr(misRegistros.Item("Consecutiva")) + ") </font> " + Redaccion
                    End If
                    TextoOpc1.Text = Redaccion
                    Opcion1.Visible = True
                    Opcion1.Text = CStr(misRegistros.Item("Consecutiva"))
                    If Not IsDBNull(misRegistros.Item("TipoArchivoApoyo")) Then
                        If misRegistros.Item("TipoArchivoApoyo") = "Imagen" Then
                            'ImOpc1.Width = 300
                            'ImOpc1.Height = 300
                            ImOpc1.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                            HLapoyoopc1.Visible = False
                        Else
                            'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                            HLapoyoopc1.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                            HLapoyoopc1.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                            ImOpc1.Visible = False
                        End If
                    Else
                        ImOpc1.Visible = False
                        HLapoyoopc1.Visible = False
                    End If

                    If misRegistros.Read() Then  'PASO A OPCION 2
                        Redaccion = Replace(HttpUtility.HtmlDecode(misRegistros.Item("Redaccion")), "*salto*", "<BR>")
                        Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                        'TextoOpc2.Text = "<font color='blue'>" + CStr(misRegistros.Item("Consecutiva")) + ") </font> " + Redaccion
                        TextoOpc2.Text = Redaccion
                        Opcion2.Visible = True
                        Opcion2.Text = CStr(misRegistros.Item("Consecutiva"))
                        If Not IsDBNull(misRegistros.Item("TipoArchivoApoyo")) Then
                            If misRegistros.Item("TipoArchivoApoyo") = "Imagen" Then
                                'ImOpc2.Width = 300
                                'ImOpc2.Height = 300
                                ImOpc2.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                                HLapoyoopc2.Visible = False
                            Else
                                'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                                HLapoyoopc2.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                                HLapoyoopc2.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                                ImOpc2.Visible = False
                            End If
                        Else
                            ImOpc2.Visible = False
                            HLapoyoopc2.Visible = False
                        End If
                    Else
                        ImOpc2.Visible = False
                        HLapoyoopc2.Visible = False
                        Opcion2.Visible = False
                    End If

                    If misRegistros.Read() Then 'PASO A OPCION 3
                        Redaccion = Replace(HttpUtility.HtmlDecode(misRegistros.Item("Redaccion")), "*salto*", "<BR>")
                        Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                        'TextoOpc3.Text = "<font color='blue'>" + CStr(misRegistros.Item("Consecutiva")) + ") </font> " + Redaccion
                        TextoOpc3.Text = Redaccion
                        Opcion3.Visible = True
                        Opcion3.Text = CStr(misRegistros.Item("Consecutiva"))
                        If Not IsDBNull(misRegistros.Item("TipoArchivoApoyo")) Then
                            If misRegistros.Item("TipoArchivoApoyo") = "Imagen" Then
                                'ImOpc3.Width = 300
                                'ImOpc3.Height = 300
                                ImOpc3.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                                HLapoyoopc3.Visible = False
                            Else
                                'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                                HLapoyoopc3.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                                HLapoyoopc3.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                                ImOpc3.Visible = False
                            End If
                        Else
                            ImOpc3.Visible = False
                            HLapoyoopc3.Visible = False
                        End If
                    Else
                        ImOpc3.Visible = False
                        HLapoyoopc3.Visible = False
                        Opcion3.Visible = False
                    End If

                    If misRegistros.Read() Then 'PASO A OPCION 4
                        Redaccion = Replace(HttpUtility.HtmlDecode(misRegistros.Item("Redaccion")), "*salto*", "<BR>")
                        Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                        'TextoOpc4.Text = "<font color='blue'>" + CStr(misRegistros.Item("Consecutiva")) + ") </font> " + Redaccion
                        TextoOpc4.Text = Redaccion
                        Opcion4.Visible = True
                        Opcion4.Text = CStr(misRegistros.Item("Consecutiva"))
                        If Not IsDBNull(misRegistros.Item("TipoArchivoApoyo")) Then
                            If misRegistros.Item("TipoArchivoApoyo") = "Imagen" Then
                                'ImOpc4.Width = 300
                                'ImOpc4.Height = 300
                                ImOpc4.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                                HLapoyoopc4.Visible = False
                            Else
                                'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                                HLapoyoopc4.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                                HLapoyoopc4.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                                ImOpc4.Visible = False
                            End If
                        Else
                            ImOpc4.Visible = False
                            HLapoyoopc4.Visible = False
                        End If
                    Else
                        ImOpc4.Visible = False
                        HLapoyoopc4.Visible = False
                        Opcion4.Visible = False
                    End If

                    If misRegistros.Read() Then 'PASO A OPCION 5
                        Redaccion = Replace(HttpUtility.HtmlDecode(misRegistros.Item("Redaccion")), "*salto*", "<BR>")
                        Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                        'TextoOpc5.Text = "<font color='blue'>" + CStr(misRegistros.Item("Consecutiva")) + ") </font> " + Redaccion
                        TextoOpc5.Text = Redaccion
                        Opcion5.Visible = True
                        Opcion5.Text = CStr(misRegistros.Item("Consecutiva"))
                        If Not IsDBNull(misRegistros.Item("TipoArchivoApoyo")) Then
                            If misRegistros.Item("TipoArchivoApoyo") = "Imagen" Then
                                'ImOpc5.Width = 300
                                'ImOpc5.Height = 300
                                ImOpc5.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                                HLapoyoopc5.Visible = False
                            Else
                                'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                                HLapoyoopc5.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                                HLapoyoopc5.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                                ImOpc5.Visible = False
                            End If
                        Else
                            ImOpc5.Visible = False
                            HLapoyoopc5.Visible = False
                        End If
                    Else
                        ImOpc5.Visible = False
                        HLapoyoopc5.Visible = False
                        Opcion5.Visible = False
                    End If

                    If misRegistros.Read() Then 'PASO A OPCION 6
                        Redaccion = Replace(HttpUtility.HtmlDecode(misRegistros.Item("Redaccion")), "*salto*", "<BR>")
                        Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                        'TextoOpc6.Text = "<font color='blue'>" + CStr(misRegistros.Item("Consecutiva")) + ") </font> " + Redaccion
                        TextoOpc6.Text = Redaccion
                        Opcion6.Visible = True
                        Opcion6.Text = CStr(misRegistros.Item("Consecutiva"))
                        If Not IsDBNull(misRegistros.Item("TipoArchivoApoyo")) Then
                            If misRegistros.Item("TipoArchivoApoyo") = "Imagen" Then
                                'ImOpc6.Width = 300
                                'ImOpc6.Height = 300
                                ImOpc6.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                                HLapoyoopc6.Visible = False
                            Else
                                'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                                HLapoyoopc6.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                                HLapoyoopc6.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                                ImOpc6.Visible = False
                            End If
                        Else
                            ImOpc6.Visible = False
                            HLapoyoopc6.Visible = False
                        End If
                    Else
                        ImOpc6.Visible = False
                        HLapoyoopc6.Visible = False
                        Opcion6.Visible = False
                    End If
                ElseIf Trim(misRegistros.Item("TipoRespuesta")) = "Abierta Calificada" Then
                    PanelAbiertaAutomatica.Visible = True
                    tbRespuestaAbiertaAutomatica.Width = misRegistros.Item("Ancho1").ToString()

                    ' guardo las respuestas correctas de las opciones en el hfVerifica para comparar sin postback
                    If misRegistros.Item("Verifica") Then
                        VerificarAA.Visible = True
                    Else
                        VerificarAA.Visible = False
                    End If
                Else
                    'Aquí iría el Algoritmo cuando el tipo de respuesta es de "Completar"
                    '********PENDIENTE***************
                    PnlOpciones.Visible = False


                End If

                misRegistros.Close()
                objConexion.Close()

            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try

        End If

    End Sub

    Protected Sub GVopciones_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVopciones.SelectedIndexChanged
        btnEliminarOpciones.Visible = True
        TBRedaccionOpcion.Text = GVopciones.SelectedDataKey("Redaccion")
        If Trim(GVopciones.SelectedDataKey("Correcta")) = "N" Then
            CBCorrectaN.Checked = True
            CBCorrectaS.Checked = False
        Else
            CBCorrectaS.Checked = True
            CBCorrectaN.Checked = False
        End If
        DDLConsecutivo.SelectedValue = GVopciones.SelectedDataKey("Consecutiva")
        CBesconder.Checked = GVopciones.SelectedDataKey("Esconder")
        DDLTipoArchivoOpcion.SelectedValue = IIf(Not IsDBNull(GVopciones.SelectedDataKey("TipoArchivoApoyo")), GVopciones.SelectedDataKey("TipoArchivoApoyo"), "0")
        ArchivoApoyoOpcionLink.NavigateUrl = IIf(Not IsDBNull(GVopciones.SelectedDataKey("ArchivoApoyo")), Config.Global.urlMaterial & GVopciones.SelectedDataKey("ArchivoApoyo"), String.Empty)
        ArchivoApoyoOpcionLink.Text = IIf(Not IsDBNull(GVopciones.SelectedDataKey("ArchivoApoyo")), GVopciones.SelectedDataKey("ArchivoApoyo"), String.Empty)

    End Sub

#End Region

#Region "DELETE"

    Protected Sub btnEliminarPlanteamiento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarPlanteamiento.Click
        Dim planteamientoRepository As IRepository(Of Planteamiento) = New PlanteamientoRepository()
        Dim opcionRepository As IRepository(Of Opcion) = New OpcionRepository()
        Dim flag As Boolean = False

        Try
            Dim planteamiento As Planteamiento = planteamientoRepository.FindById(GVplanteamientos.SelectedDataKey("IdPlanteamiento"))
            Dim filterOpciones As Opcion = (From r In opcionRepository.List Where r.IdPlanteamiento = planteamiento.IdPlanteamiento Select r).FirstOrDefault()

            If Utils.FileUtils.isThisFilesWithMultipleAssociations(IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("ArchivoApoyo")), GVplanteamientos.SelectedDataKey("ArchivoApoyo"), String.Empty)) Or
                         Utils.FileUtils.isThisFilesWithMultipleAssociations(IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("ArchivoApoyo2")), GVplanteamientos.SelectedDataKey("ArchivoApoyo2"), String.Empty)) Then
                CType(New MessageInfo(btnEliminarPlanteamiento, Me.GetType(), "El/los archivo/archivos fisicos no se han borrado pero sí el registro. El archivo de apoyo está asignado a otros subtemas/opciones/planteamientos. "), MessageInfo).show()
                flag = True
            Else
                If File.Exists(GVplanteamientos.SelectedDataKey("ArchivoApoyo")) Then
                    File.Delete(Siget.Config.Global.rutaMaterial & GVplanteamientos.SelectedDataKey("ArchivoApoyo"))
                ElseIf File.Exists(GVplanteamientos.SelectedDataKey("ArchivoApoyo2")) Then
                    File.Delete(Siget.Config.Global.rutaMaterial & GVplanteamientos.SelectedDataKey("ArchivoApoyo2"))
                End If
            End If
            planteamientoRepository.Delete(planteamiento)
            GVplanteamientos.DataBind()

            If Not flag Then
                CType(New MessageSuccess(GVplanteamientos, Me.GetType, "Se eliminó correctamente el planteamiento así como su archivo de apoyo si lo tenia"), MessageSuccess).show()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVplanteamientos, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try

    End Sub

    Protected Sub btnEliminarOpciones_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarOpciones.Click
        Dim opcionRepository As IRepository(Of Opcion) = New OpcionRepository()
        Dim flag As Boolean = False

        Try
            Dim opcion As Opcion = opcionRepository.FindById(GVopciones.SelectedDataKey("IdOpcion"))
            opcionRepository.Delete(opcion)

            If Not IsDBNull(GVopciones.SelectedDataKey("ArchivoApoyo")) Then

                If Utils.FileUtils.isThisFilesWithMultipleAssociations(GVopciones.SelectedDataKey("ArchivoApoyo")) Then
                    CType(New MessageInfo(btnEliminarOpciones, Me.GetType(), "El/los archivo/archivos fisicos no se han borrado pero sí el registro. El archivo de apoyo está asignado a otros subtemas/opciones/planteamientos. "), MessageInfo).show()
                    flag = True
                Else
                    If File.Exists(GVopciones.SelectedDataKey("ArchivoApoyo")) Then
                        File.Delete(Siget.Config.Global.rutaMaterial & GVopciones.SelectedDataKey("ArchivoApoyo"))
                    End If
                End If

            End If

            GVopciones.DataBind()
            LoadPreview()

            If Not flag Then
                CType(New MessageSuccess(GVplanteamientos, Me.GetType, "Se eliminó correctamente la opción así como su archivo de apoyo"), MessageSuccess).show()
            End If

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVplanteamientos, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try

    End Sub
#End Region

#Region "DATABOUND"

    Protected Sub GVplanteamientos_DataBound(sender As Object, e As EventArgs) Handles GVplanteamientos.SelectedIndexChanged
        If GVplanteamientos.Rows.Count = 0 Then
            btnEliminarPlanteamiento.Visible = False
            GVplanteamientos.SelectedIndex = -1
        End If
    End Sub

    Protected Sub GVopciones_DataBound(sender As Object, e As EventArgs)

        If GVopciones.Rows.Count = 0 Then
            btnEliminarOpciones.Visible = False
            GVopciones.SelectedIndex = -1
        End If

    End Sub

    Protected Sub DDLTemaPlanteamiento_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLTemaPlanteamiento.DataBound
        DDLTemaPlanteamiento.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Tema"), 0))
        If DDLTemaPlanteamiento.Items.Count = 2 Then
            DDLTemaPlanteamiento.SelectedIndex = 1
            DDLSubtemaPlanteamiento.DataBind()
        End If
    End Sub

    Protected Sub DDLNivelPlanteamientos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLNivelPlanteamientos.DataBound
        DDLNivelPlanteamientos.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Nivel"), 0))
        If DDLNivelPlanteamientos.Items.Count = 2 Then
            DDLNivelPlanteamientos.SelectedIndex = 1
            DDLGradoPlanteamiento.DataBind()
        End If
    End Sub

    Protected Sub DDLGradoPlanteamiento_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLGradoPlanteamiento.DataBound
        DDLGradoPlanteamiento.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Grado"), 0))
        If DDLGradoPlanteamiento.Items.Count = 2 Then
            DDLGradoPlanteamiento.SelectedIndex = 1
            DDLAsignaturaPlanteamiento.DataBind()
        End If
    End Sub

    Protected Sub DDLAsignaturaPlanteamiento_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLAsignaturaPlanteamiento.DataBound
        DDLAsignaturaPlanteamiento.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Asignatura"), 0))
        If DDLAsignaturaPlanteamiento.Items.Count = 2 Then
            DDLAsignaturaPlanteamiento.SelectedIndex = 1
            DDLTemaPlanteamiento.DataBind()
        End If
    End Sub

    Protected Sub DDLSubtemaPlanteamiento_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLSubtemaPlanteamiento.DataBound
        DDLSubtemaPlanteamiento.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Subtema"), 0))
        If DDLSubtemaPlanteamiento.Items.Count = 2 Then
            DDLSubtemaPlanteamiento.SelectedIndex = 1
            GVplanteamientos.DataBind()
        End If
    End Sub

#End Region

#Region "PAGEINDEXCHANGED"
    Protected Sub GVplanteamientos_PageIndexChanged(sender As Object, e As EventArgs) Handles GVplanteamientos.PageIndexChanged
        GVplanteamientos.SelectedIndex = -1
    End Sub

    Protected Sub GVopciones_PageIndexChanged(sender As Object, e As EventArgs) Handles GVopciones.PageIndexChanged
        GVopciones.SelectedIndex = -1
    End Sub
#End Region

   
End Class
