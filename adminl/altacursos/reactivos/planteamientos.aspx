﻿    <%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false"
         CodeFile="planteamientos.aspx.vb" 
        Inherits="administrador_Planteamientos" ValidateRequest="false"
        EnableEventValidation="false"     MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <style type="text/css">
    .style11 {
      width: 100%;
    }

    .style21 {
    }

    .style16 {
      width: 16px;
    }

    .style22 {
      font-family: Arial, Helvetica, sans-serif !important;
      font-size: large !important;
      font-weight: bold!important;
      color: #0000FF;
      text-align: right;
    }

    .style18 {
      width: 345px;
    }


    .style19 {
      font-family: Arial, Helvetica, sans-serif!important;
      font-size: x-large !important;
      font-weight: bold !important;
    }

    td span label{
       font-family: Arial, Helvetica, sans-serif !important;
      font-size: large !important;
      font-weight: bold!important;
      color: #0000FF;
      text-align: right;
    }

    .style23 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: x-small;
      font-weight: bold;
      color: #000099;
      background-color: #CCCCCC;
    }

    .style24 {
      text-align: left;
    }

    .style25 {
      width: 345px;
      text-align: left;
    }

    .style26 {
      font-size: x-small;
      color: #000099;
    }

    .style27 {
      width: 610px;
    }

    .style28 {
      height: 21px;
    }

    .style29 {
      font-family: Arial, Helvetica, sans-serif;
    }

    .style30 {
      width: 42px;
      height: 8px;
    }

    .style31 {
      width: 67px;
      height: 8px;
    }
  </style>

    
     <div class="col-lg-12 col-sm-12 col-xs-12 resize-mobile">
        <div class="widget flat radius-bordered">
            <div class="widget-header bg-themeprimary header-fix">

                <span class="widget-caption"><b><%=Lang_Config.Translate("reactivos", "Area_Reactivos")%></b> </span>

                <div class="col-lg-10">
                    <a data-toggle="modal"
                        class="btn btn-labeled btn-success space-header" data-target=".grid-planteamientos">
                        <i class="btn-label fa fa-table"></i><%=Lang_Config.Translate("reactivos", "Planteamientos")%>
                    </a>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main ">
                    <div class="tabbable">
                        <ul class="nav nav-tabs tabs-flat" id="myTab11">
                            <li id="AreaPlanteamiento" class="active">
                                <a data-toggle="tab" href="#reactivos"><%=Lang_Config.Translate("reactivos", "Sub_header_1")%>
                                </a>
                            </li>
                            <li id="OpcionToggle" runat="server">
                                <a id="AreaOpcion" data-toggle="tab" href="#opciones">
                                    <%=Lang_Config.Translate("reactivos", "Sub_header_2")%>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content tabs-flat">
                            <div id="reactivos" class="tab-pane in active">
                                <div class="form-horizontal">

                                    <div class="panel panel-default">

                                        <asp:UpdatePanel ID="AreaPlanteamientos" runat="server">
                                            <ContentTemplate>
                                                <div class="panel-body">
                                                    <div class="col-lg-12" id="planteamientosForm">
                                                        <div class="col-lg-12" style="position: absolute; top: 50%; left: 50%; bottom: 50%;">
                                                            <div class="load-spinner"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label"><%=Lang_Config.Translate("general", "Nivel")%></label>
                                                            <div class="col-lg-4">
                                                                <asp:DropDownList CssClass="form-control" runat="server"
                                                                    ID="DDLNivelPlanteamientos"
                                                                    AutoPostBack="True"
                                                                    DataSourceID="SDSniveles" DataTextField="Descripcion"
                                                                    DataValueField="IdNivel">
                                                                </asp:DropDownList>

                                                            </div>
                                                            <label class="col-lg-2 control-label"><%=Lang_Config.Translate("general", "Grado")%></label>
                                                            <div class="col-lg-4">
                                                                <asp:DropDownList CssClass="form-control" runat="server"
                                                                    ID="DDLGradoPlanteamiento"
                                                                    AutoPostBack="True"
                                                                    DataSourceID="SDSgrados" DataTextField="Descripcion"
                                                                    DataValueField="IdGrado">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label"><%=Lang_Config.Translate("general", "Asignatura")%></label>
                                                            <div class="col-lg-4">
                                                                <asp:DropDownList CssClass="form-control" runat="server"
                                                                    ID="DDLAsignaturaPlanteamiento"
                                                                    AutoPostBack="True"
                                                                    DataSourceID="SDSasignaturas" DataTextField="Descripcion"
                                                                    DataValueField="IdAsignatura">
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-lg-2 control-label"><%=Lang_Config.Translate("general", "Tema")%></label>
                                                            <div class="col-lg-4">
                                                                   <asp:DropDownList CssClass="form-control" runat="server"
                                                                       ID="DDLTemaPlanteamiento"
                                                                       AutoPostBack="True"
                                                                       DataSourceID="SDStemas" DataTextField="NomTema"
                                                                       DataValueField="IdTema">
                                                                </asp:DropDownList>
                                                            </div>
            
                                                        </div>

                                                          <div class="form-group">
                                                            <label class="col-lg-2 control-label"><%=Lang_Config.Translate("general", "Subtema")%></label>
                                                            <div class="col-lg-10">
                                                                   <asp:DropDownList CssClass="form-control" runat="server"
                                                                       ID="DDLSubtemaPlanteamiento"
                                                                       AutoPostBack="True"
                                                                       DataSourceID="SDSsubtemas" DataTextField="NomSubtema"
                                                                       DataValueField="IdSubtema">
                                                                   </asp:DropDownList>
                                                            </div>

                                                          </div>
                                                            <div class="form-group">
                                                                <div class="col-lg-11 col-lg-offset-1">
                                                                    <a data-toggle="modal"
                                                                        class="btn btn-labeled btn-warning" data-target=".redaccion-modal">
                                                                        <i class="btn-label fa fa-bars"></i><%=Lang_Config.Translate("reactivos", "Redactar_Planteamiento")%>
                                                                    </a>
                                                                   
                                                                </div>
                                                            </div>
                                                            <hr>
                                                    </div>
                                           

                                                 
                                                    <div class="col-lg-6" style="border-right:1px solid #D3D3D3;padding-right:40px;">
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">
                                                                <%=Lang_Config.Translate("general", "Consecutivo")%>
                                                            </label>
                                                            <div class="col-lg-4">
                                                                <asp:TextBox runat="server"
                                                                     ID="TBconsecutivo" TextMode="Number"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                             <label class="col-lg-3 control-label">
                                                                             <%=Lang_Config.Translate("reactivos", "Ponderacion")%>
                                                            </label>
                                                            <div class="col-lg-3">
                                                                <asp:TextBox runat="server" 
                                                                     ID="TBponderacion" TextMode="Number"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label ">
                                                                 <%=Lang_Config.Translate("reactivos", "Penalizacion")%>
                                                            </label>
                                                            <div class="col-lg-7">

                                                            
                                                                <asp:TextBox runat="server" 
                                                                    ClientIDMode="Static"
                                                                    ID="TBPenalizacion" 
                                                                    CssClass="form-control slider-percentage "></asp:TextBox>
                                                               
                                                            </div>
                                                            <div class="col-lg-3">
                                                                 <label id="slider-value" >0</label>%
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-lg-3">  <%=Lang_Config.Translate("reactivos", "Tipo_respuesta")%></label> 
                                                            <div class="col-lg-9">
                                                                
                                                        <asp:DropDownList ID="DDLtiporespuesta"  CssClass="form-control ddlTipoRespuesta" 
                                                             runat="server">
                                                            <asp:ListItem Value="Multiple">Opción Múltiple</asp:ListItem>
                                                            <asp:ListItem Value="Abierta">Respuesta Abierta</asp:ListItem>
                                                            <asp:ListItem Value="Abierta Calificada">Respuesta Abierta Calificada</asp:ListItem>
                                                            <asp:ListItem Value="Completar" Enabled="False">Completar Espacios</asp:ListItem>
                                                        </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <hr />
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-lg-4 control-label"> <%=Lang_Config.Translate("general", "Material_apoyo")%></label>
                                                            <div class="col-lg-8">
                                                                <asp:AsyncFileUpload style="width:200px !important;" runat="server"  ID="ArchivoApoyo" 
                                                                    Width=" 800" 
                                                                    />
                                                                <asp:HyperLink runat="server" ID="ArchivoApoyoLink"></asp:HyperLink>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="col-lg-4 control-label"><%=Lang_Config.Translate("general", "Tipo_de_Archivo")%></label>
                                                            <div class="col-lg-7">
                                                                <asp:DropDownList ID="DDLTipoArchivo1" runat="server" CssClass="form-control">
                                                                    <asp:ListItem>Ninguno</asp:ListItem>
                                                                    <asp:ListItem>Imagen</asp:ListItem>
                                                                    <asp:ListItem>PDF</asp:ListItem>
                                                                    <asp:ListItem>ZIP</asp:ListItem>
                                                                    <asp:ListItem>Word</asp:ListItem>
                                                                    <asp:ListItem>Excel</asp:ListItem>
                                                                    <asp:ListItem Value="PP">Power Pont</asp:ListItem>
                                                                    <asp:ListItem>Video</asp:ListItem>
                                                                    <asp:ListItem>Audio</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-lg-4 control-label"><%=Lang_Config.Translate("reactivos", "Material_apoyo_adicional")%></label>
                                                            <div class="col-lg-8">
                                                                <asp:AsyncFileUpload style="width:200px !important;" runat="server"  
                                                                    ID="ArchivoAdicionalApoyo"/>
                                                                     <asp:HyperLink runat="server" ID="ArchivoAdicionalLink"></asp:HyperLink>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="col-lg-4 control-label"><%=Lang_Config.Translate("general", "Tipo_de_Archivo")%></label>
                                                            <div class="col-lg-7">
                                                                <asp:DropDownList ID="DDLTipoArchivo2"  runat="server" CssClass="form-control">
                                                                    <asp:ListItem>Ninguno</asp:ListItem>
                                                                    <asp:ListItem>Imagen</asp:ListItem>
                                                                    <asp:ListItem>PDF</asp:ListItem>
                                                                    <asp:ListItem>ZIP</asp:ListItem>
                                                                    <asp:ListItem>Word</asp:ListItem>
                                                                    <asp:ListItem>Excel</asp:ListItem>
                                                                    <asp:ListItem Value="PP">Power Pont</asp:ListItem>
                                                                    <asp:ListItem>Video</asp:ListItem>
                                                                    <asp:ListItem>Audio</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-lg-6" >




                                                        <div class="form-group">
                                                                    <div id="CBCalificaAbierta" class="form-group containerAbierta " style="display: none;" runat="server">
                                                                        <label class="col-lg-4"><b><%=Lang_Config.Translate("reactivos", "Califica_profesor")%></b></label>
                                                                        <div class="col-lg-2">
                                                                            <label>
                                                                                <input runat="server" id="CBCalifica" class="checkbox-slider cbgeneric" type="checkbox" />
                                                                                <spans class="text"></spans>
                                                                            </label>
                                                                        </div>

                                                                    </div>

                                                                    <div id="CBVerificarAbiertaCalificada" class="containerAbiertaCalificada" style="display: none;" runat="server">
                                                                        <label class=" col-lg-4"><b><%=Lang_Config.Translate("reactivos", "Permite_verifica")%></b></label>
                                                                        <div class="col-lg-2">
                                                                            <label>
                                                                                <input runat="server" id="CBVerificar" class="checkbox-slider cbgeneric" type="checkbox" />
                                                                                <span class="text"></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div id="TBAnchoContenedorAbiertaCalificada" class="form-group containerAbiertaCalificada " style="display: none;" runat="server">
                                                                    <label class=" col-lg-3"><b><%=Lang_Config.Translate("reactivos", "Ancho_contenedor")%></b></label>
                                                                    <div class="col-lg-7">
                                                                        <asp:TextBox runat="server" ClientIDMode="Static"
                                                                            ID="TBAnchoContenedor"
                                                                            CssClass="form-control slider-percentage-ancho"></asp:TextBox>
                                                                    </div>
                                                                    <div class="col-lg-2">
                                                                        <label id="slider-value-ancho">0</label>
                                                                    </div>
                                                                </div>

                                                                <div id="CBComparaMayusAbiertaCalificada" runat="server" class="form-group containerAbiertaCalificada " style="display: none;">
                                                                    <label class=" col-lg-7"><b><%=Lang_Config.Translate("reactivos", "Compara_mayus")%></b></label>
                                                                    <div class="col-lg-3">
                                                                        <label>
                                                                            <input runat="server" id="CBComparaMayus" class="checkbox-slider cbgeneric colored-palegreen" type="checkbox">
                                                                            <span class="text"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div id="CBComparaAcentosAbiertaCalificada" class="form-group containerAbiertaCalificada" style="display: none;" runat="server">
                                                                    <label class=" col-lg-7"><b><%=Lang_Config.Translate("reactivos", "Compara_acentos")%></b></label>
                                                                    <div class="col-lg-3">
                                                                        <label>
                                                                            <input runat="server" id="CBComparaAcentos" class="checkbox-slider cbgeneric colored-palegreen" type="checkbox" />
                                                                            <span class="text"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                                <div id="CBAlfanumericoAbiertaCalificada" runat="server" class="form-group containerAbiertaCalificada" style="display: none;">
                                                                    <label class=" col-lg-7"><b><%=Lang_Config.Translate("reactivos", "Compara_numeros")%></b></label>
                                                                    <div class="col-lg-3">
                                                                        <label>
                                                                            <input runat="server" id="CBAlfaNumerico" name="" class="checkbox-slider cbgeneric colored-palegreen" type="checkbox" checked="checked" />
                                                                            <span class="text"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div id="CBEliminaPuntuacion" runat="server" class="form-group containerAbiertaCalificada" style="display: none;">
                                                                    <label class=" col-lg-7"><b><%=Lang_Config.Translate("reactivos", "Elimina_Puntuacion")%></b></label>
                                                                    <div class="col-lg-3">
                                                                        <label>
                                                                            <input runat="server" id="CBElpuntuacion" class="checkbox-slider cbgeneric colored-palegreen" type="checkbox" checked="checked" />
                                                                            <span class="text"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>

                                                            
                                                        <hr />



                                                        <div class="form-group">

                                                         <label class=" col-lg-7"><b>Asignar tiempo para responder</b></label>
                                                            <div class="col-lg-3">
                                                                <label>
                                                                    <input runat="server" id="CBTiempo" min="0" class="checkbox-slider cbgeneric colored-palegreen" type="checkbox" />
                                                                    <span class="text"></span>
                                                                </label>
                                                                
                                                           </div>
                                                            <div runat="server" id="panelTiempo" class="col-lg-2" style="display:none;">
                                                                 <asp:TextBox runat="server" id="TBTiempo" class="form-control" type="number"  ></asp:TextBox>
                                                                    <label id="LabelTiempo"  class="col-lg-8">Minutos</label>
                                                                 </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class=" col-lg-7"><b><%=Lang_Config.Translate("reactivos", "Reactivo_exclusivo")%></b></label>
                                                            <div class="col-lg-3">
                                                                <label>
                                                                    <input runat="server" id="CBOcultar" class="checkbox-slider cbgeneric colored-palegreen" type="checkbox" />
                                                                    <span class="text"></span>
                                                                </label>
                                                           </div>
                                                        </div>

                                                         <div class="form-group">
                                                            <label class=" col-lg-7"><b><%=Lang_Config.Translate("reactivos", "Esconder_redaccion")%></b></label>
                                                            <div class="col-lg-3">
                                                                <label>
                                                                    <input runat="server" id="CBEsconderTexto" class="checkbox-slider cbgeneric colored-palegreen" type="checkbox" />
                                                                    <span class="text"></span>
                                                                </label>
                                                           </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class=" col-lg-7"><b><%=Lang_Config.Translate("reactivos", "Permitir_segunda")%></b></label>
                                                            <div class="col-lg-3">
                                                                <label>
                                                                    <input runat="server" id="CBSegundoVuelta" class="checkbox-slider cbgeneric colored-palegreen" type="checkbox" checked="checked" />
                                                                    <span class="text"></span>
                                                                </label>
                                                           </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class=" col-lg-7"><b><%=Lang_Config.Translate("reactivos", "Habilitar_grabador")%></b></label>
                                                            <div class="col-lg-3">
                                                                <label>
                                                                    <input runat="server" id="CBHabilitaRecorder" class="checkbox-slider cbgeneric colored-palegreen" type="checkbox" />
                                                                    <span class="text"></span>
                                                                </label>
                                                          </div>
                                                        </div>

                                                    </div>
                                                          
                                                                                                      
                                                    <div class="form-group">
                                                        <asp:LinkButton ID="btnCrearPlanteamiento" runat="server" type="submit"
                                                            CssClass="btnCrearPlanteamiento btn btn-lg btn-labeled btn-palegreen ">
                                                        <i class="btn-label fa fa-plus"></i>
                                                           <%=Lang_Config.Translate("reactivos", "Crear_planteamiento")%>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnActualizarPlanteamiento" runat="server" type="submit" Visible="false"
                                                            CssClass="btn btn-lg btn-labeled btn-palegreen shiny"> 
                                                        <i class="btn-label fa fa-refresh"></i>
                                                          <%=Lang_Config.Translate("reactivos", "Actualizar_planteamiento")%>
                                                        </asp:LinkButton>
                                                    </div>

                                            <div class="form-group">
                                                 <uc1:msgSuccess runat="server" ID="msgSuccess" />
                                                 <uc1:msgError runat="server" ID="msgError" />
                                            </div>

                                                
                                            </div>
                                                    
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>

                              <div id="opciones" class="tab-pane">


                                <div class="form-horizontal">

                                    <div class="panel panel-default">

                                        <asp:UpdatePanel ID="PanelOpciones"  runat="server">
                                                <ContentTemplate>
                                        <div class="panel-heading bg-themeprimary-lg">
                                          

                                                <b>ÁREA PARA ADMINISTRAR LAS OPCIONES DE: <asp:Label runat="server" ID="planteamientoSelected"></asp:Label></b> 
                                        </div>

                                        <div class="panel-body">
                                            

                                                    <div class="col-lg-12">

                                                   
                                                    <div class="col-lg-11">

                                                        <div class="form-group">
                                                            <div class="col-lg-3 col-lg-offset-9">
                                                                <a data-toggle="modal"
                                                                    class="btn btn-labeled btn-warning" data-target=".grid-opciones">
                                                                    <i class="btn-label fa fa-search"></i>  <%=Lang_Config.Translate("reactivos", "Opciones_disponibles")%>
                                                                </a>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <div class="col-lg-7 col-lg-offset-3">
                                                                <a data-toggle="modal"
                                                                    class="btn btn-labeled btn-default" data-target=".redaccionOpcion-modal">
                                                                    <i class="btn-label fa fa-bars"></i>Redactar Opción
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">

                                                            <label class="col-lg-4 control-label">¿Es correcta esa opción?</label>

                                                            <div class="col-lg-8">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox" runat="server" id="CBCorrectaS" class="inverted" />
                                                                        <span class="text">Si</span>
                                                                </label>
                                                                <label>
                                                                    <input type="checkbox" runat="server" id="CBCorrectaN" class="inverted"  />
                                                                    <span class="text">No</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                        <div class="form-group">
                                                            <label class="col-lg-4 control-label">Inciso Consecutivo (A,B,C,D,E)</label>
                                                            <div class="col-lg-8">
                                                                <asp:DropDownList runat="server" data-toggle="tooltip" data-placement="top" 
                                                                    title="El reactivo puede contener hasta máximo 6 opciones "  ID="DDLConsecutivo">
                                                                    <asp:ListItem Value="A">A</asp:ListItem>
                                                                    <asp:ListItem Value="B">B</asp:ListItem>
                                                                    <asp:ListItem Value="C">C</asp:ListItem>
                                                                    <asp:ListItem Value="D">D</asp:ListItem>
                                                                    <asp:ListItem Value="E">E</asp:ListItem>
                                                                    <asp:ListItem Value="F">F</asp:ListItem>
                                                                </asp:DropDownList>

                                                               
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                             <div class="checkbox">
                                                                    <label>
                                                                        <input runat="server" id="CBesconder" type="checkbox" class="inverted" />
                                                                        <span class="text"><b>  <%=Lang_Config.Translate("reactivos","Solo_captura_interfaz") %>
                                                                        </b></span>
                                                                    </label>
                                                                </div>
                                                        </div>
                                                        <div class="form-group">
                                                              <asp:TextBox ID="TBresp1" runat="server" MaxLength="25" Visible="False" CssClass="form-control"></asp:TextBox>
                                                                 <asp:TextBox ID="TBresp2" runat="server" Visible="False" CssClass="form-control"></asp:TextBox>

                                                        </div>
                                                       <div class="form-group">
                                                            <label class="col-lg-4 control-label">Material de Apoyo</label>
                                                            <div class="col-lg-8">
                                                                <asp:AsyncFileUpload style="width:200px !important;" runat="server"  ID="ArchivoApoyoOpcion" Width=" 800" PersistFile="False" />
                                                                <asp:HyperLink runat="server" ID="ArchivoApoyoOpcionLink"></asp:HyperLink>
                                                                
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="col-lg-4 control-label">Tipo de Archivo</label>
                                                            <div class="col-lg-7">
                                                                <asp:DropDownList ID="DDLTipoArchivoOpcion" runat="server" CssClass="form-control">
                                                                    <asp:ListItem>Ninguno</asp:ListItem>
                                                                    <asp:ListItem>Imagen</asp:ListItem>
                                                                    <asp:ListItem>PDF</asp:ListItem>
                                                                    <asp:ListItem>ZIP</asp:ListItem>
                                                                    <asp:ListItem>Word</asp:ListItem>
                                                                    <asp:ListItem>Excel</asp:ListItem>
                                                                    <asp:ListItem Value="PP">Power Pont</asp:ListItem>
                                                                    <asp:ListItem>Video</asp:ListItem>
                                                                    <asp:ListItem>Audio</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                     


                                                    </div>

                                                         <div class="col-lg-12">
                                                        <asp:LinkButton ID="btnInsertarOpcion" runat="server"
                                                            CssClass="btnCrearPlanteamiento btn btn-lg btn-labeled btn-palegreen space-margin">
                                                        <i class=" btn-label fa fa-plus"></i>
                                                            Insertar Opción
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnActualizarOpcion" runat="server"
                                                            CssClass="btn btn-lg btn-labeled btn-palegreen space-margin shiny">
                                                        <i class="btn-label fa fa-refresh"></i>
                                                           Actualizar Opción
                                                        </asp:LinkButton>
                                                             <a id="btnPrevisualizar" data-toggle="modal"
                                                                 class="btn  btn-lg btn-labeled btn-palegreen space-margin shiny" data-target=".previsualizar">
                                                                 <i class="btn-label fa fa-eye"></i>
                                                                 Previsualizar
                                                             </a>


                                                         </div>
                                                        <div class="col-lg-12">
                                                            <uc1:msgSuccess runat="server" ID="msgSuccess1" />
                                                            <uc1:msgError runat="server" ID="msgError1" />
                                                        </div>

                                                    </div>


                                        </div>
                                                    
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         </div>

   
    <div class="modal fade redaccionOpcion-modal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <asp:UpdatePanel  runat="server" >

                    <ContentTemplate>
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myLargeModalLabel"><b>Redacta la opción </b></h4>
                        </div>
                          
                        <div class="panel panel-default">
                            <div class="panel-heading"></div>
                            <div class="panel-body">
                                <asp:TextBox ID="TBRedaccionOpcion" runat="server"
                                    ClientIDMode="Static"
                                    TextMode="MultiLine"></asp:TextBox>
                                  <a data-dismiss="modal" class="btn btn-lg btn-labeled btn-palegreen space-margin shiny" 
                                        aria-hidden="true">   <i class="btn-label fa fa-check"></i>Aceptar</a>

                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

     <div class="modal fade previsualizar" role="dialog" aria-labelledby="Previsualizar" aria-hidden="true" style="display: none;">
         <div class="modal-dialog modal-lg">
             <div class="modal-content">
                 <asp:UpdatePanel runat="server" ID="PanelPrevisualizar">

                     <ContentTemplate>
                         <div class="modal-header">
                             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                             <h4 class="modal-title" id="Previsualizar"><b>Previsualización </b></h4>
                         </div>
                               <div class="table-responsive">
                                 <table class="style11" style="background-color: white;text-align:left;margin:10px;">
                                     <tr class="titulo">
                                         <td colspan="2" style="background-color: white;">
                                             <uc1:msgError runat="server" ID="msgError2" />
                                         </td>
                                     </tr>
                                     <tr>
                                         <td class="style27" style="background-color: white;">
                                             <asp:Label ID="LblPlanteamiento" runat="server"
                                                 Style="font-family: Arial, Helvetica, sans-serif; font-size: 22px; font-weight: 700; color: #000099;"></asp:Label>
                                         </td>
                                         <td>
                                             <asp:Image ID="ImPlanteamiento" runat="server" Visible="False" />
                                             <asp:HyperLink ID="HLapoyoplanteamiento" runat="server" Target="_blank"
                                                 CssClass="style26"
                                                 Style="font-family: Arial, Helvetica, sans-serif; font-weight: 700; text-align: left; background-color: #CCCCCC"
                                                 Visible="False">[HLapoyoplanteamiento]</asp:HyperLink>
                                             <asp:Label ID="LblVideo" runat="server"
                                                 Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td class="style27" style="background-color: white;">
                                             <asp:Image ID="ImPlanteamiento2" runat="server" Visible="False" />
                                             <asp:HyperLink ID="HLapoyoplanteamiento2" runat="server" Target="_blank"
                                                 CssClass="style26"
                                                 Style="font-family: Arial, Helvetica, sans-serif; font-weight: 700; text-align: left; background-color: #CCCCCC"
                                                 Visible="False">[HLapoyoplanteamiento2]</asp:HyperLink>
                                             <asp:Label ID="LblVideo2" runat="server"
                                                 Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
                                         </td>
                                         <td>&nbsp;</td>
                                     </tr>
                                     <tr>
                                         <td class="style21" colspan="2" style="background-color: white;">
                                             <asp:Label ID="LblReferencia" runat="server"
                                                 Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td colspan="2" style="background-color: #666666">&nbsp;</td>
                                     </tr>
                                     <tr>
                                         <td class="style12" colspan="2">
                                             <asp:Panel ID="PnlOpciones" runat="server" Visible="false">
                                                 <table id="TblOpciones" class="style11">
                                                     <tr>
                                                         <td class="style30">
                                                             <asp:RadioButton ID="Opcion1" runat="server" GroupName="eleccion"
                                                                 CssClass="style22" Width="50px" Font-Bold="true" TextAlign="Left" />
                                                         </td>
                                                         <td class="style18">
                                                             <asp:Label ID="TextoOpc1" runat="server" CssClass="style19"></asp:Label>
                                                         </td>
                                                         <td class="style31">
                                                             <asp:RadioButton ID="Opcion2" runat="server" GroupName="eleccion"
                                                                 CssClass="style22" Width="50px" TextAlign="Left" />
                                                         </td>
                                                         <td>
                                                             <asp:Label ID="TextoOpc2" runat="server" CssClass="style19"></asp:Label>
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td class="style30">&nbsp;</td>
                                                         <td class="style25">
                                                             <asp:Image ID="ImOpc1" runat="server" />
                                                             <asp:HyperLink ID="HLapoyoopc1" runat="server" Target="_blank"
                                                                 CssClass="style23">[HLapoyoopc1]</asp:HyperLink>
                                                         </td>
                                                         <td class="style31">&nbsp;</td>
                                                         <td class="style24">
                                                             <asp:Image ID="ImOpc2" runat="server" />
                                                             <asp:HyperLink ID="HLapoyoopc2" runat="server" Target="_blank"
                                                                 CssClass="style23">[HLapoyoopc2]</asp:HyperLink>
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td class="style30">
                                                             <asp:RadioButton ID="Opcion3" runat="server" GroupName="eleccion"
                                                                 CssClass="style22" Width="50px" TextAlign="Left" />
                                                         </td>
                                                         <td class="style18">
                                                             <asp:Label ID="TextoOpc3" runat="server" CssClass="style19"></asp:Label>
                                                         </td>
                                                         <td class="style31">
                                                             <asp:RadioButton ID="Opcion4" runat="server" GroupName="eleccion"
                                                                 CssClass="style22" Width="50px" TextAlign="Left" />
                                                         </td>
                                                         <td>
                                                             <asp:Label ID="TextoOpc4" runat="server" CssClass="style19"></asp:Label>
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td class="style30">&nbsp;</td>
                                                         <td class="style25">
                                                             <asp:Image ID="ImOpc3" runat="server" />
                                                             <asp:HyperLink ID="HLapoyoopc3" runat="server" Target="_blank"
                                                                 CssClass="style23">[HLapoyoopc3]</asp:HyperLink>
                                                         </td>
                                                         <td class="style31">&nbsp;</td>
                                                         <td class="style24">
                                                             <asp:Image ID="ImOpc4" runat="server" />
                                                             <asp:HyperLink ID="HLapoyoopc4" runat="server" Target="_blank"
                                                                 CssClass="style23">[HLapoyoopc4]</asp:HyperLink>
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td class="style30">
                                                             <asp:RadioButton ID="Opcion5" runat="server" GroupName="eleccion"
                                                                 CssClass="style22" Width="50px" TextAlign="Left" />
                                                         </td>
                                                         <td class="style18">
                                                             <asp:Label ID="TextoOpc5" runat="server" CssClass="style19"></asp:Label>
                                                         </td>
                                                         <td class="style31">
                                                             <asp:RadioButton ID="Opcion6" runat="server" GroupName="eleccion"
                                                                 CssClass="style22" Width="50px" TextAlign="Left" />
                                                         </td>
                                                         <td>
                                                             <asp:Label ID="TextoOpc6" runat="server" CssClass="style19"></asp:Label>
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td class="style30">&nbsp;</td>
                                                         <td class="style25">
                                                             <asp:Image ID="ImOpc5" runat="server" />
                                                             <asp:HyperLink ID="HLapoyoopc5" runat="server" Target="_blank"
                                                                 CssClass="style23">[HLapoyoopc5]</asp:HyperLink>
                                                         </td>
                                                         <td class="style31">&nbsp;</td>
                                                         <td class="style24">
                                                             <asp:Image ID="ImOpc6" runat="server" />
                                                             <asp:HyperLink ID="HLapoyoopc6" runat="server" Target="_blank"
                                                                 CssClass="style23">[HLapoyoopc6]</asp:HyperLink>
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td class="style30">&nbsp;</td>
                                                         <td class="style18">&nbsp;</td>
                                                         <td class="style31">&nbsp;</td>
                                                         <td>&nbsp;</td>
                                                     </tr>
                                                     <tr>
                                                         <td class="style30">&nbsp;</td>
                                                         <td class="style18">
                                                             <input id="Button1" type="button" value="Aceptar respuesta"
                                                                 class="defaultBtn btnThemeBlue btnThemeMedium" />
                                                             &nbsp;
								<input id="Button2" type="button" value="Cancelar"
                                    class="defaultBtn btnThemeGrey btnThemeMedium" /></td>
                                                         <td class="style31">&nbsp;                       
                                                         </td>
                                                         <td>&nbsp;</td>
                                                     </tr>
                                                 </table>
                                             </asp:Panel>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td colspan="2">
                                             <asp:Panel ID="PanelAbiertaAutomatica" runat="server" Visible="false">
                                                 <table style="margin-left: auto; margin-right: auto;">
                                                     <tr>
                                                         <td>
                                                             <asp:TextBox ID="tbRespuestaAbiertaAutomatica" runat="server"
                                                                 ClientIDMode="Static" CssClass="wideTextbox"></asp:TextBox>
                                                         </td>
                                                     </tr>
                                                     <tr>
                                                         <td>&nbsp;</td>
                                                     </tr>
                                                     <tr>
                                                         <td>
                                                             <asp:Button ID="VerificarAA" runat="server" Visible="false"
                                                                 ClientIDMode="AutoID"
                                                                 Enabled="false"
                                                                 Text="Verificar Respuesta"
                                                                 CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
                                                             &nbsp;
                                <asp:Button ID="AceptarAA" runat="server"
                                    Enabled="false"
                                    ClientIDMode="AutoID"
                                    Text="Aceptar Respuesta"
                                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                                             &nbsp;
                                <asp:Button ID="CancelarAA" runat="server"
                                    Enabled="false"
                                    Text="   Salir   "
                                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                                                         </td>
                                                     </tr>
                                                 </table>
                                             </asp:Panel>
                                         </td>
                                     </tr>
                                     <tr>
                                         <td class="style28" colspan="2">
                                             <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                                         </td>
                                     </tr>

                                 </table>
                               </div>

                     </ContentTemplate>
                 </asp:UpdatePanel>
             </div>
             <!-- /.modal-content -->
         </div>
         <!-- /.modal-dialog -->
     </div>








    <div class="modal-area">
        <div class="modal fade redaccion-modal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title"><b>Redacta el planteamiento </b></h4>
                                    </div>
                                    <asp:TextBox ID="ckEditor" runat="server"
                                        ClientIDMode="Static"
                                        TextMode="MultiLine"></asp:TextBox>


                                    <a data-dismiss="modal" class="btn btn-lg btn-labeled btn-palegreen space-margin shiny" 
                                        aria-hidden="true">   <i class="btn-label fa fa-check"></i>Aceptar</a>
                                </div>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                     
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

    </div>


    <div class="modal fade grid-planteamientos" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg resize-modal-ws">
            <div class="modal-content">
                <asp:UpdatePanel ID="PanelPlanteamientos" runat="server">
                    <ContentTemplate>

                        <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title"><b>Planteamientos Disponibles</b></h4>
                        </div>
                        <div class="form-horizontal">

                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <div class="table-responsive">
                                    <asp:GridView ID="GVplanteamientos" runat="server"
                                        AllowSorting="True" 
                                        AutoGenerateColumns="False"
                                        DataKeyNames="IdPlanteamiento,Libro,Autor,
                                        Editorial,EsconderTexto, Redaccion, Ancho1, 
                                        Verifica, ComparaMayusculas, ComparaAcentos,
                                         ComparaSoloAlfanumerico,HabilitaRecorder,Consecutivo,TipoRespuesta,
                                        Ponderacion,PorcentajeRestarResp,TipoArchivoApoyo,TipoArchivoApoyo2,
                                        ArchivoApoyo,ArchivoApoyo2,
                                         Ocultar,Permite2,EliminaPuntuacion,Tiempo,Minutos"
                                        DataSourceID="SDSdatosplant"
                                        PageSize="10" 
                                        AllowPaging="True"
                                        CellPadding="3"
                                        CssClass="table table-striped table-bordered"
                                        Height="17px"
                                        Style="font-size: x-small; text-align: left;">
                                        <Columns>
                                           
                                            <asp:BoundField DataField="IdPlanteamiento" HeaderText="Id-Plant." Visible="false"
                                                InsertVisible="False" ReadOnly="True" SortExpression="IdPlanteamiento" />
                                            <asp:BoundField DataField="IdSubtema" HeaderText="Id-Subtema"
                                                SortExpression="IdSubtema" Visible="False" />
                                            <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                                                SortExpression="Consecutivo" />
                                            <asp:BoundField DataField="Redaccion" HeaderText="Redacción del planteamiento"
                                                SortExpression="Redaccion" />
                                            <asp:BoundField DataField="TipoRespuesta" HeaderText="Tipo Resp."
                                                SortExpression="TipoRespuesta" />
                                            <asp:BoundField DataField="Ponderacion" HeaderText="Ponderación"
                                                SortExpression="Ponderacion" />
                                            <asp:BoundField DataField="PorcentajeRestarResp" HeaderText="% Restar"
                                                SortExpression="PorcentajeRestarResp" />
                                            <asp:BoundField DataField="ArchivoApoyo" HeaderText="Archivo Apoyo 1"
                                                SortExpression="ArchivoApoyo" />
                                            <asp:BoundField DataField="TipoArchivoApoyo" HeaderText="Tipo de Archivo 1"
                                                SortExpression="TipoArchivoApoyo" />
                                            <asp:BoundField DataField="ArchivoApoyo2" HeaderText="Archivo Apoyo 2"
                                                SortExpression="ArchivoApoyo2" />
                                            <asp:BoundField DataField="TipoArchivoApoyo2" HeaderText="Tipo de Archivo 2"
                                                SortExpression="TipoArchivoApoyo2" />
                                            <asp:BoundField DataField="Libro" HeaderText="Libro" SortExpression="Libro"
                                                Visible="False" />
                                            <asp:BoundField DataField="Autor" HeaderText="Autor" SortExpression="Autor"
                                                Visible="False" />
                                            <asp:BoundField DataField="Editorial" HeaderText="Editorial"
                                                SortExpression="Editorial" Visible="False" />
                                            <asp:BoundField DataField="Ocultar" HeaderText="Ocultar"
                                                SortExpression="Ocultar" />
                                            <asp:BoundField DataField="Tiempo" HeaderText="Tiempo"
                                                SortExpression="Tiempo" />
                                            <asp:BoundField DataField="Minutos" DataFormatString="{0:0.0}"
                                                HeaderText="Minutos" SortExpression="Minutos">
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Permite2" HeaderText="2da. Vuelta" SortExpression="Permite2" />
                                        </Columns>
                                        <PagerTemplate>
                                            <ul runat="server" id="Pag" class="pagination">
                                            </ul>
                                        </PagerTemplate>
                                        <PagerStyle HorizontalAlign="Center" />
                                        <SelectedRowStyle CssClass="row-selected" />
                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>
                                    </div>
                                    <div class="form-group">
                                        <asp:LinkButton ID="btnEliminarPlanteamiento" Visible="false" runat="server"
                                            CssClass="btn btn-lg btn-labeled btn-darkorange space-margin">
                                                        <i class="btn-label fa fa-remove"></i>
                                                            Eliminar Planteamiento
                                        </asp:LinkButton>
                                    </div>
                                    <div class="form-group">
                                        <a id="btnCerrarModal" data-dismiss="modal" aria-hidden="true"
                                            class="btn  btn-lg btn-labeled btn-warning">
                                            <i class="btn-label fa fa-minus"></i>Cerrar
                                        </a>
                                    </div>
                                     <div class="form-group">
                                                 <uc1:msgSuccess runat="server" ID="msgSuccess3" />
                                                 <uc1:msgError runat="server" ID="msgError3" />
                                            </div>

                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>



            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade grid-opciones" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <asp:UpdatePanel ID="PanelOpcionesPlanteamientos" runat="server">
                    <ContentTemplate>
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title"><b>Opciones disponibles</b></h4>
                        </div>

                        <div class="form-horizontal">
                            
                            <div class="panel panel-default">
   
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <asp:GridView ID="GVopciones" runat="server"
                                            AllowPaging="True" 
                                            AllowSorting="True"
                                            AutoGenerateColumns="False"
                                            Caption="Pude tener hasta 6 opciones por planteamiento"
                                            DataKeyNames="IdOpcion,Redaccion,Correcta,Consecutiva,ArchivoApoyo,TipoArchivoApoyo,Esconder"
                                            DataSourceID="SDSdatosopciones"
                                            PageSize="6"
                                            CellPadding="3"
                                            CssClass="table table-striped table-bordered"
                                            Height="17px"
                                            Style="font-size: x-small; text-align: left;">
                                            <Columns>
                                              
                                                <asp:BoundField DataField="IdOpcion" HeaderText="Id-Opcion" Visible="false"
                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdOpcion" />
                                                <asp:BoundField DataField="IdPlanteamiento" HeaderText="IdPlanteamiento"
                                                    SortExpression="IdPlanteamiento" Visible="False" />
                                                <asp:BoundField DataField="Redaccion" HeaderText="Redacción"
                                                    SortExpression="Redaccion" />
                                                <asp:BoundField DataField="Correcta" HeaderText="Es correcta?"
                                                    SortExpression="Correcta" />
                                                <asp:BoundField DataField="Resp1" HeaderText="Resp. posición 1"
                                                    SortExpression="Resp1" />
                                                <asp:BoundField DataField="Resp2" HeaderText="Resp. posición 2"
                                                    SortExpression="Resp2" />
                                                <asp:BoundField DataField="Consecutiva" HeaderText="Consecutivo"
                                                    SortExpression="Consecutiva" />
                                                <asp:BoundField DataField="ArchivoApoyo" HeaderText="Archivo de Apoyo"
                                                    SortExpression="ArchivoApoyo" />
                                                <asp:BoundField DataField="TipoArchivoApoyo" HeaderText="Tipo de Archivo"
                                                    SortExpression="TipoArchivoApoyo" />
                                                <asp:BoundField DataField="Esconder" HeaderText="Solo captura"
                                                    SortExpression="Esconder" />
                                            </Columns>
                                            <PagerTemplate>
                                                <ul runat="server" id="Pag" class="pagination">
                                                </ul>
                                            </PagerTemplate>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <SelectedRowStyle CssClass="row-selected" />
                                            <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                        </asp:GridView>
                                    </div>
                                    <div class="form-group">
                                        <asp:LinkButton ID="btnEliminarOpciones" Visible="false" runat="server"
                                            CssClass="btn btn-lg btn-labeled btn-darkorange space-margin">
                                                        <i class="btn-label fa fa-remove"></i>
                                                            Eliminar Opción
                                        </asp:LinkButton>
                                    </div>
                                       <div class="form-group">
                                        <a id="btnCerrarModalOpciones" data-dismiss="modal" aria-hidden="true"
                                            class="btn  btn-lg btn-labeled btn-warning">
                                            <i class="btn-label fa fa-minus"></i>Cerrar
                                        </a>
                                    </div>
                                     
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>



            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="sqlDataSources">
             <asp:SqlDataSource ID="SDSniveles" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>
    
        <asp:SqlDataSource ID="SDSgrados" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
          <SelectParameters>
            <asp:ControlParameter ControlID="DDLNivelPlanteamientos" Name="IdNivel"
              PropertyName="SelectedValue" Type="Int32" />
          </SelectParameters>
        </asp:SqlDataSource>

            <asp:SqlDataSource ID="SDSasignaturas" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT [IdAsignatura], [IdGrado], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) ORDER BY [Descripcion]">
          <SelectParameters>
            <asp:ControlParameter ControlID="DDLGradoPlanteamiento" Name="IdGrado"
              PropertyName="SelectedValue" Type="Int32" />
          </SelectParameters>
        </asp:SqlDataSource>
    
          <asp:SqlDataSource ID="SDStemas" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT [IdTema], [IdAsignatura], Cast([Numero] as varchar(8))+ ' ' + [Descripcion] NomTema  FROM [Tema] WHERE ([IdAsignatura] = @IdAsignatura)
order by [Numero],[Descripcion]">
          <SelectParameters>
            <asp:ControlParameter ControlID="DDLAsignaturaPlanteamiento" Name="IdAsignatura"
              PropertyName="SelectedValue" Type="Int32" />
          </SelectParameters>
        </asp:SqlDataSource>

          <asp:SqlDataSource ID="SDSsubtemas" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT [IdSubtema], [IdTema], Cast([Numero] as varchar(8)) + ' ' + [Descripcion] NomSubtema FROM [Subtema] WHERE ([IdTema] = @IdTema)
order by [Numero],[Descripcion]">
          <SelectParameters>
            <asp:ControlParameter ControlID="DDLTemaPlanteamiento" Name="IdTema"
              PropertyName="SelectedValue" />
          </SelectParameters>
        </asp:SqlDataSource>
    <asp:SqlDataSource ID="SDSdatosplant" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT * FROM [Planteamiento] WHERE ([IdSubtema] = @IdSubtema)">
          <SelectParameters>
            <asp:ControlParameter ControlID="DDLSubtemaPlanteamiento" Name="IdSubtema"
              PropertyName="SelectedValue" Type="Int32" />
          </SelectParameters>

        </asp:SqlDataSource>
    </div>
     <asp:SqlDataSource ID="SDSplanteamientos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT CASE WHEN Redaccion<>'''' THEN (Cast(Consecutivo as varchar(5)) +  '.- '  +  Redaccion) ELSE (Cast(Consecutivo as varchar(5)) +  '.- ')END  Texto, IdSubtema, IdPlanteamiento FROM Planteamiento WHERE (IdSubtema = @IdSubtema)
order by [Consecutivo]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLSubtemaPlanteamiento" Name="IdSubtema"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
    
                <asp:SqlDataSource ID="SDSdatosopciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdOpcion], [IdPlanteamiento], [Redaccion], [Correcta], [Resp1], [Resp2], [Consecutiva], [ArchivoApoyo], [TipoArchivoApoyo], [Esconder] FROM [Opcion] WHERE ([IdPlanteamiento] = @IdPlanteamiento)
order by [Consecutiva]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVPlanteamientos" Name="IdPlanteamiento"
                            PropertyName="SelectedDataKey(IdPlanteamiento)" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSopciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Opcion]"></asp:SqlDataSource>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">

    <%:Scripts.Render("~/bundles/slider") %> 

    <script>
     
        Sys.Application.add_load(LoadHandler);

        function LoadHandler(sender, args0) {
            
            $(function () {
            
                var editor = CKEDITOR.replace('ckEditor', {
                    language: 'es',
                    height: 300
                });

                editor.on('change', function (evt) {
                    $('#ckEditor').val(evt.editor.getData());
                });

                var editorOpcion=CKEDITOR.replace('TBRedaccionOpcion', {
                    language: 'es',
                    height: 300
                });

                editorOpcion.on('change', function (evt) {
                    $('#TBRedaccionOpcion').val(evt.editor.getData());
                });


                $("#TBAnchoContenedor").simpleSlider({
                    highlight: true,
                    allowedValues:[0,100,120,150,200,250,270,300,400,500,600]
                });

                $("#TBPenalizacion").simpleSlider({
                    highlight: true,
                    allowedValues: [0,5,10,15,20,25,30,35,40,45,50,55,60,65,70,80,85,90,95,100]
                });

                $('#slider-value-ancho').text($("#TBAnchoContenedor").val());
                $('#slider-value').text($("#TBPenalizacion").val());

                $(".slider-percentage").bind("slider:changed", function (event, data) {
                    $('#slider-value').text(data.value);
                });

                $(".slider-percentage-ancho").bind("slider:changed", function (event, data) {
                    $('#slider-value-ancho').text(data.value);
                });
            });
        }
  
    </script>
</asp:Content>

