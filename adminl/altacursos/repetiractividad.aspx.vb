﻿Imports Siget
Imports Siget.BusinessLogic
Imports Siget.TransferObjects
Imports Siget.DataAccess


Partial Class adminl_altacursos_repetiractividad
    Inherits System.Web.UI.Page

#Region "LOAD"

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        'GValumnosterminados.Caption = "<font size='2'><b>" + Config.Etiqueta.ALUMNOS &
        '            " con actividad realizada. Seleccione el resultado que desee repetir </b></font>"
    End Sub

#End Region

#Region "DATABOUND"

    Protected Sub DDLnivel_DataBound(sender As Object, e As EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Nivel"), 0))

        If DDLnivel.Items.Count = 2 Then
            DDLnivel.SelectedIndex = 1
            DDLgrado.DataBind()
        End If

    End Sub

    Protected Sub DDLInstitucion_DataBound(sender As Object, e As EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Institucion"), 0))

        If DDLinstitucion.Items.Count = 2 Then
            DDLinstitucion.SelectedIndex = 1
            DDLplantel.DataBind()
        End If

    End Sub


    Protected Sub DDLplantel_DataBound(sender As Object, e As EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Plantel"), 0))

        If DDLplantel.Items.Count = 2 Then
            DDLplantel.SelectedIndex = 1
            DDLnivel.DataBind()
            GValumnosterminados.DataBind()

        End If

    End Sub

    Protected Sub DDLgrado_DataBound(sender As Object, e As EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Grado"), 0))

        If DDLgrado.Items.Count = 2 Then
            DDLgrado.SelectedIndex = 1
            DDLasignatura.DataBind()
        End If

    End Sub

    Protected Sub DDLcicloescolar_DataBound(sender As Object, e As EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.CICLO, 0))

        If DDLcicloescolar.Items.Count = 2 Then
            DDLcicloescolar.SelectedIndex = 1
        End If

    End Sub

    Protected Sub DDLasignatura_DataBound(sender As Object, e As EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Asignatura"), 0))
        If DDLasignatura.Items.Count = 2 Then
            DDLasignatura.SelectedIndex = 1
            DDLcalificacion.DataBind()
        End If
    End Sub

    Protected Sub DDLcalificacion_DataBound(sender As Object, e As EventArgs) Handles DDLcalificacion.DataBound
        DDLcalificacion.Items.Insert(0, New ListItem("---Elija una Calificación", 0))

        If DDLcalificacion.Items.Count = 2 Then
            DDLcalificacion.SelectedIndex = 1
            GVevaluaciones.DataBind()
        End If

    End Sub

    Protected Sub DDLgrupo_DataBound(sender As Object, e As EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Grupo"), 0))
        GValumnosterminados.DataBind()
        If DDLgrupo.Items.Count = 2 Then
            DDLgrupo.SelectedIndex = 1
            GValumnosterminados.DataBind()
        End If
    End Sub

    Protected Sub GValumnosterminados_DataBound(sender As Object, e As EventArgs) Handles GValumnosterminados.DataBound
        If GValumnosterminados.Rows.Count > 0 Then
            BtnBorrar.Visible = True
        Else
            BtnBorrar.Visible = False
        End If
    End Sub

    Protected Sub GVresultadosanteriores_DataBound(sender As Object, e As EventArgs) Handles GVresultadosanteriores.DataBound
        If GVresultadosanteriores.Rows.Count > 0 Then
            GVresultadosanteriores.Caption = "<b>Resultados anteriores de " + Trim(GValumnosterminados.SelectedRow.Cells(4).Text) + _
                " " + Trim(GValumnosterminados.SelectedRow.Cells(2).Text) + " " + Trim(GValumnosterminados.SelectedRow.Cells(3).Text) + "</b>"
        End If

    End Sub
#End Region
   
#Region "DELETE"
    Protected Sub BtnBorrar_Click(sender As Object, e As EventArgs) Handles BtnBorrar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            ' obtengo datos de la evaluación terminada
            Dim evaluacionTerminada As EvaluacionTerminadaTo = CType(New EvaluacionTerminadaDa(), EvaluacionTerminadaDa).Obten(GValumnosterminados.SelectedDataKey.Values(1).ToString())

            CType(New EvaluacionRepetidaBl, EvaluacionRepetidaBl).RepiteEvaluacion(evaluacionTerminada, User.Identity.Name.Trim())

            msgSuccess.show("Éxito", "La Actividad ya puede repetirla el " + Config.Etiqueta.ALUMNO)
            GValumnosterminados.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub


#End Region

#Region "ROW_CREATED"


    Protected Sub GVevaluaciones_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowCreated


        Dim cociente As Integer = (GVevaluaciones.PageCount / 15)
        Dim moduloIndex As Integer = (GVevaluaciones.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVevaluaciones.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVevaluaciones.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVevaluaciones.PageIndex
                final = IIf((inicial + 10 <= GVevaluaciones.PageCount), inicial + 15, GVevaluaciones.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVevaluaciones.PageCount, inicial + 15, GVevaluaciones.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVevaluaciones.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GValumnosterminados_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GValumnosterminados.RowCreated


        Dim cociente As Integer = (GValumnosterminados.PageCount / 15)
        Dim moduloIndex As Integer = (GValumnosterminados.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GValumnosterminados.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GValumnosterminados.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GValumnosterminados.PageIndex
                final = IIf((inicial + 10 <= GValumnosterminados.PageCount), inicial + 15, GValumnosterminados.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GValumnosterminados.PageCount, inicial + 15, GValumnosterminados.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GValumnosterminados.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVresultadosanteriores_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVresultadosanteriores.RowCreated


        Dim cociente As Integer = (GVresultadosanteriores.PageCount / 15)
        Dim moduloIndex As Integer = (GVresultadosanteriores.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVresultadosanteriores.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVresultadosanteriores.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVresultadosanteriores.PageIndex
                final = IIf((inicial + 10 <= GVresultadosanteriores.PageCount), inicial + 15, GVresultadosanteriores.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVresultadosanteriores.PageCount, inicial + 15, GVresultadosanteriores.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVresultadosanteriores.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub




#End Region

#Region "ROW_DATABOUND"



    Protected Sub GVevaluaciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowDataBound

        Dim cociente As Integer = (GVevaluaciones.PageCount) / 15
        Dim moduloIndex As Integer = (GVevaluaciones.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVevaluaciones.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVevaluaciones.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVevaluaciones.PageIndex
            final = IIf((inicial + 15 <= GVevaluaciones.PageCount), inicial + 15, GVevaluaciones.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVevaluaciones.PageCount), inicial + 15, GVevaluaciones.PageCount)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVevaluaciones, "Select$" & e.Row.RowIndex)
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVevaluaciones, "Page$" & counter.ToString)
            Next
        End If

    End Sub
    Protected Sub GValumnosterminados_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GValumnosterminados.RowDataBound

        Dim cociente As Integer = (GValumnosterminados.PageCount) / 15
        Dim moduloIndex As Integer = (GValumnosterminados.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GValumnosterminados.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GValumnosterminados.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GValumnosterminados.PageIndex
            final = IIf((inicial + 15 <= GValumnosterminados.PageCount), inicial + 15, GValumnosterminados.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GValumnosterminados.PageCount), inicial + 15, GValumnosterminados.PageCount)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GValumnosterminados, "Select$" & e.Row.RowIndex)
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GValumnosterminados, "Page$" & counter.ToString)
            Next
        End If

    End Sub

    Protected Sub GVresultadosanteriores_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVresultadosanteriores.RowDataBound

        Dim cociente As Integer = (GVresultadosanteriores.PageCount) / 15
        Dim moduloIndex As Integer = (GVresultadosanteriores.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVresultadosanteriores.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVresultadosanteriores.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVresultadosanteriores.PageIndex
            final = IIf((inicial + 15 <= GVresultadosanteriores.PageCount), inicial + 15, GVresultadosanteriores.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVresultadosanteriores.PageCount), inicial + 15, GVresultadosanteriores.PageCount)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVresultadosanteriores, "Select$" & e.Row.RowIndex)
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVresultadosanteriores, "Page$" & counter.ToString)
            Next
        End If

    End Sub



#End Region


    Protected Sub GValumnosterminados_PageIndexChanged(sender As Object, e As EventArgs) Handles GValumnosterminados.PageIndexChanged
        GValumnosterminados.SelectedIndex = -1
    End Sub


    Protected Sub gGVevaluaciones_PageIndexChanged(sender As Object, e As EventArgs) Handles GVevaluaciones.PageIndexChanged
        GVevaluaciones.SelectedIndex = -1
    End Sub


    Protected Sub GVresultadosanteriores_PageIndexChanged(sender As Object, e As EventArgs) Handles GVresultadosanteriores.PageIndexChanged
        GVresultadosanteriores.SelectedIndex = -1
    End Sub
End Class
