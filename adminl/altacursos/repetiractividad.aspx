﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false" CodeFile="repetiractividad.aspx.vb" Inherits="adminl_altacursos_repetiractividad" %>


<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bg-themeprimary">
                <i class="widget-icon fa fa-arrow-right"></i>
                <span class="widget-caption"><b>Seleccion una actividad </b></span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="config">
                        <i class="fa fa-cog"></i>
                    </a>
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <!--Widget Buttons-->
            </div>
            <!--Widget Header-->
            <div class="widget-body">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div class="form-horizontal" id="gruposForm">
                            <div class="panel panel-default">

                                <div class="panel-body">

                                    <div class="col-lg-5">

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"><%=Lang_Config.Translate("general","Institucion") %></label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLinstitucion"
                                                    runat="server" AutoPostBack="True" DataSourceID="OdsInstituciones"
                                                    DataTextField="Text" DataValueField="Value"
                                                    CssClass="form-control">
                                                </asp:DropDownList>

                                                   <asp:SqlDataSource ID="OdsInstituciones" runat="server"
                                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                                    SelectCommand="SELECT Descripcion AS 'Text',IdInstitucion AS 'Value' FROM Institucion">

                                                   </asp:SqlDataSource>
                                             
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"><%=Lang_Config.Translate("general","Plantel") %></label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                                    DataSourceID="OdsPlanteles"
                                                    DataTextField="Text" DataValueField="Value"
                                                   CssClass="form-control">
                                                </asp:DropDownList>

                                                  <asp:SqlDataSource ID="OdsPlanteles" runat="server"
                                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                                    SelectCommand="SELECT Descripcion AS 'Text',IdPlantel AS 'Value'   FROM PLANTEL">

                                                </asp:SqlDataSource>
                                           
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"><%=Lang_Config.Translate("general","Nivel") %></label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                                                    CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:SqlDataSource ID="SDSniveles" runat="server"
                                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                                    SelectCommand="select N.IdNivel, N.Descripcion
                        from Nivel N, Escuela E, Plantel P
                        where N.IdNivel = E.IdNivel and P.IdPlantel = E.IdPlantel
                        and P.IdPlantel = @IdPlantel
                        Order by Descripcion">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                                                            PropertyName="SelectedValue" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">
                                                <%=Lang_Config.Translate("general", "Grado")%>
                                            </label>
                                            <div class="col-lg-10">

                                                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSgrados" DataTextField="Descripcion"
                                                    DataValueField="IdGrado" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:SqlDataSource ID="SDSgrados" runat="server"
                                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
                                                    and Estatus = 'Activo'">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                                                            PropertyName="SelectedValue" Type="Int32" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Ciclo Escolar</label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                                                    DataValueField="IdCicloEscolar" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                                    SelectCommand="SELECT DISTINCT C.IdCicloEscolar, C.Descripcion + ' (' + Cast(C.FechaInicio as varchar(12)) + ' - ' + Cast(C.FechaFin as varchar(12)) + ')' Ciclo, C.Descripcion
                        FROM CicloEscolar C
                        WHERE (C.Estatus = 'Activo') 
                        order by C.Descripcion">
                                                 
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">
                                                          <%=Lang_Config.Translate("general","Asignatura") %>
                                            </label>
                                            <div class="col-lg-10">

                                                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSasignaturas" DataTextField="Descripcion"
                                                    DataValueField="IdAsignatura" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdAsignatura, IdGrado, Descripcion
from Asignatura
where IdGrado = @IdGrado
order by Descripcion">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                                                            PropertyName="SelectedValue" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Calificacion</label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDScalificaciones" DataTextField="Descripcion"
                                                    DataValueField="IdCalificacion" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                                    SelectCommand="SELECT C.IdCalificacion, C.IdCicloEscolar, C.Clave, C.IdAsignatura, A.Descripcion Materia, C.Consecutivo,C.Descripcion, C.Valor
FROM Calificacion C, Asignatura A
WHERE ([IdCicloEscolar] = @IdCicloEscolar)  and (A.IdAsignatura = C.IdAsignatura) and (A.IdAsignatura = @IdAsignatura)
ORDER BY A.Descripcion, C.Consecutivo">
                                                    <SelectParameters>
                                                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                                                            PropertyName="SelectedValue" />
                                                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                                                            PropertyName="SelectedValue" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>

                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-lg-7">
                                        <div class="table-responsive">

                                            <asp:GridView ID="GVevaluaciones" runat="server"
                                                AutoGenerateColumns="False"
                                                AllowSorting="True"
                                                DataKeyNames="IdEvaluacion"
                                                DataSourceID="SDSevaluaciones"
                                                CellPadding="3"
                                                CssClass="table table-striped table-bordered"
                                                Height="17px"
                                                Style="font-size: x-small; text-align: left;"
                                                >
                                                <Columns>
                                                    <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad" Visible="false"
                                                        InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion" />
                                                    <asp:BoundField DataField="IdCalificacion"
                                                        HeaderText="IdCalificacion"
                                                        SortExpression="IdCalificacion" Visible="False" />
                                                    <asp:BoundField DataField="ClaveBateria" HeaderText="Clave de la Actividad"
                                                        SortExpression="ClaveBateria" />
                                                    <asp:BoundField DataField="ClaveAbreviada" HeaderText="Clave p/Reporte"
                                                        SortExpression="ClaveAbreviada" />
                                                    <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                                                        SortExpression="Porcentaje">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                        HeaderText="Inicia" SortExpression="InicioContestar" />
                                                    <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                        HeaderText="Termina" SortExpression="FinContestar" />
                                                    <asp:BoundField DataField="FinSinPenalizacion"
                                                        DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Máxima"
                                                        SortExpression="FinSinPenalizacion">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Penalizacion" HeaderText="% Penalización"
                                                        SortExpression="Penalizacion">
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                                </Columns>
                                                <PagerTemplate>
                                                    <ul runat="server" id="Pag" class="pagination">
                                                    </ul>
                                                </PagerTemplate>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <SelectedRowStyle CssClass="row-selected" />
                                                <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>

                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Grupo</label>
                                            <div class="col-lg-8">
                                                  <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                                                DataSourceID="SDSgrupos" DataTextField="Descripcion"
                                                DataValueField="IdGrupo" CssClass="form-control">
                                            </asp:DropDownList>
                                            </div>
                                          
                                            <asp:SqlDataSource ID="SDSgrupos" runat="server"
                                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                                SelectCommand="SELECT * FROM [Grupo] 
WHERE ([IdGrado] = @IdGrado) and ([IdPlantel] = @IdPlantel) and ([IdCicloEscolar] = @IdCicloEscolar)">
                                                <SelectParameters>
                                                    <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                                                        PropertyName="SelectedValue" Type="Int32" />
                                                    <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                                                        PropertyName="SelectedValue" />
                                                    <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                                                        PropertyName="SelectedValue" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                                        </div>

                                    </div>
                                    <div class="col-lg-12">
                                            <asp:GridView ID="GValumnosterminados" runat="server"
                                                AllowPaging="True"
                                                AllowSorting="True"
                                                AutoGenerateColumns="False"
                                                Caption="ALUMNOS con actividad realizada. Seleccione el resultado que desee eliminar"
                                                PageSize="20"
                                                DataKeyNames="IdAlumno,IdEvaluacionT"
                                                DataSourceID="SDSalumnosterminados"
                                                CellPadding="3"
                                                CssClass="table table-striped table-bordered"
                                                Height="17px"
                                                Style="font-size: x-small; text-align: left;">
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                                                <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno" />
                                                <asp:BoundField DataField="ApePaterno" HeaderText="Apellido Paterno"
                                                    SortExpression="ApePaterno" />
                                                <asp:BoundField DataField="ApeMaterno" HeaderText="Apellido Materno"
                                                    SortExpression="ApeMaterno" />
                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre(s)"
                                                    SortExpression="Nombre" />
                                                <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                                                    SortExpression="Matricula" />
                                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                                <asp:BoundField DataField="Resultado" DataFormatString="{0:0.00}"
                                                    HeaderText="Calificación (%)" SortExpression="Resultado">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FechaTermino" DataFormatString="{0:dd/MM/yyyy}"
                                                    HeaderText="Fecha Terminada" SortExpression="FechaTermino" />
                                                <asp:BoundField DataField="IdEvaluacionT" HeaderText="IdEvaluacionT" InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacionT" Visible="False" />
                                            </Columns>
                                                <PagerTemplate>
                                                    <ul runat="server" id="Pag" class="pagination">
                                                    </ul>
                                                </PagerTemplate>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <SelectedRowStyle CssClass="row-selected" />
                                                <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>
                                        <div class="form-group">
                                            <asp:Button ID="BtnBorrar" runat="server" Text="Repetir esta Actividad"
                                                CssClass="defaultBtn btnThemeBlue btnThemeMedium"
                                                Visible="False" />
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <asp:GridView ID="GVresultadosanteriores" runat="server"
                                            AllowSorting="True"
                                            AutoGenerateColumns="False"
                                            Caption="&lt;b&gt;RESULTADOS ANTERIORES del Alumno seleccionado&lt;/b&gt;"
                                            PageSize="5"
                                            DataSourceID="SDSresultadosanteriores"
                                            CellPadding="3"
                                            CssClass="table table-striped table-bordered"
                                            Height="17px"
                                            Style="font-size: x-small; text-align: left;">
                                            <Columns>
                                                <asp:BoundField DataField="Resultado" HeaderText="Resultado" SortExpression="Resultado" DataFormatString="{0:0.00}">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FechaTermino" HeaderText="Fecha que Terminó"
                                                    SortExpression="FechaTermino" DataFormatString="{0:dd/MM/yyyy}">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Intento" HeaderText="Intento" SortExpression="Intento">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                            </Columns>
                                            <PagerTemplate>
                                                <ul runat="server" id="Pag" class="pagination">
                                                </ul>
                                            </PagerTemplate>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <SelectedRowStyle CssClass="row-selected" />
                                            <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                        </asp:GridView>
                                    </div>
                                    <div class="form-group">
                                          <uc1:msgSuccess runat="server" ID="msgSuccess" />
          
                                            <uc1:msgError runat="server" ID="msgError" />
        
                                    </div>


                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>

    </div>


                <asp:SqlDataSource ID="SDSusuarios" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Usuario]"></asp:SqlDataSource>
         
                <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select IdEvaluacion, IdCalificacion, ClaveBateria, ClaveAbreviada, ClaveAbreviada, Porcentaje, InicioContestar, FinContestar, FinSinPenalizacion, Penalizacion, Tipo
from Evaluacion 
where IdCalificacion = @IdCalificacion and SeEntregaDocto = 'False'
and IdEvaluacion not in  (select IdEvaluacion from EvaluacionPlantel where IdPlantel = @IdPlantel)
UNION
select E.IdEvaluacion, E.IdCalificacion, E.ClaveBateria, E.ClaveAbreviada, E.ClaveAbreviada, E.Porcentaje, P.InicioContestar, P.FinContestar,
P.FinSinPenalizacion, E.Penalizacion, E.Tipo
from Evaluacion E, EvaluacionPlantel P
where E.IdCalificacion = @IdCalificacion and E.IdEvaluacion = P.IdEvaluacion and P.IdPlantel = @IdPlantel and E.SeEntregaDocto = 'False'
order by IdCalificacion, InicioContestar">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
         
                <asp:SqlDataSource ID="SDSalumnosterminados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT A.IdAlumno, A.Nombre, A.ApePaterno, A.ApeMaterno, A.Matricula, A.Email, E.Resultado, E.FechaTermino, E.IdEvaluacionT
FROM Alumno A, EvaluacionTerminada E
WHERE (A.Estatus = 'Activo' or A.Estatus = 'Suspendido') and
A.IdGrupo = @IdGrupo AND A.IdPlantel = @IdPlantel 
and A.IdAlumno in (select IdAlumno from EvaluacionTerminada where
                            IdEvaluacion = @IdEvaluacion)
and E.IdAlumno = A.IdAlumno and E.IdEvaluacion = @IdEvaluacion and E.IdGrupo = A.IdGrupo 
and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido')
ORDER BY ApePaterno, ApeMaterno, Nombre">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
       
                <asp:SqlDataSource ID="SDSevaluacionterminada" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [EvaluacionTerminada]"></asp:SqlDataSource>
          
                <asp:SqlDataSource ID="SDSrespuestas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Respuesta]"></asp:SqlDataSource>
            
                <asp:SqlDataSource ID="SDSevaluacionrepetida" runat="server" ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT * FROM [EvaluacionRepetida]"></asp:SqlDataSource>
           
                <asp:SqlDataSource ID="SDSresultadosanteriores" runat="server" ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT E.Resultado, E.FechaTermino, E.Intento
FROM EvaluacionRepetida E
WHERE E.IdEvaluacion = @IdEvaluacion and E.IdAlumno = @IdAlumno 
order by E.Intento
">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="GValumnosterminados" Name="IdAlumno" PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
        


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScriptContent" runat="Server">
</asp:Content>

