﻿
Imports Siget

Imports System.Data.SqlClient
Imports System.IO
Imports System.Data
Imports Siget.Extensions

Partial Class administrador_AgruparReactivos
    Inherits System.Web.UI.Page

#Region "LOAD"

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()
    End Sub


#End Region

#Region "UPLOAD_COMPLETE"
    Protected Sub ArchivApoyo_UploadedComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles ArchivoApoyoPlanteamiento.UploadedComplete
        Beans.FileReference.Files.Insert(4, ArchivoApoyoPlanteamiento.PostedFile)
        Beans.FileReference.nameOfFiles.Insert(4, ArchivoApoyoPlanteamiento.FileName)
        Beans.FileReference.Activator.Insert(4, True)
    End Sub
#End Region

#Region "UPLOAD IMAGES"

    Protected Sub btnUploadFileQuestions_Click(
                   sender As Object,
                   e As EventArgs) Handles btnUploadFileQuestions.Click

        If (Beans.FileReference.Activator(4)) Then
            Beans.FileReference.Files(4).SaveAs(Siget.Config.Global.rutaApoyoPlanteamientos + Beans.FileReference.nameOfFiles(4))
            SDSApoyosPlanteamientos.InsertCommand = "INSERT INTO ApoyoPlanteamiento (NombreApoyo)VALUES('" & Beans.FileReference.nameOfFiles(4) & "')"
            SDSApoyosPlanteamientos.Insert()
            gvCargaImagenes.DataBind()
            Beans.FileReference.Activator(4) = False
            gvCargaImagenes.SelectedIndex = -1
        End If

        msgInfo.show("¡¡ATENCIÓN!!! LA URL ES:", "http://" + HttpContext.Current.Request.Url.Host + Siget.Config.Global.urlApoyoPlanteamientos + Beans.FileReference.nameOfFiles(4))

    End Sub


#End Region

#Region "ROW_CREATED"

    Protected Sub gvCargaImagenes_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles gvCargaImagenes.RowCreated

        Dim cociente As Integer = (gvCargaImagenes.PageCount / 15)
        Dim moduloIndex As Integer = (gvCargaImagenes.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((gvCargaImagenes.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = gvCargaImagenes.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = gvCargaImagenes.PageIndex
                final = IIf((inicial + 15 <= gvCargaImagenes.PageCount), inicial + 15, gvCargaImagenes.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= gvCargaImagenes.PageCount, inicial + 15, gvCargaImagenes.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = gvCargaImagenes.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVevaluacionesDisponibles_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVevaluacionesDisponibles.RowCreated

        Dim cociente As Integer = (GVevaluacionesDisponibles.PageCount / 15)
        Dim moduloIndex As Integer = (GVevaluacionesDisponibles.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVevaluacionesDisponibles.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVevaluacionesDisponibles.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVevaluacionesDisponibles.PageIndex
                final = IIf((inicial + 15 <= GVevaluacionesDisponibles.PageCount), inicial + 15, GVevaluacionesDisponibles.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVevaluacionesDisponibles.PageCount, inicial + 15, GVevaluacionesDisponibles.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVevaluacionesDisponibles.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub gvCalificaciones_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles gvCalificaciones.RowCreated

        Dim cociente As Integer = (gvCalificaciones.PageCount / 15)
        Dim moduloIndex As Integer = (gvCalificaciones.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((gvCalificaciones.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = gvCalificaciones.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = gvCalificaciones.PageIndex
                final = IIf((inicial + 15 <= gvCalificaciones.PageCount), inicial + 15, gvCalificaciones.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= gvCalificaciones.PageCount, inicial + 15, gvCalificaciones.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = gvCalificaciones.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVdetalleevaluacion_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVdetalleevaluacion.RowCreated

        Dim cociente As Integer = (GVdetalleevaluacion.PageCount / 15)
        Dim moduloIndex As Integer = (GVdetalleevaluacion.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVdetalleevaluacion.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVdetalleevaluacion.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVdetalleevaluacion.PageIndex
                final = IIf((inicial + 15 <= GVdetalleevaluacion.PageCount), inicial + 15, GVdetalleevaluacion.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVdetalleevaluacion.PageCount, inicial + 15, GVdetalleevaluacion.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVdetalleevaluacion.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVevalplantel_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVevalplantel.RowCreated

        Dim cociente As Integer = (GVevalplantel.PageCount / 15)
        Dim moduloIndex As Integer = (GVevalplantel.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVevalplantel.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVevalplantel.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVevalplantel.PageIndex
                final = IIf((inicial + 15 <= GVevalplantel.PageCount), inicial + 15, GVevalplantel.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVevalplantel.PageCount, inicial + 15, GVevalplantel.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVevalplantel.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVevaluaciones_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowCreated

        Dim cociente As Integer = (GVevaluaciones.PageCount / 15)
        Dim moduloIndex As Integer = (GVevaluaciones.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVevaluaciones.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVevaluaciones.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVevaluaciones.PageIndex
                final = IIf((inicial + 15 <= GVevaluaciones.PageCount), inicial + 15, GVevaluaciones.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVevaluaciones.PageCount, inicial + 15, GVevaluaciones.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVevaluaciones.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVevalalumno_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVevalalumno.RowCreated

        Dim cociente As Integer = (GVevalalumno.PageCount / 15)
        Dim moduloIndex As Integer = (GVevalalumno.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVevalalumno.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVevalalumno.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVevalalumno.PageIndex
                final = IIf((inicial + 15 <= GVevalalumno.PageCount), inicial + 15, GVevalalumno.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVevalalumno.PageCount, inicial + 15, GVevalalumno.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVevalalumno.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVseleccionados_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVseleccionados.RowCreated

        Dim cociente As Integer = (GVseleccionados.PageCount / 15)
        Dim moduloIndex As Integer = (GVseleccionados.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVseleccionados.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVseleccionados.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVseleccionados.PageIndex
                final = IIf((inicial + 15 <= GVseleccionados.PageCount), inicial + 15, GVseleccionados.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVseleccionados.PageCount, inicial + 15, GVseleccionados.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVseleccionados.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVseriaciones_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVseriaciones.RowCreated

        Dim cociente As Integer = (GVseriaciones.PageCount / 15)
        Dim moduloIndex As Integer = (GVseriaciones.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVseriaciones.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVseriaciones.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVseriaciones.PageIndex
                final = IIf((inicial + 15 <= GVseriaciones.PageCount), inicial + 15, GVseriaciones.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVseriaciones.PageCount, inicial + 15, GVseriaciones.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVseriaciones.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

#End Region

#Region "ROW_DATA_BOUND"

    Protected Sub GVevaluacionesDisponibles_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevaluacionesDisponibles.RowDataBound


        Dim cociente As Integer = (GVevaluacionesDisponibles.PageCount) / 15
        Dim moduloIndex As Integer = (GVevaluacionesDisponibles.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVevaluacionesDisponibles.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVevaluacionesDisponibles.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVevaluacionesDisponibles.PageIndex
            final = IIf((inicial + 15 <= GVevaluacionesDisponibles.PageCount), inicial + 15, GVevaluacionesDisponibles.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVevaluacionesDisponibles.PageCount), inicial + 15, GVevaluacionesDisponibles.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVevaluacionesDisponibles, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVevaluacionesDisponibles, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub gvCargaImagenes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvCargaImagenes.RowDataBound


        Dim cociente As Integer = (gvCargaImagenes.PageCount) / 15
        Dim moduloIndex As Integer = (gvCargaImagenes.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((gvCargaImagenes.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = gvCargaImagenes.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = gvCargaImagenes.PageIndex
            final = IIf((inicial + 15 <= gvCargaImagenes.PageCount), inicial + 15, gvCargaImagenes.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= gvCargaImagenes.PageCount), inicial + 15, gvCargaImagenes.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(gvCargaImagenes, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(gvCargaImagenes, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Dim totalPorcentajeCalificacion As Integer = 0

    Protected Sub gvCalificaciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvCalificaciones.RowDataBound


        Dim cociente As Integer = (gvCalificaciones.PageCount) / 15
        Dim moduloIndex As Integer = (gvCalificaciones.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((gvCalificaciones.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = gvCalificaciones.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = gvCalificaciones.PageIndex
            final = IIf((inicial + 15 <= gvCalificaciones.PageCount), inicial + 15, gvCalificaciones.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= gvCalificaciones.PageCount), inicial + 15, gvCalificaciones.PageCount)
        End If


        If totalPorcentajeCalificacion = 0 Then
            Try
                Dim view As DataView = CType(SDScalificaciones.Select(DataSourceSelectArguments.Empty), DataView)
                For Each row As DataRow In view.Table.Rows
                    totalPorcentajeCalificacion += CInt(row(7))
                Next
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                CType(New MessageError(GVevalplantel, Me.GetType, ex.Message.ToString()), MessageError).show()
            End Try

        End If

        If e.Row.RowType = DataControlRowType.Footer Then
            e.Row.Cells(3).Text = "SUMA TOTAL PORCENTAJE:"
            e.Row.Cells(3).HorizontalAlign = HorizontalAlign.Center
            e.Row.Cells(4).Text = totalPorcentajeCalificacion.ToString("0") + " %"
            e.Row.Cells(4).HorizontalAlign = HorizontalAlign.Center
            e.Row.Font.Bold = True
            e.Row.Font.Size = 11

            If totalPorcentajeCalificacion > 100 Then
                e.Row.ForeColor = Drawing.Color.Red
            End If
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(gvCalificaciones, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(gvCalificaciones, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVevalalumno_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevalalumno.RowDataBound


        Dim cociente As Integer = (GVevalalumno.PageCount) / 15
        Dim moduloIndex As Integer = (GVevalalumno.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVevalalumno.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVevalalumno.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVevalalumno.PageIndex
            final = IIf((inicial + 15 <= GVevalalumno.PageCount), inicial + 15, GVevalalumno.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVevalalumno.PageCount), inicial + 15, GVevalalumno.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVevalalumno, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVevalalumno, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Dim Suma As UInteger = 0
    Dim SumaUsados As UInteger = 0
    Protected Sub GVdetalleevaluacion_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVdetalleevaluacion.RowDataBound

        Dim cociente As Integer = (GVdetalleevaluacion.PageCount) / 15
        Dim moduloIndex As Integer = (GVdetalleevaluacion.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVdetalleevaluacion.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                If Not IsDBNull(DataBinder.Eval(e.Row.DataItem, "Reactivos")) Then
                    Suma += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Reactivos"))
                End If
                If Not IsDBNull(DataBinder.Eval(e.Row.DataItem, "UsarReactivos")) Then
                    SumaUsados += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "UsarReactivos"))
                Else
                    e.Row.Cells(5).Text = "Todos"
                    SumaUsados += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Reactivos"))
                End If
            End If

            If e.Row.RowType = DataControlRowType.Footer Then
                e.Row.Cells(3).Text = "TOTAL REACTIVOS:"
                e.Row.Cells(3).HorizontalAlign = HorizontalAlign.Right
                'e.Row.Cells(4).Text = dTotal.ToString("c") 'Si lo uso así queda como moneda (currency: $0.00)

                e.Row.Cells(4).Text = Suma.ToString("0")
                e.Row.Cells(4).HorizontalAlign = HorizontalAlign.Center

                e.Row.Cells(5).Text = SumaUsados.ToString("0")
                e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Center

                e.Row.Font.Bold = True
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError4.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try


        If cociente = 0 Then
            inicial = 1
            final = GVdetalleevaluacion.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVdetalleevaluacion.PageIndex
            final = IIf((inicial + 15 <= GVdetalleevaluacion.PageCount), inicial + 15, GVdetalleevaluacion.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVdetalleevaluacion.PageCount), inicial + 15, GVdetalleevaluacion.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVdetalleevaluacion, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final
                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVdetalleevaluacion, "Page$" & counter.ToString)
            Next
        End If
    End Sub


    Protected Sub GVevaluaciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowDataBound

        Dim cociente As Integer = (GVevaluaciones.PageCount) / 15
        Dim moduloIndex As Integer = (GVevaluaciones.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVevaluaciones.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVevaluaciones.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVevaluaciones.PageIndex
            final = IIf((inicial + 15 <= GVevaluaciones.PageCount), inicial + 15, GVevaluaciones.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVevaluaciones.PageCount), inicial + 15, GVevaluaciones.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(2).Text = HttpUtility.HtmlDecode(e.Row.Cells(2).Text)
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVevaluaciones, "Select$" & e.Row.RowIndex))
        End If


        If totalPorcentaje = 0 Then
            Try
                Dim view As DataView = CType(SDSevaluaciones.Select(DataSourceSelectArguments.Empty), DataView)
                For Each row As DataRow In view.Table.Rows
                    totalPorcentaje += CInt(row(4))
                Next
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                CType(New MessageError(GVevalplantel, Me.GetType, ex.Message.ToString()), MessageError).show()
            End Try

        End If
      
        If e.Row.RowType = DataControlRowType.Footer Then
            e.Row.Cells(3).Text = "SUMA TOTAL PORCENTAJE:"
            e.Row.Cells(3).HorizontalAlign = HorizontalAlign.Center
            e.Row.Cells(4).Text = totalPorcentaje.ToString("0") + " %"
            e.Row.Cells(4).HorizontalAlign = HorizontalAlign.Center
            e.Row.Font.Bold = True
            e.Row.Font.Size = 11

            If totalPorcentaje > 100 Then
                e.Row.ForeColor = Drawing.Color.Red
            End If
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVevaluaciones, "Page$" & counter.ToString)
            Next
        End If

    End Sub

    Protected Sub GVevalplantel_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevalplantel.RowDataBound


        Dim cociente As Integer = (GVevalplantel.PageCount) / 15
        Dim moduloIndex As Integer = (GVevalplantel.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVevalplantel.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVevalplantel.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVevalplantel.PageIndex
            final = IIf((inicial + 15 <= GVevalplantel.PageCount), inicial + 15, GVevalplantel.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVevalplantel.PageCount), inicial + 15, GVevalplantel.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVevalplantel, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVevalplantel, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVseleccionados_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVseleccionados.RowDataBound


        Dim cociente As Integer = (GVseleccionados.PageCount) / 15
        Dim moduloIndex As Integer = (GVseleccionados.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVseleccionados.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVseleccionados.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVseleccionados.PageIndex
            final = IIf((inicial + 15 <= GVseleccionados.PageCount), inicial + 15, GVseleccionados.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVseleccionados.PageCount), inicial + 15, GVseleccionados.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVseleccionados, "Select$" & e.Row.RowIndex))
            e.Row.Cells(3).Text = Regex.Replace(HttpUtility.HtmlDecode(e.Row.Cells(3).Text), "<[^>]*(>|$)", String.Empty)
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVseleccionados, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVseriaciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVseriaciones.RowDataBound


        Dim cociente As Integer = (GVseriaciones.PageCount) / 15
        Dim moduloIndex As Integer = (GVseriaciones.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVseriaciones.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVseriaciones.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVseriaciones.PageIndex
            final = IIf((inicial + 15 <= GVseriaciones.PageCount), inicial + 15, GVseriaciones.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVseriaciones.PageCount), inicial + 15, GVseriaciones.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVseriaciones, "Select$" & e.Row.RowIndex))

        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVseriaciones, "Page$" & counter.ToString)
            Next
        End If
    End Sub

#End Region

#Region "DATABOUND"

    Protected Sub DDLcicloescolarCalificaciones_DataBound(
                   ByVal sender As Object,
                   ByVal e As System.EventArgs) Handles DDLcicloescolarCalificaciones.DataBound

        DDLcicloescolarCalificaciones.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.CICLO, 0))

        If DDLcicloescolarCalificaciones.Items.Count = 2 Then
            DDLcicloescolarCalificaciones.SelectedIndex = 1
            DDLNivelCalificaciones.DataBind()
        End If
    End Sub

    Protected Sub DDLNivelCalificaciones_DataBound(
                  ByVal sender As Object,
                  ByVal e As System.EventArgs) Handles DDLNivelCalificaciones.DataBound
        DDLNivelCalificaciones.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Nivel"), 0))
        If DDLNivelCalificaciones.Items.Count = 2 Then
            DDLNivelCalificaciones.SelectedIndex = 1
            DDLGradoCalificaciones.DataBind()
        End If

    End Sub

    Protected Sub DDLGradoCalificaciones_DataBound(
                  ByVal sender As Object,
                  ByVal e As System.EventArgs) Handles DDLGradoCalificaciones.DataBound
        DDLGradoCalificaciones.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Grado"), 0))

        If DDLGradoCalificaciones.Items.Count = 2 Then
            DDLGradoCalificaciones.SelectedIndex = 1
            DDLAsignaturaCalificaciones.DataBind()
        End If

    End Sub

    Protected Sub DDLAsignaturaCalificaciones_DataBound(
                  ByVal sender As Object,
                  ByVal e As System.EventArgs) Handles DDLAsignaturaCalificaciones.DataBound

        DDLAsignaturaCalificaciones.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Asignatura"), 0))

        If DDLAsignaturaCalificaciones.Items.Count = 2 Then
            DDLAsignaturaCalificaciones.SelectedIndex = 1
            gvCalificaciones.DataBind()
        End If

    End Sub

    Protected Sub ddlCalIndicador_DataBound(
                 ByVal sender As Object,
                 ByVal e As System.EventArgs) Handles ddlCalIndicador.DataBound

        ddlCalIndicador.Items.Insert(0, New ListItem("---Elija el Indicador", 0))
    End Sub

    Protected Sub ddlAleatoria_DataBound(
                 ByVal sender As Object,
                 ByVal e As System.EventArgs) Handles ddlAleatoria.DataBound

        ddlAleatoria.Items.Insert(0, New ListItem("---Elija el modo", 0))
    End Sub

    Protected Sub ddlOpcionAleatoria_DataBound(
                ByVal sender As Object,
                ByVal e As System.EventArgs) Handles ddlOpcionAleatoria.DataBound

        ddlOpcionAleatoria.Items.Insert(0, New ListItem("---Elija el modo", 0))
    End Sub

    Protected Sub ddlIndicadorRepeticion_DataBound(
               ByVal sender As Object,
               ByVal e As System.EventArgs) Handles ddlIndicadorRepeticion.DataBound

        ddlIndicadorRepeticion.Items.Insert(0, New ListItem("---Elija el indicador", 0))
    End Sub

    Protected Sub ddlNivelesSubtemasDataBound(
             ByVal sender As Object,
             ByVal e As System.EventArgs) Handles ddlNivelesSubtemas.DataBound

        ddlNivelesSubtemas.Items.Insert(0, New ListItem("---Elija el nivel", 0))

        If ddlNivelesSubtemas.Items.Count = 2 Then
            ddlNivelesSubtemas.SelectedIndex = 1
            ddlGradosSubtemas.DataBind()
        End If

    End Sub

    Protected Sub ddlGradosSubtemas_DataBound(
         ByVal sender As Object,
         ByVal e As System.EventArgs) Handles ddlGradosSubtemas.DataBound

        ddlGradosSubtemas.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Grado"), 0))

        If ddlGradosSubtemas.Items.Count = 2 Then
            ddlGradosSubtemas.SelectedIndex = 1
            ddlAsignaturasSubtemas.DataBind()
        End If

    End Sub

    Protected Sub ddlAsignaturasSubtemas_DataBound(
         ByVal sender As Object,
         ByVal e As System.EventArgs) Handles ddlAsignaturasSubtemas.DataBound

        ddlAsignaturasSubtemas.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Asignatura"), 0))
        If ddlAsignaturasSubtemas.Items.Count = 2 Then
            ddlAsignaturasSubtemas.SelectedIndex = 1
            DDLtemas.DataBind()
        End If

    End Sub

    Protected Sub DDLtemas_DataBound(
         ByVal sender As Object,
         ByVal e As System.EventArgs) Handles DDLtemas.DataBound

        DDLtemas.Items.Insert(0, New ListItem("---Elija el tema", 0))

        If DDLtemas.Items.Count = 2 Then
            DDLtemas.SelectedIndex = 1
            DDLsubtemas.DataBind()
        End If

    End Sub

    Protected Sub DDLsubtemas_DataBound(
         ByVal sender As Object,
         ByVal e As System.EventArgs) Handles DDLsubtemas.DataBound

        DDLsubtemas.Items.Insert(0, New ListItem("---Elija el subtema", 0))
        If DDLsubtemas.Items.Count = 2 Then
            DDLsubtemas.SelectedIndex = 1
            GVdetalleevaluacion.DataBind()
        End If
    End Sub

    Protected Sub ddlResultadoRepeticion_DataBound(
         ByVal sender As Object,
         ByVal e As System.EventArgs) Handles ddlResultadoRepeticion.DataBound

        ddlResultadoRepeticion.Items.Insert(0, New ListItem("---Elija el resultado", 0))

    End Sub

    Protected Sub DDLinstitucionFechasPlanteles_DataBound(
         ByVal sender As Object,
         ByVal e As System.EventArgs) Handles DDLinstitucionFechasPlanteles.DataBound

        DDLinstitucionFechasPlanteles.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Institucion"), 0))
        If DDLinstitucionFechasPlanteles.Items.Count = 2 Then
            DDLinstitucionFechasPlanteles.SelectedIndex = 1
            DDLPlantelFechasPlanteles.DataBind()
        End If

    End Sub

    Protected Sub DDLPlantelFechasPlanteles_DataBound(
         ByVal sender As Object,
         ByVal e As System.EventArgs) Handles DDLPlantelFechasPlanteles.DataBound

        DDLPlantelFechasPlanteles.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Plantel"), 0))
        If DDLPlantelFechasPlanteles.Items.Count = 2 Then
            DDLPlantelFechasPlanteles.SelectedIndex = 1
            GVevalplantel.DataBind()
        End If

    End Sub

    Protected Sub DDLEstatusActividadFP_DataBound(
         ByVal sender As Object,
         ByVal e As System.EventArgs) Handles DDLEstatusActividadFP.DataBound

        DDLEstatusActividadFP.Items.Insert(0, New ListItem("---Elija el estatus", 0))

    End Sub

    Protected Sub DDLinstitucionFechasAlumnos_DataBound(
       ByVal sender As Object,
       ByVal e As System.EventArgs) Handles DDLinstitucionFechasAlumnos.DataBound

        DDLinstitucionFechasAlumnos.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Institucion"), 0))

        If DDLinstitucionFechasAlumnos.Items.Count = 2 Then
            DDLinstitucionFechasAlumnos.SelectedIndex = 1
            DDLplantelFechaAlumnos.DataBind()
        End If
    End Sub

    Protected Sub DDLplantelFechaAlumnos_DataBound(
      ByVal sender As Object,
      ByVal e As System.EventArgs) Handles DDLplantelFechaAlumnos.DataBound

        DDLplantelFechaAlumnos.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Plantel"), 0))

        If DDLplantelFechaAlumnos.Items.Count = 2 Then
            DDLplantelFechaAlumnos.SelectedIndex = 1
            DDLgrupoFechasAlumnos.DataBind()
        End If

    End Sub

    Protected Sub DDLgrupoFechasAlumnos_DataBound(
      ByVal sender As Object,
      ByVal e As System.EventArgs) Handles DDLgrupoFechasAlumnos.DataBound

        DDLgrupoFechasAlumnos.Items.Insert(0, New ListItem("---Elija el grupo", 0))

        If DDLgrupoFechasAlumnos.Items.Count = 2 Then
            DDLgrupoFechasAlumnos.SelectedIndex = 1
            DDLgrupoFechasAlumnos_SelectedIndexChanged(Nothing, Nothing)
            GVevalalumno.DataBind()
        End If
    End Sub

    Protected Sub DDLSubtemasADemo_DataBound(
      ByVal sender As Object,
      ByVal e As System.EventArgs) Handles DDLSubtemasADemo.DataBound

        DDLSubtemasADemo.Items.Insert(0, New ListItem("---Elija el subtema", 0))

    End Sub

    Protected Sub DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCargaImagenes.DataBound
        If (gvCargaImagenes.Rows.Count = 0) Then
            btnEliminarEvaluacion.Visible = False
            gvCargaImagenes.SelectedIndex = -1
        End If
    End Sub

    Protected Sub DDLcalificacionSeriarAct_DataBound(sender As Object, e As EventArgs) Handles DDLcalificacionSeriarAct.DataBound
        DDLcalificacionSeriarAct.Items.Insert(0, New ListItem("---Elija la calificación", 0))

        If DDLcalificacionSeriarAct.Items.Count = 2 Then
            DDLcalificacionSeriarAct.SelectedIndex = 1
            DDLasignaturaSeriarAct.DataBind()
        End If
    End Sub

    Protected Sub DDLasignaturaSeriarAct_DataBound(sender As Object, e As EventArgs) Handles DDLasignaturaSeriarAct.DataBound
        DDLasignaturaSeriarAct.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Asignatura"), 0))
        If DDLasignaturaSeriarAct.Items.Count = 2 Then
            DDLasignaturaSeriarAct.SelectedIndex = 1
            GVevaluacionesDisponibles.DataBind()
        End If

    End Sub

#End Region

#Region "SELECTED_INDEX_CHANGED"

    Protected Sub GVevaluacionesDisponibles_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVevaluacionesDisponibles.SelectedIndexChanged
        btnSeriarActividad.Visible = True
    End Sub

    Protected Sub gvCalificaciones_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCalificaciones.SelectedIndexChanged
        btnCrearCalificaciones.Visible = False
        btnModificarCalificaciones.Visible = True
        btnEliminarCalificaciones.Visible = True
        btnGuardarCambiosCalificaciones.Visible = True
        btnCrearEvaluacion.Visible = True
        GVevaluaciones.SelectedIndex = -1
        tbCaldescripcion.Text = gvCalificaciones.SelectedDataKey("Descripcion")
        tbCalClave.Text = gvCalificaciones.SelectedDataKey("Clave")
        tbCalconsecutivo.Text = IIf(Not IsDBNull(gvCalificaciones.SelectedDataKey("Consecutivo")), gvCalificaciones.SelectedDataKey("Consecutivo").ToString, String.Empty)
        tbCalValor.Text = CInt(gvCalificaciones.SelectedDataKey("Valor"))
        ddlCalIndicador.SelectedValue = IIf(Not IsDBNull(gvCalificaciones.SelectedDataKey("IdIndicador")), gvCalificaciones.SelectedDataKey("IdIndicador").ToString, "0")
        calificacionSelected.Text = gvCalificaciones.SelectedDataKey("Descripcion").ToString()
    End Sub

    Protected Sub GVevalplantel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevalplantel.SelectedIndexChanged

        btnQuitarAsignacionFP.Visible = True
        btnActualizarFP.Visible = True
        TBInicioPlanteles.Text = CDate(GVevalplantel.SelectedDataKey("InicioContestar")).ToShortDateString()
        TBPenalizacionPlanteles.Text = CDate(GVevalplantel.SelectedDataKey("FinSinPenalizacion")).ToShortDateString()
        TBFinPlanteles.Text = CDate(GVevalplantel.SelectedDataKey("FinContestar")).ToShortDateString()
        DDLEstatusActividadFP.SelectedValue = GVevalplantel.SelectedDataKey("Estatus")

    End Sub

    Protected Sub gvCargaImagenes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvCargaImagenes.SelectedIndexChanged
        btnDeleteFileQuestion.Visible = True
        msgInfo.show("¡¡ATENCIÓN!!! LA URL ES:", "http://" + HttpContext.Current.Request.Url.Host + Siget.Config.Global.urlApoyoPlanteamientos + gvCargaImagenes.SelectedDataKey("NombreApoyo"))

    End Sub

    Protected Sub DDLsubtemas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLsubtemas.SelectedIndexChanged
        btnAgregarTemas.Visible = True
    End Sub

    Protected Sub DDLgrupoFechasAlumnos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupoFechasAlumnos.SelectedIndexChanged

        RefreshEvalAlumno()
        If GVevaluaciones.Rows.Count() > 0 Then
            If GVevaluaciones.CountRowsSelectedByCheckbox() > 0 Then
                SDSalumnos.SelectCommand = String.Format("select IdAlumno, rtrim(ApePaterno) + ' ' + rtrim(ApeMaterno) + ' ' + rtrim(Nombre) + '(' +" +
                                                          " rtrim(Matricula) + ')' as NomAlumno from Alumno where IdGrupo = {0} and IdAlumno not in " +
                                                          "(select IdAlumno from EvaluacionAlumno where IdEvaluacion in ({1})) and Estatus != 'Baja'" +
                                                          "order by NomAlumno", DDLgrupoFechasAlumnos.SelectedValue, String.Join(", ", GVevaluaciones.getRowsSelectedByCheckbox().ToArray()))
                SDSalumnos.DataBind()
                CBLalumnos.DataBind()
            Else
                SDSalumnos.SelectCommand = String.Format("select IdAlumno, rtrim(ApePaterno) + ' ' + rtrim(ApeMaterno) + ' ' + rtrim(Nombre) + '(' + " +
                                                " rtrim(Matricula) + ')' as NomAlumno from Alumno where IdGrupo = {0} and IdAlumno not in" +
                                                " (select IdAlumno from EvaluacionAlumno where IdEvaluacion in ({1}) ) and Estatus != 'Baja'" +
                                                " order by NomAlumno", DDLgrupoFechasAlumnos.SelectedValue, GVevaluaciones.SelectedDataKey("IdEvaluacion"))
                SDSalumnos.DataBind()
                CBLalumnos.DataBind()
            End If

        End If


    End Sub

    Protected Sub ddlIndicadorRepeticion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlIndicadorRepeticion.SelectedIndexChanged, ddlResultadoRepeticion.SelectedIndexChanged
        If chkRepeticion.Checked Then
            panelRepeticion.Attributes("style") = "display:block;"
        End If
    End Sub



#End Region

#Region "INSERT"

    Protected Sub btnAsignarNoReactivos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAsignarNoReactivos.Click
        Try
            Dim usa As Byte
            If Trim(LBusar.Text) = String.Empty Then
                usa = 0 'Esto lo hago para que entre en la condición siguiente
            Else
                If CType(LBusar.Text, Integer) < 0 Then
                    usa = 0
                Else
                    usa = LBusar.Text
                End If
            End If

            If usa < (GVdetalleevaluacion.SelectedDataKey("Reactivos").ToString) Then
                SDSdetalleevaluacion.UpdateCommand = "SET dateformat dmy; UPDATE DetalleEvaluacion SET UsarReactivos = " + LBusar.Text + ", FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdDetalleEvaluacion = " + GVdetalleevaluacion.SelectedDataKey("IdDetalleEvaluacion").ToString()
                SDSdetalleevaluacion.Update()
                GVevaluaciones.DataBind()
                CType(New MessageSuccess(DDLsubtemas, Me.GetType, "Se ha asignado correctamente el número de de reactivos"), MessageSuccess).show()
            ElseIf usa = (GVdetalleevaluacion.SelectedDataKey("Reactivos").ToString) Then
                SDSdetalleevaluacion.UpdateCommand = "SET dateformat dmy; UPDATE DetalleEvaluacion SET UsarReactivos = NULL, FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdDetalleEvaluacion = " + GVdetalleevaluacion.SelectedDataKey("IdDetalleEvaluacion").ToString()
                SDSdetalleevaluacion.Update()
                GVevaluaciones.DataBind()
                CType(New MessageSuccess(DDLsubtemas, Me.GetType, "Se ha asignado correctamente el número de de reactivos"), MessageSuccess).show()
            Else
                CType(New MessageError(DDLsubtemas, Me.GetType, "No puede asignar igual o más reactivos aleatorios del total que tiene actualmente el Subtema."), MessageError).show()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(DDLsubtemas, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub btnAgregarFP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregarFP.Click
        Try

            Dim listIndexes As List(Of String) = GVevaluaciones.getRowsSelectedByCheckbox()
            Dim h As Integer
            Dim IdEvaluacion As Integer

            If listIndexes.Count > 0 Then

                For h = 0 To listIndexes.Count - 1
                    IdEvaluacion = listIndexes(h)
                    SDSevalPlantel.InsertCommand = "SET dateformat dmy; INSERT INTO EvaluacionPlantel (IdEvaluacion,IdPlantel,InicioContestar,FinSinPenalizacion,FinContestar,Estatus, FechaModif, Modifico) VALUES (" + IdEvaluacion.ToString() + "," + DDLPlantelFechasPlanteles.SelectedValue + ",'" + CDate(TBInicioPlanteles.Text).ToShortDateString + "','" + TBPenalizacionPlanteles.Text + " 23:59:00" + "','" + TBFinPlanteles.Text + " 23:59:00" + "','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
                    SDSevalPlantel.Insert()
                Next
            Else
                SDSevalPlantel.InsertCommand = "SET dateformat dmy; INSERT INTO EvaluacionPlantel (IdEvaluacion,IdPlantel,InicioContestar,FinSinPenalizacion,FinContestar,Estatus, FechaModif, Modifico) VALUES (" + GVevaluaciones.SelectedDataKey("IdEvaluacion").ToString() + "," + DDLPlantelFechasPlanteles.SelectedValue + ",'" + CDate(TBInicioPlanteles.Text).ToShortDateString + "','" + TBPenalizacionPlanteles.Text + " 23:59:00" + "','" + TBFinPlanteles.Text + " 23:59:00" + "','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
                SDSevalPlantel.Insert()
            End If
            RefreshEvalPlantel()
            GVevalplantel.DataBind()
            DDLPlantelFechasPlanteles.DataBind()
            CType(New MessageSuccess(GVevalplantel, Me.GetType, "Se ha agregado correctamente"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVevalplantel, Me.GetType, "Asegúrese de una Calificación Existente <br />"), MessageError).show()
        End Try
    End Sub

    Protected Sub btnCrearNuevoCalificaciones_Click(
                 sender As Object,
                 e As EventArgs) Handles btnCrearNuevoCalificaciones.Click

        creaCalificacion() ' in

    End Sub


    Protected Sub CreaEvaluacion()
        If verificaCamposEvaluacion() Then
            Try
                SDSevaluaciones.InsertParameters.Clear()
                SDSevaluaciones.InsertCommand =
" SET dateformat dmy; " &
" INSERT INTO " &
" Evaluacion (ClaveBateria,IdCalificacion, Porcentaje, InicioContestar, " &
"           FinSinPenalizacion, FinContestar, Tipo, Penalizacion, Aleatoria, Estatus, " &
"           SeEntregaDocto, Califica, Instruccion, FechaModif, Modifico, " &
"           RepiteAutomatica, RepiteIndicador, RepiteRango, RepiteMaximo, PermiteSegunda, EsForo," &
            "OpcionesAleatorias,NoActividad,CerrarCaptura) " &
" VALUES (@ClaveBateria, @IdCalificacion, @Porcentaje, @InicioContestar, " &
"           @FinSinPenalizacion, @FinContestar, @Tipo, @Penalizacion, @Aleatoria, @Estatus, " &
"           @SeEntregaDocto, @Califica, @Instruccion, getdate(), @Modifico, " &
"           @RepiteAutomatica, @RepiteIndicador, @RepiteRango, @RepiteMaximo, @SegundaVuelta," &
            " @EsForo,@OpcionesAleatorias,@NoActividad,@CerrarCaptura)"
                SDSevaluaciones.InsertParameters.Add("ClaveBateria", HttpUtility.HtmlEncode(TBclavebateria.Text.Trim()))
                SDSevaluaciones.InsertParameters.Add("IdCalificacion", gvCalificaciones.SelectedDataKey("IdCalificacion"))
                SDSevaluaciones.InsertParameters.Add("Porcentaje", TBporcentaje.Text.Trim())
                SDSevaluaciones.InsertParameters.Add("InicioContestar", tbFechaInicio.Text.Trim())
                SDSevaluaciones.InsertParameters.Add("FinSinPenalizacion", tbFechaPenalizacion.Text.Trim() & " 23:59:00")
                SDSevaluaciones.InsertParameters.Add("FinContestar", tbFechaFin.Text.Trim() & " 23:59:00")
                SDSevaluaciones.InsertParameters.Add("Tipo", DDLtipo.SelectedValue)
                SDSevaluaciones.InsertParameters.Add("Penalizacion", TBpenalizacion.Text)
                SDSevaluaciones.InsertParameters.Add("Aleatoria", ddlAleatoria.SelectedValue)
                SDSevaluaciones.InsertParameters.Add("Estatus", DDLestatus.SelectedValue)
                SDSevaluaciones.InsertParameters.Add("SeEntregaDocto", CBseentregadocto.Checked.ToString)
                SDSevaluaciones.InsertParameters.Add("Califica", DDLcalifica.SelectedValue)
                SDSevaluaciones.InsertParameters.Add("SegundaVuelta", chkSegundavuelta.Checked)
                SDSevaluaciones.InsertParameters.Add("Instruccion", HttpUtility.HtmlDecode(ckEditor.Text))
                SDSevaluaciones.InsertParameters.Add("Modifico", User.Identity.Name)
                SDSevaluaciones.InsertParameters.Add("RepiteAutomatica", chkRepeticion.Checked)
                SDSevaluaciones.InsertParameters.Add("EsForo", ChkEsForo.Checked)
                SDSevaluaciones.InsertParameters.Add("OpcionesAleatorias", ddlOpcionAleatoria.SelectedValue)
                SDSevaluaciones.InsertParameters.Add("NoActividad", Trim(TBNoActividad.Text))
                SDSevaluaciones.InsertParameters.Add("CerrarCaptura", CBCerrarCaptura.Checked)
                If chkRepeticion.Checked Then
                    SDSevaluaciones.InsertParameters.Add("RepiteIndicador", ddlIndicadorRepeticion.SelectedValue)
                    SDSevaluaciones.InsertParameters.Add("RepiteRango", ddlResultadoRepeticion.SelectedValue)
                    SDSevaluaciones.InsertParameters.Add("RepiteMaximo", Integer.Parse(tbRepeticiones.Text.Trim()))
                Else
                    SDSevaluaciones.InsertParameters.Add("RepiteIndicador", Nothing)
                    SDSevaluaciones.InsertParameters.Add("RepiteRango", Nothing)
                    SDSevaluaciones.InsertParameters.Add("RepiteMaximo", Nothing)
                End If

                SDSevaluaciones.Insert()

                GVevaluaciones.DataBind()
                CType(New MessageSuccess(gvCalificaciones, Me.GetType, "Se creó la evaluación."), MessageSuccess).show()

            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                CType(New MessageError(gvCalificaciones, Me.GetType, "Ocurrió un error desconocido. Si el problema persiste " &
                                    "notifique al administrador del sistema."), MessageError).show()

            End Try
        End If
    End Sub

    ''' <summary>
    ''' Intenta crear una calificación con el formato de creación/modificación
    ''' </summary>
    Protected Sub creaCalificacion()
        If verificaCamposCalificacion() Then
            Try
                SDScalificaciones.InsertParameters.Clear() ' para múltiples intentos esto es importante
                SDScalificaciones.InsertCommand =
"SET dateformat dmy; " &
"INSERT INTO Calificacion (IdCicloEscolar, Descripcion, Clave, Consecutivo, " &
"                           IdAsignatura, Valor, IdIndicador, FechaModif, Modifico,NumeroDias) " &
"VALUES (@IdCicloEscolar, @Descripcion, @Clave, @Consecutivo, " &
"                           @IdAsignatura, @Valor, @IdIndicador, getdate(), @Modifico,@NumeroDias) "
                SDScalificaciones.InsertParameters.Add("IdCicloEscolar", DDLcicloescolarCalificaciones.SelectedValue)
                SDScalificaciones.InsertParameters.Add("Descripcion", tbCaldescripcion.Text.Trim())
                SDScalificaciones.InsertParameters.Add("Clave", tbCalClave.Text.Trim())
                SDScalificaciones.InsertParameters.Add("Consecutivo", IIf(tbCalconsecutivo.Text <> String.Empty, tbCalconsecutivo.Text, String.Empty))
                SDScalificaciones.InsertParameters.Add("IdAsignatura", DDLAsignaturaCalificaciones.SelectedValue)
                SDScalificaciones.InsertParameters.Add("Valor", tbCalValor.Text.Trim())
                SDScalificaciones.InsertParameters.Add("NumeroDias", IIf(Trim(TBNumeroDiasAutoRegistro.Text) <> String.Empty, TBNumeroDiasAutoRegistro.Text, 1))
                If ddlCalIndicador.SelectedIndex > 0 Then
                    SDScalificaciones.InsertParameters.Add("IdIndicador", ddlCalIndicador.SelectedValue)
                Else
                    SDScalificaciones.InsertParameters.Add("IdIndicador", Nothing)
                End If
                SDScalificaciones.InsertParameters.Add("Modifico", User.Identity.Name)

                SDScalificaciones.Insert()
                gvCalificaciones.DataBind()
                CType(New MessageSuccess(gvCargaImagenes, Me.GetType, "Se creó la calificación."), MessageSuccess).show()

            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                CType(New MessageError(gvCargaImagenes, Me.GetType, "Verifica los campos"), MessageError).show()

            End Try
        End If
    End Sub
    Protected Sub btnCrearnuevaEvaluacion_Click(
                   sender As Object,
                   e As EventArgs) Handles btnCrearnuevaEvaluacion.Click

        CreaEvaluacion()
    End Sub

    Protected Sub btnAgregarTemas_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregarTemas.Click

        If Not DDLsubtemas.SelectedValue = 0 Then
            Try
                SDSdetalleevaluacion.InsertCommand = "SET dateformat dmy; INSERT INTO DetalleEvaluacion (IdEvaluacion,IdSubtema, FechaModif, Modifico) VALUES (" + GVevaluaciones.SelectedDataKey("IdEvaluacion").ToString() + "," + DDLsubtemas.SelectedValue + ", getdate(),'" & User.Identity.Name & "')"
                SDSdetalleevaluacion.Insert()
                CType(New MessageSuccess(DDLsubtemas, Me.GetType, "El tema se agregó correctamente"), MessageSuccess).show()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                CType(New MessageError(DDLsubtemas, Me.GetType, "Verifique los campos"), MessageError).show()
            End Try
            DDLsubtemas.DataBind()
            GVdetalleevaluacion.DataBind()
        End If
    End Sub

    Protected Sub btnAgregarFechasAlumnos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregarFechasAlumnos.Click
        Try
            Dim I As Integer
            Dim H As Integer
            Dim IdEvaluacion As Integer
            Dim listIndexes As List(Of String) = GVevaluaciones.getRowsSelectedByCheckbox()

            If listIndexes.Count > 0 Then

                For H = 0 To listIndexes.Count() - 1
                    IdEvaluacion = listIndexes(H)

                    For I = 0 To CBLalumnos.Items.Count - 1
                        If CBLalumnos.Items(I).Selected Then
                            SDSevalAlumno.InsertCommand =
                                "SET dateformat dmy; INSERT INTO EvaluacionAlumno (IdEvaluacion,IdAlumno,InicioContestar,FinSinPenalizacion,FinContestar,Estatus, FechaModif, Modifico) VALUES (" + IdEvaluacion.ToString + "," + CBLalumnos.Items(I).Value + ",'" + TBFechaInicioFechasAlumnos.Text + "','" + TBFechaPenalizacionFechasAlumnos.Text + " 23:59:00','" + TBFechaFinFechasAlumnos.Text + " 23:59:00','" + ddlEstatusFechasAlumnos.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
                            SDSevalAlumno.Insert()
                        End If
                    Next
                Next
            Else

                For I = 0 To CBLalumnos.Items.Count - 1
                    If CBLalumnos.Items(I).Selected Then
                        SDSevalAlumno.InsertCommand =
                            "SET dateformat dmy; INSERT INTO EvaluacionAlumno (IdEvaluacion,IdAlumno,InicioContestar,FinSinPenalizacion,FinContestar,Estatus, FechaModif, Modifico) VALUES (" + GVevaluaciones.SelectedDataKey("IdEvaluacion").ToString + "," + CBLalumnos.Items(I).Value + ",'" + TBFechaInicioFechasAlumnos.Text + "','" + TBFechaPenalizacionFechasAlumnos.Text + " 23:59:00','" + TBFechaFinFechasAlumnos.Text + " 23:59:00','" + ddlEstatusFechasAlumnos.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
                        SDSevalAlumno.Insert()
                    End If
                Next
            End If
            CType(New MessageSuccess(GVevalplantel, Me.GetType, "La asignación fue éxitosa"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVevalplantel, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
        DDLgrupoFechasAlumnos_SelectedIndexChanged(Nothing, Nothing)
        RefreshEvalAlumno()
        GVevalalumno.DataBind()
    End Sub

    Protected Sub btnAgregarReactivo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAgregarReactivo.Click
        'AQUI PUEDO ELEGIR SOLO LOS REACTIVOS QUE FUERON CLASIFICADOS COMO "DEMO" EN CADA UNO DE LOS SUBTEMAS, Y AL
        'CREAR ESTA ACTIVIDAD DEMO TOMARÁ ESOS REACTIVOS QUE YO ELIJA PARA MOSTRARLOS. LOS REACTIVOS "DEMO" NUNCA SE
        'MUESTRAN EN LAS ACTIVIDADES NORMALES
        Try
            Dim I As Integer
            For I = 0 To CBLreactivos.Items.Count - 1
                If CBLreactivos.Items(I).Selected Then
                    SDSevaluaciondemo.InsertCommand = "SET dateformat dmy; INSERT INTO EvaluacionDemo (IdEvaluacion,IdPlanteamiento, FechaModif, Modifico) VALUES (" +
                        GVevaluaciones.SelectedDataKey("IdEvaluacion").ToString + "," +
                        CBLreactivos.Items(I).Value + ", getdate(), '" & User.Identity.Name & "')"
                    SDSevaluaciondemo.Insert()
                End If
            Next
            CType(New MessageSuccess(GVevalplantel, Me.GetType, "Se han agregado los reactivos"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVevalplantel, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
        CBLreactivos.DataBind()
        GVseleccionados.DataBind()
    End Sub

    Protected Sub btnSeriarActividad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSeriarActividad.Click
        Try
            If CHKminima.Checked Then
                If verificaMinima() Then
                    'SDSevaluacionesSer.UpdateCommand = "SET dateformat dmy; UPDATE EVALUACION SET IdEvaluacionPrevia = " & GVevaluacionesDisponibles.SelectedRow.Cells(1).Text & ", PreviaMinima = " & TBminima.Text.Trim() & " WHERE IdEvaluacion = " + GVevaluacionesG.SelectedRow.Cells(1).Text
                    SDSevaluacionesSer.InsertCommand = "SET dateformat dmy; INSERT INTO SeriacionEvaluaciones (IdEvaluacion, IdEvaluacionPrevia, Minima, FechaModif, Modifico) VALUES (" & GVevaluaciones.SelectedDataKey.Value & ", " & GVevaluacionesDisponibles.SelectedDataKey.Value & ", '" & TBminima.Text.Trim() & "', getdate(), '" & User.Identity.Name & "')"
                    SDSevaluacionesSer.Insert()
                    GVevaluacionesDisponibles.DataBind()
                    GVseriaciones.DataBind()
                End If
            Else
                'SDSevaluacionesSer.UpdateCommand = "SET dateformat dmy; UPDATE EVALUACION SET IdEvaluacionPrevia = " + GVevaluacionesDisponibles.SelectedRow.Cells(1).Text + ", PreviaMinima = NULL WHERE IdEvaluacion = " + GVevaluacionesG.SelectedRow.Cells(1).Text
                SDSevaluacionesSer.InsertCommand = "SET dateformat dmy; INSERT INTO SeriacionEvaluaciones (IdEvaluacion, IdEvaluacionPrevia, FechaModif, Modifico) VALUES (" & GVevaluaciones.SelectedDataKey.Value & ", " & GVevaluacionesDisponibles.SelectedDataKey.Value & ", getdate(), '" & User.Identity.Name & "')"
                SDSevaluacionesSer.Insert()
                GVevaluacionesDisponibles.DataBind()
                GVseriaciones.DataBind()
            End If
            GVevaluacionesDisponibles.SelectRow(-1)
            CType(New MessageSuccess(GVevalplantel, Me.GetType, "Se ha seriado la actividad con éxito"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVevalplantel, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
    End Sub
#End Region

#Region "UPDATE"

    Protected Sub btnActualizarSeriacion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarSeriacion.Click
        Try
            If CHKminimaEdit.Checked Then
                If verificaMinimaEdit() Then
                    'SDSevaluacionesSer.UpdateCommand = "SET dateformat dmy; UPDATE EVALUACION SET IdEvaluacionPrevia = " & GVevaluacionesDisponibles.SelectedRow.Cells(1).Text & ", PreviaMinima = " & TBminima.Text.Trim() & " WHERE IdEvaluacion = " + GVevaluacionesG.SelectedRow.Cells(1).Text
                    SDSevaluacionesSer.UpdateCommand = "SET dateformat dmy; UPDATE SeriacionEvaluaciones SET MINIMA = '" & TBminimaEdit.Text.Trim() & "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdEvaluacion = " & GVevaluaciones.SelectedDataKey.Value & " AND IdEvaluacionPrevia = " & GVseriaciones.SelectedDataKey.Value
                    SDSevaluacionesSer.Update()
                    GVseriaciones.DataBind()
                End If
            Else
                'SDSevaluacionesSer.UpdateCommand = "SET dateformat dmy; UPDATE EVALUACION SET IdEvaluacionPrevia = " + GVevaluacionesDisponibles.SelectedRow.Cells(1).Text + ", PreviaMinima = NULL WHERE IdEvaluacion = " + GVevaluacionesG.SelectedRow.Cells(1).Text
                SDSevaluacionesSer.UpdateCommand = "SET dateformat dmy; UPDATE SeriacionEvaluaciones SET MINIMA = NULL, FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdEvaluacion = " & GVevaluaciones.SelectedDataKey.Value & " AND IdEvaluacionPrevia = " & GVseriaciones.SelectedDataKey.Value
                SDSevaluacionesSer.Update()
                GVseriaciones.DataBind()
            End If
            CType(New MessageSuccess(GVseriaciones, Me.GetType, "Se ha actualizado la calificación minima de la seriación"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVseriaciones, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try

    End Sub
    Protected Sub btnGuardarCambiosCalificacionesr_Click(
                      sender As Object,
                      e As EventArgs) Handles btnGuardarCambiosCalificaciones.Click
        modificaCalificacion() ' intenta modificar la calificación seleccionada
    End Sub

    Protected Sub modificaCalificacion()
        If verificaCamposCalificacion() Then
            Try
                SDScalificaciones.UpdateParameters.Clear() ' para múltiples intentos esto es importante
                SDScalificaciones.UpdateCommand =
"SET dateformat dmy; " &
"UPDATE Calificacion " &
"SET IdCicloEscolar = @IdCicloEscolar, Descripcion = @Descripcion, Clave = @Clave, " &
"       Consecutivo = @Consecutivo, IdAsignatura = @IdAsignatura, Valor = @Valor, " &
"       IdIndicador = @IdIndicador, FechaModif = getdate(), Modifico = @Modifico, NumeroDias=@NumeroDias " &
"WHERE IdCalificacion = @IdCalificacion"
                SDScalificaciones.UpdateParameters.Add("IdCicloEscolar", DDLcicloescolarCalificaciones.SelectedValue)
                SDScalificaciones.UpdateParameters.Add("Descripcion", tbCaldescripcion.Text.Trim())
                SDScalificaciones.UpdateParameters.Add("Clave", tbCalClave.Text.Trim())
                SDScalificaciones.UpdateParameters.Add("Consecutivo", tbCalconsecutivo.Text.Trim())
                SDScalificaciones.UpdateParameters.Add("IdAsignatura", DDLAsignaturaCalificaciones.SelectedValue)
                SDScalificaciones.UpdateParameters.Add("Valor", tbCalValor.Text.Trim())
                If ddlCalIndicador.SelectedIndex > 0 Then
                    SDScalificaciones.UpdateParameters.Add("IdIndicador", ddlCalIndicador.SelectedValue)
                Else
                    SDScalificaciones.UpdateParameters.Add("IdIndicador", Nothing)
                End If
                SDScalificaciones.UpdateParameters.Add("Modifico", User.Identity.Name)
                SDScalificaciones.UpdateParameters.Add("NumeroDias", IIf(Trim(TBNumeroDiasAutoRegistro.Text) <> String.Empty, TBNumeroDiasAutoRegistro.Text, 1))
                SDScalificaciones.UpdateParameters.Add("IdCalificacion", gvCalificaciones.SelectedDataKey("IdCalificacion").ToString())

                SDScalificaciones.Update()
                gvCalificaciones.DataBind()
                CType(New MessageSuccess(gvCargaImagenes, Me.GetType, "Se modificó la calificación."), MessageSuccess).show()

            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                CType(New MessageError(gvCargaImagenes, Me.GetType, "Verifica los campos"), MessageError).show()

            End Try
        End If
    End Sub


    Protected Sub btnActualizarEvaluacion_Click(
                  sender As Object,
                  e As EventArgs) Handles btnActualizarEvaluacion.Click
        ModificaEvaluacion()
    End Sub

    Protected Sub ModificaEvaluacion()
        If verificaCamposEvaluacion() Then
            Try
                SDSevaluaciones.UpdateParameters.Clear()
                SDSevaluaciones.UpdateCommand =
" SET dateformat dmy; " &
" UPDATE Evaluacion " &
" SET ClaveBateria = @ClaveBateria, IdCalificacion = @IdCalificacion, " &
"   Porcentaje = @Porcentaje, InicioContestar = @InicioContestar, FinSinPenalizacion = @FinSinPenalizacion, " &
"   FinContestar = @FinContestar, Tipo = @Tipo, Penalizacion = @Penalizacion, Aleatoria = @Aleatoria, " &
"   SeEntregaDocto = @SeEntregaDocto, Estatus = @Estatus, Califica = @Califica, FechaModif = getdate(), " &
"   Modifico = @Modifico, Instruccion = @Instruccion, RepiteAutomatica = @RepiteAutomatica, " &
"   RepiteIndicador = @RepiteIndicador, RepiteRango = @RepiteRango, RepiteMaximo = @RepiteMaximo, " &
"   PermiteSegunda = @SegundaVuelta, EsForo = @EsForo, OpcionesAleatorias=@OpcionesAleatorias, NoActividad=@NoActividad , CerrarCaptura=@CerrarCaptura" &
" WHERE IdEvaluacion = @IdEvaluacion"
                SDSevaluaciones.UpdateParameters.Add("ClaveBateria", HttpUtility.HtmlEncode(TBclavebateria.Text.Trim()))
                SDSevaluaciones.UpdateParameters.Add("IdCalificacion", gvCalificaciones.SelectedDataKey("IdCalificacion"))
                SDSevaluaciones.UpdateParameters.Add("Porcentaje", TBporcentaje.Text)
                SDSevaluaciones.UpdateParameters.Add("InicioContestar", tbFechaInicio.Text.Trim())
                SDSevaluaciones.UpdateParameters.Add("FinSinPenalizacion", tbFechaPenalizacion.Text.Trim() & " 23:59:00")
                SDSevaluaciones.UpdateParameters.Add("FinContestar", tbFechaFin.Text.Trim() + " 23:59:00")
                SDSevaluaciones.UpdateParameters.Add("Tipo", DDLtipo.SelectedValue)
                SDSevaluaciones.UpdateParameters.Add("Penalizacion", TBpenalizacion.Text)
                SDSevaluaciones.UpdateParameters.Add("Aleatoria", ddlAleatoria.SelectedValue)
                SDSevaluaciones.UpdateParameters.Add("SeEntregaDocto", CBseentregadocto.Checked.ToString)
                SDSevaluaciones.UpdateParameters.Add("Estatus", DDLestatus.SelectedValue)
                SDSevaluaciones.UpdateParameters.Add("Califica", DDLcalifica.SelectedValue)
                SDSevaluaciones.UpdateParameters.Add("SegundaVuelta", chkSegundavuelta.Checked)
                SDSevaluaciones.UpdateParameters.Add("Instruccion", HttpUtility.HtmlDecode(ckEditor.Text))
                SDSevaluaciones.UpdateParameters.Add("Modifico", User.Identity.Name)
                SDSevaluaciones.UpdateParameters.Add("RepiteAutomatica", chkRepeticion.Checked)
                SDSevaluaciones.UpdateParameters.Add("NoActividad", Trim(TBNoActividad.Text))
                SDSevaluaciones.UpdateParameters.Add("EsForo", ChkEsForo.Checked)
                SDSevaluaciones.UpdateParameters.Add("OpcionesAleatorias", ddlOpcionAleatoria.SelectedValue)
                SDSevaluaciones.UpdateParameters.Add("CerrarCaptura", CBCerrarCaptura.Checked)

                If chkRepeticion.Checked Then
                    SDSevaluaciones.UpdateParameters.Add("RepiteIndicador", ddlIndicadorRepeticion.SelectedValue)
                    SDSevaluaciones.UpdateParameters.Add("RepiteRango", ddlResultadoRepeticion.SelectedValue)
                    SDSevaluaciones.UpdateParameters.Add("RepiteMaximo", Integer.Parse(tbRepeticiones.Text.Trim()))
                Else
                    SDSevaluaciones.UpdateParameters.Add("RepiteIndicador", Nothing)
                    SDSevaluaciones.UpdateParameters.Add("RepiteRango", Nothing)
                    SDSevaluaciones.UpdateParameters.Add("RepiteMaximo", Nothing)
                End If
                SDSevaluaciones.UpdateParameters.Add("IdEvaluacion", GVevaluaciones.SelectedDataKey("IdEvaluacion").ToString())

                SDSevaluaciones.Update()

                GVevaluaciones.DataBind()
                CType(New MessageSuccess(GVevaluaciones, Me.GetType, "Se modificó la evaluación."), MessageSuccess).show()

            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                CType(New MessageError(GVevaluaciones, Me.GetType, "Verifica los campos."), MessageError).show()
            End Try
        End If
    End Sub

    Protected Sub btnActualizarFP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarFP.Click
        Try
            'ESTE CODIGO NO LO USE PORQUE AL SELECCIONAR NO EXISTIRIA EN EL DDL EL ID YA QUE SE VAN QUITANDO
            SDSevalPlantel.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionPlantel SET Estatus = '" + DDLEstatusActividadFP.SelectedValue + "', InicioContestar = '" + TBInicioPlanteles.Text + "', FinSinPenalizacion='" + TBPenalizacionPlanteles.Text + " 23:59:00', FinContestar='" + TBFinPlanteles.Text + " 23:59:00', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdEvaluacion = " + GVevalplantel.SelectedDataKey("IdEvaluacion").ToString + _
                                           " and IdPlantel = " + GVevalplantel.SelectedDataKey("IdPlantel").ToString
            SDSevalPlantel.Update()
            RefreshEvalPlantel()
            GVevalplantel.DataBind()
            CType(New MessageSuccess(GVevalplantel, Me.GetType, "Se ha actualizado la asignación"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVevalplantel, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub btnActualizarFechasAlumnos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarFechasAlumnos.Click
        Try
            SDSevalAlumno.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionAlumno SET InicioContestar='" +
               TBFechaInicioFechasAlumnos.Text + "', FinSinPenalizacion='" + TBFechaPenalizacionFechasAlumnos.Text +
               " 23:59:00', FinContestar='" + TBFechaFinFechasAlumnos.Text + " 23:59:00', Estatus='" +
               ddlEstatusFechasAlumnos.SelectedValue + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdEvaluacion = " +
               GVevalalumno.SelectedDataKey("IdEvaluacion").ToString + " and IdAlumno = " + GVevalalumno.SelectedDataKey("IdAlumno").ToString
            SDSevalAlumno.Update()
            RefreshEvalAlumno()
            GVevalalumno.DataBind()
            CType(New MessageSuccess(GVevalplantel, Me.GetType, "Se ha actualizado la asignación"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVevalplantel, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
    End Sub
#End Region

#Region "Utils"
    Protected Sub btnCrearCalificaciones_Click(
                     sender As Object,
                     e As EventArgs) Handles btnCrearCalificaciones.Click
        Siget.Utils.GeneralUtils.CleanAll(CalificacionesModal.Controls)
    End Sub

    Protected Function verificaCamposCalificacion() As Boolean
        If Trim(tbCalValor.Text) = "" Then
            tbCalValor.Text = 0
        End If

        If tbCaldescripcion.Text.Trim() = String.Empty Then
            msgErrorDialog.show("Error", "La calificación debe tener una descripción.")
            Return False
        End If

        If tbCalClave.Text.Trim() = String.Empty Then
            msgErrorDialog.show("Error", "La calificación debe tener una clave para reportes.")
            Return False
        End If

        Return True
    End Function

    Protected Function verificaCamposEvaluacion() As Boolean
        If TBporcentaje.Text.Trim() = "" Then
            TBporcentaje.Text = 0
        End If

        If TBclavebateria.Text.Trim() = String.Empty Then
            msgError3.show("La actividad debe tener un nombre.")

            Return False
        End If


        If TBpenalizacion.Text.Trim() = "" Then
            TBpenalizacion.Text = 0
        End If

        If chkRepeticion.Checked Then
            If Not Integer.TryParse(tbRepeticiones.Text.Trim(), Nothing) Or Integer.Parse(tbRepeticiones.Text.Trim()) > 100 Then
                msgError3.show("La actividad debe tener un número máximo de repeticiones menor a 100.")

                Return False
            End If
            If ddlIndicadorRepeticion.SelectedValue = "0" Then
                msgError3.show("La repetición automática debe estar anclada a un indicador.")

                Return False
            End If
        End If

        If Not Date.TryParse(tbFechaInicio.Text.Trim(), Nothing) Then
            msgError3.show("La fecha de inicio es inválida.")

            Return False
        End If
        If Not Date.TryParse(tbFechaPenalizacion.Text.Trim(), Nothing) Then
            msgError3.show("La fecha de penalización es inválida.")

            Return False
        End If
        If Not Date.TryParse(tbFechaFin.Text.Trim(), Nothing) Then
            msgError3.show("La fecha de fin es inválida.")

            Return False
        End If

        Return True
    End Function

    Protected Function verificaMinimaEdit() As Boolean
        If TBminimaEdit.Text Is Nothing Then
            CType(New MessageError(GVseriaciones, Me.GetType, "Si se señala una calificación mínima, ésta no debe estar vacía."), MessageError).show()
            Return False
        ElseIf Not Decimal.TryParse(TBminimaEdit.Text, New Decimal()) Then
            CType(New MessageError(GVseriaciones, Me.GetType, "La calificación mínima no es válida."), MessageError).show()
            Return False
        ElseIf Decimal.Parse(TBminimaEdit.Text) < 0 Or Decimal.Parse(TBminimaEdit.Text) > 100 Then
            CType(New MessageError(GVseriaciones, Me.GetType, "La calificación mínima debe estar entre 0 y 100."), MessageError).show()
            Return False
        End If
        Return True
    End Function

    ''' <summary>
    ''' obtiene los datos de la evaluación seleccionada y 
    ''' llena los campos del formato de creación/modificación con ellos
    ''' </summary>
    ''' <remarks></remarks>

    Protected Sub llenaDatosEvaluacion()

        TBclavebateria.Text = HttpUtility.HtmlDecode(GVevaluaciones.SelectedDataKey("ClaveBateria"))
        TBporcentaje.Text = CInt(GVevaluaciones.SelectedDataKey("Porcentaje"))
        TBpenalizacion.Text = GVevaluaciones.SelectedDataKey("Penalizacion").ToString
        tbFechaInicio.Text = CDate(GVevaluaciones.SelectedDataKey("InicioContestar").ToString).ToShortDateString
        tbFechaPenalizacion.Text = CDate(GVevaluaciones.SelectedDataKey("FinSinPenalizacion").ToString).ToShortDateString
        tbFechaFin.Text = CDate(GVevaluaciones.SelectedDataKey("FinContestar").ToString).ToShortDateString
        DDLtipo.SelectedValue = HttpUtility.HtmlDecode(GVevaluaciones.SelectedDataKey("Tipo"))
        DDLestatus.SelectedValue = GVevaluaciones.SelectedDataKey("Estatus")
        TBNoActividad.Text = IIf(GVevaluaciones.SelectedDataKey("NoActividad").ToString() <> "&nbsp;", GVevaluaciones.SelectedDataKey("NoActividad").ToString(), String.Empty)
        ddlAleatoria.SelectedValue = GVevaluaciones.SelectedDataKey("Aleatoria")
        ddlOpcionAleatoria.SelectedValue = GVevaluaciones.SelectedDataKey("OpcionesAleatorias").ToString
        If Not IsDBNull(GVevaluaciones.SelectedDataKey("Instruccion")) Then
            ckEditor.Text = HttpUtility.HtmlDecode(GVevaluaciones.SelectedDataKey("Instruccion"))
        End If

        'En la siguiente primero rellené las evaluaciones a False cuando actualicé el cambio a la tabla, por eso no necesité la comparación anterior
        CBseentregadocto.Checked = Boolean.Parse(GVevaluaciones.SelectedDataKey("SeEntregaDocto").ToString)
        ChkEsForo.Checked = Boolean.Parse(GVevaluaciones.SelectedDataKey("EsForo").ToString())
        CBCerrarCaptura.Checked = Boolean.Parse(GVevaluaciones.SelectedDataKey("CerrarCaptura").ToString())

        If GVevaluaciones.SelectedDataKey("Califica").ToString = "P" Or GVevaluaciones.SelectedDataKey("Califica").ToString = "C" Then
            DDLcalifica.SelectedValue = GVevaluaciones.SelectedDataKey("Califica").ToString
        Else
            DDLcalifica.SelectedIndex = 0
        End If
        chkRepeticion.Checked = TryCast(GVevaluaciones.SelectedRow.Cells(14).Controls(0), CheckBox).Checked

        If chkRepeticion.Checked Then
            panelRepeticion.Attributes("style") = "display:block;"
            ddlIndicadorRepeticion.DataBind()
            ddlIndicadorRepeticion.SelectedValue = GVevaluaciones.SelectedDataKey("RepiteIndicador").ToString()
            ddlResultadoRepeticion.DataBind()
            ddlResultadoRepeticion.SelectedValue = GVevaluaciones.SelectedDataKey("RepiteRango").ToString()
            tbRepeticiones.Text = GVevaluaciones.SelectedDataKey("RepiteMaximo").ToString().Trim()
        Else
            panelRepeticion.Attributes("style") = "display:none;"
        End If

        chkSegundavuelta.Checked = TryCast(GVevaluaciones.SelectedRow.Cells(15).Controls(0), CheckBox).Checked
    End Sub


    Protected Function verificaMinima() As Boolean
        If TBminima.Text Is Nothing Then
            CType(New MessageError(GVseriaciones, Me.GetType, "Si se señala una calificación mínima, ésta no debe estar vacía."), MessageError).show()
            Return False
        ElseIf Not Decimal.TryParse(TBminima.Text, New Decimal()) Then
            CType(New MessageError(GVseriaciones, Me.GetType, "La calificación mínima no es válida."), MessageError).show()
            Return False
        ElseIf Decimal.Parse(TBminima.Text) < 0 Or Decimal.Parse(TBminima.Text) > 100 Then
            CType(New MessageError(GVseriaciones, Me.GetType, "La calificación mínima debe estar entre 0 y 100."), MessageError).show()
            Return False
        End If
        Return True
    End Function

#End Region

#Region "DELETE"


    Protected Sub btnEliminarCalificaciones_Click(
                 sender As Object,
                 e As EventArgs) Handles btnEliminarCalificaciones.Click

        eliminaCalificacion()

    End Sub


    Protected Sub btnEliminarEvaluacion_Click(
                    sender As Object,
                    e As EventArgs) Handles btnEliminarEvaluacion.Click
        EliminaEvaluacion()
    End Sub



    ''' <summary>
    ''' Intenta eliminar la calificación seleccionada
    ''' </summary>
    Protected Sub eliminaCalificacion()
        Try
            SDScalificaciones.DeleteParameters.Clear() ' para múltiples intentos esto es importante
            SDScalificaciones.DeleteCommand = "Delete from Calificacion WHERE IdCalificacion = @IdCalificacion"
            SDScalificaciones.DeleteParameters.Add("IdCalificacion", gvCalificaciones.SelectedDataKey("IdCalificacion").ToString())

            SDScalificaciones.Delete()
            gvCalificaciones.DataBind()
            CType(New MessageSuccess(gvCargaImagenes, Me.GetType, "Se eliminó la calificación."), MessageSuccess).show()
        Catch sqlex As SqlException When sqlex.Number = 547
            Utils.LogManager.ExceptionLog_InsertEntry(sqlex)
            CType(New MessageError(gvCargaImagenes, Me.GetType, "No se puede borrar la calificación puesto que tiene evaluaciones asignadas."), MessageError).show()
        Catch ex As SqlException
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(gvCargaImagenes, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub btnDeleteFileQuestion_Click(
                 sender As Object,
                 e As EventArgs) Handles btnDeleteFileQuestion.Click

        Try
            SDSApoyosPlanteamientos.DeleteCommand = "DELETE FROM ApoyoPlanteamiento WHERE IdApoyoP=" + gvCargaImagenes.SelectedDataKey("IdApoyoP").ToString
            SDSApoyosPlanteamientos.Delete()
            If (File.Exists(Siget.Config.Global.rutaApoyoPlanteamientos + gvCargaImagenes.SelectedDataKey("NombreApoyo"))) Then
                File.Delete(Siget.Config.Global.rutaApoyoPlanteamientos + gvCargaImagenes.SelectedDataKey("NombreApoyo"))
            End If
            CType(New MessageSuccess(gvCargaImagenes, Me.GetType, "Se ha eliminado el archivo de apoyo correctamente"), MessageSuccess).show()
            gvCargaImagenes.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(gvCargaImagenes, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try

    End Sub

    Protected Sub EliminaEvaluacion()
        Try
            SDSevaluaciones.DeleteParameters.Clear()
            SDSevaluaciones.DeleteCommand = "Delete from Evaluacion WHERE IdEvaluacion = @IdEvaluacion"
            SDSevaluaciones.DeleteParameters.Add("IdEvaluacion", GVevaluaciones.SelectedDataKey("IdEvaluacion").ToString())
            SDSevaluaciones.Delete()
            GVevaluaciones.DataBind()
            CType(New MessageSuccess(gvCalificaciones, Me.GetType,
                          "Se eliminó la evaluación"), MessageSuccess).show()
        Catch sqlex As SqlException When sqlex.Number = 547
            Dim s As String = "No se puede borrar la evaluación por que "
            Utils.LogManager.ExceptionLog_InsertEntry(sqlex) ' registra el error
            If sqlex.Message.Contains("EvaluacionPlantel") Then
                CType(New MessageError(gvCalificaciones, Me.GetType, s &
                              "tiene fechas asignadas a planteles."), MessageError).show()
            ElseIf sqlex.Message.Contains("ApoyoEvaluacion") Then
                CType(New MessageError(gvCalificaciones, Me.GetType, s &
                              "tiene apoyos asignados."), MessageError).show()

            ElseIf sqlex.Message.Contains("DetalleEvaluacion") Then
                CType(New MessageError(gvCalificaciones, Me.GetType, s &
                              "tiene subtemas asignados."), MessageError).show()

            ElseIf sqlex.Message.Contains("EvaluacionRepetida") Then
                CType(New MessageError(gvCalificaciones, Me.GetType, s &
                              "hay alumnos que la han terminado y están repitiendo."), MessageError).show()
            ElseIf sqlex.Message.Contains("EvaluacionTerminada") Then
                CType(New MessageError(gvCalificaciones, Me.GetType,
                            s &
                              "hay alumnos que la han terminado."), MessageError).show()
            ElseIf sqlex.Message.Contains("DoctoEvaluacion") Then
                CType(New MessageError(gvCalificaciones, Me.GetType,
                          s &
                              "hay documentos entregados."), MessageError).show()
            ElseIf sqlex.Message.Contains("EvaluacionAlumno") Then
                CType(New MessageError(gvCalificaciones, Me.GetType,
                   s &
                              "tiene fechas asignadas a alumnos."), MessageError).show()
            ElseIf sqlex.Message.Contains("EvaluacionDemo") Then
                CType(New MessageError(gvCalificaciones, Me.GetType, s &
                              "tiene evaluaciones demo."), MessageError).show()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(gvCalificaciones, Me.GetType, "Error desconocidol, contacte al administrador"), MessageError).show()
        End Try
    End Sub

    Protected Sub btnQuitarTema_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuitarTema.Click

        Try
            SDSdetalleevaluacion.DeleteCommand = "DELETE FROM DetalleEvaluacion where IdDetalleEvaluacion = " + GVdetalleevaluacion.SelectedDataKey("IdDetalleEvaluacion").ToString()
            SDSdetalleevaluacion.Delete()
            CType(New MessageSuccess(DDLsubtemas, Me.GetType, "El tema se quitó correctamente"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(DDLsubtemas, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
        DDLsubtemas.DataBind()
        GVdetalleevaluacion.DataBind()
    End Sub




    Protected Sub btnQuitarAsignacionFP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuitarAsignacionFP.Click
        Try
            SDSevalPlantel.DeleteCommand = "DELETE FROM EvaluacionPlantel where IdEvaluacion = " + GVevalplantel.SelectedDataKey("IdEvaluacion").ToString + " and IdPlantel = " + GVevalplantel.SelectedDataKey("IdPlantel").ToString
            SDSevalPlantel.Delete()
            RefreshEvalPlantel()
            GVevalplantel.DataBind()
            DDLPlantelFechasPlanteles.DataBind()
            CType(New MessageSuccess(GVevalplantel, Me.GetType, "Se ha desasignado correctamente"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVevalplantel, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
    End Sub


    Protected Sub btnEliminarFechasAlumnos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarFechasAlumnos.Click
        Try
            SDSevalAlumno.DeleteCommand = "DELETE FROM EvaluacionAlumno where IdEvaluacion = " + GVevalalumno.SelectedDataKey("IdEvaluacion").ToString + " and IdAlumno = " + GVevalalumno.SelectedDataKey("IdAlumno").ToString
            SDSevalAlumno.Delete()
            SDSevalAlumno.DataBind()
            DDLgrupoFechasAlumnos_SelectedIndexChanged(Nothing, Nothing)
            RefreshEvalAlumno()
            GVevalalumno.DataBind()
            CType(New MessageSuccess(GVevalplantel, Me.GetType, "Se ha eliminado la asignación"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVevalplantel, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub btnQuitarReactivoDemo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuitarReactivoDemo.Click
        Try
            SDSevaluaciondemo.DeleteCommand = "DELETE FROM EvaluacionDemo where IdPlanteamiento = " +
                GVseleccionados.SelectedValue.ToString
            SDSevaluaciondemo.Delete()
            CType(New MessageSuccess(GVevalplantel, Me.GetType, "El reactivo ha sido desasignado"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVevalplantel, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
        CBLreactivos.DataBind()
        GVseleccionados.DataBind()
    End Sub


    Protected Sub btnQuitarSeriacion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuitarSeriacion.Click
        msgError.hide()
        Try
            SDSevaluacionesSer.DeleteCommand = "SET dateformat dmy; DELETE FROM SeriacionEvaluaciones WHERE IdEvaluacion = " & GVevaluaciones.SelectedDataKey("IdEvaluacion") & " AND IdEvaluacionPrevia = " & GVseriaciones.SelectedDataKey.Value
            SDSevaluacionesSer.Delete()
            GVseriaciones.DataBind()
            CType(New MessageSuccess(GVevalplantel, Me.GetType, "Se ha eliminado exitosamente la seriación seleccionada"), MessageSuccess).show()
            GVevaluacionesDisponibles.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVevalplantel, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
    End Sub


#End Region

#Region "SELECTED_INDEX_CHANGED"


    Protected Sub GVevaluaciones_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevaluaciones.SelectedIndexChanged

        btnCrearEvaluacion.Visible = False
        btnEliminarEvaluacion.Visible = True
        btnModificarEvaluacion.Visible = True
        If GVevaluaciones.CountRowsSelectedByCheckbox() > 0 Then
            SDSalumnos.SelectCommand = String.Format("select IdAlumno, rtrim(ApePaterno) + ' ' + rtrim(ApeMaterno) + ' ' + rtrim(Nombre) + '(' +" +
                                                      " rtrim(Matricula) + ')' as NomAlumno from Alumno where IdGrupo = {0} and IdAlumno not in " +
                                                      "(select IdAlumno from EvaluacionAlumno where IdEvaluacion in ({1})) and Estatus != 'Baja'" +
                                                      "order by NomAlumno", DDLgrupoFechasAlumnos.SelectedValue, String.Join(", ", GVevaluaciones.getRowsSelectedByCheckbox().ToArray()))
            SDSalumnos.DataBind()
            SDSplanteles.SelectCommand = String.Format(" SELECT P.IdPlantel, P.IdLicencia, P.IdInstitucion, P.Descripcion " +
                                                        " FROM Plantel P, Escuela E WHERE P.IdInstitucion = {0} and E.IdPlantel = P.IdPlantel and E.IdNivel = {1} " +
                                                        " ORDER BY Descripcion", DDLinstitucionFechasPlanteles.SelectedValue, DDLNivelCalificaciones.SelectedValue)
            SDSplanteles.DataBind()
            GVevaluaciones.SelectedIndex = -1
            btnModificarEvaluacion.Visible = False
            btnEliminarEvaluacion.Visible = False
        Else
            SDSalumnos.SelectCommand = String.Format("select IdAlumno, rtrim(ApePaterno) + ' ' + rtrim(ApeMaterno) + ' ' + rtrim(Nombre) + '(' + " +
                                            " rtrim(Matricula) + ')' as NomAlumno from Alumno where IdGrupo = {0} and IdAlumno not in" +
                                            " (select IdAlumno from EvaluacionAlumno where IdEvaluacion in ({1}) ) and Estatus != 'Baja'" +
                                            " order by NomAlumno", DDLgrupoFechasAlumnos.SelectedValue, GVevaluaciones.SelectedDataKey("IdEvaluacion"))
            SDSalumnos.DataBind()
            SDSplanteles.SelectCommand = String.Format("SELECT P.IdPlantel, P.IdLicencia, P.IdInstitucion, P.Descripcion " +
                                            " FROM Plantel P, Escuela E WHERE P.IdInstitucion = {0} and E.IdPlantel = P.IdPlantel and E.IdNivel = {1}" +
                                            " ORDER BY Descripcion", DDLinstitucionFechasPlanteles.SelectedValue, DDLNivelCalificaciones.SelectedValue,
                                            GVevaluaciones.SelectedDataKey("IdEvaluacion"))
            SDSplanteles.DataBind()
          
            btnModificarEvaluacion.Visible = True
            btnEliminarEvaluacion.Visible = True
            EvaluacionSelected.Text = GVevaluaciones.SelectedDataKey("ClaveBateria").ToString
            Evaluacion_Selected_ActNecesarias.Text = GVevaluaciones.SelectedDataKey("ClaveBateria").ToString
            Evaluacion_Selected_SeriarArea.Text = GVevaluaciones.SelectedDataKey("ClaveBateria").ToString

            llenaDatosEvaluacion()
        End If

        RefreshEvalPlantel()
        RefreshEvalAlumno()
    End Sub

    Protected Sub GVdetalleevaluacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVdetalleevaluacion.SelectedIndexChanged
        btnQuitarTema.Visible = True
        btnAsignarNoReactivos.Visible = True

    End Sub

    Protected Sub GVevalalumno_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevalalumno.SelectedIndexChanged
        btnEliminarFechasAlumnos.Visible = True
        btnActualizarFechasAlumnos.Visible = True
        TBFechaInicioFechasAlumnos.Text = CDate(GVevalalumno.SelectedDataKey("InicioContestar")).ToShortDateString()
        TBFechaPenalizacionFechasAlumnos.Text = CDate(GVevalalumno.SelectedDataKey("FinSinPenalizacion")).ToShortDateString()
        TBFechaFinFechasAlumnos.Text = CDate(GVevalalumno.SelectedDataKey("FinContestar")).ToShortDateString()
        ddlEstatusFechasAlumnos.SelectedValue = GVevalalumno.SelectedDataKey("Estatus")
    End Sub

    Protected Sub GVseriaciones_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVseriaciones.SelectedIndexChanged
        btnQuitarSeriacion.Visible = True
        btnActualizarSeriacion.Visible = True
        CHKminimaEdit.Visible = True
        TBminimaEdit.Visible = True
        AreaMinimaActSeriadas.Visible = True
    End Sub

#End Region

#Region "DATABOUND"


    Protected Sub CBLreactivos_DataBound(sender As Object, e As EventArgs) Handles CBLreactivos.DataBound

        If CBLreactivos.Items.Count > 0 Then
            btnAgregarReactivo.Visible = True
            LbReactivosDemo.Visible = True
        Else
            btnAgregarReactivo.Visible = False
            LbReactivosDemo.Visible = False
        End If

        For Each item As ListItem In CBLreactivos.Items
            item.Text = Regex.Replace(HttpUtility.HtmlDecode(item.Text), "<[^>]*(>|$)", String.Empty)
        Next

    End Sub

    Protected Sub GVseriaciones_DataBound(sender As Object, e As EventArgs) Handles GVseriaciones.DataBound
        If GVseriaciones.Rows.Count = 0 Then
            btnQuitarSeriacion.Visible = False
            btnActualizarSeriacion.Visible = False
            AreaMinimaActSeriadas.Visible = False
            GVseriaciones.SelectedIndex = -1
        End If

    End Sub

    Protected Sub gvCalificaciones_DataBound(sender As Object, e As EventArgs) Handles gvCalificaciones.DataBound
        If gvCalificaciones.Rows.Count = 0 Then
            btnEliminarCalificaciones.Visible = False
            btnModificarCalificaciones.Visible = False
            btnCrearCalificaciones.Visible = True
            gvCalificaciones.SelectedIndex = -1
        End If

        If totalPorcentajeCalificacion > 100 Then
            msgError.show("LA SUMA DE LOS VALORES DE LAS CALIFICACIONES NO DEBE SER MAYOR A 100")
        Else
            msgError.hide()
        End If
    End Sub


    Dim totalPorcentaje As Integer = 0
    Protected Sub GVevaluaciones_DataBound(sender As Object, e As EventArgs) Handles GVevaluaciones.DataBound
        If GVevaluaciones.Rows.Count = 0 Then
            btnModificarEvaluacion.Visible = False
            btnEliminarEvaluacion.Visible = False
            GVevaluaciones.SelectedIndex =
            btnCrearEvaluacion.Visible = True

        End If
        If totalPorcentaje > 100 Then
            msgError3.show("LA SUMA DE LOS VALORES DE LAS ACTIVIDADES NO DEBE SER MAYOR A 100")
        Else
            msgError3.hide()
        End If

    End Sub

    Protected Sub GVevaluacionesDisponibles_DataBound(sender As Object, e As EventArgs) Handles GVevaluacionesDisponibles.DataBound
        If GVevaluacionesDisponibles.Rows.Count = 0 Then
            btnSeriarActividad.Visible = False
            GVevaluacionesDisponibles.SelectedIndex = -1
        End If
    End Sub

    Protected Sub GVevalalumno_DataBound(sender As Object, e As EventArgs) Handles GVevalalumno.DataBound
        If GVevalalumno.Rows.Count = 0 Then
            btnEliminarFechasAlumnos.Visible = False
            btnActualizarFechasAlumnosGeneral.Visible = False
            GVevalalumno.SelectedIndex = -1
        Else
            btnActualizarFechasAlumnosGeneral.Visible = True
        End If

    End Sub
#End Region

#Region "PRERENDER"

    Protected Sub GVevalplantel_PreRender(sender As Object, e As EventArgs) Handles GVevalplantel.PreRender
        GVevalplantel.Caption = "<h3 style=font-size:16px;font-weight:bold !important; >" + Lang_Config.Translate("calificaciones", "Header_gvEvalPlanteles") + "</h3>"
        GVevalplantel.Columns(1).HeaderText = Lang_Config.Translate("general", "Plantel")
        GVevalplantel.Columns(7).HeaderText = Lang_Config.Translate("general", "Asignatura")
    End Sub
#End Region

    Protected Sub gvCalificaciones_PageIndexChanged(sender As Object, e As EventArgs) Handles gvCalificaciones.PageIndexChanged
        gvCalificaciones.SelectedIndex = -1
        Siget.Utils.GeneralUtils.CleanAll(PanelCalificaciones.Controls)
    End Sub

    Protected Sub GVevaluaciones_PageIndexChanged(sender As Object, e As EventArgs) Handles GVevaluaciones.PageIndexChanged
        GVevaluaciones.SelectedIndex = -1
        Siget.Utils.GeneralUtils.CleanAll(PanelEvaluacionesModal.Controls)
    End Sub

    Protected Sub GVevalplantel_PageIndexChanged(sender As Object, e As EventArgs) Handles GVevalplantel.PageIndexChanged
        RefreshEvalPlantel()
        GVevalplantel.SelectedIndex = -1
        Siget.Utils.GeneralUtils.CleanAll(Panelplanteles.Controls)
        btnActualizarFP.Visible = False
        btnQuitarAsignacionFP.Visible = False
    End Sub

    Protected Sub GVevalalumno_PageIndexChanged(sender As Object, e As EventArgs) Handles GVevalalumno.PageIndexChanged
        RefreshEvalAlumno()
        GVevalalumno.SelectedIndex = -1
        Siget.Utils.GeneralUtils.CleanAll(PanelFechasAlumnos.Controls)
        btnActualizarFechasAlumnos.Visible = False
        btnEliminarFechasAlumnos.Visible = False
    End Sub

    Protected Sub GVseleccionados_PageIndexChanged(sender As Object, e As EventArgs) Handles GVseleccionados.PageIndexChanged
        GVseleccionados.SelectedIndex = -1
        Siget.Utils.GeneralUtils.CleanAll(PanelActividadPractica.Controls)
        btnQuitarReactivoDemo.Visible = False
    End Sub

    Protected Sub gvCalificaciones_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles gvCalificaciones.SelectedIndexChanging
        If e.NewSelectedIndex = CType(sender, GridView).SelectedIndex Then
            gvCalificaciones.SelectedIndex = -1
            e.Cancel = True
            btnCrearCalificaciones.Visible = True
            btnModificarCalificaciones.Visible = False
            btnEliminarCalificaciones.Visible = False
            btnGuardarCambiosCalificaciones.Visible = False
            Siget.Utils.GeneralUtils.CleanAll(CalificacionesModal.Controls)
        End If
    End Sub

    Protected Sub GVevalplantel_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles GVevalplantel.SelectedIndexChanging
        If e.NewSelectedIndex = CType(sender, GridView).SelectedIndex Then
            GVevalplantel.SelectedIndex = -1
            e.Cancel = True
            btnActualizarFP.Visible = False
            btnQuitarAsignacionFP.Visible = False

            Siget.Utils.GeneralUtils.CleanAll(Panelplanteles.Controls)
        End If
    End Sub

    Protected Sub GVevalalumno_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles GVevalalumno.SelectedIndexChanging
        If e.NewSelectedIndex = CType(sender, GridView).SelectedIndex Then
            GVevalalumno.SelectedIndex = -1
            e.Cancel = True
            btnActualizarFechasAlumnos.Visible = False
            btnEliminarFechasAlumnos.Visible = False
            Siget.Utils.GeneralUtils.CleanAll(PanelFechasAlumnos.Controls)
        End If
    End Sub

    Protected Sub GVevaluaciones_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles GVevaluaciones.SelectedIndexChanging
        If e.NewSelectedIndex = CType(sender, GridView).SelectedIndex Then
            GVevaluaciones.SelectedIndex = -1
            e.Cancel = True
            btnCrearCalificaciones.Visible = True
            btnModificarCalificaciones.Visible = False
            btnEliminarCalificaciones.Visible = False
            Siget.Utils.GeneralUtils.CleanAll(PanelEvaluacionesModal.Controls)
            btnCrearEvaluacion.Visible = True
            btnModificarEvaluacion.Visible = False
            btnEliminarEvaluacion.Visible = False
        End If
    End Sub

    Protected Sub GVseleccionados_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles GVseleccionados.SelectedIndexChanging
        If e.NewSelectedIndex = CType(sender, GridView).SelectedIndex Then
            GVseleccionados.SelectedIndex = -1
            e.Cancel = True
            Siget.Utils.GeneralUtils.CleanAll(PanelActividadPractica.Controls)
            btnQuitarReactivoDemo.Visible = False
        End If
    End Sub

    Protected Sub GVseleccionados_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVseleccionados.SelectedIndexChanged
        btnQuitarReactivoDemo.Visible = True
    End Sub

    Protected Sub btnActualizarFechasAlumnosGeneral_Click(sender As Object, e As EventArgs) Handles btnActualizarFechasAlumnosGeneral.Click

        RefreshEvalAlumno()
        Dim data As DataView = CType(SDSevalAlumno.Select(DataSourceSelectArguments.Empty), DataView)

        For Each row As DataRow In data.Table.Rows
            Try
                SDSevalAlumno.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionAlumno SET InicioContestar='" +
                   TBFechaInicioFechasAlumnos.Text + "', FinSinPenalizacion='" + TBFechaPenalizacionFechasAlumnos.Text +
                   " 23:59:00', FinContestar='" + TBFechaFinFechasAlumnos.Text + " 23:59:00', Estatus='" +
                   ddlEstatusFechasAlumnos.SelectedValue + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdEvaluacion = " +
                   row(3).ToString() + " and IdAlumno = " + row(0).ToString()
                SDSevalAlumno.Update()

                CType(New MessageSuccess(GVevalplantel, Me.GetType, "Se han actualizado las asignaciones de todos"), MessageSuccess).show()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                CType(New MessageError(GVevalplantel, Me.GetType, "Verifique los campos"), MessageError).show()
            End Try
        Next

        GVevalalumno.DataBind()

    End Sub

    Protected Sub btnActualizarFechasPlantelesGeneral_Click(sender As Object, e As EventArgs) Handles btnActualizarFechasPlantelesGeneral.Click

        RefreshEvalPlantel()
        Dim data As DataView = CType(SDSevalPlantel.Select(DataSourceSelectArguments.Empty), DataView)

        For Each row As DataRow In data.Table.Rows
            Try
                'ESTE CODIGO NO LO USE PORQUE AL SELECCIONAR NO EXISTIRIA EN EL DDL EL ID YA QUE SE VAN QUITANDO
                SDSevalPlantel.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionPlantel SET Estatus = '" + DDLEstatusActividadFP.SelectedValue + "', InicioContestar = '" +
                    TBInicioPlanteles.Text + "', FinSinPenalizacion='" + TBPenalizacionPlanteles.Text + " 23:59:00', FinContestar='" +
                    TBFinPlanteles.Text + " 23:59:00', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdEvaluacion = " +
                     row(2).ToString() + _
                   " and IdPlantel = " + row(0).ToString()

                SDSevalPlantel.Update()

                CType(New MessageSuccess(GVevalplantel, Me.GetType, "Se han actualizado la asignaciones de todos"), MessageSuccess).show()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                CType(New MessageError(GVevalplantel, Me.GetType, "Verifique los campos"), MessageError).show()
            End Try
        Next

        GVevalplantel.DataBind()

    End Sub

    Protected Sub GVevalplantel_DataBound(sender As Object, e As EventArgs) Handles GVevalplantel.DataBound

   


        If GVevalplantel.Rows.Count = 0 Then
            GVevalplantel.SelectedIndex = -1
            btnActualizarFechasPlantelesGeneral.Visible = False
        Else
            btnActualizarFechasPlantelesGeneral.Visible = True
        End If
    End Sub

    Protected Sub btnActualizarTodosEstatus_Click(sender As Object, e As EventArgs) Handles btnActualizarTodosEstatus.Click

        Dim dv As DataView = CType(SDSevaluaciones.Select(DataSourceSelectArguments.Empty), DataView)
        Dim evaluacionesRepository As IRepository(Of Siget.Entity.Evaluacion) = New EvaluacionRepository()
        Dim evaluacion As Siget.Entity.Evaluacion

        Try
            For Each row As DataRow In dv.Table.Rows
                evaluacion = evaluacionesRepository.FindById(row("IdEvaluacion"))
                evaluacion.Estatus = DDLEstatusTodos.SelectedValue
                evaluacionesRepository.Update(evaluacion)
            Next
            GVevaluaciones.DataBind()
            CType(New MessageSuccess(btnActualizarTodosEstatus, Me.GetType, "Se han actualizado los estatus de todas las actividades de la calificación seleccionada"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(btnActualizarTodosEstatus, Me.GetType, "No se pudieron actualizar todos los estatus"), MessageError).show()
        End Try
       

    End Sub

    Protected Sub DDLPlantelFechasPlanteles_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLPlantelFechasPlanteles.SelectedIndexChanged

        RefreshEvalPlantel()

    End Sub

    Protected Sub RefreshEvalPlantel()

        Try
            If GVevaluaciones.CountRowsSelectedByCheckbox() > 0 Then


                SDSevalPlantel.SelectCommand = String.Format("select EP.IdPlantel, P.Descripcion as Plantel, EP.IdEvaluacion, E.ClaveBateria, " +
                                                    "EP.InicioContestar, EP.FinContestar, EP.FinSinPenalizacion, A.Descripcion as Asignatura, EP.Estatus " +
                                                    "from Plantel P, Evaluacion E, EvaluacionPlantel EP, Calificacion C, Asignatura A " +
                                                    "where C.IdCicloEscolar = {0} and E.IdCalificacion = C.IdCalificacion " +
                                                    "and A.IdAsignatura = C.IdAsignatura and " +
                                                     "P.IdPlantel = EP.IdPlantel And e.IdEvaluacion = EP.IdEvaluacion and EP.IdPlantel={1}" +
                                                    " and  P.IdInstitucion = {2} and EP.IdEvaluacion in ({3}) " +
                                                    " order by P.Descripcion, C.IdAsignatura, E.InicioContestar ", DDLcicloescolarCalificaciones.SelectedValue, DDLPlantelFechasPlanteles.SelectedValue,
                                                    DDLinstitucionFechasPlanteles.SelectedValue, String.Join(", ", GVevaluaciones.getRowsSelectedByCheckbox().ToArray()))
                SDSevalPlantel.DataBind()





            Else
                SDSevalPlantel.SelectCommand = String.Format("select EP.IdPlantel, P.Descripcion as Plantel, EP.IdEvaluacion, E.ClaveBateria, " +
                                                        "EP.InicioContestar, EP.FinContestar, EP.FinSinPenalizacion, A.Descripcion as Asignatura, EP.Estatus " +
                                                        "from Plantel P, Evaluacion E, EvaluacionPlantel EP, Calificacion C, Asignatura A " +
                                                        "where C.IdCicloEscolar = {0} and E.IdCalificacion = C.IdCalificacion " +
                                                        "and A.IdAsignatura = C.IdAsignatura and " +
                                                         "P.IdPlantel = EP.IdPlantel And e.IdEvaluacion = EP.IdEvaluacion and EP.IdPlantel={1}" +
                                                        " and  P.IdInstitucion = {2} and EP.IdEvaluacion in ({3}) " +
                                                        " order by P.Descripcion, C.IdAsignatura, E.InicioContestar ", DDLcicloescolarCalificaciones.SelectedValue, DDLPlantelFechasPlanteles.SelectedValue,
                                                        DDLinstitucionFechasPlanteles.SelectedValue, GVevaluaciones.SelectedDataKey("IdEvaluacion"))
                SDSevalPlantel.DataBind()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(btnActualizarTodosEstatus, Me.GetType, "Ha ocurrido un error"), MessageError).show()
        End Try

    End Sub

    Protected Sub RefreshEvalAlumno()

        Try
            If GVevaluaciones.CountRowsSelectedByCheckbox() > 0 Then


                SDSevalAlumno.SelectCommand = String.Format("select EA.IdAlumno, A.ApePaterno + ' ' + A.ApeMaterno + ' ' + A.Nombre as 'Nombre del Alumno', A.Matricula, EA.IdEvaluacion, E.ClaveBateria," +
                                                       "EA.InicioContestar, EA.FinContestar, EA.FinSinPenalizacion, Asig.Descripcion as Asignatura, EA.Estatus " +
                                                       "from Alumno A, Evaluacion E, EvaluacionAlumno EA, Calificacion C, Asignatura Asig " +
                                                       "where C.IdCicloEscolar = {0} and E.IdCalificacion = C.IdCalificacion " +
                                                       "and Asig.IdAsignatura = C.IdAsignatura and A.IdAlumno = EA.IdAlumno and " +
                                                       "E.IdEvaluacion = EA.IdEvaluacion and  A.IdGrupo = {1} and A.Estatus != 'Baja' and EA.IdEvaluacion in ({2}) " +
                                                       "order by A.ApePaterno,A.ApeMaterno,A.Nombre, C.IdAsignatura, E.InicioContestar ",
                                                       DDLcicloescolarCalificaciones.SelectedValue, DDLgrupoFechasAlumnos.SelectedValue, String.Join(", ", GVevaluaciones.getRowsSelectedByCheckbox().ToArray()))
                SDSevalAlumno.DataBind()


            Else


                SDSevalAlumno.SelectCommand = String.Format("select EA.IdAlumno, A.ApePaterno + ' ' + A.ApeMaterno + ' ' + A.Nombre as 'Nombre del Alumno', A.Matricula, EA.IdEvaluacion, E.ClaveBateria," +
                                               "EA.InicioContestar, EA.FinContestar, EA.FinSinPenalizacion, Asig.Descripcion as Asignatura, EA.Estatus " +
                                               "from Alumno A, Evaluacion E, EvaluacionAlumno EA, Calificacion C, Asignatura Asig " +
                                               "where C.IdCicloEscolar = {0} and E.IdCalificacion = C.IdCalificacion " +
                                               "and Asig.IdAsignatura = C.IdAsignatura and A.IdAlumno = EA.IdAlumno and " +
                                               "E.IdEvaluacion = EA.IdEvaluacion and  A.IdGrupo = {1} and A.Estatus != 'Baja' and EA.IdEvaluacion in ({2}) " +
                                               "order by A.ApePaterno,A.ApeMaterno,A.Nombre, C.IdAsignatura, E.InicioContestar ",
                                               DDLcicloescolarCalificaciones.SelectedValue, DDLgrupoFechasAlumnos.SelectedValue, GVevaluaciones.SelectedDataKey("IdEvaluacion"))
                SDSevalAlumno.DataBind()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(btnActualizarTodosEstatus, Me.GetType, "Ha ocurrido un error"), MessageError).show()
        End Try

       
    End Sub

   
End Class
