﻿
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.IO
Imports Siget
Imports Siget.Entity
Partial Class administrador_AgruparReactivos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()
    End Sub

    Protected Sub GVCoordinadores_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVCoordinadores.SelectedIndexChanged
        Eliminar.Visible = True
        Actualizar.Visible = True
        TBNombre.Text = HttpUtility.HtmlDecode(CStr(GVCoordinadores.SelectedDataKey("Nombre")))
        TBApellidos.Text = HttpUtility.HtmlDecode(CStr(GVCoordinadores.SelectedDataKey("Apellidos")))
        TBUsername.Enabled = False
        DDLEstatus.SelectedValue = HttpUtility.HtmlDecode(GVCoordinadores.SelectedDataKey("Estatus").ToString)
        TBCorreoElectronico.Text = HttpUtility.HtmlDecode(GVCoordinadores.SelectedDataKey("Email").ToString)
        TBUsername.Text = GVCoordinadores.SelectedDataKey("Login")
        TBPassword.Text = GVCoordinadores.SelectedDataKey("Password")
        coordinadorSelected.Text = GVCoordinadores.SelectedDataKey("Nombre").ToString.ToUpper + " " + GVCoordinadores.SelectedDataKey("Apellidos").ToString.ToUpper
    End Sub

    Protected Sub GVCoordinadores_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVCoordinadores.RowCreated


        Dim cociente As Integer = (GVCoordinadores.PageCount / 15)
        Dim moduloIndex As Integer = (GVCoordinadores.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVCoordinadores.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVCoordinadores.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVCoordinadores.PageIndex
                final = IIf((inicial + 10 <= GVCoordinadores.PageCount), inicial + 15, GVCoordinadores.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVCoordinadores.PageCount, inicial + 15, GVCoordinadores.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVCoordinadores.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVDatosPlanteles_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVDatosPlanteles.RowCreated
        Dim cociente As Integer = (GVDatosPlanteles.PageCount / 15)
        Dim moduloIndex As Integer = (GVDatosPlanteles.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVDatosPlanteles.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVDatosPlanteles.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVDatosPlanteles.PageIndex
                final = IIf((inicial + 10 <= GVDatosPlanteles.PageCount), inicial + 15, GVDatosPlanteles.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVDatosPlanteles.PageCount, inicial + 15, GVDatosPlanteles.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVDatosPlanteles.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVPlanteles_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVPlanteles.RowCreated


        Dim cociente As Integer = (GVPlanteles.PageCount / 15)
        Dim moduloIndex As Integer = (GVPlanteles.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVPlanteles.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVPlanteles.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVPlanteles.PageIndex
                final = IIf((inicial + 10 <= GVPlanteles.PageCount), inicial + 15, GVPlanteles.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVPlanteles.PageCount, inicial + 15, GVPlanteles.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVPlanteles.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVCoordinadores_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVCoordinadores.RowDataBound


        Dim cociente As Integer = (GVCoordinadores.PageCount) / 15
        Dim moduloIndex As Integer = (GVCoordinadores.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVCoordinadores.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVCoordinadores.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVCoordinadores.PageIndex
            final = IIf((inicial + 15 <= GVCoordinadores.PageCount), inicial + 15, GVCoordinadores.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVCoordinadores.PageCount), inicial + 15, GVCoordinadores.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVCoordinadores, "Select$" & e.Row.RowIndex)
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVCoordinadores, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub Asignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Asignar.Click

        Try
            SDSCoordPlantel.InsertCommand = "SET dateformat dmy; INSERT INTO CoordinadorPlantel (IdCoordinador,IdPlantel,FechaModif, Modifico) VALUES (" + _
                GVCoordinadores.SelectedValue.ToString + "," + GVPlanteles.SelectedValue.ToString + ",getdate(), '" & User.Identity.Name & "')"
            SDSCoordPlantel.Insert()
            GVDatosPlanteles.DataBind()
            GVPlanteles.DataBind()
            CType(New MessageSuccess(GVDatosPlanteles, Me.GetType, "El Plantel ha sido asignado al Coordinador."), MessageSuccess).show()

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVDatosPlanteles, Me.GetType, "Verifique los campos."), MessageError).show()
        End Try
    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
    
        Dim repositoryCoordinador As IRepository(Of Coordinador) = New CoordinadorRepository()
        Dim coordinador As Coordinador = New Coordinador()
        Dim usuario As Usuario = New Usuario()
        Dim status As MembershipCreateStatus
        Dim newUser As MembershipUser
        Try
            With usuario
                .Login = TBUsername.Text
                .Password = TBPassword.Text
                .PerfilASP = "Coordinador"
                .Email = IIf(TBCorreoElectronico.Text <> String.Empty, TBCorreoElectronico.Text, String.Empty)
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
            End With
            With coordinador
                .Usuario = usuario
                .Nombre = TBNombre.Text
                .Apellidos = TBApellidos.Text
                .FechaIngreso = DateTime.Now
                .Clave = ""
                .Email = IIf(TBCorreoElectronico.Text <> String.Empty, TBCorreoElectronico.Text, String.Empty)
                .Estatus = DDLEstatus.SelectedValue
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name

            End With

            newUser = Membership.CreateUser(Trim(TBUsername.Text), Trim(TBPassword.Text), _
                                                              Trim(TBCorreoElectronico.Text), Nothing, _
                                                              Nothing, True, status)
            If newUser Is Nothing Then
                CType(New MessageError(TBCorreoElectronico, Me.GetType, GetErrorMessage(status)), MessageError).show()
            Else
                Roles.AddUserToRole(Trim(TBUsername.Text), "Coordinador")
                repositoryCoordinador.Add(coordinador)
                GVCoordinadores.DataBind()
                CType(New MessageSuccess(GVDatosPlanteles, Me.GetType, "El coordinador se ha creado exitosamente."), MessageSuccess).show()
            End If

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVDatosPlanteles, Me.GetType, "Verifique los campos."), MessageError).show()
        End Try
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click

        Dim repositoryCoordinador As IRepository(Of Coordinador) = New CoordinadorRepository()
        Dim coordinador As Coordinador = repositoryCoordinador.FindById(GVCoordinadores.SelectedDataKey("IdCoordinador"))
        Dim userUpdate As MembershipUser

        msgError.hide()
        msgSuccess.hide()
        Try

            With coordinador.Usuario
                .Login = TBUsername.Text
                .Password = TBPassword.Text
                .Email = IIf(TBCorreoElectronico.Text.Trim() <> String.Empty, TBCorreoElectronico.Text, String.Empty)
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
            End With
            With coordinador
                .Nombre = TBNombre.Text
                .Apellidos = TBApellidos.Text()
                .FechaIngreso = DateTime.Now
                .Email = IIf(TBCorreoElectronico.Text <> String.Empty, TBCorreoElectronico.Text, String.Empty)
                .Estatus = DDLEstatus.SelectedValue
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name

            End With

            userUpdate = Membership.GetUser(Trim(HttpUtility.HtmlDecode(GVCoordinadores.SelectedDataKey("Login").ToString)))
            userUpdate.ChangePassword(userUpdate.ResetPassword(), Trim(TBPassword.Text))

            repositoryCoordinador.Update(coordinador)
            GVCoordinadores.DataBind()
            CType(New MessageSuccess(GVDatosPlanteles, Me.GetType, "El coordinador se ha actualizado exitosamente."), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVDatosPlanteles, Me.GetType, "Verifique los campos."), MessageError).show()

        End Try
    End Sub

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click

        Try
            msgError.hide()
            msgSuccess.hide()
            Dim repositoryCoordinador As IRepository(Of Coordinador) = New CoordinadorRepository()
            Dim repositoryUsuario As IRepository(Of Usuario) = New UsuarioRepository()
            Dim coordinador As Coordinador = repositoryCoordinador.FindById(GVCoordinadores.SelectedDataKey("IdCoordinador"))
            Dim usuario As Usuario = repositoryUsuario.FindById(coordinador.IdUsuario)
            repositoryCoordinador.Delete(coordinador)
            repositoryUsuario.Delete(usuario)
            Membership.DeleteUser(GVCoordinadores.SelectedDataKey("Login"))
            GVCoordinadores.DataBind()
            CType(New MessageSuccess(GVDatosPlanteles, Me.GetType, "El coordinador ha sido eliminado exitosamente."), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVDatosPlanteles, Me.GetType, "Verifique los campos."), MessageError).show()

        End Try


    End Sub

    Protected Sub CrearNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CrearNuevo.Click
        Siget.Utils.GeneralUtils.CleanAll(AreaAdministracionCoordinadores.Controls)
        TBUsername.Enabled = True
        GVCoordinadores.SelectedIndex = -1
        Eliminar.Visible = False
        Actualizar.Visible = False
    End Sub

    Public Function GetErrorMessage(ByVal status As MembershipCreateStatus) As String
        Select Case status
            Case MembershipCreateStatus.DuplicateUserName
                Return "El usuario ya existe, elija otro usuario."
            Case MembershipCreateStatus.DuplicateEmail
                Return "Ya existe un usuario para ese email, por favor escriba un email diferente,"
            Case MembershipCreateStatus.InvalidPassword
                Return "El password es inválido, pruebe con otro."
            Case MembershipCreateStatus.InvalidEmail
                Return "El formato del email es inválido, por favor revíselo."
            Case MembershipCreateStatus.InvalidAnswer
                Return "La respuesta secreta es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidQuestion
                Return "La pregunta de recuperación de password es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidUserName
                Return "El nombre de usuario es inválido, corríjalo."
            Case MembershipCreateStatus.ProviderError
                Return "La autentificación falló, contacte al administrador del sistema."
            Case MembershipCreateStatus.UserRejected
                Return "La creación del usuario ha sido cancelada, consulte al administrador."
            Case Else
                Return "Ha ocurrido un error desconocido, verifique o contacte al administrador."
        End Select
    End Function

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem("---Elija una " & Config.Etiqueta.INSTITUCION, 0))

        If DDLinstitucion.Items.Count = 2 Then
            DDLinstitucion.SelectedIndex = 1
            GVPlanteles.DataBind()
        End If

    End Sub

    Protected Sub Desasignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Desasignar.Click

        Try
            'Aqui uso otra sintaxis para extraer los valores de los campos del GridView:
            SDSCoordPlantel.DeleteCommand = "DELETE FROM CoordinadorPlantel WHERE IdPlantel = " + GVDatosPlanteles.SelectedDataKey(0).ToString + " and IdCoordinador = " + GVDatosPlanteles.SelectedDataKey(1).ToString
            SDSCoordPlantel.Delete()
            GVDatosPlanteles.DataBind()
            GVPlanteles.DataBind()
            CType(New MessageSuccess(GVDatosPlanteles, Me.GetType, Config.Etiqueta.ARTDET_PLANTEL & " " & Config.Etiqueta.PLANTEL &
                " ha sido desasignad" & Config.Etiqueta.LETRA_PLANTEL & " de " &
                Config.Etiqueta.ARTDET_COORDINADOR & " " & Config.Etiqueta.COORDINADOR), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVDatosPlanteles, Me.GetType, "Elija el registro con los datos a desasignar."), MessageError).show()
        End Try
    End Sub

    Protected Sub GVPlanteles_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVPlanteles.RowDataBound

        Dim cociente As Integer = (GVPlanteles.PageCount) / 15
        Dim moduloIndex As Integer = (GVPlanteles.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVPlanteles.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVPlanteles.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVPlanteles.PageIndex
            final = IIf((inicial + 15 <= GVPlanteles.PageCount), inicial + 15, GVPlanteles.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVPlanteles.PageCount), inicial + 15, GVPlanteles.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVPlanteles, "Select$" & e.Row.RowIndex)
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVPlanteles, "Page$" & counter.ToString)
            Next
        End If



    End Sub

    Protected Sub GVDatosPlanteles_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVDatosPlanteles.RowDataBound

        Dim cociente As Integer = (GVDatosPlanteles.PageCount) / 15
        Dim moduloIndex As Integer = (GVDatosPlanteles.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVDatosPlanteles.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVDatosPlanteles.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVDatosPlanteles.PageIndex
            final = IIf((inicial + 15 <= GVDatosPlanteles.PageCount), inicial + 15, GVDatosPlanteles.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVDatosPlanteles.PageCount), inicial + 15, GVDatosPlanteles.PageCount)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVDatosPlanteles, "Select$" & e.Row.RowIndex)
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVDatosPlanteles, "Page$" & counter.ToString)
            Next
        End If

    End Sub

    Protected Sub GVPlanteles_DataBound(sender As Object, e As EventArgs) Handles GVPlanteles.DataBound
        If GVPlanteles.Rows.Count = 0 Then
            Asignar.Visible = False
        End If
    End Sub

    Protected Sub GVDatosPlanteles_DataBound(sender As Object, e As EventArgs) Handles GVDatosPlanteles.DataBound
        If GVDatosPlanteles.Rows.Count = 0 Then
            Desasignar.Visible = False
            GVDatosPlanteles.SelectedIndex = -1
        End If
    End Sub

    Protected Sub GVPlanteles_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVPlanteles.SelectedIndexChanged
        Asignar.Visible = True
    End Sub

    Protected Sub GVDatosPlanteles_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDatosPlanteles.SelectedIndexChanged
        Desasignar.Visible = True

    End Sub

    Protected Sub GVCoordinadores_DataBound(sender As Object, e As EventArgs) Handles GVCoordinadores.DataBound
        If GVCoordinadores.Rows.Count = 0 Then
            Actualizar.Visible = False
            Eliminar.Visible = False
            GVCoordinadores.SelectedIndex = -1
        End If

    End Sub

    Protected Sub GVPlanteles_PreRender(sender As Object, e As EventArgs) Handles GVPlanteles.PreRender
        GVPlanteles.Columns(3).HeaderText = Lang_Config.Translate("general", "Plantel")
    End Sub

    Protected Sub GVDatosPlanteles_PreRender(sender As Object, e As EventArgs) Handles GVDatosPlanteles.PreRender
        GVDatosPlanteles.Columns(5).HeaderText = Lang_Config.Translate("general", "Plantel")
    End Sub

    Protected Sub GVCoordinadores_PageIndexChanged(sender As Object, e As EventArgs) Handles GVCoordinadores.PageIndexChanged
        GVCoordinadores.SelectedIndex = -1
        Siget.Utils.GeneralUtils.CleanAll(AreaAdministracionCoordinadores.Controls)
        Actualizar.Visible = False
        Eliminar.Visible = False
    End Sub

    Protected Sub GVPlanteles_PageIndexChanged(sender As Object, e As EventArgs) Handles GVPlanteles.PageIndexChanged
        GVPlanteles.SelectedIndex = -1
        Asignar.Visible = False
    End Sub

    Protected Sub GVDatosPlanteles_PageIndexChanged(sender As Object, e As EventArgs) Handles GVDatosPlanteles.PageIndexChanged
        GVDatosPlanteles.SelectedIndex = -1
        Desasignar.Visible = False
    End Sub


    Protected Sub GVCoordinadores_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles GVCoordinadores.SelectedIndexChanging
        If e.NewSelectedIndex = CType(sender, GridView).SelectedIndex Then
            GVCoordinadores.SelectedIndex = -1
            e.Cancel = True
            Actualizar.Visible = False
            Eliminar.Visible = False
            Siget.Utils.GeneralUtils.CleanAll(AreaAdministracionCoordinadores.Controls)
        End If
    End Sub

    Protected Sub GVPlanteles_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles GVPlanteles.SelectedIndexChanging
        If e.NewSelectedIndex = CType(sender, GridView).SelectedIndex Then
            GVPlanteles.SelectedIndex = -1
            e.Cancel = True
            Asignar.Visible = False
        End If
    End Sub

    Protected Sub GVDatosPlanteles_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles GVDatosPlanteles.SelectedIndexChanging
        If e.NewSelectedIndex = CType(sender, GridView).SelectedIndex Then
            GVDatosPlanteles.SelectedIndex = -1
            e.Cancel = True
            Desasignar.Visible = False
        End If
    End Sub
End Class
