﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master"
     AutoEventWireup="false" CodeFile="grupos.aspx.vb" EnableEventValidation="false" Inherits="administrador_Grupos" %>

<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">


    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
   <div class="widget-header bg-themeprimary">
                    <i class="widget-icon fa fa-arrow-right"></i>
                    <span class="widget-caption"><b>ÁREA DE GRUPOS </b></span>
                    <div class="widget-buttons">
                        <a href="#" data-toggle="config">
                            <i class="fa fa-cog"></i>
                        </a>
                        <a href="#" data-toggle="maximize">
                            <i class="fa fa-expand"></i>
                        </a>
                        <a href="#" data-toggle="collapse">
                            <i class="fa fa-minus"></i>
                        </a>
                        <a href="#" data-toggle="dispose">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                    <!--Widget Buttons-->
                </div>
            <!--Widget Header-->
            <div class="widget-body">
                <asp:UpdatePanel ID="PanelGrupos" runat="server">
                    <ContentTemplate>
                        <div class="form-horizontal" id="gruposForm">

                            <div class="col-lg-12" style="position: absolute; top: 50%; left: 50%; bottom: 50%;">
                                <div class="load-spinner"></div>
                            </div>
                            <div class="panel panel-default">

                                <div class="panel-body">


                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <asp:Label ID="Label2" CssClass="col-lg-2 control-label" runat="server"><%=Lang_Config.Translate("general","Institucion") %></asp:Label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                                    DataValueField="IdInstitucion" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label3" CssClass="col-lg-2 control-label" runat="server" >
                                                <%=Lang_Config.Translate("general","Plantel") %>
                                            </asp:Label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                                    DataValueField="IdPlantel" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">

                                            <asp:Label ID="Label4" CssClass="col-lg-2 control-label" runat="server">
                                                              <%=Lang_Config.Translate("general","Nivel") %>
                                            </asp:Label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSniveles" DataTextField="Descripcion"
                                                    DataValueField="IdNivel" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label5" CssClass="col-lg-2 control-label" runat="server">
                                                           <%=Lang_Config.Translate("general","Grado") %>
                                            </asp:Label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSgrados" DataTextField="Descripcion"
                                                    DataValueField="IdGrado" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label6" CssClass="col-lg-2 control-label" runat="server" Text="Ciclo" />
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLcicloescolar" runat="server"
                                                    DataSourceID="SDSciclosescolares" DataTextField="Ciclo"
                                                    DataValueField="IdCicloEscolar" CssClass="form-control" AutoPostBack="True">
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label1" runat="server" CssClass="col-lg-2 control-label" Text="Descripción" />
                                            <div class="col-lg-10">
                                                <asp:TextBox ID="TBdescripcion" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label7" runat="server" CssClass="col-lg-2 control-label" Text="Clave grupo" />
                                            <div class="col-lg-10">
                                                <asp:TextBox ID="TBClave" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="table-responsive">
                                            <asp:GridView ID="GVgruposcreados" runat="server"
                                                AllowPaging="True"
                                                AutoGenerateColumns="False"
                                                DataSourceID="SDSgrupos" 
                                                AllowSorting="True"
                                                DataKeyNames="IdGrupo,IdCicloEscolar"
                                                PageSize="5"
                                                CellPadding="3"
                                                CssClass="table table-striped table-bordered"
                                                Height="17px"
                                                Style="font-size: x-small; text-align: left;">
                                                <Columns>


                                                    <asp:BoundField DataField="IdGrupo" HeaderText="Id_Grupo" Visible="false"
                                                        SortExpression="IdGrupo" InsertVisible="False" ReadOnly="True" />
                                                    <asp:BoundField DataField="IdGrado" HeaderText="IdGrado"
                                                        SortExpression="IdGrado" Visible="False" />
                                                    <asp:BoundField DataField="Grupo" HeaderText="Grupo"
                                                        SortExpression="Grupo" />
                                                    <asp:BoundField DataField="Clave" HeaderText="Clave" SortExpression="Clave" />
                                                    <asp:BoundField DataField="Grado" HeaderText="Área" SortExpression="Grado" />
                                                    <asp:BoundField DataField="IdCicloEscolar" HeaderText="IdCicloEscolar"
                                                        InsertVisible="False" SortExpression="IdCicloEscolar" Visible="False" />
                                                    <asp:BoundField DataField="Ciclo Escolar" HeaderText="Ciclo"
                                                        SortExpression="Ciclo Escolar" />
                                                </Columns>
                                                <PagerTemplate>
                                                    <ul runat="server" id="Pag" class="pagination">
                                                    </ul>
                                                </PagerTemplate>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <SelectedRowStyle CssClass="row-selected" />
                                                <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>
                                        </div>
                                        <%-- sort YES - rowdatabound
	                            0  select
	                            1  id_GRUPO
	                            2  idgrado
	                            3  GRUPO
	                            4  clave
	                            5  GRADO
	                            6  CICLO
	                            7  
	                            8  
	                            9  
	                            10 
                                        --%>
                                    </div>

                                    <div class="col-lg-12">
                                        <asp:LinkButton ID="Insertar" type="submit" runat="server"
                                            CssClass="btn btn-labeled btn-palegreen">
                                    <i class="btn-label fa fa-plus"></i>Insertar
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="Actualizar" Visible="false" type="submit" runat="server"
                                            CssClass="btn btn-labeled btn-palegreen shiny">
                                    <i class="btn-label fa fa-refresh"></i>Actualizar
                                        </asp:LinkButton>

                                        <asp:LinkButton ID="Eliminar" Visible="false" type="submit" runat="server"
                                            CssClass="btn btn-labeled btn-darkorange">
                                     <i class="btn-label fa fa-remove"></i>Eliminar
                                        </asp:LinkButton>
                                    </div>

                                    <div class="col-lg-12">
                                        <uc1:msgError runat="server" ID="msgError" />
                                        <uc1:msgSuccess runat="server" ID="msgSuccess" />
                                    </div>
                                </div>


                            </div>

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <!--Widget-->
        </div>
    </div>

      <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                    SelectCommand="SELECT * FROM [Institucion]"></asp:SqlDataSource>


                                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                    SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion)">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                                            PropertyName="SelectedValue" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>

                                <asp:SqlDataSource ID="SDSniveles" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                    SelectCommand="select N.IdNivel, N.Descripcion
from Nivel N, Escuela E, Plantel P
where N.IdNivel = E.IdNivel and P.IdPlantel = E.IdPlantel
      and P.IdPlantel = @IdPlantel
Order by Descripcion">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                                            PropertyName="SelectedValue" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>

                                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                    SelectCommand="SELECT IdCicloEscolar, Descripcion + ' (' + Cast(FechaInicio as varchar(12)) + ' - ' + Cast(FechaFin as varchar(12)) + ')' Ciclo
FROM CicloEscolar
WHERE (Estatus = 'Activo')
order by Descripcion"></asp:SqlDataSource>

                                <asp:SqlDataSource ID="SDSgrados" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                                            PropertyName="SelectedValue" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>

                                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                    SelectCommand="SELECT G.IdGrupo, G.IdGrado, G.Descripcion Grupo, G.Clave, Gr.Descripcion Grado, C.IdCicloEscolar, C.Descripcion as 'Ciclo Escolar'
FROM Grupo G
JOIN CicloEscolar C on C.IdCicloEscolar = G.IdCicloEscolar and G.IdCicloEscolar = @IdCicloEscolar
JOIN Grado Gr
on Gr.IdGrado = G.IdGrado and G.IdPlantel = @IdPlantel and G.IdGrado = @IdGrado
order by Gr.Descripcion, G.Descripcion">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                                            PropertyName="SelectedValue" />
                                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                                            PropertyName="SelectedValue" />
                                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                                            PropertyName="SelectedValue" />
                                    </SelectParameters>
                                </asp:SqlDataSource>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">
</asp:Content>

