﻿Imports Siget

Partial Class administrador_Grupos
    Inherits System.Web.UI.Page


#Region "LOAD"

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()
    End Sub

#End Region

#Region "ROW_CREATED"

    Protected Sub GVgruposcreados_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVgruposcreados.RowCreated

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = 1 To GVgruposcreados.PageCount
                Dim li As HtmlGenericControl = New HtmlGenericControl("li")

                ' li.ID = "" & counter.ToString
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")
                If counter = GVgruposcreados.PageIndex + 1 Then
                    li.Attributes("class") = "active"
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.Attributes("class") = "sr-only"
                    span.InnerText = "(current)"
                    page.Controls.Add(span)

                End If
                page.InnerText = counter.ToString
                page.ID = "pagina" & counter.ToString

                li.Controls.Add(page)
                con.Controls.Add(li)
            Next

        End If

    End Sub

#End Region

#Region "INSERT"
    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        Try
            SDSgrupos.InsertCommand = "SET dateformat dmy; INSERT INTO Grupo (IdGrado,IdPlantel,IdCicloEscolar,Descripcion, Clave, FechaModif, Modifico) VALUES (" + DDLgrado.SelectedValue + "," + DDLplantel.SelectedValue + "," + DDLcicloescolar.SelectedValue + ", '" + TBdescripcion.Text + "','" + TBClave.Text + "', getdate(), '" & User.Identity.Name & "')"
            SDSgrupos.Insert()
            CType(New MessageSuccess(GVgruposcreados, Me.GetType, "El registro ha sido insertado exitosamente"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVgruposcreados, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try
    End Sub


#End Region

#Region "DATABOUND"

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Institucion"), 0))
        If DDLinstitucion.Items.Count = 2 Then
            DDLinstitucion.SelectedIndex = 1
            DDLplantel.DataBind()
        End If
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Plantel"), 0))
        If DDLplantel.Items.Count = 2 Then
            DDLplantel.SelectedIndex = 1
            DDLnivel.DataBind()
        End If

    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Nivel"), 0))
        If DDLnivel.Items.Count = 2 Then
            DDLnivel.SelectedIndex = 1
            DDLgrado.DataBind()
        End If
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Grado"), 0))


        If DDLgrado.Items.Count = 2 Then
            DDLgrado.SelectedIndex = 1
        End If
    End Sub

    Protected Sub GVgruposcreados_DataBound(sender As Object, e As EventArgs) Handles GVgruposcreados.DataBound

        If GVgruposcreados.Rows.Count = 0 Then
            Actualizar.Visible = False
            Eliminar.Visible = False
            GVgruposcreados.SelectedIndex = -1
        End If

    End Sub


    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija " &
            Config.Etiqueta.ARTIND_CICLO & " " & Config.Etiqueta.CICLO &
            " para " &
            Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO, 0))


        If DDLcicloescolar.Items.Count = 2 Then
            DDLcicloescolar.SelectedIndex = 1
        End If
    End Sub

#End Region

#Region "DELETE"

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        Try
            SDSgrupos.DeleteCommand = "Delete from Grupo where IdGrupo = " + GVgruposcreados.SelectedDataKey("IdGrupo").ToString() 'Me da el IdGrupo porque es el campo que puse como clave de la fila seleccionada en la propiedad DATAKEYNAMES
            SDSgrupos.Delete()
            CType(New MessageSuccess(GVgruposcreados, Me.GetType, "El item se ha eliminado"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVgruposcreados, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try
    End Sub


#End Region

#Region "ROW_DATABOUND"

    Protected Sub GVgruposcreados_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVgruposcreados.RowDataBound
        ' Etiquetas de GridView


        If e.Row.RowType = DataControlRowType.DataRow Then

            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVgruposcreados, "Select$" & e.Row.RowIndex)

        End If


        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = 1 To GVgruposcreados.PageCount

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVgruposcreados, "Page$" & counter.ToString)
            Next
        End If
    End Sub


   

#End Region

#Region "SELECTED_INDEX_CHANGED"

    Protected Sub GVgruposcreados_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVgruposcreados.SelectedIndexChanged
        Actualizar.Visible = True
        Eliminar.Visible = True
        TBdescripcion.Text = HttpUtility.HtmlDecode(GVgruposcreados.SelectedRow.Cells(2).Text)
        TBClave.Text = HttpUtility.HtmlDecode(GVgruposcreados.SelectedRow.Cells(3).Text)
    End Sub

#End Region

#Region "UPDATE"

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        Try
            SDSgrupos.UpdateCommand = "SET dateformat dmy; UPDATE Grupo set Descripcion = '" + TBdescripcion.Text + "',  Clave = '" + TBClave.Text + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdGrupo = " + GVgruposcreados.SelectedDataKey("IdGrupo").ToString()
            SDSgrupos.Update()
            CType(New MessageSuccess(GVgruposcreados, Me.GetType, "El item se ha actualizado"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageSuccess(GVgruposcreados, Me.GetType, "Verifique los campos"), MessageSuccess).show()
        End Try
    End Sub

#End Region

    Protected Sub GVevalalumno_PageIndexChanged(sender As Object, e As EventArgs) Handles GVgruposcreados.PageIndexChanged
        GVgruposcreados.SelectedIndex = -1
        Siget.Utils.GeneralUtils.CleanAll(PanelGrupos.Controls)
        Actualizar.Visible = False
        Eliminar.Visible = False
    End Sub


    Protected Sub GVgruposcreados_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles GVgruposcreados.SelectedIndexChanging
        If e.NewSelectedIndex = CType(sender, GridView).SelectedIndex Then
            GVgruposcreados.SelectedIndex = -1
            e.Cancel = True
            Actualizar.Visible = False
            Eliminar.Visible = False
            Siget.Utils.GeneralUtils.CleanAll(PanelGrupos.Controls)
        End If
    End Sub
End Class
