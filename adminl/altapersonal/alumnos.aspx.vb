﻿


Imports Siget

Imports System.Data
Imports System.IO
Imports System.Data.OleDb 'Este es para MS Access
Imports System.Data.SqlClient
Imports System.Drawing
Imports Siget.Entity
Imports AjaxControlToolkit
Imports Siget.Extensions


Partial Class administrador_AgruparReactivos
    Inherits System.Web.UI.Page

#Region "LOAD"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

    End Sub

#End Region

#Region "Utils"

    Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click
        Siget.Utils.GeneralUtils.CleanAll(AreaCrearAdministrarAlumnos.Controls)
        TBLogin.Enabled = True
        GVAlumnos.SelectedIndex = -1
        Actualizar.Visible = False
        Eliminar.Visible = False
    End Sub

    

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        GVactual.Caption = Config.Etiqueta.ALUMNOS & " de " & Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO & " " & DDLgrupo.SelectedItem.Text

        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = Color.Black

        GVactual.GridLines = GridLines.Both

        For Each row As GridViewRow In GVactual.Rows
            row.BackColor = Color.White
            For Each cell As TableCell In row.Cells
                If row.RowIndex Mod 2 = 0 Then
                    cell.BackColor = GVactual.AlternatingRowStyle.BackColor
                Else
                    cell.BackColor = GVactual.RowStyle.BackColor
                End If
                cell.CssClass = "textmode"
            Next
        Next


        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)

            GVactual.AllowPaging = False
            GVactual.AllowSorting = False
            GVactual.DataBind()
            Dim pagina As System.Web.UI.Page = New System.Web.UI.Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.Flush()
            Response.End()
        Else
            CType(New MessageError(GVAlumnos, Me.GetType, "Demasiados registros para Exportar a Excel."), MessageError).show()
        End If
    End Sub

#End Region

#Region "INSERT"

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        Dim repositoryAlumno As IRepository(Of Alumno) = New AlumnoRepository()
        Dim usuarioRepository As IRepository(Of Usuario) = New UsuarioRepository()
        Dim alumno As Alumno = New Alumno()
        Dim usuario As Usuario = New Usuario()
        Dim status As MembershipCreateStatus
        Dim newUser As MembershipUser = Nothing

        Try

            With usuario
                .Login = TBLogin.Text
                .Password = TBPassword.Text
                .PerfilASP = "Alumno"
                .Email = IIf(TBemail.Text.Trim() <> String.Empty, TBemail.Text, String.Empty)
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
            End With
            With alumno
                .Usuario = usuario
                .IdGrupo = DDLgrupo.SelectedValue
                .IdPlantel = DDLplantel.SelectedValue
                .Nombre = Trim(TBnombre.Text)
                .ApePaterno = Trim(TBApePaterno.Text)
                .ApeMaterno = IIf(Trim(TBApeMaterno.Text) <> String.Empty, TBApeMaterno.Text, " ")
                .Matricula = Trim(TBmatricula.Text)
                .Email = IIf(Trim(TBemail.Text) <> String.Empty, Trim(TBemail.Text), String.Empty)
                .Equipo = IIf(DDLequipo.SelectedValue <> "0", DDLequipo.SelectedItem.ToString, Nothing)
                .Subequipo = IIf(DDLsubequipo.SelectedValue <> "0", DDLsubequipo.SelectedItem.ToString, Nothing)
                .Estatus = DDLestatus.SelectedValue
                .FechaIngreso = DateTime.Now
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
            End With

            newUser = Membership.CreateUser(Trim(TBLogin.Text), Trim(TBPassword.Text), _
                                                              Trim(TBemail.Text), Nothing, _
                                                              Nothing, True, status)
            If newUser Is Nothing Then
                CType(New MessageError(Insertar, Me.GetType, GetErrorMessage(status)), MessageError).show()
            Else
                Roles.AddUserToRole(Trim(TBLogin.Text), "Alumno")
                repositoryAlumno.Add(alumno)
                CType(New MessageSuccess(Insertar, Me.GetType, "El Alumno ha sido creado exitosamente"), MessageSuccess).show()
                GVAlumnos.DataBind()
                panelAlumnos.Update()
            End If


        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)

            If Not (newUser Is Nothing) Then
                Membership.DeleteUser(TBLogin.Text)
            End If

            CType(New MessageError(Insertar, Me.GetType, "Por favor, Verifica los campos"), MessageError).show()
        End Try


    End Sub

#End Region

#Region "DATABOUND"


    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Institucion"), 0))

        If DDLinstitucion.Items.Count = 2 Then
            DDLinstitucion.SelectedIndex = 1
            DDLplantel.DataBind()
        End If
    End Sub

    Protected Sub DDLInstitucionCambiar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLInstitucionCambiar.DataBound
        DDLInstitucionCambiar.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Institucion"), 0))
        If DDLInstitucionCambiar.Items.Count = 2 Then
            DDLInstitucionCambiar.SelectedIndex = 1
        End If
    End Sub

    Protected Sub DDLequipo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLequipo.DataBound
        DDLequipo.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.EQUIPO, 0))
        If DDLequipo.Items.Count = 2 Then
            DDLequipo.SelectedIndex = 1
        End If
    End Sub

    Protected Sub DDLsubequipo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLsubequipo.DataBound
        DDLsubequipo.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.SUBEQUIPO, 0))

        If DDLsubequipo.Items.Count = 2 Then
            DDLsubequipo.SelectedIndex = 1
        End If
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Plantel"), 0))
        If DDLplantel.Items.Count = 2 Then
            DDLplantel.SelectedIndex = 1
            DDLnivel.DataBind()
        End If
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Nivel"), 0))
        If DDLnivel.Items.Count = 2 Then
            DDLnivel.SelectedIndex = 1
            DDLgrado.DataBind()
        End If

    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Grado"), 0))
        If DDLgrado.Items.Count = 2 Then
            DDLgrado.SelectedIndex = 1
            DDLgrupo.DataBind()
        End If
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Grupo"), 0))

        If DDLgrupo.Items.Count = 2 Then
            DDLgrupo.SelectedIndex = 1
            panelAlumnos.DataBind()
            panelAlumnos.Update()
        End If
    End Sub

    Protected Sub GVsubequipos_DataBound(sender As Object, e As EventArgs) Handles GVsubequipos.DataBound
        If GVsubequipos.Rows.Count = 0 Then
            btnActualizarSubEquipo.Visible = False
            btnEliminarSubequipo.Visible = False
            GVsubequipos.SelectedIndex = -1
        End If
    End Sub


#End Region

#Region "SELECTED_INDEX_CHANGED"

    Protected Sub GVAlumnos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVAlumnos.SelectedIndexChanged
        Siget.DataAccess.DataAccessUtils.fillFromGridToControls(GVAlumnos.SelectedDataKey.Values, ObjectAlumno.Controls, New Siget.TransferObjects.AlumnoTo())
        TBLogin.Enabled = False
        Actualizar.Visible = True
        Eliminar.Visible = True
    End Sub

    Protected Sub GVsubequipos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVsubequipos.SelectedIndexChanged
        btnActualizarSubEquipo.Visible = True
        btnEliminarSubequipo.Visible = True
        TBsubequipoAlta.Text = GVsubequipos.SelectedDataKey("Descripcion")
    End Sub

#End Region

#Region "DELETE"

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        If Trim(GVAlumnos.SelectedDataKey("Estatus")) <> "Activo" Then
            Try
                Dim objCommand As New SqlClient.SqlCommand("EliminaAlumno"), Conexion As String = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                objCommand.CommandType = CommandType.StoredProcedure
                objCommand.Parameters.Add("@IdAlumno", SqlDbType.Int, 8)
                objCommand.Parameters("@IdAlumno").Value = CInt(GVAlumnos.SelectedDataKey("IdAlumno").ToString)
                objCommand.CommandTimeout = 3000
                objCommand.Connection = New SqlClient.SqlConnection(Conexion)
                objCommand.Connection.Open()
                objCommand.ExecuteNonQuery()

                Membership.DeleteUser(GVAlumnos.SelectedDataKey("Login").ToString)
                GVAlumnos.DataBind()
                CType(New MessageSuccess(Eliminar, Me.GetType, "El item ha sido eliminado"), MessageSuccess).show()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                CType(New MessageError(Eliminar, Me.GetType, "El item ha sido eliminado"), MessageError).show()
            End Try
        Else

            CType(New MessageError(Eliminar, Me.GetType, "El " &
         Config.Etiqueta.ALUMNO &
         " no puede ser eliminado si está con el estatus de Activo. Considere que al eliminarlo se borrará todo su historial de actividades y respuestas."), MessageError).show()
        End If
    End Sub

#End Region

#Region "UPDATE"

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click


        Dim repositoryAlumno As IRepository(Of Alumno) = New AlumnoRepository()
        Dim repositoryUsuario As IRepository(Of Usuario) = New UsuarioRepository()
        Dim alumno As Alumno = repositoryAlumno.FindById(CInt(GVAlumnos.SelectedDataKey("IdAlumno").ToString))
        Dim usuario As Usuario = repositoryUsuario.FindById(alumno.Usuario.IdUsuario)
        Dim userUpdated As MembershipUser

        Try

            With usuario
                .Password = TBPassword.Text
                .Email = IIf(TBemail.Text.Trim() <> String.Empty, TBemail.Text, String.Empty)
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
            End With

            With alumno
                .IdGrupo = DDLgrupo.SelectedValue
                .IdPlantel = DDLplantel.SelectedValue
                .Nombre = Trim(TBnombre.Text)
                .ApePaterno = Trim(TBApePaterno.Text)
                .ApeMaterno = IIf(Trim(TBApeMaterno.Text) <> String.Empty, TBApeMaterno.Text, " ")
                .Matricula = Trim(TBmatricula.Text)
                .Email = IIf(Trim(TBemail.Text) <> String.Empty, Trim(TBemail.Text), String.Empty)
                .Equipo = IIf(DDLequipo.SelectedValue <> "0", DDLequipo.SelectedItem.ToString, Nothing)
                .Subequipo = IIf(DDLsubequipo.SelectedValue <> "0", DDLsubequipo.SelectedItem.ToString, Nothing)
                .Estatus = DDLestatus.SelectedValue
                .FechaIngreso = DateTime.Now
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
            End With

            repositoryUsuario.Update(usuario)
            repositoryAlumno.Update(alumno)

            userUpdated = Membership.GetUser(Trim(HttpUtility.HtmlDecode(GVAlumnos.SelectedDataKey("Login").ToString)))
            userUpdated.ChangePassword(userUpdated.ResetPassword(), Trim(TBPassword.Text))
            CType(New MessageSuccess(TBApeMaterno, Me.GetType, "El item ha sido actualizado"), MessageSuccess).show()
            GVAlumnos.DataBind()
            panelAlumnos.Update()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(TBApeMaterno, Me.GetType, "Por favor , verifique los campos"), MessageError).show()
        End Try

    End Sub
    Protected Sub BtnCambiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCambiar.Click

        Try
            Dim IdGrupo As Integer
            IdGrupo = CInt(GVAlumnosBuscar.SelectedDataKey("IdGrupo").ToString)
            Dim IdGrado As Integer
            IdGrado = CInt(GVAlumnosBuscar.SelectedDataKey("IdGrado").ToString)

            SDSalumnos.UpdateCommand = "SET dateformat dmy; UPDATE Alumno set IdGrupo = " + DDLGrupoCambiar.SelectedValue + ", IdPlantel = " + DDLPlantelCambiar.SelectedValue + ", FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdAlumno = " + GVAlumnosBuscar.SelectedDataKey("IdAlumno").ToString + " AND IdGrupo = " + IdGrupo.ToString
            SDSalumnos.Update()

            If CBactividades.Checked Then
                'Actualizo todas las evaluaciones que tenga terminadas en el mismo grado, el conjunto de reactivos asignados a un alumno se asignan por el Grado
                SDSevaluacionesterm.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionTerminada set IdGrupo = " + DDLGrupoCambiar.SelectedValue + ", IdGrado = " + DDLgradoCambiar.SelectedValue + " where IdAlumno = " + GVAlumnosBuscar.SelectedDataKey("IdAlumno").ToString + " AND IdGrupo = " + IdGrupo.ToString
                SDSevaluacionesterm.Update()
                SDSevaluacionesterm.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionRepetida set IdGrupo = " + DDLGrupoCambiar.SelectedValue + ", IdGrado = " + DDLgradoCambiar.SelectedValue + " where IdAlumno = " + GVAlumnosBuscar.SelectedDataKey("IdAlumno").ToString + " and IdGrupo = " + IdGrupo.ToString
                SDSevaluacionesterm.Update()


                SDSrespuestas.UpdateCommand = "SET dateformat dmy; UPDATE Respuesta set IdGrupo = " + DDLGrupoCambiar.SelectedValue + ", IdGrado = " + DDLgradoCambiar.SelectedValue + " where IdAlumno = " + GVAlumnosBuscar.SelectedDataKey("IdAlumno").ToString + " AND IdGrupo = " + IdGrupo.ToString
                SDSrespuestas.Update()

                ''Add this line because it didn´t consider in the change of group
                SDSComentarioEvaluacion.UpdateCommand = "SET dateformat dmy; UPDATE ComentarioEvaluacion set IdGrupo=" + DDLGrupoCambiar.SelectedValue + " where IdAlumno = " + GVAlumnosBuscar.SelectedDataKey("IdAlumno").ToString + " AND IdGrupo = " + IdGrupo.ToString
                SDSComentarioEvaluacion.Update()


                GVAlumnosBuscar.DataBind()

                CType(New MessageSuccess(GVAlumnos, Me.GetType, "El " &
                    Config.Etiqueta.GRUPO & " del " &
                    Config.Etiqueta.ALUMNO &
                    " ha sido actualizado. Las evaluaciones terminadas que tuviera este " &
                    " también fueron reasignadas al nuevo " &
                    Config.Etiqueta.GRUPO & "."), MessageSuccess).show()
            Else

                CType(New MessageSuccess(GVAlumnos, Me.GetType, Config.Etiqueta.ARTDET_GRUPO & " " &
                    Config.Etiqueta.GRUPO &
                    " de " &
                    Config.Etiqueta.ARTDET_ALUMNO & " " &
                    Config.Etiqueta.ALUMNO &
                    " ha sido actualizado. Las evaluaciones terminadas quedaron en " &
                    Config.Etiqueta.ARTDET_GRUPO & " " &
                    Config.Etiqueta.GRUPO &
                    " anterior como histórico."), MessageSuccess).show()
            End If

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVAlumnos, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub BtnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBuscar.Click
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Try
            SDSbuscar.ConnectionString = strConexion
            SDSbuscar.SelectCommand = "select Distinct A.IdAlumno, A.Matricula, A.Nombre, A.ApePaterno, A.ApeMaterno, G.IdGrupo, G.Descripcion Grupo, Gr.IdGrado, Gr.Descripcion Grado, N.IdNivel, " + _
                        "N.Descripcion Nivel, P.IdPlantel, P.Descripcion Plantel, U.Login, U.Password, A.Estatus, A.Equipo, A.Subequipo, C.Descripcion Ciclo, A.Email " + _
                        "from Alumno A, Grupo G, Grado Gr, Nivel N, Plantel P, Usuario U, CicloEscolar C " + _
                        "where U.IdUsuario = A.IdUsuario And G.IdGrupo = A.IdGrupo And Gr.IdGrado = G.IdGrado And N.IdNivel = Gr.IdNivel and C.IdCicloEscolar = G.IdCicloEscolar " + _
                        "And P.IdPlantel = G.IdPlantel and P.IdInstitucion = " + DDLInstitucionCambiar.SelectedValue + " and A.Matricula like '%" + TBMatriculaCambiar.Text + "%' and A.Nombre like '%" + TBNombreCambiar.Text + "%' and A.ApePaterno like '%" + _
                        TBApePaternoCambiar.Text + "%' and A.ApeMaterno like '%" + TBApeMaternoCambiar.Text + "%' and U.Login like '%" + TBLoginCambiar.Text + "%' order by A.ApePaterno, A.ApeMaterno, A.Nombre"
            GVAlumnosBuscar.DataBind() 'Es necesario para que se vea el resultado
            If GVAlumnosBuscar.Rows.Count > 0 Then
                CType(New MessageSuccess(BtnBuscar, Me.GetType, "Se encontraron " + CType(SDSbuscar.Select(DataSourceSelectArguments.Empty), DataView).Count.ToString() + " resultados"), MessageSuccess).show()
            Else
                CType(New MessageError(BtnBuscar, Me.GetType, "No se han encontrado resultados", String.Empty), MessageError).show()
            End If


        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(BtnBuscar, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub BtnActualizarCambiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnActualizarCambiar.Click
        Try
            If Trim(TBPasswordCambiar.Text).Length < 4 Then
                CType(New MessageError(GVAlumnos, Me.GetType, "La contraseña debe de ser de 4 caracteres por lo menos."), MessageError).show()
            Else
                Dim u As MembershipUser
                u = Membership.GetUser(Trim(HttpUtility.HtmlDecode(GVAlumnosBuscar.SelectedDataKey("Login").ToString)))
                u.ChangePassword(u.ResetPassword(), Trim(TBPasswordCambiar.Text))
                SDSusuarios.UpdateCommand = "SET dateformat dmy; UPDATE Usuario set Password = '" &
                    Trim(TBPasswordCambiar.Text) &
                    "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "'" & " where Login = '" + Trim(HttpUtility.HtmlDecode(GVAlumnosBuscar.SelectedDataKey("Login").ToString)) + "'"
                SDSusuarios.Update()
                SDSalumnos.UpdateCommand = "UPDATE Alumno SET Estatus='" & DDLEstatusCambiar.SelectedValue & "' WHERE Matricula='" & Trim(HttpUtility.HtmlDecode(GVAlumnosBuscar.SelectedDataKey("Matricula").ToString)) & "'"
                SDSalumnos.Update()
                GVAlumnosBuscar_PageIndexChanged(Nothing, Nothing)

                CType(New MessageSuccess(BtnActualizarCambiar, Me.GetType, "La contraseña de " & Config.Etiqueta.ARTDET_ALUMNO & " " &
                    Config.Etiqueta.ALUMNO &
                    " ha sido cambiada."), MessageSuccess).show()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(BtnActualizarCambiar, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try
    End Sub

#End Region

#Region "SELECTED_INDEX_CHANGED"

    Protected Sub GVAlumnosBuscar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVAlumnosBuscar.SelectedIndexChanged

        TBPasswordCambiar.Text = HttpUtility.HtmlDecode(Trim(GVAlumnosBuscar.SelectedDataKey("Password").ToString))
        DDLEstatusCambiar.SelectedValue = HttpUtility.HtmlDecode(Trim(GVAlumnosBuscar.SelectedDataKey("Estatus").ToString))
    End Sub

    Protected Sub GVequipos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVequipos.SelectedIndexChanged
        btnActualizarEquipo.Visible = True
        btnEliminarEquipo.Visible = True
        TBequipoAlta.Text = GVequipos.SelectedDataKey("Descripcion")
    End Sub
#End Region

#Region "ROW_CREATED"

    Protected Sub GVAlumnosBuscar_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVAlumnosBuscar.RowCreated

        Dim cociente As Integer = (GVAlumnosBuscar.PageCount / 15)
        Dim moduloIndex As Integer = (GVAlumnosBuscar.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVAlumnosBuscar.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVAlumnosBuscar.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVAlumnosBuscar.PageIndex
                final = IIf((inicial + 15 <= GVAlumnosBuscar.PageCount), inicial + 15, GVAlumnosBuscar.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVAlumnosBuscar.PageCount, inicial + 15, GVAlumnosBuscar.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVAlumnosBuscar.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub
    Protected Sub GVAlumnos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVAlumnos.RowCreated


        Dim cociente As Integer = (GVAlumnos.PageCount / 15)
        Dim moduloIndex As Integer = (GVAlumnos.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVAlumnos.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVAlumnos.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVAlumnos.PageIndex
                final = IIf((inicial + 10 <= GVAlumnos.PageCount), inicial + 15, GVAlumnos.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVAlumnos.PageCount, inicial + 15, GVAlumnos.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVAlumnos.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next
        End If

    End Sub

    Protected Sub GVequipos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVequipos.RowCreated

        Dim cociente As Integer = (GVequipos.PageCount / 15)
        Dim moduloIndex As Integer = (GVequipos.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVequipos.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVequipos.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVequipos.PageIndex
                final = IIf((inicial + 15 <= GVequipos.PageCount), inicial + 15, GVequipos.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVequipos.PageCount, inicial + 15, GVequipos.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVequipos.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVsubequipos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVsubequipos.RowCreated

        Dim cociente As Integer = (GVsubequipos.PageCount / 15)
        Dim moduloIndex As Integer = (GVsubequipos.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVsubequipos.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVsubequipos.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVsubequipos.PageIndex
                final = IIf((inicial + 15 <= GVsubequipos.PageCount), inicial + 15, GVsubequipos.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVsubequipos.PageCount, inicial + 15, GVsubequipos.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVsubequipos.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

#End Region

#Region "ROW_DATA_BOUND"


    Protected Sub GVAlumnos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVAlumnos.RowDataBound

        Dim cociente As Integer = (GVAlumnos.PageCount) / 15
        Dim moduloIndex As Integer = (GVAlumnos.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVAlumnos.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVAlumnos.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVAlumnos.PageIndex
            final = IIf((inicial + 15 <= GVAlumnos.PageCount), inicial + 15, GVAlumnos.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVAlumnos.PageCount), inicial + 15, GVAlumnos.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVAlumnos, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVAlumnos, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVequipos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVequipos.RowDataBound


        Dim cociente As Integer = (GVequipos.PageCount) / 15
        Dim moduloIndex As Integer = (GVequipos.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVequipos.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVequipos.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVequipos.PageIndex
            final = IIf((inicial + 15 <= GVequipos.PageCount), inicial + 15, GVequipos.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVequipos.PageCount), inicial + 15, GVequipos.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVequipos, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVequipos, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVAlumnosBuscar_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVAlumnosBuscar.RowDataBound

        Dim cociente As Integer = (GVAlumnosBuscar.PageCount) / 15
        Dim moduloIndex As Integer = (GVAlumnosBuscar.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVAlumnosBuscar.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVAlumnosBuscar.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVAlumnosBuscar.PageIndex
            final = IIf((inicial + 15 <= GVAlumnosBuscar.PageCount), inicial + 15, GVAlumnosBuscar.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVAlumnosBuscar.PageCount), inicial + 15, GVAlumnosBuscar.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVAlumnosBuscar, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVAlumnosBuscar, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVsubequipos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVsubequipos.RowDataBound


        Dim cociente As Integer = (GVsubequipos.PageCount) / 15
        Dim moduloIndex As Integer = (GVsubequipos.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVsubequipos.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVsubequipos.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVsubequipos.PageIndex
            final = IIf((inicial + 15 <= GVsubequipos.PageCount), inicial + 15, GVsubequipos.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVsubequipos.PageCount), inicial + 15, GVsubequipos.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVsubequipos, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVsubequipos, "Page$" & counter.ToString)
            Next
        End If
    End Sub

#End Region


    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVAlumnos, "Grupo-" + HttpUtility.HtmlDecode(DDLgrado.SelectedItem.ToString) + "-" + HttpUtility.HtmlDecode(DDLgrupo.SelectedItem.ToString))
    End Sub

    Protected Sub GVequipos_DataBound(sender As Object, e As EventArgs) Handles GVequipos.DataBound
        If GVsubequipos.Rows.Count = 0 Then
            btnActualizarEquipo.Visible = False
            btnEliminarEquipo.Visible = False
            GVequipos.SelectedIndex = -1
        End If
    End Sub

    Protected Sub btnCrearEquipo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrearEquipo.Click
        Try
            If Trim(TBequipoAlta.Text) <> "" Then
                SDSEquipos.InsertCommand = "SET dateformat dmy; INSERT INTO Equipo (IdInstitucion,Descripcion, FechaModif, Modifico) VALUES (" + DDLInstitucionAlta.SelectedValue + ",'" + Trim(TBequipoAlta.Text) + "', getdate(), '" & User.Identity.Name & "')"
                SDSEquipos.Insert()
                TBequipoAlta.Text = ""
                msgError.hide()

                CType(New MessageSuccess(GVAlumnos, Me.GetType, "El registro se dio de alta correctamente"), MessageSuccess).show()
                GVequipos.DataBind()
            Else

                CType(New MessageError(GVAlumnos, Me.GetType, "No puede ingresar " &
                    Config.Etiqueta.ARTIND_SUBEQUIPO & " " & Config.Etiqueta.EQUIPO &
                    " sin capturar algún nombre de máximo 20 caracteres."), MessageError).show()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVAlumnos, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub btnActualizarEquipo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarEquipo.Click
        Try
            SDSEquipos.UpdateCommand = "SET dateformat dmy; UPDATE Alumno SET Equipo = '" + Trim(TBequipoAlta.Text) + "' WHERE Equipo = '" & GVequipos.SelectedDataKey("Descripcion") & "'; UPDATE Equipo SET Descripcion = '" + Trim(TBequipoAlta.Text) + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdEquipo = " + GVequipos.SelectedValue.ToString.Trim() 'Me da el IdEquipo porque es el campo clave de la fila seleccionada
            SDSEquipos.Update()
            GVequipos.DataBind()
            CType(New MessageSuccess(GVAlumnos, Me.GetType, "El registro se actualizó correctamente"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVAlumnos, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub btnEliminarEquipo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarEquipo.Click
        Try
            SDSEquipos.DeleteCommand = "SET dateformat dmy; UPDATE Alumno SET Equipo = '' WHERE Equipo = '" & GVequipos.SelectedDataKey("Descripcion") & "'; Delete from Equipo where IdEquipo = " + GVequipos.SelectedValue.ToString 'Me da el IdEquipo porque es el campo clave de la fila seleccionada
            SDSEquipos.Delete()
            GVequipos.DataBind()
            TBequipoAlta.Text = String.Empty
            CType(New MessageSuccess(GVAlumnos, Me.GetType, "El registro se elimino correctamente"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVAlumnos, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub btnCrearSubequipo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrearSubequipo.Click
        Try
            If Trim(TBsubequipoAlta.Text) <> "" Then
                SDSSubEquipos.InsertCommand = "SET dateformat dmy; INSERT INTO SubEquipo (IdInstitucion,Descripcion, FechaModif, Modifico) VALUES (" + DDLInstitucionAlta.SelectedValue + ",'" + Trim(TBsubequipoAlta.Text) + "', getdate(), '" & User.Identity.Name & "')"
                SDSSubEquipos.Insert()
                TBsubequipoAlta.Text = String.Empty
                CType(New MessageSuccess(GVAlumnos, Me.GetType, "El registro se creó correctamente"), MessageSuccess).show()
                GVsubequipos.DataBind()
            Else
                CType(New MessageError(GVAlumnos, Me.GetType, "No puede ingresar " &
                   Config.Etiqueta.ARTIND_SUBEQUIPO & " " & Config.Etiqueta.SUBEQUIPO &
                   " sin capturar algún nombre de máximo 20 caracteres."), MessageError).show()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVAlumnos, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub btnActualizarSubEquipo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarSubEquipo.Click
        Try
            SDSSubEquipos.UpdateCommand = "SET dateformat dmy; UPDATE Alumno SET SubEquipo = '" + Trim(TBsubequipoAlta.Text) + "' WHERE SubEquipo = '" & GVsubequipos.SelectedDataKey("Descripcion") & "'; UPDATE SubEquipo SET Descripcion = '" + Trim(TBsubequipoAlta.Text) + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdSubEquipo = " + GVsubequipos.SelectedValue.ToString 'Me da el IdEquipo porque es el campo clave de la fila seleccionada
            SDSSubEquipos.Update()
            GVsubequipos.DataBind()
            CType(New MessageSuccess(GVAlumnos, Me.GetType, "El registro se actualizo correctamente"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVAlumnos, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub btnEliminarSubequipo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarSubequipo.Click
        Try
            SDSSubEquipos.DeleteCommand = "SET dateformat dmy; UPDATE Alumno SET SubEquipo = '' WHERE SubEquipo = '" & GVsubequipos.SelectedDataKey("Descripcion") & "'; Delete from SubEquipo where IdSubEquipo = " + GVsubequipos.SelectedValue.ToString 'Me da el IdEquipo porque es el campo clave de la fila seleccionada
            SDSSubEquipos.Delete()
            GVsubequipos.DataBind()
            TBsubequipoAlta.Text = String.Empty
            CType(New MessageSuccess(GVAlumnos, Me.GetType, "El registro se eliminó correctamente"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVAlumnos, Me.GetType, "Verifica los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub GVAlumnos_DataBound(sender As Object, e As EventArgs) Handles GVAlumnos.DataBound
        If GVAlumnos.Rows.Count = 0 Then
            Actualizar.Visible = False
            Eliminar.Visible = False
            GVAlumnos.SelectedIndex = -1
            BtnExportar.Visible = False
        Else
            BtnExportar.Visible = True
        End If
    End Sub


    Protected Sub GVAlumnosBuscar_PageIndexChanged(sender As Object, e As EventArgs) Handles GVAlumnosBuscar.PageIndexChanged
        GVAlumnosBuscar.SelectedIndex = -1
        SDSbuscar.ConnectionString = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString

        SDSbuscar.SelectCommand = "select Distinct A.IdAlumno, A.Matricula, A.Nombre, A.ApePaterno, A.ApeMaterno, G.IdGrupo, G.Descripcion Grupo, Gr.IdGrado, Gr.Descripcion Grado, N.IdNivel, " +
      "N.Descripcion Nivel, P.IdPlantel, P.Descripcion Plantel, U.Login, U.Password, A.Estatus, A.Equipo, A.Subequipo, C.Descripcion Ciclo, A.Email " +
      "from Alumno A, Grupo G, Grado Gr, Nivel N, Plantel P, Usuario U, CicloEscolar C " +
      "where U.IdUsuario = A.IdUsuario And G.IdGrupo = A.IdGrupo And Gr.IdGrado = G.IdGrado And N.IdNivel = Gr.IdNivel and C.IdCicloEscolar = G.IdCicloEscolar " +
      " And P.IdPlantel = G.IdPlantel and P.IdInstitucion = '" + DDLInstitucionCambiar.SelectedValue.ToString + " ' and A.Matricula like '%" + TBMatriculaCambiar.Text + "%' and A.Nombre like '%" + TBNombreCambiar.Text + "%' and A.ApePaterno like '%" +
      TBApePaternoCambiar.Text + "%' and A.ApeMaterno like '%" + TBApeMaternoCambiar.Text + "%' and U.Login like '%" + TBLoginCambiar.Text + "%' order by A.ApePaterno, A.ApeMaterno, A.Nombre"
        GVAlumnosBuscar.DataBind()
    End Sub

    Protected Sub DDLPlantelCambiar_DataBound(sender As Object, e As EventArgs) Handles DDLPlantelCambiar.DataBound

        DDLPlantelCambiar.Items.Insert(0, New ListItem("--Elija un item", "0"))

        If DDLPlantelCambiar.Items.Count = 2 Then
            DDLPlantelCambiar.SelectedIndex = 1
            DDLNivelCambiar.DataBind()
        End If
    End Sub

    Protected Sub DDLNivelCambiar_DataBound(sender As Object, e As EventArgs) Handles DDLNivelCambiar.DataBound
        DDLNivelCambiar.Items.Insert(0, New ListItem("--Elija un item", "0"))

        If DDLNivelCambiar.Items.Count = 2 Then
            DDLNivelCambiar.SelectedIndex = 1
            DDLgradoCambiar.DataBind()
        End If
    End Sub

    Protected Sub DDLgradoCambiar_DataBound(sender As Object, e As EventArgs) Handles DDLgradoCambiar.DataBound

        DDLgradoCambiar.Items.Insert(0, New ListItem("--Elija un item", "0"))
        If DDLgradoCambiar.Items.Count = 2 Then
            DDLgradoCambiar.SelectedIndex = 1
            DDLGrupoCambiar.DataBind()
        End If
    End Sub


    Protected Sub DDDLGrupoCambiar_DataBound(sender As Object, e As EventArgs) Handles DDLGrupoCambiar.DataBound

        DDLGrupoCambiar.Items.Insert(0, New ListItem("--Elija un item", "0"))
        If DDLGrupoCambiar.Items.Count = 2 Then
            DDLGrupoCambiar.SelectedIndex = 1

        End If
    End Sub

    Protected Sub DDLCicloEscolarCambiar_DataBound(sender As Object, e As EventArgs) Handles DDLCicloEscolarCambiar.DataBound

        DDLCicloEscolarCambiar.Items.Insert(0, New ListItem("--Elija un item", "0"))
        If DDLCicloEscolarCambiar.Items.Count = 2 Then
            DDLCicloEscolarCambiar.SelectedIndex = 1
            DDLgrupo.DataBind()
        End If
    End Sub


    Protected Sub GVAlumnos_PageIndexChanged(sender As Object, e As EventArgs) Handles GVAlumnos.PageIndexChanged
        GVAlumnos.SelectedIndex = -1
        Siget.Utils.GeneralUtils.CleanAll(AreaCrearAdministrarAlumnos.Controls)
        panelAlumnos.Update()
        Actualizar.Visible = False
        Eliminar.Visible = False

    End Sub


    Protected Sub GVequipos_PageIndexChanged(sender As Object, e As EventArgs) Handles GVequipos.PageIndexChanged
        GVequipos.SelectedIndex = -1
        btnActualizarEquipo.Visible = False
        btnEliminarEquipo.Visible = False
    End Sub

    Protected Sub GVsubequipos_PageIndexChanged(sender As Object, e As EventArgs) Handles GVsubequipos.PageIndexChanged
        GVsubequipos.SelectedIndex = -1
        btnActualizarSubEquipo.Visible = False
        btnEliminarSubequipo.Visible = False
    End Sub

    Protected Sub GVAlumnos_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles GVAlumnos.SelectedIndexChanging
        If e.NewSelectedIndex = CType(sender, GridView).SelectedIndex Then
            GVAlumnos.SelectedIndex = -1
            e.Cancel = True
            Actualizar.Visible = False
            Eliminar.Visible = False
            Siget.Utils.GeneralUtils.CleanAll(AreaCrearAdministrarAlumnos.Controls)
            panelAlumnos.Update()
        End If
    End Sub

    Protected Sub GVequipos_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles GVequipos.SelectedIndexChanging
        If e.NewSelectedIndex = CType(sender, GridView).SelectedIndex Then
            GVequipos.SelectedIndex = -1
            e.Cancel = True
            btnActualizarEquipo.Visible = False
            btnEliminarEquipo.Visible = False
        End If
    End Sub


    Protected Sub GVsubequipos_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles GVsubequipos.SelectedIndexChanging
        If e.NewSelectedIndex = CType(sender, GridView).SelectedIndex Then
            GVsubequipos.SelectedIndex = -1
            e.Cancel = True
            btnActualizarSubEquipo.Visible = False
            btnEliminarSubequipo.Visible = False
        End If
    End Sub

    Protected Sub ArchivoCarga_UploadedComplete(sender As Object, e As AsyncFileUploadEventArgs) Handles ArchivoCarga.UploadedComplete
        Beans.FileReference.Activator(1) = True
        Beans.FileReference.Files(1) = ArchivoCarga.PostedFile
        Beans.FileReference.nameOfFiles(1) = ArchivoCarga.FileName
    End Sub

    Protected Sub Importar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Importar.Click
        Dim alumnos As List(Of Siget.Entity.Alumno) = New List(Of Siget.Entity.Alumno)


        If Beans.FileReference.Activator(1) Then 'El atributo .HasFile compara si se indico un archivo           
            Try
                Dim Todobien = True
                If CBregistrarme.Checked Then
                    If Trim(TBemailAlumno.Text) = "" Then

                        Todobien = False
                    End If
                End If
                If Todobien Then
                    'Ahora CARGO el archivo
                    Beans.FileReference.Files(1).SaveAs(Server.MapPath("~") + "\administrador\Alumno\" & Beans.FileReference.nameOfFiles(1))
                    'Leo el archivo
                    Dim archivo_datos As String = Server.MapPath(Beans.FileReference.nameOfFiles(1))
                    'Dim leer_archivo As StreamReader
                    Dim leer_archivo As StreamReader = New StreamReader(archivo_datos, Encoding.Default, True)
                    'leer_archivo = File.OpenText(archivo_datos)
                    Dim LineaLeida, IdGrupo, Nombre, ApPaterno, ApMaterno, Matricula, Equipo, Subequipo, Login, Password, Grupo, Plantel, Grado As String
                    Dim I, F, IdUsuario, IdPlantel As Integer

                    Dim strConexion As String
                    'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                    strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                    Dim objConexion As New SqlConnection(strConexion)
                    Dim miComando As SqlCommand
                    Dim misRegistros As SqlDataReader
                    objConexion.Open()
                    miComando = objConexion.CreateCommand




                    While Not leer_archivo.EndOfStream
                        Dim duplicate As Boolean = False
                        'Leo el registro completo y ubico cada campo
                        LineaLeida = leer_archivo.ReadLine
                        I = InStr(LineaLeida, "|")
                        F = InStr(I + 1, LineaLeida, "|")
                        IdGrupo = Trim(Mid(LineaLeida, 1, I - 1)) 'CAMPO 1
                        Nombre = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 2
                        I = F
                        F = InStr(I + 1, LineaLeida, "|")
                        ApPaterno = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 3
                        I = F
                        F = InStr(I + 1, LineaLeida, "|")
                        ApMaterno = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 4
                        I = F
                        F = InStr(I + 1, LineaLeida, "|")
                        Matricula = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 5
                        I = F
                        F = InStr(I + 1, LineaLeida, "|")
                        Equipo = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 6
                        I = F
                        F = InStr(I + 1, LineaLeida, "|")
                        Subequipo = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 7
                        I = F
                        F = InStr(I + 1, LineaLeida, "|")
                        Login = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 8
                        I = F
                        Password = Trim(Mid(LineaLeida, I + 1, Len(LineaLeida))) 'CAMPO 9 (Ultimo)

                        Try
                            SDSusuarios.InsertCommand = "SET dateformat dmy; INSERT INTO Usuario (Login,Password,PerfilASP,FechaModif) VALUES ('" + Login + "','" + Password + "','Alumno',getdate())"
                            SDSusuarios.Insert()
                        Catch ex As Exception

                            If ex.Message.Contains("duplicate key") Then
                                Dim alumno As Alumno = New Alumno()
                                With alumno
                                    .IdGrupo = IdGrupo
                                    .Nombre = Nombre
                                    .ApePaterno = ApPaterno
                                    .ApeMaterno = ApMaterno
                                    .Matricula = Matricula
                                    .Equipo = Equipo
                                    .Subequipo = Subequipo
                                End With
                                alumnos.Add(alumno)
                                duplicate = True
                            End If
                        End Try

                        If Not duplicate Then
                            'Ahora busco el recien ingresado Usuario para obtener su ID                  
                            miComando.CommandText = "SELECT * FROM USUARIO WHERE Login = '" + Login + "'"
                            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                            misRegistros.Read()
                            IdUsuario = misRegistros.Item("IdUsuario")
                            misRegistros.Close()

                            'Ahora busco el IdPlantel que le pertenece al Grupo y saco los datos de descripcion del Grado y del Plantel
                            miComando.CommandText = "SELECT G.IdGrupo,G.Descripcion as Grupo,G.IdPlantel,P.Descripcion as Plantel,Gr.Descripcion as Grado FROM GRUPO G, PLANTEL P, GRADO Gr WHERE P.IdPlantel = G.IdPlantel and Gr.IdGrado = G.IdGrado and IdGrupo = " + IdGrupo
                            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                            misRegistros.Read()
                            IdPlantel = misRegistros.Item("IdPlantel")
                            Plantel = misRegistros.Item("Plantel")
                            Grado = misRegistros.Item("Grado")
                            Grupo = misRegistros.Item("Grupo")

                            misRegistros.Close()

                            If CBregistrarme.Checked Then 'AQUI DECIDO SI REGISTRO O NO EL NUEVO USUARIO EN LA TABLA DE SEGURIDAD, ORIGINALMENTE LA FECHA DE INGRESO SE TOMABA COMO LA FECHA EN QUE EL ALUMNO SE AUTOREGISTRABA EN LA BASE DE DATOS DE SEGURIDAD
                                'Ahora ingreso el registro del Alumno, el campo FECHAINGRESO lo tomo para almacenar el momento en que se da de alta en las tablas de seguridad
                                SDSalumnos.InsertCommand = "SET dateformat dmy; INSERT INTO Alumno (IdUsuario,IdGrupo,IdPlantel,Nombre,ApePaterno,ApeMaterno,Matricula,Equipo,Subequipo,Estatus,FechaIngreso,FechaModif) VALUES (" + CStr(IdUsuario) + "," + IdGrupo + "," + CStr(IdPlantel) + ",'" + Nombre + "','" + ApPaterno + "','" + ApMaterno + "','" + Matricula + "','" + Equipo + "','" + Subequipo + "','Activo',getdate(),getdate())"
                                SDSalumnos.Insert()

                                'Ahora registro el usuario en la base de datos de seguridad
                                Dim status As MembershipCreateStatus
                                Try
                                    Dim newUser As MembershipUser = Membership.CreateUser(Login, Password, _
                                                                                   Trim(TBemail.Text), Nothing, _
                                                                                   Nothing, True, status)
                                    If newUser Is Nothing Then
                                        CType(New MessageError(Importar, Me.GetType(), GetErrorMessage(status)), MessageError).show()
                                    Else
                                        Roles.AddUserToRole(Login, "Alumno")
                                    End If
                                Catch ex As MembershipCreateUserException
                                    Utils.LogManager.ExceptionLog_InsertEntry(ex)
                                    CType(New MessageError(Importar, Me.GetType(), ex.Message.ToString()), MessageError).show()
                                End Try
                            Else
                                'Ahora ingreso el registro del Alumno, como se decidió no registrarse en la tabla de seguridad, aquí no grabo la FechaIngreso
                                SDSalumnos.InsertCommand = "SET dateformat dmy; INSERT INTO Alumno (IdUsuario,IdGrupo,IdPlantel,Nombre,ApePaterno,ApeMaterno,Matricula,Equipo,Subequipo,Estatus,FechaModif) VALUES (" + CStr(IdUsuario) + "," + IdGrupo + "," + CStr(IdPlantel) + ",'" + Nombre + "','" + ApPaterno + "','" + ApMaterno + "','" + Matricula + "','" + Equipo + "','" + Subequipo + "','Activo',getdate())"
                                SDSalumnos.Insert()
                            End If
                        End If

                    End While
                    objConexion.Close()
                    leer_archivo.Close()

                    Dim data As DataSet = alumnos.ToDataSet({"Nombre", "ApePaterno", "ApeMaterno", "IdGrupo", "Matricula"})

                    GvAlumnosRepetidos.DataSource = data
                    GvAlumnosRepetidos.DataBind()
                   

                    CType(New MessageSuccess(Importar, Me.GetType(), "La migración ha terminado con éxito, revise los registros.<BR>"), MessageSuccess).show()
                    Dim MiArchivo As FileInfo = New FileInfo(archivo_datos)
                    MiArchivo.Delete()

                    If CBregistrarme.Checked Then
                        CType(New MessageSuccess(Importar, Me.GetType(), "Las cuentas de los " & Config.Etiqueta.ALUMNOS & " migrados ya han sido registradas en la base de datos de seguridad"), MessageSuccess).show()
                    Else
                        CType(New MessageSuccess(Importar, Me.GetType(), "Los " & Config.Etiqueta.ALUMNOS & " migrados deberán registrarse con su Matrícula y Login para activar su cuenta"), MessageSuccess).show()
                    End If




                Else
                    CType(New MessageError(Importar, Me.GetType(), "Especifique una dirección de correo correcta"), MessageError).show()
                End If
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                CType(New MessageError(Importar, Me.GetType(), ex.Message.ToString()), MessageError).show()
            End Try
        Else
            CType(New MessageError(Importar, Me.GetType(), "Seleccione el archivo que contiene los datos para Migrar"), MessageError).show()

        End If
    End Sub

    Public Function GetErrorMessage(ByVal status As MembershipCreateStatus) As String
        Select Case status
            Case MembershipCreateStatus.DuplicateUserName
                Return "El usuario ya existe, elija otro usuario."
            Case MembershipCreateStatus.DuplicateEmail
                Return "Ya existe un usuario para ese email, por favor escriba un email diferente."
            Case MembershipCreateStatus.InvalidPassword
                Return "El password es inválido, pruebe con otro."
            Case MembershipCreateStatus.InvalidEmail
                Return "El formato del email es inválido, por favor revíselo."
            Case MembershipCreateStatus.InvalidAnswer
                Return "La respuesta secreta es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidQuestion
                Return "La pregunta de recuperación de password es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidUserName
                Return "El nombre de usuario es inválido, corríjalo."
            Case MembershipCreateStatus.ProviderError
                Return "La autentificación falló, contacte al administrador del sistema."
            Case MembershipCreateStatus.UserRejected
                Return "La creación del usuario ha sido cancelada, consulte al administrador."
            Case Else
                Return "Ha ocurrido un error desconocido, verifique o contacte al administrador."
        End Select
    End Function

End Class



