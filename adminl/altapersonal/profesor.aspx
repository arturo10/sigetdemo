﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false" EnableEventValidation="false" CodeFile="profesor.aspx.vb" Inherits="administrador_AgruparReactivos" %>


<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat radius-bordered">
            <div class="widget-header bg-themeprimary">
                <span class="widget-caption"><b>ÁREA DE COACHES</b></span>
            </div>

            <div class="widget-body">
                <div class="widget-main ">
                    <div class="tabbable">
                        <ul class="nav nav-tabs tabs-flat" id="myTab11">
                            <li id="Home" class="active">
                                <a data-toggle="tab" href="#createProfesor">Crear y administrar
                                </a>
                            </li>
                            <li>
                                <a id="Messages" data-toggle="tab" href="#asignarGrupos">Asignar a Grupos
                                </a>
                            </li>
                            <li>
                                <a id="Search" data-toggle="tab" href="#cambiar">Buscar y Cambiar
                                </a>
                            </li>
                            
                        </ul>
                        <div class="tab-content tabs-flat">
                            <div id="createProfesor" class="tab-pane in active">

                                <div class="form-horizontal">

                                    <div class="panel panel-default">

                                      
                                        <asp:UpdatePanel ID="CrearAdministrarProfesor" runat="server">
                                            <ContentTemplate>
                                                <div class="panel-body" id="profesorForm">
                                                    <div class="col-lg-12" style="position: absolute; top: 50%; left: 50%; bottom: 50%;">
                                                        <div class="load-spinner"></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbn1" runat="server" CssClass="col-lg-2 control-label"
                                                            ><%=Lang_Config.Translate("general","Institucion") %></asp:Label>
                                                        <div class="col-lg-4">

                                                            <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                                                DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                                                DataValueField="IdInstitucion" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>

                                                        <asp:Label ID="Label5" runat="server" CssClass="col-lg-2 control-label"
                                                            ><%=Lang_Config.Translate("general","Plantel") %></asp:Label>
                                                        <div class="col-lg-4">

                                                            <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                                                DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                                                DataValueField="IdPlantel" CssClass="form-control">
                                                            </asp:DropDownList>
                                                            
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-lg-7 col-lg-offset-3">
                                                            <a data-toggle="modal"
                                                                class="btn btn-labeled btn-palegreen" data-target=".bs-example-modal-lg">
                                                                <i class="btn-label glyphicon glyphicon-search"></i><%=Lang_Config.Translate("profesor","Profesores_disponibles") %> 
                                                            </a>
                                                        </div>
                                                       
                                                    </div>

                                                    
                                                    <div class="form-group">

                                                           <asp:Label ID="Label6" runat="server" CssClass="col-lg-2 control-label"
                                                            Text="Nombre"></asp:Label>
                                                        <div class="col-lg-4">
                                                            <asp:TextBox ID="TBnombre" runat="server" MaxLength="40" CssClass="form-control"></asp:TextBox>
                                                        </div>  
                                                        <asp:Label ID="Label2" runat="server" CssClass="col-lg-2 control-label"
                                                            Text="Apellidos"></asp:Label>
                                                        <div class="col-lg-4">

                                                            <asp:TextBox ID="TBapellidos" runat="server" CssClass="form-control"
                                                                MaxLength="60"></asp:TextBox>
                                                        </div>

                                                      

                                                    </div>
                                                    <div class="form-group">
                                                        <asp:Label ID="Label3" runat="server" CssClass="col-lg-2 control-label"
                                                            Text="Usuario"></asp:Label>
                                                        <div class="col-lg-4">

                                                            <asp:TextBox ID="TBlogin" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        <asp:Label ID="Label4" runat="server" CssClass="col-lg-2 control-label"
                                                            Text="Password"></asp:Label>
                                                        <div class="col-lg-4">
                                                            <asp:TextBox ID="TBpassword" runat="server" MaxLength="20" CssClass="form-control"></asp:TextBox>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">

                                                        <asp:Label ID="Label8" runat="server" CssClass="col-lg-2 control-label"
                                                            Text="Email"></asp:Label>
                                                        <div class="col-lg-4">
                                                            <asp:TextBox ID="TBemail" runat="server" CssClass="form-control"
                                                                MaxLength="100"></asp:TextBox>
                                                        </div>
                                                      
                                                          <asp:Label ID="Label9" runat="server" CssClass="col-lg-2 control-label"
                                                            Text="Estatus"></asp:Label>
                                                        <div class="col-lg-4">
                                                            <asp:DropDownList ID="DDLestatus" CssClass="form-control" runat="server">
                                                                <asp:ListItem>Activo</asp:ListItem>
                                                                <asp:ListItem>Suspendido</asp:ListItem>
                                                                <asp:ListItem>Baja</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                    </div>
                                                   

                                                   
                                                    <div class="col-lg-12">
                                                         <asp:LinkButton ID="CrearNuevoProfesor" runat="server" type="submit"
                                                            CssClass=" btn btn-labeled btn-warning">
                                                                         <i class="btn-label fa fa-plus"></i> Limpiar
                                                             </asp:LinkButton>
                                                     
                                                        <asp:LinkButton ID="InsertarProfesor" runat="server" type="submit"
                                                            CssClass="Apr btn btn-labeled btn-palegreen">
                                                                         <i class="btn-label fa fa-plus"></i> Insertar
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="ActualizarProfesor" Visible="false" runat="server"  type="submit"
                                                            CssClass=" Apr btn btn-labeled btn-palegreen shiny">
                                                                      <i class="btn-label fa fa-refresh"></i> Actualizar
                                                        </asp:LinkButton>
                                                         
                                                    </div>
                                                    <div class="col-lg-12">

                                                        <uc1:msgSuccess runat="server" ID="msgSuccess" />
                                                        <uc1:msgError runat="server" ID="msgError" />

                                                    </div>
                                        </div>
                                                </ContentTemplate>
                                          </asp:UpdatePanel>
                                    </div>

                                </div>



                                <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myLargeModalLabel"><b>Coaches Disponibles en <%=DDLplantel.SelectedItem.Text%></b></h4>
                                            </div>
                                            <div class="modal-body">
                                                <div  class="form-group">
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="GVprofesores" runat="server"
                                                                AllowPaging="True" 
                                                                AllowSorting="True"
                                                                AutoGenerateColumns="False"
                                                                DataKeyNames="IdProfesor,IdPlantel,IdUsuario,Nombre,Apellidos,RFC,Email,Estatus,Login,Password"
                                                                DataSourceID="SDSprofesores"
                                                                PageSize="10"
                                                                CellPadding="3"
                                                                CssClass="table table-striped table-bordered"
                                                                Height="17px"
                                                                Style="font-size: x-small; text-align: left;">
                                                                <Columns>
                                                                    <asp:BoundField DataField="IdProfesor" HeaderText="Id Profesor"
                                                                        InsertVisible="False" Visible="false" ReadOnly="True" SortExpression="IdProfesor">
                                                                        <HeaderStyle Width="60px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField  DataField="IdPlantel" HeaderText="IdPlantel"
                                                                        SortExpression="IdPlantel" Visible="False" />
                                                                    <asp:BoundField DataField="IdUsuario" HeaderText="IdUsuario"
                                                                        SortExpression="IdUsuario" Visible="False" />
                                                                  
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre"
                                                                        SortExpression="Nombre" />
                                                                    <asp:BoundField DataField="Apellidos" HeaderText="Apellidos"
                                                                        SortExpression="Apellidos" />
                                                                    <asp:BoundField DataField="RFC" Visible="false"  HeaderText="RFC" SortExpression="RFC" />
                                                                    <asp:BoundField DataField="Email" Visible="false" HeaderText="Email" SortExpression="Email" />
                                                                    <asp:BoundField DataField="FechaIngreso" HeaderText="FechaIngreso"
                                                                        SortExpression="FechaIngreso" Visible="False" />
                                                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                                        SortExpression="Estatus" Visible="False" />
                                                                    <asp:BoundField DataField="FechaModif" HeaderText="FechaModif"
                                                                        SortExpression="FechaModif" Visible="False" />
                                                                    <asp:BoundField DataField="Modifico" HeaderText="Modifico"
                                                                        SortExpression="Modifico" Visible="False" />
                                                                    <asp:BoundField DataField="Login" HeaderText="Login" SortExpression="Login" />
                                                                    <asp:BoundField DataField="Password" HeaderText="Password"
                                                                        SortExpression="Password" />
                                                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                                        SortExpression="Estatus" />
                                                                </Columns>
                                                                <PagerTemplate>
                                                                    <ul runat="server" id="Pag" class="pagination">
                                                                    </ul>
                                                                </PagerTemplate>
                                                                <PagerStyle HorizontalAlign="Center" />
                                                                <SelectedRowStyle CssClass="row-selected" />
                                                                <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />

                                                            </asp:GridView>
                                                        </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:LinkButton ID="EliminarProfesor" Visible="false" runat="server"
                                                        CssClass="btn btn-labeled btn-darkorange">
                                                                         <i class="btn-label fa fa-remove"></i> Eliminar
                                                    </asp:LinkButton>
                                                    <a id="btnCerrarModal" data-dismiss="modal" aria-hidden="true"
                                                        class="btn btn-labeled btn-warning">
                                                        <i class="btn-label fa fa-minus"></i>Cerrar
                                                    </a>
                                                </div>
                                                    
                                                <div class="form-group">
                                                    <uc1:msgSuccess runat="server" ID="msgSuccess3" />
                                                    <uc1:msgError runat="server" ID="msgError3" />
                                                </div>

                                                  
                                            </div>
                                                          </ContentTemplate>
                                                </asp:UpdatePanel>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>

                            </div>

                            <div id="asignarGrupos" class="tab-pane">


                                <div class="form-horizontal">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>

                                            <div class="panel panel-default">

                                                <div class="panel-heading bg-themeprimary-lg">

                                                    <b>ÁREA PARA ASIGNAR GRUPOS A <asp:Label runat="server" CssClass="lsize-st" ID="profesorSelected"></asp:Label></b>
                                                </div>

                                                <div class="panel-body" id="asignarGruposForm">
                                                    <div class="col-lg-12" style="position: absolute; top: 50%; left: 50%; bottom: 50%;">
                                                        <div class="load-spinner"></div>
                                                    </div>


                                                    <div class="col-lg-6">


                                                        <div class="form-group">
                                                            <asp:Label ID="Label11" runat="server" CssClass="col-lg-2 control-label" Text="Ciclo" />
                                                            <div class="col-lg-10">
                                                                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                                                                    CssClass="form-control" DataSourceID="SDSciclosescolares"
                                                                    DataTextField="Descripcion" DataValueField="IdCicloEscolar">
                                                                </asp:DropDownList>
                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label12" runat="server" CssClass="col-lg-2 control-label" ><%=Lang_Config.Translate("general","Nivel") %></asp:Label>

                                                            <div class="col-lg-10">
                                                                <asp:DropDownList ID="DDLniveles" runat="server" AutoPostBack="True"
                                                                    CssClass="form-control" DataSourceID="SDSnivel"
                                                                    DataTextField="Descripcion" DataValueField="IdNivel">
                                                                </asp:DropDownList>
                                                            </div>

                                                        </div>



                                                        <div class="form-group">
                                                            <asp:Label ID="Label13" runat="server" CssClass="col-lg-2 control-label" >
                                                                <%=Lang_Config.Translate("general","AreaConocimiento") %>
                                                            </asp:Label>

                                                            <div class="col-lg-10">
                                                                <asp:DropDownList ID="DDLgrados" runat="server" AutoPostBack="True"
                                                                    CssClass="form-control" DataSourceID="SDSgrado"
                                                                    DataTextField="Descripcion" DataValueField="IdGrado">
                                                                </asp:DropDownList>
                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label14" runat="server" CssClass="col-lg-2 control-label">
                                                                <%=Lang_Config.Translate("general","Asignatura") %>
                                                            </asp:Label>

                                                            <div class="col-lg-10">
                                                                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                                                                    CssClass="form-control" DataSourceID="SDSasignaturas" DataTextField="Descripcion"
                                                                    DataValueField="IdAsignatura">
                                                                </asp:DropDownList>
                                                            </div>

                                                        </div>



                                                        <div class="form-group">
                                                            <asp:Label ID="Label15" runat="server" CssClass="col-lg-2 control-label" >
                                                                        
                                                                      <%=Lang_Config.Translate("general","Plantel") %> del grupo
                                                            </asp:Label>

                                                            <div class="col-lg-10">
                                                                <asp:DropDownList ID="DDLplantelgrupo" runat="server" AutoPostBack="True"
                                                                    DataSourceID="SDSplantelesgrupo" DataTextField="Descripcion"
                                                                    DataValueField="IdPlantel" CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label16" runat="server" CssClass="col-lg-2 control-label" Text="Grupo" />

                                                            <div class="col-lg-10">
                                                                <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                                                                    CssClass="form-control" DataSourceID="SDSgrupos" DataTextField="Descripcion"
                                                                    DataValueField="IdGrupo">
                                                                </asp:DropDownList>
                                                            </div>

                                                        </div>

                                                        <div class="col-lg-12">
                                                            <asp:LinkButton ID="Asignar" runat="server" type="submit"
                                                                CssClass="btn btn-labeled btn-palegreen shiny">
                                                                <i class="btn-label fa fa-plus-square"></i> Asignar
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="GVgrupos"
                                                                runat="server"
                                                                AllowPaging="True"
                                                                AutoGenerateColumns="False"
                                                                DataKeyNames="IdProgramacion"
                                                                DataSourceID="SDSgruposasignados"
                                                                Caption="ASIGNATURAS y GRUPOS Asignados al PROFESOR Seleccionado"
                                                                PageSize="5"
                                                                CellPadding="3"
                                                                CssClass="table table-striped table-bordered"
                                                                Height="17px"
                                                                Style="font-size: x-small; text-align: left;">
                                                                <Columns>
                                                                    <asp:BoundField DataField="Grupo" HeaderText="Grupo"
                                                                        SortExpression="Grupo" />
                                                                    <asp:BoundField DataField="Asignatura" HeaderText="Curso"
                                                                        SortExpression="Asignatura" />
                                                                    <asp:BoundField DataField="Grado" HeaderText="Área" SortExpression="Grado" />
                                                                    <asp:BoundField DataField="Plantel" HeaderText="Sucursal"
                                                                        SortExpression="Plantel" />
                                                                    <asp:BoundField DataField="CicloEscolar" HeaderText="CicloEscolar"
                                                                        SortExpression="CicloEscolar" />
                                                                </Columns>
                                                                <PagerTemplate>
                                                                    <ul runat="server" id="Pag" class="pagination">
                                                                    </ul>
                                                                </PagerTemplate>
                                                                <PagerStyle HorizontalAlign="Center" />
                                                                <SelectedRowStyle CssClass="row-selected" />
                                                                <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                            </asp:GridView>
                                                        </div>
                                                        <div class="col-l-6">
                                                            <asp:LinkButton ID="Desasignar" runat="server"  type="submit"
                                                                CssClass="btn btn-labeled btn-darkorange shiny">
                                                            <i class="btn-label fa fa-minus-square"></i>Desasignar
                                                            </asp:LinkButton>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-12">
                                                        <uc1:msgSuccess runat="server" ID="msgSuccess1" />
                                                        <uc1:msgError runat="server" ID="msgError1" />
                                                    </div>

                                                </div>

                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                            </div>

                            <div id="cambiar" class="tab-pane">



                                <div class="form-horizontal">

                                    <div class="panel panel-default">

                                        <div class="panel-heading bg-themeprimary-lg">

                                            <b></b>
                                        </div>

                                        <div class="panel-body">

                                            <asp:UpdatePanel runat="server">
                                                <ContentTemplate>

                                                    <div class="col-lg-6">
                                                        <div class="widget">
                                                            <div class="widget-header bordered-bottom bordered-themefourthcolor">
                                                                <i class="widget-icon fa fa-tags themefourthcolor"></i>
                                                                <span class="widget-caption themefourthcolor"><%=Lang_Config.Translate("profesor","Buscar_coach") %></span>
                                                            </div>
                                                            <!--Widget Header-->
                                                            <div class="widget-body  no-padding">
                                                                <div class="tickets-container">
                                                                    <div class="form-group">
                                                                        <asp:Label runat="server" CssClass="col-lg-2 control-label"
                                                                            Text=" Clave">
                                                                        </asp:Label>
                                                                        <div class="col-lg-10">
                                                                            <asp:TextBox ID="TBClaveBuscar" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">

                                                                        <asp:Label runat="server" CssClass="col-lg-2 control-label"
                                                                            Text=" Nombre "></asp:Label>
                                                                        <div class="col-lg-10">
                                                                            <asp:TextBox ID="TBNombreBuscar" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <asp:Label runat="server" CssClass="col-lg-2 control-label"
                                                                            Text=" Apellidos"></asp:Label>
                                                                        <div class="col-lg-10">
                                                                            <asp:TextBox ID="TBApellidosBuscar" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </div>
                                                                    </div>


                                                                    <div class="form-group">
                                                                        <asp:Label runat="server" CssClass="col-lg-2 control-label"
                                                                            Text=" Login"></asp:Label>
                                                                        <div class="col-lg-10">
                                                                            <asp:TextBox ID="TBLoginBuscar" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <asp:LinkButton ID="BtnBuscar" runat="server" 
                                                                             CssClass="btn btn-labeled btn-palegreen shiny" >
                                                                            <i class="btn-label fa fa-search"></i>Buscar
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                <div class="widget">
                                                                    <div class="widget-header bordered-bottom bordered-themesecondary">
                                                                        <i class="widget-icon fa fa-tags themesecondary"></i>
                                                                        <span class="widget-caption themesecondary">Área para cambio de Password</span>
                                                                    </div>
                                                                    <!--Widget Header-->
                                                                    <div class="widget-body  no-padding">
                                                                        <div class="tickets-container">
                                                                            <div class="form-group">
                                                                                <asp:Label runat="server" CssClass="col-lg-3 control-label" Text="Password"></asp:Label>
                                                                                <div class="col-lg-9">
                                                                                    <asp:TextBox ID="TBpasswordOK" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <span class="col-lg-3 control-label">Email </span></td>
                                                                    <div class="col-lg-9">
                                                                        <asp:TextBox ID="TBemailOK" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <asp:LinkButton ID="BtnActualizar" runat="server" 
                                                                                    CssClass="btn btn-labeled btn-palegreen shiny">
                                                                                     <i class="btn-label fa fa-refresh"></i> Actualizar
                                                                                </asp:LinkButton>
                                                                                <asp:LinkButton ID="BtnGenerar" runat="server" 
                                                                                    CssClass="btn btn-labeled btn-default shiny" >
                                                                                      <i class="btn-label fa fa-plus-square"></i> Generar
                                                                                </asp:LinkButton>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-lg-6">
                                                        <div class="widget">
                                                            <div class="widget-header bordered-bottom bordered-themefourthcolor">
                                                                <i class="widget-icon fa fa-tags themefourthcolor"></i>
                                                                <span class="widget-caption themefourthcolor"><%=Lang_Config.Translate("profesor","Elegir_coach") %></span>
                                                            </div>
                                                            <!--Widget Header-->
                                                            <div class="widget-body  no-padding">
                                                                <div class="tickets-container">

                                                                      <div class="form-group">
                                                            <asp:Label ID="LblProfesor" runat="server"
                                                                Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #000099"></asp:Label>
                                                        </div>
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="GVProfesoresBuscar" runat="server"
                                                                DataSourceID="SDSbuscar"
                                                                 AllowPaging="true"
                                                                AutoGenerateColumns="false"
                                                                PageSize="5"
                                                                DataKeyNames="Clave,Nombre,Apellidos,Plantel,Login,Password,Estatus"
                                                                CellPadding="3"
                                                                CssClass="table table-striped table-bordered"
                                                                Height="17px"
                                                                Style="font-size: x-small; text-align: left;">
                                                                <Columns>

                                                                    <asp:BoundField DataField="IdProfesor" Visible="false" HeaderText="Id_[Profesor]" />
                                                                    <asp:BoundField DataField="Clave" HeaderText="Clave" />
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                                                    <asp:BoundField DataField="Apellidos" HeaderText="Apellidos" />
                                                                    <asp:BoundField DataField="Plantel" HeaderText="Sucursal" />
                                                                    <asp:BoundField DataField="Login" HeaderText="Login" />
                                                                    <asp:BoundField DataField="Password" HeaderText="Password" />
                                                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus" />
                                                                </Columns>
                                                                <PagerTemplate>
                                                                    <ul runat="server" id="Pag" class="pagination">
                                                                    </ul>
                                                                </PagerTemplate>
                                                                <PagerStyle HorizontalAlign="Center" />
                                                                <SelectedRowStyle CssClass="row-selected" />
                                                                <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                            </asp:GridView>
                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <div class="col-lg-12">
                                                             <uc1:msgInfo runat="server" ID="msgInfo" />
                                                        </div>

                                                        </div>
                                                    <div class="col-lg-12">
                                                        <uc1:msgSuccess runat="server" ID="msgSuccess2" />
                                                        <uc1:msgError runat="server" ID="msgError2" />
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>

                            </div>




                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <asp:SqlDataSource ID="SDSbuscar" runat="server"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SDSprofesores" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT P.* ,U.Login,U.Password
FROM Profesor P, Usuario U
WHERE (P.IdPlantel = @IdPlantel) and U.IdUsuario = P.IdUsuario
order by P.Apellidos, P.Nombre">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSinstituciones" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Institucion]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSusuarios" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Usuario]"></asp:SqlDataSource>


    <asp:SqlDataSource ID="SDSplanteles" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion)">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>



    <asp:SqlDataSource ID="SqlDataSource1" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Institucion]"></asp:SqlDataSource>


    <asp:SqlDataSource ID="SqlDataSource2" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SqlDataSource3" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Profesor] WHERE (([IdPlantel] = @IdPlantel) AND ([Estatus] = @Estatus)) ORDER BY [Apellidos], [Nombre]">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                PropertyName="SelectedValue" Type="Int32" />
            <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [CicloEscolar] ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSasignaturas" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT IdAsignatura, Descripcion
FROM Asignatura A
where IdGrado = @IdGrado
ORDER BY Descripcion">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLgrados" Name="IdGrado"
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSgrupos" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * 
FROM Grupo G
JOIN Asignatura A
ON G.IdGrado = A.IdGrado and A.IdAsignatura = @IdAsignatura
and G.IdPlantel = @IdPlantel and G.IdCicloEscolar = @IdCicloEscolar">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="DDLplantelgrupo" Name="IdPlantel"
                PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSnivel" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT IdNivel, Descripcion 
FROM Nivel WHERE Estatus = 'Activo'
ORDER BY Descripcion"></asp:SqlDataSource>


    <asp:SqlDataSource ID="SDSgruposasignados" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.IdProgramacion, G.IdGrupo, G.Descripcion Grupo, A.Descripcion Asignatura, Gr.Descripcion Grado, Pl.Descripcion Plantel,C.Descripcion CicloEscolar
from Grupo G,Programacion P, Plantel Pl, CicloEscolar C, Asignatura A, Grado Gr
where P.IdProfesor = @IdProfesor and G.IdGrupo = P.IdGrupo and C.IdCicloEscolar = P.IdCicloEscolar and Pl.IdPlantel = G.IdPlantel and A.IdAsignatura = P.IdAsignatura and Gr.IdGrado = G.IdGrado and Gr.Estatus = 'Activo'
order by C.IdCicloEscolar, Pl.Descripcion, A.Descripcion, Gr.Descripcion, G.Descripcion">
        <SelectParameters>
            <asp:ControlParameter ControlID="GVprofesores" Name="IdProfesor"
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>


    <asp:SqlDataSource ID="SDSprogramacion" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Programacion]"></asp:SqlDataSource>


    <asp:SqlDataSource ID="SDSgrado" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLniveles" Name="IdNivel"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSplantelesgrupo" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" runat="Server">
</asp:Content>

