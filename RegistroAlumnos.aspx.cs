﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security; //este contine la funcion Roles

//Agregue las siguientes importaciones
//using System.Data;
//using System.Configuration;
//using System.Collections;
//using System.Web.UI.WebControls.WebParts;
//using System.Web.UI.HtmlControls;

public partial class RegistroAlumnos : System.Web.UI.Page
{
    protected void CreateUserWizard1_CreatedUser1(object sender, EventArgs e)
    {
        try
        {
        // Crea un profile vacío para el usuario recien creado
        ProfileCommon p = (ProfileCommon)ProfileCommon.Create(CreateUserWizard1.UserName, true);

        //Propaga algunas propiedades en off del usuario creado por el wizard
        p.IdEntidad = Int32.Parse(((DropDownList)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Alumno")).SelectedValue);
        p.DDLtipo = ((DropDownList)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("DDLtipo")).SelectedValue;

        //Guarda el profile (debe ser hecho desde que nosotros explicitamente lo creamos)
        p.Save();
        }
        catch (Exception ex)
        {
            Lblerror.Text = ex.Message;
        }
    }

      //El evento "activate" se dispara cuando el usuario presiona "Next" en el CreateUserWizard
      public void AssignUserToRoles_Activate(object sender, EventArgs e)
      {
          try
          {
          //Databind list of roles in the role manager system to a listbox in the wizard
          AvailableRoles.DataSource = Roles.GetAllRoles();
          AvailableRoles.DataBind();
          }
          catch (Exception ex)
          {
              Lblerror.Text = ex.Message;
          }
      }

      // Deactivate event fires when user hits "next" in the CreateUserWizard 
      public void AssignUserToRoles_Deactivate(object sender, EventArgs e)
      {
          try
          {
          // Add user to all selected roles from the roles listbox
       //   for (int i = 0; i < AvailableRoles.Items.Count; i++)
       //   {
       //       if (AvailableRoles.Items[i].Selected == true)
                  //0=Alumno, 1=Capturista, 2=Coordinador, 3=Profesor,4=Superadministrador
                  //**Agrego el usuario al RoleGroup Alumno
                  Roles.AddUserToRole(CreateUserWizard1.UserName, AvailableRoles.Items[0].Value);
                  
                  //**Agrego el login del usuario creado a la tabla alumno
                  SDSusuarios.InsertCommand = "SET dateformat dmy; INSERT INTO Usuario (Login,PerfilASP,Email) VALUES ('" + CreateUserWizard1.UserName + "','" + ((DropDownList)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("DDLtipo")).SelectedValue + "','" + CreateUserWizard1.Email +  "')";
                  SDSusuarios.Insert();
                  //**Amarro el usuario recien creado con el alumno
                  SDSalumnos.UpdateCommand = "SET dateformat dmy; UPDATE Alumno set IdUsuario = (select IdUsuario from Usuario where login = '" + CreateUserWizard1.UserName + "') where IdAlumno = " + ((DropDownList)CreateUserWizard1.CreateUserStep.ContentTemplateContainer.FindControl("Alumno")).SelectedValue;
                  SDSalumnos.Update();
          }
          catch (Exception ex)
          {
              Lblerror.Text = ex.Message;
          }
       //   }

      }
}
