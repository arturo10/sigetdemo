﻿<%@ Application Language="C#" %>

<script runat="server">

        void Application_Start(object sender, EventArgs e)
        {
            // Código que se ejecuta al iniciarse la aplicación

            // solución bruta
            //ValidationSettings.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None

                    // solución elegante
            RouteTable.Routes.MapHttpRoute(
            name: "DefaultApi",
            routeTemplate: "api/{controller}/{id}",
            defaults: new { id = System.Web.Http.RouteParameter.Optional }
            );

            var formatters = GlobalConfiguration.Configuration.Formatters;
            formatters.Remove(formatters.XmlFormatter);
            
            var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            json.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
            json.SerializerSettings.PreserveReferencesHandling =
             Newtonsoft.Json.PreserveReferencesHandling.All;

            ScriptResourceDefinition myScriptResDef = new ScriptResourceDefinition();
            myScriptResDef.Path = "~/scripts/" + Siget.UserInterface.Include.jquery_file;
            myScriptResDef.DebugPath = "~/scripts/" + Siget.UserInterface.Include.jquery_file;
            myScriptResDef.CdnPath = "~/scripts/" + Siget.UserInterface.Include.jquery_file;
            myScriptResDef.CdnDebugPath = "~/scripts/" + Siget.UserInterface.Include.jquery_file;
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery", null, myScriptResDef);

            // *****************************************************************************************
            // inicializa configuración
            // *****************************************************************************************
            Siget.Config.Global_Import.getConfig();

            
            
            // *****************************************************************************************
            // inicializa configuración de lenguaje
            // *****************************************************************************************
            Siget.Lang.Lang_Config.InicializaTodosLosLenguajes();

            // la siguiente línea debe eliminarse al completar la propagación de la ui 5.1.0 a los otros perfiles
            Siget.Config.Etiqueta.importa_Etiquetas_OLD();

            // *****************************************************************************************
            // inicializa configuración de colores
            // *****************************************************************************************
            Siget.Config.Color_Import.configuraColores();
            Siget.App_Start.BundleConfig.RegisterBundles(BundleTable.Bundles);
            
            Siget.App_Start.RouteConfig.RegisterRoutes(RouteTable.Routes);

            Siget.Lang.Lang_Config.ImportAllSettings();
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Código que se ejecuta al cerrarse la aplicación

        }

        
        void Application_Error(object sender, EventArgs e)
        {
           
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(Server.GetLastError());
            Exception ex = Server.GetLastError();
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(Server.GetLastError());


            HttpException httpEx = ex as HttpException;

            if (httpEx != null && httpEx.GetHttpCode() == 404)
            {
                Response.Redirect("~/Pages/Error404.html");
            }
            else
            {
                Response.Redirect("~/Pages/Error500.html");
            }
           
        }

        void Session_Start(object sender, EventArgs e)
        {
           

        }

        void Session_End(object sender, EventArgs e)
        {

           
        }

</script>
