﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Siget.DataAccess;

/// <summary>
/// Descripción breve de PlantelBI
/// </summary>
/// namespac
namespace Siget.BusinessLogic
{
    public class PlantelBI
    {

        /// <summary>
        /// Obtiene una dataset para DropDownLists con una lista de Planteles 
        /// asignados al operador indicado, donde el primer registro es una 
        /// instrucción "Elija un elemento"
        /// </summary>
        public DataSet FormatoTextoValor_IdOperador_IdInstitucion(string idOperador, string idInstitucion)
        {
            // obten la lista de datos reales
            DataSet dst = (new PlantelDa()).ObtenTextoValor_IdOperador_IdInstitucion(idOperador, idInstitucion);

            // inserta una nueva fila instrucción al inicio del dataset
            DataTable dt = dst.Tables[0];
            DataRow defaultRow = dt.NewRow();
            defaultRow[0] = Lang.FileSystem.General.ddl_elija_un_elemento[System.Web.HttpContext.Current.Session["Usuario_Idioma"].ToString()];
            defaultRow[1] = "0";
            dt.Rows.InsertAt(defaultRow, 0);

            return dst;
        }


    }
}