﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Data;
using System.Diagnostics;
using Siget.DataAccess;
using Siget;
/// <summary>
/// Descripción breve de InstitucionBI
/// </summary>
/// 
namespace Siget.BusinessLogic
{
    public class InstitucionBI
    {
       /// <summary>
		/// Obtiene una dataset para DropDownLists con una lista de Instituciones
		/// con planteles asignados al operador indicado, donde el primer registro 
		/// es una instrucción "Elija un elemento"
		/// </summary>
		public DataSet FormatoTextoValor_IdOperador(string idOperador)
		{
			// obten la lista de datos reales
			DataSet dst = (new InstitucionDa()).ObtenTextoValor_IdOperador(idOperador);

			// inserta una nueva fila instrucción al inicio del dataset
			DataTable dt = dst.Tables[0];
			DataRow defaultRow = dt.NewRow();
			defaultRow[0] = Siget.Lang.FileSystem.General.ddl_elija_un_elemento[System.Web.HttpContext.Current.Session["Usuario_Idioma"].ToString()];
			defaultRow[1] = "0";
			dt.Rows.InsertAt(defaultRow, 0);

			return dst;
		}

		/// <summary>
		/// Obtiene una dataset para DropDownLists con una lista de Instituciones
		/// con planteles asignados al operador indicado, donde el primer registro 
		/// es una instrucción "Elija un elemento"
		/// </summary>
		public DataSet FormatoTextoValor_IdCoordinador(string idCoordinador)
		{
			// obten la lista de datos reales
			DataSet dst = (new InstitucionDa()).ObtenTextoValor_IdCoordinador(idCoordinador);

			// inserta una nueva fila instrucción al inicio del dataset
			DataTable dt = dst.Tables[0];
			DataRow defaultRow = dt.NewRow();
			defaultRow[0] = Lang.FileSystem.General.ddl_elija_un_elemento[System.Web.HttpContext.Current.Session["Usuario_Idioma"].ToString()];
			defaultRow[1] = "0";
			dt.Rows.InsertAt(defaultRow, 0);

			return dst;
		}

	}
}