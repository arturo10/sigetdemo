﻿using System;

// 
// EvaluacionRepetidaBl.vb

using Siget.TransferObjects;
using Siget.DataAccess;

namespace Siget.BusinessLogic
{

  public class EvaluacionRepetidaBl
  {

    /// <summary>
    /// Inserta un la evaluación a repetir en EvaluacionRepetida, 
    /// y elimina las respuestas y la evaluación para que el alumno 
    /// pueda realizarla nuevamente.
    /// </summary>
    /// <param name="evTerminada"></param>
    /// <param name="modifico"></param>

    public void RepiteEvaluacion(ref EvaluacionTerminadaTo evTerminada, ref string modifico)
    {
      try
      {
        byte TotalReactivos = 0;
        byte ReactivosContestados = 0;
        byte PuntosPosibles = 0;
        byte PuntosAcertados = 0;

        if (evTerminada.TotalReactivos.HasValue)
        {
          TotalReactivos = (Byte)evTerminada.TotalReactivos;
        }
        else
        {
          TotalReactivos = 0;
        }

        if (evTerminada.ReactivosContestados.HasValue)
        {
          ReactivosContestados = (Byte)evTerminada.ReactivosContestados;
        }
        else
        {
          ReactivosContestados = 0;
        }

        if (evTerminada.PuntosPosibles.HasValue)
        {
          PuntosPosibles = (Byte)evTerminada.PuntosPosibles;
        }
        else
        {
          PuntosPosibles = 0;
        }

        if (evTerminada.PuntosAcertados.HasValue)
        {
          PuntosAcertados = (Byte)evTerminada.PuntosAcertados;
        }
        else
        {
          PuntosAcertados = 0;
        }

        ((EvaluacionRepetidaDa)new EvaluacionRepetidaDa()).Inserta(ref evTerminada, TotalReactivos, ReactivosContestados, PuntosPosibles, PuntosAcertados, modifico);

        ((EvaluacionTerminadaDa)new EvaluacionTerminadaDa()).Elimina(evTerminada.IdAlumno.ToString(), evTerminada.IdEvaluacion.ToString(), evTerminada.IdCicloEscolar.ToString());

        ((RespuestaAbiertaDa)new RespuestaAbiertaDa()).Elimina(evTerminada.IdAlumno.ToString(), evTerminada.IdEvaluacion.ToString(), evTerminada.IdCicloEscolar.ToString());

        ((RespuestaDa)new RespuestaDa()).Elimina(evTerminada.IdAlumno.ToString(), evTerminada.IdEvaluacion.ToString(), evTerminada.IdCicloEscolar.ToString());

        //NOTA: Será necesario borrar material cargado en caso de que se trate de una actividad de subir documento?
        //Por el momento filtré que solo aparezcan actividades de opcion múltiple

      }
      catch (Exception ex)
      {
        Utils.LogManager.ExceptionLog_InsertEntry(ex);
        throw ex;
      }
    }

  }

}