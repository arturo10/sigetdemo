﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang
{

  public class Etiqueta
  {

    // el nombre del esquema bajo el que trabaja el filesystem

    public static Dictionary<string, string> Sistema_Corto = new Dictionary<string, string>();

    // el nombre completo del proyecto para el pie de página de la página principal
    public static Dictionary<string, string> Sistema_Largo = new Dictionary<string, string>();
    // el nombre de la empresa para el pie de página de la página principal
    public static Dictionary<string, string> Nombre_Empresa = new Dictionary<string, string>();

    // Cada sustantivo a Config.Etiqueta.aaaaaaa tiene:
    //   versión singular
    //   versión plural
    //   artículo determinado singular (el, la)
    //   artículo determinado plural (los, las)
    //   artículo indeterminado singular (un, una)
    //   artículo indeterminado plural (unos, unas)
    //   letra de terminación de verbos (seleccionadas, elegidas, capturados, ...)

    public static Dictionary<string, string> Institucion = new Dictionary<string, string>();
    public static Dictionary<string, string> Instituciones = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Institucion = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Instituciones = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Institucion = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Instituciones = new Dictionary<string, string>();

    public static Dictionary<string, string> Letra_Institucion = new Dictionary<string, string>();
    public static Dictionary<string, string> Ciclo = new Dictionary<string, string>();
    public static Dictionary<string, string> Ciclos = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Ciclo = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Ciclos = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Ciclo = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Ciclos = new Dictionary<string, string>();

    public static Dictionary<string, string> Letra_Ciclo = new Dictionary<string, string>();

    public static Dictionary<string, string> Nivel = new Dictionary<string, string>();
    public static Dictionary<string, string> Niveles = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Nivel = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Niveles = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Nivel = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Niveles = new Dictionary<string, string>();

    public static Dictionary<string, string> Letra_Nivel = new Dictionary<string, string>();

    public static Dictionary<string, string> Grado = new Dictionary<string, string>();
    public static Dictionary<string, string> Grados = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Grado = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Grados = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Grado = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Grados = new Dictionary<string, string>();

    public static Dictionary<string, string> Letra_Grado = new Dictionary<string, string>();

    public static Dictionary<string, string> Asignatura = new Dictionary<string, string>();
    public static Dictionary<string, string> Asignaturas = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Asignatura = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Asignaturas = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Asignatura = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Asignaturas = new Dictionary<string, string>();

    public static Dictionary<string, string> Letra_Asignatura = new Dictionary<string, string>();

    public static Dictionary<string, string> Plantel = new Dictionary<string, string>();
    public static Dictionary<string, string> Planteles = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Plantel = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Planteles = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Plantel = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Planteles = new Dictionary<string, string>();

    public static Dictionary<string, string> Letra_Plantel = new Dictionary<string, string>();

    public static Dictionary<string, string> Grupo = new Dictionary<string, string>();
    public static Dictionary<string, string> Grupos = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Grupo = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Grupos = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Grupo = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Grupos = new Dictionary<string, string>();

    public static Dictionary<string, string> Letra_Grupo = new Dictionary<string, string>();

    public static Dictionary<string, string> Alumno = new Dictionary<string, string>();
    public static Dictionary<string, string> Alumnos = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Alumno = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Alumnos = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Alumno = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Alumnos = new Dictionary<string, string>();

    public static Dictionary<string, string> Letra_Alumno = new Dictionary<string, string>();

    public static Dictionary<string, string> Profesor = new Dictionary<string, string>();
    public static Dictionary<string, string> Profesores = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Profesor = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Profesores = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Profesor = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Profesores = new Dictionary<string, string>();

    public static Dictionary<string, string> Letra_Profesor = new Dictionary<string, string>();

    public static Dictionary<string, string> Coordinador = new Dictionary<string, string>();
    public static Dictionary<string, string> Coordinadores = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Coordinador = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Coordinadores = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Coordinador = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Coordinadores = new Dictionary<string, string>();

    public static Dictionary<string, string> Letra_Coordinador = new Dictionary<string, string>();

    public static Dictionary<string, string> Equipo = new Dictionary<string, string>();
    public static Dictionary<string, string> Equipos = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Equipo = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Equipos = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Equipo = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Equipos = new Dictionary<string, string>();

    public static Dictionary<string, string> Letra_Equipo = new Dictionary<string, string>();

    public static Dictionary<string, string> SubEquipo = new Dictionary<string, string>();
    public static Dictionary<string, string> SubEquipos = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_SubEquipo = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_SubEquipos = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_SubEquipo = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_SubEquipos = new Dictionary<string, string>();

    public static Dictionary<string, string> Letra_SubEquipo = new Dictionary<string, string>();

    public static Dictionary<string, string> Matricula = new Dictionary<string, string>();
    public static Dictionary<string, string> Matriculas = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Matricula = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Matriculas = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Matricula = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Matriculas = new Dictionary<string, string>();

    public static Dictionary<string, string> Letra_Matricula = new Dictionary<string, string>();

    // actualmente ésta sólo se necesita en el menú de actividades pendientes del alumno
    public static Dictionary<string, string> Calificacion = new Dictionary<string, string>();
    public static Dictionary<string, string> Calificaciones = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Calificacion = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtDet_Calificaciones = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Calificacion = new Dictionary<string, string>();
    public static Dictionary<string, string> ArtInd_Calificaciones = new Dictionary<string, string>();

    public static Dictionary<string, string> Letra_Calificacion = new Dictionary<string, string>();

  }

}