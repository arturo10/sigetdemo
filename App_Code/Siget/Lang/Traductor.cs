﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// Traductor.vb

using System.Web;

namespace Siget.Lang
{

  /// <summary>
  /// Contiene funciones para traducir texto.
  /// </summary>
  public class Traductor
  {

    public static string TraducetipoActividad(string tipo)
    {
      if (tipo == "Aprendizaje")
      {
        return Siget.Lang.FileSystem.General.TIPO_ACTIVIDAD_APRENDIZAJE[HttpContext.Current.Session["Usuario_Idioma"].ToString()];
      }
      else if (tipo == "Evaluación")
      {
        return Siget.Lang.FileSystem.General.TIPO_ACTIVIDAD_EVALUACION[HttpContext.Current.Session["Usuario_Idioma"].ToString()];
      }
      else if (tipo == "Material")
      {
        return Siget.Lang.FileSystem.General.TIPO_ACTIVIDAD_MATERIAL[HttpContext.Current.Session["Usuario_Idioma"].ToString()];
      }
      else if (tipo == "Reactivos")
      {
        return Siget.Lang.FileSystem.General.TIPO_ACTIVIDAD_REACTIVOS[HttpContext.Current.Session["Usuario_Idioma"].ToString()];
      }
      else if (tipo == "Documento")
      {
        return Siget.Lang.FileSystem.General.TIPO_ACTIVIDAD_DOCUMENTO[HttpContext.Current.Session["Usuario_Idioma"].ToString()];
      }
      else if (tipo == "Examen")
      {
        return Siget.Lang.FileSystem.General.TIPO_ACTIVIDAD_EXAMEN[HttpContext.Current.Session["Usuario_Idioma"].ToString()];
      }
      else if (tipo == "Tarea")
      {
        return Siget.Lang.FileSystem.General.TIPO_ACTIVIDAD_TAREA[HttpContext.Current.Session["Usuario_Idioma"].ToString()];
      }
      else
      {
        return string.Empty;
      }
    }

  }

}