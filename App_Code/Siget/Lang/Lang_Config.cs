﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Xml;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;



namespace Siget.Lang
{

  public class Lang_Config
  {
     public static JArray mapTranslateSite;

      public static void ImportAllSettings()
      {
          string route = "~/Resources/i18n/" + Siget.Config.Global.Idioma_Administrador + ".json";
          JObject root = JsonConvert.DeserializeObject<JObject>(File.ReadAllText(HttpContext.Current.Server.MapPath(route)));
          string model= Siget.Config.Global.Modelo_Sistema==0?"Escuela":"Empresa";
          mapTranslateSite=(JArray)root[model];

      }

      public static string Translate(string contextPage,string Key)
      {
          try
          {
              JToken pageEnumerable = mapTranslateSite.SingleOrDefault(r => (string)r["title"] == contextPage);
              return (string)pageEnumerable[Key];
          }
          catch (Exception ex)
          {
              Utils.LogManager.ExceptionLog_InsertEntry(ex);
              return "";
          }
         
      }

    /// <summary>
    /// Prepara todos los lenguajes de la aplicación.
    /// </summary>
    public static void InicializaTodosLosLenguajes()
    {
      foreach (string lenguaje in Siget.Config.Global.Lenguajes)
      {
        try
        {
          InicializaLenguaje(lenguaje);
        }
        catch (Exception ex)
        {
          Utils.LogManager.ExceptionLog_InsertEntry(ex);
        }
      }
    }


    public static void InicializaLenguaje(string lenguaje)
    {
      // utilizo la abreviación del lenguaje para acceder a su carpeta respectiva 
      // (debe estar nombrada de la misma manera que la abreviación del lenguaje)

      // este método debe llamarse antes que los demás,
      // puesto que las etiquetas se utilizan en las demás oraciones
      importa_Etiquetas(lenguaje);
      importa_General(lenguaje);

      importa_Home(lenguaje);
      importa_Creditos(lenguaje);
      importa_DataAccess(lenguaje);

      // ************************************
      // perfil alumno
      Lang_Config_Alumno.importa_Alumno_Actividad(lenguaje);
      Lang_Config_Alumno.importa_Alumno_Biblioteca(lenguaje);
      Lang_Config_Alumno.importa_CambiarPassword(lenguaje);
      Lang_Config_Alumno.importa_Alumno_Contestar(lenguaje);
      Lang_Config_Alumno.importa_Alumno_ContestarDemo(lenguaje);
      Lang_Config_Alumno.importa_Alumno_Corregir(lenguaje);
      Lang_Config_Alumno.importa_Alumno_Cursos(lenguaje);
      Lang_Config_Alumno.importa_Alumno_CursosInscritos(lenguaje);
      Lang_Config_Alumno.importa_Alumno_CursosInscribir(lenguaje);
      Lang_Config_Alumno.importa_Alumno_CursosBorrar(lenguaje);
      Lang_Config_Alumno.importa_Alumno_Entrada(lenguaje);
      Lang_Config_Alumno.importa_Alumno_Enviar(lenguaje);
      Lang_Config_Alumno.importa_Alumno_ListaReactivos(lenguaje);
      Lang_Config_Alumno.importa_Alumno_Master_AgregarCursos(lenguaje);
      Lang_Config_Alumno.importa_Alumno_Master_Comunicacion(lenguaje);
      Lang_Config_Alumno.importa_Alumno_Master_Principal(lenguaje);
      Lang_Config_Alumno.importa_Alumno_Mensaje(lenguaje);
      Lang_Config_Alumno.importa_Alumno_MisDatos(lenguaje);
      Lang_Config_Alumno.importa_Alumno_Procesa(lenguaje);
      Lang_Config_Alumno.importa_Alumno_Procesa2(lenguaje);
      Lang_Config_Alumno.importa_Alumno_ProcesaDemo(lenguaje);
      Lang_Config_Alumno.importa_Alumno_Responder(lenguaje);
      Lang_Config_Alumno.importa_Alumno_Salida(lenguaje);


    }

    #region "procesamiento de etiquetas"

    /// <summary>
    /// Al leer un registro de XML, se incluyen los saltos de línea englobados por cada tag; 
    /// ésta función se asegura que el registro leído tenga ambos extremos libres de espacios y saltos de línea.
    /// </summary>
    /// <param name="input">El String a procesar</param>
    /// <returns>Un String limpio</returns>
    /// <remarks></remarks>
    protected static string clean(string input)
    {
      return input.Replace(Environment.NewLine, "").Trim();
    }

    /// <summary>
    /// Remplaza las etiquetas definidas en Etiqueta dentro del String recibido.
    /// 
    /// Las etiquetas deben estar entre pipes y asteriscos, y llamarse de la misma manera 
    /// en que se definieron en Etiqueta, por ejemplo |*ARTDET_INSTITUCION*|
    /// 
    /// También asegura que las cadenas traducidas tengan un formato limpio, 
    /// a través del método clean(String) de esa clase.
    /// </summary>
    /// <param name="input">El String a procesar, donde se remplazarán las etiquetas encontradas.</param>
    /// <returns>Un nuevo String con las etiquetas aplicadas.</returns>
    /// <remarks></remarks>
    public static string parseTags(string input, string lenguaje)
    {
      if (input.Contains("|*"))
      {
        if (input.Contains("|*NOMBRE_FILESYSTEM*|"))
          input = input.Replace("|*NOMBRE_FILESYSTEM*|", Config.Global.NOMBRE_FILESYSTEM);

        if (input.Contains("|*SISTEMA_CORTO*|"))
          input = input.Replace("|*SISTEMA_CORTO*|", Etiqueta.Sistema_Corto[lenguaje]);

        if (input.Contains("|*INSTITUCION*|"))
          input = input.Replace("|*INSTITUCION*|", Etiqueta.Institucion[lenguaje]);
        if (input.Contains("|*INSTITUCIONES*|"))
          input = input.Replace("|*INSTITUCIONES*|", Etiqueta.Instituciones[lenguaje]);
        if (input.Contains("|*ARTDET_INSTITUCION*|"))
          input = input.Replace("|*ARTDET_INSTITUCION*|", Etiqueta.ArtDet_Institucion[lenguaje]);
        if (input.Contains("|*ARTDET_INSTITUCIONES*|"))
          input = input.Replace("|*ARTDET_INSTITUCIONES*|", Etiqueta.ArtDet_Instituciones[lenguaje]);
        if (input.Contains("|*ARTIND_INSTITUCION*|"))
          input = input.Replace("|*ARTIND_INSTITUCION*|", Etiqueta.ArtInd_Institucion[lenguaje]);
        if (input.Contains("|*ARTIND_INSTITUCIONES*|"))
          input = input.Replace("|*ARTIND_INSTITUCIONES*|", Etiqueta.ArtInd_Instituciones[lenguaje]);
        if (input.Contains("|*LETRA_INSTITUCION*|"))
          input = input.Replace("|*LETRA_INSTITUCION*|", Etiqueta.Letra_Institucion[lenguaje]);

        if (input.Contains("|*CICLO*|"))
          input = input.Replace("|*CICLO*|", Etiqueta.Ciclo[lenguaje]);
        if (input.Contains("|*CICLOS*|"))
          input = input.Replace("|*CICLOS*|", Etiqueta.Ciclos[lenguaje]);
        if (input.Contains("|*ARTDET_CICLO*|"))
          input = input.Replace("|*ARTDET_CICLO*|", Etiqueta.ArtDet_Ciclo[lenguaje]);
        if (input.Contains("|*ARTDET_CICLOS*|"))
          input = input.Replace("|*ARTDET_CICLOS*|", Etiqueta.ArtDet_Ciclos[lenguaje]);
        if (input.Contains("|*ARTIND_CICLO*|"))
          input = input.Replace("|*ARTIND_CICLO*|", Etiqueta.ArtInd_Ciclo[lenguaje]);
        if (input.Contains("|*ARTIND_CICLOS*|"))
          input = input.Replace("|*ARTIND_CICLOS*|", Etiqueta.ArtInd_Ciclos[lenguaje]);
        if (input.Contains("|*LETRA_CICLO*|"))
          input = input.Replace("|*LETRA_CICLO*|", Etiqueta.Letra_Ciclo[lenguaje]);

        if (input.Contains("|*NIVEL*|"))
          input = input.Replace("|*NIVEL*|", Etiqueta.Nivel[lenguaje]);
        if (input.Contains("|*NIVELES*|"))
          input = input.Replace("|*NIVELES*|", Etiqueta.Niveles[lenguaje]);
        if (input.Contains("|*ARTDET_NIVEL*|"))
          input = input.Replace("|*ARTDET_NIVEL*|", Etiqueta.ArtDet_Nivel[lenguaje]);
        if (input.Contains("|*ARTDET_NIVELES*|"))
          input = input.Replace("|*ARTDET_NIVELES*|", Etiqueta.ArtDet_Niveles[lenguaje]);
        if (input.Contains("|*ARTIND_NIVEL*|"))
          input = input.Replace("|*ARTIND_NIVEL*|", Etiqueta.ArtInd_Nivel[lenguaje]);
        if (input.Contains("|*ARTIND_NIVELES*|"))
          input = input.Replace("|*ARTIND_NIVELES*|", Etiqueta.ArtInd_Niveles[lenguaje]);
        if (input.Contains("|*LETRA_NIVEL*|"))
          input = input.Replace("|*LETRA_NIVEL*|", Etiqueta.Letra_Nivel[lenguaje]);

        if (input.Contains("|*GRADO*|"))
          input = input.Replace("|*GRADO*|", Etiqueta.Grado[lenguaje]);
        if (input.Contains("|*GRADOS*|"))
          input = input.Replace("|*GRADOS*|", Etiqueta.Grados[lenguaje]);
        if (input.Contains("|*ARTDET_GRADO*|"))
          input = input.Replace("|*ARTDET_GRADO*|", Etiqueta.ArtDet_Grado[lenguaje]);
        if (input.Contains("|*ARTDET_GRADOS*|"))
          input = input.Replace("|*ARTDET_GRADOS*|", Etiqueta.ArtDet_Grados[lenguaje]);
        if (input.Contains("|*ARTIND_GRADO*|"))
          input = input.Replace("|*ARTIND_GRADO*|", Etiqueta.ArtInd_Grado[lenguaje]);
        if (input.Contains("|*ARTIND_GRADOS*|"))
          input = input.Replace("|*ARTIND_GRADOS*|", Etiqueta.ArtInd_Grados[lenguaje]);
        if (input.Contains("|*LETRA_GRADO*|"))
          input = input.Replace("|*LETRA_GRADO*|", Etiqueta.Letra_Grado[lenguaje]);

        if (input.Contains("|*ASIGNATURA*|"))
          input = input.Replace("|*ASIGNATURA*|", Etiqueta.Asignatura[lenguaje]);
        if (input.Contains("|*ASIGNATURAS*|"))
          input = input.Replace("|*ASIGNATURAS*|", Etiqueta.Asignaturas[lenguaje]);
        if (input.Contains("|*ARTDET_ASIGNATURA*|"))
          input = input.Replace("|*ARTDET_ASIGNATURA*|", Etiqueta.ArtDet_Asignatura[lenguaje]);
        if (input.Contains("|*ARTDET_ASIGNATURAS*|"))
          input = input.Replace("|*ARTDET_ASIGNATURAS*|", Etiqueta.ArtDet_Asignaturas[lenguaje]);
        if (input.Contains("|*ARTIND_ASIGNATURA*|"))
          input = input.Replace("|*ARTIND_ASIGNATURA*|", Etiqueta.ArtInd_Asignatura[lenguaje]);
        if (input.Contains("|*ARTIND_ASIGNATURAS*|"))
          input = input.Replace("|*ARTIND_ASIGNATURAS*|", Etiqueta.ArtInd_Asignaturas[lenguaje]);
        if (input.Contains("|*LETRA_ASIGNATURA*|"))
          input = input.Replace("|*LETRA_ASIGNATURA*|", Etiqueta.Letra_Asignatura[lenguaje]);

        if (input.Contains("|*PLANTEL*|"))
          input = input.Replace("|*PLANTEL*|", Etiqueta.Plantel[lenguaje]);
        if (input.Contains("|*PLANTELES*|"))
          input = input.Replace("|*PLANTELES*|", Etiqueta.Planteles[lenguaje]);
        if (input.Contains("|*ARTDET_PLANTEL*|"))
          input = input.Replace("|*ARTDET_PLANTEL*|", Etiqueta.ArtDet_Plantel[lenguaje]);
        if (input.Contains("|*ARTDET_PLANTELES*|"))
          input = input.Replace("|*ARTDET_PLANTELES*|", Etiqueta.ArtDet_Planteles[lenguaje]);
        if (input.Contains("|*ARTIND_PLANTEL*|"))
          input = input.Replace("|*ARTIND_PLANTEL*|", Etiqueta.ArtInd_Plantel[lenguaje]);
        if (input.Contains("|*ARTIND_PLANTELES*|"))
          input = input.Replace("|*ARTIND_PLANTELES*|", Etiqueta.ArtInd_Planteles[lenguaje]);
        if (input.Contains("|*LETRA_PLANTEL*|"))
          input = input.Replace("|*LETRA_PLANTEL*|", Etiqueta.Letra_Plantel[lenguaje]);

        if (input.Contains("|*GRUPO*|"))
          input = input.Replace("|*GRUPO*|", Etiqueta.Grupo[lenguaje]);
        if (input.Contains("|*GRUPOS*|"))
          input = input.Replace("|*GRUPOS*|", Etiqueta.Grupos[lenguaje]);
        if (input.Contains("|*ARTDET_GRUPO*|"))
          input = input.Replace("|*ARTDET_GRUPO*|", Etiqueta.ArtDet_Grupo[lenguaje]);
        if (input.Contains("|*ARTDET_GRUPOS*|"))
          input = input.Replace("|*ARTDET_GRUPOS*|", Etiqueta.ArtDet_Grupos[lenguaje]);
        if (input.Contains("|*ARTIND_GRUPO*|"))
          input = input.Replace("|*ARTIND_GRUPO*|", Etiqueta.ArtInd_Grupo[lenguaje]);
        if (input.Contains("|*ARTIND_GRUPOS*|"))
          input = input.Replace("|*ARTIND_GRUPOS*|", Etiqueta.ArtInd_Grupos[lenguaje]);
        if (input.Contains("|*LETRA_GRUPO*|"))
          input = input.Replace("|*LETRA_GRUPO*|", Etiqueta.Letra_Grupo[lenguaje]);

        if (input.Contains("|*ALUMNO*|"))
          input = input.Replace("|*ALUMNO*|", Etiqueta.Alumno[lenguaje]);
        if (input.Contains("|*ALUMNOS*|"))
          input = input.Replace("|*ALUMNOS*|", Etiqueta.Alumnos[lenguaje]);
        if (input.Contains("|*ARTDET_ALUMNO*|"))
          input = input.Replace("|*ARTDET_ALUMNO*|", Etiqueta.ArtDet_Alumno[lenguaje]);
        if (input.Contains("|*ARTDET_ALUMNOS*|"))
          input = input.Replace("|*ARTDET_ALUMNOS*|", Etiqueta.ArtDet_Alumnos[lenguaje]);
        if (input.Contains("|*ARTIND_ALUMNO*|"))
          input = input.Replace("|*ARTIND_ALUMNO*|", Etiqueta.ArtInd_Alumno[lenguaje]);
        if (input.Contains("|*ARTIND_ALUMNOS*|"))
          input = input.Replace("|*ARTIND_ALUMNOS*|", Etiqueta.ArtInd_Alumnos[lenguaje]);
        if (input.Contains("|*LETRA_ALUMNO*|"))
          input = input.Replace("|*LETRA_ALUMNO*|", Etiqueta.Letra_Alumno[lenguaje]);


        if (input.Contains("|*PROFESOR*|"))
          input = input.Replace("|*PROFESOR*|", Etiqueta.Profesor[lenguaje]);
        if (input.Contains("|*PROFESORES*|"))
          input = input.Replace("|*PROFESORES*|", Etiqueta.Profesores[lenguaje]);
        if (input.Contains("|*ARTDET_PROFESOR*|"))
          input = input.Replace("|*ARTDET_PROFESOR*|", Etiqueta.ArtDet_Profesor[lenguaje]);
        if (input.Contains("|*ARTDET_PROFESORES*|"))
          input = input.Replace("|*ARTDET_PROFESORES*|", Etiqueta.ArtDet_Profesores[lenguaje]);
        if (input.Contains("|*ARTIND_PROFESOR*|"))
          input = input.Replace("|*ARTIND_PROFESOR*|", Etiqueta.ArtInd_Profesor[lenguaje]);
        if (input.Contains("|*ARTIND_PROFESORES*|"))
          input = input.Replace("|*ARTIND_PROFESORES*|", Etiqueta.ArtInd_Profesores[lenguaje]);
        if (input.Contains("|*LETRA_PROFESOR*|"))
          input = input.Replace("|*LETRA_PROFESOR*|", Etiqueta.Letra_Profesor[lenguaje]);

        if (input.Contains("|*COORDINADOR*|"))
          input = input.Replace("|*COORDINADOR*|", Etiqueta.Coordinador[lenguaje]);
        if (input.Contains("|*COORDINADORES*|"))
          input = input.Replace("|*COORDINADORES*|", Etiqueta.Coordinadores[lenguaje]);
        if (input.Contains("|*ARTDET_COORDINADOR*|"))
          input = input.Replace("|*ARTDET_COORDINADOR*|", Etiqueta.ArtDet_Coordinador[lenguaje]);
        if (input.Contains("|*ARTDET_COORDINADORES*|"))
          input = input.Replace("|*ARTDET_COORDINADORES*|", Etiqueta.ArtDet_Coordinadores[lenguaje]);
        if (input.Contains("|*ARTIND_COORDINADOR*|"))
          input = input.Replace("|*ARTIND_COORDINADOR*|", Etiqueta.ArtInd_Coordinador[lenguaje]);
        if (input.Contains("|*ARTIND_COORDINADORES*|"))
          input = input.Replace("|*ARTIND_COORDINADORES*|", Etiqueta.ArtInd_Coordinadores[lenguaje]);
        if (input.Contains("|*LETRA_COORDINADOR*|"))
          input = input.Replace("|*LETRA_COORDINADOR*|", Etiqueta.Letra_Coordinador[lenguaje]);

        if (input.Contains("|*EQUIPO*|"))
          input = input.Replace("|*EQUIPO*|", Etiqueta.Equipo[lenguaje]);
        if (input.Contains("|*EQUIPOS*|"))
          input = input.Replace("|*EQUIPOS*|", Etiqueta.Equipos[lenguaje]);
        if (input.Contains("|*ARTDET_EQUIPO*|"))
          input = input.Replace("|*ARTDET_EQUIPO*|", Etiqueta.ArtDet_Equipo[lenguaje]);
        if (input.Contains("|*ARTDET_EQUIPOS*|"))
          input = input.Replace("|*ARTDET_EQUIPOS*|", Etiqueta.ArtDet_Equipos[lenguaje]);
        if (input.Contains("|*ARTIND_EQUIPO*|"))
          input = input.Replace("|*ARTIND_EQUIPO*|", Etiqueta.ArtInd_Equipo[lenguaje]);
        if (input.Contains("|*ARTIND_EQUIPOS*|"))
          input = input.Replace("|*ARTIND_EQUIPOS*|", Etiqueta.ArtInd_Equipos[lenguaje]);
        if (input.Contains("|*LETRA_EQUIPO*|"))
          input = input.Replace("|*LETRA_EQUIPO*|", Etiqueta.Letra_Equipo[lenguaje]);

        if (input.Contains("|*SUBEQUIPO*|"))
          input = input.Replace("|*SUBEQUIPO*|", Etiqueta.SubEquipo[lenguaje]);
        if (input.Contains("|*SUBEQUIPOS*|"))
          input = input.Replace("|*SUBEQUIPOS*|", Etiqueta.SubEquipos[lenguaje]);
        if (input.Contains("|*ARTDET_SUBEQUIPO*|"))
          input = input.Replace("|*ARTDET_SUBEQUIPO*|", Etiqueta.ArtDet_SubEquipo[lenguaje]);
        if (input.Contains("|*ARTDET_SUBEQUIPOS*|"))
          input = input.Replace("|*ARTDET_SUBEQUIPOS*|", Etiqueta.ArtDet_SubEquipos[lenguaje]);
        if (input.Contains("|*ARTIND_SUBEQUIPO*|"))
          input = input.Replace("|*ARTIND_SUBEQUIPO*|", Etiqueta.ArtInd_SubEquipo[lenguaje]);
        if (input.Contains("|*ARTIND_SUBEQUIPOS*|"))
          input = input.Replace("|*ARTIND_SUBEQUIPOS*|", Etiqueta.ArtInd_SubEquipos[lenguaje]);
        if (input.Contains("|*LETRA_SUBEQUIPO*|"))
          input = input.Replace("|*LETRA_SUBEQUIPO*|", Etiqueta.Letra_SubEquipo[lenguaje]);

        if (input.Contains("|*MATRICULA*|"))
          input = input.Replace("|*MATRICULA*|", Etiqueta.Matricula[lenguaje]);
        if (input.Contains("|*MATRICULAS*|"))
          input = input.Replace("|*MATRICULAS*|", Etiqueta.Matriculas[lenguaje]);
        if (input.Contains("|*ARTDET_MATRICULA*|"))
          input = input.Replace("|*ARTDET_MATRICULA*|", Etiqueta.ArtDet_Matricula[lenguaje]);
        if (input.Contains("|*ARTDET_MATRICULAS*|"))
          input = input.Replace("|*ARTDET_MATRICULAS*|", Etiqueta.ArtDet_Matriculas[lenguaje]);
        if (input.Contains("|*ARTIND_MATRICULA*|"))
          input = input.Replace("|*ARTIND_MATRICULA*|", Etiqueta.ArtInd_Matricula[lenguaje]);
        if (input.Contains("|*ARTIND_MATRICULAS*|"))
          input = input.Replace("|*ARTIND_MATRICULAS*|", Etiqueta.ArtInd_Matriculas[lenguaje]);
        if (input.Contains("|*LETRA_MATRICULA*|"))
          input = input.Replace("|*LETRA_MATRICULA*|", Etiqueta.Letra_Matricula[lenguaje]);

        if (input.Contains("|*CALIFICACION*|"))
          input = input.Replace("|*CALIFICACION*|", Etiqueta.Calificacion[lenguaje]);
        if (input.Contains("|*CALIFICACIONES*|"))
          input = input.Replace("|*CALIFICACIONES*|", Etiqueta.Calificaciones[lenguaje]);
        if (input.Contains("|*ARTDET_CALIFICACION*|"))
          input = input.Replace("|*ARTDET_CALIFICACION*|", Etiqueta.ArtDet_Calificacion[lenguaje]);
        if (input.Contains("|*ARTDET_CALIFICACIONES*|"))
          input = input.Replace("|*ARTDET_CALIFICACIONES*|", Etiqueta.ArtDet_Calificaciones[lenguaje]);
        if (input.Contains("|*ARTIND_CALIFICACION*|"))
          input = input.Replace("|*ARTIND_CALIFICACION*|", Etiqueta.ArtInd_Calificacion[lenguaje]);
        if (input.Contains("|*ARTIND_CALIFICACIONES*|"))
          input = input.Replace("|*ARTIND_CALIFICACIONES*|", Etiqueta.ArtInd_Calificaciones[lenguaje]);
        if (input.Contains("|*LETRA_CALIFICACION*|"))
          input = input.Replace("|*LETRA_CALIFICACION*|", Etiqueta.Letra_Calificacion[lenguaje]);
      }

      return clean(input);
    }

    #endregion

    #region "funciones de importacion"

    // Estas funciones se encargan de cargar el idioma almacenado en los archivos 
    // xml de traducción (ubicados en ~/Lang/) hacia las clases que contienen los contenidos a presentar (en ~/App_Code/Lang/);
    // la carpeta ~/Lang/ tiene en su interior carpetas 

    /// <summary>
    /// Obtiene las etiquetas del lenguaje definido y las escribe en la configuración de etiquetas del filesystem.
    /// </summary>
    /// <param name="lenguaje">Indicador del lenguaje a utilizar.</param>
    /// <remarks>Las lecturas de este archivo xml no deben parsearse por etiquetas, puesto que son las etiquetas mismas.</remarks>
    protected static void importa_Etiquetas(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaPersonalizacion + "\\Etiquetas\\" + lenguaje + ".xml"))
      {

        langFile.ReadToFollowing("SISTEMA_CORTO");
        Etiqueta.Sistema_Corto[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("Sistema_Largo");
        Etiqueta.Sistema_Largo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("Nombre_Empresa");
        Etiqueta.Nombre_Empresa[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("INSTITUCION");
        Etiqueta.Institucion[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("INSTITUCIONES");
        Etiqueta.Instituciones[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_INSTITUCION");
        Etiqueta.ArtDet_Institucion[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_INSTITUCIONES");
        Etiqueta.ArtDet_Instituciones[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_INSTITUCION");
        Etiqueta.ArtInd_Institucion[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_INSTITUCIONES");
        Etiqueta.ArtInd_Instituciones[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("LETRA_INSTITUCION");
        Etiqueta.Letra_Institucion[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("CICLO");
        Etiqueta.Ciclo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("CICLOS");
        Etiqueta.Ciclos[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_CICLO");
        Etiqueta.ArtDet_Ciclo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_CICLOS");
        Etiqueta.ArtDet_Ciclos[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_CICLO");
        Etiqueta.ArtInd_Ciclo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_CICLOS");
        Etiqueta.ArtInd_Ciclos[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("LETRA_CICLO");
        Etiqueta.Letra_Ciclo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("NIVEL");
        Etiqueta.Nivel[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("NIVELES");
        Etiqueta.Niveles[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_NIVEL");
        Etiqueta.ArtDet_Nivel[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_NIVELES");
        Etiqueta.ArtDet_Niveles[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_NIVEL");
        Etiqueta.ArtInd_Nivel[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_NIVELES");
        Etiqueta.ArtInd_Niveles[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("LETRA_NIVEL");
        Etiqueta.Letra_Nivel[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("GRADO");
        Etiqueta.Grado[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("GRADOS");
        Etiqueta.Grados[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_GRADO");
        Etiqueta.ArtDet_Grado[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_GRADOS");
        Etiqueta.ArtDet_Grados[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_GRADO");
        Etiqueta.ArtInd_Grado[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_GRADOS");
        Etiqueta.ArtInd_Grados[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("LETRA_GRADO");
        Etiqueta.Letra_Grado[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ASIGNATURA");
        Etiqueta.Asignatura[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ASIGNATURAS");
        Etiqueta.Asignaturas[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_ASIGNATURA");
        Etiqueta.ArtDet_Asignatura[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_ASIGNATURAS");
        Etiqueta.ArtDet_Asignaturas[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_ASIGNATURA");
        Etiqueta.ArtInd_Asignatura[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_ASIGNATURAS");
        Etiqueta.ArtInd_Asignaturas[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("LETRA_ASIGNATURA");
        Etiqueta.Letra_Asignatura[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("PLANTEL");
        Etiqueta.Plantel[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("PLANTELES");
        Etiqueta.Planteles[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_PLANTEL");
        Etiqueta.ArtDet_Plantel[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_PLANTELES");
        Etiqueta.ArtDet_Planteles[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_PLANTEL");
        Etiqueta.ArtInd_Plantel[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_PLANTELES");
        Etiqueta.ArtInd_Planteles[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("LETRA_PLANTEL");
        Etiqueta.Letra_Plantel[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("GRUPO");
        Etiqueta.Grupo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("GRUPOS");
        Etiqueta.Grupos[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_GRUPO");
        Etiqueta.ArtDet_Grupo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_GRUPOS");
        Etiqueta.ArtDet_Grupos[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_GRUPO");
        Etiqueta.ArtInd_Grupo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_GRUPOS");
        Etiqueta.ArtInd_Grupos[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("LETRA_GRUPO");
        Etiqueta.Letra_Grupo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ALUMNO");
        Etiqueta.Alumno[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ALUMNOS");
        Etiqueta.Alumnos[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_ALUMNO");
        Etiqueta.ArtDet_Alumno[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_ALUMNOS");
        Etiqueta.ArtDet_Alumnos[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_ALUMNO");
        Etiqueta.ArtInd_Alumno[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_ALUMNOS");
        Etiqueta.ArtInd_Alumnos[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("LETRA_ALUMNO");
        Etiqueta.Letra_Alumno[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("PROFESOR");
        Etiqueta.Profesor[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("PROFESORES");
        Etiqueta.Profesores[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_PROFESOR");
        Etiqueta.ArtDet_Profesor[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_PROFESORES");
        Etiqueta.ArtDet_Profesores[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_PROFESOR");
        Etiqueta.ArtInd_Profesor[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_PROFESORES");
        Etiqueta.ArtInd_Profesores[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("LETRA_PROFESOR");
        Etiqueta.Letra_Profesor[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("COORDINADOR");
        Etiqueta.Coordinador[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("COORDINADORES");
        Etiqueta.Coordinadores[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_COORDINADOR");
        Etiqueta.ArtDet_Coordinador[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_COORDINADORES");
        Etiqueta.ArtDet_Coordinadores[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_COORDINADOR");
        Etiqueta.ArtInd_Coordinador[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_COORDINADORES");
        Etiqueta.ArtInd_Coordinadores[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("LETRA_COORDINADOR");
        Etiqueta.Letra_Coordinador[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("EQUIPO");
        Etiqueta.Equipo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("EQUIPOS");
        Etiqueta.Equipos[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_EQUIPO");
        Etiqueta.ArtDet_Equipo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_EQUIPOS");
        Etiqueta.ArtDet_Equipos[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_EQUIPO");
        Etiqueta.ArtInd_Equipo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_EQUIPOS");
        Etiqueta.ArtInd_Equipos[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("LETRA_EQUIPO");
        Etiqueta.Letra_Equipo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("SUBEQUIPO");
        Etiqueta.SubEquipo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("SUBEQUIPOS");
        Etiqueta.SubEquipos[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_SUBEQUIPO");
        Etiqueta.ArtDet_SubEquipo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_SUBEQUIPOS");
        Etiqueta.ArtDet_SubEquipos[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_SUBEQUIPO");
        Etiqueta.ArtInd_SubEquipo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_SUBEQUIPOS");
        Etiqueta.ArtInd_SubEquipos[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("LETRA_SUBEQUIPO");
        Etiqueta.Letra_SubEquipo[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("MATRICULA");
        Etiqueta.Matricula[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("MATRICULAS");
        Etiqueta.Matriculas[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_MATRICULA");
        Etiqueta.ArtDet_Matricula[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_MATRICULAS");
        Etiqueta.ArtDet_Matriculas[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_MATRICULA");
        Etiqueta.ArtInd_Matricula[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_MATRICULAS");
        Etiqueta.ArtInd_Matriculas[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("LETRA_MATRICULA");
        Etiqueta.Letra_Matricula[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("CALIFICACION");
        Etiqueta.Calificacion[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("CALIFICACIONES");
        Etiqueta.Calificaciones[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_CALIFICACION");
        Etiqueta.ArtDet_Calificacion[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTDET_CALIFICACIONES");
        Etiqueta.ArtDet_Calificaciones[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_CALIFICACION");
        Etiqueta.ArtInd_Calificacion[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("ARTIND_CALIFICACIONES");
        Etiqueta.ArtInd_Calificaciones[lenguaje] = clean(langFile.ReadString());

        langFile.ReadToFollowing("LETRA_CALIFICACION");
        Etiqueta.Letra_Calificacion[lenguaje] = clean(langFile.ReadString());

        langFile.Close();
      }
    }

    protected static void importa_General(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\General.xml"))
      {

        langFile.ReadToFollowing("MSG_ERROR");
        Siget.Lang.FileSystem.General.MSG_ERROR[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_SUCCESS");
        Siget.Lang.FileSystem.General.MSG_SUCCESS[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_WARNING");
        Siget.Lang.FileSystem.General.MSG_WARNING[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NOTE");
        Siget.Lang.FileSystem.General.MSG_NOTE[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TIPO_ACTIVIDAD_APRENDIZAJE");
        Siget.Lang.FileSystem.General.TIPO_ACTIVIDAD_APRENDIZAJE[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TIPO_ACTIVIDAD_EVALUACION");
        Siget.Lang.FileSystem.General.TIPO_ACTIVIDAD_EVALUACION[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TIPO_ACTIVIDAD_MATERIAL");
        Siget.Lang.FileSystem.General.TIPO_ACTIVIDAD_MATERIAL[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TIPO_ACTIVIDAD_REACTIVOS");
        Siget.Lang.FileSystem.General.TIPO_ACTIVIDAD_REACTIVOS[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TIPO_ACTIVIDAD_DOCUMENTO");
        Siget.Lang.FileSystem.General.TIPO_ACTIVIDAD_DOCUMENTO[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TIPO_ACTIVIDAD_EXAMEN");
        Siget.Lang.FileSystem.General.TIPO_ACTIVIDAD_EXAMEN[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TIPO_ACTIVIDAD_TAREA");
        Siget.Lang.FileSystem.General.TIPO_ACTIVIDAD_TAREA[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ESTADO_ACTIVIDAD_BORRADOR");
        Siget.Lang.FileSystem.General.ESTADO_ACTIVIDAD_BORRADOR[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ESTADO_ACTIVIDAD_ENTREGADO");
        Siget.Lang.FileSystem.General.ESTADO_ACTIVIDAD_ENTREGADO[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ddl_elija_un_elemento");
        Siget.Lang.FileSystem.General.ddl_elija_un_elemento[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_SuSesionExpiro");
        Siget.Lang.FileSystem.General.Msg_SuSesionExpiro[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    protected static void importa_Home(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Home.xml"))
      {

        langFile.ReadToFollowing("LOGIN_FAILURE");
        Siget.Lang.FileSystem.Home.LOGIN_FAILURE[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LOGIN_USERNAME");
        Siget.Lang.FileSystem.Home.LOGIN_USERNAME[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LOGIN_PASSWORD");
        Siget.Lang.FileSystem.Home.LOGIN_PASSWORD[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LOGIN_USER_REQUIRED");
        Siget.Lang.FileSystem.Home.LOGIN_USER_REQUIRED[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LOGIN_PASSWORD_REQUIRED");
        Siget.Lang.FileSystem.Home.LOGIN_PASSWORD_REQUIRED[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LOGIN_BUTTON");
        Siget.Lang.FileSystem.Home.LOGIN_BUTTON[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LOGIN_REMEMBER_ME");
        Siget.Lang.FileSystem.Home.LOGIN_REMEMBER_ME[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LOGIN_SHOW_PASSWORD");
        Siget.Lang.FileSystem.Home.LOGIN_SHOW_PASSWORD[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("HL_PRIVACY");
        Siget.Lang.FileSystem.Home.HL_PRIVACY[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_RIGHTS_RESERVED");
        Siget.Lang.FileSystem.Home.LT_RIGHTS_RESERVED[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Lt_GuiaUsuario");
        Siget.Lang.FileSystem.Home.Lt_GuiaUsuario[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Lt_SoporteTecnico");
        Siget.Lang.FileSystem.Home.Lt_SoporteTecnico[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Boton3");
        Siget.Lang.FileSystem.Home.Boton3[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Boton4");
        Siget.Lang.FileSystem.Home.Boton4[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    protected static void importa_Creditos(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Creditos.xml"))
      {

        langFile.ReadToFollowing("LT_TITULO");
        Siget.Lang.FileSystem.Creditos.LT_TITULO[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_DESARROLLO");
        Siget.Lang.FileSystem.Creditos.LT_DESARROLLO[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_ICONOS");
        Siget.Lang.FileSystem.Creditos.LT_ICONOS[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_PRIVACY");
        Siget.Lang.FileSystem.Creditos.LT_PRIVACY[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_PRIVACY_LINK");
        Siget.Lang.FileSystem.Creditos.LT_PRIVACY_LINK[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_GO_BACK");
        Siget.Lang.FileSystem.Creditos.LT_GO_BACK[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    protected static void importa_DataAccess(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\App_Code\\DataAccess.xml"))
      {

        langFile.ReadToFollowing("msg_calificacion_minima_1");
        Siget.Lang.FileSystem.App_Code.DataAccess.msg_calificacion_minima_1[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("msg_calificacion_minima_2");
        Siget.Lang.FileSystem.App_Code.DataAccess.msg_calificacion_minima_2[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("msg_calificacion_minima_3");
        Siget.Lang.FileSystem.App_Code.DataAccess.msg_calificacion_minima_3[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("msg_falta_previa_1");
        Siget.Lang.FileSystem.App_Code.DataAccess.msg_falta_previa_1[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("msg_falta_previa_2");
        Siget.Lang.FileSystem.App_Code.DataAccess.msg_falta_previa_2[lenguaje] = parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    #endregion

  }

}