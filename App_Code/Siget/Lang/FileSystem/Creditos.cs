﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem
{

  public class Creditos
  {


    public static Dictionary<string, string> LT_TITULO = new Dictionary<string, string>();

    public static Dictionary<string, string> LT_DESARROLLO = new Dictionary<string, string>();

    public static Dictionary<string, string> LT_ICONOS = new Dictionary<string, string>();

    public static Dictionary<string, string> LT_PRIVACY = new Dictionary<string, string>();

    public static Dictionary<string, string> LT_PRIVACY_LINK = new Dictionary<string, string>();

    public static Dictionary<string, string> LT_GO_BACK = new Dictionary<string, string>();
  }

}