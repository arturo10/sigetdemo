﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem
{

  public class Home
  {


    public static Dictionary<string, string> LOGIN_FAILURE = new Dictionary<string, string>();

    public static Dictionary<string, string> LOGIN_USERNAME = new Dictionary<string, string>();

    public static Dictionary<string, string> LOGIN_PASSWORD = new Dictionary<string, string>();

    public static Dictionary<string, string> LOGIN_USER_REQUIRED = new Dictionary<string, string>();

    public static Dictionary<string, string> LOGIN_PASSWORD_REQUIRED = new Dictionary<string, string>();

    public static Dictionary<string, string> LOGIN_BUTTON = new Dictionary<string, string>();

    public static Dictionary<string, string> LOGIN_REMEMBER_ME = new Dictionary<string, string>();

    public static Dictionary<string, string> LOGIN_SHOW_PASSWORD = new Dictionary<string, string>();

    public static Dictionary<string, string> HL_PRIVACY = new Dictionary<string, string>();

    public static Dictionary<string, string> LT_RIGHTS_RESERVED = new Dictionary<string, string>();

    public static Dictionary<string, string> Lt_GuiaUsuario = new Dictionary<string, string>();

    public static Dictionary<string, string> Lt_SoporteTecnico = new Dictionary<string, string>();

    public static Dictionary<string, string> Boton3 = new Dictionary<string, string>();

    public static Dictionary<string, string> Boton4 = new Dictionary<string, string>();
  }

}