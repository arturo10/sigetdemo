﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.App_Code
{

  public class DataAccess
  {

    /// <summary>
    /// No puede resolver esta actividad porque aún no obtuvo la calificación mínima (
    /// </summary>

    public static Dictionary<string, string> msg_calificacion_minima_1 = new Dictionary<string, string>();
    /// <summary>
    /// ) en la actividad previa
    /// </summary>

    public static Dictionary<string, string> msg_calificacion_minima_2 = new Dictionary<string, string>();
    /// <summary>
    /// . Para más información, consulte al administrador.
    /// </summary>

    public static Dictionary<string, string> msg_calificacion_minima_3 = new Dictionary<string, string>();
    /// <summary>
    /// No puede resolver esta actividad porque aún no ha terminado la actividad Previa
    /// </summary>

    public static Dictionary<string, string> msg_falta_previa_1 = new Dictionary<string, string>();
    /// <summary>
    /// . Para más información, consulte al administrador.
    /// </summary>

    public static Dictionary<string, string> msg_falta_previa_2 = new Dictionary<string, string>();
  }

}