
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.Alumno
{

	public class Corregir
	{


		public static Dictionary<string, string> LT_TITULO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_INICIO_TITULO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_SEGUNDA_OPORTUNIDAD = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_ACEPTAR = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_SALIR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_ALUMNO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_DISPONIBLE_FECHA_PENALIZACION = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_DISPONIBLE_FECHAS = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_DISPONIBLE_DOCTO_ENTREGADO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_FALTAN_REACTIVOS = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_FALTAN_SUBTEMA = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_MINUTOS_INICIO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_MINUTOS_FIN = new Dictionary<string, string>();

		public static Dictionary<string, string> HINT_REFERENCIA = new Dictionary<string, string>();

		public static Dictionary<string, string> HINT_APOYO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_HAY_REACTIVOS = new Dictionary<string, string>();

		public static Dictionary<string, string> HL_MATERIAL_APOYO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_TERMINO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_DISPONIBLE = new Dictionary<string, string>();
	}

}