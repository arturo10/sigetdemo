
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.Alumno
{

	public class CambiarPassword
	{

		/// <summary>
		/// Cambiar Contraseña
		/// </summary>

		public static Dictionary<string, string> LT_TITULO_PESTANIA = new Dictionary<string, string>();
		/// <summary>
		/// Cambio de Contraseña
		/// </summary>

		public static Dictionary<string, string> LT_TITULO = new Dictionary<string, string>();
		/// <summary>
		/// Contraseña Actual
		/// </summary>

		public static Dictionary<string, string> LT_CONTRASENIA_ACTUAL = new Dictionary<string, string>();
		/// <summary>
		/// Nueva Contraseña
		/// </summary>

		public static Dictionary<string, string> LT_CONTRASENIA_NUEVA = new Dictionary<string, string>();
		/// <summary>
		/// Confirmar Nueva Contraseña
		/// </summary>

		public static Dictionary<string, string> LT_CONTRASENIA_CONFIRMAR = new Dictionary<string, string>();
		/// <summary>
		/// Cambiar
		/// </summary>

		public static Dictionary<string, string> BTN_CAMBIAR = new Dictionary<string, string>();
		/// <summary>
		/// La contraseña se actualizó correctamente.
		/// </summary>

		public static Dictionary<string, string> MSG_SUCCESS = new Dictionary<string, string>();
		/// <summary>
		/// Es necesario completar todos los campos.
		/// </summary>

		public static Dictionary<string, string> MSG_COMPLETAR = new Dictionary<string, string>();
		/// <summary>
		/// La contraseña actual no es correcta.
		/// </summary>

		public static Dictionary<string, string> MSG_CURRENT_INCORRECT = new Dictionary<string, string>();
		/// <summary>
		/// La nueva contraseña y su confirmación no coinciden.
		/// </summary>

		public static Dictionary<string, string> MSG_CONFIRMATION_INCORRECT = new Dictionary<string, string>();
		/// <summary>
		/// La nueva contraseña debe tener al menos 4 caracteres.
		/// </summary>

		public static Dictionary<string, string> MSG_NEW_PASS_RESTRICTION_1 = new Dictionary<string, string>();
	}

}