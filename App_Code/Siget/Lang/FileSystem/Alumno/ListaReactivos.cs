
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.Alumno
{

	public class ListaReactivos
	{


		public static Dictionary<string, string> LT_TITULO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_TITULO_PAGINA = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_CONTINUAR = new Dictionary<string, string>();

		public static Dictionary<string, string> LB_REACTIVOS_MULTIPLES = new Dictionary<string, string>();

		public static Dictionary<string, string> LB_REACTIVOS_ABIERTA = new Dictionary<string, string>();

		public static Dictionary<string, string> TABLA_PLANTEAMIENTO = new Dictionary<string, string>();

		public static Dictionary<string, string> TABLA_APOYO_1 = new Dictionary<string, string>();

		public static Dictionary<string, string> TABLA_APOYO_2 = new Dictionary<string, string>();

		public static Dictionary<string, string> TABLA_OPORTUNIDAD = new Dictionary<string, string>();

		public static Dictionary<string, string> TABLA_PENALIZACION = new Dictionary<string, string>();

		public static Dictionary<string, string> TABLA_OK = new Dictionary<string, string>();
        public static Dictionary<string, string> TABLA_CONSECUTIVO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_HAY_REACTIVOS_MULTIPLES = new Dictionary<string, string>();

		public static Dictionary<string, string> TABLA_RESPUESTA = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_REVISA_POSTERIORMENTE = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_HAY_REACTIVOS_ABIERTOS = new Dictionary<string, string>();
		/// <summary>
		/// ¡Bien hecho! Has terminado la actividad. Tu resultado es:
		/// </summary>

		public static Dictionary<string, string> lt_resultado1 = new Dictionary<string, string>();
		/// <summary>
		/// Si respondiste respuestas abiertas que serán calificadas por el profesor, tu calificación puede cambiar en cuanto se califiquen.
		/// </summary>

		public static Dictionary<string, string> lt_resultado2 = new Dictionary<string, string>();
		/// <summary>
		/// Puedes realizar una segunda vuelta para corregir tus respuestas erróneas.
		/// </summary>

		public static Dictionary<string, string> lt_segundavuelta = new Dictionary<string, string>();
		/// <summary>
		/// Lamentablemente no obtuviste la calificación mínima necesaria en esta actividad (
		/// </summary>

		public static Dictionary<string, string> lt_repite1 = new Dictionary<string, string>();
		/// <summary>
		/// ), por lo que debes realizarla nuevamente.
		/// </summary>

		public static Dictionary<string, string> lt_repite2_repite = new Dictionary<string, string>();
		/// <summary>
		/// ). Sin embargo, has alcanzado el límite de intentos para obtenerlo.
		/// </summary>

		public static Dictionary<string, string> lt_repite2_norepite = new Dictionary<string, string>();
		/// <summary>
		/// Aún tienes (1) intento para obtener el resultado mínimo.
		/// </summary>

		public static Dictionary<string, string> lt_repite_intentos1_uno = new Dictionary<string, string>();
		/// <summary>
		/// Aún tienes (
		/// </summary>

		public static Dictionary<string, string> lt_repite_intentos1_parte1 = new Dictionary<string, string>();
		/// <summary>
		/// ) intentos para obtener el resultado mínimo.
		/// </summary>

		public static Dictionary<string, string> lt_repite_intentos1_parte2 = new Dictionary<string, string>();
		/// <summary>
		/// Aceptar
		/// </summary>

		public static Dictionary<string, string> btnAceptarDialogo = new Dictionary<string, string>();

       
	}

}