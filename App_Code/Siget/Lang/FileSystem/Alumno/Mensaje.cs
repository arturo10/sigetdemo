
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.Alumno
{

	public class Mensaje
	{


		public static Dictionary<string, string> LT_TITULO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_TITULO_PAGINA_ENTRADA_PROFESOR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_TITULO_PAGINA_ENTRADA_COORDINADOR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_TITULO_PAGINA_SALIDA_PROFESOR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_TITULO_PAGINA_SALIDA_COORDINADOR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_PROFESOR_RECIBE = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_COORDINADOR_RECIBE = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_PROFESOR_ENVIA = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_COORDINADOR_ENVIA = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_FECHA = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_ASUNTO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_CONTENIDO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_ADJUNTO = new Dictionary<string, string>();

		public static Dictionary<string, string> CBELIMINAR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_BTN_RESPONDER = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_BTN_ELIMINAR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_BTN_REENVIAR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_BTN_REGRESAR = new Dictionary<string, string>();

		public static Dictionary<string, string> ERR_MARCAR_PARA_ELIMINAR = new Dictionary<string, string>();

		public static Dictionary<string, string> ERR_ADJUNTO_NO_DISPONIBLE = new Dictionary<string, string>();
	}

}