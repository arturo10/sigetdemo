
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.Alumno
{

	public class Responder
	{


		public static Dictionary<string, string> LT_PAGE_TITLE = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_PROFESOR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_COORDINADOR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_ASIGNATURA = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_PLANTEL = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_ASUNTO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_CONTENIDO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_ADJUNTAR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_BTN_ADJUNTAR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_BTN_QUITAR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_BTN_ENVIAR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_BTN_LIMPIAR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HL_REGRESARMENSAJES = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_ADJUNTADO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_DESADJUNTADO = new Dictionary<string, string>();

		public static Dictionary<string, string> ERR_FALTA_ARCHIVO = new Dictionary<string, string>();

		public static Dictionary<string, string> ERR_NO_HA_ADJUNTADO = new Dictionary<string, string>();

		public static Dictionary<string, string> ERR_ADJUNTO_NO_DISPONIBLE = new Dictionary<string, string>();

		public static Dictionary<string, string> ERR_FALTA_CONTENIDO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NOTIFICACION_ASUNTO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NOTIFICACION_CUERPO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_ENVIADO_PROFESOR = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_ENVIADO_COORDINADOR = new Dictionary<string, string>();
	}

}