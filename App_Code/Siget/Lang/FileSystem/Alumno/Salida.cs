
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.Alumno
{

	public class Salida
	{


		public static Dictionary<string, string> LT_TITULO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_TITULO_PAGINA = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_HAY_MENSAJES = new Dictionary<string, string>();

		public static Dictionary<string, string> GV_SELECT = new Dictionary<string, string>();

		public static Dictionary<string, string> GV_FECHA = new Dictionary<string, string>();

		public static Dictionary<string, string> GV_ASUNTO = new Dictionary<string, string>();

		public static Dictionary<string, string> GV_ESTATUS = new Dictionary<string, string>();

		public static Dictionary<string, string> GV_ESTATUS_LEIDO = new Dictionary<string, string>();

		public static Dictionary<string, string> GV_ESTATUS_NUEVO = new Dictionary<string, string>();

		public static Dictionary<string, string> GV_ESTATUS_BAJA = new Dictionary<string, string>();

		public static Dictionary<string, string> GVMENSAJESPROFESORES_CAPTION = new Dictionary<string, string>();

		public static Dictionary<string, string> GVMENSAJESPROFESORES_PROFE_RECIBE = new Dictionary<string, string>();

		public static Dictionary<string, string> GVMENSAJESCOORDINADORES_CAPTION = new Dictionary<string, string>();

		public static Dictionary<string, string> GVMENSAJESCOORDINADORES_COORD_RECIBE = new Dictionary<string, string>();
	}

}