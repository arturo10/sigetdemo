
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.Alumno
{

	public class MisDatos
	{


		public static Dictionary<string, string> LT_TITULO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_TITULO_PAGINA = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_PASSWORD_CHANGE_TEXT = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HERE_PASSWORD_CHANGE = new Dictionary<string, string>();

		public static Dictionary<string, string> TxtIdioma = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_GRUPO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_MATRICULA = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_NOMBRE = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_APPAT = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_APMAT = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_EQUIPO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_SUBEQUIPO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_EMAIL = new Dictionary<string, string>();

		public static Dictionary<string, string> hint_infoEmail_1 = new Dictionary<string, string>();

		public static Dictionary<string, string> lt_email_2 = new Dictionary<string, string>();

		public static Dictionary<string, string> hint_infoEmail_2 = new Dictionary<string, string>();

		public static Dictionary<string, string> facebook = new Dictionary<string, string>();

		public static Dictionary<string, string> twitter = new Dictionary<string, string>();

        public static Dictionary<string, string> LT_SEXO = new Dictionary<string, string>();

        public static Dictionary<string, string> LT_CURP = new Dictionary<string, string>();

        public static Dictionary<string, string> LT_PUESTO = new Dictionary<string, string>();

        public static Dictionary<string, string> LT_DISCAPACIDAD = new Dictionary<string, string>();

        public static Dictionary<string, string> LT_ESTADO_CIVIL = new Dictionary<string, string>();

        public static Dictionary<string, string> LT_NIVEL_ESTUDIOS = new Dictionary<string, string>();

        public static Dictionary<string, string> LT_TITULO_ESTUDIOS = new Dictionary<string, string>();

        public static Dictionary<string, string> LT_SELECCIONE_SU_SEXO = new Dictionary<string, string>();

        public static Dictionary<string, string> LT_FEMENINO = new Dictionary<string, string>();

        public static Dictionary<string, string> LT_MASCULINO = new Dictionary<string, string>();

        public static Dictionary<string, string> LT_SI = new Dictionary<string, string>();

        public static Dictionary<string, string> LT_NO = new Dictionary<string, string>();

        public static Dictionary<string, string> LT_DISCAPACIDAD_ESPECIFICA = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_ACTUALIZAR = new Dictionary<string, string>();
       
        public static Dictionary<string, string> BTN_CERTIFICADO = new Dictionary<string, string>();

        public static Dictionary<string, string> LT_TITULO_CERTIFICADO = new Dictionary<string, string>();

        public static Dictionary<string, string> MSG_UPDATED = new Dictionary<string, string>();

    }

}