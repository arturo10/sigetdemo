
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.Alumno
{

	public class Master_Principal
	{


		public static Dictionary<string, string> LT_LOGOUT_BUTTON = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_LOGOUT_BUTTON_UNAUTHORIZED = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_CURSOS = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_COMUNICACION = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_BIBLIOTECA = new Dictionary<string, string>();

        public static Dictionary<string, string> LT_INSCRIPCION = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_BLOG = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_RIGHTS_RESERVED = new Dictionary<string, string>();

		public static Dictionary<string, string> HL_CREDITOS = new Dictionary<string, string>();
	}

}