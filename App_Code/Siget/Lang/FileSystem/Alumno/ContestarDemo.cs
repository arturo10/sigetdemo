
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.Alumno
{

	public class ContestarDemo
	{


		public static Dictionary<string, string> LT_TITULO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_SINVALOR = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_ACEPTAR_MULTIPLE = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_SALIR_MULTIPLE = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_REDACTE_RESPUESTA = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_ACEPTAR_ABIERTA = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_SALIR_ABIERTA = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_ALUMNO = new Dictionary<string, string>();
		// <Reactivo> 5 de 6

		public static Dictionary<string, string> LT_FALTA_1 = new Dictionary<string, string>();
		// Reactivo 5 <de> 6

		public static Dictionary<string, string> LT_FALTA_DE = new Dictionary<string, string>();

		public static Dictionary<string, string> HINT_REFERENCIA = new Dictionary<string, string>();

		public static Dictionary<string, string> HINT_APOYO = new Dictionary<string, string>();

		public static Dictionary<string, string> HL_MATERIAL_APOYO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_ELIGIO_RESPUESTA = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_FALTAN_SUBTEMA = new Dictionary<string, string>();
	}

}