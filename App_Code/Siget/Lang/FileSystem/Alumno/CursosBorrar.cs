
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.Alumno
{

	public class CursosBorrar
	{


		public static Dictionary<string, string> LT_TITULO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_TITULO_PAGINA = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_HAY_CURSOS_BORRAR = new Dictionary<string, string>();

		public static Dictionary<string, string> GV_CURSO = new Dictionary<string, string>();

		public static Dictionary<string, string> GV_HORAS_VALIDEZ = new Dictionary<string, string>();

		public static Dictionary<string, string> GV_INSTRUCTOR = new Dictionary<string, string>();

        public static Dictionary<string, string> GV_LUGAR = new Dictionary<string, string>();

        public static Dictionary<string, string> GV_HORARIO = new Dictionary<string, string>();

        public static Dictionary<string, string> GV_INICIA = new Dictionary<string, string>();

        public static Dictionary<string, string> GV_FINALIZA = new Dictionary<string, string>();

        public static Dictionary<string, string> GV_TIPO = new Dictionary<string, string>();

        public static Dictionary<string, string> GV_CICLO_ESCOLAR = new Dictionary<string, string>();

        public static Dictionary<string, string> GV_BORRAR = new Dictionary<string, string>();

		public static Dictionary<string, string> GVCURSOSBORRAR_CAPTION = new Dictionary<string, string>();

	}

}