
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.Alumno
{

	public class Cursos
	{


		public static Dictionary<string, string> LT_PAGE_TITLE = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_BIENVENIDA = new Dictionary<string, string>();

		public static Dictionary<string, string> ACT_TABLE_ACTIVIDAD = new Dictionary<string, string>();

		public static Dictionary<string, string> ACT_TABLE_TIPO = new Dictionary<string, string>();

		public static Dictionary<string, string> ACT_TABLE_FECHA_INICIO = new Dictionary<string, string>();

		public static Dictionary<string, string> ACT_TABLE_FECHA_PENALIZACION = new Dictionary<string, string>();

		public static Dictionary<string, string> ACT_TABLE_FECHA_FINCONTESTAR = new Dictionary<string, string>();

		public static Dictionary<string, string> ACT_TABLE_PENALIZACION = new Dictionary<string, string>();

		public static Dictionary<string, string> ACT_TABLE_CALIFICACION = new Dictionary<string, string>();

		public static Dictionary<string, string> ACT_TABLE_FECHA_TERMINADA = new Dictionary<string, string>();

		public static Dictionary<string, string> ACT_TABLE_ENTREGA = new Dictionary<string, string>();

		public static Dictionary<string, string> HINT_AVISOS = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_AVISOS = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_ACTIVO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_CURSOS = new Dictionary<string, string>();

		public static Dictionary<string, string> msg_evaluacion_no_identificada = new Dictionary<string, string>();
		/// <summary>
		/// Mis Datos
		/// </summary>

		public static Dictionary<string, string> Txt_MisDatos = new Dictionary<string, string>();

        public static Dictionary<string, string> Txt_Revisado = new Dictionary<string, string>();
	}

}