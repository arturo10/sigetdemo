
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.Alumno
{

	public class Biblioteca
	{

		/// <summary>
		/// Biblioteca de Recursos
		/// </summary>

		public static Dictionary<string, string> LT_TITULO = new Dictionary<string, string>();
		/// <summary>
		/// Apoyos Académicos para |*ARTDET_NIVEL*| |*NIVEL*|
		/// </summary>

		public static Dictionary<string, string> APOYOS_NIVEL = new Dictionary<string, string>();
		/// <summary>
		/// Mostrar
		/// </summary>

		public static Dictionary<string, string> LT_MOSTRAR = new Dictionary<string, string>();
		/// <summary>
		/// Si el archivo no se abre automáticamente, da clic aquí
		/// </summary>

		public static Dictionary<string, string> LT_BACKUP_LINK = new Dictionary<string, string>();
		/// <summary>
		/// Elija el tipo de archivo
		/// </summary>

		public static Dictionary<string, string> DDL_ARCHIVO_SELECCIONE = new Dictionary<string, string>();
		/// <summary>
		/// Todos
		/// </summary>

		public static Dictionary<string, string> DDL_ARCHIVO_TODOS = new Dictionary<string, string>();
		/// <summary>
		/// Imagen
		/// </summary>

		public static Dictionary<string, string> DDL_ARCHIVO_IMAGEN = new Dictionary<string, string>();
		/// <summary>
		/// Animación
		/// </summary>

		public static Dictionary<string, string> DDL_ARCHIVO_ANIMACION = new Dictionary<string, string>();
		/// <summary>
		/// Da clic en el apoyo académico que deseas consultar
		/// </summary>

		public static Dictionary<string, string> GV_APOYOS_CAPTION = new Dictionary<string, string>();
		/// <summary>
		/// Seleccionar
		/// </summary>

		public static Dictionary<string, string> GV_SELECCIONAR = new Dictionary<string, string>();
		/// <summary>
		/// Consecutivo
		/// </summary>

		public static Dictionary<string, string> GVAPOYOS_HEADER_CONSECUTIVO = new Dictionary<string, string>();
		/// <summary>
		/// Descripción
		/// </summary>

		public static Dictionary<string, string> GVAPOYOS_HEADER_DESCRIPCION = new Dictionary<string, string>();
		/// <summary>
		/// Apoyo Académico
		/// </summary>

		public static Dictionary<string, string> GVAPOYOS_HEADER_ARCHIVO = new Dictionary<string, string>();
		/// <summary>
		/// Tipo
		/// </summary>

		public static Dictionary<string, string> GVAPOYOS_HEADER_TIPO = new Dictionary<string, string>();
		/// <summary>
		/// Reproduciendo
		/// </summary>

		public static Dictionary<string, string> REPRODUCIENDO = new Dictionary<string, string>();
		/// <summary>
		/// No existen recursos del tipo elegido
		/// </summary>

		public static Dictionary<string, string> NO_HAY_RECURSOS = new Dictionary<string, string>();
	}

}