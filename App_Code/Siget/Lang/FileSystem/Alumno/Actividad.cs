
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

using System.Reflection;
// reflexión de vb

namespace Siget.Lang.FileSystem.Alumno
{

	public class Actividad
	{

		/// <summary>
		/// Material de Actividad
		/// </summary>

		public static Dictionary<string, string> Txt_Titulo = new Dictionary<string, string>();
		/// <summary>
		/// Continúe con la Actividad:
		/// </summary>

		public static Dictionary<string, string> Txt_Liga = new Dictionary<string, string>();
		/// <summary>
		/// Si ya repasó el MATERIAL DE APOYO, continúe con la Actividad:
		/// </summary>

		public static Dictionary<string, string> Txt_LigaConMaterial = new Dictionary<string, string>();
		/// <summary>
		/// Si ya REALIZÓ EL EJERCICIO DE PRÁCTICA, continúe con la Actividad:
		/// </summary>

		public static Dictionary<string, string> Txt_LigaConPractica = new Dictionary<string, string>();
		/// <summary>
		/// Si ya repasó el material y REALIZÓ EL EJERCICIO DE PRÁCTICA, continúe con la Actividad:
		/// </summary>

		public static Dictionary<string, string> Txt_LigaConMaterialYPractica = new Dictionary<string, string>();
		/// <summary>
		/// Iniciar la Evaluación
		/// </summary>

		public static Dictionary<string, string> Btn_Iniciar = new Dictionary<string, string>();
		/// <summary>
		/// Regresar
		/// </summary>

		public static Dictionary<string, string> Btn_Regresar = new Dictionary<string, string>();
		/// <summary>
		/// Esta actividad no tiene reactivos.
		/// </summary>

		public static Dictionary<string, string> Txt_NoHayReactivos = new Dictionary<string, string>();
		/// <summary>
		/// Una vez terminada la Actividad, cargue el archivo de trabajo.
		/// </summary>

		public static Dictionary<string, string> Txt_CargaArchivos = new Dictionary<string, string>();
		/// <summary>
		/// (Solo se puede cargar un archivo, si requiere cargar varios archivos, comprímalos en un solo ZIP)
		/// </summary>

		public static Dictionary<string, string> Txt_CargaUnArchivo = new Dictionary<string, string>();
		/// <summary>
		/// Cargar
		/// </summary>

		public static Dictionary<string, string> Btn_Cargar = new Dictionary<string, string>();
		/// <summary>
		/// Cargar
		/// </summary>

		public static Dictionary<string, string> Btn_Cerrar = new Dictionary<string, string>();
		/// <summary>
		/// Continuar
		/// </summary>

		public static Dictionary<string, string> Btn_Continuar = new Dictionary<string, string>();
		/// <summary>
		/// Cancelar
		/// </summary>

		public static Dictionary<string, string> Btn_Cancelar = new Dictionary<string, string>();
		/// <summary>
		/// Eliminar
		/// </summary>

		public static Dictionary<string, string> Btn_Eliminar = new Dictionary<string, string>();
		/// <summary>
		/// Descargar Acrobat Reader si no se pueden visualizar documentos PDF:
		/// </summary>

		public static Dictionary<string, string> Btn_DescargaAcrobatReader = new Dictionary<string, string>();
		/// <summary>
		/// Material de Apoyo
		/// </summary>

		public static Dictionary<string, string> Tab_Material = new Dictionary<string, string>();
		/// <summary>
		/// Actividades de Práctica
		/// </summary>

		public static Dictionary<string, string> Tab_Practicas = new Dictionary<string, string>();
		/// <summary>
		/// Si el archivo no se abre automáticamente, da clic aquí:
		/// </summary>

		public static Dictionary<string, string> Msg_MaterialAlternativo = new Dictionary<string, string>();
		/// <summary>
		/// Esta actividad no tiene asignado material de apoyo.
		/// </summary>

		public static Dictionary<string, string> Msg_NoHayMaterial = new Dictionary<string, string>();
		/// <summary>
		/// Por favor repasa los materiales de apoyo antes de continuar con la evaluación.
		/// </summary>

		public static Dictionary<string, string> Msg_RepasaPrimero = new Dictionary<string, string>();
		/// <summary>
		/// La actividad aún no se puede completar: la fecha de inicio no ha llegado.
		/// </summary>

		public static Dictionary<string, string> Msg_NoHaIniciado = new Dictionary<string, string>();
		/// <summary>
		/// La actividad ya no está disponible: la fecha límite ya pasó.
		/// </summary>

		public static Dictionary<string, string> Msg_FechaLimiteExcedida = new Dictionary<string, string>();
		/// <summary>
		/// La actividad ya fue contestada y no hay más intentos.
		/// </summary>

		public static Dictionary<string, string> Msg_NoHayMasIntentos = new Dictionary<string, string>();
		/// <summary>
		/// No se puede realizar una segunda vuelta si ya pasó la fecha de penalización.
		/// </summary>

		public static Dictionary<string, string> Msg_NoSegundaVueltaPorPenalizacion = new Dictionary<string, string>();
		/// <summary>
		/// Lo sentimos. Ya no puede cargar material en esta Actividad porque ya venció la fecha de entrega.
		/// </summary>

		public static Dictionary<string, string> Msg_CargaFechaVencida = new Dictionary<string, string>();
		/// <summary>
		/// Lo sentimos. Aún no puede cargar material a esta actividad ya que no ha iniciado. Espere a que llegue la Fecha de Inicio.
		/// </summary>

		public static Dictionary<string, string> Msg_CargaAunNoInicia = new Dictionary<string, string>();
		/// <summary>
		/// Ya no puede cargar otro archivo porque el que colocó ya fue calificado por |*ARTDET_PROFESOR*| |*PROFESOR*|
		/// </summary>

		public static Dictionary<string, string> Msg_CargaYaCalifico = new Dictionary<string, string>();
		/// <summary>
		/// YA EXISTE UN ARCHIVO CON ESE NOMBRE EN EL ALMACEN DE ARCHIVOS, CAMBIE EL NOMBRE DE SU ARCHIVO E INTENTE CARGARLO NUEVAMENTE, POR FAVOR.
		/// </summary>

		public static Dictionary<string, string> Msg_CargaYaExiste = new Dictionary<string, string>();
		/// <summary>
		/// Retroalimentación de |*ARTDET_PROFESOR*| |*PROFESOR*|
		/// </summary>

		public static Dictionary<string, string> Msg_Retroalimentacion = new Dictionary<string, string>();
		/// <summary>
		/// Envío de documento en |*SISTEMA_CORTO*|
		/// </summary>

		public static Dictionary<string, string> Mail_Asunto = new Dictionary<string, string>();
		/// <summary>
		/// |*ARTDET_ALUMNO*| |*ALUMNO*|
		/// </summary>

		public static Dictionary<string, string> Mail_InicioContenido = new Dictionary<string, string>();
		/// <summary>
		/// ha enviado un documento en la evaluación
		/// </summary>

		public static Dictionary<string, string> Mail_FinContenido = new Dictionary<string, string>();
		/// <summary>
		/// El archivo ha sido cargado y se ha enviado una notificación |*ARTDET_PROFESOR*| |*PROFESOR*|. La actividad permanecerá como pendiente hasta que |*ARTDET_PROFESOR*| |*PROFESOR*| lo califique
		/// </summary>

		public static Dictionary<string, string> Msg_CargaExitosaConCorreo = new Dictionary<string, string>();
		/// <summary>
		/// El archivo ha sido cargado. La actividad permanecerá como pendiente hasta que |*ARTDET_PROFESOR*| |*PROFESOR*| lo califique.
		/// </summary>

		public static Dictionary<string, string> Msg_CargaExitosa = new Dictionary<string, string>();
		/// <summary>
		/// Ocurrió un error con la carga del archivo. Vuelva a intentar subirlo.
		/// </summary>

		public static Dictionary<string, string> Msg_CargaError = new Dictionary<string, string>();
		/// <summary>
		/// No ha seleccionado ningún archivo para cargar.
		/// </summary>

		public static Dictionary<string, string> Msg_CargaNoSeleccionoArchivo = new Dictionary<string, string>();
		/// <summary>
		/// No puede eliminar el archivo porque el que colocó ya fue calificado por |*ARTDET_PROFESOR*| |*PROFESOR*|
		/// </summary>

		public static Dictionary<string, string> Msg_CargaEliminarYaCalifico = new Dictionary<string, string>();
		/// <summary>
		/// El archivo ha sido eliminado
		/// </summary>

		public static Dictionary<string, string> Msg_CargaEliminado = new Dictionary<string, string>();
		/// <summary>
		/// No hay ningún archivo cargado para eliminar
		/// </summary>

		public static Dictionary<string, string> Msg_CargaNoHayArchivoParaEliminar = new Dictionary<string, string>();
		/// <summary>
		/// Abrir
		/// </summary>

		public static Dictionary<string, string> Gv_Abrir = new Dictionary<string, string>();
		/// <summary>
		/// Primero consulte el material de apoyo:
		/// </summary>

		public static Dictionary<string, string> GvMaterial_Caption = new Dictionary<string, string>();
		/// <summary>
		/// Material
		/// </summary>

		public static Dictionary<string, string> GvMaterial_Material = new Dictionary<string, string>();
		/// <summary>
		/// Tipo
		/// </summary>

		public static Dictionary<string, string> GvMaterial_Tipo = new Dictionary<string, string>();
		/// <summary>
		/// |*ASIGNATURA*|
		/// </summary>

		public static Dictionary<string, string> GvMaterial_Asignatura = new Dictionary<string, string>();
		/// <summary>
		/// No. Tema
		/// </summary>

		public static Dictionary<string, string> GvMaterial_NumeroTema = new Dictionary<string, string>();
		/// <summary>
		/// Tema
		/// </summary>

		public static Dictionary<string, string> GvMaterial_Tema = new Dictionary<string, string>();
		/// <summary>
		/// No. Subtema
		/// </summary>

		public static Dictionary<string, string> GvMaterial_NumeroSubtema = new Dictionary<string, string>();
		/// <summary>
		/// Subtema
		/// </summary>

		public static Dictionary<string, string> GvMaterial_Subtema = new Dictionary<string, string>();
		/// <summary>
		/// Información sobre la evaluación:
		/// </summary>

		public static Dictionary<string, string> GvMaterialAdicional_Caption = new Dictionary<string, string>();
		/// <summary>
		/// Consecutivo
		/// </summary>

		public static Dictionary<string, string> GvMaterialAdicional_Consecutivo = new Dictionary<string, string>();
		/// <summary>
		/// Archivo de Consulta
		/// </summary>

		public static Dictionary<string, string> GvMaterialAdicional_ArchivoConsulta = new Dictionary<string, string>();
		/// <summary>
		/// Descripción
		/// </summary>

		public static Dictionary<string, string> GvMaterialAdicional_Descripcion = new Dictionary<string, string>();
		/// <summary>
		/// Puede seleccionar el siguiente EJERCICIO DE PRÁCTICA como preparación para la Actividad
		/// </summary>

		public static Dictionary<string, string> GvActividadDemo_Caption = new Dictionary<string, string>();
		/// <summary>
		/// Iniciar Práctica
		/// </summary>

		public static Dictionary<string, string> GvActividadDemo_Iniciar = new Dictionary<string, string>();
		/// <summary>
		/// Ejercicio de Comprensión (No cuenta para calificación)
		/// </summary>

		public static Dictionary<string, string> GvActividadDemo_Actividad = new Dictionary<string, string>();

    /// <summary>
    /// No hay comentarios aún.
    /// </summary>
    public static Dictionary<string, string> Lt_No_Hay_Comentarios = new Dictionary<string, string>();

    /// <summary>
    /// El coemntario fue borrado.
    /// </summary>
    public static Dictionary<string, string> Msg_ComentarioBorrado = new Dictionary<string, string>();


    /// <summary>
    /// El coemntario fue borrado.
    /// </summary>
    public static Dictionary<string, string> Msg_Feedback = new Dictionary<string, string>();

    public static Dictionary<string, string> FeedBack = new Dictionary<string, string>();
	}

}