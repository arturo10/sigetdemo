
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.Alumno
{

	public class Enviar
	{


		public static Dictionary<string, string> LT_TITULO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_TITULO_PAGINA = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_ALUMNO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_CICLO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_DESTINO = new Dictionary<string, string>();

		public static Dictionary<string, string> DDLDESTINO_PROFESORES = new Dictionary<string, string>();

		public static Dictionary<string, string> DDLDESTINO_COORDINADORES = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_SEARCH_PROFE_NOMBRE = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_SEARCH_PROFE_APELLIDOS = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_SEARCH_PROFE_ASIGNATURA = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_BUSCA_PROFESOR = new Dictionary<string, string>();

		public static Dictionary<string, string> BT_LIMPIA_BUSQUEDA_PROFESOR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_DESTINATARIO = new Dictionary<string, string>();

		public static Dictionary<string, string> GVPROFESORES_HEADER_PROFESOR = new Dictionary<string, string>();

		public static Dictionary<string, string> GVPROFESORES_HEADER_ASIGNATURA = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_SEARCH_COORD_NOMBRE = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_SEARCH_COORD_APELLIDOS = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_BUSCA_COORDINADOR = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_LIMPIA_BUSQUEDA_COORDINADOR = new Dictionary<string, string>();

		public static Dictionary<string, string> GVCOORDINADORES_HEADER_COORDINADOR = new Dictionary<string, string>();

		public static Dictionary<string, string> GV_SELECCIONAR = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_ASUNTO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_CONTENIDO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_ADJUNTAR = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_ADJUNTAR = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_QUITAR = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_ENVIAR = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_LIMPIAR = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_ADJUNTADO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_DESADJUNTADO = new Dictionary<string, string>();

		public static Dictionary<string, string> ERR_FALTA_ARCHIVO = new Dictionary<string, string>();

		public static Dictionary<string, string> ERR_NO_HA_ADJUNTADO = new Dictionary<string, string>();

		public static Dictionary<string, string> ERR_ADJUNTO_NO_DISPONIBLE = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_PROFESORES_ASIGNADOS = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_PROFESORES_ENCONTRADOS = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_COORDINADORES_ASIGNADOS = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_COORDINADORES_ENCONTRADOS = new Dictionary<string, string>();

		public static Dictionary<string, string> ERR_FALTA_CONTENIDO = new Dictionary<string, string>();

		public static Dictionary<string, string> ERR_FALTA_PROFESOR_DESTINO = new Dictionary<string, string>();

		public static Dictionary<string, string> ERR_FALTA_COORDINADOR_DESTINO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NOTIFICACION_ASUNTO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NOTIFICACION_CUERPO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_ENVIADO_PROFESOR = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_ENVIADO_COORDINADOR = new Dictionary<string, string>();
	}

}