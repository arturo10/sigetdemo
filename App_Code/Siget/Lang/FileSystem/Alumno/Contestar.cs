
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.Alumno
{

	public class Contestar
	{


		public static Dictionary<string, string> LT_TITULO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_INICIO_TITULO = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_ACEPTAR_MULTIPLE = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_SALIR_MULTIPLE = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_BRINCAR_MULTIPLE = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_REDACTE_RESPUESTA = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_ACEPTAR_ABIERTA = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_CONFIRMA_RESPUESTA_ABIERTA = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_GUARDAR_ABIERTA = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_CONFIRMA_RESPUESTA_GUARDAR = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_SALIR_ABIERTA = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_CONFIRMA_RENUNCIA = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_HINT_ALUMNO = new Dictionary<string, string>();

		public static Dictionary<string, string> BtnVerificaRespuesta = new Dictionary<string, string>();

		/// <summary>
		/// Su respuesta no puede estar vacía.
		/// </summary>
		public static Dictionary<string, string> rfv_respuestaAbierta = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_DISPONIBLE_FECHAS = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_YA_TERMINADA = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_TERMINO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_TERMINO_SUGIERE_SEGUNDA = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_FALTAN_REACTIVOS = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_MINUTOS_INICIO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_MINUTOS_FIN = new Dictionary<string, string>();

		public static Dictionary<string, string> HINT_REFERENCIA = new Dictionary<string, string>();

		public static Dictionary<string, string> HINT_APOYO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_NO_HAY_REACTIVOS = new Dictionary<string, string>();

		public static Dictionary<string, string> HL_MATERIAL_APOYO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_TERMINO_EDITAR_GUARDADAS = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_FALTAN_SUBTEMA = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_RENUNCIADO = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_ILEGAL = new Dictionary<string, string>();

		public static Dictionary<string, string> Msg_Verifica_Bien = new Dictionary<string, string>();

		public static Dictionary<string, string> Msg_Verifica_Mal = new Dictionary<string, string>();
	}

}