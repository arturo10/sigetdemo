
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem.Alumno
{

	public class ContestarDemo_Procesa
	{


		public static Dictionary<string, string> LT_TITULO = new Dictionary<string, string>();

		public static Dictionary<string, string> LT_BTN_REGRESAR = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_SIGUIENTE = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_REINICIAR = new Dictionary<string, string>();

		public static Dictionary<string, string> BTN_TERMINAR = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_INCORRECTA = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_CORRECTA = new Dictionary<string, string>();

		public static Dictionary<string, string> MSG_FALTAN_TOTALES = new Dictionary<string, string>();
	}

}