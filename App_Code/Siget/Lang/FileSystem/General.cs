﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace Siget.Lang.FileSystem
{

  public class General
  {


    public static Dictionary<string, string> MSG_ERROR = new Dictionary<string, string>();

    public static Dictionary<string, string> MSG_SUCCESS = new Dictionary<string, string>();

    public static Dictionary<string, string> MSG_WARNING = new Dictionary<string, string>();

    public static Dictionary<string, string> MSG_NOTE = new Dictionary<string, string>();

    public static Dictionary<string, string> TIPO_ACTIVIDAD_APRENDIZAJE = new Dictionary<string, string>();

    public static Dictionary<string, string> TIPO_ACTIVIDAD_EVALUACION = new Dictionary<string, string>();

    public static Dictionary<string, string> TIPO_ACTIVIDAD_MATERIAL = new Dictionary<string, string>();

    public static Dictionary<string, string> TIPO_ACTIVIDAD_REACTIVOS = new Dictionary<string, string>();

    public static Dictionary<string, string> TIPO_ACTIVIDAD_DOCUMENTO = new Dictionary<string, string>();

    public static Dictionary<string, string> TIPO_ACTIVIDAD_EXAMEN = new Dictionary<string, string>();

    public static Dictionary<string, string> TIPO_ACTIVIDAD_TAREA = new Dictionary<string, string>();

    public static Dictionary<string, string> ESTADO_ACTIVIDAD_BORRADOR = new Dictionary<string, string>();

    public static Dictionary<string, string> ESTADO_ACTIVIDAD_ENTREGADO = new Dictionary<string, string>();

    public static Dictionary<string, string> ddl_elija_un_elemento = new Dictionary<string, string>();

    public static Dictionary<string, string> Msg_SuSesionExpiro = new Dictionary<string, string>();
  }

}