﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Xml;

namespace Siget.Lang
{

  public class Lang_Config_Alumno
  {

    public static void importa_Alumno_Actividad(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\Actividad.xml"))
      {

        langFile.ReadToFollowing("Txt_Titulo");
        Siget.Lang.FileSystem.Alumno.Actividad.Txt_Titulo[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Txt_Liga");
        Siget.Lang.FileSystem.Alumno.Actividad.Txt_Liga[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Txt_LigaConMaterial");
        Siget.Lang.FileSystem.Alumno.Actividad.Txt_LigaConMaterial[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Txt_LigaConPractica");
        Siget.Lang.FileSystem.Alumno.Actividad.Txt_LigaConPractica[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Txt_LigaConMaterialYPractica");
        Siget.Lang.FileSystem.Alumno.Actividad.Txt_LigaConMaterialYPractica[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Btn_Iniciar");
        Siget.Lang.FileSystem.Alumno.Actividad.Btn_Iniciar[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Btn_Regresar");
        Siget.Lang.FileSystem.Alumno.Actividad.Btn_Regresar[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Txt_NoHayReactivos");
        Siget.Lang.FileSystem.Alumno.Actividad.Txt_NoHayReactivos[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Txt_CargaArchivos");
        Siget.Lang.FileSystem.Alumno.Actividad.Txt_CargaArchivos[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Txt_CargaUnArchivo");
        Siget.Lang.FileSystem.Alumno.Actividad.Txt_CargaUnArchivo[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Btn_Cargar");
        Siget.Lang.FileSystem.Alumno.Actividad.Btn_Cargar[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Btn_Cerrar");
        Siget.Lang.FileSystem.Alumno.Actividad.Btn_Cerrar[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Btn_Continuar");
        Siget.Lang.FileSystem.Alumno.Actividad.Btn_Continuar[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Btn_Cancelar");
        Siget.Lang.FileSystem.Alumno.Actividad.Btn_Cancelar[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Btn_Eliminar");
        Siget.Lang.FileSystem.Alumno.Actividad.Btn_Eliminar[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Btn_DescargaAcrobatReader");
        Siget.Lang.FileSystem.Alumno.Actividad.Btn_DescargaAcrobatReader[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Tab_Material");
        Siget.Lang.FileSystem.Alumno.Actividad.Tab_Material[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Tab_Practicas");
        Siget.Lang.FileSystem.Alumno.Actividad.Tab_Practicas[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_MaterialAlternativo");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_MaterialAlternativo[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_NoHayMaterial");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_NoHayMaterial[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_RepasaPrimero");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_RepasaPrimero[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_NoHaIniciado");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_NoHaIniciado[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_FechaLimiteExcedida");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_FechaLimiteExcedida[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_NoHayMasIntentos");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_NoHayMasIntentos[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_NoSegundaVueltaPorPenalizacion");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_NoSegundaVueltaPorPenalizacion[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_CargaFechaVencida");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_CargaFechaVencida[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_CargaAunNoInicia");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_CargaAunNoInicia[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_CargaYaCalifico");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_CargaYaCalifico[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_CargaYaExiste");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_CargaYaExiste[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_Retroalimentacion");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_Retroalimentacion[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Mail_Asunto");
        Siget.Lang.FileSystem.Alumno.Actividad.Mail_Asunto[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Mail_InicioContenido");
        Siget.Lang.FileSystem.Alumno.Actividad.Mail_InicioContenido[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Mail_FinContenido");
        Siget.Lang.FileSystem.Alumno.Actividad.Mail_FinContenido[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_CargaExitosaConCorreo");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_CargaExitosaConCorreo[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_CargaExitosa");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_CargaExitosa[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_CargaError");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_CargaError[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_CargaNoSeleccionoArchivo");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_CargaNoSeleccionoArchivo[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_CargaEliminarYaCalifico");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_CargaEliminarYaCalifico[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_CargaEliminado");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_CargaEliminado[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_CargaNoHayArchivoParaEliminar");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_CargaNoHayArchivoParaEliminar[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Gv_Abrir");
        Siget.Lang.FileSystem.Alumno.Actividad.Gv_Abrir[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GvMaterial_Caption");
        Siget.Lang.FileSystem.Alumno.Actividad.GvMaterial_Caption[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GvMaterial_Material");
        Siget.Lang.FileSystem.Alumno.Actividad.GvMaterial_Material[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GvMaterial_Tipo");
        Siget.Lang.FileSystem.Alumno.Actividad.GvMaterial_Tipo[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GvMaterial_Asignatura");
        Siget.Lang.FileSystem.Alumno.Actividad.GvMaterial_Asignatura[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GvMaterial_NumeroTema");
        Siget.Lang.FileSystem.Alumno.Actividad.GvMaterial_NumeroTema[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GvMaterial_Tema");
        Siget.Lang.FileSystem.Alumno.Actividad.GvMaterial_Tema[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GvMaterial_NumeroSubtema");
        Siget.Lang.FileSystem.Alumno.Actividad.GvMaterial_NumeroSubtema[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GvMaterial_Subtema");
        Siget.Lang.FileSystem.Alumno.Actividad.GvMaterial_Subtema[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GvMaterialAdicional_Caption");
        Siget.Lang.FileSystem.Alumno.Actividad.GvMaterialAdicional_Caption[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GvMaterialAdicional_Consecutivo");
        Siget.Lang.FileSystem.Alumno.Actividad.GvMaterialAdicional_Consecutivo[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GvMaterialAdicional_ArchivoConsulta");
        Siget.Lang.FileSystem.Alumno.Actividad.GvMaterialAdicional_ArchivoConsulta[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GvMaterialAdicional_Descripcion");
        Siget.Lang.FileSystem.Alumno.Actividad.GvMaterialAdicional_Descripcion[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GvActividadDemo_Caption");
        Siget.Lang.FileSystem.Alumno.Actividad.GvActividadDemo_Caption[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GvActividadDemo_Iniciar");
        Siget.Lang.FileSystem.Alumno.Actividad.GvActividadDemo_Iniciar[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GvActividadDemo_Actividad");
        Siget.Lang.FileSystem.Alumno.Actividad.GvActividadDemo_Actividad[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Lt_No_Hay_Comentarios");
        Siget.Lang.FileSystem.Alumno.Actividad.Lt_No_Hay_Comentarios[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_ComentarioBorrado");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_ComentarioBorrado[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_btn_Feedback");
        Siget.Lang.FileSystem.Alumno.Actividad.Msg_Feedback[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("lt_Feedback");
        Siget.Lang.FileSystem.Alumno.Actividad.FeedBack[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_Biblioteca(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\Biblioteca.xml"))
      {

        langFile.ReadToFollowing("LT_TITULO");
        Siget.Lang.FileSystem.Alumno.Biblioteca.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("APOYOS_NIVEL");
        Siget.Lang.FileSystem.Alumno.Biblioteca.APOYOS_NIVEL[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_MOSTRAR");
        Siget.Lang.FileSystem.Alumno.Biblioteca.LT_MOSTRAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_BACKUP_LINK");
        Siget.Lang.FileSystem.Alumno.Biblioteca.LT_BACKUP_LINK[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("DDL_ARCHIVO_SELECCIONE");
        Siget.Lang.FileSystem.Alumno.Biblioteca.DDL_ARCHIVO_SELECCIONE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("DDL_ARCHIVO_TODOS");
        Siget.Lang.FileSystem.Alumno.Biblioteca.DDL_ARCHIVO_TODOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("DDL_ARCHIVO_IMAGEN");
        Siget.Lang.FileSystem.Alumno.Biblioteca.DDL_ARCHIVO_IMAGEN[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("DDL_ARCHIVO_ANIMACION");
        Siget.Lang.FileSystem.Alumno.Biblioteca.DDL_ARCHIVO_ANIMACION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_APOYOS_CAPTION");
        Siget.Lang.FileSystem.Alumno.Biblioteca.GV_APOYOS_CAPTION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_SELECCIONAR");
        Siget.Lang.FileSystem.Alumno.Biblioteca.GV_SELECCIONAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GVAPOYOS_HEADER_CONSECUTIVO");
        Siget.Lang.FileSystem.Alumno.Biblioteca.GVAPOYOS_HEADER_CONSECUTIVO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GVAPOYOS_HEADER_DESCRIPCION");
        Siget.Lang.FileSystem.Alumno.Biblioteca.GVAPOYOS_HEADER_DESCRIPCION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GVAPOYOS_HEADER_ARCHIVO");
        Siget.Lang.FileSystem.Alumno.Biblioteca.GVAPOYOS_HEADER_ARCHIVO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GVAPOYOS_HEADER_TIPO");
        Siget.Lang.FileSystem.Alumno.Biblioteca.GVAPOYOS_HEADER_TIPO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("REPRODUCIENDO");
        Siget.Lang.FileSystem.Alumno.Biblioteca.REPRODUCIENDO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("NO_HAY_RECURSOS");
        Siget.Lang.FileSystem.Alumno.Biblioteca.NO_HAY_RECURSOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_CambiarPassword(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\CambiarPassword.xml"))
      {

        langFile.ReadToFollowing("LT_TITULO_PESTANIA");
        Siget.Lang.FileSystem.Alumno.CambiarPassword.LT_TITULO_PESTANIA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_TITULO");
        Siget.Lang.FileSystem.Alumno.CambiarPassword.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_CONTRASENIA_ACTUAL");
        Siget.Lang.FileSystem.Alumno.CambiarPassword.LT_CONTRASENIA_ACTUAL[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_CONTRASENIA_NUEVA");
        Siget.Lang.FileSystem.Alumno.CambiarPassword.LT_CONTRASENIA_NUEVA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_CONTRASENIA_CONFIRMAR");
        Siget.Lang.FileSystem.Alumno.CambiarPassword.LT_CONTRASENIA_CONFIRMAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_CAMBIAR");
        Siget.Lang.FileSystem.Alumno.CambiarPassword.BTN_CAMBIAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_SUCCESS");
        Siget.Lang.FileSystem.Alumno.CambiarPassword.MSG_SUCCESS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_COMPLETAR");
        Siget.Lang.FileSystem.Alumno.CambiarPassword.MSG_COMPLETAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_CURRENT_INCORRECT");
        Siget.Lang.FileSystem.Alumno.CambiarPassword.MSG_CURRENT_INCORRECT[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_CONFIRMATION_INCORRECT");
        Siget.Lang.FileSystem.Alumno.CambiarPassword.MSG_CONFIRMATION_INCORRECT[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NEW_PASS_RESTRICTION_1");
        Siget.Lang.FileSystem.Alumno.CambiarPassword.MSG_NEW_PASS_RESTRICTION_1[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_Contestar(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\Contestar.xml"))
      {

        langFile.ReadToFollowing("LT_TITULO");
        Siget.Lang.FileSystem.Alumno.Contestar.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_INICIO_TITULO");
        Siget.Lang.FileSystem.Alumno.Contestar.LT_INICIO_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_ACEPTAR_MULTIPLE");
        Siget.Lang.FileSystem.Alumno.Contestar.BTN_ACEPTAR_MULTIPLE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_SALIR_MULTIPLE");
        Siget.Lang.FileSystem.Alumno.Contestar.BTN_SALIR_MULTIPLE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_BRINCAR_MULTIPLE");
        Siget.Lang.FileSystem.Alumno.Contestar.BTN_BRINCAR_MULTIPLE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_REDACTE_RESPUESTA");
        Siget.Lang.FileSystem.Alumno.Contestar.LT_HINT_REDACTE_RESPUESTA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_ACEPTAR_ABIERTA");
        Siget.Lang.FileSystem.Alumno.Contestar.BTN_ACEPTAR_ABIERTA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_CONFIRMA_RESPUESTA_ABIERTA");
        Siget.Lang.FileSystem.Alumno.Contestar.MSG_CONFIRMA_RESPUESTA_ABIERTA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_GUARDAR_ABIERTA");
        Siget.Lang.FileSystem.Alumno.Contestar.BTN_GUARDAR_ABIERTA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_CONFIRMA_RESPUESTA_GUARDAR");
        Siget.Lang.FileSystem.Alumno.Contestar.MSG_CONFIRMA_RESPUESTA_GUARDAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_SALIR_ABIERTA");
        Siget.Lang.FileSystem.Alumno.Contestar.BTN_SALIR_ABIERTA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_CONFIRMA_RENUNCIA");
        Siget.Lang.FileSystem.Alumno.Contestar.MSG_CONFIRMA_RENUNCIA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_ALUMNO");
        Siget.Lang.FileSystem.Alumno.Contestar.LT_HINT_ALUMNO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BtnVerificaRespuesta");
        Siget.Lang.FileSystem.Alumno.Contestar.BtnVerificaRespuesta[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("rfv_respuestaAbierta");
        Siget.Lang.FileSystem.Alumno.Contestar.rfv_respuestaAbierta[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_DISPONIBLE_FECHAS");
        Siget.Lang.FileSystem.Alumno.Contestar.MSG_NO_DISPONIBLE_FECHAS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_YA_TERMINADA");
        Siget.Lang.FileSystem.Alumno.Contestar.MSG_YA_TERMINADA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_TERMINO");
        Siget.Lang.FileSystem.Alumno.Contestar.MSG_TERMINO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_TERMINO_SUGIERE_SEGUNDA");
        Siget.Lang.FileSystem.Alumno.Contestar.MSG_TERMINO_SUGIERE_SEGUNDA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_FALTAN_REACTIVOS");
        Siget.Lang.FileSystem.Alumno.Contestar.MSG_FALTAN_REACTIVOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_MINUTOS_INICIO");
        Siget.Lang.FileSystem.Alumno.Contestar.MSG_MINUTOS_INICIO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_MINUTOS_FIN");
        Siget.Lang.FileSystem.Alumno.Contestar.MSG_MINUTOS_FIN[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("HINT_REFERENCIA");
        Siget.Lang.FileSystem.Alumno.Contestar.HINT_REFERENCIA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("HINT_APOYO");
        Siget.Lang.FileSystem.Alumno.Contestar.HINT_APOYO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_HAY_REACTIVOS");
        Siget.Lang.FileSystem.Alumno.Contestar.MSG_NO_HAY_REACTIVOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("HL_MATERIAL_APOYO");
        Siget.Lang.FileSystem.Alumno.Contestar.HL_MATERIAL_APOYO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_TERMINO_EDITAR_GUARDADAS");
        Siget.Lang.FileSystem.Alumno.Contestar.MSG_TERMINO_EDITAR_GUARDADAS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_FALTAN_SUBTEMA");
        Siget.Lang.FileSystem.Alumno.Contestar.LT_FALTAN_SUBTEMA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_RENUNCIADO");
        Siget.Lang.FileSystem.Alumno.Contestar.MSG_RENUNCIADO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_ILEGAL");
        Siget.Lang.FileSystem.Alumno.Contestar.MSG_ILEGAL[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_Verifica_Bien");
        Siget.Lang.FileSystem.Alumno.Contestar.Msg_Verifica_Bien[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Msg_Verifica_Mal");
        Siget.Lang.FileSystem.Alumno.Contestar.Msg_Verifica_Mal[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_ContestarDemo(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\ContestarDemo.xml"))
      {

        langFile.ReadToFollowing("LT_TITULO");
        Siget.Lang.FileSystem.Alumno.ContestarDemo.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_SINVALOR");
        Siget.Lang.FileSystem.Alumno.ContestarDemo.LT_HINT_SINVALOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_ACEPTAR_MULTIPLE");
        Siget.Lang.FileSystem.Alumno.ContestarDemo.BTN_ACEPTAR_MULTIPLE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_SALIR_MULTIPLE");
        Siget.Lang.FileSystem.Alumno.ContestarDemo.BTN_SALIR_MULTIPLE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_REDACTE_RESPUESTA");
        Siget.Lang.FileSystem.Alumno.ContestarDemo.LT_HINT_REDACTE_RESPUESTA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_ACEPTAR_ABIERTA");
        Siget.Lang.FileSystem.Alumno.ContestarDemo.BTN_ACEPTAR_ABIERTA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_SALIR_ABIERTA");
        Siget.Lang.FileSystem.Alumno.ContestarDemo.BTN_SALIR_ABIERTA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_ALUMNO");
        Siget.Lang.FileSystem.Alumno.ContestarDemo.LT_HINT_ALUMNO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_FALTA_1");
        Siget.Lang.FileSystem.Alumno.ContestarDemo.LT_FALTA_1[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_FALTA_DE");
        Siget.Lang.FileSystem.Alumno.ContestarDemo.LT_FALTA_DE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("HINT_REFERENCIA");
        Siget.Lang.FileSystem.Alumno.ContestarDemo.HINT_REFERENCIA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("HINT_APOYO");
        Siget.Lang.FileSystem.Alumno.ContestarDemo.HINT_APOYO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("HL_MATERIAL_APOYO");
        Siget.Lang.FileSystem.Alumno.ContestarDemo.HL_MATERIAL_APOYO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_ELIGIO_RESPUESTA");
        Siget.Lang.FileSystem.Alumno.ContestarDemo.MSG_NO_ELIGIO_RESPUESTA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_FALTAN_SUBTEMA");
        Siget.Lang.FileSystem.Alumno.ContestarDemo.LT_FALTAN_SUBTEMA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_Corregir(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\Corregir.xml"))
      {

        langFile.ReadToFollowing("LT_TITULO");
        Siget.Lang.FileSystem.Alumno.Corregir.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_INICIO_TITULO");
        Siget.Lang.FileSystem.Alumno.Corregir.LT_INICIO_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_SEGUNDA_OPORTUNIDAD");
        Siget.Lang.FileSystem.Alumno.Corregir.LT_SEGUNDA_OPORTUNIDAD[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_ACEPTAR");
        Siget.Lang.FileSystem.Alumno.Corregir.BTN_ACEPTAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_SALIR");
        Siget.Lang.FileSystem.Alumno.Corregir.BTN_SALIR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_ALUMNO");
        Siget.Lang.FileSystem.Alumno.Corregir.LT_HINT_ALUMNO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_DISPONIBLE_FECHA_PENALIZACION");
        Siget.Lang.FileSystem.Alumno.Corregir.MSG_NO_DISPONIBLE_FECHA_PENALIZACION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_DISPONIBLE_FECHAS");
        Siget.Lang.FileSystem.Alumno.Corregir.MSG_NO_DISPONIBLE_FECHAS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_DISPONIBLE_DOCTO_ENTREGADO");
        Siget.Lang.FileSystem.Alumno.Corregir.MSG_NO_DISPONIBLE_DOCTO_ENTREGADO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_FALTAN_REACTIVOS");
        Siget.Lang.FileSystem.Alumno.Corregir.MSG_FALTAN_REACTIVOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_FALTAN_SUBTEMA");
        Siget.Lang.FileSystem.Alumno.Corregir.LT_FALTAN_SUBTEMA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_MINUTOS_INICIO");
        Siget.Lang.FileSystem.Alumno.Corregir.MSG_MINUTOS_INICIO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_MINUTOS_FIN");
        Siget.Lang.FileSystem.Alumno.Corregir.MSG_MINUTOS_FIN[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("HINT_REFERENCIA");
        Siget.Lang.FileSystem.Alumno.Corregir.HINT_REFERENCIA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("HINT_APOYO");
        Siget.Lang.FileSystem.Alumno.Corregir.HINT_APOYO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_HAY_REACTIVOS");
        Siget.Lang.FileSystem.Alumno.Corregir.MSG_NO_HAY_REACTIVOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("HL_MATERIAL_APOYO");
        Siget.Lang.FileSystem.Alumno.Corregir.HL_MATERIAL_APOYO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_TERMINO");
        Siget.Lang.FileSystem.Alumno.Corregir.MSG_TERMINO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_DISPONIBLE");
        Siget.Lang.FileSystem.Alumno.Corregir.MSG_NO_DISPONIBLE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_Cursos(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\Cursos.xml"))
      {

        langFile.ReadToFollowing("LT_PAGE_TITLE");
        Siget.Lang.FileSystem.Alumno.Cursos.LT_PAGE_TITLE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_BIENVENIDA");
        Siget.Lang.FileSystem.Alumno.Cursos.LT_BIENVENIDA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ACT_TABLE_ACTIVIDAD");
        Siget.Lang.FileSystem.Alumno.Cursos.ACT_TABLE_ACTIVIDAD[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ACT_TABLE_TIPO");
        Siget.Lang.FileSystem.Alumno.Cursos.ACT_TABLE_TIPO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ACT_TABLE_FECHA_INICIO");
        Siget.Lang.FileSystem.Alumno.Cursos.ACT_TABLE_FECHA_INICIO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ACT_TABLE_FECHA_PENALIZACION");
        Siget.Lang.FileSystem.Alumno.Cursos.ACT_TABLE_FECHA_PENALIZACION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ACT_TABLE_FECHA_FINCONTESTAR");
        Siget.Lang.FileSystem.Alumno.Cursos.ACT_TABLE_FECHA_FINCONTESTAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ACT_TABLE_PENALIZACION");
        Siget.Lang.FileSystem.Alumno.Cursos.ACT_TABLE_PENALIZACION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ACT_TABLE_CALIFICACION");
        Siget.Lang.FileSystem.Alumno.Cursos.ACT_TABLE_CALIFICACION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ACT_TABLE_FECHA_TERMINADA");
        Siget.Lang.FileSystem.Alumno.Cursos.ACT_TABLE_FECHA_TERMINADA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ACT_TABLE_ENTREGA");
        Siget.Lang.FileSystem.Alumno.Cursos.ACT_TABLE_ENTREGA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("HINT_AVISOS");
        Siget.Lang.FileSystem.Alumno.Cursos.HINT_AVISOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_AVISOS");
        Siget.Lang.FileSystem.Alumno.Cursos.MSG_NO_AVISOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_ACTIVO");
        Siget.Lang.FileSystem.Alumno.Cursos.MSG_NO_ACTIVO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_CURSOS");
        Siget.Lang.FileSystem.Alumno.Cursos.MSG_NO_CURSOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("msg_evaluacion_no_identificada");
        Siget.Lang.FileSystem.Alumno.Cursos.msg_evaluacion_no_identificada[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Txt_MisDatos");
        Siget.Lang.FileSystem.Alumno.Cursos.Txt_MisDatos[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("Txt_Revisado");
        Siget.Lang.FileSystem.Alumno.Cursos.Txt_Revisado[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_CursosInscritos(string lenguaje)
    {
        using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\CursosInscritos.xml"))
        {

            langFile.ReadToFollowing("LT_TITULO");
            Siget.Lang.FileSystem.Alumno.CursosInscritos.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("LT_TITULO_PAGINA");
            Siget.Lang.FileSystem.Alumno.CursosInscritos.LT_TITULO_PAGINA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("MSG_NO_HAY_CURSOS_INSCRITOS");
            Siget.Lang.FileSystem.Alumno.CursosInscritos.MSG_NO_HAY_CURSOS_INSCRITOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_CURSO");
            Siget.Lang.FileSystem.Alumno.CursosInscritos.GV_CURSO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_HORAS_VALIDEZ");
            Siget.Lang.FileSystem.Alumno.CursosInscritos.GV_HORAS_VALIDEZ[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_INSTRUCTOR");
            Siget.Lang.FileSystem.Alumno.CursosInscritos.GV_INSTRUCTOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);
            
            langFile.ReadToFollowing("GV_LUGAR");
            Siget.Lang.FileSystem.Alumno.CursosInscritos.GV_LUGAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_HORARIO");
            Siget.Lang.FileSystem.Alumno.CursosInscritos.GV_HORARIO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_INICIA");
            Siget.Lang.FileSystem.Alumno.CursosInscritos.GV_INICIA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_FINALIZA");
            Siget.Lang.FileSystem.Alumno.CursosInscritos.GV_FINALIZA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_TIPO");
            Siget.Lang.FileSystem.Alumno.CursosInscritos.GV_TIPO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_CICLO_ESCOLAR");
            Siget.Lang.FileSystem.Alumno.CursosInscritos.GV_CICLO_ESCOLAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GVCURSOSINSCRITOS_CAPTION");
            Siget.Lang.FileSystem.Alumno.CursosInscritos.GVCURSOSINSCRITOS_CAPTION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.Close();
        }
    }

    public static void importa_Alumno_CursosInscribir(string lenguaje)
    {
        using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\CursosInscribir.xml"))
        {

            langFile.ReadToFollowing("LT_TITULO");
            Siget.Lang.FileSystem.Alumno.CursosInscribir.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("LT_TITULO_PAGINA");
            Siget.Lang.FileSystem.Alumno.CursosInscribir.LT_TITULO_PAGINA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("MSG_NO_HAY_CURSOS_INSCRIBIR");
            Siget.Lang.FileSystem.Alumno.CursosInscribir.MSG_NO_HAY_CURSOS_INSCRIBIR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_CURSO");
            Siget.Lang.FileSystem.Alumno.CursosInscribir.GV_CURSO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_HORAS_VALIDEZ");
            Siget.Lang.FileSystem.Alumno.CursosInscribir.GV_HORAS_VALIDEZ[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_INSTRUCTOR");
            Siget.Lang.FileSystem.Alumno.CursosInscribir.GV_INSTRUCTOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_LUGAR");
            Siget.Lang.FileSystem.Alumno.CursosInscribir.GV_LUGAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_HORARIO");
            Siget.Lang.FileSystem.Alumno.CursosInscribir.GV_HORARIO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_INICIA");
            Siget.Lang.FileSystem.Alumno.CursosInscribir.GV_INICIA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_FINALIZA");
            Siget.Lang.FileSystem.Alumno.CursosInscribir.GV_FINALIZA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_TIPO");
            Siget.Lang.FileSystem.Alumno.CursosInscribir.GV_TIPO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);
            
            langFile.ReadToFollowing("GV_CICLO_ESCOLAR");
            Siget.Lang.FileSystem.Alumno.CursosInscribir.GV_CICLO_ESCOLAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_INSCRIBIR");
            Siget.Lang.FileSystem.Alumno.CursosInscribir.GV_INSCRIBIR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GVCURSOSINSCRIBIR_CAPTION");
            Siget.Lang.FileSystem.Alumno.CursosInscribir.GVCURSOSINSCRIBIR_CAPTION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.Close();
        }
    }

    public static void importa_Alumno_CursosBorrar(string lenguaje)
    {
        using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\CursosBorrar.xml"))
        {

            langFile.ReadToFollowing("LT_TITULO");
            Siget.Lang.FileSystem.Alumno.CursosBorrar.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("LT_TITULO_PAGINA");
            Siget.Lang.FileSystem.Alumno.CursosBorrar.LT_TITULO_PAGINA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("MSG_NO_HAY_CURSOS_BORRAR");
            Siget.Lang.FileSystem.Alumno.CursosBorrar.MSG_NO_HAY_CURSOS_BORRAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_CURSO");
            Siget.Lang.FileSystem.Alumno.CursosBorrar.GV_CURSO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_HORAS_VALIDEZ");
            Siget.Lang.FileSystem.Alumno.CursosBorrar.GV_HORAS_VALIDEZ[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_INSTRUCTOR");
            Siget.Lang.FileSystem.Alumno.CursosBorrar.GV_INSTRUCTOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_LUGAR");
            Siget.Lang.FileSystem.Alumno.CursosBorrar.GV_LUGAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_HORARIO");
            Siget.Lang.FileSystem.Alumno.CursosBorrar.GV_HORARIO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_INICIA");
            Siget.Lang.FileSystem.Alumno.CursosBorrar.GV_INICIA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_FINALIZA");
            Siget.Lang.FileSystem.Alumno.CursosBorrar.GV_FINALIZA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_TIPO");
            Siget.Lang.FileSystem.Alumno.CursosBorrar.GV_TIPO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_CICLO_ESCOLAR");
            Siget.Lang.FileSystem.Alumno.CursosBorrar.GV_CICLO_ESCOLAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GV_BORRAR");
            Siget.Lang.FileSystem.Alumno.CursosBorrar.GV_BORRAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("GVCURSOSBORRAR_CAPTION");
            Siget.Lang.FileSystem.Alumno.CursosBorrar.GVCURSOSBORRAR_CAPTION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.Close();
        }
    }

    public static void importa_Alumno_Entrada(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\Entrada.xml"))
      {

        langFile.ReadToFollowing("LT_TITULO");
        Siget.Lang.FileSystem.Alumno.Entrada.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_TITULO_PAGINA");
        Siget.Lang.FileSystem.Alumno.Entrada.LT_TITULO_PAGINA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_HAY_MENSAJES");
        Siget.Lang.FileSystem.Alumno.Entrada.MSG_NO_HAY_MENSAJES[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_SELECT");
        Siget.Lang.FileSystem.Alumno.Entrada.GV_SELECT[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_FECHA");
        Siget.Lang.FileSystem.Alumno.Entrada.GV_FECHA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_ASUNTO");
        Siget.Lang.FileSystem.Alumno.Entrada.GV_ASUNTO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_ESTATUS");
        Siget.Lang.FileSystem.Alumno.Entrada.GV_ESTATUS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_ESTATUS_LEIDO");
        Siget.Lang.FileSystem.Alumno.Entrada.GV_ESTATUS_LEIDO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_ESTATUS_NUEVO");
        Siget.Lang.FileSystem.Alumno.Entrada.GV_ESTATUS_NUEVO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_ESTATUS_BAJA");
        Siget.Lang.FileSystem.Alumno.Entrada.GV_ESTATUS_BAJA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GVMENSAJESPROFESORES_CAPTION");
        Siget.Lang.FileSystem.Alumno.Entrada.GVMENSAJESPROFESORES_CAPTION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GVMENSAJESPROFESORES_PROFE_ENVIA");
        Siget.Lang.FileSystem.Alumno.Entrada.GVMENSAJESPROFESORES_PROFE_ENVIA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GVMENSAJESCOORDINADORES_CAPTION");
        Siget.Lang.FileSystem.Alumno.Entrada.GVMENSAJESCOORDINADORES_CAPTION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GVMENSAJESCOORDINADORES_COORD_ENVIA");
        Siget.Lang.FileSystem.Alumno.Entrada.GVMENSAJESCOORDINADORES_COORD_ENVIA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_Enviar(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\Enviar.xml"))
      {

        langFile.ReadToFollowing("LT_TITULO");
        Siget.Lang.FileSystem.Alumno.Enviar.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_TITULO_PAGINA");
        Siget.Lang.FileSystem.Alumno.Enviar.LT_TITULO_PAGINA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_ALUMNO");
        Siget.Lang.FileSystem.Alumno.Enviar.LT_HINT_ALUMNO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_CICLO");
        Siget.Lang.FileSystem.Alumno.Enviar.LT_HINT_CICLO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_DESTINO");
        Siget.Lang.FileSystem.Alumno.Enviar.LT_HINT_DESTINO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("DDLDESTINO_PROFESORES");
        Siget.Lang.FileSystem.Alumno.Enviar.DDLDESTINO_PROFESORES[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("DDLDESTINO_COORDINADORES");
        Siget.Lang.FileSystem.Alumno.Enviar.DDLDESTINO_COORDINADORES[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_SEARCH_PROFE_NOMBRE");
        Siget.Lang.FileSystem.Alumno.Enviar.LT_SEARCH_PROFE_NOMBRE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_SEARCH_PROFE_APELLIDOS");
        Siget.Lang.FileSystem.Alumno.Enviar.LT_SEARCH_PROFE_APELLIDOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_SEARCH_PROFE_ASIGNATURA");
        Siget.Lang.FileSystem.Alumno.Enviar.LT_SEARCH_PROFE_ASIGNATURA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_BUSCA_PROFESOR");
        Siget.Lang.FileSystem.Alumno.Enviar.BTN_BUSCA_PROFESOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BT_LIMPIA_BUSQUEDA_PROFESOR");
        Siget.Lang.FileSystem.Alumno.Enviar.BT_LIMPIA_BUSQUEDA_PROFESOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_DESTINATARIO");
        Siget.Lang.FileSystem.Alumno.Enviar.LT_HINT_DESTINATARIO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GVPROFESORES_HEADER_PROFESOR");
        Siget.Lang.FileSystem.Alumno.Enviar.GVPROFESORES_HEADER_PROFESOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GVPROFESORES_HEADER_ASIGNATURA");
        Siget.Lang.FileSystem.Alumno.Enviar.GVPROFESORES_HEADER_ASIGNATURA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_SEARCH_COORD_NOMBRE");
        Siget.Lang.FileSystem.Alumno.Enviar.LT_SEARCH_COORD_NOMBRE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_SEARCH_COORD_APELLIDOS");
        Siget.Lang.FileSystem.Alumno.Enviar.LT_SEARCH_COORD_APELLIDOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_BUSCA_COORDINADOR");
        Siget.Lang.FileSystem.Alumno.Enviar.BTN_BUSCA_COORDINADOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_LIMPIA_BUSQUEDA_COORDINADOR");
        Siget.Lang.FileSystem.Alumno.Enviar.BTN_LIMPIA_BUSQUEDA_COORDINADOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GVCOORDINADORES_HEADER_COORDINADOR");
        Siget.Lang.FileSystem.Alumno.Enviar.GVCOORDINADORES_HEADER_COORDINADOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_SELECCIONAR");
        Siget.Lang.FileSystem.Alumno.Enviar.GV_SELECCIONAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_ASUNTO");
        Siget.Lang.FileSystem.Alumno.Enviar.LT_HINT_ASUNTO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_CONTENIDO");
        Siget.Lang.FileSystem.Alumno.Enviar.LT_HINT_CONTENIDO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_ADJUNTAR");
        Siget.Lang.FileSystem.Alumno.Enviar.LT_HINT_ADJUNTAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_ADJUNTAR");
        Siget.Lang.FileSystem.Alumno.Enviar.BTN_ADJUNTAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_QUITAR");
        Siget.Lang.FileSystem.Alumno.Enviar.BTN_QUITAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_ENVIAR");
        Siget.Lang.FileSystem.Alumno.Enviar.BTN_ENVIAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_LIMPIAR");
        Siget.Lang.FileSystem.Alumno.Enviar.BTN_LIMPIAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_ADJUNTADO");
        Siget.Lang.FileSystem.Alumno.Enviar.MSG_ADJUNTADO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_DESADJUNTADO");
        Siget.Lang.FileSystem.Alumno.Enviar.MSG_DESADJUNTADO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ERR_FALTA_ARCHIVO");
        Siget.Lang.FileSystem.Alumno.Enviar.ERR_FALTA_ARCHIVO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ERR_NO_HA_ADJUNTADO");
        Siget.Lang.FileSystem.Alumno.Enviar.ERR_NO_HA_ADJUNTADO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ERR_ADJUNTO_NO_DISPONIBLE");
        Siget.Lang.FileSystem.Alumno.Enviar.ERR_ADJUNTO_NO_DISPONIBLE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_PROFESORES_ASIGNADOS");
        Siget.Lang.FileSystem.Alumno.Enviar.MSG_NO_PROFESORES_ASIGNADOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_PROFESORES_ENCONTRADOS");
        Siget.Lang.FileSystem.Alumno.Enviar.MSG_PROFESORES_ENCONTRADOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_COORDINADORES_ASIGNADOS");
        Siget.Lang.FileSystem.Alumno.Enviar.MSG_NO_COORDINADORES_ASIGNADOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_COORDINADORES_ENCONTRADOS");
        Siget.Lang.FileSystem.Alumno.Enviar.MSG_COORDINADORES_ENCONTRADOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ERR_FALTA_CONTENIDO");
        Siget.Lang.FileSystem.Alumno.Enviar.ERR_FALTA_CONTENIDO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ERR_FALTA_PROFESOR_DESTINO");
        Siget.Lang.FileSystem.Alumno.Enviar.ERR_FALTA_PROFESOR_DESTINO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ERR_FALTA_COORDINADOR_DESTINO");
        Siget.Lang.FileSystem.Alumno.Enviar.ERR_FALTA_COORDINADOR_DESTINO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NOTIFICACION_ASUNTO");
        Siget.Lang.FileSystem.Alumno.Enviar.MSG_NOTIFICACION_ASUNTO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NOTIFICACION_CUERPO");
        Siget.Lang.FileSystem.Alumno.Enviar.MSG_NOTIFICACION_CUERPO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_ENVIADO_PROFESOR");
        Siget.Lang.FileSystem.Alumno.Enviar.MSG_ENVIADO_PROFESOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_ENVIADO_COORDINADOR");
        Siget.Lang.FileSystem.Alumno.Enviar.MSG_ENVIADO_COORDINADOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_ListaReactivos(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\ListaReactivos.xml"))
      {

        langFile.ReadToFollowing("LT_TITULO");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_TITULO_PAGINA");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.LT_TITULO_PAGINA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_CONTINUAR");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.BTN_CONTINUAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LB_REACTIVOS_MULTIPLES");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.LB_REACTIVOS_MULTIPLES[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LB_REACTIVOS_ABIERTA");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.LB_REACTIVOS_ABIERTA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TABLA_PLANTEAMIENTO");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.TABLA_PLANTEAMIENTO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TABLA_APOYO_1");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.TABLA_APOYO_1[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TABLA_APOYO_2");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.TABLA_APOYO_2[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TABLA_OPORTUNIDAD");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.TABLA_OPORTUNIDAD[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TABLA_PENALIZACION");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.TABLA_PENALIZACION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TABLA_OK");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.TABLA_OK[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TABLA_CONSECUTIVO");
         Siget.Lang.FileSystem.Alumno.ListaReactivos.TABLA_CONSECUTIVO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_HAY_REACTIVOS_MULTIPLES");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.MSG_NO_HAY_REACTIVOS_MULTIPLES[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TABLA_RESPUESTA");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.TABLA_RESPUESTA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_REVISA_POSTERIORMENTE");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.MSG_REVISA_POSTERIORMENTE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_HAY_REACTIVOS_ABIERTOS");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.MSG_NO_HAY_REACTIVOS_ABIERTOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("lt_resultado1");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.lt_resultado1[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("lt_resultado2");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.lt_resultado2[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("lt_segundavuelta");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.lt_segundavuelta[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("lt_repite1");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.lt_repite1[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("lt_repite2_repite");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.lt_repite2_repite[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("lt_repite2_norepite");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.lt_repite2_norepite[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("lt_repite_intentos1_uno");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.lt_repite_intentos1_uno[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("lt_repite_intentos1_parte1");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.lt_repite_intentos1_parte1[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("lt_repite_intentos1_parte2");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.lt_repite_intentos1_parte2[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("btnAceptarDialogo");
        Siget.Lang.FileSystem.Alumno.ListaReactivos.btnAceptarDialogo[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_Master_AgregarCursos(string lenguaje)
    {
        using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\Master_AgregarCursos.xml"))
        {

            langFile.ReadToFollowing("LT_CURSOS_AGREGAR");
            Siget.Lang.FileSystem.Alumno.Master_AgregarCursos.LT_CURSOS_AGREGAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("LT_CURSOS_INSCRITOS");
            Siget.Lang.FileSystem.Alumno.Master_AgregarCursos.LT_CURSOS_INSCRITOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.ReadToFollowing("LT_CURSOS_BORRAR");
            Siget.Lang.FileSystem.Alumno.Master_AgregarCursos.LT_CURSOS_BORRAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

            langFile.Close();
        }
    }

    public static void importa_Alumno_Master_Comunicacion(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\Master_Comunicacion.xml"))
      {

        langFile.ReadToFollowing("LT_ENVIAR");
        Siget.Lang.FileSystem.Alumno.Master_Comunicacion.LT_ENVIAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_ENTRADA");
        Siget.Lang.FileSystem.Alumno.Master_Comunicacion.LT_ENTRADA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_SALIDA");
        Siget.Lang.FileSystem.Alumno.Master_Comunicacion.LT_SALIDA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_Master_Principal(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\Master_Principal.xml"))
      {

        langFile.ReadToFollowing("LT_LOGOUT_BUTTON");
        Siget.Lang.FileSystem.Alumno.Master_Principal.LT_LOGOUT_BUTTON[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_LOGOUT_BUTTON_UNAUTHORIZED");
        Siget.Lang.FileSystem.Alumno.Master_Principal.LT_LOGOUT_BUTTON_UNAUTHORIZED[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_CURSOS");
        Siget.Lang.FileSystem.Alumno.Master_Principal.LT_CURSOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_COMUNICACION");
        Siget.Lang.FileSystem.Alumno.Master_Principal.LT_COMUNICACION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_BIBLIOTECA");
        Siget.Lang.FileSystem.Alumno.Master_Principal.LT_BIBLIOTECA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_INSCRIPCION");
        Siget.Lang.FileSystem.Alumno.Master_Principal.LT_INSCRIPCION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_BLOG");
        Siget.Lang.FileSystem.Alumno.Master_Principal.LT_BLOG[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_RIGHTS_RESERVED");
        Siget.Lang.FileSystem.Alumno.Master_Principal.LT_RIGHTS_RESERVED[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("HL_CREDITOS");
        Siget.Lang.FileSystem.Alumno.Master_Principal.HL_CREDITOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_Mensaje(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\Mensaje.xml"))
      {

        langFile.ReadToFollowing("LT_TITULO");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_TITULO_PAGINA_ENTRADA_PROFESOR");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_TITULO_PAGINA_ENTRADA_PROFESOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_TITULO_PAGINA_ENTRADA_COORDINADOR");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_TITULO_PAGINA_ENTRADA_COORDINADOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_TITULO_PAGINA_SALIDA_PROFESOR");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_TITULO_PAGINA_SALIDA_PROFESOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_TITULO_PAGINA_SALIDA_COORDINADOR");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_TITULO_PAGINA_SALIDA_COORDINADOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_PROFESOR_RECIBE");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_HINT_PROFESOR_RECIBE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_COORDINADOR_RECIBE");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_HINT_COORDINADOR_RECIBE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_PROFESOR_ENVIA");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_HINT_PROFESOR_ENVIA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_COORDINADOR_ENVIA");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_HINT_COORDINADOR_ENVIA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_FECHA");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_HINT_FECHA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_ASUNTO");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_HINT_ASUNTO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_CONTENIDO");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_HINT_CONTENIDO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_ADJUNTO");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_HINT_ADJUNTO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("CBELIMINAR");
        Siget.Lang.FileSystem.Alumno.Mensaje.CBELIMINAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_BTN_RESPONDER");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_BTN_RESPONDER[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_BTN_ELIMINAR");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_BTN_ELIMINAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_BTN_REENVIAR");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_BTN_REENVIAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_BTN_REGRESAR");
        Siget.Lang.FileSystem.Alumno.Mensaje.LT_BTN_REGRESAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ERR_MARCAR_PARA_ELIMINAR");
        Siget.Lang.FileSystem.Alumno.Mensaje.ERR_MARCAR_PARA_ELIMINAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ERR_ADJUNTO_NO_DISPONIBLE");
        Siget.Lang.FileSystem.Alumno.Mensaje.ERR_ADJUNTO_NO_DISPONIBLE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_MisDatos(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\MisDatos.xml"))
      {

        langFile.ReadToFollowing("LT_TITULO");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_TITULO_PAGINA");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_TITULO_PAGINA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_PASSWORD_CHANGE_TEXT");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_PASSWORD_CHANGE_TEXT[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HERE_PASSWORD_CHANGE");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_HERE_PASSWORD_CHANGE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("TxtIdioma");
        Siget.Lang.FileSystem.Alumno.MisDatos.TxtIdioma[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_GRUPO");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_GRUPO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_MATRICULA");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_MATRICULA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_NOMBRE");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_NOMBRE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_APPAT");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_APPAT[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_APMAT");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_APMAT[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_EQUIPO");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_EQUIPO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_SUBEQUIPO");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_SUBEQUIPO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_EMAIL");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_EMAIL[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("hint_infoEmail_1");
        Siget.Lang.FileSystem.Alumno.MisDatos.hint_infoEmail_1[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("lt_email_2");
        Siget.Lang.FileSystem.Alumno.MisDatos.lt_email_2[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("hint_infoEmail_2");
        Siget.Lang.FileSystem.Alumno.MisDatos.hint_infoEmail_2[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("facebook");
        Siget.Lang.FileSystem.Alumno.MisDatos.facebook[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("twitter");
        Siget.Lang.FileSystem.Alumno.MisDatos.twitter[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_SEXO");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_SEXO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_CURP");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_CURP[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_PUESTO");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_PUESTO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_DISCAPACIDAD");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_DISCAPACIDAD[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_ESTADO_CIVIL");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_ESTADO_CIVIL[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_NIVEL_ESTUDIOS");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_NIVEL_ESTUDIOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_TITULO_ESTUDIOS");
          Siget.Lang.FileSystem.Alumno.MisDatos.LT_TITULO_ESTUDIOS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_SELECCIONE_SU_SEXO");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_SELECCIONE_SU_SEXO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_FEMENINO");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_FEMENINO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_MASCULINO");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_MASCULINO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_SI");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_SI[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_NO");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_NO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_DISCAPACIDAD_ESPECIFICA");
        Siget.Lang.FileSystem.Alumno.MisDatos.LT_DISCAPACIDAD_ESPECIFICA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_ACTUALIZAR");
        Siget.Lang.FileSystem.Alumno.MisDatos.BTN_ACTUALIZAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_CERTIFICADO");
        Siget.Lang.FileSystem.Alumno.MisDatos.BTN_CERTIFICADO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);


          langFile.ReadToFollowing("LT_TITULO_CERTIFICADO");
          Siget.Lang.FileSystem.Alumno.MisDatos.LT_TITULO_CERTIFICADO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);
                
         langFile.ReadToFollowing("MSG_UPDATED");
        Siget.Lang.FileSystem.Alumno.MisDatos.MSG_UPDATED[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_Procesa(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\Contestar_Procesa.xml"))
      {

        langFile.ReadToFollowing("LT_TITULO");
        Siget.Lang.FileSystem.Alumno.Contestar_Procesa.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_BTN_REGRESAR");
        Siget.Lang.FileSystem.Alumno.Contestar_Procesa.LT_BTN_REGRESAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_Procesa2(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\Corregir_Procesa.xml"))
      {

        langFile.ReadToFollowing("LT_TITULO");
        Siget.Lang.FileSystem.Alumno.Corregir_Procesa.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_BTN_REGRESAR");
        Siget.Lang.FileSystem.Alumno.Corregir_Procesa.LT_BTN_REGRESAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_ProcesaDemo(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\ContestarDemo_Procesa.xml"))
      {

        langFile.ReadToFollowing("LT_TITULO");
        Siget.Lang.FileSystem.Alumno.ContestarDemo_Procesa.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_BTN_REGRESAR");
        Siget.Lang.FileSystem.Alumno.ContestarDemo_Procesa.LT_BTN_REGRESAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_SIGUIENTE");
        Siget.Lang.FileSystem.Alumno.ContestarDemo_Procesa.BTN_SIGUIENTE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_REINICIAR");
        Siget.Lang.FileSystem.Alumno.ContestarDemo_Procesa.BTN_REINICIAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("BTN_TERMINAR");
        Siget.Lang.FileSystem.Alumno.ContestarDemo_Procesa.BTN_TERMINAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_INCORRECTA");
        Siget.Lang.FileSystem.Alumno.ContestarDemo_Procesa.MSG_INCORRECTA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_CORRECTA");
        Siget.Lang.FileSystem.Alumno.ContestarDemo_Procesa.MSG_CORRECTA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_FALTAN_TOTALES");
        Siget.Lang.FileSystem.Alumno.ContestarDemo_Procesa.MSG_FALTAN_TOTALES[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_Responder(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\Responder.xml"))
      {

        langFile.ReadToFollowing("LT_PAGE_TITLE");
        Siget.Lang.FileSystem.Alumno.Responder.LT_PAGE_TITLE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_PROFESOR");
        Siget.Lang.FileSystem.Alumno.Responder.LT_HINT_PROFESOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_COORDINADOR");
        Siget.Lang.FileSystem.Alumno.Responder.LT_HINT_COORDINADOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_ASIGNATURA");
        Siget.Lang.FileSystem.Alumno.Responder.LT_HINT_ASIGNATURA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_PLANTEL");
        Siget.Lang.FileSystem.Alumno.Responder.LT_HINT_PLANTEL[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_ASUNTO");
        Siget.Lang.FileSystem.Alumno.Responder.LT_HINT_ASUNTO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_CONTENIDO");
        Siget.Lang.FileSystem.Alumno.Responder.LT_HINT_CONTENIDO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HINT_ADJUNTAR");
        Siget.Lang.FileSystem.Alumno.Responder.LT_HINT_ADJUNTAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_BTN_ADJUNTAR");
        Siget.Lang.FileSystem.Alumno.Responder.LT_BTN_ADJUNTAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_BTN_QUITAR");
        Siget.Lang.FileSystem.Alumno.Responder.LT_BTN_QUITAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_BTN_ENVIAR");
        Siget.Lang.FileSystem.Alumno.Responder.LT_BTN_ENVIAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_BTN_LIMPIAR");
        Siget.Lang.FileSystem.Alumno.Responder.LT_BTN_LIMPIAR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_HL_REGRESARMENSAJES");
        Siget.Lang.FileSystem.Alumno.Responder.LT_HL_REGRESARMENSAJES[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_ADJUNTADO");
        Siget.Lang.FileSystem.Alumno.Responder.MSG_ADJUNTADO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_DESADJUNTADO");
        Siget.Lang.FileSystem.Alumno.Responder.MSG_DESADJUNTADO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ERR_FALTA_ARCHIVO");
        Siget.Lang.FileSystem.Alumno.Responder.ERR_FALTA_ARCHIVO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ERR_NO_HA_ADJUNTADO");
        Siget.Lang.FileSystem.Alumno.Responder.ERR_NO_HA_ADJUNTADO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ERR_ADJUNTO_NO_DISPONIBLE");
        Siget.Lang.FileSystem.Alumno.Responder.ERR_ADJUNTO_NO_DISPONIBLE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("ERR_FALTA_CONTENIDO");
        Siget.Lang.FileSystem.Alumno.Responder.ERR_FALTA_CONTENIDO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NOTIFICACION_ASUNTO");
        Siget.Lang.FileSystem.Alumno.Responder.MSG_NOTIFICACION_ASUNTO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NOTIFICACION_CUERPO");
        Siget.Lang.FileSystem.Alumno.Responder.MSG_NOTIFICACION_CUERPO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_ENVIADO_PROFESOR");
        Siget.Lang.FileSystem.Alumno.Responder.MSG_ENVIADO_PROFESOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_ENVIADO_COORDINADOR");
        Siget.Lang.FileSystem.Alumno.Responder.MSG_ENVIADO_COORDINADOR[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

    public static void importa_Alumno_Salida(string lenguaje)
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaLenguajes + "\\" + lenguaje + "\\Alumno\\Salida.xml"))
      {

        langFile.ReadToFollowing("LT_TITULO");
        Siget.Lang.FileSystem.Alumno.Salida.LT_TITULO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("LT_TITULO_PAGINA");
        Siget.Lang.FileSystem.Alumno.Salida.LT_TITULO_PAGINA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("MSG_NO_HAY_MENSAJES");
        Siget.Lang.FileSystem.Alumno.Salida.MSG_NO_HAY_MENSAJES[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_SELECT");
        Siget.Lang.FileSystem.Alumno.Salida.GV_SELECT[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_FECHA");
        Siget.Lang.FileSystem.Alumno.Salida.GV_FECHA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_ASUNTO");
        Siget.Lang.FileSystem.Alumno.Salida.GV_ASUNTO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_ESTATUS");
        Siget.Lang.FileSystem.Alumno.Salida.GV_ESTATUS[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_ESTATUS_LEIDO");
        Siget.Lang.FileSystem.Alumno.Salida.GV_ESTATUS_LEIDO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_ESTATUS_NUEVO");
        Siget.Lang.FileSystem.Alumno.Salida.GV_ESTATUS_NUEVO[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GV_ESTATUS_BAJA");
        Siget.Lang.FileSystem.Alumno.Salida.GV_ESTATUS_BAJA[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GVMENSAJESPROFESORES_CAPTION");
        Siget.Lang.FileSystem.Alumno.Salida.GVMENSAJESPROFESORES_CAPTION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GVMENSAJESPROFESORES_PROFE_RECIBE");
        Siget.Lang.FileSystem.Alumno.Salida.GVMENSAJESPROFESORES_PROFE_RECIBE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GVMENSAJESCOORDINADORES_CAPTION");
        Siget.Lang.FileSystem.Alumno.Salida.GVMENSAJESCOORDINADORES_CAPTION[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.ReadToFollowing("GVMENSAJESCOORDINADORES_COORD_RECIBE");
        Siget.Lang.FileSystem.Alumno.Salida.GVMENSAJESCOORDINADORES_COORD_RECIBE[lenguaje] = Lang_Config.parseTags(langFile.ReadString(), lenguaje);

        langFile.Close();
      }
    }

  }

}