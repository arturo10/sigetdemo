﻿using System;
// 
// DataAccessUtils.vb
//
//
// Algunas funciones de esta clase utilizan reflexión para iterar a través de las propiedades del 
// objeto de transferencia;
// http://en.wikipedia.org/wiki/Reflection_%28computer_programming%29

using System.Data.SqlClient;
// conexiones sql
using System.Reflection;
// reflexión de vb
using System.ComponentModel;
// conversor de tipos

using Siget.TransferObjects;
// objetos de transferencia de sadcome

using System.Configuration;
using System.Text;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Collections.Specialized;
using System.Data;
using System.IO;

namespace Siget.DataAccess
{

  /// <summary>
  /// Clase de utilidades para objetos de acceso de datos
  /// </summary>
  public class DataAccessUtils
  {

    public static string InsertableDateTime(DateTime input)
    {
      return input.ToString("dd/MM/yyyy HH:mm:ss");
    }

    public static string InsertableDateOnly(DateTime input)
    {
      return input.ToShortDateString();
    }

    /// <summary>
    /// Cadena de conexión de la base de datos de negocios.
    /// </summary>
    public static string SadcomeConnectionString
    {
      get { return ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString; }
    }

    /// <summary>
    /// Función de vaciado de un set de resultados Sql a un objeto de transferencia.
    /// </summary>
    /// <param name="resultados">
    /// El objeto de datos Sql
    /// </param>
    /// <param name="objeto">
    /// El objeto a llenar
    /// </param>
    /// <remarks>
    /// En lugar de generar una función para cada objeto de transferencia se itera a través de 
    /// las propiedades públicas del objeto recibido.
    /// </remarks>
    public static void LlenaTransferObject(ref TransferObject objeto, ref SqlDataReader resultados)
    {
      try
      {
        Type tipo = objeto.GetType();

        // obtiene solo las propiedades de instancia y públicas
        BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;

        PropertyInfo[] propiedades = tipo.GetProperties(flags);

        foreach (PropertyInfo propiedad in propiedades)
        {
          // si está definida en el origen de datos
          if (resultados[propiedad.Name.ToString()] != DBNull.Value)
          {
            // si no es nulo ese resultado
            if (resultados[propiedad.Name.ToString()] != null)
            {
              // obtengo el conversor de la propiedad actual para establecer el valor
              TypeConverter convertidor = TypeDescriptor.GetConverter(propiedad.PropertyType);

              propiedad.SetValue(objeto, convertidor.ConvertFrom(resultados[propiedad.Name.ToString()].ToString()), null);
            }
          }
        }

      }
      catch (Exception ex)
      {
        Utils.LogManager.ExceptionLog_InsertEntry(ex);
        throw ex;
      }
    }

    /// <summary>
    /// Genera parámetros para un comando sql a partir de un objeto de transferencia. 
    /// Los nombres de parámetros se introducen con formato @PARAM.
    /// </summary>
    /// <param name="objeto">
    /// El objeto desde donde se generan parámetros.
    /// </param>
    /// <param name="command">
    /// El comando sql que recibe los parámetros.
    /// </param>
    /// <remarks>
    /// En lugar de generar una función para cada objeto de transferencia se itera a través de 
    /// las propiedades públicas del objeto recibido.
    /// </remarks>

    public static void ParametrizaTransferObject(ref SqlCommand command, ref TransferObject objeto)
    {
      try
      {
        Type tipo = objeto.GetType();

        // obtiene solo las propiedades de instancia y públicas
        BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;

        PropertyInfo[] propiedades = tipo.GetProperties(flags);

        foreach (PropertyInfo propiedad in propiedades)
        {
          if ((propiedad.GetValue(objeto) == null))
          {
            command.Parameters.AddWithValue("@" + propiedad.Name.ToString(), DBNull.Value);
          }
          else
          {
            if ((propiedad.GetValue(objeto)) is DateTime)
            {
              command.Parameters.AddWithValue("@" + propiedad.Name.ToString(), ((DateTime)propiedad.GetValue(objeto)).ToString("dd/MM/yyyy HH:mm:ss"));
            }
            else
            {
              command.Parameters.AddWithValue("@" + propiedad.Name.ToString(), propiedad.GetValue(objeto).ToString());
            }
          }
        }

      }
      catch (Exception ex)
      {
        Utils.LogManager.ExceptionLog_InsertEntry(ex);
        throw ex;
      }
    }



    public static void fillFromGridToControls(IOrderedDictionary dictionary, ControlCollection controls,TransferObject transferObject)
    {
           Type tipo = transferObject.GetType();
           BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;
           PropertyInfo[] propiedades = tipo.GetProperties(flags);
           System.Collections.IEnumerator myEnumerator = controls.GetEnumerator();

          foreach (PropertyInfo propiedad in propiedades)
          {
              while (myEnumerator.MoveNext())
              {
                  Control controlCurrent = (Control)myEnumerator.Current;
                  if (dictionary.Contains(propiedad.Name.ToString()) && 
                      (controlCurrent is TextBox || controlCurrent is DropDownList))
                  {

                      if (controlCurrent is TextBox)
                          ((TextBox)controlCurrent).Text = dictionary[propiedad.Name.ToString()].ToString().Trim();
                      else if (controlCurrent is DropDownList)
                          if (dictionary[propiedad.Name.ToString()].ToString().Trim() != string.Empty)
                              ((DropDownList)controlCurrent).SelectedValue = dictionary[propiedad.Name.ToString()].ToString().Trim();
                          else
                              ((DropDownList)controlCurrent).SelectedValue = "0";
                      break;
                  }
              }
           }
        
      }

  }

}