
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// IndicadorDa.vb

// DataSet

using System.Data.SqlClient;
// SqlDataAdapter
using System.Reflection;
// reflexión de vb
using System.ComponentModel;
// conversor de tipos

using Siget.TransferObjects;

namespace Siget.DataAccess
{

	/// <summary>
	/// Contiene métodos de acceso de datos relacionados a indicadores.
	/// </summary>
	public class IndicadorDa
	{

		public IndicadorTo obten_IdIndicador(string idIndicador)
		{
			// el objeto que reportará la búsqueda
			IndicadorTo indicador = null;

			try {
				using (SqlConnection conn = new SqlConnection(DataAccessUtils.SadcomeConnectionString)) {
					conn.Open();
					SqlCommand cmd = new SqlCommand("SELECT * " + "FROM " + "   Indicador " + "WHERE " + "   IdIndicador = @IdIndicador ", conn);

					cmd.Parameters.AddWithValue("@IdIndicador", idIndicador);

					SqlDataReader resultado = cmd.ExecuteReader();

					if (resultado.Read()) {
						indicador = new IndicadorTo();

                        try
                        {
                          Type tipo = indicador.GetType();

                          // obtiene solo las propiedades de instancia y públicas
                          BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;

                          PropertyInfo[] propiedades = tipo.GetProperties(flags);

                          foreach (PropertyInfo propiedad in propiedades)
                          {
                            // si está definida en el origen de datos
                            if (resultado[propiedad.Name.ToString()] != DBNull.Value)
                            {
                              // si no es nulo ese resultado
                              if (resultado[propiedad.Name.ToString()] != null)
                              {
                                // obtengo el conversor de la propiedad actual para establecer el valor
                                TypeConverter convertidor = TypeDescriptor.GetConverter(propiedad.PropertyType);

                                propiedad.SetValue(indicador, convertidor.ConvertFrom(resultado[propiedad.Name.ToString()].ToString()), null);
                              }
                            }
                          }

                        }
                        catch (Exception ex)
                        {
                          Utils.LogManager.ExceptionLog_InsertEntry(ex);
                          throw ex;
                        }
					}

					resultado.Close();
					cmd.Dispose();
					conn.Close();
				}
			} catch (Exception ex) {
				Utils.LogManager.ExceptionLog_InsertEntry(ex);
				throw ex;
			}

			return indicador;
		}

		/// <summary>
		/// Obtiene la [Descripcion] (como 'Text') y el [IdAsignatura] (como 'Value') de todos los 
		/// registros de [Asignatura] cuyo [IdGrado] coincida con el recibido, ordenados 
		/// por [Descripcion].
		/// </summary>
		public DataSet ObtenTextoValor_IdGrado(string idGrado)
		{
			DataSet dst = new DataSet();

			using (SqlConnection conn = new SqlConnection(DataAccessUtils.SadcomeConnectionString)) {
				conn.Open();
				using (SqlDataAdapter sqlAdapter = new SqlDataAdapter()) {
					sqlAdapter.SelectCommand = new SqlCommand();
					sqlAdapter.SelectCommand.Connection = conn;
					sqlAdapter.SelectCommand.CommandText = " SELECT " + "   Descripcion AS 'Text' " + "   ,IdAsignatura AS 'Value' " + " FROM " + "   Asignatura " + " WHERE " + "   IdGrado = @IdGrado " + " ORDER BY " + "   Descripcion ";

					sqlAdapter.SelectCommand.Parameters.AddWithValue("@IdGrado", idGrado);

					sqlAdapter.Fill(dst);
				}
			}

			return dst;
		}

	}

}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
