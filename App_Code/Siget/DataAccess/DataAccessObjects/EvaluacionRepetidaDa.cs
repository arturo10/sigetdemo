
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// EvaluacionRepetidaDa.vb

// DataSet
using System.Data.SqlClient;
using System.Configuration;
// SqlDataAdapter

using Siget.TransferObjects;

namespace Siget.DataAccess
{

	/// <summary>
	/// Contiene métodos de acceso de datos relacionados a evaluaciones repetidas.
	/// </summary>
	public class EvaluacionRepetidaDa
	{

		/// <summary>
		/// Inserta un registro en la tabla EvaluacionRepetida utilizando 
		/// valores de la evaluación terminada y otros parámetros recibidos.
		/// </summary>
		/// <param name="evaluacionTerminada"></param>
		/// <param name="totalReactivos"></param>
		/// <param name="reactivosContestados"></param>
		/// <param name="puntosPosibles"></param>
		/// <param name="puntosAcertados"></param>
		/// <param name="modifico"></param>
		/// <remarks></remarks>

		public void Inserta(ref EvaluacionTerminadaTo evaluacionTerminada, byte totalReactivos, byte reactivosContestados, byte puntosPosibles, byte puntosAcertados, string modifico)
		{
			try {
				using (SqlConnection conn = new SqlConnection(DataAccessUtils.SadcomeConnectionString)) {
					conn.Open();

					using (SqlCommand cmd = new SqlCommand()) {
						cmd.Connection = conn;

						cmd.CommandText = "SET dateformat dmy; " + "INSERT INTO EvaluacionRepetida( " + "   IdEvaluacionT, " + "   IdAlumno, " + "   IdGrado, " + "   IdGrupo, " + "   IdCicloEscolar, " + "   IdEvaluacion, " + "   TotalReactivos, " + "   ReactivosContestados, " + "   PuntosPosibles, " + "   PuntosAcertados, " + "   Resultado, " + "   FechaTermino, " + "   Intento, " + "   Modifico, " + "   FechaModif " + " ) VALUES ( " + "   @IdEvaluacionT, " + "   @IdAlumno, " + "   @IdGrado, " + "   @IdGrupo, " + "   @IdCicloEscolar, " + "   @IdEvaluacion, " + "   @TotalReactivos, " + "   @ReactivosContestados, " + "   @PuntosPosibles, " + "   @PuntosAcertados, " + "   @Resultado, " + "   @FechaTermino, " + "   @Intento, " + "   @Modifico, " + "   getdate())";

						cmd.Parameters.AddWithValue("@IdEvaluacionT", evaluacionTerminada.IdEvaluacionT.ToString());
						cmd.Parameters.AddWithValue("@IdAlumno", evaluacionTerminada.IdAlumno.ToString());
						cmd.Parameters.AddWithValue("@IdGrado", evaluacionTerminada.IdGrado.ToString());
						cmd.Parameters.AddWithValue("@IdGrupo", evaluacionTerminada.IdGrupo.ToString());
						cmd.Parameters.AddWithValue("@IdCicloEscolar", evaluacionTerminada.IdCicloEscolar.ToString());
						cmd.Parameters.AddWithValue("@IdEvaluacion", evaluacionTerminada.IdEvaluacion.ToString());
						cmd.Parameters.AddWithValue("@TotalReactivos", totalReactivos.ToString());
						cmd.Parameters.AddWithValue("@ReactivosContestados", reactivosContestados.ToString());
						cmd.Parameters.AddWithValue("@PuntosPosibles", puntosPosibles.ToString());
						cmd.Parameters.AddWithValue("@PuntosAcertados", puntosAcertados.ToString());
						cmd.Parameters.AddWithValue("@Resultado", evaluacionTerminada.Resultado.ToString());
						cmd.Parameters.AddWithValue("@FechaTermino", DataAccessUtils.InsertableDateTime(evaluacionTerminada.FechaTermino.Value));
						cmd.Parameters.AddWithValue("@Intento", 1 + CuentaRepeticiones(evaluacionTerminada.IdEvaluacion.ToString(), evaluacionTerminada.IdAlumno.ToString(), evaluacionTerminada.IdCicloEscolar.ToString()));
						cmd.Parameters.AddWithValue("@Modifico", modifico);

						cmd.ExecuteNonQuery();
					}
				}
			} catch (Exception ex) {
				Utils.LogManager.ExceptionLog_InsertEntry(ex);
				throw;
			}

		}

		/// <summary>
		/// Cuenta el número de registros que un alumno tiene en la 
		/// tabla EvaluacionRepetida para una evaluación dada.
		/// </summary>
		/// <param name="idEvaluacion"></param>
		/// <param name="idAlumno"></param>
		/// <returns>
		/// Número de veces que un alumno ha intentado una evaluación y obtenido 
		/// una calificación insatisfactoria.
		/// </returns>
		/// <remarks></remarks>
        public int CuentaRepeticiones(string idEvaluacion, string idAlumno, string idCicloEscolar)
        {
            int cuenta = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(DataAccessUtils.SadcomeConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = conn;

                        cmd.CommandText = "SELECT Count(*) AS 'Cuenta'" + "FROM EvaluacionRepetida " + "WHERE " + "   IdEvaluacion = @IdEvaluacion " + "   AND IdAlumno = @IdAlumno " + "   AND IdCicloEscolar = @IdCicloEscolar";

                        cmd.Parameters.AddWithValue("@IdEvaluacion", idEvaluacion);
                        cmd.Parameters.AddWithValue("@IdAlumno", idAlumno);
                        cmd.Parameters.AddWithValue("@IdCicloEscolar", idCicloEscolar);

                        using (SqlDataReader resultado = cmd.ExecuteReader())
                        {
                            if (resultado.Read())
                            {
                                cuenta = int.Parse(resultado["Cuenta"].ToString());
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.LogManager.ExceptionLog_InsertEntry(ex);
                throw;
            }

            return cuenta;
        }
            
 
	}

}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
