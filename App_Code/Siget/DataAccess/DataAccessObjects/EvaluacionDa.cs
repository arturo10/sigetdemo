
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;

// 
// EvaluacionDa.vb

// DataSet

using System.Configuration;
// SqlDataAdapter

using Siget.Beans;
using Siget.TransferObjects;

namespace Siget.DataAccess
{

    /// <summary>
    /// Contiene métodos de acceso de datos relacionados a evaluaciones.
    /// </summary>
    public class EvaluacionDa
    {

        /// <summary>
        /// Obtiene el nombre de la evaluación señalada por el Id recibido.
        /// </summary>
        /// <param name="id">El id de la evaluación a buscar.</param>
        /// <returns></returns>
        /// <remarks>Si no se encuentra actividad con ese Id, reporta vacío: ""</remarks>
        public string ObtenNombreDeEvaluacion(int id)
        {
            string strCon = null;
            strCon = ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString;
            SqlConnection objCon = new SqlConnection(strCon);
            SqlCommand Com = default(SqlCommand);
            SqlDataReader Registros = default(SqlDataReader);
            Com = objCon.CreateCommand();

            // IdOperador
            Com.CommandText = "SELECT ClaveBateria FROM Evaluacion WHERE IdEvaluacion = @IdEvaluacion";
            Com.Parameters.AddWithValue("@IdEvaluacion", id);
            try
            {
                objCon.Open();
                Registros = Com.ExecuteReader();
                //Creo conjunto de registros
                if (Registros.Read())
                {
                    return " ''" + Registros["ClaveBateria"].ToString() + "''";
                }
                Registros.Close();
                objCon.Close();
            }
            catch (Exception ex)
            {
            }

            return "";
        }

        /// <summary>
        /// Determina si la actividad indicada por el idEvaluacion tiene reactivos o no.
        /// </summary>
        /// <param name="idEvaluacion"></param>
        /// <param name="connection"></param>
        /// <returns>True si tiene reactivos, False de lo contrario</returns>
        /// <remarks>La conexión ya debe estar abierta</remarks>
        public bool Evaluacion_TieneReactivos(string idEvaluacion, ref SqlConnection connection)
        {
            SqlCommand command = default(SqlCommand);
            SqlDataReader results = default(SqlDataReader);
            command = connection.CreateCommand();

            command.CommandText = @"SELECT  P.IdPlanteamiento FROM Planteamiento P  JOIN
                                    (SELECT distinct D.IdSubtema
                                     FROM Planteamiento P 
                                        JOIN DetalleEvaluacion D  
	                                        ON D.IdSubtema=P.IdSubtema 
                                        JOIN Evaluacion E ON
                                        D.IdEvaluacion=E.IdEvaluacion
	                                    AND E.IdEvaluacion=@IdEvaluacion) AS TP
	                                ON P.IdSubtema=TP.IdSubtema
                                    WHERE P.IdPlanteamiento not in
                                            (SELECT IdPlanteamiento 
                                              FROM EvaluacionDemo 
                                              WHERE IdEvaluacion=@IdEvaluacion)";

            command.Parameters.AddWithValue("@IdEvaluacion", idEvaluacion);

            results = command.ExecuteReader();
            if (results.Read())
            {
                results.Close();
                return true;
            }
            else
            {
                results.Close();
                return false;
            }
        }

        /// <summary>
        /// Reporta un objeto evaluación que contiene toda la información de ese registro en la base de datos.
        /// </summary>
        /// <param name="idEvaluacion">El identificador de la evaluación a buscar</param>
        /// <returns>Nothing cuando no se encuentra, un objeto Evalaucion de lo contrario.</returns>
        public Evaluacion ObtenDatosEvaluacion(string idEvaluacion)
        {
            Evaluacion evaluacion = null;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
            {
                conn.Open();
                //Calificacion --> Evaluacion --> DetalleEvaluacion --> Subtema --> Planteamiento
                // Aquí obtengo los subtemas asignados a la actividad, 
                // el número de reactivos a usar de cada uno y su total de planteamientos, 
                // así como datos de la evaluación
                SqlCommand cmd = new SqlCommand("SELECT * FROM Evaluacion WHERE IdEvaluacion = @IdEvaluacion", conn);

                cmd.Parameters.AddWithValue("@IdEvaluacion", idEvaluacion);

                SqlDataReader resultado = cmd.ExecuteReader();

                if (resultado.Read())
                {
                    evaluacion = new Evaluacion();
                    evaluacion.OpcionesAleatorias = short.Parse(resultado["OpcionesAleatorias"].ToString().Trim());
                    evaluacion.IdEvaluacion = uint.Parse(resultado["IdEvaluacion"].ToString().Trim());
                    if (resultado["Alcance"] != DBNull.Value)
                    {
                        evaluacion.Alcance = short.Parse(resultado["Alcance"].ToString().Trim());
                    }
                    evaluacion.Aleatoria = short.Parse(resultado["Aleatoria"].ToString().Trim());

                    if (resultado["Califica"] != DBNull.Value && !(resultado["Califica"].ToString().Trim().Length == 0))
                    {
                        evaluacion.Califica = char.Parse(resultado["Califica"].ToString().Trim());
                    }
                    evaluacion.ClaveAbreviada = resultado["ClaveAbreviada"].ToString().Trim();
                    evaluacion.ClaveBateria = resultado["ClaveBateria"].ToString().Trim();
                    if (resultado["Estatus"] != DBNull.Value)
                    {
                        evaluacion.Estatus = resultado["Estatus"].ToString().Trim();
                    }
                    if (resultado["FechaModif"] != DBNull.Value)
                    {
                        evaluacion.FechaModif = System.DateTime.Parse(resultado["FechaModif"].ToString().Trim());
                    }
                    evaluacion.FinContestar = System.DateTime.Parse(resultado["FinContestar"].ToString().Trim());
                    evaluacion.FinSinPenalizacion = System.DateTime.Parse(resultado["FinSinPenalizacion"].ToString().Trim());
                    evaluacion.IdCalificacion = uint.Parse(resultado["IdCalificacion"].ToString().Trim());
                    evaluacion.InicioContestar = System.DateTime.Parse(resultado["InicioContestar"].ToString().Trim());
                    if (resultado["Modifico"] != DBNull.Value)
                    {
                        evaluacion.Modifico = resultado["Modifico"].ToString().Trim();
                    }
                    evaluacion.Penalizacion = decimal.Parse(resultado["Penalizacion"].ToString().Trim());
                    evaluacion.PermiteSegunda = bool.Parse(resultado["PermiteSegunda"].ToString().Trim());
                    evaluacion.Porcentaje = decimal.Parse(resultado["Porcentaje"].ToString().Trim());
                    evaluacion.SeEntregaDocto = bool.Parse(resultado["SeEntregaDocto"].ToString().Trim());
                    evaluacion.Tipo = resultado["Tipo"].ToString().Trim();
                    if (resultado["UsarReactivos"] != DBNull.Value)
                    {
                        evaluacion.UsarReactivos = uint.Parse(resultado["UsarReactivos"].ToString().Trim());
                    }
                    if (resultado["Instruccion"] != DBNull.Value)
                    {
                        evaluacion.Instruccion = resultado["Instruccion"].ToString().Trim();
                    }
                    evaluacion.RepiteAutomatica = bool.Parse(resultado["RepiteAutomatica"].ToString().Trim());
                    if (resultado["RepiteIndicador"] != DBNull.Value)
                    {
                        evaluacion.RepiteIdIndicador = uint.Parse(resultado["RepiteIndicador"].ToString().Trim());
                    }
                    if (resultado["RepiteRango"] != DBNull.Value)
                    {
                        evaluacion.RepiteRango = short.Parse(resultado["RepiteRango"].ToString().Trim());
                    }
                    if (resultado["RepiteMaximo"] != DBNull.Value)
                    {
                        evaluacion.RepiteMaximo = short.Parse(resultado["RepiteMaximo"].ToString().Trim());
                    }
                }

                resultado.Close();
                cmd.Dispose();
                conn.Close();
            }

            return evaluacion;
        }

        /// <summary>
        /// Genera la estructura de Beans de asignatura > calificación > evaluación del alumno
        /// y la almacena en el ArrayList de esta clase.
        /// </summary>
        /// <remarks></remarks>
        public ArrayList ObtenEvaluacionesConJerarquia(ref object idAlumno, ref object idPlantel, ref object idGrado, ref object idCicloEscolar)
        {
            ArrayList listaEvaluaciones = new ArrayList();

            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("Consulta_EvaluacionesConEstructura", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@IdAlumno", idAlumno.ToString());
                        cmd.Parameters.AddWithValue("@IdPlantel", idPlantel.ToString());
                        cmd.Parameters.AddWithValue("@IdGrado", idGrado.ToString());
                        cmd.Parameters.AddWithValue("@IdCicloEscolar", idCicloEscolar.ToString());

                        using (SqlDataReader results = cmd.ExecuteReader())
                        {

                            // creo los objetos temporales con índices 0: así la primera vez siempre serán diferentes y simplifica el código
                            Asignatura currentAsignatura = new Asignatura(0);
                            Calificacion currentCalificacion = new Calificacion(0);
                            Evaluacion currentEvaluacion = null;

                            uint resIdAsignatura = 0;
                            short? resIdIndicadorAsignatura = null;
                            string resAsignatura = null;

                            uint resIdCalificacion = 0;
                            short? resIdIndicadorCalificacion = null;
                            //Dim resConsecutivo As Integer
                            decimal resValorCalificacion = default(decimal);
                            string resCalificacion = null;

                            uint resIdEvaluacion = 0;
                            string resClaveBateria = null;
                            string resTipo = null;
                            decimal resPorcentajeEvaluacion = default(decimal);
                            decimal resPenalizacion = default(decimal);
                            bool resSeEntregaDocto = false;
                            System.DateTime resInicioContestar = default(System.DateTime);
                            System.DateTime resFinSinPenalizacion = default(System.DateTime);
                            System.DateTime resFincontestar = default(System.DateTime);

                            short resAlcance = 0;
                            // la consulta indica si detectó fecha de alumno, plantel o general
                            bool resBorrador = false;

                            decimal resResultado = default(decimal);
                            System.DateTime resFechaTermino = default(System.DateTime);
                            string resEntrega = null;

                            while (results.Read())
                            {
                                // obtengo y parseo todos los campos
                                resIdAsignatura = uint.Parse(results["IdAsignatura"].ToString());
                                if (results["IdIndicador_A"] != DBNull.Value)
                                {
                                    resIdIndicadorAsignatura = short.Parse(results["IdIndicador_A"].ToString());
                                }
                                resAsignatura = results["Asignatura"].ToString();
                                resIdCalificacion = uint.Parse(results["IdCalificacion"].ToString());
                                resValorCalificacion = decimal.Parse(results["Valor"].ToString());
                                //resConsecutivo = Integer.Parse(results["Consecutivo"))
                                resCalificacion = results["Calificacion"].ToString();
                                if (results["IdIndicador_C"] != DBNull.Value)
                                {
                                    resIdIndicadorCalificacion = short.Parse(results["IdIndicador_C"].ToString());
                                }
                                resIdEvaluacion = uint.Parse(results["IdEvaluacion"].ToString());
                                resPorcentajeEvaluacion = decimal.Parse(results["Porcentaje"].ToString());
                                resClaveBateria = results["ClaveBateria"].ToString();
                                resTipo = results["Tipo"].ToString();
                                resPenalizacion = decimal.Parse(results["Penalizacion"].ToString());
                                resSeEntregaDocto = Convert.ToBoolean(results["SeEntregaDocto"].ToString());
                                resInicioContestar = System.DateTime.Parse(results["InicioContestar"].ToString());
                                resFinSinPenalizacion = System.DateTime.Parse(results["FinSinPenalizacion"].ToString());
                                resFincontestar = System.DateTime.Parse(results["FinContestar"].ToString());
                                resAlcance = short.Parse(results["Alcance"].ToString());
                                resBorrador = Convert.ToBoolean(int.Parse(results["Borrador"].ToString()));
                                if (results["Resultado"] != DBNull.Value)
                                {
                                    resResultado = decimal.Parse(results["Resultado"].ToString());
                                }
                                else
                                {
                                    resResultado = -1;
                                }
                                if (results["FechaTermino"] != DBNull.Value)
                                {
                                    resFechaTermino = System.DateTime.Parse(results["FechaTermino"].ToString());
                                }
                                else
                                {
                                    resFechaTermino = System.DateTime.MinValue;
                                }
                                if (results["Entrega"] != DBNull.Value)
                                {
                                    resEntrega = results["Entrega"].ToString();
                                }
                                else
                                {
                                    resEntrega = string.Empty;
                                }

                                // *****************************************************************************
                                // Asignaturas

                                // cuando el índice cambie
                                if (!(currentAsignatura.getIdAsignatura() == resIdAsignatura))
                                {
                                    currentAsignatura = new Asignatura();
                                    // genera una nueva asignatura

                                    listaEvaluaciones.Add(currentAsignatura);
                                    // añade la asignatura al contenedor

                                    currentAsignatura.setIdAsignatura(resIdAsignatura);
                                    // establece el índice
                                    currentAsignatura.setDescripcion(resAsignatura);
                                    // establece la descripción
                                    currentAsignatura.IdIndicador = resIdIndicadorAsignatura;
                                }


                                // *****************************************************************************
                                // Calificaciones

                                // cuando el índice cambie
                                if (!(currentCalificacion.IdCalificacion == resIdCalificacion))
                                {
                                    currentCalificacion = new Calificacion();
                                    // genera una nueva asignatura

                                    currentAsignatura.getCalificaciones().Add(currentCalificacion);
                                    // añade la calificación a la asignatura

                                    currentCalificacion.IdAsignatura_Nombre = resAsignatura;
                                    // establece el nombre de la asignatura a la que pertenece

                                    currentCalificacion.Valor = resValorCalificacion;
                                    currentCalificacion.IdCalificacion = resIdCalificacion;
                                    // establece el índice
                                    currentCalificacion.Descripcion = resCalificacion;
                                    // establece descripción
                                    currentCalificacion.IdIndicador = resIdIndicadorCalificacion;
                                    //currentCalificacion.setConsecutivo(resConsecutivo) ' establece consecutivo
                                }


                                // *****************************************************************************
                                // Evaluaciones

                                currentEvaluacion = new Evaluacion();
                                // genera una nueva asignatura
                                currentCalificacion.Evaluaciones.Add(currentEvaluacion);
                                // añade la evaluacion a la calificación

                                currentEvaluacion.IdEvaluacion = resIdEvaluacion;
                                // establece el índice
                                currentEvaluacion.ClaveBateria = resClaveBateria;
                                currentEvaluacion.Tipo = resTipo;
                                currentEvaluacion.Porcentaje = resPorcentajeEvaluacion;
                                currentEvaluacion.Penalizacion = resPenalizacion;
                                currentEvaluacion.SeEntregaDocto = resSeEntregaDocto;
                                currentEvaluacion.InicioContestar = resInicioContestar;
                                currentEvaluacion.FinSinPenalizacion = resFinSinPenalizacion;
                                currentEvaluacion.FinContestar = resFincontestar;
                                currentEvaluacion.Alcance = resAlcance;
                                currentEvaluacion.Borrador = resBorrador;
                                currentEvaluacion.ResultadoTerminada = resResultado;
                                currentEvaluacion.FechaTermino = resFechaTermino;
                                currentEvaluacion.Entrega = resEntrega;

                            }

                            // DEBUGGING CHECKPOINT
                            // Imprimo la jerarquía de asignaturas > calificaciones > actividades
                            // en el label de asignaturas para verificar su estructura
                            //For Each a As Asignatura In asignaturas
                            //    lt_activityList.Text += a.ToString()

                            //    For Each c As Calificacion In a.getCalificaciones()
                            //        lt_activityList.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & c.ToString()

                            //        For Each e As Evaluacion In c.getEvaluaciones()
                            //            lt_activityList.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & e.ToString()
                            //        Next
                            //    Next
                            //Next

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.LogManager.ExceptionLog_InsertEntry(ex);
                throw ex;
            }

            return listaEvaluaciones;
        }

        public string Get_Hour_From_Subject(string IdAsignatura)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
                {
                    conn.Open();
                    string query = @"SELECT Horas FROM Asignatura where IdAsignatura=@IdAsignatura";
                    SqlCommand command = new SqlCommand(query, conn);
                    command.CommandType = CommandType.Text;

                    command.Parameters.AddWithValue("@IdAsignatura", IdAsignatura);
        
                    SqlDataReader results = command.ExecuteReader();

                    results.Read();

                    return results["Horas"].ToString();
                }

            }
            catch (Exception ex)
            {
                Utils.LogManager.ExceptionLog_InsertEntry(ex);
                return "";
            }
        }

        public string Get_Last_DateFrom_Subject(string IdAsignatura, string IdAlumno)
        {
           
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
                {
                    conn.Open();
                    string query = @"SELECT MAX(FechaTermino) AS FechaTermino FROM EvaluacionTerminada where IdEvaluacion in (
                                                SELECT IdEvaluacion FROM Evaluacion where IdCalificacion in 
                                                (SELECT IdCalificacion from Calificacion where IdAsignatura = @IdAsignatura))
                                         AND IdAlumno = @IdAlumno";
                    SqlCommand command = new SqlCommand(query, conn);
                    command.CommandType = CommandType.Text;

                    command.Parameters.AddWithValue("@IdAlumno", IdAlumno);
                    command.Parameters.AddWithValue("@IdAsignatura", IdAsignatura);
                    SqlDataReader results = command.ExecuteReader();

                    results.Read();

                    return results["FechaTermino"].ToString();
                }

            }
            catch (Exception ex)
            {
                Utils.LogManager.ExceptionLog_InsertEntry(ex);
                return "";
            }
        }
    }

}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
