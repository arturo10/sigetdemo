
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// ComunicacionDa.vb

// DataSet

using System.Data.SqlClient;
using System.Configuration;
// SqlDataAdapter

namespace Siget.DataAccess
{

	/// <summary>
	/// Contiene métodos de acceso de datos relacionados a comunicación entre perfiles.
	/// </summary>
	public class ComunicacionDa
	{

		/// <summary>
		/// Reporta el número de mensajes entrantes del alumno
		/// </summary>
		public static int CuentaMensajesAlumno(ref string idAlumno)
		{
			int msgCount = 0;
			// inicio el contador de total de mensajes

			try {
				using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString)) {
					conn.Open();

					SqlCommand cmd = new SqlCommand("SELECT Count(IdComunicacion) AS Total FROM Comunicacion WHERE IdAlumno = @IdAlumno AND Sentido = 'E' AND EstatusA = 'Nuevo'" + " UNION ALL" + " SELECT Count(IdComunicacionCA) AS Total FROM ComunicacionCA WHERE IdAlumno = @IdAlumno AND Sentido = 'E' AND EstatusA = 'Nuevo'", conn);

					cmd.Parameters.AddWithValue("@IdAlumno", idAlumno);

					SqlDataReader results = cmd.ExecuteReader();

					// la primera fila tiene el número de mensajes recibidos de Profesores
					results.Read();
					msgCount += int.Parse(results["Total"].ToString());
					// la segunda fila tiene el número de mesnajes recibidos de Coordinadores
					results.Read();
					msgCount += int.Parse(results["Total"].ToString());

					results.Close();
					cmd.Dispose();
					conn.Close();
				}
			} catch (Exception ex) {
				Utils.LogManager.ExceptionLog_InsertEntry(ex);
				throw ex;
			}

			return msgCount;
		}

		/// <summary>
		/// Reporta el número de mensajes entrantes del profesor
		/// </summary>
		public static int CuentaMensajesProfesor(ref string idProfesor)
		{
			int msgCount = 0;
			// inicio el contador de total de mensajes

			try {
				using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString)) {
					conn.Open();

					SqlCommand cmd = new SqlCommand("SELECT Count(IdComunicacion) AS Total FROM Comunicacion WHERE IdProgramacion IN (SELECT IdProgramacion FROM Programacion WHERE IdProfesor = @IdProfesor) AND Sentido = 'R' AND Estatus = 'Nuevo'" + " UNION ALL" + " SELECT Count(IdComunicacionCP) AS Total FROM ComunicacionCP WHERE IdProfesor = @IdProfesor AND Sentido = 'E' AND EstatusP = 'Nuevo'", conn);

					cmd.Parameters.AddWithValue("@IdProfesor", idProfesor);

					SqlDataReader results = cmd.ExecuteReader();

					// la primera fila tiene el número de mensajes recibidos de Profesores
					results.Read();
					msgCount += int.Parse(results["Total"].ToString());
					// la segunda fila tiene el número de mesnajes recibidos de Coordinadores
					results.Read();
					msgCount += int.Parse(results["Total"].ToString());

					results.Close();
					cmd.Dispose();
					conn.Close();
				}
			} catch (Exception ex) {
				Utils.LogManager.ExceptionLog_InsertEntry(ex);
				throw ex;
			}

			return msgCount;
		}

	}

}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
