
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// SeriacionEvaluacionesDa.vb

// DataSet
using System.Configuration;
using System.Web;
using Siget;

using System.Data.SqlClient;
// SqlDataAdapter

namespace Siget.DataAccess
{

	/// <summary>
	/// Contiene métodos de acceso de datos relacionados a la seriación de evaluaciones.
	/// </summary>
	public class SeriacionEvaluacionesDa
	{

		/// <summary>
		/// verifica si la evaluación está bloqueada por seriaciones, 
		/// y de ser así, verifica que el alumno haya terminado o 
		/// entregado documento en las actividades necesarias para 
		/// la evaluación bloqueada
		/// </summary>
		/// <param name="idEvaluacion">
		/// El id de la evaluación a verificar.
		/// </param>
		/// <param name="idAlumno">
		/// El id del alumno a verificar.
		/// </param>
		/// <returns>
		/// True si pasa la verificación (no hay seriación, o las 
		/// necesarias están terminadas); False de lo contrario.
		/// </returns>
		/// <remarks>
		/// Si falla la verificación, establece en Session["Nota"] el motivo.
		/// </remarks>
		public bool VerificaActividadesPrevias(string idEvaluacion, string idAlumno)
		{
			string strConexion = ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString;
			SqlConnection connection = new SqlConnection(strConexion);
			SqlCommand command = default(SqlCommand);
			SqlDataReader results = default(SqlDataReader);
			command = connection.CreateCommand();
			connection.Open();

			// obten información de la actividad
			command.CommandText = "SELECT SeEntregaDocto FROM Evaluacion WHERE IdEvaluacion = " + idEvaluacion;
			results = command.ExecuteReader();
			results.Read();
			bool seEntregaDocto = false;
            if (results["SeEntregaDocto"] != DBNull.Value)
            {
				seEntregaDocto = false;
			} else {
				seEntregaDocto = bool.Parse(results["SeEntregaDocto"].ToString());
			}

			// primero verifica si esta evaluación tiene requisitos de evaluaciones previas
			results.Close();
			command.CommandText = "SELECT * FROM SeriacionEvaluaciones WHERE IdEvaluacion = " + idEvaluacion;
			results = command.ExecuteReader();

			// si entra aqui es por que si tiene asignaciones
			while (results.Read()) {
				SqlConnection connection2 = new SqlConnection(strConexion);
				SqlCommand command2 = default(SqlCommand);
				SqlDataReader results2 = default(SqlDataReader);
				command2 = connection2.CreateCommand();

				// busco si ya la Terminó
				command2.CommandText = "SELECT Resultado FROM EvaluacionTerminada WHERE IdEvaluacion = " + results["IdEvaluacionPrevia"].ToString() + " and IdAlumno = " + idAlumno;
				connection2.Open();
				results2 = command2.ExecuteReader();

				if (results2.Read()) {
					// si si hay calificación de la actividad previa, chekar si hay calificación mínima.
                  if (results["Minima"] != DBNull.Value)
                  {
						// si hay calificación minima, chekar que la cumpla
						if (decimal.Parse(results2["Resultado"].ToString()) < decimal.Parse(results["Minima"].ToString())) {
							//NO PUEDE CONTINUAR Y LO MANDO DE REGRESO PORQUE NO SACÓ CALIFICACIÓN MINIMA
							HttpContext.Current.Session["Nota"] = 
                              Lang.FileSystem.App_Code.DataAccess.msg_calificacion_minima_1[HttpContext.Current.Session["Usuario_Idioma"].ToString()].ToString() + 
                              results["Minima"].ToString() + 
                              Lang.FileSystem.App_Code.DataAccess.msg_calificacion_minima_2[HttpContext.Current.Session["Usuario_Idioma"].ToString()].ToString() + 
                              ((EvaluacionDa)new EvaluacionDa()).ObtenNombreDeEvaluacion(Int32.Parse(results["IdEvaluacionPrevia"].ToString())) + 
                              Lang.FileSystem.App_Code.DataAccess.msg_calificacion_minima_3[HttpContext.Current.Session["Usuario_Idioma"].ToString()].ToString();
							//Cierro todo porque me voy a salir
							results.Close();
							connection.Close();
							results2.Close();
							connection2.Close();
							return false;
						}
					}
				} else {
					// si no hay calificación, Cheko si es de entrega de documentos la actividad, y si es, si hay un documento entregado
					if (seEntregaDocto) {
						results2.Close();
						command2.CommandText = "SELECT * FROM DoctoEvaluacion WHERE IdEvaluacion = " + results["IdEvaluacionPrevia"].ToString() + " AND IdAlumno = " + idAlumno;
						results2 = command2.ExecuteReader();
						if (!results2.Read()) {
							//NO PUEDE CONTINUAR Y LO MANDO DE REGRESO PORQUE NO HA TERMINADO LA ACTIVIDAD SERIADA
							HttpContext.Current.Session["Nota"] = 
                              Lang.FileSystem.App_Code.DataAccess.msg_falta_previa_1[HttpContext.Current.Session["Usuario_Idioma"].ToString()].ToString() + 
                                ((EvaluacionDa)new EvaluacionDa()).ObtenNombreDeEvaluacion(Int32.Parse(results["IdEvaluacionPrevia"].ToString())) + 
                                  Lang.FileSystem.App_Code.DataAccess.msg_falta_previa_2[HttpContext.Current.Session["Usuario_Idioma"].ToString()].ToString();
							//Cierro todo porque me voy a salir
							results.Close();
							connection.Close();
							results2.Close();
							connection2.Close();
							return false;
						}
					} else {
						//NO PUEDE CONTINUAR Y LO MANDO DE REGRESO PORQUE NO HA TERMINADO LA ACTIVIDAD SERIADA
						HttpContext.Current.Session["Nota"] = 
                          Lang.FileSystem.App_Code.DataAccess.msg_falta_previa_1[HttpContext.Current.Session["Usuario_Idioma"].ToString()].ToString() + 
                            ((EvaluacionDa)new EvaluacionDa()).ObtenNombreDeEvaluacion(Int32.Parse(results["IdEvaluacionPrevia"].ToString())) + 
                              Lang.FileSystem.App_Code.DataAccess.msg_falta_previa_2[HttpContext.Current.Session["Usuario_Idioma"].ToString()].ToString();
						//Cierro todo porque me voy a salir
						results.Close();
						connection.Close();
						results2.Close();
						connection2.Close();
						return false;
					}
				}
				results2.Close();
				connection2.Close();
			}
			results.Close();
			connection.Close();

			return true;
		}

	}

}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
