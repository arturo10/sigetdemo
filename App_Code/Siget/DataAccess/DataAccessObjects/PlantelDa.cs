﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Descripción breve de PlantelDa
/// </summary>
namespace Siget.DataAccess
{
    public class PlantelDa
    {
        /// <summary>
        /// Obtiene la [Descripcion] (como 'Text') y el [IdPlantel] (como 'Value') de todos los 
        /// registros de [Plantel] asignados al operador del id recibido, 
        /// ordenados por [Descripcion].
        /// </summary>
        public DataSet ObtenTextoValor_IdOperador_IdInstitucion(string idOperador, string idInstitucion)
        {
            DataSet dst = new DataSet();

            using (SqlConnection conn = new SqlConnection(DataAccessUtils.SadcomeConnectionString))
            {
                conn.Open();
                using (SqlDataAdapter sqlAdapter = new SqlDataAdapter())
                {
                    sqlAdapter.SelectCommand = new SqlCommand();
                    sqlAdapter.SelectCommand.Connection = conn;
                    sqlAdapter.SelectCommand.CommandText = " SELECT " + "   Descripcion AS 'Text' " + "   ,IdPlantel AS 'Value' " + " FROM " + "   Plantel " + " WHERE " + "   IdPlantel IN " + "       (SELECT IdPlantel " + "       FROM OperadorPlantel " + "       WHERE IdOperador = @IdOperador)" + "   AND IdInstitucion = @IdInstitucion " + " ORDER BY " + "   Descripcion ";

                    sqlAdapter.SelectCommand.Parameters.AddWithValue("@IdOperador", idOperador);
                    sqlAdapter.SelectCommand.Parameters.AddWithValue("@IdInstitucion", idInstitucion);

                    sqlAdapter.Fill(dst);
                }
            }

            return dst;
        }

    }
}