
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// CicloEscolarDa.vb

// DataSet

using System.Data.SqlClient;
// SqlDataAdapter
using Siget.Beans;

namespace Siget.DataAccess
{

	/// <summary>
	/// Contiene métodos de acceso de datos relacionados a ciclos escolares.
	/// </summary>
	public class CicloEscolarDa
	{

		/// <summary>
		/// Obtiene la [Descripcion] (como 'Text') y el [IdCicloEscolar] (como 'Value') de todos los 
		/// registros de [CicloEscolar] cuyo [Estatus] coincida con el recibido, ordenados por 
		/// [FechaInicio].
		/// </summary>
		public DataSet ObtenTextoValor_Estatus(string estatus)
		{
			DataSet dst = new DataSet();

			using (SqlConnection conn = new SqlConnection(DataAccessUtils.SadcomeConnectionString)) {
				conn.Open();
				using (SqlDataAdapter sqlAdapter = new SqlDataAdapter()) {
					sqlAdapter.SelectCommand = new SqlCommand();
					sqlAdapter.SelectCommand.Connection = conn;
					sqlAdapter.SelectCommand.CommandText = " SELECT " + "   Descripcion AS 'Text' " + "   ,IdCicloEscolar AS 'Value' " + "   ,FechaInicio " + " FROM " + "   CicloEscolar " + " WHERE " + "   Estatus = @Estatus " + " ORDER BY " + "   FechaInicio ";

					sqlAdapter.SelectCommand.Parameters.AddWithValue("@Estatus", estatus);

					sqlAdapter.Fill(dst);
				}
			}

			return dst;
		}

		/// <summary>
		/// Reporta un objeto CicloEscolar que contiene toda la información de ese registro en la base de datos.
		/// </summary>
		/// <param name="conn">SqlConnection abierta</param>
		/// <param name="idEvaluacion">El identificador de la evaluación cuyo ciclo se busca</param>
		/// <returns>Nothing cuando no se encuentra, un objeto Evalaucion de lo contrario.</returns>
		public CicloEscolar ObtenCicloDeEvaluacion(ref SqlConnection conn, string idEvaluacion)
		{
			CicloEscolar ciclo = null;

			SqlCommand cmd = new SqlCommand("SELECT " + "   CE.* " + "FROM " + "   CicloEscolar CE, " + "   Calificacion Cal, " + "   Evaluacion E " + "WHERE " + "   E.IdEvaluacion = @IdEvaluacion " + "   AND E.IdCalificacion = Cal.IdCalificacion " + "   AND Cal.IdCicloEscolar = CE.IdCicloEscolar ", conn);

			cmd.Parameters.AddWithValue("@IdEvaluacion", idEvaluacion);

			SqlDataReader resultado = cmd.ExecuteReader();

			if (resultado.Read()) {
				ciclo = new CicloEscolar();

				ciclo.IdCicloEscolar = uint.Parse(resultado["IdCicloEscolar"].ToString());
				ciclo.Descripcion = resultado["Descripcion"].ToString();
			}

			resultado.Close();
			cmd.Dispose();

			return ciclo;
		}

	}

}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
