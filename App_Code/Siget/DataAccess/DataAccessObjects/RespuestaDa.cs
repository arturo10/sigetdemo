
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// EvaluacionRepetidaDa.vb

// DataSet
using System.Data.SqlClient;
// SqlDataAdapter

using Siget.TransferObjects;
using System.Configuration;

namespace Siget.DataAccess
{

	/// <summary>
	/// Contiene métodos de acceso de datos relacionados a respuestas.
	/// </summary>
	public class RespuestaDa
	{

		/// <summary>
		/// Elimina todos los registros de la tabla Respuesta que coincidan con los 
		/// parámetros recibidos. Reporta el número de registros eliminados.
		/// </summary>
		/// <param name="idAlumno"></param>
		/// <param name="idEvaluacion"></param>
		/// <param name="idCicloEscolar"></param>
		/// <returns></returns>
		/// <remarks></remarks>
		public int Elimina(object idAlumno, object idEvaluacion, object idCicloEscolar)
		{
			int cuenta = 0;

			try {
				using (SqlConnection conn = new SqlConnection(DataAccessUtils.SadcomeConnectionString)) {
					conn.Open();

					using (SqlCommand cmd = new SqlCommand()) {

						cmd.Connection = conn;

						cmd.CommandText = " SET dateformat dmy; " + " DELETE FROM Respuesta " + " WHERE " + "   IdAlumno = @IdAlumno " + "   AND IdDetalleEvaluacion IN " + "       (SELECT IdDetalleEvaluacion " + "       FROM DetalleEvaluacion " + "       WHERE IdEvaluacion = @IdEvaluacion)" + "   AND IdCicloEscolar = @IdCicloEscolar ";

						cmd.Parameters.AddWithValue("@IdAlumno", idAlumno);
						cmd.Parameters.AddWithValue("@IdEvaluacion", idEvaluacion);
						cmd.Parameters.AddWithValue("@IdCicloEscolar", idCicloEscolar);

						cuenta = cmd.ExecuteNonQuery();
					}
				}
			} catch (Exception ex) {
				Utils.LogManager.ExceptionLog_InsertEntry(ex);
				throw;
			}

			return cuenta;
		}

		/// <summary>
		/// Reporta el número de respuestas de un alumno en un subtema específico, de acuerdo a su idDetalleEvaluacion.
		/// </summary>
		/// <param name="conn">SqlConnection abierta</param>
		/// <returns></returns>
		public int? CuentaReactivosContestadosDeSubtema(ref SqlConnection conn, string idDetalleEvaluacion, string idAlumno)
		{
			int? usar = null;

			SqlCommand cmd = new SqlCommand("SELECT " + "   COUNT(IdPlanteamiento) AS 'Total' " + "FROM " + "   Respuesta " + "WHERE " + "   IdAlumno = @IdAlumno " + "   AND IdDetalleEvaluacion = @IdDetalleEvaluacion", conn);

			cmd.Parameters.AddWithValue("@IdAlumno", idAlumno);
			cmd.Parameters.AddWithValue("@IdDetalleEvaluacion", idDetalleEvaluacion);

			SqlDataReader resultado = cmd.ExecuteReader();

			if (resultado.Read()) {
				usar = int.Parse(resultado["Total"].ToString().Trim());
			}

			resultado.Close();
			cmd.Dispose();

			return usar;
		}

		/// <summary>
		/// Indica si hay reactivos pendientes en segunda vuelta 
		/// para la actividad y alumno dados.
		/// </summary>
		/// <param name="idEvaluacion"></param>
		/// <param name="idAlumno"></param>
		/// <returns></returns>
		/// <remarks></remarks>
		public bool SegundaVueltaPendiente(string idEvaluacion, string idAlumno)
		{
			bool result = false;

			try {
				using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString)) {
					conn.Open();

					SqlCommand cmd = new SqlCommand("SELECT " + "  COUNT(*) AS 'Reactivos' " + "FROM " + "  (SELECT * FROM Respuesta WHERE IdAlumno = @IdAlumno) R, " + "  (" + "    SELECT * FROM Planteamiento P WHERE IdSubtema IN ( " + "      SELECT IdSubtema FROM DetalleEvaluacion WHERE IdEvaluacion = @IdEvaluacion " + "    ) " + "  ) P " + "WHERE " + "  R.IdPlanteamiento = P.IdPlanteamiento " + "  AND R.Intento = 1 " + "  AND R.Acertada = 'N' " + "  AND P.TipoRespuesta <> 'Abierta' " + "  AND P.Permite2 = 'true' ", conn);

					cmd.Parameters.AddWithValue("@IdAlumno", idAlumno);
					cmd.Parameters.AddWithValue("@IdEvaluacion", idEvaluacion);

					SqlDataReader results = cmd.ExecuteReader();

					// la primera fila tiene el número de mensajes recibidos de Profesores
					results.Read();

					if (int.Parse(results["Reactivos"].ToString()) > 0) {
						result = true;
					}

					results.Close();
					cmd.Dispose();
					conn.Close();
				}
			} catch (Exception ex) {
				Utils.LogManager.ExceptionLog_InsertEntry(ex);
			}

			return result;
		}

	}

}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
