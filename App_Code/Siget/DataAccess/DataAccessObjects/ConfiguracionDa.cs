
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// ComunicacionDa.vb

// DataSet

using System.Data.SqlClient;
using System.Configuration;
// SqlDataAdapter

namespace Siget.DataAccess
{

	/// <summary>
	/// Contiene métodos de acceso de datos relacionados a comunicación entre perfiles.
	/// </summary>
	public class ConfiguracionDa
	{

		public void GuardaConfiguracion(ref string llave, ref string valor, ref string login)
		{
			try {
				using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString)) {
					conn.Open();
					// la consulta debe sobreescribir cualquier configuración de este mísmo parámetro
					SqlCommand cmd = new SqlCommand("SET dateformat dmy; DELETE FROM Configuracion WHERE Llave = @Llave; INSERT INTO Configuracion (Llave, Valor, FechaModif, Modifico) VALUES (@Llave, @Valor, getdate(), @Login)", conn);

					cmd.Parameters.AddWithValue("@Llave", llave);
					cmd.Parameters.AddWithValue("@Valor", valor);
					cmd.Parameters.AddWithValue("@Login", login);

					cmd.ExecuteNonQuery();
					cmd.Dispose();
					conn.Close();
				}
			} catch (Exception ex) {
				Utils.LogManager.ExceptionLog_InsertEntry(ex);
				throw ex;
			}
		}

	}

}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
