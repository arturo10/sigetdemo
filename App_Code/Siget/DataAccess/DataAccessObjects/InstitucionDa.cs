﻿using System;
using System.Linq;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// InstitucionDa.vb

// DataSet

using System.Data.SqlClient;

/// <summary>
/// Descripción breve de InstitucionDa
/// </summary>
/// 
namespace Siget.DataAccess{
    public class InstitucionDa
    {
	    /// <summary>
		/// Obtiene la [Descripcion] (como 'Text') y el [IdInstitucion] (como 'Value') de todos los 
		/// registros de [Institucion] que contengan planteles asignados al operador del id recibido, 
		/// ordenados por [Descripcion].
		/// </summary>
		public DataSet ObtenTextoValor_IdOperador(string idOperador)
		{
			DataSet dst = new DataSet();

			using (SqlConnection conn = new SqlConnection(DataAccessUtils.SadcomeConnectionString)) {
				conn.Open();
				using (SqlDataAdapter sqlAdapter = new SqlDataAdapter()) {
					sqlAdapter.SelectCommand = new SqlCommand();
					sqlAdapter.SelectCommand.Connection = conn;
					sqlAdapter.SelectCommand.CommandText = " SELECT " + "   Descripcion AS 'Text' " + "   ,IdInstitucion AS 'Value' " + " FROM " + "   Institucion I " + " WHERE " + "   I.IdInstitucion IN (" + "       SELECT P.IdInstitucion " + "       FROM " + "           Plantel P " + "       WHERE " + "           IdPlantel IN " + "               (SELECT IdPlantel " + "               FROM OperadorPlantel " + "               WHERE IdOperador = @IdOperador))" + " ORDER BY " + "   Descripcion ";

					sqlAdapter.SelectCommand.Parameters.AddWithValue("@IdOperador", idOperador);

					sqlAdapter.Fill(dst);
				}
			}

			return dst;
		}

		/// <summary>
		/// Obtiene la [Descripcion] (como 'Text') y el [IdInstitucion] (como 'Value') de todos los 
		/// registros de [Institucion] que contengan planteles asignados al coordinador del id recibido, 
		/// ordenados por [Descripcion].
		/// </summary>
		public DataSet ObtenTextoValor_IdCoordinador(string idCoordinador)
		{
			DataSet dst = new DataSet();

			using (SqlConnection conn = new SqlConnection(DataAccessUtils.SadcomeConnectionString)) {
				conn.Open();
				using (SqlDataAdapter sqlAdapter = new SqlDataAdapter()) {
					sqlAdapter.SelectCommand = new SqlCommand();
					sqlAdapter.SelectCommand.Connection = conn;
					sqlAdapter.SelectCommand.CommandText = " SELECT " + "   Descripcion AS 'Text' " + "   ,IdInstitucion AS 'Value' " + " FROM " + "   Institucion I " + " WHERE " + "   I.IdInstitucion IN (" + "       SELECT P.IdInstitucion " + "       FROM " + "           Plantel P " + "       WHERE " + "           IdPlantel IN " + "               (SELECT IdPlantel " + "               FROM CoordinadorPlantel " + "               WHERE IdCoordinador = @IdCoordinador))" + " ORDER BY " + "   Descripcion ";

					sqlAdapter.SelectCommand.Parameters.AddWithValue("@IdCoordinador", idCoordinador);

					sqlAdapter.Fill(dst);
				}
			}

			return dst;
		}

	}

}