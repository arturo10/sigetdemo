
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// EvaluacionRepetidaDa.vb

// DataSet
using System.Data.SqlClient;
using System.Configuration;
// SqlDataAdapter
using System.Reflection;
// reflexión de vb
using System.ComponentModel;
// conversor de tipos

using Siget.TransferObjects;

namespace Siget.DataAccess
{

  /// <summary>
  /// Contiene métodos de acceso de datos relacionados a evaluaciones terminadas.
  /// </summary>
  public class EvaluacionTerminadaDa
  {

    /// <summary>
    /// Elimina todos los registros de la tabla EvaluacionTerminada que coincidan 
    /// con los parámetros recibidos. Reporta el número de registros eliminados.
    /// </summary>
    /// <param name="idAlumno"></param>
    /// <param name="idEvaluacion"></param>
    /// <param name="idCicloEscolar"></param>
    /// <returns></returns>
    /// <remarks></remarks>
    public int Elimina(object idAlumno, object idEvaluacion, object idCicloEscolar)
    {
      int cuenta = 0;

      try
      {
        using (SqlConnection conn = new SqlConnection(DataAccessUtils.SadcomeConnectionString))
        {
          conn.Open();

          using (SqlCommand cmd = new SqlCommand())
          {

            cmd.Connection = conn;

            cmd.CommandText = " SET dateformat dmy; " + " DELETE FROM EvaluacionTerminada " + " WHERE " + "   IdAlumno = @IdAlumno " + "   AND IdEvaluacion = @IdEvaluacion " + "   AND IdCicloEscolar = @IdCicloEscolar ";

            cmd.Parameters.AddWithValue("@IdAlumno", idAlumno.ToString());
            cmd.Parameters.AddWithValue("@IdEvaluacion", idEvaluacion.ToString());
            cmd.Parameters.AddWithValue("@IdCicloEscolar", idCicloEscolar.ToString());

            cuenta = cmd.ExecuteNonQuery();
          }
        }
      }
      catch (Exception ex)
      {
        Utils.LogManager.ExceptionLog_InsertEntry(ex);
        throw;
      }

      return cuenta;
    }

    /// <summary>
    /// Obtiene la evaluacion terminada cuyo IdEvaluacionT coincida con el recibido.
    /// De no haber coincidencia reporta Nothing.
    /// </summary>
    /// <param name="idEvaluacionT"></param>
    /// <returns></returns>
    public EvaluacionTerminadaTo Obten(ref object idEvaluacionT)
    {
      EvaluacionTerminadaTo objeto = null;

      try
      {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
        {
          conn.Open();
          using (SqlCommand cmd = new SqlCommand(" SELECT * " + " FROM " + "   EvaluacionTerminada " + " WHERE IdEvaluacionT = @IdEvaluacionT ", conn))
          {

            cmd.Parameters.AddWithValue("@IdEvaluacionT", idEvaluacionT.ToString());

            using (SqlDataReader resultado = cmd.ExecuteReader())
            {

              while (resultado.Read())
              {
                objeto = new EvaluacionTerminadaTo();

                try
                {
                  Type tipo = objeto.GetType();

                  // obtiene solo las propiedades de instancia y públicas
                  BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;

                  PropertyInfo[] propiedades = tipo.GetProperties(flags);

                  foreach (PropertyInfo propiedad in propiedades)
                  {
                    // si está definida en el origen de datos
                    if (resultado[propiedad.Name.ToString()] != DBNull.Value)
                    {
                      // si no es nulo ese resultado
                      if (resultado[propiedad.Name.ToString()] != null)
                      {
                        // obtengo el conversor de la propiedad actual para establecer el valor
                        TypeConverter convertidor = TypeDescriptor.GetConverter(propiedad.PropertyType);

                        propiedad.SetValue(objeto, convertidor.ConvertFrom(resultado[propiedad.Name.ToString()].ToString()), null);
                      }
                    }
                  }

                }
                catch (Exception ex)
                {
                  Utils.LogManager.ExceptionLog_InsertEntry(ex);
                  throw ex;
                }
              }

            }
          }
        }
      }
      catch (Exception ex)
      {
        Utils.LogManager.ExceptionLog_InsertEntry(ex);
        throw ex;
      }

      return objeto;
    }

    /// <summary>
    /// Obtiene la evaluacion terminada cuyos campos 
    /// coincidan con los parámetros recibidos.
    /// De no haber coincidencia reporta Nothing.
    /// </summary>
    /// <param name="idEvaluacion"></param>
    /// <param name="idAlumno"></param>
    /// <param name="idCicloEscolar"></param>
    public EvaluacionTerminadaTo Obten(ref object idEvaluacion, ref object idAlumno, ref object idCicloEscolar)
    {
      EvaluacionTerminadaTo objeto = null;

      try
      {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
        {
          conn.Open();
          using (SqlCommand cmd = new SqlCommand(" SELECT * " + " FROM " + "   EvaluacionTerminada " + " WHERE " + "   IdEvaluacion = @IdEvaluacion " + "   AND IdAlumno = @IdAlumno " + "   AND IdCicloEscolar = @IdCicloEscolar ", conn))
          {

            cmd.Parameters.AddWithValue("@IdEvaluacion", idEvaluacion.ToString());
            cmd.Parameters.AddWithValue("@IdAlumno", idAlumno.ToString());
            cmd.Parameters.AddWithValue("@IdCicloEscolar", idCicloEscolar.ToString());

            using (SqlDataReader resultado = cmd.ExecuteReader())
            {

              while (resultado.Read())
              {
                objeto = new EvaluacionTerminadaTo();

                try
                {
                  Type tipo = objeto.GetType();

                  // obtiene solo las propiedades de instancia y públicas
                  BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;

                  PropertyInfo[] propiedades = tipo.GetProperties(flags);

                  foreach (PropertyInfo propiedad in propiedades)
                  {
                    // si está definida en el origen de datos
                    if (resultado[propiedad.Name.ToString()] != DBNull.Value)
                    {
                      // si no es nulo ese resultado
                      if (resultado[propiedad.Name.ToString()] != null)
                      {
                        // obtengo el conversor de la propiedad actual para establecer el valor
                        TypeConverter convertidor = TypeDescriptor.GetConverter(propiedad.PropertyType);

                        propiedad.SetValue(objeto, convertidor.ConvertFrom(resultado[propiedad.Name.ToString()].ToString()), null);
                      }
                    }
                  }

                }
                catch (Exception ex)
                {
                  Utils.LogManager.ExceptionLog_InsertEntry(ex);
                  throw ex;
                }
              }

            }
          }
        }
      }
      catch (Exception ex)
      {
        Utils.LogManager.ExceptionLog_InsertEntry(ex);
        throw ex;
      }

      return objeto;
    }

    /// <summary>
    /// Indica si el alumno ya terminó la evaluación.
    /// </summary>
    /// <param name="idEvaluacion"></param>
    /// <param name="idAlumno"></param>
    /// <param name="idCicloEscolar"></param>
    /// <param name="idGrado"></param>
    /// <param name="idGrupo"></param>
    /// <returns></returns>
    /// <remarks></remarks>
    public bool TerminoEvalaucion(string idEvaluacion, string idAlumno, string idCicloEscolar, string idGrado, string idGrupo)
    {
      bool result = false;

      try
      {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
        {
          conn.Open();

          SqlCommand cmd = new SqlCommand("SELECT Resultado " + "FROM EvaluacionTerminada " + "WHERE " + "   IdAlumno = @IdAlumno " + "   AND IdEvaluacion = @IdEvaluacion " + "   AND IdCicloEscolar = @IdCicloEscolar " + "   AND IdGrado = @IdGrado " + "   AND IdGrupo = @IdGrupo", conn);

          cmd.Parameters.AddWithValue("@IdAlumno", idAlumno);
          cmd.Parameters.AddWithValue("@IdEvaluacion", idEvaluacion);
          cmd.Parameters.AddWithValue("@IdCicloEscolar", idCicloEscolar);
          cmd.Parameters.AddWithValue("@IdGrado", idGrado);
          cmd.Parameters.AddWithValue("@IdGrupo", idGrupo);

          SqlDataReader results = cmd.ExecuteReader();

          // la primera fila tiene el número de mensajes recibidos de Profesores
          if (results.Read())
          {
            result = true;
          }

          results.Close();
          cmd.Dispose();
          conn.Close();
        }
      }
      catch (Exception ex)
      {
        Utils.LogManager.ExceptionLog_InsertEntry(ex);
      }

      return result;
    }

  }

}