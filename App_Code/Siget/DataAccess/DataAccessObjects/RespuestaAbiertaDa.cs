
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// EvaluacionRepetidaDa.vb

// DataSet
using System.Data.SqlClient;
// SqlDataAdapter

using Siget.TransferObjects;

namespace Siget.DataAccess
{

	/// <summary>
	/// Contiene métodos de acceso de datos relacionados a respuestas abiertas.
	/// </summary>
	public class RespuestaAbiertaDa
	{

		/// <summary>
		/// Elimina todos los registros de la tabla RespuestaAbierta que coincidan con los 
		/// parámetros recibidos. Reporta el número de registros eliminados.
		/// </summary>
		/// <param name="idAlumno"></param>
		/// <param name="idEvaluacion"></param>
		/// <param name="idCicloEscolar"></param>
		/// <returns></returns>
		/// <remarks></remarks>
		public int Elimina(object idAlumno, object idEvaluacion, object idCicloEscolar)
		{
			int cuenta = 0;

			try {
				using (SqlConnection conn = new SqlConnection(DataAccessUtils.SadcomeConnectionString)) {
					conn.Open();

					using (SqlCommand cmd = new SqlCommand()) {

						cmd.Connection = conn;

						cmd.CommandText = " SET dateformat dmy; " + " DELETE FROM RespuestaAbierta " + " WHERE " + "   IdRespuesta IN " + "       (SELECT IdRespuesta " + "       FROM Respuesta " + "       WHERE IdAlumno = @IdAlumno " + "           AND IdDetalleEvaluacion IN " + "               (SELECT IdDetalleEvaluacion " + "               FROM DetalleEvaluacion " + "               WHERE IdEvaluacion = @IdEvaluacion) " + "           AND IdCicloEscolar = @IdCicloEscolar) ";

						cmd.Parameters.AddWithValue("@IdAlumno", idAlumno);
						cmd.Parameters.AddWithValue("@IdEvaluacion", idEvaluacion);
						cmd.Parameters.AddWithValue("@IdCicloEscolar", idCicloEscolar);

						cuenta = cmd.ExecuteNonQuery();
					}
				}
			} catch (Exception ex) {
				Utils.LogManager.ExceptionLog_InsertEntry(ex);
				throw;
			}

			return cuenta;
		}

	}

}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
