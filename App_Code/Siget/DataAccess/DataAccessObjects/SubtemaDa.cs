
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// SubtemaDa.vb

// DataSet

using System.Data.SqlClient;
// SqlDataAdapter

namespace Siget.DataAccess
{

	/// <summary>
	/// Contiene métodos de acceso de datos relacionados a subtemas.
	/// </summary>
	public class SubtemaDa
	{

		/// <summary>
		/// Reporta una lista de objetos Subtema donde cada uno contiene toda la información 
		/// del registro respectivo en la base de datos.
		/// </summary>
		/// <param name="conn">SqlConnection abierta</param>
		/// <param name="idEvaluacion">El identificador de la evaluación a buscar</param>
		/// <returns>Nothing cuando no se encuentra, un objeto Evalaucion de lo contrario.</returns>
		public ArrayList ObtenSubtemasDeEvalaucion(ref SqlConnection conn, string idEvaluacion, string idGrado)
		{
			ArrayList subtemas = new ArrayList();

			SqlCommand cmd = new SqlCommand("select distinct " + "    D.IdDetalleEvaluacion, " + "    ST.IdSubtema, " + "    ST.Numero, " + "    ST.Descripcion, " + "    D.UsarReactivos, " + "    (SELECT COUNT(*) FROM Planteamiento P WHERE P.IdSubtema = ST.IdSubtema AND P.Ocultar = 0) Reactivos " + "from " + "    DetalleEvaluacion D,  " + "    Subtema ST, " + "    Tema T, " + "    Asignatura A " + "where " + "    D.IdEvaluacion = @IdEvaluacion " + "    and ST.IdSubtema = D.IdSubtema " + "    and T.IdTema = ST.IdTema " + "    and A.IdAsignatura = T.IdAsignatura " + "    and A.IdGrado = @IdGrado " + "order by " + "    ST.Numero, " + "    ST.Descripcion, " + "    ST.IdSubtema, " + "    D.IdDetalleEvaluacion ", conn);

			cmd.Parameters.AddWithValue("@IdEvaluacion", idEvaluacion);
			cmd.Parameters.AddWithValue("@IdGrado", idGrado);

			SqlDataReader listaSubtemas = cmd.ExecuteReader();

			// en la página de material de apoyo (~/alumno/cursos/actividad/Default.aspx)
			// se impide iniciar la actividad cuando no tiene reactivos; si llega aquí es por que si hay.
			// leo la lista de subtemas y los agrego al contenedor
			while (listaSubtemas.Read()) {
				// creo el subtema, y lo lleno con la información
				Beans.Subtema newSubtema = new Beans.Subtema();

				newSubtema.datos.IdSubtema = int.Parse(listaSubtemas["IdSubtema"].ToString());
				newSubtema.IdDetalleEvaluacion = uint.Parse(listaSubtemas["IdDetalleEvaluacion"].ToString());
				newSubtema.datos.Descripcion = listaSubtemas["Descripcion"].ToString();
                if (listaSubtemas["Numero"] != DBNull.Value)
                {
					newSubtema.datos.Numero = decimal.Parse(listaSubtemas["Numero"].ToString());
				}
                if (listaSubtemas["UsarReactivos"] != DBNull.Value)
                {
					newSubtema.UsarReactivos = int.Parse(listaSubtemas["UsarReactivos"].ToString());
				}
				newSubtema.Reactivos = int.Parse(listaSubtemas["Reactivos"].ToString());

				// añado el subtema a la lista
				subtemas.Add(newSubtema);
			}

			listaSubtemas.Close();
			cmd.Dispose();

			return subtemas;
		}

	}

}

//=======================================================
//Service provided by Telerik (www.telerik.com)
//Conversion powered by NRefactory.
//Twitter: @telerik
//Facebook: facebook.com/telerik
//=======================================================
