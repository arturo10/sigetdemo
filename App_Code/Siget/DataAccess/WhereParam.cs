﻿// 
// WhereParam.vb

namespace Siget.DataAccess
{

  /// <summary>
  /// Almacena los componentes de un parámetro para una consulta sql.
  /// </summary>
  /// <remarks></remarks>
  public class WhereParam
  {

    // *************************************************
    //                                       Propiedades
    // *************************************************

    private string _conjuncion;
    /// <summary>
    /// La conjunción de la clausula {AND, OR}
    /// </summary>
    public string Conjuncion
    {
      get { return _conjuncion; }
      set { _conjuncion = value; }
    }

    private string _operador;
    /// <summary>
    /// El operador a utilizar entre el nombre y el valor, ej {=, >=, IS NOT, etc}
    /// </summary>
    public string Operador
    {
      get { return _operador; }
      set { _operador = value; }
    }

    private string _valor;
    /// <summary>
    /// El valor de la clausula.
    /// Establecerlo como Nothing|null se traduce a DBNull en la consulta.
    /// </summary>
    public string Valor
    {
      get { return _valor; }
      set { _valor = value; }
    }

    // *************************************************
    //                                     Constructores
    // *************************************************

    public WhereParam()
    {
      Conjuncion = null;
      Operador = null;
      Valor = null;
    }

    public WhereParam(string conjuncion, string operador, string valor)
    {
      this.Conjuncion = conjuncion;
      this.Operador = operador;
      this.Valor = valor;
    }

  }

}