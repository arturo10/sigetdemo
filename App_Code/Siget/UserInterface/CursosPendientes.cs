﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using Siget;

using System.Web;
using System.Text;

namespace Siget.UserInterface
{

  public class CursosPendientes
  {

    #region "Asignaturas" ' genera el botón originalmente visible de la asignatura.
    public string printCourseButton(ref Beans.Asignatura asig)
    {
      asig.obtainStartDate();
      asig.obtainEndDate();

      StringBuilder btn = new StringBuilder();
      btn.Append("<li id='Asignatura_" + asig.getIdAsignatura() + "' class='sideMenu_asignatura' onclick=\"showCalificaciones('#course_" + asig.getIdAsignatura() + "', '#Asignatura_" + asig.getIdAsignatura() + "')\">");
      btn.Append("    <table class='sideMenu_button' style='width: 100%;border-collapse:initial!important;'><tbody><tr><td style='width: 100%;'>");
      btn.Append("  <div>");
      btn.Append("    <img src='" + Config.Global.urlImagenes + "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/book_open.png' class='menuBarIcon' style='padding-right:8px;' width='32'>");
      btn.Append(asig.getDescripcion());
      btn.Append("    <br />");
      btn.Append(getActivitiesStartDate(ref asig));
      btn.Append(" - ");
      btn.Append(getActivitiesEndDate(ref asig));

      // cuenta las evaluaciones totales de las calificaciones de esta asignatura
      int evalTotal = 0;
      int evalTerminadas = 0;
      foreach (Beans.Calificacion cal in asig.getCalificaciones())
      {
        evalTotal += cal.TotalActividades;
        evalTerminadas += cal.ActividadesTerminadas;
      }

      if (evalTotal > 0)
      {
        // si hay evaluaciones y el número de terminadas es menor al total
        if (evalTotal > evalTerminadas && asig.getEndDate() > System.DateTime.Now)
        {
          // calcula el porcentaje
          int porcentaje = 0;
          if (evalTerminadas == 0)
          {
            porcentaje = 0;
          }
          else
          {
            porcentaje = (int)((double)evalTerminadas / (double)evalTotal * 100.0);
          }

          // añade el progress bar
          btn.Append("  <div class='progressBar' title='" + porcentaje * 80 / 100 + "%'>");
          btn.Append("    <div style='width: " + porcentaje * 80 / 100 + "px;' class='progressBarValue'></div>");
          btn.Append("  </div>");
        }
        else
        {

            
          btn.Append("</td>");
          btn.Append("<td>");
          // si el total es igual o menor a las terminadas, califícalas
          decimal resultado = 0;
          string colorIndicador = "#C5C9CE";

          decimal totalPuntos = 0;
          foreach (Beans.Calificacion cal in asig.getCalificaciones())
          {
            cal.EvaluaCalificacion();
            totalPuntos += cal.Valor;
            resultado += (cal.ResultadoTerminada * cal.Valor);
          }
          if (totalPuntos == 0)
          {
            btn.Append("&nbsp;");
          }
          else
          {
            resultado /= totalPuntos;

            if ((asig.IdIndicador != null))
            {
              TransferObjects.IndicadorTo indicador = ((DataAccess.IndicadorDa)new DataAccess.IndicadorDa()).obten_IdIndicador(asig.IdIndicador.ToString());

              if (resultado >= indicador.MinAzul)
              {
                  colorIndicador = "#9EC1E4";
              }
              else if (resultado >= indicador.MinAmarillo && resultado < indicador.MinVerde)
              {
                  colorIndicador = "#FFF200";
              }
              else if (resultado >= indicador.MinVerde && resultado < indicador.MinAzul)
              {
                  colorIndicador = "#9DD77C";
              }
              else
              {
                  colorIndicador = "#F2B6B6";

              }
            }

            btn.Append("  <div class='resultadoAsignatura' style='margin-left: 0px; font-size: 20px;background: ");
            btn.Append(colorIndicador);
            btn.Append(";'>");
            btn.Append(resultado.ToString("0.00"));
            btn.Append("</div>");
          }
        }
      }
      btn.Append("</div>");
      btn.Append("    </td></tr></tbody></table>");
      btn.Append("</li>");
      return btn.ToString();
    }

    // genera las tags de apertura del contenedor para las calificaciones de la asignatura.
    // Es necesario llamar esta función al terminar de imprimir el botón de la asignatura;
    // de lo contrario el markup de la lista de asignaturas queda incompleto.
    public string printOpenPeriodTags(ref Beans.Asignatura asig)
    {
      StringBuilder tags = new StringBuilder();
      tags.Append("<li class='periodList' id='course_" + asig.getIdAsignatura() + "' style='display: none;'>");
      tags.Append("  <ul class='sideMenu'>");
      return tags.ToString();
    }

    // genera las tags de cierre del contenedor para las calificaciones de la asignatura.
    // Es necesario llamar esta función al terminar de imprimir las calificaciones;
    // de lo contrario el markup de la lista de asignaturas queda incompleto.
    public string printClosePeriodTags()
    {
      StringBuilder tags = new StringBuilder();
      tags.Append("  </ul>");
      tags.Append("</li>");
      return tags.ToString();
    }

    protected string getActivitiesStartDate(ref Beans.Asignatura asig)
    {
      string color = string.Empty;
      string img = string.Empty;

      if (asig.getStartDate() == System.DateTime.MaxValue)
      {
        return "&#8734;";
      }

      if (asig.getStartDate() < System.DateTime.Now)
      {
        if (!(asig.getEndDate() == System.DateTime.MinValue) && asig.getEndDate() < System.DateTime.Now)
        {
          // si está definida fecha límite, y ésta ya pasó
          // imprime imágen roja y colorea de rojo la fecha
          color = "color: #c41e00;";
          img = "<img src='" + Config.Global.urlImagenes + "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/bullet_red_slick.png' class='menuBarIcon' width='16'>";
        }
        else
        {
          // si no hay fecha límite o ésta no ha pasado
          // imprime imágen verde y colorea de verde la fecha
          color = "color: #338033;";
          img = "<img src='" + Config.Global.urlImagenes + "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/bullet_green_slick.png' class='menuBarIcon' width='16'>";
        }
      }

      return "<span style='" + color + "'>" + img + asig.getStartDate().ToShortDateString();
    }

    protected string getActivitiesEndDate(ref Beans.Asignatura asig)
    {

      if (asig.getEndDate() == System.DateTime.MinValue)
      {
        return "&#8734;";
      }

      return asig.getEndDate().ToShortDateString() + "</span>";
    }

    #endregion

    #region "Calificaciones"

    // Aquí imprimo el botón originalmente visible de la calificación.
    public string ImprimeBotonCalificacion(ref Beans.Calificacion calif)
    {
      StringBuilder btn = new StringBuilder();
      btn.Append("<li class='sideMenu_calificacion' id='Calificacion_" + calif.IdCalificacion + "' onclick=\"showActividades('#period_" + calif.IdCalificacion + "', '#Calificacion_" + calif.IdCalificacion + "')\">");
      btn.Append("  <table class='sideMenu_button' style='width: 100%;border-collapse:initial !important;background-color:#ddddd9;'><tr>");
      btn.Append("    <td>");
      btn.Append("    <img src='" + Config.Global.urlImagenes + "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/bullet_toggle_minus_slick.png'  class='menuBarIcon' width='20' style='display: none;'>");
      btn.Append(calif.Descripcion);
      btn.Append("    </td><td style='text-align: right; vertical-align: top;'>");
      //btn.Append("<span style='font-weight: bold; color: #338033;'>" & (CType(getFinishedActivities() / getTotalActivities() * 100, Integer)) & "%</span>")
      if (calif.ActividadesTerminadas >= calif.TotalActividades | calif.FechaFin < System.DateTime.Now)
      {
        // si el total es igual o menor a las terminadas, califícalas
        string colorIndicador = "#C5C9CE";

        decimal totalPorcentaje = 0;
        foreach (Beans.Evaluacion e in calif.Evaluaciones)
        {
          totalPorcentaje += e.Porcentaje;
        }

        if (totalPorcentaje > 0)
        {
          calif.EvaluaCalificacion();
          if ((calif.IdIndicador != null))
          {
            TransferObjects.IndicadorTo indicador = ((DataAccess.IndicadorDa)new DataAccess.IndicadorDa()).obten_IdIndicador(calif.IdIndicador.ToString());

            if (calif.ResultadoTerminada >= indicador.MinAzul)
            {
                colorIndicador = "#9EC1E4";
            }
            else if (calif.ResultadoTerminada >= indicador.MinAmarillo && calif.ResultadoTerminada< indicador.MinVerde )
            {
              colorIndicador = "#FFF200";
            }
            else if (calif.ResultadoTerminada >= indicador.MinVerde && calif.ResultadoTerminada< indicador.MinAzul)
            {
              colorIndicador = "#9DD77C";
            }
            else
            {
                colorIndicador = "#F2B6B6";
             
            }
          }

          btn.Append("  <span class='resultadoAsignatura' style='background-color: ");
          btn.Append(colorIndicador);
          btn.Append(";'>");
          btn.Append(calif.ResultadoTerminada.ToString("0.00"));
          btn.Append("</span>");
        }
        else
        {
          btn.Append("&nbsp;");
        }
      }
      else
      {
        btn.Append("<span style='font-weight: bold; color: #338033;'>" + calif.ActividadesTerminadas + "&nbsp;/&nbsp;" + calif.TotalActividades + "</span>");
      }
      btn.Append("</td>");
      btn.Append("  </tr></table>");
      btn.Append("</li>");
      return btn.ToString();
    }

    // Genera las tags de apertura del contenedor para las actividades de la calificación.
    // Incluye los títulos de la tabla de actividades disponibles.
    // Es necesario llamar esta función al terminar de imprimir el botón de la calificación;
    // de lo contrario el markup de la lista de evaluaciones queda incompleto.
    public string ImprimeInicioListaActividades(ref Beans.Calificacion calif)
    {
      StringBuilder tags = new StringBuilder();
      tags.Append("<div class='AssignmentsContainer hiddenContainer pendingTableRow' id='period_" + calif.IdCalificacion + "' style='display: none;'>");

      tags.Append("   <table style='width: 100%; margin-bottom: 20px;' ><tr><td>");
      tags.Append("       <span style='font-weight: bold; font-size: 1.17em;'>" + calif.IdAsignatura_Nombre + "</span>");
      tags.Append("       <img src='" + Config.Global.urlImagenes + "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/bullet_arrow_right_slick.png' class='menuBarIcon' width='16'>");
      tags.Append("       <span style='font-weight: bold;'>" + calif.Descripcion + "</span>");
      tags.Append("   </td></tr></table>");

      tags.Append("    <div class='table-responsive'>   <div class='col-lg-12 col-sm-6 col-md-12 col-xs-12'><table class='pendingActivitiesTable table '>");
      tags.Append("        <thead>");
      tags.Append("            <tr class='AssignmentHeaderRow' style='background-color:#696969;color:white;'>");
      tags.Append("                <td class=' centerText padded' style='border-right: solid 1px #333;'>" + Lang.FileSystem.Alumno.Cursos.ACT_TABLE_ACTIVIDAD[HttpContext.Current.Session["Usuario_Idioma"].ToString()] + "</td>");

      if (Config.Global.ALUMNO_PENDIENTES_COLUMNA_TIPO)
      {
        tags.Append("                <td class=' centerText padded' style='border-right: solid 1px #333;'>" + Lang.FileSystem.Alumno.Cursos.ACT_TABLE_TIPO[HttpContext.Current.Session["Usuario_Idioma"].ToString()] + "</td>");
      }

      tags.Append("                <td class=' centerText padded' style='border-right: solid 1px #333;'>" + Lang.FileSystem.Alumno.Cursos.ACT_TABLE_FECHA_INICIO[HttpContext.Current.Session["Usuario_Idioma"].ToString()] + "</td>");

      if (Config.Global.ALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION)
      {
        tags.Append("                <td class=' centerText padded' style='border-right: solid 1px #333;'>" + Lang.FileSystem.Alumno.Cursos.ACT_TABLE_FECHA_PENALIZACION[HttpContext.Current.Session["Usuario_Idioma"].ToString()] + "</td>");
      }

      tags.Append("                <td class=' centerText padded' style='border-right: solid 1px #333;'>" + Lang.FileSystem.Alumno.Cursos.ACT_TABLE_FECHA_FINCONTESTAR[HttpContext.Current.Session["Usuario_Idioma"].ToString()] + "</td>");

      if (Config.Global.ALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION)
      {
        tags.Append("                <td class=' centerText padded' style='border-right: solid 1px #333;'>" + Lang.FileSystem.Alumno.Cursos.ACT_TABLE_PENALIZACION[HttpContext.Current.Session["Usuario_Idioma"].ToString()] + "</td>");
      }

       
      tags.Append("                <td class=' centerText padded' style='border-right: solid 1px #333;'>" + Lang.FileSystem.Alumno.Cursos.ACT_TABLE_CALIFICACION[HttpContext.Current.Session["Usuario_Idioma"].ToString()] + "</td>");
      tags.Append("                <td class=' centerText padded' style='border-right: solid 1px #333;'>" + Lang.FileSystem.Alumno.Cursos.ACT_TABLE_FECHA_TERMINADA[HttpContext.Current.Session["Usuario_Idioma"].ToString()] + "</td>");
      
        if(Config.Global.ALUMNO_PENDIENTES_COLUMNA_ENTREGA)
             tags.Append("                <td class=' centerText padded'>" + Lang.FileSystem.Alumno.Cursos.ACT_TABLE_ENTREGA[HttpContext.Current.Session["Usuario_Idioma"].ToString()] + "</td>");
     
        
      tags.Append("            </tr>");
      tags.Append("        </thead>");
      tags.Append("        <tbody>");
      return tags.ToString();
    }

    // genera las tags de cierre del contenedor para las actividades de la calificación.
    // Es necesario llamar esta función al terminar de imprimir las actividades;
    // de lo contrario el markup de la lista de evaluaciones queda incompleto.
    public string ImprimeFinListaActividades()
    {
      StringBuilder tags = new StringBuilder();
      tags.Append("        </tbody>");
      tags.Append("    </table> </div></div> ");
      tags.Append("</div>");
      return tags.ToString();
    }

    #endregion

    #region "Evaluaciones"

    // Aquí imprimo el botón originalmente visible de la calificación.
    public string printActivityRow(ref Beans.Evaluacion eval)
    {
      StringBuilder row = new StringBuilder();
      StringBuilder str = new StringBuilder();
      bool past = false;
      //If getResultadoTerminada() >= 0 Then
      //    ' si está contestada, la pinto de verde
      //    str.Append("<tr class='AssignmentRow' style='background: #ddeecc;'>")
      //    str.Append("    <td class=' leftText assignmentTitle' style='font-size: small;'>")
      //    past = True
      //Else
      if (eval.ResultadoTerminada >= 0)
      {
        str.Append("<tr class='AssignmentRow' style='color: green;'>");
        str.Append("    <td class=' leftText assignmentTitle' style='padding: 5px;'>");
      }
      else if (eval.FinContestar < System.DateTime.Parse(System.DateTime.Now.ToShortDateString()) && eval.ResultadoTerminada < 0)
      {
        str.Append("<tr class='AssignmentRowRed' style='font-weight: bolder; color: #c41e00;'>");
        str.Append("    <td class=' leftText assignmentTitle' style='padding: 5px; font-size: small;'>");
        past = true;
      }
      else
      {
        str.Append("<tr class='AssignmentRow'>");
        str.Append("    <td class=' leftText assignmentTitle' style='padding: 5px;'>");
      }

      str.Append("        <a CssClass='btn' href='actividad/?IdEvaluacion=");
      str.Append(eval.IdEvaluacion);
      str.Append("&Tarea=");
      str.Append(eval.ClaveBateria.Replace(" ", "%20"));
      str.Append("&Alcance=");
      str.Append(eval.Alcance);
      str.Append("&Extra=");
      if (eval.Borrador)
      {
        str.Append("B");
      }
      else
      {
        str.Append("N");
      }
      str.Append("'>");
      str.Append(eval.ClaveBateria + "</a>");
      str.Append("    </td>");

      if (Config.Global.ALUMNO_PENDIENTES_COLUMNA_TIPO)
      {
        str.Append("    <td class=' centerText padded'>");
        str.Append(Lang.Traductor.TraducetipoActividad(eval.Tipo));
        if (!(eval.Entrega == string.Empty))
        {
          str.Append("<br /><b>(" + Lang.FileSystem.General.ESTADO_ACTIVIDAD_ENTREGADO[HttpContext.Current.Session["Usuario_Idioma"].ToString()] + ")</b>");
        }

        if (eval.Borrador)
        {
          str.Append("<br /><b>(" + Lang.FileSystem.General.ESTADO_ACTIVIDAD_BORRADOR[HttpContext.Current.Session["Usuario_Idioma"].ToString()] + ")</b>");
        }

        str.Append("</td>");
      }

      if (eval.InicioContestar <= System.DateTime.Parse(System.DateTime.Now.ToShortDateString()) && !past)
      {
        str.Append("    <td class=' centerText padded' style='font-size: 1.1em; font-weight: bolder; color: #338033;'>");
        str.Append(eval.InicioContestar.Value.ToShortDateString() + "</td>");
      }
      else
      {
        str.Append("    <td class=' centerText padded'>");
        str.Append(eval.InicioContestar.Value.ToShortDateString() + "</td>");
      }

      if (Config.Global.ALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION)
      {
        if (eval.FinSinPenalizacion < System.DateTime.Parse(System.DateTime.Now.ToShortDateString()) && !past && eval.ResultadoTerminada < 0)
        {
          str.Append("    <td class=' centerText padded' style='font-size: 1.1em; font-weight: bolder; color: #c66C00;'>");
          str.Append(eval.FinSinPenalizacion.Value.ToShortDateString() + "</td>");
        }
        else
        {
          str.Append("    <td class=' centerText padded'>");
          str.Append(eval.FinSinPenalizacion.Value.ToShortDateString() + "</td>");
        }
      }

      str.Append("    <td class=' centerText padded'>");
      str.Append(eval.FinContestar.Value.ToShortDateString() + "</td>");

      if (Config.Global.ALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION)
      {
        str.Append("    <td class=' centerText padded'>");
        str.Append(eval.Penalizacion + "</td>");
      }

      if (eval.Tipo == "Material" && eval.ResultadoTerminada >= 0)
      {
          str.Append("    <td class=' centerText padded' style='background: #efefef; font-weight: bold; font-size: 14px; color: #333;'>");
          str.Append(Siget.Lang.FileSystem.Alumno.Cursos.Txt_Revisado[HttpContext.Current.Session["Usuario_Idioma"].ToString()] + "</td>");
      }
      else
      {

          if (eval.ResultadoTerminada >= 0)
          {
              str.Append("    <td class=' centerText padded' style='background: #efefef; font-weight: bold; font-size: 14px; color: #333;'>");
              str.Append(eval.ResultadoTerminada + "</td>");
          } 
          else
          {
              str.Append("    <td class=' centerText padded'>");
              str.Append("</td>");
          }
      }
      str.Append("    <td class=' centerText padded'>");
      if (!(eval.FechaTermino == System.DateTime.MinValue))
      {
        str.Append(eval.FechaTermino.ToShortDateString() + "</td>");
      }
      else
      {
        str.Append("</td>");
      }

      if (Siget.Config.Global.ALUMNO_PENDIENTES_COLUMNA_ENTREGA)
      {
          str.Append("    <td class=' centerText padded'>");
          if (!(eval.Entrega == string.Empty))
          {
              str.Append("<a href='" + Config.Global.urlCargas + HttpContext.Current.User.Identity.Name + "/" + eval.Entrega + "' target='_blank'>");
              str.Append("<img src='" + Config.Global.urlImagenes + "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/attach.png' class='menuBarIcon' width='16'>");
              str.Append("</a>");
          }
          str.Append("</td>");
      }
     

      str.Append("</tr>");
      return str.ToString();
    }

    #endregion

  }

}