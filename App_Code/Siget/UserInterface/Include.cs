﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;
using System.Web.UI.WebControls;

namespace Siget.UserInterface
{

  public class Include
  {

    public const string jquery_file = "jquery-1.11.1.min.js";
    public const string jquery_ui_folder = "jquery-ui-1.11.1";
    public const string spinner_file = "spin.min.js";
    public const string tinymce_file = "tinymce.min.js";
    public const string chartist_file = "chartist.min.js";

    public const string chartist_css_file = "chartist.min.css";
    /// <summary>
    /// Inserta (a un Literal parámetro) un control script que incluye la fuente de JQuery
    /// </summary>
    /// <param name="target">El Literal donde se insertará el control</param>
    /// <remarks></remarks>
    public static void JQuery(ref Literal target)
    {
      StringBuilder control = new StringBuilder();
      control.Append("<script src='");
      control.Append(Config.Global.urlScripts);
      control.Append(jquery_file);
      control.Append("'></script>");
      target.Text += control.ToString();
    }

    /// <summary>
    /// Inserta (a un Literal parámetro) un control script que incluye la fuente de JQuery User Interface
    /// </summary>
    /// <param name="target">El Literal donde se insertará el control</param>
    /// <remarks></remarks>
    public static void JQuery_UI(ref Literal target)
    {
      StringBuilder control = new StringBuilder();
      control.Append("<script src='");
      control.Append(Config.Global.urlScripts);
      control.Append(jquery_ui_folder);
      control.Append("/jquery-ui.min.js");
      control.Append("'></script>");
      target.Text += control.ToString();
    }

    /// <summary>
    /// Inserta (a un Literal parámetro) un control script que incluye la fuente de Spinner progressbar
    /// </summary>
    /// <param name="target">El Literal donde se insertará el control</param>
    /// <remarks></remarks>
    public static void Spinner(ref Literal target)
    {
      StringBuilder control = new StringBuilder();
      // aqui adjunto la implementación del spinner
      control.Append("<script src='");
      control.Append(Config.Global.urlScripts);
      control.Append(spinner_file);
      control.Append("'></script>");

      // aqui adjunto el control necesario para activar y mostrar el spinner
      control.Append("<script src='");
      control.Append(Config.Global.urlScripts);
      control.Append("spinControl.js'></script>");
      target.Text += control.ToString();
    }

    /// <summary>
    /// Inserta (a un Literal parámetro) un control script que incluye la fuente del editor de texto en html tinymce 
    /// </summary>
    /// <param name="target">El Literal donde se insertará el control</param>
    /// <remarks></remarks>
    public static void HtmlEditor(ref Literal target, ref int width, ref int height, ref int fontSize, ref string languaje)
    {
      StringBuilder control = new StringBuilder();
      // aqui adjunto la implementación del editor
      control.Append("<script src='");
      control.Append(Config.Global.urlScripts + "tinymce/");
      control.Append(tinymce_file);
      control.Append("'></script>");

      // adjunto también el script de inicialización del editor con los parámetros recibidos
      control.Append("<script>");
      control.Append("    tinymce.init({");
      //if (languaje == "es-mx")
      //{
      //  control.Append("        language:   'es',");
      //}

      control.Append("        selector: \".tinymce\",");
      control.Append("        theme: \"modern\",");
      control.Append("        width: " + width + ",");
      control.Append("        height: " + height + ",");
      control.Append("        resize: true,");
      control.Append("        plugins: [");
      control.Append("             \"advlist autolink link image lists charmap preview hr\",");
      control.Append("             \"searchreplace wordcount visualblocks visualchars insertdatetime nonbreaking\",");
      control.Append("             \"contextmenu directionality paste textcolor\"");
      control.Append("        ],");
      control.Append("        menubar: \"edit format\",");
      control.Append("        toolbar1: \"undo redo | fontselect | fontsizeselect | bold italic | forecolor backcolor | preview\",");
      control.Append("        toolbar2: \"alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link\",");
      control.Append("        setup: function (ed) {");
      control.Append("            ed.on('init', function () {");
      control.Append("                this.getDoc().body.style.fontSize = '" + fontSize + "px';");
      control.Append("            });");
      control.Append("        }");
      control.Append("    });");
      control.Append("</script>");

      target.Text += control.ToString();
    }

    /// <summary>
    /// Inserta (a un Literal parámetro) un control script que incluye la fuente de chartist y sus estilos
    /// </summary>
    /// <param name="target">El Literal donde se insertará el control</param>
    public static void Chartist(ref Literal target)
    {
      StringBuilder control = new StringBuilder();
      // aqui adjunto la implementación del spinner
      control.Append("<link rel='stylesheet' href='");
      control.Append(Config.Global.urlScripts + "chartist-js/");
      control.Append(chartist_css_file);
      control.Append("' />");
      control.Append("<script src='");
      control.Append(Config.Global.urlScripts + "chartist-js/");
      control.Append(chartist_file);
      control.Append("'></script>");
      target.Text += control.ToString();
    }

  }

}