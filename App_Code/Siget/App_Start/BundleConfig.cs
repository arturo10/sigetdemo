﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;

namespace Siget.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/css/bootstrap").Include(
                "~/Resources/css/bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/css/bootstrap-rtl").Include(
                "~/Resources/css/bootstrap-rtl.min.css"));


            bundles.Add(new StyleBundle("~/css/beyond").Include(
                 "~/Resources/css/select2.min.css",
                "~/Resources/css/beyond.min.css",
                "~/Resources/css/demo.min.css",
                "~/Resources/css/font-awesome.min.css",
                "~/Resources/css/flaticon.css",
                "~/Resources/css/typicons.min.css",
                "~/Resources/css/weather-icons.min.css",
                "~/Resources/css/animate.min.css",
                "~/Resources/css/Controles.css",
                "~/Resources/css/PageLayout.css",
                "~/Resources/css/Fonts.css" ,
                "~/Resources/css/Buttons.css" ,
                "~/Resources/css/DataGrids.css",
                "~/Resources/css/MessageBoxes.css",
                "~/Resources/css/LoadingSpinner.css",
                "~/Resources/css/SuperAdministrador.css",
                 "~/Resources/css/Dropdownlist.css",
                 "~/Resources/css/sweetalert.css",
                 "~/Resources/css/cards.css",
                 "~/Resources/css/simple_slider/simple-slider.css",
                 "~/Resources/css/datepicker.css",
                 "~/Resources/css/introjs.min.css"
                ));

              bundles.Add(new StyleBundle("~/css/admin").Include(
                 "~/Resources/css/SuperAdministrador.css"
                ));


            bundles.Add(new StyleBundle("~/css/fontsAlerts").Include(
               "~/Resources/css/font-awesome.min.css",
               "~/Resources/css/flaticon.css",
               "~/Resources/css/typicons.min.css",
               "~/Resources/css/weather-icons.min.css",
               "~/Resources/css/animate.min.css",
                "~/Resources/css/sweetalert.css"

               ));

            bundles.Add(new StyleBundle("~/css/beyond-rtl").Include(
                "~/Resources/css/beyond-rtl.min.css",
                "~/Resources/css/demo.min.css",
                "~/Resources/css/font-awesome.min.css",
                "~/Resources/css/typicons.min.css",
                "~/Resources/css/weather-icons.min.css",
                "~/Resources/css/animate.min.css",
                "~/Resources/css/jquery.spin.css"
                
                ));
            bundles.Add(new StyleBundle("~/css/jstree").Include(
                 "~/Resources/css/default/style.min.css"
                ));

            bundles.Add(new StyleBundle("~/css/btnBeyonds").Include(
                "~/Resources/css/beyondbuttons.min.css"
               ));


            bundles.Add(new ScriptBundle("~/bundles/skin").Include(
                "~/Resources/scripts/skins.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/general").Include(
             "~/Resources/scripts/siget.js"));

            bundles.Add(new ScriptBundle("~/bundles/wizard").Include(
               "~/Resources/scripts/fuelux/wizard/wizard-custom.js",
               "~/Resources/scripts/toastr/toastr.js"));
   

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Resources/scripts/bootstrap.min.js",
                "~/Resources/scripts/slimscroll/jquery.slimscroll.min.js",
                "~/Resources/scripts/moment.js",
                "~/Resources/scripts/bootstrap-datetimepicker.js",
                "~/Resources/scripts/select2/select2.js",
                 "~/Resources/scripts/datetime/bootstrap-datepicker.js",
                "~/Resources/scripts/datetime/daterangepicker.js",
                "~/Resources/scripts/validation/bootstrapValidator.js",
                "~/Resources/scripts/datetime/bootstrap-timepicker.js",
                "~/Resources/scripts/intro.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
             "~/Resources/scripts/jquery.min.js",
              "~/Resources/scripts/jquery.mask.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/jstree").Include(
             "~/Resources/scripts/jstree.min.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/alumno").Include(
            "~/Resources/scripts/alumno.js"
            ));



            bundles.Add(new ScriptBundle("~/bundles/input-mask").Include(
                   "~/Resources/scripts/jquery-input-mask/jquery.inputmask.js",
                   "~/Resources/scripts/jquery-input-mask/inputmask.js",
             "~/Resources/scripts/jquery-input-mask/inputmask.date.extensions.js",
             "~/Resources/scripts/jquery-input-mask/inputmask.extensions.js",
             "~/Resources/scripts/jquery-input-mask/inputmask.numeric.extensions.js",
             "~/Resources/scripts/jquery-input-mask/inputmask.phone.extensions.js",
             "~/Resources/scripts/jquery-input-mask/inputmask.regex.extensions.js",
             "~/Resources/scripts/spin.js"  
             ));

            bundles.Add(new ScriptBundle("~/bundles/Utils").Include(
                   "~/Resources/scripts/Utils.js",
                    "~/Resources/scripts/sweetalert/sweetalert.min.js"
                ));
        
            bundles.Add(new ScriptBundle("~/bundles/beyond").Include(
                "~/Resources/scripts/beyond.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Resources/scripts/jqueryval/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/editor").Include(
                       "~/Resources/scripts/ckeditor/ckeditor.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/slider").Include(
                    "~/Resources/scripts/simple_slider/js/simple-slider.min.js"
                      ));

        }
    }
}