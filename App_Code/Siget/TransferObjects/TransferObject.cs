﻿using System;

// 
// TransferObject.vb

using System.Reflection;
// reflexión de vb
using System.Text;

namespace Siget.TransferObjects
{

  /// <summary>
  /// Modelo base de transferencia de datos.
  /// </summary>
  /// <remarks>
  /// 1) Sus atributos deben ser propiedades.
  /// 2) Los nombres de sus propiedades deben coincidir con el nombre de la columna que 
  /// representan en la base de datos.
  /// 
  /// Esto permite que sean consumidos por asp:ObJectDataSource, y permite procesar sus 
  /// propiedades  mediante reflexión.
  /// </remarks>
  public class TransferObject
  {

    // *************************************************
    //                                           Métodos
    // *************************************************

    /// <summary>
    /// Genera una cadena con el nombre y el valor de todas las propiedades públicas y de 
    /// instancia de el objeto separadas por ||.
    /// </summary>
    public override string ToString()
    {
      string separador = " || ";
      StringBuilder returnString = new StringBuilder();
      Type tipo = this.GetType();

      // obtiene solo las propiedades de instancia y públicas
      BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;

      PropertyInfo[] propiedades = tipo.GetProperties(flags);

      foreach (PropertyInfo propiedad in propiedades)
      {
        if ((propiedad.GetValue(this) != null))
        {
          returnString.Append(propiedad.Name.ToString() + ": ");

          if ((propiedad.GetValue(this)) is DateTime)
          {
            returnString.Append(((DateTime)propiedad.GetValue(this)).ToString("dd/MM/yyyy HH:mm:ss") + separador);
          }
          else
          {
            returnString.Append(propiedad.GetValue(this).ToString() + separador);
          }
        }
      }

      return returnString.ToString();
    }

  }

}