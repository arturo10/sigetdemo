﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// EvaluacionTo.vb

namespace Siget.TransferObjects
{

  /// <summary>
  /// Representa un registro de la tabla [Grado] en la base de datos.
  /// </summary>
  public class EvaluacionTo : TransferObject
  {

    // *************************************************
    //                                       Propiedades
    // *************************************************

    private int? _idEvaluacion;
    /// <summary>
    /// int, not null, primary key
    /// </summary>
    public int? IdEvaluacion
    {
      get { return _idEvaluacion; }
      set { _idEvaluacion = value; }
    }

    private int? _idCalificacion;
    /// <summary>
    /// int, not null
    /// </summary>
    public int? IdCalificacion
    {
      get { return _idCalificacion; }
      set { _idCalificacion = value; }
    }

    private string _claveBateria;
    /// <summary>
    /// varchar(200), not null
    /// </summary>
    public string ClaveBateria
    {
      get { return _claveBateria; }
      set { _claveBateria = value; }
    }

    private string _claveAbreviada;
    /// <summary>
    /// varchar(8), not null
    /// </summary>
    public string ClaveAbreviada
    {
      get { return _claveAbreviada; }
      set { _claveAbreviada = value; }
    }

    private decimal? _porcentaje;
    /// <summary>
    /// decimal(5,2), not null
    /// </summary>
    public decimal? Porcentaje
    {
      get { return _porcentaje; }
      set { _porcentaje = value; }
    }

    private DateTime? _inicioContestar;
    /// <summary>
    /// smallDateTime?, not null
    /// </summary>
    public DateTime? InicioContestar
    {
      get { return _inicioContestar; }
      set { _inicioContestar = value; }
    }

    private DateTime? _fechaSinPenalizacion;
    /// <summary>
    /// smallDateTime?, not null
    /// </summary>
    public DateTime? FechaSinPenalizacion
    {
      get { return _fechaSinPenalizacion; }
      set { _fechaSinPenalizacion = value; }
    }

    private DateTime? _finContestar;
    /// <summary>
    /// smallDateTime, not null
    /// </summary>
    public DateTime? FinContestar
    {
      get { return _finContestar; }
      set { _finContestar = value; }
    }

    private string _tipo;
    /// <summary>
    /// vaarchar(15), not null
    /// </summary>
    public string Tipo
    {
      get { return _tipo; }
      set { _tipo = value; }
    }

    private decimal? _penalizacion;
    /// <summary>
    /// decimal(5,2), not null
    /// </summary>
    public decimal? Penalizacion
    {
      get { return _penalizacion; }
      set { _penalizacion = value; }
    }

    private string _estatus;
    /// <summary>
    /// varchar(12), not null
    /// </summary>
    public string Estatus
    {
      get { return _estatus; }
      set { _estatus = value; }
    }

    private short? _aleatoria;
    /// <summary>
    /// smallint, not null
    /// </summary>
    public short? Aleatoria
    {
      get { return _aleatoria; }
      set { _aleatoria = value; }
    }

    private bool? _seEntregaDocto;
    /// <summary>
    /// bit, not null
    /// </summary>
    public bool? SeEntregaDocto
    {
      get { return _seEntregaDocto; }
      set { _seEntregaDocto = value; }
    }

    private char? _califica;
    /// <summary>
    /// char
    /// </summary>
    public char? Califica
    {
      get { return _califica; }
      set { _califica = value; }
    }

    private char? _usarReactivos;
    /// <summary>
    /// char
    /// </summary>
    public char? UsarReactivos
    {
      get { return _usarReactivos; }
      set { _usarReactivos = value; }
    }

    private string _modifico;
    /// <summary>
    /// varchar(20)
    /// </summary>
    public string Modifico
    {
      get { return _modifico; }
      set { _modifico = value; }
    }

    private DateTime? _fechaModif;
    /// <summary>
    /// DateTime
    /// </summary>
    public DateTime? FechaModif
    {
      get { return _fechaModif; }
      set { _fechaModif = value; }
    }

    private short? _alcance;
    /// <summary>
    /// smallint
    /// </summary>
    public short? Alcance
    {
      get { return _alcance; }
      set { _alcance = value; }
    }

    private string _instruccion;
    /// <summary>
    /// varchar(MAX)
    /// </summary>
    public string Instruccion
    {
      get { return _instruccion; }
      set { _instruccion = value; }
    }

    private bool? _permiteSegunda;
    /// <summary>
    /// bit, not null
    /// </summary>
    public bool? PermiteSegunda
    {
      get { return _permiteSegunda; }
      set { _permiteSegunda = value; }
    }

    private bool? _repiteAutomatica;
    /// <summary>
    /// bit, not null
    /// </summary>
    public bool? RepiteAutomatica
    {
      get { return _repiteAutomatica; }
      set { _repiteAutomatica = value; }
    }

    private short? _repiteIndicador;
    /// <summary>
    /// smallint
    /// </summary>
    public short? RepiteIndicador
    {
      get { return _repiteIndicador; }
      set { _repiteIndicador = value; }
    }

    private short? _repiteRango;
    /// <summary>
    /// smallint
    /// </summary>
    public short? RepiteRango
    {
      get { return _repiteRango; }
      set { _repiteRango = value; }
    }

    private short? _repiteMaximo;
    /// <summary>
    /// smallint
    /// </summary>
    public short? RepiteMaximo
    {
      get { return _repiteMaximo; }
      set { _repiteMaximo = value; }
    }

    // *************************************************
    //                                     Constructores
    // *************************************************

    /// <summary>
    /// Constructor base. Inicializa un objeto vacío.
    /// </summary>
    public EvaluacionTo()
    {
      IdEvaluacion = null;
      IdCalificacion = null;
      ClaveBateria = null;
      ClaveAbreviada = null;
      Porcentaje = null;
      InicioContestar = null;
      FechaSinPenalizacion = null;
      FinContestar = null;
      Tipo = null;
      Penalizacion = null;
      Estatus = null;
      Aleatoria = null;
      SeEntregaDocto = null;
      Califica = null;
      UsarReactivos = null;
      Modifico = null;
      FechaModif = null;
      Alcance = null;
      Instruccion = null;
      PermiteSegunda = null;
      RepiteAutomatica = null;
      RepiteIndicador = null;
      RepiteRango = null;
      RepiteMaximo = null;
    }

  }

}