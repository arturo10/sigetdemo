﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
/// <summary>
/// Summary description for AlumnoTo
/// </summary>
/// 

namespace Siget.TransferObjects
{

    public class AlumnoTo : TransferObject
    {

     
        private string _nombre;
        /// <summary>
        /// bigint, not null, primary key
        /// </summary>
        public string Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        private string _apePaterno;
        /// <summary>
        /// bigint, not null, primary key
        /// </summary>
        public string ApePaterno
        {
            get { return _apePaterno; }
            set { _apePaterno = value; }
        }


        private string _apeMaterno;
        /// <summary>
        /// bigint, not null, primary key
        /// </summary>
        public string ApeMaterno
        {
            get { return _apeMaterno; }
            set { _apeMaterno = value; }
        }

        private string _matricula;
        /// <summary>
        /// bigint, not null, primary key
        /// </summary>
        public string Matricula
        {
            get { return _matricula; }
            set { _matricula = value; }
        }

        private string _equipo;
        /// <summary>
        /// bigint, not null, primary key
        /// </summary>
        public string Equipo
        {
            get { return _equipo; }
            set { _equipo = value; }
        }

        private string _subequipo;
        /// <summary>
        /// bigint, not null, primary key
        /// </summary>
        public string SubEquipo
        {
            get { return _subequipo; }
            set { _subequipo = value; }
        }


      
 

        private string _email;
        /// <summary>
        /// bigint, not null, primary key
        /// </summary>
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }


        private DateTime? _fechaIngreso;
        /// <summary>
        /// bigint, not null, primary key
        /// </summary>
        public DateTime? FechaIngreso
        {
            get { return _fechaIngreso; }
            set { _fechaIngreso = value; }
        }



        private string _login;
        /// <summary>
        /// bigint, not null, primary key
        /// </summary>
        public string Login
        {
            get { return _login; }
            set { _login = value; }
        }

        private string _password;
        /// <summary>
        /// bigint, not null, primary key
        /// </summary>
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private string _estatus;
        /// <summary>
        /// bigint, not null, primary key
        /// </summary>
        public string Estatus
        {
            get { return _estatus; }
            set { _estatus = value; }
        }


      
        public AlumnoTo()
        {
           
            this._apeMaterno=string.Empty;
            this._apePaterno = string.Empty;
            this._nombre = string.Empty;
            this._email = string.Empty;
            this._matricula = string.Empty;
            this._fechaIngreso = null;
            this._login = string.Empty;
            this._password = string.Empty;
            this._subequipo = string.Empty;
            this._equipo = string.Empty;
            this._estatus = string.Empty;
        }
    }
}