﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// EvaluacionTerminadaTo.vb

namespace Siget.TransferObjects
{

  /// <summary>
  /// Representa un registro de la tabla [EvaluacionTerminada] en la base de datos.
  /// </summary>
  public class EvaluacionTerminadaTo : TransferObject
  {

    // *************************************************
    //                                     Constructores
    // *************************************************

    private long? _idEvaluacionT;
    /// <summary>
    /// bigint, not null, primary key
    /// </summary>
    public long? IdEvaluacionT
    {
      get { return _idEvaluacionT; }
      set { _idEvaluacionT = value; }
    }

    private long? _idAlumno;
    /// <summary>
    /// bigint, not null
    /// </summary>
    public long? IdAlumno
    {
      get { return _idAlumno; }
      set { _idAlumno = value; }
    }

    private int? _idGrado;
    /// <summary>
    /// int, not null
    /// </summary>
    public int? IdGrado
    {
      get { return _idGrado; }
      set { _idGrado = value; }
    }

    private int? _idGrupo;
    /// <summary>
    /// int, not null
    /// </summary>
    public int? IdGrupo
    {
      get { return _idGrupo; }
      set { _idGrupo = value; }
    }

    private int? _idCicloEscolar;
    /// <summary>
    /// int, not null
    /// </summary>
    public int? IdCicloEscolar
    {
      get { return _idCicloEscolar; }
      set { _idCicloEscolar = value; }
    }

    private int? _idEvaluacion;
    /// <summary>
    /// int
    /// </summary>
    public int? IdEvaluacion
    {
      get { return _idEvaluacion; }
      set { _idEvaluacion = value; }
    }

    private short? _totalReactivos;
    /// <summary>
    /// smallint
    /// </summary>
    public short? TotalReactivos
    {
      get { return _totalReactivos; }
      set { _totalReactivos = value; }
    }

    private short? _reactivosContestados;
    /// <summary>
    /// smallint
    /// </summary>
    public short? ReactivosContestados
    {
      get { return _reactivosContestados; }
      set { _reactivosContestados = value; }
    }

    private decimal? _puntosPosibles;
    /// <summary>
    /// decimal(5,2)
    /// </summary>
    public decimal? PuntosPosibles
    {
      get { return _puntosPosibles; }
      set { _puntosPosibles = value; }
    }

    private decimal? _puntosAcertados;
    /// <summary>
    /// decimal(5,2)
    /// </summary>
    public decimal? PuntosAcertados
    {
      get { return _puntosAcertados; }
      set { _puntosAcertados = value; }
    }

    private decimal? _resultado;
    /// <summary>
    /// decimal(5,2)
    /// </summary>
    public decimal? Resultado
    {
      get { return _resultado; }
      set { _resultado = value; }
    }

    private DateTime? _fechaTermino;
    /// <summary>
    /// smalldatetime
    /// </summary>
    public DateTime? FechaTermino
    {
      get { return _fechaTermino; }
      set { _fechaTermino = value; }
    }


    // *************************************************
    //                                     Constructores
    // *************************************************

    /// <summary>
    /// Constructor base. Inicializa un objeto vacío.
    /// </summary>
    public EvaluacionTerminadaTo()
    {
      IdEvaluacionT = null;
      IdAlumno = null;
      IdGrado = null;
      IdGrupo = null;
      IdCicloEscolar = null;
      IdEvaluacion = null;
      TotalReactivos = null;
      ReactivosContestados = null;
      PuntosPosibles = null;
      PuntosAcertados = null;
      Resultado = null;
      FechaTermino = null;
    }

  }

}