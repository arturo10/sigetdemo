﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// TemaTo.vb

namespace Siget.TransferObjects
{

  /// <summary>
  /// Representa un registro de la tabla [Tema] en la base de datos.
  /// </summary>
  public class TemaTo : TransferObject
  {

    // *************************************************
    //                                       Propiedades
    // *************************************************

    private int? _idtema;
    /// <summary>
    /// int, not null, primary key
    /// </summary>
    public int? IdTema
    {
      get { return _idtema; }
      set { _idtema = value; }
    }

    private int? _idAsignatura;
    /// <summary>
    /// int
    /// </summary>
    public int? IdAsignatura
    {
      get { return _idAsignatura; }
      set { _idAsignatura = value; }
    }

    private string _descripcion;
    /// <summary>
    /// varchar(150), not null
    /// </summary>
    public string Descripcion
    {
      get { return _descripcion; }
      set { _descripcion = value; }
    }

    private short? _numero;
    /// <summary>
    /// smallint
    /// </summary>
    public short? Numero
    {
      get { return _numero; }
      set { _numero = value; }
    }

    private DateTime? _fechaModif;
    /// <summary>
    /// DateTime?
    /// </summary>
    public DateTime? FechaModif
    {
      get { return _fechaModif; }
      set { _fechaModif = value; }
    }

    private string _modifico;
    /// <summary>
    /// varchar(20)
    /// </summary>
    public string Modifico
    {
      get { return _modifico; }
      set { _modifico = value; }
    }

    // *************************************************
    //                                     Constructores
    // *************************************************

    /// <summary>
    /// Constructor base. Inicializa un objeto vacío.
    /// </summary>
    public TemaTo()
    {
      IdTema = null;
      IdAsignatura = null;
      Descripcion = null;
      Numero = null;
      FechaModif = null;
      Modifico = null;
    }

  }

}