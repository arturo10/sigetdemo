﻿// 
// SubtemaTo.vb

namespace Siget.TransferObjects
{

  /// <summary>
  /// Representa un registro de la tabla [Subtema] en la base de datos.
  /// </summary>
  public class SubtemaTo : TransferObject
  {

    // *************************************************
    //                                       Propiedades
    // *************************************************

    private int? _idSubtema;
    /// <summary>
    /// int, not null, primary key
    /// </summary>
    public int? IdSubtema
    {
      get { return _idSubtema; }
      set { _idSubtema = value; }
    }

    private int? _idTema;
    /// <summary>
    /// int
    /// </summary>
    public int? IdTema
    {
      get { return _idTema; }
      set { _idTema = value; }
    }

    private int? _idCompetencia;
    /// <summary>
    /// int
    /// </summary>
    public int? IdCompetencia
    {
      get { return _idCompetencia; }
      set { _idCompetencia = value; }
    }

    private string _descripcion;
    /// <summary>
    /// varchar(250), not null
    /// </summary>
    public string Descripcion
    {
      get { return _descripcion; }
      set { _descripcion = value; }
    }

    private decimal? _numero;
    /// <summary>
    /// decimal(5,2)
    /// </summary>
    public decimal? Numero
    {
      get { return _numero; }
      set { _numero = value; }
    }

    private System.DateTime? _fechaModif;
    /// <summary>
    /// datetime
    /// </summary>
    public System.DateTime? FechaModif
    {
      get { return _fechaModif; }
      set { _fechaModif = value; }
    }

    private string _modifico;
    /// <summary>
    /// varchar(20)
    /// </summary>
    public string Modifico
    {
      get { return _modifico; }
      set { _modifico = value; }
    }

    // *************************************************
    //                                     Constructores
    // *************************************************

    /// <summary>
    /// Constructor base. Inicializa un objeto vacío.
    /// </summary>
    public SubtemaTo()
    {
      IdSubtema = null;
      IdTema = null;
      IdCompetencia = null;
      Descripcion = null;
      Numero = null;
      FechaModif = null;
      Modifico = null;
    }

  }

}