﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// IndicadorTo.vb

namespace Siget.TransferObjects
{

  /// <summary>
  /// Representa un registro de la tabla [Indicador] en la base de datos.
  /// </summary>
  public class IndicadorTo : TransferObject
  {

    // *************************************************
    //                                       Propiedades
    // *************************************************

    private short? _idIndicador;
    /// <summary>
    /// smallint, not null, primary key
    /// </summary>
    public short? IdIndicador
    {
      get { return _idIndicador; }
      set { _idIndicador = value; }
    }

    private string _descripcion;
    /// <summary>
    /// varchar(25)
    /// </summary>
    public string Descripcion
    {
      get { return _descripcion; }
      set { _descripcion = value; }
    }

    private decimal? _minRojo;
    /// <summary>
    /// decimal(5,2), not null
    /// </summary>
    public decimal? MinRojo
    {
      get { return _minRojo; }
      set { _minRojo = value; }
    }

    private decimal? _minAmarillo;
    /// <summary>
    /// decimal(5,2), not null
    /// </summary>
    public decimal? MinAmarillo
    {
      get { return _minAmarillo; }
      set { _minAmarillo = value; }
    }

    private decimal? _minVerde;
    /// <summary>
    /// decimal(5,2), not null
    /// </summary>
    public decimal? MinVerde
    {
      get { return _minVerde; }
      set { _minVerde = value; }
    }

    private decimal? _minAzul;
    /// <summary>
    /// decimal(5,2), not null
    /// </summary>
    public decimal? MinAzul
    {
      get { return _minAzul; }
      set { _minAzul = value; }
    }

    private decimal? _minRojoPorc;
    /// <summary>
    /// decimal(5,2)
    /// </summary>
    public decimal? MinRojoPorc
    {
      get { return _minRojoPorc; }
      set { _minRojoPorc = value; }
    }

    private decimal? _minAmarilloPorc;
    /// <summary>
    /// decimal(5,2)
    /// </summary>
    public decimal? MinAmarilloPorc
    {
      get { return _minAmarilloPorc; }
      set { _minAmarilloPorc = value; }
    }

    private decimal? _minVerdePorc;
    /// <summary>
    /// decimal(5,2)
    /// </summary>
    public decimal? MinVerdePorc
    {
      get { return _minVerdePorc; }
      set { _minVerdePorc = value; }
    }

    private decimal? _minAzulPorc;
    /// <summary>
    /// decimal(5,2)
    /// </summary>
    public decimal? MinAzulPorc
    {
      get { return _minAzulPorc; }
      set { _minAzulPorc = value; }
    }

    private DateTime? _fechaModif;
    /// <summary>
    /// DateTime?
    /// </summary>
    public DateTime? FechaModif
    {
      get { return _fechaModif; }
      set { _fechaModif = value; }
    }

    private string _modifico;
    /// <summary>
    /// varchar(20)
    /// </summary>
    public string Modifico
    {
      get { return _modifico; }
      set { _modifico = value; }
    }

    // *************************************************
    //                                     Constructores
    // *************************************************

    /// <summary>
    /// Constructor base. Inicializa un objeto vacío.
    /// </summary>
    public IndicadorTo()
    {
      IdIndicador = null;
      Descripcion = null;
      MinRojo = null;
      MinAmarillo = null;
      MinVerde = null;
      MinAzul = null;
      MinRojoPorc = null;
      MinAmarilloPorc = null;
      MinVerdePorc = null;
      MinAzulPorc = null;
      FechaModif = null;
      Modifico = null;
    }

  }

}