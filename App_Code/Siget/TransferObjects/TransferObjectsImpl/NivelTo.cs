﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
// 
// NivelTo.vb

namespace Siget.TransferObjects
{

  /// <summary>
  /// Representa un registro de la tabla [Nivel] en la base de datos.
  /// </summary>
  public class NivelTo : TransferObject
  {

    // *************************************************
    //                                       Propiedades
    // *************************************************

    private int? _idNivel;
    /// <summary>
    /// int, not null, primary key
    /// </summary>
    public int? IdNivel
    {
      get { return _idNivel; }
      set { _idNivel = value; }
    }

    private string _descripcion;
    /// <summary>
    /// varchar(50), not null
    /// </summary>
    public string Descripcion
    {
      get { return _descripcion; }
      set { _descripcion = value; }
    }

    private DateTime? _fechaModif;
    /// <summary>
    /// DateTime
    /// </summary>
    public DateTime? FechaModif
    {
      get { return _fechaModif; }
      set { _fechaModif = value; }
    }

    private string _modifico;
    /// <summary>
    /// varchar(20)
    /// </summary>
    public string Modifico
    {
      get { return _modifico; }
      set { _modifico = value; }
    }

    private string _estatus;
    /// <summary>
    /// varchar(12)
    /// </summary>
    public string Estatus
    {
      get { return _estatus; }
      set { _estatus = value; }
    }

    // *************************************************
    //                                     Constructores
    // *************************************************

    /// <summary>
    /// Constructor base. Inicializa un objeto vacío.
    /// </summary>
    public NivelTo()
    {
      IdNivel = null;
      Descripcion = null;
      FechaModif = null;
      Modifico = null;
      Estatus = null;
    }

  }

}