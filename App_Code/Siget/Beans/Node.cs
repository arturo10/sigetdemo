﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Node
/// </summary>
public class Node
{

    private string _text { get; set; }
    public string text
    {
        get
        {
            return this._text;
        }

        set
        {
            this._text = value;
        }
    }

    private string _icon { get; set; }
    public string icon
    {
        get
        {
            return this._icon;
        }

        set
        {
            this._icon = value;
        }
    }

  


}