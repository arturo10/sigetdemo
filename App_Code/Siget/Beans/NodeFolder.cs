﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for NodeFolder
/// </summary>
public class NodeFolder:Node
{
    private List<NodeFile> _children{
        get;set;
    }

    public List<NodeFile> children
    {
        get
        {
            return this._children;
        }

        set
        {
            this._children = value;
        }
    }


    public NodeFolder(string name)
	{
        this.children = new List<NodeFile>();
        this.text = name;
	}

   
}