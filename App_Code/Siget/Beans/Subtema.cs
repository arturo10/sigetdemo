﻿using Siget;
using Siget.TransferObjects;
using System.Collections;


namespace Siget.Beans
{

  /// <summary>
  /// Objeto de transferencia de la tabla Subtema en la Base de Datos.
  /// 
  /// Contiene un conjunto de Planteamientos, y Pertenece a un Tema.
  /// </summary>
  public class Subtema
  {
    //Inherits DatabaseBean

    #region "Constructores"

    /// <summary>
    /// Constructor predeterminado, genera un Planteamiento Inválido
    /// </summary>
    public Subtema()
    {
      datos = new SubtemaTo();

      Planteamientos = new ArrayList();
      PlanteamientosDemo = new ArrayList();
    }

    #endregion

    #region "Atributos"

    /// <summary>
    /// Contiene la información del registro respectivo en la base de datos
    /// </summary>

    public SubtemaTo datos;
    /// <summary>
    /// Identificador principal del DetalleEvaluación que liga 
    /// este subtema con una evaluación que lo utilice.
    /// </summary>
    /// <remarks>Usado durante el contestado de actividades.</remarks>

    public uint IdDetalleEvaluacion = 0;
    /// <summary>
    /// Total de reactivos que contiene el subtema
    /// </summary>
    /// <remarks>Pertenece a DetalleEvalaucion</remarks>

    public int Reactivos = 0;
    /// <summary>
    /// Total de reactivos de práctica que contiene el subtema
    /// </summary>
    /// <remarks>Pertenece a DetalleEvalaucion</remarks>

    public int ReactivosDemo = 0;
    /// <summary>
    /// Número de reactivos a utilizar en la evaluación del total del susbtema
    /// </summary>
    /// <remarks>Pertenece a DetalleEvalaucion</remarks>

    public int? UsarReactivos = null;
    /// <summary>
    /// Contenedor para los Planteamientos que pertenecen a este subtema
    /// </summary>

    public ArrayList Planteamientos;
    /// <summary>
    /// Contenedor para los Planteamientos que pertenecen a este subtema
    /// </summary>

    public ArrayList PlanteamientosDemo;
    #endregion

  }

}