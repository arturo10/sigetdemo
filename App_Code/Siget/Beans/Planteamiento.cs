﻿using Siget;


namespace Siget.Beans
{

  /// <summary>
  /// Objeto de transferencia de la tabla Planteamiento en la Base de Datos.
  /// 
  /// Representa un reactivo a resolver durante una actividad.
  /// 
  /// Contiene un conjunto de Opciones, y Pertenece a un Subtema.
  /// </summary>
  public class Planteamiento
  {
    //Inherits DatabaseBean

    #region "Constructores"

    /// <summary>
    /// Constructor predeterminado, genera un Planteamiento Inválido
    /// </summary>
    public Planteamiento()
    {
      this.IdPlanteamiento = 0;
      this.IdSubtema = 0;
      this.Subtema_Descripcion = string.Empty;
      this.Subtema_Consecutivo = string.Empty;
      this.Consecutivo = -1;
      this.Redaccion = string.Empty;
      this.TipoRespuesta = string.Empty;
      this.Ponderacion = double.NaN;
      this.PorcentajeRestarResp = double.NaN;
      this.ArchivoApoyo = string.Empty;
      this.TipoArchivoApoyo = string.Empty;
      this.ArchivoApoyo2 = string.Empty;
      this.TipoArchivoApoyo2 = string.Empty;
      this.Libro = string.Empty;
      this.Autor = string.Empty;
      this.Editorial = string.Empty;
      this.Ocultar = null;
      this.EsconderTexto = null;
      this.Tiempo = null;
      this.Segundos = 0;
      this.Permite2 = null;
      this.Modifico = string.Empty;
      this.FechaModif = null;
    }

    #endregion

    #region "Atributos"

    private uint _idPlanteamiento;
    /// <summary>
    /// Identificador principal del planteamiento
    /// </summary>
    public uint IdPlanteamiento
    {
      get { return _idPlanteamiento; }
      set { _idPlanteamiento = value; }
    }

    private uint _idSubtema;
    /// <summary>
    /// Identificador del subtema al que pertenece
    /// </summary>
    public uint IdSubtema
    {
      get { return _idSubtema; }
      set { _idSubtema = value; }
    }

    private string _subtema_Descripcion;
    /// <summary>
    /// Descripción del subtema al que pertenece
    /// </summary>
    public string Subtema_Descripcion
    {
      get { return _subtema_Descripcion; }
      set { _subtema_Descripcion = value; }
    }

    private string _subtema_Consecutivo;
    /// <summary>
    /// Consecutivo del subtema al que pertenece
    /// </summary>
    public string Subtema_Consecutivo
    {
      get { return _subtema_Consecutivo; }
      set { _subtema_Consecutivo = value; }
    }

    private int _consecutivo;
    /// <summary>
    /// Consecutivo del planteamiento
    /// </summary>
    public int Consecutivo
    {
      get { return _consecutivo; }
      set { _consecutivo = value; }
    }

    private string _redaccion;
    /// <summary>
    /// Redacción del planteamiento
    /// </summary>
    public string Redaccion
    {
      get { return _redaccion; }
      set { _redaccion = value; }
    }

    private string _tipoRespuesta;
    /// <summary>
    /// Tipo de respuesta del planteamiento ['Multiple', 'Abierta']
    /// </summary>
    public string TipoRespuesta
    {
      get { return _tipoRespuesta; }
      set { _tipoRespuesta = value; }
    }

    private double _ponderacion;
    /// <summary>
    /// Puntos que vale este planteamiento para la ponderación
    /// del resultado de la actividad [0.0, 100.0]
    /// </summary>
    public double Ponderacion
    {
      get { return _ponderacion; }
      set { _ponderacion = value; }
    }

    private double _porcentajeRestarResp;
    /// <summary>
    /// Penalización al contestarse en segunda vuelta [0, 100]
    /// </summary>
    public double PorcentajeRestarResp
    {
      get { return _porcentajeRestarResp; }
      set { _porcentajeRestarResp = value; }
    }

    private string _archivoApoyo;
    /// <summary>
    /// Dirección física al archivo de apoyo 1
    /// </summary>
    public string ArchivoApoyo
    {
      get { return _archivoApoyo; }
      set { _archivoApoyo = value; }
    }

    private string _tipoArchivoApoyo;
    /// <summary>
    /// Tipo del archivo de apoyo 1
    /// </summary>
    public string TipoArchivoApoyo
    {
      get { return _tipoArchivoApoyo; }
      set { _tipoArchivoApoyo = value; }
    }

    private string _archivoApoyo2;
    /// <summary>
    /// Dirección física al archivo de apoyo 2
    /// </summary>
    public string ArchivoApoyo2
    {
      get { return _archivoApoyo2; }
      set { _archivoApoyo2 = value; }
    }

    private string _tipoArchivoApoyo2;
    /// <summary>
    /// Tipo del archivo de apoyo 2
    /// </summary>
    public string TipoArchivoApoyo2
    {
      get { return _tipoArchivoApoyo2; }
      set { _tipoArchivoApoyo2 = value; }
    }

    private string _libro;
    /// <summary>
    /// Referencia de libro
    /// </summary>
    public string Libro
    {
      get { return _libro; }
      set { _libro = value; }
    }

    private string _autor;
    /// <summary>
    /// Referencia de autor
    /// </summary>
    public string Autor
    {
      get { return _autor; }
      set { _autor = value; }
    }

    private string _editorial;
    /// <summary>
    /// Referencia de editorial
    /// </summary>
    public string Editorial
    {
      get { return _editorial; }
      set { _editorial = value; }
    }

    private bool? _ocultar;
    /// <summary>
    /// Indica si el planteamiento es para actividades de práctica (sin calificación)
    /// </summary>
    public bool? Ocultar
    {
      get { return _ocultar; }
      set { _ocultar = value; }
    }

    private bool? _esconderTexto;
    /// <summary>
    /// Indica si se debe o no ocultar la redacción del planteamiento al mostrarse
    /// </summary>
    public bool? EsconderTexto
    {
      get { return _esconderTexto; }
      set { _esconderTexto = value; }
    }

    private bool? _tiempo;
    /// <summary>
    /// Indica si el planteamiento tiene un límite de tiempo en el cual puede ser respondido
    /// </summary>
    public bool? Tiempo
    {
      get { return _tiempo; }
      set { _tiempo = value; }
    }

    private uint _segundos;
    /// <summary>
    /// Número de segundos con los que se dispone para contestar el reactivo cuando éste es de tiempo
    /// </summary>
    public uint Segundos
    {
      get { return _segundos; }
      set { _segundos = value; }
    }

    private bool? _permite2;
    /// <summary>
    /// Indica si este reactivo puede corregirse en segunda vuelta
    /// </summary>
    public bool? Permite2
    {
      get { return _permite2; }
      set { _permite2 = value; }
    }

    private string _modifico;
    /// <summary>
    /// Registro de control de modificaciones
    /// </summary>
    public string Modifico
    {
      get { return _modifico; }
      set { _modifico = value; }
    }

    private System.DateTime? _fechaModif;
    /// <summary>
    /// Registro de control de modificaciones
    /// </summary>
    public System.DateTime? FechaModif
    {
      get { return _fechaModif; }
      set { _fechaModif = value; }
    }

    #endregion

  }

}