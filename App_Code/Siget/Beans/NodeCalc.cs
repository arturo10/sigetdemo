﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for NodeCalcs
/// </summary>
public class NodeCalc:Node
{
    private int _id { get; set; }

    public int id
    {
        get
        {
            return this._id;
        }

        set
        {
            this._id = value;
        }
    }


    private List<NodeFolder> _children
    {
        get;set;
    }

    public List<NodeFolder> children
    {
        get
        {
            return this._children;
        }

        set
        {
            this._children = value;
        }
    }


    public NodeCalc(string name)
	{
        this.children = new List<NodeFolder>();
        this.text = name;
	}

    public NodeCalc(string name,int id)
    {
        this.children = new List<NodeFolder>();
        this.text = name;
        this.id = id;
    }
}