﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;


/// <summary>
/// Summary description for Mensaje
/// </summary>
public class Message
{
    private string _message {get;set;}
    public string message { get { return this._message; } set { this._message = value; } }
    
    private Control _control { get; set; }
    public Control control { get { return this._control; } set { this._control = value; } }

    private Type _type { get; set; }
    public Type type { get { return this._type; } set { this._type = value; } }



    public Message(string message, Control control, Type type)
	{
        this._message = message;
        this._control = control;
        this._type = type;
	}
  
}