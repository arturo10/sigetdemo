﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Node
/// </summary>
public class NodeRoot:Node
{
    private List<NodeCalc> _children
    {
        get;set;
    }

    public List<NodeCalc> children
    {
        get
        {
            return this._children;
        }

        set
        {
            this._children = value;
        }
    }


    public NodeRoot(string name)
	{
        this.children = new List<NodeCalc>();
        this.text = name;
	}

   
}