﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Siget;
using System.Collections;


namespace Siget.Beans
{

  public class Asignatura
  {
    //Inherits DatabaseBean

    // Constructor principal
    public Asignatura()
    {
      //IdAsignatura = 0
      //IdGrado = 0
      //IdArea = 0
      //Descripcion = String.Empty
      //Modifico = String.Empty
      //FechaModif = Nothing
      IdIndicador = null;
      Calificaciones = new ArrayList();
      startDate = System.DateTime.MaxValue;
      endDate = System.DateTime.MinValue;
    }

    // Constructor con IdAsignatura
    public Asignatura(uint Idasignatura)
    {
      setIdAsignatura(Idasignatura);
    }

    #region "Atributos"

    // Primary Key Identity del registro

    private uint _idAsignatura;
    // Foreign key de la tabla Grado

    private uint _idGrado;
    // Foreign key de la tabla Área

    private uint _idArea;
    // Descripción principal

    private string _descripcion;
    private short? _idIndicador;
    /// <summary>
    /// smallint, not null
    /// </summary>
    public short? IdIndicador
    {
      get { return _idIndicador; }
      set { _idIndicador = value; }
    }

    // Registros de control de modificaciones
    private string _modifico;

    private System.DateTime _fechaModif;
    // Contenedor para las Calificaciones que pertenecen a ésta asignatura

    private ArrayList Calificaciones;
    // *********************************************************************************************
    // the following are work variables

    // stores the minimum and maximum dates to complete activities of this course
    private System.DateTime startDate;

    private System.DateTime endDate;
    #endregion

    #region "Getters & Setters"

    public void setCalificaciones(ref ArrayList Calificaciones)
    {
      this.Calificaciones = Calificaciones;
    }

    public ArrayList getCalificaciones()
    {
      return this.Calificaciones;
    }

    public void setStartDate(System.DateTime startDate)
    {
      this.startDate = startDate;
    }

    public System.DateTime getStartDate()
    {
      return this.startDate;
    }

    public void setEndDate(System.DateTime endDate)
    {
      this.endDate = endDate;
    }

    public System.DateTime getEndDate()
    {
      return this.endDate;
    }

    // *********************************************************************************************

    public void setIdAsignatura(uint IdAsignatura)
    {
      this._idAsignatura = IdAsignatura;
    }

    public uint getIdAsignatura()
    {
      return this._idAsignatura;
    }

    public void setIdGrado(uint IdGrado)
    {
      this._idGrado = IdGrado;
    }

    public uint getIdGrado()
    {
      return this._idGrado;
    }

    public void setIdArea(uint IdArea)
    {
      this._idArea = IdArea;
    }

    public uint getIdArea()
    {
      return this._idArea;
    }

    public void setDescripcion(string Descripcion)
    {
      this._descripcion = Descripcion;
    }

    public string getDescripcion()
    {
      return this._descripcion;
    }

    public string getModifico()
    {
      return this._modifico;
    }

    public void setModifico(string Modifico)
    {
      this._modifico = Modifico;
    }

    public DateTime getFechaModif()
    {
      return this._fechaModif;
    }

    public void setFechaModif(ref DateTime FechaModif)
    {
      this._fechaModif = FechaModif;
    }

    #endregion

    #region "Methods"

    public void obtainStartDate()
    {
      if (!(getCalificaciones().Count == 0))
      {
        // para cada calificación de la asignatura
        foreach (Calificacion c in getCalificaciones())
        {
          // hago que obtenga la mínima fecha de inicio de sus evaluaciones
          c.ObtenFechaInicio();

          if (c.FechaInicio < getStartDate())
          {
            // si es menor a la guardada actualmente, la sobreescribo
            setStartDate(c.FechaInicio);
          }
        }
      }
    }

    public void obtainEndDate()
    {
      if (!(getCalificaciones().Count == 0))
      {
        // para cada evaluación de la calificación
        foreach (Calificacion c in getCalificaciones())
        {
          // hago que obtenga la máxima fecha de termino de sus evaluaciones
          c.ObtenFechaFin();

          if (c.FechaFin > getEndDate())
          {
            // si es mayor a la guardada actualmente, la sobreescribo
            setEndDate(c.FechaFin);
          }
        }
      }
    }

    #endregion

    #region "Core"

    public override string ToString()
    {
      return getIdAsignatura() + "_" + getDescripcion() + "<br />";
    }

    #endregion

  }

}