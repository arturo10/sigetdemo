﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;

/// <summary>
/// Summary description for MessageSuccess
/// </summary>
public class MessageSuccess : Message
{
	public MessageSuccess(Control control, Type type,string message):base(message,control, type)
	{   
        StringBuilder build=new StringBuilder( "swal({ title:'!!ÉXITO¡¡',text:'");
        build.Append(this.message);
        build.Append( "',type:'success',confirmButtonColor:'#546e7a',confirmButtonText:'Cerrar',timer:1500, html: true});");
        this.message = build.ToString();
    }

    public void show()
    {
        ScriptManager.RegisterStartupScript(this.control, this.type,  "exito",this.message, true);
    }
}