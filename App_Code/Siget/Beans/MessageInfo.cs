﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;

/// <summary>
/// Summary description for MessageInfo
/// </summary>
public class MessageInfo : Message
{
    public MessageInfo(Control control, Type type, string message)
        : base(message, control, type)
	{
        string messageTemp = this.message.Replace("\r\n", "").Replace("\n", "").Replace("\r", "").Replace("\"", "").Replace("'", "");
        StringBuilder build=new StringBuilder( "swal({ title:'!!ÉXITO¡¡',text:'");
        build.Append(messageTemp);
        build.Append("',type:'info',confirmButtonColor:'#546e7a',confirmButtonText:'Cerrar', html: true});");
        this.message = build.ToString();
    }

    public void show()
    {
        ScriptManager.RegisterStartupScript(this.control, this.type,  "info",this.message, true);
    }
}