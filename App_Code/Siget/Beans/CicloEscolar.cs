﻿using Siget;


namespace Siget.Beans
{

  /// <summary>
  /// Objeto de transferencia de la tabla CicloEscolar en la base de datos.
  /// </summary>
  public class CicloEscolar
  {
    //Inherits DatabaseBean

    #region "Constructores"

    /// <summary>
    /// Constructor predeterminado, genera un CicloEscolar inválido
    /// </summary>
    public CicloEscolar()
    {
      IdCicloEscolar = 0;
      IdIndicador = 0;
      Descripcion = string.Empty;
      FechaInicio = System.DateTime.MinValue;
      FechaFin = System.DateTime.MinValue;
      Estatus = string.Empty;
      FechaModif = System.DateTime.MinValue;
      Modifico = string.Empty;
    }

    // Constructor con IdCicloEscolar
    public CicloEscolar(ref uint idCicloEscolar)
      : this()
    {
      this.IdCicloEscolar = idCicloEscolar;
    }

    #endregion

    #region "Atributos"

    private uint _idCicloEscolar;
    public uint IdCicloEscolar
    {
      get { return _idCicloEscolar; }
      set { _idCicloEscolar = value; }
    }

    private uint _idIndicador;
    public uint IdIndicador
    {
      get { return _idIndicador; }
      set { _idIndicador = value; }
    }

    private string _descripcion;
    public string Descripcion
    {
      get { return _descripcion; }
      set { _descripcion = value; }
    }

    private System.DateTime _fechaInicio;
    public System.DateTime FechaInicio
    {
      get { return _fechaInicio; }
      set { _fechaInicio = value; }
    }

    private System.DateTime _fechaFin;
    public System.DateTime FechaFin
    {
      get { return _fechaFin; }
      set { _fechaFin = value; }
    }

    private string _estatus;
    public string Estatus
    {
      get { return _estatus; }
      set { _estatus = value; }
    }

    private string _modifico;
    /// <summary>
    /// Registro de control de modificaciones
    /// </summary>
    public string Modifico
    {
      get { return _modifico; }
      set { _modifico = value; }
    }

    private System.DateTime _fechaModif;
    /// <summary>
    /// Registro de control de modificaciones
    /// </summary>
    public System.DateTime FechaModif
    {
      get { return _fechaModif; }
      set { _fechaModif = value; }
    }

    #endregion

    #region "Metodos"

    #endregion

    #region "Core"

    public override string ToString()
    {
      return IdCicloEscolar + " " + Descripcion + " " + FechaInicio.ToShortDateString() + " - " + FechaFin.ToShortDateString() + " " + Estatus + "<br />";
    }

    #endregion

  }

}