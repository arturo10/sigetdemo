﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for NodeFile
/// </summary>
public class NodeFile : Node
{

    private string _data { get; set; }
    public string data
    {

        get
        {
            return this._data;
        }

        set
        {
            this._data = value;
        }

    }


    public NodeFile(string name)
    {

        this.text = name;
    }

  

}