﻿using Siget;


namespace Siget.Beans
{

  /// <summary>
  /// Objeto de transferencia de la tabla Opcion en la Base de Datos.
  /// 
  /// Representa una opción de respuesta de un planteamiento.
  /// 
  /// Pertenece a un Planteamiento.
  /// </summary>
  public class Opcion
  {
    //Inherits DatabaseBean

    #region "Constructores"

    /// <summary>
    /// Constructor predeterminado, genera una Opción Inválida
    /// </summary>
    public Opcion()
    {
      IdOpcion = 0;
      IdPlanteamiento = 0;
      Redaccion = string.Empty;
      Correcta = false;
      Resp1 = string.Empty;
      Resp2 = string.Empty;
      Consecutiva = -1;
      ArchivoApoyo = string.Empty;
      TipoArchivoApoyo = string.Empty;
      Esconder = false;
      SeCalifica = false;
      FechaModif = null;
      Modifico = string.Empty;
    }

    #endregion

    #region "Atributos"

    private uint _idOpcion;
    /// <summary>
    /// Identificador principal de la opción
    /// </summary>
    public uint IdOpcion
    {
      get { return _idOpcion; }
      set { _idOpcion = value; }
    }

    private uint _idPlanteamiento;
    /// <summary>
    /// Identificador principal del planteamiento al que pertenece esta opción
    /// </summary>
    public uint IdPlanteamiento
    {
      get { return _idPlanteamiento; }
      set { _idPlanteamiento = value; }
    }

    private string _redaccion;
    /// <summary>
    /// Redacción principal del planteamiento
    /// </summary>
    public string Redaccion
    {
      get { return _redaccion; }
      set { _redaccion = value; }
    }

    private bool _correcta;
    /// <summary>
    /// Indicador de si es una respuesta correcta.
    /// </summary>
    public bool Correcta
    {
      get { return _correcta; }
      set { _correcta = value; }
    }

    private string _resp1;
    /// <summary>
    /// 
    /// </summary>
    public string Resp1
    {
      get { return _resp1; }
      set { _resp1 = value; }
    }

    private string _resp2;
    /// <summary>
    /// 
    /// </summary>
    public string Resp2
    {
      get { return _resp2; }
      set { _resp2 = value; }
    }

    private int _consecutiva;
    /// <summary>
    /// Inciso 
    /// </summary>
    public int Consecutiva
    {
      get { return _consecutiva; }
      set { _consecutiva = value; }
    }

    private string _archivoApoyo;
    /// <summary>
    /// Nombre del archivo de apoyo de la opción.
    /// </summary>
    public string ArchivoApoyo
    {
      get { return _archivoApoyo; }
      set { _archivoApoyo = value; }
    }

    private string _tipoArchivoApoyo;
    /// <summary>
    /// Tipo de Archivo de Apoyo
    /// </summary>
    public string TipoArchivoApoyo
    {
      get { return _tipoArchivoApoyo; }
      set { _tipoArchivoApoyo = value; }
    }

    private bool _esconder;
    /// <summary>
    /// 
    /// </summary>
    public bool Esconder
    {
      get { return _esconder; }
      set { _esconder = value; }
    }

    private bool _seCalifica;
    /// <summary>
    /// 
    /// </summary>
    public bool SeCalifica
    {
      get { return _seCalifica; }
      set { _seCalifica = value; }
    }

    private string _modifico;
    /// <summary>
    /// Registro de control de modificaciones
    /// </summary>
    public string Modifico
    {
      get { return _modifico; }
      set { _modifico = value; }
    }

    private System.DateTime? _fechaModif;
    /// <summary>
    /// Registro de control de modificaciones
    /// </summary>
    public System.DateTime? FechaModif
    {
      get { return _fechaModif; }
      set { _fechaModif = value; }
    }

    #endregion

  }

}