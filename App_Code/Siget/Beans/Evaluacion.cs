﻿using Siget;


namespace Siget.Beans
{

    public class Evaluacion
    {
        //Inherits DatabaseBean

        // Constructor principal
        public Evaluacion()
        {
            IdEvaluacion = 0;
            IdCalificacion = 0;
            ClaveBateria = string.Empty;
            ClaveAbreviada = string.Empty;
            Porcentaje = -1;
            InicioContestar = null;
            FinSinPenalizacion = null;
            FinContestar = null;
            Tipo = string.Empty;
            Penalizacion = -1;
            Estatus = string.Empty;
            Aleatoria = 0;
            OpcionesAleatorias = 0;
            SeEntregaDocto = false;
            Califica = '.';
            UsarReactivos = 0;
            PermiteSegunda = false;
            Modifico = string.Empty;
            FechaModif = null;
            Alcance = -1;
            Instruccion = string.Empty;
            RepiteAutomatica = false;
            RepiteIdIndicador = 0;
            RepiteRango = -1;
            RepiteMaximo = -1;
            ResultadoTerminada = 0;

        }

        #region "Atributos"

        /// <summary>
        /// Primary Key Identity del registro
        /// </summary>

        public uint IdEvaluacion;
        /// <summary>
        /// Identificador de la calificación a la que pertenece
        /// </summary>
        /// <remarks>
        /// Foreign key de la tabla Calificacion
        /// </remarks>

        public uint IdCalificacion;
        /// <summary>
        /// Descripción principal
        /// </summary>

        public string ClaveBateria;
        /// <summary>
        /// Descripción abreviada
        /// </summary>

        public string ClaveAbreviada;
        /// <summary>
        /// Porcentaje de valuación que representa de la calificación
        /// </summary>

        public decimal Porcentaje;
        /// <summary>
        /// Fecha de apertura general para realizarse
        /// </summary>

        public System.DateTime? InicioContestar;
        /// <summary>
        /// Fecha de penalización aplicada al realizarse
        /// </summary>

        public System.DateTime? FinSinPenalizacion;
        /// <summary>
        /// Fecha límite para realizarse
        /// </summary>

        public System.DateTime? FinContestar;
        /// <summary>
        /// Indicador si es fecha de nivel Alumno (2), Plantel (1), o Institución (0)
        /// 
        /// Se utiliza para indicar el nivel de fecha detectado al presentar actividades para el alumno,
        /// pero también puede indicar el nivel de profundidad de un reporte.
        /// </summary>

        public short Alcance;
        /// <summary>
        /// Tipo de evaluación
        /// </summary>

        public string Tipo;
        /// <summary>
        /// Porcentaje de penalización durante el periodo de penalización
        /// </summary>

        public decimal Penalizacion;
        /// <summary>
        /// Estado de la actividad
        /// </summary>

        public string Estatus;
        /// <summary>
        /// Despliega o no aleatoriamente los reactivos
        /// 0 = no aleatorio
        /// 1 = aleatorio por subtema
        /// 2 = aleatorio del total
        /// </summary>

        public short Aleatoria;

     /// <summary>
      /// Despliega o no aleatoriamente los reactivos
      /// 0 = no aleatorio
      /// 1 = aleatorio 
      /// </summary>
    public short OpcionesAleatorias;

        /// <summary>
        /// Es de entrega de documento
        /// </summary>
        /// 

        public bool SeEntregaDocto;
    /// <summary>
    /// Quién califica la actividad
    /// </summary>

    public char Califica;
    /// <summary>
    /// Número de reactivos que se desplegarán aleatoriamente.
    /// Null significa todos, >= 0 indica una cantidad específica
    /// </summary>

    public uint UsarReactivos;
    /// <summary>
    /// La fecha en la que se terminó la actividad o ésta se calificó
    /// </summary>

    public System.DateTime FechaTermino;
    /// <summary>
    /// Indica si la evaluación permite realizar una segunda vuelta 
    /// con los reactivos de ella que también indiquen segunda vuelta.
    /// </summary>

    public bool PermiteSegunda;
    /// <summary>
    /// Registro de control de modificaciones
    /// </summary>

    public string Modifico;
    /// <summary>
    /// Registro de control de modificaciones
    /// </summary>

    public System.DateTime? FechaModif;
    // *********************************************************************************************
    // Contestado de Evaluaciones
    // *********************************************************************************************

    /// <summary>
    /// Indica si el alumno tiene respuestas abiertas guardadas como borradores en esta actividad
    /// </summary>

    public bool Borrador;
    /// <summary>
    /// Indica el resultado de la evaluación terminada para el alumno
    /// </summary>

    public decimal ResultadoTerminada;
    /// <summary>
    /// Nombre del archivo entregado cuando la actividad es de entrega de documentos
    /// </summary>

    public string Entrega;
    // *********************************************************************************************
    // Repetición
    // *********************************************************************************************

    /// <summary>
    /// Instrucción de la evalaución, con markup html.
    /// </summary>

    public string Instruccion;
    /// <summary>
    /// Indica si el sistema debe habilitar una segunda vuelta desde cero 
    /// cuando el alumno termine con un resultado menos a un indicador
    /// determinado.
    /// </summary>

    public bool RepiteAutomatica;
    /// <summary>
    /// Id del indicador al que está atada la repetición automática
    /// </summary>

    public uint RepiteIdIndicador;
    /// <summary>
    /// Utilizado para identificar al rango que debe alcanzarse para no repetir; 
    /// 0-rojo; 
    /// 1-Amarillo; 
    /// 2-Verde; 
    /// 3-Azul
    /// </summary>

    public short RepiteRango;
    /// <summary>
    /// Establece el límite de repeticiones que se permiten automáticamente. 
    /// Si se encuentra en el límite el sistema no repite, 
    /// pero un operador puede habilitar repetición manualmente.
    /// </summary>

    public short RepiteMaximo;
    #endregion

    #region "funciones de estado"

    public bool EsBorrador()
    {
      return this.Borrador;
    }

    public bool EsTerminada()
    {
      if (this.ResultadoTerminada < 0)
      {
        return false;
      }
      else
      {
        return true;
      }
    }

    public bool TieneEntrega()
    {
      if (Entrega == string.Empty)
      {
        return false;
      }
      else
      {
        return true;
      }
    }


    public void set1(ref string s)
    {
    }

    public void set1(ref int s)
    {
    }

    #endregion

    #region "Core"

    public override string ToString()
    {
      return IdEvaluacion + "_" + ClaveBateria + "_" + Tipo + "_" + Penalizacion + "_" + SeEntregaDocto + "_" + InicioContestar + "_" + Borrador + "_" + Entrega + "<br />";
    }

    #endregion

  }

}