﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;

/// <summary>
/// Summary description for MessageError
/// </summary>
public class MessageError:Message
{

    public MessageError(Control control, Type type, string message)
        : base(message, control, type)
	{

        string urlImageMsgError ="&quot;"+ Siget.Config.Global.urlBase + "//Resources//imagenes//msgError.png&quot;";
        string messageTemp = this.message.Replace("\r\n", "").Replace("\n", "").Replace("\r", "").Replace("\"","").Replace("'","") ;
        StringBuilder build = new StringBuilder("swal({ title:'<h2>¡¡ERROR!!</h2>',imageUrl: ");
        build.Append(HttpUtility.HtmlDecode(urlImageMsgError));
        build.Append(",text:");
        build.Append('\"' + messageTemp + "\"");
        build.Append(",confirmButtonColor:'#546e7a',confirmButtonText:'Cerrar', html: true});");
        this.message = build.ToString();
    }

    public MessageError(Control control, Type type, string message,string title)
        : base(message, control, type)
    {

        string urlImageMsgError = "&quot;" + Siget.Config.Global.urlBase + "//Resources//imagenes//msgError.png&quot;";
        string messageTemp = this.message.Replace("\r\n", "").Replace("\n", "").Replace("\r", "").Replace("\"", "").Replace("'", "");
        StringBuilder build = new StringBuilder("swal({ title:'<h2>"+title+"</h2>',imageUrl: ");
        build.Append(HttpUtility.HtmlDecode(urlImageMsgError));
        build.Append(",text:");
        build.Append('\"' + messageTemp + "\"");
        build.Append(",confirmButtonColor:'#546e7a',confirmButtonText:'Cerrar', html: true});");
        this.message = build.ToString();
    }

    public void show()
    {
        ScriptManager.RegisterStartupScript(this.control, this.type,  "error",this.message, true);
    }
}