﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Siget;
using System.Collections;


namespace Siget.Beans
{

  /// <summary>
  /// Objeto de transferencia de la tabla Calificacion en la base de datos.
  /// 
  /// Contiene un conjunto de actividades, y pertenece a una asignatura.
  /// </summary>
  public class Calificacion
  {
    //Inherits DatabaseBean

    #region "Constructores"

    /// <summary>
    /// Constructor predeterminado, genera una Calificación inválida
    /// </summary>
    public Calificacion()
    {
      this.IdCalificacion = 0;
      this.IdCicloEscolar = 0;
      this.CicloEscolar_Nombre = string.Empty;
      this.IdAsignatura = 0;
      this.IdAsignatura_Nombre = string.Empty;
      this.IdIndicador = null;
      this.IdIndicador_Nombre = string.Empty;
      this.Consecutivo = -1;
      this.Descripcion = string.Empty;
      this.Clave = string.Empty;
      this.Valor = -1;
      this.Modifico = string.Empty;
      this.FechaModif = System.DateTime.MinValue;
      this.FechaInicio = System.DateTime.MaxValue;
      this.FechaFin = System.DateTime.MinValue;
      this.TotalActividades = 0;
      this.ActividadesTerminadas = 0;

      this.Evaluaciones = new ArrayList();
    }

    // Constructor con IdCalificacion
    public Calificacion(uint idCalificacion)
      : this()
    {
      this.IdCalificacion = idCalificacion;
    }

    #endregion

    #region "Atributos"

    private uint _idCalificacion;
    /// <summary>
    /// Primary Key Identity del registro
    /// </summary>
    public uint IdCalificacion
    {
      get { return _idCalificacion; }
      set { _idCalificacion = value; }
    }

    private uint _idCicloEscolar;
    /// <summary>
    /// Identificador del ciclo escolar al que pertenece esta calificación
    /// </summary>
    public uint IdCicloEscolar
    {
      get { return _idCicloEscolar; }
      set { _idCicloEscolar = value; }
    }

    private string _cicloEscolar_Nombre;
    /// <summary>
    /// Nombre del ciclo escolar al que pertenece esta calificación
    /// </summary>
    public string CicloEscolar_Nombre
    {
      get { return _cicloEscolar_Nombre; }
      set { _cicloEscolar_Nombre = value; }
    }

    private uint _idAsignatura;
    /// <summary>
    /// Identificador de la asignatura a la que pertenece
    /// </summary>
    public uint IdAsignatura
    {
      get { return _idAsignatura; }
      set { _idAsignatura = value; }
    }

    private string _idAsignatura_Nombre;
    /// <summary>
    /// Nombre de la asignatura a la que pertenece
    /// </summary>
    public string IdAsignatura_Nombre
    {
      get { return _idAsignatura_Nombre; }
      set { _idAsignatura_Nombre = value; }
    }

    private short? _idIndicador;
    /// <summary>
    /// Identificador del indicador que utiliza
    /// </summary>
    public short? IdIndicador
    {
      get { return _idIndicador; }
      set { _idIndicador = value; }
    }

    private string _indicador_Nombre;
    /// <summary>
    /// Nombre del indicador al que pertenece
    /// </summary>
    public string IdIndicador_Nombre
    {
      get { return _indicador_Nombre; }
      set { _indicador_Nombre = value; }
    }

    private int _consecutivo;
    /// <summary>
    /// Identificador de orden dentro de la asignatura a la que pertenece esta calificación
    /// </summary>
    public int Consecutivo
    {
      get { return _consecutivo; }
      set { _consecutivo = value; }
    }

    private string _descripcion;
    /// <summary>
    /// Descripción principal
    /// </summary>
    public string Descripcion
    {
      get { return _descripcion; }
      set { _descripcion = value; }
    }

    // 
    private string _clave;
    /// <summary>
    /// Abreviación para reportes
    /// </summary>
    public string Clave
    {
      get { return _clave; }
      set { _clave = value; }
    }

    private decimal _valor;
    /// <summary>
    /// Porcentaje de valuación que representa de la asignatura
    /// </summary>
    public decimal Valor
    {
      get { return _valor; }
      set { _valor = value; }
    }

    private string _modifico;
    /// <summary>
    /// Registro de control de modificaciones
    /// </summary>
    public string Modifico
    {
      get { return _modifico; }
      set { _modifico = value; }
    }

    private System.DateTime _fechaModif;
    /// <summary>
    /// Registro de control de modificaciones
    /// </summary>
    public System.DateTime FechaModif
    {
      get { return _fechaModif; }
      set { _fechaModif = value; }
    }

    private ArrayList _evaluaciones;
    /// <summary>
    /// Contenedor para las Evaluaciones que pertenecen a ésta calificación
    /// </summary>
    public ArrayList Evaluaciones
    {
      get { return _evaluaciones; }
      set { _evaluaciones = value; }
    }

    // *********************************************************************************************
    // Las siguientes son variables de trabajo

    private System.DateTime _fechaInicio;
    /// <summary>
    /// Almacena la fecha de inicio más chica de las actividades de esta calificación
    /// </summary>
    public System.DateTime FechaInicio
    {
      get { return _fechaInicio; }
      set { _fechaInicio = value; }
    }

    private System.DateTime _fechaFin;
    /// <summary>
    /// Almacena la fecha límite más grande de las actividades de esta calificación
    /// </summary>
    public System.DateTime FechaFin
    {
      get { return _fechaFin; }
      set { _fechaFin = value; }
    }

    private int _totalActividades;
    /// <summary>
    /// Contador que indica el total de actividades que contiene esta calificación
    /// </summary>
    public int TotalActividades
    {
      get { return _totalActividades; }
      set { _totalActividades = value; }
    }

    private int _actividadesTerminadas;
    /// <summary>
    /// Contador que indica el número de actividades terminadas que contiene esta calificación
    /// </summary>
    public int ActividadesTerminadas
    {
      get { return _actividadesTerminadas; }
      set { _actividadesTerminadas = value; }
    }

    private decimal _ResultadoTerminada;
    /// <summary>
    /// Indica la calificación obtenida del conjunto de 
    /// evaluaciones terminadas de esta calificación.
    /// </summary>
    public decimal ResultadoTerminada
    {
      get { return _ResultadoTerminada; }
      set { _ResultadoTerminada = value; }
    }

    #endregion

    #region "Metodos"

    /// <summary>
    /// Incrementa el total de actividades
    /// </summary>
    public void IncrementaTotalActividades()
    {
      this._totalActividades += 1;
    }

    /// <summary>
    /// Incrementa el total de actividades terminadas
    /// </summary>
    public void IncrementaActividadesTerminadas()
    {
      this._actividadesTerminadas += 1;
    }

    /// <summary>
    /// Obtiene la fecha de inicio más chica de las evaluaciones que contiene
    /// </summary>
    public void ObtenFechaInicio()
    {
      if (!(Evaluaciones.Count == 0))
      {
        // para cada evaluación de la calificación
        foreach (Evaluacion e in Evaluaciones)
        {
          if (e.InicioContestar < FechaInicio)
          {
            FechaInicio = e.InicioContestar.Value;
          }
        }
      }
    }

    /// <summary>
    /// Obtiene la fecha de fin más grande de las evaluaciones que contiene
    /// </summary>
    public void ObtenFechaFin()
    {
      if (!(Evaluaciones.Count == 0))
      {
        // para cada evaluación de la calificación
        foreach (Evaluacion e in Evaluaciones)
        {
          if (e.FinContestar > FechaFin)
          {
            FechaFin = e.FinContestar.Value;
          }
        }
      }
    }

    /// <summary>
    /// Cuenta el número de evaluaciones terminadas que contiene
    /// </summary>
    public void CuentaActividadesTerminadas()
    {
      if (!(Evaluaciones.Count == 0))
      {
        // para cada evaluación de la calificación
        foreach (Evaluacion e in Evaluaciones)
        {
          // incremento el total de evaluaciones
          IncrementaTotalActividades();
          // y si tiene calificacion, incremento las contestadas
          if (e.ResultadoTerminada >= 0)
          {
            IncrementaActividadesTerminadas();
          }
        }
      }
    }

    /// <summary>
    /// Totaliza los resultados de las evaluaciones de esta calificación.
    /// </summary>
    public void EvaluaCalificacion()
    {
      CuentaActividadesTerminadas();

      ResultadoTerminada = 0;
      decimal totalPuntos = 0;
      foreach (Evaluacion eval in Evaluaciones)
      {
        totalPuntos += eval.Porcentaje;
        if (eval.ResultadoTerminada > 0)
        {
          ResultadoTerminada += (eval.ResultadoTerminada * eval.Porcentaje);
        }
      }
      if (totalPuntos == 0)
      {
        ResultadoTerminada = 100;
      }
      else
      {
        ResultadoTerminada /= totalPuntos;
      }
    }

    #endregion

    #region "Core"

    public override string ToString()
    {
      return IdCalificacion + " " + Consecutivo + " " + Descripcion + "<br />";
    }

    #endregion

  }

}