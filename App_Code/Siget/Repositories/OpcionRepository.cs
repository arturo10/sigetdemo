﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;
/// <summary>
/// Summary description for EvaluacionRepository
/// </summary>
public class OpcionRepository:IRepository<Opcion>
{
    public sigetdemo_sadcomeEntities _evaluacionContext;
     public OpcionRepository()
	{
        _evaluacionContext = new sigetdemo_sadcomeEntities();
	}

     public IEnumerable<Opcion> List
    {
        get
        {
            return _evaluacionContext.Opcions;
        }

    }

     public void Add(Opcion entity)
    {
        _evaluacionContext.Opcions.Add(entity);
        _evaluacionContext.SaveChanges();
    }

     public void Delete(Opcion entity)
    {
        _evaluacionContext.Opcions.Remove(entity);
        _evaluacionContext.SaveChanges();
    }

     public void Update(Opcion entity)
    {
        _evaluacionContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _evaluacionContext.SaveChanges();

    }

     public Opcion FindById(int Id)
    {
        var result = (from r in _evaluacionContext.Opcions where r.IdOpcion == Id select r).FirstOrDefault();
        return result;
    }
}