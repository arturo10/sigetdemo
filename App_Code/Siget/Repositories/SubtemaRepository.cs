﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;
/// <summary>
/// Summary description for SubtemaRepository
/// </summary>
public class SubtemaRepository : IRepository<Subtema>
{
    public sigetdemo_sadcomeEntities _temaContext;
	public SubtemaRepository()
	{
        _temaContext = new sigetdemo_sadcomeEntities();
	}

    public IEnumerable<Subtema> List
    {
        get
        {
            return _temaContext.Subtemas;
        }

    }

    public void Add(Subtema entity)
    {
        _temaContext.Subtemas.Add(entity);
        _temaContext.SaveChanges();
    }

    public void Delete(Subtema entity)
    {
        _temaContext.Subtemas.Remove(entity);
        _temaContext.SaveChanges();
    }

    public void Update(Subtema entity)
    {
        _temaContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _temaContext.SaveChanges();

    }

    public Subtema FindById(int Id)
    {
        var result = (from r in _temaContext.Subtemas where r.IdSubtema == Id select r).FirstOrDefault();
        return result;
    }
}