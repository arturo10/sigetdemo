﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;

/// <summary>
/// Summary description for AlumnoRepository
/// </summary>
public class AlumnoRepository:IRepository<Alumno>
{

    public sigetdemo_sadcomeEntities _areaContext;
    public AlumnoRepository()
	{
        _areaContext = new sigetdemo_sadcomeEntities();
	}

    public IEnumerable<Alumno> List
    {
        get
        {
            return _areaContext.Alumnoes;
        }

    }

    public void Add(Alumno entity)
    {
        _areaContext.Alumnoes.Add(entity);
        _areaContext.SaveChanges();
    }

    public void Delete(Alumno entity)
    {
        _areaContext.Alumnoes.Remove(entity);
        _areaContext.SaveChanges();
    }

    public void Update(Alumno entity)
    {
        _areaContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _areaContext.SaveChanges();

    }

    public Alumno FindById(int Id)
    {
        var result = (from r in _areaContext.Alumnoes where r.IdAlumno == Id select r).FirstOrDefault();
        return result;
    }
}