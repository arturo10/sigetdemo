﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;

/// <summary>
/// Summary description for AdminRepository
/// </summary>
public class AdminRepository:IRepository<Admin>
{
	public sigetdemo_sadcomeEntities _context;
    public AdminRepository()
	{
        _context = new sigetdemo_sadcomeEntities();
	}

    public IEnumerable<Admin> List
    {
        get
        {
            return _context.Admins;
        }

    }

    public void Add(Admin entity)
    {
        _context.Admins.Add(entity);
        _context.SaveChanges();
    }

    public void Delete(Admin entity)
    {
        _context.Admins.Remove(entity);
        _context.SaveChanges();
    }

    public void Update(Admin entity)
    {
        _context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _context.SaveChanges();

    }

    public Admin FindById(int Id)
    {
        var result = (from r in _context.Admins where r.IdAdmin == Id select r).FirstOrDefault();
        return result;
    }
}