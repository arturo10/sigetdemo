﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;

/// <summary>
/// Summary description for OperadorRepository
/// </summary>
public class OperadorRepository: IRepository<Operador>
{
    public sigetdemo_sadcomeEntities _context;
    public OperadorRepository()
	{
        _context = new sigetdemo_sadcomeEntities();
	}

    public IEnumerable<Operador> List
    {
        get
        {
            return _context.Operadors;
        }

    }

    public void Add(Operador entity)
    {
        _context.Operadors.Add(entity);
        _context.SaveChanges();
    }

    public void Delete(Operador entity)
    {
        _context.Operadors.Remove(entity);
        _context.SaveChanges();
    }

    public void Update(Operador entity)
    {
        _context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _context.SaveChanges();

    }

    public Operador FindById(int Id)
    {
        var result = (from r in _context.Operadors where r.IdOperador == Id select r).FirstOrDefault();
        return result;
    }
}