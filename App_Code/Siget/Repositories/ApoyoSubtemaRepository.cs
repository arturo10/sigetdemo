﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;
/// <summary>
/// Summary description for ApoyoSubtemaRepository
/// </summary>
public class ApoyoSubtemaRepository : IRepository<ApoyoSubtema>
{
    public sigetdemo_sadcomeEntities _context;
	public ApoyoSubtemaRepository()
	{
        _context = new sigetdemo_sadcomeEntities();
	}

    public IEnumerable<ApoyoSubtema> List
    {
        get
        {
            return _context.ApoyoSubtemas;
        }

    }

    public void Add(ApoyoSubtema entity)
    {
        _context.ApoyoSubtemas.Add(entity);
        _context.SaveChanges();
    }

    public void Delete(ApoyoSubtema entity)
    {
        _context.ApoyoSubtemas.Remove(entity);
        _context.SaveChanges();
    }

    public void Update(ApoyoSubtema entity)
    {
        _context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _context.SaveChanges();

    }

    public ApoyoSubtema FindById(int Id)
    {
        var result = (from r in _context.ApoyoSubtemas where r.IdSubtema == Id select r).FirstOrDefault();
        return result;
    }
}