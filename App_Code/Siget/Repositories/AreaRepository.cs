﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;

/// <summary>
/// Summary description for AreaRepository
/// </summary>
public class AreaRepository:IRepository<Area>
{
    public sigetdemo_sadcomeEntities _areaContext;
    public AreaRepository()
	{
        _areaContext = new sigetdemo_sadcomeEntities();
	}

    public IEnumerable<Area> List
    {
        get
        {
            return _areaContext.Areas;
        }

    }

    public void Add(Area entity)
    {
        _areaContext.Areas.Add(entity);
        _areaContext.SaveChanges();
    }

    public void Delete(Area entity)
    {
        _areaContext.Areas.Remove(entity);
        _areaContext.SaveChanges();
    }

    public void Update(Area entity)
    {
        _areaContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _areaContext.SaveChanges();

    }

    public Area FindById(int Id)
    {
        var result = (from r in _areaContext.Areas where r.IdArea == Id select r).FirstOrDefault();
        return result;
    }
}