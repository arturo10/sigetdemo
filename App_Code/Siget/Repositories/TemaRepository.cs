﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;
/// <summary>
/// Summary description for TemaRepository
/// </summary>
public class TemaRepository : IRepository<Tema>
{
    public sigetdemo_sadcomeEntities _temaContext;
    public TemaRepository()
	{
        _temaContext = new sigetdemo_sadcomeEntities();
	}

    public IEnumerable<Tema> List
    {
        get
        {
            return _temaContext.Temas;
        }

    }

    public void Add(Tema entity)
    {
        _temaContext.Temas.Add(entity);
        _temaContext.SaveChanges();
    }

    public void Delete(Tema entity)
    {
        _temaContext.Temas.Remove(entity);
        _temaContext.SaveChanges();
    }

    public void Update(Tema entity)
    {
        _temaContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _temaContext.SaveChanges();

    }

    public Tema FindById(int Id)
    {
        var result = (from r in _temaContext.Temas where r.IdTema == Id select r).FirstOrDefault();
        return result;
    }
}