﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;

/// <summary>
/// Summary description for CoordinadorRepository
/// </summary>
public class CoordinadorRepository:IRepository<Coordinador>
{
    public sigetdemo_sadcomeEntities _areaContext;
    public CoordinadorRepository()
	{
        _areaContext = new sigetdemo_sadcomeEntities();
	}

    public IEnumerable<Coordinador> List
    {
        get
        {
            return _areaContext.Coordinadors;
        }

    }

    public void Add(Coordinador entity)
    {
        _areaContext.Coordinadors.Add(entity);
        _areaContext.SaveChanges();
    }

    public void Delete(Coordinador entity)
    {
        _areaContext.Coordinadors.Remove(entity);
        _areaContext.SaveChanges();
    }

    public void Update(Coordinador entity)
    {
        _areaContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _areaContext.SaveChanges();

    }

    public Coordinador FindById(int Id)
    {
        var result = (from r in _areaContext.Coordinadors where r.IdCoordinador == Id select r).FirstOrDefault();
        return result;
    }
}