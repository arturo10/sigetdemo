﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Siget.Entity;
/// <summary>
/// Summary description for PlanteamientoRepository
/// </summary>
public class PlanteamientoRepository:IRepository<Planteamiento>
{
    public sigetdemo_sadcomeEntities _planteamientoContext;

    public PlanteamientoRepository()
    {
        _planteamientoContext = new sigetdemo_sadcomeEntities();
    }

    public IEnumerable<Planteamiento> List
    {
        get
        {
            return _planteamientoContext.Planteamientoes;
        }

    }

    public void Add(Planteamiento entity)
    {
        _planteamientoContext.Planteamientoes.Add(entity);
        _planteamientoContext.SaveChanges();
    }

    public void Delete(Planteamiento entity)
    {
        _planteamientoContext.Planteamientoes.Remove(entity);
        _planteamientoContext.SaveChanges();
    }

    public void Update(Planteamiento entity)
    {
        _planteamientoContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _planteamientoContext.SaveChanges();

    }

    public Planteamiento FindById(int Id)
    {
        var result = (from r in _planteamientoContext.Planteamientoes where r.IdPlanteamiento == Id select r).FirstOrDefault();
        return result;
    }
}