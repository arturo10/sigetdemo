﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;

/// <summary>
/// Summary description for UsuarioRepository
/// </summary>
public class UsuarioRepository:IRepository<Usuario>
{

    public sigetdemo_sadcomeEntities _areaContext;
    public UsuarioRepository()
	{
        _areaContext = new sigetdemo_sadcomeEntities();
	}

    public IEnumerable<Usuario> List
    {
        get
        {
            return _areaContext.Usuarios;
        }

    }

    public void Add(Usuario entity)
    {
        _areaContext.Usuarios.Add(entity);
        _areaContext.SaveChanges();
    }

    public void Delete(Usuario entity)
    {
        _areaContext.Usuarios.Remove(entity);
        _areaContext.SaveChanges();
    }

    public void Update(Usuario entity)
    {
        _areaContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _areaContext.SaveChanges();

    }

    public Usuario FindById(int Id)
    {
        var result = (from r in _areaContext.Usuarios where r.IdUsuario == Id select r).FirstOrDefault();
        return result;
    }
}