﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;

/// <summary>
/// Summary description for DetalleEvaluacionRepository
/// </summary>
public class DetalleEvaluacionRepository:IRepository<DetalleEvaluacion>
{
    public sigetdemo_sadcomeEntities _context;

	public DetalleEvaluacionRepository()
	{
        _context = new sigetdemo_sadcomeEntities();
	}
    public IEnumerable<DetalleEvaluacion> List
    {
        get
        {
            return _context.DetalleEvaluacions;
        }

    }

    public void Add(DetalleEvaluacion entity)
    {
        _context.DetalleEvaluacions.Add(entity);
        _context.SaveChanges();
    }

    public void Delete(DetalleEvaluacion entity)
    {
        _context.DetalleEvaluacions.Remove(entity);
        _context.SaveChanges();
    }

    public void Update(DetalleEvaluacion entity)
    {
        _context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _context.SaveChanges();

    }

    public DetalleEvaluacion FindById(int Id)
    {
        var result = (from r in _context.DetalleEvaluacions where r.IdDetalleEvaluacion == Id select r).FirstOrDefault();
        return result;
    }

}