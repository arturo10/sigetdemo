﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;

/// <summary>
/// Summary description for CompetenciasRepository
/// </summary>
public class CompetenciasRepository:IRepository<Competencia>
{
    public sigetdemo_sadcomeEntities _competenciaContext;
    public CompetenciasRepository()
	{
        _competenciaContext = new sigetdemo_sadcomeEntities();
	}

    public IEnumerable<Competencia> List
    {
        get
        {
            return _competenciaContext.Competencias;
        }

    }

    public void Add(Competencia entity)
    {
        _competenciaContext.Competencias.Add(entity);
        _competenciaContext.SaveChanges();
    }

    public void Delete(Competencia entity)
    {
        _competenciaContext.Competencias.Remove(entity);
        _competenciaContext.SaveChanges();
    }

    public void Update(Competencia entity)
    {
        _competenciaContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _competenciaContext.SaveChanges();

    }

    public Competencia FindById(int Id)
    {
        var result = (from r in _competenciaContext.Competencias where r.IdCompetencia == Id select r).FirstOrDefault();
        return result;
    }
}