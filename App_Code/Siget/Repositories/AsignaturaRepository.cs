﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;

/// <summary>
/// Summary description for AsignaturaRepository
/// </summary>
public class AsignaturaRepository:IRepository<Asignatura>
{
    public sigetdemo_sadcomeEntities _asignaturaContext;
	public AsignaturaRepository()
	{
        _asignaturaContext = new sigetdemo_sadcomeEntities();
	}

    public IEnumerable<Asignatura> List
    {
        get
        {
            return _asignaturaContext.Asignaturas;
        }

    }

    public void Add(Asignatura entity)
    {
        _asignaturaContext.Asignaturas.Add(entity);
        _asignaturaContext.SaveChanges();
    }

    public void Delete(Asignatura entity)
    {
        _asignaturaContext.Asignaturas.Remove(entity);
        _asignaturaContext.SaveChanges();
    }

    public void Update(Asignatura entity)
    {
        _asignaturaContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _asignaturaContext.SaveChanges();

    }

    public Asignatura FindById(int Id)
    {
        var result = (from r in _asignaturaContext.Asignaturas where r.IdAsignatura == Id select r).FirstOrDefault();
        return result;
    }
}