﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;

/// <summary>
/// Summary description for CalificacionRepository
/// </summary>
public class CalificacionRepository : IRepository<Calificacion>
{
    public sigetdemo_sadcomeEntities _calificacionContext;
	public CalificacionRepository()
	{
        _calificacionContext = new sigetdemo_sadcomeEntities();
	}

    public IEnumerable<Calificacion> List
    {
        get
        {
            return _calificacionContext.Calificacions;
        }

    }

    public void Add(Calificacion entity)
    {
        _calificacionContext.Calificacions.Add(entity);
        _calificacionContext.SaveChanges();
    }

    public void Delete(Calificacion entity)
    {
        _calificacionContext.Calificacions.Remove(entity);
        _calificacionContext.SaveChanges();
    }

    public void Update(Calificacion entity)
    {
        _calificacionContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _calificacionContext.SaveChanges();

    }

    public Calificacion FindById(int Id)
    {
        var result = (from r in _calificacionContext.Calificacions where r.IdCalificacion == Id select r).FirstOrDefault();
        return result;
    }
}