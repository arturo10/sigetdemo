﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;

/// <summary>
/// Summary description for ProfesorRepository
/// </summary>
public class ProfesorRepository:IRepository<Profesor>
{

    public sigetdemo_sadcomeEntities _areaContext;
    public ProfesorRepository()
	{
        _areaContext = new sigetdemo_sadcomeEntities();
	}

    public IEnumerable<Profesor> List
    {
        get
        {
            return _areaContext.Profesors;
        }

    }

    public void Add(Profesor entity)
    {
        _areaContext.Profesors.Add(entity);
        _areaContext.SaveChanges();
    }

    public void Delete(Profesor entity)
    {
        _areaContext.Profesors.Remove(entity);
        _areaContext.SaveChanges();
    }

    public void Update(Profesor entity)
    {
        _areaContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _areaContext.SaveChanges();

    }

    public Profesor FindById(int Id)
    {
        var result = (from r in _areaContext.Profesors where r.IdProfesor == Id select r).FirstOrDefault();
        return result;
    }
	
}