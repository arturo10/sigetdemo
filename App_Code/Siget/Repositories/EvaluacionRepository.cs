﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Entity;
/// <summary>
/// Summary description for EvaluacionRepository
/// </summary>
public class EvaluacionRepository:IRepository<Evaluacion>
{
    public sigetdemo_sadcomeEntities _evaluacionContext;
	public EvaluacionRepository()
	{
        _evaluacionContext = new sigetdemo_sadcomeEntities();
	}
   
    public IEnumerable<Evaluacion> List
    {
        get
        {
            return _evaluacionContext.Evaluacions;
        }

    }

    public void Add(Evaluacion entity)
    {
        _evaluacionContext.Evaluacions.Add(entity);
        _evaluacionContext.SaveChanges();
    }

    public void Delete(Evaluacion entity)
    {
        _evaluacionContext.Evaluacions.Remove(entity);
        _evaluacionContext.SaveChanges();
    }

    public void Update(Evaluacion entity)
    {
        _evaluacionContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        _evaluacionContext.SaveChanges();

    }

    public Evaluacion FindById(int Id)
    {
        var result = (from r in _evaluacionContext.Evaluacions where r.IdEvaluacion == Id select r).FirstOrDefault();
        return result;
    }
}