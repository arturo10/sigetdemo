﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Siget.Config
{
  /// <summary>
  /// Almacena la configuración de la página principal (inicio de sesión) en memoria para no hacer consultas 
  /// a la BD durante cada cargado de la página.
  /// </summary>
  public class PaginaInicio
  {

    /// <summary>
    /// Indica si la configuración debe ser cargada desde la base de datos.
    /// El estado inicial debe ser falso para hacer la primera carga.
    /// </summary>
    public static Boolean Vigente = false;

    #region aviso publico

    /// <summary>
    /// Mensaje de gran tamaño visible al público.
    /// </summary>
    public static string AvisoPublico
    {
      get 
      {
        if (!Vigente) carga();
        return _avisoPublico;
      }
      set { _avisoPublico = value; }
    }
    private static string _avisoPublico;

    public static DateTime AvisoPublico_FechaInicio
    {
      get
      {
        if (!Vigente) carga();
        return _avisoPublico_FechaInicio;
      }
      set { _avisoPublico_FechaInicio = value; }
    }
    private static DateTime _avisoPublico_FechaInicio;

    public static DateTime AvisoPublico_FechaFin
    {
      get
      {
        if (!Vigente) carga();
        return _avisoPublico_FechaFin;
      }
      set { _avisoPublico_FechaFin = value; }
    }
    private static DateTime _avisoPublico_FechaFin;

    #endregion

    #region logo del cliente

    /// <summary>
    /// Archivo o enlace objetivo al hacer click en el logo del cliente.
    /// </summary>
    public static string LogoCliente_Objetivo
    {
      get 
      {
        if (!Vigente) carga();
        return _logoCliente_Objetivo;
      }
      set { _logoCliente_Objetivo = value; }
    }
    private static string _logoCliente_Objetivo;

    public static int LogoCliente_Tipo;

    #endregion

    #region boton 1

    /// <summary>
    /// Texto visible del botón de extrema izquierda.
    /// </summary>
    public static string Boton1_Texto = "";
    /// <summary>
    /// Archivo o enlace objetivo al hacer click en el botón de extrema izquierda.
    /// </summary>
    public static string Boton1_Objetivo
    {
      get 
      {
        if (!Vigente) carga();
        return _boton1_Objetivo;
      }
      set { _boton1_Objetivo = value; }
    }
    private static string _boton1_Objetivo;
    /// <summary>
    /// -1 indica desactivado, 0 indica archivo, 1 indica link
    /// </summary>
    public static int Boton1_Tipo;

    #endregion

    #region boton 2
    
    /// <summary>
    /// Texto visible del botón izquierdo central.
    /// </summary>
    public static string Boton2_Texto = "";
    /// <summary>
    /// Archivo o enlace objetivo al hacer click en el botón izquierdo central.
    /// </summary>
    public static string Boton2_Objetivo
    {
      get 
      {
        if (!Vigente) carga();
        return _boton2_Objetivo;
      }
      set { _boton2_Objetivo = value; }
    }
    private static string _boton2_Objetivo;
    /// <summary>
    /// -1 indica desactivado, 0 indica archivo, 1 indica link
    /// </summary>
    public static int Boton2_Tipo;

    #endregion

    #region boton 3

    /*
     * El botón derecho central presenta la guía de usuario y siempre es visible.
     */
    /// <summary>
    /// Archivo o enlace objetivo al hacer click en el botón derecho central.
    /// </summary>
    public static string Boton3_Objetivo
    {
      get 
      {
        if (!Vigente) carga();
        return _boton3_Objetivo;
      }
      set { _boton3_Objetivo = value; }
    }
    private static string _boton3_Objetivo;
    /// <summary>
    /// -1 indica desactivado, 0 indica archivo, 1 indica link
    /// </summary>
    public static int Boton3_Tipo;

    #endregion

    #region boton 4

    /*
     * El botón extremo derecho presenta el soporte técnico y siempre es visible.
     */
    /// <summary>
    /// Archivo o enlace objetivo al hacer click en el botón extremo derecho.
    /// </summary>
    public static string Boton4_Objetivo
    {
      get 
      {
        if (!Vigente) carga();
        return _boton4_Objetivo;
      }
      set { _boton4_Objetivo = value; }
    }
    private static string _boton4_Objetivo;
    /// <summary>
    /// -1 indica desactivado, 0 indica archivo, 1 indica link
    /// </summary>
    public static int Boton4_Tipo;

    #endregion
    
    /// <summary>
    /// Inicializa los atributos en un estado vacío.
    /// </summary>
    static PaginaInicio ()
    {
      Init();
    }

    public static void Init()
    {
      AvisoPublico = "";

      LogoCliente_Objetivo = "";
      LogoCliente_Tipo = -1;

      Boton1_Objetivo = "";
      Boton1_Texto = "";
      Boton1_Tipo = -1;

      Boton2_Objetivo = "";
      Boton2_Texto = "";
      Boton2_Tipo = -1;

      Boton3_Objetivo = "";
      Boton3_Tipo = -1;

      Boton4_Objetivo = "";
      Boton4_Tipo = -1;
    }

    /// <summary>
    /// Loads the paths found in the database to the shared link variables
    /// </summary>
    public static void carga()
    {
      Init();

      try {
        using (SqlConnection conn  = new SqlConnection(ConfigurationManager
                                                      .ConnectionStrings["sadcomeConnectionString"]
                                                      .ConnectionString)
                                                      )
        {
          conn.Open();
          using (SqlCommand cmd = new SqlCommand("SELECT * FROM PaginaInicio", conn))
          {
            using (SqlDataReader result = cmd.ExecuteReader())
            {
              while (result.Read())
              {
                if (result["Control"].ToString() == "logoCliente")
                {
                  LogoCliente_Objetivo = loadRoute(Int16.Parse(result["Tipo"].ToString()), result["Objetivo"].ToString());
                  LogoCliente_Tipo = Int16.Parse(result["Tipo"].ToString());
                }

                else if (result["Control"].ToString() == "avisoPublico")
                {
                  AvisoPublico = result["Texto"].ToString();
                  AvisoPublico_FechaInicio = DateTime.Parse(result["FechaInicia"].ToString());
                  AvisoPublico_FechaFin = DateTime.Parse(result["FechaTermina"].ToString());
                }
                
                else if (result["Control"].ToString() == "boton1")
                {
                  Boton1_Objetivo = loadRoute(Int16.Parse(result["Tipo"].ToString()), result["Objetivo"].ToString());
                  Boton1_Texto = result["Texto"].ToString();
                  Boton1_Tipo = Int16.Parse(result["Tipo"].ToString());
                }
                
                else if (result["Control"].ToString() == "boton2")
                {
                  Boton2_Objetivo = loadRoute(Int16.Parse(result["Tipo"].ToString()), result["Objetivo"].ToString());
                  Boton2_Texto = result["Texto"].ToString();
                  Boton2_Tipo = Int16.Parse(result["Tipo"].ToString());
                }
                
                else if (result["Control"].ToString() == "boton3")
                {
                  Boton3_Objetivo = loadRoute(Int16.Parse(result["Tipo"].ToString()), result["Objetivo"].ToString());
                  Boton3_Tipo = Int16.Parse(result["Tipo"].ToString());
                }
                
                else if (result["Control"].ToString() == "boton4")
                {
                  Boton4_Objetivo = loadRoute(Int16.Parse(result["Tipo"].ToString()), result["Objetivo"].ToString());
                  Boton4_Tipo = Int16.Parse(result["Tipo"].ToString());
                }
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
      }

      Vigente = true;
    }

    /// <summary>
    /// Creates the virtual link path to a resource
    /// </summary>
    /// <param name="endLink">a string target variable in which the result will be stored</param>
    /// <param name="type">the type of the route to load, 0 = file, 1 = link</param>
    /// <param name="path">the name or adress of the actual link to be generated</param>
    /// <remarks></remarks>
    private static string loadRoute(short type, string path)
    {
      if (type == 0)
        // es un archivo, crea un camino relativo de descarga
        return Siget.Config.Global.urlEnlaces + path;
      else if (type == 1)
        // si es un enlace
        if (path.Contains("//"))
          // y el enlace contiene un protocolo
          return path;
        else
          // y el enlace no contiene un protocolo, añade un protocolo genérico
          return "//" + path;
      else
        // en caso de desactivado, reporta un objetivo vacío
        return "";

    }

  }
}