﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace Siget.Config
{

  /// <summary>
  /// ESTA CLASE DEBE DESAPARECERSE.
  /// 
  /// LA NUEVA VERSIÓN INCORPORADA AL SISTEMA DE LENGUAJES ESTÁ EN ~/App_Code/Integrant/Sifa/Lang/Config.Etiqueta.vb
  /// ESTA VERSIÓN PERMANECE SOLAMENTE POR QUE NO TODOS LOS PERFILES ESTÁN ACTUALIZADOS AL REDISEÑO GUI  5.1.0
  /// </summary>
  /// <remarks></remarks>
  public class Etiqueta
  {

    // inserté esta función copia de lang_config para en cuanto inicia la aplicación inicializar las etiquetas antiguas
    // (usadas en todos los perfiles con ui anterior a 5.1.0), y poder mover la "fuente" de las etiquetas a la carpeta Resources,
    // de modo que esa sea la única carpeta con archivos no sustituibles durante actualizaciones.
    // 
    // ESTA FUNCION SE DEBE EJECUTAR EN GLOBAL.ASAX HASTA QUE SE DESAPAREZCA ESTA CLASE
    /// <summary>
    /// Obtiene las etiquetas del lenguaje definido y las escribe en la configuración de etiquetas del filesystem.
    /// </summary>
    /// <remarks>
    /// Las lecturas de este archivo xml no deben parsearse por etiquetas, puesto que son las etiquetas mismas.
    /// </remarks>
    public static void importa_Etiquetas_OLD()
    {
      using (XmlTextReader langFile = new XmlTextReader(Siget.Config.Global.rutaPersonalizacion + "\\Etiquetas\\es-mx.xml"))
      {

        langFile.ReadToFollowing("SISTEMA_CORTO");
        SISTEMA_CORTO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("INSTITUCION");
        INSTITUCION = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("INSTITUCIONES");
        INSTITUCIONES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_INSTITUCION");
        ARTDET_INSTITUCION = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_INSTITUCIONES");
        ARTDET_INSTITUCIONES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_INSTITUCION");
        ARTIND_INSTITUCION = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_INSTITUCIONES");
        ARTIND_INSTITUCIONES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("LETRA_INSTITUCION");
        LETRA_INSTITUCION = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("CICLO");
        CICLO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("CICLOS");
        CICLOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_CICLO");
        ARTDET_CICLO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_CICLOS");
        ARTDET_CICLOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_CICLO");
        ARTIND_CICLO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_CICLOS");
        ARTIND_CICLOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("LETRA_CICLO");
        LETRA_CICLO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("NIVEL");
        NIVEL = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("NIVELES");
        NIVELES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_NIVEL");
        ARTDET_NIVEL = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_NIVELES");
        ARTDET_NIVELES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_NIVEL");
        ARTIND_NIVEL = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_NIVELES");
        ARTIND_NIVELES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("LETRA_NIVEL");
        LETRA_NIVEL = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("GRADO");
        GRADO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("GRADOS");
        GRADOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_GRADO");
        ARTDET_GRADO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_GRADOS");
        ARTDET_GRADOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_GRADO");
        ARTIND_GRADO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_GRADOS");
        ARTIND_GRADOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("LETRA_GRADO");
        LETRA_GRADO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ASIGNATURA");
        ASIGNATURA = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ASIGNATURAS");
        ASIGNATURAS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_ASIGNATURA");
        ARTDET_ASIGNATURA = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_ASIGNATURAS");
        ARTDET_ASIGNATURAS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_ASIGNATURA");
        ARTIND_ASIGNATURA = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_ASIGNATURAS");
        ARTIND_ASIGNATURAS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("LETRA_ASIGNATURA");
        LETRA_ASIGNATURA = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("PLANTEL");
        PLANTEL = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("PLANTELES");
        PLANTELES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_PLANTEL");
        ARTDET_PLANTEL = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_PLANTELES");
        ARTDET_PLANTELES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_PLANTEL");
        ARTIND_PLANTEL = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_PLANTELES");
        ARTIND_PLANTELES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("LETRA_PLANTEL");
        LETRA_PLANTEL = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("GRUPO");
        GRUPO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("GRUPOS");
        GRUPOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_GRUPO");
        ARTDET_GRUPO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_GRUPOS");
        ARTDET_GRUPOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_GRUPO");
        ARTIND_GRUPO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_GRUPOS");
        ARTIND_GRUPOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("LETRA_GRUPO");
        LETRA_GRUPO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ALUMNO");
        ALUMNO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ALUMNOS");
        ALUMNOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_ALUMNO");
        ARTDET_ALUMNO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_ALUMNOS");
        ARTDET_ALUMNOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_ALUMNO");
        ARTIND_ALUMNO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_ALUMNOS");
        ARTIND_ALUMNOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("LETRA_ALUMNO");
        LETRA_ALUMNO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("PROFESOR");
        PROFESOR = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("PROFESORES");
        PROFESORES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_PROFESOR");
        ARTDET_PROFESOR = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_PROFESORES");
        ARTDET_PROFESORES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_PROFESOR");
        ARTIND_PROFESOR = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_PROFESORES");
        ARTIND_PROFESORES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("LETRA_PROFESOR");
        LETRA_PROFESOR = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("COORDINADOR");
        COORDINADOR = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("COORDINADORES");
        COORDINADORES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_COORDINADOR");
        ARTDET_COORDINADOR = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_COORDINADORES");
        ARTDET_COORDINADORES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_COORDINADOR");
        ARTIND_COORDINADOR = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_COORDINADORES");
        ARTIND_COORDINADORES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("LETRA_COORDINADOR");
        LETRA_COORDINADOR = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("EQUIPO");
        EQUIPO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("EQUIPOS");
        EQUIPOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_EQUIPO");
        ARTDET_EQUIPO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_EQUIPOS");
        ARTDET_EQUIPOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_EQUIPO");
        ARTIND_EQUIPO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_EQUIPOS");
        ARTIND_EQUIPOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("LETRA_EQUIPO");
        LETRA_EQUIPO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("SUBEQUIPO");
        SUBEQUIPO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("SUBEQUIPOS");
        SUBEQUIPOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_SUBEQUIPO");
        ARTDET_SUBEQUIPO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_SUBEQUIPOS");
        ARTDET_SUBEQUIPOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_SUBEQUIPO");
        ARTIND_SUBEQUIPO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_SUBEQUIPOS");
        ARTIND_SUBEQUIPOS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("LETRA_SUBEQUIPO");
        LETRA_SUBEQUIPO = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("MATRICULA");
        MATRICULA = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("MATRICULAS");
        MATRICULAS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_MATRICULA");
        ARTDET_MATRICULA = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_MATRICULAS");
        ARTDET_MATRICULAS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_MATRICULA");
        ARTIND_MATRICULA = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_MATRICULAS");
        ARTIND_MATRICULAS = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("LETRA_MATRICULA");
        LETRA_MATRICULA = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("CALIFICACION");
        CALIFICACION = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("CALIFICACIONES");
        CALIFICACIONES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_CALIFICACION");
        ARTDET_CALIFICACION = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTDET_CALIFICACIONES");
        ARTDET_CALIFICACIONES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_CALIFICACION");
        ARTIND_CALIFICACION = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("ARTIND_CALIFICACIONES");
        ARTIND_CALIFICACIONES = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.ReadToFollowing("LETRA_CALIFICACION");
        LETRA_CALIFICACION = langFile.ReadString().Replace(Environment.NewLine, "").Trim();

        langFile.Close();
      }
    }

    // el nombre del esquema bajo el que trabaja el filesystem

    public static string SISTEMA_CORTO = string.Empty;
    // Cada sustantivo a etiquetar tiene:
    //   versión singular
    //   versión plural
    //   artículo determinado singular (el, la)
    //   artículo determinado plural (los, las)
    //   artículo indeterminado singular (un, una)
    //   artículo indeterminado plural (unos, unas)
    //   letra de terminación de verbos (seleccionadas, elegidas, capturados, ...)

    public static string INSTITUCION = string.Empty;
    public static string INSTITUCIONES = string.Empty;
    public static string ARTDET_INSTITUCION = string.Empty;
    public static string ARTDET_INSTITUCIONES = string.Empty;
    public static string ARTIND_INSTITUCION = string.Empty;
    public static string ARTIND_INSTITUCIONES = string.Empty;

    public static string LETRA_INSTITUCION = string.Empty;
    public static string CICLO = string.Empty;
    public static string CICLOS = string.Empty;
    public static string ARTDET_CICLO = string.Empty;
    public static string ARTDET_CICLOS = string.Empty;
    public static string ARTIND_CICLO = string.Empty;
    public static string ARTIND_CICLOS = string.Empty;

    public static string LETRA_CICLO = string.Empty;
    public static string NIVEL = string.Empty;
    public static string NIVELES = string.Empty;
    public static string ARTDET_NIVEL = string.Empty;
    public static string ARTDET_NIVELES = string.Empty;
    public static string ARTIND_NIVEL = string.Empty;
    public static string ARTIND_NIVELES = string.Empty;

    public static string LETRA_NIVEL = string.Empty;
    public static string GRADO = string.Empty;
    public static string GRADOS = string.Empty;
    public static string ARTDET_GRADO = string.Empty;
    public static string ARTDET_GRADOS = string.Empty;
    public static string ARTIND_GRADO = string.Empty;
    public static string ARTIND_GRADOS = string.Empty;

    public static string LETRA_GRADO = string.Empty;
    public static string ASIGNATURA = string.Empty;
    public static string ASIGNATURAS = string.Empty;
    public static string ARTDET_ASIGNATURA = string.Empty;
    public static string ARTDET_ASIGNATURAS = string.Empty;
    public static string ARTIND_ASIGNATURA = string.Empty;
    public static string ARTIND_ASIGNATURAS = string.Empty;

    public static string LETRA_ASIGNATURA = string.Empty;
    public static string PLANTEL = string.Empty;
    public static string PLANTELES = string.Empty;
    public static string ARTDET_PLANTEL = string.Empty;
    public static string ARTDET_PLANTELES = string.Empty;
    public static string ARTIND_PLANTEL = string.Empty;
    public static string ARTIND_PLANTELES = string.Empty;

    public static string LETRA_PLANTEL = string.Empty;
    public static string GRUPO = string.Empty;
    public static string GRUPOS = string.Empty;
    public static string ARTDET_GRUPO = string.Empty;
    public static string ARTDET_GRUPOS = string.Empty;
    public static string ARTIND_GRUPO = string.Empty;
    public static string ARTIND_GRUPOS = string.Empty;

    public static string LETRA_GRUPO = string.Empty;
    public static string ALUMNO = string.Empty;
    public static string ALUMNOS = string.Empty;
    public static string ARTDET_ALUMNO = string.Empty;
    public static string ARTDET_ALUMNOS = string.Empty;
    public static string ARTIND_ALUMNO = string.Empty;
    public static string ARTIND_ALUMNOS = string.Empty;

    public static string LETRA_ALUMNO = string.Empty;
    public static string PROFESOR = string.Empty;
    public static string PROFESORES = string.Empty;
    public static string ARTDET_PROFESOR = string.Empty;
    public static string ARTDET_PROFESORES = string.Empty;
    public static string ARTIND_PROFESOR = string.Empty;
    public static string ARTIND_PROFESORES = string.Empty;

    public static string LETRA_PROFESOR = string.Empty;
    public static string COORDINADOR = string.Empty;
    public static string COORDINADORES = string.Empty;
    public static string ARTDET_COORDINADOR = string.Empty;
    public static string ARTDET_COORDINADORES = string.Empty;
    public static string ARTIND_COORDINADOR = string.Empty;
    public static string ARTIND_COORDINADORES = string.Empty;

    public static string LETRA_COORDINADOR = string.Empty;
    public static string EQUIPO = string.Empty;
    public static string EQUIPOS = string.Empty;
    public static string ARTDET_EQUIPO = string.Empty;
    public static string ARTDET_EQUIPOS = string.Empty;
    public static string ARTIND_EQUIPO = string.Empty;
    public static string ARTIND_EQUIPOS = string.Empty;

    public static string LETRA_EQUIPO = string.Empty;
    public static string SUBEQUIPO = string.Empty;
    public static string SUBEQUIPOS = string.Empty;
    public static string ARTDET_SUBEQUIPO = string.Empty;
    public static string ARTDET_SUBEQUIPOS = string.Empty;
    public static string ARTIND_SUBEQUIPO = string.Empty;
    public static string ARTIND_SUBEQUIPOS = string.Empty;

    public static string LETRA_SUBEQUIPO = string.Empty;
    public static string MATRICULA = string.Empty;
    public static string MATRICULAS = string.Empty;
    public static string ARTDET_MATRICULA = string.Empty;
    public static string ARTDET_MATRICULAS = string.Empty;
    public static string ARTIND_MATRICULA = string.Empty;
    public static string ARTIND_MATRICULAS = string.Empty;

    public static string LETRA_MATRICULA = string.Empty;
    // actualmente ésta etiqueta sólo se necesita en el menú de actividades pendientes del alumno
    public static string CALIFICACION = string.Empty;
    public static string CALIFICACIONES = string.Empty;
    public static string ARTDET_CALIFICACION = string.Empty;
    public static string ARTDET_CALIFICACIONES = string.Empty;
    public static string ARTIND_CALIFICACION = string.Empty;
    public static string ARTIND_CALIFICACIONES = string.Empty;

    public static string LETRA_CALIFICACION = string.Empty;
  }

}