﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Siget.Config
{
  /// <summary>
  /// Summary description for Global_Import
  /// </summary>
  public class Global_Import
  {
    
    // 09/09/2014
    /// <summary>
    /// Obtiene la configuración almacenada en la base de datos y la aplica en las banderas globales del filesystem
    /// </summary>
    public static void getConfig()
    {
      try
      {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager
          .ConnectionStrings["sadcomeConnectionString"]
          .ConnectionString))
          {
          conn.Open();
          using (SqlCommand cmd = new SqlCommand("SELECT * FROM Configuracion", conn))
          {
            using (SqlDataReader result = cmd.ExecuteReader())
            {
              while (result.Read())
              {
                switch (Int64.Parse(result["Llave"].ToString().Trim()))
                {
                  case Global.KEY_DEPLOYED:
                        
                    Global.DEPLOYED = Boolean.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["DEPLOYED"] = Boolean.Parse(result["Valor"].ToString());
                    break;
                  case Global.KEY_MAIL_ACTIVO:
                    Global.MAIL_ACTIVO = Boolean.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["MAIL_ACTIVO"] = Boolean.Parse(result["Valor"].ToString());
                    break;
                  case Global.KEY_GUARDAR_RESPUESTA_ABIERTA:
                    Global.GUARDAR_RESPUESTA_ABIERTA = Boolean.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["GUARDAR_RESPUESTA_ABIERTA"] = Boolean.Parse(result["Valor"].ToString());
                    break;
                  case Global.KEY_FIRMA_PIE_INTEGRANT:
                    Global.FIRMA_PIE_INTEGRANT = Boolean.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["FIRMA_PIE_INTEGRANT"] = Boolean.Parse(result["Valor"].ToString());
                    break;
                  case Global.Key_Idioma_General:
                    Global.Idioma_General = result["Valor"].ToString().Trim();
                    HttpContext.Current.Application["Idioma_General"] = result["Valor"].ToString().Trim();
                    break;
                  case Global.KEY_ALUMNO_CONTESTAR_RENUNCIAR:
                    Global.ALUMNO_CONTESTAR_RENUNCIAR = Boolean.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["ALUMNO_CONTESTAR_RENUNCIAR"] = Boolean.Parse(result["Valor"].ToString());
                    break;
                  case Global.KEY_ALUMNO_PENDIENTES_COLUMNA_TIPO:
                    Global.ALUMNO_PENDIENTES_COLUMNA_TIPO = Boolean.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["ALUMNO_PENDIENTES_COLUMNA_TIPO"] = Boolean.Parse(result["Valor"].ToString());
                    break;
                  case Global.KEY_ALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION:
                    Global.ALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION = Boolean.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["ALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION"] = Boolean.Parse(result["Valor"].ToString());
                    break;
                  case Global.KEY_ALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION:
                    Global.ALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION = Boolean.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["ALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION"] = Boolean.Parse(result["Valor"].ToString());
                    break;
                  case Global.KEY_ALUMNO_PENDIENTES_COLUMNA_ENTREGA:
                    Global.ALUMNO_PENDIENTES_COLUMNA_ENTREGA= Boolean.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["ALUMNO_PENDIENTES_COLUMNA_ENTREGA"] = Boolean.Parse(result["Valor"].ToString());
                    break;
                  case Global.KEY_PROFESOR_CALIFICA_NUMERICO:
                    Global.PROFESOR_CALIFICA_NUMERICO = Int32.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["PROFESOR_CALIFICA_NUMERICO"] = Int32.Parse(result["Valor"].ToString());
                    break;
                  case Global.KEY_PROFESOR_CALIFICA_DOCUMENTO:
                    Global.PROFESOR_CALIFICA_DOCUMENTO = Int32.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["PROFESOR_CALIFICA_DOCUMENTO"] = Int32.Parse(result["Valor"].ToString());
                    break;
                  case Global.Key_Modelo_Sistema:
                    Global.Modelo_Sistema = Int32.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["Modelo_Sistema"] = Int32.Parse(result["Valor"].ToString());
                    break;
                  case Global.Key_Login_Screen:
                    Global.Login_Screen = Int32.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["Login_Screen"] = Int32.Parse(result["Valor"].ToString());
                    break;
                  case Global.KEY_DOCUMENTO_PROTEGIDO:
                    Global.DOCUMENTO_PROTEGIDO = Boolean.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["DOCUMENTO_PROTEGIDO"] = Boolean.Parse(result["Valor"].ToString());
                    break;
                  case Global.KEY_INSCRIBIR_CURSOS:
                    Global.V_INSCRIBIR_CURSOS = Boolean.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["V_INSCRIBIR_CURSOS"] = Boolean.Parse(result["Valor"].ToString());
                    break;
                  case Global.KEY_CONTRA_OBLIGATORIA:
                    Global.CONTRA_OBLIGATORIA = Boolean.Parse(result["Valor"].ToString());
                    HttpContext.Current.Application["CONTRA_OBLIGATORIA"] = Boolean.Parse(result["Valor"].ToString());
                    break;
                }
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
      }
    }
  }
}
