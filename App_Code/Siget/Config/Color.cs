﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Siget.Config
{
  /// <summary>
  /// Contiene los colores usados en lugares diferentes del sistema, 
  /// como la barra de título de cada página en el diseño 5.0
  /// o el indicador de sección actual en el menú superior del diseño 5.1
  /// </summary>
  public class Color
  {

    /// <summary>
    /// Color de los bordes de los textbox de login en la página principal
    /// </summary>
    public static string Login_TextBox_Border = "#008000";

    // _________________________________________________________________________________________
    // colores del diseño 5.0

    /// <summary>
    /// Color del degradado de la barra de título
    /// en la master page de páginas de contenido
    /// (parte superior)
    /// </summary>
    public static string TituloPrincipal_Top = "#008000";
    /// <summary>
    /// Color del degradado de la barra de título
    /// en la master page de páginas de contenido
    /// (parte inferior)
    /// </summary>
    public static string TituloPrincipal_Bottom = "#006400";

    /// <summary>
    /// Color del botón de asignatura al estar presionado, de la página Pendientes.aspx
    /// </summary>
    public static string MenuPendientes_Asignatura_Displayed = "#338033";
    /// <summary>
    /// Color del texto del botón de asignatura al estar presionado, de la página Pendientes.aspx
    /// </summary>
    public static string MenuPendientes_Asignatura_Displayed_Font = "#fff";

    /// <summary>
    /// Color del botón de calificación al estar presionado, de la página Pendientes.aspx
    /// </summary>
    public static string MenuPendientes_Calificacion_Displayed = "#666";
    /// <summary>
    /// Color del texto del botón de calificación al estar presionado, de la página Pendientes.aspx
    /// </summary>
    public static string MenuPendientes_Calificacion_Displayed_Font = "#fff";

    /// <summary>
    /// Color del encabezado de las tablas de actividades pendientes de la página Pendientes.aspx
    /// </summary>
    public static string MenuPendientes_Subtitle = "#ddeecc";

    /// <summary>
    /// Color de fondo de los encabezados de gridviews
    /// </summary>
    public static string Table_HeaderRow_Background = "#dec";
    /// <summary>
    /// Color de texto de los encabezados de gridviews
    /// </summary>
    public static string Table_HeaderRow_Color = "#000";
    /// <summary>
    /// Color de fondo de filas seleccionABLES cuando se coloca el mouse sobre ellas
    /// </summary>
    public static string Table_SelectableRowHover_Background = "#ddd"; //"#ddd"
    /// <summary>
    /// Color de fondo de filas seleccionadas de gridviews
    /// </summary>
    public static string Table_SelectedRow_Background = "#cfcfbf"; //"#dfc"
    /// <summary>
    /// Color de fondo de filas seleccionADAS cuando se coloca el mouse sobre ellas
    /// </summary>
    public static string Table_SelectedRowHover_Background = "#cfcfbf"; //"#d5f5c5"

    // _________________________________________________________________________________________
    // colores del diseño 5.1

    /// <summary>
    /// Color de fondo de opción seleccionada en el menú superior
    /// </summary>
    public static string MenuBar_SelectedOption_BorderColor = "#666";

  }
}