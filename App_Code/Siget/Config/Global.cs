﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Siget.Config
{
  /// <summary>
  /// Contiene las variables principales de configuración de la aplicación.
  /// </summary>
  public class Global
  {
    /// <summary>
    /// Lista de lenguajes soportados por la plataforma
    /// </summary>
    public static string[] Lenguajes = {"es-mx", "en-us"};

    // *****************************************************************************************
    //                                         Perfiles
    // *****************************************************************************************

    public const string Perfil_Alumno = "alumno";

    public const string Perfil_Profesor = "profesor";

    public const string Perfil_Coordinador = "coordinador";

    public const string Perfil_Operador = "operador";

    public const string Perfil_Capturista = "capturista";

    public const string Perfil_Administrador = "superadministrador";

    public const string Perfil_SysAdmin = "sysadmin";

    // *****************************************************************************************
    //                                         Banderas
    // *****************************************************************************************

    // Cada bandera tiene una llave asociada, bajo la que se identifica en la tabla [Configuracion] en la base de datos.
    // ULTIMA LLAVE CREADA: 21

    // ***************************************
    //                                 General
    // ***************************************

    /// <summary>
    /// El idioma predeterminado de las páginas públicas (como Home).
    /// El idioma configurado debe inicializarse desde Global.asax con la clase Lang.Lang.
    /// </summary>
    public static string Idioma_General = "es-mx";
    /// <summary>
    /// Llave asociada a IDIOMA_ALUMNO para la tabla de configuraciones
    /// </summary>
    public const int Key_Idioma_General = 1;

    /// <summary>
    /// El idioma predeterminado del perfil Alumno.
    /// El idioma configurado debe inicializarse desde Global.asax con la clase Lang.Lang.
    /// </summary>
    public static string Idioma_Alumno = "es-mx";
    /// <summary>
    /// Llave asociada a IDIOMA_ALUMNO para la tabla de configuraciones
    /// </summary>
    public const int Key_Idioma_Alumno = 12;

    /// <summary>
    /// El idioma predeterminado del perfil Profesor.
    /// El idioma configurado debe inicializarse desde Global.asax con la clase Lang.Lang.
    /// </summary>
    public static string Idioma_Profesor = "es-mx";
    /// <summary>
    /// Llave asociada a IDIOMA_ALUMNO para la tabla de configuraciones
    /// </summary>
    public const int Key_Idioma_Profesor = 13;

    /// <summary>
    /// El idioma predeterminado del perfil Coordinador.
    /// El idioma configurado debe inicializarse desde Global.asax con la clase Lang.Lang.
    /// </summary>
    public static string Idioma_Coordinador = "es-mx";
    /// <summary>
    /// Llave asociada a IDIOMA_ALUMNO para la tabla de configuraciones
    /// </summary>
    public const int Key_Idioma_Coordinador = 14;

    /// <summary>
    /// El idioma predeterminado del perfil Administrador, Operador, y Capturista.
    /// El idioma configurado debe inicializarse desde Global.asax con la clase Lang.Lang.
    /// </summary>
    public static string Idioma_Administrador = "es-mx";
    /// <summary>
    /// Llave asociada a IDIOMA_ALUMNO para la tabla de configuraciones
    /// </summary>
    public const int Key_Idioma_Administrador = 15;

    // 16/05/2014
    /// <summary>
    /// Determina si se controlan los errores en tiempo de ejecución del programa.
    /// Si ocurre una excepción controlada y se encuentra en true - Registra el tiempo, tipo y origen de la excepción en ~/App_Data/ExceptionLog.xml
    /// </summary>
    public static Boolean DEPLOYED = true;
    /// <summary>
    /// Llave asociada a DEPLOYED para la tabla de configuraciones
    /// </summary>
    public const int KEY_DEPLOYED = 2;

    // 11/06/2014
    /// <summary>
    /// Activa o desactiva el envío de mensajes de correo electrónico de este filesystem
    /// </summary>
    public static Boolean MAIL_ACTIVO = false;
    /// <summary>
    /// Llave asociada a MAIL_ACTIVO para la tabla de configuraciones
    /// </summary>
    public const int KEY_MAIL_ACTIVO = 3;

    // 13/03/2014
    // 
    /// <summary>
    /// Determina si se muestra el botón de "guardar respuesta" al contestar Respuestas Abiertas, 
    /// así como el checkbox "Mostrar Borradores" al calificar respuestas abiertas
    /// </summary>
    public static Boolean GUARDAR_RESPUESTA_ABIERTA = true;
    /// <summary>
    /// Llave asociada a GUARDAR_RESPUESTA_ABIERTA para la tabla de configuraciones
    /// </summary>
    public const int KEY_GUARDAR_RESPUESTA_ABIERTA = 4;

    // 04/09/2014
    /// <summary>
    /// Determina si se muestra información de Integrant en el pie de las páginas de la aplicación
    /// </summary>
    public static Boolean FIRMA_PIE_INTEGRANT = true;
    /// <summary>
    /// Llave asociada a FIRMA_PIE_INTEGRANT para la tabla de configuraciones
    /// </summary>
    public const int KEY_FIRMA_PIE_INTEGRANT= 11;

    // 04/09/2014
    /// <summary>
    /// Determina el método de trabajo de la aplicación.
    /// 0 - Escuela
    /// 1 - Empresa
    /// </summary>
    /// <remarks>
    /// Escuela
    /// -------
    /// Las Calificaciones valen porcentajes de Asiganturas, 
    /// y las Evaluaciones valen porcentajes de Calificaciones.
    /// 
    /// Empresa
    /// -------
    /// Las Calificaciones tienen peso en Asignaturas,
    /// y las Evaluaciones tienen peso en Calificaciones.
    /// </remarks>
    public static int Modelo_Sistema = 0;
    /// <summary>
    /// Llave asociada a Modelo_Sistema para la tabla de configuraciones
    /// </summary>
    public const int Key_Modelo_Sistema= 16;

    // 16/04/2015
    /// <summary>
    /// Indica el estilo de inicio de sesión.
    /// 0 - login_jp1 Primera versión de Juan Pablo, 
    /// 1 - login_a1
    /// </summary>
    public static int Login_Screen= 0;
    /// <summary>
    /// Llave asociada a Login_Screen para la tabla de configuraciones
    /// </summary>
    public const int Key_Login_Screen= 17;

    // ***************************************
    //                                  Alumno
    // ***************************************

    // 25-02-2014
    /// <summary>
    /// Determina si en la página de contestar del alumno aparece el botón de renunciar a la actividad
    /// </summary>
    public static Boolean ALUMNO_CONTESTAR_RENUNCIAR = true;
    /// <summary>
    /// Llave asociada a ALUMNO_CONTESTAR_RENUNCIAR para la tabla de configuraciones
    /// </summary>
    public const int KEY_ALUMNO_CONTESTAR_RENUNCIAR = 5;

    // 14/03/2014
    /// <summary>
    /// Determina si en la página de pendientes del alumno aparece la columna de tipo de actividad
    /// </summary>
    public static Boolean ALUMNO_PENDIENTES_COLUMNA_TIPO = true;
    /// <summary>
    /// Llave asociada a ALUMNO_PENDIENTES_COLUMNA_TIPO para la tabla de configuraciones
    /// </summary>
    public const int KEY_ALUMNO_PENDIENTES_COLUMNA_TIPO = 6;

    // 14/03/2014
    /// <summary>
    /// Determina si en la página de pendientes del alumno aparece la columna de fecha de penalización
    /// </summary>
    public static Boolean ALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION = true;
    /// <summary>
    /// Llave asociada a ALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION para la tabla de configuraciones
    /// </summary>
    public const int KEY_ALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION = 7;

    // 14/03/2014
    /// <summary>
    /// Determina si en la página de pendientes del alumno aparece la columna de porcentaje de penalización
    /// </summary>
    public static Boolean ALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION  = true;
    /// <summary>
    /// Llave asociada a ALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION para la tabla de configuraciones
    /// </summary>
    public const int KEY_ALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION = 8;

    /// <summary>
    /// Determina si en la página de pendientes del alumno aparece la columna de entrega
    /// </summary>
    public static Boolean ALUMNO_PENDIENTES_COLUMNA_ENTREGA = true;

    /// <summary>
    /// Llave asociada a ALUMNO_PENDIENTES_COLUMNA_ENTREGA para la tabla de configuraciones
    /// </summary>
    public const int KEY_ALUMNO_PENDIENTES_COLUMNA_ENTREGA = 21;

    // ***************************************
    //                                Profesor
    // ***************************************

    // 25-02-2014
    /// <summary>
    /// Determina si en las opciones de calificar respuesta abierta del profesor aparecen
    /// 0 = calificaciones 0-100
    /// 1 = escalas bien-mal
    /// </summary>
    public static int PROFESOR_CALIFICA_NUMERICO = 0;
    /// <summary>
    /// Llave asociada a PROFESOR_CALIFICA_NUMERICO para la tabla de configuraciones
    /// </summary>
    public const int KEY_PROFESOR_CALIFICA_NUMERICO = 9;

    // 31/03/2014
    /// <summary>
    /// Determina si en las opciones de calificar documento del profesor aparecen 
    /// 0 = calificaciones 0-100
    /// 1 = escalas competente-nocompetente
    /// </summary>
    public static int PROFESOR_CALIFICA_DOCUMENTO = 0;
    /// <summary>
    /// Llave asociada a PROFESOR_CALIFICA_DOCUMENTO para la tabla de configuraciones
    /// </summary>
    public const int KEY_PROFESOR_CALIFICA_DOCUMENTO = 10;

    /// <summary>
    /// Determina si ViewerJS debe abrir el archivo para no poderse descargar 
    /// 0 = sin proteccion
    /// 1 = protegido (redireccion con VIEWERJS
    /// </summary>
    public static Boolean DOCUMENTO_PROTEGIDO = false;
    /// <summary>
    /// Llave asociada a PROFESOR_CALIFICA_DOCUMENTO para la tabla de configuraciones
    /// </summary>
    public const int KEY_DOCUMENTO_PROTEGIDO = 18;

    /// <summary>
    /// Determina si se ve el área para inscribir cursos
    /// 0 = no visible
    /// 1 = visible
    /// </summary>
    public static Boolean V_INSCRIBIR_CURSOS = false;
    /// <summary>
    /// Llave asociada a PROFESOR_CALIFICA_DOCUMENTO para la tabla de configuraciones
    /// </summary>
    public const int KEY_INSCRIBIR_CURSOS = 19;

    /// <summary>
    /// Determina si es obligatorio que se cambie la contraseña la primera vez que entre el alumno a la plataforma
    /// 0 = no obligatoria
    /// 1 = obligatoria
    /// </summary>
    public static Boolean CONTRA_OBLIGATORIA = false;
    /// <summary>
    /// Llave asociada a PROFESOR_CALIFICA_DOCUMENTO para la tabla de configuraciones
    /// </summary>
    public const int KEY_CONTRA_OBLIGATORIA = 20;




    // *****************************************************************************************
    //                                         Rutas
    // *****************************************************************************************

    // 20-09-2013
    /// <summary>
    /// Éste es el nombre del filesystem real en el servidor, controla las rutas de cargas y materiales
    /// </summary>
    /// <remarks>
    /// Server.MapPath("~/") entrega "C:\inetpub\wwwroot\CARPETA_DE_FILESYTEM\"
    /// Remove(0, 18) quita "C:\inetpub\wwwroot"
    /// Trim("\") quita "\" al inicio y al final del nombre
    /// </remarks>
    public static string NOMBRE_FILESYSTEM =
        HttpContext.Current.Server.MapPath("~/").Remove(0, 18).Trim('\\');

    public static string rutaBase = 
      "C:\\inetpub\\wwwroot\\" + Global.NOMBRE_FILESYSTEM;

    public static string urlBase =
        "/" + NOMBRE_FILESYSTEM;

    // _____________________________________________________________________
    // Éstas funciones controlan la ubicación física y web de 
    // materiales, recursos y apoyos del filesystem.
    // La estructura del filesystem debe coincidir con estas variables.

    /// <summary>
    /// Ruta física de apoyos 
    /// (desde C:\\inetpub\...)
    /// </summary>
    public static string rutaApoyo =
        rutaBase + "\\Resources\\apoyo\\";


    /// <summary>s
    /// Ruta web para guardar las imágenes 
    /// (desde DOMINIO_ACTUAL/CARPETA_DE_FILESYTEM/...)
    /// </summary>
    /// <remarks>


    public static string rutaPerfil =
  rutaBase + "\\Resources\\profileImages\\";

    public static string urlImagenPerfil =
   "/" + NOMBRE_FILESYSTEM + "/Resources/profileImages/";



    /// <summary>
    /// Ruta web de apoyos 
    /// (desde DOMINIO_ACTUAL/CARPETA_DE_FILESYTEM/...)
    /// </summary>
    /// <remarks>
    /// ~/Resources/apoyo/ 
    /// Está hardcoded en algunos gridviews (páginas .aspx)
    /// </remarks>
    /// 



    public static string urlApoyo =
        "/" + NOMBRE_FILESYSTEM + "/Resources/apoyo/";

    /// <summary>
    /// Ruta física de cargas 
    /// (desde C:\\inetpub\...)
    /// </summary>
    public static string rutaCargas =
        rutaBase + "\\Resources\\cargas\\";
    /// <summary>
    /// Ruta web de cargas 
    /// (desde DOMINIO_ACTUAL/CARPETA_DE_FILESYTEM/...)
    /// </summary>
    /// <remarks>
    /// ~/Resources/cargas/ 
    /// Está hardcoded en algunos gridviews (páginas .aspx)
    /// </remarks>
    public static string urlCargas  =
        "/" + NOMBRE_FILESYSTEM + "/Resources/cargas/";

    /// <summary>
    /// Ruta física de materiales 
    /// (desde C:\\inetpub\...)
    /// </summary>
    public static string rutaMaterial =
        rutaBase + "\\Resources\\material\\";


    public static string rutaApoyoPlanteamientos =
      rutaBase + "\\Resources\\apoyoPlanteamientos\\";

    public static string urlApoyoPlanteamientos =
        "/" + NOMBRE_FILESYSTEM + "/Resources/apoyoPlanteamientos/";


    /// <summary>
    /// Ruta web de materiales 
    /// (desde DOMINIO_ACTUAL/CARPETA_DE_FILESYTEM/...)
    /// </summary>
    /// <remarks>
    /// ~/Resources/material/ 
    /// Está hardcoded en algunos gridviews (páginas .aspx)
    /// </remarks>
    public static string urlMaterial =
        "/" + NOMBRE_FILESYSTEM + "/Resources/material/";
    public static string urlMaterialAux ="~\\Resources\\material\\";

    /// <summary>
    // ~/Resources/material/ 
    /// Url de Material PDF
    /// </summary>
    public static string urlMaterialPDF=
        "/" + NOMBRE_FILESYSTEM + "/ViewerJS/#../Resources/material/";
    /// <summary>
    /// Ruta física de adjuntos 
    /// (desde C:\\inetpub\...)
    /// </summary>
    public static string rutaAdjuntos_fisica =
        rutaBase + "\\Resources\\adjuntos\\";
    /// <summary>
    /// Ruta web de adjuntos 
    /// (desde DOMINIO_ACTUAL/CARPETA_DE_FILESYTEM/...)
    /// </summary>
    public static string urlAdjuntos =
        "/" + NOMBRE_FILESYSTEM + "/Resources/adjuntos/";

    public static string urlCss=
        "/" + NOMBRE_FILESYSTEM + "/Resources/css/";

    /// <summary>
    /// Ruta física de imagenes 
    /// (desde C:\\inetpub\...)
    /// </summary>
    public static string rutaImagenes =
        rutaBase + "\\Resources\\imagenes\\";

        public static string rutaCustomization =
        "/" + NOMBRE_FILESYSTEM + "/Resources/Customization/imagenes/";
        /// <summary>
        /// Ruta web de imagenes 
        /// (desde DOMINIO_ACTUAL/CARPETA_DE_FILESYTEM/...)
        /// </summary>
        /// <remarks>
        /// ~/Resources/imagenes/ 
        /// Está hardcoded en el markup de muchas páginas (.aspx)
        /// </remarks>
        public static string urlImagenes =
        "/" + NOMBRE_FILESYSTEM + "/Resources/imagenes/";

    /// <summary>
    /// Ruta física de enlaces personalizados del filesystem 
    /// (desde C:\\inetpub\...)
    /// </summary>
    public static string rutaEnlaces =
        rutaBase + "\\Resources\\enlaces\\";
    /// <summary>
    /// Ruta web de enlaces personalizados del filesystem 
    /// (desde DOMINIO_ACTUAL/CARPETA_DE_FILESYTEM/...)
    /// </summary>
    public static string urlEnlaces =
        "/" + NOMBRE_FILESYSTEM + "/Resources/enlaces/";

    /// <summary>
    /// Ruta web de librerías de scripts del filesystem
    /// </summary>
    public static string urlScripts =
        "/" + NOMBRE_FILESYSTEM + "/Resources/scripts/";

    /// <summary>
    /// Ruta de archivos de traducciones de la aplicación
    /// </summary>
    public static string rutaLenguajes =
        rutaBase + "\\Resources\\Lang\\";

    /// <summary>
    /// Ruta de archivos de personalización de la aplicación
    /// </summary>
    public static string rutaPersonalizacion  =
        rutaBase + "\\Resources\\Customization\\";

    /// <summary>
    /// Ruta de archivo de bloqueos. 
    /// Los períodos de bloqueo de sistema por perfil y 
    /// por hora se almacenan en este archivo.
    /// </summary>
    public static string rutaBloqueos =
        rutaBase + "\\App_Data\\Bloqueos.xml";

    /// <summary>
    /// Ruta de archivo de mantenimientos. 
    /// Los períodos de bloqueo de sistema por mantenimientos 
    /// (todos los perfiles a cierta hora) se almacenan en este archivo.
    /// </summary>
    public static string rutaMantenimientos =
        rutaBase + "\\App_Data\\Mantenimientos.xml";

    /// <summary>
    /// Ruta de archivo de errores de aplicación. 
    /// ExceptionLog registra aquí los errores.
    /// </summary>
    public static string rutaLogExcepciones  =
        rutaBase + "\\App_Data\\ExceptionLog.xml";

    public static string rutaEstructuraBiblioteca =
    rutaBase + "\\Resources\\cargas\\libreria.json";

  }
}