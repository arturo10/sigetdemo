﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace Siget.Config
{

  /// <summary>
  /// Summary description for Color_Import
  /// </summary>
  public class Color_Import
  {

    public static void configuraColores()
    {
      importa_Colores();
    }

    /// <summary>
    /// Al leer un registro de XML, se incluyen los saltos de línea englobados por cada tag; 
    /// ésta función se asegura que el registro leído tenga ambos extremos libres de espacios y saltos de línea.
    /// </summary>
    /// <param name="input">El String a procesar</param>
    /// <returns>Un String limpio</returns>
    /// <remarks></remarks>
    protected static string clean(string input)
    {
      return input.Replace(Environment.NewLine, "").Trim();
    }

    /// <summary>
    /// Obtiene las los colores aplicados y los escribe en la configuración de colores del filesystem.
    /// </summary>
    /// <remarks>Las lecturas de este archivo xml no deben parsearse por etiquetas, puesto que son las etiquetas mismas.</remarks>
    protected static void importa_Colores()
    {
      using (XmlTextReader langFile = new XmlTextReader(Config.Global.rutaPersonalizacion + "\\Colores.xml"))
      {

        langFile.ReadToFollowing("Login_TextBox_Border");
        Color.Login_TextBox_Border = clean(langFile.ReadString());

        langFile.ReadToFollowing("TituloPrincipal_Top");
        Color.TituloPrincipal_Top = clean(langFile.ReadString());

        langFile.ReadToFollowing("TituloPrincipal_Bottom");
        Color.TituloPrincipal_Bottom = clean(langFile.ReadString());

        langFile.ReadToFollowing("MenuPendientes_Asignatura_Displayed");
        Color.MenuPendientes_Asignatura_Displayed = clean(langFile.ReadString());

        langFile.ReadToFollowing("MenuPendientes_Asignatura_Displayed_Font");
        Color.MenuPendientes_Asignatura_Displayed_Font = clean(langFile.ReadString());

        langFile.ReadToFollowing("MenuPendientes_Calificacion_Displayed");
        Color.MenuPendientes_Calificacion_Displayed = clean(langFile.ReadString());

        langFile.ReadToFollowing("MenuPendientes_Calificacion_Displayed_Font");
        Color.MenuPendientes_Calificacion_Displayed_Font = clean(langFile.ReadString());

        langFile.ReadToFollowing("MenuPendientes_Subtitle");
        Color.MenuPendientes_Subtitle = clean(langFile.ReadString());

        langFile.ReadToFollowing("Table_HeaderRow_Background");
        Color.Table_HeaderRow_Background = clean(langFile.ReadString());

        langFile.ReadToFollowing("Table_HeaderRow_Color");
        Color.Table_HeaderRow_Color = clean(langFile.ReadString());

        langFile.ReadToFollowing("Table_SelectableRowHover_Background");
        Color.Table_SelectableRowHover_Background = clean(langFile.ReadString());

        langFile.ReadToFollowing("Table_SelectedRow_Background");
        Color.Table_SelectedRow_Background = clean(langFile.ReadString());

        langFile.ReadToFollowing("Table_SelectedRowHover_Background");
        Color.Table_SelectedRowHover_Background = clean(langFile.ReadString());

        // _________________________________________________________________________________________
        // colores del diseño 5.1.0

        langFile.ReadToFollowing("MenuBar_SelectedOption_BorderColor");
        Color.MenuBar_SelectedOption_BorderColor = clean(langFile.ReadString());

        langFile.Close();
      }
    }

  }

}