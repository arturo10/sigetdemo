﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;
using Siget;
using Siget.Entity;
/// <summary>
/// Summary description for Files
/// </summary>
/// 
namespace Siget.Utils
{
    public class FileUtils
    {
        public FileUtils()
        {
           
        }

        public static void SaveFileToDisk(FileUpload file,string fileName,HyperLink li)
        {

            //if (File.Exists(Config.Global.rutaMaterial + fileName))
            //{
                
            //    li.Text = "El archivo que está intentando cargar ya existía, revise que sea el correcto, de lo contrario, actualice el registro.";
            //    li.NavigateUrl = Config.Global.urlMaterial + fileName;
            //}
            //else
            //{
            //    file.SaveAs(HttpContext.Current.Server.MapPath(Siget.Config.Global.urlMaterialAux + fileName));
            //    li.Text = "Archivo cargado: " + fileName;
            //    li.NavigateUrl = Config.Global.urlMaterial + fileName;
            //}
                        
        }


        public static void ProcessFile(FileUpload file,
            string fileName,
            string SupportFile,
            HyperLink li,string oldFile){

            //try{
            //    if (File.Exists(Config.Global.rutaMaterial + fileName))
            //    {
            //            li.Text = "El archivo que está intentando cargar ya existía, revise que sea el correcto, de lo contrario, actualice el registro.";
            //            li.NavigateUrl = Config.Global.urlMaterial + fileName;
            //    }
            //        else{
            //            File.Delete(HttpContext.Current.Server.MapPath(Siget.Config.Global.urlMaterialAux + oldFile));
            //            li.Text = "Archivo cargado: " + fileName;
            //            file.SaveAs(HttpContext.Current.Server.MapPath(Siget.Config.Global.urlMaterialAux + fileName));
            //            li.NavigateUrl = Config.Global.urlMaterial + fileName;
            //         }
            //}catch(Exception e){
            //    Siget.Utils.LogManager.ExceptionLog_InsertEntry(e);
            //}
                
            
        }

        public static  String getUrlProfileImage(string dirPath,string nameUser)
        {
            DirectoryInfo Dir = new DirectoryInfo(dirPath);
            FileInfo[] file=Dir.GetFiles(nameUser + ".*");

            return file.Count()>0? file[0].Name:String.Empty;
        }

        public static void clearAllProfileImages(string dirPath,string nameUser)
        {
            DirectoryInfo Dir = new DirectoryInfo(dirPath);
            FileInfo[] files = Dir.GetFiles(nameUser + ".*");


            foreach (FileInfo file in files)
                file.Delete();
            
            
        }


        public static bool isThisFilesWithMultipleAssociations(string FileName)
        {

            IRepository<Planteamiento>  repositoryPlanteamiento = new PlanteamientoRepository();
            IRepository<Opcion>repositoryOpcion=new OpcionRepository();
            IRepository<ApoyoSubtema> repositoryApoyo=new ApoyoSubtemaRepository();
            int numberOfFiles = 0;

            numberOfFiles += (from r in repositoryPlanteamiento.List
                              where
                                (r.ArchivoApoyo == FileName ||
                               r.ArchivoApoyo2 == FileName )
                              select r).ToList().Count();

            numberOfFiles += (from r in repositoryOpcion.List
                              where(r.ArchivoApoyo == FileName)
                              select r).ToList().Count();

            numberOfFiles += (from r in repositoryApoyo.List
                              where (r.ArchivoApoyo == FileName )
                              select r).ToList().Count();


            if (numberOfFiles > 0)
                return true;

            return false;


        }


    }
}