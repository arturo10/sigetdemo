﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace Siget.Utils
{

  public class DesbloquearUsuarios
  {
    // inicia en una hora imposible para que cuando se inicie la aplicación desbloquee:
    // Almacena la fecha del último desbloqueo automático, 
    // para controlar que se haga cada 20 minutos

    public static DateTime? LastExecution = null;
    public static void LiberaBloqueados()
    {
      try
      {
        string strCon = null;
        strCon = ConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString;
        SqlConnection objCon = new SqlConnection(strCon);
        SqlCommand comm = default(SqlCommand);
        comm = objCon.CreateCommand();
        objCon.Open();
        comm.CommandText = "SET dateformat dmy; UPDATE dbo.aspnet_Membership SET IsLockedOut = 0, FailedPasswordAttemptCount = 0, " + "FailedPasswordAttemptWindowStart = CONVERT( datetime, '17540101', 112 ), FailedPasswordAnswerAttemptCount = 0, " + "FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 ), " + "LastLockoutDate = CONVERT( datetime, '17540101', 112 )";
        comm.ExecuteNonQuery();
        objCon.Close();

      }
      catch (Exception ex)
      {
      }
    }

    // revisa si la última hora en la que se desbloqueó usuarios es más de 20 minutos mayor a ésta, 
    // entonces desbloquea
    // (cada 20 minutos se desbloquean usuarios, 
    // aún mientras se ejecute esta función continuamente)
    public static void Revisa_Desbloquea()
    {
      if ((LastExecution == null))
      {
        LastExecution = DateTime.Now;
        LiberaBloqueados();
      }
      if (DateTime.Now > LastExecution.Value.AddMinutes(20))
      {
        LastExecution = DateTime.Now;
        LiberaBloqueados();
      }
    }

  }

}