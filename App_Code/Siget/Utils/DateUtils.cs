﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Siget.Utils
{

  /// <summary>
  /// Contiene métodos de apoyo para el manejo de fechas.
  /// </summary>
  public class DateUtils
  {

    /// <summary>
    /// Recibe una cadena con únicamente una fecha y le añade la hora en ceros: "DD/MM/AAAA 00:00:00"
    /// </summary>
    /// <param name="input">Cadena con la fecha a modificar</param>
    /// <returns>
    /// Una cadena compuesta por el día recibido y la hora en ceros: "DD/MM/AAAA 00:00:00"
    /// </returns>
    public static string Date_to_StartOfDay(string input)
    {
      return input + " 00:00:00";
    }

    /// <summary>
    /// Recibe un DateTime con únicamente una fecha y le añade la hora en ceros: "DD/MM/AAAA 00:00:00"
    /// </summary>
    /// <param name="input">Datetime con la fecha a modificar</param>
    /// <returns>
    /// Un DateTime compuesta por el día recibido y la hora en ceros: "DD/MM/AAAA 00:00:00"
    /// </returns>
    public static DateTime Date_to_StartOfDay(DateTime input)
    {
      return DateTime.Parse(input.ToShortDateString() + " 00:00:00");
    }

    /// <summary>
    /// Recibe una cadena con únicamente una fecha y le añade el último segundo del día: "DD/MM/AAAA 00:00:00"
    /// </summary>
    /// <param name="input">Cadena con la fecha a modificar</param>
    /// <returns>
    /// Una cadena compuesta con el día recibido y el último segundo del día: "DD/MM/AAAA 00:00:00"
    /// </returns>
    public static string Date_to_EndOfDay(string input)
    {
      return input + " 23:59:59";
    }

    /// <summary>
    /// Recibe un DateTime con únicamente una fecha y le añade el último segundo del día: "DD/MM/AAAA 23:59:59"
    /// </summary>
    /// <param name="input">Datetime con la fecha a modificar</param>
    /// <returns>
    /// Un DateTime con el día recibido y el último segundo del día: "DD/MM/AAAA 23:59:59"
    /// </returns>
    public static DateTime Date_to_EndOfDay(DateTime input)
    {
      return DateTime.Parse(input.ToShortDateString() + " 23:59:59");
    }
  }

}