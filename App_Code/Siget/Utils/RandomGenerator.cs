﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using Siget.Beans;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for RandomGenerator
/// </summary>
/// 

namespace Siget.Utils
{
    public class RandomGenerator
    {
        public RandomGenerator()
        {

        }

        public ArrayList getRandomCombination( ArrayList arrayConsecutivo, int opcionesAleatorias, DataTable dt)
        {
            Random generatorNumber = new Random();
            int randomNumber = 0;
            bool SALIDA = false;
            

            if (opcionesAleatorias == 1)
            {
                arrayConsecutivo.Clear();
                if (HttpContext.Current.Session["arrayConsecutivo"]!=null)
                   arrayConsecutivo = (ArrayList)HttpContext.Current.Session["arrayConsecutivo"];
                else {
                    randomNumber = generatorNumber.Next(0, dt.Rows.Count);
                    do {
                        if (!arrayConsecutivo.Contains(randomNumber))
                            arrayConsecutivo.Add(randomNumber);
                        else if (arrayConsecutivo.Count == dt.Rows.Count)
                            SALIDA = true;
                        else
                            randomNumber = generatorNumber.Next(0, dt.Rows.Count);

                  } while (!SALIDA);
                }

                HttpContext.Current.Session["arrayConsecutivo"] = arrayConsecutivo;
            }
            return arrayConsecutivo;
        }
    }
}