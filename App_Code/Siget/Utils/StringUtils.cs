﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Collections;

namespace Siget.Utils
{

  /// <summary>
  /// Contiene métodos de apoyo para el manejo de cadenas de caracteres.
  /// </summary>
  public class StringUtils
  {

    /// <summary>
    /// Compara dos cadenas de caracteres de acuerdo a criterios específicos.
    /// </summary>
    /// <param name="a">Primer cadena</param>
    /// <param name="b">Segunda cadena</param>
    /// <param name="comparaMayusculas">
    /// True - Hola y hola son diferentes.
    /// </param>
    /// <param name="comparaAcentos">
    /// True - holá y hola son diferentes.
    /// </param>
    /// <param name="soloNumerosYLetras">
    /// True - Ignora todos los caracteres que no sean alfanuméricos.
    /// </param>
    /// <returns>
    /// 0 si las cadenas son iguales bajo los parámetros establecidos; 
    /// diferente de 0 si hay diferencias.
    /// </returns>
    public static int ComparaRespuesta(
      string a, 
      string b, 
      bool comparaMayusculas, 
      bool comparaAcentos, 
      bool soloNumerosYLetras,
        bool eliminaPuntuacion)
    {
      // opciones de comparación de strings
      System.Globalization.CompareOptions compOptions;


      // si no compara mayusculas, manda todo a minúsculas para comparar
      if (!comparaMayusculas)
      {
        a = a.ToLower();
        b = b.ToLower(); 
      }


      // si compara solo números y letras, extrae alfanuméricos de cada uno
      if (soloNumerosYLetras)
      {
          string pattern="['´`]";
          Regex matcher=new Regex(pattern);
          a = matcher.Replace(a,"x");
          b = matcher.Replace(b, "x");
      }

      if (eliminaPuntuacion)
      {
          string pattern = "[,:;.\\s]";
          Regex matcher = new Regex(pattern);
          a = matcher.Replace(a, String.Empty);
          b = matcher.Replace(b, String.Empty);
      }

      // si no compara acentos, especifica que ignore caracteres no espaciales en la comparación (´`^);
      // éstos son los que tienes que presionar dos veces en el teclado por que por sí solos no aparecen
      if (!comparaAcentos)
        compOptions = System.Globalization.CompareOptions.IgnoreNonSpace;
      else
        compOptions = System.Globalization.CompareOptions.None;

      // limpio los espacios sobrantes de las respuestas;
      // esto debe hacerse justo antes de la comparación puesto que los otros 
      // métodos pueden cambiar cadenas como "Soy . David" por "Soy  David" (generan dobles espacios)
      a = LimpiaEspacios(a);
      b = LimpiaEspacios(b);

      // realiza la comparación con las opciones y entradas procesadas
      return String.Compare(a, b, System.Globalization.CultureInfo.CurrentCulture, compOptions);
    }

    /// <summary>
    /// Elimina los espacios al inicio y al final de una cadena, 
    /// y simplifica todos los espacios dobles a espacios unicos.
    /// </summary>
    /// <param name="input">cadena a limpiar</param>
    /// <returns>
    /// Cadena sin espacios dobles y sin espacios al inicio o al final.
    /// </returns>
    public static string LimpiaEspacios(string input)
    {
      string s = input.Trim();
      while (s.Contains("  "))
        s = s.Replace("  ", " ");
      return s;
    }

  }

}