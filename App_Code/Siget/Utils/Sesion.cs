﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;
using System.Web;

namespace Siget.Utils
{

  public class Sesion
  {

    /// <summary>
    /// Elimina las variables de sesisón de contestado
    /// </summary>
    public static void LimpiaSesionContestado()
    {
      if ((HttpContext.Current.Session["Contestar_IdEvaluacion"] != null))
      {
        HttpContext.Current.Session.Clear();
        Siget.Utils.Sesion.sesionAbierta();
      }
    }

    public static bool verificaSiEsPrimeraVez(string login)
    {
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(
                  "SELECT FT FROM Usuario WHERE Login=@Login", conn))
                {

                    cmd.Parameters.AddWithValue("@Login", login);
                    using (SqlDataReader results = cmd.ExecuteReader())
                    {
                        results.Read();
                        if (results["FT"] == DBNull.Value)
                            return true;
                        return false;

                    }
                }
            }
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
            return false;
        }
    }


    public static bool SeConectaPrimeraVez(string login)
    {
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(
                  "UPDATE Usuario SET FT=1 WHERE Login=@Login", conn))
                {

                    cmd.Parameters.AddWithValue("@Login", login);
                    cmd.ExecuteNonQuery();
                    return true;
                }
            }
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
            return false;
        }
    }

    /// <summary>
    /// Extensión del cierre de sesión de autenticación; 
    /// se asegura que se terminen todas las variables de la sesión de servidor
    /// </summary>
    public static void CierraSesion()
    {
      // primero destruyo todas variables de sesión del usuario
      HttpContext.Current.Session.Clear();

      // luego hago logout
      FormsAuthentication.SignOut();
    }

    /// <summary>
    /// Verifica si están establecidas las variables de sesión de datos de usuario; 
    /// de no estarlo las actualiza.
    /// </summary>
    public static bool sesionAbierta()
    {
      return sesionAbierta(HttpContext.Current.User.Identity.Name.Trim());
    }

    // Sobrecarga
    public static bool sesionAbierta(string login)
    {
      // Establece las variables de sesion del usuario
      if ((Roles.IsUserInRole(login, Siget.Config.Global.Perfil_Alumno)))
      {
        if (HttpContext.Current.Session["Usuario_IdAlumno"] == null)
        {
          Alumno_IniciaSesion(login);
          return false;
        }
      }
      else if ((Roles.IsUserInRole(login, Siget.Config.Global.Perfil_Administrador) | Roles.IsUserInRole(login, Siget.Config.Global.Perfil_SysAdmin) | Roles.IsUserInRole(login, Siget.Config.Global.Perfil_Capturista)))
      {
        if (HttpContext.Current.Session["Usuario_IdAdmin"] == null)
        {
          Admin_IniciaSesion(login);
          return false;
        }
      }
      else if ((Roles.IsUserInRole(login, Siget.Config.Global.Perfil_Coordinador)))
      {
        if (HttpContext.Current.Session["Usuario_IdCoordinador"] == null)
        {
          Coordinador_IniciaSesion(login);
          return false;
        }
      }
      else if ((Roles.IsUserInRole(login, Siget.Config.Global.Perfil_Operador)))
      {
        if (HttpContext.Current.Session["Usuario_IdOperador"] == null)
        {
          Operador_IniciaSesion(login);
          return false;
        }
      }
      else if ((Roles.IsUserInRole(login, Siget.Config.Global.Perfil_Profesor)))
      {
        if (HttpContext.Current.Session["Usuario_IdProfesor"] == null)
        {
          Profesor_IniciaSesion(login);
          return false;
        }
      }

      return true;
    }

    /// <summary>
    /// Obtiene información del usuario necesaria en su perfil
    /// </summary>
    /// <param name="login">nombre de usuario que ingresa</param>
    /// <returns>Boolean true si termina exitosamente, false si hay errores</returns>
    /// <remarks></remarks>
    public static bool Alumno_IniciaSesion(string login)
    {
      try
      {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
        {
          conn.Open();
          using (SqlCommand cmd = new SqlCommand(
            "SELECT " + 
            "   A.IdAlumno, " + 
            "   A.Estatus, " + 
            "   A.Nombre, " + 
            "   A.ApePaterno, " + 
            "   A.ApeMaterno, " + 
            "   U.Password, " + 
            "   U.Idioma, " +
            "   U.Email, "+
            "   A.Email As EmailSecundario, "+
            "   A.IdPlantel, " + 
            "   G.IdGrado, " + 
            "   G.IdCicloEscolar, " + 
            "   G.IdGrupo " + 
            "FROM " + 
            "   Alumno A, " + 
            "   (SELECT " + 
            "       IdUsuario, " + 
            "       Email,"+
            "       Password, " + 
            "       Idioma " + 
            "   FROM Usuario " + 
            "   WHERE Login = @Login) U, " + 
            "   Grupo G " + 
            "WHERE " + 
            "   A.IdUsuario = U.IdUsuario " + 
            "   AND A.IdGrupo = G.IdGrupo ", conn))
          {

            cmd.Parameters.AddWithValue("@Login", login);

            using (SqlDataReader results = cmd.ExecuteReader())
            {

              results.Read();

              HttpContext.Current.Session["Usuario_IdAlumno"] = results["IdAlumno"].ToString().Trim();
              HttpContext.Current.Session["Usuario_Estatus"] = results["Estatus"].ToString().Trim();
              HttpContext.Current.Session["Usuario_Nombre"] = results["Nombre"].ToString().Trim() + " " + results["ApePaterno"].ToString().Trim() + " " + results["ApeMaterno"].ToString().Trim();
              HttpContext.Current.Session["Usuario_IdPlantel"] = results["IdPlantel"].ToString().Trim();
              HttpContext.Current.Session["Usuario_IdGrado"] = results["IdGrado"].ToString().Trim();
              HttpContext.Current.Session["Usuario_IdCicloEscolar"] = results["IdCicloEscolar"].ToString().Trim();
              HttpContext.Current.Session["Usuario_IdGrupo"] = results["IdGrupo"].ToString().Trim();


              if (results["Idioma"] != DBNull.Value)
                  HttpContext.Current.Session["Usuario_Idioma"] = results["Idioma"].ToString().Trim();
              else
                  HttpContext.Current.Session["Usuario_Idioma"] = Config.Global.Idioma_Alumno;

              if (results["EmailSecundario"] != DBNull.Value || results["EmailSecundario"].ToString().Trim()!=String.Empty)
                  HttpContext.Current.Session["Usuario_Email"] = results["EmailSecundario"].ToString().Trim();
              else if (results["Email"]!=DBNull.Value)
                  HttpContext.Current.Session["Usuario_Email"] = results["Email"].ToString().Trim();

              //Las siguientes variables de Aplicación las lee el archivo App_Code/PTBlogTemplate/UserFunctions.vb
              //La quize poner en If Menu3.SelectedItem.Value = "Blog" Then pero no funcionó
              HttpContext.Current.Application["Login"] = login;
              HttpContext.Current.Application["Passwd"] = results["Password"];

            }
          }
        }
      }
      catch (Exception ex)
      {
        Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Obtiene información del usuario necesaria en su perfil
    /// </summary>
    /// <param name="login">nombre de usuario que ingresa</param>
    /// <returns>Boolean true si termina exitosamente, false si hay errores</returns>
    /// <remarks></remarks>
    public static bool Admin_IniciaSesion(string login)
    {
      try
      {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager
          .ConnectionStrings["sadcomeConnectionString"]
          .ConnectionString))
        {
          conn.Open();

          using (SqlCommand cmd = new SqlCommand(
            "SELECT " + 
            "   A.IdAdmin, " + 
            "   U.Idioma, " +
            "   U.Email "+
            "FROM " + 
            "   Admin A " + 
            "   ,Usuario U " + 
            "WHERE " + 
            "   U.Login = @Login " + 
            "   AND A.IdUsuario = U.IdUsuario", conn))
          {

            cmd.Parameters.AddWithValue("@Login", login);

            using (SqlDataReader results = cmd.ExecuteReader())
            {
                
              results.Read();

              HttpContext.Current.Session["Usuario_Email"] = results["Email"]!=DBNull.Value ?results["Email"].ToString().Trim():string.Empty;  
              HttpContext.Current.Session["Usuario_IdAdmin"] = results["IdAdmin"].ToString().Trim();
              HttpContext.Current.Session["Usuario_Idioma"] = Siget.Config.Global.Idioma_Administrador;
            
            }
          }
        }
      }
      catch (Exception ex)
      {
        Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Obtiene información del usuario necesaria en su perfil
    /// </summary>
    /// <param name="login">nombre de usuario que ingresa</param>
    /// <returns>Boolean true si termina exitosamente, false si hay errores</returns>
    /// <remarks></remarks>
    public static bool Operador_IniciaSesion(string login)
    {
      try
      {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
        {
          conn.Open();

          using (SqlCommand cmd = new SqlCommand(
            "SELECT " + 
            "   O.IdOperador, " + 
            "   U.Idioma " + 
            "FROM " + 
            "   Operador O, " + 
            "   (SELECT " + 
            "       IdUsuario, " + 
            "       Idioma " + 
            "   FROM Usuario " + 
            "   WHERE PerfilASP = 'operador' " + 
            "       AND Login = @Login) U " + 
            "WHERE " + 
            "   U.IdUsuario = O.IdUsuario", conn))
          {

            cmd.Parameters.AddWithValue("@Login", login);

            using (SqlDataReader results = cmd.ExecuteReader())
            {

              results.Read();

              HttpContext.Current.Session["Usuario_IdOperador"] = results["IdOperador"].ToString().Trim();
             
                HttpContext.Current.Session["Usuario_Idioma"] = Siget.Config.Global.Idioma_Administrador;
             

            }
          }
        }
      }
      catch (Exception ex)
      {
        Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Obtiene información del usuario necesaria en su perfil
    /// </summary>
    /// <param name="login">nombre de usuario que ingresa</param>
    /// <returns>Boolean true si termina exitosamente, false si hay errores</returns>
    /// <remarks></remarks>
    public static bool Coordinador_IniciaSesion(string login)
    {
      try
      {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
        {
          conn.Open();

          using (SqlCommand cmd = new SqlCommand(
            "SELECT " + 
            "   C.IdCoordinador, " + 
            "   U.Idioma " + 
            "   U.Email, "+
            "   C.Email EmailSecundario " +
            "FROM " + 
            "   Coordinador C, " + 
            "   (SELECT " + 
            "       IdUsuario, " + 
            "       Idioma, " +
            "       Email , "+
            "   FROM Usuario " + 
            "   WHERE PerfilASP = 'coordinador' " + 
            "       AND Login = @Login) U " + 
            "WHERE " + 
            "   U.IdUsuario = C.IdUsuario", conn))
          {

            cmd.Parameters.AddWithValue("@Login", login);

            using (SqlDataReader results = cmd.ExecuteReader())
            {

              results.Read();

              HttpContext.Current.Session["Usuario_IdCoordinador"] = results["IdCoordinador"].ToString().Trim();
              HttpContext.Current.Session["Usuario_Idioma"] = Siget.Config.Global.Idioma_Coordinador;

                if(results["EmailSecundario"]!=DBNull.Value || results["EmailSecundario"].ToString().Trim()!=string.Empty)
                    HttpContext.Current.Session["Usuario_Email"] = results["EmailSecundario"].ToString().Trim();
                else if(results["Email"]!=DBNull.Value || results["Email"].ToString().Trim()!=string.Empty)
                    HttpContext.Current.Session["Usuario_Email"] = results["Email"].ToString().Trim();

             
            }
          }
        }
      }
      catch (Exception ex)
      {
        Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Obtiene información del usuario necesaria en su perfil
    /// </summary>
    /// <param name="login">nombre de usuario que ingresa</param>
    /// <returns>Boolean true si termina exitosamente, false si hay errores</returns>
    /// <remarks></remarks>
    public static bool Profesor_IniciaSesion(string login)
    {
      try
      {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
        {
          conn.Open();

          using (SqlCommand cmd = new SqlCommand(
            "SELECT " + 
            "   P.IdProfesor, " + 
            "   U.Idioma " + 
            "FROM " + 
            "   Profesor P, " + 
            "   (SELECT " + 
            "       IdUsuario, " + 
            "       Idioma " + 
            "   FROM Usuario " + 
            "   WHERE PerfilASP = 'profesor' " + 
            "       AND Login = @Login) U " + 
            "WHERE " + 
            "   U.IdUsuario = P.IdUsuario ", conn))
          {

            cmd.Parameters.AddWithValue("@Login", login);

            using (SqlDataReader results = cmd.ExecuteReader())
            {

              results.Read();

              HttpContext.Current.Session["Usuario_IdProfesor"] = results["IdProfesor"].ToString().Trim();
              HttpContext.Current.Session["Usuario_Idioma"] = Siget.Config.Global.Idioma_Profesor;
           
            }
          }
        }
      }
      catch (Exception ex)
      {
        Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
        return false;
      }

      return true;
    }

    /// <summary>
    /// Registra la información del inicio de sesión en la base de datos.
    /// </summary>
    /// <param name="username">Nombre de usuario que inició sesión</param>
    /// <param name="browser">Objecto HttpBrowserCapabilities obtenido de la petición de inicio de sesión</param>
    public static void RegistraInicioSesion(ref string username, ref System.Web.HttpBrowserCapabilities browser, ref string resolutionX, ref string resolutionY)
    {
      try
      {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
        {
          conn.Open();

          using (SqlCommand cmd = new SqlCommand("InicioSesion_Inserta", conn))
          {
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Login", username);
            cmd.Parameters.AddWithValue("@Plataforma", browser.Platform.ToString());
            cmd.Parameters.AddWithValue("@Navegador", browser.Browser.ToString());
            cmd.Parameters.AddWithValue("@Version", browser.Version.ToString());
            cmd.Parameters.AddWithValue("@ResolucionX", resolutionX);
            cmd.Parameters.AddWithValue("@ResolucionY", resolutionY);
            cmd.Parameters.AddWithValue("@JavaScript", browser["JavaScriptVersion"].ToString());
            cmd.Parameters.AddWithValue("@Cookies", browser.Cookies.ToString());

            cmd.ExecuteNonQuery();
          }
        }
      }
      catch (Exception ex)
      {
        Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
      }
    }

  }
}