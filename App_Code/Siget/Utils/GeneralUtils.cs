﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Siget.Utils
{

    public class GeneralUtils
    {
        public GeneralUtils()
        { }

        public static void CleanAll(ControlCollection collection)
        {

            foreach (Control c in collection)
            {

                if (c.HasControls())
                    CleanAll(c.Controls);
                else
                {
                    if (c is TextBox)
                        ((TextBox)c).Text = String.Empty;
                    else if (c is CheckBox)
                        ((CheckBox)c).Checked = false;
                    else if (c is HyperLink)
                        ((HyperLink)c).Text = String.Empty;
                  
                }

            }
        }

        public static void CleanAllWithLabels(ControlCollection collection)
        {

            foreach (Control c in collection)
            {

                if (c.HasControls())
                    CleanAllWithLabels(c.Controls);
                else
                {
                    if (c is TextBox)
                        ((TextBox)c).Text = String.Empty;
                    else if (c is CheckBox)
                        ((CheckBox)c).Checked = false;
                    else if (c is HyperLink)
                        ((HyperLink)c).Text = String.Empty;
                    else if (c is Label)
                        ((Label)c).Text = String.Empty;
                }

            }
        }
    }
}