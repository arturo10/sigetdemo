﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Xml;
using System.IO;

namespace Siget.Utils
{

  public class Bloqueo
  {
    public static bool alumnoBloqueado(string inst)
    {
      return perfilBloqueado(Siget.Config.Global.Perfil_Alumno, inst);
    }

    public static bool profesorBloqueado(string inst)
    {
      return perfilBloqueado(Siget.Config.Global.Perfil_Profesor, inst);
    }

    public static bool coordinadorBloqueado(string inst)
    {
      return perfilBloqueado(Siget.Config.Global.Perfil_Coordinador, inst);
    }

    public static bool perfilBloqueado(string perfil, string inst)
    {
      try
      {
        XmlTextReader datos = new XmlTextReader(Siget.Config.Global.rutaBloqueos);

        // obtengo el dia de semana actual
        string weekday = Siget.Utils.Bloqueo.DiaDeSemana((int)System.DateTime.Today.DayOfWeek);

        // busca bloqueos
        while (datos.ReadToFollowing("Bl"))
        {
          // si bloquea esta institucion
          datos.ReadToDescendant("Institucion");
          if (datos.ReadString().Equals(inst))
          {
            datos.ReadToFollowing("Dia");
            // si este nodo bloquea este dia de la semana
            if (datos.ReadString().Equals(weekday))
            {
              // si este nodo bloquea para este tipo de usuario
              datos.ReadToFollowing("Perfil");
              if (datos.ReadString().Equals(perfil))
              {
                string value = null;
                int horaActual = DateTime.Now.Hour;
                int minutoActual = DateTime.Now.Minute;

                // obtengo las horas de el bloqueo
                datos.ReadToFollowing("Inicio");
                value = datos.ReadString();
                int horaInicio = int.Parse(value.Split(':')[0]);
                int minutoInicio = int.Parse(value.Split(':')[1]);

                datos.ReadToFollowing("Fin");
                value = datos.ReadString();
                int horaFin = int.Parse(value.Split(':')[0]);
                int minutoFin = int.Parse(value.Split(':')[1]);

                // si la hora actual es mayor al inicio de este bloqueo,
                // y menor al fin de este bloqueo
                if (HoraMayorOIgualQue(horaActual, minutoActual, horaInicio, minutoInicio) && HoraMenorQue(horaActual, minutoActual, horaFin, minutoFin))
                {
                  // entonces esta bloqueado
                  datos.Close();
                  return true;
                }
              }
            }
          }

        }

        datos.Close();
      }
      catch (Exception ex)
      {
        // Si algo falla, permitir acceso por default
        return false;
      }

      // si no se encuentra bloqueo, permitir acceso
      return false;
    }

    public static bool sistemaBloqueado()
    {
      try
      {
        XmlTextReader datos = new XmlTextReader(Siget.Config.Global.rutaMantenimientos);

        // obtengo el dia de semana actual
        string weekday = Siget.Utils.Bloqueo.DiaDeSemana((int)System.DateTime.Today.DayOfWeek);

        // busca bloqueos
        while (datos.ReadToFollowing("Bl"))
        {
          datos.ReadToDescendant("Dia");
          // si este nodo bloquea este dia de la semana
          if (datos.ReadString().Equals(weekday))
          {
            string value = null;
            int horaActual = DateTime.Now.Hour;
            int minutoActual = DateTime.Now.Minute;

            // obtengo las horas de el bloqueo
            datos.ReadToFollowing("Inicio");
            value = datos.ReadString();
            int horaInicio = int.Parse(value.Split(':')[0]);
            int minutoInicio = int.Parse(value.Split(':')[1]);

            datos.ReadToFollowing("Fin");
            value = datos.ReadString();
            int horaFin = int.Parse(value.Split(':')[0]);
            int minutoFin = int.Parse(value.Split(':')[1]);

            // si la hora actual es mayor al inicio de este bloqueo,
            // y menor al fin de este bloqueo
            if (HoraMayorOIgualQue(horaActual, minutoActual, horaInicio, minutoInicio) && HoraMenorQue(horaActual, minutoActual, horaFin, minutoFin))
            {
              // entonces esta bloqueado
              datos.Close();
              return true;
            }
          }

        }

        datos.Close();
      }
      catch (Exception ex)
      {
        // Si algo falla, permitir acceso por default
        return false;
      }

      // si no se encuentra bloqueo, permitir acceso
      return false;
    }

    public static bool HoraMayorOIgualQue(int H1, int M1, int H2, int M2)
    {
      if ((H1 == H2 && M1 >= M2) | (H1 > H2))
      {
        return true;
      }

      return false;
    }

    public static bool HoraMenorQue(int H1, int M1, int H2, int M2)
    {
      if ((H1 == H2 && M1 <= M2) | (H1 < H2))
      {
        return true;
      }

      return false;
    }

    public static string DiaDeSemana(int input)
    {
      switch (input)
      {
        case 0:
          return "Domingo";
        case 1:
          return "Lunes";
        case 2:
          return "Martes";
        case 3:
          return "Miercoles";
        case 4:
          return "Jueves";
        case 5:
          return "Viernes";
        case 6:
          return "Sabado";
      }
      return "";
    }

  }

}