﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.IO;
using Siget.Entity;
using System.Data.SqlClient;

namespace Siget.Utils
{
    public static class Library
    {
        public static void recalculateStructure()
        {
            NodeRoot root = new NodeRoot("Root Node");
            AsignaturaRepository Asignaturarepository = new AsignaturaRepository();
            List<Asignatura> listAsignaturas = Asignaturarepository.List.Cast<Asignatura>().ToList();
     
            sigetdemo_sadcomeEntities dataContext=new sigetdemo_sadcomeEntities();
            string[] tipoArchivos = { "PDF", "ZIP", "Word", "Excel", "PP", "Imagen", "Video", "Audio", "Link", "FLV", "SWF", "YouTube" };
            List<ApoyoSubtema> apoyoPorTipo;
            List<NodeCalc> calcAsignatura = new List<NodeCalc>();
          

            string json = String.Empty;

            foreach (Asignatura asign in listAsignaturas)
            {
                //List<Calificacion> listCalificacion = (from r in dataContext.Calificacions where r.IdAsignatura == asign.IdAsignatura select r).ToList();
                NodeCalc temp = new NodeCalc(asign.Descripcion, asign.IdAsignatura);
                List<Tema> listTemas = (from r in dataContext.Temas where r.IdAsignatura == asign.IdAsignatura select r).ToList();
                List<ApoyoSubtema> listApoyoSubtema=new List<ApoyoSubtema>();


                listTemas.ForEach(delegate(Tema tema)
                {
                    List<Subtema> listSubtemas = (from r in dataContext.Subtemas where r.IdTema == tema.IdTema select r).ToList();

                    listSubtemas.ForEach(delegate(Subtema subtema)
                    {
                        List<ApoyoSubtema> apoyoSubtemaTemp = (from r in dataContext.ApoyoSubtemas where r.IdSubtema == subtema.IdSubtema select r).ToList();

                        apoyoSubtemaTemp.ForEach(delegate(ApoyoSubtema apoyo)
                        {

                            listApoyoSubtema.Add(apoyo);
                        });

                    });
                });


                foreach (string s in tipoArchivos)
                {
                    apoyoPorTipo = (from r in listApoyoSubtema where r.TipoArchivoApoyo == s select r).ToList();

                    if (apoyoPorTipo.Count() > 0)
                    {
                        NodeFolder folder = new NodeFolder(s);

                        apoyoPorTipo.ForEach(delegate(ApoyoSubtema apoyo)
                        {
                            NodeFile file = new NodeFile(apoyo.Descripcion);
                            if (apoyo.TipoArchivoApoyo != "Link" && apoyo.TipoArchivoApoyo != "YouTube")
                                file.data = HttpUtility.HtmlDecode(Siget.Config.Global.urlMaterial + apoyo.ArchivoApoyo);
                            else
                                file.data = HttpUtility.HtmlDecode(apoyo.ArchivoApoyo);

                            switch (apoyo.TipoArchivoApoyo)
                            {
                                case "Video":
                                    file.icon = "fa fa-file-video-o";
                                    break;
                                case "FLV":
                                    file.icon = "fa fa-file-video-o";
                                    break;
                                case "Word":
                                    file.icon = "fa fa-file-word-o";
                                    break;
                                case "PDF":
                                    file.icon = "fa fa-file-pdf-o";
                                    break;
                                case "ZIP":
                                    file.icon = "fa fa-file-archive-o";
                                    break;
                                case "Excel":
                                    file.icon = "fa fa-file-excel-o";
                                    break;
                                case "PP":
                                    file.icon = "fa fa-file-powerpoint-o";
                                    break;
                                case "Imagen":
                                    file.icon = "fa fa-file-image-o";
                                    break;
                                case "Audio":
                                    file.icon = "fa fa-file-audio-o";
                                    break;
                                case "Link":
                                    file.icon = "fa fa-link";
                                    break;
                                case "SWF":
                                    file.icon = "fa fa-film";
                                    break;
                                case "YouTube":
                                    file.icon = "fa fa-youtube";
                                    break;
                            }


                            folder.children.Add(file);
                        });

                        temp.children.Add(folder);
                    }
                }

                if (listTemas.Count() > 0)
                    calcAsignatura.Add(temp);
            }

                json = JsonConvert.SerializeObject(calcAsignatura, Formatting.Indented);
                File.Delete(Config.Global.rutaEstructuraBiblioteca);
                File.WriteAllText(Config.Global.rutaEstructuraBiblioteca, json);
            }
        }

    }


