﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Siget.Utils
{

  /// <summary>
  /// 
  /// </summary>
  public class Correo
  {

    /// <summary>
    /// Envía un mensaje desde la cuenta estándar de soporte.
    /// </summary>
    /// <param name="receptor"></param>
    /// <param name="asunto"></param>
    /// <param name="cuerpo"></param>
    /// <returns></returns>
    /// <remarks></remarks>
    public static bool EnviaCorreo(string receptor, string asunto, string cuerpo, bool esHtml)
    {
      System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage();
      correo.From = new System.Net.Mail.MailAddress("soporte@integrant.com.mx");
      correo.To.Add(receptor);
      correo.Subject = asunto;
      correo.Body = cuerpo;
      correo.IsBodyHtml = esHtml;
      correo.Priority = System.Net.Mail.MailPriority.High ;

      System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
      smtp.Host = "mail.salmarsistemas.com.mx";
      //smtp.Port = 25 no fue necesario indicarlo
      //smtp.EnableSsl = True SOLO SI EL SERVIDOR ADMITE CONEXIONES SEGURAS

      // Si el servidor de correo necesita autenticación, podemos hacerlo 
      // mediante un objeto del tipo NetworkCredential, en el que indicaremos 
      // el nombre del usuario y la clave, ese objeto lo asignaremos a la 
      // propiedad Credentials del objeto SmtpClient que acabamos de crear:l
      smtp.Credentials = new System.Net.NetworkCredential("ljmariscal", "integrant2009");
      //Le quité: @salmarsistemas.com.mx a ljmariscal
      //smtp.UseDefaultCredentials = False
      try
      {
        smtp.Send(correo);
      }
      catch (Exception ex)
      {
        Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
      }

      return false;
    }

  }

}