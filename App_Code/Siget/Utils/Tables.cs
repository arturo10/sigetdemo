﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Siget.Utils
{

  /// <summary>
  /// Contiene métodos de soporte relacionados con tablas de datos.
  /// </summary>
  public class Tables
  {

    /// <summary>
    /// Recieves a column name and a collection of columns to search in; 
    /// returns the index of the column whose header matches the referred parameter.
    /// </summary>
    /// <param name="gv">Gridview to search in</param>
    /// <param name="columnName">name of column to search in, matching the declaration in the gridview</param>
    /// <returns>the index of the column when found, -1 if not found</returns>
    /// <remarks></remarks>
    public static int GetColumnIndexByName(ref GridView gv, ref string columnName)
    {
      int columnIndex = 0;

      foreach (DataControlField cell in gv.Columns)
      {
        if (cell.HeaderText.Equals(columnName))
        {
          return columnIndex;
        }
        columnIndex = columnIndex + 1;
        // keep adding 1 while we don't have the correct name
      }

      return columnIndex;
    }

  }

}
