﻿using System;
using System.Xml;

namespace Siget.Utils
{

  public class LogManager
  {

    // 24-10-2013
    /// <summary>
    /// Almacena los datos de la excepción recibida en el archivo 
    /// de registro de excepciones de la aplicación.
    /// </summary>
    /// <param name="ex"></param>
    /// <remarks>
    /// El archivo de registro se reescribe con cada recepción, 
    /// por lo que se debe cuidar que no cresca demasiado si se 
    /// utiliza en producción (A partir de 50mb degrada desempeño).
    /// </remarks>
    public static void ExceptionLog_InsertEntry(Exception ex)
    {
      if (Config.Global.DEPLOYED)
      {
        XmlDocument xmldoc = new XmlDocument();
        xmldoc.Load(Config.Global.rutaLogExcepciones);

        //Select main node
        XmlNode newXMLNode = xmldoc.SelectSingleNode("/Log");
        //get the node where you want to insert the data
        XmlNode childNode = xmldoc.CreateNode(XmlNodeType.Element, "Exception", "");

        // Añado el subnodo de tiempo
        XmlNode newAttribute = xmldoc.CreateNode(XmlNodeType.Element, "Time", "");
        newAttribute.InnerText = DateTime.Now.ToString();
        childNode.AppendChild(newAttribute);

        // Añado él subnodo de mensaje
        newAttribute = xmldoc.CreateNode(XmlNodeType.Element, "Message", "");
        newAttribute.InnerText = ex.ToString();
        childNode.AppendChild(newAttribute);

        newXMLNode.PrependChild(childNode);
        // añade al inicio de la lista

        xmldoc.Save(Config.Global.rutaLogExcepciones);
        // reescribo la lista
      }
    }

  }

}