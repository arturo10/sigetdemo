﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Siget.Beans;

/// <summary>
/// Summary description for ICRUD
/// </summary>
public interface IRepository<T> 
{
    IEnumerable<T> List { get; }
    void Add(T entity);
    void Delete(T entity);
    void Update(T entity);
    T FindById(int Id);
}