﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for GridExtensions
/// </summary>
/// 
namespace Siget.Extensions
{
    public static class GridExtensions
    {
        public static int CountRowsSelectedByCheckbox(this GridView gr)
        {
            int counterRowsSelected = 0;
            foreach (GridViewRow row in gr.Rows)
            {
                CheckBox checboxSelection=row.Cells[0].Controls[1] as CheckBox;
                if (checboxSelection.Checked)
                {
                    counterRowsSelected++;
                }
            }
            return counterRowsSelected;
        }

        public static List<string> getRowsSelectedByCheckbox(this GridView gr)
        {
            List<string> listIndexes = new List<string>();

            foreach (GridViewRow row in gr.Rows)
            {
                CheckBox checboxSelection = row.Cells[0].Controls[1] as CheckBox;
                if (checboxSelection.Checked)
                {
                    listIndexes.Add(gr.DataKeys[row.RowIndex].Values["IdEvaluacion"].ToString());
                }
            }
            return listIndexes;

        }

    }
}