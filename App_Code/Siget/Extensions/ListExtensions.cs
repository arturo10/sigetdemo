﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using System.Data;

/// <summary>
/// Summary description for ListExtensions
/// </summary>
public static class ListExtensions
{


    public static DataSet ToDataSet<T>(this IList<T> list, string[] args = null)
    {
        Type elementType = typeof(T);
        DataSet ds = new DataSet();
        DataTable t = new DataTable();
        ds.Tables.Add(t);

        //add a column to table for each public property on T

        if (args == null)
        {
            foreach (var propInfo in elementType.GetProperties())
            {
                Type ColType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;

                t.Columns.Add(propInfo.Name, ColType);
            }
        }
        else
        {
            foreach (var propInfo in args)
            {
                var prop = elementType.GetProperty(propInfo);

                Type ColType = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;

                t.Columns.Add(prop.Name, ColType);
            }
        }
        //go through each property on T and add each value to the table
        foreach (T item in list)
        {
            DataRow row = t.NewRow();

            if (args == null)
            {
                foreach (var propInfo in elementType.GetProperties())
                {
                    row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                }
            }
            else
            {
                foreach (var propInfo in args)
                {
                    var prop = elementType.GetProperty(propInfo);
                    row[prop.Name] = prop.GetValue(item, null) ?? DBNull.Value;
                }
            }

            t.Rows.Add(row);

        }

        return ds;
    }
}