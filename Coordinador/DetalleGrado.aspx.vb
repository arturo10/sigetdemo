﻿Imports Siget


Partial Class coordinador_DetalleGrado
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        TitleLiteral.Text = "Avance Global de Actividades"

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.PLANTELES
        Label3.Text = Config.Etiqueta.COORDINADOR

        'Observar si no llega a dar problema el que esto se controle con variables de sesion, de ser
        'asi puedo grabar en campos ocultos los valores para que al seleccionar leerlos de nuevo y pasarlos al siguiente script
        Try
            'Para generar el contenido del GridView dinamicamente, es necesario que su propiedad AutoGenerateColumns = True 
            SDSdatos.ConnectionString = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            GVdatos.Caption = Session("Institucion").ToString + " - " + Session("Plantel").ToString + " <BR> " + Session("Nivel").ToString + " - " + Session("Grado").ToString
            If Session("IdPlantel") < 0 Then 'significa que eligio todos los planteles
                SDSdatos.SelectCommand = "select distinct E.IdEvaluacion, E.ClaveBateria + ' (' + E.ClaveAbreviada + ')' as 'Actividad', E.Tipo, CONVERT(varchar(10),E.InicioContestar,103) as Inicio_Contestar, " + _
                 "C.Descripcion + ' (' + C.Clave + ')' as 'Calificacion', A.Descripcion Asignatura " + _
                 "from Calificacion C, Evaluacion E, Asignatura A, Grado G, Escuela Es, Plantel P " + _
                 "where G.IdGrado = " + Session("IdGrado").ToString + _
                 " and Es.IdNivel = G.IdNivel and Es.IdPlantel = P.IdPlantel and " + _
                 "P.IdInstitucion = " + Session("IdInstitucion").ToString + " and " + _
                 "P.IdPlantel in (Select IdPlantel from CoordinadorPlantel where IdCoordinador = " + Session("IdCoordinador").ToString + ") " + _
                 "and A.IdGrado = G.IdGrado and C.IdAsignatura = A.IdAsignatura and " + _
                 "C.IdCicloEscolar = " + Session("IdCicloEscolar").ToString + _
                 " and e.IdCalificacion = C.IdCalificacion " + _
                 "order by A.Descripcion,Inicio_Contestar, Calificacion, E.Tipo, E.IdEvaluacion, Actividad"
                'Tuve que poner Inicio_Contestar porque no lo reconocia con su nombre real al estar en un convert
            Else
                'Filtro desde el Plantel porque puede haber un plantel que no maneje cierto grado
                SDSdatos.SelectCommand = "select E.IdEvaluacion, E.ClaveBateria + ' (' + E.ClaveAbreviada + ')' as 'Actividad', E.Tipo,  CONVERT(varchar(10),E.InicioContestar,103) as 'Inicio contestar', " + _
                        "C.Descripcion + ' (' + C.Clave + ')' as 'Calificación', A.Descripcion Asignatura " + _
                        "from Calificacion C, Evaluacion E, Asignatura A, Grado G, Escuela Es " + _
                        "where G.IdGrado = " + Session("IdGrado").ToString + _
                        " and Es.IdNivel = G.IdNivel and Es.IdPlantel = " + Session("IdPlantel").ToString + _
                        " and A.IdGrado = G.IdGrado and C.IdAsignatura = A.IdAsignatura and " + _
                        "C.IdCicloEscolar = " + Session("IdCicloEscolar").ToString + _
                        " and e.IdCalificacion = C.IdCalificacion order by A.Descripcion,C.Consecutivo, E.InicioContestar, Actividad"
            End If
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVdatos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVdatos.SelectedIndexChanged
        'Variables que vienen desde AvancePlantel.aspx:
        'Session("IdCicloEscolar") = DDLcicloescolar.SelectedValue
        'Session("IdInstitucion") = DDLinstitucion.SelectedValue
        'Session("IdPlantel") = DDLplantel.SelectedValue
        'Session("IdNivel") = GVdatos.SelectedRow.Cells(1).Text
        'Session("IdGrado") = GVdatos.SelectedRow.Cells(3).Text
        'Session("Institucion") = DDLinstitucion.SelectedItem
        'Session("Nivel") = GVdatos.SelectedRow.Cells(2).Text
        'Session("Grado") = GVdatos.SelectedRow.Cells(4).Text
        'Session("Plantel") = DDLplantel.SelectedItem
        Session("IdEvaluacion") = GVdatos.SelectedDataKey.Values(0).ToString()
        Session("Evaluacion") = GVdatos.SelectedRow.Cells(2).Text
        Response.Redirect("ResumenEvaluaciones.aspx")
    End Sub

    Protected Sub GVdatos_DataBound(sender As Object, e As EventArgs) Handles GVdatos.DataBound
        If GVdatos.Rows.Count > 0 Then
            msgInfo.hide()
            msgError.hide()
        Else
            msgInfo.show("No hay actividades asignadas en ese " & Config.Etiqueta.CICLO & " para el " & Config.Etiqueta.PLANTEL & " o " & Config.Etiqueta.PLANTELES & ".")
        End If
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVdatos_PreRender(sender As Object, e As EventArgs) Handles GVdatos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVdatos.Rows.Count > 0 Then
            GVdatos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVdatos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If GVdatos.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub GVdatos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVdatos, "Select$" & e.Row.RowIndex).ToString())
        End If

        e.Row.Cells(1).Visible = False

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(6).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVdatos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVdatos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

End Class
