﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="DetalleGrado.aspx.vb" 
    Inherits="coordinador_DetalleGrado" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            width: 100%;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 9px;
        }

        .style24 {
            height: 40px;
        }

        .style25 {
            font-size: small;
            font-weight: normal;
        }

        .auto-style1 {
            height: 23px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Avance - Global de Actividades
    </h1>
    Presenta un resumen de la cantidad global de 
		<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    que 
        pertenecen a los 
		<asp:Label ID="Label2" runat="server" Text="[PLANTELES]" />
    que tiene asignados como 
		<asp:Label ID="Label3" runat="server" Text="[COORDINADORES]" />
    que han 
        realizado una actividad específica.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style11">
        <tr>
            <td class="style24"></td>
            <td class="style24" style="text-align: left;">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/coordinador/AvancePlantel.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
            <td class="style24"></td>
            <td class="style24"></td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GVdatos" runat="server"
                    AllowSorting="True" 

                    DataSourceID="SDSdatos" 
                    DataKeyNames="IdEvaluacion" 
                    Width="879px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                    <%-- sort YES - rowdatabound
                        0  select
	                    1  E.IdEvaluacion
	                    2  E.ClaveBateria + ' (' + E.ClaveAbreviada + ')' as 'Actividad'
                        3  tipo
	                    4  CONVERT(varchar(10),E.InicioContestar,103) as Inicio_Contestar
	                    5  C.Descripcion + ' (' + C.Clave + ')' as 'Calificacion'
	                    6  A.Descripcion ASIGNATURA
                    --%>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="text-align: left;">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/coordinador/AvancePlantel.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSdatos" runat="server"></asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

