﻿Imports Siget

Partial Class superadministrador_EditaMensajes
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        TitleLiteral.Text = "Avisos Generales"

    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            SDSMensajes.InsertCommand = "SET dateformat dmy; INSERT INTO Mensaje(Contenido,FechaInicia,FechaTermina) VALUES ('" + TBcontenido.Text + "', '" &
                CalInicia.SelectedDate.ToShortDateString + "','" + CalTermina.SelectedDate.ToShortDateString + " 23:59:00')"
            SDSMensajes.Insert()
            msgSuccess.show("Éxito", "El registro ha sido guardado.")
            GVmensajes.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            SDSMensajes.DeleteCommand = "Delete from Mensaje WHERE IdMensaje = " + GVmensajes.SelectedRow.Cells(1).Text
            SDSMensajes.Delete()
            GVmensajes.DataBind()
            TBcontenido.Text = ""
            msgSuccess.show("Éxito", "El registro ha sido eliminado.")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            SDSMensajes.UpdateCommand = "SET dateformat dmy; UPDATE Mensaje SET Contenido = '" + TBcontenido.Text + "', FechaInicia='" + CalInicia.SelectedDate.ToShortDateString + "', FechaTermina='" + CalTermina.SelectedDate.ToShortDateString + " 23:59:00' WHERE IdMensaje = " + GVmensajes.SelectedRow.Cells(1).Text
            SDSMensajes.Update()
            GVmensajes.DataBind()
            msgSuccess.show("Éxito", "El registro ha sido actualizado.")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVmensajes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmensajes.SelectedIndexChanged
        msgError.hide()
        msgSuccess.hide()

        Try
            TBcontenido.Text = HttpUtility.HtmlDecode(GVmensajes.SelectedRow.Cells(2).Text)
            CalInicia.SelectedDate = CDate(GVmensajes.SelectedRow.Cells(4).Text)
            CalTermina.SelectedDate = CDate(GVmensajes.SelectedRow.Cells(5).Text)
        Catch ex As Exception
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVmensajes_PreRender(sender As Object, e As EventArgs) Handles GVmensajes.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVmensajes.Rows.Count > 0 Then
            GVmensajes.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVmensajes_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVmensajes.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If GVmensajes.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub GVmensajes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVmensajes.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVmensajes, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVmensajes.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVmensajes, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

End Class
