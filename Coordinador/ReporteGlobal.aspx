﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="ReporteGlobal.aspx.vb"
    Inherits="coordinador_ReporteGlobal"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            height: 37px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style23 {
            text-align: left;
            height: 43px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000099;
        }

        .style24 {
            width: 275px;
            height: 43px;
        }

        .style25 {
            height: 43px;
        }

        .style26 {
            text-align: right;
            height: 18px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 173px;
        }

        .style20 {
            height: 18px;
        }

        .style18 {
            height: 18px;
        }

        .style42 {
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: center;
        }

        .style33 {
            width: 49px;
        }

        .style41 {
            width: 123px;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: center;
        }

        .style40 {
            width: 665px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
        }

        .style36 {
            text-align: left;
            font-size: x-small;
            font-weight: bold;
            color: #000099;
            height: 33px;
        }

        .style37 {
            text-align: left;
            font-size: x-small;
            color: #000099;
            height: 33px;
        }

        .style14 {
            text-align: left;
        }

        .style38 {
            text-align: left;
            font-size: x-small;
            color: #000099;
        }

        .style43 {
            text-align: left;
            width: 173px;
        }

        .style28 {
            text-align: left;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style29 {
            text-align: right;
            height: 43px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 173px;
        }

        .style39 {
            text-align: left;
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
            color: #000099;
        }

        .style13 {
            width: 275px;
        }

        .style44 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            color: #FFFFFF;
            height: 8px;
            width: 275px;
        }

        .style45 {
            text-align: left;
            width: 275px;
        }

        .style46 {
            text-align: right;
            height: 18px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 173px;
        }

        .style48 {
            text-align: right;
            height: 18px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 173px;
            font-weight: normal;
        }

        .style32 {
            height: 22px;
            width: 479px;
            font-weight: normal;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Resultados - Global
    </h1>
    Presenta un promedio global por área de conocimiento (a la 
                    que pertenecen las 
												<asp:Label ID="Label1" runat="server" Text="[ASIGNATURAS]" />
    ) de todos los 
												<asp:Label ID="Label2" runat="server" Text="[PLANTELES]" />
    de la 
												<asp:Label ID="Label3" runat="server" Text="[INSTITUCION]" />
    que Ud. tiene asignados, incluyendo todos los 
												<asp:Label ID="Label4" runat="server" Text="[GRADOS]" />
    .
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style23" colspan="2">Indique los siguientes datos 
                para generar el reporte:</td>
            <td class="style24"></td>
            <td class="style25" colspan="2"></td>
        </tr>
        <tr>
            <td class="style48">
                <asp:Label ID="Label5" runat="server" Text="[INSTITUCION]" />
            </td>
            <td class="style20">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    Height="22px" Width="270px" DataSourceID="SDSinstituciones"
                    DataTextField="Descripcion" DataValueField="IdInstitucion">
                </asp:DropDownList>
            </td>
            <td class="style44">&nbsp;</td>
            <td class="style18" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style48">
                <asp:Label ID="Label6" runat="server" Text="[CICLO]" />
            </td>
            <td class="style20">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    Height="22px" 
                    Width="270px" 
                    DataSourceID="SDSciclosescolares"
                    DataTextField="duracion" 
                    DataValueField="IdCicloEscolar"
                    onchange="setSpinner(1)">
                </asp:DropDownList>
            </td>
            <td class="style44">&nbsp;</td>
            <td class="style18" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style46">
                <asp:Label ID="Label7" runat="server" Text="[NIVEL]" />
            </td>
            <td class="style20">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    Height="22px" 
                    Width="270px" 
                    DataSourceID="SDSniveles"
                    DataTextField="Descripcion" 
                    DataValueField="IdNivel"
                    onchange="setSpinner(1)">
                </asp:DropDownList>
            </td>
            <td class="style44">&nbsp;</td>
            <td class="style18" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style26">&nbsp;</td>
            <td class="style20">&nbsp;</td>
            <td class="style44">&nbsp;</td>
            <td class="style18" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style26">&nbsp;</td>
            <td class="style20" colspan="2">
                <table align="center" class="style10" border="1" bordercolor="black">
                    <tr>
                        <td class="style42" colspan="3">DESCRIPCIÓN DE LA ESCALA</td>
                    </tr>
                    <tr>
                        <td bgcolor="LightBlue" class="style33">&nbsp;</td>
                        <td class="style41">Excelente</td>
                        <td class="style40">Supera las expectativas</td>
                    </tr>
                    <tr>
                        <td bgcolor="LightGreen" class="style33">&nbsp;</td>
                        <td class="style41">Bien</td>
                        <td class="style40">De acuerdo a las expectativas</td>
                    </tr>
                    <tr>
                        <td bgcolor="Yellow" class="style33">&nbsp;</td>
                        <td class="style41">Regular</td>
                        <td class="style40">Alerta, las expectativas no están siendo alcanzadas</td>
                    </tr>
                    <tr>
                        <td bgcolor="LightSalmon" class="style33">&nbsp;</td>
                        <td class="style41">Mal</td>
                        <td class="style40">Las expectativas están muy por debajo de lo esperado</td>
                    </tr>
                    <tr>
                        <td class="style33">&nbsp;</td>
                        <td class="style41">Sin valor</td>
                        <td class="style40">No hay resultados capturados</td>
                    </tr>
                </table>
            </td>
            <td class="style18">&nbsp;</td>
            <td class="style18">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td colspan="5" style="text-align: left">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <div id="spin1" style="display:none;" class="spinnerLoading">
                    <div style="padding-top: 80px; text-align: center;">
                        Trabajando...
                    </div>
			    </div>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="5">
                <asp:GridView ID="GVreporte" runat="server" 
                    AllowSorting="True" 

                    DataSourceID="SDSsp_promedioglobal"
                    Width="877px"

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  P.Descripcion AS PLANTEL
	                --%>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style38" colspan="5">
                <b>NOTA IMPORTANTE:</b> Cada actividad representa un porcentaje de la 
                Calificación, por lo tanto, el porcentaje se irá acumulando hasta llegar al 100% 
                conforme se vaya completando la cantidad de tareas que componen cada 
                calificación.</td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="5">
                <asp:Button ID="BtnExportar1" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style28" colspan="5">Si desea detallar un 
								<asp:Label ID="Label8" runat="server" Text="[GRADO]" />
                en particular del reporte superior, selecciónelo de 
                la siguiente lista:</td>
        </tr>
        <tr>
            <td class="style29">
                <asp:Label ID="Label9" runat="server" Text="[GRADO]" />
            </td>
            <td class="style25">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" 
                    DataTextField="Descripcion" 
                    DataValueField="IdGrado"
                    Height="22px" 
                    Width="271px"
                    onchange="setSpinner(2)">
                </asp:DropDownList>
            </td>
            <td class="style24">
                &nbsp;</td>
            <td class="style25" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="5">
                <div id="spin2" style="display:none;" class="spinnerLoading">
                    <div style="padding-top: 80px; text-align: center;">
                        Trabajando...
                    </div>
			    </div>
            </td>
        </tr>
        <tr>
            <td class="style22" colspan="5">
                <asp:GridView ID="GVreportegrados" runat="server" 
                    AllowSorting="True" 

                    DataSourceID="SDSsp_promedioglobalporgrado" 
                    Width="879px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="style39" colspan="5">
                <b>NOTA IMPORTANTE:</b> Cada actividad representa un porcentaje de la 
                Calificación, por lo tanto, el porcentaje se irá acumulando hasta llegar al 100% 
                conforme se vaya completando la cantidad de tareas que componen cada 
                calificación.</td>
        </tr>
        <tr>
            <td class="style43">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
            <td>&nbsp;</td>
            <td class="style13">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style43">&nbsp;</td>
            <td>&nbsp;</td>
            <td class="style13">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style43">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td>&nbsp;</td>
            <td class="style13">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT distinct I.IdInstitucion, I.Descripcion 
FROM Institucion I, CoordinadorPlantel CP, Plantel P
WHERE I.Estatus = 'Activo' and I.IdInstitucion = P.IdInstitucion
and P.IdPlantel = CP.IdPlantel and 
CP.IdCoordinador = (select C.IdCoordinador from Coordinador C, Usuario U where
U.Login = @Login and C.IdUsuario = U.IdUsuario)
ORDER BY I.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Distinct N.IdNivel, N.Descripcion
from Nivel N, Plantel P, Escuela E
where P.IdInstitucion = @IdInstitucion and
E.IdPlantel = P.IdPlantel and N.IdNivel = E.IdNivel and (N.Estatus = 'Activo')">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdCicloEscolar, Descripcion as  duracion
from CicloEscolar
where Estatus = 'Activo'
order by Descripcion"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSsp_promedioglobal" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SP_promedioglobal" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" DefaultValue="" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" DefaultValue="" />
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" DefaultValue="" />
                        <asp:SessionParameter Name="IdCoordinador" SessionField="IdCoordinador"
                            Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSsp_promedioglobalporgrado" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SP_promedioglobalporgrado" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:SessionParameter Name="IdCoordinador" SessionField="IdCoordinador"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE ([IdNivel] = @IdNivel) and (Estatus = 'Activo') order by IdGrado">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

