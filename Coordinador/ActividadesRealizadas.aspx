﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="ActividadesRealizadas.aspx.vb" 
    Inherits="coordinador_ActividadesRealizadas" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 35px;
        }

        .style13 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
        }

        .style15 {
            font-family: Arial, Helvetica, sans-serif;
        }

        .style36 {
            font-weight: normal;
            font-size: small;
        }

        .style37 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 23px;
        }
        .estandar {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Actividades Realizadas
    </h1>
    Muestra a los 
	<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
     de un 
	<asp:Label ID="Label2" runat="server" Text="[GRUPO]" />
     en una tabla con los totales de actividades realizadas.  
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label4" runat="server" Text="[INSTITUCION]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    Height="22px" Width="350px" DataSourceID="SDSinstituciones"
                    DataTextField="Descripcion" DataValueField="IdInstitucion">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label5" runat="server" Text="[CICLO]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px"
                    Width="350px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label6" runat="server" Text="[PLANTEL]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"  
                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                    DataValueField="IdPlantel" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label7" runat="server" Text="[NIVEL]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Height="22px" Width="270px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label8" runat="server" Text="[GRADO]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                    Height="22px" Width="270px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label9" runat="server" Text="[GRUPO]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrupos" DataTextField="Descripcion" DataValueField="IdGrupo"
                    Width="270px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                Tipo de Actividad
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLtipo" runat="server" Width="105px" AutoPostBack="true"
                    CssClass="style41">
                    <asp:ListItem Value="Todos" Selected="true">Todos</asp:ListItem>
                    <asp:ListItem Value="Aprendizaje">Aprendizaje</asp:ListItem>
                    <asp:ListItem Value="Evaluación">Evaluación</asp:ListItem>
                    <asp:ListItem Value="Material">Material</asp:ListItem>
                    <asp:ListItem Value="Reactivos">Reactivos</asp:ListItem>
                    <asp:ListItem Value="Documento">Documento</asp:ListItem>
                    <asp:ListItem Value="Examen">Examen</asp:ListItem>
                    <asp:ListItem Value="Tarea">Tarea</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">&nbsp;</td>
            <td class="estandar" colspan="2">
                <asp:CheckBox ID="CBfechas" runat="server" AutoPostBack="True"
                    Text="Incluir un rango de fecha de realización"
                    Style="color: #000099" />
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">&nbsp;</td>
            <td class="estandar" colspan="2">
                <asp:Panel ID="PnlFechas" runat="server" Visible="False">
                    <table class="style10">
                        <tr>
                            <td class="style24">
                                <span class="style25">Del</span>:</td>
                            <td>
                                <asp:TextBox ID="TBde" runat="server" AutoPostBack="true"></asp:TextBox>
                                <asp:CalendarExtender ID="TBde_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="TBde">
                                </asp:CalendarExtender>
                            </td>
                            <td class="style24">
                                <span class="style25">Al</span>:</td>
                            <td>
                                <asp:TextBox ID="TBal" runat="server" AutoPostBack="true"></asp:TextBox>
                                <asp:CalendarExtender ID="TBal_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="TBal">
                                </asp:CalendarExtender>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                Incluir <asp:Label ID="Label3" runat="server" Text="Alumno"></asp:Label>
            </td>
            <td class="estandar" colspan="2">
                <asp:RadioButtonList ID="RBestatus" runat="server" AutoPostBack="True"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Selected="true">Todos</asp:ListItem>
                    <asp:ListItem Value="Activos">Solo Activos</asp:ListItem>
                    <asp:ListItem Value="Inactivos">Solo Inactivos</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
            </td>
            <td class="estandar" colspan="2">
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td>
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style15" colspan="6">
                <asp:GridView ID="GValumnos" runat="server" 
                    AllowSorting="True" 
                    AutoGenerateColumns="false"
                    ShowFooter="true"

                    DataSourceID="SDSactividades" 
                    DataKeyNames="IdAlumno, Equipo"
                    Width="880px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="true" SelectText="Ver&nbsp;Reporte&nbsp;Inividual" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="NombreAlumno" HeaderText="Nombre del Alumno" SortExpression="NombreAlumno"/>
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" SortExpression="Estatus"/>
                        <asp:BoundField DataField="Login" HeaderText="Login" SortExpression="Login"/>
                        <asp:BoundField DataField="Actividades Realizadas" HeaderText="Actividades Realizadas" SortExpression="Actividades Realizadas"/>
                        <asp:BoundField DataField="Actividades Cumplidas" HeaderText="Actividades con Meta Cumplida" SortExpression="Actividades Cumplidas"/>
                        <asp:BoundField DataField="% Cumplimiento" HeaderText="% Actividades con Meta Cumplida" SortExpression="% Cumplimiento"/>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
                    'select A.,A. as "",A. as "",A.,A.,A.'
	                0  Matricula
	                1  Apellido Paterno
	                2  Apellido Materno
	                3  Nombre
	                4  EQUIPO
	                5  Estatus
	                6  ... dinamicas
	                --%>
            </td>
        </tr>
        <tr>
            <td class="style15">
                &nbsp;</td>
            <td colspan="4">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style15">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td colspan="4">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT distinct I.IdInstitucion, I.Descripcion 
FROM Institucion I, CoordinadorPlantel CP, Plantel P
WHERE I.Estatus = 'Activo' and I.IdInstitucion = P.IdInstitucion
and P.IdPlantel = CP.IdPlantel and 
CP.IdCoordinador = (select C.IdCoordinador from Coordinador C, Usuario U where
U.Login = @Login and C.IdUsuario = U.IdUsuario)
ORDER BY I.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.IdPlantel, P.Descripcion
from Plantel P, CoordinadorPlantel C
where C.IdCoordinador in (Select IdCoordinador from Coordinador, Usuario
where Coordinador.IdUsuario = Usuario.IdUsuario and Login = @Login)
and P.IdPlantel = C.IdPlantel and P.IdInstitucion = @IdInstitucion 
order by P.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE (([IdNivel] = @IdNivel) AND ([Estatus] = @Estatus)) ORDER BY [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrupo], [Descripcion] FROM [Grupo] WHERE (([IdGrado] = @IdGrado) AND ([IdPlantel] = @IdPlantel)) and ([IdCicloEscolar] = @IdCicloEscolar)
ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>
                <asp:SqlDataSource ID="SDSactividades" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="sp_actividadesrealizadas"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLPlantel" Name="IdPlantel" Type="Int32" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="RBestatus" Name="Estatus"
                            PropertyName="SelectedValue" Type="String" />
                        <asp:SessionParameter Name="De" SessionField="De" Type="String" />
                        <asp:SessionParameter Name="Al" SessionField="Al" Type="String" />
                        <asp:ControlParameter ControlID="DDLtipo" Name="Tipo"
                            PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>
                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Distinct N.IdNivel, N.Descripcion
from Nivel N, Escuela E
where E.IdPlantel = @IdPlantel
and N.IdNivel = E.IdNivel and (N.Estatus = 'Activo')
order by N.IdNivel">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

