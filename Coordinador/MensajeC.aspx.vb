﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO

Partial Class coordinador_MensajeC
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        If Session("Envio_Previous").Equals("LeerMensajeSalidaC") Then
            HyperLink2.NavigateUrl = "~/coordinador/LeerMensajeSalidaC.aspx"
            LBresponder.Visible = False
            LBresponderA.Visible = False
        Else
            HyperLink2.NavigateUrl = "~/coordinador/LeerMensajeEntradaC.aspx"
        End If

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            aplicaLenguaje()

            initPageData()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = "Mensaje"

        If Session("Envio_Previous").Equals("LeerMensajeSalidaC") Then
            lt_hintProfesor.Text = Config.Etiqueta.PROFESOR & " Destinatario"
            lt_hintAlumno.Text = Config.Etiqueta.ALUMNO & " Destinatario"

            If Session("Envio_IdComunicacionCP") <> "0" Then
                lt_titulo.Text = "Mensaje Enviado a " & Config.Etiqueta.PROFESOR
            ElseIf Session("Envio_IdComunicacionCA") <> "0" Then
                lt_titulo.Text = "Mensaje Enviado a " & Config.Etiqueta.ALUMNO
            End If
        Else
            lt_hintProfesor.Text = Config.Etiqueta.PROFESOR & " que Envía"
            lt_hintAlumno.Text = Config.Etiqueta.ALUMNO & " que Envía"

            If Session("Envio_IdComunicacionCP") <> "0" Then
                lt_titulo.Text = "Mensaje Recibido de " & Config.Etiqueta.PROFESOR
            ElseIf Session("Envio_IdComunicacionCA") <> "0" Then
                lt_titulo.Text = "Mensaje Recibido de " & Config.Etiqueta.ALUMNO
            End If
        End If
        lt_hintPlantelP.Text = Config.Etiqueta.PLANTEL
        lt_hintPlantelA.Text = Config.Etiqueta.PLANTEL
    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub initPageData()
        Try
            If Session("Envio_IdComunicacionCP") <> "0" Then ' MENSAJE DE PROFESOR
                pnlMensajeAlumno.Visible = False ' oculto el panel no necesitado

                'Hago consulta para sacar los datos del mensaje
                Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                    conn.Open()

                    Dim cmd As SqlCommand = New SqlCommand("select C.IdComunicacionCP, C.IdCoordinador, C.Fecha, C.Asunto, C.IdProfesor, U.Login, P.Nombre + ' ' + P.Apellidos as 'Profesor', P.Email," + _
                        " C.Contenido, C.Fecha, C.Estatus, C.ArchivoAdjunto, C.ArchivoAdjuntoFisico, C.CarpetaAdjunto from ComunicacionCP C, Profesor P, Usuario U " + _
                        "where C.IdComunicacionCP = @IdComunicacion and P.IdProfesor = C.IdProfesor AND P.IdUsuario = U.IdUsuario", conn)
                    cmd.Parameters.AddWithValue("@IdComunicacion", Session("Envio_IdComunicacionCP"))

                    Dim results As SqlDataReader = cmd.ExecuteReader()
                    results.Read()
                    ' obtengo el contenido del mensaje
                    ltEnvia.Text = HttpUtility.HtmlDecode(results.Item("Profesor"))
                    ltPlantelP.Text = Session("Envio_Plantel") 'Viene de LeerMensajeC.aspx
                    ltFechaEnvio.Text = HttpUtility.HtmlDecode(results.Item("Fecha"))
                    If Not IsDBNull(results.Item("Asunto")) Then
                        ltAsunto.Text = HttpUtility.HtmlDecode(results.Item("Asunto"))
                    End If
                    ltContenido.Text = HttpUtility.HtmlDecode(results.Item("Contenido"))
                    If Not IsDBNull(results.Item("ArchivoAdjunto")) Then
                        lbAdjunto.Visible = True
                        lt_hintAdjunto.Visible = True
                        'Verifico si ya existe el directorio del alumno, si no, lo creo
                        lbAdjunto.Text = results.Item("ArchivoAdjunto")
                        hfAdjuntoFisico.Value = results.Item("ArchivoAdjuntoFisico")
                        hfCarpetaAdjunto.Value = results.Item("CarpetaAdjunto")
                    Else
                        lbAdjunto.Visible = False
                        lt_hintAdjunto.Visible = False
                    End If

                    'Los siguientes valores los utilizaré si le dan responder al mensaje
                    hfIdCoordinador.Value = results.Item("IdCoordinador")
                    hfIdProfesor.Value = results.Item("IdProfesor")
                    If IsDBNull(results.Item("Email")) Then 'Si intento asignar un valor Nulo a una variable, marca error
                        hfEmail.Value = ""
                    Else
                        hfEmail.Value = results.Item("Email")
                    End If

                    ' este lo necesito por si le dan reenviar
                    Session("Envio_Login") = results.Item("Login")

                    'Modificar el mensaje a "Leido" en caso de que este como "Nuevo"
                    If results.Item("Estatus") <> "Leido" AndAlso Not Session("Envio_Previous").Equals("LeerMensajeSalidaC") Then
                        SDSmensaje.UpdateCommand = "SET dateformat dmy; UPDATE ComunicacionCP set Estatus = 'Leido' where IdComunicacionCP = @IdComunicacionCP"
                        SDSmensaje.UpdateParameters.Add("IdComunicacionCP", Session("Envio_IdComunicacionCP"))
                        SDSmensaje.Update()
                    End If

                    results.Close()
                    cmd.Dispose()
                    conn.Close()
                End Using
            ElseIf Session("Envio_IdComunicacionCA") <> "0" Then ' MENSAJE PARA COORDINADOR
                pnlMensajeProfesor.Visible = False ' oculto el panel no necesitado

                'Hago consulta para sacar los datos del mensaje
                Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                    conn.Open()

                    Dim cmd As SqlCommand = New SqlCommand("select CA.Fecha, CA.Asunto, CA.IdCoordinador, CA.IdAlumno, U.Login, A.Nombre + ' ' + A.ApeMaterno + ' ' + A.ApePaterno as 'Alumno', A.Email, P.Descripcion as Plantel, CA.Contenido, CA.Estatus,CA.ArchivoAdjunto, CA.ArchivoAdjuntoFisico, CA.CarpetaAdjunto " &
                        "FROM ComunicacionCA CA, Coordinador C, Plantel P, Alumno A, Usuario U " &
                        "WHERE U.IdUsuario = A.IdUsuario AND CA.IdComunicacionCA = @IdComunicacionCA AND CA.IdCoordinador = C.IdCoordinador AND CA.IdAlumno = A.IdAlumno AND A.IdPlantel = P.IdPlantel", conn)
                    cmd.Parameters.AddWithValue("@IdComunicacionCA", Session("Envio_IdComunicacionCA"))

                    Dim results As SqlDataReader = cmd.ExecuteReader()
                    results.Read()
                    ' obtengo el contenido del mensaje
                    ltEnviaA.Text = HttpUtility.HtmlDecode(results.Item("Alumno"))
                    ltPlantelA.Text = Session("Envio_Plantel") 'Viene de LeerMensajeC.aspx
                    ltFechaEnvioA.Text = HttpUtility.HtmlDecode(results.Item("Fecha"))
                    If Not IsDBNull(results.Item("Asunto")) Then
                        ltAsuntoA.Text = HttpUtility.HtmlDecode(results.Item("Asunto"))
                    End If
                    ltContenidoA.Text = HttpUtility.HtmlDecode(results.Item("Contenido"))
                    If Not IsDBNull(results.Item("ArchivoAdjunto")) Then
                        lbAdjuntoA.Visible = True
                        lt_hintAdjuntoA.Visible = True
                        lbAdjuntoA.Text = results.Item("ArchivoAdjunto")
                        hfAdjuntoFisico.Value = results.Item("ArchivoAdjuntoFisico")
                        hfCarpetaAdjunto.Value = results.Item("CarpetaAdjunto")
                    Else
                        lbAdjuntoA.Visible = False
                        lt_hintAdjuntoA.Visible = False
                    End If

                    'Los siguientes valores los utilizaré si le dan responder al mensaje
                    hfIdAlumno.Value = results.Item("IdAlumno")
                    hfIdCoordinador.Value = results.Item("IdCoordinador")
                    If IsDBNull(results.Item("Email")) Then 'Si intento asignar un valor Nulo a una variable, marca error
                        hfEmail.Value = ""
                    Else
                        hfEmail.Value = results.Item("Email")
                    End If

                    ' este lo necesito por si le dan reenviar
                    Session("Envio_Login") = results.Item("Login")

                    'Modificar el mensaje a "Leido" en caso de que este como "Nuevo"
                    If results.Item("Estatus") <> "Leido" AndAlso Not Session("Envio_Previous").Equals("LeerMensajeSalidaC") Then
                        SDSmensaje.UpdateCommand = "SET dateformat dmy; UPDATE ComunicacionCA set Estatus = 'Leido' where IdComunicacionCA = @IdComunicacionCA"
                        SDSmensaje.UpdateParameters.Add("IdComunicacionCA", Session("Envio_IdComunicacionCA"))
                        SDSmensaje.Update()
                    End If

                    results.Close()
                    cmd.Dispose()
                    conn.Close()
                End Using

            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

#End Region

#Region "eliminar"

    Protected Sub LBeliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBeliminar.Click
        Dim eliminado As Boolean = False

        Try
            If CBeliminar.Checked Then
                'Para eliminarlo solo es necesario darlo de baja, y si lo está desplegando es que no está dado de baja
                SDSmensaje.UpdateCommand = "SET dateformat dmy; UPDATE ComunicacionCP set Estatus = 'Baja' where IdComunicacionCP = @IdComunicacionCP"
                SDSmensaje.UpdateParameters.Add("IdComunicacionCP", Session("Envio_IdComunicacionCP"))
                SDSmensaje.Update()

                'SIEMPRE NO LO ELIMINO PORQUE EL MISMO ARCHIVO LO PUEDEN ESTAR RECIBIENDO VARIOS ALUMNOS:
                'Elimino el archivo adjunto para no acumular basura
                'If Trim(lbAdjunto.Text).Length > 0 Then
                'Dim ruta As String
        'ruta = Config.Global.rutaMaterial
        'ruta = ruta + "adjuntos\" + Session("LoginProfesor") + "\" 'Esta ruta contempla un directorio como alumno nombrado como su login
        'Dim MiArchivo As FileInfo = New FileInfo(ruta & lbAdjunto.Text)
        'MiArchivo.Delete()
        'End If

        eliminado = True
      Else
        msgError.show("Debe marcar el mensaje para poder eliminarlo.")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try

    If eliminado Then
      If Session("Envio_Previous").Equals("LeerMensajeSalidaC") Then
        Response.Redirect("LeerMensajeSalidaC.aspx")
      Else
        Response.Redirect("LeerMensajeEntradaC.aspx")
      End If
    End If
  End Sub

  Protected Sub LBeliminarA_Click(sender As Object, e As EventArgs) Handles LBeliminarA.Click
    Dim eliminado As Boolean = False

    Try
      If CBeliminarA.Checked Then
        'Para eliminarlo solo es necesario darlo de baja, y si lo está desplegando es que no está dado de baja
        SDSmensaje.UpdateCommand = "SET dateformat dmy; UPDATE ComunicacionCA set Estatus = 'Baja' where IdComunicacionCA = @IdComunicacionCA"
        SDSmensaje.UpdateParameters.Add("IdComunicacionCA", Session("Envio_IdComunicacionCA"))
        SDSmensaje.Update()

        'SIEMPRE NO LO ELIMINO PORQUE EL MISMO ARCHIVO LO PUEDEN ESTAR RECIBIENDO VARIOS ALUMNOS:
        'Elimino el archivo adjunto para no acumular basura
        'If Trim(lbAdjunto.Text).Length > 0 Then
        'Dim ruta As String
        'ruta = Config.Global.rutaMaterial
        'ruta = ruta + "adjuntos\" + Session("LoginProfesor") + "\" 'Esta ruta contempla un directorio como alumno nombrado como su login
        'Dim MiArchivo As FileInfo = New FileInfo(ruta & lbAdjunto.Text)
        'MiArchivo.Delete()
        'End If

        eliminado = True
      Else
        msgError.show("Debe marcar el mensaje para poder eliminarlo.")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try

    If eliminado Then
      If Session("Envio_Previous").Equals("LeerMensajeSalidaC") Then
        Response.Redirect("LeerMensajeSalidaC.aspx")
      Else
        Response.Redirect("LeerMensajeEntradaC.aspx")
      End If
    End If
  End Sub

#End Region

#Region "responder"

  Protected Sub LBresponder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBresponder.Click
    Session("Responder") = "P"
    Session("Envio_IdCoordinador") = hfIdCoordinador.Value
    Session("Envio_IdProfesor") = hfIdProfesor.Value
    Session("Envio_NombreProfesor") = ltEnvia.Text
    Session("Envio_Email") = hfEmail.Value
    Session("Envio_Asunto") = ltAsunto.Text
    Session("Envio_Plantel") = ltPlantelP.Text
    Session("Envio_Fecha") = ltFechaEnvio.Text
    Response.Redirect("ResponderMensajeC.aspx")
  End Sub

  Protected Sub LBresponderA_Click(sender As Object, e As EventArgs) Handles LBresponderA.Click
    Session("Responder") = "A"
    Session("Envio_IdCoordinador") = hfIdCoordinador.Value
    Session("Envio_IdAlumno") = hfIdAlumno.Value
    Session("Envio_NombreAlumno") = ltEnviaA.Text
    Session("Envio_Email") = hfEmail.Value
    Session("Envio_Asunto") = ltAsuntoA.Text
    Session("Envio_Plantel") = ltPlantelA.Text
    Session("Envio_Fecha") = ltFechaEnvioA.Text
    Response.Redirect("ResponderMensajeC.aspx")
  End Sub

  Protected Sub LBreenviar_Click(sender As Object, e As EventArgs) Handles LBreenviar.Click
    Session("Reenvio") = "P"
    Session("Envio_Asunto") = ltAsunto.Text
    Session("Envio_Fecha") = ltFechaEnvio.Text
    Session("Envio_Contenido") = ltContenido.Text
    Session("Envio_AdjuntoNombre") = lbAdjunto.Text
    Session("Envio_AdjuntoFisico") = hfAdjuntoFisico.Value
    Session("Envio_Carpeta") = hfCarpetaAdjunto.Value
    Response.Redirect("EnviarMensajeC.aspx")
  End Sub

  Protected Sub LBreenviarA_Click(sender As Object, e As EventArgs) Handles LBreenviarA.Click
    Session("Reenvio") = "A"
    Session("Envio_Asunto") = ltAsuntoA.Text
    Session("Envio_Fecha") = ltFechaEnvioA.Text
    Session("Envio_Contenido") = ltContenidoA.Text
    Session("Envio_AdjuntoNombre") = lbAdjuntoA.Text
    Session("Envio_AdjuntoFisico") = hfAdjuntoFisico.Value
    Session("Envio_Carpeta") = hfCarpetaAdjunto.Value
    Response.Redirect("EnviarMensajeC.aspx")
  End Sub

#End Region

#Region "adjuntos"

  Protected Sub lbAdjunto_Click(sender As Object, e As EventArgs) Handles lbAdjunto.Click
    Dim file As System.IO.FileInfo =
        New System.IO.FileInfo(Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value + "\" & hfAdjuntoFisico.Value)

    If file.Exists Then 'set appropriate headers
      Response.Clear()
      Response.AddHeader("Content-Disposition", "attachment; filename=""" & lbAdjunto.Text & """")
      Response.AddHeader("Content-Length", file.Length.ToString())
      Response.ContentType = "application/octet-stream"
      Response.WriteFile(file.FullName)
      'Response.End()
    Else
      msgError.show("Ha ocurrido un error con el archivo adjunto: éste ya no está disponible.")
    End If 'nothing in the URL as HTTP GET
  End Sub

  Protected Sub lbAdjuntoA_Click(sender As Object, e As EventArgs) Handles lbAdjuntoA.Click
    Dim file As System.IO.FileInfo =
        New System.IO.FileInfo(Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value + "\" & hfAdjuntoFisico.Value)

    If file.Exists Then 'set appropriate headers
      Response.Clear()
      Response.AddHeader("Content-Disposition", "attachment; filename=" & lbAdjuntoA.Text)
      Response.AddHeader("Content-Length", file.Length.ToString())
      Response.ContentType = "application/octet-stream"
      Response.WriteFile(file.FullName)
      'Response.End()
    Else
      msgError.show("Ha ocurrido un error con el archivo adjunto: éste ya no está disponible.")
    End If 'nothing in the URL as HTTP GET
  End Sub

#End Region

End Class
