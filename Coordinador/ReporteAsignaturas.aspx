﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="ReporteAsignaturas.aspx.vb" 
    Inherits="coordinador_ReporteAsignaturas"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            height: 602px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table style="width: 100%">
        <tr>
            <td colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="3" class="auto-style1">
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" style="font-size: small" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="852px" Height="600px">
                    <LocalReport ReportPath="coordinador\ReporteAsignaturas.rdlc" EnableExternalImages="True">
                        <DataSources>
                            <rsweb:ReportDataSource DataSourceId="ODSasignaturas" Name="DSasignaturas" />
                            <rsweb:ReportDataSource DataSourceId="ODSsubtemas" Name="DSsubtemas" />
                            <rsweb:ReportDataSource DataSourceId="ODSsubtemasmejorar" Name="DSsubtemasejorar" />
                        </DataSources>
                    </LocalReport>
                </rsweb:ReportViewer>
            </td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
								<asp:HyperLink ID="HyperLink5" runat="server"
										NavigateUrl="javascript:history.back()"
										CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink></td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

            </td>
            <td>

                <asp:ObjectDataSource ID="ODSasignaturas" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="DSasignaturasTableAdapters.sp_repasignaturasTableAdapter">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar" Type="Int32" />
                        <asp:SessionParameter Name="IdGrado" SessionField="IdGrado" Type="Int32" />
                        <asp:SessionParameter Name="IdAlumno" SessionField="IdAlumno" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>

            </td>
            <td>

                <asp:ObjectDataSource ID="ODSsubtemas" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="DSasignaturasTableAdapters.sp_repsubtemasTableAdapter">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar" Type="Int32" />
                        <asp:SessionParameter Name="IdGrado" SessionField="IdGrado" Type="Int32" />
                        <asp:SessionParameter Name="IdAlumno" SessionField="IdAlumno" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>

            </td>
            <td>

                <asp:ObjectDataSource ID="ODSsubtemasmejorar" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="DSasignaturasTableAdapters.sp_repsubtemasmejorarTableAdapter">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar" Type="Int32" />
                        <asp:SessionParameter Name="IdGrado" SessionField="IdGrado" Type="Int32" />
                        <asp:SessionParameter Name="IdAlumno" SessionField="IdAlumno" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>

            </td>
        </tr>
    </table>
</asp:Content>

