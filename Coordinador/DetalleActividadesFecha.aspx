<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="DetalleActividadesFecha.aspx.vb"
    Inherits="coordinador_DetalleActividadesFecha"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
    .estandar {
        text-align: left;
    }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Consultas - Actividades Usando Filtros
    </h1>
    Mustra una matriz con los resultados de todas las  actividades de una 
	<asp:Label ID="Label1" runat="server" Text="[ASIGNATURA]" />
    por cada 
	<asp:Label ID="Label2" runat="server" Text="[ALUMNO]" />
    de un 
	<asp:Label ID="Label3" runat="server" Text="[GRADO]" />
    , permitiendo filtrar por varios criterios de selecci�n.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table>
        <tr>
            <td>
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Men�
                </asp:HyperLink>
            </td>
            <td>
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="titleColumn" colspan="3">
                <asp:Label ID="Label4" runat="server" Text="[INSTITUCION]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    DataSourceID="SdsInstituciones"
                    DataTextField="Descripcion"
                    DataValueField="IdInstitucion"
                    CssClass="wideTextbox"
                    Width="350px">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SdsInstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="Institucion_ObtenLista_IdCoordinador"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter 
                            Name="IdCoordinador" 
                            SessionField="Usuario_IdCoordinador" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="titleColumn" colspan="3">
                <asp:Label ID="Label5" runat="server" Text="[CICLO]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SdsCiclosEscolares"
                    DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" 
                    CssClass="wideTextbox"
                    Width="350px">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SdsCiclosEscolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="CicloEscolar_ObtenLista_Estatus"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:Parameter 
                            Name="estatus" 
                            DefaultValue="Activo" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="titleColumn" colspan="3">
                <asp:Label ID="Label6" runat="server" Text="[NIVEL]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SdsNiveles"
                    DataTextField="Descripcion"
                    DataValueField="IdNivel"
                    CssClass="wideTextbox"
                    Width="270px">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SdsNiveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="Nivel_ObtenLista_IdInstitucion_Estatus"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter 
                            Name="idInstitucion"
                            ControlID="DDLinstitucion" 
                            PropertyName="SelectedValue" />
                        <asp:Parameter 
                            Name="estatus" 
                            DefaultValue="Activo" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="titleColumn" colspan="3">
                <asp:Label ID="Label7" runat="server" Text="[GRADO]" />

            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SdsGrados"
                    DataTextField="Descripcion"
                    DataValueField="IdGrado" 
                    CssClass="wideTextbox"
                    Width="270px">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SdsGrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="Grado_ObtenLista_IdNivel_Estatus"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter 
                            Name="IdNivel"
                            ControlID="DDLnivel" 
                            PropertyName="SelectedValue" />
                        <asp:Parameter 
                            Name="estatus" 
                            DefaultValue="Activo" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="titleColumn" colspan="3">
                <asp:Label ID="Label8" runat="server" Text="[ASIGNATURAS]" />
                que desea revisar</td>
            <td class="estandar" colspan="2" style="font-size: small;">
                <asp:LinkButton ID="lbnSeleccionarTodasLasAsignaturas" runat="server">Seleccionar Todos</asp:LinkButton>
                -
                <asp:LinkButton ID="lbnDeseleccionarTodasLasAsignaturas" runat="server">Deseleccionar Todos</asp:LinkButton>
                <asp:CheckBoxList ID="CblAsignaturas" runat="server"
                    DataSourceID="SdsAsignaturas" 
                    DataTextField="Descripcion"
                    DataValueField="IdAsignatura" 
                    style="margin-left: 50px;">
                </asp:CheckBoxList>
                <asp:SqlDataSource ID="SdsAsignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="Asignatura_ObtenLista_IdGrado"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter 
                            Name="IdGrado"
                            ControlID="DDLgrado" 
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="titleColumn" colspan="3">&nbsp;</td>
            <td class="estandar" colspan="2">
                <asp:CheckBox ID="CBfechas" runat="server" AutoPostBack="True"
                    Text="Incluir un rango de fecha en que los  terminaron la actividad"
                    Style="color: #000099" />
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="titleColumn" colspan="3">&nbsp;</td>
            <td class="estandar" colspan="2">
                <asp:Panel ID="PnlFechas" runat="server" Visible="False">
                    <table class="style10">
                        <tr>
                            <td class="style24">
                                <span class="style25">Del</span>:</td>
                            <td>
                                <asp:TextBox ID="TBde" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="TBde_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="TBde">
                                </asp:CalendarExtender>
                                <asp:CustomValidator ID="CV_TBde" runat="server" 
                                    ControlToValidate="TBde"
                                    OnServerValidate="CV_TBde_ServerValidate"
                                    ErrorMessage="<br />La fecha inicial no es v�lida."></asp:CustomValidator>
                            </td>
                            <td class="style24">
                                <span class="style25">Al</span>:</td>
                            <td>
                                <asp:TextBox ID="TBal" runat="server"></asp:TextBox>
                                <asp:CalendarExtender ID="TBal_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="TBal">
                                </asp:CalendarExtender>
                                <asp:CustomValidator ID="CV_TBal" runat="server" 
                                    ControlToValidate="TBal"
                                    OnServerValidate="CV_TBal_ServerValidate"
                                    ErrorMessage="<br />La fecha final no es v�lida."></asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="titleColumn" colspan="3">Incluir 
								<asp:Label ID="Label9" runat="server" Text="[ALUMNOS]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:RadioButtonList ID="RBestatus" runat="server"
                    RepeatDirection="Horizontal">
                    <asp:ListItem Selected="true">Todos</asp:ListItem>
                    <asp:ListItem Value="Activos">Solo Activos</asp:ListItem>
                    <asp:ListItem Value="Inactivos">Solo Inactivos</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="titleColumn" colspan="3">&nbsp;</td>
            <td class="style26" colspan="2">NOTA: &quot;Solo Activos&quot; son los que tienen estatus de Activo y Suspendido</td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="2">
                <asp:LinkButton ID="LbnConsultar" runat="server" 
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium">
                    <asp:Image ID="Image3" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/table_heatmap(2).png"
                        CssClass="btnIcon"
                        height="24" />
                    Consultar
                </asp:LinkButton>
            </td>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style15">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Men�
                </asp:HyperLink>
            </td>
            <td colspan="4">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

