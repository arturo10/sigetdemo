﻿Imports Siget

'Imports System.Web.Security
'Imports System.Data
'Imports System.Data.OleDb
Imports System.Data.SqlClient

Partial Class coordinador_AvancePlantel
		Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = User.Identity.Name
        TitleLiteral.Text = "Avance Global de Actividades"

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.PLANTELES
        Label3.Text = Config.Etiqueta.COORDINADOR
        Label4.Text = Config.Etiqueta.PLANTEL
        Label5.Text = Config.Etiqueta.INSTITUCION
        Label6.Text = Config.Etiqueta.CICLO
        Label7.Text = Config.Etiqueta.PLANTEL
        GVdatos.Caption = "Seleccione " &
            Config.Etiqueta.ARTDET_GRADO & " " & Config.Etiqueta.GRADO &
            " del cual desea ver el avance en el llenado de tareas:"

        '1) Cargo el DropDownList con todos los planteles que puede ver el coordinador
        'If DDLplantel.Items.Count <= 0 Then 'Significaria que ya habia cargado los items, para que no se dupliquen
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader

        miComando = objConexion.CreateCommand
        'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
        miComando.CommandText = "select C.IdCoordinador from Coordinador C, Usuario U where C.IdUsuario = U.IdUsuario " + _
                                                        "and U.Login = '" + Trim(User.Identity.Name) + "'"
        Try
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            'Agrego Primeramente los siguiente Items
            'ANTES TENIA OTRA CONSULTA Y METIA LOS PLANTELES CON CODIGO:
            'DDLplantel.Items.Add(New ListItem("---Elija un Plantel", -1))
            'DDLplantel.Items.Add(New ListItem("Todos mis Planteles", 0))
            'While misRegistros.Read()
            'DDLplantel.Items.Add(New ListItem(misRegistros.Item("Descripcion"), misRegistros.Item("IdPlantel")))
            'End While
            misRegistros.Read()
            Session("IdCoordinador") = misRegistros.Item("IdCoordinador")
            misRegistros.Close()
            objConexion.Close()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
        'End If        
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        'Session("Plantel") = "Plantel " + DDLplantel.SelectedItem.Text 'Lo carga?
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
        DDLplantel.Items.Insert(1, New ListItem("Tod" & Config.Etiqueta.LETRA_PLANTEL & "s mis " & Config.Etiqueta.PLANTELES, -1))
    End Sub

    Protected Sub DDLcicloescolar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.SelectedIndexChanged
        'Session("IdCicloEscolar") = DDLcicloescolar.SelectedValue
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_CICLO & " " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLplantel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.SelectedIndexChanged
        Try
            'Para generar el contenido del GridView dinamicamente, es necesario que su propiedad AutoGenerateColumns = True 
            If DDLplantel.SelectedValue < 0 Then
                GVdatos.DataSourceID = "SDSgrados1"
                GVdatos.DataBind()
            Else
                GVdatos.DataSourceID = "SDSgrados2"
                GVdatos.DataBind()
            End If
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        'DDLinstitucion.Items.Insert(0, New ListItem("---Elija una Institución", 0))
    End Sub

    Protected Sub GVdatos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVdatos.SelectedIndexChanged
        'Variables que necesito pasar
        'ademas de Session("IdCoordinador")
        Session("IdCicloEscolar") = DDLcicloescolar.SelectedValue
        Session("IdInstitucion") = DDLinstitucion.SelectedValue
        Session("IdPlantel") = DDLplantel.SelectedValue
        Session("IdNivel") = GVdatos.SelectedDataKey.Values(0).ToString()
        Session("IdGrado") = GVdatos.SelectedDataKey.Values(1).ToString()
        Session("Institucion") = DDLinstitucion.SelectedItem
        Session("Nivel") = GVdatos.SelectedRow.Cells(2).Text
        Session("Grado") = GVdatos.SelectedRow.Cells(4).Text
        Session("Plantel") = DDLplantel.SelectedItem
        Response.Redirect("DetalleGrado.aspx")
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVdatos_PreRender(sender As Object, e As EventArgs) Handles GVdatos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVdatos.Rows.Count > 0 Then
            GVdatos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVdatos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If GVdatos.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub GVdatos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVdatos, "Select$" & e.Row.RowIndex).ToString())
        End If

        'Para que el siguiente código funcione, no debe estar habilitada la PAGINACION en el GridView
        e.Row.Cells(1).Visible = False
        e.Row.Cells(3).Visible = False

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(2).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.NIVEL

            LnkHeaderText = e.Row.Cells(4).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.GRADO
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVdatos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVdatos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

End Class
