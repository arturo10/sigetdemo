﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="ReporteAlumnos.aspx.vb"
    Inherits="coordinador_ReporteAlumnos"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
            height: 102px;
        }

        .style11 {
            height: 37px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style14 {
            text-align: left;
        }

        .style21 {
            width: 273px;
        }

        .style13 {
            width: 143px;
        }

        .style23 {
            text-align: left;
            width: 324px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style24 {
            text-align: left;
            font-size: x-small;
            font-weight: bold;
            color: #000099;
        }

        .style25 {
            text-align: left;
            font-size: x-small;
            color: #000099;
        }

        .style32 {
            height: 22px;
            width: 479px;
            font-weight: normal;
            font-size: small;
        }

        .style33 {
            text-align: left;
            height: 35px;
        }

        .style37 {
            text-align: left;
            font-size: x-small;
            color: #0000CC;
        }

        .style43 {
            text-align: left;
            height: 59px;
        }

        .style45 {
            color: #000099;
        }

        .style46 {
            color: #000099;
            font-weight: bold;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Resultados -
        <asp:Label ID="Label6" runat="server" Text="[ASIGNATURA]" />
    </h1>
    Reporte general de resultados por 
												<asp:Label ID="Label1" runat="server" Text="[ASIGNATURA]" />
    mostrando la 
                    calificación promedio y la acumulada. Incluye todos los 
												<asp:Label ID="Label2" runat="server" Text="[PLANTELES]" />
    de los que 
                    Usted es 
												<asp:Label ID="Label3" runat="server" Text="[COORDINADOR]" />
    y se puede ver el detalle por 
												<asp:Label ID="Label4" runat="server" Text="[GRADOS]" />
    y por 
												<asp:Label ID="Label5" runat="server" Text="[ALUMNOS]" />
    .
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td colspan="7">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style24" colspan="3">&nbsp;</td>
            <td class="style24">
                <asp:GridView ID="GVmaximos" runat="server" 
                    AutoGenerateColumns="False"

                    DataSourceID="SDSmaximascal" 
                    Width="308px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Calificacion" HeaderText="Calificación"
                            SortExpression="Calificacion" />
                        <asp:BoundField DataField="Maximo" HeaderText="% Acumulado Maximo Posible" ReadOnly="True"
                            SortExpression="Maximo">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
            <td class="style24">&nbsp;</td>
            <td class="style24" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style24" colspan="3">&nbsp;</td>
            <td class="style24" colspan="2">&nbsp;</td>
            <td class="style24" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style24" colspan="3">&nbsp;</td>
            <td class="style24" colspan="2">
                <asp:RadioButtonList ID="RBacumuladas" runat="server" AutoPostBack="True"
                    RepeatDirection="Horizontal"
                    Style="font-weight: 700; color: #000066; font-size: small">
                    <asp:ListItem Value="M">Mostrar acumuladas</asp:ListItem>
                    <asp:ListItem Value="O" Selected="True">Ocultar acumuladas</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td class="style24" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style33" colspan="7">
                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/coordinador/ReporteGrupo.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
        </tr>
        <tr class="estandar">
            <td colspan="7">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="7">
                <asp:GridView ID="GVreporte" runat="server" 

                    DataSourceID="SDSmateriaalumno" 
                    Width="884px"

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort NO, dynamic - rowdatabound
	                0  IdAlumno
	                1  EQUIPO
	                2  Matrícula
	                3  ALUMNO
                    ... dynamic
	                --%>
            </td>
        </tr>
        <tr class="estandar">
            <td colspan="7">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style37" colspan="7">
                <span class="style46">NOTA:</span><span class="style45"> Para el cálculo de las Calificaciones Acumuladas, cada actividad representa un porcentaje de la 
                Calificación, por lo tanto, el porcentaje se irá acumulando hasta llegar al 100% 
                conforme se vaya completando la cantidad de actividades que componen cada 
                calificación. 
                <br />
                    El Aprovechamiento de actividades es el resultado directo del promedio obtenido 
                en las actividades que se lleven 
                realizadas, independientemente de la ponderación que representen para una Calificación.</span></td>
        </tr>
        <tr class="estandar">
            <td class="style43" colspan="2">
                <asp:GridView ID="GVindicadores" runat="server" 
                    AutoGenerateColumns="False"
                    Caption="INDICADORES ESPECÍFICOS" 

                    DataSourceID="SDSindicadores" 
                    DataKeyNames="Color"
                    Width="237px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Categoria" HeaderText="Categoría"
                            SortExpression="Categoria" />
                        <asp:BoundField DataField="Color" HeaderText="Color" SortExpression="Color" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
            <td class="style43" colspan="2">&nbsp;</td>
            <td class="style43" colspan="3">
                <asp:GridView ID="GVindicadores2" runat="server" 
                    AutoGenerateColumns="False"
                    Caption="ESCALA DE VALORES  (PARA PROMEDIOS)" 

                    DataSourceID="SDSindicadores2" 
                    Width="334px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="MinAzul" HeaderText="Mínimo para EXCELENTE"
                            SortExpression="MinAzul">
                            <ItemStyle HorizontalAlign="Right" BackColor="LightBlue" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinVerde" HeaderText="Mínimo para BIEN"
                            SortExpression="MinVerde">
                            <ItemStyle HorizontalAlign="Right" BackColor="LightGreen" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinAmarillo" HeaderText="Mínimo para REGULAR"
                            SortExpression="MinAmarillo">
                            <ItemStyle HorizontalAlign="Right" BackColor="Yellow" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinRojo" HeaderText="Mínimo para MAL"
                            SortExpression="MinRojo">
                            <ItemStyle HorizontalAlign="Right" BackColor="LightSalmon" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="4">
                &nbsp;</td>
            <td class="style14" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/coordinador/ReporteGrupo.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
            <td class="style21" colspan="3">
                &nbsp;</td>
            <td class="style13" colspan="2">
                &nbsp;</td>
            <td class="style15">&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSmateriaalumno" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SP_promediomateriaalumno" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar"
                            Type="Int32" />
                        <asp:SessionParameter Name="IdAsignatura" SessionField="IdAsignatura"
                            Type="Int32" />
                        <asp:SessionParameter Name="IdGrupo" SessionField="IdGrupo" Type="Int32" />
                        <asp:SessionParameter Name="TotalC" SessionField="TotalC" Type="Int16" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSmaximascal" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Cal.Descripcion as Calificacion, (select SUM(Porcentaje) from Evaluacion where IdCalificacion = Cal.IdCalificacion and InicioContestar &lt;= getdate()) as Maximo
from Calificacion Cal
where Cal.IdCalificacion in (select Distinct C.IdCalificacion from Calificacion C, Evaluacion E  where (C.IdAsignatura = @IdAsignatura and C.IdCicloEscolar = @IdCicloEscolar) and
(E.IdCalificacion = C.IdCalificacion ))
order by Cal.Consecutivo">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdAsignatura" SessionField="IdAsignatura" />
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSindicadores2" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT MinRojo, MinAmarillo, MinVerde, MinAzul FROM Indicador, CicloEscolar
where Indicador.IdIndicador = CicloEscolar.IdIndicador and IdCicloEscolar = @IdCicloEscolar">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSindicadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT DISTINCT Categoria, Color
FROM         Rango
WHERE     IdIndicador in 
(select IdIndicador from Calificacion where IdAsignatura = @IdAsignatura and IdCicloEscolar = @IdCicloEscolar)">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdAsignatura" SessionField="IdAsignatura" />
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

