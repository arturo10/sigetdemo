﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="EditaMensajes.aspx.vb" 
    Inherits="superadministrador_EditaMensajes" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">

        .style14
        {
            width: 153px;
        }
        .style18
        {
            width: 117px;
        }
        .style13
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 153px;
            text-align: right;
        }
        .style15
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
        .style19
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 117px;
            text-align: right;
        }
        .style23
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
        .style24
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 153px;
            text-align: right;
        }
        .style25
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 117px;
            text-align: right;
        }
        .style32
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Comunicación - Avisos
		</h1>
		Permite agendar mensajes para que aparezcan en la ventana 
                    de inicio del sitio web, y en las páginas de inicio de los usuarios.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style10">
        <tr>
            <td class="style24">
                Contenido del Mensaje</td>
            <td class="style15" colspan="3">
                <asp:TextBox ID="TBcontenido" runat="server" MaxLength="120" Width="563px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style24">
                Fecha de inicio</td>
            <td class="style17">
                <asp:Calendar ID="CalInicia" runat="server" BackColor="White" 
                    BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" 
                    Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" 
                    Width="200px">
                    <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                    <SelectorStyle BackColor="#CCCCCC" />
                    <WeekendDayStyle BackColor="#FFFFCC" />
                    <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <OtherMonthDayStyle ForeColor="#808080" />
                    <NextPrevStyle VerticalAlign="Bottom" />
                    <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                    <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                </asp:Calendar>
            </td>
            <td class="style25">
                Fecha de término</td>
            <td>
                <asp:Calendar ID="CalTermina" runat="server" BackColor="White" 
                    BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" 
                    Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" 
                    Width="200px" style="font-weight: 700">
                    <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                    <SelectorStyle BackColor="#CCCCCC" />
                    <WeekendDayStyle BackColor="#FFFFCC" />
                    <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <OtherMonthDayStyle ForeColor="#808080" />
                    <NextPrevStyle VerticalAlign="Bottom" />
                    <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                    <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                </asp:Calendar>
            </td>
        </tr>
        <tr>
            <td class="style13">
                &nbsp;</td>
            <td class="style17" colspan="3">
                <asp:Button ID="Insertar" runat="server" Text="Insertar" 
										CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
								&nbsp;
                <asp:Button ID="Eliminar" runat="server" Text="Eliminar" 
										CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
								&nbsp;
                <asp:Button ID="Actualizar" runat="server" Text="Actualizar" 
										CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style14">
                &nbsp;</td>
            <td class="style16" colspan="3">
                <asp:GridView ID="GVmensajes" runat="server" 
                    AllowPaging="True" 
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 
                    PageSize="6" 
                    
                    DataKeyNames="IdMensaje" 
                    DataSourceID="SDSMensajes" 
                    Width="623px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdMensaje" HeaderText="IdMensaje" 
                            InsertVisible="False" ReadOnly="True" SortExpression="IdMensaje" />
                        <asp:BoundField DataField="Contenido" HeaderText="Contenido del Mensaje" 
                            SortExpression="Contenido" />
                        <asp:BoundField DataField="FechaInicia" DataFormatString="{0:dd/MM/yyyy}" 
                            HeaderText="Fecha de Inicio" SortExpression="FechaInicia" />
                        <asp:BoundField DataField="FechaTermina" DataFormatString="{0:dd/MM/yyyy}" 
                            HeaderText="Fecha de Término" SortExpression="FechaTermina" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="style14">
                &nbsp;</td>
            <td class="style16">
                &nbsp;</td>
            <td class="style18">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style11" colspan="4">
                <asp:HyperLink ID="HyperLink3" runat="server"
										NavigateUrl="~/"
										CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
								</asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSMensajes" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT [IdMensaje], [Contenido], [FechaInicia], [FechaTermina] FROM [Mensaje] ORDER BY [FechaInicia], [Posicion]">
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

