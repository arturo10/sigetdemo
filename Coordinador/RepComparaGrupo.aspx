﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="RepComparaGrupo.aspx.vb"
    Inherits="coordinador_RepComparaGrupo"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            height: 37px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style14 {
            text-align: left;
        }

        .style33 {
            text-align: left;
            height: 30px;
        }

        .style24 {
            text-align: left;
            font-size: x-small;
            font-weight: bold;
            color: #000099;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style21 {
            width: 273px;
        }

        .style13 {
            width: 143px;
        }

        .style23 {
            text-align: left;
            width: 324px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style32 {
            height: 22px;
            width: 479px;
            font-weight: normal;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Resultados - Comparación
    </h1>
    Muestra la calificación (acumulada) de varias 
		<asp:Label ID="Label1" runat="server" Text="[ASIGNATURAS]" />
    , de un mismo 
		<asp:Label ID="Label2" runat="server" Text="[GRADO]" />
    , para realizar una comparación de resultados por 
		<asp:Label ID="Label3" runat="server" Text="[PLANTEL]" />
    , 
		<asp:Label ID="Label4" runat="server" Text="[GRUPOS]" />
    y 
		<asp:Label ID="Label5" runat="server" Text="[ALUMNOS]" />
    .
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style14">&nbsp;</td>
            <td class="style14">
                <asp:GridView ID="GVmaximos" runat="server"

                    DataSourceID="SDSmaxcal" 
                    Width="308px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
            <td class="style14">&nbsp;</td>
            <td class="style14">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style33" colspan="4">
                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/coordinador/ReporteCompara.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="4">
                <asp:GridView ID="GVreporte" runat="server" 

                    DataSourceID="SDScomparagrupos" 
                    DataKeyNames="IdGrupo" 
                    Width="875px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort NO, dynamic - rowdatabound
	                0  select
	                1  idgrupo
	                2  GRUPO
	                ... dynamic
	                --%>
            </td>
        </tr>
        <tr>
            <td class="style24" colspan="4">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="4">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td class="style22">
                &nbsp;</td>
            <td class="style21">
                &nbsp;</td>
            <td class="style13">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style23">
                <asp:HyperLink ID="HyperLink5" runat="server"
                    NavigateUrl="javascript:history.back()"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
            <td class="style21">&nbsp;</td>
            <td class="style13">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDScomparagrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SP_comparacalgrupos" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter Name="calificaciones" SessionField="calificaciones"
                            Type="String" />
                        <asp:SessionParameter DefaultValue="" Name="IdPlantel" SessionField="IdPlantel"
                            Type="Int32" />
                        <asp:SessionParameter Name="IdGrado" SessionField="IdGrado" Type="Int32" />
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar" Type="Int32" />
                        <asp:SessionParameter Name="Rep" SessionField="Rep" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSmaxcal" runat="server"></asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

