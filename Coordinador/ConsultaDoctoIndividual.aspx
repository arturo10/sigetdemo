﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="ConsultaDoctoIndividual.aspx.vb" 
    Inherits="coordinador_ConsultaDoctoIndividual" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            height: 50px;
        }

        .style24 {
            height: 50px;
            text-align: left;
        }

        .style25 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 34px;
        }

        .style26 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            height: 5px;
            font-weight: bold;
            color: #000066;
        }

        .style27 {
            height: 5px;
            text-align: left;
        }

        .style28 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            height: 23px;
            color: #000066;
            font-weight: bold;
        }

        .style29 {
            height: 23px;
            text-align: left;
        }

        .style30 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 18px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }

        .auto-style1 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            height: 5px;
            font-weight: normal;
            color: #000066;
        }

        .auto-style2 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            height: 23px;
            color: #000066;
            font-weight: normal;
        }

        .auto-style3 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            height: 50px;
            font-weight: normal;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Consultas - Documentos Entregados
    </h1>
    Permite consultar la calificación y el archivo de los 
                    documentos entregados por los 
												<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    en una actividad de Entrega de Archivo.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="auto-style1">Actividad</td>
            <td class="style27" style="background: #ddd;">
                <asp:Label ID="LblActividad" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700"></asp:Label>
            </td>
            <td class="style27">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style1">
                <asp:Label ID="Label2" runat="server" Text="[ASIGNATURA]" />
            </td>
            <td class="style27" style="background: #ddd;">
                <asp:Label ID="LblAsignatura" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700"></asp:Label>
            </td>
            <td class="style27"></td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Label ID="Label3" runat="server" Text="[GRADO]" />
            </td>
            <td class="style29" style="background: #ddd;">
                <asp:Label ID="LblGrado" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700"></asp:Label>
            </td>
            <td class="style29"></td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Label ID="Label4" runat="server" Text="[NIVEL]" />
            </td>
            <td class="style29" style="background: #ddd;">
                <asp:Label ID="LblNivel" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700"></asp:Label>
            </td>
            <td class="style29">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">Elija el 
								<asp:Label ID="Label5" runat="server" Text="[GRUPO]" />
                que desea consultar</td>
            <td class="style24">
                <asp:DropDownList ID="DDLgrupo" runat="server" Width="300px"
                    AutoPostBack="True" DataSourceID="SDSgrupos" DataTextField="Descripcion"
                    DataValueField="IdGrupo">
                </asp:DropDownList>
            </td>
            <td class="style24">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:GridView ID="GVdatos" runat="server"
                    AutoGenerateColumns="False"
                    AllowSorting="True"

                    DataKeyNames="IdAlumno,Login"
                    DataSourceID="SDSalumnosterminaron" 
                    Width="883px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno"
                            Visible="False" />
                        <asp:BoundField DataField="Login" HeaderText="Usuario" SortExpression="Login"
                            Visible="False" />
                        <asp:BoundField DataField="Matricula" HeaderText="Matricula"
                            SortExpression="Matricula" />
                        <asp:BoundField DataField="Nombre Alumno" HeaderText="Nombre Alumno"
                            ReadOnly="True" SortExpression="Nombre Alumno" />
                        <asp:BoundField DataField="Grupo" HeaderText="Grupo" SortExpression="Grupo" />
                        <asp:BoundField DataField="Equipo" HeaderText="Equipo" SortExpression="Equipo" />
                        <asp:BoundField DataField="Calificacion" HeaderText="Resultado"
                            SortExpression="Calificacion">
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Archivo Entregado" HeaderText="Archivo Entregado"
                            SortExpression="Archivo Entregado" Visible="False" />
                        <asp:BoundField DataField="Documento" HeaderText="Documento"
                            SortExpression="Documento" Visible="False" />
                        <asp:HyperLinkField DataNavigateUrlFields="Login,Documento"
                            ItemStyle-CssClass="columnaHyperlink"
                            DataNavigateUrlFormatString="~/Resources/cargas/{0}/{1}" DataTextField="Documento"
                            HeaderText="Archivo Entregado" Target="_blank">
                            <ItemStyle Font-Size="Small" />
                        </asp:HyperLinkField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  idalumno
	                1  login Usuario
	                2  matricula
	                3  Nombre ALUMNO
	                4  GRUPO
	                5  EQUIPO
	                6  resultado
	                7  archivo entregado
	                --%>
            </td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: left">
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/coordinador/ConsultaDoctos.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrupo], [Descripcion] FROM [Grupo] WHERE (([IdGrado] = @IdGrado) AND ([IdPlantel] = @IdPlantel) AND ([IdCicloEscolar] = @IdCicloEscolar)) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdGrado" SessionField="IdGrado" Type="Int32" />
                        <asp:SessionParameter Name="IdPlantel" SessionField="IdPlantel" Type="Int32" />
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar"
                            Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSalumnosterminaron" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select A.IdAlumno, U.Login, A.Matricula, A.ApePaterno + ' ' + A.ApeMaterno + ', ' + A.Nombre as 'Nombre Alumno', G.Descripcion as Grupo, DE.Calificacion, A.Equipo, DE.Documento as 'Archivo Entregado', DE.Documento
from Alumno A
join Usuario U on U.IdUsuario = A.IdUsuario
left join DoctoEvaluacion DE on DE.IdEvaluacion = @IdEvaluacion and A.IdAlumno = DE.IdAlumno and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido')
join Grupo G on G.IdGrupo = A.IdGrupo and G.IdPlantel = @IdPlantel and G.IdGrupo = @IdGrupo
order by G.Descripcion, A.ApePaterno, A.ApeMaterno, A.Nombre">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdEvaluacion" SessionField="IdEvaluacion" />
                        <asp:SessionParameter DefaultValue="" Name="IdPlantel"
                            SessionField="IdPlantel" />
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

