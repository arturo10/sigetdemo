﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="RespuestasAbiertasCoord.aspx.vb"
    Inherits="coordinador_RespuestasAbiertasCoord"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">

        .style23 {
            font-family: Arial;
            font-size: small;
            font-weight: bold;
            height: 38px;
        }

        .style32 {
            height: 22px;
            width: 479px;
            font-weight: normal;
            font-size: small;
        }

        .style35 {
            font-family: Arial;
            font-size: small;
            font-weight: bold;
            height: 21px;
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
        }

        .style33 {
            height: 10px;
        }

        .style34 {
            height: 19px;
        }

        .style36 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style38 {
            font-family: Arial;
            font-size: small;
            font-weight: bold;
            height: 26px;
            width: 243px;
        }

        .style41 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 252px;
            text-align: right;
            height: 7px;
        }

        .style42 {
            height: 7px;
        }

        .style43 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 252px;
            text-align: right;
        }

        .style37 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000066;
            text-align: center;
        }
        .auto-style1 {
            height: 7px;
            text-align: left;
        }
        .auto-style2 {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Consultas - Respuestas abiertas
    </h1>
    Permite desplegar todas las respuestas abiertas escritas 
                    por los 
												<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    en una actividad específica.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style43">&nbsp;</td>
            <td class="style43">
                <asp:Label ID="Label2" runat="server" Text="[CICLO]" />
            </td>
            <td colspan="4" class="auto-style2">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px"
                    Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style43"></td>
            <td class="style43">
                <asp:Label ID="Label3" runat="server" Text="[INSTITUCION]" />
            </td>
            <td colspan="4" class="auto-style2">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    Height="22px" Width="350px" DataSourceID="SDSinstituciones"
                    DataTextField="Descripcion" DataValueField="IdInstitucion">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style43"></td>
            <td class="style43">
                <asp:Label ID="Label4" runat="server" Text="[PLANTEL]" />
            </td>
            <td colspan="4" class="auto-style2">
                <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                    DataValueField="IdPlantel" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style43"></td>
            <td class="style43">
                <asp:Label ID="Label5" runat="server" Text="[NIVEL]" />
            </td>
            <td colspan="4" class="auto-style2">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Height="22px" Width="270px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style41"></td>
            <td class="style41">
                <asp:Label ID="Label6" runat="server" Text="[GRADO]" />
            </td>
            <td colspan="4" class="auto-style1">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                    Height="22px" Width="270px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="6" class="style33" style="text-align:left;">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="GVdatos" runat="server" 
                    AllowPaging="True"
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 
                    Caption="<h3>Seleccione alguno de los GRUPOS</h3>"

                    DataSourceID="SDSgrupos" 
                    DataKeyNames="IdGrupo" 
                    Width="650px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell">
                        </asp:CommandField>
                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo" InsertVisible="False"
                            ReadOnly="True" SortExpression="IdGrupo" Visible="False" />
                        <asp:BoundField DataField="Grupo" HeaderText="Grupo" SortExpression="Grupo">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdGrado" HeaderText="IdGrado"
                            SortExpression="IdGrado" InsertVisible="False" ReadOnly="True"
                            Visible="False" />
                        <asp:BoundField DataField="Grado" HeaderText="Grado"
                            SortExpression="Grado">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Nivel" HeaderText="Nivel" SortExpression="Nivel">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                            SortExpression="IdPlantel" InsertVisible="False" ReadOnly="True"
                            Visible="False" />
                        <asp:BoundField DataField="Plantel" HeaderText="Plantel"
                            SortExpression="Plantel">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  select
	                1  idgrupo
	                2  GRUPO
	                3  idgrado
	                4  GRADO
	                5  NIVEL
	                6  idplantel
	                7  PLANTEL
	                --%>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="4">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style43"></td>
            <td class="style43">Asignatura</td>
            <td colspan="4" class="auto-style2">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSasignatura" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style43"></td>
            <td class="style43">Calificación</td>
            <td colspan="4" class="auto-style2">
                <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True"
                    DataSourceID="SDScalificaciones" DataTextField="Descripcion"
                    DataValueField="IdCalificacion" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style43"></td>
            <td class="style43">Actividad</td>
            <td colspan="4" class="auto-style2">
                <asp:DropDownList ID="DDLactividad" runat="server" AutoPostBack="True"
                    DataSourceID="SDSactividades" DataTextField="ClaveBateria"
                    DataValueField="IdEvaluacion" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="4" class="auto-style2">
                Mostrar:&nbsp;
                <asp:DropDownList ID="ddlEstado" runat="server" AutoPostBack="true"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: medium">
                    <asp:ListItem Value="Enviado" Selected="true">Envíos</asp:ListItem>
                    <asp:ListItem Value="Borrador">Borradores</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="GVrespuestas" runat="server"
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 
                    Caption="<h3>Respuestas Abiertas</h3>"

                    DataSourceID="SDSrespuestas" 
                    DataKeyNames="IdRespuesta" 
                    Width="885px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell">
                            <ItemStyle Width="90px" />
                        </asp:CommandField>
                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                            SortExpression="Consecutivo">
                            <HeaderStyle Width="60px" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Reactivo" HeaderText="Reactivo"
                            SortExpression="Reactivo">
                            <HeaderStyle Width="150px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdRespuesta" HeaderText="IdRespuesta"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdRespuesta"
                            Visible="False" />
                        <asp:BoundField DataField="Respuesta" HeaderText="Respuesta"
                            SortExpression="Respuesta" />
                        <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                            SortExpression="Matricula">
                            <HeaderStyle Width="60px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NomAlumno" HeaderText="Nombre del Alumno" ReadOnly="True"
                            SortExpression="NomAlumno">
                            <HeaderStyle Width="140px" />
                        </asp:BoundField>
                        
                        <asp:BoundField DataField="Respuesta" HeaderText="MarkupRespuesta"
                            SortExpression="Respuesta"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        
                        <asp:BoundField DataField="Reactivo" HeaderText="MarkupReactivo"
                            SortExpression="Reactivo"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  select
	                1  consecutivo
	                2  reactivo
	                3  idrespuesta
	                4  respuesta
	                5  matricula
	                6  nombre del ALUMNO
	                --%>
            </td>
        </tr>
        <tr>
            <td colspan="6" class="style34">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td colspan="6" class="style34">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" class="style34">
                &nbsp;</td>
            <td colspan="3" class="style34">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: left">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <asp:Panel ID="PnlConfirma" runat="server" CssClass="dialogOverwrite dialogRespuestaAbierta">
        <div style="margin-bottom: 20px;">
            Contenido de la Respuesta
        </div>
        <div style="text-align: left; border: 2px solid #444; padding: 10px; width: 766px; max-height: 300px; overflow-y: scroll;">
            <asp:Literal ID="TBrespuesta" runat="server"></asp:Literal>
        </div>
        <div style="margin-top: 20px;">
            <asp:Button ID="btnDialogClose" runat="server" Text="Cerrar"
                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
        </div>
    </asp:Panel>

    <table class="dataSources">
        <tr>
            <td>
                
                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>
                
            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT DISTINCT G.IdGrupo, G.Descripcion AS Grupo, Gr.IdGrado, Gr.Descripcion AS Grado, N.Descripcion AS Nivel, Pl.IdPlantel, Pl.Descripcion AS Plantel
FROM         Grupo G, Grado Gr,  Plantel Pl,  Nivel N
WHERE     (G.IdGrado = @IdGrado) AND (G.IdPlantel = @IdPlantel)
and G.IdGrado = Gr.IdGrado and  G.IdPlantel = Pl.IdPlantel and G.IdCicloEscolar = @IdCicloEscolar
ORDER BY Grupo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE (([IdNivel] = @IdNivel) AND ([Estatus] = @Estatus)) ORDER BY [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignatura" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdAsignatura, Descripcion 
FROM Asignatura WHERE IdGrado = @IdGrado
ORDER BY IdArea, Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>
                
                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdCalificacion], [Descripcion] FROM [Calificacion] WHERE (([IdCicloEscolar] = @IdCicloEscolar) AND ([IdAsignatura] = @IdAsignatura)) ORDER BY [Consecutivo]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                
            </td>
            <td>

                <asp:SqlDataSource ID="SDSactividades" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdEvaluacion], [ClaveBateria] FROM [Evaluacion] WHERE ([IdCalificacion] = @IdCalificacion) ORDER BY [InicioContestar], [ClaveBateria]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion"
                            PropertyName="SelectedValue" Type="Int64" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT distinct I.IdInstitucion, I.Descripcion 
FROM Institucion I, CoordinadorPlantel CP, Plantel P
WHERE I.Estatus = 'Activo' and I.IdInstitucion = P.IdInstitucion
and P.IdPlantel = CP.IdPlantel and 
CP.IdCoordinador = (select C.IdCoordinador from Coordinador C, Usuario U where
U.Login = @Login and C.IdUsuario = U.IdUsuario)
ORDER BY I.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Distinct N.IdNivel, N.Descripcion
from Nivel N, Escuela E
where E.IdPlantel = @IdPlantel
and N.IdNivel = E.IdNivel and (N.Estatus = 'Activo')
order by N.IdNivel">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSrespuestas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.Consecutivo, P.Redaccion as Reactivo, R.IdRespuesta, A.Redaccion as Respuesta,
Al.Matricula, Al.Nombre + ' ' + Al.ApePaterno + ' ' + Al.ApeMaterno as NomAlumno
from Planteamiento P, Respuesta R, RespuestaAbierta A, Alumno Al
where R.IdDetalleEvaluacion in 
(select IdDetalleEvaluacion from DetalleEvaluacion where IdEvaluacion = @IdEvaluacion)
and R.IdGrupo = @IdGrupo and A.IdRespuesta = R.IdRespuesta and 
P.IdPlanteamiento = R.IdPlanteamiento and P.TipoRespuesta = 'Abierta' and
R.IdCicloEscolar = @IdCicloEscolar and Al.IdAlumno = R.IdAlumno and A.Estado = @Estado
order by P.Consecutivo, NomAlumno">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLactividad" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="GVdatos" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="ddlEstado" Name="Estado"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.IdPlantel, P.Descripcion
from Plantel P, CoordinadorPlantel C
where C.IdCoordinador in (Select IdCoordinador from Coordinador, Usuario
where Coordinador.IdUsuario = Usuario.IdUsuario and Login = @Login)
and P.IdPlantel = C.IdPlantel and P.IdInstitucion = @IdInstitucion 
order by P.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>
                <asp:HiddenField ID="HF1" runat="server" />
                <asp:ModalPopupExtender ID="HF1_ModalPopupExtender" runat="server"
                    Enabled="True" PopupControlID="PnlConfirma" TargetControlID="HF1" BackgroundCssClass="overlay">
                </asp:ModalPopupExtender>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>

