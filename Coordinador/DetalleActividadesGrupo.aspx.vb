﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO

Partial Class coordinador_DetalleActividadesGrupo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = Trim(User.Identity.Name)
        Session("RolIndicador") = 0
        TitleLiteral.Text = "Resultados por Actividades"

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.GRUPO
        Label3.Text = Config.Etiqueta.ASIGNATURA
        Label4.Text = Config.Etiqueta.INSTITUCION
        Label5.Text = Config.Etiqueta.CICLO
        Label6.Text = Config.Etiqueta.PLANTEL
        Label7.Text = Config.Etiqueta.NIVEL
        Label8.Text = Config.Etiqueta.GRADO
        Label9.Text = Config.Etiqueta.GRUPO
        Label10.Text = Config.Etiqueta.ASIGNATURA
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        'DDLplantel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.PLANTEL, 0))
        DDLplantel.Items.Insert(0, New ListItem("---Todos los planteles", 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija una " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub GValumnos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GValumnos.RowDataBound
        If e.Row.Cells.Count > 7 Then 'Cuenta el total de COLUMNAS que hay en el grid,  GValumnos.Columns.Count NO funciona, para saber si hay actividades
            '1)PRIMERO OBTENGO LOS RANGOS DE LOS INDICADORES PARA EL SEMAFORO ROJO, AMARILLO Y VERDE
            Dim strConexion As String
            'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objConexion As New SqlConnection(strConexion)
            Dim miComando As SqlCommand
            Dim misRegistros As SqlDataReader
            miComando = objConexion.CreateCommand

            'ESTOS LOS USARE PARA SACAR LA FECHA DE TÉMINO:
            Dim miTermino As SqlCommand
            Dim RegistrosT As SqlDataReader
            miTermino = objConexion.CreateCommand

            miComando.CommandText = "select I.MinRojo,I.MinAmarillo, I.MinVerde, I.MinAzul from Indicador I, CicloEscolar C where " + _
                                    "I.IdIndicador = C.IdIndicador and C.IdCicloEscolar = " + DDLcicloescolar.SelectedValue
            Dim MinRojo, MinAmarillo, MinVerde, MinAzul As Decimal
            Try
                objConexion.Open()
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read()
                MinRojo = misRegistros.Item("MinRojo")
                MinAmarillo = misRegistros.Item("MinAmarillo")
                MinVerde = misRegistros.Item("MinVerde")
                MinAzul = misRegistros.Item("MinAzul")
                misRegistros.Close()
                'objConexion.Close()
                msgError.hide()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
            End Try

            '2)HAGO UN CICLO QUE VAYA DE 1 AL TOTAL DE COLUMNAS (EL INDICE DE LAS CELDAS(COLUMNAS) INICIA EN 0 (CELLS(0)= )
            'La parte de la condición: (e.Row.RowIndex > -1) es porque en este caso que comparo fechas estaba pintando tambien el encabezado (fila -1), que tambien comparaba
            If e.Row.RowType = DataControlRowType.DataRow Then 'ANTES PONÍA: (e.Row.Cells.Count > 1) And (e.Row.RowIndex > -1) Then  Para que se realizara la comparacion cuando haya mas de una columna
                For I As Byte = 8 To e.Row.Cells.Count - 1 'Inicio a partir de la octava columna que es la que tiene el primer valor de una actividad
                    If IsNumeric(Trim(e.Row.Cells(I).Text)) Then 'para que no compare celdas vacias
                        If (CDbl(e.Row.Cells(I).Text) >= MinRojo) And (CDbl(e.Row.Cells(I).Text) < MinAmarillo) Then
                            e.Row.Cells(I).BackColor = Drawing.Color.LightSalmon
                        ElseIf (CDbl(e.Row.Cells(I).Text) >= MinAmarillo) And (CDbl(e.Row.Cells(I).Text) < MinVerde) Then
                            e.Row.Cells(I).BackColor = Drawing.Color.Yellow
                        ElseIf (CDbl(e.Row.Cells(I).Text) >= MinVerde) And (CDbl(e.Row.Cells(I).Text) < MinAzul) Then
                            e.Row.Cells(I).BackColor = Drawing.Color.LightGreen
                        ElseIf (CDbl(e.Row.Cells(I).Text) >= MinAzul) And (CDbl(e.Row.Cells(I).Text) <= 100.0) Then 'La escala solo puede ser hasta 100
                            e.Row.Cells(I).BackColor = Drawing.Color.LightBlue
                        End If
                    Else 'No IsNumeric
                        miTermino.CommandText = "select FinSinPenalizacion from EvaluacionAlumno where IdAlumno = " + e.Row.Cells(0).Text + " and IdEvaluacion = " + e.Row.Cells(I - 1).Text
                        RegistrosT = miTermino.ExecuteReader()
                        If Not RegistrosT.Read() Then
                            RegistrosT.Close()
                            miTermino.CommandText = "select FinSinPenalizacion from EvaluacionPlantel where IdEvaluacion = " + e.Row.Cells(I - 1).Text + " and IdPlantel = (select IdPlantel from Alumno where IdAlumno = " + e.Row.Cells(0).Text + ")"
                            RegistrosT = miTermino.ExecuteReader()
                            If Not RegistrosT.Read() Then
                                RegistrosT.Close()
                                miTermino.CommandText = "select FinSinPenalizacion from Evaluacion where IdEvaluacion = " + e.Row.Cells(I - 1).Text
                                RegistrosT = miTermino.ExecuteReader()
                                RegistrosT.Read()
                            End If
                        End If
                        e.Row.Cells(I).Text = CDate(RegistrosT.Item("FinSinPenalizacion")).ToString("dd/MM/yyyy")
                        RegistrosT.Close()
                    End If
                Next
            End If
            objConexion.Close()
        End If 'del if e.Row.Cells.Count > 7
        'If e.Row.RowType = DataControlRowType.DataRow And e.Row.Cells(6).Text <> "&nbsp;" Then 'Fecha de Ingreso
        '    e.Row.Cells(6).Text = CDate(e.Row.Cells(6).Text).ToString("dd/MM/yyyy")
        'End If
        e.Row.Cells(0).Visible = False
        'Oculto las celdas donde está el Id de la Evalucion
        Dim Cont = 8
        Do While Cont <= e.Row.Cells.Count - 1
            e.Row.Cells(Cont).Visible = False
            Cont += 2
        Loop

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(5).Controls(1)
        End If
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        'Este procedimiento convierte el GridView a Excel eliminando la primera y segunda columna
        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GValumnos, "DetalleActividadesGrupo")
    End Sub

    Protected Sub GValumnos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GValumnos.DataBound
        GValumnos.Caption = "<B> Listado de " & Config.Etiqueta.ALUMNOS &
            " de " & Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO &
            " " + DDLgrupo.SelectedItem.ToString + " con el resultado de las Actividades realizadas en " &
            Config.Etiqueta.ARTDET_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA & " " + _
            DDLasignatura.SelectedItem.ToString + " en " &
            Config.Etiqueta.ARTDET_CICLO & " " & Config.Etiqueta.CICLO & " " +
            DDLcicloescolar.SelectedItem.ToString + "<br>" + _
            "Reporte emitido el " + Date.Today.ToShortDateString + "</B>"
    End Sub

    Protected Sub GValumnos_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GValumnos.Sorting
        'Verifico si la actividad seleccionada tiene material de apoyo
        Dim strConexion As String
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        miComando = objConexion.CreateCommand
        miComando.CommandText = "select Distinct IdProgramacion,IdEvaluacion from ApoyoEvaluacion where IdProgramacion in (select distinct IdProgramacion from Programacion where IdAsignatura = " + _
            DDLasignatura.SelectedValue.ToString + " and IdCicloEscolar = " + DDLcicloescolar.SelectedValue.ToString + " and IdGrupo = " + _
            DDLgrupo.SelectedValue.ToString + ") and IdEvaluacion = (select distinct E.IdEvaluacion from Evaluacion E, Calificacion C where E.ClaveBateria = '" + _
            e.SortExpression + "' and  E.IdCalificacion = C.IdCalificacion and C.IdAsignatura = " + DDLasignatura.SelectedValue.ToString + " and C.IdCicloEscolar = " + _
            DDLasignatura.SelectedValue.ToString + ")"
        Try
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            If misRegistros.Read() Then
                Session("IdProgramacion") = misRegistros.Item("IdProgramacion")
                Session("IdEvaluacion") = misRegistros.Item("IdEvaluacion")
                misRegistros.Close()
                objConexion.Close()
                PnlConfirma.Visible = True
                HF1_ModalPopupExtender.Show()
            Else
                misRegistros.Close()
                objConexion.Close()
            End If
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GValumnos0_PreRender(sender As Object, e As EventArgs) Handles GValumnos0.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GValumnos0.Rows.Count > 0 Then
            GValumnos0.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GValumnos0_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GValumnos0.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GValumnos0.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GValumnos_PreRender(sender As Object, e As EventArgs) Handles GValumnos.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GValumnos.Rows.Count > 0 Then
      GValumnos.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GValumnos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GValumnos.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GValumnos.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub
End Class
