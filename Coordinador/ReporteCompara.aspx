﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="ReporteCompara.aspx.vb"
    Inherits="coordinador_ReporteCompara"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style25 {
            height: 43px;
        }

        .style26 {
            text-align: right;
            height: 18px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style33 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
        }

        .style34 {
            font-size: x-small;
            color: #000099;
            height: 33px;
            text-align: left;
        }

        .style35 {
            font-size: x-small;
            color: #000099;
            height: 20px;
            text-align: left;
        }

        .style36 {
            color: #000000;
            height: 20px;
            text-align: right;
        }

        .style32 {
            height: 22px;
            width: 479px;
            font-weight: normal;
            font-size: small;
        }

        .style43 {
            height: 37px;
            font-family: Arial, Helvetica, sans-serif;
            text-align: left;
            color: #000066;
            font-weight: bold;
        }

        .style44 {
            color: #000066;
            font-weight: bold;
        }
        .auto-style1 {
            height: 45px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Resultados - Comparación
    </h1>
    Muestra la calificación (acumulada) de varias 
	<asp:Label ID="Label1" runat="server" Text="[ASIGNATURAS]" />
    , de un mismo 
	<asp:Label ID="Label2" runat="server" Text="[GRADO]" />
    , para realizar una comparación de resultados por 
	<asp:Label ID="Label3" runat="server" Text="[PLANTEL]" />
    , 
	<asp:Label ID="Label4" runat="server" Text="[GRUPOS]" />
    y 
	<asp:Label ID="Label5" runat="server" Text="[ALUMNOS]" />
    .
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style43" colspan="4">Indique los siguientes datos 
                para generar el reporte:</td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="Label6" runat="server" Text="[INSTITUCION]" />
            </td>
            <td style="text-align: left" colspan="2">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    Height="22px" Width="400px" DataSourceID="SDSinstituciones"
                    DataTextField="Descripcion" DataValueField="IdInstitucion">
                </asp:DropDownList>
            </td>
            <td rowspan="6">
                <asp:GridView ID="GVmaximos" runat="server"

                    DataSourceID="SDSmaxcal" 
                    Width="349px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="Label7" runat="server" Text="[CICLO]" />
            </td>
            <td style="text-align: left" colspan="2">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="duracion"
                    DataValueField="IdCicloEscolar" Height="22px" Width="400px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="Label8" runat="server" Text="[NIVEL]" />
            </td>
            <td style="text-align: left" colspan="2">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Height="22px" Width="400px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="Label9" runat="server" Text="[GRADO]" />
            </td>
            <td style="text-align: left" colspan="2">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                    Height="22px" Width="400px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">
                <asp:Label ID="Label10" runat="server" Text="[ASIGNATURA]" />
            </td>
            <td style="text-align: left" colspan="2">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSasignatura" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Height="22px" Width="400px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align: right">Calificación</td>
            <td style="text-align: left" colspan="2">
                <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True"
                    DataSourceID="SDScalificaciones" DataTextField="Cal"
                    DataValueField="IdCalificacion" Height="22px" Width="400px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="text-align: left">
                <asp:Button ID="Agregar" runat="server" CssClass="defaultBtn btnThemeGrey btnThemeSlick"
                    Text="Agregar" />
                &nbsp;
								<asp:Button ID="Quitar" runat="server" Text="Quitar"
                                    Visible="False" CssClass="defaultBtn btnThemeGrey btnThemeSlick" />
            </td>
            <td style="text-align: left">
                <asp:RadioButtonList ID="RBLres" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" style="font-size: small">
                    <asp:ListItem Value="A">Acumuladas</asp:ListItem>
                    <asp:ListItem Selected="True" Value="P">Promedio</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style44">Calificaciones agregadas para comparar:</td>
            <td colspan="3" style="text-align: left">
                <asp:CheckBoxList ID="CBLcalificaciones" runat="server" Width="646px"
                    BackColor="#CCCCCC" BorderColor="Black"
                    Style="font-size: small; color: #000066">
                </asp:CheckBoxList>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">
                &nbsp;</td>
            <td style="text-align: left" class="auto-style1" colspan="2">
                <asp:Button ID="Generar" runat="server" Text="Generar Consulta"
                    Visible="False" CssClass="defaultBtn btnThemeBlue btnThemeWide"
                    OnClientClick="setSpinner('')" />
            </td>
            <td class="auto-style1"></td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="spin" style="display:none;" class="spinnerLoading">
                    <div style="padding-top: 80px; text-align: center;">
                        Trabajando...
                    </div>
			    </div>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GVsalida" runat="server" 

                    DataSourceID="SDScomparacion" 
                    DataKeyNames="IdPlantel" 
                    Width="907px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort NO, dynamic - rowdatabound
	                0  select
	                1   idplantel
	                2  PLANTEL
	                --%>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
            <td colspan="2">
                &nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSmaxcal" runat="server"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE ([IdNivel] = @IdNivel) 
and (Estatus = 'Activo') ORDER BY [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT distinct I.IdInstitucion, I.Descripcion 
FROM Institucion I, CoordinadorPlantel CP, Plantel P
WHERE I.Estatus = 'Activo' and I.IdInstitucion = P.IdInstitucion
and P.IdPlantel = CP.IdPlantel and 
CP.IdCoordinador = (select C.IdCoordinador from Coordinador C, Usuario U where
U.Login = @Login and C.IdUsuario = U.IdUsuario)
ORDER BY I.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="LoginCoord" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDScomparacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SP_comparacalificaciones" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter Name="calificaciones" SessionField="Calificaciones"
                            Type="String" />
                        <asp:SessionParameter Name="loginCoord" SessionField="LoginCoord"
                            Type="String" />
                        <asp:SessionParameter Name="IdInstitucion" SessionField="IdInstitucion"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="RBLres" Name="Rep" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT C.IdCalificacion, C.Descripcion + ' (' + A.Descripcion + ')' as Cal
FROM Calificacion C, Asignatura A
WHERE C.IdCicloEscolar = @IdCicloEscolar
and A.IdAsignatura = C.IdAsignatura and A.IdAsignatura = @IdAsignatura
order by C.Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdCicloEscolar, Descripcion as  duracion
from CicloEscolar
where Estatus = 'Activo'
order by Descripcion"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignatura" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdAsignatura], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) 
AND IdAsignatura in (select IdAsignatura from Calificacion where IdCicloEscolar = @IdCicloEscolar)
ORDER BY [IdArea]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT DISTINCT N.IdNivel, N.Descripcion
FROM         Nivel AS N INNER JOIN
                      Escuela AS E ON N.IdNivel = E.IdNivel INNER JOIN
                      CoordinadorPlantel AS CP ON E.IdPlantel = CP.IdPlantel INNER JOIN
                      Coordinador AS C ON CP.IdCoordinador = C.IdCoordinador INNER JOIN
                      Usuario AS U ON C.IdUsuario = U.IdUsuario
WHERE     (U.Login = @Login) and (N.Estatus = 'Activo')
ORDER BY N.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="LoginCoord" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
    </table>
</asp:Content>

