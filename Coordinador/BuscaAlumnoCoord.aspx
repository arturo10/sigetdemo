﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="BuscaAlumnoCoord.aspx.vb" 
    Inherits="coordinador_BuscaAlumnoCoord" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>




<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style15 {
            width: 208px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 37px;
        }

        .style26 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            color: #000066;
            height: 37px;
        }

        .style24 {
            width: 208px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style18 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style20 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style27 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 23px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }
        .style16 {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Programación - Buscar
        <asp:Label ID="Label4" runat="server" Text="[ALUMNO]" />
    </h1>
    Permite buscar un 
												<asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
    por varios criterios para 
                    consultar sus datos y poder cambiar su contraseña.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style15"></td>
            <td class="style26" colspan="3">Capture uno o varios de los datos siguientes para hacer la búsqueda:</td>
        </tr>
        <tr>
            <td class="style24">
                <asp:Label ID="Label2" runat="server" Text="[INSTITUCION]" />
            </td>
            <td class="style16">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    Height="22px" Width="450px" DataSourceID="SDSinstituciones"
                    DataTextField="Descripcion" DataValueField="IdInstitucion">
                </asp:DropDownList>
            </td>
            <td class="style18">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style24">Matrícula</td>
            <td class="style16">
                <asp:TextBox ID="TBmatricula" runat="server" Width="220px"></asp:TextBox>
            </td>
            <td class="style18" rowspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style24">Nombre</td>
            <td class="style16">
                <asp:TextBox ID="TBnombre" runat="server" Width="220px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style24">Apellido Paterno</td>
            <td class="style16">
                <asp:TextBox ID="TBapepaterno" runat="server" Width="220px"></asp:TextBox>
            </td>
            <td class="style18">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style24">Apellido Materno</td>
            <td class="style16">
                <asp:TextBox ID="TBapematerno" runat="server" Width="220px"></asp:TextBox>
            </td>
            <td class="style18">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style24">Usuario</td>
            <td class="style16">
                <asp:TextBox ID="TBlogin" runat="server" Width="220px"></asp:TextBox>
            </td>
            <td class="style18">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style22">&nbsp;</td>
            <td>
                <asp:Button ID="BtnBuscar" runat="server" Text="Buscar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
				<asp:Button ID="BtnLimpiar" runat="server" Text="Limpiar"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="4">
                <asp:GridView ID="GValumnos" runat="server" 

                    DataSourceID="SDSbuscar" 
                    Width="896px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell">
                            <HeaderStyle Width="90px" />
                        </asp:CommandField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort NO - prerender
	                0  select
	                1  A.IdALUMNO
	                2  A.Matricula
	                3  A.Nombre
	                4  A.ApePaterno
	                5  A.ApeMaterno
	                6  G.IdGRUPO
	                7  G.Descripcion GRUPO
	                8  Gr.Descripcion GRADO
	                9  N.IdNIVEL
	                10 N.Descripcion NIVEL
                    11 P.IdPLANTEL
                    12 P.Descripcion PLANTEL
                    13 U.Login as usuario
                    14 U.Password
                    15 A.Estatus
                    16 A.EQUIPO
	                --%>
            </td>
        </tr>
        <tr>
            <td class="style22">
                <asp:SqlDataSource ID="SDSbuscar" runat="server"></asp:SqlDataSource>
            </td>
            <td colspan="2">
                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT distinct I.IdInstitucion, I.Descripcion 
FROM Institucion I, CoordinadorPlantel CP, Plantel P
WHERE I.Estatus = 'Activo' and I.IdInstitucion = P.IdInstitucion
and P.IdPlantel = CP.IdPlantel and 
CP.IdCoordinador = (select C.IdCoordinador from Coordinador C, Usuario U where
U.Login = @Login and C.IdUsuario = U.IdUsuario)
ORDER BY I.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style23">Actualizar Password:</td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style24">
                <asp:Label ID="Label3" runat="server" Text="[ALUMNO]" />
                seleccionado:</td>
            <td colspan="2">
                <asp:Label ID="LblAlumno" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #000099"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style24">Password</td>
            <td colspan="2">
                <asp:TextBox ID="TBpasswordOK" runat="server" CssClass="style20" Width="144px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style22">&nbsp;</td>
            <td colspan="2">
                <asp:Button ID="BtnActualizar" runat="server" Text="Actualizar Password"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
            <td>
                <asp:SqlDataSource ID="SDSusuarios" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Usuario]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style22">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

