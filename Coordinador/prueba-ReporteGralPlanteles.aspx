<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="prueba-ReporteGralPlanteles.aspx.vb" 
    Inherits="coordinador_ReporteGralPlanteles" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
		<style type="text/css">
				.style10 {
						width: 100%;
				}

				.style11 {
						width: 200px;
				}

				.style24 {
						width: 200px;
						font-family: Arial, Helvetica, sans-serif;
						font-size: small;
						font-weight: bold;
				}
		</style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Reporte General de 
								<asp:Label ID="Label1" runat="server" Text="[PLANTELES]" />
		</h1>
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
		<table class="style10">
				<tr>
						<td colspan="4">
								<rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana"
										Font-Size="8pt" Height="400px" Width="706px">
										<LocalReport ReportPath="c:\inetpub\wwwroot\sifa\coordinador\ReporteGrl.rdlc">
												<DataSources>
														<rsweb:ReportDataSource DataSourceId="ObjectDataSource1"
																Name="Prueba1_Alumno" />
												</DataSources>
										</LocalReport>
								</rsweb:ReportViewer>
								<asp:ObjectDataSource ID="ObjectDataSource1" runat="server"
										SelectMethod="GetData" TypeName="Prueba1TableAdapters.AlumnoTableAdapter"></asp:ObjectDataSource>
						</td>
				</tr>
				<tr>
						<td class="style11">&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
				</tr>
				<tr>
						<td class="style11">&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
				</tr>
				<tr>
						<td class="style11">
								<asp:HyperLink ID="HyperLink3" runat="server"
										NavigateUrl="~/"
										CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Men�
								</asp:HyperLink>
						</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
				</tr>
		</table>
</asp:Content>

