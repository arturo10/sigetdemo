﻿Imports Siget


Partial Class coordinador_GenerarReporteAsignaturas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = Trim(User.Identity.Name)

        Label1.Text = Config.Etiqueta.ALUMNO
        Label3.Text = Config.Etiqueta.ASIGNATURA
        Label4.Text = Config.Etiqueta.INSTITUCION
        Label5.Text = Config.Etiqueta.CICLO
        Label6.Text = Config.Etiqueta.PLANTEL
        Label7.Text = Config.Etiqueta.NIVEL
        Label8.Text = Config.Etiqueta.GRADO
        Label9.Text = Config.Etiqueta.GRUPO
    End Sub

    Protected Sub DDLcicloescolar_DataBound(sender As Object, e As EventArgs) Handles DDLcicloescolar.DataBound
        'DDLcicloescolar.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLplantel_DataBound(sender As Object, e As EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(sender As Object, e As EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(sender As Object, e As EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(sender As Object, e As EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub GVdatos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVdatos.SelectedIndexChanged
        Session("IdCicloEscolar") = DDLcicloescolar.SelectedValue
        Session("IdGrado") = DDLgrado.SelectedValue
        Session("IdAlumno") = CInt(GVdatos.SelectedDataKey.Values(0).ToString)
        Session("Nombre") = Trim(HttpUtility.HtmlDecode(GVdatos.SelectedRow.Cells(3).Text)) + " " + Trim(HttpUtility.HtmlDecode(GVdatos.SelectedRow.Cells(4).Text)) + " " + Trim(HttpUtility.HtmlDecode(GVdatos.SelectedRow.Cells(5).Text))
        Session("Plantel") = DDLplantel.SelectedItem.ToString + "/" + DDLgrupo.SelectedItem.ToString
        Session("Equipo") = Trim(HttpUtility.HtmlDecode(GVdatos.SelectedRow.Cells(6).Text))
        Response.Redirect("ReporteAsignaturas.aspx")
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVdatos_PreRender(sender As Object, e As EventArgs) Handles GVdatos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVdatos.Rows.Count > 0 Then
            GVdatos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVdatos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If GVdatos.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub GVdatos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVdatos, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVdatos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVdatos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

End Class
