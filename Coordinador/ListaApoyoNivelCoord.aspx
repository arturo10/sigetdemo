﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="ListaApoyoNivelCoord.aspx.vb" 
    Inherits="coordinador_ListaApoyoNivelCoord" 
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style24 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style27 {
            width: 100%;
        }

        .style31 {
            width: 360px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            text-align: right;
        }

        .style14 {
            width: 496px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 37px;
        }

        .style36 {
            font-weight: normal;
            font-size: small;
        }

        .style37 {
            height: 9px;
        }

        .style38 {
            height: 6px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Consultas - Apoyos de
        <asp:Label ID="Label5" runat="server" Text="[NIVEL]" />
    </h1>
    Muestra todos los archivos que se cargaron como apoyo para 
                    un 
												<asp:Label ID="Label1" runat="server" Text="[NIVEL]" />
    y que son vistos por todos los 
												<asp:Label ID="Label2" runat="server" Text="[ALUMNOS]" />
    de ese 
												<asp:Label ID="Label3" runat="server" Text="[NIVEL]" />
    .
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style24" style="background: #dddddd;">Indique el 
								<asp:Label ID="Label4" runat="server" Text="[NIVEL]" />
                del cual desee 
                consultar el apoyo académico</td>
            <td style="background: #dddddd;">
                <asp:DropDownList ID="DDLniveles" runat="server" AutoPostBack="True"
                    DataSourceID="SDSNiveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Height="22px" Width="251px">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td style="background: #dddddd;" class="style24">Mostrar</td>
            <td style="background: #dddddd;">
                <asp:DropDownList ID="DDLtipoarchivo" runat="server" Width="130px"
                    AutoPostBack="True">
                    <asp:ListItem Value="%">Todos</asp:ListItem>
                    <asp:ListItem>PDF</asp:ListItem>
                    <asp:ListItem>ZIP</asp:ListItem>
                    <asp:ListItem>Word</asp:ListItem>
                    <asp:ListItem>Excel</asp:ListItem>
                    <asp:ListItem Value="PP">Power Point</asp:ListItem>
                    <asp:ListItem>Imagen</asp:ListItem>
                    <asp:ListItem Value="Video"></asp:ListItem>
                    <asp:ListItem>Audio</asp:ListItem>
                    <asp:ListItem Value="Link"></asp:ListItem>
                    <asp:ListItem>FLV</asp:ListItem>
                    <asp:ListItem>SWF</asp:ListItem>
                    <asp:ListItem>YouTube</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td class="style37"></td>
            <td class="style37"></td>
            <td class="style37"></td>
            <td class="style37"></td>
            <td class="style37"></td>
            <td colspan="3" class="style37"></td>
        </tr>
        <tr>
            <td colspan="8">
                <asp:Panel ID="PnlDespliega" runat="server" BackColor="#E3EAEB"
                    Style="text-align: center">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style38"></td>
            <td class="style38"></td>
            <td class="style38"></td>
            <td class="style38"></td>
            <td class="style38"></td>
            <td colspan="3" class="style38"></td>
        </tr>
        <tr>
            <td colspan="8">
                <asp:GridView ID="GVapoyos" runat="server" 
                    AllowPaging="True"
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 
                    Caption="De clic en el apoyo académico que desee consultar"
                    PageSize="15" 
                    
                    DataKeyNames="IdApoyoNivel" 
                    DataSourceID="SDSapoyos" 
                    Width="856px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdApoyoNivel" HeaderText="IdApoyo"
                            SortExpression="IdApoyoNivel" InsertVisible="False" ReadOnly="True"
                            Visible="False" />
                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                            SortExpression="Consecutivo" />
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripción"
                            SortExpression="Descripcion" />
                        <asp:BoundField DataField="ArchivoApoyo" HeaderText="Apoyo Académico"
                            SortExpression="ArchivoApoyo" />
                        <asp:HyperLinkField DataNavigateUrlFields="ArchivoApoyo"
                            DataNavigateUrlFormatString="~/Resources/apoyo/{0}" DataTextField="ArchivoApoyo"
                            HeaderText="Apoyo Académico" Target="_blank" Visible="False" />
                        <asp:BoundField DataField="TipoArchivoApoyo" HeaderText="Tipo"
                            SortExpression="TipoArchivoApoyo" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                            SortExpression="Estatus" Visible="False" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <asp:Panel ID="PnlLink" runat="server" BackColor="#FFFF66" Visible="False">
                    <table class="style27">
                        <tr>
                            <td class="style31">Si el archivo no se abre automáticamente, da clic aquí:</td>
                            <td>
                                <asp:HyperLink ID="HLarchivo" runat="server"
                                    Style="font-family: Arial, Helvetica, sans-serif; font-size: medium; color: #000099">HyperLink</asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <asp:Label ID="LblMensaje" runat="server"
                    CssClass="LabelInfoDefault"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <asp:Label ID="LblError" runat="server"
                    CssClass="LabelErrorDefault"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style14" colspan="5">
                <asp:SqlDataSource ID="SDSNiveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select distinct N.IdNivel, N.Descripcion
from Nivel N, Escuela E, Plantel P
where P.IdPlantel in (select IdPlantel from CoordinadorPlantel where IdCoordinador = (select IdCoordinador from Coordinador C,Usuario U where U.Login = @Login and C.IdUsuario = U.IdUsuario))
 and E.IdPlantel = P.IdPlantel and N.IdNivel = E.IdNivel
and (Estatus = 'Activo')">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style14" colspan="5">
                <asp:SqlDataSource ID="SDSapoyos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT [IdApoyoNivel], [Consecutivo], [Descripcion], [ArchivoApoyo], [TipoArchivoApoyo], [Estatus] FROM [ApoyoNivel] WHERE ([IdNivel] = @IdNivel) and ([Estatus] = 'Activo')
and TipoArchivoApoyo Like @Tipo
ORDER BY [Consecutivo]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLniveles" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLtipoarchivo" Name="Tipo"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="8">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

