﻿Imports Siget

Imports System.Data.SqlClient

Partial Class coordinador_LeerMensajeSalidaC
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            aplicaLenguaje()

            initPageData()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = "Buzón de Mensajes"

        Label1.Text = Config.Etiqueta.PROFESORES
        Label2.Text = Config.Etiqueta.PLANTELES
        Label3.Text = Config.Etiqueta.ALUMNOS
        Label4.Text = Config.Etiqueta.ARTDET_PLANTELES
    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub initPageData()
        Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
            conn.Open()

            Dim cmd As SqlCommand = New SqlCommand("SELECT C.IdCoordinador, C.Nombre + ' ' + C.Apellidos AS NombreCoordinador FROM Usuario U, Coordinador C WHERE U.Login = @Login and C.IdUsuario = U.IdUsuario", conn)
            cmd.Parameters.AddWithValue("@Login", Trim(User.Identity.Name))

            Dim results As SqlDataReader = cmd.ExecuteReader()
            results.Read()
            ' obtengo el nombre del coordinador que necesito por si responde un mensaje
            Session("Envio_NombreCoordinador") = results.Item("NombreCoordinador").ToString()

            results.Close()
            cmd.Dispose()
            conn.Close()
        End Using
        Session("LeerMensajeSalidaC_Login") = User.Identity.Name
    End Sub

#End Region

#Region "GVmensajesProfesores"

    Protected Sub GVmensajesProfesores_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmensajesProfesores.DataBound
        If GVmensajesProfesores.Rows.Count <> 0 Then
            GVmensajesProfesores.Caption = "<h3>(" & GVmensajesProfesores.Rows.Count & ") Mensajes para " & Config.Etiqueta.PROFESORES & "</h3>"
        End If
    End Sub

    Protected Sub GVmensajesProfesores_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmensajesProfesores.RowDataBound
        'Los ESTATUS que se manejaran son Nuevo, Leido, Baja (los mensajes de baja ya no aparecen)
        If (DataBinder.Eval(e.Row.DataItem, "EstatusP") = "Leido" Or
            DataBinder.Eval(e.Row.DataItem, "EstatusP") = "Baja") And (e.Row.RowIndex > -1) Then
            e.Row.Cells(Utils.Tables.GetColumnIndexByName(GVmensajesProfesores, "Estatus")).BackColor = Drawing.Color.GreenYellow
        End If

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(3).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.PROFESOR & " que Recibe"

            LnkHeaderText = e.Row.Cells(4).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.PLANTEL
        End If

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVmensajesProfesores, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    Protected Sub GVmensajesProfesores_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmensajesProfesores.SelectedIndexChanged
        Session("Envio_IdComunicacionCP") = Trim(GVmensajesProfesores.SelectedDataKey.Values(0).ToString)
        Session("Envio_IdComunicacionCA") = "0"
        Session("Envio_LoginProfesor") = Trim(GVmensajesProfesores.SelectedDataKey.Values(1).ToString)
        Session("Envio_Plantel") = GVmensajesProfesores.SelectedRow.Cells(4).Text
        Session("Envio_Previous") = "LeerMensajeSalidaC"
        Response.Redirect("MensajeC.aspx")
    End Sub

    Protected Sub GVmensajesProfesores_PreRender(sender As Object, e As EventArgs) Handles GVmensajesProfesores.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVmensajesProfesores.Rows.Count > 0 Then
            GVmensajesProfesores.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVmensajesProfesores_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVmensajesProfesores.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVmensajesProfesores.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

#End Region

#Region "GVmensajesAlumnos"

  Protected Sub GVmensajesAlumnos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmensajesAlumnos.DataBound
    If GVmensajesAlumnos.Rows.Count <> 0 Then
      GVmensajesAlumnos.Caption = "<h3>(" & GVmensajesAlumnos.Rows.Count & ") Mensajes para " & Config.Etiqueta.ALUMNOS & "</h3>"
    End If
  End Sub

  Protected Sub GVmensajesAlumnos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmensajesAlumnos.RowDataBound
    'Los ESTATUS que se manejaran son Nuevo, Leido, Baja (los mensajes de baja ya no aparecen)
    If (DataBinder.Eval(e.Row.DataItem, "EstatusA") = "Leido" Or
        DataBinder.Eval(e.Row.DataItem, "EstatusA") = "Baja") And (e.Row.RowIndex > -1) Then
      e.Row.Cells(Utils.Tables.GetColumnIndexByName(GVmensajesAlumnos, "Estatus")).BackColor = Drawing.Color.GreenYellow
    End If

    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(3).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.ALUMNO & " que Recibe"

      LnkHeaderText = e.Row.Cells(4).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.PLANTEL
    End If

    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVmensajesAlumnos, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  Protected Sub GVmensajesAlumnos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmensajesAlumnos.SelectedIndexChanged
    Session("Envio_IdComunicacionCP") = "0"
    Session("Envio_IdComunicacionCA") = Trim(GVmensajesAlumnos.SelectedDataKey.Values(0).ToString)
    Session("Envio_LoginAlumno") = Trim(GVmensajesAlumnos.SelectedDataKey.Values(1).ToString)
    Session("Envio_Plantel") = GVmensajesAlumnos.SelectedRow.Cells(4).Text
    Session("Envio_Previous") = "LeerMensajeSalidaC"
    Response.Redirect("MensajeC.aspx")
  End Sub

  Protected Sub GVmensajesAlumnos_PreRender(sender As Object, e As EventArgs) Handles GVmensajesAlumnos.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVmensajesAlumnos.Rows.Count > 0 Then
      GVmensajesAlumnos.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVmensajesAlumnos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVmensajesAlumnos.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVmensajesAlumnos.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

#End Region

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        If GVmensajesProfesores.Rows.Count = 0 AndAlso GVmensajesAlumnos.Rows.Count = 0 Then
            msgInfo.show("No ha enviado ningún mensaje.")
        Else
            msgInfo.hide()
        End If

        Dim r As GridViewRow
        For Each r In GVmensajesProfesores.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVmensajesProfesores, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        For Each r In GVmensajesAlumnos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVmensajesAlumnos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub
End Class
