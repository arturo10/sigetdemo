﻿Imports Siget

Imports Microsoft.Reporting.WebForms

Partial Class coordinador_ReporteAsignaturas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Dim parameters As New List(Of ReportParameter)()
        parameters.Add(New ReportParameter("Nombre", Session("Nombre").ToString))
        parameters.Add(New ReportParameter("Plantel", Session("Plantel").ToString))
        parameters.Add(New ReportParameter("Equipo", Session("Equipo").ToString))
    parameters.Add(New ReportParameter("Ruta", Config.Global.rutaImagenes + "\logocliente.jpg"))
        If (Not IsPostBack) Then
            ReportViewer1.LocalReport.SetParameters(parameters)
        End If
    End Sub
End Class
