﻿Imports Siget

Imports System.IO
Imports System.Data
Imports System.Data.SqlClient

Partial Class coordinador_ResultadoReactivos
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Utils.Sesion.sesionAbierta()

    Session("Login") = User.Identity.Name

    Label1.Text = Config.Etiqueta.GRUPOS
    Label2.Text = Config.Etiqueta.PLANTEL
    Label3.Text = Config.Etiqueta.ALUMNO
    Label4.Text = Config.Etiqueta.INSTITUCION
    Label5.Text = Config.Etiqueta.CICLO
    Label6.Text = Config.Etiqueta.NIVEL
    Label7.Text = Config.Etiqueta.GRADO
    Label8.Text = Config.Etiqueta.ASIGNATURA
    Label9.Text = Config.Etiqueta.ARTDET_GRUPOS
    Label10.Text = Config.Etiqueta.ARTIND_PLANTEL
    GVplanteles.Caption = "<h3>" &
        Config.Etiqueta.PLANTELES &
        " asignados a " & Config.Etiqueta.ARTDET_COORDINADOR & " " &
        Config.Etiqueta.COORDINADOR &
        " de los cuales se realiza el conteo de reactivos contestados:</h3>"
  End Sub

  Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
    DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
  End Sub

  Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
    DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRADO & " " & Config.Etiqueta.GRADO, 0))
  End Sub

  Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
    DDLasignatura.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA, 0))
  End Sub

  Protected Sub DDLcalificacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacion.DataBound
    DDLcalificacion.Items.Insert(0, New ListItem("---Elija una Calificación", 0))
  End Sub

  Protected Sub DDLactividad_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLactividad.DataBound
    DDLactividad.Items.Insert(0, New ListItem("---Elija una Actividad", 0))
  End Sub

  Protected Sub GVreporte_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVreporte.DataBound
    If (GVreporte.Rows.Count = 0) And (DDLactividad.SelectedValue > 0) Then
      msgInfo.show("Esta actividad no contiene reactivos para contestar")
    Else
      msgInfo.hide()
    End If
    If GVreporte.Rows.Count > 0 Then
      BtnExportar.Visible = True
    Else
      BtnExportar.Visible = False
    End If
    GVreporte.Caption = "<B>Reactivos de la actividad " + DDLactividad.SelectedItem.ToString + " - " &
        DDLgrado.SelectedItem.ToString + "<BR>" + "Incluye las respuestas de todos " &
        Config.Etiqueta.ARTDET_ALUMNOS & " " & Config.Etiqueta.ALUMNOS &
        " de " & Config.Etiqueta.ARTDET_PLANTELES & " " & Config.Etiqueta.PLANTELES &
        " asignad" & Config.Etiqueta.LETRA_PLANTEL & "s a " & Config.Etiqueta.ARTDET_COORDINADOR & " " & Config.Etiqueta.COORDINADOR &
        " " + Session("Login") + ".   Reporte emitido el " + Date.Today.ToShortDateString + "</B>"

    ' AÑADO PORCENTAJES DE TERMINADAS
    ' para cada fila calcula el porcentaje, y añadelo a la fila
    Try
      For pC As Integer = 0 To GVreporte.Rows.Count() - 1
        Dim divisor As Decimal =
                Decimal.Add(Decimal.Parse(GVreporte.Rows(pC).Cells(4).Text),
                Decimal.Parse(GVreporte.Rows(pC).Cells(5).Text))

        If divisor = 0 Then ' protejo contra posibles resultados vacios
          Continue For
        End If

        GVreporte.Rows(pC).Cells(6).Text =
            Math.Round(Decimal.Multiply(Decimal.Divide(Decimal.Parse(
                GVreporte.Rows(pC).Cells(4).Text),
                divisor
            ), 100), 2, MidpointRounding.AwayFromZero) ' 2 decimales de precisión
      Next
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
    End Try
  End Sub

  Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
    ' añado los estilos, por que el gridview no se exporta predefinidamente con css
    GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
    GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
    GVactual.GridLines = GridLines.Both

    Dim headerText As String
    ' quito la imágen de ordenamiento de los encabezados
    For Each tc As TableCell In GVactual.HeaderRow.Cells
      If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
        headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
        tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
        tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
        tc.Text = headerText ' establezco el texto simple
      End If
    Next

    'Este procedimiento convierte el GridView a Excel eliminando la primera y segunda columna
    If GVactual.Rows.Count.ToString + 1 < 65536 Then
      Dim sb As StringBuilder = New StringBuilder()
      Dim sw As StringWriter = New StringWriter(sb)
      Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
      Dim pagina As Page = New Page
      Dim form = New HtmlForm
      pagina.EnableEventValidation = False
      pagina.DesignerInitialize()
      pagina.Controls.Add(form)
      form.Controls.Add(GVactual)
      pagina.RenderControl(htw)
      Response.Clear()
      Response.Buffer = True
      Response.ContentType = "application/vnd.ms-excel"
      Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
      Response.Charset = "UTF-8"
      Response.ContentEncoding = Encoding.Default
      Response.Write(sb.ToString())
      Response.End()
    Else
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
    End If
  End Sub

  Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
    Convierte(GVreporte, "ReactivosGeneral")
  End Sub

  Protected Sub GVplanteles_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVplanteles.SelectedIndexChanged
    If DDLactividad.SelectedValue > 0 Then
      msgInfo.hide()
      Session("IdCicloEscolar") = DDLcicloescolar.SelectedValue
      Session("IdEvaluacion") = DDLactividad.SelectedValue
      Session("IdAsignatura") = DDLasignatura.SelectedValue
      Session("IdPlantel") = GVplanteles.SelectedValue.ToString 'Por default es el unico valor dentro de datakeyname
      Session("IdGrado") = DDLgrado.SelectedValue
      Session("Titulo") = "<B>Reactivos de la actividad " + DDLactividad.SelectedItem.ToString + " - " + _
          DDLgrado.SelectedItem.ToString + "<BR>" +
          "Incluye las respuestas de " &
          Config.Etiqueta.ARTDET_ALUMNOS & " " &
          Config.Etiqueta.ALUMNOS & " de " &
          Config.Etiqueta.ARTDET_PLANTEL & " " &
          Config.Etiqueta.PLANTEL & " " + _
          GVplanteles.SelectedRow.Cells(2).Text + ".   Reporte emitido el " + Date.Today.ToShortDateString + "</B>"
      Response.Redirect("ReactivosPlantel.aspx")
    Else
      msgInfo.show("Primero seleccione una Actividad")
    End If
  End Sub

  Protected Sub DDLactividad_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLactividad.SelectedIndexChanged

    'DEBO CAMBIAR EL CAMPO DE ORDENAMIENTO PORQUE QUEDA MARCADO Y SI LA NUEVA CONSULTA NO GENERA ESE CAMPO, MARCA ERROR
    GVreporte.Sort("", SortDirection.Ascending)
    GVreporte.Columns.Clear()
    ' antes de repintar, añado la columna vacía de % donde calculo la proporción de temrinadas
    Dim terminadas As BoundField = New BoundField
    terminadas.HeaderText = HttpUtility.HtmlDecode("%&nbsp;Acertadas")
    GVreporte.Columns.Add(terminadas)
    GVreporte.DataBind()
  End Sub

  Protected Sub GVreporte_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVreporte.RowDataBound
    If (e.Row.Cells.Count > 1) And (e.Row.RowIndex > -1) Then  'Para que pinte cuando haya mas de una COLUMNA
      e.Row.Cells(4).BackColor = Drawing.Color.LightGreen
      e.Row.Cells(5).BackColor = Drawing.Color.LightSalmon

      e.Row.Cells(3).Text = HttpUtility.HtmlDecode(e.Row.Cells(3).Text)

      ' convierto el campo de respuesta de modo que se presente con estilos, no como markup
      e.Row.Cells(3).Text = Regex.Replace(HttpUtility.HtmlDecode(e.Row.Cells(3).Text), "<[^>]*(>|$)", String.Empty)
    End If
  End Sub

  Protected Sub SDSreporte_Selecting(sender As Object, e As SqlDataSourceSelectingEventArgs) Handles SDSreporte.Selecting
    e.Command.CommandTimeout = 3000
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GVreporte_PreRender(sender As Object, e As EventArgs) Handles GVreporte.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVreporte.Rows.Count > 0 Then
      GVreporte.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  ' cuando se generan automáticamente las columnas de un gridview, las predefinnidas se insertan
  ' a la izquierda de las generadas. queremos que el % de terminadas esté hasta la derecha, así que
  ' en la creación de la fila se acomoden las columnas
  Protected Sub GVreporte_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVreporte.RowCreated
    Dim row As GridViewRow = e.Row
    Dim columns As New List(Of TableCell)
    Dim cell As TableCell = row.Cells(0)
    row.Cells.Remove(cell) ' retira la columna %
    columns.Add(cell) ' insertala al final de la fila
    row.Cells.AddRange(columns.ToArray())

    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVreporte.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GVplanteles_PreRender(sender As Object, e As EventArgs) Handles GVplanteles.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVplanteles.Rows.Count > 0 Then
      GVplanteles.HeaderRow.TableSection = TableRowSection.TableHeader
    End If

    ' Etiquetas de GridView
    GVplanteles.Columns(2).HeaderText = Config.Etiqueta.PLANTEL
  End Sub

  Protected Sub GVplanteles_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVplanteles.RowDataBound
    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVplanteles, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVplanteles.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVplanteles, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub


End Class
