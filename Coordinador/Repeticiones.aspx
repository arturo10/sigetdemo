﻿<%@ Page Title=""
    Language="C#"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="true"
    CodeFile="Repeticiones.aspx.cs"
    Inherits="coordinador_Repeticiones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            width: 900px;
        }

        .style30 {
            font-family: Arial, Helvetica, sans-serif;
        }

        .style21 {
            font-size: x-small;
        }

        .style40 {
        }

        .style41 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style42 {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: small;
        }

        .style44 {
            color: #CC0000;
            font-weight: bold;
            width: 155px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style45 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            height: 34px;
        }

        .style46 {
            height: 34px;
        }

        .style47 {
            height: 34px;
            text-align: right;
            width: 209px;
        }

        .style48 {
            text-align: center;
        }

        .style49 {
            width: 209px;
        }

        .style50 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            width: 209px;
        }

        .style51 {
            text-align: center;
            width: 209px;
        }

        .style52 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
            color: #000066;
            height: 27px;
        }

        .style53 {
            height: 37px;
        }

        .style54 {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: small;
            height: 25px;
            color: #000066;
        }

        .style55 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style56 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: xx-small;
        }

        .titleColumn {
            text-align: right;
        }

        .control_column {
            text-align: left;
        }

        .overlay_top {
            z-index: 10004;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Consultas - Avance de Actividades con Repetición
    </h1>
    Muestra los intentos y resultados en todas las actividades de
     <asp:Literal ID="Literal2" runat="server">[UNA]</asp:Literal>
    <asp:Literal ID="Literal3" runat="server" Text="[ASIGNATURA]" />
    de 
    <asp:Literal ID="Literal8" runat="server">[LOS]</asp:Literal>
    <asp:Literal ID="Literal1" runat="server">[ALUMNOS]</asp:Literal>
    de
     <asp:Literal ID="Literal9" runat="server">[UN]</asp:Literal>
    <asp:Literal ID="Literal10" runat="server">[GRADO]</asp:Literal>
    específico.
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" runat="Server">
    <table style="width: 100%;">
        <tr>
            <td class="titleColumn">
                <asp:Literal ID="Literal4" runat="server" Text="[CICLO]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SdsCiclosEscolares"
                    DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar"
                    OnDataBound="DDLcicloescolar_DataBound"
                    Width="300px"
                    CssClass="style41"
                    Height="22px">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SdsCiclosEscolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="CicloEscolar_ObtenLista_Estatus"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:Parameter 
                            Name="estatus" 
                            DefaultValue="Activo" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Literal ID="Literal5" runat="server" Text="[NIVEL]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SdsNiveles" 
                    DataTextField="Descripcion" 
                    DataValueField="IdNivel"
                    OnDataBound="DDLnivel_DataBound"
                    Width="300px" 
                    Height="22px" 
                    CssClass="style41">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SdsNiveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="Nivel_ObtenLista_IdCoordinador_Estatus"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter 
                            Name="idCoordinador" 
                            SessionField="Usuario_IdCoordinador" />
                        <asp:Parameter 
                            Name="estatus" 
                            DefaultValue="Activo" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Literal ID="Literal6" runat="server" Text="[GRADO]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SdsGrados" 
                    DataTextField="Descripcion"
                    DataValueField="IdGrado" 
                    OnDataBound="DDLgrado_DataBound"
                    Width="300px" 
                    Height="22px"
                    CssClass="style41">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SdsGrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="Grado_ObtenLista_IdNivel_Estatus"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter 
                            Name="idNivel"
                            ControlID="DDLnivel" 
                            PropertyName="SelectedValue" />
                        <asp:Parameter 
                            Name="estatus" 
                            DefaultValue="Activo" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Literal ID="Literal7" runat="server" Text="[ASIGNATURA]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SdsAsignaturas" 
                    DataTextField="Descripcion"
                    DataValueField="IdAsignatura" 
                    OnDataBound="DDLasignatura_DataBound"
                    Width="300px" 
                    Height="22px"
                    CssClass="style41">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SdsAsignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="Asignatura_ObtenLista_IdGrado"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter 
                            Name="IdGrado"
                            ControlID="DDLgrado" 
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">Del
            </td>
            <td class="control_column">
                <asp:TextBox ID="tbFechaInicio" runat="server"></asp:TextBox>
                <span style="font-size: x-small;">(Opcional)</span>
                <asp:CalendarExtender ID="tbFechaInicio_CalendarExtender" runat="server"
                    Format="dd/MM/yyyy" TargetControlID="tbFechaInicio">
                </asp:CalendarExtender>
                                <asp:CustomValidator ID="CV_TBde" runat="server" 
                                    ControlToValidate="tbFechaInicio"
                                    OnServerValidate="CV_TBde_ServerValidate"
                                    ErrorMessage="<br />La fecha inicial no es válida."></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">Al
            </td>
            <td class="control_column">
                <asp:TextBox ID="tbFechaFin" runat="server"></asp:TextBox>
                <span style="font-size: x-small;">(Opcional)</span>
                <asp:CalendarExtender ID="tbFechaFin_CalendarExtender" runat="server"
                    Format="dd/MM/yyyy" TargetControlID="tbFechaFin">
                </asp:CalendarExtender>
                                <asp:CustomValidator ID="CV_TBal" runat="server" 
                                    ControlToValidate="tbFechaFin"
                                    OnServerValidate="CV_TBal_ServerValidate"
                                    ErrorMessage="<br />La fecha final no es válida."></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:LinkButton ID="LbnConsultar" runat="server" 
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" OnClick="LbnConsultar_Click">
                    <asp:Image ID="Image3" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/table_heatmap(2).png"
                        CssClass="btnIcon"
                        height="24" />
                    Consultar
                </asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msginfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td></td>
            <td></td>
            <td>


            </td>
            <td>


            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSRepeticiones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    OnSelecting="SDSRepeticiones_Selecting"
                    SelectCommand="
                    SET DATEFORMAT dmy;
SELECT 
	Al.IdAlumno, 
	Cal.IdCalificacion, 
	Ev.IdEvaluacion, 
	EvR.IdEvaluacionT, 
                    
    Al.Matricula,
	Al.ApePaterno + ' ' + Al.ApeMaterno + ' ' + Al.Nombre AS 'Nombre', 
    G.Descripcion As 'Grupo',
    P.Descripcion As 'Plantel',
    Al.Estatus,
    Al.FechaIngreso,
    AL.Equipo,
	Cal.Descripcion 'Calificacion', 
	Cal.Consecutivo, 
	Ev.ClaveBateria AS 'Evaluacion', 
	CAST(EvR.Intento AS VARCHAR(3)) AS 'Intento', 
	EvR.Resultado, 
    EvR.FechaTermino
FROM 
	Asignatura Asi, 
	Calificacion Cal, 
	EvaluacionRepetida EvR, 
    Evaluacion Ev,
    Alumno Al,
    Plantel P,
    Grupo G
WHERE 
	Asi.IdAsignatura = @IdAsignatura 
	AND Asi.IdAsignatura = Cal.IdAsignatura 
	AND Cal.IdCalificacion = Ev.IdCalificacion 
    AND Al.IdGrupo = G.IdGrupo 
    AND Al.IdPlantel = P.IdPlantel 
	AND Ev.IdEvaluacion = EvR.IdEvaluacion 
	AND EvR.IdAlumno = Al.IdAlumno 
    AND EvR.FechaTermino &gt;= @Inicio
    AND EvR.FechaTermino &lt;= @Fin
UNION
SELECT 
	Al.IdAlumno, 
	Cal.IdCalificacion, 
	Ev.IdEvaluacion, 
	ET.IdEvaluacionT, 

    Al.Matricula,
	Al.ApePaterno + ' ' + Al.ApeMaterno + ' ' + Al.Nombre AS 'Nombre', 
    G.Descripcion As 'Grupo',
    P.Descripcion As 'Plantel',
    Al.Estatus,
    Al.FechaIngreso,
    AL.Equipo,
	Cal.Descripcion 'Calificacion', 
	Cal.Consecutivo, 
	Ev.ClaveBateria AS 'Evaluacion', 
	(SELECT COUNT(Intento)+1 FROM EvaluacionRepetida WHERE IdAlumno = Al.IdAlumno AND IdEvaluacion = Ev.IdEvaluacion) AS 'Intento', 
	ET.Resultado, 
    ET.FechaTermino
FROM 
	Asignatura Asi, 
	Calificacion Cal, 
    Evaluacion Ev,
    Plantel P,
    Grupo G,
    Alumno Al 
    LEFT JOIN EvaluacionTerminada ET
	    ON ET.IdAlumno = Al.IdAlumno 
        AND ET.FechaTermino &gt;= @Inicio
        AND ET.FechaTermino &lt;= @Fin
WHERE 
	Asi.IdAsignatura = @IdAsignatura 
	AND Asi.IdAsignatura = Cal.IdAsignatura 
	AND Cal.IdCalificacion = Ev.IdCalificacion 
    AND Al.IdGrupo = G.IdGrupo 
    AND Al.IdPlantel = P.IdPlantel 
	AND Ev.IdEvaluacion = ET.IdEvaluacion 
UNION
SELECT 
	Al.IdAlumno, 
	Cal.IdCalificacion, 
	Ev.IdEvaluacion, 
	0, 
                    
    Al.Matricula,
	Al.ApePaterno + ' ' + Al.ApeMaterno + ' ' + Al.Nombre AS 'Nombre', 
    G.Descripcion As 'Grupo',
    P.Descripcion As 'Plantel',
    Al.Estatus,
    Al.FechaIngreso,
    AL.Equipo,
	Cal.Descripcion 'Calificacion', 
	Cal.Consecutivo, 
	Ev.ClaveBateria AS 'Evaluacion', 
	(SELECT COUNT(Intento)+1 FROM EvaluacionRepetida WHERE IdAlumno = Al.IdAlumno AND IdEvaluacion = Ev.IdEvaluacion) AS 'Intento', 
	NULL AS 'Resultado', 
    NULL AS 'FechaTermino'
FROM 
	Asignatura Asi, 
	Calificacion Cal, 
    Evaluacion Ev,
    Alumno Al,
    Plantel P,
    Grupo G
WHERE 
	Asi.IdAsignatura = @IdAsignatura 
	AND Asi.IdAsignatura = Cal.IdAsignatura 
	AND Cal.IdCalificacion = Ev.IdCalificacion 
    AND Al.IdGrupo = G.IdGrupo 
    AND Al.IdPlantel = P.IdPlantel 
    AND NOT EXISTS (SELECT ET.IdEvaluacionT FROM EvaluacionTerminada ET WHERE ET.IdAlumno = Al.IdAlumno AND ET.IdEvaluacion = Ev.IdEvaluacion)
ORDER BY 
    Plantel, 
    Grupo, 
    Nombre,
	Cal.Consecutivo, 
	Cal.Descripcion,
    Evaluacion,
    Intento">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlAsignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="hfInicio" Name="Inicio"
                            PropertyName="Value" DefaultValue="01/01/1900" />
                        <asp:ControlParameter ControlID="hfFin" Name="Fin"
                            PropertyName="Value" DefaultValue="31/12/2050" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:HiddenField ID="hfInicio" runat="server" />
                <asp:HiddenField ID="hfFin" runat="server" />
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>

