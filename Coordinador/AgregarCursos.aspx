﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="AgregarCursos.aspx.vb" 
    Inherits="coordinador_AgregarCursos" 
    MaintainScrollPositionOnPostback="true" %>


<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>



<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style15 {
            width: 208px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 37px;
        }

        .style26 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            color: #000066;
            height: 37px;
        }

        .style24 {
            width: 208px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style18 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style20 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style27 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 23px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }
        .style16 {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Programación - Agregar Cursos
    </h1>
    Permite agregar cursos para el sistema de Educación Continua.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style15"></td>
            <td class="style26" colspan="3">Ingrese la siguiente información del Curso que desea agregar</td>
        </tr>
        <tr>
            <td class="style24">Nombre</td>
            <td class="style16">
                <asp:TextBox ID="TBNombre" runat="server" Width="220px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFVNombre" runat="server" Display="Dynamic" ControlToValidate="TBNombre" ValidationGroup="Save">*</asp:RequiredFieldValidator>
            </td>
            <td class="style18" rowspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style24">Tipo</td>
            <td class="style16">
               <asp:DropDownList ID="DDLTipo" runat="server"
                    Height="22px" Width="100px">
                   <asp:ListItem Value="Presencial">Presencial</asp:ListItem>
                   <asp:ListItem Value="Virtual">Virtual</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="style18" rowspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style24">Cupo</td>
            <td class="style16">
                <asp:TextBox type="number" ID="TBcupo" runat="server" Width="50px" Height="20px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style24">Horas de Validez</td>
            <td class="style16">
                <asp:TextBox type="number" ID="TBhorasValidez" runat="server" Width="50px" Height="20px"></asp:TextBox>
            </td>
            <td class="style18">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style24">Instructor</td>
            <td class="style16">
                <asp:TextBox ID="TBinstructor" runat="server" Width="220px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFVinstructor" runat="server" Display="Dynamic" ControlToValidate="TBinstructor" ValidationGroup="Save">*</asp:RequiredFieldValidator>
            </td>
            <td class="style18">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style24">Lugar</td>
            <td class="style16">
                <asp:TextBox ID="TBlugar" runat="server" Width="220px" Height="30" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFVlugar" runat="server" Display="Dynamic" ControlToValidate="TBlugar" ValidationGroup="Save">*</asp:RequiredFieldValidator>
            </td>
            <td class="style18">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td class="style24">Fecha de Inicio</td>
            <td class="style16">
                <asp:TextBox ID="TBFechaInicio" runat="server" Width="220px" ></asp:TextBox>
                <asp:CalendarExtender ID="TBFechaInicio_CalendarExtender" runat="server" Enabled="True" TargetControlID="TBFechaInicio" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RFVFechaInicio" runat="server" Display="Dynamic" ControlToValidate="TBFechaInicio" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="REVFechaInicio" runat="server" ForeColor ="Red" ControlToValidate ="TBFechaInicio" ValidationGroup="Save" ValidationExpression="^(((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00|[048])))$">*</asp:RegularExpressionValidator>
                

           </td>
            <td class="style18">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

                <tr>
            <td class="style24">Fecha de Finalización</td>
            <td class="style16">
                <asp:TextBox ID="TBFechaFin" runat="server" Width="220px"></asp:TextBox>
                <asp:CalendarExtender ID="TBFechaFin_CalendarExtender" runat="server" Enabled="True" TargetControlID="TBFechaFin" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RFVFechaFin" runat="server" Display="Dynamic" ControlToValidate="TBFechaFin" ValidationGroup="Save">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="REVFechaFin" runat="server" ForeColor ="Red" ControlToValidate ="TBFechaFin" ValidationGroup="Save" ValidationExpression="^(((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00|[048])))$">*</asp:RegularExpressionValidator>
                
               </td>
            <td class="style18">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td class="style24">Horario</td>
            <td class="style16">
                <asp:TextBox ID="TBhorario" runat="server" Width="220px" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RFVhorario" runat="server" Display="Dynamic" ControlToValidate="TBhorario" ValidationGroup="Save">*</asp:RequiredFieldValidator>
            </td>
            <td class="style18">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td class="style24"></td>
            <td class="style16">
                <asp:DropDownList ID="DDLEstatus" runat="server"
                    Height="22px" Width="100px">
                   <asp:ListItem Value="Activo">Activo</asp:ListItem>
                   <asp:ListItem Value="Inactivo">Inactivo</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="style18">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td class="style22">&nbsp;</td>
            <td>
                <asp:Button ID="BtnAgregar" runat="server" Text="Agregar Curso" CausesValidation="true" OnClientClick="return validateAgregar();"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" ValidationGroup="Save" />
                        <script type="text/javascript"  >
                            function validateAgregar() {
                                if (Page_ClientValidate()!=null && Page_ClientValidate())
                                    return confirm('Estas a punto de agregar un Curso');
                            }
                        </script>
                <asp:Button ID="BtnActualizar" runat="server" Text="Actualizar Curso" CausesValidation="true" OnClientClick="return validateActualizar();"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" ValidationGroup="Save" />
                <script type="text/javascript" >
                    function validateActualizar() {
                        if (Page_ClientValidate() != null && Page_ClientValidate())
                            return confirm('Vas a modificar la información de un Curso');
                    }
                        </script>
                <asp:Button ID="BtnCancelar" runat="server" Text="Cancelar" OnClientClick="return confirm('Eliminarás los datos ingresados');"
                    CssClass="defaultBtn btnThemeGray btnThemeMedium" />
				
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="4">
                <asp:GridView ID="GVcursos" runat="server" 

                    DataSourceID="SDScursos" 
                    Width="896px" AutoGenerateColumns="false" AllowSorting="true"
                    
                    CssClass="dataGrid_clear_selectable"
                    GridLines="None" DataKeyNames="IdCurso,Estatus">
                    <Columns>
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                        <asp:BoundField DataField="Cupo" HeaderText="Cupo" SortExpression="Cupo" />
                        <asp:BoundField DataField="HorasValidez" HeaderText="Horas de Validez" SortExpression="HorasValidez" />
                        <asp:BoundField DataField="Instructor" HeaderText="Instructor" SortExpression="Instructor" />
                        <asp:BoundField DataField="Lugar" HeaderText="Lugar" SortExpression="Lugar" />
                        <asp:BoundField DataField="Horario" HeaderText="Horario" SortExpression="Horario" />
                        <asp:BoundField DataField="FechaInicio" HeaderText="Inicia(Fecha)" SortExpression="FechaInicio" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:BoundField DataField="FechaFin" HeaderText="Finaliza(Fecha)" SortExpression="FechaFin" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" SortExpression="Estatus" />
                        <asp:CommandField SelectText="Modificar" ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:SqlDataSource ID="SDScursos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM Cursos ORDER BY Estatus ASC">
                    <SelectParameters>
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style22">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

