﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="GenerarReporteAsignaturas.aspx.vb" 
    Inherits="coordinador_GenerarReporteAsignaturas" 
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">

        .auto-style1 {
            width: 100%;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" Runat="Server">
    <h1>Reportes - Informe de Asignaturas </h1>
    Muestra los resultados de cada 
                    <asp:Label ID="Label3" runat="server" Text="[ASIGNATURA]" />
    &nbsp;que tiene un 
												<asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
    , así como su PERCENTIL.
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="auto-style1">
    <tr>
        <td style="text-align: right">
            <asp:Label ID="Label4" runat="server" Text="[INSTITUCION]" />
        </td>
        <td colspan="2" style="text-align: left">
            <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    Height="22px" Width="450px" DataSourceID="SDSinstituciones"
                    DataTextField="Descripcion" DataValueField="IdInstitucion">
            </asp:DropDownList>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: right">
            <asp:Label ID="Label5" runat="server" Text="[CICLO]" />
        </td>
        <td colspan="2" style="text-align: left">
            <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px"
                    Width="450px">
            </asp:DropDownList>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: right">
            <asp:Label ID="Label6" runat="server" Text="[PLANTEL]" />
        </td>
        <td colspan="2" style="text-align: left">
            <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                    DataValueField="IdPlantel" Height="22px" Width="450px">
            </asp:DropDownList>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: right">
            <asp:Label ID="Label7" runat="server" Text="[NIVEL]" />
        </td>
        <td colspan="2" style="text-align: left">
            <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Height="22px" Width="270px">
            </asp:DropDownList>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: right">
            <asp:Label ID="Label8" runat="server" Text="[GRADO]" />
        </td>
        <td colspan="2" style="text-align: left">
            <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                    Height="22px" Width="270px">
            </asp:DropDownList>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: right">
            <asp:Label ID="Label9" runat="server" Text="[GRUPO]" />
        </td>
        <td colspan="2" style="text-align: left">
            <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrupos" DataTextField="Descripcion" DataValueField="IdGrupo"
                    Width="270px">
            </asp:DropDownList>
        </td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td style="text-align: right" colspan="4">
                <asp:GridView ID="GVdatos" runat="server"
                    AutoGenerateColumns="False"
                    AllowSorting="True"

                    DataKeyNames="IdAlumno" 
                    DataSourceID="SDSalumnos"
                    Width="883px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" SelectText="Ver Reporte">
                            <ControlStyle ForeColor="White" Font-Bold="true" Width="6em"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                        </asp:CommandField>
                        <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno" InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno" Visible="False">
                        </asp:BoundField>
                        <asp:BoundField DataField="Matricula" HeaderText="Matrícula" SortExpression="Matricula" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                        <asp:BoundField DataField="ApePaterno" HeaderText="Apellido Paterno" SortExpression="ApePaterno" />
                        <asp:BoundField DataField="ApeMaterno" HeaderText="Apellido Materno" SortExpression="ApeMaterno" />
                        <asp:BoundField DataField="Equipo" HeaderText="Equipo" SortExpression="Equipo" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
        </td>
    </tr>
    <tr>
        <td>
            <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT distinct I.IdInstitucion, I.Descripcion 
FROM Institucion I, CoordinadorPlantel CP, Plantel P
WHERE I.Estatus = 'Activo' and I.IdInstitucion = P.IdInstitucion
and P.IdPlantel = CP.IdPlantel and 
CP.IdCoordinador = (select C.IdCoordinador from Coordinador C, Usuario U where
U.Login = @Login and C.IdUsuario = U.IdUsuario)
ORDER BY I.Descripcion">
                <SelectParameters>
                    <asp:SessionParameter Name="Login" SessionField="Login" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
        <td>
            <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>
        </td>
        <td>
            <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.IdPlantel, P.Descripcion
from Plantel P, CoordinadorPlantel C
where C.IdCoordinador in (Select IdCoordinador from Coordinador, Usuario
where Coordinador.IdUsuario = Usuario.IdUsuario and Login = @Login)
and P.IdPlantel = C.IdPlantel and P.IdInstitucion = @IdInstitucion 
order by P.Descripcion">
                <SelectParameters>
                    <asp:SessionParameter Name="Login" SessionField="Login" />
                    <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
        <td>
            <asp:SqlDataSource ID="SDSmaterias" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdAsignatura, Descripcion
from Asignatura
where IdGrado = @IdGrado
AND IdAsignatura in (select C.IdAsignatura from Calificacion C, Evaluacion E 
where C.IdCicloEscolar = @IdCicloEscolar and E.IdCalificacion = C.IdCalificacion and SeEntregaDocto = 'True')
order by descripcion">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
        </td>
        <td>
                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Distinct N.IdNivel, N.Descripcion
from Nivel N, Escuela E
where E.IdPlantel = @IdPlantel
and N.IdNivel = E.IdNivel and (N.Estatus = 'Activo')
order by N.IdNivel">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
        </td>
        <td>
            <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE (([IdNivel] = @IdNivel) AND ([Estatus] = @Estatus)) ORDER BY [IdGrado]">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
        <td>
            <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrupo], [Descripcion] FROM [Grupo] WHERE (([IdGrado] = @IdGrado) AND ([IdPlantel] = @IdPlantel)) and ([IdCicloEscolar] = @IdCicloEscolar)
ORDER BY [Descripcion]">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                    <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
        <td>
            <asp:SqlDataSource ID="SDSalumnos" runat="server" ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT [IdAlumno],[Matricula], [Nombre], [ApePaterno], [ApeMaterno], [Equipo] FROM [Alumno] WHERE ([Estatus] = 'Activo') AND [IdGrupo] = @IdGrupo
ORDER BY [ApePaterno], [ApeMaterno], [Nombre]">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo" PropertyName="SelectedValue" />
                </SelectParameters>
            </asp:SqlDataSource>
        </td>
        <td colspan="2">
            &nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="4">
            &nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
</table>
</asp:Content>

