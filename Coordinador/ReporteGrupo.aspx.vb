﻿Imports Siget

'Imports System.Data
'Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.IO

Partial Class coordinador_ReporteGrupo
    Inherits System.Web.UI.Page

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        Utils.Sesion.sesionAbierta()

        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        'Este procedimiento convierte el GridView a Excel eliminando 3 columnas
        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            'Algoritmo que oculta toda la columna donde aparece el "Seleccionar"
            Dim Cont As Integer
            GVactual.HeaderRow.Cells.Item(0).Visible = False
            For Cont = 0 To GVactual.Rows.Count - 1
                GVactual.Rows(Cont).Cells.Item(0).Visible = False
            Next
            'Algoritmo que oculta toda la columna IdGrupo
            'GVactual.HeaderRow.Cells.Item(1).Visible = False
            'For Cont = 0 To GVactual.Rows.Count - 1
            'GVactual.Rows(Cont).Cells.Item(1).Visible = False
            'Next
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVreporte, "RepGrupo")
    End Sub

    Protected Sub GVreporte_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVreporte.SelectedIndexChanged
        Session("IdGrupo") = GVreporte.SelectedDataKey.Values(0).ToString()
        Session("NomGrupo") = GVreporte.SelectedRow.Cells(2).Text

        Response.Redirect("ReporteAlumnos.aspx")
    End Sub

    Protected Sub GVreporte_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVreporte.DataBound
        'Para generar el contenido del GridView dinamicamente, es necesario que su propiedad AutoGenerateColumns = True 
        GVreporte.Caption = "<b>" + Session("Institucion") + " - " + Session("Plantel") + " " + Session("CicloEscolar") + "<BR>" + Session("NomNivel") + _
        " - " + Session("NomGrado") + " - " + Session("Asignatura") + "<BR>Reporte de " &
        Config.Etiqueta.GRUPOS & " por " &
        Config.Etiqueta.PLANTEL & ". Emitido el " + Date.Today.ToShortDateString + "</b>"
        If GVreporte.Rows.Count > 0 Then
            BtnExportar.Visible = True
        Else
            BtnExportar.Visible = False
        End If
    End Sub

    Protected Sub GVreporte_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVreporte.RowDataBound
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        '1)OBTENGO LOS RANGOS GENERALES PARA EL SEMAFORO ROJO, AMARILL, VERDE Y AZUL
        miComando = objConexion.CreateCommand
        'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
        miComando.CommandText = "select I.MinRojo, I.MinAmarillo, I.MinVerde, I.MinAzul from Indicador I, CicloEscolar C where " + _
                                "I.IdIndicador = C.IdIndicador and C.IdCicloEscolar = " + Session("IdCicloEscolar")
        Dim MinRojo, MinAmarillo, MinVerde, MinAzul As Decimal
        Try
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            MinRojo = misRegistros.Item("MinRojo")
            MinAmarillo = misRegistros.Item("MinAmarillo")
            MinVerde = misRegistros.Item("MinVerde")
            MinAzul = misRegistros.Item("MinAzul")
            misRegistros.Close()

            'La siguiente variable la necesito mas adelante (si la calificacion es solo 1, no presenta promedios)
            Dim Quita As Byte
            If Session("TotalC") > 1 Then
                Quita = 4
            Else
                Quita = 2
            End If

            '2)HAGO UN CICLO QUE VAYA DE 1 AL TOTAL DE COLUMNAS (EL INDICE DE LAS CELDAS(COLUMNAS) INICIA EN 0 (CELLS(0)= )
            If (e.Row.Cells.Count > 1) And (e.Row.RowType = DataControlRowType.DataRow) Then  'Para que se realice la comparacion cuando haya mas de una columna y que esta sea de datos (no de Encabezado) ANTES TENÍA: And (e.Row.RowIndex > -1) Then
                For I As Byte = 3 To e.Row.Cells.Count - 1 'Inicio a partir de la tercer columna a comparar
                    If IsNumeric(Trim(e.Row.Cells(I).Text)) Then 'para que no compare celdas vacias
                        If (CDbl(e.Row.Cells(I).Text) >= MinRojo) And (CDbl(e.Row.Cells(I).Text) < MinAmarillo) Then
                            e.Row.Cells(I).BackColor = Drawing.Color.LightSalmon
                        ElseIf (CDbl(e.Row.Cells(I).Text) >= MinAmarillo) And (CDbl(e.Row.Cells(I).Text) < MinVerde) Then
                            e.Row.Cells(I).BackColor = Drawing.Color.Yellow
                        ElseIf (CDbl(e.Row.Cells(I).Text) >= MinVerde) And (CDbl(e.Row.Cells(I).Text) < MinAzul) Then
                            e.Row.Cells(I).BackColor = Drawing.Color.LightGreen
                        ElseIf (CDbl(e.Row.Cells(I).Text) >= MinAzul) And (CDbl(e.Row.Cells(I).Text) <= 100.0) Then 'La escala solo puede ser hasta 100
                            e.Row.Cells(I).BackColor = Drawing.Color.LightBlue
                        End If
                    End If
                Next

                '3)AHORA VERIFICO CALIFICACION POR CALIFICACION SI TIENEN UN INDICADOR ESPECÍFICO PARA PINTARLA EN BASE A SU COLOR
                Dim TC, Cuenta As Byte
                TC = e.Row.Cells.Count
                Cuenta = 3 'La columna 3 es la primera de las calificaciones acumuladas, luego aparecen cada 2 columnas
                Do While (Cuenta < (TC - Quita))
                    misRegistros.Close()
                    'Obtengo los datos de la Calificacion para ver si tiene asignado un Indicador específico
                    miComando.CommandText = "select * from Calificacion where IdCalificacion = " + e.Row.Cells(Cuenta).Text
                    misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                    misRegistros.Read()

                    'Verifico si tiene asignado algun Indicador Específico
                    Dim Ind = 0
                    If Not IsDBNull(misRegistros.Item("IdIndicador")) Then
                        Ind = misRegistros.Item("IdIndicador")
                    End If

                    'Si tiene un indicador, obtengo los Rangos para pintar la celda
                    If Ind > 0 Then
                        misRegistros.Close()
                        miComando.CommandText = "select * from Rango where IdIndicador=" + Ind.ToString
                        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                        Do While misRegistros.Read()
                            'Voy leyendo Rango por Rango y pinto comparando pintando donde está la celda con el valor de la calificación, que es la que está a la derecha del IdCalificacion
                            If (CDbl(e.Row.Cells(Cuenta + 1).Text) >= misRegistros.Item("Inferior")) And (CDbl(e.Row.Cells(Cuenta + 1).Text) <= misRegistros.Item("Superior")) And e.Row.Cells(Cuenta + 1).Text <> "&nbsp;" Then
                                e.Row.Cells(Cuenta + 1).BackColor = Drawing.ColorTranslator.FromHtml("#" + misRegistros.Item("Color"))
                                Exit Do
                            End If
                        Loop
                    End If
                    Cuenta += 3
                Loop
            End If ' del if (e.Row.Cells.Count > 1) And (e.Row.RowType = DataControlRowType.DataRow) Then
            misRegistros.Close()
            objConexion.Close()

            '4) Con lo siguiente oculto la columna IdGrupo, para que ese código funcione NO debe estar habilitada la PAGINACION en el GridView
            e.Row.Cells(1).Visible = False

            '5)Ahora oculto todos los IdCalificacion
            Dim TotCol, Cont As Byte
            TotCol = e.Row.Cells.Count
            Cont = 3 'La columna 3 es el Primero de los IdCalificacion, luego aparecen cada 2 columnas
            Do While (Cont < (TotCol - Quita))
                e.Row.Cells(Cont).Visible = False
                Cont += 3
            Loop

            '6)Lo siguiente es para ocultar o mostrar las columnas de calificaciones acumuladas. Quize usar un checkbox pero no hace el postback
            If Session("TotalC") > 1 Then
                Quita = 2
            Else
                Quita = 0
            End If
            If RBacumuladas.SelectedValue = "O" Then
                TotCol = e.Row.Cells.Count
                Cont = 4 'La columna 4 es la primera de las calificaciones acumuladas, luego aparecen cada 3 columnas
                Do While (Cont < (TotCol - Quita))
                    e.Row.Cells(Cont).Visible = False
                    Cont += 3
                Loop
                If Session("TotalC") > 1 Then
                    e.Row.Cells(TotCol - Quita).Visible = False
                End If
            End If
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(2).Text = Config.Etiqueta.GRUPO
        End If

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVreporte, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    Protected Sub RBacumuladas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBacumuladas.SelectedIndexChanged
        GVreporte.DataBind()
    End Sub

    Protected Sub GVindicadores_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVindicadores.RowDataBound
        If (e.Row.Cells.Count > 1) And (e.Row.RowType = DataControlRowType.DataRow) Then
            e.Row.Cells(0).BackColor = Drawing.ColorTranslator.FromHtml("#" + e.Row.Cells(1).Text)
        End If
        e.Row.Cells(1).Visible = False
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Label1.Text = Config.Etiqueta.ASIGNATURA
        Label2.Text = Config.Etiqueta.PLANTELES
        Label3.Text = Config.Etiqueta.COORDINADOR
        Label4.Text = Config.Etiqueta.GRUPOS
        Label5.Text = Config.Etiqueta.ALUMNOS
        Label6.Text = Config.Etiqueta.ASIGNATURA
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVreporte_PreRender(sender As Object, e As EventArgs) Handles GVreporte.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVreporte.Rows.Count > 0 Then
            GVreporte.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVreporte.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVreporte, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

End Class
