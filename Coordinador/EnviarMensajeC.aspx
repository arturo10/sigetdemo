﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="EnviarMensajeC.aspx.vb"
    Inherits="coordinador_EnviarMensajeC"
    MaintainScrollPositionOnPostback="true"
    ValidateRequest="false" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .style11 {
            height: 25px;
            text-align: left;
        }

        .style26 {
            height: 42px;
            text-align: left;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style23 {
            text-align: left;
            height: 27px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style24 {
            width: 143px;
            height: 5px;
        }

        .style25 {
            height: 5px;
        }

        .style21 {
            width: 273px;
        }

        .style13 {
            width: 143px;
        }

        .style28 {
            height: 14px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style29 {
            height: 25px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style30 {
            height: 42px;
        }

        .style31 {
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
        }

        .style32 {
            width: 10px;
        }

        .style33 {
            width: 10px;
            font-weight: normal;
            font-size: small;
        }

        .style34 {
            text-align: left;
            height: 5px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000000;
        }
        .style27 {
            text-align: center;
        }
        .auto-style2 {
            text-align: right;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Comunicación - Enviar Mensajes
    </h1>
    Permite enviar mensajes a los 
	<asp:Label ID="Label1" runat="server" Text="[PROFESORES]" />
    de los 
	<asp:Label ID="Label2" runat="server" Text="[PLANTELES]" />
    que Ud. tiene asignados.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table style="width: 100%;">
        <tr class="titulo">
            <td class="style26" colspan="5">
                <asp:Label ID="LblNombre" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #000099"></asp:Label>
            </td>
            <td class="style30" colspan="2"></td>
        </tr>
        <tr class="titulo">
            <td class="style29" colspan="2">
                <asp:Label ID="Label3" runat="server" Text="[CICLO]" />
            </td>
            <td class="style11" colspan="3">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Width="355px">
                </asp:DropDownList>
            </td>
            <td class="style11" colspan="2">&nbsp;</td>
        </tr>
        <tr class="titulo">
            <td class="style29" colspan="2">Enviar mensaje a</td>
            <td class="style11" colspan="3">
                <asp:DropDownList ID="DDLenvia" runat="server" Width="150px"
                    AutoPostBack="True">
                    <asp:ListItem Value="P" Selected="True">Profesores</asp:ListItem>
                    <asp:ListItem Value="A">Alumnos</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="style11" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style34" colspan="4"></td>
            <td class="style24" colspan="2"></td>
            <td class="style25"></td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="7">
                <asp:GridView ID="GVplanteles" runat="server" CellPadding="3"
                    AllowPaging="True"
                    AutoGenerateColumns="False" 
                    PageSize="8" 
                    Caption="<h3>Seleccione el PLANTEL donde pertenece el destinatario.</h3>"

                    DataSourceID="SDSplanteles" 
                    DataKeyNames="IdPlantel"
                    Width="741px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell">
                            <HeaderStyle Width="100px" />
                        </asp:CommandField>
                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel" InsertVisible="False"
                            ReadOnly="True" SortExpression="IdPlantel" Visible="False">
                            <ItemStyle Width="100px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion"
                            SortExpression="Descripcion" />
                        <asp:BoundField DataField="Municipio" HeaderText="Municipio"
                            SortExpression="Municipio" />
                        <asp:BoundField DataField="Estado" HeaderText="Estado"
                            SortExpression="Estado" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
    </table>

    <asp:Panel ID="pnlDestinatarios" runat="server" Visible="false">
        <table style="width: 100%;">
            <tr class="estandar">
                <td class="style14" colspan="7">
                    <asp:Panel ID="PnlProfesor" runat="server" Visible="True">
                        <table style="margin-left: auto; margin-right: auto;">
                            <tr class="estandar">
                                <td class="style27" colspan="7">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table style="float: right;">
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td style="text-align: left;">
                                                Nombre
                                            </td>
                                            <td style="text-align: left;">
                                                Apellidos
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Resources/imagenes/btnIcons/woocons/Search.png" Height="24" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbBuscaProfesorNombre" runat="server" Width="140"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbBuscaProfesorApellidos" runat="server" Width="140"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnBuscaProfesor" runat="server" Text="Buscar" CssClass="defaultBtn btnThemeGrey btnThemeSlick" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiaBusquedaProfesor" runat="server" Text="Limpiar Búsqueda" CssClass="defaultBtn btnThemeGrey btnThemeSlick" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" style="font-size: small;">
                                                Nota: La Búsqueda se aplica sobre el plantel seleccionado.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <uc1:msgInfo runat="server" ID="msgInfoProfesores" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="estandar">
                                <td class="style27" colspan="7" style="font-weight: bold;">Seleccione el o los 
								                    <asp:Label ID="Label4" runat="server" Text="[PROFESORES]" />
                                a los que enviará el mensaje:
                                    </td>
                            </tr>
                            <tr class="estandar">
                                <td class="style27" colspan="7">
                                    <asp:LinkButton ID="LinkButton1" runat="server"
                                        CssClass="defaultBtn btnThemeGrey btnThemeSlick">Marcar todos</asp:LinkButton>
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:LinkButton ID="LinkButton2" runat="server"
                                        CssClass="defaultBtn btnThemeGrey btnThemeSlick">Desmarcar todos</asp:LinkButton>
                                </td>
                            </tr>
                            <tr class="estandar">
                                <td class="style14" colspan="7">
                                    <asp:CheckBoxList ID="CBLprofesores" runat="server" DataSourceID="SDSprofesores"
                                        DataTextField="nombrecompleto" DataValueField="IdProfesor"
                                        Style="background-color: #efefef; text-align: left; margin-left: auto; margin-right: auto;" Width="494px">
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <asp:Panel ID="PnlAlumnos" runat="server" Visible="false">
                        <table style="width: 100%;">
                            <tr>
                                <td>
                                    <table style="float: right;">
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td style="text-align: left;">
                                                Grupo</td>
                                            <td style="text-align: left;">
                                                Grado</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Image ID="Image3" runat="server" ImageUrl="~/Resources/imagenes/btnIcons/woocons/Search.png" Height="24" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbBuscaGruposNombre" runat="server" Width="140"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="tbBuscaGruposGrado" runat="server" Width="140"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Button ID="btnBuscaGrupo" runat="server" Text="Buscar" CssClass="defaultBtn btnThemeGrey btnThemeSlick" />
                                            </td>
                                            <td>
                                                <asp:Button ID="btnLimpiaBusquedaGrupo" runat="server" Text="Limpiar Búsqueda" CssClass="defaultBtn btnThemeGrey btnThemeSlick" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" style="font-size: small;">
                                                Nota: La Búsqueda se aplica sobre el plantel seleccionado.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <uc1:msgInfo runat="server" ID="msgInfoGrupos" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="GVgrupos" runat="server"
                                        AllowSorting="false"
                                        AllowPaging="True"
                                        AutoGenerateColumns="False"
                                        PageSize="8"
                                        Caption="<h3>Seleccione el GRUPO al que pertenece el o los ALUMNOS.</h3>"

                                        DataKeyNames="IdGrupo"
                                        DataSourceID="SDSgrupos"
                                        Width="860px"

                                        CssClass="dataGrid_clear_selectable"
                                        GridLines="None">
                                        <Columns>
                                            <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                                            <asp:BoundField DataField="Grupo" HeaderText="Grupo" SortExpression="Grupo" />
                                            <asp:BoundField DataField="Grado" HeaderText="Grado" SortExpression="Grado" />
                                        </Columns>
                                        <FooterStyle CssClass="footer" />
                                        <PagerStyle CssClass="pager" />
                                        <SelectedRowStyle CssClass="selected" />
                                        <HeaderStyle CssClass="header" />
                                        <AlternatingRowStyle CssClass="altrow" />
                                    </asp:GridView>
                                    <%-- sort NO - prerender -
                                        0  Select
                                        1  IdGrupo
                                        2  GRUPO
                                        3  IdAsignatura
                                        4  ASIGNATURA
                                        5  GRADO
                                        6  IdPlantel
                                        7  PLANTEL
                                        8  IdCicloEscolar
                                        9  CICLO
                                        10 IdProgramacion
                                    --%>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="style27" style="font-weight: bold;">Seleccione <asp:Label ID="Label12" runat="server" Text="[EL]" /> o 
                                    <asp:Label ID="Label13" runat="server" Text="[LOS]" /> 
                                    <asp:Label ID="Label6" runat="server" Text="[ALUMNOS]" />
                                     a l<asp:Label ID="Label14" runat="server" Text="[O]" />s que enviará el mensaje:</td>
                            </tr>
                            <tr>
                                <td class="style27">
                                    <asp:LinkButton ID="LinkButton3" runat="server"
                                        CssClass="defaultBtn btnThemeGrey btnThemeSlick">Marcar todos</asp:LinkButton>
                                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton4" runat="server"
                                        CssClass="defaultBtn btnThemeGrey btnThemeSlick">Desmarcar todos</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBoxList ID="CBLalumnos" runat="server" DataSourceID="SDSalumnos"
                                        DataTextField="nombrecompleto" DataValueField="IdAlumno"
                                        Style="background-color: #efefef; text-align: left; margin-left: auto; margin-right: auto;" Width="494px">
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <table style="width: 100%;">
        <tr>
            <td colspan="7">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="7">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="auto-style2" colspan="2">Asunto</td>
            <td class="style14" colspan="3">
                <asp:TextBox ID="TBasunto" runat="server" MaxLength="100" Width="665px"></asp:TextBox>
            </td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="auto-style2" colspan="2">Contenido</td>
            <td class="style14" colspan="3">
                <asp:TextBox ID="TBcontenido" runat="server" 
                    ClientIDMode="Static" 
                    TextMode="MultiLine"
                    CssClass="tinymce"></asp:TextBox>
            </td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="auto-style2" colspan="2">Adjuntar archivo<br />(solo uno)</td>
            <td class="style32">
                <asp:FileUpload ID="FUadjunto" runat="server" />
            </td>
            <td class="style14">
                <asp:Button ID="BtnAdjuntar" runat="server" Text="Adjuntar"
                    CssClass="defaultBtn btnThemeGrey btnThemeSlick" />
                &nbsp;
								<asp:Button ID="BtnQuitar" runat="server" Text="Quitar"
                                    CssClass="defaultBtn btnThemeGrey btnThemeSlick" />
            </td>
            <td class="style14" colspan="3">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="auto-style2" colspan="2"></td>
            <td class="style14" colspan="5" style="text-align: left">
                <asp:LinkButton ID="lbAdjunto" runat="server" CssClass="LabelInfoDefault"></asp:LinkButton>
            </td>
        </tr>
        <tr class="estandar">
            <td class="auto-style2" colspan="2">&nbsp;</td>
            <td class="style14" colspan="3">&nbsp;</td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="auto-style2" colspan="2">&nbsp;</td>
            <td class="style14" colspan="3">
                <asp:Button ID="BtnEnviar" runat="server" Text="Enviar mensaje"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
								<asp:Button ID="BtnLimpiar" runat="server" Text="Limpiar"
                                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style22">
                &nbsp;</td>
            <td class="style21" colspan="3">
                &nbsp;</td>
            <td class="style13" colspan="2">
                &nbsp;</td>
            <td class="style15">&nbsp;</td>
        </tr>
        <tr>
            <td class="style22" style="text-align: right">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td class="style21" colspan="3">&nbsp;</td>
            <td class="style13" colspan="2">&nbsp;</td>
            <td class="style15">&nbsp;</td>
        </tr>
    </table>

    <table class="hiddenFields">
        <tr>
            <td>
                <asp:HiddenField ID="hfAdjuntoFisico" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfBuscaProfNombre" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfBuscaProfApellidos" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <%-- Los hidden fields de búsqueda se deben inicializar con la wild card 
                    para que funcionen las consultas sin buscar --%>
                <asp:HiddenField ID="hfBusquedaProfesores_isActive" runat="server" />

            </td>
            <td>
                <%-- Los hidden fields de búsqueda se deben inicializar con la wild card 
                    para que funcionen las consultas sin buscar --%>
                <asp:HiddenField ID="hfBusquedaGrupos_isActive" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hfBuscaGrupoNombre" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfBuscaGrupoGrado" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfCarpetaAdjunto" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfIsReenvio" runat="server" Value="False" />
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSprofesores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdProfesor, Nombre + ' ' + Apellidos + ' (' + Clave + ')' as nombrecompleto
from Profesor
where (Estatus = 'Activo' or Estatus = 'Suspendido') and IdPlantel = @IdPlantel 
AND Apellidos LIKE @BuscaApellidos AND Nombre LIKE @BuscaNombre 
order by Apellidos, Nombre">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVplanteles" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter Name="BuscaNombre" ControlID="hfBuscaProfNombre" PropertyName="Value" />
                        <asp:ControlParameter Name="BuscaApellidos" ControlID="hfBuscaProfApellidos" PropertyName="Value" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select distinct P.IdPlantel,P.Descripcion,P.Municipio,P.Estado 
from Plantel P, CoordinadorPlantel C
where C.IdCoordinador = @IdCoordinador
and P.IdPlantel = C.IdPlantel
order by P.Estado, P.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdCoordinador" SessionField="IdCoordinador" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdCicloEscolar], [Descripcion] FROM [CicloEscolar] WHERE ([Estatus] = @Estatus) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDScomunicacionCP" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [ComunicacionCP]"></asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSalumnos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdAlumno, ApePaterno + ' ' + ApeMaterno + ', ' + Nombre + ' (' + Matricula + ')' as nombrecompleto
from Alumno
where ((Estatus = 'Activo' or Estatus = 'Suspendido') and IdGrupo = @IdGrupo) 
order by ApePaterno, ApeMaterno, Nombre">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVgrupos" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="
SELECT G.IdGrupo, G.Descripcion Grupo, Gr.Descripcion Grado
FROM Grupo G, Grado Gr, Plantel P
WHERE G.IdGrado = Gr.IdGrado AND G.IdPlantel = P. IdPlantel AND P.IdPlantel = @IdPlantel AND G.IdCicloEscolar = @IdCicloEscolar 
AND G.Descripcion LIKE @BuscaGrupo AND Gr.Descripcion LIKE @BuscaGrado ">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVplanteles" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter Name="BuscaGrupo" ControlID="hfBuscaGrupoNombre" PropertyName="Value" />
                        <asp:ControlParameter Name="BuscaGrado" ControlID="hfBuscaGrupoGrado" PropertyName="Value" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

