﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO

Partial Class coordinador_RespuestasAbiertasCoord
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Utils.Sesion.sesionAbierta()

    Session("Login") = Trim(User.Identity.Name)

    Label1.Text = Config.Etiqueta.ALUMNOS
    Label2.Text = Config.Etiqueta.CICLO
    Label3.Text = Config.Etiqueta.INSTITUCION
    Label4.Text = Config.Etiqueta.PLANTEL
    Label5.Text = Config.Etiqueta.NIVEL
    Label6.Text = Config.Etiqueta.GRADO

    GVdatos.Caption = "<h3>Seleccione alguno de " & Config.Etiqueta.ARTDET_GRUPOS & " " &
        Config.Etiqueta.GRUPOS &
        "</h3>"
  End Sub

  Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
    DDLasignatura.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA, 0))
  End Sub

  Protected Sub DDLcalificacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacion.DataBound
    DDLcalificacion.Items.Insert(0, New ListItem("---Elija una Calificación", 0))
  End Sub

  Protected Sub DDLactividad_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLactividad.DataBound
    DDLactividad.Items.Insert(0, New ListItem("---Elija una Actividad", 0))
  End Sub

  Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
    DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
  End Sub

  Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
    DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
  End Sub

  Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
    DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRADO & " " & Config.Etiqueta.GRADO, 0))
  End Sub

  Protected Sub DDLactividad_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLactividad.SelectedIndexChanged
    TBrespuesta.Text = ""

    If DDLactividad.SelectedIndex > 0 Then
      '1)PRIMERO OBTENGO LOS RANGOS DE LOS INDICADORES PARA EL SEMAFORO ROJO, AMARILLO Y VERDE
      Dim strConexion As String
      'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
      strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
      Dim objConexion As New SqlConnection(strConexion)
      Dim miComando As SqlCommand
      Dim misRegistros As SqlDataReader

      miComando = objConexion.CreateCommand
      'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
      miComando.CommandText = "select distinct P.IdPlanteamiento, D.IdDetalleEvaluacion " + _
                              "from Evaluacion E, DetalleEvaluacion D, Planteamiento P " + _
                              "where D.IdEvaluacion = " + DDLactividad.SelectedValue.ToString + " " + _
                              "and P.IdSubtema = D.IdSubtema and P.TipoRespuesta = 'Abierta'"

      Try
        objConexion.Open()
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        If misRegistros.Read() Then
          Session("IdGrupo") = GVdatos.SelectedDataKey.Values(0).ToString
        Else
          msgInfo.show("Esta actividad no contiene reactivos de respuesta abierta.")
        End If
        misRegistros.Close()
        objConexion.Close()
        msgError.hide()
      Catch ex As Exception
        Utils.LogManager.ExceptionLog_InsertEntry(ex)
        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
      End Try
    Else
      msgInfo.hide()
    End If
  End Sub

  Protected Sub DDLasignatura_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.SelectedIndexChanged
    msgInfo.hide()
  End Sub

  Protected Sub GVrespuestas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVrespuestas.DataBound
    If GVrespuestas.Rows.Count > 0 Then
      GVrespuestas.Caption = "<font size=""2""><b>Respuestas Abiertas para la Actividad " + DDLactividad.SelectedItem.Text &
          " de " & Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO &
          " " + GVdatos.SelectedRow.Cells(2).Text + " de " &
          Config.Etiqueta.ARTDET_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA &
          " " + DDLasignatura.SelectedItem.Text + "</b></font>"
      msgInfo.hide()
      BtnExportar.Visible = True
    Else
      If (DDLactividad.SelectedIndex > 0) And (Not msgInfo.isActive()) Then
        msgInfo.show("Aún no hay registros de " &
            Config.Etiqueta.ARTDET_ALUMNOS & " " & Config.Etiqueta.ALUMNOS & ".")
      End If
      BtnExportar.Visible = False
    End If
  End Sub

  Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
    ' añado los estilos, por que el gridview no se exporta predefinidamente con css
    GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
    GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
    GVactual.GridLines = GridLines.Both

    Dim headerText As String
    ' quito la imágen de ordenamiento de los encabezados
    For Each tc As TableCell In GVactual.HeaderRow.Cells
      If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
        headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
        tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
        tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
        tc.Text = headerText ' establezco el texto simple
      End If
    Next

    'Este procedimiento convierte el GridView a Excel eliminando la primera y segunda columna
    Try
      ' antes de exportar la copia del gridview, en cada fila:
      For Each r As GridViewRow In GVactual.Rows()

        ' paso el markup limpio completo a la columna de respuesta
        r.Cells(Utils.Tables.GetColumnIndexByName(GVactual, "Respuesta")).Text =
            Regex.Replace(HttpUtility.HtmlDecode(r.Cells(Utils.Tables.GetColumnIndexByName(GVactual, "MarkupRespuesta")).Text), "<[^>]*(>|$)", String.Empty)

        ' elimino la columna de markup, y de "seleccionar"
        r.Cells.RemoveAt(Utils.Tables.GetColumnIndexByName(GVactual, "MarkupReactivo"))
        r.Cells.RemoveAt(Utils.Tables.GetColumnIndexByName(GVactual, "MarkupRespuesta"))
        r.Cells.RemoveAt(0)

        ' quito la imágen de ordenamiento de los encabezados
        If r.RowType = DataControlRowType.Header Then
          For Each tc As TableCell In r.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
              tc.Controls.RemoveAt(0)
            End If
          Next
        End If
      Next
      ' ahora quito también las columnas de seleccionar y markup en el encabezado
      GVactual.HeaderRow.Cells.RemoveAt(Utils.Tables.GetColumnIndexByName(GVactual, "MarkupReactivo"))
      GVactual.HeaderRow.Cells.RemoveAt(Utils.Tables.GetColumnIndexByName(GVactual, "MarkupRespuesta"))
      GVactual.HeaderRow.Cells.RemoveAt(0)

      If GVactual.Rows.Count.ToString + 1 < 65536 Then
        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As StringWriter = New StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form = New HtmlForm
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(GVactual)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()
      Else
        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
    End Try
  End Sub

  Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
    Convierte(GVrespuestas, "RespuestasAbiertas")
  End Sub

  Protected Sub GVrespuestas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVrespuestas.SelectedIndexChanged
    Try
      TBrespuesta.Text = HttpUtility.HtmlDecode(GVrespuestas.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(GVrespuestas, "MarkupRespuesta")).Text)
      HF1_ModalPopupExtender.Show()
    Catch ex As Exception
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GVrespuestas_PreRender(sender As Object, e As EventArgs) Handles GVrespuestas.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVrespuestas.Rows.Count > 0 Then
      GVrespuestas.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVrespuestas_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVrespuestas.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVrespuestas.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GVrespuestas_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVrespuestas.RowDataBound
    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(6).Controls(1)
      LnkHeaderText.Text = "Nombre del " & Config.Etiqueta.ALUMNO

      LnkHeaderText = e.Row.Cells(5).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.MATRICULA
    End If

    If e.Row.RowType = DataControlRowType.DataRow Then
      ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVrespuestas, "Select$" & e.Row.RowIndex).ToString())

      ' convierto el campo de respuesta de modo que se presente con estilos, no como markup
      Dim s As String = Regex.Replace(HttpUtility.HtmlDecode(e.Row.Cells(Utils.Tables.GetColumnIndexByName(GVrespuestas, "Respuesta")).Text), "<[^>]*(>|$)", String.Empty)
      Dim l As Integer = s.Length
      Dim continua As String = ""
      If l > 100 Then
        l = 100
        continua = "...(Continúa)"
      End If
      e.Row.Cells(Utils.Tables.GetColumnIndexByName(GVrespuestas, "Respuesta")).Text = s.Substring(0, l) & continua

      ' convierto el campo de planteamiento de modo que se presente con estilos, no como markup
      e.Row.Cells(Utils.Tables.GetColumnIndexByName(GVrespuestas, "Reactivo")).Text = Regex.Replace(HttpUtility.HtmlDecode(e.Row.Cells(Utils.Tables.GetColumnIndexByName(GVrespuestas, "Reactivo")).Text), "<[^>]*(>|$)", String.Empty)
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVrespuestas.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVrespuestas, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    For Each r In GVdatos.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVdatos, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

  Protected Sub GVdatos_PreRender(sender As Object, e As EventArgs) Handles GVdatos.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVdatos.Rows.Count > 0 Then
      GVdatos.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVdatos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVdatos.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GVdatos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowDataBound
    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(2).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.GRUPO

      LnkHeaderText = e.Row.Cells(4).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.GRADO

      LnkHeaderText = e.Row.Cells(5).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.NIVEL

      LnkHeaderText = e.Row.Cells(7).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.PLANTEL
    End If

    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVdatos, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub
End Class
