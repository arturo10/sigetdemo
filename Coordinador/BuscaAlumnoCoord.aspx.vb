﻿Imports Siget


Partial Class coordinador_BuscaAlumnoCoord
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = Trim(User.Identity.Name)
        TitleLiteral.Text = "Búsqueda de " & Config.Etiqueta.ALUMNOS

        Label1.Text = Config.Etiqueta.ALUMNO
        Label2.Text = Config.Etiqueta.INSTITUCION
        Label3.Text = Config.Etiqueta.ALUMNO
        Label4.Text = Config.Etiqueta.ALUMNO
        GValumnos.Caption = "<h3>Listado de " & Config.Etiqueta.ALUMNOS &
            " encontrad" & Config.Etiqueta.LETRA_ALUMNO & "s en " &
            Config.Etiqueta.ARTDET_PLANTELES & " " & Config.Etiqueta.PLANTELES &
            " que Ud. tiene asignados como " &
            Config.Etiqueta.COORDINADOR & "</h3>"

    End Sub

    Protected Sub BtnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBuscar.Click
        msgInfo.hide()
        msgSuccess.hide()
        msgError.hide()

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Try
            SDSbuscar.ConnectionString = strConexion
            SDSbuscar.SelectCommand = "select Distinct A.IdAlumno, A.Matricula, A.Nombre, A.ApePaterno, A.ApeMaterno, G.IdGrupo, G.Descripcion Grupo, Gr.Descripcion Grado, N.IdNivel, " + _
                        "N.Descripcion Nivel, P.IdPlantel, P.Descripcion Plantel, U.Login AS Usuario, U.Password, A.Estatus, A.Equipo " + _
                        "from Alumno A, Grupo G, Grado Gr, Nivel N, Plantel P, Usuario U " + _
                        "where U.IdUsuario = A.IdUsuario And G.IdGrupo = A.IdGrupo And Gr.IdGrado = G.IdGrado And N.IdNivel = Gr.IdNivel " + _
                        "And P.IdPlantel = G.IdPlantel and P.IdInstitucion = " + DDLinstitucion.SelectedValue + " and P.IdPlantel in " + _
                        "(select IdPlantel from CoordinadorPlantel where IdCoordinador = (select C1.IdCoordinador from Coordinador C1, Usuario U1 where U1.Login = '" + Session("Login").ToString + "' and C1.IdUsuario = U1.IdUsuario)) " + _
                        " and A.Matricula like '%" + TBmatricula.Text + "%' and A.Nombre like '%" + TBnombre.Text + "%' and A.ApePaterno like '%" + _
                        TBapepaterno.Text + "%' and A.ApeMaterno like '%" + TBapematerno.Text + "%' and U.Login like '%" + TBlogin.Text + "%' order by A.ApePaterno, A.ApeMaterno, A.Nombre"
            GValumnos.DataBind() 'Es necesario para que se vea el resultado
            msgInfo.show("Se encontraron " + GValumnos.Rows.Count.ToString &
                " " & Config.Etiqueta.ALUMNOS & " con los datos capturados.")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GValumnos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GValumnos.SelectedIndexChanged
        msgInfo.show("Seleccionado", HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(3).Text) + " " + HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(4).Text) + " " + HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(5).Text))
        LblAlumno.Text = HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(3).Text) + " " + HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(4).Text) + " " + HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(5).Text)
        TBpasswordOK.Text = HttpUtility.HtmlDecode(Trim(GValumnos.SelectedRow.Cells(14).Text))
    End Sub

    Protected Sub BtnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnActualizar.Click
        msgSuccess.hide()
        msgError.hide()

        Try
            'Actualizo password en tabla de seguridad, no puedo cambiar Login
            Dim u As MembershipUser
            u = Membership.GetUser(Trim(HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(13).Text))) 'Obtiene datos del usuario con el LOGIN
            u.ChangePassword(Trim(HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(14).Text)), Trim(TBpasswordOK.Text)) 'canbia PASSWORD
            'u.Email = TBemail.Text
            'Membership.UpdateUser(u)
            SDSusuarios.UpdateCommand = "SET dateformat dmy; UPDATE Usuario set Password = '" + Trim(TBpasswordOK.Text) + "' where Login = '" + Trim(HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(13).Text)) + "'"
            SDSusuarios.Update()
            GValumnos.DataBind()
            msgSuccess.show("Éxito", "El password de " &
                Config.Etiqueta.ARTDET_ALUMNO & " " & Config.Etiqueta.ALUMNO &
                " ha sido cambiado")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLimpiar.Click
        TBmatricula.Text = ""
        TBnombre.Text = ""
        TBapepaterno.Text = ""
        TBapepaterno.Text = ""
        TBlogin.Text = ""
        msgInfo.hide()
        msgSuccess.hide()
        msgError.hide()
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GValumnos_PreRender(sender As Object, e As EventArgs) Handles GValumnos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GValumnos.Rows.Count > 0 Then
            GValumnos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GValumnos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GValumnos.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GValumnos, "Select$" & e.Row.RowIndex).ToString())
        End If

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(1).Text = "Id_" & Config.Etiqueta.ALUMNO

            e.Row.Cells(6).Text = "Id_" & Config.Etiqueta.GRUPO

            e.Row.Cells(7).Text = Config.Etiqueta.GRUPO

            e.Row.Cells(8).Text = Config.Etiqueta.GRADO

            e.Row.Cells(9).Text = "Id_" & Config.Etiqueta.NIVEL

            e.Row.Cells(10).Text = Config.Etiqueta.NIVEL

            e.Row.Cells(11).Text = "Id_" & Config.Etiqueta.PLANTEL

            e.Row.Cells(12).Text = Config.Etiqueta.PLANTEL

            e.Row.Cells(16).Text = Config.Etiqueta.EQUIPO
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GValumnos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GValumnos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

End Class
