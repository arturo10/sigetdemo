﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="LeerMensajeEntradaC.aspx.vb" 
    Inherits="coordinador_LeerMensajeEntradaC" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style33 {
            width: 10px;
            font-weight: normal;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>
        Comunicación - Leer Mensajes Recibidos
    </h1>
    Permite revisar los mensajes recibidos de los 
		<asp:Label ID="Label1" runat="server" Text="[PROFESORES]" /> y 
    <asp:Label ID="Label3" runat="server" Text="[ALUMNOS]" /> 
    asignados a los 
		<asp:Label ID="Label2" runat="server" Text="[PLANTELES]" />
    que Ud. coordina.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table style="width: 100%;">
        <tr>
            <td>
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td>
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GVmensajesProfesores" runat="server" 
                    AllowSorting="True"
                    AutoGenerateColumns="False" 
                    Caption="<h3>Mensajes de Profesores</h3>"

                    DataKeyNames="IdComunicacionCP,Login"
                    DataSourceID="SDSmensajesProfesores" 
                    Width="882px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField SelectText="Leer" ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha" />
                        <asp:BoundField DataField="Asunto" HeaderText="Asunto"
                            SortExpression="Asunto" />
                        <asp:BoundField DataField="Profesor que Envía" HeaderText="Profesor que Envía"
                            ReadOnly="True" SortExpression="Profesor que Envía" />
                        <asp:BoundField DataField="Plantel" HeaderText="Plantel"
                            SortExpression="Plantel" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                            SortExpression="Estatus" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  select
	                1  idcomunicacioncp
	                2  fecha
	                3  asunto
	                4  PROFESOR que envia
	                5  PLANTEL
	                6  estatus
	                7  login Usuario
	                --%>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GVmensajesAlumnos" runat="server" 
                    AllowSorting="True"
                    AutoGenerateColumns="False" 
                    Caption="<h3>Mensajes de Alumnos</h3>"

                    DataKeyNames="IdComunicacionCA,Login"
                    DataSourceID="SDSmensajesAlumnos" 
                    Width="882px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField SelectText="Leer" ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha" />
                        <asp:BoundField DataField="Asunto" HeaderText="Asunto"
                            SortExpression="Asunto" />
                        <asp:BoundField DataField="Alumno que Envía" HeaderText="Alumno que Envía"
                            ReadOnly="True" SortExpression="Alumno que Envía" />
                        <asp:BoundField DataField="Plantel" HeaderText="Plantel"
                            SortExpression="Plantel" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                            SortExpression="Estatus" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  select
	                1  idcomunicacioncp
	                2  fecha
	                3  asunto
	                4  PROFESOR que envia
	                5  PLANTEL
	                6  estatus
	                7  login Usuario
	                --%>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSmensajesProfesores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select C.IdComunicacionCP, C.Fecha, C.Asunto, P.Nombre + ' ' + P.Apellidos as 'Profesor que Envía', Pl.Descripcion as Plantel, C.Estatus, U.Login
from ComunicacionCP C, Profesor P, Usuario U,Plantel Pl
where C.IdCoordinador = @IdCoordinador
and P.IdProfesor = C.IdProfesor and Pl.IdPlantel = P.IdPlantel and U.Idusuario = P.IdUsuario
and C.Estatus &lt;&gt; 'Baja' and C.Sentido = 'R'
order by C.Fecha DESC">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdCoordinador" SessionField="LeerMensajeEntradaC_IdCoordinador" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>
                <asp:SqlDataSource ID="SDSmensajesAlumnos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="
select C.IdComunicacionCA, C.Fecha, C.Asunto, A.Nombre + ' ' + A.ApePaterno + ' ' + A.ApeMaterno AS 'Alumno que Envía', P.Descripcion as Plantel, C.Estatus, U.Login
from ComunicacionCA C, Alumno A, Usuario U, Plantel P
where C.IdCoordinador = @IdCoordinador
and A.IdAlumno = C.IdAlumno and P.IdPlantel = A.IdPlantel and U.Idusuario = A.IdUsuario
and C.Estatus &lt;&gt; 'Baja' and C.Sentido = 'R'
order by C.Fecha DESC">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdCoordinador" SessionField="LeerMensajeEntradaC_IdCoordinador" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

