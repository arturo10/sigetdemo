﻿Imports Siget

Imports System.IO
Imports System.Data.SqlClient
Imports System.Data

Partial Class coordinador_DetalleActividadesFecha
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = User.Identity.Name   'Se utiliza para sacar las instituciones
        TitleLiteral.Text = "Actividades Usando Filtros"

        Label1.Text = Config.Etiqueta.ASIGNATURA
        Label2.Text = Config.Etiqueta.ALUMNO
        Label3.Text = Config.Etiqueta.GRADO
        Label4.Text = Config.Etiqueta.INSTITUCION
        Label5.Text = Config.Etiqueta.CICLO
        Label6.Text = Config.Etiqueta.NIVEL
        Label7.Text = Config.Etiqueta.GRADO
        Label8.Text = Config.Etiqueta.ASIGNATURA
        Label9.Text = Config.Etiqueta.ALUMNOS
        CBfechas.Text = "Incluir un rango de fecha en que los " & Config.Etiqueta.ALUMNOS & " terminaron la actividad"

        Dim strConexion As String
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        miComando = objConexion.CreateCommand
        'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
        miComando.CommandText = "select C.IdCoordinador from Coordinador C, Usuario U where C.IdUsuario = U.IdUsuario " + _
                                "and U.Login = '" + Trim(User.Identity.Name) + "'"
        Try
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            Session("IdCoordinador") = misRegistros.Item("IdCoordinador")
            misRegistros.Close()
            objConexion.Close()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try

        'No asigné el parametro de fecha directamente al control porque el script espera a que le haya pasado todos los
        'parámetros para ejecutar la consulta y en este caso a veces se ejecuta sin que se haya seleccionado fechas
        If Not (PnlFechas.Visible) Then 'Ya que este procedimiento se ejecuta cada que se activa un post (al seleccionar el RB)
            Session("De") = "ninguna"
            Session("Al") = "ninguna"
        End If
    End Sub

    Protected Sub CBfechas_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CBfechas.CheckedChanged
        If Not (CBfechas.Checked) Then
            Session("De") = "ninguna"
            Session("Al") = "ninguna"
            PnlFechas.Visible = False
            TBde.Text = ""
            TBal.Text = ""
        Else
            PnlFechas.Visible = True
        End If
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        'Este procedimiento convierte el GridView a Excel eliminando la primera y segunda columna
        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub FormateaDatos(ByRef input As GridView)
        For Each r As GridViewRow In input.Rows
            '1)PRIMERO OBTENGO LOS RANGOS DE LOS INDICADORES PARA EL SEMAFORO ROJO, AMARILLO Y VERDE
            Dim strConexion As String
            'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objConexion As New SqlConnection(strConexion)
            Dim miComando As SqlCommand
            Dim misRegistros As SqlDataReader

            miComando = objConexion.CreateCommand
            'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
            miComando.CommandText = "select I.MinAmarillo, I.MinVerde, I.MinAzul from Indicador I, CicloEscolar C where " + _
                                    "I.IdIndicador = C.IdIndicador and C.IdCicloEscolar = " + DDLcicloescolar.SelectedValue
            Dim MinAmarillo, MinVerde, MinAzul As Decimal
            Try
                objConexion.Open()
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read()
                MinAmarillo = misRegistros.Item("MinAmarillo")
                MinVerde = misRegistros.Item("MinVerde")
                MinAzul = misRegistros.Item("MinAzul")
                misRegistros.Close()
                objConexion.Close()
                msgError.hide()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
            End Try

            '2)HAGO UN CICLO QUE VAYA DE 1 AL TOTAL DE COLUMNAS (EL INDICE DE LAS CELDAS(COLUMNAS) INICIA EN 0 (CELLS(0)= )
            'La parte de la condición: (r.RowIndex > -1) es porque en este caso que comparo fechas estaba pintando tambien el encabezado (fila -1), que tambien comparaba
            If (r.Cells.Count > 1) And (r.RowIndex > -1) Then  'Para que se realice la comparacion cuando haya mas de una columna
                For I As Integer = 8 To r.Cells.Count - 1 'Inicio a partir de la cuarta columna a comparar (o,1,2,3...)
                    If IsNumeric(Trim(r.Cells(I).Text)) Then 'para que no compare celdas vacias
                        If (CDbl(r.Cells(I).Text) < MinAmarillo) Then
                            r.Cells(I).BackColor = Drawing.Color.LightSalmon
                        ElseIf (CDbl(r.Cells(I).Text) < MinVerde) Then
                            r.Cells(I).BackColor = Drawing.Color.Yellow
                        ElseIf (CDbl(r.Cells(I).Text) < MinAzul) Then
                            r.Cells(I).BackColor = Drawing.Color.LightGreen
                        Else 'Significa que es mayor o igual al MinAzul:
                            r.Cells(I).BackColor = Drawing.Color.LightBlue
                        End If
                    End If
                Next
            End If

            If r.RowType = DataControlRowType.DataRow Then
                If r.Cells(7).Text <> "&nbsp;" Then
                    r.Cells(7).Text = CDate(r.Cells(7).Text).ToString("dd/MM/yyyy")
                End If
            End If

            ' Etiquetas de GridView
            If r.RowType = DataControlRowType.Header Then
                Dim LnkHeaderText As LinkButton = r.Cells(4).Controls(1)
                LnkHeaderText.Text = Config.Etiqueta.GRUPO

                LnkHeaderText = r.Cells(5).Controls(1)
                LnkHeaderText.Text = Config.Etiqueta.PLANTEL

                LnkHeaderText = r.Cells(8).Controls(1)
                LnkHeaderText.Text = Config.Etiqueta.EQUIPO
            End If
        Next
    End Sub

    Protected Sub LbnConsultar_Click(sender As Object, e As EventArgs) Handles LbnConsultar.Click
        If Page.IsValid Then
            msgError.hide()
            msgInfo.hide()

            Dim tvp As DataTable = New DataTable()
            tvp.Columns.Add("Id", GetType(Integer))

            For Each item As ListItem In CblAsignaturas.Items
                If item.Selected Then
                    tvp.Rows.Add(item.Value)
                End If
            Next

            If tvp.Rows.Count > 0 Then
                Dim dst As DataSet = New DataSet()

                Using conn As SqlConnection = New SqlConnection(DataAccess.
                                                                DataAccessUtils.
                                                                SadcomeConnectionString)
                    conn.Open()
                    Using sqlAdapter As SqlDataAdapter = New SqlDataAdapter()
                        sqlAdapter.SelectCommand = New SqlCommand()
                        sqlAdapter.SelectCommand.Connection = conn
                        sqlAdapter.SelectCommand.CommandText = "SP_actividadescriterios"
                        sqlAdapter.SelectCommand.CommandType = CommandType.StoredProcedure
                        sqlAdapter.SelectCommand.CommandTimeout = 300

                        sqlAdapter.SelectCommand.Parameters.AddWithValue("@IdCicloEscolar", DDLcicloescolar.SelectedValue)
                        sqlAdapter.SelectCommand.Parameters.AddWithValue("@IdGrado", DDLgrado.SelectedValue)
                        sqlAdapter.SelectCommand.Parameters.AddWithValue("@Estatus", RBestatus.SelectedValue)
                        sqlAdapter.SelectCommand.Parameters.AddWithValue("@De", Session("De").ToString())
                        sqlAdapter.SelectCommand.Parameters.AddWithValue("@Al", Session("Al").ToString())
                        sqlAdapter.SelectCommand.Parameters.AddWithValue("@IdCoordinador", Session("IdCoordinador").ToString())
                        Dim tvparam As SqlParameter = sqlAdapter.SelectCommand.Parameters.AddWithValue("@IdentificadoresAsignatura", tvp)
                        tvparam.SqlDbType = SqlDbType.Structured

                        sqlAdapter.Fill(dst)

                        Dim gvExportar As GridView = New GridView()

                        gvExportar.DataSource = dst
                        gvExportar.DataBind()

                        If gvExportar.Rows.Count = 0 Then
                            msgInfo.show("No hay " & Config.Etiqueta.ALUMNOS & " con en este criterio.")
                        ElseIf gvExportar.HeaderRow.Cells.Count <= 8 Then
                            msgInfo.show("No hay actividades incluidas en el criterio señalado.")
                        Else
                            FormateaDatos(gvExportar)
                            Convierte(gvExportar, "Resultados de Actividades")
                        End If

                    End Using
                End Using
            Else
                msgInfo.show("No se han seleccionado " & Config.Etiqueta.ASIGNATURAS)
            End If
        Else
            msgError.show("Existen errores en el formulario. Por favor corríjalos.")
        End If

    End Sub

    Protected Sub lbnSeleccionarTodasLasAsignaturas_Click(sender As Object, e As EventArgs) Handles lbnSeleccionarTodasLasAsignaturas.Click
        For Each item As ListItem In CblAsignaturas.Items
            item.Selected = True
        Next
    End Sub

    Protected Sub lbnDeseleccionarTodasLasAsignaturas_Click(sender As Object, e As EventArgs) Handles lbnDeseleccionarTodasLasAsignaturas.Click
        For Each item As ListItem In CblAsignaturas.Items
            item.Selected = False
        Next
    End Sub

    Protected Sub CV_TBde_ServerValidate(source As Object, args As ServerValidateEventArgs)
        Try
            Session("De") = CDate(TBde.Text)
            args.IsValid = True
        Catch ex As Exception
            args.IsValid = False
        End Try
    End Sub

    Protected Sub CV_TBal_ServerValidate(source As Object, args As ServerValidateEventArgs)
        Try
            Session("Al") = CDate(TBal.Text)
            args.IsValid = True
        Catch ex As Exception
            args.IsValid = False
        End Try
    End Sub

    Protected Sub DDLcicloescolar_DataBound(sender As Object, e As EventArgs) Handles DDLcicloescolar.DataBound
    DDLcicloescolar.Items.Insert(0, New ListItem(Siget.Lang.FileSystem.General.ddl_elija_un_elemento.Item(Session("Usuario_Idioma").ToString()), "0"))
  End Sub

  Protected Sub DDLnivel_DataBound(sender As Object, e As EventArgs) Handles DDLnivel.DataBound
    DDLnivel.Items.Insert(0, New ListItem(Siget.Lang.FileSystem.General.ddl_elija_un_elemento.Item(Session("Usuario_Idioma").ToString()), "0"))
  End Sub

  Protected Sub DDLgrado_DataBound(sender As Object, e As EventArgs) Handles DDLgrado.DataBound
    DDLgrado.Items.Insert(0, New ListItem(Siget.Lang.FileSystem.General.ddl_elija_un_elemento.Item(Session("Usuario_Idioma").ToString()), "0"))
  End Sub
End Class
