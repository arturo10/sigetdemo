<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="ReporteGeneral.aspx.vb"
    Inherits="coordinador_ReporteGeneral"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style11 {
            height: 37px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style13 {
            width: 143px;
        }

        .style14 {
            text-align: left;
        }

        .style15 {
        }

        .style17 {
        }

        .style19 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style20 {
            width: 273px;
            height: 18px;
        }

        .style21 {
            width: 273px;
        }

        .style22 {
            text-align: left;
        }

        .style23 {
            text-align: left;
            height: 43px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
        }

        .style24 {
            width: 143px;
            height: 43px;
        }

        .style25 {
            height: 43px;
        }

        .style26 {
            text-align: right;
            height: 18px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style31 {
            font-size: x-small;
            color: #000099;
            text-align: left;
        }

        .style33 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
        }

        .style32 {
            height: 22px;
            width: 479px;
            font-weight: normal;
            font-size: small;
            text-align: left;
        }

        .style34 {
            font-size: x-small;
            color: #000099;
            height: 33px;
            text-align: left;
        }

        .style37 {
            color: #000099;
        }

        .style38 {
            font-weight: bold;
        }

        .style45 {
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Resultados -
        <asp:Label ID="Label11" runat="server" Text="[ASIGNATURA]" />
    </h1>
    Reporte general de resultados por 
	<asp:Label ID="Label1" runat="server" Text="[ASIGNATURA]" />
    mostrando la calificaci�n promedio y la acumulada. Incluye todos los 
	<asp:Label ID="Label2" runat="server" Text="[PLANTELES]" />
    de los que Usted es 
	<asp:Label ID="Label3" runat="server" Text="[COORDINADOR]" />
    y se puede ver el detalle por 
	<asp:Label ID="Label4" runat="server" Text="[GRADOS]" />
    y por 
	<asp:Label ID="Label5" runat="server" Text="[ALUMNOS]" />
    .
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style23" colspan="3">Indique los siguientes datos 
                para generar el reporte:</td>
            <td class="style24" colspan="3"></td>
            <td class="style25"></td>
        </tr>
        <tr>
            <td class="style26">
                <asp:Label ID="Label6" runat="server" Text="[INSTITUCION]" />
            </td>
            <td class="style20" colspan="2">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    Height="22px" Width="350px" DataSourceID="SDSinstituciones"
                    DataTextField="Descripcion" DataValueField="IdInstitucion">
                </asp:DropDownList>
            </td>
            <td class="style17" colspan="4" rowspan="5">
                <asp:GridView ID="GVmaximos" runat="server" 
                    AutoGenerateColumns="False"

                    DataSourceID="SDSmaximascal" 
                    Width="366px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Calificacion" HeaderText="Calificaci�n"
                            SortExpression="Calificacion" />
                        <asp:BoundField DataField="Maximo" HeaderText="% M�ximo Posible" ReadOnly="True"
                            SortExpression="Maximo" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style26">
                <asp:Label ID="Label7" runat="server" Text="[CICLO]" />
            </td>
            <td class="style21" colspan="2">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="duracion"
                    DataValueField="IdCicloEscolar" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style19">
                <asp:Label ID="Label8" runat="server" Text="[NIVEL]" />
            </td>
            <td class="style21" colspan="2">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style19">
                <asp:Label ID="Label9" runat="server" Text="[GRADO]" />
            </td>
            <td class="style21" colspan="2">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                    Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style19">
                <asp:Label ID="Label10" runat="server" Text="[ASIGNATURA]" />
            </td>
            <td class="style21" colspan="2">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSasignatura" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Height="22px" Width="350px"
                    onchange="setSpinner('')">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style31">&nbsp;</td>
            <td class="style31" colspan="4">&nbsp;</td>
            <td class="style31" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style31">&nbsp;</td>
            <td class="style31" colspan="4">
                <asp:RadioButtonList ID="RBacumuladas" runat="server" AutoPostBack="True"
                    RepeatDirection="Horizontal"
                    onchange="setSpinner('')"
                    Style="font-weight: 700; color: #000066; font-size: small">
                    <asp:ListItem Value="M">Mostrar acumuladas</asp:ListItem>
                    <asp:ListItem Value="O" Selected="True">Ocultar acumuladas</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td class="style31" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style22" colspan="7">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Men� </asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <div id="spin" style="display:none;" class="spinnerLoading">
                    <div style="padding-top: 80px; text-align: center;">
                        Trabajando...
                    </div>
			    </div>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="7">
                <asp:GridView ID="GVsalida" runat="server" 

                    DataKeyNames="IdPlantel" 
                    Width="919px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort NO, dynamic - rowdatabound
                    'select P.IdPlantel, P.Descripcion AS Plantel'
	                0  select
	                1  idplantel
	                2  PLANTEL
	                --%>
            </td>
        </tr>
        <tr class="estandar">
            <td colspan="7">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="7">
                <span class="style38">NOTA:</span><span class="style37"> Para el c�lculo de las Calificaciones Acumuladas, cada actividad representa un porcentaje de la 
                Calificaci�n, por lo tanto, el porcentaje se ir� acumulando hasta llegar al 100% 
                conforme se vaya completando la cantidad de actividades que componen cada 
                calificaci�n. 
                <br />
                    El Aprovechamiento de actividades es el resultado directo del promedio obtenido 
                en las actividades que se lleven 
                realizadas, independientemente de la ponderaci�n que representen para una Calificaci�n.</span></td>
        </tr>
        <tr class="estandar">
            <td class="style45">
                <asp:GridView ID="GVindicadores" runat="server" 
                    AutoGenerateColumns="False"
                    Caption="INDICADORES ESPEC�FICOS" 

                    DataSourceID="SDSindicadores" 
                    DataKeyNames="Color"
                    Width="237px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Categoria" HeaderText="Categor�a"
                            SortExpression="Categoria" />
                        <asp:BoundField DataField="Color" HeaderText="Color" SortExpression="Color" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
            <td class="style45">&nbsp;</td>
            <td class="style45" colspan="5">
                <asp:GridView ID="GVindicadores2" runat="server" 
                    AutoGenerateColumns="False"
                    Caption="ESCALA DE VALORES  (PARA PROMEDIOS)" 

                    DataSourceID="SDSindicadores2" 
                    Width="334px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="MinAzul" HeaderText="M�nimo para EXCELENTE"
                            SortExpression="MinAzul">
                            <ItemStyle HorizontalAlign="Right" BackColor="LightBlue" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinVerde" HeaderText="M�nimo para BIEN"
                            SortExpression="MinVerde">
                            <ItemStyle HorizontalAlign="Right" BackColor="LightGreen" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinAmarillo" HeaderText="M�nimo para REGULAR"
                            SortExpression="MinAmarillo">
                            <ItemStyle HorizontalAlign="Right" BackColor="Yellow" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinRojo" HeaderText="M�nimo para MAL"
                            SortExpression="MinRojo">
                            <ItemStyle HorizontalAlign="Right" BackColor="LightSalmon" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style22">
                &nbsp;</td>
            <td class="style21" colspan="2">
                &nbsp;</td>
            <td class="style13" colspan="3">
                &nbsp;</td>
            <td class="style15">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style22" colspan="3">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Men� </asp:HyperLink>
            </td>
            <td class="style13" colspan="3">
                &nbsp;</td>
            <td class="style15">
                &nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT distinct I.IdInstitucion, I.Descripcion 
FROM Institucion I, CoordinadorPlantel CP, Plantel P
WHERE I.Estatus = 'Activo' and I.IdInstitucion = P.IdInstitucion
and P.IdPlantel = CP.IdPlantel and 
CP.IdCoordinador = (select C.IdCoordinador from Coordinador C, Usuario U where
U.Login = @Login and C.IdUsuario = U.IdUsuario)
ORDER BY I.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSmaximascal" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Cal.Descripcion as Calificacion, (select SUM(Porcentaje) from Evaluacion where IdCalificacion = Cal.IdCalificacion and InicioContestar &lt;= getdate()) as Maximo
from Calificacion Cal
where Cal.IdCalificacion in (select Distinct C.IdCalificacion from Calificacion C, Evaluacion E  where (C.IdAsignatura = @IdAsignatura and C.IdCicloEscolar = @IdCicloEscolar) and
(E.IdCalificacion = C.IdCalificacion ))
order by Cal.Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdCicloEscolar, Descripcion as  duracion
from CicloEscolar
where Estatus = 'Activo'
order by Descripcion"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT DISTINCT N.IdNivel, N.Descripcion
FROM         Nivel AS N INNER JOIN
                      Escuela AS E ON N.IdNivel = E.IdNivel INNER JOIN
                      CoordinadorPlantel AS CP ON E.IdPlantel = CP.IdPlantel INNER JOIN
                      Coordinador AS C ON CP.IdCoordinador = C.IdCoordinador INNER JOIN
                      Usuario AS U ON C.IdUsuario = U.IdUsuario
WHERE     (U.Login = @Login) and (N.Estatus = 'Activo')
ORDER BY N.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE ([IdNivel] = @IdNivel) 
and (Estatus = 'Activo') ORDER BY [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignatura" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdAsignatura], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) 
AND IdAsignatura in (select IdAsignatura from Calificacion where IdCicloEscolar = @IdCicloEscolar)
ORDER BY [IdArea]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSindicadores2" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT MinRojo, MinAmarillo, MinVerde, MinAzul FROM Indicador, CicloEscolar
where Indicador.IdIndicador = CicloEscolar.IdIndicador and IdCicloEscolar = @IdCicloEscolar">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSindicadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT DISTINCT Categoria, Color
FROM         Rango
WHERE     IdIndicador in 
(select IdIndicador from Calificacion where IdAsignatura = @IdAsignatura and IdCicloEscolar = @IdCicloEscolar)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
    </table>
</asp:Content>

