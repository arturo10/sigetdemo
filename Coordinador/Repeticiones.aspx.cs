﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

using System.Text;
using System.Web.UI.HtmlControls;
using System.IO;

using Siget;
using Siget.Config;

public partial class coordinador_Repeticiones : System.Web.UI.Page
{

    #region init

    protected void Page_Load(object sender, EventArgs e)
    {
        Siget.Utils.Sesion.sesionAbierta();

        msgError.hide();

        // personaliza las etiquetas de la página
        Literal2.Text = Etiqueta.ARTIND_ASIGNATURA;
        Literal3.Text = Etiqueta.ASIGNATURA;
        Literal8.Text = Etiqueta.ARTDET_ALUMNOS;
        Literal9.Text = Etiqueta.ARTIND_GRADO;
        Literal10.Text = Etiqueta.GRADO;
        Literal1.Text = Etiqueta.ALUMNOS;
        Literal4.Text = Etiqueta.CICLO;
        Literal5.Text = Etiqueta.NIVEL;
        Literal6.Text = Etiqueta.GRADO;
        Literal7.Text = Etiqueta.ASIGNATURA;

        if (tbFechaInicio.Text.Trim().Equals(""))
            hfInicio.Value = "01/01/1900";
        else
            hfInicio.Value = tbFechaInicio.Text.Trim();

        if (tbFechaFin.Text.Trim().Equals(""))
            hfFin.Value = "31/12/2050";
        else
            hfFin.Value = tbFechaFin.Text.Trim();
    }

    #endregion

    protected void FormateaDatos(GridView input)
    {
        foreach (GridViewRow r in input.Rows)
        {
            if (r.RowType == DataControlRowType.Header)
            {
                ((LinkButton)r.Cells[0].Controls[0]).Text = Etiqueta.MATRICULA;
                ((LinkButton)r.Cells[1].Controls[0]).Text = "Nombre de " + Etiqueta.ARTDET_ALUMNO + " " + Etiqueta.ALUMNO;
                ((LinkButton)r.Cells[2].Controls[0]).Text = Etiqueta.GRUPO;
                ((LinkButton)r.Cells[3].Controls[0]).Text = Etiqueta.PLANTEL;
                ((LinkButton)r.Cells[6].Controls[0]).Text = Etiqueta.EQUIPO;
                ((LinkButton)r.Cells[7].Controls[0]).Text = Etiqueta.CALIFICACION;
            }

            // 1)PRIMERO OBTENGO LOS RANGOS DE LOS INDICADORES PARA EL SEMAFORO ROJO, AMARILLO Y VERDE
            string strConexion;
            // TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            strConexion = ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString;
            SqlConnection objConexion = new SqlConnection(strConexion);
            SqlCommand miComando;
            SqlDataReader misRegistros;

            miComando = objConexion.CreateCommand();
            // El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
            miComando.CommandText = "select I.MinAmarillo, I.MinVerde, I.MinAzul from Indicador I, CicloEscolar C where " +
                                    "I.IdIndicador = C.IdIndicador and C.IdCicloEscolar = " + DDLcicloescolar.SelectedValue;
            Decimal MinAmarillo = 0, MinVerde = 0, MinAzul = 0;
            try
            {
                objConexion.Open();
                misRegistros = miComando.ExecuteReader(); // Creo conjunto de registros
                misRegistros.Read();
                MinAmarillo = Decimal.Parse(misRegistros["MinAmarillo"].ToString());
                MinVerde = Decimal.Parse(misRegistros["MinVerde"].ToString());
                MinAzul = Decimal.Parse(misRegistros["MinAzul"].ToString());
                misRegistros.Close();
                objConexion.Close();
                msgError.hide();
            }
            catch (Exception ex)
            {
                Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
                msgError.show(Siget.Lang.FileSystem.General.MSG_ERROR[Session["Usuario_Idioma"].ToString()], "De refrescar (F5) a esta página. " + ex.Message.ToString());
            }

            // 2)HAGO UN CICLO QUE VAYA DE 1 AL TOTAL DE COLUMNAS (EL INDICE DE LAS CELDAS(COLUMNAS) INICIA EN 0 (CELLS(0)= )
            // La parte de la condición: (r.RowIndex > -1) es porque en este caso que comparo fechas estaba pintando tambien el encabezado (fila -1), que tambien comparaba
            Decimal a;
            if ((r.Cells.Count > 1) && (r.RowIndex > -1))  // Para que se realice la comparacion cuando haya mas de una columna
                if (Decimal.TryParse(r.Cells[9].Text.Trim(), out a)) // para que no compare celdas vacias
                    if (Decimal.Parse(r.Cells[9].Text) < MinAmarillo)
                        r.Cells[9].BackColor = System.Drawing.Color.LightSalmon;
                    else if (Decimal.Parse(r.Cells[9].Text) < MinVerde)
                        r.Cells[9].BackColor = System.Drawing.Color.Yellow;
                    else if (Decimal.Parse(r.Cells[9].Text) < MinAzul)
                        r.Cells[9].BackColor = System.Drawing.Color.LightGreen;
                    else // Significa que es mayor o igual al MinAzul:
                        r.Cells[9].BackColor = System.Drawing.Color.LightBlue;
        }
    }

    protected void SDSRepeticiones_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        try
        {
            DateTime.Parse(hfInicio.Value.Trim());
        }
        catch (Exception)
        {
            msgError.show("La fecha inicial no es válida.");
            e.Cancel = true;
        }

        try
        {
            DateTime.Parse(hfFin.Value.Trim());
        }
        catch (Exception)
        {
            msgError.show("La fecha límite no es válida.");
            e.Cancel = true;
        }
    }

    protected void Convierte(GridView GVactual, string archivo)
    {
        // añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef");
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Color.Table_HeaderRow_Background);
        GVactual.GridLines = GridLines.Both;

        string headerText;
        // quito la imágen de ordenamiento de los encabezados
        foreach (TableCell tc in GVactual.HeaderRow.Cells)
        {
            if (tc.HasControls()) // evita añadir íconos en celdas que no tengan link
            {
                headerText = ((LinkButton)tc.Controls[0]).Text; // obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0); // quito el ícono de ordenamiento
                //tc.Controls.RemoveAt(0); // quito el linkbutton de ordenamiento
                tc.Text = headerText; // establezco el texto simple
            }
        }

        //if (GVactual.Rows.Count + 1 < 65536)
        //{
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Page pagina = new Page();
            HtmlForm form = new HtmlForm();
            pagina.EnableEventValidation = false;
            pagina.DesignerInitialize();
            pagina.Controls.Add(form);
            form.Controls.Add(GVactual);
            pagina.RenderControl(htw);
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(sb.ToString());
            Response.End();
        //}
        //else
        //    msgError.show(Siget.Lang.FileSystem.General.MSG_ERROR[Session["Usuario_Idioma"].ToString()], "Demasiados registros para Exportar a Excel.");

    }

    protected void CV_TBde_ServerValidate(Object source, ServerValidateEventArgs args)
    {
        try
        {
            hfInicio.Value = DateTime.Parse(tbFechaInicio.Text).ToShortDateString();
            args.IsValid = true;
        }
        catch
        {
            args.IsValid = false;
        }
    }

    protected void CV_TBal_ServerValidate(Object source, ServerValidateEventArgs args)
    {
        try
        {
            hfFin.Value = DateTime.Parse(tbFechaFin.Text).ToShortDateString();
            args.IsValid = true;
        }
        catch
        {
            args.IsValid = false;
        }
    }
    protected void LbnConsultar_Click(object sender, EventArgs e)
    {
        
        if (Page.IsValid)
        {
            msgError.hide();
            msgInfo.hide();

                DataSet dst = new DataSet();

                using (SqlConnection conn = new SqlConnection(Siget.DataAccess.
                                                                DataAccessUtils.
                                                                SadcomeConnectionString)) {
                    conn.Open();

                    using (SqlDataAdapter sqlAdapter = new SqlDataAdapter()) 
                    {
                        sqlAdapter.SelectCommand = new SqlCommand();
                        sqlAdapter.SelectCommand.Connection = conn;
                        sqlAdapter.SelectCommand.CommandText = SDSRepeticiones.SelectCommand;
                        sqlAdapter.SelectCommand.CommandTimeout = 300;

                        sqlAdapter.SelectCommand.Parameters.AddWithValue("@IdAsignatura", DDLasignatura.SelectedValue);

                        sqlAdapter.SelectCommand.Parameters.AddWithValue("@Inicio", hfInicio.Value);
                        sqlAdapter.SelectCommand.Parameters.AddWithValue("@Fin", hfFin.Value);

                        sqlAdapter.Fill(dst);

                        GridView gvExportar = new GridView();

                        gvExportar.DataSource = dst;
                        gvExportar.DataBind();

                        if (gvExportar.Rows.Count == 0)
                            msgInfo.show("No hay " + Siget.Config.Etiqueta.ALUMNOS + " asignados a esta evaluación.");
                        else
                        {
                            FormateaDatos(gvExportar);
                            Convierte(gvExportar, "Repeticiones");
                        }

                    }
                }
        }
        else
            msgError.show("Existen errores en el formulario. Por favor corríjalos.");
        
        
    }

    protected void DDLasignatura_DataBound(object sender, EventArgs e)
    {
        DDLasignatura.Items.Insert(0, new ListItem(Siget.Lang.FileSystem.General.ddl_elija_un_elemento[Session["Usuario_Idioma"].ToString()], "0"));
    }

    protected void DDLcicloescolar_DataBound(object sender, EventArgs e)
    {
        DDLcicloescolar.Items.Insert(0, new ListItem(Siget.Lang.FileSystem.General.ddl_elija_un_elemento[Session["Usuario_Idioma"].ToString()], "0"));
    }

    protected void DDLnivel_DataBound(object sender, EventArgs e)
    {
        DDLnivel.Items.Insert(0, new ListItem(Siget.Lang.FileSystem.General.ddl_elija_un_elemento[Session["Usuario_Idioma"].ToString()], "0"));
    }

    protected void DDLgrado_DataBound(object sender, EventArgs e)
    {
        DDLgrado.Items.Insert(0, new ListItem(Siget.Lang.FileSystem.General.ddl_elija_un_elemento[Session["Usuario_Idioma"].ToString()], "0"));
    }
}