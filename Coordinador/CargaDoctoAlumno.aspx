﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="CargaDoctoAlumno.aspx.vb" 
    Inherits="coordinador_CargaDoctoAlumno" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 34px;
        }

        .style46 {
            width: 630px;
            font-weight: normal;
            font-size: small;
        }

        .style13 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
        }

        .style16 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            text-align: left;
        }

        .style24 {
        }

        .style27 {
            width: 100%;
        }

        .style29 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            width: 392px;
            text-align: right;
            height: 16px;
        }

        .style30 {
            width: 392px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
            text-align: center;
        }

        .style28 {
            width: 243px;
        }
        .style11 {
            text-align: left;
        }
        .style15 {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Evaluación - Cargar Documento
        <asp:Label ID="Label9" runat="server" Text="[ALUMNO]" />
    </h1>
    Permite cargar un documento y poner una calificación a un 
                <asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
    en una actividad determinada.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td colspan="4" class="style23">Calificar documentos entregados por 
								<asp:Label ID="Label2" runat="server" Text="[ALUMNOS]" />
            </td>
        </tr>
        <tr>
            <td class="style13">&nbsp;</td>
            <td class="style13">
                <asp:Label ID="Label3" runat="server" Text="[INSTITUCION]" />
            </td>
            <td class="estandar">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    Height="22px" Width="350px" DataSourceID="SDSinstituciones"
                    DataTextField="Descripcion" DataValueField="IdInstitucion">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">&nbsp;</td>
            <td class="style13">
                <asp:Label ID="Label4" runat="server" Text="[CICLO]" />
            </td>
            <td class="estandar">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px"
                    Width="350px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">&nbsp;</td>
            <td class="style13">
                <asp:Label ID="Label5" runat="server" Text="[PLANTEL]" />
            </td>
            <td class="estandar">
                <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                    DataValueField="IdPlantel" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">&nbsp;</td>
            <td class="style13">
                <asp:Label ID="Label6" runat="server" Text="[NIVEL]" />
            </td>
            <td class="estandar">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Height="22px" Width="270px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">&nbsp;</td>
            <td class="style13">
                <asp:Label ID="Label7" runat="server" Text="[GRADO]" />
            </td>
            <td class="estandar">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                    Height="22px" Width="270px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style15" colspan="4">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td class="style13" colspan="2">
                &nbsp;</td>
            <td class="estandar">
                &nbsp;</td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style16" colspan="3">
                <asp:GridView ID="GVactividades" runat="server"
                    AutoGenerateColumns="False" 
                    Caption="<h3>Listado de Actividades de entrega de documento</h3>"
                    AllowPaging="True" 
                    AllowSorting="True" 

                    DataSourceID="SDSdatos"
                    DataKeyNames="IdEvaluacion"
                    Width="873px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdEvaluacion" HeaderText="IdEvaluacion"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion"
                            Visible="False" />
                        <asp:BoundField DataField="Evaluacion (Actividad)"
                            HeaderText="Actividad" SortExpression="Evaluacion (Actividad)" />
                        <asp:BoundField DataField="Abreviación" HeaderText="Abreviación"
                            SortExpression="Abreviación" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                        <asp:BoundField DataField="Periodo Califica" HeaderText="Periodo Califica"
                            SortExpression="Periodo Califica" />
                        <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"
                            SortExpression="Asignatura" />
                        <asp:BoundField DataField="IdAsignatura" HeaderText="IdAsignatura"
                            InsertVisible="False" SortExpression="IdAsignatura" Visible="False" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort 
	                0  select
	                1  idevaluacion
	                2  actividad
	                3  abreviacion
	                4  tipo
	                5  periodo califica
	                6  ASIGNATURA
	                7  idasignatura
	                --%>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style11" colspan="2">
                &nbsp;</td>
            <td class="style11" colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style13">
                &nbsp;</td>
            <td class="style13">
                <asp:Label ID="Label8" runat="server" Text="[GRUPO]" />
                para cargar</td>
            <td class="estandar">
                <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrupos" DataTextField="Descripcion" DataValueField="IdGrupo"
                    Width="270px">
                </asp:DropDownList>
            </td>
            <td class="style13">&nbsp;</td>
        </tr>
        <tr>
            <td class="style24" colspan="2">
                &nbsp;</td>
            <td class="style24">
                &nbsp;</td>
            <td class="style24"></td>
        </tr>
        <tr>
            <td class="style15" colspan="4">
                <asp:GridView ID="GVdatos" runat="server"
                    AutoGenerateColumns="False"
                    AllowSorting="True"
                    Caption="<h3>ALUMNOS del GRUPO seleccionado. Elija al que desee cargarle el documento</h3>"

                    DataKeyNames="IdAlumno,Login,Documento"
                    DataSourceID="SDSalumnos" 
                    Width="883px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno"
                            Visible="False" />
                        <asp:BoundField DataField="Login" HeaderText="Usuario" SortExpression="Login"
                            Visible="False" />
                        <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                            SortExpression="Matricula" />
                        <asp:BoundField DataField="Nombre Alumno" HeaderText="Nombre del Alumno"
                            ReadOnly="True" SortExpression="Nombre Alumno" />
                        <asp:BoundField DataField="Grupo" HeaderText="Grupo" SortExpression="Grupo" />
                        <asp:BoundField DataField="Calificacion" HeaderText="Resultado"
                            SortExpression="Calificacion">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Documento" HeaderText="Documento"
                            SortExpression="Documento" Visible="False" />
                        <asp:BoundField DataField="Equipo" HeaderText="Equipo" SortExpression="Equipo" />
                        <asp:HyperLinkField DataNavigateUrlFields="Login,Documento"
                            ItemStyle-CssClass="columnaHyperlink"
                            DataNavigateUrlFormatString="~/Resources/cargas/{0}/{1}" DataTextField="Documento"
                            HeaderText="Documento cargado" Target="_blank" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort 
	                0  select
	                1  idalumno
	                2  login Usuario
	                3  matricula
	                4  Nombre del ALUMNO
	                5  GRUPO
	                6  resultado
	                7  documento
	                8  EQUIPO
	                9  documento cargado
	                10 
	                --%>
            </td>
        </tr>
        <tr>
            <td class="style15" colspan="2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style15" colspan="4">
                <asp:Panel ID="PnlCargar" runat="server" BackColor="#E3EAEB" Visible="False">
                    <table class="style27">
                        <tr>
                            <td class="style29">Una vez terminada la Actividad, cargue el archivo de trabajo</td>
                            <td class="style34">
                                <asp:FileUpload ID="RutaArchivo" runat="server" />
                            </td>
                            <td class="style35" colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style30">(Solo se puede cargar un archivo, si requiere cargar varios archivos, 
                                comprímalos en un solo ZIP)</td>
                            <td class="style28">
                                <asp:Button ID="BtnCargar" runat="server" Text="Cargar"
                                    CssClass="defaultBtn btnThemeBlue btnThemeSlick" />
                                &nbsp;
																<asp:Button ID="BtnEliminar" runat="server" Text="Eliminar"
                                                                    CssClass="defaultBtn btnThemeGrey btnThemeSlick" />
                            </td>
                            <td class="style13">Calificación</td>
                            <td>
                                <asp:DropDownList ID="DDLcalifica" runat="server">
                                    <asp:ListItem>100</asp:ListItem>
                                    <asp:ListItem>95</asp:ListItem>
                                    <asp:ListItem>90</asp:ListItem>
                                    <asp:ListItem>85</asp:ListItem>
                                    <asp:ListItem>80</asp:ListItem>
                                    <asp:ListItem>75</asp:ListItem>
                                    <asp:ListItem>70</asp:ListItem>
                                    <asp:ListItem>65</asp:ListItem>
                                    <asp:ListItem>60</asp:ListItem>
                                    <asp:ListItem>55</asp:ListItem>
                                    <asp:ListItem>50</asp:ListItem>
                                    <asp:ListItem>45</asp:ListItem>
                                    <asp:ListItem>40</asp:ListItem>
                                    <asp:ListItem>35</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                    <asp:ListItem>25</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>15</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>0</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="NULL">Ninguna</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style15" colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style11" colspan="4">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT distinct I.IdInstitucion, I.Descripcion 
FROM Institucion I, CoordinadorPlantel CP, Plantel P
WHERE I.Estatus = 'Activo' and I.IdInstitucion = P.IdInstitucion
and P.IdPlantel = CP.IdPlantel and 
CP.IdCoordinador = (select C.IdCoordinador from Coordinador C, Usuario U where
U.Login = @Login and C.IdUsuario = U.IdUsuario)
ORDER BY I.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.IdPlantel, P.Descripcion
from Plantel P, CoordinadorPlantel C
where C.IdCoordinador in (Select IdCoordinador from Coordinador, Usuario
where Coordinador.IdUsuario = Usuario.IdUsuario and Login = @Login)
and P.IdPlantel = C.IdPlantel and P.IdInstitucion = @IdInstitucion 
order by P.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Distinct N.IdNivel, N.Descripcion
from Nivel N, Escuela E
where E.IdPlantel = @IdPlantel
and N.IdNivel = E.IdNivel and (N.Estatus = 'Activo')
order by N.IdNivel">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE (([IdNivel] = @IdNivel) AND ([Estatus] = @Estatus)) ORDER BY [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrupo], [Descripcion] FROM [Grupo] WHERE (([IdGrado] = @IdGrado) AND ([IdPlantel] = @IdPlantel)) and ([IdCicloEscolar] = @IdCicloEscolar)
ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSdatos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select E.IdEvaluacion, E.ClaveBateria as 'Evaluacion (Actividad)', E.ClaveAbreviada as 'Abreviación', E.Tipo, C.Descripcion 'Periodo Califica', A.Descripcion Asignatura, A.IdAsignatura
from Calificacion C, Evaluacion E, Asignatura A, Grado G, Escuela Es
     where G.IdGrado = @IdGrado
     and Es.IdNivel = G.IdNivel and Es.IdPlantel = @IdPlantel
     and A.IdGrado = G.IdGrado and C.IdAsignatura = A.IdAsignatura and C.IdCicloEscolar = @IdCicloEscolar
and E.IdCalificacion = C.IdCalificacion and E.SeEntregaDocto = 'True'
order by C.Consecutivo, E.InicioContestar">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSalumnos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select A.IdAlumno, U.Login, A.Matricula, A.ApePaterno + ' ' + A.ApeMaterno + ', ' + A.Nombre as 'Nombre Alumno',
G.Descripcion as Grupo, DE.Calificacion, DE.Documento, A.Equipo
from Alumno A
join Usuario U on U.IdUsuario = A.IdUsuario
left join DoctoEvaluacion DE on DE.IdEvaluacion = @IdEvaluacion and A.IdAlumno = DE.IdAlumno and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido')
join Grupo G on G.IdGrupo = A.IdGrupo and G.IdPlantel = @IdPlantel and G.IdGrupo = @IdGrupo
order by G.Descripcion, A.ApePaterno, A.ApeMaterno, A.Nombre">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVactividades" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLplantel" DefaultValue="" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSevaluacionterminada" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [EvaluacionTerminada]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSdoctoevaluacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [DoctoEvaluacion]"></asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

