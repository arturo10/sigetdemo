﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="DetalleAlumnos.aspx.vb" 
    Inherits="coordinador_DetalleAlumnos" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            width: 100%;
            height: 240px;
        }

        .style14 {
            color: #000099;
            height: 26px;
        }

        .style21 {
            height: 2px;
            text-align: left;
        }

        .style22 {
            height: 36px;
        }

        .style24 {
            height: 107px;
        }

        .style27 {
            height: 36px;
        }

        .style28 {
            height: 25px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
        }

        .style29 {
            height: 12px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
        }

        .informal {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style25 {
            font-size: small;
            font-weight: normal;
        }

        .style37 {
            height: 2px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Avance - Global de Actividades - Detalle de
        <asp:Label ID="Label6" runat="server" Text="[ALUMNOS]" />
    </h1>
    Presenta un resumen de la cantidad global de 
	<asp:Label ID="Label3" runat="server" Text="[ALUMNOS]" />
    que pertenecen a los 
	<asp:Label ID="Label4" runat="server" Text="[PLANTELES]" />
    que tiene asignados como 
	<asp:Label ID="Label5" runat="server" Text="[COORDINADOR]" />
    que han realizado una actividad específica.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style11">
        <tr>
            <td class="style37">Detalle del que desea consultar su avance:</td>
            <td class="style27">
                <asp:DropDownList ID="DDLconsultar" runat="server" AutoPostBack="True"
                    Height="22px" Width="305px" onchange="setSpinner('')">
                    <asp:ListItem Selected="True">Terminadas</asp:ListItem>
                    <asp:ListItem>Iniciadas</asp:ListItem>
                    <asp:ListItem>Pendientes</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="style22">&nbsp;</td>
            <td class="style22">&nbsp;</td>
        </tr>
        <tr>
            <td class="style29" colspan="4">*Las tareas INICIADAS son las que el 
								<asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
                empezó a realizar pero no ha 
                terminado todos los reactivos, o que ya subió el archivo de entrega pero el 
                <asp:Label ID="Label2" runat="server" Text="[PROFESOR]" />
                no lo ha revisado, y&nbsp;las PENDIENTES son las que no ha realizado ningún reactivo 
                ni ha subido el archivo de entrega.</td>
        </tr>
        <tr>
            <td class="style14" colspan="4"
                style="text-align: left;">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/coordinador/ResumenEvaluaciones.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="spin" style="display:none;" class="spinnerLoading">
                    <div style="padding-top: 80px; text-align: center;">
                        Trabajando...
                    </div>
			    </div>
            </td>
        </tr>
        <tr>
            <td class="style24" colspan="4">
                <asp:GridView ID="GVdatos" runat="server"
                    AutoGenerateColumns="true"
                    AllowSorting="True"

                    DataSourceID="SDSdetalle"
                    Width="883px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
                    TERMINADAS
	                0  matricula
	                1  nombre del ALUMNO
	                2  GRUPO
	                ...
	                --%>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td class="style21" colspan="4">
                <asp:Button ID="Button1" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td class="style21" colspan="2">&nbsp;</td>
            <td class="style21" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style21" colspan="2">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/coordinador/ResumenEvaluaciones.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
            <td class="style21" colspan="2">&nbsp;</td>
        </tr>
    </table>

    <table>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSdetalle" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="sp_detallealumnos"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter Name="Estatus" ControlID="DDLconsultar" PropertyName="SelectedValue" />
                        <asp:SessionParameter Name="IdPlantel" SessionField="IdPlantel" />
                        <asp:SessionParameter Name="IdEvaluacion" SessionField="IdEvaluacion" />
                        <asp:SessionParameter Name="IdCoordinador" SessionField="IdCoordinador" />
                        <asp:SessionParameter Name="IdInstitucion" SessionField="IdInstitucion" />
                        <asp:SessionParameter Name="IdGrado" SessionField="IdGrado" />
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

