﻿Imports Siget

Imports System.Data
Imports System.IO
Imports System.Data.SqlClient

Partial Class coordinador_AvanceActividadesGrupos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        'UserInterface.Include.JQuery(CType(Master.FindControl("pnlHeader"), Literal))
        UserInterface.Include.Spinner(CType(Master.FindControl("pnlHeader"), Literal))

        TitleLiteral.Text = "Avance en Realización"

        Label1.Text = Config.Etiqueta.ASIGNATURA
        Label2.Text = Config.Etiqueta.PLANTELES
        Label3.Text = Config.Etiqueta.GRUPOS

        Try
            'ESTE CODIGO LO USO PORQUE EL PROCESAMIENTO DE LA CONSULTA PUEDE TARDAR MUCHO Y SE CANCELA LA EJECUCIÓN, POR ESO AQUI AUMENTO EL TIEMPO
            GVreporte.Caption = Session("Titulo2")
            'Para generar el contenido del GridView dinamicamente, es necesario que su propiedad AutoGenerateColumns = True 
            Dim objCommand As New SqlClient.SqlCommand("sp_detalleactivgrupos"), Conexion As String = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Parameters.Add("@IdCicloEscolar", SqlDbType.Int, 4)
            objCommand.Parameters("@IdCicloEscolar").Value = CInt(Session("IdCicloEscolar"))
            objCommand.Parameters.Add("@IdPlantel", SqlDbType.Int, 4)
            objCommand.Parameters("@IdPlantel").Value = CInt(Session("IdPlantel"))
            objCommand.Parameters.Add("@IdEvaluacion ", SqlDbType.Int, 4)
            objCommand.Parameters("@IdEvaluacion ").Value = CInt(Session("IdEvaluacion"))
            objCommand.Parameters.Add("@IdGrado ", SqlDbType.Int, 4)
            objCommand.Parameters("@IdGrado ").Value = CInt(Session("IdGrado"))
            objCommand.CommandTimeout = 3000
            objCommand.Connection = New SqlClient.SqlConnection(Conexion)
            objCommand.Connection.Open()
            ' Crear el DataReader
            Dim Datos As SqlClient.SqlDataReader
            ' Con el método ExecuteReader() del comando se traen los datos
            Datos = objCommand.ExecuteReader()
            GVreporte.DataSource = Datos
            GVreporte.DataBind() 'Es necesario para que se vea el resultado
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVreporte, "AvanceActividadesGrupos")
    End Sub

    Protected Sub GVreporte_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVreporte.RowDataBound
        If (e.Row.Cells.Count > 1) And (e.Row.RowIndex > -1) Then  'Para que pinte cuando haya mas de una columna (o fila quize decir?)
            e.Row.Cells(4).BackColor = Drawing.Color.LightBlue
            e.Row.Cells(5).BackColor = Drawing.Color.LightGreen
            e.Row.Cells(6).BackColor = Drawing.Color.Yellow
            e.Row.Cells(7).BackColor = Drawing.Color.LightSalmon
        End If

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(0).Text = Config.Etiqueta.GRUPO
        End If
    End Sub

    Protected Sub GVreporte_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVreporte.DataBound
        If GVreporte.Rows.Count > 0 Then
            BtnExportar.Visible = True
        Else
            BtnExportar.Visible = False
        End If

        ' AÑADO PORCENTAJES DE TERMINADAS
        ' para cada fila calcula el porcentaje, y añadelo a la fila
        Try
            For pC As Integer = 0 To GVreporte.Rows.Count() - 1
                Dim divisor As Decimal =
                        Decimal.Add(Decimal.Parse(GVreporte.Rows(pC).Cells(1).Text),
                        Decimal.Add(Decimal.Parse(GVreporte.Rows(pC).Cells(2).Text),
                        Decimal.Parse(GVreporte.Rows(pC).Cells(3).Text)))

                If divisor = 0 Then ' protejo contra posibles resultados vacios
                    Continue For
                End If

                GVreporte.Rows(pC).Cells(8).Text =
                    Math.Round(Decimal.Multiply(Decimal.Divide(Decimal.Parse(
                        GVreporte.Rows(pC).Cells(3).Text),
                        divisor
                    ), 100), 2, MidpointRounding.AwayFromZero) ' 2 decimales de precisión
            Next
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try
    End Sub

    ' cuando se generan automáticamente las columnas de un gridview, las predefinnidas se insertan
    ' a la izquierda de las generadas. queremos que el % de terminadas esté hasta la derecha, así que
    ' en la creación de la fila se acomoden las columnas
    Protected Sub GVreporte_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVreporte.RowCreated
        Dim row As GridViewRow = e.Row
        Dim columns As New List(Of TableCell)
        Dim cell As TableCell = row.Cells(0)
        row.Cells.Remove(cell) ' retira la columna %
        columns.Add(cell) ' insertala al final de la fila
        row.Cells.AddRange(columns.ToArray())
    End Sub
End Class
