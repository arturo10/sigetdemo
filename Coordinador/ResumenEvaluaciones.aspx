﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="ResumenEvaluaciones.aspx.vb"
    Inherits="coordinador_ResumenEvaluaciones"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style11 {
            height: 38px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style13 {
        }

        .style14 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style15 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            color: #FF0000;
        }

        .style16 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            color: #FF0000;
            height: 30px;
        }

        .style17 {
            height: 30px;
        }

        .style23 {
            width: 244px;
        }

        .style25 {
            font-size: small;
            font-weight: normal;
        }

        .style26 {
            height: 83px;
        }

        .style27 {
            height: 60px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Avance - Global de Actividades
    </h1>
    Presenta un resumen de la cantidad global de 
												<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    que 
                    pertenecen a los 
												<asp:Label ID="Label2" runat="server" Text="[PLANTELES]" />
    que tiene asignados como 
												<asp:Label ID="Label3" runat="server" Text="[COORDINADOR]" />
    que han 
                    realizado una actividad específica.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style14" rowspan="3">Resumen General:</td>
            <td class="style23" rowspan="3">
                <asp:GridView ID="GVdatosglobal" runat="server"
                    AllowSorting="True"

                    DataSourceID="SDSall"
                    Width="241px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
            <td class="style27"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style26">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td class="style16"></td>
            <td class="style17" colspan="2" style="text-align: left;">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/coordinador/DetalleAlumnos.aspx"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium"
                    onclick="setSpinner('')">
                    Ver detalle de 
										<asp:Label ID="Label4" runat="server" Text="[ALUMNOS]" />
                </asp:HyperLink></td></tr><tr>
            <td colspan="3">
                <div id="spin" style="display: none;" class="spinnerLoading">
                    <div style="padding-top: 80px; text-align: center;">
                        Trabajando... </div></div></td></tr><tr>
            <td colspan="3">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style16">&nbsp;</td><td class="style17" colspan="2">&nbsp;</td></tr><tr style="font-family: Arial, Helvetica, sans-serif">
            <td>&nbsp;</td><td colspan="2">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/coordinador/DetalleGrado.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink></td><td>&nbsp;</td></tr></table><table class="dataSources">
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSall" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="RESUMENEVALUACIONESALL"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdEvaluacion" SessionField="IdEvaluacion" />
                        <asp:SessionParameter Name="IdInstitucion" SessionField="IdInstitucion" />
                        <asp:SessionParameter Name="IdCoordinador" SessionField="IdCoordinador" />
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDS1pl" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="RESUMENEVALUACIONES1PL"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdEvaluacion" SessionField="IdEvaluacion" />
                        <asp:SessionParameter Name="IdPlantel" SessionField="IdPlantel" />
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>
