﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="DatosCoordinador.aspx.vb"
    Inherits="profesor_DatosCoordinador"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>



<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .leftSpacer1 {
            width: 150px;
        }

        .rightSpacer1 {
            width: 15px;
        }

        .labelcolumn {
            text-align: right;
            padding-right: 15px;
            padding-bottom: 5px;
            width: 140px;
        }

        .contentColumn {
            padding-bottom: 5px;
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>
        Mis Datos
    </h1>
    Verifique sus datos y corrija o actualice los que sean necesarios.
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" runat="Server">
    <table style="width: 100%;">
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td class="leftSpacer1">&nbsp;</td>
            <td class="labelcolumn">
                Nombres
            </td>
            <td class="contentColumn">
                <asp:TextBox ID="tbNombres" runat="server" Width="300"></asp:TextBox>
            </td>
            <td class="rightSpacer1">&nbsp;</td>
        </tr>
        <tr>
            <td class="leftSpacer1">&nbsp;</td>
            <td class="labelcolumn">
                Apellidos
            </td>
            <td class="contentColumn">
                <asp:TextBox ID="tbApellidos" runat="server" Width="300"></asp:TextBox>
            </td>
            <td class="rightSpacer1">&nbsp;</td>
        </tr>
        <tr>
            <td class="leftSpacer1">&nbsp;</td>
            <td class="labelcolumn">
                Email
            </td>
            <td class="contentColumn">
                <asp:TextBox ID="tbEmail" runat="server" Width="300"></asp:TextBox>
            </td>
            <td class="rightSpacer1">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="leftSpacer1">&nbsp;</td>
            <td class="labelcolumn">
                
            </td>
            <td class="contentColumn">
                <asp:Button ID="BtnActualizar" runat="server" Text="Actualizar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
            <td class="rightSpacer1">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: left;">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSprofesores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Profesor]"></asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

