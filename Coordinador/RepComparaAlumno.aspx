﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="RepComparaAlumno.aspx.vb"
    Inherits="coordinador_RepComparaAlumno"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            height: 37px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style14 {
            text-align: left;
        }

        .style24 {
            text-align: left;
            font-size: x-small;
            font-weight: bold;
            color: #000099;
        }

        .style33 {
            text-align: left;
            height: 35px;
        }

        .style25 {
            text-align: left;
            font-size: x-small;
            color: #000099;
        }

        .style23 {
            text-align: left;
            width: 324px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style21 {
            width: 273px;
        }

        .style13 {
            width: 143px;
        }

        .style32 {
            height: 22px;
            width: 479px;
            font-weight: normal;
            font-size: small;
        }

        .style37 {
            text-align: left;
            font-size: x-small;
            color: #0000CC;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Resultados - Comparación
    </h1>
    Muestra la calificación (acumulada) de varias asignaturas, 
                    de un mismo 
												<asp:Label ID="Label1" runat="server" Text="[GRADO]" />
    , para realizar una comparación de resultados por 
												<asp:Label ID="Label2" runat="server" Text="[PLANTEL]" />
    , 
												<asp:Label ID="Label3" runat="server" Text="[GRUPOS]" />
    y 
												<asp:Label ID="Label4" runat="server" Text="[ALUMNOS]" />
    .
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td colspan="6">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style24" colspan="2">&nbsp;</td>
            <td class="style24" colspan="2">
                <asp:GridView ID="GVmaximos" runat="server"
                    DataSourceID="SDSmaxcal" 
                    Width="308px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
            <td class="style24" colspan="2">
                <asp:GridView ID="GVindicadores" runat="server" 
                    AutoGenerateColumns="False"
                    Caption="INDICADORES ESPECÍFICOS" 

                    DataKeyNames="Color"
                    DataSourceID="SDSindicadores" 
                    Width="237px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Categoria" HeaderText="Categoria"
                            SortExpression="Categoria" />
                        <asp:BoundField DataField="Color" HeaderText="Color" SortExpression="Color" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style33" colspan="6">
                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="RepComparaGrupo.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="6">
                <asp:GridView ID="GVreporte" runat="server" 

                    DataSourceID="SDScomparaalumno" 
                    Width="884px"

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort NO, dynamic
	                0  IdAlumno
	                1  EQUIPO
	                2  Matrícula
	                3  ALUMNO
	                --%>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style37" colspan="6">NOTA: Para el cálculo de las Calificaciones Acumuladas, cada actividad representa un porcentaje de la 
                Calificación, por lo tanto, el porcentaje se irá acumulando hasta llegar al 100% 
                conforme se vaya completando la cantidad de tareas que componen cada 
                calificación. El Aprovechamiento de actividades es un promedio del resultado de las tareas 
                realizadas independientemente de lo que representen para una Calificación.</td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="6">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="3">
                &nbsp;</td>
            <td class="style14" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style23">
                <asp:HyperLink ID="HyperLink5" runat="server"
                    NavigateUrl="javascript:history.back()"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
            <td class="style21" colspan="2">&nbsp;</td>
            <td class="style13" colspan="2">&nbsp;</td>
            <td class="style15">
                &nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDScomparaalumno" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SP_comparacalalumnos" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter Name="calificaciones" SessionField="Calificaciones"
                            Type="String" />
                        <asp:SessionParameter Name="IdGrupo" SessionField="IdGrupo" Type="Int32" />
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar" Type="Int32" />
                        <asp:SessionParameter Name="Rep" SessionField="Rep" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSmaxcal" runat="server"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSindicadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT DISTINCT IdIndicador,Inferior,Categoria, Color
                        FROM         Rango
                        WHERE     IdIndicador in 
                        (select IdIndicador from Calificacion where IdAsignatura = @IdAsignatura and IdCicloEscolar = @IdCicloEscolar)
                        order by IdIndicador,Inferior" >
                    <SelectParameters>
                        <asp:SessionParameter Name="IdAsignatura" SessionField="IdAsignatura" />
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

