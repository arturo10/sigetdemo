﻿Imports Siget

Imports System.Data
Imports System.IO

Partial Class coordinador_ResumenEvaluaciones
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        'UserInterface.Include.JQuery(CType(Master.FindControl("pnlHeader"), Literal))
        UserInterface.Include.Spinner(CType(Master.FindControl("pnlHeader"), Literal))

        TitleLiteral.Text = "Avance Global de Actividades"

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.PLANTELES
        Label3.Text = Config.Etiqueta.COORDINADOR
        Label4.Text = Config.Etiqueta.ALUMNOS

        Try
            'Para generar el contenido del GridView dinamicamente, es necesario que su propiedad AutoGenerateColumns = True 
            GVdatosglobal.Caption = "<B>" + Session("Institucion").ToString + " - " + _
            Session("Plantel").ToString + " <BR> " + Session("Nivel").ToString + _
            " - " + Session("Grado").ToString + "</B><BR>" + _
            Session("Evaluacion").ToString + " - al " + Date.Today.ToShortDateString

            If Session("IdPlantel") < 0 Then 'significa que eligio todos los planteles
                'SDSdatosglobal.SelectCommand = "execute RESUMENEVALUACIONESALL @IdEvaluacion = " + Session("IdEvaluacion").ToString + ", @IdInstitucion = " + Session("IdInstitucion").ToString + ", @IdCoordinador = " + Session("IdCoordinador").ToString

                GVdatosglobal.DataSourceID = "SDSall"
            Else 'Eligio un Plantel en específico
                'SDSdatosglobal.SelectCommand = "execute RESUMENEVALUACIONES1PL @IdEvaluacion = " + Session("IdEvaluacion").ToString + ", @IdPlantel = " + Session("IdPlantel").ToString

                GVdatosglobal.DataSourceID = "SDS1pl"
            End If
            GVdatosglobal.DataBind() 'Es necesario para que se actualice (refresque) el Grid
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVdatosglobal, "ResumenGlobal")
    End Sub

    Protected Sub GVdatosglobal_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVdatosglobal.DataBound
        If GVdatosglobal.Rows.Count > 0 Then
            BtnExportar.Visible = True
        Else
            BtnExportar.Visible = False
        End If
    End Sub

    Protected Sub SDS1pl_Selecting(sender As Object, e As SqlDataSourceSelectingEventArgs) Handles SDS1pl.Selecting
        e.Command.CommandTimeout = 3000
    End Sub

    Protected Sub SDSall_Selecting(sender As Object, e As SqlDataSourceSelectingEventArgs) Handles SDSall.Selecting
        e.Command.CommandTimeout = 3000
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVdatosglobal_PreRender(sender As Object, e As EventArgs) Handles GVdatosglobal.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVdatosglobal.Rows.Count > 0 Then
            GVdatosglobal.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVdatosglobal_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVdatosglobal.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If GVdatosglobal.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub

End Class
