﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO

Partial Class coordinador_ResponderMensajeC
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            UserInterface.Include.HtmlEditor(CType(Master.FindControl("pnlHeader"), Literal), 665, 200, 12, Session("Usuario_Idioma").ToString())

            aplicaLenguaje()

            initPageData()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = "Responder Mensaje"

        Label1.Text = Config.Etiqueta.PROFESORES
        Label2.Text = Config.Etiqueta.PLANTELES
        Label4.Text = Config.Etiqueta.PLANTEL

        If Session("Responder") = "P" Then ' MENSAJE DE PROFESOR
            Label3.Text = Config.Etiqueta.PROFESOR
        ElseIf Session("Responder") = "A" Then ' MENSAJE PARA COORDINADOR
            Label3.Text = Config.Etiqueta.ALUMNO
        End If
    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub initPageData()
        hfCarpetaAdjunto.Value = User.Identity.Name.Trim()

        If Session("Envio_Previous").Equals("LeerMensajeSalidaC") Then
            HyperLink2.NavigateUrl = "~/coordinador/LeerMensajeSalidaC.aspx"
        Else
            HyperLink2.NavigateUrl = "~/coordinador/LeerMensajeEntradaC.aspx"
        End If

        TBasunto.Text = Mid("RESP: " + Session("Envio_Asunto") + " [" + Session("Envio_Fecha") + "]", 1, 100)
        If Session("Responder") = "P" Then ' MENSAJE DE PROFESOR
            LblNombre.Text = Session("Envio_NombreProfesor")
            LblPlantel.Text = Session("Envio_Plantel")
        ElseIf Session("Responder") = "A" Then ' MENSAJE DE ALUMNO
            LblNombre.Text = Session("Envio_NombreAlumno")
            LblPlantel.Text = Session("Envio_Plantel")
        End If
    End Sub

#End Region

    Protected Sub BtnEnviar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEnviar.Click
        If (Trim(TBasunto.Text).Length > 0) And (Trim(TBcontenido.Text).Length) > 0 Then
            If Session("Responder") = "P" Then ' MENSAJE DE PROFESOR
                enviarMensajeProfesor()
            ElseIf Session("Responder") = "A" Then ' MENSAJE PARA COORDINADOR
                enviarMensajeAlumno()
            End If
        Else
            msgError.show("Indique el asunto y el contenido de su mensaje.")
            msgSuccess.hide()
        End If
    End Sub

    Protected Sub enviarMensajeProfesor()
        Try
            '[Sentido (E=Envia, E=Recibe - desde el punto de vista del coordinador)][Estatus (Leido, Nuevo, Baja)]
            SDScomunicacionCP.InsertCommand = "SET dateformat dmy; INSERT INTO ComunicacionCP(IdCoordinador,IdProfesor,Sentido,Asunto,Contenido,Fecha,ArchivoAdjunto,ArchivoAdjuntoFisico,CarpetaAdjunto,EmailEnviado,Estatus,EstatusP)" &
                                " VALUES(@IdCoordinador, @IdProfesor, 'E', @Asunto, @Contenido, getdate(), @NombreAdjunto, @RutaAdjunto, @CarpetaAdjunto, @EmailEnviado, 'Nuevo', 'Nuevo')"
            SDScomunicacionCP.InsertParameters.Add("IdCoordinador", Session("Envio_IdCoordinador").ToString)
            SDScomunicacionCP.InsertParameters.Add("IdProfesor", Session("Envio_IdProfesor").ToString)
            SDScomunicacionCP.InsertParameters.Add("Asunto", TBasunto.Text)
            SDScomunicacionCP.InsertParameters.Add("Contenido", TBcontenido.Text)
            SDScomunicacionCP.InsertParameters.Add("NombreAdjunto", lbAdjunto.Text)
            SDScomunicacionCP.InsertParameters.Add("RutaAdjunto", hfAdjuntoFisico.Value)
            SDScomunicacionCP.InsertParameters.Add("CarpetaAdjunto", hfCarpetaAdjunto.Value)
            SDScomunicacionCP.InsertParameters.Add("EmailEnviado", Session("Envio_Email").ToString)
            SDScomunicacionCP.Insert()

            If Trim(Session("Envio_Email")).Length > 1 Then
        If Config.Global.MAIL_ACTIVO Then
          Utils.Correo.EnviaCorreo(Trim(Session("Envio_Email")),
                                   Config.Global.NOMBRE_FILESYSTEM & " - Mensaje en " & Config.Etiqueta.SISTEMA_CORTO,
                                   "Tiene un nuevo mensaje en " & Config.Global.NOMBRE_FILESYSTEM & " que le ha enviado " + Session("NombreCoordinador"),
                                   False)
        End If
      End If

      msgSuccess.show("Enviado", "Se ha enviado el mensaje a " &
          Config.Etiqueta.ARTDET_PROFESOR & " " & Config.Etiqueta.PROFESOR)
      msgError.hide()
      TBasunto.Text = ""
      TBcontenido.Text = ""
      lbAdjunto.Text = ""
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      msgSuccess.hide()
    End Try
  End Sub

  Protected Sub enviarMensajeAlumno()
    Try
      'Inserto el mensaje [Sentido (E=Envia, R=Recibe - desde el punto de vista del profesor)][Estatus (Leido, Nuevo, Baja)]
      SDScomunicacionCA.InsertCommand = "SET dateformat dmy; INSERT INTO ComunicacionCA(IdCoordinador,IdAlumno,Sentido,Asunto,Contenido,Fecha,ArchivoAdjunto,ArchivoAdjuntoFisico,CarpetaAdjunto,EmailEnviado,Estatus, EstatusA)" &
                          " VALUES(@IdCoordinador, @IdAlumno, 'E', @Asunto, @Contenido, getdate(), @NombreAdjunto, @RutaAdjunto, @CarpetaAdjunto, @EmailEnviado, 'Nuevo', 'Nuevo')"
      SDScomunicacionCA.InsertParameters.Add("IdCoordinador", Session("Envio_IdCoordinador"))
      SDScomunicacionCA.InsertParameters.Add("IdAlumno", Session("Envio_IdAlumno").ToString)
      SDScomunicacionCA.InsertParameters.Add("Asunto", TBasunto.Text)
      SDScomunicacionCA.InsertParameters.Add("Contenido", TBcontenido.Text)
      SDScomunicacionCA.InsertParameters.Add("NombreAdjunto", lbAdjunto.Text)
      SDScomunicacionCA.InsertParameters.Add("RutaAdjunto", hfAdjuntoFisico.Value)
      SDScomunicacionCA.InsertParameters.Add("CarpetaAdjunto", hfCarpetaAdjunto.Value)
      SDScomunicacionCA.InsertParameters.Add("EmailEnviado", Session("Envio_Email"))
      SDScomunicacionCA.Insert()

      'Si tenía un Email registrado el Profesor, le envía una notificación
      If Trim(Session("Envio_Email")).Length > 1 Then
        If Config.Global.MAIL_ACTIVO Then
          Utils.Correo.EnviaCorreo(Trim(Session("Envio_Email")),
                                   Config.Global.NOMBRE_FILESYSTEM & " - Mensaje en " & Config.Etiqueta.SISTEMA_CORTO,
                                   "Tiene un nuevo mensaje en " & Config.Global.NOMBRE_FILESYSTEM & " que le ha enviado " & Session("NombreCoordinador"),
                                   False)
        End If
      End If

      msgSuccess.show("Enviado", "Se ha enviado el mensaje a " &
          Config.Etiqueta.ARTDET_ALUMNO & " " & Config.Etiqueta.ALUMNO)
      msgError.hide()
      TBasunto.Text = ""
      TBcontenido.Text = ""
      lbAdjunto.Text = ""
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      msgSuccess.hide()
    End Try
  End Sub

#Region "Adjuntos"

  Protected Sub BtnAdjuntar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdjuntar.Click
    Try  'Cuando se adjunta el archivo, se borra el nombre del FileUpload
      If FUadjunto.HasFile Then
        Dim ruta As String = Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value & "\" 'Esta ruta contempla un directorio como alumno nombrado como su login

        'Verifico si ya existe el directorio del alumno, si no, lo creo
        If Not (Directory.Exists(ruta)) Then
          Dim directorio As DirectoryInfo = Directory.CreateDirectory(ruta)
        End If

        'Subo archivo
        Dim now As String = Date.Now.Year.ToString & "_" & Date.Now.Month.ToString & "_" & Date.Now.Day.ToString & "_" & Date.Now.Hour.ToString & "_" & Date.Now.Minute.ToString & "_" & Date.Now.Second.ToString & "_" & Date.Now.Millisecond.ToString
        hfAdjuntoFisico.Value = now & FUadjunto.FileName
        FUadjunto.SaveAs(ruta & hfAdjuntoFisico.Value)
        lbAdjunto.Text = FUadjunto.FileName
        msgError.hide()
        msgSuccess.show("Se ha adjuntado el archivo.")
      Else
        msgError.show("No ha seleccionado un archivo para adjuntar.")
        msgSuccess.hide()
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      msgSuccess.hide()
    End Try
  End Sub

  Protected Sub lbAdjunto_Click(sender As Object, e As EventArgs) Handles lbAdjunto.Click
    Dim file As System.IO.FileInfo = New System.IO.FileInfo(Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value + "\" & hfAdjuntoFisico.Value) '-- if the file exists on the server
    If file.Exists Then 'set appropriate headers
      Response.Clear()
      Response.AddHeader("Content-Disposition", "attachment; filename=""" & lbAdjunto.Text & """")
      Response.AddHeader("Content-Length", file.Length.ToString())
      Response.ContentType = "application/octet-stream"
      Response.WriteFile(file.FullName)
      'Response.End()
    Else
      msgError.show("Ha ocurrido un error con el archivo adjunto: éste ya no está disponible.")
    End If 'nothing in the URL as HTTP GET
  End Sub

  Protected Sub BtnQuitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnQuitar.Click
    quitarAdjunto()
  End Sub

  Protected Sub quitarAdjunto()
    Try
      If hfAdjuntoFisico.Value.Length > 0 Then
        Dim ruta As String = Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value & "\" 'Esta ruta contempla un directorio como alumno nombrado como su login

        'Borro el archivo que estaba anteriormente porque se pondrá uno nuevo
        Dim adjunto As FileInfo = New FileInfo(ruta & hfAdjuntoFisico.Value)
        adjunto.Delete()

        lbAdjunto.Text = ""
        hfAdjuntoFisico.Value = ""
        msgError.hide()
        msgSuccess.show("Se ha eliminado el archivo.")
      Else
        msgError.show("No ha adjuntado ningún archivo todavía.")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgSuccess.hide()
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

#End Region

    Protected Sub BtnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLimpiar.Click
        TBcontenido.Text = ""
        ' al limpiar se limpia el adjunto, y debe eliminarse el archivo
        quitarAdjunto()
        ' se ocultan los mensajes al final puesto que al quitar el archivo pueden mostrarse
        msgSuccess.hide()
        msgError.hide()
    End Sub
End Class
