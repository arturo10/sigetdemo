﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="AvanceActividades.aspx.vb"
    Inherits="coordinador_AvanceActividades"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style11 {
            height: 37px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style23 {
            text-align: left;
            height: 43px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000066;
        }

        .style24 {
            width: 143px;
            height: 43px;
        }

        .style25 {
            height: 43px;
        }

        .style26 {
            text-align: right;
            height: 18px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style20 {
            width: 273px;
            height: 18px;
            text-align: left;
        }

        .style21 {
            width: 273px;
        }

        .style31 {
            font-size: x-small;
            color: #000099;
            text-align: left;
        }

        .style22 {
            text-align: left;
        }

        .style13 {
            width: 143px;
        }

        .style33 {
            text-align: right;
            height: 18px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
        }

        .style32 {
            height: 22px;
            width: 479px;
            font-weight: normal;
            font-size: small;
        }

        .style34 {
            font-size: x-small;
            color: #000099;
            height: 33px;
            text-align: left;
        }

        .style14 {
            text-align: left;
        }

        .style37 {
            width: 273px;
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Avance - Detalle Realizadas
    </h1>
    Presenta un resumen por 
		<asp:Label ID="Label1" runat="server" Text="[ASIGNATURA]" />
    del avance de todas las 
		actividades realizadas, permitiendo elegir una y ver el detalle por 
		<asp:Label ID="Label2" runat="server" Text="[PLANTELES]" />
    y                  
		<asp:Label ID="Label3" runat="server" Text="[GRUPOS]" />
    .
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style23" colspan="4">Indique los siguientes datos 
                para generar el reporte:</td>
            <td class="style24" colspan="2"></td>
            <td class="style25"></td>
        </tr>
        <tr>
            <td class="style26">
                <asp:Label ID="Label4" runat="server" Text="[INSTITUCION]" />
            </td>
            <td class="style20" colspan="3">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    Height="22px" Width="350px" DataSourceID="SDSinstituciones"
                    DataTextField="Descripcion" DataValueField="IdInstitucion">
                </asp:DropDownList>
            </td>
            <td class="style17" colspan="3" rowspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td class="style26">
                <asp:Label ID="Label5" runat="server" Text="[CICLO]" />
            </td>
            <td class="style20" colspan="3">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="duracion"
                    DataValueField="IdCicloEscolar" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style26">
                <asp:Label ID="Label6" runat="server" Text="[NIVEL]" />
            </td>
            <td class="style20" colspan="3">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Height="22px" Width="270px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style26">
                <asp:Label ID="Label7" runat="server" Text="[GRADO]" />
            </td>
            <td class="style20" colspan="3">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                    Height="22px" Width="270px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style26">
                <asp:Label ID="Label8" runat="server" Text="[ASIGNATURA]" />
            </td>
            <td class="style37" colspan="3">
                <asp:DropDownList ID="DDLasignatura" runat="server" 
                    AutoPostBack="True"
                    DataSourceID="SDSasignatura" 
                    DataTextField="Descripcion"
                    DataValueField="IdAsignatura" 
                    Height="22px" 
                    Width="350px"
                    onchange="setSpinner('')">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style22" colspan="7">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <div id="spin" style="display:none;" class="spinnerLoading">
                    <div style="padding-top: 80px; text-align: center;">
                        Trabajando...
                    </div>
			    </div>
            </td>
        </tr>
        <tr class="estandar">
            <td colspan="7">
                <asp:GridView ID="GVreporte" runat="server"

                    DataKeyNames="IdActividad"
                    Width="883px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="" HeaderText="%&nbsp;Terminadas" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort 
	                0  
	                1  
	                2  
	                3  
	                4  
	                5  
	                6  
	                7  
	                8  
	                9  
	                10 
                --%>
            </td>
        </tr>
        <tr class="estandar">
            <td colspan="7" style="text-align: left">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr class="estandar">
            <td colspan="2">&nbsp;</td>
            <td colspan="2">
                <asp:GridView ID="GVindicadores" runat="server" 
                    AutoGenerateColumns="False"
                    Caption="<h3>ESCALA DE VALORES</h3>" 

                    DataSourceID="SDSindicadores"
                    Width="334px"  

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="MinAzul" HeaderText="Mínimo para EXCELENTE"
                            SortExpression="MinAzul">
                            <ItemStyle HorizontalAlign="Right" BackColor="LightBlue" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinVerde" HeaderText="Mínimo para BIEN"
                            SortExpression="MinVerde">
                            <ItemStyle HorizontalAlign="Right" BackColor="LightGreen" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinAmarillo" HeaderText="Mínimo para REGULAR"
                            SortExpression="MinAmarillo">
                            <ItemStyle HorizontalAlign="Right" BackColor="Yellow" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinRojo" HeaderText="Mínimo para MAL"
                            SortExpression="MinRojo">
                            <ItemStyle HorizontalAlign="Right" BackColor="LightSalmon" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
            <td class="style14" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style22">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td class="style21" colspan="3">&nbsp;</td>
            <td class="style13" colspan="2">&nbsp;</td>
            <td class="style15">&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT distinct I.IdInstitucion, I.Descripcion 
FROM Institucion I, CoordinadorPlantel CP, Plantel P
WHERE I.Estatus = 'Activo' and I.IdInstitucion = P.IdInstitucion
and P.IdPlantel = CP.IdPlantel and 
CP.IdCoordinador = (select C.IdCoordinador from Coordinador C, Usuario U where
U.Login = @Login and C.IdUsuario = U.IdUsuario)
ORDER BY I.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSindicadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT MinRojo, MinAmarillo, MinVerde, MinAzul FROM Indicador, CicloEscolar
where Indicador.IdIndicador = CicloEscolar.IdIndicador and IdCicloEscolar = @IdCicloEscolar">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdCicloEscolar, Descripcion as  duracion
from CicloEscolar
where Estatus = 'Activo'
order by Descripcion"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT DISTINCT N.IdNivel, N.Descripcion
FROM         Nivel AS N INNER JOIN
                      Escuela AS E ON N.IdNivel = E.IdNivel INNER JOIN
                      CoordinadorPlantel AS CP ON E.IdPlantel = CP.IdPlantel INNER JOIN
                      Coordinador AS C ON CP.IdCoordinador = C.IdCoordinador INNER JOIN
                      Usuario AS U ON C.IdUsuario = U.IdUsuario
WHERE     (U.Login = @Login) and (N.Estatus = 'Activo')
ORDER BY N.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE ([IdNivel] = @IdNivel) 
and (Estatus = 'Activo') ORDER BY [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignatura" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdAsignatura], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) 
AND IdAsignatura in (select IdAsignatura from Calificacion where IdCicloEscolar = @IdCicloEscolar)
ORDER BY [IdArea]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

