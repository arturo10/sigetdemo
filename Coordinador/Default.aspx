<%@ Page Title=""
  Language="VB"
  MasterPageFile="~/Principal.master"
  AutoEventWireup="false"
  CodeFile="Default.aspx.vb"
  Inherits="coordinador_Default"
  MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
  <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <style type='text/css'>
    #toolBarContainer {
      display: none;
    }
  </style>
  <!-- la toolbar no es necesaria en p�ginas principales -->

  <!-- Estilos para los men�s de inicio con esquema de lista -->
  <link id="MainMenu_List" rel="stylesheet" media="screen" href="../Resources/css/MainMenu_List.css" />

  <style type="text/css">
    .linkDatos {
      float: right;
      font-weight: normal;
      margin-top: 5px;
      margin-right: 10px;
    }
  </style>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
  <table style="width: 100%;">
    <tr>
      <td>
        <h1 style="max-width: 700px;">Men� de
				    <asp:Label ID="Label1" runat="server" Text="[COORDINADORES]" />
        </h1>
      </td>
      <td>
        <asp:HyperLink ID="HyperLink28" runat="server"
          CssClass="defaultBtn btnThemeGrey btnThemeSlick linkDatos"
          NavigateUrl="~/coordinador/DatosCoordinador ">
                    Mis&nbsp;Datos
        </asp:HyperLink>
      </td>
    </tr>
  </table>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
  <div class="centeringContainer">
    <div class="centered">
      <table class="mainMenuTable">
        <tbody>
          <tr>
            <td colspan="4">
              <div style="width: 100%; text-align: center;">
                <asp:HyperLink ID="HLmensaje" runat="server"
                  NavigateUrl="~/coordinador/LeerMensajeEntradaC "
                  Style="text-decoration: underline; font-family: Verdana, Arial, sans-serif; font-size: medium; color: #000066; font-weight: 700">Tiene mensajes pendientes de Leer</asp:HyperLink>
              </div>
            </td>
          </tr>
          <tr>
            <td colspan="4">
              <uc1:msgError runat="server" ID="msgError" />
            </td>
          </tr>
          <tr>
            <td class="lineSeparator">&nbsp;
            </td>
          </tr>
          <tr class="MenuLineA">
            <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
            <td class="MenuColumn VerticalIndented">
              <div class="MenuTitleDiv">
                <asp:Image ID="Image7" runat="server"
                  ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/pie_10_percent.png" />
                <br />
                Avance
              </div>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable leftColumnTable LeftMenuColumn">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink20" runat="server"
                      NavigateUrl="~/coordinador/AvancePlantel ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image8" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Global de Actividades
                      </div>
                    </asp:HyperLink>
                  </td>
                </tr>
              </table>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink4" runat="server"
                      NavigateUrl="~/coordinador/AvanceActividades ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image9" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Detalle Realizadas
                      </div>
                    </asp:HyperLink>
                  </td>
                </tr>
                <%-- deprecated 04/11/2013, archivos comprimidos en 5.0.13
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink25" runat="server"
                                            NavigateUrl="~/coordinador/AvanceRegistroAl ">
											<div class="mnItem mnIndent1 mnItemLink">
													Registro
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                --%>
              </table>
            </td>
              <td class="OptionsColumn">
                  <table class="mainMenuTable">
                      <tr>
                          <td>
                              <asp:HyperLink ID="HyperLink19" runat="server"
                                  NavigateUrl="~/coordinador/DetalleEvaluaciones ">
                                  <div class="mnItem mnIndent1 mnItemLink">
                                      <asp:Image ID="Image10" runat="server"
                                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                                          Width="10" Height="10" />
                                      Resumen por Actividad
                                  </div>
                              </asp:HyperLink>
                          </td>
                      </tr>
                      <tr>
                          <td class="OptionsColumn">
                              <table class="mainMenuTable">
                                  <tr>
                                      <td>
                                          <asp:HyperLink ID="HyperLink8" runat="server"
                                              NavigateUrl="~/coordinador/AvanceAsignatura ">
                                              <div class="mnItem mnIndent1 mnItemLink">
                                                  <asp:Image ID="Image28" runat="server"
                                                      ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                      Width="10" Height="10" />
                                                  Avance por Asignatura
                                              </div>
                                          </asp:HyperLink>
                                      </td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                  </table>
              </td>

          </tr>
            <tr>
                <td class="lineSeparator">&nbsp;
                </td>
            </tr>
            <tr class="MenuLineB">
                <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
            <td class="MenuColumn VerticalIndented">
              <div class="MenuTitleDiv">
                <asp:Image ID="Image6" runat="server"
                  ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/clipboard.png" />
                <br />
                Resultados
              </div>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable leftColumnTable LeftMenuColumn">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink18" runat="server"
                      NavigateUrl="~/coordinador/ReporteGeneral ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image11" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        <asp:Label ID="Label2" runat="server" Text="[ASIGNATURA]" />
                      </div>
                    </asp:HyperLink></td>
                </tr>
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink26" runat="server"
                      NavigateUrl="~/coordinador/ReporteGlobal ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image12" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Global
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink3" runat="server"
                      NavigateUrl="~/coordinador/ResultadoReactivos ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image13" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Reactivos
                      </div>
                    </asp:HyperLink></td>
                </tr>
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink27" runat="server"
                      NavigateUrl="~/coordinador/DetalleActividadesGrupo ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image14" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Actividades
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink5" runat="server"
                      NavigateUrl="~/coordinador/ReporteCompara ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image15" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Comparaci�n
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td class="lineSeparator">&nbsp; </td>
          </tr>
          <tr class="MenuLineA">
            <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
            <td class="MenuColumn VerticalIndented">
              <div class="MenuTitleDiv">
                <asp:Image ID="Image5" runat="server"
                  ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/search_1.png" />
                <br />
                Consultas
              </div>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable leftColumnTable LeftMenuColumn">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink17" runat="server"
                      NavigateUrl="~/coordinador/ConsultaMaterial ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image16" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Material de Actividades
                      </div>
                    </asp:HyperLink></td>
                </tr>
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink21" runat="server"
                      NavigateUrl="~/coordinador/ConsultaDoctos ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image17" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Documentos Entregados
                      </div>
                    </asp:HyperLink></td>
                </tr>
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink2" runat="server"
                      NavigateUrl="~/coordinador/ValoracionProfes ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image18" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Valoraci�n de 
                                                <asp:Label ID="Label3" runat="server" Text="[PROFESORES]" />
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink22" runat="server"
                      NavigateUrl="~/coordinador/DetalleActividadesDocto ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image19" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Actividades con Documento
                      </div>
                    </asp:HyperLink></td>
                </tr>
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink23" runat="server"
                      NavigateUrl="~/coordinador/ListaApoyoNivelCoord ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image20" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Biblioteca de Recursos
                      </div>
                    </asp:HyperLink></td>
                </tr>
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink29" runat="server"
                      NavigateUrl="~/coordinador/Repeticiones ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image35" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Repeticiones
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink16" runat="server"
                      NavigateUrl="~/coordinador/DetalleActividadesFecha ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image21" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Actividades Usando Filtros
                      </div>
                    </asp:HyperLink></td>
                </tr>
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink24" runat="server"
                      NavigateUrl="~/coordinador/RespuestasAbiertasCoord ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image22" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Respuestas Abiertas
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td class="lineSeparator">&nbsp; </td>
          </tr>
          <tr class="MenuLineB">
            <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
            <td class="MenuColumn VerticalIndented">
              <div class="MenuTitleDiv">
                <asp:Image ID="Image3" runat="server"
                  ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/invoice_document_file.png" />
                <br />
                Reportes
              </div>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable leftColumnTable LeftMenuColumn">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink10" runat="server"
                      NavigateUrl="~/coordinador/GenerarReporteAsignaturas ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image31" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Informe Individual
                      </div>
                    </asp:HyperLink></td>
                </tr>
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink30" runat="server"
                      NavigateUrl="~/coordinador/IndicadoresAvance ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image36" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Indicadores de Avance
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable leftColumnTable LeftMenuColumn">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink12" runat="server"
                      NavigateUrl="~/coordinador/ActividadesRealizadas ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image33" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Actividades por Grupos
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable leftColumnTable LeftMenuColumn">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink25" runat="server"
                      NavigateUrl="~/coordinador/ParticipacionActividades ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image34" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Participaci�n 
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td class="lineSeparator">&nbsp; </td>
          </tr>
          <tr class="MenuLineA">
            <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
            <td class="MenuColumn VerticalIndented">
              <div class="MenuTitleDiv">
                <asp:Image ID="Image4" runat="server"
                  ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/chat_talk_speak.png" />
                <br />
                Comunicaci�n
              </div>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable leftColumnTable LeftMenuColumn">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink1" runat="server"
                      NavigateUrl="~/coordinador/EnviarMensajeC ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image25" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Enviar Mensajes
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink14" runat="server"
                      NavigateUrl="~/coordinador/LeerMensajeEntradaC ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image26" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Mensajes Recibidos
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink11" runat="server"
                      NavigateUrl="~/coordinador/LeerMensajeSalidaC ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image32" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Mensajes Enviados
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
          </tr>
          <%-- deprecated 04/11/2013, archivos comprimidos en 5.0.13
                    <tr>
                        <td class="lineSeparator">
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="MenuLineA">
                        <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
                        <td class="MenuColumn VerticalIndented">
                            <div class="MenuTitleDiv">
                                <asp:Image ID="Image3" runat="server"
                                    ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/key_password.png" />
                                <br />
                                Accesos
                            </div>
                        </td>
                        <td class="OptionsColumn">
                            <table class="mainMenuTable leftColumnTable LeftMenuColumn">
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink12" runat="server"
                                            NavigateUrl="~/coordinador/AccesosProfes ">
                                            <div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Label ID="Label4" runat="server" Text="[PROFESORES]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn">
                            <table class="mainMenuTable">
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink11" runat="server"
                                            NavigateUrl="~/coordinador/AccesosCoords ">
                                            <div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Label ID="Label5" runat="server" Text="[COORDINADORES]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
          --%><tr>
            <td class="lineSeparator">&nbsp; </td>
          </tr>
          <tr class="MenuLineB">
            <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
            <td class="MenuColumn VerticalIndented">
              <div class="MenuTitleDiv">
                <asp:Image ID="Image1" runat="server"
                  ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/stop_watch_time.png" />
                <br />
                Programaci�n
              </div>
            </td>
            <%-- deprecated 04/11/2013, archivos comprimidos en 5.0.13
                        <td class="OptionsColumn">
                            <table class="mainMenuTable leftColumnTable LeftMenuColumn">
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink10" runat="server"
                                            NavigateUrl="~/coordinador/ProgramaProfes ">
                                            <div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Label ID="Label6" runat="server" Text="[PROFESORES]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
            --%><td class="OptionsColumn">
              <table class="mainMenuTable leftColumnTable LeftMenuColumn">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink9" runat="server"
                      NavigateUrl="~/coordinador/BuscaAlumnoCoord ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image27" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Buscar
                                                <asp:Label ID="Label7" runat="server" Text="[ALUMNO]" />
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable leftColumnTable LeftMenuColumn">
                <tr>
                  <td>
                    <asp:HyperLink ID="HLagregarCursos" runat="server"
                      NavigateUrl="~/coordinador/AgregarCursos ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image23" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Agregar Cursos
                      </div>
                    </asp:HyperLink></td>
                </tr>
                  <tr>
                  <td>
                    <asp:HyperLink ID="HLgestionarInscripcionesCursos" runat="server"
                      NavigateUrl="~/coordinador/GestionarInscripcionesCursos ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image24" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Gestionar Inscripciones
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
            <td class="OptionsColumn">
              &nbsp;
            </td>
          </tr>
          <tr>
            <td class="lineSeparator">&nbsp; </td>
          </tr>
          <tr class="MenuLineA">
            <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
            <td class="MenuColumn VerticalIndented">
              <div class="MenuTitleDiv">
                <asp:Image ID="Image2" runat="server"
                  ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/edit_pencil.png" />
                <br />
                Evaluaci�n
              </div>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable leftColumnTable LeftMenuColumn">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink7" runat="server"
                      NavigateUrl="~/coordinador/LlenarEvaluacionesCoord ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image29" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Captura
                                                <asp:Label ID="Label8" runat="server" Text="[ALUMNO]" />
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
            <td class="OptionsColumn">
              <table class="mainMenuTable">
                <tr>
                  <td>
                    <asp:HyperLink ID="HyperLink6" runat="server"
                      NavigateUrl="~/coordinador/CargaDoctoAlumno ">
                      <div class="mnItem mnIndent1 mnItemLink">
                        <asp:Image ID="Image30" runat="server"
                          ImageUrl="~/Resources/imagenes/dotsmall.png"
                          Width="10" Height="10" />
                        Cargar Documento a 
                                                <asp:Label ID="Label9" runat="server" Text="[ALUMNO]" />
                      </div>
                    </asp:HyperLink></td>
                </tr>
              </table>
            </td>
            <td></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</asp:Content>
