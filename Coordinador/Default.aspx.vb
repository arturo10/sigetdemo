﻿Imports Siget

Imports System.Data.SqlClient

Partial Class coordinador_Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' esto debe ir antes que cualquier operación que utilice base de datos
        If Utils.Bloqueo.sistemaBloqueado() Then
            Utils.Sesion.CierraSesion()
            Response.Redirect("~/EnMantenimiento.aspx")
        End If

        TitleLiteral.Text = "Menú Principal"

        Label1.Text = Config.Etiqueta.COORDINADORES
        Label2.Text = Config.Etiqueta.ASIGNATURA
        Label3.Text = Config.Etiqueta.PROFESORES
        'Label4.Text = Config.Etiqueta.PROFESORES
        'Label5.Text = Config.Etiqueta.COORDINADORES
        'Label6.Text = Config.Etiqueta.PROFESORES
        Label7.Text = Config.Etiqueta.ALUMNO
        Label8.Text = Config.Etiqueta.ALUMNO
        Label9.Text = Config.Etiqueta.ALUMNO

        'Estas variables de Aplicación las lee el archivo App_Code/PTBlogTemplate/UserFunctions.vb
        'La quize poner en If Menu3.SelectedItem.Value = "Blog" Then pero no funcionó
        Application("Login") = Trim(User.Identity.Name)

        Dim strConexion As String
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexionEv As New SqlConnection(strConexion)
        Dim miComandoEv As SqlCommand
        Dim misClaves As SqlDataReader
        Dim misRegistros As SqlDataReader
        miComandoEv = objConexionEv.CreateCommand


        miComandoEv.CommandText = "SELECT C.* FROM Usuario U, Coordinador C where U.Login = '" + User.Identity.Name + "' and C.IdUsuario = U.IdUsuario"
        Try
            'Abrir la conexión y leo los registros del Alumno
            objConexionEv.Open() 'Abro la conexion
            misRegistros = miComandoEv.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read() 'Leo para poder accesarlos
            Dim IdCoordinador = misRegistros.Item("IdCoordinador")
            'Cierro y ahora buscaré si tiene mensajes de profesores (El profesor E=Envía o R=Recibe
            misRegistros.Close()

            miComandoEv.CommandText = "select Count(IdComunicacionCP) as Total from ComunicacionCP where IdCoordinador=" + _
                                            IdCoordinador.ToString + " and Sentido = 'R' and Estatus = 'Nuevo'"
            misRegistros = miComandoEv.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read() 'Leo para poder accesarlos
            If misRegistros.Item("Total") > 0 Then
                HLmensaje.Text = "Tiene " + misRegistros.Item("Total").ToString + " NUEVO(S) mensaje(s) de " & Config.Etiqueta.PROFESORES & " en su bandeja."
            Else
                HLmensaje.Text = ""
            End If

            'Cierro y ahora buscaré si tiene mensajes de alumnos (El profesor E=Envía o R=Recibe
            misRegistros.Close()

            miComandoEv.CommandText = "select Count(IdComunicacionCA) as Total from ComunicacionCA where IdCoordinador=" + _
                                            IdCoordinador.ToString + " and Sentido = 'R' and Estatus = 'Nuevo'"
            misRegistros = miComandoEv.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read() 'Leo para poder accesarlos
            If misRegistros.Item("Total") > 0 Then
                HLmensaje.Text += "<br />Tiene " + misRegistros.Item("Total").ToString + " NUEVO(S) mensaje(s) de " & Config.Etiqueta.ALUMNOS & " en su bandeja."
            Else
                HLmensaje.Text += ""
            End If

            'Cierro
            misRegistros.Close()

            miComandoEv.CommandText = "select Password from Usuario where Login = '" + Trim(User.Identity.Name) + "'"
            misClaves = miComandoEv.ExecuteReader() 'Creo conjunto de registros
            misClaves.Read()
            Application("Passwd") = misClaves.Item("Password")
            misClaves.Close()
            objConexionEv.Close()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub
End Class
