<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="DetalleEvaluaciones.aspx.vb"
    Inherits="coordinador_DetalleEvaluaciones"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style11 {
            height: 38px;
        }

        .style14 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style16 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            color: #FF0000;
            height: 30px;
            width: 232px;
        }

        .style17 {
            height: 30px;
        }

        .style15 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            color: #FF0000;
        }

        .style19 {
            width: 232px;
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 22px;
        }

        .style25 {
            height: 22px;
            width: 479px;
        }

        .style26 {
            height: 22px;
        }

        .style27 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: left;
            height: 25px;
        }

        .style28 {
            height: 25px;
            width: 479px;
        }

        .style29 {
            height: 25px;
        }

        .style31 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
        }

        .style32 {
            height: 22px;
            width: 479px;
            font-weight: normal;
            font-size: small;
        }

        .style33 {
            height: 16px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Avance - Resumen por Actividad
    </h1>
    Presenta un resumen general del avance de todas las 
                    actividades de 
    <asp:Label ID="Label7" runat="server" Text="[LAS]" /> 
	<asp:Label ID="Label1" runat="server" Text="[ASIGNATURAS]" />
    de <asp:Label ID="Label8" runat="server" Text="[EL]" /> 
	<asp:Label ID="Label2" runat="server" Text="[GRADO]" /> 
    elegid<asp:Label ID="Label9" runat="server" Text="[O]" />.
                    <br />
    Este reporte puede tardar VARIOS MINUTOS en generarse.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style31" colspan="2">
                <asp:Label ID="Label3" runat="server" Text="[INSTITUCION]" />
            </td>
            <td class="style28" colspan="2" style="text-align: left;">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                    DataValueField="IdInstitucion" Height="22px" Width="380px">
                </asp:DropDownList>
            </td>
            <td class="style29" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style31" colspan="2">
                <asp:Label ID="Label4" runat="server" Text="[CICLO]" />
            </td>
            <td class="style28" colspan="2" style="text-align: left;">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" 
                    DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" 
                    Height="22px" 
                    Width="380px"
                    onchange="setSpinner('')">
                </asp:DropDownList>
            </td>
            <td class="style29" colspan="2"></td>
        </tr>
        <tr>
            <td class="style24" colspan="2">
                <asp:Label ID="Label5" runat="server" Text="[NIVEL]" />
            </td>
            <td class="style25" colspan="2" style="text-align: left;">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Height="22px" Width="230px">
                </asp:DropDownList>
            </td>
            <td class="style26" colspan="2"></td>
        </tr>
        <tr>
            <td class="style31" colspan="2">
                <asp:Label ID="Label6" runat="server" Text="[GRADO]" />
            </td>
            <td class="style28" colspan="2" style="text-align: left;">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" 
                    DataTextField="Descripcion" 
                    DataValueField="IdGrado"
                    Height="22px" 
                    Width="230px"
                    onchange="setSpinner('')">
                </asp:DropDownList>
            </td>
            <td class="style29" colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td class="style28" colspan="2">&nbsp;</td>
            <td class="style29" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="spin" style="display:none;" class="spinnerLoading">
                    <div style="padding-top: 80px; text-align: center;">
                        Trabajando...
                    </div>
			    </div>
            </td>
        </tr>
        <tr>
            <td class="style14" colspan="6">
                <asp:GridView ID="GVdatosglobal" runat="server" 
                    AutoGenerateColumns="false"

                    Width="861px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="IdEvaluacion" HeaderText="IdEvaluacion"/>
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo"/>
                        <asp:BoundField DataField="Actividad" HeaderText="Actividad"/>
                        <asp:BoundField DataField="Clave" HeaderText="Clave"/>
                        <asp:BoundField DataField="Calificación" HeaderText="Calificación"/>
                        <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"/>
                        <asp:BoundField DataField="Area" HeaderText="Area"/>
                        <asp:BoundField DataField="Terminadas" HeaderText="Terminadas"/>
                        <asp:BoundField DataField="Iniciadas" HeaderText="Iniciadas"/>
                        <asp:BoundField DataField="Pendientes" HeaderText="Pendientes"/>
                        <asp:BoundField HeaderText="%&nbsp;Terminadas" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort NO - prerender
	                0  E.IdEvaluacion
	                1  E.Tipo
	                2  E.ClaveBateria AS 'Actividad'
	                3  E.ClaveAbreviada as 'Clave'
	                4  C.Descripcion as 'Calificación'
	                5  A.Descripcion as 'ASIGNATURA'
	                6  Ar.Descripcion as 'Area'
	                --%>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
            <td class="style17" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style19" colspan="4">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT distinct I.IdInstitucion, I.Descripcion 
FROM Institucion I, CoordinadorPlantel CP, Plantel P
WHERE I.Estatus = 'Activo' and I.IdInstitucion = P.IdInstitucion
and P.IdPlantel = CP.IdPlantel and 
CP.IdCoordinador = (select C.IdCoordinador from Coordinador C, Usuario U where
U.Login = @Login and C.IdUsuario = U.IdUsuario)
ORDER BY I.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Distinct N.IdNivel, N.Descripcion
from Nivel N, Escuela E, Plantel P
where P.IdInstitucion = @IdInstitucion and E.IdPlantel = P.IdPlantel
and N.IdNivel = E.IdNivel and (N.Estatus = 'Activo') and
P.IdPlantel in (select IdPlantel from CoordinadorPlantel where IdCoordinador = (select IdCoordinador from Coordinador C, Usuario U where C.IdUsuario = U.IdUsuario and U.Login = @Login))
order by N.IdNivel">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel) and (Estatus = 'Activo')
order by [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
    </table>
</asp:Content>

