<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="AvancePlantel.aspx.vb" 
    Inherits="coordinador_AvancePlantel" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            width: 100%;
        }

        .style14 {
            text-align: right;
            width: 118px;
        }

        .style13 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style15 {
        }

        .style16 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000066;
            height: 33px;
        }

        .style25 {
            font-size: small;
            font-weight: normal;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Avance - Global de Actividades
    </h1>
    Presenta un resumen de la cantidad global de 
										<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    que han realizado una actividad espec�fica y 
										que pertenecen a 
										<asp:Label ID="Label2" runat="server" Text="[PLANTELES]" />
    que tiene Ud. asignados 
										como
    <asp:Label ID="Label3" runat="server" Text="[COORDINADOR]" />.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style11">
        <tr>
            <td colspan="10" class="style23">Elija el 
								<asp:Label ID="Label4" runat="server" Text="[PLANTEL]" />
            </td>
        </tr>
        <tr>
            <td colspan="10">
                <table class="estandar">
                    <tr>
                        <td class="style14">&nbsp;</td>
                        <td class="style13">
                            <asp:Label ID="Label5" runat="server" Text="[INSTITUCION]" />
                        </td>
                        <td>
                            <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                DataValueField="IdInstitucion" Height="22px" Width="279px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style14"></td>
                        <td class="style13">
                            <asp:Label ID="Label6" runat="server" Text="[CICLO]" />
                        </td>
                        <td>
                            <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                                DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                                DataValueField="IdCicloEscolar" Width="279px" Height="22px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style14"></td>
                        <td class="style13">
                            <asp:Label ID="Label7" runat="server" Text="[PLANTEL]" />
                        </td>
                        <td>
                            <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                DataValueField="IdPlantel" Height="22px" Width="279px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style16" colspan="10">&nbsp;</td>
        </tr>
        <tr>
            <td class="style15" colspan="7">
                <asp:GridView ID="GVdatos" runat="server"
                    AllowSorting="True" 

                    DataSourceID="SDSgrados1"
                    DataKeyNames="IdNivel,IdGrado"
                    Width="806px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  select
	                1  idnivel
	                2  NIVEL
	                3  idgrado
	                4  GRADO
                    5  ...
                    --%>
            </td>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="4"></td>
            <td colspan="3"></td>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="10">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: left">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Men�
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT distinct I.IdInstitucion, I.Descripcion 
FROM Institucion I, CoordinadorPlantel CP, Plantel P
WHERE I.Estatus = 'Activo' and I.IdInstitucion = P.IdInstitucion
and P.IdPlantel = CP.IdPlantel and 
CP.IdCoordinador = (select C.IdCoordinador from Coordinador C, Usuario U where
U.Login = @Login and C.IdUsuario = U.IdUsuario)
ORDER BY I.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.IdPlantel, P.Descripcion
from Plantel P, CoordinadorPlantel C
where C.IdCoordinador = @IdCoordinador
and P.IdPlantel = C.IdPlantel and P.IdInstitucion = @IdInstitucion 
order by P.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdCoordinador" SessionField="IdCoordinador" />
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>
                <asp:SqlDataSource ID="SDSgrados1" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select distinct N.IdNivel, N.Descripcion Nivel, G.IdGrado, G.Descripcion Grado 
                    from Nivel N, Escuela E, Grado G, Plantel P 
                    where (P.IdInstitucion = @IdInstitucion and E.IdPlantel = P.IdPlantel) and 
                    P.IdPlantel in (Select IdPlantel from CoordinadorPlantel where IdCoordinador = @IdCoordinador) 
                    and N.IdNivel = E.IdNivel and G.IdNivel = N.IdNivel and G.Estatus = 'Activo' 
                    and G.IdNivel = N.IdNivel order by N.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdCoordinador" SessionField="IdCoordinador" />
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSgrados2" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select distinct N.IdNivel, N.Descripcion Nivel, G.IdGrado, G.Descripcion Grado 
                    from Nivel N, Escuela E, Grado G 
                    where E.IdPlantel = @IdPlantel and N.IdNivel = E.IdNivel 
                    and G.IdNivel = N.IdNivel and G.Estatus = 'Activo' order by N.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

