﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="ConsultaMaterial.aspx.vb" 
    Inherits="coordinador_ConsultaMaterial" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            height: 24px;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style31 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
        }

        .style28 {
            height: 25px;
            width: 479px;
            text-align: left;
        }

        .style29 {
            height: 25px;
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 22px;
        }

        .style25 {
            height: 22px;
            width: 479px;
            text-align: left;
        }

        .style26 {
            height: 22px;
        }

        .style27 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: left;
            height: 25px;
            width: 888px;
        }

        .style14 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: center;
        }

        .style15 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            color: #FF0000;
        }

        .style19 {
            width: 232px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Consultas - Material de Actividades
    </h1>
    Muestra el material de apoyo (archivos) de todas las 
                    actividades asignadas a una 
												<asp:Label ID="Label1" runat="server" Text="[ASIGNATURA]" />
    .
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10" style="width: 91%">
        <tr>
            <td class="style31" colspan="2">
                &nbsp;</td>
            <td class="style28" colspan="2">
                &nbsp;</td>
            <td class="style29" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style31" colspan="2">
                <asp:Label ID="Label2" runat="server" Text="[INSTITUCION]" />
            </td>
            <td class="style28" colspan="2">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                    DataValueField="IdInstitucion" Height="22px" Width="400px">
                </asp:DropDownList>
            </td>
            <td class="style29" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style31" colspan="2">
                <asp:Label ID="Label3" runat="server" Text="[CICLO]" />
            </td>
            <td class="style28" colspan="2">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px" Width="400px">
                </asp:DropDownList>
            </td>
            <td class="style29" colspan="2"></td>
        </tr>
        <tr>
            <td class="style24" colspan="2">
                <asp:Label ID="Label4" runat="server" Text="[NIVEL]" />
            </td>
            <td class="style25" colspan="2">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Height="22px" Width="250px">
                </asp:DropDownList>
            </td>
            <td class="style26" colspan="2"></td>
        </tr>
        <tr>
            <td class="style31" colspan="2">
                <asp:Label ID="Label5" runat="server" Text="[GRADO]" />
            </td>
            <td class="style28" colspan="2">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                    Height="22px" Width="250px">
                </asp:DropDownList>
            </td>
            <td class="style29" colspan="2"></td>
        </tr>
        <tr>
            <td class="style31" colspan="2">
                <asp:Label ID="Label6" runat="server" Text="[ASIGNATURA]" />
            </td>
            <td class="style28" colspan="2">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSasignaturas" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Height="22px" Width="400px">
                </asp:DropDownList>
            </td>
            <td class="style29" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style27" colspan="2">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td class="style28" colspan="2">&nbsp;</td>
            <td class="style29" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style14" colspan="6">
                <asp:GridView ID="GVactividades" runat="server"  
                    AllowPaging="True" 
                    AutoGenerateColumns="False"
                    PageSize="15"

                    DataKeyNames="IdEvaluacion" 
                    DataSourceID="SDSactividades"
                    Width="665px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell">
                            <HeaderStyle Width="75px" />
                            <ItemStyle Width="75px" />
                        </asp:CommandField>
                        <asp:BoundField DataField="IdEvaluacion" HeaderText="IdEvaluacion"
                            InsertVisible="False" SortExpression="IdEvaluacion" Visible="False" />
                        <asp:BoundField DataField="Actividad" HeaderText="Actividad"
                            SortExpression="Actividad">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Inicia" DataFormatString="{0:dd/MM/yyyy}"
                            HeaderText="Inicia" SortExpression="Inicia">
                            <HeaderStyle Width="110px" />
                            <ItemStyle Width="110px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Termina" DataFormatString="{0:dd/MM/yyyy}"
                            HeaderText="Termina" SortExpression="Termina">
                            <HeaderStyle Width="110px" />
                            <ItemStyle Width="110px" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="style16" colspan="4"></td>
            <td class="style17" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style16" colspan="4">
                <asp:GridView ID="GVmaterial" runat="server"
                    AllowPaging="True"
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 
                    Caption="<h3>Seleccione el material de apoyo que desea consultar</h3>" 

                    DataSourceID="SDSmaterial" 
                    DataKeyNames="archivoMaterial"
                    Width="884px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True">
                            <HeaderStyle Width="75px" />
                            <ItemStyle Width="75px" CssClass="selectCell" />
                        </asp:CommandField>
                        <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"
                            SortExpression="Asignatura" />
                        <asp:BoundField DataField="NumTema" HeaderText="No. Tema"
                            SortExpression="NumTema" Visible="False" />
                        <asp:BoundField DataField="Tema" HeaderText="Tema" SortExpression="Tema" />
                        <asp:BoundField DataField="Numsubtema" HeaderText="No. Subtema"
                            SortExpression="Numsubtema" Visible="False" />
                        <asp:BoundField DataField="Subtema" HeaderText="Subtema"
                            SortExpression="Subtema" />
                        <asp:BoundField DataField="Material" HeaderText="Material"
                            SortExpression="Material"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort 
	                0  select
	                1  ASIGNATURA
	                2  numtema
	                3  tema
	                4  numsubtema
	                5  subtema
	                6  material
	                7  tipo
	                --%>
            </td>
            <td class="style17" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:Panel ID="PnlLink" runat="server" BackColor="#FFFF66" Visible="False">
                    <table class="style27" align="center">
                        <tr>
                            <td class="style32">Si el archivo no se abre automáticamente, da clic aquí:</td>
                            <td style="text-align: left">
                                <asp:HyperLink ID="HLarchivo" runat="server"
                                    Style="font-family: Arial, Helvetica, sans-serif; font-size: medium; color: #000099">HyperLink</asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:Panel ID="PnlDespliega" runat="server" BackColor="#E3EAEB"
                    Style="text-align: center">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style19" colspan="4">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT distinct I.IdInstitucion, I.Descripcion 
FROM Institucion I, CoordinadorPlantel CP, Plantel P
WHERE I.Estatus = 'Activo' and I.IdInstitucion = P.IdInstitucion
and P.IdPlantel = CP.IdPlantel and 
CP.IdCoordinador = (select C.IdCoordinador from Coordinador C, Usuario U where
U.Login = @Login and C.IdUsuario = U.IdUsuario)
ORDER BY I.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel) and (Estatus = 'Activo')
order by [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [Descripcion], [IdAsignatura] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) 
ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Distinct N.IdNivel, N.Descripcion
from Nivel N, Escuela E, Plantel P
where P.IdInstitucion = @IdInstitucion and E.IdPlantel = P.IdPlantel
and N.IdNivel = E.IdNivel and (N.Estatus = 'Activo') and
P.IdPlantel in (select IdPlantel from CoordinadorPlantel where IdCoordinador = (select IdCoordinador from Coordinador C, Usuario U where C.IdUsuario = U.IdUsuario and U.Login = @Login))
order by N.IdNivel">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSactividades" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select E.IdEvaluacion, E.ClaveBateria Actividad, E.Tipo, E.InicioContestar as 'Inicia', E.FinContestar as 'Termina'
from Evaluacion E, Asignatura A, Calificacion C
where A.IdAsignatura = @IdAsignatura and C.IdAsignatura = A.IdAsignatura 
and C.IdCicloEscolar = @IdCicloEscolar and E.IdCalificacion = C.IdCalificacion
order by E.InicioContestar, E.ClaveBateria">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSmaterial" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT     A.Descripcion AS Asignatura, T.Numero AS NumTema, T.Descripcion AS Tema, S.Numero AS Numsubtema, S.Descripcion AS Subtema, M.Descripcion AS Material, M.ArchivoApoyo AS archivoMaterial, M.TipoArchivoApoyo AS Tipo
FROM Evaluacion AS E 
INNER JOIN DetalleEvaluacion AS D ON E.IdEvaluacion = D.IdEvaluacion 
INNER JOIN Subtema AS S ON D.IdSubtema = S.IdSubtema 
INNER JOIN ApoyoSubtema AS M ON M.IdSubtema = S.IdSubtema
INNER JOIN Tema AS T ON S.IdTema = T.IdTema 
INNER JOIN Asignatura AS A ON T.IdAsignatura = A.IdAsignatura
WHERE (E.IdEvaluacion = @IdEvaluacion)
ORDER BY T.Numero, S.Numero, M.Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVactividades" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

