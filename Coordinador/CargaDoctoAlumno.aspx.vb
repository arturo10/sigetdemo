﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO 'Para que funcione: FileInfo

Partial Class coordinador_CargaDoctoAlumno
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = Trim(User.Identity.Name)
        TitleLiteral.Text = "Cargar Documento a " & Config.Etiqueta.ALUMNO

        Label1.Text = Config.Etiqueta.ALUMNO
        Label2.Text = Config.Etiqueta.ALUMNOS
        Label3.Text = Config.Etiqueta.INSTITUCION
        Label4.Text = Config.Etiqueta.CICLO
        Label5.Text = Config.Etiqueta.PLANTEL
        Label6.Text = Config.Etiqueta.NIVEL
        Label7.Text = Config.Etiqueta.GRADO
        Label8.Text = Config.Etiqueta.GRUPO
        Label9.Text = Config.Etiqueta.ALUMNO
        GVdatos.Caption = "<h3>" &
            Config.Etiqueta.ALUMNOS &
            " de " & Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO &
            " seleccionad" & Config.Etiqueta.LETRA_GRUPO &
            ". Elija a " & Config.Etiqueta.ARTDET_GRUPO & " que desee cargarle el documento</h3>"
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRADO & " " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRUPO & " " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub BtnCargar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCargar.Click
        If (RutaArchivo.HasFile) Then 'El atributo .HasFile compara si se indico un archivo
            Dim strConexion As String
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objConexion As New SqlConnection(strConexion)
            Dim miComando As SqlCommand
            Dim misRegistros As SqlDataReader
            miComando = objConexion.CreateCommand
            Try
                miComando.CommandText = "select * from DoctoEvaluacion where IdEvaluacion = " + GVactividades.SelectedDataKey("IdEvaluacion").ToString + " and IdAlumno = " + GVdatos.SelectedDataKey("IdAlumno").ToString
                objConexion.Open() 'Abro la conexion
                misRegistros = miComando.ExecuteReader()
                Dim Cargar As Boolean

                If misRegistros.Read() Then
                    If IsDBNull(misRegistros.Item("Calificacion")) Then
                        Cargar = True
                    Else
                        Cargar = False
                        msgError.show("Ya no puede cargar el archivo porque uno ya fue cargado y calificado.")
                    End If
                Else
                    Cargar = True
                End If

                If Cargar Then
                    'ALGORITMO PARA CARGAR ARCHIVO
                    Dim ruta As String
          ruta = Config.Global.rutaCargas

          'Verifico si ya existe el directorio del alumno, si no, lo creo
          ruta = ruta + Trim(GVdatos.SelectedDataKey("Login").ToString) + "\" 'Esta ruta contempla un directorio como alumno nombrado como su login
          If Not (Directory.Exists(ruta)) Then
            Dim directorio As DirectoryInfo = Directory.CreateDirectory(ruta)
          End If

          'Lo siguiente es por si ya había subido archivo, lo borre antes de volver a subir uno nuevo. De esta manera,
          'Si esta subiendo un archivo con el mismo nombre pero es de él mismo, no importa. Pero en caso de que exista
          'un archivo con el mismo nombre de otro usuario, entonces si le restringe el subirlo

          'If Trim(GVdatos.SelectedRow.Cells(9).Text) <> "&nbsp;"  Then --> no funciona porque el campo 9 es un link
          'Ni if Trim(GVdatos.SelectedDataKey("Documento").ToString) <> "&nbsp;" then
          If Trim(GVdatos.SelectedDataKey("Documento").ToString).Length > 3 Then
            SDSdoctoevaluacion.DeleteCommand = "DELETE FROM DoctoEvaluacion WHERE IdEvaluacion = " + GVactividades.SelectedDataKey("IdEvaluacion").ToString + " and IdAlumno = " + GVdatos.SelectedDataKey("IdAlumno").ToString
            SDSdoctoevaluacion.Delete()

            Dim MiArchivo As FileInfo = New FileInfo(ruta & Trim(GVdatos.SelectedRow.Cells(9).Text))
            MiArchivo.Delete()
          End If

          'Ahora inserto el archivo
          Dim MiArchivo2 As FileInfo = New FileInfo(ruta & RutaArchivo.FileName)
          If MiArchivo2.Exists Then
            'Avisa que ya existe, NO LO SUBE pero sí almacena el registro
            msgError.show("YA EXISTE UN ARCHIVO CON ESE NOMBRE EN EL ALMACEN DE ARCHIVOS DEL " & Config.Etiqueta.ALUMNO & ", CAMBIE EL NOMBRE DE SU ARCHIVO E INTENTE CARGARLO NUEVAMENTE, POR FAVOR.")
          Else
            RutaArchivo.SaveAs(ruta & RutaArchivo.FileName)

            SDSdoctoevaluacion.InsertCommand = "SET dateformat dmy; INSERT INTO DoctoEvaluacion (IdEvaluacion,IdAlumno,Documento,Calificacion) VALUES (" + GVactividades.SelectedDataKey("IdEvaluacion").ToString + "," + GVdatos.SelectedDataKey("IdAlumno").ToString + ",'" + RutaArchivo.FileName + "'," + DDLcalifica.SelectedValue.ToString + ")"
            SDSdoctoevaluacion.Insert()
            msgError.hide()
          End If
        End If
        GVdatos.DataBind()
        misRegistros.Close()

        'PONGO LA CALIFICACIÓN
        miComando.CommandText = "select * from EvaluacionTerminada where IdEvaluacion = " + GVactividades.SelectedDataKey("IdEvaluacion").ToString + " and IdAlumno = " + GVdatos.SelectedDataKey.Values(0).ToString
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros            
        If misRegistros.Read() Then 'Leo para poder accesarlos
          SDSevaluacionterminada.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionTerminada set Resultado = " + DDLcalifica.SelectedValue.ToString + ", FechaTermino = cast(getdate() as smalldatetime) where IdEvaluacionT = " + misRegistros.Item("IdEvaluacionT").ToString
          SDSevaluacionterminada.Update()
        Else
          misRegistros.Close()
          miComando.CommandText = "select * from DetalleEvaluacion where IdEvaluacion = " + GVactividades.SelectedDataKey("IdEvaluacion").ToString
          misRegistros = miComando.ExecuteReader()
          misRegistros.Read()

          SDSevaluacionterminada.InsertCommand = "SET dateformat dmy; INSERT INTO EvaluacionTerminada(IdAlumno,IdGrado,IdGrupo,IdEvaluacion,TotalReactivos,ReactivosContestados,PuntosPosibles,PuntosAcertados,Resultado,FechaTermino) " + _
                          "VALUES(" + GVdatos.SelectedDataKey("IdAlumno").ToString + "," + DDLgrado.SelectedValue.ToString + "," + _
                              DDLgrupo.SelectedValue.ToString + "," + _
                              GVactividades.SelectedDataKey("IdEvaluacion").ToString + ",0,0,100," + DDLcalifica.SelectedValue.ToString + "," + DDLcalifica.SelectedValue.ToString + ",cast(getdate() as smalldatetime))"
          SDSevaluacionterminada.Insert()

        End If
        misRegistros.Close()
        objConexion.Close()
        GVdatos.DataBind()
        msgError.hide()
      Catch ex As Exception
        Utils.LogManager.ExceptionLog_InsertEntry(ex)
        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      End Try
    Else
      msgError.show("Debe elegir un archivo para ser cargado.")
    End If
  End Sub

  Protected Sub GVdatos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVdatos.SelectedIndexChanged
    PnlCargar.Visible = True
  End Sub

  Protected Sub BtnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEliminar.Click
    Try
      'If Trim(GVdatos.SelectedRow.Cells(9).Text) <> "&nbsp;"  Then --> no funciona porque el campo 9 es un link
      'Ni if Trim(GVdatos.SelectedDataKey("Documento").ToString) <> "&nbsp;" then
      If Trim(GVdatos.SelectedDataKey("Documento").ToString).Length > 3 Then
        SDSdoctoevaluacion.DeleteCommand = "DELETE FROM DoctoEvaluacion WHERE IdEvaluacion = " + GVactividades.SelectedDataKey("IdEvaluacion").ToString + " and IdAlumno = " + GVdatos.SelectedDataKey("IdAlumno").ToString
        SDSdoctoevaluacion.Delete()
        Dim ruta As String
        ruta = Config.Global.rutaCargas
        ruta = ruta + Trim(GVdatos.SelectedDataKey("Login").ToString) + "\"
        'Borro el archivo:
        Dim MiArchivo As FileInfo = New FileInfo(ruta & Trim(GVdatos.SelectedDataKey("Documento").ToString))
        MiArchivo.Delete()
        GVdatos.DataBind()
        msgError.hide()
      Else
        msgError.show("NO HAY NINGUN ARCHIVO CARGADO PARA ELIMINAR.")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GVactividades_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVactividades.RowDataBound
    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(6).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA
    End If

    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVactividades, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  Protected Sub GVactividades_PreRender(sender As Object, e As EventArgs) Handles GVactividades.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVactividades.Rows.Count > 0 Then
      GVactividades.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVactividades_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVactividades.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVactividades.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVactividades.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVactividades, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    For Each r In GVdatos.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVdatos, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

  Protected Sub GVdatos_PreRender(sender As Object, e As EventArgs) Handles GVdatos.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVdatos.Rows.Count > 0 Then
      GVdatos.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVdatos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVdatos.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

    Protected Sub GVdatos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(4).Controls(1)
            LnkHeaderText.Text = "Nombre de " & Config.Etiqueta.ARTDET_ALUMNO & " " & Config.Etiqueta.ALUMNO

            LnkHeaderText = e.Row.Cells(5).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.GRUPO

            LnkHeaderText = e.Row.Cells(8).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.EQUIPO
        End If

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVdatos, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

End Class
