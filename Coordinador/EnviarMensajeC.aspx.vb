﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO

Partial Class coordinador_EnviarMensajeC
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            UserInterface.Include.HtmlEditor(CType(Master.FindControl("pnlHeader"), Literal), 665, 200, 12, Session("Usuario_Idioma").ToString())

            aplicaLenguaje()

            initPageData()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        Label1.Text = Config.Etiqueta.PROFESORES
        Label2.Text = Config.Etiqueta.PLANTELES
        Label3.Text = Config.Etiqueta.CICLO
        Label4.Text = Config.Etiqueta.PROFESORES
        GVplanteles.Caption = "<h3>Seleccione " &
            Config.Etiqueta.ARTDET_PLANTEL & " " & Config.Etiqueta.PLANTEL &
            " donde pertenece el destinatario.</h3>"

        DDLenvia.Items(0).Text = Config.Etiqueta.PROFESORES
        DDLenvia.Items(1).Text = Config.Etiqueta.ALUMNOS
    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub initPageData()
        Session("Login") = User.Identity.Name

        ' necesito obtener los datos del alumno para usarse al enviar mensajes
        Try
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()

                Dim cmd As SqlCommand = New SqlCommand("SELECT C.IdCoordinador, C.Nombre, C.Apellidos FROM Usuario U, Coordinador C where U.Login = @Login and C.IdUsuario = U.IdUsuario", conn)
                cmd.Parameters.AddWithValue("@Login", Trim(User.Identity.Name))

                Dim results As SqlDataReader = cmd.ExecuteReader()
                results.Read()
                LblNombre.Text = "COORDINADOR: " + results.Item("Nombre") + " " + results.Item("Apellidos")
                Session("NombreCoordinador") = results.Item("Nombre") + " " + results.Item("Apellidos")
                Session("IdCoordinador") = results.Item("IdCoordinador")
                hfCarpetaAdjunto.Value = User.Identity.Name.Trim()

                results.Close()
                cmd.Dispose()
                conn.Close()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try

        ' si viene de mensaje y presionó reenviar, paso la información del mensaje a reenviar
        ' esto debe estar después de initpagedata() puesto que sobreescribo la carpeta de adjuntos si es reenvio
        If Not IsNothing(Session("Reenvio")) Then
            DDLenvia.SelectedValue = Session("Reenvio") ' reenvio tiene que tener P o A
            actualizaPaneles() ' actualizo páneles de acuerdo al destino
            hfIsReenvio.Value = "True"

            Dim asunto As String
            asunto = "RENV: " + Session("Envio_Asunto") + " [" + Session("Envio_Fecha") + "]"
            TBasunto.Text = Mid(asunto, 1, 100)

            TBcontenido.Text = Session("Envio_Contenido")
            lbAdjunto.Text = Session("Envio_AdjuntoNombre")
            hfAdjuntoFisico.Value = Session("Envio_AdjuntoFisico")
            hfCarpetaAdjunto.Value = Session("Envio_Carpeta")

            BtnAdjuntar.Visible = False
            FUadjunto.Visible = False
            BtnQuitar.Visible = False

            Session.Remove("Reenvio")
            Session.Remove("Envio_Asunto")
            Session.Remove("Envio_Fecha")
            Session.Remove("Envio_Contenido")
            Session.Remove("Envio_AdjuntoNombre")
            Session.Remove("Envio_AdjuntoFisico")
            Session.Remove("Envio_Carpeta")
        End If

        ' inicializo los hidden fields de búsqueda para que las consultas funcionen bien sin búsquedas
        hfBuscaProfNombre.Value = "%"
        hfBuscaProfApellidos.Value = "%"
        hfBuscaGrupoNombre.Value = "%"
        hfBuscaGrupoGrado.Value = "%"
        hfBuscaProfApellidos.Value = "%"
    End Sub

#End Region

#Region "GVplanteles"

    Protected Sub GVplanteles_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVplanteles.DataBound
        If (GVplanteles.Rows.Count = 0) And (DDLcicloescolar.SelectedIndex > 0) Then
            msgInfo.show("No tiene " & Config.Etiqueta.PLANTELES & " asignados.")
        Else
            msgInfo.hide()
        End If
    End Sub

    Protected Sub GVplanteles_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVplanteles.SelectedIndexChanged
        If GVplanteles.SelectedIndex > -1 Then
            pnlDestinatarios.Visible = True
            GVgrupos.SelectedIndex = -1
        End If
        msgError.hide()
    End Sub

    Protected Sub GVplanteles_PreRender(sender As Object, e As EventArgs) Handles GVplanteles.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVplanteles.Rows.Count > 0 Then
            GVplanteles.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVplanteles_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVplanteles.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVplanteles, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

#End Region

#Region "GVgrupos"

    Protected Sub GVgrupos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVgrupos.DataBound
        If hfBusquedaGrupos_isActive.Value.Length > 0 Then
            msgInfoGrupos.show(GVgrupos.Rows.Count & " " & Config.Etiqueta.GRUPOS & " encontrad" & Config.Etiqueta.LETRA_GRUPO & "s.")
        Else
            If (GVgrupos.Rows.Count = 0) And (DDLcicloescolar.SelectedIndex > 0) Then
                msgInfoGrupos.show("No hay " & Config.Etiqueta.GRUPOS & " asignad" & Config.Etiqueta.LETRA_GRUPO & "s.")
            End If
        End If
    End Sub

    Protected Sub GVgrupos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVgrupos.SelectedIndexChanged
        CBLprofesores.Visible = True
        msgInfo.hide()
        msgError.hide()
    End Sub

    Protected Sub GVgrupos_PreRender(sender As Object, e As EventArgs) Handles GVgrupos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVgrupos.Rows.Count > 0 Then
            GVgrupos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVgrupos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVgrupos.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVgrupos, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

#End Region

#Region "adjuntos"

    Protected Sub BtnAdjuntar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdjuntar.Click
        Try  'Cuando se adjunta el archivo, se borra el nombre del FileUpload
            If FUadjunto.HasFile Then
        Dim ruta As String = Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value & "\" 'Esta ruta contempla un directorio como alumno nombrado como su login

        'Verifico si ya existe el directorio del alumno, si no, lo creo
        If Not (Directory.Exists(ruta)) Then
          Dim directorio As DirectoryInfo = Directory.CreateDirectory(ruta)
        End If

        'Subo archivo
        Dim now As String = Date.Now.Year.ToString & "_" & Date.Now.Month.ToString & "_" & Date.Now.Day.ToString & "_" & Date.Now.Hour.ToString & "_" & Date.Now.Minute.ToString & "_" & Date.Now.Second.ToString & "_" & Date.Now.Millisecond.ToString
        hfAdjuntoFisico.Value = now & FUadjunto.FileName
        FUadjunto.SaveAs(ruta & hfAdjuntoFisico.Value)
        lbAdjunto.Text = FUadjunto.FileName

        msgError.hide()
        msgSuccess.show("Se ha adjuntado el archivo.")
      Else
        msgError.show("No ha seleccionado un archivo para adjuntar.")
        msgSuccess.hide()
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      msgSuccess.hide()
    End Try
  End Sub

  Protected Sub lbAdjunto_Click(sender As Object, e As EventArgs) Handles lbAdjunto.Click
    Dim file As System.IO.FileInfo =
        New System.IO.FileInfo(Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value + "\" & hfAdjuntoFisico.Value) '-- if the file exists on the server

    If file.Exists Then 'set appropriate headers
      Response.Clear()
      Response.AddHeader("Content-Disposition", "attachment; filename=""" & lbAdjunto.Text & """")
      Response.AddHeader("Content-Length", file.Length.ToString())
      Response.ContentType = "application/octet-stream"
      Response.WriteFile(file.FullName)
      'Response.End()
    Else
      msgError.show("Ha ocurrido un error con el archivo adjunto: éste ya no está disponible.")
    End If 'nothing in the URL as HTTP GET
  End Sub

  Protected Sub BtnQuitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnQuitar.Click
    quitarAdjunto()
  End Sub

  Protected Sub quitarAdjunto()
    Try
      If hfAdjuntoFisico.Value.Length > 0 Then
        Dim ruta As String = Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value & "\" 'Esta ruta contempla un directorio como alumno nombrado como su login

        'Si NO es reenvio, borro el archivo que estaba anteriormente porque se pondrá uno nuevo
        If hfIsReenvio.Value = "False" Then
          Dim adjunto As FileInfo = New FileInfo(ruta & hfAdjuntoFisico.Value)
          adjunto.Delete()
        End If

        lbAdjunto.Text = ""
        hfAdjuntoFisico.Value = ""
        msgError.hide()
        msgSuccess.show("Se ha eliminado el archivo.")
      Else
        msgError.show("No ha adjuntado ningún archivo todavía.")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgSuccess.hide()
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

#End Region

  Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click
    Dim I As UInteger
    For I = 0 To CBLprofesores.Items.Count - 1
      CBLprofesores.Items(I).Selected = True
    Next
  End Sub

  Protected Sub LinkButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton2.Click
    Dim I As UInteger
    For I = 0 To CBLprofesores.Items.Count - 1
      CBLprofesores.Items(I).Selected = False
    Next
  End Sub

  Protected Sub DDLenvia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLenvia.SelectedIndexChanged
    actualizaPaneles()
  End Sub

  Protected Sub actualizaPaneles()
    If DDLenvia.SelectedValue = "P" Then
      PnlAlumnos.Visible = False
      PnlProfesor.Visible = True
      BtnEnviar.Visible = True
    ElseIf DDLenvia.SelectedValue = "A" Then
      PnlAlumnos.Visible = True
      PnlProfesor.Visible = False
      BtnEnviar.Visible = True
    End If
  End Sub

  Protected Sub BtnLimpiar_Click(sender As Object, e As EventArgs) Handles BtnLimpiar.Click
    TBasunto.Text = ""
    TBcontenido.Text = ""
    ' al limpiar se limpia el adjunto, y debe eliminarse el archivo
    quitarAdjunto()
    ' se ocultan los mensajes al final puesto que al quitar el archivo pueden mostrarse
    msgSuccess.hide()
    msgError.hide()
  End Sub

#Region "enviar"

  Protected Sub BtnEnviar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEnviar.Click
    If (Trim(TBasunto.Text).Length > 0) And (Trim(TBcontenido.Text).Length) > 0 Then
      If DDLenvia.SelectedValue = "P" Then
        ' correo para profesores
        ' _____________________________________________________________
        ' itero a través de los profesores para asegurarme que al menos esté uno seleccionado
        Dim seleccionado As Boolean = False
        For Each l As ListItem In CBLprofesores.Items
          If l.Selected Then
            seleccionado = True
          End If
        Next

        If Not seleccionado Then
          msgError.show("Debe seleccionar al menos un destinatario.")
        Else
          enviarMensajeProfesor()
        End If
      Else
        ' correo para coordinadores
        ' _____________________________________________________________
        ' itero a través de los profesores para asegurarme que al menos esté uno seleccionado
        Dim seleccionado As Boolean = False
        For Each l As ListItem In CBLalumnos.Items
          If l.Selected Then
            seleccionado = True
          End If
        Next

        If Not seleccionado Then
          msgError.show("Debe seleccionar al menos un destinatario.")
        Else
          enviarMensajeAlumno()
        End If
      End If
    Else
      msgError.show("Indique el asunto y el contenido de su mensaje.")
      msgSuccess.hide()
    End If
  End Sub

  Protected Sub enviarMensajeProfesor()
    Try
      'Las siguientes variables las necesitaré para buscar Email del profesor y mandarle correo
      Dim Email As String

      Dim I As UInteger
      Dim Cont = 0
      For I = 0 To CBLprofesores.Items.Count - 1
        If CBLprofesores.Items(I).Selected Then
          Cont += 1
          ' busco el Email del profesor para mandarle correo
          Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
            conn.Open()

            Dim cmd As SqlCommand = New SqlCommand("SELECT Email FROM Profesor WHERE IdProfesor = @IdProfesor", conn)
            cmd.Parameters.AddWithValue("@IdProfesor", CBLprofesores.Items(I).Value)

            Dim results As SqlDataReader = cmd.ExecuteReader()
            results.Read()
            If IsDBNull(results.Item("Email")) Then 'Si intento asignar un valor Nulo a una variable, marca error
              Email = ""
            Else
              Email = results.Item("Email")
            End If

            results.Close()
            cmd.Dispose()
            conn.Close()
          End Using

          'Si el profesor está seleccionado, inserto el mensaje para él [Sentido (E=Envia, E=Recibe - desde el punto de vista del profesor)][Estatus (Leido, Nuevo, Baja)]
          SDScomunicacionCP.InsertCommand = "SET dateformat dmy; INSERT INTO ComunicacionCP(IdCoordinador,IdProfesor,Sentido,Asunto,Contenido,Fecha,ArchivoAdjunto,ArchivoAdjuntoFisico,CarpetaAdjunto,EmailEnviado,Estatus, EstatusP)" &
                  " VALUES(@IdCoordinador, @IdProfesor, 'E', @Asunto, @Contenido, getdate(), @NombreAdjunto, @RutaAdjunto, @CarpetaAdjunto, @EmailEnviado, 'Nuevo', 'Nuevo')"
          SDScomunicacionCP.InsertParameters.Clear()
          SDScomunicacionCP.InsertParameters.Add("IdCoordinador", Session("IdCoordinador").ToString)
          SDScomunicacionCP.InsertParameters.Add("IdProfesor", CBLprofesores.Items(I).Value)
          SDScomunicacionCP.InsertParameters.Add("Asunto", TBasunto.Text)
          SDScomunicacionCP.InsertParameters.Add("Contenido", TBcontenido.Text)
          SDScomunicacionCP.InsertParameters.Add("NombreAdjunto", lbAdjunto.Text)
          SDScomunicacionCP.InsertParameters.Add("RutaAdjunto", hfAdjuntoFisico.Value)
          SDScomunicacionCP.InsertParameters.Add("CarpetaAdjunto", hfCarpetaAdjunto.Value)
          SDScomunicacionCP.InsertParameters.Add("EmailEnviado", Email)
          SDScomunicacionCP.Insert()

          If Trim(Email).Length > 1 Then
            If Config.Global.MAIL_ACTIVO Then
              Utils.Correo.EnviaCorreo(Trim(Email),
                                       Config.Global.NOMBRE_FILESYSTEM & " - Mensaje en " & Config.Etiqueta.SISTEMA_CORTO,
                                       "Tiene un nuevo mensaje en " & Config.Global.NOMBRE_FILESYSTEM & " que le ha enviado " + Session("NombreCoordinador"),
                                       False)
            End If
          End If
        End If
      Next

      msgSuccess.show("Éxito", "Se han enviado los mensajes a " + Cont.ToString + " " & Config.Etiqueta.PROFESORES & ". A los que tienen EMAIL registrado, se les envió una notificación del mensaje.")
      msgError.hide()
      TBasunto.Text = ""
      TBcontenido.Text = ""
      lbAdjunto.Text = ""
      hfAdjuntoFisico.Value = ""
      ' Deselecciono a los profesores porque ya se les envió el mensaje
      For Each l As ListItem In CBLprofesores.Items
        l.Selected = False
      Next
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      msgSuccess.hide()
    End Try
  End Sub

  Protected Sub enviarMensajeAlumno()
    Try
      'Las siguientes variables las necesitaré para buscar Email del alumno y mandarle correo
      Dim Email As String

      Dim I As UInteger
      Dim Cont = 0
      For I = 0 To CBLalumnos.Items.Count - 1
        If CBLalumnos.Items(I).Selected Then
          Cont += 1
          ' busco el Email del Alumno para mandarle correo
          Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
            conn.Open()

            Dim cmd As SqlCommand = New SqlCommand("SELECT Email FROM Alumno WHERE IdAlumno = @IdAlumno", conn)
            cmd.Parameters.AddWithValue("@IdAlumno", CBLalumnos.Items(I).Value)

            Dim results As SqlDataReader = cmd.ExecuteReader()
            results.Read()
            If IsDBNull(results.Item("Email")) Then 'Si intento asignar un valor Nulo a una variable, marca error
              Email = ""
            Else
              Email = results.Item("Email")
            End If

            results.Close()
            cmd.Dispose()
            conn.Close()
          End Using

          'Si el profesor está seleccionado, inserto el mensaje para él [Sentido (E=Envia, E=Recibe - desde el punto de vista del profesor)][Estatus (Leido, Nuevo, Baja)]
          SDScomunicacionCP.InsertCommand = "SET dateformat dmy; INSERT INTO ComunicacionCA(IdCoordinador,IdAlumno,Sentido,Asunto,Contenido,Fecha,ArchivoAdjunto,ArchivoAdjuntoFisico,CarpetaAdjunto,EmailEnviado,Estatus, EstatusA)" &
                  " VALUES(@IdCoordinador, @IdAlumno, 'E', @Asunto, @Contenido, getdate(), @NombreAdjunto, @RutaAdjunto, @CarpetaAdjunto, @EmailEnviado, 'Nuevo', 'Nuevo')"
          SDScomunicacionCP.InsertParameters.Clear()
          SDScomunicacionCP.InsertParameters.Add("IdCoordinador", Session("IdCoordinador").ToString)
          SDScomunicacionCP.InsertParameters.Add("IdAlumno", CBLalumnos.Items(I).Value)
          SDScomunicacionCP.InsertParameters.Add("Asunto", TBasunto.Text)
          SDScomunicacionCP.InsertParameters.Add("Contenido", TBcontenido.Text)
          SDScomunicacionCP.InsertParameters.Add("NombreAdjunto", lbAdjunto.Text)
          SDScomunicacionCP.InsertParameters.Add("RutaAdjunto", hfAdjuntoFisico.Value)
          SDScomunicacionCP.InsertParameters.Add("CarpetaAdjunto", hfCarpetaAdjunto.Value)
          SDScomunicacionCP.InsertParameters.Add("EmailEnviado", Email)
          SDScomunicacionCP.Insert()

          If Trim(Email).Length > 1 Then
            If Config.Global.MAIL_ACTIVO Then
              Utils.Correo.EnviaCorreo(Trim(Email),
                                       Config.Global.NOMBRE_FILESYSTEM & " - Mensaje en " & Config.Etiqueta.SISTEMA_CORTO,
                                       "Tiene un nuevo mensaje en " & Config.Global.NOMBRE_FILESYSTEM & " que le ha enviado " + Session("NombreCoordinador"),
                                       False)
            End If
          End If
        End If
      Next

      msgSuccess.show("Éxito", "Se han enviado los mensajes a " + Cont.ToString + " " & Config.Etiqueta.ALUMNOS & ". A los que tienen EMAIL registrado, se les envió una notificación del mensaje.")
      msgError.hide()
      TBasunto.Text = ""
      TBcontenido.Text = ""
      lbAdjunto.Text = ""
      hfAdjuntoFisico.Value = ""
      ' Deselecciono a los alumnos porque ya se les envió el mensaje
      For Each l As ListItem In CBLalumnos.Items
        l.Selected = False
      Next
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      msgSuccess.hide()
    End Try
  End Sub

#End Region

#Region "busqueda"

    Protected Sub CBLprofesores_DataBound(sender As Object, e As EventArgs) Handles CBLprofesores.DataBound
        If GVplanteles.SelectedIndex > -1 Then
            If hfBusquedaProfesores_isActive.Value.Length > 0 Then
                msgInfoProfesores.show(CBLprofesores.Items.Count & " " & Config.Etiqueta.PROFESORES & " encontrad" & Config.Etiqueta.LETRA_PROFESOR & "s.")
            Else
                If (CBLprofesores.Items.Count = 0) Then
                    msgInfoProfesores.show("No hay " & Config.Etiqueta.PROFESORES & " asignad" & Config.Etiqueta.LETRA_PROFESOR & "s.")
                End If
            End If
        End If
    End Sub

    Protected Sub btnBuscaProfesor_Click(sender As Object, e As EventArgs) Handles btnBuscaProfesor.Click
        ' si había una fila seleccionada la quito
        Dim I As UInteger
        For I = 0 To CBLprofesores.Items.Count - 1
            CBLprofesores.Items(I).Selected = False
        Next

        hfBuscaProfNombre.Value = "%" & tbBuscaProfesorNombre.Text & "%"
        hfBuscaProfApellidos.Value = "%" & tbBuscaProfesorApellidos.Text & "%"
        hfBusquedaProfesores_isActive.Value = "1"
    End Sub

    Protected Sub btnLimpiaBusquedaProfesor_Click(sender As Object, e As EventArgs) Handles btnLimpiaBusquedaProfesor.Click
        ' si había una fila seleccionada la quito
        Dim I As UInteger
        For I = 0 To CBLprofesores.Items.Count - 1
            CBLprofesores.Items(I).Selected = False
        Next

        tbBuscaProfesorNombre.Text = ""
        tbBuscaProfesorApellidos.Text = ""
        hfBuscaProfNombre.Value = "%"
        hfBuscaProfApellidos.Value = "%"
        hfBusquedaProfesores_isActive.Value = ""
        msgInfoProfesores.hide()
    End Sub

    Protected Sub CBLalumnos_DataBound(sender As Object, e As EventArgs) Handles CBLalumnos.DataBound
        If GVplanteles.SelectedIndex > -1 Then
            If GVgrupos.SelectedIndex > -1 Then
                If (CBLalumnos.Items.Count = 0) Then
                    msgInfo.show("No hay " & Config.Etiqueta.ALUMNOS & " asignad" & Config.Etiqueta.LETRA_ALUMNO & "s.")
                Else
                    msgInfo.hide()
                End If
            End If
        End If
    End Sub

    Protected Sub btnBuscaGrupo_Click(sender As Object, e As EventArgs) Handles btnBuscaGrupo.Click
        ' si había una fila seleccionada la quito
        GVgrupos.SelectedIndex = -1

        hfBuscaGrupoNombre.Value = "%" & tbBuscaGruposNombre.Text & "%"
        hfBuscaGrupoGrado.Value = "%" & tbBuscaGruposGrado.Text & "%"
        hfBusquedaGrupos_isActive.Value = "1"
    End Sub

    Protected Sub btnLimpiaBusquedaGrupo_Click(sender As Object, e As EventArgs) Handles btnLimpiaBusquedaGrupo.Click
        ' si había una fila seleccionada la quito
        GVgrupos.SelectedIndex = -1

        tbBuscaGruposNombre.Text = ""
        tbBuscaGruposGrado.Text = ""
        hfBuscaGrupoNombre.Value = "%"
        hfBuscaGrupoGrado.Value = "%"
        hfBusquedaGrupos_isActive.Value = ""
        msgInfoGrupos.hide()
    End Sub

#End Region

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVplanteles.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVplanteles, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        For Each r In GVgrupos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVgrupos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub
End Class
