﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="ConsultaDoctos.aspx.vb" 
    Inherits="coordinador_ConsultaDoctos" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 36px;
        }

        .style13 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
        }

        .style15 {
            font-family: Arial, Helvetica, sans-serif;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }
        .auto-style1 {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Consultas - Documentos Entregados
    </h1>
    Permite consultar la calificación y el archivo de los 
                    documentos entregados por los 
												<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    en una actividad de Entrega de Archivo.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label2" runat="server" Text="[INSTITUCION]" />
            </td>
            <td class="auto-style1" colspan="2">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    Height="22px" Width="350px" DataSourceID="SDSinstituciones"
                    DataTextField="Descripcion" DataValueField="IdInstitucion">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label3" runat="server" Text="[CICLO]" />
            </td>
            <td class="auto-style1" colspan="2">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px"
                    Width="350px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label4" runat="server" Text="[NIVEL]" />
            </td>
            <td class="auto-style1" colspan="2">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Height="22px" Width="270px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label5" runat="server" Text="[GRADO]" />
            </td>
            <td class="auto-style1" colspan="2">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                    Height="22px" Width="270px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label6" runat="server" Text="[PLANTEL]" />
            </td>
            <td class="auto-style1" colspan="2">
                <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                    DataValueField="IdPlantel" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style15" colspan="6">
                <asp:GridView ID="GVactividades" runat="server"
                    AllowPaging="True" 
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 
                    Caption="<h3>Listado de Actividades para Entrega de Documentos</h3>"
                    PageSize="20"

                    DataSourceID="SDSdatos"
                    DataKeyNames="IdEvaluacion,IdAsignatura"
                    Width="888px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell">
                            <HeaderStyle Width="80px" />
                            <ItemStyle Width="80px" />
                        </asp:CommandField>
                        <asp:BoundField DataField="IdEvaluacion" HeaderText="IdEvaluacion"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion"
                            Visible="False" />
                        <asp:BoundField DataField="Evaluacion (Actividad)"
                            HeaderText="Evaluacion (Actividad)" SortExpression="Evaluacion (Actividad)" />
                        <asp:BoundField DataField="Abreviación" HeaderText="Abreviación"
                            SortExpression="Abreviación" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                        <asp:BoundField DataField="Periodo Califica" HeaderText="Periodo Califica"
                            SortExpression="Periodo Califica" />
                        <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"
                            SortExpression="Asignatura" />
                        <asp:BoundField DataField="IdAsignatura" HeaderText="IdAsignatura"
                            InsertVisible="False" SortExpression="IdAsignatura" Visible="False" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort 
	                0  select
	                1  idevaluacion
	                2  evaluacion (actividad)
	                3  abreviacion
	                4  tipo
	                5  periodo califica
	                6  ASIGNATURA
	                7  idasignatura
	                8  
	                9  
	                10 
	                --%>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td class="style15">&nbsp;</td>
            <td colspan="4">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT distinct I.IdInstitucion, I.Descripcion 
FROM Institucion I, CoordinadorPlantel CP, Plantel P
WHERE I.Estatus = 'Activo' and I.IdInstitucion = P.IdInstitucion
and P.IdPlantel = CP.IdPlantel and 
CP.IdCoordinador = (select C.IdCoordinador from Coordinador C, Usuario U where
U.Login = @Login and C.IdUsuario = U.IdUsuario)
ORDER BY I.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.IdPlantel, P.Descripcion
from Plantel P, CoordinadorPlantel C
where C.IdCoordinador in (Select IdCoordinador from Coordinador, Usuario
where Coordinador.IdUsuario = Usuario.IdUsuario and Login = @Login)
and P.IdPlantel = C.IdPlantel and P.IdInstitucion = @IdInstitucion 
order by P.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Distinct N.IdNivel, N.Descripcion
from Nivel N, Escuela E, Plantel P
where P.IdInstitucion = @IdInstitucion and E.IdPlantel = P.IdPlantel
and N.IdNivel = E.IdNivel and (N.Estatus = 'Activo')
order by N.IdNivel">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE (([IdNivel] = @IdNivel) AND ([Estatus] = @Estatus)) ORDER BY [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSdatos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select E.IdEvaluacion, E.ClaveBateria as 'Evaluacion (Actividad)', E.ClaveAbreviada as 'Abreviación', E.Tipo, C.Descripcion 'Periodo Califica', A.Descripcion Asignatura, A.IdAsignatura
from Calificacion C, Evaluacion E, Asignatura A, Grado G, Escuela Es
     where G.IdGrado = @IdGrado
     and Es.IdNivel = G.IdNivel and Es.IdPlantel = @IdPlantel
     and A.IdGrado = G.IdGrado and C.IdAsignatura = A.IdAsignatura and C.IdCicloEscolar = @IdCicloEscolar
and E.IdCalificacion = C.IdCalificacion and E.SeEntregaDocto = 'True'
order by A.Descripcion, C.Consecutivo, E.InicioContestar">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

