﻿<%@ Page Title="" 
    Language="C#" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="true" 
    CodeFile="IndicadoresAvance.aspx.cs" 
    Inherits="coordinador_IndicadoresAvance"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .titleColumn {
            text-align: right;
        }

        .control_column {
            text-align: left;
        }

        .overlay_top {
            z-index: 10004;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" Runat="Server">

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" Runat="Server">
    <h1>
        Indicadores de Realización de <asp:Literal ID="Literal6" runat="server">CURSOS</asp:Literal>
    </h1>
    Calcula la cantidad de Actividades (consideradas <asp:Literal ID="Literal8" runat="server">ASIGNATURAS</asp:Literal>) realizadas por alumnos ACTIVOS E INACTIVOS.
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table style="width: 100%;">
        <tr>
            <td class="titleColumn">
                <asp:Literal ID="Literal2" runat="server" Text="[INSTITUCION]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DdlInstitucion" runat="server" AutoPostBack="True"
                    DataSourceID="SdsInstituciones"
                    DataTextField="Descripcion"
                    DataValueField="IdInstitucion"
                    Width="300px"
                    CssClass="wideTextbox">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SdsInstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="Institucion_ObtenLista_IdCoordinador"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:SessionParameter 
                            Name="idCoordinador" 
                            SessionField="Usuario_IdCoordinador" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Literal ID="Literal4" runat="server" Text="[CICLO]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DdlCicloEscolar" runat="server" AutoPostBack="True"
                    DataSourceID="SdsCiclosEscolares"
                    DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar"
                    Width="300px"
                    CssClass="wideTextbox">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SdsCiclosEscolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="CicloEscolar_ObtenLista_Estatus"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:Parameter 
                            Name="estatus" 
                            DefaultValue="Activo" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Literal ID="Literal1" runat="server" Text="[PLANTEL]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DdlPlantel" runat="server" AutoPostBack="True"
                    DataSourceID="SdsPlanteles"
                    DataTextField="Descripcion"
                    DataValueField="IdPlantel"
                    OnDataBound="DdlPlantel_DataBound"
                    OnSelectedIndexChanged="DdlPlantel_SelectedIndexChanged"
                    Width="300px"
                    CssClass="wideTextbox">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SdsPlanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="Plantel_ObtenLista_IdInstitucion_IdCoordinador"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter 
                            Name="IdInstitucion"
                            ControlID="DdlInstitucion" 
                            PropertyName="SelectedValue" />
                        <asp:SessionParameter 
                            Name="idCoordinador" 
                            SessionField="Usuario_IdCoordinador" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Literal ID="Literal3" runat="server" Text="[GRADO]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DdlGrado" runat="server" AutoPostBack="True"
                    DataSourceID="SdsGradosPorPlantel"
                    DataTextField="Descripcion"
                    DataValueField="IdGrado"
                    OnDataBound="DdlGrado_DataBound"
                    Width="300px"
                    CssClass="wideTextbox">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SdsGradosTodos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="Grado_ObtenLista_IdInstitucion_IdCoordinador_Estatus"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter 
                            Name="IdInstitucion"
                            ControlID="DdlInstitucion" 
                            PropertyName="SelectedValue" />
                        <asp:SessionParameter 
                            Name="idCoordinador" 
                            SessionField="Usuario_IdCoordinador" />
                        <asp:Parameter 
                            Name="estatus" 
                            DefaultValue="Activo" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SdsGradosPorPlantel" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="Grado_ObtenLista_IdPlantel_Estatus"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter 
                            Name="IdPlantel"
                            ControlID="DdlPlantel" 
                            PropertyName="SelectedValue" />
                        <asp:Parameter 
                            Name="estatus" 
                            DefaultValue="Activo" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                Tipo de Actividad
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DDLtipo" runat="server" AutoPostBack="true"
                    Width="150px"
                    CssClass="wideTextbox">
                    <asp:ListItem Value="Todos" Selected="true">Todos</asp:ListItem>
                    <asp:ListItem Value="Aprendizaje">Aprendizaje</asp:ListItem>
                    <asp:ListItem Value="Evaluación">Evaluación</asp:ListItem>
                    <asp:ListItem Value="Material">Material</asp:ListItem>
                    <asp:ListItem Value="Reactivos">Reactivos</asp:ListItem>
                    <asp:ListItem Value="Documento">Documento</asp:ListItem>
                    <asp:ListItem Value="Examen">Examen</asp:ListItem>
                    <asp:ListItem Value="Tarea">Tarea</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Literal ID="Literal41" runat="server">SUBEQUIPO</asp:Literal>
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DdlSubequipo" runat="server" AutoPostBack="true"
                    DataSourceID="SdsSubequipos"
                    DataTextField="Descripcion"
                    DataValueField="Descripcion"
                    OnDataBound="DdlSubequipo_DataBound"
                    Width="250px"
                    CssClass="wideTextbox">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SdsSubequipos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="Subequipo_ObtenLista_IdInstitucion"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter 
                            Name="IdInstitucion"
                            ControlID="DdlInstitucion" 
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                Etiqueta de <asp:Literal ID="Literal5" runat="server" Text="[EtiqPlantel]" />
            </td>
            <td class="control_column">
                <asp:TextBox ID="TxtEtiquetaPlanteles" runat="server"></asp:TextBox>
                &nbsp;
                <asp:LinkButton ID="LbnRefresh" runat="server" 
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium"> <asp:Image ID="Image3" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/arrow_refresh_small.png"
                        CssClass="btnIcon"
                        height="24" />
                </asp:LinkButton>
                &nbsp;
                <span style="font-size: x-small;">
                    Se muestra como encabezado de la columna de <asp:Literal ID="Literal7" runat="server">Planteles</asp:Literal>
                </span>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msginfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="BtnExportarPlanteles" runat="server" 
                    Text="Exportar a Excel"
                    Visible="False" 
                    CssClass="defaultBtn btnThemeGreen btnThemeMedium" 
                    OnClick="BtnExportarPlanteles_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GvPlanteles" runat="server"
                    AllowSorting="true" 
                    AutoGenerateColumns="false"
                    ShowHeaderWhenEmpty="true"

                    DataSourceID="SdsIndicadoresP1" 
                    DataKeyNames="IdPlantel"
                    Width="883px"

                    OnSelectedIndexChanged="GvPlanteles_SelectedIndexChanged"
                    OnRowDataBound="GvPlanteles_RowDataBound"
                    OnDataBound="GvPlanteles_DataBound"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField 
                            ShowSelectButton="True" 
                            SelectText="Ver Detalle" />
                        <asp:BoundField 
                            DataField="Plantel" 
                            HeaderText="Plantel" 
                            SortExpression="Plantel" />
                        <asp:BoundField 
                            DataField="Actividades Terminadas" 
                            HeaderText="Actividades Terminadas" 
                            SortExpression="Actividades Terminadas" />
                        <asp:BoundField 
                            DataField="Promedio de Calificaciones" 
                            HeaderText="Promedio de Calificaciones" 
                            SortExpression="Promedio de Calificaciones" />
                        <asp:BoundField 
                            DataField="Participantes con Actividad" 
                            HeaderText="Participantes con Actividad (%)" 
                            SortExpression="Participantes con Actividad" />
                        <asp:BoundField 
                            DataField="Actividades por Participante" 
                            HeaderText="Actividades por Participante" 
                            SortExpression="Actividades por Participante" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="BtnExportarGrupos" runat="server" 
                    Text="Exportar a Excel"
                    Visible="False" 
                    CssClass="defaultBtn btnThemeGreen btnThemeMedium" 
                    OnClick="BtnExportarGrupos_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GvGrupos" runat="server"
                    AllowSorting="true" 
                    AutoGenerateColumns="false"

                    DataSourceID="SdsIndicadoresGruposP1" 
                    Width="883px"

                    OnDataBound="GvGrupos_DataBound"

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField 
                            DataField="Grupo" 
                            HeaderText="Grupo" 
                            SortExpression="Grupo" />
                        <asp:BoundField 
                            DataField="Actividades Terminadas" 
                            HeaderText="Actividades Terminadas" 
                            SortExpression="Actividades Terminadas" />
                        <asp:BoundField 
                            DataField="Promedio de Calificaciones" 
                            HeaderText="Promedio de Calificaciones" 
                            SortExpression="Promedio de Calificaciones" />
                        <asp:BoundField 
                            DataField="Participantes con Actividad" 
                            HeaderText="Participantes con Actividad (%)" 
                            SortExpression="Participantes con Actividad" />
                        <asp:BoundField 
                            DataField="Actividades por Participante" 
                            HeaderText="Actividades por Participante" 
                            SortExpression="Actividades por Participante" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Notas de los indicadores:
                <ul style="text-align: left; font-size: small;">
                    <li>
                        Actividades Terminadas
                        <p>
                            Totaliza las evaluaciones terminadas de 
                            <asp:Literal ID="Literal9" runat="server">LOS</asp:Literal>
                            <asp:Literal ID="Literal10" runat="server">ALUMNOS</asp:Literal> 
                            activ<asp:Literal ID="Literal11" runat="server">O</asp:Literal>s o inactiv<asp:Literal ID="Literal12" runat="server">O</asp:Literal>s, que sean parte de
                            <asp:Literal ID="Literal13" runat="server">EL</asp:Literal>
                            <asp:Literal ID="Literal14" runat="server">SUBEQUIPO</asp:Literal>
                             seleccionad<asp:Literal ID="Literal15" runat="server">O</asp:Literal>.
                        </p>
                    </li>
                    <li>
                        Promedio de Calificaciones
                        <p>
                            Promedia los resultados de las evaluaciones terminadas de 
                            <asp:Literal ID="Literal16" runat="server">LOS</asp:Literal>
                            <asp:Literal ID="Literal17" runat="server">ALUMNOS</asp:Literal> 
                             activ<asp:Literal ID="Literal18" runat="server">O</asp:Literal>s o inactiv<asp:Literal ID="Literal19" runat="server">O</asp:Literal>s, que sean parte de
                            <asp:Literal ID="Literal20" runat="server">EL</asp:Literal>
                            <asp:Literal ID="Literal21" runat="server">SUBEQUIPO</asp:Literal>
                             seleccionad<asp:Literal ID="Literal22" runat="server">O</asp:Literal>.
                        </p>
                    </li>
                    <li>
                        Participantes con Actividad
                        <p>
                            Totaliza todos 
                            <asp:Literal ID="Literal23" runat="server">LOS</asp:Literal>
                            <asp:Literal ID="Literal24" runat="server">ALUMNOS</asp:Literal> 
                             activ<asp:Literal ID="Literal25" runat="server">O</asp:Literal>s que sean parte de
                            <asp:Literal ID="Literal26" runat="server">EL</asp:Literal>
                            <asp:Literal ID="Literal27" runat="server">SUBEQUIPO</asp:Literal>
                             seleccionad<asp:Literal ID="Literal28" runat="server">O</asp:Literal>
                             y busca cuántos de ell<asp:Literal ID="Literal29" runat="server">O</asp:Literal>s tienen por lo menos una evaluación terminada.
                        </p>
                    </li>
                    <li>
                        Actividades por Participante
                        <p>
                            Totaliza la cantidad de evaluaciones terminadas de todos 
                            <asp:Literal ID="Literal30" runat="server">LOS</asp:Literal>
                            <asp:Literal ID="Literal31" runat="server">ALUMNOS</asp:Literal> 
                             activ<asp:Literal ID="Literal32" runat="server">O</asp:Literal>s o inactiv<asp:Literal ID="Literal33" runat="server">O</asp:Literal>s 
                            (mismo dato del primer punto) que sean parte de 
                            <asp:Literal ID="Literal34" runat="server">EL</asp:Literal>
                            <asp:Literal ID="Literal35" runat="server">SUBEQUIPO</asp:Literal>
                             seleccionad<asp:Literal ID="Literal36" runat="server">O</asp:Literal>, y lo divide entre el total de 
                            <asp:Literal ID="Literal37" runat="server">ALUMNOS</asp:Literal> 
                             activ<asp:Literal ID="Literal38" runat="server">O</asp:Literal>s de 
                            un determinado 
                            <asp:Literal ID="Literal39" runat="server">GRADO</asp:Literal> 
                             o 
                            <asp:Literal ID="Literal40" runat="server">PLANTEL</asp:Literal> .
                        </p>
                    </li>
                </ul>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>
                <asp:SqlDataSource ID="SdsIndicadoresP1" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommandType="StoredProcedure"
                    SelectCommand="IndicadoresP1">
                    <SelectParameters>
                        <asp:SessionParameter 
                            Name="idCoordinador" 
                            SessionField="Usuario_IdCoordinador" />
                        <asp:ControlParameter 
                            Name="IdCicloEscolar"
                            ControlID="DdlCicloEscolar" 
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter 
                            Name="IdPlantel"
                            ControlID="DdlPlantel" 
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter 
                            Name="IdGrado"
                            ControlID="DdlGrado" 
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter 
                            Name="IdSubequipo"
                            ControlID="DdlSubequipo" 
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter 
                            Name="TipoEvaluacion"
                            ControlID="DDLtipo" 
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SdsIndicadoresGruposP1" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommandType="StoredProcedure"
                    SelectCommand="IndicadoresGruposP1">
                    <SelectParameters>
                        <asp:SessionParameter 
                            Name="idCoordinador" 
                            SessionField="Usuario_IdCoordinador" />
                        <asp:Parameter
                            Name="IdPlantel"
                            DefaultValue="0" />
                        <asp:ControlParameter 
                            Name="IdCicloEscolar"
                            ControlID="DdlCicloEscolar" 
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter 
                            Name="IdGrado"
                            ControlID="DdlGrado" 
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter 
                            Name="IdSubequipo"
                            ControlID="DdlSubequipo" 
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter 
                            Name="TipoEvaluacion"
                            ControlID="DDLtipo" 
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>

