﻿Imports Siget

'Imports System.Data
'Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data

Partial Class coordinador_ReporteGeneral
		Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        'UserInterface.Include.JQuery(CType(Master.FindControl("pnlHeader"), Literal))
        UserInterface.Include.Spinner(CType(Master.FindControl("pnlHeader"), Literal))

        Session("Login") = Trim(User.Identity.Name) 'La necesito para que cargue las Instituciones y los Niveles (en los SDS)

        Label1.Text = Config.Etiqueta.ASIGNATURA
        Label2.Text = Config.Etiqueta.PLANTELES
        Label3.Text = Config.Etiqueta.COORDINADOR
        Label4.Text = Config.Etiqueta.GRUPOS
        Label5.Text = Config.Etiqueta.ALUMNOS
        Label6.Text = Config.Etiqueta.INSTITUCION
        Label7.Text = Config.Etiqueta.CICLO
        Label8.Text = Config.Etiqueta.NIVEL
        Label9.Text = Config.Etiqueta.GRADO
        Label10.Text = Config.Etiqueta.ASIGNATURA
        Label11.Text = Config.Etiqueta.ASIGNATURA

        'Inicializo las siguientes variables para que no cargue datos el grid si regreso a esta función
        If DDLasignatura.SelectedIndex <= 0 Then
            Session("IdCoordinador") = 0
            Session("IdAsignatura") = 0
        End If
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound, GVsalida.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija una " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLasignatura_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.SelectedIndexChanged
        msgError.hide()

        'Para generar el contenido del GridView dinamicamente, es necesario que su propiedad AutoGenerateColumns = True 
        GVsalida.Caption = "<b>" + DDLinstitucion.SelectedItem.ToString + " - " + DDLcicloescolar.SelectedItem.ToString + "<BR>" + _
            DDLnivel.SelectedItem.ToString + " - " + DDLgrado.SelectedItem.ToString + " - " + DDLasignatura.SelectedItem.ToString + "<BR>Reporte de promedios por " &
            Config.Etiqueta.PLANTEL & ". Emitido el " + Date.Today.ToShortDateString + "</b>"

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        Try
            miComando = objConexion.CreateCommand
            objConexion.Open()

            'OBTENGO EL ID DEL COORDINADOR
            miComando.CommandText = "select IdCoordinador from Usuario U, Coordinador C " + _
            "where C.IdUsuario = U.IdUsuario and U.Login = '" + Session("Login") + "'"
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read() 'Leo para poder accesarlos
            Session("IdCoordinador") = CStr(misRegistros.Item("IdCoordinador"))
            misRegistros.Close()

            'CUENTO LAS CUANTAS CALIFICACIONES HAY
            miComando.CommandText = " select Count(Distinct C.IdCalificacion) as Total from Calificacion C,Asignatura A" + _
                " where A.IdAsignatura = " + DDLasignatura.SelectedValue + " and C.IdAsignatura = A.IdAsignatura and" + _
                " C.IdCicloEscolar = " + DDLcicloescolar.SelectedValue
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            Session("TotalC") = misRegistros.Item("Total")
            misRegistros.Close()
            objConexion.Close()
            Refresca()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Refresca()
        'AHORA EJECUTO EL PROCEDIMIENTO ALMACENADO
        Dim objCommand As New SqlClient.SqlCommand("SP_promediomateria"), Conexion As String = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.Add("@IdInstitucion", SqlDbType.Int, 4)
        objCommand.Parameters("@IdInstitucion").Value = DDLinstitucion.SelectedValue
        objCommand.Parameters.Add("@IdCicloEscolar", SqlDbType.Int, 4)
        objCommand.Parameters("@IdCicloEscolar").Value = DDLcicloescolar.SelectedValue
        objCommand.Parameters.Add("@IdCoordinador", SqlDbType.Int, 4)
        objCommand.Parameters("@IdCoordinador").Value = CInt(Session("IdCoordinador"))
        objCommand.Parameters.Add("@IdAsignatura", SqlDbType.Int, 4)
        objCommand.Parameters("@IdAsignatura").Value = DDLasignatura.SelectedValue
        objCommand.Parameters.Add("@TotalC", SqlDbType.Int, 2)
        objCommand.Parameters("@TotalC").Value = CInt(Session("TotalC"))
        objCommand.CommandTimeout = 3000
        objCommand.Connection = New SqlClient.SqlConnection(Conexion)
        objCommand.Connection.Open()
        ' Crear el DataReader
        Dim Datos As SqlClient.SqlDataReader
        ' Con el método ExecuteReader() del comando se traen los datos
        Datos = objCommand.ExecuteReader()
        GVsalida.DataSource = Datos
        GVsalida.DataBind()
        If GVsalida.Rows.Count > 0 Then
            BtnExportar.Visible = True
        Else
            BtnExportar.Visible = False
        End If
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        'Este procedimiento convierte el GridView a Excel eliminando la primera y segunda columna
        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            'Algoritmo que oculta toda la columna donde aparece el "Seleccionar"
            Dim Cont As Integer
            GVactual.HeaderRow.Cells.Item(0).Visible = False
            For Cont = 0 To GVactual.Rows.Count - 1
                GVactual.Rows(Cont).Cells.Item(0).Visible = False
            Next
            'Algoritmo que oculta toda la columna IdPlantel
            'GVactual.HeaderRow.Cells.Item(1).Visible = False
            'For Cont = 0 To GVactual.Rows.Count - 1
            'GVactual.Rows(Cont).Cells.Item(1).Visible = False
            'Next
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub


    Protected Sub GVsalida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVsalida.SelectedIndexChanged
        'Variables que necesito pasar
        'Session("Institucion") no se necesita porque el Plantel solo puede pertenecer a una Institucion
        'Session("TotalC") y Session("IdCoordinador") ya están cargadas

        Session("IdPlantel") = GVsalida.SelectedDataKey.Values(0).ToString()
        Session("Plantel") = GVsalida.SelectedRow.Cells(2).Text
        Session("Institucion") = DDLinstitucion.SelectedItem.Text
        Session("NomNivel") = DDLnivel.SelectedItem.Text
        Session("NomGrado") = DDLgrado.SelectedItem.Text
        Session("CicloEscolar") = DDLcicloescolar.SelectedItem.Text
        Session("IdCicloEscolar") = DDLcicloescolar.SelectedValue
        Session("IdNivel") = DDLnivel.SelectedValue
        Session("IdGrado") = DDLgrado.SelectedValue
        Session("IdAsignatura") = DDLasignatura.SelectedValue
        Session("Asignatura") = DDLasignatura.SelectedItem.Text

        Response.Redirect("ReporteGrupo.aspx")
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVsalida, "RepPlantel")
    End Sub

    Protected Sub GVsalida_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVsalida.RowDataBound
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        miComando = objConexion.CreateCommand
        '1)OBTENGO LOS RANGOS GENERALES PARA EL SEMAFORO ROJO, AMARILL, VERDE Y AZUL
        'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
        miComando.CommandText = "select I.MinRojo, I.MinAmarillo, I.MinVerde, I.MinAzul from Indicador I, CicloEscolar C where " + _
                                "I.IdIndicador = C.IdIndicador and C.IdCicloEscolar = " + DDLcicloescolar.SelectedValue
        Dim MinRojo, MinAmarillo, MinVerde, MinAzul As Decimal
        Try
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            MinRojo = misRegistros.Item("MinRojo")
            MinAmarillo = misRegistros.Item("MinAmarillo")
            MinVerde = misRegistros.Item("MinVerde")
            MinAzul = misRegistros.Item("MinAzul")
            misRegistros.Close()

            'La siguiente variable la necesito mas adelante (si la calificacion es solo 1, no presenta promedios)
            Dim Quita As Byte
            If Session("TotalC") > 1 Then
                Quita = 4
            Else
                Quita = 2
            End If

            '2)HAGO UN CICLO QUE VAYA DE 1 AL TOTAL DE COLUMNAS (EL INDICE DE LAS CELDAS(COLUMNAS) INICIA EN 0 (CELLS(0)= ) Y PINTO TODAS (despues, si hay indicador específico las vuelve a pintar)
            If (e.Row.Cells.Count > 1) And (e.Row.RowType = DataControlRowType.DataRow) Then  'Para que se realice la comparacion cuando haya mas de una columna y que esta sea de datos (no de Encabezado) ANTES TENÍA: And (e.Row.RowIndex > -1) Then
                For I As Byte = 3 To e.Row.Cells.Count - 1 'Inicio a partir de la tercer columna a comparar
                    If IsNumeric(Trim(e.Row.Cells(I).Text)) Then 'para que no compare celdas vacias
                        If (CDbl(e.Row.Cells(I).Text) >= MinRojo) And (CDbl(e.Row.Cells(I).Text) < MinAmarillo) Then
                            e.Row.Cells(I).BackColor = Drawing.Color.LightSalmon
                        ElseIf (CDbl(e.Row.Cells(I).Text) >= MinAmarillo) And (CDbl(e.Row.Cells(I).Text) < MinVerde) Then
                            e.Row.Cells(I).BackColor = Drawing.Color.Yellow
                        ElseIf (CDbl(e.Row.Cells(I).Text) >= MinVerde) And (CDbl(e.Row.Cells(I).Text) < MinAzul) Then
                            e.Row.Cells(I).BackColor = Drawing.Color.LightGreen
                        ElseIf (CDbl(e.Row.Cells(I).Text) >= MinAzul) And (CDbl(e.Row.Cells(I).Text) <= 100.0) Then 'La escala solo puede ser hasta 100
                            e.Row.Cells(I).BackColor = Drawing.Color.LightBlue
                        End If
                    End If
                Next

                '3)AHORA VERIFICO CALIFICACION POR CALIFICACION SI TIENEN UN INDICADOR ESPECÍFICO PARA PINTARLA EN BASE A SU COLOR
                Dim TC, Cuenta As Byte
                TC = e.Row.Cells.Count
                Cuenta = 3 'La columna 3 es la primera de las calificaciones acumuladas, luego aparecen cada 2 columnas
                Do While (Cuenta < (TC - Quita))
                    misRegistros.Close()
                    'Obtengo los datos de la Calificacion para ver si tiene asignado un Indicador específico
                    miComando.CommandText = "select * from Calificacion where IdCalificacion = " + e.Row.Cells(Cuenta).Text
                    misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                    misRegistros.Read()

                    'Verifico si tiene asignado algun Indicador Específico
                    Dim Ind = 0
                    If Not IsDBNull(misRegistros.Item("IdIndicador")) Then
                        Ind = misRegistros.Item("IdIndicador")
                    End If

                    'Si tiene un indicador, obtengo los Rangos para pintar la celda
                    If Ind > 0 Then
                        misRegistros.Close()
                        miComando.CommandText = "select * from Rango where IdIndicador=" + Ind.ToString
                        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                        Do While misRegistros.Read()
                            'Voy leyendo Rango por Rango y pinto comparando pintando donde está la celda con el valor de la calificación, que es la que está a la derecha del IdCalificacion
                            If e.Row.Cells(Cuenta + 1).Text <> "&nbsp;" Then 'NO AGREGAR ESTA CONDICION EN UN OR JUNTO AL IF INTERNO PORQUE NO FUNCIONA
                                If (CDbl(e.Row.Cells(Cuenta + 1).Text) >= misRegistros.Item("Inferior")) And (CDbl(e.Row.Cells(Cuenta + 1).Text) <= misRegistros.Item("Superior")) Then
                                    e.Row.Cells(Cuenta + 1).BackColor = Drawing.ColorTranslator.FromHtml("#" + misRegistros.Item("Color"))
                                    Exit Do
                                End If
                            End If
                        Loop
                    End If
                    Cuenta += 3
                Loop
            End If ' del if (e.Row.Cells.Count > 1) And (e.Row.RowType = DataControlRowType.DataRow) Then
            misRegistros.Close()
            objConexion.Close()

            '4)Ahora oculto todos los IdCalificacion
            Dim TotCol, Cont As Byte
            TotCol = e.Row.Cells.Count
            Cont = 3 'La columna 3 es el Primero de los IdCalificacion, luego aparecen cada 2 columnas
            Do While (Cont < (TotCol - Quita))
                e.Row.Cells(Cont).Visible = False
                Cont += 3
            Loop


            '5)Lo siguiente es para ocultar o mostrar las columnas de calificaciones acumuladas. Quize usar un checkbox pero no hace el postback
            If Session("TotalC") > 1 Then
                Quita = 2
            Else
                Quita = 0
            End If
            If RBacumuladas.SelectedValue = "O" Then
                TotCol = e.Row.Cells.Count
                Cont = 4 'La columna 4 es la primera de las calificaciones acumuladas, luego aparecen cada 3 columnas
                Do While (Cont < (TotCol - Quita))
                    e.Row.Cells(Cont).Visible = False
                    Cont += 3
                Loop
                If Session("TotalC") > 1 Then
                    e.Row.Cells(TotCol - Quita).Visible = False
                End If
            End If

            '6)Con lo siguiente oculto la columna IdPlantel
            'OJO, EL GRIDVIEW DEBE TENER DESHABILITADA LA OPCION DE HABILITAR PAGINACION
            e.Row.Cells(1).Visible = False
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(2).Text = Config.Etiqueta.PLANTEL
        End If

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVsalida, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    Protected Sub RBacumuladas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBacumuladas.SelectedIndexChanged
        Refresca()
    End Sub

    Protected Sub GVindicadores_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVindicadores.RowDataBound
        If (e.Row.Cells.Count > 1) And (e.Row.RowType = DataControlRowType.DataRow) Then
            e.Row.Cells(0).BackColor = Drawing.ColorTranslator.FromHtml("#" + e.Row.Cells(1).Text)
        End If
        e.Row.Cells(1).Visible = False
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVsalida_PreRender(sender As Object, e As EventArgs) Handles GVsalida.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVsalida.Rows.Count > 0 Then
            GVsalida.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVsalida.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVsalida, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

End Class
