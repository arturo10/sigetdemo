<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="AvanceActividadesGrupos.aspx.vb" 
    Inherits="coordinador_AvanceActividadesGrupos" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style11 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style13 {
        }

        .style23 {
            height: 32px;
        }

        .style32 {
            height: 22px;
            width: 479px;
            font-weight: normal;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Avance - Detalle Realizadas
    </h1>
    Presenta un resumen por 
	<asp:Label ID="Label1" runat="server" Text="[ASIGNATURA]" />
    del avance de todas las 
	actividades realizadas, permitiendo elegir una y ver el detalle por 
	<asp:Label ID="Label2" runat="server" Text="[PLANTELES]" />
    y 
	<asp:Label ID="Label3" runat="server" Text="[GRUPOS]" />
    .
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style23">
                <asp:HyperLink ID="HyperLink6" runat="server"
                    NavigateUrl="javascript:history.back()"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"
                    onclick="setSpinner('')">Regresar</asp:HyperLink>
            </td>
            <td class="style23"></td>
            <td class="style23"></td>
            <td class="style23"></td>
        </tr>
        <tr>
            <td colspan="4">
                <div id="spin" style="display:none;" class="spinnerLoading">
                    <div style="padding-top: 80px; text-align: center;">
                        Trabajando...
                    </div>
			    </div>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GVreporte" runat="server"
                    Width="860px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField HeaderText="%&nbsp;Terminadas" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort NO, dynamic - rowdatabound
	                0  GRUPO
	                1  iniciadas
	                2  ...
	                3  
	                4  
	                5  
	                6  
	                7  
	                8  
	                9  
	                10 
	                --%>
            </td>
        </tr>
        <tr>
            <td class="style13" colspan="4">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="4">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td class="style13" colspan="4">
                <asp:GridView ID="GVindicadores" runat="server" 
                    AutoGenerateColumns="False"
                    Caption="<h3>ESCALA DE VALORES</h3>" 

                    DataSourceID="SDSindicadores" 
                    Width="334px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="MinAzul" HeaderText="M�nimo para EXCELENTE"
                            SortExpression="MinAzul">
                            <ItemStyle HorizontalAlign="Right" BackColor="LightBlue" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinVerde" HeaderText="M�nimo para BIEN"
                            SortExpression="MinVerde">
                            <ItemStyle HorizontalAlign="Right" BackColor="LightGreen" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinAmarillo" HeaderText="M�nimo para REGULAR"
                            SortExpression="MinAmarillo">
                            <ItemStyle HorizontalAlign="Right" BackColor="Yellow" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinRojo" HeaderText="M�nimo para MAL"
                            SortExpression="MinRojo">
                            <ItemStyle HorizontalAlign="Right" BackColor="LightSalmon" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style13">
                <asp:HyperLink ID="HyperLink5" runat="server"
                    NavigateUrl="javascript:history.back()"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"
                    onclick="setSpinner('')">Regresar</asp:HyperLink>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSindicadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT MinRojo, MinAmarillo, MinVerde, MinAzul FROM Indicador, CicloEscolar
where Indicador.IdIndicador = CicloEscolar.IdIndicador and IdCicloEscolar = @IdCicloEscolar">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

