﻿Imports Siget

Imports System.Data
Imports System.IO
'Imports System.Web.Security
'Imports System.Data.OleDb
Imports System.Data.SqlClient

Partial Class coordinador_DetalleEvaluaciones
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        'UserInterface.Include.JQuery(CType(Master.FindControl("pnlHeader"), Literal))
        UserInterface.Include.Spinner(CType(Master.FindControl("pnlHeader"), Literal))

        Session("Login") = User.Identity.Name
        TitleLiteral.Text = "Resumen por Actividad"

        Label1.Text = Config.Etiqueta.ASIGNATURAS
        Label2.Text = Config.Etiqueta.GRADO
        Label3.Text = Config.Etiqueta.INSTITUCION
        Label4.Text = Config.Etiqueta.CICLO
        Label5.Text = Config.Etiqueta.NIVEL
        Label6.Text = Config.Etiqueta.GRADO
        Label7.Text = Config.Etiqueta.ARTDET_ASIGNATURAS
        Label8.Text = Config.Etiqueta.ARTDET_GRADO
        Label9.Text = Config.Etiqueta.LETRA_GRUPO

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader

        miComando = objConexion.CreateCommand
        'El Login de Usuario es único en el sistema
        miComando.CommandText = "select C.IdCoordinador from Coordinador C, Usuario U where C.IdUsuario = U.IdUsuario " + _
                                "and U.Login = '" + Trim(User.Identity.Name) + "'"
        Try
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            Session("IdCoordinador") = misRegistros.Item("IdCoordinador")
            misRegistros.Close()
            objConexion.Close()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLgrado_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.SelectedIndexChanged, DDLnivel.SelectedIndexChanged, DDLcicloescolar.SelectedIndexChanged
        Try
            'Para generar el contenido del GridView dinamicamente, es necesario que su propiedad AutoGenerateColumns = True 
            Dim objCommand As New SqlClient.SqlCommand("sp_detalleevaluaciones"), Conexion As String = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Parameters.Add("@IdCicloEscolar", SqlDbType.Int, 4)
            objCommand.Parameters("@IdCicloEscolar").Value = DDLcicloescolar.SelectedValue
            objCommand.Parameters.Add("@IdInstitucion", SqlDbType.Int, 4)
            objCommand.Parameters("@IdInstitucion").Value = DDLinstitucion.SelectedValue
            objCommand.Parameters.Add("@IdCoordinador", SqlDbType.Int, 4)
            objCommand.Parameters("@IdCoordinador").Value = CInt(Session("IdCoordinador"))
            objCommand.Parameters.Add("@IdGrado", SqlDbType.Int, 4)
            objCommand.Parameters("@IdGrado").Value = DDLgrado.SelectedValue
            objCommand.CommandTimeout = 3000
            objCommand.Connection = New SqlClient.SqlConnection(Conexion)
            objCommand.Connection.Open()
            ' Crear el DataReader
            Dim Datos As SqlClient.SqlDataReader
            ' Con el método ExecuteReader() del comando se traen los datos
            Datos = objCommand.ExecuteReader()
            GVdatosglobal.DataSource = Datos
            GVdatosglobal.DataBind() 'Es necesario para que se vea el resultado
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        'DDLinstitucion.Items.Insert(0, New ListItem("---Elija una Institución", 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub GVdatosglobal_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVdatosglobal.DataBound
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader

        miComando = objConexion.CreateCommand
        'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
        miComando.CommandText = "select P.Descripcion from Plantel P, CoordinadorPlantel C where C.IdCoordinador = " + _
                                Session("IdCoordinador").ToString + " and P.IdPlantel = C.IdPlantel order by P.Descripcion"
        Try
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            GVdatosglobal.Caption = "<B>" + DDLinstitucion.SelectedItem.ToString +
                " - " + DDLcicloescolar.SelectedItem.ToString +
                "<BR>" + "" & Config.Etiqueta.PLANTELES & ": " + misRegistros.Item("Descripcion").ToString
            While misRegistros.Read()
                GVdatosglobal.Caption = GVdatosglobal.Caption + ", " + misRegistros.Item("Descripcion").ToString
            End While
            GVdatosglobal.Caption = GVdatosglobal.Caption + "<BR>" + DDLgrado.SelectedItem.ToString + " - " + _
                                    DDLnivel.SelectedItem.ToString + "<BR>" + "Reporte al " + Date.Today.ToShortDateString + "</B>"
            misRegistros.Close()
            objConexion.Close()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
        If GVdatosglobal.Rows.Count > 0 Then
            BtnExportar.Visible = True
        Else
            BtnExportar.Visible = False
        End If

        ' AÑADO PORCENTAJES DE TERMINADAS
        ' para cada fila calcula el porcentaje, y añadelo a la fila
        Try
            For pC As Integer = 0 To GVdatosglobal.Rows.Count() - 1
                Dim divisor As Decimal =
                        Decimal.Add(Decimal.Parse(GVdatosglobal.Rows(pC).Cells(7).Text),
                        Decimal.Add(Decimal.Parse(GVdatosglobal.Rows(pC).Cells(8).Text),
                        Decimal.Parse(GVdatosglobal.Rows(pC).Cells(9).Text)))

                If divisor = 0 Then ' protejo contra posibles resultados vacios
                    Continue For
                End If

                GVdatosglobal.Rows(pC).Cells(10).Text =
                    Math.Round(Decimal.Multiply(Decimal.Divide(Decimal.Parse(
                        GVdatosglobal.Rows(pC).Cells(7).Text),
                        divisor
                    ), 100), 2, MidpointRounding.AwayFromZero) ' 2 decimales de precisión
            Next
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVdatosglobal, "DetalleEvaluaciones")
    End Sub

    Protected Sub GVdatosglobal_PreRender(sender As Object, e As EventArgs) Handles GVdatosglobal.PreRender
        ' Etiquetas de GridView
        GVdatosglobal.Columns(5).HeaderText = Config.Etiqueta.ASIGNATURA
    End Sub

    Protected Sub GVdatosglobal_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVdatosglobal.RowDataBound
        'Con lo siguiente oculto la primer columna (IdEvaluacion)
        e.Row.Cells(0).Visible = False
    End Sub
End Class
