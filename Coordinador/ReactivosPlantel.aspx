﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="ReactivosPlantel.aspx.vb"
    Inherits="coordinador_ReactivosPlantel"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style11 {
            height: 29px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style21 {
            width: 273px;
        }

        .style22 {
            text-align: left;
        }

        .style13 {
            width: 143px;
        }

        .style32 {
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style33 {
            height: 29px;
        }

        .style36 {
            font-weight: normal;
            font-size: small;
        }

        .style37 {
            height: 34px;
            width: 243px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Resultados - Reactivos
    </h1>
    Presenta el detalle de respuestas a los reactivos 
                    contestados en una actividad, mostrando el detalle por los 
												<asp:Label ID="Label1" runat="server" Text="[GRUPOS]" />
    de un 
												<asp:Label ID="Label2" runat="server" Text="[PLANTEL]" />
    y por 
												<asp:Label ID="Label3" runat="server" Text="[ALUMNO]" />
    .
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr class="estandar">
            <td colspan="4" class="style37">
                <asp:HyperLink ID="HyperLink6" runat="server"
                    NavigateUrl="javascript:history.back()"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
        </tr>
        <tr class="estandar">
            <td colspan="4">
                <asp:GridView ID="GVreporte" runat="server"
                    AllowSorting="True" 
                    AutoGenerateColumns="False"

                    DataSourceID="SDStotalreactivos" 
                    Width="883px"

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="No. Subtema" HeaderText="No. Subtema"
                            SortExpression="No. Subtema" />
                        <asp:BoundField DataField="Subtema" HeaderText="Subtema"
                            SortExpression="Subtema">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="No. Reactivo" HeaderText="No. Reactivo"
                            SortExpression="No. Reactivo" />
                        <asp:BoundField DataField="Reactivo" HeaderText="Reactivo"
                            SortExpression="Reactivo">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Respuestas Acertadas"
                            HeaderText="Respuestas Acertadas" ReadOnly="True"
                            SortExpression="Respuestas Acertadas">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Respuestas Erróneas"
                            HeaderText="Respuestas Erróneas" ReadOnly="True"
                            SortExpression="Respuestas Erróneas">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="%&nbsp;Acertadas" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr class="estandar">
            <td colspan="4">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style32" colspan="4">
                <asp:Label ID="Label4" runat="server" Text="[GRUPOS]" />
                que pertenecen al 
								<asp:Label ID="Label5" runat="server" Text="[PLANTEL]" />
                en el 
								<asp:Label ID="Label6" runat="server" Text="[GRADO]" />
                al que está asignada la Actividad:</td>
        </tr>
        <tr class="estandar">
            <td colspan="4">
                <asp:GridView ID="GVgrupos" runat="server" 
                    AutoGenerateColumns="False"
                    
                    DataKeyNames="IdGrupo" 
                    DataSourceID="SDSgrupos"
                    Width="311px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo" InsertVisible="False"
                            ReadOnly="True" SortExpression="IdGrupo" Visible="False" />
                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                            SortExpression="IdPlantel" Visible="False" />
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion"
                            SortExpression="Descripcion" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="style22">
                &nbsp;</td>
            <td class="style21">
                &nbsp;</td>
            <td class="style13">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style22">
                <asp:HyperLink ID="HyperLink5" runat="server"
                    NavigateUrl="javascript:history.back()"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
            <td class="style21"></td>
            <td class="style13">&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDStotalreactivos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select S.Numero as 'No. Subtema', S.Descripcion as Subtema, P.Consecutivo as 'No. Reactivo', P.Redaccion as Reactivo,
(select COUNT(R.Acertada)
from Respuesta R, Grupo G, Alumno A where R.Acertada = 'S'
and R.IdPlanteamiento = P.IdPlanteamiento and R.IdDetalleEvaluacion = DE.IdDetalleEvaluacion and A.IdAlumno = R.IdAlumno and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido') and G.IdGrupo = R.IdGrupo and G.IdPlantel = @IdPlantel) as 'Respuestas Acertadas',
(select COUNT(R.Acertada)
from Respuesta R, Grupo G, Alumno A where R.Acertada = 'N'
and R.IdPlanteamiento = P.IdPlanteamiento and R.IdDetalleEvaluacion = DE.IdDetalleEvaluacion and A.IdAlumno = R.IdAlumno and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido') and G.IdGrupo = R.IdGrupo and G.IdPlantel = @IdPlantel) as 'Respuestas Erróneas'
from DetalleEvaluacion DE, Tema T, Subtema S, Planteamiento P
where T.IdAsignatura = @IdAsignatura and S.IdTema = T.IdTema and S.IdSubtema = DE.IdSubtema and
DE.IdEvaluacion = @IdEvaluacion and P.IdSubtema = S.IdSubtema and P.Ocultar = 0
order by S.Numero, P.Consecutivo">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdPlantel" SessionField="IdPlantel" />
                        <asp:SessionParameter Name="IdAsignatura" SessionField="IdAsignatura" />
                        <asp:SessionParameter Name="IdEvaluacion" SessionField="IdEvaluacion" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrupo], [IdPlantel], [Descripcion] FROM [Grupo] WHERE (([IdPlantel] = @IdPlantel) AND ([IdGrado] = @IdGrado)) AND IdCicloEscolar = @IdCicloEscolar ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdPlantel" SessionField="IdPlantel" Type="Int32" />
                        <asp:SessionParameter Name="IdGrado" SessionField="IdGrado" Type="Int32" />
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

