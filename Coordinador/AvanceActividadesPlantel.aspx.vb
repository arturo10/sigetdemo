﻿Imports Siget

Imports System.Data
Imports System.IO
Imports System.Data.SqlClient

Partial Class coordinador_AvanceActividadesPlantel
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        'UserInterface.Include.JQuery(CType(Master.FindControl("pnlHeader"), Literal))
        UserInterface.Include.Spinner(CType(Master.FindControl("pnlHeader"), Literal))

        TitleLiteral.Text = "Avance en Realización"

        Label1.Text = Config.Etiqueta.ASIGNATURA
        Label2.Text = Config.Etiqueta.PLANTELES
        Label3.Text = Config.Etiqueta.GRUPOS

        Try
            GVreporte2.Caption = Session("Titulo")
            'Para generar el contenido del GridView dinamicamente, es necesario que su propiedad AutoGenerateColumns = True 
            Dim objCommand As New SqlClient.SqlCommand("sp_detalleactivplanteles"), Conexion As String = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Parameters.Add("@IdCicloEscolar", SqlDbType.Int, 4)
            objCommand.Parameters("@IdCicloEscolar").Value = CInt(Session("IdCicloEscolar"))
            objCommand.Parameters.Add("@IdInstitucion", SqlDbType.Int, 4)
            objCommand.Parameters("@IdInstitucion").Value = CInt(Session("IdInstitucion"))
            objCommand.Parameters.Add("@IdCoordinador", SqlDbType.Int, 4)
            objCommand.Parameters("@IdCoordinador").Value = CInt(Session("IdCoordinador"))
            objCommand.Parameters.Add("@IdEvaluacion ", SqlDbType.Int, 4)
            objCommand.Parameters("@IdEvaluacion ").Value = CInt(Session("IdEvaluacion"))
            objCommand.CommandTimeout = 3000
            objCommand.Connection = New SqlClient.SqlConnection(Conexion)
            objCommand.Connection.Open()
            ' Crear el DataReader
            Dim Datos As SqlClient.SqlDataReader
            ' Con el método ExecuteReader() del comando se traen los datos
            Datos = objCommand.ExecuteReader()
            GVreporte2.DataSource = Datos
            GVreporte2.DataBind()   'Es necesario para que se vea el resultado
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        'Este procedimiento convierte el GridView a Excel eliminando la primera y segunda columna
        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            'Algoritmo que oculta toda la columna donde aparece el "Seleccionar"
            Dim Cont As Integer
            GVactual.HeaderRow.Cells.Item(0).Visible = False
            For Cont = 0 To GVactual.Rows.Count - 1
                GVactual.Rows(Cont).Cells.Item(0).Visible = False
            Next
            'Algoritmo que oculta toda la columna IdPlantel
            GVactual.HeaderRow.Cells.Item(1).Visible = False
            For Cont = 0 To GVactual.Rows.Count - 1
                GVactual.Rows(Cont).Cells.Item(1).Visible = False
            Next
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVreporte2, "AvanceActividadesPlantel")
    End Sub

    Protected Sub GVreporte_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVreporte2.SelectedIndexChanged
        'Session("IdCicloEscolar") Ya está cargada
        Session("IdPlantel") = GVreporte2.SelectedDataKey.Values(0).ToString 'GVreporte2.SelectedRow.Cells(1).Text
        'Session("IdEvaluacion") Ya está cargada
        Session("Titulo2") = GVreporte2.Caption.ToString + "<BR>Actividades Realizadas en " &
            Config.Etiqueta.ARTDET_PLANTEL & " " & Config.Etiqueta.PLANTEL &
            ": " + GVreporte2.SelectedRow.Cells(2).Text
        Response.Redirect("AvanceActividadesGrupos.aspx")
    End Sub

    Protected Sub GVreporte2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVreporte2.RowDataBound
        If (e.Row.Cells.Count > 1) And (e.Row.RowIndex > -1) Then  'Para que pinte cuando haya mas de una columna (o fila quize decir?)
            e.Row.Cells(6).BackColor = Drawing.Color.LightBlue
            e.Row.Cells(7).BackColor = Drawing.Color.LightGreen
            e.Row.Cells(8).BackColor = Drawing.Color.Yellow
            e.Row.Cells(9).BackColor = Drawing.Color.LightSalmon
        End If
        e.Row.Cells(1).Visible = False

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(2).Text = Config.Etiqueta.PLANTEL
        End If

        ' añado el evento del spinner, y función en javascript para que seleccione la fila entera
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", "setSpinner(''); " + Page.ClientScript.GetPostBackClientHyperlink(GVreporte2, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    Protected Sub GVreporte2_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVreporte2.DataBound
        If GVreporte2.Rows.Count > 0 Then
            BtnExportar.Visible = True
        Else
            BtnExportar.Visible = False
        End If

        ' AÑADO PORCENTAJES DE TERMINADAS
        ' para cada fila calcula el porcentaje, y añadelo a la fila
        Try
            For pC As Integer = 0 To GVreporte2.Rows.Count() - 1
                Dim divisor As Decimal =
                        Decimal.Add(Decimal.Parse(GVreporte2.Rows(pC).Cells(3).Text),
                        Decimal.Add(Decimal.Parse(GVreporte2.Rows(pC).Cells(4).Text),
                        Decimal.Parse(GVreporte2.Rows(pC).Cells(5).Text)))

                If divisor = 0 Then ' protejo contra posibles resultados vacios
                    Continue For
                End If

                GVreporte2.Rows(pC).Cells(10).Text =
                    Math.Round(Decimal.Multiply(Decimal.Divide(Decimal.Parse(
                        GVreporte2.Rows(pC).Cells(5).Text),
                        divisor
                    ), 100), 2, MidpointRounding.AwayFromZero) ' 2 decimales de precisión
            Next
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try
    End Sub

    ' cuando se generan automáticamente las columnas de un gridview, las predefinnidas se insertan
    ' a la izquierda de las generadas. queremos que el % de terminadas esté hasta la derecha, así que
    ' en la creación de la fila se acomoden las columnas
    Protected Sub GVreporte2_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVreporte2.RowCreated
        Dim row As GridViewRow = e.Row
        Dim columns As New List(Of TableCell)
        Dim cell As TableCell = row.Cells(1)
        row.Cells.Remove(cell) ' retira la columna %
        columns.Add(cell) ' insertala al final de la fila
        row.Cells.AddRange(columns.ToArray())
    End Sub

    Protected Sub GVreporte2_PreRender(sender As Object, e As EventArgs) Handles GVreporte2.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVreporte2.Rows.Count > 0 Then
            GVreporte2.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub
End Class
