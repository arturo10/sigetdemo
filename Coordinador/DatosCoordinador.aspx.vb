﻿Imports Siget

Imports System.Data.SqlClient

Partial Class profesor_DatosCoordinador
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            aplicaLenguaje()

            initPageData()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = "Mis Datos"
    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub initPageData()
        Try
            'Hago consulta para sacar los datos del mensaje
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()

                Dim cmd As SqlCommand = New SqlCommand("SELECT C.Nombre, C.Apellidos, C.Email FROM Coordinador C, Usuario U WHERE C.IdUsuario = U.IdUsuario AND U.Login = @Login", conn)
                cmd.Parameters.AddWithValue("@Login", User.Identity.Name.Trim())

                Dim results As SqlDataReader = cmd.ExecuteReader()
                results.Read()
                tbNombres.Text = results.Item("Nombre").ToString()
                tbApellidos.Text = results.Item("Apellidos").ToString()
                tbEmail.Text = results.Item("Email").ToString()

                results.Close()
                cmd.Dispose()
                conn.Close()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

#End Region

    Protected Sub BtnActualizar_Click(sender As Object, e As EventArgs) Handles BtnActualizar.Click
        msgError.hide()
        msgSuccess.hide()

        If tbNombres.Text.Trim() = "" Then
            msgError.show("El nombre no puede estar vacío.")
        ElseIf tbApellidos.Text.Trim() = "" Then
            msgError.show("Los apellidos no pueden estar vacíos.")
        Else
            SDSprofesores.UpdateCommand = "SET dateformat dmy; UPDATE Coordinador SET Nombre = @Nombres, Apellidos = @Apellidos, Email = @Email, FechaModif = getdate(), Modifico = @Login WHERE IdCoordinador = (SELECT C.IdCoordinador FROM Coordinador C, Usuario U WHERE C.IdUsuario = U.IdUsuario AND U.Login = @Login)"
            SDSprofesores.UpdateParameters.Add("Nombres", tbNombres.Text.Trim())
            SDSprofesores.UpdateParameters.Add("Apellidos", tbApellidos.Text.Trim())
            SDSprofesores.UpdateParameters.Add("Email", tbEmail.Text.Trim())
            SDSprofesores.UpdateParameters.Add("Login", User.Identity.Name.Trim())
            SDSprofesores.Update()
            msgSuccess.show("Se actualizó su información.")
        End If
    End Sub
End Class
