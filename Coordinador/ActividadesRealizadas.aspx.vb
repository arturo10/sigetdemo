﻿Imports Siget

Imports System.IO
Imports System.Data.SqlClient

Partial Class coordinador_ActividadesRealizadas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = Trim(User.Identity.Name)
        TitleLiteral.Text = "Actividades Realizadas"

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.GRUPO
        Label3.Text = Config.Etiqueta.ALUMNOS
        Label4.Text = Config.Etiqueta.INSTITUCION
        Label5.Text = Config.Etiqueta.CICLO
        Label6.Text = Config.Etiqueta.PLANTEL
        Label7.Text = Config.Etiqueta.NIVEL
        Label8.Text = Config.Etiqueta.GRADO
        Label9.Text = Config.Etiqueta.GRUPO

        'No asigné el parametro de fecha directamente al control porque el script espera a que le haya pasado todos los
        'parámetros para ejecutar la consulta y en este caso a veces se ejecuta sin que se haya seleccionado fechas
        If Not (PnlFechas.Visible) Then 'Ya que este procedimiento se ejecuta cada que se activa un post (al seleccionar el RB)
            Session("De") = "ninguna"
            Session("Al") = "ninguna"
        End If
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
        DDLplantel.Items.Insert(1, New ListItem("---TODOS LOS PLANTELES", -1))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRADO & " " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRUPO & " " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            ' antes de exportar la copia del gridview, en cada fila:
            For Each r As GridViewRow In GVactual.Rows()
                ' elimino la columna de "seleccionar"
                r.Cells.RemoveAt(0)
            Next
            ' ahora quito también la columna de seleccionar, y una del footer para que no se desfase
            GVactual.HeaderRow.Cells.RemoveAt(0)
            GVactual.FooterRow.Cells.RemoveAt(0)

            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GValumnos, "ActividadesRealizadas")
    End Sub

    Protected Sub GValumnos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GValumnos.RowDataBound

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(1)
            LnkHeaderText.Text = "Nombre del " & Config.Etiqueta.ALUMNO
        End If

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GValumnos, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    Protected Sub GValumnos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GValumnos.SelectedIndexChanged
        Session("IdCicloEscolar") = DDLcicloescolar.SelectedValue
        Session("IdGrado") = DDLgrado.SelectedValue
        Session("IdAlumno") = GValumnos.SelectedDataKey(0).ToString()
        Session("Nombre") = HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(1).Text.ToString())
        Session("Plantel") = DDLplantel.SelectedItem.ToString() + " - " + DDLgrupo.SelectedItem.Text
        Session("Equipo") = GValumnos.SelectedDataKey(1).ToString()
        Response.Redirect("ReporteAsignaturas.aspx")
    End Sub

    Protected Sub GValumnos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GValumnos.DataBound
        ' calculo los totales de realizadas y cumplidas, y el promedio de el cumplimiento
        Dim TotalRealizadas As Integer = 0
        Dim TotalCumplidas As Integer = 0
        Dim CuentaCumplimiento As Integer = 0
        Dim PromedioCumplimiento As Decimal
        For Each r As GridViewRow In GValumnos.Rows
            TotalRealizadas += CType(r.Cells(4).Text, Integer)
            TotalCumplidas += CType(r.Cells(5).Text, Integer)
            If Decimal.TryParse(r.Cells(6).Text, New Decimal) Then
                CuentaCumplimiento += 1
                PromedioCumplimiento += Decimal.Parse(r.Cells(6).Text)
            End If
        Next
        If CuentaCumplimiento > 0 Then
            PromedioCumplimiento = Math.Round(
                PromedioCumplimiento / CType(CuentaCumplimiento, Decimal),
                2,
                MidpointRounding.AwayFromZero) ' 2 decimales de precisión
        End If
        ' imprimo los totales
        If Not IsNothing(GValumnos.FooterRow) Then
            GValumnos.FooterRow.Cells(4).Text = "<h3><b>Total:&nbsp;" & TotalRealizadas.ToString() + "</b> </h3>"
            GValumnos.FooterRow.Cells(5).Text = "<h3><b>Total:&nbsp;" & TotalCumplidas.ToString() + "</b> </h3>"
            GValumnos.FooterRow.Cells(6).Text = "<h3><b>Promedio:&nbsp;" & PromedioCumplimiento.ToString() + "</b></h3>"
            GValumnos.FooterRow.Cells(1).Text = "<h3><b>Total Alumnos:&nbsp;" & CType(SDSactividades.Select(DataSourceSelectArguments.Empty), Data.DataView).Table.Rows.Count().ToString() & "</b></h3>"
        End If

        If GValumnos.Rows.Count > 0 Then
            BtnExportar.Visible = True

            Dim cap As String = "<B>Listado de " & Config.Etiqueta.ALUMNOS &
                " de " & Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO &
                " " + DDLgrupo.SelectedItem.ToString +
                "<br />con el resultado de actividades realizadas"
            If CBfechas.Checked Then
                cap += " del " & TBde.Text & " al " & TBal.Text
            End If
            cap += " en " & Config.Etiqueta.ARTDET_CICLO & " " & Config.Etiqueta.CICLO &
                    " " + DDLcicloescolar.SelectedItem.ToString + "<br>" + _
                    "Reporte emitido el " + Date.Today.ToShortDateString + "</B>"
            GValumnos.Caption = cap

            msgInfo.hide()
        Else
            BtnExportar.Visible = False
            If Not DDLgrupo.SelectedIndex = 0 Then
                msgInfo.show("Ningún " & Config.Etiqueta.ALUMNO & " de este " & Config.Etiqueta.GRUPO & " y con ese estado ha terminado actividaes.")
            End If
        End If
    End Sub

    Protected Sub CBfechas_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CBfechas.CheckedChanged
        If Not (CBfechas.Checked) Then
            Session("De") = "ninguna"
            Session("Al") = "ninguna"
            PnlFechas.Visible = False
            TBde.Text = ""
            TBal.Text = ""
        Else
            PnlFechas.Visible = True
        End If
    End Sub

    Protected Sub TBde_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TBde.TextChanged
        If Date.TryParse(TBde.Text, New Date()) Then
            Session("De") = CDate(TBde.Text)
        End If
    End Sub

    Protected Sub TBal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TBal.TextChanged
        If Date.TryParse(TBal.Text, New Date()) Then
            Session("Al") = CDate(TBal.Text)
        End If
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GValumnos_PreRender(sender As Object, e As EventArgs) Handles GValumnos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GValumnos.Rows.Count > 0 Then
            GValumnos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GValumnos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GValumnos.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If GValumnos.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GValumnos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GValumnos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

    Protected Sub DDLplantel_DataBound1(sender As Object, e As EventArgs)

    End Sub
End Class
