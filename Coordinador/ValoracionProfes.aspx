﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="ValoracionProfes.aspx.vb" 
    Inherits="coordinador_ValoracionProfes" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-family: Arial;
            font-size: small;
            font-weight: bold;
            height: 38px;
            color: #000000;
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }
        .auto-style1 {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Consultas - Valoración
        <asp:Label ID="Label11" runat="server" Text="[PROFESORES]" />
    </h1>
    Permite consultar el documento cargado donde se encuentran 
                    los resultados de la valoración de un 
												<asp:Label ID="Label1" runat="server" Text="[PROFESOR]" />
    en un 
												<asp:Label ID="Label2" runat="server" Text="[GRUPO]" />
    , 
												<asp:Label ID="Label3" runat="server" Text="[ASIGNATURA]" />
    y 
												<asp:Label ID="Label4" runat="server" Text="[CICLO]" />
    específicos.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style23" colspan="6">Consultar valoraciones de 
								<asp:Label ID="Label5" runat="server" Text="[PROFESORES]" />
                por 
								<asp:Label ID="Label6" runat="server" Text="[ASIGNATURA]" />
                .</td>
        </tr>
        <tr>
            <td class="style24" colspan="3">
                <asp:Label ID="Label7" runat="server" Text="[INSTITUCION]" />
            </td>
            <td colspan="3" class="auto-style1">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    Height="22px" Width="330px" DataSourceID="SDSinstituciones"
                    DataTextField="Descripcion" DataValueField="IdInstitucion">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style24" colspan="3">
                <asp:Label ID="Label8" runat="server" Text="[CICLO]" />
            </td>
            <td colspan="3" class="auto-style1">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px"
                    Width="330px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style24" colspan="3">
                <asp:Label ID="Label9" runat="server" Text="[PLANTEL]" />
            </td>
            <td colspan="3" class="auto-style1">
                <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                    DataValueField="IdPlantel" Height="22px" Width="330px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style24" colspan="3">
                <asp:Label ID="Label10" runat="server" Text="[PROFESOR]" />
            </td>
            <td colspan="3" class="auto-style1">
                <asp:DropDownList ID="DDLprofesor" runat="server" AutoPostBack="True"
                    DataSourceID="SDSprofesores" DataTextField="NomProfesor"
                    DataValueField="IdProfesor" Height="22px"
                    Width="330px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="GVdatos" runat="server"
                    AllowSorting="True"
                    AutoGenerateColumns="False" 
                    Caption="<h3>GRUPOS y ASIGNATURAS asignados en el Calendario escolar elegido</h3>"

                    DataSourceID="SDSgrupos" 
                    DataKeyNames="IdGrupo,IdGrado" 
                    Width="884px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"
                            SortExpression="Asignatura" />
                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo" InsertVisible="False"
                            ReadOnly="True" SortExpression="IdGrupo" Visible="False" />
                        <asp:BoundField DataField="Grupo" HeaderText="Grupo" SortExpression="Grupo" />
                        <asp:BoundField DataField="IdGrado" HeaderText="IdGrado" InsertVisible="False"
                            ReadOnly="True" SortExpression="IdGrado" Visible="False" />
                        <asp:BoundField DataField="Grado" HeaderText="Grado" SortExpression="Grado" />
                        <asp:BoundField DataField="Nivel" HeaderText="Nivel" SortExpression="Nivel" />
                        <asp:BoundField DataField="ArchivoValoracion" HeaderText="ArchivoValoracion"
                            SortExpression="ArchivoValoracion" Visible="False" />
                        <asp:HyperLinkField DataNavigateUrlFields="ArchivoValoracion"
                            DataNavigateUrlFormatString="~/Resources/cargas/valoraciones/{0}"
                            ItemStyle-CssClass="columnaHyperlink"
                            DataTextField="ArchivoValoracion" HeaderText="Archivo con la Valoración"
                            Target="_blank" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  ASIGNATURA
	                1  idgrupo
	                2  GRUPO
	                3  idgrado
	                4  GRADO
	                5  NIVEL
	                6  archivovaloracion
	                7  archivo con la valoracion
	                --%>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: left">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSprofesores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select distinct P.IdProfesor, P.Apellidos + ', ' + P.Nombre as NomProfesor
from Profesor P, CoordinadorPlantel C, Programacion Pr
where C.IdCoordinador = (select IdCoordinador from Coordinador, Usuario where Usuario.Login = @Login and Coordinador.IdUsuario = Usuario.IdUsuario)
and C.IdPlantel = @IdPlantel
and  Pr.IdCicloEscolar = @IdCicloEscolar and Pr.IdProfesor in (select IdProfesor from Profesor where IdPlantel = @IdPlantel)
and P.IdProfesor = Pr.IdProfesor
order by NomProfesor">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select distinct A.Descripcion as Asignatura, G.IdGrupo, G.Descripcion as Grupo, Gr.IdGrado, Gr.Descripcion Grado, N.Descripcion as Nivel,  Pr.ArchivoValoracion
from Grupo G, Grado Gr, Nivel N, Asignatura A, Programacion Pr 
where Pr.IdProfesor = @IdProfesor
and Pr.IdCicloEscolar = @IdCicloEscolar and G.IdGrupo = Pr.IdGrupo and G.IdPlantel = @IdPlantel
and Gr.IdGrado = G.IdGrado and N.IdNivel = Gr.IdNivel
and A.IdAsignatura = Pr.IdAsignatura
order by N.Descripcion, Gr.Descripcion, G.Descripcion, A.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLprofesor" Name="IdProfesor"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT distinct I.IdInstitucion, I.Descripcion 
FROM Institucion I, CoordinadorPlantel CP, Plantel P
WHERE I.Estatus = 'Activo' and I.IdInstitucion = P.IdInstitucion
and P.IdPlantel = CP.IdPlantel and 
CP.IdCoordinador = (select C.IdCoordinador from Coordinador C, Usuario U where
U.Login = @Login and C.IdUsuario = U.IdUsuario)
ORDER BY I.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.IdPlantel, P.Descripcion
from Plantel P, CoordinadorPlantel C
where C.IdCoordinador in (Select IdCoordinador from Coordinador, Usuario
where Coordinador.IdUsuario = Usuario.IdUsuario and Login = @Login)
and P.IdPlantel = C.IdPlantel and P.IdInstitucion = @IdInstitucion 
order by P.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

