﻿Imports Siget

Imports System.Data
Imports System.IO
Imports System.Data.SqlClient

Partial Class coordinador_AvanceActividades
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        'UserInterface.Include.JQuery(CType(Master.FindControl("pnlHeader"), Literal))
        UserInterface.Include.Spinner(CType(Master.FindControl("pnlHeader"), Literal))

        Session("Login") = User.Identity.Name
        TitleLiteral.Text = "Avance en Realización"

        Label1.Text = Config.Etiqueta.ASIGNATURA
        Label2.Text = Config.Etiqueta.PLANTELES
        Label3.Text = Config.Etiqueta.GRUPOS
        Label4.Text = Config.Etiqueta.INSTITUCION
        Label5.Text = Config.Etiqueta.CICLO
        Label6.Text = Config.Etiqueta.NIVEL
        Label7.Text = Config.Etiqueta.GRADO
        Label8.Text = Config.Etiqueta.ASIGNATURA

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader

        miComando = objConexion.CreateCommand
        'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
        miComando.CommandText = "select C.IdCoordinador from Coordinador C, Usuario U where C.IdUsuario = U.IdUsuario " + _
                                "and U.Login = '" + Trim(User.Identity.Name) + "'"
        Try
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            Session("IdCoordinador") = misRegistros.Item("IdCoordinador")
            misRegistros.Close()
            objConexion.Close()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLasignatura_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.SelectedIndexChanged
        Try
            'Para generar el contenido del GridView dinamicamente, es necesario que su propiedad AutoGenerateColumns = True 
            Dim objCommand As New SqlClient.SqlCommand("sp_detalleactividades"), Conexion As String = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Parameters.Add("@IdCicloEscolar", SqlDbType.Int, 4)
            objCommand.Parameters("@IdCicloEscolar").Value = DDLcicloescolar.SelectedValue
            objCommand.Parameters.Add("@IdInstitucion", SqlDbType.Int, 4)
            objCommand.Parameters("@IdInstitucion").Value = DDLinstitucion.SelectedValue
            objCommand.Parameters.Add("@IdCoordinador", SqlDbType.Int, 4)
            objCommand.Parameters("@IdCoordinador").Value = CInt(Session("IdCoordinador"))
            objCommand.Parameters.Add("@IdAsignatura", SqlDbType.Int, 4)
            objCommand.Parameters("@IdAsignatura").Value = DDLasignatura.SelectedValue
            objCommand.CommandTimeout = 3000
            objCommand.Connection = New SqlClient.SqlConnection(Conexion)
            objCommand.Connection.Open()
            'Crear el DataReader
            'Dim Datos As SqlClient.SqlDataReader
            'Con el método ExecuteReader() del comando se traen los datos
            'Datos = objCommand.ExecuteReader()
            GVreporte.DataSource = objCommand.ExecuteReader()   'Antes usaba: GVreporte.DataSource = Datos


            GVreporte.DataBind() 'Es necesario para que se vea el resultado

            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_CICLO & " " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRADO & " " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub GVreporte_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVreporte.DataBound
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader

        miComando = objConexion.CreateCommand
        'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
        miComando.CommandText = "select P.Descripcion from Plantel P, CoordinadorPlantel C where P.IdInstitucion = " + DDLinstitucion.SelectedValue + _
                                " and C.IdCoordinador = " + Session("IdCoordinador").ToString + " and P.IdPlantel = C.IdPlantel order by P.Descripcion"
        Try
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            GVreporte.Caption = "<B>" + DDLinstitucion.SelectedItem.ToString + " - " + _
                                  DDLcicloescolar.SelectedItem.ToString + "<BR>" + _
                                  DDLgrado.SelectedItem.ToString + " - " + DDLnivel.SelectedItem.ToString + _
                                  "<BR>Materia: " + DDLasignatura.SelectedItem.ToString + "<BR>"

            GVreporte.Caption = GVreporte.Caption + Config.Etiqueta.PLANTELES + " Promediados: " + misRegistros.Item("Descripcion").ToString

            While misRegistros.Read()
                GVreporte.Caption = GVreporte.Caption + ", " + misRegistros.Item("Descripcion").ToString
            End While
            GVreporte.Caption = GVreporte.Caption + "<BR>" + "Reporte al " + Date.Today.ToShortDateString + "</B>"

            misRegistros.Close()
            objConexion.Close()

            If GVreporte.Rows.Count = 0 Then
                msgInfo.show("No hay Actividades registradas en esta " & Config.Etiqueta.ASIGNATURA)
            Else
                msgInfo.hide()
            End If
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
        If GVreporte.Rows.Count > 0 Then
            BtnExportar.Visible = True
        Else
            BtnExportar.Visible = False
        End If

        ' AÑADO PORCENTAJES DE TERMINADAS
        ' para cada fila calcula el porcentaje, y añadelo a la fila
        Try
            For pC As Integer = 0 To GVreporte.Rows.Count() - 1
                Dim divisor As Decimal =
                        Decimal.Add(Decimal.Parse(GVreporte.Rows(pC).Cells(6).Text),
                        Decimal.Add(Decimal.Parse(GVreporte.Rows(pC).Cells(7).Text),
                        Decimal.Parse(GVreporte.Rows(pC).Cells(8).Text)))

                If divisor = 0 Then ' protejo contra posibles resultados vacios
                    Continue For
                End If

                GVreporte.Rows(pC).Cells(13).Text =
                    Math.Round(Decimal.Multiply(Decimal.Divide(Decimal.Parse(
                        GVreporte.Rows(pC).Cells(8).Text),
                        divisor
                    ), 100), 2, MidpointRounding.AwayFromZero) ' 2 decimales de precisión
            Next
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        'Este procedimiento convierte el GridView a Excel eliminando la primera y segunda columna
        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            'Algoritmo que oculta toda la columna donde aparece el "Seleccionar"
            Dim Cont As Integer
            GVactual.HeaderRow.Cells.Item(0).Visible = False
            For Cont = 0 To GVactual.Rows.Count - 1
                GVactual.Rows(Cont).Cells.Item(0).Visible = False
            Next
            'Algoritmo que oculta toda la columna IdActividad
            GVactual.HeaderRow.Cells.Item(1).Visible = False
            For Cont = 0 To GVactual.Rows.Count - 1
                GVactual.Rows(Cont).Cells.Item(1).Visible = False
            Next
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVreporte, "AvanceActividades")
    End Sub

    Protected Sub GVreporte_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVreporte.SelectedIndexChanged
        Session("IdCicloEscolar") = DDLcicloescolar.SelectedValue
        Session("IdInstitucion") = DDLinstitucion.SelectedValue
        Session("IdGrado") = DDLgrado.SelectedValue 'Este dato lo usaré al sacar los grupos
        'Session("IdCoordinador") ya está creada
        Session("IdEvaluacion") = GVreporte.SelectedDataKey.Values(0).ToString    'GVreporte.SelectedRow.Cells(1).Text
        Session("Titulo") = "<B>" + DDLinstitucion.SelectedItem.ToString + " - " + _
                                  DDLcicloescolar.SelectedItem.ToString + "<BR>" + _
                                  DDLgrado.SelectedItem.ToString + " - " + DDLnivel.SelectedItem.ToString + _
                                  "<BR>Materia: " + DDLasignatura.SelectedItem.ToString + " - " + _
                                  "Actividad: " + GVreporte.SelectedRow.Cells(3).Text + "<BR>" + "Reporte al " + Date.Today.ToShortDateString + "</B>"
        Response.Redirect("AvanceActividadesPlantel.aspx")
    End Sub

    Protected Sub GVreporte_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVreporte.RowDataBound
        If (e.Row.Cells.Count > 1) And (e.Row.RowIndex > -1) Then  'Para que pinte cuando haya mas de una columna en el Grid
            e.Row.Cells(9).BackColor = Drawing.Color.LightBlue
            e.Row.Cells(10).BackColor = Drawing.Color.LightGreen
            e.Row.Cells(11).BackColor = Drawing.Color.Yellow
            e.Row.Cells(12).BackColor = Drawing.Color.LightSalmon
        End If
        e.Row.Cells(1).Visible = False

        ' añado el evento del spinner, y función en javascript para que seleccione la fila entera
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", "setSpinner(''); " + Page.ClientScript.GetPostBackClientHyperlink(GVreporte, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    ' cuando se generan automáticamente las columnas de un gridview, las predefinnidas se insertan
    ' a la izquierda de las generadas. queremos que el % de terminadas esté hasta la derecha, así que
    ' en la creación de la fila se acomoden las columnas
    Protected Sub GVreporte_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVreporte.RowCreated
        Dim row As GridViewRow = e.Row
        Dim columns As New List(Of TableCell)
        Dim cell As TableCell = row.Cells(1)
        row.Cells.Remove(cell) ' retira la columna %
        columns.Add(cell) ' insertala al final de la fila
        row.Cells.AddRange(columns.ToArray())
    End Sub

    Protected Sub GVreporte_PreRender(sender As Object, e As EventArgs) Handles GVreporte.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVreporte.Rows.Count > 0 Then
            GVreporte.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub
End Class
