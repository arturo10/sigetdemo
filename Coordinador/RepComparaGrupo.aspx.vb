﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO

Partial Class coordinador_RepComparaGrupo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.ASIGNATURAS
        Label2.Text = Config.Etiqueta.GRADO
        Label3.Text = Config.Etiqueta.PLANTEL
        Label4.Text = Config.Etiqueta.GRUPOS
        Label5.Text = Config.Etiqueta.ALUMNOS
        If Session("Rep") = "A" Then
            Dim strConexion As String
            'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            'TUVE QUE CARGAR LA CADENA DE CONSULTA ASI PORQUE SI LO PONIA DIRECTAMENTE EN EL SDS MARCABA ERROR POR SER VARIOS VALORES
            'RECORDAR QUE PARA QUE SE CARGUE EN EL GRIDVIEW ESTE DEBE TENER MARCADO EL CHECKBOX DE "GENERAR CAMPOS AUTOMATICAMENTE"
            SDSmaxcal.ConnectionString = strConexion
            SDSmaxcal.SelectCommand = "select Cal.Descripcion as Calificacion, (select SUM(Porcentaje) from Evaluacion where " + _
                                                    "IdCalificacion = Cal.IdCalificacion and InicioContestar <= getdate()) as Maximo " + _
                                                    "from Calificacion Cal where Cal.IdCalificacion in (" + Session("Calificaciones") + ")"
            GVmaximos.DataBind()
        Else
            GVmaximos.Visible = False
        End If
    End Sub

    Protected Sub GVreporte_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVreporte.RowDataBound
        '1)PRIMERO OBTENGO LOS RANGOS DE LOS INDICADORES PARA EL SEMAFORO ROJO, AMARILLO Y VERDE
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader

        miComando = objConexion.CreateCommand
        miComando.CommandText = "select I.MinAmarillo, I.MinVerde, I.MinAzul from Indicador I, CicloEscolar C where " + _
                                "I.IdIndicador = C.IdIndicador and C.IdCicloEscolar = " + Session("IdCicloEscolar").ToString
        Dim MinAmarillo, MinVerde, MinAzul As Decimal
        Try
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            MinAmarillo = misRegistros.Item("MinAmarillo")
            MinVerde = misRegistros.Item("MinVerde")
            MinAzul = misRegistros.Item("MinAzul")
            misRegistros.Close()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. <br />" & ex.Message.ToString())
        End Try

        '2)HAGO UN CICLO QUE VAYA DE 1 AL TOTAL DE COLUMNAS (EL INDICE DE LAS CELDAS(COLUMNAS) INICIA EN 0 (CELLS(0)= )
        'La parte de la condición: (e.Row.RowIndex > -1) es porque en este caso que comparo fechas estaba pintando tambien el encabezado (fila -1), que tambien comparaba
        If (e.Row.Cells.Count > 1) And (e.Row.RowIndex > -1) Then  'Para que se realice la comparacion cuando haya mas de una columna
            For I As Byte = 3 To e.Row.Cells.Count - 1 'Inicio a partir de la tercer columna a comparar
                If IsNumeric(Trim(e.Row.Cells(I).Text)) Then 'para que no compare celdas vacias
                    If (CDbl(e.Row.Cells(I).Text) < MinAmarillo) Then
                        e.Row.Cells(I).BackColor = Drawing.Color.LightSalmon
                    ElseIf (CDbl(e.Row.Cells(I).Text) < MinVerde) Then
                        e.Row.Cells(I).BackColor = Drawing.Color.Yellow
                    ElseIf (CDbl(e.Row.Cells(I).Text) < MinAzul) Then
                        e.Row.Cells(I).BackColor = Drawing.Color.LightGreen
                    Else 'Significa que es mayor o igual al MinAzul:
                        e.Row.Cells(I).BackColor = Drawing.Color.LightBlue
                    End If
                End If
            Next

            Dim Cuenta As Byte
            Cuenta = 4 'La columna 4 es la primera de las calificaciones acumuladas, luego aparecen cada 2 columnas
            Do While (Cuenta <= (e.Row.Cells.Count))
                'Obtengo los datos de la Calificacion para ver si tiene asignado un Indicador específico
                miComando.CommandText = "select * from Calificacion where IdCalificacion = " + e.Row.Cells(Cuenta - 1).Text
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read()

                'Verifico si tiene asignado algun Indicador Específico
                Dim Ind = 0
                If Not IsDBNull(misRegistros.Item("IdIndicador")) Then
                    Ind = misRegistros.Item("IdIndicador")
                End If
                misRegistros.Close()
                'Si tiene un indicador, obtengo los Rangos para pintar la celda
                If Ind > 0 Then
                    miComando.CommandText = "select * from Rango where IdIndicador=" + Ind.ToString
                    misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                    Do While misRegistros.Read()
                        'Aquí sucede que una actividad no realizada no aparece 0, sino espacio en blanco, por tanto tengo que hacer lo siguiente
                        Dim Valor As Decimal
                        If e.Row.Cells(Cuenta).Text = "&nbsp;" Then
                            Valor = 0
                        Else
                            Valor = CDbl(e.Row.Cells(Cuenta).Text)
                        End If

                        'Voy leyendo Rango por Rango y pinto comparando pintando donde está la celda con el valor de la calificación, que es la que está a la derecha del IdCalificacion
                        If (Valor >= misRegistros.Item("Inferior")) And (Valor <= misRegistros.Item("Superior")) And e.Row.Cells(Cuenta).Text <> "&nbsp;" Then
                            e.Row.Cells(Cuenta).BackColor = Drawing.ColorTranslator.FromHtml("#" + misRegistros.Item("Color"))
                            Exit Do
                        End If
                    Loop
                    misRegistros.Close()
                End If
                Cuenta += 2
            Loop
        End If 'del (e.Row.Cells.Count > 1) And (e.Row.RowIndex > -1) Then
        objConexion.Close()

        'Con lo siguiente oculto la columna IdPlantel
        'OJO, EL GRIDVIEW DEBE TENER DESHABILITADA LA OPCION DE HABILITAR PAGINACION
        e.Row.Cells(1).Visible = False

        'Ahora oculto todos los IdCalificacion
        Dim Cont As Byte
        Cont = 3 'La columna 3 es el Primero de los IdCalificacion, luego aparecen cada 2 columnas
        Do While (Cont < e.Row.Cells.Count)
            e.Row.Cells(Cont).Visible = False
            Cont += 2
        Loop

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(2).Text = Config.Etiqueta.GRUPO
        End If

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVreporte, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    Protected Sub GVreporte_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVreporte.SelectedIndexChanged
        Session("IdGrupo") = GVreporte.SelectedDataKey.Values(0).ToString()
        Session("NomGrupo") = GVreporte.SelectedRow.Cells(2).Text
        'Session("Calificaciones"), Session("Titulo") y Session("NomPlantel") ya existen en memoria
        Response.Redirect("RepComparaAlumno.aspx")
    End Sub

    Protected Sub GVreporte_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVreporte.DataBound
        If GVreporte.Rows.Count > 0 Then
            GVreporte.Caption = Session("Titulo") + "<BR> para " & Config.Etiqueta.ARTDET_PLANTEL & " " &
         Config.Etiqueta.PLANTEL & " " + Session("NomPlantel")
            BtnExportar.Visible = True
        Else
            BtnExportar.Visible = False
        End If
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        'Este procedimiento convierte el GridView a Excel eliminando la primera y segunda columna
        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            'Algoritmo que oculta toda la columna donde aparece el "Seleccionar"
            Dim Cont As Integer
            GVactual.HeaderRow.Cells.Item(0).Visible = False
            For Cont = 0 To GVactual.Rows.Count - 1
                GVactual.Rows(Cont).Cells.Item(0).Visible = False
            Next
            'Algoritmo que oculta toda la columna IdPlantel
            'GVactual.HeaderRow.Cells.Item(1).Visible = False
            'For Cont = 0 To GVactual.Rows.Count - 1
            'GVactual.Rows(Cont).Cells.Item(1).Visible = False
            'Next
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVreporte, "ComparacionGrupo")
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVreporte_PreRender(sender As Object, e As EventArgs) Handles GVreporte.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVreporte.Rows.Count > 0 Then
            GVreporte.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVreporte.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVreporte, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

End Class
