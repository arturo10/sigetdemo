﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.master" AutoEventWireup="true"
     CodeFile="AvanceAsignatura.aspx.cs" Inherits="Coordinador_AvanceAsignatura" %>


<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
     <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" Runat="Server">
</asp:Content>


<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" Runat="Server">
    
    <table style="width:100%;">
        <tr style="text-align:center;">
            <td colspan="5" style="width:100%;display:table;margin-left:auto;margin-right:auto;">
                <asp:Label runat="server" ID="TextSelectPlantel" style="margin:8px;">Selecciona un plantel</asp:Label>
                <asp:DropDownList ID="DDLPlantel"
                    runat="server" OnDataBound="DDLPlantel_DataBound"
                    DataSourceID="SDSPlanteles" DataValueField="IdPlantel" AutoPostBack="true"
                    DataTextField="Descripcion" style="width:200px;">
                </asp:DropDownList>

            </td>
        </tr>
        <tr style="text-align:center;">
            <td>
                 <asp:Label runat="server" ID="Label1" style="margin:8px;">Selecciona un nivel</asp:Label>
                <asp:DropDownList ID="DDLNivel"
                    runat="server" OnDataBound="DDLNivel_DataBound"
                    DataSourceID="SDSNiveles" DataValueField="IdNivel" AutoPostBack="true"
                    DataTextField="Descripcion" style="width:200px;">
                </asp:DropDownList>

            </td>
        </tr>
        <tr style="text-align:center;">
            <td>
                 <asp:Label runat="server" ID="Label2" style="margin:8px;">Selecciona un grado</asp:Label>
                <asp:DropDownList ID="DDLGrado"
                    runat="server" OnDataBound="DDLGrado_DataBound"
                    DataSourceID="SDSGrados" DataValueField="IdGrado" AutoPostBack="true"
                    DataTextField="Descripcion" style="width:200px;">
                </asp:DropDownList>

            </td>
        </tr>
          <tr style="width:100%;display:table;margin-left:auto;margin-right:auto;text-decoration: none !important;">
            <td>
            <asp:GridView ID="GVAvancePorAsignatura" runat="server" AllowPaging="True"
                AllowSorting="True" AutoGenerateColumns="False" BackColor="White" CssClass="WL"
                BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" OnRowCommand="GVAvancePorAsignatura_RowCommand"
                DataSourceID="SDSAvanceAsignatura" GridLines="Vertical" OnDataBound="GVAvancePorAsignatura_DataBound"
                Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small; "
                Width="882px" DataKeyNames="IdAsignatura"
                Caption="<h3> Reporte Avance de Actividades por Asignatura</h3>"
                ForeColor="Black" PageSize="20">
                <Columns>
                 
                    <asp:ButtonField DataTextField="Descripcion"  HeaderText="Asignatura"
                        SortExpression="Descripcion" />
                    <asp:ButtonField DataTextField="Evaluaciones_Asignadas" HeaderText="Evaluaciones Asignadas" DataTextFormatString="{0:N0}"
                        SortExpression="Evaluaciones_Asignadas" itemstyle-horizontalalign="Right" />
                      <asp:ButtonField DataTextField="Evaluaciones_Terminadas" HeaderText="Evaluaciones Terminadas"
                        SortExpression="Evaluaciones_Terminadas" itemstyle-horizontalalign="Right" />
                    <asp:ButtonField DataTextField="Avance" HeaderText="Avance" 
                        SortExpression="Avance" itemstyle-horizontalalign="Right" />
                    <asp:ButtonField DataTextField="Promedio" HeaderText="Promedio"
                        SortExpression="Promedio" itemstyle-horizontalalign="Right" />

                </Columns>
                <FooterStyle BackColor="#CCCCCC" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="#CCCCCC" />
            </asp:GridView>
            </td>
        </tr>
      

         <tr style="text-align:center;">
            <td>
                  <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel" OnClick="BtnExportar_Click"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
           
        </tr>
     
          <tr style="width:100%;display:table;margin-left:auto;margin-right:auto;">
            <td>
            <asp:GridView ID="GVDetalleAsignatura" runat="server" AllowPaging="True"
                AllowSorting="True" AutoGenerateColumns="False" BackColor="White" 
                BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" OnDataBound="GVDetalleAsignatura_DataBound"
                DataSourceID="SDSDetalleAvance" GridLines="Vertical"  
                Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"
                Width="882px" 
                Caption="<h3> Detalle Reporte Avance de Actividades por Asignatura</h3>"
                ForeColor="Black" PageSize="20">
                <Columns>
                  
                    <asp:BoundField DataField="Descripcion" HeaderText="Plantel"
                        SortExpression="Descripcion" />
                    <asp:BoundField DataField="No_Asignaciones" HeaderText="Evaluaciones Asignadas"
                        SortExpression="No_Asignaciones" itemstyle-horizontalalign="Right" />
                      <asp:BoundField DataField="Evaluaciones_Terminadas" HeaderText="Evaluaciones Terminadas"
                        SortExpression="Evaluaciones_Terminadas" itemstyle-horizontalalign="Right" />
                    <asp:BoundField DataField="Avance" HeaderText="Avance" 
                        SortExpression="Avance" itemstyle-horizontalalign="Right" />
                    <asp:BoundField DataField="Promedio" HeaderText="Promedio"
                        SortExpression="Promedio" itemstyle-horizontalalign="Right" />

                </Columns>
                <FooterStyle BackColor="#CCCCCC" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="#CCCCCC" />
            </asp:GridView>
            </td>
        </tr>
      
        
         <tr style="text-align:center;">
            <td>
                  <asp:Button ID="btnExportarDetalle" runat="server" Text="Exportar a Excel" OnClick="btnExportarDetalle_Click"
              CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
           
        </tr>

           <tr>
            <td>
                 <uc1:msgError runat="server" ID="msgError" />
            </td>
           
        </tr>
        
        <tr>
            <td colspan="2" style="text-align: left">
                <asp:HyperLink ID="HyperLink5" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
            <td class="style13">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <div class="sqlDataSources">


        <asp:SqlDataSource ID="SDSDetalleAvance" runat="server" SelectCommand="[ObtenAvanceEvaluaciones_Desglose_Directo]"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter Name="IdAsignatura" ControlID="GVAvancePorAsignatura"
                     PropertyName="SelectedDataKey('IdAsignatura')" />
            </SelectParameters>
        </asp:SqlDataSource>

         <asp:SqlDataSource ID="SDSAvanceAsignatura" runat="server" SelectCommand="ObtenAvanceEvaluacionesDirecto"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter Name="IdPlantel" ControlID="DDLPlantel" PropertyName="SelectedValue" />
                  <asp:ControlParameter Name="IdGrado" ControlID="DDLGrado" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>

        

        <asp:SqlDataSource ID="SDSPlanteles" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT * FROM PLANTEL"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSNiveles" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT * FROM NIVEL"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSGrados" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT * FROM GRADO WHERE IdNivel=@IdNivel">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLNivel" Name="IdNivel"  PropertyName="SelectedValue"/>
                    </SelectParameters>
                </asp:SqlDataSource>
    </div>


</asp:Content>

