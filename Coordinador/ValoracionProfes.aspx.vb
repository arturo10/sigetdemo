﻿Imports Siget


Partial Class coordinador_ValoracionProfes
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = User.Identity.Name

        Label1.Text = Config.Etiqueta.PROFESOR
        Label2.Text = Config.Etiqueta.GRUPO
        Label3.Text = Config.Etiqueta.ASIGNATURA
        Label4.Text = Config.Etiqueta.CICLO
        Label5.Text = Config.Etiqueta.PROFESORES
        Label6.Text = Config.Etiqueta.ASIGNATURA
        Label7.Text = Config.Etiqueta.INSTITUCION
        Label8.Text = Config.Etiqueta.CICLO
        Label9.Text = Config.Etiqueta.PLANTEL
        Label10.Text = Config.Etiqueta.PROFESOR
        Label11.Text = Config.Etiqueta.PROFESORES


    End Sub

    Protected Sub GVdatos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVdatos.DataBound
        If (GVdatos.Rows.Count <= 0) And (DDLprofesor.SelectedValue > 0) Then
            msgInfo.show("Este " &
            Config.Etiqueta.PROFESOR & " no tiene " &
            Config.Etiqueta.GRUPOS & " y " &
            Config.Etiqueta.ASIGNATURAS & " asignados.")
        Else
            msgInfo.hide()
            GVdatos.Caption = "<font size=""2""><b>" &
         Config.Etiqueta.GRUPOS & " y " &
         Config.Etiqueta.ASIGNATURAS & " asignados en " &
         Config.Etiqueta.ARTDET_CICLO & " " & Config.Etiqueta.CICLO &
         " " + DDLcicloescolar.SelectedItem.ToString &
         "<br>para " &
         Config.Etiqueta.ARTDET_PROFESOR & " " & Config.Etiqueta.PROFESOR &
         " " + DDLprofesor.SelectedItem.ToString + "</b></font>"
        End If
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLprofesor_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLprofesor.DataBound
        DDLprofesor.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PROFESOR & " " & Config.Etiqueta.PROFESOR, 0))
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVdatos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVdatos, "Select$" & e.Row.RowIndex).ToString())
        End If

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(0).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA

            LnkHeaderText = e.Row.Cells(2).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.GRUPO

            LnkHeaderText = e.Row.Cells(4).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.GRADO

            LnkHeaderText = e.Row.Cells(5).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.NIVEL
        End If
    End Sub

    Protected Sub GVdatos_PreRender(sender As Object, e As EventArgs) Handles GVdatos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVdatos.Rows.Count > 0 Then
            GVdatos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVdatos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If GVdatos.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub


    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVdatos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVdatos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

End Class
