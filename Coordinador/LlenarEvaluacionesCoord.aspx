<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="LlenarEvaluacionesCoord.aspx.vb"
    Inherits="coordinador_LlenarEvaluacionesCoord"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style27 {
            height: 26px;
        }

        .style11 {
            width: 97%;
        }

        .style26 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 144px;
        }

        .style25 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
        }

        .style14 {
            width: 268435520px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style24 {
            width: 268435488px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            height: 40px;
        }

        .style13 {
            width: 100%;
            height: 1px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 900px;
        }

        .style33 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 144px;
            height: 40px;
        }

        .style34 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            height: 40px;
        }

        .style35 {
            width: 268435520px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            height: 40px;
        }

        .style36 {
            height: 35px;
        }

        .style37 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 100px;
        }

        .style38 {
            text-align: left;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            height: 15px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Evaluaci�n - Captura
        <asp:Label ID="Label10" runat="server" Text="[ALUMNO]" />
    </h1>
    Permite capturar las respuestas que hizo un 
												<asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
    en una 
                    actividad de opci�n m�ltiple o respuesta abierta.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td>
                <table class="style11">
                    <tr>
                        <td>
                            <table class="style22">
                                <tr>
                                    <td class="style38" colspan="4">
                                        <asp:Label ID="Mensaje0" runat="server"
                                            Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #000066;">Seleccione los siguientes datos para realizar la Evaluaci�n del 
										<asp:Label ID="Label2" runat="server" Text="[ALUMNO]" />
                                            :</asp:Label></td></tr><tr>
                                    <td class="style37">
                                        <asp:Label ID="Label3" runat="server" Text="[CICLO]" />
                                    </td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                                            DataValueField="IdCicloEscolar" Width="350px" CssClass="style25"
                                            Height="22px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td><td class="style16">&nbsp;</td></tr><tr>
                                    <td class="style37">
                                        <asp:Label ID="Label4" runat="server" Text="[PLANTEL]" />
                                    </td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                            DataValueField="IdPlantel" Height="22px" Width="350px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td><td class="style16">&nbsp;</td></tr><tr>
                                    <td class="style37">
                                        <asp:Label ID="Label5" runat="server" Text="[NIVEL]" />
                                    </td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                                            Height="22px" Width="270px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td><td class="style16">&nbsp;</td></tr><tr>
                                    <td class="style37">
                                        <asp:Label ID="Label6" runat="server" Text="[GRADO]" />
                                    </td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                                            Height="22px" Width="270px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td><td class="style16">&nbsp;</td></tr><tr>
                                    <td class="style37">
                                        <span class="estandar" style="text-align: right">
                                            <asp:Label ID="Label7" runat="server" Text="[GRUPO]" />
                                        </span></td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSgrupos" DataTextField="Descripcion"
                                            DataValueField="IdGrupo" Width="270px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td><td class="style16">&nbsp;</td></tr><tr>
                                    <td class="style37">
                                        <asp:Label ID="Label8" runat="server" Text="[ASIGNATURA]" />
                                    </td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSasignaturas" DataTextField="Descripcion"
                                            DataValueField="IdAsignatura" Width="350px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td><td class="style16">&nbsp;</td></tr><tr>
                                    <td class="style37">Calificaci�n</td><td style="text-align: left">
                                        <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True"
                                            DataSourceID="SDScalificaciones" DataTextField="Descripcion"
                                            DataValueField="IdCalificacion" Width="350px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td><td class="style16">&nbsp;</td></tr><tr>
                                    <td class="style38" colspan="2">
                                        <asp:HyperLink ID="HyperLink3" runat="server"
                                            NavigateUrl="~/"
                                            CssClass="defaultBtn btnThemeGrey btnThemeWide">
																						Regresar&nbsp;al&nbsp;Men�
                                        </asp:HyperLink></td><td class="style35" style="text-align: right"></td>
                                    <td class="style24"></td>
                                </tr>
                                <tr>
                                    <td class="style37">Actividad para capturar</td><td class="style25" colspan="3">
                                        <asp:GridView ID="GVevaluaciones" runat="server"
                                            AutoGenerateColumns="False" 
                                            AutoGenerateSelectButton="False" 
                                            AllowSorting="True"
                                            
                                            DataKeyNames="IdEvaluacion" 
                                            DataSourceID="SDSevaluaciones"
                                            Width="830px" 

                                            CssClass="dataGrid_clear_selectable"
                                            GridLines="None">
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                                                <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion"
                                                    Visible="False" />
                                                <asp:BoundField DataField="IdCalificacion"
                                                    HeaderText="IdCalificacion"
                                                    SortExpression="IdCalificacion" Visible="False" />
                                                <asp:BoundField DataField="ClaveBateria" HeaderText="Clave de la Actividad"
                                                    SortExpression="ClaveBateria" />
                                                <asp:BoundField DataField="ClaveAbreviada" HeaderText="Clave p/Reporte"
                                                    SortExpression="ClaveAbreviada" />
                                                <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificaci�n"
                                                    SortExpression="Porcentaje">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                    HeaderText="Inicia" SortExpression="InicioContestar" />
                                                <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                    HeaderText="Termina" SortExpression="FinContestar" />
                                                <asp:BoundField DataField="FinSinPenalizacion"
                                                    DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha M�xima"
                                                    SortExpression="FinSinPenalizacion" />
                                                <asp:BoundField DataField="Penalizacion" HeaderText="% Penalizaci�n"
                                                    SortExpression="Penalizacion">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                            </Columns>
                                            <FooterStyle CssClass="footer" />
                                            <PagerStyle CssClass="pager" />
                                            <SelectedRowStyle CssClass="selected" />
                                            <HeaderStyle CssClass="header" />
                                            <AlternatingRowStyle CssClass="altrow" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style37">&nbsp;</td><td class="style25" colspan="3">&nbsp;</td></tr><tr>
                                    <td class="style37">Seleccione al <asp:Label ID="Label9" runat="server" Text="[ALUMNO]" />
                                    </td>
                                    <td class="style25" colspan="3">
                                        <asp:GridView ID="GValumnos" runat="server" 
                                            AllowPaging="True"
                                            AllowSorting="True" 
                                            AutoGenerateColumns="False" 
                                            PageSize="20" 
                                            Caption="ALUMNOS Con Actividad Pendiente de Realizar"
                                            
                                            DataKeyNames="IdAlumno,IdPlantel" 
                                            DataSourceID="SDSalumnos" 
                                            Width="835px" 

                                            CssClass="dataGrid_clear_selectable"
                                            GridLines="None">
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                                                <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno" Visible="False" />
                                                <asp:BoundField DataField="IdUsuario" HeaderText="IdUsuario"
                                                    SortExpression="IdUsuario" Visible="False" />
                                                <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo"
                                                    SortExpression="IdGrupo" Visible="False" />
                                                <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                                                    SortExpression="IdPlantel" Visible="False" />
                                                <asp:BoundField DataField="ApePaterno" HeaderText="Apellido Paterno"
                                                    SortExpression="ApePaterno" />
                                                <asp:BoundField DataField="ApeMaterno" HeaderText="Apellido Materno"
                                                    SortExpression="ApeMaterno" />
                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre(s)"
                                                    SortExpression="Nombre" />
                                                <asp:BoundField DataField="Matricula" HeaderText="Matr�cula"
                                                    SortExpression="Matricula" />
                                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                                <asp:BoundField DataField="FechaIngreso" HeaderText="FechaIngreso"
                                                    SortExpression="FechaIngreso" Visible="False" />
                                                <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                    SortExpression="Estatus" />
                                                <asp:BoundField DataField="FechaModif" HeaderText="FechaModif"
                                                    SortExpression="FechaModif" Visible="False" />
                                                <asp:BoundField DataField="Modifico" HeaderText="Modifico"
                                                    SortExpression="Modifico" Visible="False" />
                                            </Columns>
                                            <FooterStyle CssClass="footer" />
                                            <PagerStyle CssClass="pager" />
                                            <SelectedRowStyle CssClass="selected" />
                                            <HeaderStyle CssClass="header" />
                                            <AlternatingRowStyle CssClass="altrow" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style37">&nbsp;</td><td class="style25" colspan="3">&nbsp;</td></tr><tr>
                                    <td colspan="4">
                                        <uc1:msgInfo runat="server" ID="msgInfo" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <uc1:msgError runat="server" ID="msgError" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style37">&nbsp;</td><td class="style25" colspan="3">
                                        <asp:GridView ID="GValumnosterminados" runat="server" 
                                            AllowPaging="True"
                                            AllowSorting="True" 
                                            AutoGenerateColumns="False" 
                                            Caption="ALUMNOS CON ACTIVIDAD REALIZADA" 
                                            PageSize="20" 
                                            
                                            DataKeyNames="IdAlumno" 
                                            DataSourceID="SDSalumnosterminados" 
                                            Width="835px" 

                                            CssClass="dataGrid_clear"
                                            GridLines="None">
                                            <Columns>
                                                <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno" Visible="False" />
                                                <asp:BoundField DataField="ApePaterno" HeaderText="Apellido Paterno"
                                                    SortExpression="ApePaterno" />
                                                <asp:BoundField DataField="ApeMaterno" HeaderText="Apellido Materno"
                                                    SortExpression="ApeMaterno" />
                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre(s)"
                                                    SortExpression="Nombre" />
                                                <asp:BoundField DataField="Matricula" HeaderText="Matr�cula"
                                                    SortExpression="Matricula" />
                                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                                <asp:BoundField DataField="Resultado" DataFormatString="{0:0.00}"
                                                    HeaderText="Calificaci�n (%)" SortExpression="Resultado" />
                                                <asp:BoundField DataField="FechaTermino" DataFormatString="{0:dd/MM/yyyy}"
                                                    HeaderText="Fecha Terminada" SortExpression="FechaTermino" />
                                            </Columns>
                                            <FooterStyle CssClass="footer" />
                                            <PagerStyle CssClass="pager" />
                                            <SelectedRowStyle CssClass="selected" />
                                            <HeaderStyle CssClass="header" />
                                            <AlternatingRowStyle CssClass="altrow" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp; </td></tr><tr>
            <td style="text-align: left">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Men�
                </asp:HyperLink></td></tr></table><table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrupo], [Descripcion] FROM [Grupo] WHERE (([IdGrado] = @IdGrado) AND ([IdPlantel] = @IdPlantel)) and ([IdCicloEscolar] = @IdCicloEscolar) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSalumnos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Alumno] WHERE ([IdGrupo] = @IdGrupo) 
and IdAlumno not in (select IdAlumno from EvaluacionTerminada where
                                 IdEvaluacion = @IdEvaluacion) and (Estatus = 'Activo' or Estatus = 'Suspendido')
ORDER BY [ApePaterno], [ApeMaterno], [Nombre]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSusuarios" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Usuario]"></asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT C.IdCalificacion, C.IdCicloEscolar, C.Clave, C.IdAsignatura, A.Descripcion Materia, C.Consecutivo,C.Descripcion, C.Valor
FROM Calificacion C, Asignatura A
WHERE ([IdCicloEscolar] = @IdCicloEscolar)  and (A.IdAsignatura = C.IdAsignatura) and (A.IdAsignatura = @IdAsignatura)
ORDER BY A.Descripcion, C.Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select IdEvaluacion, IdCalificacion, ClaveBateria, ClaveAbreviada, ClaveAbreviada, Porcentaje, InicioContestar, FinContestar, FinSinPenalizacion, Penalizacion, Tipo
from Evaluacion 
where IdCalificacion = @IdCalificacion and Califica = 'C' and SeEntregaDocto = 'False'
and IdEvaluacion not in (select IdEvaluacion from EvaluacionPlantel where IdPlantel in (select IdPlantel from Grupo where IdGrupo = @IdGrupo))
UNION
select E.IdEvaluacion, E.IdCalificacion, E.ClaveBateria, E.ClaveAbreviada, E.ClaveAbreviada, E.Porcentaje, P.InicioContestar, P.FinContestar,
P.FinSinPenalizacion, E.Penalizacion, E.Tipo
from Evaluacion E, EvaluacionPlantel P
where E.IdCalificacion = @IdCalificacion and E.Califica = 'C' and E.SeEntregaDocto = 'False' and
E.IdEvaluacion = P.IdEvaluacion and P.IdPlantel in (select IdPlantel from Grupo where IdGrupo = @IdGrupo)
order by IdCalificacion, InicioContestar
">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar] WHERE ([Estatus] = 'Activo') ORDER BY [FechaInicio]"></asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdAsignatura, IdGrado, Descripcion
from Asignatura
where IdGrado=@IdGrado
order by Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLGrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSalumnosterminados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT A.IdAlumno, A.Nombre, A.ApePaterno, A.ApeMaterno, A.Matricula, A.Email, E.Resultado, E.FechaTermino
FROM Alumno A, EvaluacionTerminada E
WHERE A.IdGrupo = @IdGrupo and 
A.IdAlumno in (select IdAlumno from EvaluacionTerminada where
                                 IdEvaluacion = @IdEvaluacion)
and E.IdAlumno = A.IdAlumno and E.IdEvaluacion = @IdEvaluacion and E.IdGrupo = A.IdGrupo 
and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido')
ORDER BY ApePaterno, ApeMaterno, Nombre">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSreactivos" runat="server"></asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.IdPlantel, P.Descripcion
from Plantel P, CoordinadorPlantel C
where C.IdCoordinador in (Select IdCoordinador from Coordinador, Usuario
where Coordinador.IdUsuario = Usuario.IdUsuario and Login = @Login)
and P.IdPlantel = C.IdPlantel 
order by P.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Distinct N.IdNivel, N.Descripcion
from Nivel N, Escuela E
where E.IdPlantel =@IdPlantel and N.IdNivel = E.IdNivel
and N.Estatus = 'Activo'
order by N.IdNivel">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE (([IdNivel] = @IdNivel) AND ([Estatus] = @Estatus)) ORDER BY [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

