﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="GestionarInscripcionesCursos.aspx.vb" 
    Inherits="coordinador_AgregarCursos" 
    MaintainScrollPositionOnPostback="true" %>


<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>



<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style15 {
            width: 208px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 37px;
        }

        .style26 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            color: #000066;
            height: 37px;
        }

        .style24 {
            width: 208px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style18 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style20 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style27 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 23px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }
        .style16 {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Programación - Gestionar Inscripciones (Sistema de Educación Continua)
    </h1>
    Permite verificar cupos, número de alumnos inscritos y en dado caso dar de baja a un alumno de un curso.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td colspan="4">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="4">
                <asp:GridView ID="GVcursosActivosCupo" runat="server"

                    DataSourceID="SDScursosActivosCupo" 
                    Width="896px" AutoGenerateColumns="false" AllowSorting="true"
                    
                    CssClass="dataGrid_clear_selectable"
                    GridLines="None" DataKeyNames="IdCurso,IdCicloEscolar">
                    <Columns>
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                        <asp:BoundField DataField="Descripcion" HeaderText="Ciclo Escolar" SortExpression="Descripcion" />
                        <asp:BoundField DataField="Cupo" HeaderText="Cupo" SortExpression="Cupo" />
                        <asp:BoundField DataField="AlumnosInscritos" HeaderText="Alumnos Inscritos" SortExpression="AlumnosInscritos" />
                        <asp:BoundField DataField="SobreCupo" HeaderText="Sobrecupo" SortExpression="SobreCupo" />
                        <asp:BoundField DataField="HorasValidez" HeaderText="Horas de Validez" SortExpression="HorasValidez" />
                        <asp:BoundField DataField="Instructor" HeaderText="Instructor" SortExpression="Instructor" />
                        <asp:BoundField DataField="FechaInicio" HeaderText="Inicia(Fecha)" SortExpression="FechaInicio" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:BoundField DataField="FechaFin" HeaderText="Finaliza(Fecha)" SortExpression="FechaFin" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                        <asp:CommandField SelectText="Ver Inscritos" ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <HeaderStyle CssClass="header" />
                    <SelectedRowStyle CssClass="selected"/>
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>

        <tr>
            <td class="style11" colspan="4">
                <asp:GridView ID="GVcursosAlumno" runat="server" Visible-="false"

                    DataSourceID="SDScursosAlumnoAlumno" 
                    Width="896px" AutoGenerateColumns="false"
                    
                    CssClass="dataGrid_clear_selectable"
                    GridLines="None" DataKeyNames="IdCurso,IdAlumno,IdCicloEscolar">
                    <Columns>
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                        <asp:BoundField DataField="ApePaterno" HeaderText="Apellido Paterno" SortExpression="ApePaterno" />
                        <asp:BoundField DataField="ApeMaterno" HeaderText="Apellido Materno" SortExpression="ApeMaterno" />
                        <asp:BoundField DataField="FechaInscripcion" HeaderText="Fecha Inscripcion" SortExpression="FechaInicio" HtmlEncode="false" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
                        <asp:CommandField SelectText="Desinscribir" ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:SqlDataSource ID="SDScursosActivosCupo" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT M.IdCurso, Nombre, M.IdCicloEscolar, Descripcion, Cupo, AlumnosInscritos, CASE WHEN AlumnosInscritos - Cupo>0 THEN AlumnosInscritos - Cupo ELSE 0 END AS SobreCupo, Tipo, HorasValidez, Instructor, N.FechaInicio, N.FechaFin FROM (SELECT c.IdCurso, COUNT(ca.IdAlumno) as AlumnosInscritos, IdCicloEscolar FROM Cursos as c INNER JOIN CursosAlumno as ca on c.IdCurso = ca.IdCurso WHERE c.Estatus='Activo' GROUP BY c.IdCurso, ca.IdCicloEscolar) as M INNER JOIN Cursos as N on M.IdCurso = N.IdCurso INNER JOIN CicloEscolar as ce ON M.IdCicloEScolar = ce.IdCicloEscolar">
                    <SelectParameters>
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:SqlDataSource ID="SDScursosAlumno" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM CursosAlumno ORDER BY IdCurso">
                    <SelectParameters>
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td colspan="2">
                <asp:SqlDataSource ID="SDScursosAlumnoAlumno" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM CursosAlumno AS ca INNER JOIN Alumno AS a ON ca.IdAlumno = a.IdAlumno ORDER BY FechaInscripcion DESC">
                </asp:SqlDataSource>
            </td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td class="style22">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

