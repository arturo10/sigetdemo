﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Siget;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;


public partial class Coordinador_AvanceAsignatura : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        TitleLiteral.Text = "Avance de Actividades por Asignatura";
    }


    protected void DDLPlantel_DataBound(object sender, EventArgs e)
    {
        DDLPlantel.Items.Insert(0, new ListItem("---Todos los planteles ", "0"));
    }

    protected void DDLNivel_DataBound(object sender, EventArgs e)
    {
        DDLNivel.Items.Insert(0, new ListItem("---Elija Nivel ", "0"));
    }

    protected void DDLGrado_DataBound(object sender, EventArgs e)
    {
        DDLGrado.Items.Insert(0, new ListItem("---Elija el grado ", "0"));
    }

    protected void Convierte(GridView GVActual, string archivo)
    {

        GVActual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef");
        GVActual.HeaderStyle.BackColor = System.Drawing.Color.Black;
        GVActual.GridLines = GridLines.Both;
        GVActual.AllowPaging = false;
        GVActual.AllowSorting = false;
        GVActual.DataBind();

        if (GVActual.Rows.Count + 1 < 65536)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            Page pagina = new Page();
            HtmlForm form = new HtmlForm();
            pagina.EnableEventValidation = false;
            pagina.DesignerInitialize();
            pagina.Controls.Add(form);
            form.Controls.Add(GVActual);
            pagina.RenderControl(htw);
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(sb.ToString());
            Response.End();
        }
        else
        {
            msgError.show(Siget.Lang.FileSystem.General.MSG_ERROR[Session["Usuario"].ToString()], "Hay demasiados registros");
        }


    }
    protected void GVAvancePorAsignatura_DataBound(object sender, EventArgs e)
    {
       if(GVAvancePorAsignatura.Rows.Count==0){
           BtnExportar.Visible = false;
           GVAvancePorAsignatura.SelectedIndex = -1;
           GVDetalleAsignatura.DataBind();
       }
        else
            BtnExportar.Visible = true;
    }
    protected void BtnExportar_Click(object sender, EventArgs e)
    {
        Convierte(GVAvancePorAsignatura, "Avance_Por_Asignatura");
    }
   

    protected void GVDetalleAsignatura_DataBound(object sender, EventArgs e)
    {
        if (GVDetalleAsignatura.Rows.Count == 0){
            btnExportarDetalle.Visible = false;
            GVDetalleAsignatura.SelectedIndex = -1;
        }
           
        else
            btnExportarDetalle.Visible = true;

    }
    protected void btnExportarDetalle_Click(object sender, EventArgs e)
    {
        Convierte(GVDetalleAsignatura, "Detalle_Avance_Por_Asignatura");
    }
  
    protected void GVAvancePorAsignatura_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (Convert.ToInt32(e.CommandArgument) != -1)
        {
            GVAvancePorAsignatura.SelectedIndex = Convert.ToInt32(e.CommandArgument);
            GVAvancePorAsignatura.DataBind();
        }
    }
}
