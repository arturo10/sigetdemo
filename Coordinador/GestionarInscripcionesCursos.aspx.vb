﻿Imports Siget
Imports System.Data.SqlClient


Partial Class coordinador_AgregarCursos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = Trim(User.Identity.Name)
        TitleLiteral.Text = "Gestionar Inscripciones"


        GVcursosActivosCupo.Caption = "<h3>Inscripciones a los Cursos del Sistema de Educación Continua</h3>"
        GVcursosAlumno.Caption = "<h3>Alumnos inscritos al curso</h3>"


    End Sub

    Protected Sub GVcursosActivosCupo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVcursosActivosCupo.SelectedIndexChanged

        msgInfo.hide()
        msgSuccess.hide()
        msgError.hide()

        SDScursosAlumnoAlumno.SelectParameters.Clear()

        Try

            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()

                Dim IdCurso As Integer = GVcursosActivosCupo.SelectedDataKey.Values(0)
                Dim IdCicloEscolar As Integer = GVcursosActivosCupo.SelectedDataKey.Values(1)

                SDScursosAlumnoAlumno.SelectCommand = "SELECT * FROM CursosAlumno AS ca INNER JOIN Alumno AS a ON ca.IdAlumno = a.IdAlumno  WHERE IdCurso=@IdCurso AND IdCicloEscolar=@IdCicloEscolar ORDER BY FechaInscripcion DESC;"
                SDScursosAlumnoAlumno.SelectParameters.Add("IdCurso", IdCurso.ToString())
                SDScursosAlumnoAlumno.SelectParameters.Add("IdCicloEscolar", IdCicloEscolar.ToString())

                For Each q As Parameter In SDScursosAlumnoAlumno.SelectParameters
                    q.ConvertEmptyStringToNull = False
                Next

                conn.Close()
            End Using

            GVcursosAlumno.DataBind()
            GVcursosAlumno.Visible = True

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try

    End Sub

    Protected Sub GVcursosAlumno_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVcursosAlumno.SelectedIndexChanged

        msgInfo.hide()
        msgSuccess.hide()
        msgError.hide()

        Try

            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()

                Dim IdCurso As Integer = GVcursosAlumno.SelectedDataKey.Values(0)
                Dim IdAlumno As Integer = GVcursosAlumno.SelectedDataKey.Values(1)
                Dim IdCicloEscolar As Integer = GVcursosAlumno.SelectedDataKey.Values(2)

                SDScursosAlumno.DeleteCommand = "DELETE FROM CursosAlumno WHERE IdCurso=@IdCurso AND IdAlumno=@IdAlumno AND IdCicloEscolar = @IdCicloEscolar"
                SDScursosAlumno.DeleteParameters.Add("IdCurso", IdCurso.ToString())
                SDScursosAlumno.DeleteParameters.Add("IdAlumno", IdAlumno.ToString())
                SDScursosAlumno.DeleteParameters.Add("IdCicloEscolar", IdCicloEscolar.ToString())

                For Each q As Parameter In SDScursosAlumno.DeleteParameters
                    q.ConvertEmptyStringToNull = False
                Next

                SDScursosAlumno.Delete()
                SDScursosAlumno.DeleteParameters.Clear()

                conn.Close()
            End Using

            GVcursosAlumno.DataBind()
            GVcursosAlumno.Visible = True

            Response.Redirect(Request.RawUrl)
            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")))

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try

    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVcursosActivosCupo_PreRender(sender As Object, e As EventArgs) Handles GVcursosActivosCupo.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVcursosActivosCupo.Rows.Count > 0 Then
            GVcursosActivosCupo.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVcursosAlumno_PreRender(sender As Object, e As EventArgs) Handles GVcursosAlumno.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVcursosAlumno.Rows.Count > 0 Then
            GVcursosAlumno.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVcursosActivosCupo_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVcursosActivosCupo.RowDataBound

        If (DataBinder.Eval(e.Row.DataItem, "Tipo") = "Presencial") And (e.Row.RowIndex > -1) Then
            e.Row.Cells(9).BackColor = Drawing.Color.GreenYellow
        ElseIf (DataBinder.Eval(e.Row.DataItem, "Tipo") = "Virtual") And (e.Row.RowIndex > -1) Then
            e.Row.Cells(9).BackColor = Drawing.Color.LightSkyBlue
        End If

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(10).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVcursosActivosCupo, "Select$" & e.Row.RowIndex).ToString())
        End If

    End Sub

    Protected Sub GVcursosAlumnoRowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVcursosAlumno.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(4).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVcursosAlumno, "Select$" & e.Row.RowIndex).ToString())
        End If

    End Sub


    

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)

        If GVcursosActivosCupo.Rows.Count = 0 Then
            msgInfo.show("No hay cursos con alumnos inscritos.")
        Else
            msgInfo.hide()
        End If

        Dim r As GridViewRow
        For Each r In GVcursosActivosCupo.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVcursosActivosCupo, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        For Each r In GVcursosAlumno.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVcursosAlumno, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub



End Class
