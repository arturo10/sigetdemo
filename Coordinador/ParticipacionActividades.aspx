﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="ParticipacionActividades.aspx.vb"
    Inherits="coordinador_ParticipacionActividades"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style11 {
            height: 37px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style23 {
            text-align: left;
            height: 43px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000099;
        }

        .style24 {
            width: 143px;
            height: 43px;
        }

        .style25 {
            height: 43px;
        }

        .style26 {
            text-align: right;
            height: 18px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style20 {
            width: 273px;
            height: 18px;
            text-align: left;
        }

        .style19 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style21 {
            width: 273px;
        }

        .style31 {
            font-size: x-small;
            color: #000099;
        }

        .style22 {
            text-align: left;
        }

        .style13 {
            width: 143px;
        }

        .style32 {
            font-weight: bold;
            text-align: left;
        }

        .style34 {
            text-align: right;
            height: 18px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
        }

        .style35 {
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style36 {
            font-weight: normal;
            font-size: small;
        }

        .style37 {
            width: 233px;
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Participación en Actividades
    </h1>
    Muestra a los 
	<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
     que realizaron una actividad específica, así como su resultado. 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style23" colspan="3">Indique los siguientes datos 
                para generar el reporte:</td>
            <td class="style24" colspan="2"></td>
            <td class="style25"></td>
        </tr>
        <tr>
            <td class="style19">
                <asp:Label ID="Label4" runat="server" Text="[INSTITUCION]" />
            </td>
            <td class="style20" colspan="2">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    Height="22px" Width="350px" DataSourceID="SDSinstituciones"
                    DataTextField="Descripcion" DataValueField="IdInstitucion">
                </asp:DropDownList>
            </td>
            <td class="style17" colspan="3" rowspan="7">
                <table>
                    <tr>
                        <td>
                            <asp:GridView ID="GVindicadores2" runat="server" 
                                AutoGenerateColumns="False"
                                Caption="Escala de Valores (Indicador) utilizada<br />como mínimo para Acreditación" 

                                DataSourceID="SDSindicadores2" 

                                CssClass="dataGrid_clear"
                                GridLines="None">
                                <Columns>
                                    <asp:BoundField DataField="MinVerde" HeaderText="Mínimo para BIEN"
                                        SortExpression="MinVerde">
                                        <ItemStyle HorizontalAlign="Right" BackColor="LightGreen" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle CssClass="footer" />
                                <PagerStyle CssClass="pager" />
                                <SelectedRowStyle CssClass="selected" />
                                <HeaderStyle CssClass="header" />
                                <AlternatingRowStyle CssClass="altrow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style19">
                <asp:Label ID="Label5" runat="server" Text="[CICLO]" />
            </td>
            <td class="style37" colspan="2">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="duracion"
                    DataValueField="IdCicloEscolar" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style19">
                <asp:Label ID="Label2" runat="server" Text="[PLANTEL]" />
            </td>
            <td class="style37" colspan="2">
                <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                    DataValueField="IdPlantel" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style19">
                <asp:Label ID="Label6" runat="server" Text="[NIVEL]" />
            </td>
            <td class="style37" colspan="2">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style19">
                <asp:Label ID="Label7" runat="server" Text="[GRADO]" />
            </td>
            <td class="style37" colspan="2">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                    Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style19">
                <asp:Label ID="Label9" runat="server" Text="[GRUPO]" />
            </td>
            <td class="style37" colspan="2">
                <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrupos" DataTextField="Descripcion" DataValueField="IdGrupo"
                    Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style19">
                <asp:Label ID="Label8" runat="server" Text="[ASIGNATURA]" />
            </td>
            <td class="style37" colspan="2">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSasignatura" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style19">Calificación</td>
            <td class="style37" colspan="2">
                <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True"
                    DataSourceID="SDScalificaciones" DataTextField="Descripcion"
                    DataValueField="IdCalificacion" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style19">Actividad</td>
            <td class="style37" colspan="2">
                <asp:DropDownList ID="DDLactividad" runat="server" AutoPostBack="True"
                    DataSourceID="SDSactividades" DataTextField="ClaveBateria"
                    DataValueField="IdEvaluacion" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style13">&nbsp;</td>
            <td class="estandar" colspan="2">
                <asp:CheckBox ID="CBfechas" runat="server" AutoPostBack="True"
                    Text="Incluir un rango de fecha de realización"
                    Style="color: #000099" />
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">&nbsp;</td>
            <td class="estandar" colspan="4">
                <asp:Panel ID="PnlFechas" runat="server" Visible="False">
                    <table>
                        <tr>
                            <td class="style24">
                                <span class="style25">Del</span>:</td>
                            <td>
                                <asp:TextBox ID="TBde" runat="server" AutoPostBack="true"></asp:TextBox>
                                <asp:CalendarExtender ID="TBde_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="TBde">
                                </asp:CalendarExtender>
                            </td>
                            <td class="style24">
                                <span class="style25">Al</span>:</td>
                            <td>
                                <asp:TextBox ID="TBal" runat="server" AutoPostBack="true"></asp:TextBox>
                                <asp:CalendarExtender ID="TBal_CalendarExtender" runat="server" Enabled="True"
                                    Format="dd/MM/yyyy" TargetControlID="TBal">
                                </asp:CalendarExtender>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <table class="style10">
        <tr>
            <td colspan="6">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr class="estandar">
            <td colspan="6">
                <asp:GridView ID="GVreporte" runat="server"
                    AllowSorting="True" 
                    AutoGenerateColumns="False"

                    DataSourceID="SDSreporte"
                    DataKeyNames="IdAlumno"
                    Width="883px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="true" SelectText="Ver&nbsp;Reporte&nbsp;Inividual" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="NombreCompleto" HeaderText="Nombre del Alumno" SortExpression="NombreCompleto"/>
                        <asp:BoundField DataField="Equipo" HeaderText="Equipo" SortExpression="Equipo"/>
                        <asp:BoundField DataField="Plantel" HeaderText="Plantel" SortExpression="Plantel"/>
                        <asp:BoundField DataField="Grado" HeaderText="Grado" SortExpression="Grado"/>
                        <asp:BoundField DataField="FechaTermino" HeaderText="Fecha de Término" SortExpression="FechaTermino"/>
                        <asp:BoundField DataField="Resultado" HeaderText="Resultado" SortExpression="Resultado"/>
                        <asp:BoundField DataField="Acreditado" HeaderText="Acreditado" SortExpression="Acreditado"/>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr class="estandar">
            <td colspan="6">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style32" colspan="6">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style22">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td class="style21" colspan="2"></td>
            <td class="style13" colspan="2"></td>
            <td></td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT distinct I.IdInstitucion, I.Descripcion 
FROM Institucion I, CoordinadorPlantel CP, Plantel P
WHERE I.Estatus = 'Activo' and I.IdInstitucion = P.IdInstitucion
and P.IdPlantel = CP.IdPlantel and 
CP.IdCoordinador = (select C.IdCoordinador from Coordinador C, Usuario U where
U.Login = @Login and C.IdUsuario = U.IdUsuario)
ORDER BY I.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdCicloEscolar, Descripcion as  duracion
from CicloEscolar
where Estatus = 'Activo'
order by Descripcion"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT DISTINCT N.IdNivel, N.Descripcion
FROM         Nivel AS N INNER JOIN
                      Escuela AS E ON N.IdNivel = E.IdNivel INNER JOIN
                      CoordinadorPlantel AS CP ON E.IdPlantel = CP.IdPlantel INNER JOIN
                      Coordinador AS C ON CP.IdCoordinador = C.IdCoordinador INNER JOIN
                      Usuario AS U ON C.IdUsuario = U.IdUsuario
WHERE     (U.Login = @Login) and (N.Estatus = 'Activo')
ORDER BY N.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE ([IdNivel] = @IdNivel) 
and (Estatus = 'Activo') ORDER BY [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSasignatura" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdAsignatura], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) 
AND IdAsignatura in (select IdAsignatura from Calificacion where IdCicloEscolar = @IdCicloEscolar)
ORDER BY [IdArea]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdCalificacion], [Descripcion] FROM [Calificacion] WHERE (([IdCicloEscolar] = @IdCicloEscolar) AND ([IdAsignatura] = @IdAsignatura)) ORDER BY [Consecutivo]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSactividades" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdEvaluacion], [ClaveBateria] FROM [Evaluacion] WHERE ([IdCalificacion] = @IdCalificacion) ORDER BY [InicioContestar], [ClaveBateria]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion"
                            PropertyName="SelectedValue" Type="Int64" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSreporte" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="sp_participacionactividades"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter 
                            Name="IdEvaluacion" 
                            ControlID="DDLactividad" 
                            PropertyName="SelectedValue" 
                            Type="Int32" />
                        <asp:ControlParameter 
                            ControlID="DDLgrupo" 
                            Name="IdGrupo"
                            PropertyName="SelectedValue" 
                            Type="Int32" />
                        
                        <asp:SessionParameter Name="De" SessionField="De" Type="String" />
                        <asp:SessionParameter Name="Al" SessionField="Al" Type="String" />

                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrupo], [Descripcion] FROM [Grupo] WHERE (([IdGrado] = @IdGrado) AND ([IdPlantel] = @IdPlantel)) and ([IdCicloEscolar] = @IdCicloEscolar)
ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.IdPlantel, P.Descripcion
from Plantel P, CoordinadorPlantel C
where C.IdCoordinador in (Select IdCoordinador from Coordinador, Usuario
where Coordinador.IdUsuario = Usuario.IdUsuario and Login = @Login)
and P.IdPlantel = C.IdPlantel and P.IdInstitucion = @IdInstitucion 
order by P.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSindicadores2" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT MinRojo, MinAmarillo, MinVerde, MinAzul FROM Indicador, CicloEscolar
where Indicador.IdIndicador = CicloEscolar.IdIndicador and IdCicloEscolar = @IdCicloEscolar">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

