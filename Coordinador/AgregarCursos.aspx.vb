﻿Imports Siget
Imports System.Data.SqlClient


Partial Class coordinador_AgregarCursos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = Trim(User.Identity.Name)
        TitleLiteral.Text = "Agregar Cursos"


        GVcursos.Caption = "<h3>Cursos del Sistema de Educación Continua</h3>"
        BtnActualizar.Visible = False
        BtnCancelar.Visible = False
        BtnAgregar.Visible = True
        DDLEstatus.Visible = False

    End Sub

    Protected Sub BtnAgregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAgregar.Click
        msgInfo.hide()
        msgSuccess.hide()
        msgError.hide()

        Try

            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()

                Dim cmdSelect As SqlCommand = New SqlCommand(" SELECT MAX(IdCurso) AS IdCurso FROM Cursos", conn)
                Dim results As SqlDataReader = cmdSelect.ExecuteReader()
                Dim IdCurso As Integer
                If results.Read() = True Then
                    If Not IsDBNull(results.Item("IdCurso")) Then
                        IdCurso = results.Item("IdCurso") + 1
                    Else
                        IdCurso = 1
                    End If
                End If

                results.Close()
                cmdSelect.Dispose()

                SDScursos.InsertCommand = "SET DATEFORMAT dmy;INSERT INTO Cursos VALUES (@IdCurso,@Nombre,@Tipo,@Cupo,@HorasValidez,@Instructor,@Lugar,@FechaInicio,@FechaFin,@Horario,@Estatus)"
                SDScursos.InsertParameters.Add("IdCurso", IdCurso)
                SDScursos.InsertParameters.Add("Nombre", TBNombre.Text)
                SDScursos.InsertParameters.Add("Tipo", DDLTipo.SelectedValue.ToString())
                SDScursos.InsertParameters.Add("Cupo", TBcupo.Text)
                SDScursos.InsertParameters.Add("HorasValidez", TBhorasValidez.Text)
                SDScursos.InsertParameters.Add("Instructor", TBinstructor.Text)
                SDScursos.InsertParameters.Add("Lugar", TBlugar.Text)
                SDScursos.InsertParameters.Add("FechaInicio", TBFechaInicio.Text)
                SDScursos.InsertParameters.Add("FechaFin", TBFechaFin.Text)
                SDScursos.InsertParameters.Add("Horario", TBhorario.Text)
                SDScursos.InsertParameters.Add("Estatus", "Activo")

                For Each q As Parameter In SDScursos.InsertParameters
                    q.ConvertEmptyStringToNull = False
                Next

                SDScursos.Insert()
                SDScursos.InsertParameters.Clear()

                conn.Close()
            End Using

            TBNombre.Text = ""
            DDLTipo.SelectedValue = "Presencial"
            TBcupo.Text = ""
            TBhorasValidez.Text = ""
            TBinstructor.Text = ""
            TBlugar.Text = ""
            TBFechaInicio.Text = ""
            TBFechaFin.Text = ""
            TBhorario.Text = ""
            DDLEstatus.SelectedValue = "Activo"

            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")))

            GVcursos.DataBind()

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVcursos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVcursos.SelectedIndexChanged

        msgInfo.hide()
        msgSuccess.hide()
        msgError.hide()

        BtnAgregar.Visible = False
        BtnActualizar.Visible = True
        BtnCancelar.Visible = True
        DDLEstatus.Visible = True


        'Se usa el HttpUtility.HtmlDecode() para recuperar correctamente los acentos
        TBNombre.Text = HttpUtility.HtmlDecode(GVcursos.SelectedRow.Cells(0).Text)
        TBcupo.Text = HttpUtility.HtmlDecode(GVcursos.SelectedRow.Cells(1).Text)
        TBhorasValidez.Text = HttpUtility.HtmlDecode(GVcursos.SelectedRow.Cells(2).Text)
        TBinstructor.Text = HttpUtility.HtmlDecode(GVcursos.SelectedRow.Cells(3).Text)
        TBlugar.Text = HttpUtility.HtmlDecode(GVcursos.SelectedRow.Cells(4).Text)
        TBhorario.Text = HttpUtility.HtmlDecode(GVcursos.SelectedRow.Cells(5).Text)
        TBFechaInicio.Text = HttpUtility.HtmlDecode(GVcursos.SelectedRow.Cells(6).Text)
        TBFechaFin.Text = HttpUtility.HtmlDecode(GVcursos.SelectedRow.Cells(7).Text)
        DDLTipo.SelectedValue = HttpUtility.HtmlDecode(GVcursos.SelectedRow.Cells(8).Text)
        DDLEstatus.SelectedValue = HttpUtility.HtmlDecode(GVcursos.SelectedRow.Cells(9).Text)

    End Sub


    Protected Sub BtnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnActualizar.Click

        msgInfo.hide()
        msgSuccess.hide()
        msgError.hide()

        Try

            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()

                Dim IdCurso As Integer = GVcursos.SelectedDataKey.Values(0)

                SDScursos.UpdateCommand = "SET DATEFORMAT dmy;UPDATE Cursos SET Nombre=@Nombre,Tipo=@Tipo,Cupo=@Cupo,HorasValidez=@HorasValidez,Instructor=@Instructor,Lugar=@Lugar,FechaInicio=@FechaInicio,FechaFin=@FechaFin,Horario=@Horario,Estatus=@Estatus WHERE IdCurso=@IdCurso"
                SDScursos.UpdateParameters.Add("Nombre", TBNombre.Text)
                SDScursos.UpdateParameters.Add("Tipo", DDLTipo.SelectedValue.ToString())
                SDScursos.UpdateParameters.Add("Cupo", TBcupo.Text)
                SDScursos.UpdateParameters.Add("HorasValidez", TBhorasValidez.Text)
                SDScursos.UpdateParameters.Add("Instructor", TBinstructor.Text)
                SDScursos.UpdateParameters.Add("Lugar", TBlugar.Text)
                SDScursos.UpdateParameters.Add("FechaInicio", TBFechaInicio.Text)
                SDScursos.UpdateParameters.Add("FechaFin", TBFechaFin.Text)
                SDScursos.UpdateParameters.Add("Horario", TBhorario.Text)
                SDScursos.UpdateParameters.Add("Estatus", DDLEstatus.SelectedValue.ToString())
                SDScursos.UpdateParameters.Add("IdCurso", IdCurso)

                For Each q As Parameter In SDScursos.UpdateParameters
                    q.ConvertEmptyStringToNull = False
                Next

                SDScursos.Update()
                SDScursos.UpdateParameters.Clear()

                conn.Close()
            End Using

            TBNombre.Text = ""
            DDLTipo.SelectedValue = "Presencial"
            TBcupo.Text = ""
            TBhorasValidez.Text = ""
            TBinstructor.Text = ""
            TBlugar.Text = ""
            TBFechaInicio.Text = ""
            TBFechaFin.Text = ""
            TBhorario.Text = ""
            DDLEstatus.SelectedValue = "Activo"

            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")))

            GVcursos.DataBind()

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try

        BtnActualizar.Visible = False
        BtnCancelar.Visible = False
        BtnAgregar.Visible = True
        DDLEstatus.Visible = False

    End Sub

    Protected Sub BtnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancelar.Click

        msgInfo.hide()
        msgSuccess.hide()
        msgError.hide()

        Response.Redirect(Request.RawUrl)

    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVcursos_PreRender(sender As Object, e As EventArgs) Handles GVcursos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVcursos.Rows.Count > 0 Then
            GVcursos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVcursos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVcursos.RowDataBound

        If (DataBinder.Eval(e.Row.DataItem, "Tipo") = "Presencial") And (e.Row.RowIndex > -1) Then
            e.Row.Cells(8).BackColor = Drawing.Color.GreenYellow
        ElseIf (DataBinder.Eval(e.Row.DataItem, "Tipo") = "Virtual") And (e.Row.RowIndex > -1) Then
            e.Row.Cells(8).BackColor = Drawing.Color.LightSkyBlue
        End If

        
        If (DataBinder.Eval(e.Row.DataItem, "Estatus") = "Inactivo") And (e.Row.RowIndex > -1) Then
            e.Row.Cells(9).ForeColor = Drawing.Color.DarkGray
        End If

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(10).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVcursos, "Select$" & e.Row.RowIndex).ToString())
        End If

    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVcursos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVcursos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub



End Class
