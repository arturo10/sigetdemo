﻿Imports Siget

Imports System.IO

Partial Class coordinador_ReactivosPlantel
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
    Utils.Sesion.sesionAbierta()

    Label1.Text = Config.Etiqueta.GRUPOS
    Label2.Text = Config.Etiqueta.PLANTEL
    Label3.Text = Config.Etiqueta.ALUMNO
    Label4.Text = Config.Etiqueta.GRUPOS
    Label5.Text = Config.Etiqueta.PLANTEL
    Label6.Text = Config.Etiqueta.GRADO

    If Not User.Identity.IsAuthenticated Then
      Response.Redirect("~/")
    End If
  End Sub

  Protected Sub GVreporte_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVreporte.DataBound
    GVreporte.Caption = Session("Titulo").ToString
    If GVreporte.Rows.Count > 0 Then
      BtnExportar.Visible = True
    Else
      BtnExportar.Visible = False
    End If

    ' AÑADO PORCENTAJES DE TERMINADAS
    ' para cada fila calcula el porcentaje, y añadelo a la fila
    Try
      For pC As Integer = 0 To GVreporte.Rows.Count() - 1
        Dim divisor As Decimal =
                Decimal.Add(Decimal.Parse(GVreporte.Rows(pC).Cells(4).Text),
                Decimal.Parse(GVreporte.Rows(pC).Cells(5).Text))

        If divisor = 0 Then ' protejo contra posibles resultados vacios
          Continue For
        End If

        GVreporte.Rows(pC).Cells(6).Text =
            Math.Round(Decimal.Multiply(Decimal.Divide(Decimal.Parse(
                GVreporte.Rows(pC).Cells(4).Text),
                divisor
            ), 100), 2, MidpointRounding.AwayFromZero) ' 2 decimales de precisión
      Next
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
    End Try
  End Sub

  Protected Sub GVgrupos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVgrupos.SelectedIndexChanged
    Session("IdGrupo") = GVgrupos.SelectedValue
    'Session("IdEvaluacion") Session("IdAsignatura") ya están creadas 
    Session("Titulo") = GVreporte.Caption + "<BR><font size=""3""> Incluye todos " &
        Config.Etiqueta.ARTDET_ALUMNOS & " " &
 Config.Etiqueta.ALUMNOS & " activos de " &
 Config.Etiqueta.ARTDET_GRUPO & " " &
 Config.Etiqueta.GRUPO & " " + GVgrupos.SelectedRow.Cells(3).Text() + "</font>"
    Response.Redirect("ReactivosGrupos.aspx")
  End Sub

  Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
    ' añado los estilos, por que el gridview no se exporta predefinidamente con css
    GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
    GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
    GVactual.GridLines = GridLines.Both

    Dim headerText As String
    ' quito la imágen de ordenamiento de los encabezados
    For Each tc As TableCell In GVactual.HeaderRow.Cells
      If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
        headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
        tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
        tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
        tc.Text = headerText ' establezco el texto simple
      End If
    Next

    'Este procedimiento convierte el GridView a Excel eliminando la primera y segunda columna
    If GVactual.Rows.Count.ToString + 1 < 65536 Then
      Dim sb As StringBuilder = New StringBuilder()
      Dim sw As StringWriter = New StringWriter(sb)
      Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
      Dim pagina As Page = New Page
      Dim form = New HtmlForm
      pagina.EnableEventValidation = False
      pagina.DesignerInitialize()
      pagina.Controls.Add(form)
      form.Controls.Add(GVactual)
      pagina.RenderControl(htw)
      Response.Clear()
      Response.Buffer = True
      Response.ContentType = "application/vnd.ms-excel"
      Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
      Response.Charset = "UTF-8"
      Response.ContentEncoding = Encoding.Default
      Response.Write(sb.ToString())
      Response.End()
    Else
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
    End If
  End Sub

  Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
    Convierte(GVreporte, "ReactivosPlantel")
  End Sub

  Protected Sub GVreporte_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVreporte.RowDataBound
    If (e.Row.Cells.Count > 1) And (e.Row.RowIndex > -1) Then  'Para que pinte cuando haya mas de una columna
      e.Row.Cells(4).BackColor = Drawing.Color.LightGreen
      e.Row.Cells(5).BackColor = Drawing.Color.LightSalmon

      e.Row.Cells(3).Text = HttpUtility.HtmlDecode(e.Row.Cells(3).Text)

      ' convierto el campo de respuesta de modo que se presente con estilos, no como markup
      e.Row.Cells(3).Text = Regex.Replace(HttpUtility.HtmlDecode(e.Row.Cells(3).Text), "<[^>]*(>|$)", String.Empty)
    End If
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GVreporte_PreRender(sender As Object, e As EventArgs) Handles GVreporte.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVreporte.Rows.Count > 0 Then
      GVreporte.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVreporte_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVreporte.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVreporte.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GVgrupos_PreRender(sender As Object, e As EventArgs) Handles GVgrupos.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVgrupos.Rows.Count > 0 Then
      GVgrupos.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVgrupos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVgrupos.RowDataBound
    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVgrupos, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVgrupos.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVgrupos, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

End Class
