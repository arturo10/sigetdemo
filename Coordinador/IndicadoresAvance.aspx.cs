﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using Siget.Utils;
using System.Text;
using System.IO;
using System.Web.UI.HtmlControls;

public partial class coordinador_IndicadoresAvance : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Siget.Utils.Sesion.sesionAbierta();

        msgError.hide();

        // personaliza las etiquetas de la página
        Literal8.Text = Siget.Config.Etiqueta.ASIGNATURAS;
        Literal2.Text = Siget.Config.Etiqueta.INSTITUCION;
        Literal4.Text = Siget.Config.Etiqueta.CICLO;
        Literal1.Text = Siget.Config.Etiqueta.PLANTEL;
        Literal3.Text = Siget.Config.Etiqueta.GRADO;
        Literal5.Text = Siget.Config.Etiqueta.PLANTELES;
        Literal7.Text = Siget.Config.Etiqueta.PLANTELES;
        Literal41.Text = Siget.Config.Etiqueta.SUBEQUIPO;

        Literal9.Text = Literal16.Text = Literal23.Text = Literal30.Text = 
            Siget.Config.Etiqueta.ARTDET_ALUMNOS;
        Literal10.Text = Literal17.Text = Literal24.Text = Literal31.Text = 
            Siget.Config.Etiqueta.ALUMNOS;
        Literal11.Text = Literal12.Text = Literal18.Text = Literal19.Text = Literal25.Text = Literal29.Text = Literal32.Text = Literal33.Text = Literal38.Text = 
            Siget.Config.Etiqueta.LETRA_ALUMNO;
        Literal13.Text = Literal20.Text = Literal26.Text = Literal34.Text = 
            Siget.Config.Etiqueta.ARTDET_SUBEQUIPO;
        Literal14.Text = Literal21.Text = Literal27.Text = Literal35.Text = 
            Siget.Config.Etiqueta.SUBEQUIPO;
        Literal15.Text = Literal22.Text = Literal28.Text = Literal36.Text = 
            Siget.Config.Etiqueta.LETRA_SUBEQUIPO;
        Literal37.Text = Siget.Config.Etiqueta.ALUMNOS;
        Literal39.Text = Siget.Config.Etiqueta.GRADO;
        Literal40.Text = Siget.Config.Etiqueta.PLANTEL;

        if (GvPlanteles.HeaderRow != null)
            GvPlanteles.HeaderRow.Cells[1].Text = TxtEtiquetaPlanteles.Text;
    }

    protected void DdlPlantel_DataBound(object sender, EventArgs e)
    {
        DdlPlantel.Items.Insert(0, new ListItem(Siget.Lang.FileSystem.General.ddl_elija_un_elemento[Session["Usuario_Idioma"].ToString()], "0"));
        DdlPlantel.Items.Insert(1, new ListItem("Todos", "-1"));
    }

    protected void DdlGrado_DataBound(object sender, EventArgs e)
    {
        DdlGrado.Items.Insert(0, new ListItem(Siget.Lang.FileSystem.General.ddl_elija_un_elemento[Session["Usuario_Idioma"].ToString()], "0"));
        DdlGrado.Items.Insert(1, new ListItem("Todos", "-1"));
    }

    protected void GvPlanteles_SelectedIndexChanged(object sender, EventArgs e)
    {
        SdsIndicadoresGruposP1.SelectParameters["IdPlantel"].DefaultValue = GvPlanteles.SelectedDataKey[0].ToString();
        GvGrupos.DataBind();
    }

    protected void DdlPlantel_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DdlPlantel.SelectedValue == "-1")
        {
            DdlGrado.DataSourceID = SdsGradosTodos.ID;
        }
        else
        {
            DdlGrado.DataSourceID = SdsGradosPorPlantel.ID;
        }
        DdlGrado.DataBind();
    }

    protected void GvPlanteles_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
            e.Row.Cells[1].Text = TxtEtiquetaPlanteles.Text;
    }

    protected void Convierte(GridView GVactual, string archivo, Boolean quitaPrimerColumna)
    {
        
        // añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef");
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Siget.Config.Color.Table_HeaderRow_Background);
        GVactual.GridLines = GridLines.Both;

        if (quitaPrimerColumna)
        {
            // antes de exportar la copia del gridview, en cada fila:
            foreach (GridViewRow r in GVactual.Rows)
                // elimino la columna de "seleccionar"
                r.Cells.RemoveAt(0);
            // ahora quito también la columna de seleccionar, y una del footer para que no se desfase
            GVactual.HeaderRow.Cells.RemoveAt(0);
            GVactual.FooterRow.Cells.RemoveAt(0);
        }

        string headerText;
        // quito la imágen de ordenamiento de los encabezados
        foreach (TableCell tc in GVactual.HeaderRow.Cells)
        {
            if (tc.HasControls()) // evita añadir íconos en celdas que no tengan link
            {
                headerText = ((LinkButton)tc.Controls[0]).Text; // obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0); // quito el ícono de ordenamiento
                //tc.Controls.RemoveAt(0); // quito el linkbutton de ordenamiento
                tc.Text = headerText; // establezco el texto simple
            }
        }

        //if (GVactual.Rows.Count + 1 < 65536)
        //{
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Page pagina = new Page();
            HtmlForm form = new HtmlForm();
            pagina.EnableEventValidation = false;
            pagina.DesignerInitialize();
            pagina.Controls.Add(form);
            form.Controls.Add(GVactual);
            pagina.RenderControl(htw);
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(sb.ToString());
            Response.End();
        //}
        //else
        //    msgError.show(Siget.Lang.FileSystem.General.MSG_ERROR[Session["Usuario_Idioma"].ToString()], "Demasiados registros para Exportar a Excel.");
    }

    protected void BtnExportarPlanteles_Click(object sender, EventArgs e)
    {
        Convierte(GvPlanteles, "Indicadores por Planteles", true);
    }

    protected void BtnExportarGrupos_Click(object sender, EventArgs e)
    {
        Convierte(GvGrupos, "Indicadores por Grupos", false);
    }

    protected void GvPlanteles_DataBound(object sender, EventArgs e)
    {
        if (GvPlanteles.Rows.Count > 0)
            BtnExportarPlanteles.Visible = true;
        else
        {
            BtnExportarPlanteles.Visible = false;
            GvPlanteles.SelectedIndex = -1;
            SdsIndicadoresGruposP1.SelectParameters["IdPlantel"].DefaultValue = DdlPlantel.SelectedIndex.ToString();
            GvGrupos.DataBind();
        }
    }

    protected void GvGrupos_DataBound(object sender, EventArgs e)
    {
        if (GvGrupos.Rows.Count > 0)
            BtnExportarGrupos.Visible = true;
        else
            BtnExportarGrupos.Visible = false;
    }

    protected void DdlSubequipo_DataBound(object sender, EventArgs e)
    {
        DdlSubequipo.Items.Insert(0, new ListItem(Siget.Lang.FileSystem.General.ddl_elija_un_elemento[Session["Usuario_Idioma"].ToString()], "0"));
        DdlSubequipo.Items.Insert(1, new ListItem("Todos", "Todos"));
    }
}