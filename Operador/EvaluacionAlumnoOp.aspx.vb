﻿Imports Siget


Partial Class operador_EvaluacionAlumnoOp
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        TitleLiteral.Text = "Asignar Fechas de Actividades"

        GVevalalumno.Caption = "<h3>Relación de " &
            Config.Etiqueta.ALUMNOS &
            " y Actividades asignadas en " &
            Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO &
            " y " &
            Config.Etiqueta.CICLO &
            " seleccionad" & Config.Etiqueta.LETRA_CICLO & "s</h3>"

        Label1.Text = Config.Etiqueta.ALUMNO
        Label2.Text = Config.Etiqueta.CICLO
        Label3.Text = Config.Etiqueta.NIVEL
        Label4.Text = Config.Etiqueta.GRADO
        Label5.Text = Config.Etiqueta.ASIGNATURA
        Label6.Text = Config.Etiqueta.ALUMNO
        Label7.Text = Config.Etiqueta.INSTITUCION
        Label8.Text = Config.Etiqueta.PLANTEL
        Label9.Text = Config.Etiqueta.GRUPO
        Label10.Text = Config.Etiqueta.ALUMNOS
        Label11.Text = Config.Etiqueta.ALUMNOS
        Label16.Text = Config.Etiqueta.ALUMNOS

        RadioButtonSearchMode.Items(0).Text = Config.Etiqueta.GRUPO
        RadioButtonSearchMode.Items(1).Text = Config.Etiqueta.PLANTEL
        RadioButtonSearchMode.Items(2).Text = Config.Etiqueta.NIVEL
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_CICLO & " " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_GRADO & " " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLcalificacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacion.DataBound
        DDLcalificacion.Items.Insert(0, New ListItem("---Elija una Calificación", 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRUPO & " " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub Agregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Agregar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            Dim I As Integer
            For I = 0 To CBLalumnos.Items.Count - 1
                If CBLalumnos.Items(I).Selected Then
                    SDSevalAlumnoGrupo.InsertCommand = "SET dateformat dmy; INSERT INTO EvaluacionAlumno (IdEvaluacion,IdAlumno,InicioContestar,FinSinPenalizacion,FinContestar,Estatus, FechaModif, Modifico) VALUES (" + GVevaluaciones.SelectedRow.Cells(1).Text + "," + CBLalumnos.Items(I).Value + ",'" + CalInicioContestar.SelectedDate.ToShortDateString + "','" + CalLimiteSinPenalizacion.SelectedDate.ToShortDateString + " 23:59:00','" + CalFinContestar.SelectedDate.ToShortDateString + " 23:59:00','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
                    SDSevalAlumnoGrupo.Insert()
                End If
            Next
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
        CBLalumnos.DataBind()
        GVevalalumno.DataBind()
    End Sub

    Protected Sub GVevalalumno_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevalalumno.SelectedIndexChanged
        msgError.hide()

        Try
            CalInicioContestar.SelectedDate = CDate(GVevalalumno.SelectedRow.Cells(6).Text)
            CalFinContestar.SelectedDate = CDate(GVevalalumno.SelectedRow.Cells(7).Text)
            CalLimiteSinPenalizacion.SelectedDate = CDate(GVevalalumno.SelectedRow.Cells(8).Text)
            'DDLalumno.SelectedValue = CInt(GVevalalumno.SelectedRow.Cells(1).Text) --> MARCARÍA ERROR PORQUE YA NO ESTÁ EN EL DLL PORQUE SE VAN QUITANDO AL AGREGARSE
            DDLestatus.SelectedValue = GVevalalumno.SelectedRow.Cells(10).Text
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVevalalumno_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevalalumno.DataBound
        If GVevalalumno.Rows.Count > 0 Then
            PanelModificar.Visible = True
        Else
            PanelModificar.Visible = False
        End If
    End Sub

    Protected Sub Quitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Quitar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            SDSevalAlumnoGrupo.DeleteCommand = "DELETE FROM EvaluacionAlumno where IdEvaluacion = " + GVevalalumno.SelectedRow.Cells(4).Text + " and IdAlumno = " + GVevalalumno.SelectedRow.Cells(1).Text
            SDSevalAlumnoGrupo.Delete()
            SDSevalAlumnoGrupo.DataBind()
            CBLalumnos.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnActualizar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            SDSevalAlumnoGrupo.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionAlumno SET InicioContestar='" + CalInicioContestar.SelectedDate.ToShortDateString + "', FinSinPenalizacion='" + CalLimiteSinPenalizacion.SelectedDate.ToShortDateString + " 23:59:00', FinContestar='" + CalFinContestar.SelectedDate.ToShortDateString + " 23:59:00', Estatus='" + DDLestatus.SelectedValue + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdEvaluacion = " + GVevalalumno.SelectedRow.Cells(4).Text + " and IdAlumno = " + GVevalalumno.SelectedRow.Cells(1).Text
            SDSevalAlumnoGrupo.Update()
            GVevalalumno.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        Dim I As Integer
        For I = 0 To CBLalumnos.Items.Count - 1
            CBLalumnos.Items(I).Selected = True
        Next
    End Sub

    Protected Sub LinkButton2_Click(sender As Object, e As EventArgs) Handles LinkButton2.Click
        Dim I As Integer
        For I = 0 To CBLalumnos.Items.Count - 1
            CBLalumnos.Items(I).Selected = False
        Next
    End Sub

    Protected Sub CBLalumnos_DataBound(sender As Object, e As EventArgs) Handles CBLalumnos.DataBound
        If CBLalumnos.Items.Count > 0 Then
            LinkButton1.Visible = True
            LinkButton2.Visible = True
        Else
            LinkButton1.Visible = False
            LinkButton2.Visible = False
        End If
    End Sub

    Protected Sub GVevaluaciones_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVevaluaciones.SelectedIndexChanged
        If GVevaluaciones.SelectedIndex <> -1 Then
            PanelAlumnos.Visible = True
        End If
    End Sub

    Protected Sub RadioButtonSearchMode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RadioButtonSearchMode.SelectedIndexChanged
        If RadioButtonSearchMode.SelectedIndex = 0 Then
            PanelGrupo.Visible = True
            PanelPlantel.Visible = False
            PanelNivel.Visible = False
            CBLalumnos.DataSourceID = "SDSalumnosGrupo"
            GVevalalumno.DataSourceID = "SDSevalAlumnoGrupo"
            SDSalumnosGrupo.DataBind()
            SDSevalAlumnoGrupo.DataBind()
        ElseIf RadioButtonSearchMode.SelectedIndex = 1 Then
            PanelGrupo.Visible = False
            PanelPlantel.Visible = True
            PanelNivel.Visible = False
            CBLalumnos.DataSourceID = "SDSalumnosPlantel"
            GVevalalumno.DataSourceID = "SDSevalAlumnoPlantel"
            SDSalumnosPlantel.DataBind()
            SDSevalAlumnoPlantel.DataBind()
        ElseIf RadioButtonSearchMode.SelectedIndex = 2 Then
            PanelGrupo.Visible = False
            PanelPlantel.Visible = False
            PanelNivel.Visible = True
            CBLalumnos.DataSourceID = "SDSalumnosNivel"
            GVevalalumno.DataSourceID = "SDSevalAlumnoNivel"
            SDSalumnosNivel.DataBind()
            SDSevalAlumnoNivel.DataBind()
        End If
    End Sub

    Protected Sub DDLplantel_plantel_DataBound(sender As Object, e As EventArgs) Handles DDLplantel_plantel.DataBound
        DDLplantel_plantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
    End Sub


    Protected Sub DDLnivel_nivel_DataBound(sender As Object, e As EventArgs) Handles DDLnivel_nivel.DataBound
        DDLnivel_nivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVevaluaciones_PreRender(sender As Object, e As EventArgs) Handles GVevaluaciones.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVevaluaciones.Rows.Count > 0 Then
            GVevaluaciones.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVevaluaciones_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVevaluaciones.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GVevaluaciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowDataBound
    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVevaluaciones, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVevaluaciones.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVevaluaciones, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    For Each r In GVevalalumno.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVevalalumno, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GVevalalumno_PreRender(sender As Object, e As EventArgs) Handles GVevalalumno.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVevalalumno.Rows.Count > 0 Then
      GVevalalumno.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVevalalumno_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVevalalumno.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVevalalumno.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

    Protected Sub GVevalalumno_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevalalumno.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVevalalumno, "Select$" & e.Row.RowIndex).ToString())
        End If

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(1)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.ALUMNO

            LnkHeaderText = e.Row.Cells(2).Controls(1)
            LnkHeaderText.Text = "Nombre de " & Config.Etiqueta.ARTDET_ALUMNO & " " & Config.Etiqueta.ALUMNO

            LnkHeaderText = e.Row.Cells(9).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA
        End If
    End Sub

End Class
