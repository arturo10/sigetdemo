﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="operador_Default"
    MaintainScrollPositionOnPostback="true" %>

<asp:content id="Content5" contentplaceholderid="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:content>

<asp:content id="Content1" contentplaceholderid="head" runat="Server">
    <style type='text/css'>#toolBarContainer{display: none;}</style> <!-- la toolbar no es necesaria en páginas principales -->

    <!-- Estilos para los menús de inicio con esquema de lista -->
    <link id="MainMenu_List" rel="stylesheet" media="screen" href="../Resources/css/MainMenu_List.css" />

</asp:content>

<asp:content id="Content4" contentplaceholderid="Scripts_ContentPlaceHolder" runat="Server">
</asp:content>

<asp:content id="Content3" contentplaceholderid="Title_ContentPlaceHolder" runat="Server">
    <h1>Menú de Operadores
    </h1>

</asp:content>

<asp:content id="Content2" contentplaceholderid="Contenedor" runat="Server">
    <div class="centeringContainer">
        <div class="centered">
            <table class="mainMenuTable">
                <tbody>
                    <tr>
                        <td class="lineSeparator">&nbsp;
                        </td>
                    </tr>
                    <tr class="MenuLineA">
                        <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
                        <td class="MenuColumn VerticalIndented">
                            <div class="MenuTitleDiv">
                                <asp:Image ID="Image14" runat="server"
                                    ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/group_users.png" />
                                <br />
                                <asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
                            </div>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink3" runat="server"
                                            NavigateUrl="~/operador/AlumnosOp ">
											<div class="mnItem mnIndent1 mnItemLink">
												Administrar
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink5" runat="server"
                                            NavigateUrl="~/operador/BuscaAlumnoOp ">
											<div class="mnItem mnIndent1 mnItemLink">
												Buscar y Cambiar
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink2" runat="server"
                                            NavigateUrl="~/operador/RepetirActividad ">
											<div class="mnItem mnIndent1 mnItemLink">
												Repetir Actividad
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="MenuLineB">
                        <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
                        <td class="MenuColumn VerticalIndented">
                            <div class="MenuTitleDiv">
                                <asp:Image ID="Image12" runat="server"
                                    ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/date_calendar.png" />
                                <br />
                                Escolar
                            </div>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink39" runat="server"
                                            NavigateUrl="~/operador/EvaluacionAlumnoOp ">
                                            <div class="mnItem mnIndent1 mnItemLink">
                                                Fechas de Actividades
                                                <br /> para 
												<asp:Label ID="Label20" runat="server" Text="[ALUMNOS]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink1" runat="server"
                                            NavigateUrl="~/operador/EvaluacionPlantelOp ">
                                            <div class="mnItem mnIndent1 mnItemLink">
                                                Fechas de Actividades
                                                <br />  para 
												<asp:Label ID="Label19" runat="server" Text="[PLANTELES]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink4" runat="server"
                                            NavigateUrl="~/operador/ApoyoNivelOp ">
											<div class="mnItem mnIndent2 mnItemLink">
											    Apoyo Académico
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:content>

