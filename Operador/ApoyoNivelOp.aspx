﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="ApoyoNivelOp.aspx.vb"
    Inherits="operador_ApoyoNivelOp"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
            height: 450px;
        }

        .style22 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style15 {
            width: 100%;
        }

        .style23 {
        }

        .style25 {
            text-align:left;
        }

        .style28 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style29 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 368px;
        }

        .style30 {
            font-size: x-small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>
        Alta de apoyos académicos para 
        <asp:Label ID="Label5" runat="server" Text="[UN]" /> 
		<asp:Label ID="Label2" runat="server" Text="[NIVEL]" />
    </h1>
    Permite cargar apoyos a la biblioteca de recursos de 
    <asp:Label ID="Label6" runat="server" Text="[UN]" /> 
    <asp:Label ID="Label4" runat="server" Text="[NIVEL]" />
     específico.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td colspan="3">
                
            </td>
        </tr>
        <tr>
            <td style="text-align: right" class="style29">
                <asp:Label ID="Label3" runat="server" Text="[NIVEL]" />
                :</td>
            <td class="style25" colspan="2">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion"
                    DataValueField="IdNivel" Width="297px" CssClass="style22"
                    Height="22px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align: right" class="style29">Consecutivo:</td>
            <td class="style25" colspan="2">
                <asp:TextBox ID="TBconsecutivo" runat="server" MaxLength="3" Width="48px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right" class="style29">Descripción del Material:</td>
            <td class="style25" colspan="2">
                <asp:TextBox ID="TBdescripcion" runat="server" MaxLength="150" Width="297px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right" class="style29">Tipo de archivo:</td>
            <td class="style25" colspan="2">
                <asp:DropDownList ID="DDLtipoarchivo" runat="server" Width="100px"
                    AutoPostBack="True">
                    <asp:ListItem>PDF</asp:ListItem>
                    <asp:ListItem>ZIP</asp:ListItem>
                    <asp:ListItem>Word</asp:ListItem>
                    <asp:ListItem>Excel</asp:ListItem>
                    <asp:ListItem Value="PP">Power Point</asp:ListItem>
                    <asp:ListItem>Imagen</asp:ListItem>
                    <asp:ListItem Value="Video"></asp:ListItem>
                    <asp:ListItem>Audio</asp:ListItem>
                    <asp:ListItem Value="Link"></asp:ListItem>
                    <asp:ListItem>FLV</asp:ListItem>
                    <asp:ListItem>SWF</asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="Label1" runat="server"
                    Style="color: #000099; font-size: x-small; font-weight: 700; font-family: Arial, Helvetica, sans-serif;"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: right" class="style29">Cargar material de apoyo:<br />
                <span class="style30">*al insertar se carga el archivo</span></td>
            <td class="style25">
                <asp:AsyncFileUpload ID="RutaArchivo" runat="server" CssClass="imageUploaderField"/>
                <asp:TextBox ID="TBlink" runat="server" MaxLength="150" Visible="False"
                    Width="231px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: right" class="style29">Estatus:</td>
            <td class="style25" colspan="2">
                <asp:DropDownList ID="DDLestatus" runat="server" Width="87px">
                    <asp:ListItem Value="Activo"></asp:ListItem>
                    <asp:ListItem Value="Suspendido">Suspendido</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align: right" class="style29">&nbsp;</td>
            <td class="style23" colspan="2">
                <asp:Button ID="Insertar" runat="server" Text="Insertar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
                <asp:Button ID="Eliminar" runat="server" Text="Eliminar"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                &nbsp;
                <asp:Button ID="Actualizar" runat="server" Text="Actualizar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" style="font-size: x-small; text-align: left;">
                NOTA: Actualizar no modifica el archivo cargado.</td>
        </tr>
        <tr>
            <td colspan="3">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table class="style15">
                    <tr>
                        <td colspan="5">
                            <asp:GridView ID="GVapoyosacademicos" runat="server" 
                                AllowPaging="True"
                                AllowSorting="True" 
                                AutoGenerateColumns="False" 
                                PageSize="20"
                                Caption="<h3>Apoyos académicos capturados</h3>"
                                
                                DataSourceID="SDSApoyoNivel" 
                                DataKeyNames="IdApoyoNivel"
                                Width="882px" 

                                CssClass="dataGrid_clear_selectable"
                                GridLines="None">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                                    <asp:BoundField DataField="IdApoyoNivel" HeaderText="IdApoyo"
                                        SortExpression="IdApoyoNivel" />
                                    <asp:BoundField DataField="IdNivel" HeaderText="IdNivel"
                                        SortExpression="IdNivel" 
                                        HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                                    <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                                        SortExpression="Consecutivo" />
                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripción"
                                        SortExpression="Descripcion" />
                                    <asp:BoundField DataField="ArchivoApoyo" HeaderText="Archivo de Apoyo"
                                        SortExpression="ArchivoApoyo" />
                                    <asp:BoundField DataField="TipoArchivoApoyo" HeaderText="Tipo"
                                        SortExpression="TipoArchivoApoyo" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle CssClass="footer" />
                                <PagerStyle CssClass="pager" />
                                <SelectedRowStyle CssClass="selected" />
                                <HeaderStyle CssClass="header" />
                                <AlternatingRowStyle CssClass="altrow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%-- Niveles de los planteles de un Operador, en base a su IdOperador --%>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSniveles" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT DISTINCT N.IdNivel, N.Descripcion FROM Nivel N, Escuela E, Plantel P, OperadorPlantel OP WHERE N.IdNivel = E.IdNivel AND P.IdPlantel = E.IdPlantel AND P.IdPlantel = OP.IdPlantel AND OP.IdOperador = @IdOperador ORDER BY N.Descripcion">
                                <SelectParameters>
                                    <asp:SessionParameter Name="IdOperador" Type="Int32" SessionField="Usuario_IdOperador" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSApoyoNivel" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [ApoyoNivel] WHERE ([IdNivel] = @IdNivel) ORDER BY [Consecutivo]">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

