﻿Imports Siget

Imports System.IO

Partial Class operador_ApoyoNivelOp
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        TitleLiteral.Text = "Cargar a Biblioteca de Recursos"

        Label2.Text = Config.Etiqueta.NIVEL
        Label4.Text = Config.Etiqueta.NIVEL
        Label5.Text = Config.Etiqueta.ARTIND_NIVEL
        Label6.Text = Config.Etiqueta.ARTIND_NIVEL
    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        msgError.hide()
        msgSuccess.hide()

        If Trim(TBconsecutivo.Text) = "" Then TBconsecutivo.Text = "0"
        Try
            Dim archivocargado As String

            If RutaArchivo.HasFile Or (Trim(TBlink.Text).Length > 0) Then 'El atributo .HasFile compara si se indico un archivo           
                If DDLtipoarchivo.SelectedValue = "Link" Then
                    archivocargado = Trim(TBlink.Text)
                Else
                    archivocargado = RutaArchivo.FileName
                End If
                SDSApoyoNivel.InsertCommand = "SET dateformat dmy; INSERT INTO ApoyoNivel (IdNivel,Consecutivo,Descripcion,ArchivoApoyo,TipoArchivoApoyo,Estatus, FechaModif, Modifico) VALUES (" + _
                DDLnivel.SelectedValue + "," + TBconsecutivo.Text + ",'" + TBdescripcion.Text + "','" + archivocargado + "','" + DDLtipoarchivo.SelectedValue + "','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"

                'Ahora CARGO el archivo solo si NO es un link
                If DDLtipoarchivo.SelectedValue <> "Link" Then
                    Dim ruta As String
          ruta = Config.Global.rutaApoyo

          Dim MiArchivo2 As FileInfo = New FileInfo(ruta & RutaArchivo.FileName)
          If MiArchivo2.Exists Then
            'Avisa que ya existe, NO LO SUBE pero sí almacena el registro
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "YA EXISTE UN ARCHIVO CON ESE NOMBRE EN EL ALMACEN DE ARCHIVOS, CAMBIE EL NOMBRE DE SU ARCHIVO E INTENTE CARGARLO NUEVAMENTE, POR FAVOR.")
          Else
            RutaArchivo.SaveAs(ruta & RutaArchivo.FileName)
            SDSApoyoNivel.Insert()
            msgSuccess.show("Éxito", "El registro ha sido guardado.")
            GVapoyosacademicos.DataBind()
            Label1.Text = "Archivo cargado: " & archivocargado
          End If
        Else
          SDSApoyoNivel.Insert()
          msgSuccess.show("Éxito", "El registro ha sido guardado.")
          GVapoyosacademicos.DataBind()
          Label1.Text = "Archivo cargado: " & archivocargado
        End If
      Else 'Significa que NO indicó archivo para cargar
        msgError.show("No ha indicado ningún archivo o link")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
    DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
  End Sub

  Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
    msgSuccess.hide()
    msgError.hide()

    Try
      SDSApoyoNivel.DeleteCommand = "Delete from ApoyoNivel where IdApoyoNivel = " + GVapoyosacademicos.SelectedRow.Cells(1).Text
      SDSApoyoNivel.Delete()

      'Elimino el archivo de apoyo que tenga la opción
      If Trim(HttpUtility.HtmlDecode(GVapoyosacademicos.SelectedRow.Cells(5).Text)).Length > 1 Then   'Significa que tiene un valor en ese campo
        Dim ruta As String
        ruta = Config.Global.rutaApoyo

        'Borro el archivo que estaba anteriormente porque se pondrá uno nuevo 
        Dim MiArchivo As FileInfo = New FileInfo(ruta & Trim(HttpUtility.HtmlDecode(GVapoyosacademicos.SelectedRow.Cells(5).Text)))
        MiArchivo.Delete()
      End If
      GVapoyosacademicos.DataBind() 'Dejo esta instrucción al final porque si se refresca el GridView antes de borrar el archivo de apoyo, ya no podré saber su nombre
      msgSuccess.show("Éxito", "El registro ha sido eliminado, al igual que su archivo de apoyo si lo tenía.")
      Label1.Text = ""

    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
    msgError.hide()
    msgSuccess.hide()

    'ESTE CODIGO NO CONVIENE UTILIZARLO
    Try
      SDSApoyoNivel.UpdateCommand = "SET dateformat dmy; UPDATE ApoyoNivel SET Consecutivo = " &
          TBconsecutivo.Text &
          ", Descripcion = '" + TBdescripcion.Text +
          "', Estatus = '" + DDLestatus.SelectedValue +
          "', IdNivel = " & DDLnivel.SelectedValue &
          ", TipoArchivoApoyo = '" & DDLtipoarchivo.SelectedValue & "' , FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' " &
          " WHERE IdApoyoNivel =" + GVapoyosacademicos.SelectedRow.Cells(1).Text
      'Para eliminar la referencia al archivo de apoyo, agregaría al SET: ArchivoApoyo=NULL, TipoArchivoApoyo=NULL,

      SDSApoyoNivel.Update()
      msgSuccess.show("Éxito", "El registro ha sido actualizado.")
      GVapoyosacademicos.DataBind()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub GVapoyosacademicos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVapoyosacademicos.SelectedIndexChanged
    If Trim(GVapoyosacademicos.SelectedRow.Cells(6).Text) = "Link" Then
      RutaArchivo.Visible = False
      TBlink.Visible = True
      TBlink.Text = GVapoyosacademicos.SelectedRow.Cells(5).Text
      Label1.Text = ""
    Else
      RutaArchivo.Visible = True
      TBlink.Visible = False
      Label1.Text = Trim(HttpUtility.HtmlDecode(GVapoyosacademicos.SelectedRow.Cells(5).Text))
      TBlink.Text = ""
    End If

    'La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
    TBconsecutivo.Text = GVapoyosacademicos.SelectedRow.Cells(3).Text
    TBdescripcion.Text = HttpUtility.HtmlDecode(Trim(GVapoyosacademicos.SelectedRow.Cells(4).Text))
    DDLtipoarchivo.SelectedValue = HttpUtility.HtmlDecode(Trim(GVapoyosacademicos.SelectedRow.Cells(6).Text))
    DDLestatus.SelectedValue = HttpUtility.HtmlDecode(Trim(GVapoyosacademicos.SelectedRow.Cells(7).Text))
    DDLnivel.SelectedIndex = DDLnivel.Items.IndexOf(DDLnivel.Items.FindByValue(GVapoyosacademicos.SelectedRow.Cells(2).Text))
    DDLtipoarchivo.SelectedIndex = DDLtipoarchivo.Items.IndexOf(DDLtipoarchivo.Items.FindByText(GVapoyosacademicos.SelectedRow.Cells(6).Text))
  End Sub

  Protected Sub DDLtipoarchivo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLtipoarchivo.SelectedIndexChanged
    If DDLtipoarchivo.SelectedValue = "Link" Then
      RutaArchivo.Visible = False
      TBlink.Visible = True
    Else
      RutaArchivo.Visible = True
      TBlink.Text = ""
      TBlink.Visible = False
    End If
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GVapoyosacademicos_PreRender(sender As Object, e As EventArgs) Handles GVapoyosacademicos.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVapoyosacademicos.Rows.Count > 0 Then
      GVapoyosacademicos.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVapoyosacademicos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVapoyosacademicos.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVapoyosacademicos.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

    Protected Sub GVapoyosacademicos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVapoyosacademicos.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVapoyosacademicos, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVapoyosacademicos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVapoyosacademicos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub


End Class
