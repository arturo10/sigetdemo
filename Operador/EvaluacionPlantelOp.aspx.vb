﻿Imports Siget


Partial Class operador_EvaluacionPlantelOp
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        TitleLiteral.Text = "Asignar Fechas de Actividades"

        Label1.Text = Config.Etiqueta.PLANTELES
        Label2.Text = Config.Etiqueta.CICLO
        Label3.Text = Config.Etiqueta.NIVEL
        Label4.Text = Config.Etiqueta.GRADO
        Label5.Text = Config.Etiqueta.ASIGNATURA
        Label6.Text = Config.Etiqueta.PLANTELES
        Label7.Text = Config.Etiqueta.INSTITUCION
        Label8.Text = Config.Etiqueta.PLANTEL
        Label9.Text = Config.Etiqueta.PLANTELES

        GVevalplantel.Caption = "<h3>Relación de " &
            Config.Etiqueta.PLANTELES &
            " y Actividades asignadas en " &
            Config.Etiqueta.ARTDET_CICLO & " " & Config.Etiqueta.CICLO &
            " seleccionad" & Config.Etiqueta.LETRA_CICLO & "</h3>"
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_CICLO & " " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_GRADO & " " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLcalificacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacion.DataBound
        DDLcalificacion.Items.Insert(0, New ListItem("---Elija una Calificación", 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub Agregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Agregar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            SDSevalPlantel.InsertCommand = "SET dateformat dmy; INSERT INTO EvaluacionPlantel (IdEvaluacion,IdPlantel,InicioContestar,FinSinPenalizacion,FinContestar,Estatus, FechaModif, Modifico) VALUES (" + GVevaluaciones.SelectedRow.Cells(1).Text + "," + DDLplantel.SelectedValue + ",'" + CalInicioContestar.SelectedDate.ToShortDateString + "','" + CalLimiteSinPenalizacion.SelectedDate.ToShortDateString + " 23:59:00','" + CalFinContestar.SelectedDate.ToShortDateString + " 23:59:00','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
            SDSevalPlantel.Insert()
            GVevalplantel.DataBind()
            DDLplantel.DataBind()
            msgSuccess.show("Éxito", "La actividad se asignó correctamente.")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Asegúrese de haber seleccionado una calificación, fechas y plantel. <br />" & ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Quitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Quitar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            SDSevalPlantel.DeleteCommand = "DELETE FROM EvaluacionPlantel where IdEvaluacion = " + GVevalplantel.SelectedRow.Cells(3).Text + " and IdPlantel = " + GVevalplantel.SelectedRow.Cells(1).Text
            SDSevalPlantel.Delete()
            GVevalplantel.DataBind()
            DDLplantel.DataBind()
            msgSuccess.show("Éxito", "La asignación se eliminó correctamente.")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVevalplantel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevalplantel.SelectedIndexChanged
        msgError.hide()

        Try
            CalInicioContestar.SelectedDate = CDate(GVevalplantel.SelectedRow.Cells(5).Text)
            CalFinContestar.SelectedDate = CDate(GVevalplantel.SelectedRow.Cells(6).Text)
            CalLimiteSinPenalizacion.SelectedDate = CDate(GVevalplantel.SelectedRow.Cells(7).Text)
            'DDLplantel.SelectedValue = CInt(GVevalplantel.SelectedRow.Cells(1).Text) --> MARCARÍA ERROR PORQUE YA NO ESTÁ EN EL DLL PORQUE SE VAN QUITANDO AL AGREGARSE
            DDLestatus.SelectedValue = GVevalplantel.SelectedRow.Cells(9).Text
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            'ESTE CODIGO NO LO USE PORQUE AL SELECCIONAR NO EXISTIRIA EN EL DDL EL ID YA QUE SE VAN QUITANDO
            SDSevalPlantel.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionPlantel SET Estatus = '" + DDLestatus.SelectedValue + "', InicioContestar = '" + CalInicioContestar.SelectedDate.ToShortDateString + "', FinSinPenalizacion='" + CalLimiteSinPenalizacion.SelectedDate.ToShortDateString + " 23:59:00', FinContestar='" + CalFinContestar.SelectedDate.ToShortDateString + " 23:59:00', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdEvaluacion = " + GVevalplantel.SelectedRow.Cells(3).Text + _
                                           " and IdPlantel = " + GVevalplantel.SelectedRow.Cells(1).Text
            SDSevalPlantel.Update()
            GVevalplantel.DataBind()
            'No puede modificarse el plantel para que luego no se vayan a hacer bolas
            'DDLplantel.SelectedValue = CInt(GVevalplantel.SelectedRow.Cells(1).Text)
            msgSuccess.show("Éxito", "La asignación se actializó correctamente.")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVevalplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevalplantel.DataBound
        If GVevalplantel.Rows.Count > 0 Then
            PanelModificar.Visible = True
        Else
            PanelModificar.Visible = False
        End If
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVevaluaciones_PreRender(sender As Object, e As EventArgs) Handles GVevaluaciones.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVevaluaciones.Rows.Count > 0 Then
            GVevaluaciones.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVevaluaciones_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVevaluaciones.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GVevaluaciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowDataBound
    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVevaluaciones, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVevaluaciones.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVevaluaciones, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    For Each r In GVevalplantel.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVevalplantel, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GVevalplantel_PreRender(sender As Object, e As EventArgs) Handles GVevalplantel.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVevalplantel.Rows.Count > 0 Then
      GVevalplantel.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVevalplantel_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVevalplantel.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVevalplantel.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

    Protected Sub GVevalplantel_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevalplantel.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVevalplantel, "Select$" & e.Row.RowIndex).ToString())
        End If

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(1)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.PLANTEL

            LnkHeaderText = e.Row.Cells(2).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.PLANTEL

            LnkHeaderText = e.Row.Cells(8).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA
        End If
    End Sub

End Class
