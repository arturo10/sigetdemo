﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="EvaluacionPlantelOp.aspx.vb" 
    Inherits="operador_EvaluacionPlantelOp"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            width: 100%;
        }

        .style13 {
            text-align: left;
        }

        .style14 {
            width: 133px;
            text-align: right;
        }

        .style15 {
            text-align: right;
            height: 16px;
        }

        .style18 {
            height: 16px;
            text-align: left;
            font-weight: bold;
        }

        .style25 {
            height: 16px;
            text-align: center;
            width: 226px;
        }

        .style26 {
            height: 16px;
            text-align: center;
            width: 466px;
        }

        .style24 {
            height: 16px;
            text-align: center;
        }

        .style21 {
            font-size: x-small;
        }

        .style23 {
            height: 16px;
            text-align: left;
            font-weight: bold;
            width: 226px;
        }

        .style27 {
            height: 16px;
            text-align: left;
            font-weight: bold;
            width: 466px;
        }

        .style28 {
            height: 32px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            text-align: left;
        }

        .style29 {
            width: 127px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style30 {
            width: 127px;
            text-align: right;
            height: 16px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style32 {
            height: 16px;
            text-align: center;
            width: 226px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style33 {
            text-align: left;
            font-weight: bold;
            color: #000066;
            height: 24px;
        }

        .style41 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style42 {
            height: 16px;
            text-align: left;
            font-weight: bold;
            color: #000066;
        }

        .style43 {
            text-align: right;
            height: 20px;
        }

        .style44 {
            height: 20px;
            text-align: left;
        }

        .style17 {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Asignar fechas específicas de Actividades a 
								<asp:Label ID="Label1" runat="server" Text="[PLANTELES]" />
    </h1>
    Permite asignar a 
    <asp:Label ID="Label9" runat="server" Text="[PLANTELES]" />
     una fecha especial para realizar una actividad, 
    diferente a la establecida en la actividad misma.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style11">
        <tr>
            <td>
                <table class="estandar">
                    <tr>
                        <td class="style29" style="text-align: right;">
                            <asp:Label ID="Label2" runat="server" Text="[CICLO]" />
                        </td>
                        <td colspan="6" style="text-align: left">
                            <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                                DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                                DataValueField="IdCicloEscolar" Width="322px" Height="22px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14" colspan="5">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style29" style="text-align: right;">
                            <asp:Label ID="Label3" runat="server" Text="[NIVEL]" />
                        </td>
                        <td colspan="6" style="text-align: left">
                            <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                                Height="22px" Width="322px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14" colspan="5">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style29" style="text-align: right;">
                            <asp:Label ID="Label4" runat="server" Text="[GRADO]" />
                        </td>
                        <td colspan="6" style="text-align: left">
                            <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                                Height="22px" Width="322px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14" colspan="5">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style29" style="text-align: right;">
                            <asp:Label ID="Label5" runat="server" Text="[ASIGNATURA]" />
                        </td>
                        <td colspan="12" style="text-align: left">
                            <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                                DataSourceID="SDSasignaturas" DataTextField="Materia"
                                DataValueField="IdAsignatura" Height="22px" Width="650px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style29" style="text-align: right;">Calificación</td>
                        <td colspan="12" style="text-align: left">
                            <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True"
                                DataSourceID="SDScalificaciones" DataTextField="Cal"
                                DataValueField="IdCalificacion" Height="22px" Width="650px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style13">&nbsp;</td>
                        <td colspan="12">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style13">&nbsp;</td>
                        <td colspan="12">
                            <asp:GridView ID="GVevaluaciones" runat="server" 
                                AllowPaging="True"
                                AllowSorting="True" 
                                AutoGenerateColumns="False" 
                                Caption="<h3>Actividades creadas:</h3>"
                                
                                DataKeyNames="IdEvaluacion" 
                                DataSourceID="SDSevaluaciones"

                                CssClass="dataGrid_clear_selectable"
                                GridLines="None">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                                    <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                                        InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion"
                                        ItemStyle-CssClass="hiddenContainer" 
                                        HeaderStyle-CssClass="hiddenContainer" />
                                    <asp:BoundField DataField="IdCalificacion" HeaderText="Id Calificación"
                                        SortExpression="IdCalificacion"
                                        ItemStyle-CssClass="hiddenContainer"
                                        HeaderStyle-CssClass="hiddenContainer" />
                                    <asp:BoundField DataField="ClaveBateria" HeaderText="Nombre de la Actividad"
                                        SortExpression="ClaveBateria">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle Width="200px" />
                                    </asp:BoundField>
                                    <%--<asp:BoundField DataField="Resultado" HeaderText="Resultado"
                                        SortExpression="Resultado" />--%>
                                    <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                                        SortExpression="Porcentaje">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderText="Inicia" SortExpression="InicioContestar" />
                                    <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderText="Termina" SortExpression="FinContestar" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle CssClass="footer" />
                                <PagerStyle CssClass="pager" />
                                <SelectedRowStyle CssClass="selected" />
                                <HeaderStyle CssClass="header" />
                                <AlternatingRowStyle CssClass="altrow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="style13">&nbsp;</td>
                        <td colspan="12">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style42" style="background: #cccccc;" colspan="13">Ahora elija los 
														<asp:Label ID="Label6" runat="server" Text="[PLANTELES]" />
                            que tendrán fechas diferentes a las definidas para la 
                            Actividad</td>
                    </tr>
                    <tr>
                        <td class="style29" style="text-align: right;">
                            <asp:Label ID="Label7" runat="server" Text="[INSTITUCION]" /></td>
                        <td colspan="12" style="text-align: left">
                            <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                DataValueField="IdInstitucion" Height="22px" Width="500px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style30"  style="text-align: right;">
                            <asp:Label ID="Label8" runat="server" Text="[PLANTEL]" /></td>
                        <td class="style18" colspan="12">
                            <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                DataValueField="IdPlantel" Height="22px" Width="500px">
                                <asp:ListItem Value="Todos">Todos</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style30"  style="text-align: right;">Estatus</td>
                        <td class="style18" colspan="12">
                            <asp:DropDownList ID="DDLestatus" runat="server" Width="108px"
                                CssClass="style41">
                                <asp:ListItem>Sin iniciar</asp:ListItem>
                                <asp:ListItem>Iniciada</asp:ListItem>
                                <asp:ListItem>Terminada</asp:ListItem>
                                <asp:ListItem>Cancelada</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">
                            <asp:HyperLink ID="HyperLink1" runat="server"
                                NavigateUrl="~/"
                                CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                            </asp:HyperLink>
                        </td>
                        <td class="style25">&nbsp;</td>
                        <td class="style26">&nbsp;</td>
                        <td class="style32">
                            <b>Inicio</b> de 
                                Aplicación<br />
                            de la Actividad</td>
                        <td class="style25">&nbsp;</td>
                        <td class="style24">
                            <b>Fin</b> de
                                Aplicación<br />
                            de la Actividad<b><br class="style21" />
                                <span class="style21">(después de esta fecha ya no<br />
                                    se podrá realizar la actividad)</span></b></td>
                        <td class="style24" colspan="2">&nbsp;</td>
                        <td class="style24">Fecha <b>máxima</b> en que se<br />
                            debe realizar la Actividad<br />
                            para evitar penalización</td>
                        <td class="style24">&nbsp;</td>
                        <td class="style24">&nbsp;</td>
                        <td class="style24" colspan="2"></td>
                    </tr>
                    <tr>
                        <td class="style15">&nbsp;</td>
                        <td class="style23">&nbsp;</td>
                        <td class="style27">&nbsp;</td>
                        <td class="style23">
                            <asp:Calendar ID="CalInicioContestar" runat="server" BackColor="White"
                                BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest"
                                Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px"
                                Style="font-family: Arial, Helvetica, sans-serif; font-size: xx-small"
                                Width="200px">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#808080" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>
                        </td>
                        <td class="style23">&nbsp;</td>
                        <td class="style18">
                            <asp:Calendar ID="CalFinContestar" runat="server" BackColor="White"
                                BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest"
                                Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px"
                                Style="font-family: Arial, Helvetica, sans-serif; font-size: xx-small"
                                Width="200px">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#808080" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>
                        </td>
                        <td class="style18" colspan="2">&nbsp;</td>
                        <td class="style18">
                            <asp:Calendar ID="CalLimiteSinPenalizacion" runat="server" BackColor="White"
                                BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest"
                                Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px"
                                Style="font-family: Arial, Helvetica, sans-serif; font-size: xx-small"
                                Width="200px">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#808080" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>
                        </td>
                        <td class="style18">&nbsp;</td>
                        <td class="style18">&nbsp;</td>
                        <td class="style18" colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style15"></td>
                        <td class="style16" colspan="6" style="text-align: right">
                            <%-- selecciona solo las asignaturas de los planteles pertenientes al coordinador --%>
                            <asp:SqlDataSource ID="SDSevalPlantel" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select DISTINCT EP.IdPlantel, P.Descripcion as Plantel, EP.IdEvaluacion, E.ClaveBateria, 
EP.InicioContestar, EP.FinContestar, EP.FinSinPenalizacion, A.Descripcion as Asignatura, EP.Estatus, C.IdAsignatura, E.InicioContestar
from Plantel P, Evaluacion E, EvaluacionPlantel EP, Calificacion C, Asignatura A, OperadorPlantel OP 
where C.IdCicloEscolar = @IdCicloEscolar and E.IdCalificacion = C.IdCalificacion and P.IdPlantel = OP.IdPlantel and OP.IdOperador = @IdOperador 
and A.IdAsignatura = C.IdAsignatura and
P.IdPlantel = EP.IdPlantel and E.IdEvaluacion = EP.IdEvaluacion
and  P.IdInstitucion = @IdInstitucion
order by P.Descripcion, C.IdAsignatura, E.InicioContestar">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                                        PropertyName="SelectedValue" />
                                    <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                                        PropertyName="SelectedValue" />
                                    <asp:SessionParameter Name="IdOperador" Type="Int32" SessionField="Usuario_IdOperador" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td class="style17" colspan="5">
                            <asp:Button ID="Agregar" runat="server" Style="text-align: center"
                                Text="Agregar" CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                        </td>
                        <td class="style16">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="13">
                            <uc1:msgSuccess runat="server" ID="msgSuccess" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="13">
                            <uc1:msgError runat="server" ID="msgError" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style15" colspan="13">
                            <asp:GridView ID="GVevalplantel" runat="server" 
                                AllowPaging="True"
                                AllowSorting="True" 
                                AutoGenerateColumns="False" 
                                Caption="<h3>Relación de PLANTELES y Actividades asignadas en el CICLO seleccionado</h3>"
                                PageSize="20"

                                DataKeyNames="IdPlantel,IdEvaluacion" 
                                DataSourceID="SDSevalPlantel"
                                Width="882px"

                                CssClass="dataGrid_clear_selectable"
                                GridLines="None">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                                    <asp:BoundField DataField="IdPlantel" HeaderText="Id_[Plantel]" ReadOnly="True"
                                        SortExpression="IdPlantel"
                                        ItemStyle-CssClass="hiddenContainer"
                                        HeaderStyle-CssClass="hiddenContainer">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Plantel" HeaderText="[Plantel]"
                                        SortExpression="Plantel">
                                        <HeaderStyle Width="190px" />
                                        <ItemStyle Width="190px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                                        ReadOnly="True" SortExpression="IdEvaluacion" 
                                        ItemStyle-CssClass="hiddenContainer"
                                        HeaderStyle-CssClass="hiddenContainer"/>
                                    <asp:BoundField DataField="ClaveBateria" HeaderText="Actividad"
                                        SortExpression="ClaveBateria">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle Width="200px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="InicioContestar" HeaderText="Inicia Actividad"
                                        SortExpression="InicioContestar" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="FinContestar" HeaderText="Finaliza Actividad"
                                        SortExpression="FinContestar" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="FinSinPenalizacion" HeaderText="Fecha Máxima"
                                        SortExpression="FinSinPenalizacion" DataFormatString="{0:dd/MM/yyyy}" />
                                    <asp:BoundField DataField="Asignatura" HeaderText="[Asignatura]"
                                        SortExpression="Asignatura" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle CssClass="footer" />
                                <PagerStyle CssClass="pager" />
                                <SelectedRowStyle CssClass="selected" />
                                <HeaderStyle CssClass="header" />
                                <AlternatingRowStyle CssClass="altrow" />
                            </asp:GridView>
                            <%-- sort YES - rowdatabound
	                            0  select
	                            1  id_PLANTEL
	                            2  PLANTEL
	                            3  idactividad
	                            4  actividad
	                            5  inicia actividad
	                            6  fin actividad
	                            7  fecha maxima
	                            8  ASIGNATURA
	                            9  estatus
	                            --%>
                        </td>
                    </tr>
                        <tr>
                            <td colspan="13">
                                <asp:Panel ID="PanelModificar" runat="server" Visible="false">
                                    <table style="width: 100%">
                                        <tr>
                                            <td class="style16" style="text-align: right; font-size:x-small;">
                                                NOTA: Actualizar sólo modifica las fechas y el estatus.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style16" style="text-align: right;">
                                                <asp:Button ID="Quitar" runat="server" Text="Quitar"
                                                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                                                &nbsp;
                                                <asp:Button ID="Actualizar" runat="server" Text="Actualizar"
                                                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    <tr>
                        <td class="style15">&nbsp;</td>
                        <td class="style16" colspan="3">
                            <%-- Instituciones de los planteles de un Operador, en base a su IdOperador --%>
                            <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT DISTINCT I.* FROM Institucion I, Plantel P, OperadorPlantel OP WHERE P.IdInstitucion = I.IdInstitucion AND P.IdPlantel = OP.IdPlantel AND OP.IdOperador = @IdOperador ORDER BY I.Descripcion">
                                <SelectParameters>
                                    <asp:SessionParameter Name="IdOperador" Type="Int32" SessionField="Usuario_IdOperador" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td class="style16" colspan="3">
                            <%-- Planteles de un Operador, en base a su IdOperador y la institución, nivel, y evaluación seleccionados --%>
                            <asp:SqlDataSource ID="SDSplanteles" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT DISTINCT P.IdPlantel, P.IdLicencia, P.IdInstitucion, P.Descripcion 
FROM Plantel P, Escuela E, OperadorPlantel OP
WHERE P.IdInstitucion = @IdInstitucion and E.IdPlantel = P.IdPlantel and E.IdNivel = @IdNivel and P.IdPlantel = OP.IdPlantel and OP.IdOperador= @IdOperador
and P.IdPlantel not in (select IdPlantel from EvaluacionPlantel where IdEvaluacion = @IdEvaluacion)
ORDER BY Descripcion">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                                        PropertyName="SelectedValue" Type="Int32" />
                                    <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel" PropertyName="SelectedValue" />
                                    <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                                        PropertyName="SelectedValue" />
                                    <asp:SessionParameter Name="IdOperador" Type="Int32" SessionField="Usuario_IdOperador" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td class="style16" colspan="3">
                            <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Evaluacion] WHERE ([IdCalificacion] = @IdCalificacion)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion"
                                        PropertyName="SelectedValue" Type="Int64" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td class="style16" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style15">&nbsp;</td>
                        <td class="style16" colspan="3">
                            <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT C.IdCalificacion, C.Descripcion + ' (' + A.Descripcion + ')' as Cal
FROM Calificacion C, Asignatura A
WHERE C.IdCicloEscolar = @IdCicloEscolar
and A.IdAsignatura = C.IdAsignatura and A.IdAsignatura = @IdAsignatura
order by C.Consecutivo">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                                        PropertyName="SelectedValue" Type="Int32" />
                                    <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                                        PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td class="style16" colspan="3">
                            <asp:SqlDataSource ID="SDSgrados" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT IdGrado,Descripcion FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td class="style16" colspan="3">
                            <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT A.IdAsignatura, A.Descripcion + ' (' + Ar.Descripcion + ')'  as Materia
FROM Asignatura A, Area Ar
WHERE (A.IdGrado = @IdGrado) and (Ar.IdArea = A.IdArea)
ORDER BY Ar.Descripcion,A.Descripcion">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td class="style16" colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style15">&nbsp;</td>
                        <td class="style16" colspan="3">
                            <%-- Ciclos Escolares de los grupos de los planteles de un Operador, en base a su IdOperador --%>
                            <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT DISTINCT C.* FROM CicloEscolar C, Grupo G, Plantel P, OperadorPlantel OP WHERE (C.Estatus = 'Activo') AND C.IdCicloEscolar = G.IdCicloEscolar AND G.IdPlantel = P.IdPlantel AND P.IdPlantel = OP.IdPlantel AND OP.IdOperador = @IdOperador ORDER BY C.FechaInicio">
                                <SelectParameters>
                                    <asp:SessionParameter Name="IdOperador" Type="Int32" SessionField="Usuario_IdOperador" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td class="style16" colspan="3">
                            <%-- Niveles de los planteles de un Operador, en base a su IdOperador --%>
                            <asp:SqlDataSource ID="SDSniveles" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT DISTINCT N.IdNivel, N.Descripcion FROM Nivel N, Escuela E, Plantel P, OperadorPlantel OP WHERE N.IdNivel = E.IdNivel AND P.IdPlantel = E.IdPlantel AND P.IdPlantel = OP.IdPlantel AND OP.IdOperador = @IdOperador ORDER BY N.Descripcion">
                                <SelectParameters>
                                    <asp:SessionParameter Name="IdOperador" Type="Int32" SessionField="Usuario_IdOperador" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td class="style16" colspan="3">&nbsp;</td>
                        <td class="style16" colspan="3">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; text-align: left;">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
    
    <table class="dataSources">
        <tr>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

