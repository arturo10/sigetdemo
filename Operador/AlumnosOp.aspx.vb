﻿Imports Siget

Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Partial Class operador_AlumnosOp
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        TitleLiteral.Text = "Administración de " & Config.Etiqueta.ALUMNOS

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.INSTITUCION
        Label3.Text = Config.Etiqueta.PLANTEL
        Label4.Text = Config.Etiqueta.NIVEL
        Label5.Text = Config.Etiqueta.GRADO
        Label6.Text = Config.Etiqueta.CICLO
        Label7.Text = Config.Etiqueta.GRUPO
        Label8.Text = Config.Etiqueta.ALUMNO
        Label9.Text = Config.Etiqueta.EQUIPO
        Label10.Text = Config.Etiqueta.SUBEQUIPO
        Label11.Text = Config.Etiqueta.ALUMNO
        Label12.Text = Config.Etiqueta.ALUMNOS
        Label13.Text = Config.Etiqueta.GRUPO
        Label14.Text = Config.Etiqueta.ALUMNO
        Label15.Text = Config.Etiqueta.ARTIND_ALUMNO
        Label16.Text = Config.Etiqueta.ARTDET_GRUPO
        Label21.Text = Config.Etiqueta.ARTDET_GRUPO
        Label17.Text = Config.Etiqueta.LETRA_ALUMNO
        Label18.Text = Config.Etiqueta.ARTIND_GRUPO
        Label19.Text = Config.Etiqueta.GRUPO
        Label20.Text = Config.Etiqueta.LETRA_GRUPO

        'AQUI HAGO CICLO PRIMERO PARA LIMPIAR DDL Y LUEGO PARA LLENARLOS CON LOS VALORES DE EQUIPO Y SUBEQUIPO
        If (DDLequipo.Items.Count <= 0) And (DDLsubequipo.Items.Count <= 0) And (DDLinstitucion.SelectedIndex > 0) Then
            'Limpio por si las dudas
            DDLequipo.Items.Clear()
            DDLsubequipo.Items.Clear()

            Dim strConexion As String
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objConexion As New SqlConnection(strConexion)
            Dim miComando As SqlCommand
            Dim misRegistros As SqlDataReader
            miComando = objConexion.CreateCommand
            'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
            miComando.CommandText = "select * from Equipo where IdInstitucion = " + DDLinstitucion.SelectedValue.ToString + " order by Descripcion desc"
            Try
                objConexion.Open()
                'CARGO LOS EQUIPOS:
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                Do While misRegistros.Read()
                    DDLequipo.Items.Insert(0, misRegistros.Item("Descripcion"))
                Loop
                DDLequipo.Items.Insert(0, "")
                'AHORA CARGO LOS SUBEQUIPOS:
                misRegistros.Close()
                miComando.CommandText = "select * from Subequipo where IdInstitucion = " + DDLinstitucion.SelectedValue.ToString + " order by Descripcion desc"
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                Do While misRegistros.Read()
                    DDLsubequipo.Items.Insert(0, misRegistros.Item("Descripcion"))
                Loop
                DDLsubequipo.Items.Insert(0, "")
                misRegistros.Close()
                objConexion.Close()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try
        End If
        'DDLequipo.ClearSelection()
        'DDLsubequipo.ClearSelection()
    End Sub

    Public Function GetErrorMessage(ByVal status As MembershipCreateStatus) As String
        Select Case status
            Case MembershipCreateStatus.DuplicateUserName
                Return "El usuario ya existe, elija otro usuario."
            Case MembershipCreateStatus.DuplicateEmail
                Return "Ya existe un usuario para ese email, por favor escriba un email diferente,"
            Case MembershipCreateStatus.InvalidPassword
                Return "El password es inválido, pruebe con otro."
            Case MembershipCreateStatus.InvalidEmail
                Return "El formato del email es inválido, por favor revíselo."
            Case MembershipCreateStatus.InvalidAnswer
                Return "La respuesta secreta es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidQuestion
                Return "La pregunta de recuperación de password es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidUserName
                Return "El nombre de usuario es inválido, corríjalo."
            Case MembershipCreateStatus.ProviderError
                Return "La autentificación falló, contacte al administrador del sistema."
            Case MembershipCreateStatus.UserRejected
                Return "La creación del usuario ha sido cancelada, consulte al administrador."
            Case Else
                Return "Ha ocurrido un error desconocido, verifique o contacte al administrador."
        End Select
    End Function

    Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click
        TBnombre.Text = ""
        TBApePaterno.Text = ""
        TBApeMaterno.Text = ""
        TBmatricula.Text = ""
        DDLequipo.Text = ""
        DDLsubequipo.Text = ""
        TBemail.Text = ""
        TBingreso.Text = ""
        DDLestatus.ClearSelection()
        TBlogin.Text = ""
        TBpassword.Text = ""
    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        msgError.hide()
        msgSuccess.hide()

        If Not TBnombre.Text <> "" Then
            msgError.show("El estudiante debe tener un nombre.")
            Return
        ElseIf Not TBlogin.Text <> "" Then
            msgError.show("El estudiante debe tener un login.")
            Return
        ElseIf TBpassword.Text.Length < 4 Then
            msgError.show("La contraseña debe ser por lo menos de 4 caracteres.")
            Return
        End If
        Try
            If Trim(TBlogin.Text).Length > 0 Then
                'ESTE ALGORITMO NO AFECTA LAS TABLAS DE SEGURIDAD DE USUARIOS
                'Inserto el login y password (el email que aqui pongo puede o no coincidir con el que pondrá el alumno
                'al momento de registrarse
                SDSusuarios.InsertCommand = "SET dateformat dmy; INSERT INTO Usuario(Login,Password,PerfilASP,Email, FechaModif, Modifico) VALUES('" + Trim(TBlogin.Text) + "','" + Trim(TBpassword.Text) + "','Alumno','" + Trim(TBemail.Text) + "', getdate(), '" & User.Identity.Name & "')"
                SDSusuarios.Insert()

                'Inserto el Alumno pero primero obtengo el IdUsuario recién ingresado
                Dim strConexion As String
                'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                Dim objConexion As New SqlConnection(strConexion)
                Dim miComando As SqlCommand
                Dim misRegistros As SqlDataReader
                objConexion.Open()
                miComando = objConexion.CreateCommand
                miComando.CommandText = "SELECT * FROM USUARIO WHERE Login = '" + Trim(TBlogin.Text) + "'"
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read()
                'Obtengo el ID del Usuario recien creado para usarlo en la tabla de ALUMNO
                Dim IdUsuario = misRegistros.Item("IdUsuario").ToString
                misRegistros.Close()
                objConexion.Close()

                'NO ME PERMITIO ESTA SUBCONSULTA: SDSalumnos.InsertCommand = "SET dateformat dmy; INSERT INTO Alumno(IdUsuario,IdGrupo,IdPlantel,Nombre,ApePaterno,ApeMaterno,Matricula,Equipo,Subequipo,Estatus) VALUES ((select IdUsuario from Usuario where Login = '" + Trim(TBlogin.Text) + "')," + DDLgrupo.SelectedValue + "," + DDLplantel.SelectedValue + ",'" + Trim(TBnombre.Text) + "','" + Trim(TBApePaterno.Text) + "','" + Trim(TBApeMaterno.Text) + "','" + Trim(TBmatricula.Text) + "','" + DDLequipo.SelectedItem.ToString + "','" + "','" + DDLsubequipo.SelectedItem.ToString + DDLestatus.SelectedValue + "')"
                'OJO: El campo FECHAINGRESO lo utilizo para registrar el momento en que el alumno se registra en las tablas de seguridad
                If CBregistrarme.Checked Then 'Aqui registro al alumno en la base de datos de seguridad
                    'Como se registgrará en las tablas de seguridad, le pongo FechaIngreso que la estoy tomando como la fecha de registro
                    SDSalumnos.InsertCommand = "SET dateformat dmy; INSERT INTO Alumno(IdUsuario,IdGrupo,IdPlantel,Nombre,ApePaterno,ApeMaterno,Matricula,Email,Equipo,Subequipo,Estatus,FechaIngreso, FechaModif, Modifico) VALUES (" + IdUsuario + "," + DDLgrupo.SelectedValue + "," + DDLplantel.SelectedValue + ",'" + Trim(TBnombre.Text) + "','" + Trim(TBApePaterno.Text) + "','" + Trim(TBApeMaterno.Text) + "','" + Trim(TBmatricula.Text) + "','" + Trim(TBemail.Text) + "','" + DDLequipo.SelectedItem.ToString + "','" + DDLsubequipo.SelectedItem.ToString + "','" + DDLestatus.SelectedValue + "',getdate(), getdate(), '" & User.Identity.Name & "')"
                    SDSalumnos.Insert()

                    'Ahora Creo el Usuario
                    Dim status As MembershipCreateStatus

                    ' 17/10/2013 se deseleccionó requiresQuestionAndAnswer="false"  de web config, no es necesario
                    'Dim passwordQuestion As String = ""
                    'Dim passwordAnswer As String = ""
                    'If Membership.RequiresQuestionAndAnswer Then
                    '    passwordQuestion = "Empresa desarrolladora de " & Config.Etiqueta.SISTEMA_CORTO
                    '    passwordAnswer = "Integrant"
                    'End If

                    Try
                        Dim newUser As MembershipUser = Membership.CreateUser(Trim(TBlogin.Text), Trim(TBpassword.Text), _
                                                                       Trim(TBemail.Text), Nothing, _
                                                                       Nothing, True, status)
                        If newUser Is Nothing Then
                            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), GetErrorMessage(status))
                        Else
                            Roles.AddUserToRole(Trim(TBlogin.Text), "Alumno")
                        End If
                    Catch ex As MembershipCreateUserException
                        Utils.LogManager.ExceptionLog_InsertEntry(ex)
                        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
                    End Try
                    msgSuccess.show("Éxito", "El " & Config.Etiqueta.ALUMNO &
                        " ha sido creado.<BR>La cuenta de " &
                        Config.Etiqueta.ARTDET_ALUMNO & " " & Config.Etiqueta.ALUMNO &
                        " ya ha sido registrada en la base de datos de seguridad.")
                Else
                    SDSalumnos.InsertCommand = "SET dateformat dmy; INSERT INTO Alumno(IdUsuario,IdGrupo,IdPlantel,Nombre,ApePaterno,ApeMaterno,Matricula,Email,Equipo,Subequipo,Estatus,FechaModif, Modifico) VALUES (" + IdUsuario + "," + DDLgrupo.SelectedValue + "," + DDLplantel.SelectedValue + ",'" + Trim(TBnombre.Text) + "','" + Trim(TBApePaterno.Text) + "','" + Trim(TBApeMaterno.Text) + "','" + Trim(TBmatricula.Text) + "','" + Trim(TBemail.Text) + "','" + DDLequipo.SelectedItem.ToString + "','" + DDLsubequipo.SelectedItem.ToString + "','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
                    SDSalumnos.Insert()
                    msgSuccess.show("'Exito", Config.Etiqueta.ARTDET_ALUMNO & " " & Config.Etiqueta.ALUMNO &
                        " ha sido cread" & Config.Etiqueta.LETRA_ALUMNO & ".")
                End If
                GVAlumnos.DataBind()
            Else
                msgError.show("Debe proporcionar un nombre de usuario (login) para " &
                    Config.Etiqueta.ARTDET_ALUMNO & " " & Config.Etiqueta.ALUMNO & ".")
                TBlogin.Focus()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem("---Elija la " & Config.Etiqueta.INSTITUCION, 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub GVAlumnos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVAlumnos.SelectedIndexChanged
        'La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
        TBnombre.Text = HttpUtility.HtmlDecode(Trim(GVAlumnos.SelectedRow.Cells(4).Text))
        TBApePaterno.Text = HttpUtility.HtmlDecode(Trim(GVAlumnos.SelectedRow.Cells(2).Text))
        TBApeMaterno.Text = HttpUtility.HtmlDecode(Trim(GVAlumnos.SelectedRow.Cells(3).Text))
        If GVAlumnos.SelectedRow.Cells(5).Text = "&nbsp;" Then
            TBmatricula.Text = ""
        Else
            TBmatricula.Text = HttpUtility.HtmlDecode(Trim(GVAlumnos.SelectedRow.Cells(5).Text))
        End If
        TBemail.Text = HttpUtility.HtmlDecode(Trim(GVAlumnos.SelectedRow.Cells(6).Text))
        TBingreso.Text = HttpUtility.HtmlDecode(Trim(GVAlumnos.SelectedRow.Cells(7).Text))
        DDLestatus.SelectedValue = Trim(GVAlumnos.SelectedRow.Cells(10).Text)
        'OJO: Si se graba un " " (espacio vacío) en el campo, marca error al seleccionar
        If Trim(GVAlumnos.SelectedRow.Cells(11).Text) = "&nbsp;" Then
            DDLequipo.SelectedValue = ""
        Else
            DDLequipo.SelectedValue = HttpUtility.HtmlDecode(Trim(GVAlumnos.SelectedRow.Cells(11).Text))
        End If
        If Trim(GVAlumnos.SelectedRow.Cells(12).Text) = "&nbsp;" Then
            DDLsubequipo.SelectedValue = ""
        Else
            DDLsubequipo.SelectedValue = Trim(GVAlumnos.SelectedRow.Cells(12).Text)
        End If

        If Trim(GVAlumnos.SelectedRow.Cells(8).Text) = "&nbsp;" Then
            TBlogin.Text = ""
        Else
            TBlogin.Text = HttpUtility.HtmlDecode(GVAlumnos.SelectedRow.Cells(8).Text)
        End If

        If Trim(GVAlumnos.SelectedRow.Cells(9).Text) = "&nbsp;" Then
            TBpassword.Text = ""
        Else
            TBpassword.Text = HttpUtility.HtmlDecode(GVAlumnos.SelectedRow.Cells(9).Text)
        End If

    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        msgSuccess.hide()
        msgError.hide()

        Try
            If Not TBnombre.Text <> "" Then
                msgError.show("El estudiante debe tener un nombre.")
                Return
            ElseIf Not TBlogin.Text <> "" Then
                msgError.show("El estudiante debe tener un Nombre de Usuario.")
                Return
            ElseIf TBpassword.Text.Length < 4 Then
                msgError.show("La contraseña debe ser por lo menos de 4 caracteres.")
                Return
            End If

            'Actualizo password en tabla de seguridad, no puedo cambiar Login
            Dim u As MembershipUser
            u = Membership.GetUser(Trim(HttpUtility.HtmlDecode(GVAlumnos.SelectedRow.Cells(8).Text))) 'Obtiene datos del usuario con el LOGIN
            'u.ChangePassword(Trim(HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(15).Text)), Trim(TBpasswordOK.Text)) 'cambia PASSWORD
            u.ChangePassword(u.ResetPassword(), Trim(TBpassword.Text)) 'cambia PASSWORD

            SDSalumnos.UpdateCommand = "SET dateformat dmy; UPDATE Usuario SET Password = '" & TBpassword.Text.Trim() & "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdUsuario = (SELECT IdUsuario FROM Alumno WHERE IdAlumno = " & GVAlumnos.SelectedRow.Cells(1).Text & ")"
            SDSalumnos.Update()

            If Trim(TBingreso.Text) <> "" Then
                SDSalumnos.UpdateCommand = "SET dateformat dmy; UPDATE Alumno SET Nombre = '" + TBnombre.Text + "',ApePaterno = '" + _
                        TBApePaterno.Text + "',ApeMaterno = '" + TBApeMaterno.Text + "',Matricula = '" + _
                        TBmatricula.Text + "',Equipo = '" + DDLequipo.SelectedItem.ToString + "',Subequipo = '" + DDLsubequipo.SelectedItem.ToString + "',Email = '" + TBemail.Text + "',FechaIngreso = '" + _
                        TBingreso.Text + "',Estatus = '" + DDLestatus.SelectedValue.ToString + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' " + _
                        "where IdAlumno = " + GVAlumnos.SelectedRow.Cells(1).Text
            Else
                'Marca error si el campo FechaIngreso se ingresa en vacío
                SDSalumnos.UpdateCommand = "SET dateformat dmy; UPDATE Alumno SET Nombre = '" + TBnombre.Text + "',ApePaterno = '" + _
                        TBApePaterno.Text + "',ApeMaterno = '" + TBApeMaterno.Text + "',Matricula = '" + TBmatricula.Text + _
                        "',Equipo = '" + DDLequipo.SelectedItem.ToString + "',Subequipo = '" + DDLsubequipo.SelectedItem.ToString + "',Email = '" + TBemail.Text + "',FechaIngreso = NULL, Estatus = '" + DDLestatus.SelectedValue.ToString + _
                        "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' " + "where IdAlumno = " + GVAlumnos.SelectedRow.Cells(1).Text
            End If
            SDSalumnos.Update()

            msgSuccess.show("Éxito", "El registro ha sido actualizado.")
            GVAlumnos.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLgrupo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.SelectedIndexChanged
        'LLAMAR A PROCEDIMIENTO: Limpiar_Click
        TBnombre.Text = ""
        TBApePaterno.Text = ""
        TBApeMaterno.Text = ""
        TBmatricula.Text = ""
        TBemail.Text = ""
        TBingreso.Text = ""
        DDLestatus.ClearSelection()
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            'Algoritmo que oculta toda la columna donde aparece el "Seleccionar"
            Dim Cont As Integer
            GVactual.HeaderRow.Cells.Item(0).Visible = False
            For Cont = 0 To GVactual.Rows.Count - 1
                GVactual.Rows(Cont).Cells.Item(0).Visible = False
            Next
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel")
        End If
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVAlumnos, "Grupo-" + HttpUtility.HtmlDecode(DDLgrado.SelectedItem.ToString) + "-" + HttpUtility.HtmlDecode(DDLgrupo.SelectedItem.ToString))
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVAlumnos_PreRender(sender As Object, e As EventArgs) Handles GVAlumnos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVAlumnos.Rows.Count > 0 Then
            GVAlumnos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVAlumnos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVAlumnos.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If GVAlumnos.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub GVAlumnos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVAlumnos.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVAlumnos, "Select$" & e.Row.RowIndex).ToString())
        End If

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(1)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.ALUMNO

            LnkHeaderText = e.Row.Cells(11).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.EQUIPO

            LnkHeaderText = e.Row.Cells(12).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.SUBEQUIPO

            LnkHeaderText = e.Row.Cells(5).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.MATRICULA
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVAlumnos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVAlumnos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub


End Class
