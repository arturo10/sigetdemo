﻿Imports Siget


Partial Class operador_Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' esto debe ir antes que cualquier operación que utilice base de datos
        If Utils.Bloqueo.sistemaBloqueado() Then
            Utils.Sesion.CierraSesion()
            Response.Redirect("~/EnMantenimiento.aspx")
        End If

        TitleLiteral.Text = "Menú Principal"

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label19.Text = Config.Etiqueta.PLANTELES
        Label20.Text = Config.Etiqueta.ALUMNOS
    End Sub

End Class
