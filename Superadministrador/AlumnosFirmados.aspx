﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="AlumnosFirmados.aspx.vb"
    Inherits="superadministrador_AlumnosFirmados"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style11 {
            text-align: left;
        }

        .style13 {
            width: 328px;
            text-align: right;
        }

        .style14 {
            text-align: left;
            width: 328px;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
        }

        .style25 {
            width: 328px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
        }

        .style32 {
            text-align: right;
        }

        .style37 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Reporte de 
								<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
        registrados y no registrados de una
        <asp:Label ID="Label4" runat="server" Text="[INSTITUCION]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style37">
                <asp:Label ID="Label2" runat="server" Text="[INSTITUCION]" />
            </td>
            <td style="text-align: left">
                <asp:DropDownList ID="DDLinstitucion" runat="server" Width="350px"
                    DataSourceID="SDSinstitucion" DataTextField="Descripcion"
                    DataValueField="IdInstitucion" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style37">
                <asp:Label ID="Label3" runat="server" Text="[ALUMNOS]" />
                a desplegar</td>
            <td style="text-align: left">
                <asp:DropDownList ID="DDLalumnos" runat="server" Height="22px" Width="244px"
                    AutoPostBack="True">
                    <asp:ListItem Value="IS NOT NULL">Registrados</asp:ListItem>
                    <asp:ListItem Value="IS NULL">Sin registrarse</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style13">&nbsp;</td>
            <td>
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td class="style13">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style11" colspan="2">
                <asp:GridView ID="GVsalida" runat="server"
                    AutoGenerateColumns="false"
                    PageSize="100"

                    DataSourceID="SDSreporte"
                    Width="722px"

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Matricula" HeaderText="Matricula" />
                        <asp:BoundField DataField="ApePaterno" HeaderText="ApePaterno" />
                        <asp:BoundField DataField="ApeMaterno" HeaderText="ApeMaterno" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                        <asp:BoundField DataField="Grupo" HeaderText="Grupo" />
                        <asp:BoundField DataField="Plantel" HeaderText="Plantel" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort NO - prerender
	                0  A.Matricula
	                1  A.ApePaterno
	                2  A.ApeMaterno
	                3  A.Nombre
	                4  G.Descripcion GRUPO
	                5  P.Descripcion PLANTEL
                --%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style11" colspan="2">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSinstitucion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdInstitucion, Descripcion, Estatus
from Institucion
where Estatus = 'Activo'
order by Descripcion"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSreporte" runat="server"></asp:SqlDataSource>

            </td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>

