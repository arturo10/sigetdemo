﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="CapturaResultados.aspx.vb" 
    Inherits="superadministrador_CapturaResultados" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">




        .style11
        {
            width: 100%;
        }
        .style37
        {
            height: 36px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000000;
        }
        .style23
        {
            height: 22px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
        .style24
        {
            width: 127px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            color: #000000;
        }
        .style14
        {
            width: 133px;
            text-align: right;
        }
        .style13
        {
            text-align: right;
        }
        .style15
        {
            text-align: right;
            height: 16px;
        }
        .style38
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }
        </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Capturar resultados (de actividades).
		</h1>
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style11">
        <tr>
            <td colspan="8">
                <table class="estandar">
                    <tr>
                        <td class="style24">
                                <asp:Label ID="Label1" runat="server" Text="[CICLO]" /></td>
                        <td>
                            <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True" 
                                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion" 
                                    DataValueField="IdCicloEscolar" Width="322px" Height="22px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style24">
                                <asp:Label ID="Label2" runat="server" Text="[NIVEL]" /></td>
                        <td>
                            <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True" 
                                DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel" 
                                Height="22px" Width="322px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style24">
                                <asp:Label ID="Label3" runat="server" Text="[GRADO]" /></td>
                        <td>
                            <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True" 
                                DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado" 
                                Height="22px" Width="322px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style24">
                                <asp:Label ID="Label4" runat="server" Text="[ASIGNATURA]" /></td>
                        <td colspan="3" style="text-align: left;">
                            <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True" 
                                DataSourceID="SDSasignaturas" DataTextField="Materia" 
                                DataValueField="IdAsignatura" Height="22px" Width="650px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style24">
                            Calificaciones</td>
                        <td colspan="3" style="text-align: left;">
                            <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True" 
                                DataSourceID="SDScalificaciones" DataTextField="Cal" 
                                DataValueField="IdCalificacion" Height="22px" Width="650px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style13" colspan="4">
                            <asp:GridView ID="GVevaluaciones" runat="server" AllowPaging="True" 
                                AllowSorting="True" AutoGenerateColumns="False" BackColor="White" 
                                BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" 
                                DataKeyNames="IdEvaluacion" DataSourceID="SDSevaluaciones" 
                                GridLines="Vertical" style="font-size: x-small; text-align: left;" 
                                
                                Caption="<h3>Seleccione la Actividad a la que le capturará el resultado</h3>" 
                                ForeColor="Black" Width="873px">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad" 
                                        InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion" />
                                    <asp:BoundField DataField="IdCalificacion" HeaderText="Id Calificacion" 
                                        SortExpression="IdCalificacion" />
                                    <asp:BoundField DataField="ClaveBateria" HeaderText="Nombre de la Actividad" 
                                        SortExpression="ClaveBateria" />
                                    <%--<asp:BoundField DataField="Resultado" HeaderText="Resultado" 
                                        SortExpression="Resultado" />--%>
                                    <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación" 
                                        SortExpression="Porcentaje" >
                                    <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}" 
                                        HeaderText="Inicia" SortExpression="InicioContestar" />
                                    <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}" 
                                        HeaderText="Termina" SortExpression="FinContestar" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus" 
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">
                                &nbsp;</td>
                        <td class="style16" colspan="2">
                            &nbsp;</td>
                        <td class="style16">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style15">
                            <asp:Label ID="Label5" runat="server" Text="[INSTITUCION]" /></td>
                        <td class="style16" colspan="2">
                            <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True" 
                                    DataSourceID="SDSinstituciones" DataTextField="Descripcion" 
                                    DataValueField="IdInstitucion" Height="22px" Width="500px">
                            </asp:DropDownList>
                        </td>
                        <td class="style16">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style15">
                            <asp:Label ID="Label6" runat="server" Text="[PLANTEL]" /></td>
                        <td class="style16" colspan="2">
                            <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True" 
                                DataSourceID="SDSplanteles" DataTextField="Descripcion" 
                                DataValueField="IdPlantel" Height="22px" Width="500px">
                            </asp:DropDownList>
                        </td>
                        <td class="style16">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style15">
                            <asp:Label ID="Label7" runat="server" Text="[GRUPO]" /></td>
                        <td class="style16" colspan="2">
                <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True" 
                    DataSourceID="SDSgrupos" DataTextField="Descripcion" DataValueField="IdGrupo" 
                    Width="500px">
                </asp:DropDownList>
                        </td>
                        <td class="style16">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style15">
                            &nbsp;</td>
                        <td class="style16" colspan="2">
                            &nbsp;</td>
                        <td class="style16">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style15" colspan="4">
                            <asp:GridView ID="GVAlumnos" runat="server" 
                                AllowSorting="True" 
                                AutoGenerateColumns="False" 
                                Caption="<h3>Seleccione al ALUMNO</h3>"
                                DataKeyNames="IdAlumno" 
                                DataSourceID="SDSlistado" 

                                CellPadding="3" 
                                ForeColor="Black" 
                                GridLines="Vertical" 
                                style="text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: x-small" 
                                Width="871px" 
                                BackColor="White" 
                                BorderColor="#999999" 
                                BorderStyle="Solid" 
                                BorderWidth="1px" >
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno" 
                                        InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno" />
                                    <asp:BoundField DataField="ApePaterno" HeaderText="Ap. Paterno" 
                                        SortExpression="ApePaterno" />
                                    <asp:BoundField DataField="ApeMaterno" HeaderText="Ap. Materno" 
                                        SortExpression="ApeMaterno" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                                        SortExpression="Nombre" />
                                    <asp:BoundField DataField="Matricula" HeaderText="Matrícula" 
                                        SortExpression="Matricula" >
                                    <ItemStyle Width="85px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                    <asp:BoundField DataField="FechaIngreso" DataFormatString="{0:dd/MM/yyyy}" 
                                        HeaderText="Ingreso" SortExpression="FechaIngreso" />
                                    <asp:BoundField DataField="Login" HeaderText="Login" SortExpression="Login" />
                                    <asp:BoundField DataField="Password" HeaderText="Password" 
                                        SortExpression="Password" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus" 
                                        SortExpression="Estatus" />
                                    <asp:BoundField DataField="Equipo" HeaderText="Equipo" SortExpression="Equipo" />
                                    <asp:BoundField DataField="Subequipo" HeaderText="Subequipo" 
                                        SortExpression="Subequipo" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                            <%-- sort YES - rowdatabound
	                            0  select
	                            1  Id_ALUMNO
	                            2  ap paterno
	                            3  apmaterno
	                            4  nombre
	                            5  matricula
	                            6  email
	                            7  fechaingreso
	                            8  login
	                            9  password
	                            10 estatus
                                11 EQUIPO
                                12 SUBEQUIPO
	                            --%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style38">
                Capture la calificación (sobre 100):</td>
            <td colspan="2" style="text-align: left;">
                <asp:TextBox ID="TBresultado" runat="server" Width="70px"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:Button ID="BtnAsignar" runat="server" Text="Asignar" 
										CssClass="defaultBtn btnThemeBlue btnThemeMedium"/>
            </td>
            <td colspan="2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="8">
                            <asp:GridView ID="GVresultados" runat="server" AllowSorting="True" 
                                AutoGenerateColumns="False" CellPadding="3" DataKeyNames="IdEvaluacionT" 
                                DataSourceID="SDSresultados" ForeColor="Black" GridLines="Vertical" 
                                style="text-align: left; font-family: Arial, Helvetica, sans-serif; font-size: x-small" 
                                Width="871px" BackColor="White" BorderColor="#999999" BorderStyle="Solid" 
                                BorderWidth="1px" 
																Caption="<h3>Resultado registrado</h3>">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="IdEvaluacionT" HeaderText="IdEvaluacionT" 
                                        InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacionT" />
                                    <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno" 
                                        SortExpression="IdAlumno" />
                                    <asp:BoundField DataField="IdGrado" HeaderText="IdGrado" 
                                        SortExpression="IdGrado" Visible="False" />
                                    <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo" 
                                        SortExpression="IdGrupo" />
                                    <asp:BoundField DataField="IdCicloEscolar" HeaderText="IdCicloEscolar" 
                                        SortExpression="IdCicloEscolar" />
                                    <asp:BoundField DataField="IdEvaluacion" HeaderText="IdActividad" 
                                        SortExpression="IdEvaluacion" />
                                    <asp:BoundField DataField="TotalReactivos" HeaderText="TotalReactivos" 
                                        SortExpression="TotalReactivos" Visible="False" />
                                    <asp:BoundField DataField="ReactivosContestados" 
                                        HeaderText="ReactivosContestados" SortExpression="ReactivosContestados" 
                                        Visible="False" />
                                    <asp:BoundField DataField="PuntosPosibles" HeaderText="PuntosPosibles" 
                                        SortExpression="PuntosPosibles" Visible="False" />
                                    <asp:BoundField DataField="PuntosAcertados" HeaderText="PuntosAcertados" 
                                        SortExpression="PuntosAcertados" Visible="False" />
                                    <asp:BoundField DataField="Resultado" HeaderText="Resultado" 
                                        SortExpression="Resultado" />
                                    <asp:BoundField DataField="FechaTermino" DataFormatString="{0:dd/MM/yyyy}" 
                                        HeaderText="Fecha Captura" SortExpression="FechaTermino">
                                    <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                        </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2" style="text-align: right">
                <asp:Button ID="BtnEliminar" runat="server" Text="Eliminar" Visible="False" 
										CssClass="defaultBtn btnThemeGrey btnThemeMedium"/>
            </td>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="8">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="8">
			<asp:HyperLink ID="HyperLink1" runat="server"
					NavigateUrl="~/" 
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
                
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                            <asp:SqlDataSource ID="SDSciclosescolares" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                                    SelectCommand="SELECT * FROM [CicloEscolar] 
WHERE Estatus = 'Activo'
ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDScalificaciones" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                                SelectCommand="SELECT C.IdCalificacion, C.Descripcion + ' (' + A.Descripcion + ')' as Cal
FROM Calificacion C, Asignatura A
WHERE C.IdCicloEscolar = @IdCicloEscolar
and A.IdAsignatura = C.IdAsignatura and A.IdAsignatura = @IdAsignatura
order by C.Consecutivo">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar" 
                                        PropertyName="SelectedValue" Type="Int32" />
                                    <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura" 
                                        PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSniveles" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    
                        SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]">
                            </asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSevaluaciones" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                                    
                                SelectCommand="SELECT * FROM [Evaluacion] WHERE ([IdCalificacion] = @IdCalificacion)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion" 
                                        PropertyName="SelectedValue" Type="Int64" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                            <asp:SqlDataSource ID="SDSgrados" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                        
                                SelectCommand="SELECT IdGrado,Descripcion FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel" 
                                PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSasignaturas" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                        
                                SelectCommand="SELECT A.IdAsignatura, A.Descripcion + ' (' + Ar.Descripcion + ')'  as Materia
FROM Asignatura A, Area Ar
WHERE (A.IdGrado = @IdGrado) and (Ar.IdArea = A.IdArea)
ORDER BY Ar.Descripcion,A.Descripcion">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado" 
                                PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSinstituciones" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                        SelectCommand="SELECT * FROM [Institucion] ORDER BY [Descripcion]">
                            </asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSplanteles" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                        
                            SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion" 
                            PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    
                                SelectCommand="SELECT [IdGrupo], [Descripcion] FROM [Grupo] WHERE (([IdGrado] = @IdGrado) AND ([IdPlantel] = @IdPlantel)) and ([IdCicloEscolar] = @IdCicloEscolar) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado" 
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel" 
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar" 
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSlistado" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select A.IdAlumno, A.ApePaterno, A.ApeMaterno, A.Nombre, A.Matricula, A.FechaIngreso, A.Email, U.Login, U.Password, A.Estatus, A.Equipo, A.Subequipo
from Alumno A, Usuario U
where A.IdGrupo = @IdGrupo and U.IdUsuario = A.IdUsuario
order by A.ApePaterno, A.ApeMaterno, A.Nombre,U.Login">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo" 
                                        PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSresultados" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [EvaluacionTerminada] WHERE (([IdAlumno] = @IdAlumno) AND ([IdEvaluacion] = @IdEvaluacion) AND ([IdGrupo] = @IdGrupo))">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVAlumnos" Name="IdAlumno" 
                            PropertyName="SelectedValue" Type="Int64" />
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion" 
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo" 
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

