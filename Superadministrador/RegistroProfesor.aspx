﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="RegistroProfesor.aspx.vb" 
    Inherits="superadministrador_RegistroProfesor" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            width: 566px;
            height: 165px;
        }

        .style14 {
            text-align: left;
        }

        .style28 {
            width: 204px;
            height: 14px;
            text-align: right;
            font-size: x-small;
        }

        .style29 {
            text-align: right;
            width: 176px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style15 {
            width: 203px;
        }

        .style26 {
            width: 200px;
        }

        .style19 {
            text-align: right;
            height: 14px;
        }

        .style20 {
            width: 203px;
            height: 14px;
        }

        .style21 {
            width: 114px;
            height: 14px;
            text-align: right;
        }

        .style27 {
            width: 200px;
            height: 14px;
        }

        .style23 {
            font-size: x-small;
        }

        .style24 {
            text-align: left;
        }

        .style12 {
            text-align: right;
        }

        .style18 {
            width: 100%;
        }

        .style31 {
            width: 100%;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style34 {
            width: 155px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style35 {
            text-align: right;
            height: 14px;
            font-size: x-small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Registro de 
								<asp:Label ID="Label1" runat="server" Text="[PROFESORES]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="estandar" style="width: 871px; height: 450px;">
        <tr>
            <td class="style11">
                <table>
                    <tr>
                        <td class="style14" colspan="4">
                            <span class="style28">* Si no se selecciona &quot;Registrar Usuario&quot;, solo se afectan las tablas de Profesor y Usuario, después 
                            el
                                <asp:Label ID="Label6" runat="server" Text="[PROFESOR]" />
                                deberá ingresar a la liga &quot;Registrarme&quot; a generar su 
                            <br />
                                &nbsp;&nbsp; usuario en las tablas de seguridad<br />
                                * Si se desean modificar los datos de un 
																<asp:Label ID="Label5" runat="server" Text="[PROFESOR]" />, se debe ingresar a la función 
                            PLANTILLA --&gt;
                                <asp:Label ID="Label7" runat="server" Text="[PROFESORES]" />
                                --&gt; ADMINISTRAR</span></td>
                    </tr>
                    <tr>
                        <td class="style14" colspan="4">
                            <asp:Label ID="Mensaje" runat="server" CssClass="titulo"
                                Style="color: #000066; font-family: Arial, Helvetica, sans-serif;">Capture los datos</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style29">
                            <asp:Label ID="Label2" runat="server" Text="[INSTITUCION]" />
                        </td>
                        <td class="style15" style="text-align: left">
                            <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                DataValueField="IdInstitucion" Height="22px" Width="300px">
                            </asp:DropDownList>
                        </td>
                        <td class="style34" style="text-align: right">
                            <asp:Label ID="Label3" runat="server" Text="[PLANTEL]" /></td>
                        <td class="style26" style="text-align: left">
                            <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                DataValueField="IdPlantel" Height="23px" Width="245px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style29">Matrícula o Clave</td>
                        <td class="style15" style="text-align: left">
                            <asp:TextBox ID="TBclave" runat="server" MaxLength="15"></asp:TextBox>
                        </td>
                        <td class="style34">&nbsp;</td>
                        <td class="style26">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style29">Nombre</td>
                        <td class="style15" style="text-align: left">
                            <asp:TextBox ID="TBnombre" runat="server" MaxLength="40"></asp:TextBox>
                        </td>
                        <td class="style34" style="text-align: right">Apellidos</td>
                        <td class="style26" style="text-align: left">
                            <asp:TextBox ID="TBapellidos" runat="server" Height="22px" Width="230px"
                                MaxLength="60"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style29">RFC</td>
                        <td class="style15" style="text-align: left">
                            <asp:TextBox ID="TBrfc" runat="server" MaxLength="13"></asp:TextBox>
                        </td>
                        <td class="style34" style="text-align: right">Email</td>
                        <td class="style26">
                            <asp:TextBox ID="TBemail" runat="server" Height="22px" Width="230px"
                                MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style29">Login</td>
                        <td class="style15" style="text-align: left">
                            <asp:TextBox ID="TBlogin" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                        <td class="style34" style="text-align: right">Password</td>
                        <td class="style26" style="text-align: left">
                            <asp:TextBox ID="TBpassword" runat="server" MaxLength="20"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style29">Estatus</td>
                        <td class="style15" style="text-align: left">
                            <asp:DropDownList ID="DDLestatus" runat="server">
                                <asp:ListItem>Activo</asp:ListItem>
                                <asp:ListItem>Suspendido</asp:ListItem>
                                <asp:ListItem>Baja</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="style34" style="text-align: right">Fecha de Ingreso</td>
                        <td class="style26">
                            <asp:TextBox ID="TBingreso" runat="server" MaxLength="10"></asp:TextBox>
                            <span class="style23">(dd/mm/aaaa)</span></td>
                    </tr>
                    <tr>
                        <td class="style35" colspan="2">NOTA: Estatus SUSPENDIDO: El
                            <asp:Label ID="Label4" runat="server" Text="[PROFESOR]" />
                            no puede realizar actividades pero sí 
                            aparece en reportes y en funciones de revisión y captura</td>
                        <td class="style21">&nbsp;</td>
                        <td class="style27">
                            <asp:CheckBox ID="CBregistrarme" runat="server" Style="font-weight: 700; font-family: Arial, Helvetica, sans-serif;"
                                Text="Registrar Usuario" Checked="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style19">&nbsp;</td>
                        <td class="style20">
                            <asp:Button ID="Insertar" runat="server" Text="Insertar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Limpiar" runat="server" Text="Limpiar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                        </td>
                        <td class="style21">&nbsp;</td>
                        <td class="style27">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style17" colspan="4">
                            <asp:GridView ID="GVprofesores" runat="server"
                                AllowPaging="True"
                                AllowSorting="True"
                                AutoGenerateColumns="False"
                                DataKeyNames="IdProfesor"
                                DataSourceID="SDSprofesores"
                                Caption="<h3>PROFESORES capturados</h3>"
                                PageSize="10"
                                CellPadding="3"
                                CssClass="table table-striped table-bordered"
                                Height="17px"
                                Style="font-size: x-small; text-align: left;">
                                <Columns>
                                    <asp:BoundField DataField="IdProfesor" HeaderText="Id Profesor"
                                        InsertVisible="False" ReadOnly="True" SortExpression="IdProfesor">
                                        <HeaderStyle Width="60px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                                        SortExpression="IdPlantel" Visible="False" />
                                    <asp:BoundField DataField="IdUsuario" HeaderText="IdUsuario"
                                        SortExpression="IdUsuario" Visible="False" />
                                    <asp:BoundField DataField="Clave" HeaderText="Clave" SortExpression="Clave" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre"
                                        SortExpression="Nombre" />
                                    <asp:BoundField DataField="Apellidos" HeaderText="Apellidos"
                                        SortExpression="Apellidos" />
                                    <asp:BoundField DataField="RFC" HeaderText="RFC" SortExpression="RFC" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                    <asp:BoundField DataField="FechaIngreso" HeaderText="FechaIngreso"
                                        SortExpression="FechaIngreso" Visible="False" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" Visible="False" />
                                    <asp:BoundField DataField="FechaModif" HeaderText="FechaModif"
                                        SortExpression="FechaModif" Visible="False" />
                                    <asp:BoundField DataField="Modifico" HeaderText="Modifico"
                                        SortExpression="Modifico" Visible="False" />
                                    <asp:BoundField DataField="Login" HeaderText="Login" SortExpression="Login" />
                                    <asp:BoundField DataField="Password" HeaderText="Password"
                                        SortExpression="Password" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                            <%-- sort YES - rowdatabound
	                            0  id_PROFESOR
	                            1  ...
	                            2  
	                            3  
	                            4  
	                            5  
	                            6  
	                            7  
	                            8  
	                            9  
	                            10 
	                            --%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <uc1:msgError runat="server" ID="msgError" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style12">
                <table class="style18">
                    <tr>
                        <td>
                            <asp:SqlDataSource ID="SDSprofesores" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT P.* ,U.Login,U.Password
FROM Profesor P, Usuario U
WHERE (P.IdPlantel = @IdPlantel) and U.IdUsuario = P.IdUsuario
order by P.Apellidos, P.Nombre">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td>
                            <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Institucion]"></asp:SqlDataSource>
                        </td>
                        <td>
                            <asp:SqlDataSource ID="SDSusuarios" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Usuario]"></asp:SqlDataSource>
                        </td>
                        <td>
                            <asp:SqlDataSource ID="SDSplanteles" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style10">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

