﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO

Partial Class superadministrador_BorraDocumentos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.CICLO
        Label2.Text = Config.Etiqueta.NIVEL
        Label3.Text = Config.Etiqueta.GRADO
        Label4.Text = Config.Etiqueta.ASIGNATURA
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija una " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub EliminarE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles EliminarE.Click
        Try
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("SELECT DE.Documento, U.Login FROM DoctoEvaluacion DE, Alumno A, Usuario U WHERE DE.IdEvaluacion = @IdEvaluacion AND DE.IdAlumno = A.IdAlumno AND A.IdUsuario = U.IdUsuario", conn)

                cmd.Parameters.AddWithValue("@IdEvaluacion", GVevaluaciones.SelectedRow.Cells(1).Text)

                Dim results As SqlDataReader = cmd.ExecuteReader()

                While results.Read()
                    Dim ruta As String
          ruta = Config.Global.rutaCargas
          ruta = ruta + results.Item("Login") + "\"
          'Borro el archivo:
          Dim MiArchivo As FileInfo = New FileInfo(ruta & results.Item("Documento"))
          MiArchivo.Delete()
        End While

        results.Close()
        cmd.Dispose()
        conn.Close()
      End Using

      SDSevaluaciones.DeleteCommand = "DELETE FROM EvaluacionTerminada WHERE IdEvaluacionT IN (SELECT ET.IdEvaluacionT FROM EvaluacionTerminada ET, DoctoEvaluacion DE WHERE ET.IdAlumno = DE.IdAlumno AND DE.IdEvaluacion = ET.IdEvaluacion AND ET.IdEvaluacion = @IdEvaluacion); DELETE FROM DoctoEvaluacion WHERE IdEvaluacion = @IdEvaluacion"
      SDSevaluaciones.DeleteParameters.Add("IdEvaluacion", GVevaluaciones.SelectedRow.Cells(1).Text)
      SDSevaluaciones.Delete()

      GVevaluaciones.DataBind()

      msgError.hide()
      msgSuccess.show("Éxito", "Se eliminaron los documentos.")
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub GVevaluaciones_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevaluaciones.DataBound
    If GVevaluaciones.Rows.Count > 0 Then
      EliminarE.Visible = True
    Else
      EliminarE.Visible = False
    End If
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GVcalificaciones_PreRender(sender As Object, e As EventArgs) Handles GVcalificaciones.PreRender
    ' Etiquetas de GridView
    GVcalificaciones.Columns(2).HeaderText = "Id_" & Config.Etiqueta.CICLO
    GVcalificaciones.Columns(2).SortExpression = "IdCicloEscolar"

    GVcalificaciones.Columns(6).HeaderText = Config.Etiqueta.ASIGNATURA
    GVcalificaciones.Columns(6).SortExpression = "Materia"

    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVcalificaciones.Rows.Count > 0 Then
      GVcalificaciones.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVcalificaciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVcalificaciones.RowDataBound
    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVcalificaciones, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVcalificaciones.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVcalificaciones, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    For Each r In GVevaluaciones.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVevaluaciones, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

  Protected Sub GVevaluaciones_PreRender(sender As Object, e As EventArgs) Handles GVevaluaciones.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVevaluaciones.Rows.Count > 0 Then
      GVevaluaciones.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVevaluaciones_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVevaluaciones.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

    Protected Sub GVevaluaciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVevaluaciones, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    Protected Sub DDLasignatura_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLasignatura.SelectedIndexChanged
        If DDLasignatura.SelectedIndex <> 0 Then
            pnlCalificacion.Visible = True
        Else
            pnlCalificacion.Visible = False
        End If
    End Sub

    Protected Sub GVcalificaciones_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVcalificaciones.SelectedIndexChanged
        If GVcalificaciones.SelectedIndex <> -1 Then
            pnlEvaluacion.Visible = True
        Else
            pnlEvaluacion.Visible = False
        End If
    End Sub
End Class
