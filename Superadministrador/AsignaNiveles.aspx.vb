﻿Imports Siget


Partial Class superadministrador_AsignaNiveles
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GVnivelesasignados.Caption = "<h3>" &
            Config.Etiqueta.NIVELES &
            " asignad" & Config.Etiqueta.LETRA_NIVEL & "s</h3>"

        Label1.Text = Config.Etiqueta.NIVELES
        Label2.Text = Config.Etiqueta.PLANTELES
        Label3.Text = Config.Etiqueta.INSTITUCION
        Label4.Text = Config.Etiqueta.PLANTEL
        Label5.Text = Config.Etiqueta.NIVEL
    End Sub

    Protected Sub Asignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Asignar.Click
        Try
            SDSescuelas.InsertCommand = "SET dateformat dmy; INSERT INTO Escuela (IdPlantel,IdNivel) VALUES (" + DDLplantel.SelectedValue + "," + DDLnivel.SelectedValue + ")"
            SDSescuelas.Insert()
            Mensaje.Text = "El registro ha sido guardado"
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub


    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem("---Elija una " & Config.Etiqueta.INSTITUCION, 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.NIVEL & " a asignar", 0))
    End Sub

    Protected Sub Desasignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Desasignar.Click
        Try
            SDSescuelas.DeleteCommand = "Delete from Escuela where IdEscuela = " + GVnivelesasignados.SelectedValue.ToString 'Me da el IdEscuela porque es el campo clave de la fila seleccionada
            SDSescuelas.Delete()
            GVnivelesasignados.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVnivelesasignados_PreRender(sender As Object, e As EventArgs) Handles GVnivelesasignados.PreRender
        ' Etiquetas de GridView
        GVnivelesasignados.Columns(1).HeaderText = "Id_" & Config.Etiqueta.PLANTEL

        GVnivelesasignados.Columns(2).HeaderText = Config.Etiqueta.PLANTEL

        GVnivelesasignados.Columns(3).HeaderText = "Id_" & Config.Etiqueta.NIVEL

        GVnivelesasignados.Columns(4).HeaderText = Config.Etiqueta.NIVEL & " Asignad" & Config.Etiqueta.LETRA_NIVEL
    End Sub
End Class
