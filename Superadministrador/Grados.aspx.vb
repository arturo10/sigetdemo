﻿Imports Siget

Partial Class superadministrador_Grados
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.GRADOS
        Label2.Text = Config.Etiqueta.NIVEL
        Label3.Text = Config.Etiqueta.GRADO
    End Sub

    Protected Sub Guardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        Try
            SDSgrados.InsertCommand = "SET dateformat dmy; INSERT INTO Grado (IdNivel,Descripcion,Estatus, FechaModif, Modifico) VALUES (" + DDLnivel.SelectedValue + ", '" + TBdescripcion.Text + "','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
            SDSgrados.Insert()
            Mensaje.Text = "El registro ha sido guardado"
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            Mensaje.Text = ""
        End Try
    End Sub

    Protected Sub Cancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cancelar.Click
        TBdescripcion.Text = ""
        Mensaje.Text = "Capture los datos"
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija el nivel del Grado", 0))
    End Sub

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        Try
            SDSgrados.DeleteCommand = "Delete from Grado where IdGrado = " + GVgradoscreados.SelectedValue.ToString 'Me da el IdGrado porque es el campo clave de la fila seleccionada
            SDSgrados.Delete()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            Mensaje.Text = ""
        End Try
    End Sub

    Protected Sub GVgradoscreados_PreRender(sender As Object, e As EventArgs) Handles GVgradoscreados.PreRender
        ' Etiquetas de GridView
        GVgradoscreados.Columns(1).HeaderText = "Id_" & Config.Etiqueta.GRADO
        GVgradoscreados.Columns(1).SortExpression = "IdGrado"

        GVgradoscreados.Columns(3).HeaderText = Config.Etiqueta.GRADOS & " Creados"
        GVgradoscreados.Columns(3).SortExpression = "Descripcion"
    End Sub

    Protected Sub GVgradoscreados_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVgradoscreados.SelectedIndexChanged
        TBdescripcion.Text = HttpUtility.HtmlDecode(GVgradoscreados.SelectedRow.Cells(3).Text)
        DDLestatus.SelectedValue = GVgradoscreados.SelectedRow.Cells(6).Text
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        Try
            SDSgrados.UpdateCommand = "SET dateformat dmy; UPDATE Grado set Descripcion = '" + TBdescripcion.Text + "', Estatus = '" + DDLestatus.SelectedValue + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdGrado = " + GVgradoscreados.SelectedValue.ToString 'Me da el IdGrado porque es el campo clave de la fila seleccionada
            SDSgrados.Update()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            Mensaje.Text = ""
        End Try
    End Sub
End Class
