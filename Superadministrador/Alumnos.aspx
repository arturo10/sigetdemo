﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="Alumnos.aspx.vb"
    Inherits="superadministrador_Alumnos"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style12 {
            text-align: right;
        }

        .style10 {
            width: 100%;
            height: 450px;
        }

        .style11 {
            width: 88%;
        }

        .style15 {
            text-align: right;
            width: 89px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
        }

        .style18 {
            text-align: left;
        }

        .style13 {
            width: 100%;
            height: 1px;
        }

        .style19 {
            width: 48px;
        }

        .style20 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
            text-align: left;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            text-align: left;
        }

        .style24 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style26 {
            text-align: left;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
        }

        .style27 {
            height: 41px;
            text-align: left;
        }

        .style28 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
        }

        .style17 {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Administración de 
										<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td>
                <table class="style11">
                    <tr>
                        <td colspan="5" class="style27">
                            <span class="style20">*En Insertar no impacta la tabla de usuarios ni las de 
                seguridad, solo la de alumnos, por eso se deshabilita en esta página. Usar solo 
                para modificar<br />
                                *En Eliminar borra todos los registros del usuario, incluyendo los de su usuario 
                en las tablas de seguridad</span></td>
                    </tr>
                    <tr>
                        <td colspan="5" class="style27">
                            <asp:Label ID="Mensaje" runat="server" CssClass="titulo"
                                Style="color: #000099; font-family: Arial, Helvetica, sans-serif;">Capture los datos</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">
                            <asp:Label ID="Label2" runat="server" Text="[INSTITUCION]" />:</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="DDLinstitucion"
                                runat="server" AutoPostBack="True" DataSourceID="SDSinstituciones"
                                DataTextField="Descripcion" DataValueField="IdInstitucion"
                                Width="350px">
                            </asp:DropDownList>
                        </td>
                        <td class="style26">
                            <asp:Label ID="Label3" runat="server" Text="[PLANTEL]" />:</td>
                        <td class="style17" colspan="2">
                            <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                DataValueField="IdPlantel" Width="350px" Height="22px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">
                            <asp:Label ID="Label4" runat="server" Text="[NIVEL]" />:</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                                Width="350px">
                            </asp:DropDownList>
                        </td>
                        <td class="style26">
                            <asp:Label ID="Label5" runat="server" Text="[GRADO]" />:</td>
                        <td class="style17" colspan="2">
                            <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                DataSourceID="SDSgrados" DataTextField="Descripcion"
                                DataValueField="IdGrado" Width="350px" Height="22px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style15">
                            <asp:Label ID="Label6" runat="server" Text="[CICLO]" />:</td>
                        <td class="style26">
                            <asp:DropDownList ID="DDLcicloescolar" runat="server"
                                DataSourceID="SDSciclosescolares" DataTextField="Ciclo"
                                DataValueField="IdCicloEscolar" Width="350px" AutoPostBack="True"
                                Style="text-align: left">
                            </asp:DropDownList>
                        </td>
                        <td class="style26">
                            <asp:Label ID="Label7" runat="server" Text="[GRUPO]" />:</td>
                        <td class="style18" colspan="2">
                            <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                                DataSourceID="SDSgrupos" DataTextField="Descripcion"
                                DataValueField="IdGrupo" Width="350px" Height="22px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">Nombre</td>
                        <td class="style18">
                            <asp:TextBox ID="TBnombre" runat="server" MaxLength="40"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">Apellido Paterno</td>
                        <td class="style18">
                            <asp:TextBox ID="TBApePaterno" runat="server" MaxLength="30"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">Apellido Materno</td>
                        <td class="style18">
                            <asp:TextBox ID="TBApeMaterno" runat="server" MaxLength="30"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">Matrícula del
                            <asp:Label ID="Label8" runat="server" Text="[ALUMNO]" /></td>
                        <td class="style18">
                            <asp:TextBox ID="TBmatricula" runat="server" MaxLength="20"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">
                            <asp:Label ID="Label9" runat="server" Text="[EQUIPO]" /></td>
                        <td class="style18">
                            <asp:DropDownList ID="DDLequipo" runat="server" Width="350px">
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">
                            <asp:Label ID="Label10" runat="server" Text="[SUBEQUIPO]" /></td>
                        <td class="style18">
                            <asp:DropDownList ID="DDLsubequipo" runat="server" Width="145px">
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">Email</td>
                        <td class="style18">
                            <asp:TextBox ID="TBemail" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">Fecha Ingreso (dd/mm/aaaa)
                        </td>
                        <td class="style18">
                            <asp:TextBox ID="TBingreso" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">Estatus</td>
                        <td class="style18">
                            <asp:DropDownList ID="DDLestatus" runat="server">
                                <asp:ListItem>Activo</asp:ListItem>
                                <asp:ListItem>Suspendido</asp:ListItem>
                                <asp:ListItem>Baja</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style12" colspan="4">
                            <asp:Button ID="Insertar" runat="server" Text="Insertar" Enabled="False"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Limpiar" runat="server" Text="Limpiar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Actualizar" runat="server" Text="Actualizar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Eliminar" runat="server" Text="Eliminar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style28" colspan="4">NOTA: Estatus SUSPENDIDO: El 
														<asp:Label ID="Label11" runat="server" Text="[ALUMNO]" />
                            no puede realizar actividades pero sí 
                            aparece en reportes y en funciones de revisión y captura</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <uc1:msgError runat="server" ID="msgError" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style12" colspan="5">
                            <asp:GridView ID="GVAlumnos" runat="server"
                                AllowSorting="True"
                                AutoGenerateColumns="False"

                                DataKeyNames="IdAlumno"
                                DataSourceID="SDSlistado"
                                Width="857px"

                                CssClass="dataGrid_clear_selectable"
                                GridLines="None">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                                    <asp:BoundField DataField="IdAlumno" HeaderText="[IdAlumno]"
                                        InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno" />
                                    <asp:BoundField DataField="ApePaterno" HeaderText="Ap. Paterno"
                                        SortExpression="ApePaterno" />
                                    <asp:BoundField DataField="ApeMaterno" HeaderText="Ap. Materno"
                                        SortExpression="ApeMaterno" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre"
                                        SortExpression="Nombre" />
                                    <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                                        SortExpression="Matricula">
                                        <ItemStyle Width="85px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                    <asp:BoundField DataField="FechaIngreso" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderText="Ingreso" SortExpression="FechaIngreso" />
                                    <asp:BoundField DataField="Login" HeaderText="Login" SortExpression="Login" />
                                    <asp:BoundField DataField="Password" HeaderText="Password"
                                        SortExpression="Password" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                    <asp:BoundField DataField="Equipo" HeaderText="[Equipo]" SortExpression="Equipo" />
                                    <asp:BoundField DataField="Subequipo" HeaderText="[Subequipo]"
                                        SortExpression="Subequipo" />
                                </Columns>
                                <FooterStyle CssClass="footer" />
                                <PagerStyle CssClass="pager" />
                                <SelectedRowStyle CssClass="selected" />
                                <HeaderStyle CssClass="header" />
                                <AlternatingRowStyle CssClass="altrow" />
                            </asp:GridView>
                            <%-- sort YES - rowdatabound
	                            0  select
	                            1  IdALUMNO
	                            2  appaterno
	                            3  apmaterno
	                            4  nombre
	                            5  matricula
	                            6  email
	                            7  fechaingreso
	                            8  login
	                            9  pass
	                            10 estatus
                                11 EQUIPO
                                12 SUBEQUIPO
                            --%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Institucion] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion)
ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select N.IdNivel, N.Descripcion
from Nivel N, Escuela E, Plantel P
where N.IdNivel = E.IdNivel and P.IdPlantel = E.IdPlantel
      and P.IdPlantel = @IdPlantel
Order by Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grupo] 
WHERE ([IdGrado] = @IdGrado) and ([IdPlantel] = @IdPlantel) and ([IdCicloEscolar] = @IdCicloEscolar)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSalumnos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Alumno]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSlistado" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select A.IdAlumno, A.ApePaterno, A.ApeMaterno, A.Nombre, A.Matricula, A.FechaIngreso, A.Email, U.Login, U.Password, A.Estatus, A.Equipo, A.Subequipo
from Alumno A, Usuario U
where A.IdGrupo = @IdGrupo and U.IdUsuario = A.IdUsuario
order by A.ApePaterno, A.ApeMaterno, A.Nombre,U.Login">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdCicloEscolar, Descripcion + ' (' + Cast(FechaInicio as varchar(12)) + ' - ' + Cast(FechaFin as varchar(12)) + ')' Ciclo
FROM CicloEscolar
WHERE (Estatus = 'Activo')
order by Descripcion"></asp:SqlDataSource>

            </td>
        </tr>
    </table>
</asp:Content>

