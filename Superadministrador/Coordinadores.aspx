<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    EnableEventValidation="false"
    CodeFile="Coordinadores.aspx.vb" 
    Inherits="superadministrador_Coordinadores" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style10
        {
            width: 100%;
            height: 450px;
        }
        
        .style12
        {
            height: 36px;
        }
        .style14
        {
            height: 25px;
        }
        .style15
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
        .style23
        {
            font-family: Arial, Helvetica, sans-serif;
        }
        .style24
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            width: 155px;
            height: 8px;
        }
        .style25
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 155px;
            height: 8px;
        }
        .style26
        {
            height: 66px;
        }
        .style27
        {
            height: 14px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
        .style28
        {
            height: 14px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 632px;
            font-weight: bold;
        }
    .style40
    {
        font-size: small;
    }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Administraci�n de 
										<asp:Label ID="Label2" runat="server" Text="[COORDINADORES]" />
		</h1>
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style10" style="width: 900px">
        <tr>
            <td class="style15" colspan="2">
                <asp:DetailsView ID="DVcoordinadores" runat="server" 
                    AutoGenerateRows="False" BackColor="White" BorderColor="#999999" 
                    BorderStyle="Solid" BorderWidth="1px" CellPadding="3" CssClass="estandar" 
                    DataKeyNames="IdCoordinador" DataSourceID="LDScoordinadores" 
                    GridLines="Vertical" Height="50px" Width="478px" ForeColor="Black" 
                    AllowPaging="True">
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <Fields>
                        <asp:BoundField DataField="IdCoordinador" HeaderText="Id del sistema" 
                            InsertVisible="False" ReadOnly="True" SortExpression="IdCoordinador" >
                        <HeaderStyle Width="80px" />
                        <ItemStyle Width="100px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdUsuario" HeaderText="IdUsuario" 
                            SortExpression="IdUsuario" Visible="False" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                            SortExpression="Nombre" />
                        <asp:BoundField DataField="Apellidos" HeaderText="Apellidos" 
                            SortExpression="Apellidos" />
                        <asp:BoundField DataField="Clave" HeaderText="C�digo asignado" 
                            SortExpression="Clave" />
                        <asp:BoundField DataField="FechaIngreso" HeaderText="Fecha de Ingreso" 
                            SortExpression="FechaIngreso" DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" 
                            SortExpression="Estatus" />
                        <asp:BoundField DataField="Email" HeaderText="Correo Electr�nico" 
                            SortExpression="Email" />
                        <asp:BoundField DataField="FechaModif" HeaderText="FechaModif" 
                            SortExpression="FechaModif" Visible="False" />
                        <asp:BoundField DataField="Modifico" HeaderText="Modifico" 
                            SortExpression="Modifico" Visible="False" />
                        <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                            ShowInsertButton="True" />
                    </Fields>
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:DetailsView>
                <br />
                <span class="style24">Indique los Estatus: </span>
                <span class="style25">Activo</span><span class="style24">, </span>
                <span class="style25">Suspendido</span><span class="style24"> o </span>
                <span class="style25">Baja</span></td>
        </tr>
        <tr>
            <td class="style12" colspan="2">
              <asp:LinqDataSource ID="LDScoordinadores" runat="server"
                ContextTypeName="CoordinadoresDataContext" EnableDelete="True"
                EnableInsert="True" EnableUpdate="True" TableName="Coordinadors" EntityTypeName="">
                </asp:LinqDataSource>
                <asp:SqlDataSource ID="SDScoordinadores" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [Coordinador]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style26" colspan="2">
                            <span class="style40"><span class="estandar">
                            <span class="estandar" style="text-align: center">
                <asp:Label ID="Label1" runat="server" 
                    Text="El siguiente men� antes estaba en la p�gina Default.aspx (aqu� est� invisible):" 
                    Visible="False"></asp:Label>
            <asp:Menu ID="Menu2" runat="server" BackColor="#E3EAEB" CssClass="estandar" 
                DynamicHorizontalOffset="2" Font-Names="Verdana" Font-Size="0.8em" 
                ForeColor="#666666" StaticSubMenuIndent="10px" style="text-align: left" Height="61px" 
                                Visible="False">
                <StaticSelectedStyle BackColor="#1C5E55" />
                <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                <DynamicHoverStyle BackColor="#666666" ForeColor="White" />
                <DynamicMenuStyle BackColor="#E3EAEB" />
                <DynamicSelectedStyle BackColor="#1C5E55" />
                <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                <StaticHoverStyle BackColor="#666666" ForeColor="White" />
                <Items>
                    <asp:MenuItem NavigateUrl="~/RegistroAlumnos.aspx" 
                        Text="Crear usuario para el perfil de Alumno" 
                        Value="Crear usuario para el perfil de Alumno"></asp:MenuItem>
                    <asp:MenuItem NavigateUrl="~/RegistroProfesor.aspx" 
                        Text="Crear usuario para el perfil de Profesor" 
                        Value="Crear usuario para el perfil de Profesor"></asp:MenuItem>
                    <asp:MenuItem Text="Crear usuario para el perfil de Coordinador" 
                        Value="Crear usuario para el perfil de Coordinador" 
                        NavigateUrl="~/RegistroCoordinador.aspx"></asp:MenuItem>
                </Items>
            </asp:Menu>
                            </span>
                            </span>
                            </span>
            </td>
        </tr>
        <tr>
            <td class="style28">
                Una vez creado el 
								<asp:Label ID="Label3" runat="server" Text="[COORDINADOR]" />
								, Usted puede generar las claves de acceso en el 
                siguiente bot�n:</td>
            <td class="style27" style="font-size: medium;">
                <asp:LinkButton ID="LinkButton1" runat="server" 
                    PostBackUrl="~/RegistroCoordinador.aspx" 
                    CssClass="defaultBtn btnThemeLightBlue btnThemeMedium"
										>Crear login de acceso</asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style14" colspan="2">
                <asp:HyperLink ID="HyperLink2" runat="server"
					NavigateUrl="~/" 
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Men�
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

