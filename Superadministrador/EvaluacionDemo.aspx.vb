﻿Imports Siget


Partial Class superadministrador_EvaluacionDemo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.CICLO
        Label2.Text = Config.Etiqueta.NIVEL
        Label3.Text = Config.Etiqueta.GRADO
        Label4.Text = Config.Etiqueta.ASIGNATURA
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija la " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLcalificaciones_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificaciones.DataBound
        DDLcalificaciones.Items.Insert(0, New ListItem("---Elija la Calificación", 0))
    End Sub

    Protected Sub GVevaluaciones_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevaluaciones.SelectedIndexChanged

    End Sub

    Protected Sub DDLsubtemas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLsubtemas.DataBound
        DDLsubtemas.Items.Insert(0, New ListItem("---Elija el Subtema", 0))
    End Sub

    Protected Sub Agregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Agregar.Click
        'AQUI PUEDO ELEGIR SOLO LOS REACTIVOS QUE FUERON CLASIFICADOS COMO "DEMO" EN CADA UNO DE LOS SUBTEMAS, Y AL
        'CREAR ESTA ACTIVIDAD DEMO TOMARÁ ESOS REACTIVOS QUE YO ELIJA PARA MOSTRARLOS. LOS REACTIVOS "DEMO" NUNCA SE
        'MUESTRAN EN LAS ACTIVIDADES NORMALES
        Try
            Dim I As Integer
            For I = 0 To CBLreactivos.Items.Count - 1
                If CBLreactivos.Items(I).Selected Then
                    SDSevaluaciondemo.InsertCommand = "SET dateformat dmy; INSERT INTO EvaluacionDemo (IdEvaluacion,IdPlanteamiento, FechaModif, Modifico) VALUES (" + GVevaluaciones.SelectedRow.Cells(1).Text + "," + CBLreactivos.Items(I).Value + ", getdate(), '" & User.Identity.Name & "')"
                    SDSevaluaciondemo.Insert()
                End If
            Next
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
        CBLreactivos.DataBind()
        GVseleccionados.DataBind()
    End Sub

    Protected Sub Quitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Quitar.Click
        Try
            SDSevaluaciondemo.DeleteCommand = "DELETE FROM EvaluacionDemo where IdPlanteamiento = " + GVseleccionados.SelectedValue.ToString 'Me da el IdDetalleEvaluacion porque es el campo clave de la fila seleccionada
            SDSevaluaciondemo.Delete()

            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
        CBLreactivos.DataBind()
        GVseleccionados.DataBind()
    End Sub

    Protected Sub Consecutivo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Consecutivo.Click
        Try
            SDSevaluaciondemo.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionDemo set Consecutivo = " + Trim(TBconsecutivo.Text) + ", FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdEvaluacion = " + GVevaluaciones.SelectedValue.ToString + " and IdPlanteamiento = " + GVseleccionados.SelectedValue.ToString
            SDSevaluaciondemo.Update()
            GVseleccionados.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

  Protected Sub GVseleccionados_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVseleccionados.RowDataBound

    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(4).Text = HttpUtility.HtmlDecode(e.Row.Cells(4).Text)

      ' convierto el campo de respuesta de modo que se presente con estilos, no como markup
      e.Row.Cells(4).Text = Regex.Replace(HttpUtility.HtmlDecode(e.Row.Cells(4).Text), "<[^>]*(>|$)", String.Empty)
    End If
  End Sub
End Class
