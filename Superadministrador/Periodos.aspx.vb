﻿Imports Siget


Partial Class superadministrador_Periodos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.CICLO
        Label2.Text = Config.Etiqueta.CICLO
    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        SDSperiodos.InsertCommand = "SET dateformat dmy; INSERT INTO Periodo(IdCicloEscolar,NoPeriodo,FechaInicio,FechaFin, FechaModif, Modifico) VALUES (" + DDLcicloescolar.SelectedValue + "," + TBnumperiodo.Text + ",'" + TBinicioperiodo.Text + "','" + Trim(TBfinperiodo.Text) + " 23:59:00', getdate(), '" & User.Identity.Name & "')"
        SDSperiodos.Insert()
        Mensaje.Text = "El registro ha sido guardado"
        GVperiodos.DataBind()
    End Sub

    Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click
        TBnumperiodo.Text = ""
        Mensaje.Text = "Capture los datos"
    End Sub

    Protected Sub GVperiodos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVperiodos.SelectedIndexChanged
        'La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
        TBnumperiodo.Text = GVperiodos.SelectedRow.Cells(3).Text

        'PENDIENTE VER COMO NO MARCA ERROR
        CalInicioPeriodo.SelectedDate = CDate(GVperiodos.SelectedRow.Cells(4).Text)
        TBinicioperiodo.Text = CalInicioPeriodo.SelectedDate

        CalFinPeriodo.SelectedDate = CDate(GVperiodos.SelectedRow.Cells(5).Text)
        TBfinperiodo.Text = CalFinPeriodo.SelectedDate
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        SDSperiodos.UpdateCommand = "SET dateformat dmy; UPDATE Periodo SET NoPeriodo = " + TBnumperiodo.Text + ", FechaInicio = '" + TBinicioperiodo.Text + "',FechaFin = '" + Trim(TBfinperiodo.Text) + " 23:59:00', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdPeriodo =" + GVperiodos.SelectedRow.Cells(1).Text
        SDSperiodos.Update()
        Mensaje.Text = "El registro ha sido actualizado"
        GVperiodos.DataBind()
    End Sub

    Protected Sub CalInicioPeriodo_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalInicioPeriodo.SelectionChanged
        TBinicioperiodo.Text = CalInicioPeriodo.SelectedDate
    End Sub

    Protected Sub CalFinPeriodo_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalFinPeriodo.SelectionChanged
        TBfinperiodo.Text = CalFinPeriodo.SelectedDate
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        SDSperiodos.DeleteCommand = "Delete from Periodo where IdPeriodo = " + GVperiodos.SelectedValue.ToString 'Me da el IdPeriodo porque es el campo clave de la fila seleccionada
        SDSperiodos.Delete()
    End Sub
End Class
