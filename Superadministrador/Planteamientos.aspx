<%@ Page Title=""
  Language="VB"
  MasterPageFile="~/Principal.master"
  AutoEventWireup="false"
  CodeFile="Planteamientos.aspx.vb"
  Inherits="superadministrador_Planteamientos"
  MaintainScrollPositionOnPostback="true"
  ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
  <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <style type="text/css">
    .style10 {
      width: 100%;
      height: 450px;
    }

    .style22 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: small;
    }

    .style21 {
      font-size: x-small;
      text-align: right;
    }

    .style20 {
      text-align: right;
    }

    .style17 {
      font-size: x-small;
    }

    .style16 {
      width: 397px;
      text-align: left;
    }

    .style18 {
      width: 295px;
      font-size: x-small;
    }

    .style19 {
      font-size: xx-small;
    }

    .style15 {
      width: 69%;
    }

    .style23 {
      color: #000099;
    }

    .style36 {
      font-weight: normal;
      font-size: small;
    }

    .style37 {
      width: 320px;
      height: 8px;
      text-align: left;
    }

    .style38 {
      width: 320px;
    }
  </style>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
  <h1>Reactivos - Planteamientos
  </h1>
  Permite capturar los planteamientos de opci�n m�ltiple o 
                    respuesta abierta para cada subtema.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
  <table class="style10">
    <tr>
      <td colspan="8">
        <asp:Label ID="Mensaje1" runat="server" CssClass="LabelInfoDefault" Style="color: #000066">Capture los datos</asp:Label>
      </td>
    </tr>
    <tr>
      <td style="text-align: right" class="style22">
        <asp:Label ID="Label1" runat="server" Text="[NIVEL]" />:</td>
      <td>
        <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
          DataSourceID="SDSniveles" DataTextField="Descripcion"
          DataValueField="IdNivel" Width="285px" CssClass="style22" AppendDataBoundItems="True" EnableViewState="false">
            <asp:listitem Value="0"> ---Elija un nivel-- </asp:listitem>
        </asp:DropDownList>
      </td>
      <td style="text-align: right" class="style22">
        <asp:Label ID="Label2" runat="server" Text="[GRADO]" />:</td>
      <td>
        <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
          DataSourceID="SDSgrados" DataTextField="Descripcion"
          DataValueField="IdGrado" Width="410px" CssClass="style22" AppendDataBoundItems="true" EnableViewState="false">
            <asp:ListItem Value="0">
                --Elija un Grado--
            </asp:ListItem>
        </asp:DropDownList>
      </td>
      <td class="style22">&nbsp;</td>
      <td class="style22">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td style="text-align: right" class="style22">
        <asp:Label ID="Label3" runat="server" Text="[ASIGNATURA]" />:</td>
      <td>
        <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
          DataSourceID="SDSasignaturas" DataTextField="Descripcion"
          DataValueField="IdAsignatura" Width="285px"
          CssClass="style22" Height="22px" AppendDataBoundItems="true" EnableViewState="false">
            <asp:ListItem Value="0">--Elija una Asignatura--</asp:ListItem>
        </asp:DropDownList>
      </td>
      <td style="text-align: right" class="style22">Temas:</td>
      <td>
        <asp:DropDownList ID="DDLtemas" runat="server" AutoPostBack="True"
          DataSourceID="SDStemas" DataTextField="NomTema"
          DataValueField="IdTema" Width="410px" CssClass="style22" Height="22px" AppendDataBoundItems="true" EnableViewState="false">
            <asp:ListItem Value="0">--Elija un Tema--</asp:ListItem>
        </asp:DropDownList>
      </td>
      <td style="text-align: right" class="style22">&nbsp;</td>
      <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
      <td style="text-align: right" class="style22">Subtema:</td>
      <td colspan="7" style="text-align: left">
        <asp:DropDownList ID="DDLsubtemas" runat="server" AutoPostBack="True"
          DataSourceID="SDSsubtemas" DataTextField="NomSubtema"
          DataValueField="IdSubtema" Width="764px"
          CssClass="style22" Height="22px" AppendDataBoundItems="true" EnableViewState="false">
            <asp:ListItem Value="0"> --Elija un Subtema--</asp:ListItem>
        </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td colspan="8">
        <table class="estandar">
          <tr>
            <td colspan="3" class="style21">Para ingresar saltos de l�nea en la redacci�n escriba la cadena: *salto* y para 
                                poner espacios en blanco: *_*</td>
          </tr>
          <tr>
            <td style="text-align: right" class="style20">Redacci�n del Planteamiento</td>
            <td class="style16" colspan="2">
              <asp:TextBox ID="TBredaccion" runat="server"
                ClientIDMode="Static"
                TextMode="MultiLine"
                CssClass="tinymce"></asp:TextBox>
            </td>
          </tr>
          <tr>
            <td style="text-align: right" class="style20">N�mero Consecutivo</td>
            <td class="style16" colspan="2">
              <asp:TextBox ID="TBconsecutivo" runat="server" Width="70px"></asp:TextBox>
            </td>
          </tr>
          <tr>
            <td style="text-align: right" class="style20">Tipo de Respuesta</td>
            <td class="style37">
              <asp:DropDownList ID="DDLtiporespuesta" ClientIDMode="Static" runat="server" >
                <asp:ListItem Value="Multiple">Opci�n M�ltiple</asp:ListItem>
                <asp:ListItem Value="Abierta">Respuesta Abierta</asp:ListItem>
                <asp:ListItem Value="Abierta Calificada">Respuesta Abierta Calificada</asp:ListItem>
                <asp:ListItem Value="Completar" Enabled="False">Completar Espacios</asp:ListItem>
              </asp:DropDownList>
            </td>
            <td class="style16 ConCalifica " runat="server" id="ContainerCalifica" style="display:none;">
              <asp:CheckBox ID="CBcalifica" ClientIDMode="Static" runat="server"
                Text="Califica el PROFESOR" Style="font-weight: 700;"  />
            </td>
          </tr>
          <tr>
            <td>&nbsp;
            </td>
            <td colspan="2" id="panelAbiertaCalificada" runat="server" class="panelAbiertaC" style="display:none;">
              <asp:Panel ID="PnlWidth" runat="server" >
                <table style="width: 100%;">
                  <tr>
                    <td style="text-align: left;" colspan="3">Ancho del Contenedor para la Respuesta
                                            <br />
                      <asp:Button ID="Button1" runat="server"
                        Text="-"
                        OnClientClick="decWidth(); return false;"
                        CssClass="defaultBtn btnThemeGrey btnThemeSlick" />&nbsp;
                                            <asp:Label ID="WidthLabel" ClientIDMode="Static" runat="server" Text="180"></asp:Label>&nbsp;
                                            <asp:Button ID="Button2" runat="server"
                                              Text="+"
                                              OnClientClick="incWidth(); return false;"
                                              CssClass="defaultBtn btnThemeGrey btnThemeSlick" />
                      <br />
                      <asp:TextBox ID="tbExample" ClientIDMode="Static" runat="server"></asp:TextBox>
                      <script>
                        function incWidth() {
                          var width = parseInt($("#WidthLabel").text());
                          if (width < 700)
                            width += 20;
                          $("#tbExample").width(width);
                          $("#WidthLabel").text(width);
                          $("#HfWidth").val(width);
                        }

                        function decWidth() {
                          var width = parseInt($("#WidthLabel").text());
                          if (width > 180)
                            width -= 20;
                          $("#tbExample").width(width);
                          $("#WidthLabel").text(width);
                          $("#HfWidth").val(width);
                        }
                      </script>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <asp:CheckBox ID="ChkMayusculas" ClientIDMode="Static" runat="server"
                        Text="Compara May�sculas y Min�sculas"
                        onclick="toggleMayus()" />
                      <script>
                        function toggleMayus() {
                          var checked = $("#HfMayusculas").val() == 'true';

                          checked = !checked;

                          if (checked) {
                            $("#ChkMayusculas").prop('checked', true);
                          }
                          else {
                            $("#ChkMayusculas").prop('checked', false);
                          }
                          $("#HfMayusculas").val(checked.toString());
                        }
                      </script>
                    </td>
                    <td>
                      <asp:CheckBox ID="ChkVerifica" ClientIDMode="Static" runat="server"
                        Text="Permite Verificar"
                        onclick="toggleVerify()" />
                      <script>
                        function toggleVerify() {
                          var checked = $("#HfVerifica").val() == 'true';

                          checked = !checked;

                          if (checked) {
                            $("#ChkVerifica").prop('checked', true);
                          }
                          else {
                            $("#ChkVerifica").prop('checked', false);
                          }
                          $("#HfVerifica").val(checked.toString());
                        }
                      </script>
                    </td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>
                      <asp:CheckBox ID="ChkAcentos" ClientIDMode="Static" runat="server"
                        Text="Compara Acentos"
                        onclick="toggleAcentos()" />
                      <script>
                        function toggleAcentos() {
                          var checked = $("#HfAcentos").val() == 'true';

                          checked = !checked;

                          if (checked) {
                            $("#ChkAcentos").prop('checked', true);
                          }
                          else {
                            $("#ChkAcentos").prop('checked', false);
                          }
                          $("#HfAcentos").val(checked.toString());
                        }
                      </script>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <asp:CheckBox ID="ChkAlfaNum" ClientIDMode="Static" runat="server"
                        Text="Elimina acentos y ap�strofes"
                        onclick="toggleAlfanum()" />
                      <script>
                          function toggleAlfanum() {
                           
                          var checked = $("#HfAlfanumerico").val() == 'true';

                          checked = !checked;

                          if (checked) {
                            $("#ChkAlfaNum").prop('checked', true);
                          }
                          else {
                            $("#ChkAlfaNum").prop('checked', false);
                          }
                          $("#HfAlfanumerico").val(checked.toString());

                        }
                      </script>
                    </td>
                  </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="CkPuntuacion" ClientIDMode="Static" runat="server"
                                Text="Elimina s�mbolos de puntuaci�n y espacios" 
                                onclick="togglePuntuacion()" />
                             <script>
                                 function togglePuntuacion() {

                                     var checked = $("#HfPuntuacion").val() == 'true';

                                     checked = !checked;

                                     if (checked) {
                                         $("#CkPuntuacion").prop('checked', true);
                                     }
                                     else {
                                         $("#CkPuntuacion").prop('checked', false);
                                     }
                                     $("#HfPuntuacion").val(checked.toString());
                                 }
                      </script>
                        </td>
                         
                    </tr>
                </table>
              </asp:Panel>
            </td>
          </tr>
          <tr>
            <td style="text-align: right" class="style20">Puntos para Ponderaci�n</td>
            <td class="style16" colspan="2">
              <asp:TextBox ID="TBponderacion" runat="server" Width="50px"></asp:TextBox>
            </td>
          </tr>
          <tr>
            <td style="text-align: right" class="style20">Porcentaje que deber� restarse<br />
              al 100% en segundo intento</td>
            <td class="style16" colspan="2">
              <asp:TextBox ID="TBporcentaje" runat="server" Width="50px"></asp:TextBox>
              %</td>
          </tr>
          <tr>
            <td style="text-align: right" class="style20">Cargar material de apoyo</td>
            <td class="style38">
              <asp:FileUpload ID="RutaArchivo" runat="server" />
            </td>
            <td class="style18">*Al insertar o actualizar se cargar� el archivo</td>
          </tr>
          <tr>
            <td style="text-align: right" class="style20">Tipo de archivo</td>
            <td class="style38">
              <asp:DropDownList ID="DDLtipoarchivo" runat="server" Width="100px">
                <asp:ListItem>Ninguno</asp:ListItem>
                <asp:ListItem>Imagen</asp:ListItem>
                <asp:ListItem>PDF</asp:ListItem>
                <asp:ListItem>ZIP</asp:ListItem>
                <asp:ListItem>Word</asp:ListItem>
                <asp:ListItem>Excel</asp:ListItem>
                <asp:ListItem Value="PP">Power Pont</asp:ListItem>
                <asp:ListItem>Video</asp:ListItem>
                <asp:ListItem>Audio</asp:ListItem>
              </asp:DropDownList>
              <span class="style19">(usar im�genes de m�x. 350x350 px)</span></td>
            <td class="style18">
              <asp:HyperLink ID="HLarchivo1" runat="server"
                Style="color: #0000CC; font-weight: 700" Target="_blank">No hay archivo de apoyo cargado</asp:HyperLink>
            </td>
          </tr>
          <tr>
            <td style="text-align: right" class="style20">Cargar material adicional de apoyo</td>
            <td class="style38">
              <asp:FileUpload ID="RutaArchivo2" runat="server" />
            </td>
            <td class="style18">*Al insertar o actualizar se cargar� el archivo</td>
          </tr>
          <tr>
            <td style="text-align: right" class="style20">Tipo de archivo adicional</td>
            <td class="style38">
              <asp:DropDownList ID="DDLtipoarchivo2" runat="server" Width="100px">
                <asp:ListItem>Ninguno</asp:ListItem>
                <asp:ListItem>Imagen</asp:ListItem>
                <asp:ListItem>PDF</asp:ListItem>
                <asp:ListItem>ZIP</asp:ListItem>
                <asp:ListItem>Word</asp:ListItem>
                <asp:ListItem>Excel</asp:ListItem>
                <asp:ListItem Value="PP">Power Pont</asp:ListItem>
                <asp:ListItem>Video</asp:ListItem>
                <asp:ListItem>Audio</asp:ListItem>
              </asp:DropDownList>
              <span class="style19">(usar im�genes de m�x. 350x350 px)</span></td>
            <td class="style18">
              <asp:HyperLink ID="HLarchivo2" runat="server"
                Style="color: #0000CC; font-weight: 700" Target="_blank">No hay archivo de apoyo cargado</asp:HyperLink>
            </td>
          </tr>
          <tr>
            <td style="text-align: right" class="style20">&nbsp;</td>
            <td   style="text-align: left;">
              <asp:CheckBox ID="CBtiempo" ClientIDMode="Static" runat="server"
                Style="text-align: left; font-weight: 700; font-family: Arial, Helvetica, sans-serif; font-style: italic;"
                Text="Asignar tiempo para responder"  />
            </td>
            <td id="SnippetTiempo" runat="server" class="panelTiempo" valign="middle" style="display:none;">
              <asp:Panel ID="PnlMin" runat="server">
                <asp:Label ID="LblMin" runat="server"
                  Text="Indique los minutos: "></asp:Label>
                <asp:TextBox ID="TBmin" runat="server" Width="100px"></asp:TextBox>
                <asp:NumericUpDownExtender ID="TBmin_NumericUpDownExtender" runat="server"
                  Enabled="True" Maximum="30" Minimum="0.5" RefValues="" ServiceDownMethod=""
                  ServiceDownPath="" ServiceUpMethod="" Step="0.5" Tag="" TargetButtonDownID=""
                  TargetButtonUpID="" TargetControlID="TBmin" Width="50">
                </asp:NumericUpDownExtender>
              </asp:Panel>
            </td>
          </tr>
          <tr>
            <td style="text-align: right" class="style20">&nbsp;</td>
            <td colspan="2" id="COcultar" style="text-align: left">
              <asp:CheckBox ID="CBocultar" ClientIDMode="Static" runat="server"
                Style="text-align: left; font-weight: 700; font-family: Arial, Helvetica, sans-serif; font-style: italic;"
                Text="Reactivo exclusivo para Actividades de Pr�ctica (demo)" />
            </td>
          </tr>
          <tr>
            <td style="text-align: right" class="style20">&nbsp;</td>
            <td colspan="2" style="text-align: left">
              <asp:CheckBox ID="CBescondetexto" runat="server"
                Style="text-align: left; font-weight: 700; font-family: Arial, Helvetica, sans-serif; font-style: italic;"
                Text="Esconder redacci�n del Planteamiento" />
            </td>
          </tr>
          <tr>
            <td style="text-align: right" class="style20">&nbsp;&nbsp;</td>
            <td colspan="2" style="text-align: left" id="SVuelta">
              <asp:CheckBox ID="CB2vuelta" ClientIDMode="Static"  runat="server"
                Style="text-align: left; font-weight: 700; font-family: Arial, Helvetica, sans-serif; font-style: italic;"
                Text="Permitir segunda vuelta al reactivo" Checked="True" />
            </td>
          </tr>
            <tr>
            <td style="text-align: right" class="style20">&nbsp;&nbsp;</td>
            <td colspan="2" style="text-align: left">
              <asp:CheckBox ID="CBRecorder" runat="server"
                Style="text-align: left; font-weight: 700; font-family: Arial, Helvetica, sans-serif; font-style: italic;"
                Text="Habilitar grabador de voz" />
            </td>
          </tr>
          <tr>
            <td class="style23" colspan="3">
            Si desea poner una referencia del reactivo, capture los siguientes datos:
          </tr>
          <tr>
            <td class="style20">Libro(s)</td>
            <td class="style16" colspan="2">
              <asp:TextBox ID="TBlibro" runat="server" MaxLength="100" Width="310px"></asp:TextBox>
            </td>
          </tr>
          <tr>
            <td class="style20">Autor(es)</td>
            <td class="style16" colspan="2">
              <asp:TextBox ID="TBautor" runat="server" MaxLength="100" Width="310px"></asp:TextBox>
            </td>
          </tr>
          <t|>
            <td class="style20">Editorial(es)</td>
            <td class="style16" colspan="2">
              <asp:TextBox ID="TBeditorial" runat="server" MaxLength="100" Width="310px"></asp:TextBox>
            </td>
          </t|>
          <tr>
            <td class="style20">&nbsp;</td>
            <td colspan="2">
              <table class="style24">
                <tr>
                  <td>
                    <asp:Button ID="Insertar" runat="server" Text="Insertar"
                      CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                    &nbsp;
                            <asp:Button ID="Limpiar" runat="server" Text="Limpiar"
                              CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                    &nbsp;
                            <asp:Button ID="Actualizar" runat="server" Text="Actualizar"
                              CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                    &nbsp;
                            <asp:Button ID="Eliminar" runat="server" Text="Eliminar"
                              CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="8">
        <uc1:msgSuccess runat="server" ID="msgSuccess" />
      </td>
    </tr>
    <tr>
      <td colspan="8">
        <uc1:msgError runat="server" ID="msgError" />
      </td>
    </tr>
    <tr>
      <td colspan="8">
        <table class="style15">
          <tr>
            <td colspan="5">
              <asp:GridView ID="GVplanteamientos" runat="server"
                AllowSorting="True"
                AutoGenerateColumns="False"
                CellPadding="3"
                PageSize="15"
                DataKeyNames="IdPlanteamiento,Libro,Autor,Editorial,EsconderTexto, Redaccion, 
                  Ancho1, Verifica, ComparaMayusculas, ComparaAcentos, ComparaSoloAlfanumerico,
                  HabilitaRecorder,EliminaPuntuacion "
                DataSourceID="SDSdatosplant"
                Width="890px"
                CssClass="dataGrid_clear_selectable"
                GridLines="None">
                <Columns>
                  <asp:CommandField ShowSelectButton="true" ItemStyle-CssClass="selectCell" />
                  <asp:BoundField DataField="IdPlanteamiento" HeaderText="Id-Plant."
                    InsertVisible="False" ReadOnly="True" SortExpression="IdPlanteamiento" />
                  <asp:BoundField DataField="IdSubtema" HeaderText="Id-Subtema"
                    SortExpression="IdSubtema" Visible="False" />
                  <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                    SortExpression="Consecutivo" />
                  <asp:BoundField DataField="Redaccion" HeaderText="Redacci�n del planteamiento"
                    SortExpression="Redaccion" />
                  <asp:BoundField DataField="TipoRespuesta" HeaderText="Tipo Resp."
                    SortExpression="TipoRespuesta" />
                  <asp:BoundField DataField="Ponderacion" HeaderText="Ponderaci�n"
                    SortExpression="Ponderacion" />
                  <asp:BoundField DataField="PorcentajeRestarResp" HeaderText="% Restar"
                    SortExpression="PorcentajeRestarResp" />
                  <asp:BoundField DataField="ArchivoApoyo" HeaderText="Archivo Apoyo 1"
                    SortExpression="ArchivoApoyo" />
                  <asp:BoundField DataField="TipoArchivoApoyo" HeaderText="Tipo de Archivo 1"
                    SortExpression="TipoArchivoApoyo" />
                  <asp:BoundField DataField="ArchivoApoyo2" HeaderText="Archivo Apoyo 2"
                    SortExpression="ArchivoApoyo2" />
                  <asp:BoundField DataField="TipoArchivoApoyo2" HeaderText="Tipo de Archivo 2"
                    SortExpression="TipoArchivoApoyo2" />
                  <asp:BoundField DataField="Libro" HeaderText="Libro" SortExpression="Libro"
                    Visible="False" />
                  <asp:BoundField DataField="Autor" HeaderText="Autor" SortExpression="Autor"
                    Visible="False" />
                  <asp:BoundField DataField="Editorial" HeaderText="Editorial"
                    SortExpression="Editorial" Visible="False" />
                  <asp:BoundField DataField="Ocultar" HeaderText="Ocultar"
                    SortExpression="Ocultar" />
                  <asp:BoundField DataField="Tiempo" HeaderText="Tiempo"
                    SortExpression="Tiempo" />
                  <asp:BoundField DataField="Minutos" DataFormatString="{0:0.0}"
                    HeaderText="Minutos" SortExpression="Minutos">
                    <ItemStyle HorizontalAlign="Right" />
                  </asp:BoundField>
                  <asp:BoundField DataField="Permite2" HeaderText="2da. Vuelta" SortExpression="Permite2" />
                </Columns>
                <FooterStyle CssClass="footer" />
                <PagerStyle CssClass="pager" />
                <SelectedRowStyle CssClass="selected" />
                <HeaderStyle CssClass="header" />
                <AlternatingRowStyle CssClass="altrow" />
              </asp:GridView>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="8" style="text-align: left;">
        <asp:HyperLink ID="HyperLink1" runat="server"
          NavigateUrl="~/"
          CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Men� </asp:HyperLink>
      </td>
    </tr>
  </table>

  <table class="dataSources">
    <tr>
      <td></td>
      <td>

        <asp:SqlDataSource ID="SDSniveles" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>

      </td>
      <td>

        <asp:SqlDataSource ID="SDSgrados" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
          <SelectParameters>
            <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
              PropertyName="SelectedValue" Type="Int32" />
          </SelectParameters>
        </asp:SqlDataSource>

      </td>
      <td>

        <asp:SqlDataSource ID="SDSasignaturas" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT [IdAsignatura], [IdGrado], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) ORDER BY [Descripcion]">
          <SelectParameters>
            <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
              PropertyName="SelectedValue" Type="Int32" />
          </SelectParameters>
        </asp:SqlDataSource>

      </td>
    </tr>
    <tr>
      <td>

        <asp:SqlDataSource ID="SDStemas" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT [IdTema], [IdAsignatura], Cast([Numero] as varchar(8))+ ' ' + [Descripcion] NomTema  FROM [Tema] WHERE ([IdAsignatura] = @IdAsignatura)
order by [Numero],[Descripcion]">
          <SelectParameters>
            <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
              PropertyName="SelectedValue" Type="Int32" />
          </SelectParameters>
        </asp:SqlDataSource>

      </td>
      <td>

        <asp:SqlDataSource ID="SDSsubtemas" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT [IdSubtema], [IdTema], Cast([Numero] as varchar(8)) + ' ' + [Descripcion] NomSubtema FROM [Subtema] WHERE ([IdTema] = @IdTema)
order by [Numero],[Descripcion]">
          <SelectParameters>
            <asp:ControlParameter ControlID="DDLtemas" Name="IdTema"
              PropertyName="SelectedValue" />
          </SelectParameters>
        </asp:SqlDataSource>

      </td>
      <td>

        <asp:SqlDataSource ID="SDSdatosplant" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT * FROM [Planteamiento] WHERE ([IdSubtema] = @IdSubtema)">
          <SelectParameters>
            <asp:ControlParameter ControlID="DDLsubtemas" Name="IdSubtema"
              PropertyName="SelectedValue" Type="Int32" />
          </SelectParameters>
        </asp:SqlDataSource>

      </td>
        <td>
             <asp:SqlDataSource ID="SDSplanteamientos" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT * FROM [Planteamiento]">
          <InsertParameters>
            <asp:Parameter Direction="Output" Name="NuevoId" Size="4" Type="Int16" />
          </InsertParameters>
        </asp:SqlDataSource>
        </td>
    </tr>
    <tr>
      <td>

        <asp:SqlDataSource ID="SDSopciones" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT * FROM [Opcion]"></asp:SqlDataSource>

      </td>
      <td>
        <asp:HiddenField ID="HfWidth" ClientIDMode="Static" runat="server" Value="180" />
      </td>
      <td>
        <asp:HiddenField ID="HfMayusculas" ClientIDMode="Static" runat="server" Value="false" />
      </td>
      <td>
        <asp:HiddenField ID="HfVerifica" ClientIDMode="Static" runat="server" Value="false" />
      </td>
    </tr>
    <tr>
      <td>
        <asp:HiddenField ID="HfAcentos" ClientIDMode="Static" runat="server" Value="false" />
      </td>
      <td>
        <asp:HiddenField ID="HfAlfanumerico" ClientIDMode="Static" runat="server" Value="false" />
      </td>
         <td>
        <asp:HiddenField ID="HfPuntuacion" ClientIDMode="Static" runat="server" Value="false" />
      </td>
    </tr>
  </table>
</asp:Content>

