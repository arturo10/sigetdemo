﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="Calificaciones.aspx.vb"
    Inherits="superadministrador_Calificaciones"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">

        .titleColumn {
            text-align: right;
        }

        .control_column {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Crear Calificaciones (Parciales de <asp:Label ID="Label7" runat="server">[ASIGNATURAS]</asp:Label>)
    </h1>

</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" runat="Server">

    <table>
        <tr>
            <td colspan="2">
                <b style="text-align: left">Primero Seleccione la
                    <asp:Label ID="Label10" runat="server" Text="[ASIGNATURA]" />
                    donde se manejarán las Calificaciones</b>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Label ID="Label1" runat="server" Text="[CICLO]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Width="300px"
                    CssClass="style41" Height="22px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Label ID="Label2" runat="server" Text="[NIVEL]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="ddlNivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Width="300px" Height="22px" CssClass="style41">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Label ID="Label3" runat="server" Text="[GRADO]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="ddlGrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion"
                    DataValueField="IdGrado" Width="300px" Height="22px"
                    CssClass="style41">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Label ID="Label4" runat="server" Text="[ASIGNATURA]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="ddlAsignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSasignaturas" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Width="300px" Height="22px"
                    CssClass="style41">
                </asp:DropDownList>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <asp:Panel ID="pnlCalificaciones" runat="server" Visible="false">
                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:LinkButton ID="btnNueva" runat="server"
                                    CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                                    <asp:Image ID="imgNueva" runat="server"
                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/add.png"
                                        CssClass="btnIcon"
                                        Height="24" />
                                    <asp:Label ID="lblNueva" runat="server">
                                        Crear
                                    </asp:Label>
                                </asp:LinkButton>
                                &nbsp;
                                <asp:LinkButton ID="btnModificar" runat="server"
                                    Visible="False" CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                                    <asp:Image ID="imgModificar" runat="server"
                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/disk.png"
                                        CssClass="btnIcon"
                                        Height="24" />
                                    <asp:Label ID="lblModificar" runat="server">
                                        Modificar
                                    </asp:Label>
                                </asp:LinkButton>
                                &nbsp;
                                <asp:LinkButton ID="btnEliminar" runat="server"
                                    Visible="False" CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                                    <asp:Image ID="imgEliminar" runat="server"
                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/delete.png"
                                        CssClass="btnIcon"
                                        Height="24" />
                                    <asp:Label ID="lblEliminar" runat="server">
                                        Eliminar
                                    </asp:Label>
                                </asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <uc1:msgSuccess runat="server" ID="msgSuccess" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <uc1:msgError runat="server" ID="msgError" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <uc1:msgInfo runat="server" ID="msgInfo" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:GridView ID="gvCalificaciones" runat="server"
                                    AllowPaging="True"
                                    AllowSorting="true"
                                    AutoGenerateColumns="False"

                                    DataKeyNames="IdCalificacion,IdIndicador"
                                    DataSourceID="SDScalificaciones"
                                    Width="959px"

                                    CssClass="dataGrid_clear_selectable"
                                    GridLines="None">
                                    <Columns>
                                        <asp:CommandField
                                            ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                                        <asp:BoundField DataField="IdCalificacion" HeaderText="Id"
                                            Visible="False" ReadOnly="True" SortExpression="IdCalificacion" />
                                        <%--<asp:BoundField DataField="IdCicloEscolar" HeaderText="Id_[Ciclo]"
                                            SortExpression="IdCicloEscolar" />--%>
                                        <asp:BoundField
                                            DataField="Descripcion"
                                            HeaderText="Descripcion"
                                            SortExpression="Descripcion" />
                                        <asp:BoundField
                                            DataField="Clave"
                                            HeaderText="Clave"
                                            SortExpression="Clave" />
                                        <asp:BoundField
                                            DataField="Consecutivo"
                                            HeaderText="Consecutivo"
                                            SortExpression="Consecutivo">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <%--<asp:BoundField DataField="Materia" HeaderText="[Asignatura]"
                                            SortExpression="Materia" />--%>
                                        <asp:BoundField DataField="Valor" HeaderText="Valor" SortExpression="Valor">
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <%--<asp:BoundField DataField="IdIndicador" HeaderText="IdIndicador"
                                            SortExpression="IdIndicador" Visible="False" />--%>
                                        <asp:BoundField DataField="DescripcionIndicador" HeaderText="Indicador"
                                            SortExpression="DescripcionIndicador" />
                                        <asp:BoundField DataField="FechaModif" HeaderText="Fecha de Modificación"
                                            Visible="False" SortExpression="FechaModif" />
                                        <asp:BoundField DataField="Modifico" HeaderText="Modificó"
                                            Visible="False" SortExpression="Modifico" />
                                    </Columns>
                                    <FooterStyle CssClass="footer" />
                                    <PagerStyle CssClass="pager" />
                                    <SelectedRowStyle CssClass="selected" />
                                    <HeaderStyle CssClass="header" />
                                    <AlternatingRowStyle CssClass="altrow" />
                                </asp:GridView>
                                <%-- sort NO - prerender
	                                0  select
	                                1  idcalificacion
	                                2  id_CICLO
	                                3  descripcion
	                                4  clave
	                                5  consecutivo
	                                6  ASIGNATURA
	                                7  valor
	                                8  idindicador
                                --%>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
            </td>
        </tr>
    </table>

    <asp:Panel ID="pnlDatosCalificacion" runat="server" CssClass="dialogOverwrite dialogRespuestaAbierta">
        <table>
            <tr>
                <td class="titleColumn">Nombre para la Calificación
                </td>
                <td class="control_column">
                    <asp:TextBox ID="tbCaldescripcion" runat="server" Width="290px"
                        MaxLength="400" CssClass="style41"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="titleColumn">Abreviatura para reportes
                </td>
                <td class="control_column">
                    <asp:TextBox ID="tbCalClave" runat="server" MaxLength="6"
                        Width="64px" CssClass="style41"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="titleColumn">Porcentaje de la 
                    <asp:Label ID="Label6" runat="server" Text="ASIGNATURA"></asp:Label> 
                </td>
                <td class="control_column">
                    <asp:TextBox ID="tbCalValor" runat="server" Width="80px"
                        CssClass="style41"></asp:TextBox>
                    <asp:Image ID="Image6" runat="server"
                        Title=
"Qué porción representa. 
La suma de todas las Calificaciones debe ser 100."
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                        CssClass="infoPopper" />
                </td>
            </tr>
            <tr>
                <td class="titleColumn">Consecutivo <span style="font-size: x-small;">(opcional)</span>
                </td>
                <td class="control_column">
                    <asp:TextBox ID="tbCalconsecutivo" runat="server" Width="80px"
                        CssClass="style41"></asp:TextBox>
                    <asp:Image ID="Image2" runat="server"
                        Title=
"El consecutivo afecta el orden de aparición en el menú de actividades del Alumno.
Mientras menor sea el consecutivo mayor es la prioridad de aparición. 
De no haber consecutivo (o entre iguales), se ordenan alfabéticamente."
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                        CssClass="infoPopper" />
                </td>
            </tr>
            <tr>
                <td class="titleColumn">Indicador específico <span style="font-size: x-small;">(opcional)</span>
                </td>
                <td class="control_column">
                    <asp:DropDownList ID="ddlCalIndicador" runat="server"
                        DataSourceID="SDSIndicadores" DataTextField="Descripcion"
                        DataValueField="IdIndicador" Width="280px">
                    </asp:DropDownList>
                    <asp:Image ID="Image1" runat="server"
                        Title=
"Especifica un indicador que será utilizado para clasificar los resultados de las actividades en rangos."
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                        CssClass="infoPopper" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:msgError runat="server" ID="msgErrorDialog" />
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:LinkButton ID="btnAceptarCrear" runat="server"
                        Visible="false" CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                        <asp:Image ID="imgAceptarCrear" runat="server"
                            ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/accept.png"
                            CssClass="btnIcon"
                            Height="24" />
                        <asp:Label ID="lblAceptarCrear" runat="server">
                        Crear
                        </asp:Label>
                    </asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="btnAceptarModificar" runat="server"
                        Visible="False" CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                        <asp:Image ID="imgAceptarModificar" runat="server"
                            ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/accept.png"
                            CssClass="btnIcon"
                            Height="24" />
                        <asp:Label ID="lblAceptarModificar" runat="server">
                            Guardar Cambios
                        </asp:Label>
                    </asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="btnCancelar" runat="server"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                        <asp:Image ID="imgCalCancelar" runat="server"
                            ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/cancel.png"
                            CssClass="btnIcon"
                            Height="24" />
                        <asp:Label ID="lblCancelar" runat="server">
                            Cancelar
                        </asp:Label>
                    </asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="btnCrearModificar" runat="server"
                        Visible="False" CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                        <asp:Image ID="Image11" runat="server"
                            ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/add.png"
                            CssClass="btnIcon"
                            Height="24" />
                        <asp:Label ID="Label5" runat="server">
                            Crear Nuevo
                        </asp:Label>
                    </asp:LinkButton>
                    <div class="prueba"></div>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar] WHERE ([Estatus] = 'Activo') ORDER BY [FechaInicio]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSIndicadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdIndicador], [Descripcion] FROM [Indicador] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT C.IdCalificacion, C.IdCicloEscolar, C.Clave, C.IdAsignatura, A.Descripcion Materia, C.Consecutivo,C.Descripcion, C.Valor, C.IdIndicador, I.Descripcion AS DescripcionIndicador, C.FechaModif, C.Modifico
FROM Calificacion C LEFT JOIN Indicador I ON C.IdIndicador = I.IdIndicador, Asignatura A
WHERE ([IdCicloEscolar] = @IdCicloEscolar)  and (A.IdAsignatura = C.IdAsignatura) and (A.IdAsignatura = @IdAsignatura)
ORDER BY C.Consecutivo, C.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdAsignatura], [IdGrado], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="HF1" runat="server" Text="HF1" />
                <asp:ModalPopupExtender ID="ModalPopupCalificacion" runat="server" 
                    PopupControlID="pnlDatosCalificacion" TargetControlID="HF1" BackgroundCssClass="overlay">
                </asp:ModalPopupExtender>
            </td>
            <td>
            </td>
            <td></td>
        </tr>
    </table>
</asp:Content>

