﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="AsignaNiveles.aspx.vb"
    Inherits="superadministrador_AsignaNiveles"
    MaintainScrollPositionOnPostback="true"
      EnableEventValidation="false" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
            height: 446px;
        }

        .style17 {
            height: 109px;
        }

        .style13 {
            width: 67%;
        }

        .style15 {
            text-align: left;
        }

        .style16 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: 700;
            color: #000066;
        }

        .style14 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style21 {
            text-align: left;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            height: 8px;
        }

        .style18 {
            height: 25px;
        }

        .style23 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            color: #000000;
            font-weight: normal;
        }

        .style24 {
            color: #000000;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style25 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
        }

        .style26 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 48px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Asignación de 
								<asp:Label ID="Label1" runat="server" Text="[NIVELES]" />
        a 
								<asp:Label ID="Label2" runat="server" Text="[PLANTELES]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style17" colspan="4">
                <table class="style13">
                    <tr>
                        <td class="style15" colspan="2">
                            <asp:Label ID="Mensaje" runat="server" CssClass="style16">Capture los datos</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style23">
                            <asp:Label ID="Label3" runat="server" Text="[INSTITUCION]" />

                        </td>
                        <td style="text-align: left;">
                            <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                DataValueField="IdInstitucion" Height="22px" Width="330px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style23">
                            <asp:Label ID="Label4" runat="server" Text="[PLANTEL]" />

                        </td>
                        <td style="text-align: left;">
                            <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                DataValueField="IdPlantel" Height="22px" Width="330px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style25" colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style25">Nuevo
                            <asp:Label ID="Label5" runat="server" Text="[NIVEL]" />
                            a asignar

                        </td>
                        <td style="text-align: left;">
                            <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSniveles" DataTextField="Descripcion"
                                DataValueField="IdNivel" Height="22px" Width="330px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style25">&nbsp;</td>
                        <td>
                            <asp:Button ID="Asignar" runat="server" Text="Asignar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style25" colspan="2">
                            <asp:GridView ID="GVnivelesasignados" runat="server"
                                AllowPaging="True"
                                AutoGenerateColumns="False"
                                DataSourceID="SDSescuelas"
                                DataKeyNames="IdEscuela"
                                Caption="<h3>NIVELES asignados</h3>"
                                PageSize="5"

                                BackColor="White"
                                BorderColor="#999999"
                                BorderStyle="Solid"
                                BorderWidth="1px"
                                CellPadding="3"
                                GridLines="Vertical"
                                Style="font-size: small; font-family: Arial, Helvetica, sans-serif; text-align: left;"
                                ForeColor="Black"
                                Width="862px">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True">
                                        <HeaderStyle Width="80px" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="IdPlantel" HeaderText="Id Plantel"
                                        InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel">
                                        <HeaderStyle HorizontalAlign="Right" Width="90px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Descripcion" HeaderText="Plantel"
                                        SortExpression="Descripcion" />
                                    <asp:BoundField DataField="IdNivel" HeaderText="Id Nivel" InsertVisible="False"
                                        ReadOnly="True" SortExpression="IdNivel">
                                        <HeaderStyle HorizontalAlign="Right" Width="70px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Descripcion1" HeaderText="Nivel asignado"
                                        SortExpression="Descripcion1" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                            <%-- sort NO - prerender
	                            0  select
	                            1  id_PLANTEL
	                            2  PLANTEL
	                            3  id_NIVEL
	                            4  NIVEL asignado
	                            --%>
                        </td>
                    </tr>
                    <tr>
                        <td class="style21">&nbsp;</td>
                        <td class="style21">
                            <asp:Button ID="Desasignar" runat="server" Text="Desasignar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: left;">
                            <uc1:msgError runat="server" ID="msgError" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style14">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: left">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Institucion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Nivel]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSescuelas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT E.IdEscuela,P.IdPlantel, P.Descripcion,N.IdNivel, N.Descripcion
FROM Escuela E, Nivel N, Plantel P
where P.IdPlantel = @IdPlantel and E.IdPlantel = P.IdPlantel and E.IdNivel = N.IdNivel
">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

