﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="Competencias.aspx.vb" 
    Inherits="superadministrador_Competencias" 
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style10
        {
            width: 100%;
            height: 356px;
        }
        .style15
        {
            font-weight: normal;
            font-family: Arial, Helvetica, sans-serif;
        }
        .style11
        {
            height: 147px;
        }
        .style16
        {
            height: 64px;
        }
        .style23
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
        .style24
        {
            font-size: small;
            font-weight: bold;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Administración de Competencias
		</h1>
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style10">
        <tr>
            <td>
                <span class="style24"><span class="style15">Indique los Estatus: Activo, Suspendido o Baja</span></span></td>
        </tr>
        <tr>
            <td class="style11">
                <asp:DetailsView ID="DVcompetencias" runat="server" AllowPaging="True" 
                        AutoGenerateRows="False" BackColor="White" BorderColor="#999999" 
                        BorderStyle="Solid" BorderWidth="1px" CellPadding="3" 
                        DataKeyNames="IdCompetencia" DataSourceID="LDScompetencias" Font-Names="Arial" 
                        Font-Size="Small" GridLines="Vertical" Height="50px" Width="625px" 
                    ForeColor="Black">
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <Fields>
                        <asp:BoundField DataField="IdCompetencia" HeaderText="Id del Sistema" 
                                InsertVisible="False" ReadOnly="True" SortExpression="IdCompetencia" >
                        <HeaderStyle Width="210px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Nombre de la Competencia" 
                                SortExpression="Descripcion" />
                        <asp:BoundField DataField="Area" HeaderText="Clasificación de la Competencia" 
                                SortExpression="Area" />
                        <asp:BoundField DataField="FechaModif" HeaderText="FechaModif" 
                                SortExpression="FechaModif" Visible="False" />
                        <asp:BoundField DataField="Modifico" HeaderText="Modifico" 
                                SortExpression="Modifico" Visible="False" />
                        <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                                ShowInsertButton="True" />
                    </Fields>
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:DetailsView>
            </td>
        </tr>
        <tr>
            <td class="style16">
              <asp:LinqDataSource ID="LDScompetencias" runat="server"
                ContextTypeName="CompetenciasDataContext" EnableDelete="True"
                EnableInsert="True" EnableUpdate="True" TableName="Competencias" EntityTypeName="">
                </asp:LinqDataSource>
                <asp:SqlDataSource ID="SDScompetencia" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [Competencia]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HyperLink ID="HyperLink2" runat="server"
					NavigateUrl="~/" 
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

