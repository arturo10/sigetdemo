﻿Imports Siget

Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Partial Class superadministrador_GeneraProfesores
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.PROFESORES
    End Sub

    Protected Sub Importar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Importar.Click
        If ArchivoCarga.HasFile Then 'El atributo .HasFile compara si se indico un archivo           
            Try
                Dim Todobien = True
                If CBregistrarme.Checked Then
                    If Trim(TBemail.Text) = "" Then
                        msgError.show("Requiere escribir una cuenta de Email para registrar a los " & Config.Etiqueta.PROFESORES)
                        Todobien = False
                    End If
                End If
                If Todobien Then
                    LblMensaje.Text = "Espere un momento por favor, la migración puede tardar varios minutos..."
                    'Ahora CARGO el archivo
                    ArchivoCarga.SaveAs(Server.MapPath("~") + "\superadministrador\" & ArchivoCarga.FileName)
                    'Leo el archivo
                    Dim archivo_datos As String = Server.MapPath(ArchivoCarga.FileName())
                    Dim leer_archivo As StreamReader
                    leer_archivo = File.OpenText(archivo_datos)
                    Dim LineaLeida, IdPlantel, Clave, Nombre, Apellidos, Login, Password As String
                    Dim I, F, IdUsuario As Integer

                    Dim strConexion As String
                    'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                    strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                    Dim objConexion As New SqlConnection(strConexion)
                    Dim miComando As SqlCommand
                    Dim misRegistros As SqlDataReader
                    objConexion.Open()
                    miComando = objConexion.CreateCommand

                    'Pongo el inicio de una tablita donde iré desplegando los profesores creados
                    LblSalida.Text = "<table><tr><td>NOMBRE</td><td>APELLIDOS</td><td>CLAVE</td><td>LOGIN</td><td>PASSWORD</td></tr>"

                    While Not leer_archivo.EndOfStream
                        'Leo el registro completo y ubico cada campo
                        LineaLeida = leer_archivo.ReadLine
                        I = InStr(LineaLeida, ",")
                        F = InStr(I + 1, LineaLeida, ",")
                        IdPlantel = Trim(Mid(LineaLeida, 1, I - 1)) 'CAMPO 1
                        Clave = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 2
                        I = F
                        F = InStr(I + 1, LineaLeida, ",")
                        Nombre = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 3
                        I = F
                        F = InStr(I + 1, LineaLeida, ",")
                        Apellidos = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 4
                        I = F
                        F = InStr(I + 1, LineaLeida, ",")
                        Login = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 5
                        I = F
                        Password = Trim(Mid(LineaLeida, I + 1, Len(LineaLeida))) 'CAMPO 6 (Ultimo)

                        'Ahora ingreso el registro del Usuario
                        'Primero creo una cadena de 3 numeros aleatoria para agregarsela al password
                        Dim aleatorio As Integer
                        Randomize()
                        aleatorio = (Rnd() * 1000) + 1
                        Password = Password + CStr(aleatorio)

                        SDSusuarios.InsertCommand = "SET dateformat dmy; INSERT INTO Usuario (Login,Password,PerfilASP,FechaModif) VALUES ('" + Login + "','" + Password + "','Profesor',getdate())"
                        SDSusuarios.Insert()

                        'Ahora busco el recien ingresado Usuario para obtener su ID                    

                        miComando.CommandText = "SELECT * FROM USUARIO WHERE Login = '" + Login + "'"
                        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                        misRegistros.Read()
                        IdUsuario = misRegistros.Item("IdUsuario")
                        misRegistros.Close()

                        'Ahora ingreso el registro del Profesor
                        SDSprofesor.InsertCommand = "SET dateformat dmy; INSERT INTO Profesor (IdUsuario,IdPlantel,Clave,Nombre,Apellidos,Estatus,FechaModif) VALUES (" + CStr(IdUsuario) + "," + IdPlantel + ",'" + Clave + "','" + Nombre + "','" + Apellidos + "','Activo','" + Date.Today + "')"
                        SDSprofesor.Insert()

                        'Ahora registro el usuario en la base de datos de seguridad
                        If CBregistrarme.Checked Then
                            'Creo el Usuario
                            Dim status As MembershipCreateStatus

                            ' 17/10/2013 se deseleccionó requiresQuestionAndAnswer="false"  de web config, no es necesario
                            'Dim passwordQuestion As String = ""
                            'Dim passwordAnswer As String = ""
                            'If Membership.RequiresQuestionAndAnswer Then
                            '    passwordQuestion = "Empresa desarrolladora de " & Config.Etiqueta.SISTEMA_CORTO
                            '    passwordAnswer = "Integrant"
                            'End If

                            Try
                                Dim newUser As MembershipUser = Membership.CreateUser(Login, Password, _
                                                                               Trim(TBemail.Text), Nothing, _
                                                                               Nothing, True, status)
                                If newUser Is Nothing Then
                                    msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), GetErrorMessage(status))
                                Else
                                    Roles.AddUserToRole(Login, "Profesor")
                                End If
                            Catch ex As MembershipCreateUserException
                                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
                            End Try
                        End If
                        'Pongo los datos del profesor creado en la pantalla
                        LblSalida.Text = LblSalida.Text + "<tr><td>" + Nombre + "</td><td>" + Apellidos + "</td><td>" + Clave + "</td><td>" + Login + "</td><td>" + Password + "</td></tr>"
                    End While
                    LblSalida.Text = LblSalida.Text + "</table>"

                    objConexion.Close()
                    leer_archivo.Close()
                    msgError.hide()
                    LblMensaje.Text = "La migración ha terminado con éxito, revise los registros.<br>"

                    'Elimino el archivo utilizado para la migración
                    Dim MiArchivo As FileInfo = New FileInfo(archivo_datos)
                    MiArchivo.Delete()

                    If CBregistrarme.Checked Then
                        LblMensaje.Text = LblMensaje.Text + "Las cuentas de los " & Config.Etiqueta.PROFESORES & " migrados ya han sido registradas en la base de datos de seguridad"
                    Else
                        LblMensaje.Text = LblMensaje.Text + "Los " & Config.Etiqueta.PROFESORES & " migrados deberán registrarse con su Matrícula y Login para activar su cuenta"
                    End If
                End If 'de Todobien
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                LblMensaje.Text = ""
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try
        Else
            msgError.show("Seleccione el archivo que contiene los datos para Migrar")
        End If
    End Sub

    Public Function GetErrorMessage(ByVal status As MembershipCreateStatus) As String
        Select Case status
            Case MembershipCreateStatus.DuplicateUserName
                Return "El usuario ya existe, elija otro usuario."
            Case MembershipCreateStatus.DuplicateEmail
                Return "Ya existe un usuario para ese email, por favor escriba un email diferente."
            Case MembershipCreateStatus.InvalidPassword
                Return "El password es inválido, pruebe con otro."
            Case MembershipCreateStatus.InvalidEmail
                Return "El formato del email es inválido, por favor revíselo."
            Case MembershipCreateStatus.InvalidAnswer
                Return "La respuesta secreta es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidQuestion
                Return "La pregunta de recuperación de password es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidUserName
                Return "El nombre de usuario es inválido, corríjalo."
            Case MembershipCreateStatus.ProviderError
                Return "La autentificación falló, contacte al administrador del sistema."
            Case MembershipCreateStatus.UserRejected
                Return "La creación del usuario ha sido cancelada, consulte al administrador."
            Case Else
                Return "Ha ocurrido un error desconocido, verifique o contacte al administrador."
        End Select
    End Function

End Class
