﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="Periodos.aspx.vb" 
    Inherits="superadministrador_Periodos" 
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">


        .style11
        {
            width: 100%;
        }
        .style14
        {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }
        .style20
        {
            text-align: right;
            }
        .style18
        {
            width: 183px;
        }
        .style19
        {
            width: 166px;
        }
        .style23
        {
            color: #000000;
        }
        .style24
        {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            color: #FFFFFF;
            height: 8px;
            width: 143px;
            text-align: right;
        }
        .style25
        {
            font-weight: normal;
        }
        .style26
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
        .style27
        {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Administración de Periodos que componen un 
								<asp:Label ID="Label1" runat="server" Text="[CICLO]" />
		</h1>
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style11">
        <tr>
            <td colspan="4">
                <asp:Label ID="Mensaje" runat="server" CssClass="titulo" 
                        style="color: #000099; font-family: Arial, Helvetica, sans-serif;">Capture los datos</asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <table class="estandar">
                    <tr>
                        <td class="style14" colspan="3">
                                <asp:Label ID="Label2" runat="server" Text="[CICLO]" />
														 al que pertenece el Periodo</td>
                        <td class="style15">
                            <asp:DropDownList ID="DDLcicloescolar" runat="server" 
                                    DataSourceID="SDSciclosescolares" DataTextField="Ciclo" 
                                    DataValueField="IdCicloEscolar" Width="250px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style14" colspan="3">
                                Número consecutivo del Periodo</td>
                        <td class="style15">
                            <asp:TextBox ID="TBnumperiodo" runat="server" Width="64px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style20">
                                <br />
                                <span class="style27">Fecha de <b>Inicio</b> del Periodo<br />
                            <asp:TextBox ID="TBinicioperiodo" runat="server" Width="100px" 
                                    style="text-align: right"></asp:TextBox>
                                </span>
                        </td>
                        <td class="style15">
                            <asp:Calendar ID="CalInicioPeriodo" runat="server" BackColor="White" 
                                    BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" 
                                    Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" 
                                    Width="200px">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#808080" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>
                        </td>
                        <td class="style24">
                            <span class="style23"><span class="style25"><span class="style27">Fecha de <b>Finalización</b> del Periodo</span>&nbsp;</span>
                            </span>
                            <asp:TextBox ID="TBfinperiodo" runat="server" Width="100px"></asp:TextBox>
                        </td>
                        <td class="style15">
                            <asp:Calendar ID="CalFinPeriodo" runat="server" BackColor="White" 
                                    BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" 
                                    Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" 
                                    Width="200px">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#808080" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>
                        </td>
                    </tr>
                    <tr>
                        <td class="style16">
                                &nbsp;</td>
                        <td class="style15" colspan="3">
                            <asp:Button ID="Insertar" runat="server" Text="Insertar" 
																CssClass="defaultBtn btnThemeBlue btnThemeMedium"/>
														&nbsp;
                            <asp:Button ID="Limpiar" runat="server" Text="Limpiar" 
																CssClass="defaultBtn btnThemeGrey btnThemeMedium"/>
														&nbsp;
                            <asp:Button ID="Actualizar" runat="server" Text="Actualizar" 
																CssClass="defaultBtn btnThemeBlue btnThemeMedium"/>
														&nbsp;
                            <asp:Button ID="Eliminar" runat="server" Text="Eliminar" 
																CssClass="defaultBtn btnThemeGrey btnThemeMedium"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GVperiodos" runat="server" AllowPaging="True" 
                        AllowSorting="True" AutoGenerateColumns="False" AutoGenerateSelectButton="True" 
                        BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" 
                        CellPadding="3" DataKeyNames="IdPeriodo" DataSourceID="SDSperiodos" 
                        GridLines="Vertical" 
                        style="font-family: Arial, Helvetica, sans-serif; font-size: x-small" 
                        Width="640px" ForeColor="Black" 
                    Caption="<h3>Periodos capturados</h3>">
                    <Columns>
                        <asp:BoundField DataField="IdPeriodo" HeaderText="Id Periodo" 
                                InsertVisible="False" ReadOnly="True" SortExpression="IdPeriodo" />
                        <asp:BoundField DataField="IdCicloEscolar" HeaderText="IdCicloEscolar" 
                                SortExpression="IdCicloEscolar" Visible="False" />
                        <asp:BoundField DataField="NoPeriodo" HeaderText="No Periodo" 
                                SortExpression="NoPeriodo" />
                        <asp:BoundField DataField="FechaInicio" DataFormatString="{0:dd/MM/yyyy}" 
                                HeaderText="Fecha Inicio" SortExpression="FechaInicio" />
                        <asp:BoundField DataField="FechaFin" DataFormatString="{0:dd/MM/yyyy}" 
                                HeaderText="Fecha Fin" SortExpression="FechaFin" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="style18">
                <asp:SqlDataSource ID="SDSciclosescolares" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                        SelectCommand="SELECT IdCicloEscolar, Descripcion + ' (' + Cast(FechaInicio as varchar(12)) + ' - ' + Cast(FechaFin as varchar(12)) + ')' Ciclo
FROM CicloEscolar
WHERE (Estatus = 'Activo')
order by Descripcion"></asp:SqlDataSource>
            </td>
            <td class="style19">
                <asp:SqlDataSource ID="SDSperiodos" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                        
                        
                    SelectCommand="SELECT [IdPeriodo], [IdCicloEscolar], [NoPeriodo], [FechaInicio], [FechaFin] FROM [Periodo] WHERE ([IdCicloEscolar] = @IdCicloEscolar)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar" 
                                PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                    &nbsp;</td>
            <td>
                    &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:HyperLink ID="HyperLink2" runat="server"
					NavigateUrl="~/" 
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

