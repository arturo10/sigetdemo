﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="Errores.aspx.vb" 
    Inherits="superadministrador_Errores"
    MaintainScrollPositionOnPostback="true"  %>

<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" Runat="Server">
    <h1>
        Registro de Excepciones no Controladas de Ejecución
    </h1>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table>
        <tr>
            <td class="separatorLine" colspan="2">
                &nbsp;
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="text-align: left;">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td style="text-align: left;">
                <asp:Button ID="Eliminar" runat="server" 
                    Text="Eliminar Todos"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GVErrores" runat="server"
                    AllowSorting="True"
                    AutoGenerateColumns="True"
                    AutoGenerateSelectButton="False"
                    Caption="<h3>Errores Registrados</h3>"
                    CellPadding="3"
                    ForeColor="Black"
                    GridLines="Vertical"
                    Height="17px"
                    Style="font-size: x-small; text-align: left;"
                    Width="876px"
                    BackColor="White"
                    BorderColor="#999999"
                    BorderStyle="Solid"
                    BorderWidth="1px">
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>

