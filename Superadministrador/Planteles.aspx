﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="Planteles.aspx.vb"
    Inherits="superadministrador_Planteles"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="System.Web.DynamicData, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.DynamicData" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
            height: 450px;
        }

        .style21 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 38px;
        }

        .style14 {
            width: 73%;
        }

        .style15 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
        }

        .style19 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
        }

        .style18 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 31px;
        }

        .style17 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 37px;
        }

        .style16 {
            height: 13px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Administración de
        <asp:Label ID="Label1" runat="server" Text="[PLANTELES]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style11" colspan="5">
                <table class="style14">
                    <tr>
                        <td class="style15" colspan="2">
                            <asp:Label ID="Mensaje" runat="server"
                                Style="font-weight: 700; color: #003399; font-family: Arial, Helvetica, sans-serif;">Capture los datos</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style19">Licencia de Uso</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="DDLlicencia" runat="server" AutoPostBack="True"
                                DataSourceID="SDSlicencias" DataTextField="Clave" DataValueField="IdLicencia"
                                Style="font-family: Arial, Helvetica, sans-serif; font-size: small"
                                Width="250px" Height="22px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style19">
                            <asp:Label ID="Label2" runat="server" Text="[INSTITUCION]" />
                            a la que Pertenece</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                DataValueField="IdInstitucion" Width="350px" Height="22px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style19">Descripción del 
														<asp:Label ID="Label3" runat="server" Text="[PLANTEL]" />
                        </td>
                        <td style="text-align: left">
                            <asp:TextBox ID="TBdescripcion" runat="server"
                                Style="font-family: Arial, Helvetica, sans-serif; font-size: small"
                                Width="350px" MaxLength="60" Height="22px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style19">Clave asignada</td>
                        <td style="text-align: left">
                            <asp:TextBox ID="TBclave" runat="server" MaxLength="15" Width="196px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="Insertar" runat="server"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium"
                                Text="Insertar" />
                            &nbsp;
                            <asp:Button ID="Limpiar" runat="server"
                                Text="Limpiar" CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Actualizar" runat="server" Text="Actualizar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Eliminar" runat="server" Text="Eliminar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <uc1:msgError runat="server" ID="msgError" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style18" colspan="5">
                <asp:GridView ID="GVDatosPlanteles" runat="server"
                    AllowPaging="True"
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 
                    AutoGenerateSelectButton="True"
                    DataSourceID="SDSinformacion" 
                    DataKeyNames="IdPlantel" 
                    Caption="<h3>PLANTELES capturados</h3>"
                    PageSize="20" 

                    CellPadding="3" 
                    ForeColor="Black"
                    GridLines="Vertical" 
                    Height="17px" 
                    Style="font-size: x-small; text-align: left;"
                    Width="876px" 
                    BackColor="White"
                    BorderColor="#999999" 
                    BorderStyle="Solid" 
                    BorderWidth="1px">
                    <Columns>
                        <asp:BoundField DataField="IdLicencia" HeaderText="Id Licencia"
                            SortExpression="IdLicencia" />
                        <asp:BoundField DataField="Clave" HeaderText="Clave Licencia"
                            SortExpression="Clave" />
                        <asp:BoundField DataField="IdInstitucion" HeaderText="Id_[Institución]"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdInstitucion" />
                        <asp:BoundField DataField="Descripcion" HeaderText="[Institución]"
                            SortExpression="Descripcion" />
                        <asp:BoundField DataField="IdPlantel" HeaderText="Id_[Plantel]"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel" />
                        <asp:BoundField DataField="Descripcion1" HeaderText="[Plantel]"
                            SortExpression="Descripcion1" />
                        <asp:BoundField DataField="ClaveP" HeaderText="Clave_[Plantel]"
                            SortExpression="ClaveP" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="style17">
                <asp:SqlDataSource ID="SDSlicencias" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Licencia]"></asp:SqlDataSource>
            </td>
            <td class="style17">
                <asp:SqlDataSource ID="SDScoordinadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdCoordinador, Nombre + ' ' + Apellidos Nom
from Coordinador"></asp:SqlDataSource>
            </td>
            <td class="style17">
                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Plantel]"></asp:SqlDataSource>
            </td>
            <td class="style17">
                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdInstitucion], [Descripcion] FROM [Institucion] ORDER BY [Descripcion]"></asp:SqlDataSource>
            </td>
            <td class="style17">&nbsp;</td>
        </tr>
        <tr>
            <td class="style16" colspan="5">
                <asp:SqlDataSource ID="SDSinformacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select P.IdLicencia, L.Clave, I.IdInstitucion, I.Descripcion, P.IdPlantel, P.Descripcion, P.Clave ClaveP
from Licencia L, Institucion I, Plantel P
where L.IdLicencia = @IdLicencia and P.IdLicencia = L.IdLicencia and P.IdInstitucion = I.IdInstitucion and I.IdInstitucion = @IdInstitucion
order by I.Descripcion, P.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLlicencia" Name="IdLicencia"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style16" colspan="5">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
                <br />
            </td>
        </tr>
    </table>
</asp:Content>

