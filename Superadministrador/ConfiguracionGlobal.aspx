﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="ConfiguracionGlobal.aspx.vb" 
    Inherits="superadministrador_ConfiguracionGlobal"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/PageTitle_v1.ascx" TagPrefix="uc1" TagName="PageTitle_v1" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css" >
        .stateColumn {
            text-align: right;
            padding-right: 20px;
        }

        .featureColumn {
            text-align: left;
        }

        .descriptionColumn {
            padding-bottom: 20px;
            font-size: x-small;
            text-align: center;
        }

        .login_thumb {
            border: 1px solid #666;
            border-width: 1px;
            border-radius: 5px;
            padding: 10px;
            width: 200px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" Runat="Server">
    <h1>
        Configuración del Filesystem
    </h1>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table style="width: 100%;">
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Panel ID="pnlRecargar" runat="server" Visible="false">
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="4" style="text-align: center;">
                                <asp:LinkButton ID="lbRecargar" runat="server"
                                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
                                    <asp:Image ID="lbRecargar_img" runat="server" 
                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/database_refresh.png" 
                                        CssClass="btnIcon" />
                                    Recargar
                                </asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="2" class="descriptionColumn">
                                Obtiene las configuraciones y lenguajes almacenados y prepara el filesystem con ellos, sin reiniciar la aplicación.
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1_GENERAL" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlModelo" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="0">Escuela</asp:ListItem>
                    <asp:ListItem Value="1">Empresa</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Modelo
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                El esquema de trabajo de la aplicación.
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlIdiomaGeneral" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="es-mx">Español</asp:ListItem>
                    <asp:ListItem Value="en-us">English</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Idioma
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                El idioma de las páginas públicas (como la página de Inicio).
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlDEPLOYED" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="true">Habilitado</asp:ListItem>
                    <asp:ListItem Value="false">Deshabilitado</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Habilitación de Registro de Errores
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                Controla si la aplicación almacena o no información de cada error encontrado. Los errores se almacenan en el registro de errores de cada aplicación.
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlMAIL_ACTIVO" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="true">Habilitado</asp:ListItem>
                    <asp:ListItem Value="false">Deshabilitado</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Envío de Correos Electrónicos
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                Controla si la aplicación envía o no correos electrónicos en las funcionalidades de la aplicación que así lo permiten, como durante el envío de mensajes.
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlGUARDAR_RESPUESTA_ABIERTA" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="true">Habilitado</asp:ListItem>
                    <asp:ListItem Value="false">Deshabilitado</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Permite Guardar Respuestas Abiertas
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                Determina si se muestra el botón de "guardar respuesta" al contestar Respuestas Abiertas, así como el checkbox "Mostrar Borradores" al calificar respuestas abiertas.
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlFIRMA_PIE_INTEGRANT" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="true">Visible</asp:ListItem>
                    <asp:ListItem Value="false">Invisible</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Firma Integrant en Pies de Página
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                Determina si se muestra información de Integrant en el pie de las páginas de la aplicación.
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="descriptionColumn" colspan="2">
                <asp:RadioButton ID="RbLogin_Jp1" runat="server" AutoPostBack="true" 
                    GroupName="Login" Checked="true"
                    style="display:inline-block; vertical-align: middle;" /> 
                <asp:Image ID="Image1" runat="server" 
                    CssClass="login_thumb" 
                    ImageUrl="~/Resources/imagenes/login_jp1_thumb.png" 
                    style="display:inline-block; vertical-align: middle;" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:RadioButton ID="RbLogin_A1" runat="server" AutoPostBack="true" 
                    GroupName="Login" 
                    style="display:inline-block; vertical-align: middle;" /> 
                <asp:Image ID="Image2" runat="server" 
                    CssClass="login_thumb" 
                    ImageUrl="~/Resources/imagenes/login_a1_thumb.png" 
                    style="display:inline-block; vertical-align: middle;" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                Diseño del Inicio de Sesión
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1_Alumno" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlIdiomaAlumno" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="es-mx">Español</asp:ListItem>
                    <asp:ListItem Value="en-us">English</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Idioma
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                El idioma para este perfil.
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlALUMNO_CONTESTAR_RENUNCIAR" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="true">Visible</asp:ListItem>
                    <asp:ListItem Value="false">Invisible</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Visibilidad del botón de renuncia
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                Controla la visibilidad del botón de renuncia en el perfil del alumno durante la realización de una actividad.
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlALUMNO_PENDIENTES_COLUMNA_TIPO" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="true">Visible</asp:ListItem>
                    <asp:ListItem Value="false">Invisible</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Visibilidad de columna tipo de actividad
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                Determina si en la página de pendientes del alumno aparece la columna de tipo de actividad.
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="true">Visible</asp:ListItem>
                    <asp:ListItem Value="false">Invisible</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Visibilidad de columna fecha límite sin penalización
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                Determina si en la página de pendientes del alumno aparece la columna de fecha de penalización.
            </td>
            <td>&nbsp;</td>
        </tr>

         <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
               <asp:DropDownList ID="ddlALUMNO_PENDIENTES_COLUMNA_ENTREGA" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="true">Visible</asp:ListItem>
                    <asp:ListItem Value="false">Invisible</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Visibilidad de columna Entrega de documento
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                Determina si en la página de pendientes del alumno aparece la columna de entrega
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="true">Visible</asp:ListItem>
                    <asp:ListItem Value="false">Invisible</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Visibilidad de columna fecha porcentaje de penalización
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                Determina si en la página de pendientes del alumno aparece la columna de porcentaje de penalización.
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlALUMNO_DOCUMENTOPROTEGIDO" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="true">Protegido</asp:ListItem>
                    <asp:ListItem Value="false">Sin Proteger</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Proteger Documentos Contra Descarga
            </td>
            <td>&nbsp;</td>
        </tr>
         <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                Determina si los documentos en pdf estan protegidos contra descarga.
            </td>
            <td>&nbsp;</td>
        </tr>
         <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlALUMNO_INSCRIBIR_CURSOS" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="true">Visible</asp:ListItem>
                    <asp:ListItem Value="false">No Visible</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Visibilidad para Inscribir Cursos
            </td>
            <td>&nbsp;</td>
        </tr>
         <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                Determina la visibilidad del área de inscripción de Cursos
            </td>
            <td>&nbsp;</td>
        </tr>
         <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddl_ContraObligatoria" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="true">Obligatoria</asp:ListItem>
                    <asp:ListItem Value="false">No Obligatoria</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Cambiar contraseña obligatoria al entrar por primera vez 
            </td>
            <td>&nbsp;</td>
        </tr>
         <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                Determina si un alumno debe de cambiar obligatoriamente su contraseña
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1_Profesor" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlIdiomaProfesor" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="es-mx">Español</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Idioma
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                El idioma para este perfil.
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlPROFESOR_CALIFICA_NUMERICO" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="0">0 - 100</asp:ListItem>
                    <asp:ListItem Value="1">Bien - Mal</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Escala de calificaciones para respuestas abiertas
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                Determina si en las opciones de calificar respuesta abierta del profesor aparecen calificaciones 0-100 o escalas bien-mal.
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlPROFESOR_CALIFICA_DOCUMENTO" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="0">0 - 100</asp:ListItem>
                    <asp:ListItem Value="1">Competente - No Competente</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Escala de calificaciones para documentos
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                Determina si en las opciones de calificar documento del profesor aparecen calificaciones 0-100 o escalas competente-nocompetente
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1_Coordinador" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlIdiomaCoordinador" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="es-mx">Español</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Idioma
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                El idioma para este perfil.
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1_Administrador" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="stateColumn">
                <asp:DropDownList ID="ddlIdiomaAdministrador" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="es-mx">Español</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="featureColumn">
                Idioma
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="descriptionColumn">
                El idioma para los perfiles Administrador, Operador y Capturista.
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

