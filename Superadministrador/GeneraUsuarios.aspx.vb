﻿Imports Siget

Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Partial Class superadministrador_GeneraUsuarios
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.ALUMNOS
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Genera.Click
        If ArchivoCarga.HasFile Then 'El atributo .HasFile compara si se indico un archivo           
            Try
                LblMensaje.Text = "Espere un momento por favor, la migración puede tardar varios minutos..."
                'Ahora CARGO el archivo
                ArchivoCarga.SaveAs(Server.MapPath("~") + "\superadministrador\" & ArchivoCarga.FileName)
                'Leo el archivo
                Dim archivo_datos As String = Server.MapPath(ArchivoCarga.FileName())
                Dim leer_archivo As StreamReader
                leer_archivo = File.OpenText(archivo_datos)
                Dim LineaLeida, IdPlantel, IdGrupo, Nombre, ApPaterno, ApMaterno, Matricula, Email As String
                Dim I, F, IdUsuario, Cont As Integer

                Cont = 0
                Dim strConexion As String
                'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                Dim objConexion As New SqlConnection(strConexion)
                Dim miComando As SqlCommand
                Dim misRegistros As SqlDataReader
                objConexion.Open()
                miComando = objConexion.CreateCommand

                While Not leer_archivo.EndOfStream
                    Cont = Cont + 1 'Utilizare este contador como parte del nombre de Usuario
                    'Leo el registro completo y ubico cada campo
                    LineaLeida = leer_archivo.ReadLine
                    I = InStr(LineaLeida, ",")
                    F = InStr(I + 1, LineaLeida, ",")
                    IdPlantel = Trim(Mid(LineaLeida, 1, I - 1)) 'CAMPO 1
                    IdGrupo = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 2
                    I = F
                    F = InStr(I + 1, LineaLeida, ",")
                    Nombre = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 3
                    I = F
                    F = InStr(I + 1, LineaLeida, ",")
                    ApPaterno = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 4
                    I = F
                    F = InStr(I + 1, LineaLeida, ",")
                    ApMaterno = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 5
                    I = F
                    F = InStr(I + 1, LineaLeida, ",")
                    Matricula = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 6
                    I = F
                    Email = Trim(Mid(LineaLeida, I + 1, Len(LineaLeida))) 'CAMPO 7 (Ultimo)

                    'Creo el Usuario
                    Dim status As MembershipCreateStatus

                    ' 17/10/2013 se deseleccionó requiresQuestionAndAnswer="false"  de web config, no es necesario
                    'Dim passwordQuestion As String = ""
                    'Dim passwordAnswer As String = ""
                    'If Membership.RequiresQuestionAndAnswer Then
                    '    passwordQuestion = "Empresa desarrolladora del sistema"
                    '    passwordAnswer = "Integrant"
                    'End If

                    Try
                        'Usaré el Nombre como parte del usuario y la Matricula como parte del Password
                        Dim newUser As MembershipUser = Membership.CreateUser(Nombre + CStr(Cont), "a-" + Matricula, _
                                                                       Email, Nothing, _
                                                                       Nothing, True, status)
                        If newUser Is Nothing Then
                            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), GetErrorMessage(status))
                        Else
                            Roles.AddUserToRole(Nombre + CStr(Cont), "Alumno")
                        End If
                    Catch ex As MembershipCreateUserException
                        Utils.LogManager.ExceptionLog_InsertEntry(ex)
                        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
                    End Try

                    'Ahora ingreso el registro del Usuario
                    SDSusuarios.InsertCommand = "SET dateformat dmy; INSERT INTO Usuario (Login,Password,PerfilASP,Email,FechaModif) VALUES ('" + Nombre + CStr(Cont) + "','a-" + Matricula + "','Alumno','" + Email + "',getdate())"
                    SDSusuarios.Insert()
                    'Ahora busco el recien ingresado Usuario para obtener su ID   
                    miComando.CommandText = "SELECT * FROM USUARIO WHERE Login = '" + Nombre + CStr(Cont) + "'"
                    misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                    misRegistros.Read()
                    IdUsuario = misRegistros.Item("IdUsuario")
                    misRegistros.Close()

                    'Ahora ingreso el registro del Alumno
                    SDSalumnos.InsertCommand = "SET dateformat dmy; INSERT INTO Alumno (IdUsuario,IdGrupo,IdPlantel,Nombre,ApePaterno,ApeMaterno,Matricula,Email,Estatus,FechaModif,FechaIngreso) VALUES (" + CStr(IdUsuario) + "," + IdGrupo + "," + IdPlantel + ",'" + Nombre + "','" + ApPaterno + "','" + ApMaterno + "','" + Matricula + "','" + Email + "','Activo',getdate())"
                    SDSalumnos.Insert()
                End While
                objConexion.Close()
                leer_archivo.Close()
                LblMensaje.Text = "La migración ha terminado con éxito, revise los registros."
                'Elimino el archivo utilizado para la migración
                Dim MiArchivo As FileInfo = New FileInfo(archivo_datos)
                MiArchivo.Delete()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try
        Else
            msgError.show("Seleccione el archivo que contiene los datos para Migrar.")
        End If
    End Sub

    Public Function GetErrorMessage(ByVal status As MembershipCreateStatus) As String
        Select Case status
            Case MembershipCreateStatus.DuplicateUserName
                Return "El usuario ya existe, elija otro usuario."
            Case MembershipCreateStatus.DuplicateEmail
                Return "Ya existe un usuario pra ese email, por favor escriba un email diferente."
            Case MembershipCreateStatus.InvalidPassword
                Return "El password es inválido, pruebe con otro."
            Case MembershipCreateStatus.InvalidEmail
                Return "El formato del email es inválido, por favor revíselo."
            Case MembershipCreateStatus.InvalidAnswer
                Return "La respuesta secreta es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidQuestion
                Return "La pregunta de recuperación de password es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidUserName
                Return "El nombre de usuario es inválido, corríjalo."
            Case MembershipCreateStatus.ProviderError
                Return "La autentificación falló, contacte al administrador del sistema."
            Case MembershipCreateStatus.UserRejected
                Return "La creación del usuario ha sido cancelada, consulte al administrador."
            Case Else
                Return "Ha ocurrido un error desconocido, verifique o contacte al administrador."
        End Select
    End Function

End Class
