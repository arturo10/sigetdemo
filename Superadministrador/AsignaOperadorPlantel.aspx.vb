﻿Imports Siget


Partial Class superadministrador_AsignaOperadorPlantel
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GVOperadores.Caption = "<h3>Listado de Operadores disponibles</h3>"

        GVPlanteles.Caption = "<h3>Seleccione " &
            Config.Etiqueta.ARTDET_PLANTEL & " " & Config.Etiqueta.PLANTEL &
            " que desea asignar al Operador elegido, debe asignar un" &
            Config.Etiqueta.LETRA_PLANTEL & " por un" & Config.Etiqueta.LETRA_PLANTEL & "</h3>"

        GVDatosPlanteles.Caption = "<h3>Listado de " &
            Config.Etiqueta.PLANTELES &
            " asignad" & Config.Etiqueta.LETRA_PLANTEL & "s a Operadores de " &
            Config.Etiqueta.ARTDET_INSTITUCION & " " & Config.Etiqueta.INSTITUCION &
            " elegid" & Config.Etiqueta.LETRA_INSTITUCION & "</h3>"

        Label2.Text = Config.Etiqueta.PLANTELES
        Label3.Text = Config.Etiqueta.INSTITUCION
    End Sub

    Protected Sub Asignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Asignar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            SDSOpePlantel.InsertCommand = "SET dateformat dmy; INSERT INTO OperadorPlantel (IdOperador,IdPlantel,FechaModif, Modifico) VALUES (" + _
                GVOperadores.SelectedValue.ToString + "," + GVPlanteles.SelectedValue.ToString + ",getdate(), '" & User.Identity.Name & "')"
            SDSOpePlantel.Insert()
            GVDatosPlanteles.DataBind()
            GVPlanteles.DataBind()
            msgSuccess.show("Éxito", "El Plantel ha sido asignado al Operador.")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem("---Elija una " & Config.Etiqueta.INSTITUCION, 0))
    End Sub

    Protected Sub Desasignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Desasignar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            'Aqui uso otra sintaxis para extraer los valores de los campos del GridView:
            SDSOpePlantel.DeleteCommand = "DELETE FROM OperadorPlantel WHERE IdPlantel = " + GVDatosPlanteles.SelectedDataKey(0).ToString + " and IdOperador = " + GVDatosPlanteles.SelectedDataKey(1).ToString
            SDSOpePlantel.Delete()
            GVDatosPlanteles.DataBind()
            GVPlanteles.DataBind()
            msgSuccess.show("Éxito", Config.Etiqueta.ARTDET_PLANTEL & " " & Config.Etiqueta.PLANTEL &
                " ha sido desasignad" & Config.Etiqueta.LETRA_PLANTEL & " del Operador")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Elija el registro con los datos a desasignar.")
        End Try
    End Sub

    Protected Sub GVPlanteles_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVPlanteles.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.PLANTEL

            LnkHeaderText = e.Row.Cells(3).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.INSTITUCION
        End If
    End Sub

    Protected Sub GVDatosPlanteles_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVDatosPlanteles.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(3).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.INSTITUCION

            LnkHeaderText = e.Row.Cells(4).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.INSTITUCION

            LnkHeaderText = e.Row.Cells(7).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.PLANTEL

            LnkHeaderText = e.Row.Cells(8).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.PLANTEL
        End If
    End Sub
End Class
