﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="BuscaAlumno.aspx.vb"
    Inherits="superadministrador_BuscaAlumno"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>




<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            width: 208px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style15 {
            width: 208px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style19 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000066;
        }

        .style18 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            width: 717px;
        }

        .style20 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style21 {
            font-size: xx-small;
        }

        .style26 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            color: #000066;
            height: 8px;
        }

        .style37 {
            width: 208px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            color: #000000;
        }

        .style38 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            color: #000000;
            width: 717px;
        }

        .style39 {
            width: 208px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000000;
        }

        .style40 {
            width: 717px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Buscar un 
								<asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style37">
                <asp:Label ID="Label2" runat="server" Text="[INSTITUCION]" />
            </td>
            <td class="style16" colspan="3" style="text-align: left;">
                <asp:DropDownList ID="DDLinstitucion"
                    runat="server" AutoPostBack="True" DataSourceID="SDSinstituciones"
                    DataTextField="Descripcion" DataValueField="IdInstitucion"
                    Width="720px" Height="24px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style37">&nbsp;</td>
            <td class="style16" rowspan="2">Capture uno o varios de los datos siguientes para hacer la búsqueda:</td>
            <td class="style38">&nbsp;</td>
            <td>Nuevos datos para <asp:Label ID="Label10" runat="server" Text="[EL]"></asp:Label> 
								<asp:Label ID="Label3" runat="server" Text="[ALUMNO]" />
                seleccionado:</td>
        </tr>
        <tr>
            <td class="style37">&nbsp;</td>
            <td class="style38">
                <asp:Label ID="Label4" runat="server" Text="[CICLO]" />
            </td>
            <td style="text-align: left;">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Width="360px"
                    CssClass="style25">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style37">Matrícula</td>
            <td class="style16" style="text-align: left;">
                <asp:TextBox ID="TBmatricula" runat="server" Width="220px"></asp:TextBox>
            </td>
            <td class="style38">
                <asp:Label ID="Label5" runat="server" Text="[PLANTEL]" />
            </td>
            <td style="text-align: left;">
                <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                    DataValueField="IdPlantel" Width="360px" Height="22px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style37">Nombre</td>
            <td class="style16" style="text-align: left;">
                <asp:TextBox ID="TBnombre" runat="server" Width="220px"></asp:TextBox>
            </td>
            <td class="style38">
                <asp:Label ID="Label6" runat="server" Text="[NIVEL]" />
            </td>
            <td style="text-align: left;">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Width="360px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style37">Apellido Paterno</td>
            <td class="style16" style="text-align: left;">
                <asp:TextBox ID="TBapepaterno" runat="server" Width="220px"></asp:TextBox>
            </td>
            <td class="style38">
                <asp:Label ID="Label7" runat="server" Text="[GRADO]" />
            </td>
            <td style="text-align: left;">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion"
                    DataValueField="IdGrado" Width="360px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style37">Apellido Materno</td>
            <td class="style16" style="text-align: left;">
                <asp:TextBox ID="TBapematerno" runat="server" Width="220px"></asp:TextBox>
            </td>
            <td class="style38">
                <asp:Label ID="Label8" runat="server" Text="[GRUPO]" />
            </td>
            <td style="text-align: left;">
                <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrupos" DataTextField="Descripcion"
                    DataValueField="IdGrupo" Width="360px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style37">Login</td>
            <td class="style16" style="text-align: left;">
                <asp:TextBox ID="TBlogin" runat="server" Width="220px"></asp:TextBox>
            </td>
            <td class="style18">&nbsp;</td>
            <td>
                <asp:CheckBox ID="CBactividades" runat="server" Checked="True"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; color: #000000;"
                    Text="Mover ACTIVIDADES terminadas" />
            </td>
        </tr>
        <tr>
            <td class="style22">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td class="style16">
                <asp:Button ID="BtnBuscar" runat="server" Text="Buscar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
                <asp:Button ID="BtnLimpiar" runat="server" Text="Limpiar"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
            <td class="style40">&nbsp;</td>
            <td>
                <asp:Button ID="BtnCambiar" runat="server" Text="Cambiar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        </table>
        <table>
        <tr>
            <td>
                <asp:GridView ID="GValumnos" runat="server"
                    Caption="<h3>Listado de ALUMNOS encontrados</h3>" 
                    AutoGenerateColumns="False"
                    AllowPaging="true"
                    PageSize="15"
                    DataSourceID="SDSbuscar" 
                    Width="896px" 

                    CssClass="dataGrid_clear_selectable"
                    Font-Size="XX-Small"  
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" >
<ItemStyle CssClass="selectCell"></ItemStyle>
                        </asp:CommandField>
                        <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"/>
                        <asp:BoundField DataField="Matricula" HeaderText="Matricula"/>
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre"/>
                        <asp:BoundField DataField="ApePaterno" HeaderText="ApePaterno"/>
                        <asp:BoundField DataField="ApeMaterno" HeaderText="ApeMaterno"/>
                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo"/>
                        <asp:BoundField DataField="Grupo" HeaderText="Grupo"/>
                        <asp:BoundField DataField="IdGrado" HeaderText="IdGrado"/>
                        <asp:BoundField DataField="Grado" HeaderText="Grado"/>
                        <asp:BoundField DataField="IdNivel" HeaderText="IdNivel"/>
                        <asp:BoundField DataField="Nivel" HeaderText="Nivel"/>
                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"/>
                        <asp:BoundField DataField="Plantel" HeaderText="Plantel"/>
                        <asp:BoundField DataField="Login" HeaderText="Login"/>
                        <asp:BoundField DataField="Password" HeaderText="Password"/>
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"/>
                        <asp:BoundField DataField="Equipo" HeaderText="Equipo"/>
                        <asp:BoundField DataField="Subequipo" HeaderText="Subequipo"/>
                        <asp:BoundField DataField="Ciclo" HeaderText="Ciclo"/>
                        <asp:BoundField DataField="Email" HeaderText="Email" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort 
	                0  select
	                1  Id_ALUMNO
	                2  Matricula
	                3  Nombre
	                4  ApePaterno
	                5  ApeMaterno
	                6  Id_GRUPO
	                7  GRUPO
	                8  Id_GRADO
	                9  GRADO
	                10 Id_NIVEL
                    11 NIVEL
                    12 Id_PLANTEL
                    13 PLANTEL
                    14 Login
                    15 Password
                    16 Estatus
                    17 EQUIPO
                    18 SUBEQUIPO
                    19 CICLO
	                --%>
            </td>
        </tr>
        </table>
        <table>
        <tr>
            <td class="style22">
                <asp:Button ID="Button1" runat="server" Text="Exportar a Excel"
                    CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style22">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style23">Actualizar password</td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style37">
                <asp:Label ID="Label9" runat="server" Text="[ALUMNO]" />
                seleccionado:</td>
            <td colspan="2">
                <asp:Label ID="LblAlumno" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #000099"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style37">Contraseña</td>
            <td colspan="2">
                <asp:TextBox ID="TBpasswordOK" runat="server" CssClass="style20"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style37">Email <span class="style21">(solo cuando se genere)</span></td>
            <td colspan="2">
                <asp:TextBox ID="TBemailOK" runat="server" CssClass="style20" Width="258px"></asp:TextBox>
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style22">
                <asp:Button ID="BtnObtener" runat="server" Text="Obtener"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                </td>
            <td colspan="2">
                <asp:Button ID="BtnActualizar" runat="server" Text="Actualizar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                <asp:Button ID="BtnGenerar" runat="server" Text="Generar"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                <asp:Button ID="BtnRestaurar" runat="server" Text="Restaurar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
            <tr>
                <td colspan="4">
                    <uc1:msgError runat="server" ID="msgError2" />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    &nbsp;
                </td>
            </tr>
        <tr>
            <td class="style22">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td colspan="2">&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>

    <table>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSbuscar" runat="server"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Institucion] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion)
ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select N.IdNivel, N.Descripcion
from Nivel N, Escuela E, Plantel P
where N.IdNivel = E.IdNivel and P.IdPlantel = E.IdPlantel
      and P.IdPlantel = @IdPlantel
Order by Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grupo] WHERE ([IdGrado] = @IdGrado) and ([IdPlantel] = @IdPlantel) and ([IdCicloEscolar] = @IdCicloEscolar)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSalumnos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Alumno] WHERE (([IdGrupo] = @IdGrupo) AND ([IdPlantel] = @IdPlantel)) ORDER BY [ApePaterno], [ApeMaterno], [Nombre]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluacionesterm" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [EvaluacionTerminada]"></asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar] WHERE ([Estatus] = 'Activo') ORDER BY [FechaInicio]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSusuarios" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Usuario]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSrespuestas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Respuesta]"></asp:SqlDataSource>

            </td>
            <td>
                 <asp:SqlDataSource ID="SDSComentarioEvaluacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [ComentarioEvaluacion]"></asp:SqlDataSource>


            </td>
        </tr>
    </table>
</asp:Content>

