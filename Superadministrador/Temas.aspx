﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="Temas.aspx.vb" 
    Inherits="superadministrador_Temas" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">

        .style11
        {
            height: 108px;
        }
        .style13
        {
            text-align: left;
        }
        .style23
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
        .style25
        {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            color: #FFFFFF;
            height: 8px;
            width: 130px;
            text-align: right;
        }
        .style27
        {
            width: 123px;
            height: 8px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }
        .style37
        {
            height: 20px;
        }
        .style38
        {
            text-align: left;
            width: 123px;
        }
        </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Administración de Temas
		</h1>
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style10">
        <tr>
            <td class="style11" colspan="4">
                <table class="estandar" style="width: 757px">
                    <tr>
                        <td colspan="5" class="style37">
                            </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <asp:Label ID="Mensaje1" runat="server" CssClass="titulo" 
                                style="color: #000066; font-family: Arial, Helvetica, sans-serif;">Capture los datos</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style27">
                                <asp:Label ID="Label1" runat="server" Text="[NIVEL]" />:</td>
                        <td class="style16" style="text-align: left">
                            <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True" 
                                DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel" 
                                    Width="180px">
                            </asp:DropDownList>
                        </td>
                        <td class="style25" style="color: #000000">
                                <asp:Label ID="Label2" runat="server" Text="[GRADO]" />:</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True" 
                                    DataSourceID="SDSgrados" DataTextField="Descripcion" 
                                    DataValueField="IdGrado" Width="180px">
                            </asp:DropDownList>
                        </td>
                        <td>
                                &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style27">
                            <asp:Label ID="Label3" runat="server" Text="[ASIGNATURA]" />:</td>
                        <td class="style16" colspan="4" style="text-align: left">
                            <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True" 
                                    DataSourceID="SDSasignaturas" DataTextField="Descripcion" 
                                    DataValueField="IdAsignatura" Width="470px" Height="22px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style27">
                                Número del Tema:</td>
                        <td class="style16" colspan="4" style="text-align: left">
                            <asp:TextBox ID="TBnumero" runat="server" Width="58px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style27">
                                Descripción del Tema:</td>
                        <td class="style16" colspan="4" style="text-align: left">
                            <asp:TextBox ID="TBdescripcion" runat="server" Width="470px" MaxLength="150"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style38">
                                &nbsp;</td>
                        <td class="style13" colspan="4">
                            <asp:Button ID="Insertar" runat="server" Text="Insertar" 
																CssClass="defaultBtn btnThemeBlue btnThemeMedium"/>
														&nbsp;
                            <asp:Button ID="Limpiar" runat="server" Text="Limpiar" 
																CssClass="defaultBtn btnThemeGrey btnThemeMedium"/>
														&nbsp;
                            <asp:Button ID="Actualizar" runat="server" Text="Actualizar" 
																CssClass="defaultBtn btnThemeBlue btnThemeMedium"/>
														&nbsp;
                            <asp:Button ID="Eliminar" runat="server" Text="Eliminar" 
																CssClass="defaultBtn btnThemeGrey btnThemeMedium"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GVtemas" runat="server" AllowPaging="True" 
                        AllowSorting="True" AutoGenerateColumns="False" AutoGenerateSelectButton="True" 
                        CellPadding="3" DataKeyNames="IdTema" DataSourceID="SDSdatostemas" 
                        ForeColor="Black" GridLines="Vertical" PageSize="5" style="font-size: x-small; font-family: Arial, Helvetica, sans-serif;" 
                        Width="871px" BackColor="White" BorderColor="#999999" 
                    BorderStyle="Solid" BorderWidth="1px" 
                    Caption="<h3>Temas capturados</h3>">
                    <Columns>
                        <asp:BoundField DataField="IdAsignatura" HeaderText="Id-Asign." 
                                SortExpression="IdAsignatura" Visible="False" />
                        <asp:BoundField DataField="NomAsignatura" HeaderText="Asignatura" 
                                SortExpression="NomAsignatura" Visible="False" />
                        <asp:BoundField DataField="IdTema" HeaderText="Id_Tema" InsertVisible="False" 
                                ReadOnly="True" SortExpression="IdTema" />
                        <asp:BoundField DataField="NomTema" HeaderText="Tema" 
                                SortExpression="NomTema" />
                        <asp:BoundField DataField="Numero" HeaderText="Número" 
                                SortExpression="Numero" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                    &nbsp;</td>
            <td>
                    &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:HyperLink ID="HyperLink2" runat="server"
					NavigateUrl="~/" 
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    
                        SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]">
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                        SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel" 
                                PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignaturas" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                        
                        SelectCommand="SELECT [IdAsignatura], [IdGrado], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado" 
                                PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDStemas" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                        SelectCommand="SELECT * FROM [Tema]"></asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSdatostemas" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT T.IdAsignatura, A.Descripcion NomAsignatura, T.IdTema, T.Descripcion NomTema, T.Numero 
FROM Tema T, Asignatura A
WHERE (T.IdAsignatura = @IdAsignatura) and (A.IdAsignatura = T.IdAsignatura)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura" 
                                PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

