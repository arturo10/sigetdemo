﻿Imports Siget

Imports System.Data.SqlClient

Partial Class superadministrador_Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' esto debe ir antes que cualquier operación que utilice base de datos
        If Utils.Bloqueo.sistemaBloqueado() Then
            Utils.Sesion.CierraSesion()
            Response.Redirect("~/EnMantenimiento.aspx")
        End If

        If Roles.IsUserInRole(User.Identity.Name, "sysadmin") Then
            pnlSysadmin.Visible = True
        End If

        Label1.Text = Config.Etiqueta.INSTITUCIONES
        Label2.Text = Config.Etiqueta.PLANTELES
        Label3.Text = Config.Etiqueta.NIVELES
        Label4.Text = Config.Etiqueta.GRADOS
        Label5.Text = Config.Etiqueta.GRUPOS
        Label6.Text = Config.Etiqueta.COORDINADORES
        Label7.Text = Config.Etiqueta.PLANTEL
        Label8.Text = Config.Etiqueta.PROFESORES
        Label9.Text = Config.Etiqueta.GRUPOS
        Label10.Text = Config.Etiqueta.PROFESORES
        Label11.Text = Config.Etiqueta.ALUMNOS
        Label12.Text = Config.Etiqueta.ALUMNOS
        Label13.Text = Config.Etiqueta.SUBEQUIPOS
        Label14.Text = Config.Etiqueta.EQUIPOS
        Label15.Text = Config.Etiqueta.SUBEQUIPOS
        Label16.Text = Config.Etiqueta.ASIGNATURAS
        Label17.Text = Config.Etiqueta.CICLO
        Label19.Text = Config.Etiqueta.PLANTELES
        Label20.Text = Config.Etiqueta.ALUMNOS
        Label21.Text = Config.Etiqueta.PROFESORES
        Label22.Text = Config.Etiqueta.ALUMNOS
        Label23.Text = Config.Etiqueta.PLANTEL

        'Estas variables las lee el archivo App_Code/PTBlogTemplate/UserFunctions.vb
        'Tengo que dar de alta un usuario administrador del Blog
        Application("Login") = Trim(User.Identity.Name)
        If Trim(User.Identity.Name) = "administrador" Then 'El password default "Administrador" no está en la tabla admin (Ver si lo meto)
            Application("Passwd") = "mariscal" 'OJO, el password en el blog es diferente al de superadministrador del SIFA, y no aceptó caracter "+"
            'Luego hacer que los passwords coincidan, cuando lo haga, quitar el IF del script usuario/CambiarPassword.aspx
        Else
            Dim strConexion As String
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objConexionEv As New SqlConnection(strConexion)
            Dim miComandoEv As SqlCommand
            Dim misClaves As SqlDataReader
            miComandoEv = objConexionEv.CreateCommand
            miComandoEv.CommandText = "select Password from Usuario where Login = '" + Trim(User.Identity.Name) + "'"
            objConexionEv.Open() 'Abro la conexion
            misClaves = miComandoEv.ExecuteReader() 'Creo conjunto de registros
            If misClaves.Read() Then
                Application("Passwd") = misClaves.Item("Password")
            End If
            misClaves.Close()
            objConexionEv.Close()
        End If
    End Sub

End Class
