﻿Imports Siget


Partial Class superadministrador_SeriarEvaluaciones
    Inherits System.Web.UI.Page

#Region "Init"

    ' Proceso de inicio de la página
    Protected Sub Page_Load(
                    sender As Object,
                    e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' modifica la visiblidad de la página para usuarios administradores de sistema
        If Roles.IsUserInRole(User.Identity.Name, "sysadmin") Then
            presentaElementosOcultos()
        End If

        ' personaliza las etiquetas de la página
        Label1.Text = Config.Etiqueta.CICLO
        Label2.Text = Config.Etiqueta.NIVEL
        Label3.Text = Config.Etiqueta.GRADO
        Label4.Text = Config.Etiqueta.ASIGNATURA
        Label5.Text = Config.Etiqueta.ASIGNATURA
    End Sub

    ' se encarga de mostrar todos los elementos que sólo
    ' deben ser visibles a usuarios privilegiados
    Protected Sub presentaElementosOcultos()
        GVevaluacionesG.Columns(1).Visible = True

        GVseriaciones.Columns(1).Visible = True
        GVseriaciones.Columns(9).Visible = True
        GVseriaciones.Columns(10).Visible = True

        GVevaluacionesDisponibles.Columns(1).Visible = True
    End Sub

#End Region
    ' *********************************************************************************************

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.NIVEL, 0))
    End Sub


    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija la " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLcalificacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacion.DataBound
        DDLcalificacion.Items.Insert(0, New ListItem("---Elija una calificación", 0))
    End Sub

    Protected Sub DDLasignaturaSeriadas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignaturaSeriadas.DataBound
        DDLasignaturaSeriadas.Items.Insert(0, New ListItem("---Elija la " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLcalificacionseriadas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacionseriadas.DataBound
        DDLcalificacionseriadas.Items.Insert(0, New ListItem("---Elija una calificación", 0))
    End Sub

    Protected Sub BtnSeriar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSeriar.Click
        msgError.hide()
        Try
            If CHKminima.Checked Then
                If verificaMinima() Then
                    'SDSevaluacionesSer.UpdateCommand = "SET dateformat dmy; UPDATE EVALUACION SET IdEvaluacionPrevia = " & GVevaluacionesDisponibles.SelectedRow.Cells(1).Text & ", PreviaMinima = " & TBminima.Text.Trim() & " WHERE IdEvaluacion = " + GVevaluacionesG.SelectedRow.Cells(1).Text
                    SDSevaluacionesSer.InsertCommand = "SET dateformat dmy; INSERT INTO SeriacionEvaluaciones (IdEvaluacion, IdEvaluacionPrevia, Minima, FechaModif, Modifico) VALUES (" & GVevaluacionesG.SelectedDataKey.Value & ", " & GVevaluacionesDisponibles.SelectedDataKey.Value & ", '" & TBminima.Text.Trim() & "', getdate(), '" & User.Identity.Name & "')"
                    SDSevaluacionesSer.Insert()
                    GVevaluacionesDisponibles.DataBind()
                    GVseriaciones.DataBind()
                End If
            Else
                'SDSevaluacionesSer.UpdateCommand = "SET dateformat dmy; UPDATE EVALUACION SET IdEvaluacionPrevia = " + GVevaluacionesDisponibles.SelectedRow.Cells(1).Text + ", PreviaMinima = NULL WHERE IdEvaluacion = " + GVevaluacionesG.SelectedRow.Cells(1).Text
                SDSevaluacionesSer.InsertCommand = "SET dateformat dmy; INSERT INTO SeriacionEvaluaciones (IdEvaluacion, IdEvaluacionPrevia, FechaModif, Modifico) VALUES (" & GVevaluacionesG.SelectedDataKey.Value & ", " & GVevaluacionesDisponibles.SelectedDataKey.Value & ", getdate(), '" & User.Identity.Name & "')"
                SDSevaluacionesSer.Insert()
                GVevaluacionesDisponibles.DataBind()
                GVseriaciones.DataBind()
            End If
            GVevaluacionesDisponibles.SelectRow(-1)
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        msgError.hide()
        Try
            If CHKminimaEdit.Checked Then
                If verificaMinimaEdit() Then
                    'SDSevaluacionesSer.UpdateCommand = "SET dateformat dmy; UPDATE EVALUACION SET IdEvaluacionPrevia = " & GVevaluacionesDisponibles.SelectedRow.Cells(1).Text & ", PreviaMinima = " & TBminima.Text.Trim() & " WHERE IdEvaluacion = " + GVevaluacionesG.SelectedRow.Cells(1).Text
                    SDSevaluacionesSer.UpdateCommand = "SET dateformat dmy; UPDATE SeriacionEvaluaciones SET MINIMA = '" & TBminimaEdit.Text.Trim() & "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdEvaluacion = " & GVevaluacionesG.SelectedDataKey.Value & " AND IdEvaluacionPrevia = " & GVseriaciones.SelectedDataKey.Value
                    SDSevaluacionesSer.Update()
                    GVseriaciones.DataBind()
                End If
            Else
                'SDSevaluacionesSer.UpdateCommand = "SET dateformat dmy; UPDATE EVALUACION SET IdEvaluacionPrevia = " + GVevaluacionesDisponibles.SelectedRow.Cells(1).Text + ", PreviaMinima = NULL WHERE IdEvaluacion = " + GVevaluacionesG.SelectedRow.Cells(1).Text
                SDSevaluacionesSer.UpdateCommand = "SET dateformat dmy; UPDATE SeriacionEvaluaciones SET MINIMA = NULL, FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdEvaluacion = " & GVevaluacionesG.SelectedDataKey.Value & " AND IdEvaluacionPrevia = " & GVseriaciones.SelectedDataKey.Value
                SDSevaluacionesSer.Update()
                GVseriaciones.DataBind()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Function verificaMinima() As Boolean
        If TBminima.Text Is Nothing Then
            msgError.show("Si se señala una calificación mínima, ésta no debe estar vacía.")
            Return False
        ElseIf Not Decimal.TryParse(TBminima.Text, New Decimal()) Then
            msgError.show("La calificación mínima no es válida.")
            Return False
        ElseIf Decimal.Parse(TBminima.Text) < 0 Or Decimal.Parse(TBminima.Text) > 100 Then
            msgError.show("La calificación mínima debe estar entre 0 y 100.")
            Return False
        End If
        Return True
    End Function

    Protected Function verificaMinimaEdit() As Boolean
        If TBminimaEdit.Text Is Nothing Then
            msgError.show("Si se señala una calificación mínima, ésta no debe estar vacía.")
            Return False
        ElseIf Not Decimal.TryParse(TBminimaEdit.Text, New Decimal()) Then
            msgError.show("La calificación mínima no es válida.")
            Return False
        ElseIf Decimal.Parse(TBminimaEdit.Text) < 0 Or Decimal.Parse(TBminimaEdit.Text) > 100 Then
            msgError.show("La calificación mínima debe estar entre 0 y 100.")
            Return False
        End If
        Return True
    End Function

    Protected Sub BtnQuitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnQuitar.Click
        msgError.hide()
        Try
            SDSevaluacionesSer.DeleteCommand = "SET dateformat dmy; DELETE FROM SeriacionEvaluaciones WHERE IdEvaluacion = " & GVevaluacionesG.SelectedDataKey.Value & " AND IdEvaluacionPrevia = " & GVseriaciones.SelectedDataKey.Value
            SDSevaluacionesSer.Delete()
            GVseriaciones.DataBind()
            GVevaluacionesDisponibles.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub CHKminima_CheckedChanged(sender As Object, e As EventArgs) Handles CHKminima.CheckedChanged
        If CHKminima.Checked Then
            PanelMinima.Visible = True
        Else
            PanelMinima.Visible = False
        End If
    End Sub

    Protected Sub CHKminimaEdit_CheckedChanged(sender As Object, e As EventArgs) Handles CHKminimaEdit.CheckedChanged
        If CHKminimaEdit.Checked Then
            PanelMinimaEdit.Visible = True
        Else
            PanelMinimaEdit.Visible = False
        End If
    End Sub

    Protected Sub GVevaluacionesDisponibles_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVevaluacionesDisponibles.SelectedIndexChanged
        If GVevaluacionesDisponibles.SelectedIndex > -1 AndAlso GVevaluacionesDisponibles.SelectedIndex < GVevaluacionesDisponibles.Rows.Count Then
            pnlSeriar.Visible = True
            GVseriaciones.SelectRow(-1)
        Else
            pnlSeriar.Visible = False
        End If
    End Sub

    Protected Sub GVseriaciones_DataBound(sender As Object, e As EventArgs) Handles GVseriaciones.DataBound
        If GVseriaciones.Rows.Count < 1 Then
            pnlActualizar.Visible = False
        End If
    End Sub

    Protected Sub GVseriaciones_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVseriaciones.SelectedIndexChanged
        If GVseriaciones.SelectedIndex > -1 AndAlso GVseriaciones.SelectedIndex < GVseriaciones.Rows.Count Then
            Dim minima As Decimal
            If Decimal.TryParse(GVseriaciones.SelectedDataKey(1).ToString(), minima) Then
                CHKminimaEdit.Checked = True
                TBminimaEdit.Text = minima
            End If
            pnlActualizar.Visible = True
        Else
            pnlActualizar.Visible = False
        End If
    End Sub

    Protected Sub DDLcalificacionseriadas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLcalificacionseriadas.SelectedIndexChanged
        GVevaluacionesG.SelectRow(-1)
    End Sub

    Protected Sub DDLcalificacion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLcalificacion.SelectedIndexChanged
        GVevaluacionesDisponibles.SelectRow(-1)
    End Sub
End Class
