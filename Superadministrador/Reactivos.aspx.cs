﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class superadministrador_Reactivos : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void DDLnivel_DataBound(object sender, EventArgs e)
    {
        DDLnivel.Items.Insert(0, new ListItem(Siget.Lang.FileSystem.General.ddl_elija_un_elemento[Session["Usuario_Idioma"].ToString()], "0"));
    }
    protected void DDLgrado_DataBound(object sender, EventArgs e)
    {
        DDLgrado.Items.Insert(0, new ListItem(Siget.Lang.FileSystem.General.ddl_elija_un_elemento[Session["Usuario_Idioma"].ToString()], "0"));
    }
    protected void DDLasignatura_DataBound(object sender, EventArgs e)
    {
        DDLasignatura.Items.Insert(0, new ListItem(Siget.Lang.FileSystem.General.ddl_elija_un_elemento[Session["Usuario_Idioma"].ToString()], "0"));
    }
    protected void DDLtemas_DataBound(object sender, EventArgs e)
    {
        DDLtemas.Items.Insert(0, new ListItem(Siget.Lang.FileSystem.General.ddl_elija_un_elemento[Session["Usuario_Idioma"].ToString()], "0"));
    }
    protected void DDLsubtemas_DataBound(object sender, EventArgs e)
    {
        DDLsubtemas.Items.Insert(0, new ListItem(Siget.Lang.FileSystem.General.ddl_elija_un_elemento[Session["Usuario_Idioma"].ToString()], "0"));
    }
    protected void DDLsubtemas_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLsubtemas.SelectedValue != "0")
            pnlPlanteamientos.Visible = true;
        else
            pnlPlanteamientos.Visible = false;
    }
}