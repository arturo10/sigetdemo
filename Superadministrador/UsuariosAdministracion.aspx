﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="UsuariosAdministracion.aspx.vb" 
    Inherits="superadministrador_UsuariosAdministracion" EnableEventValidation="false"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style10
        {
            width: 100%;
        }
        .style11
        {
            text-align: left;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
        .style13
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
        .style23
        {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
        .auto-style1 {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
        <h1>
			Usuarios de Administración del Sistema
		</h1>
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style10">
        <tr>
            <td class="style23">
                Nombre</td>
            <td class="auto-style1">
                <asp:TextBox ID="TBnombre" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style23">
                Apellidos</td>
            <td class="auto-style1">
                <asp:TextBox ID="TBapellidos" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style23">
                Email</td>
            <td class="auto-style1">
                <asp:TextBox ID="TBemail" runat="server" MaxLength="100" Width="200px"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style23">
                Login</td>
            <td class="auto-style1">
                <asp:TextBox ID="TBlogin" runat="server" MaxLength="50" Width="150px"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style23">
                Password</td>
            <td class="auto-style1">
                <asp:TextBox ID="TBpassword" runat="server" MaxLength="20" Width="150px"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style23">
                Perfil</td>
            <td class="auto-style1">
                <asp:DropDownList ID="DDLperfil" runat="server" Height="22px" Width="200px" 
                    AutoPostBack="True">
                    <asp:ListItem Value="superadministrador ">Superadministrador </asp:ListItem>
                    <asp:ListItem Value="capturista" Selected="True">Capturista</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style11">
                &nbsp;</td>
            <td>
                <asp:Button ID="BtnInsertar" runat="server" Text="Insertar" 
										CssClass="defaultBtn btnThemeBlue btnThemeMedium"/>
								&nbsp;
                <asp:Button ID="BtnEliminar" runat="server" Text="Eliminar" 
										CssClass="defaultBtn btnThemeGrey btnThemeMedium"/>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style11" colspan="2">
                <table class="style10">
                    <tr>
                        <td>
                            <asp:GridView ID="GVadmin" runat="server" AllowPaging="True" 
                                AllowSorting="True" AutoGenerateColumns="False" Caption="Cuentas creadas" 
                                CellPadding="3" DataKeyNames="IdAdmin" DataSourceID="SDSadmin" 
                                ForeColor="Black" GridLines="Vertical" 
                                style="font-family: Arial, Helvetica, sans-serif; font-size: x-small; text-align: left" 
                                Width="866px" BackColor="White" BorderColor="#999999" BorderStyle="Solid" 
                                BorderWidth="1px">
                                <Columns>
                                    <asp:BoundField DataField="IdAdmin" HeaderText="IdAdmin" InsertVisible="False" 
                                        ReadOnly="True" SortExpression="IdAdmin" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" 
                                        SortExpression="Nombre" />
                                    <asp:BoundField DataField="Apellidos" HeaderText="Apellidos" 
                                        SortExpression="Apellidos" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                    <asp:BoundField DataField="Login" HeaderText="Login" SortExpression="Login" />
                                    <asp:BoundField DataField="Password" HeaderText="Password" 
                                        SortExpression="Password" />
                                    <asp:BoundField DataField="PerfilASP" HeaderText="Perfil" 
                                        SortExpression="PerfilASP" />
                                    <asp:BoundField DataField="FechaAlta" DataFormatString="{00:dd/MM/yyyy}" 
                                        HeaderText="Fecha Alta" SortExpression="FechaAlta" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus" 
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style11">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style11">
                <asp:HyperLink ID="HyperLink2" runat="server"
					NavigateUrl="~/" 
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
                </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>
                
                <asp:SqlDataSource ID="SDSadmin" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    
                    
                    SelectCommand="
                    SELECT 
                        A.[IdAdmin], 
                        A.[Nombre], 
                        A.[Apellidos], 
                        A.[Email], 
                        U.[Login], 
                        A.[FechaAlta], 
                        U.[PerfilASP], 
                        U.[Password], 
                        A.[Estatus] 
                    FROM 
                        [Admin] A
                        ,Usuario U
                    WHERE 
                        (U.PerfilASP = @PerfilASP) 
                        AND A.IdUsuario = U.IdUsuario
                    ORDER BY A.[Apellidos], A.[Nombre], A.[FechaAlta]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLperfil" DefaultValue="Superadministrador" 
                            Name="PerfilASP" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                
            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

