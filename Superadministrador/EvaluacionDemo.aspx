﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="EvaluacionDemo.aspx.vb" 
    Inherits="superadministrador_EvaluacionDemo" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
        }

        .style24 {
            width: 190px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style25 {
            width: 200px;
        }

        .style26 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style27 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style29 {
            width: 200px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style30 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
        }

        .style31 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            height: 32px;
            font-weight: bold;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Crear Actividades de Práctica
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style27">
                <asp:Label ID="Label1" runat="server" Text="[CICLO]" />
            </td>
            <td class="style25">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Width="200px"
                    CssClass="style26">
                </asp:DropDownList>
            </td>
            <td class="style27">
                <asp:Label ID="Label2" runat="server" Text="[NIVEL]" />
            </td>
            <td>
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Width="323px" Height="22px" CssClass="style26">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style27">
                <asp:Label ID="Label3" runat="server" Text="[GRADO]" />
            </td>
            <td class="style25">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion"
                    DataValueField="IdGrado" Width="200px" Height="22px"
                    CssClass="style26">
                </asp:DropDownList>
            </td>
            <td class="style27">
                <asp:Label ID="Label4" runat="server" Text="[ASIGNATURA]" />
            </td>
            <td>
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSasignaturas" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Width="325px" Height="22px"
                    CssClass="style26">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style26">&nbsp;</td>
            <td class="style29">&nbsp;</td>
            <td class="style26">&nbsp;</td>
            <td class="style26">&nbsp;</td>
        </tr>
        <tr>
            <td class="style27">Calificación</td>
            <td class="style29">
                <asp:DropDownList ID="DDLcalificaciones" runat="server" AutoPostBack="True"
                    DataSourceID="SDScalificaciones" DataTextField="Descripcion"
                    DataValueField="IdCalificacion" Width="200px">
                </asp:DropDownList>
            </td>
            <td class="style26">&nbsp;</td>
            <td class="style26">&nbsp;</td>
        </tr>
        <tr>
            <td class="style26">&nbsp;</td>
            <td class="style29">&nbsp;</td>
            <td class="style26">&nbsp;</td>
            <td class="style26">&nbsp;</td>
        </tr>
        <tr>
            <td class="style23" colspan="4">
                <asp:GridView ID="GVevaluaciones" runat="server"
                    AutoGenerateColumns="False"
                    AutoGenerateSelectButton="True" BackColor="White"
                    BorderColor="#999999" BorderStyle="Solid"
                    BorderWidth="1px" CellPadding="3"
                    DataKeyNames="IdEvaluacion" DataSourceID="SDSevaluaciones"
                    GridLines="Vertical" AllowSorting="True"
                    ForeColor="Black"
                    Caption="<h3>Elija la actividad a la que desea configurarle la práctica</h3>"
                    CssClass="style30" Width="886px">
                    <Columns>
                        <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion" />
                        <asp:BoundField DataField="IdCalificacion"
                            HeaderText="IdCalificacion"
                            SortExpression="IdCalificacion" Visible="False" />
                        <asp:BoundField DataField="ClaveBateria" HeaderText="Clave de la Actividad"
                            SortExpression="ClaveBateria" />
                        <asp:BoundField DataField="ClaveAbreviada" HeaderText="Clave p/Reporte"
                            SortExpression="ClaveAbreviada" />
                        <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                            HeaderText="Inicia" SortExpression="InicioContestar" />
                        <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                            HeaderText="Termina" SortExpression="FinContestar" />
                        <asp:BoundField DataField="FinSinPenalizacion"
                            DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Máxima"
                            SortExpression="FinSinPenalizacion" />
                        <asp:BoundField DataField="Penalizacion" HeaderText="% Penalización"
                            SortExpression="Penalizacion">
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                        <asp:BoundField DataField="Aleatoria" HeaderText="Aleatoria"
                            SortExpression="Aleatoria" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White"
                        HorizontalAlign="Center" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="style30">&nbsp;</td>
            <td class="style31">&nbsp;</td>
            <td class="style30">&nbsp;</td>
            <td class="style30">&nbsp;</td>
        </tr>
        <tr>
            <td class="style27">Subtemas asignados a la actividad</td>
            <td class="style31" colspan="3">
                <asp:DropDownList ID="DDLsubtemas" runat="server" AutoPostBack="True"
                    DataSourceID="SDSsubtemas" DataTextField="NomSubtema"
                    DataValueField="IdSubtema" Height="22px" Width="632px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style32" colspan="4">Listado de reactivos del subtema que no se despliegan en las actividades 
                normales. Elija los que desee incorporar a la actividad demo:</td>
        </tr>
        <tr>
            <td class="style30" colspan="4">
                <asp:CheckBoxList ID="CBLreactivos" runat="server" Width="885px"
                    BackColor="#CCCCCC" DataSourceID="SDSreactivos" DataTextField="Reactivo"
                    DataValueField="IdPlanteamiento">
                </asp:CheckBoxList>
            </td>
        </tr>
        <tr>
            <td class="style30">&nbsp;</td>
            <td>
                <asp:Button ID="Agregar" runat="server"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium"
                    Text="Agregar" />
            </td>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style30" colspan="4">
                <asp:GridView ID="GVseleccionados" runat="server" AutoGenerateColumns="False"
                    BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px"
                    Caption="<h3>Reactivos seleccionados para la actividad demo</h3>"
                    CellPadding="3" DataKeyNames="IdPlanteamiento" DataSourceID="SDSevaluaciondemo"
                    ForeColor="Black" GridLines="Vertical" Width="882px">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="IdPlanteamiento" HeaderText="IdPlanteamiento"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdPlanteamiento"
                            Visible="False" />
                        <asp:BoundField DataField="Secuencia" HeaderText="Secuencia"
                            SortExpression="Secuencia" />
                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                            SortExpression="Consecutivo" />
                        <asp:BoundField DataField="Reactivo" HeaderText="Reactivo"
                            SortExpression="Reactivo" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                        <asp:BoundField DataField="Archivo de Apoyo 1" HeaderText="Archivo de Apoyo 1"
                            SortExpression="Archivo de Apoyo 1" />
                        <asp:BoundField DataField="Archivo de Apoyo 2" HeaderText="Archivo de Apoyo 2"
                            SortExpression="Archivo de Apoyo 2" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="style30">&nbsp;</td>
            <td>
                <asp:Button ID="Quitar" runat="server" Text="Quitar"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
            <td>
                <asp:Button ID="Consecutivo" runat="server" Text="Secuencia"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
            <td class="style30">
                <asp:TextBox ID="TBconsecutivo" runat="server" MaxLength="3" Width="66px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style30">&nbsp;</td>
            <td class="style31">&nbsp;</td>
            <td class="style30">&nbsp;</td>
            <td class="style30">&nbsp;</td>
        </tr>
        <tr>
            <td class="style23">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td class="style25">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar] WHERE ([Estatus] = 'Activo') ORDER BY [FechaInicio]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT C.IdCalificacion, C.IdCicloEscolar, C.Clave, C.IdAsignatura, A.Descripcion Materia, C.Consecutivo,C.Descripcion, C.Valor
FROM Calificacion C, Asignatura A
WHERE ([IdCicloEscolar] = @IdCicloEscolar)  and (A.IdAsignatura = C.IdAsignatura) and (A.IdAsignatura = @IdAsignatura)
ORDER BY A.Descripcion, C.Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdAsignatura], [IdGrado], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Evaluacion] 
where IdCalificacion = @IdCalificacion
ORDER BY [IdEvaluacion] DESC">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcalificaciones" Name="IdCalificacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSsubtemas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select S.IdSubtema, CAST(S.Numero as varchar(5)) + '.-' + S.Descripcion + '  (' + T.Descripcion + ')' as NomSubtema
from DetalleEvaluacion D, Subtema S, Tema T
where D.IdEvaluacion = @IdEvaluacion and 
S.IdSubtema = D.IdSubtema
and T.IdTema = S.IdTema
order by T.Numero, S.Numero">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSreactivos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdPlanteamiento, cast(Consecutivo as varchar(3)) + '.- ' + ISNULL(Redaccion,'-') + ' ('  + ISNULL(ArchivoApoyo,'-') + ')' as Reactivo, ArchivoApoyo, ArchivoApoyo2  FROM Planteamiento
WHERE IdSubtema = @IdSubtema and Ocultar = 1
and IdPlanteamiento not in (select IdPlanteamiento from EvaluacionDemo 
where IdEvaluacion = @IdEvaluacion)
ORDER BY Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLsubtemas" Name="IdSubtema"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSevaluaciondemo" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.IdPlanteamiento, E.Consecutivo as Secuencia, P.Consecutivo, P.Redaccion as Reactivo, P.TipoRespuesta as Tipo, 
ArchivoApoyo as 'Archivo de Apoyo 1', ArchivoApoyo2 as 'Archivo de Apoyo 2'
from Planteamiento P, EvaluacionDemo E
where P.IdPlanteamiento = E.IdPlanteamiento
and E.IdEvaluacion = @IdEvaluacion
order by E.COnsecutivo, P.Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

