﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="AsignaCoordPlantel.aspx.vb"
    Inherits="superadministrador_AsignaCoordPlantel"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style15 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 13px;
        }

        .style11 {
            text-align: left;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style24 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            color: #000000;
            height: 8px;
            text-align: right;
        }

        .style25 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 45px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Asignar 
								<asp:Label ID="Label1" runat="server" Text="[COORDINADORES]" />
        a los 
								<asp:Label ID="Label2" runat="server" Text="[PLANTELES]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td colspan="8" style="text-align: left">
                <asp:GridView ID="GVCoordinadores" runat="server" 
                    AllowPaging="True"
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 
                    DataKeyNames="IdCoordinador" 
                    DataSourceID="SDSCoordinadores"
                    Caption="<h3>Listado de COORDINADORES disponibles</h3>"
                    PageSize="15"

                    BackColor="White"
                    BorderColor="#999999" 
                    BorderStyle="Solid" 
                    BorderWidth="1px" 
                    CellPadding="3"
                    GridLines="Vertical"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"
                    Width="882px"
                    ForeColor="Black" >
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="IdCoordinador" HeaderText="IdCoordinador"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdCoordinador" />
                        <asp:BoundField DataField="IdUsuario" HeaderText="IdUsuario"
                            SortExpression="IdUsuario" Visible="False" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre"
                            SortExpression="Nombre" />
                        <asp:BoundField DataField="Apellidos" HeaderText="Apellidos"
                            SortExpression="Apellidos" />
                        <asp:BoundField DataField="Clave" HeaderText="Clave" SortExpression="Clave" />
                        <asp:BoundField DataField="FechaIngreso" HeaderText="FechaIngreso"
                            SortExpression="FechaIngreso" Visible="False" />
                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                            SortExpression="Estatus" Visible="False" />
                        <asp:BoundField DataField="FechaModif" HeaderText="FechaModif"
                            SortExpression="FechaModif" Visible="False" />
                        <asp:BoundField DataField="Modifico" HeaderText="Modifico"
                            SortExpression="Modifico" Visible="False" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  select
	                1  idCOORDINADOR
	                2  idusuario
	                3  nombre
	                4  apellidos
	                5  clave
	                6  fechaingreso
	                7  email
	                8  estatus
	                9  fechamodif
	                10 modifico
	                --%>
            </td>
        </tr>
        <tr>
            <td class="style17">&nbsp;</td>
            <td style="text-align: left">&nbsp;</td>
            <td style="text-align: left">&nbsp;</td>
            <td style="text-align: left">&nbsp;</td>
            <td style="text-align: left">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style24">Elija la
                <asp:Label ID="Label3" runat="server" Text="[INSTITUCION]" /></td>
            <td style="text-align: left" colspan="3">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                    DataValueField="IdInstitucion" Width="600px">
                </asp:DropDownList>
            </td>
            <td style="text-align: left">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style15" colspan="5">&nbsp;</td>
            <td class="style16"></td>
            <td class="style16"></td>
            <td class="style16"></td>
        </tr>
        <tr>
            <td colspan="8" style="text-align: left">
                <asp:GridView ID="GVPlanteles" runat="server" 
                    AllowPaging="True"
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 
                    DataKeyNames="IdPlantel" 
                    DataSourceID="SDSPlanteles" 
                    Caption="<h3>Seleccione el PLANTEL que desea asignar al COORDINADOR elegido, debe asignar uno por uno</h3>"
                    PageSize="15"

                    BackColor="White"
                    BorderColor="#999999" 
                    BorderStyle="Solid" 
                    BorderWidth="1px" 
                    CellPadding="3"
                    GridLines="Vertical"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"
                    Width="880px"
                    ForeColor="Black" >
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel" />
                        <asp:BoundField DataField="IdLicencia" HeaderText="IdLicencia"
                            SortExpression="IdLicencia" />
                        <asp:BoundField DataField="IdInstitucion" HeaderText="IdInstitucion"
                            SortExpression="IdInstitucion" />
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion"
                            SortExpression="Descripcion" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  select
	                1  idPLANTEL
	                2  idlicencia
	                3  idINSTITUCION
	                4  descripcion
	                --%>
            </td>
        </tr>
        <tr>
            <td colspan="8" style="text-align: left">
                <asp:Button ID="Asignar" runat="server" Text="Asignar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td class="style23" colspan="5">&nbsp;</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="8">
                <asp:GridView ID="GVDatosPlanteles" runat="server" 
                    AllowPaging="True"
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 
                    AutoGenerateSelectButton="True"
                    DataSourceID="SDSinformacion" 
                    DataKeyNames="IdPlantel,IdCoordinador" 
                    Caption="<h3>Listado de PLANTELES asignados a COORDINADORES de la INSTITUCION elegida</h3>"
                    PageSize="15" 

                    CellPadding="3" 
                    ForeColor="Black"
                    GridLines="Vertical" 
                    Height="17px" 
                    Style="font-size: x-small; font-family: Arial, Helvetica, sans-serif;"
                    Width="881px" 
                    BackColor="White"
                    BorderColor="#999999" 
                    BorderStyle="Solid" 
                    BorderWidth="1px">
                    <Columns>
                        <asp:BoundField DataField="IdLicencia" HeaderText="Id_Licencia"
                            SortExpression="IdLicencia" />
                        <asp:BoundField DataField="Clave" HeaderText="Licencia"
                            SortExpression="Clave" />
                        <asp:BoundField DataField="IdInstitucion" HeaderText="Id-Inst."
                            InsertVisible="False" ReadOnly="True" SortExpression="IdInstitucion" />
                        <asp:BoundField DataField="Descripcion" HeaderText="Institución"
                            SortExpression="Descripcion" />
                        <asp:BoundField DataField="IdCoordinador" HeaderText="Id-Coord."
                            InsertVisible="False" ReadOnly="True" SortExpression="IdCoordinador" />
                        <asp:BoundField DataField="NomCoordinador" HeaderText="Coordinador"
                            ReadOnly="True" SortExpression="NomCoordinador" />
                        <asp:BoundField DataField="IdPlantel" HeaderText="Id-Plantel"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel" />
                        <asp:BoundField DataField="Descripcion1" HeaderText="Plantel"
                            SortExpression="Descripcion1" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  select
	                1  idlicencia
	                2  clave
	                3  id_INSTITUCION
	                4  INSTITUCION
	                5  idCOORDINADOR
	                6  COORDINADOR
	                7  idPLANTEL
	                8  PLANTEL
	                9  
	                10 
	                --%>
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="8">
                <asp:Button ID="Desasignar" runat="server" CssClass="defaultBtn btnThemeGrey btnThemeMedium"
                    Text="Desasignar" />
            </td>
        </tr>
        <tr>
            <td class="style11">
                &nbsp;</td>
            <td class="style11">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="8">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSinformacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select P.IdLicencia, L.Clave, I.IdInstitucion, I.Descripcion, C.IdCoordinador, C.Apellidos+', '+Nombre+ ' (' + C.Clave+ ')' NomCoordinador, P.IdPlantel, P.Descripcion
from Licencia L, Institucion I, Coordinador C, Plantel P, CoordinadorPlantel CP
where P.IdLicencia = L.IdLicencia and P.IdInstitucion = I.IdInstitucion and P.IdPlantel = CP.IdPlantel and C.IdCoordinador = CP.IdCoordinador and I.IdInstitucion = @IdInstitucion
order by I.Descripcion, P.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdInstitucion], [Descripcion] FROM [Institucion] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSCoordinadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Coordinador] ORDER BY [Apellidos], [Nombre]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSPlanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE [IdInstitucion] = @IdInstitucion AND IdPlantel NOT IN (SELECT IdPlantel FROM CoordinadorPlantel WHERE IdCoordinador = @IdCoordinador) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="GVCoordinadores" Name="IdCoordinador"
                            PropertyName="SelectedDataKey.Values[IdCoordinador]" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSCoordPlantel" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CoordinadorPlantel]"></asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

