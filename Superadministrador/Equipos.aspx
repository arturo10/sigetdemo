﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="Equipos.aspx.vb"
    Inherits="superadministrador_Equipos"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style25 {
            font-size: small;
            font-weight: normal;
        }

        .style26 {
            text-align: right;
        }

        .style29 {
            text-align: left;
        }

        .style30 {
            height: 31px;
            width: 192px;
            text-align: right;
        }

        .style31 {
            height: 31px;
            text-align: left;
        }

        .style32 {
            height: 13px;
            text-align: left;
        }

        .style33 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .estandar {
            font-family: Arial, Helvetica, sans-serif;
        }

        .estandar {
            font-family: Arial, Helvetica, sans-serif;
        }

        .estandar {
            font-size: small;
        }

        .style34 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: medium;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>
        <asp:Label ID="Label13" runat="server" Text="[EQUIPOS]" />
        y 
				<asp:Label ID="Label14" runat="server" Text="[SUBEQUIPOS]" />
    </h1>
    Permite crear un catálogo de 
												<asp:Label ID="Label1" runat="server" Text="[EQUIPOS]" />
    y 
												<asp:Label ID="Label2" runat="server" Text="[SUBEQUIPOS]" />
    para elegirlos al dar de alta los 
												<asp:Label ID="Label3" runat="server" Text="[ALUMNOS]" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td colspan="3">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style30">
                <span class="estandar" style="text-align: right">
                    <asp:Label ID="Label4" runat="server" Text="[INSTITUCION]" /></span></td>
            <td class="style31" colspan="2">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                    DataValueField="IdInstitucion" Width="550px" Height="22px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style29">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style29" colspan="3">
                <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"
                    Width="880px">
                    <asp:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1">
                        <HeaderTemplate>
                            <span class="style34">
                                <asp:Label ID="Label5" runat="server" Text="[EQUIPOS]" /></span>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:Panel ID="Panel3" runat="server" BackColor="#CCCCCC"
                                Style="text-align: center" Width="617px">
                                <table class="style10">
                                    <tr>
                                        <td class="style31" colspan="2">
                                            <span class="style33">Capture los 
																								<asp:Label ID="Label6" runat="server" Text="[EQUIPOS]" />
                                                que podrán ser seleccionados para los 
                                            <asp:Label ID="Label7" runat="server" Text="[ALUMNOS]" /></span></td>
                                    </tr>
                                    <tr>
                                        <td class="style26">
                                            <span class="estandar">
                                                <asp:Label ID="Label8" runat="server" Text="[EQUIPO]" />
                                            </span></td>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="TBequipo" runat="server" MaxLength="100"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style26">&nbsp;</td>
                                        <td style="text-align: left">
                                            <asp:Button ID="BtnAgregar" runat="server" Text="Agregar"
                                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                            &nbsp;
                                            <asp:Button ID="BtnQuitar" runat="server" Text="Quitar" Visible="False"
                                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                                            &nbsp;
                                            <asp:Button ID="BtnActualizar" runat="server" Text="Actualizar" Visible="False"
                                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style26" colspan="2">
                                            <asp:GridView ID="GVequipos" runat="server" 
                                                AllowPaging="True"
                                                AllowSorting="True" 
                                                AutoGenerateColumns="False" 
                                                DataKeyNames="IdEquipo" 
                                                DataSourceID="SDSequipos"
                                                Caption="<h3>EQUIPOS capturados</h3>"

                                                BackColor="White"
                                                BorderColor="#999999" 
                                                BorderStyle="Solid" 
                                                BorderWidth="1px"
                                                CellPadding="3" 
                                                ForeColor="Black" GridLines="Vertical" Height="17px"
                                                Style="font-size: small; text-align: left; font-family: Arial, Helvetica, sans-serif;"
                                                Width="594px">
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True">
                                                        <HeaderStyle Width="100px" />
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="IdEquipo" HeaderText="Id Equipo"
                                                        InsertVisible="False" ReadOnly="True" SortExpression="IdEquipo">
                                                        <HeaderStyle Width="60px" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="IdInstitucion" HeaderText="Id Institución"
                                                        SortExpression="IdInstitucion">
                                                        <HeaderStyle Width="90px" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripción" SortExpression="Descripcion" />
                                                </Columns>
                                                <AlternatingRowStyle BackColor="#CCCCCC" />
                                                <FooterStyle BackColor="#CCCCCC" />
                                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" Font-Size="Small"/>
                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>
                                            <%-- sort YES - rowdatabound
	                                            0  select
	                                            1  id_EQUIPO
	                                            2  id_INSTITUCION
	                                            3  descripcion
	                                            --%>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <span class="style34">
                                <asp:Label ID="Label12" runat="server" Text="[SUBEQUIPOS]" /></span>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <asp:Panel ID="Panel4" runat="server" BackColor="#CCCCCC"
                                Style="text-align: center" Width="617px">
                                <table class="style10">
                                    <tr>
                                        <td class="style31" colspan="2">
                                            <span class="style33">Capture los 
												<asp:Label ID="Label9" runat="server" Text="[SUBEQUIPOS]" />
                                                que podrán ser seleccionados para 
                                                los 
												<asp:Label ID="Label10" runat="server" Text="[ALUMNOS]" />
                                            </span></td>
                                    </tr>
                                    <tr>
                                        <td class="style26">
                                            <span class="estandar">
                                                <asp:Label ID="Label11" runat="server" Text="[SUBEQUIPO]" /></span></td>
                                        <td style="text-align: left">
                                            <asp:TextBox ID="TBsubequipo" runat="server" MaxLength="20"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style26">&nbsp;</td>
                                        <td style="text-align: left">
                                            <asp:Button ID="BtnAgregarS" runat="server" Text="Agregar"
                                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                            &nbsp;
                                            <asp:Button ID="BtnQuitarS" runat="server" Text="Quitar" Visible="False"
                                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                                            &nbsp;
                                            <asp:Button ID="BtnActualizarS" runat="server" Text="Actualizar"
                                                Visible="False" CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style26" colspan="2">
                                            <asp:GridView ID="GVsubequipos" runat="server" 
                                                AllowPaging="True"
                                                AllowSorting="True" 
                                                AutoGenerateColumns="False" 
                                                Caption="<h3>SUBEQUIPOS capturados</h3>"
                                                DataKeyNames="IdSubequipo" 
                                                DataSourceID="SDSsubequipos"

                                                BackColor="White"
                                                BorderColor="#999999" 
                                                BorderStyle="Solid" 
                                                BorderWidth="1px"
                                                CellPadding="3" 
                                                ForeColor="Black" 
                                                GridLines="Vertical" 
                                                Height="17px"
                                                Style="font-size: small; text-align: left; font-family: Arial, Helvetica, sans-serif;"
                                                Width="570px">
                                                <Columns>
                                                    <asp:CommandField ShowSelectButton="True">
                                                        <HeaderStyle Width="100px" />
                                                    </asp:CommandField>
                                                    <asp:BoundField DataField="IdSubequipo" HeaderText="Id Subequipo"
                                                        InsertVisible="False" ReadOnly="True" SortExpression="IdSubequipo">
                                                        <HeaderStyle Width="90px" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="IdInstitucion" HeaderText="Id Institución"
                                                        SortExpression="IdInstitucion">
                                                        <HeaderStyle Width="90px" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripción"
                                                        SortExpression="Descripcion" />
                                                </Columns>
                                                <AlternatingRowStyle BackColor="#CCCCCC" />
                                                <FooterStyle BackColor="#CCCCCC" />
                                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White"  Font-Size="Small"/>
                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>
                                            <%-- sort YES - rowdatabound
	                                            0  select
	                                            1  id_SUBEQUIPO
	                                            2  id_INSTITUCION
	                                            3  descripcion
	                                            --%>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
            </td>
        </tr>
        <tr>
            <td class="style29">&nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style29">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td colspan="2">&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdInstitucion], [Descripcion] FROM [Institucion] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSequipos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Equipo] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSsubequipos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Subequipo] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

