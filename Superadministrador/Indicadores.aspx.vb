﻿Imports Siget


Partial Class coordinador_Indicadores
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        'Puede haber otro tipo de escala donde el valor mínimo para rojo no sea Cero
        If Trim(TBrojo.Text).Length = 0 Then
            TBrojo.Text = "0.0"
        End If
        If Trim(TBrojoPorc.Text).Length = 0 Then
            TBrojoPorc.Text = "0.0"
        End If
    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        Try
            SDSindicadores.InsertCommand = "SET dateformat dmy; INSERT INTO Indicador (Descripcion,MinAzul,MinVerde,MinAmarillo,MinRojo,MinAzulPorc,MinVerdePorc,MinAmarilloPorc,MinRojoPorc, FechaModif, Modifico) VALUES ('" + TBdescripcion.Text + "'," + TBazul.Text + "," + TBverde.Text + "," + TBamarillo.Text + "," + TBrojo.Text + "," + TBazulPorc.Text + "," + TBverdePorc.Text + "," + TBamarilloPorc.Text + "," + TBrojoPorc.Text + ", getdate(), '" & User.Identity.Name & "')"
            SDSindicadores.Insert()
            Mensaje.Text = "El registro ha sido guardado"
            GVindicadores.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            Mensaje.Text = ""
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click
        TBdescripcion.Text = ""
        TBazul.Text = ""
        TBverde.Text = ""
        TBamarillo.Text = ""
        TBrojo.Text = ""
        TBazulPorc.Text = ""
        TBverdePorc.Text = ""
        TBamarilloPorc.Text = ""
        TBrojoPorc.Text = ""
        Mensaje.Text = "Capture los datos"
    End Sub

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        Try
            SDSindicadores.DeleteCommand = "Delete from Indicador where IdIndicador = " + GVindicadores.SelectedValue.ToString 'Me da el IdIndicador porque es el campo clave de la fila seleccionada
            SDSindicadores.Delete()

            TBdescripcion.Text = ""
            TBazul.Text = ""
            TBverde.Text = ""
            TBamarillo.Text = ""
            TBrojo.Text = ""
            TBazulPorc.Text = ""
            TBverdePorc.Text = ""
            TBamarilloPorc.Text = ""
            TBrojoPorc.Text = ""
            'MsgBox("El registro ha sido borrado")
            Mensaje.Text = "El registro ha sido borrado. Puede insertar nuevos datos"
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            'MsgBox(ex.Message.ToString)
            Mensaje.Text = ""
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        Try
            SDSindicadores.UpdateCommand = "SET dateformat dmy; UPDATE Indicador SET Descripcion = '" + TBdescripcion.Text + "', MinAzul='" + TBazul.Text + "', MinVerde='" + TBverde.Text + "',MinAmarillo='" + TBamarillo.Text + "',MinRojo='" + TBrojo.Text + "',MinAzulPorc='" + TBazulPorc.Text + "',MinVerdePorc='" + TBverdePorc.Text + "',MinAmarilloPorc='" + TBamarilloPorc.Text + "',MinRojoPorc='" + TBrojoPorc.Text + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdIndicador = " + GVindicadores.SelectedValue.ToString 'Me da el IdIndicador porque es el campo clave de la fila seleccionada
            SDSindicadores.Update()
            Mensaje.Text = "El registro ha sido actualizado"
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            Mensaje.Text = ""
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVindicadores_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVindicadores.SelectedIndexChanged
        'Los campos deben estar con visible=true para poder leer el dato con SelectedRow (el numero depende en como se acomodaron en el GridView en Colección de columnas)
        TBdescripcion.Text = HttpUtility.HtmlDecode(GVindicadores.SelectedRow.Cells(2).Text)
        TBazul.Text = GVindicadores.SelectedRow.Cells(6).Text
        TBverde.Text = GVindicadores.SelectedRow.Cells(5).Text
        TBamarillo.Text = GVindicadores.SelectedRow.Cells(4).Text
        TBrojo.Text = GVindicadores.SelectedRow.Cells(3).Text
        TBazulPorc.Text = GVindicadores.SelectedRow.Cells(10).Text
        TBverdePorc.Text = GVindicadores.SelectedRow.Cells(9).Text
        TBamarilloPorc.Text = GVindicadores.SelectedRow.Cells(8).Text
        TBrojoPorc.Text = GVindicadores.SelectedRow.Cells(7).Text
        Mensaje.Text = "Si modifica los datos, deberá dar Actualizar"

        'GVrangos.DataBind()
        TBinferior.Text = ""
        TBsuperior.Text = ""
        TBcategoria.Text = ""
        TBcolor.Text = ""
        PnlColor.BackColor = Drawing.Color.White
        msgError.hide()
    End Sub

    Protected Sub BtnInsertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnInsertar.Click
        Try
            SDSrangos.InsertCommand = "SET dateformat dmy; INSERT INTO Rango(IdIndicador,Inferior,Superior,Categoria,Color, FechaModif, Modifico) VALUES(" + _
                    GVindicadores.SelectedValue.ToString + "," + TBinferior.Text + "," + TBsuperior.Text + ",'" + TBcategoria.Text + _
                    "','" + TBcolor.Text + "', getdate(), '" & User.Identity.Name & "')"
            SDSrangos.Insert()
            msgError.hide()
            PnlColor.BackColor = Drawing.ColorTranslator.FromHtml("#" + TBcolor.Text)
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnActualizar.Click
        Try
            SDSrangos.UpdateCommand = "SET dateformat dmy; UPDATE Rango SET Inferior = " + TBinferior.Text + ", Superior = " + TBsuperior.Text + ", Categoria = '" + _
                        TBcategoria.Text + "', Color = '" + TBcolor.Text + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdRango = " + GVrangos.SelectedRow.Cells(1).Text
            SDSrangos.Update()
            GVrangos.DataBind()
            msgError.hide()
            PnlColor.BackColor = Drawing.ColorTranslator.FromHtml("#" + TBcolor.Text)
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEliminar.Click
        Try
            SDSrangos.DeleteCommand = "DELETE FROM Rango WHERE IdRango = " + GVrangos.SelectedRow.Cells(1).Text
            SDSrangos.Delete()
            GVrangos.DataBind()
            TBinferior.Text = ""
            TBsuperior.Text = ""
            TBcategoria.Text = ""
            TBcolor.Text = ""
            PnlColor.BackColor = Drawing.Color.White
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVrangos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVrangos.DataBound
        If GVrangos.Rows.Count > 0 Then
            BtnEliminar.Visible = True
        Else
            BtnEliminar.Visible = False
        End If
    End Sub

    Protected Sub GVrangos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVrangos.SelectedIndexChanged
        Try
            TBinferior.Text = Trim(GVrangos.SelectedRow.Cells(3).Text)
            TBsuperior.Text = Trim(GVrangos.SelectedRow.Cells(4).Text)
            TBcategoria.Text = HttpUtility.HtmlDecode(GVrangos.SelectedRow.Cells(5).Text)
            TBcolor.Text = Trim(GVrangos.SelectedRow.Cells(6).Text)
            PnlColor.BackColor = Drawing.ColorTranslator.FromHtml("#" + GVrangos.SelectedRow.Cells(6).Text)
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVrangos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVrangos.RowDataBound
        If (e.Row.Cells.Count > 1) And (e.Row.RowType = DataControlRowType.DataRow) Then
            e.Row.Cells(5).BackColor = Drawing.ColorTranslator.FromHtml("#" + e.Row.Cells(6).Text)
        End If
        e.Row.Cells(6).Visible = False
    End Sub
End Class
