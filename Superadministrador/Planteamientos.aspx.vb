﻿Imports Siget

Imports System.IO
Imports System.Data.SqlClient
Imports Siget.Entity

Partial Class superadministrador_Planteamientos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        TitleLiteral.Text = "Planteamiento"

        Label1.Text = Config.Etiqueta.NIVEL
        Label2.Text = Config.Etiqueta.GRADO
        Label3.Text = Config.Etiqueta.ASIGNATURA
        CBcalifica.Text = "Califica el " & Config.Etiqueta.PROFESOR

        UserInterface.Include.HtmlEditor(CType(Master.FindControl("pnlHeader"), Literal), 700, 250, 12, Session("Usuario_Idioma"))

        If PnlWidth.Visible Then
            tbExample.Width = HfWidth.Value
            WidthLabel.Text = HfWidth.Value
            ChkMayusculas.Checked = Boolean.Parse(HfMayusculas.Value)
            ChkVerifica.Checked = Boolean.Parse(HfVerifica.Value)
        End If
    End Sub

    Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click
        Siget.Utils.GeneralUtils.CleanAll(Me.Controls)
    End Sub


    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        msgError.hide()
        msgSuccess.hide()
        Dim ArchivoApoyo2 As String = String.Empty
        Dim ArchivoApoyo1 As String = String.Empty
        Dim TipoArchivo1 As String = String.Empty
        Dim TipoArchivo2 As String = String.Empty

        If Trim(TBconsecutivo.Text) = "" Then TBconsecutivo.Text = "0"
        Dim Minutos = Trim(TBmin.Text)
        If SnippetTiempo.Attributes("style") = "display:none;" Then
            Minutos = "NULL"
        End If

        Try
            Dim ruta As String
            ruta = Config.Global.rutaMaterial
            If RutaArchivo.HasFile Then 'El atributo .HasFile compara si se indico un archivo
                Siget.Utils.FileUtils.SaveFileToDisk(RutaArchivo, RutaArchivo.FileName, HLarchivo1)
                ArchivoApoyo1 = RutaArchivo.FileName
                TipoArchivo1 = DDLtipoarchivo.SelectedValue
            End If

            If RutaArchivo2.HasFile Then
                Siget.Utils.FileUtils.SaveFileToDisk(RutaArchivo2, RutaArchivo2.FileName, HLarchivo2)
                ArchivoApoyo2 = RutaArchivo2.FileName
                TipoArchivo2 = DDLtipoarchivo2.SelectedValue

            End If

            SDSplanteamientos.InsertParameters.Clear()

            SDSplanteamientos.InsertCommand = "SET dateformat dmy; INSERT INTO Planteamiento (IdSubtema, Consecutivo," +
                    "Redaccion,TipoRespuesta,Ponderacion,PorcentajeRestarResp,Libro,Autor,Editorial,FechaModif,Modifico" +
                    ",Ocultar,EsconderTexto,Tiempo,Minutos,Permite2,HabilitaRecorder,ArchivoApoyo,TipoArchivoApoyo,ArchivoApoyo2,TipoArchivoApoyo2," +
                    "Ancho1,Verifica,ComparaMayusculas,ComparaAcentos,EliminaPuntuacion" +
                    ",ComparaSoloAlfanumerico) VALUES (" + DDLsubtemas.SelectedValue + "," +
                    TBconsecutivo.Text + ",@Redaccion,'" + DDLtiporespuesta.SelectedValue + "'," + TBponderacion.Text +
                    "," + TBporcentaje.Text + ",'" + TBlibro.Text + "','" + TBautor.Text + "','" + TBeditorial.Text +
                    "',getdate(),'" + User.Identity.Name.ToString + "','" + CBocultar.Checked.ToString + "','" +
                    CBescondetexto.Checked.ToString + "','" + CBtiempo.Checked.ToString + "'," + Minutos + ",'" +
                    CB2vuelta.Checked.ToString + "','" + IIf(CBRecorder.Checked, CBRecorder.Checked.ToString, Nothing) +
                    "','" + ArchivoApoyo1 + "','" + TipoArchivo1 + "','" + ArchivoApoyo2 + "','" + TipoArchivo2 + "',@Ancho1, @Verifica," +
                    " @ComparaMayusculas, @ComparaAcentos, @ComparaSoloAlfanumerico,@EliminaPuntuacion); SELECT @NuevoID = @@Identity"

            Dim p As Parameter = New Parameter("NuevoId")
            p.Type = TypeCode.Int16
            p.Direction = Data.ParameterDirection.Output
            p.Size = 4
            SDSplanteamientos.InsertParameters.Add(p)
            If panelAbiertaCalificada.Attributes("style") = "display:none;" Then
                SDSplanteamientos.InsertParameters.Add("Ancho1", HfWidth.Value)
                SDSplanteamientos.InsertParameters.Add("Verifica", HfVerifica.Value)
                SDSplanteamientos.InsertParameters.Add("ComparaMayusculas", HfMayusculas.Value)
                SDSplanteamientos.InsertParameters.Add("ComparaAcentos", HfAcentos.Value)
                SDSplanteamientos.InsertParameters.Add("ComparaSoloAlfanumerico", HfAlfanumerico.Value)
                SDSplanteamientos.InsertParameters.Add("EliminaPuntuacion", HFPuntuacion.Value)
            Else
                SDSplanteamientos.InsertParameters.Add("Ancho1", 180)
                SDSplanteamientos.InsertParameters.Add("Verifica", False)
                SDSplanteamientos.InsertParameters.Add("ComparaMayusculas", False)
                SDSplanteamientos.InsertParameters.Add("ComparaAcentos", False)
                SDSplanteamientos.InsertParameters.Add("ComparaSoloAlfanumerico", False)
                SDSplanteamientos.InsertParameters.Add("EliminaPuntuacion", False)
            End If

            SDSplanteamientos.InsertParameters.Add("Redaccion", Replace(TBredaccion.Text, "'", "´"))
            SDSplanteamientos.Insert()
            msgSuccess.show("Éxito", "El registro ha sido guardado.")
            GVplanteamientos.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub


    Protected Sub GVplanteamientos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVplanteamientos.SelectedIndexChanged
        msgError.hide()
        msgSuccess.hide()

        ''PnlWidth.Visible = False

        'La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
        TBredaccion.Text = GVplanteamientos.SelectedDataKey(5).ToString()
        DDLtiporespuesta.SelectedValue = HttpUtility.HtmlDecode(Trim(GVplanteamientos.SelectedRow.Cells(5).Text)) 'Es necesario el Trim porque lee el valor con espacios
        TBponderacion.Text = HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(6).Text)
        TBporcentaje.Text = HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(7).Text)
        HLarchivo1.Text = HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(8).Text)
        HLarchivo1.NavigateUrl = Config.Global.urlMaterial & HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(8).Text)
        HLarchivo2.Text = HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(10).Text)
        HLarchivo2.NavigateUrl = Config.Global.urlMaterial & HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(10).Text)
        'Si ya tenía un Planteamiento con un archivo de apoyo y selecciono ese Planteamiento y le pongo "Ninguno" al Tipo de Archivo, este se borra
        'Por tanto es importante tener presente cuando ya hay un archivo precargado
        If Trim(GVplanteamientos.SelectedRow.Cells(9).Text) = "&nbsp;" Then
            DDLtipoarchivo.SelectedIndex = 0 'Ninguno
        Else
            DDLtipoarchivo.SelectedValue = HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(9).Text)
        End If
        If Trim(GVplanteamientos.SelectedRow.Cells(11).Text) = "&nbsp;" Then
            DDLtipoarchivo2.SelectedIndex = 0 'Ninguno
        Else
            DDLtipoarchivo2.SelectedValue = HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(11).Text)
        End If
        TBconsecutivo.Text = HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(3).Text)
        TBlibro.Text = HttpUtility.HtmlDecode(GVplanteamientos.SelectedDataKey("Libro").ToString)
        TBautor.Text = HttpUtility.HtmlDecode(GVplanteamientos.SelectedDataKey("Autor").ToString)
        TBeditorial.Text = HttpUtility.HtmlDecode(GVplanteamientos.SelectedDataKey("Editorial").ToString)
        CBocultar.Checked = GVplanteamientos.SelectedRow.Cells(15).Text
        CBtiempo.Checked = GVplanteamientos.SelectedRow.Cells(16).Text
        CBescondetexto.Checked = GVplanteamientos.SelectedDataKey("EsconderTexto").ToString
        CBRecorder.Checked = IIf(Not IsDBNull(GVplanteamientos.SelectedDataKey("HabilitaRecorder")), GVplanteamientos.SelectedDataKey("HabilitaRecorder").ToString, 0)
        If CBtiempo.Checked Then
            SnippetTiempo.Attributes("style") = "display:block;"
        Else
            SnippetTiempo.Attributes("style") = "display:none;"
        End If
        TBmin.Text = GVplanteamientos.SelectedRow.Cells(17).Text
        CB2vuelta.Checked = GVplanteamientos.SelectedRow.Cells(18).Text
        If HttpUtility.HtmlDecode(Trim(GVplanteamientos.SelectedRow.Cells(5).Text)) = "Abierta" Then
            CBcalifica.Visible = True
            CB2vuelta.Checked = False
            CB2vuelta.Visible = False

            panelAbiertaCalificada.Attributes("style") = "display:none;"
            ContainerCalifica.Attributes("style") = "display:block;"

            'Como era respuesta abierta abierta, saco el valor que tenía guardado en la tabla de opciones
            Dim strConexion As String
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objConexion As New SqlConnection(strConexion)
            Dim miComando As SqlCommand
            Dim datoOpcion As SqlDataReader
            miComando = objConexion.CreateCommand
            miComando.CommandText = "select SeCalifica from Opcion where IdPlanteamiento = " + GVplanteamientos.SelectedRow.Cells(1).Text
            objConexion.Open() 'Abro la conexion
            datoOpcion = miComando.ExecuteReader() 'Creo conjunto de registros
            If datoOpcion.Read() Then
                If IsDBNull(datoOpcion("SeCalifica")) Then
                    CBcalifica.Checked = False
                Else

                    CBcalifica.Checked = datoOpcion("SeCalifica")
                End If
            End If
            datoOpcion.Close()
            objConexion.Close()
        ElseIf HttpUtility.HtmlDecode(Trim(GVplanteamientos.SelectedRow.Cells(5).Text)) = "Abierta Calificada" Then
            panelAbiertaCalificada.Attributes("style") = "display:block;"
            HfWidth.Value = GVplanteamientos.SelectedDataKey(6).ToString()
            HfVerifica.Value = GVplanteamientos.SelectedDataKey(7).ToString()
            HfMayusculas.Value = GVplanteamientos.SelectedDataKey(8).ToString()
            HfAcentos.Value = GVplanteamientos.SelectedDataKey(9).ToString()
            HfAlfanumerico.Value = GVplanteamientos.SelectedDataKey(10).ToString()
            HfPuntuacion.Value = GVplanteamientos.SelectedDataKey("EliminaPuntuacion").ToString()

            tbExample.Width = HfWidth.Value
            WidthLabel.Text = HfWidth.Value
            ChkMayusculas.Checked = Boolean.Parse(HfMayusculas.Value)
            ChkVerifica.Checked = Boolean.Parse(HfVerifica.Value)
            ChkAcentos.Checked = Boolean.Parse(HfAcentos.Value)
            ChkAlfaNum.Checked = Boolean.Parse(HfAlfanumerico.Value)
            CkPuntuacion.Checked = Boolean.Parse(HFPuntuacion.Value)
            ContainerCalifica.Attributes("style") = "display:none;"
        Else
            CBcalifica.Visible = False
            CB2vuelta.Visible = True
            panelAbiertaCalificada.Attributes("style") = "display:none;"
            ContainerCalifica.Attributes("style") = "display:none;"
        End If
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        Dim RepositoryPlanteamiento As IRepository(Of Planteamiento) = New PlanteamientoRepository()
        Dim RepositoryOpcion As IRepository(Of Opcion) = New OpcionRepository()
        Dim flagOut As Boolean = False
        Dim planteamiento As Planteamiento = RepositoryPlanteamiento.FindById(GVplanteamientos.SelectedRow.Cells(1).Text)
        Dim opcion As Opcion
        Dim ruta As String = Config.Global.rutaMaterial

        Try
            If Trim(planteamiento.TipoRespuesta) <> Trim(DDLtiporespuesta.SelectedValue) Then
                If (planteamiento.Opcions.Count = 1 And Trim(planteamiento.TipoRespuesta) <> "Multiple") Or
                     (Trim(planteamiento.TipoRespuesta) = "Multiple" And (planteamiento.Opcions.Count = 0)) Then
                    If Trim(planteamiento.TipoRespuesta) = "Abierta" Or Trim(planteamiento.TipoRespuesta) = "Abierta Calificada" Then
                        SDSopciones.DeleteCommand = "delete from Opcion where IdPlanteamiento = " + planteamiento.IdPlanteamiento
                        SDSopciones.Delete()
                    End If

                    If Trim(DDLtiporespuesta.SelectedValue) <> "Multiple" Then
                        Select Case Trim(DDLtiporespuesta.SelectedValue)
                            Case "Abierta"
                                SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion(IdPlanteamiento,Redaccion,Correcta," +
                                                           "Consecutiva,FechaModif,Modifico,Esconder,SeCalifica)" +
                                                           " VALUES(" + planteamiento.IdPlanteamiento.ToString +
                                                           ",'Respuesta Abierta','" + IIf(CBcalifica.Checked, "", "S") +
                                                           "','A',getdate(),'" + User.Identity.Name.ToString + "','False','" +
                                                           CBcalifica.Checked.ToString + "')"
                                SDSopciones.Insert()

                            Case "Abierta Calificada"
                                SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion(IdPlanteamiento,Redaccion,Correcta," +
                                                            "Consecutiva,FechaModif,Modifico,Esconder,SeCalifica)" +
                                                            "VALUES(" + planteamiento.IdPlanteamiento.ToString + ",'Incorrecta','N','-',getdate(),'" +
                                                            User.Identity.Name.ToString + "','False','False')"
                                SDSopciones.Insert()
                        End Select
                    End If
                Else
                    msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")),
                                "El planteamiento tiene más de una opción, si desea modificarlo , por favor eliminelas")
                    flagOut = True
                End If
            End If

            If Not flagOut Then
                'Si en el paso anterior no marca error, entonces procedo a actualizar el planteamiento
                If RutaArchivo.HasFile Then 'El atributo .HasFile compara si se indico un archivo
                    planteamiento.TipoArchivoApoyo = DDLtipoarchivo.SelectedValue
                    planteamiento.ArchivoApoyo = RutaArchivo.FileName
                    Siget.Utils.FileUtils.ProcessFile(RutaArchivo, RutaArchivo.FileName, planteamiento.ArchivoApoyo, HLarchivo1, planteamiento.ArchivoApoyo)

                End If
                If RutaArchivo2.HasFile Then 'Si entra aquí es que no cargará el archivo 1 pero sí el archivo 2 s
                    planteamiento.TipoArchivoApoyo2 = DDLtipoarchivo2.SelectedValue
                    planteamiento.ArchivoApoyo2 = RutaArchivo2.FileName
                    Siget.Utils.FileUtils.ProcessFile(RutaArchivo2, RutaArchivo2.FileName, planteamiento.ArchivoApoyo2, HLarchivo2, planteamiento.ArchivoApoyo2)

                End If

                If DDLtipoarchivo2.SelectedValue = "Ninguno" Or DDLtipoarchivo.SelectedValue = "Ninguno" Then

                    If DDLtipoarchivo2.SelectedValue = "Ninguno" Then

                        If planteamiento.ArchivoApoyo2 <> String.Empty Then 
                            Dim MiArchivo2 As FileInfo = New FileInfo(ruta & planteamiento.ArchivoApoyo2)
                            MiArchivo2.Delete()
                            HLarchivo2.Text = ""
                            HLarchivo2.NavigateUrl = ""
                        End If
                        planteamiento.TipoArchivoApoyo2 = String.Empty
                        planteamiento.ArchivoApoyo2 = String.Empty
                    End If

                    If DDLtipoarchivo.SelectedValue = "Ninguno" Then
                        If planteamiento.ArchivoApoyo <> String.Empty Then 
                            Dim MiArchivo As FileInfo = New FileInfo(ruta & Trim(planteamiento.ArchivoApoyo))
                            MiArchivo.Delete()
                            HLarchivo1.Text = ""
                            HLarchivo1.NavigateUrl = ""
                        End If
                        planteamiento.ArchivoApoyo = String.Empty
                        planteamiento.TipoArchivoApoyo = String.Empty
                    End If

                End If

                If SnippetTiempo.Attributes("style") = "display:block;" Then
                    planteamiento.Minutos = TBmin.Text
                Else
                    planteamiento.Minutos = Nothing
                End If


                With planteamiento
                    .Consecutivo = TBconsecutivo.Text
                    .Redaccion = HttpUtility.HtmlDecode(Replace(TBredaccion.Text, "'", "´"))
                    .TipoRespuesta = DDLtiporespuesta.SelectedValue
                    .Ponderacion = TBponderacion.Text
                    .PorcentajeRestarResp = TBporcentaje.Text
                    .Libro = TBlibro.Text
                    .Autor = TBautor.Text
                    .Editorial = TBeditorial.Text
                    .FechaModif = DateTime.Now()
                    .Modifico = User.Identity.Name
                    .Ocultar = CBocultar.Checked.ToString
                    .EsconderTexto = CBescondetexto.Checked.ToString
                    .Tiempo = CBtiempo.Checked.ToString
                    .Permite2 = CB2vuelta.Checked.ToString
                    .HabilitaRecorder = CBRecorder.Checked.ToString

                End With

                If Trim(planteamiento.TipoRespuesta) = "Abierta" Then
                    opcion = RepositoryOpcion.FindById((planteamiento.Opcions.FirstOrDefault()).IdOpcion)
                    opcion.SeCalifica = CBcalifica.Checked.ToString()
                    RepositoryOpcion.Update(opcion)
                End If

                If panelAbiertaCalificada.Attributes("style") = "display:block;" Then
                    With planteamiento
                        .Ancho1 = HfWidth.Value
                        .Verifica = HfVerifica.Value
                        .ComparaMayusculas = HfMayusculas.Value
                        .ComparaAcentos = HfAcentos.Value
                        .ComparaSoloAlfanumerico = HfAlfanumerico.Value
                        .EliminaPuntuacion = HfPuntuacion.Value
                    End With
                Else
                    With planteamiento
                        .Ancho1 = 180
                        .Verifica = False
                        .ComparaMayusculas = False
                        .ComparaAcentos = False
                        .ComparaSoloAlfanumerico = False
                        .EliminaPuntuacion = False
                    End With
                End If

                RepositoryPlanteamiento.Update(planteamiento)

                msgSuccess.show("Éxito", "El registro ha sido actualizado.")
                GVplanteamientos.DataBind()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    
    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            'NOTA: Se debe de tener cuidado de que al borrar planteamientos que tienen archivos de apoyo asignados, que no haya otros
            'Planteamientos que hagan referencia a esos mismos archivos porque cuando se borre el primer planteamiento también se borrarán
            'los archivos de apoyo

            'LOS REACTIVOS DE RESPUESTA ABIERTA GENERAN EN LA TABLA OPCION UN REGISTRO QUE TENGO QUE UTILIZAR
            If Trim(HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(5).Text)) = "Abierta" Or
                Trim(HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(5).Text)) = "Abierta Calificada" Then
                SDSopciones.DeleteCommand = "Delete from Opcion where IdPlanteamiento = " + GVplanteamientos.SelectedValue.ToString
                SDSopciones.Delete()
            End If

            SDSplanteamientos.DeleteCommand = "Delete from Planteamiento where IdPlanteamiento = " + GVplanteamientos.SelectedValue.ToString 'Me da el IdPlanteamiento porque es el campo clave de la fila seleccionada
            SDSplanteamientos.Delete()

            'Primero borro los archivos ligados al planteamiento (si los hay)
            Dim ruta As String
            ruta = Config.Global.rutaMaterial

            'Antes de borrar los archivos verifico que no estén referenciados en otros registros
            Dim Total = 0
            Dim strConexion As String
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objConexion As New SqlConnection(strConexion)
            Dim miComando As SqlCommand
            Dim misRegistros As SqlDataReader
            miComando = objConexion.CreateCommand
            If GVplanteamientos.SelectedRow.Cells(8).Text <> "&nbsp;" Then 'Significa que tiene un valor en ese campo
                miComando.CommandText = "select Count(*) as Total from ApoyoSubtema where ArchivoApoyo = '" + Trim(HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(8).Text)) + "'"
                objConexion.Open()
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read()
                Total = misRegistros.Item("Total")

                misRegistros.Close()
                miComando.CommandText = "select Count(*) as Total from Opcion where ArchivoApoyo = '" + Trim(HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(8).Text)) + "'"
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read()
                Total = Total + misRegistros.Item("Total")

                misRegistros.Close()
                miComando.CommandText = "select Count(*) as Total from Planteamiento where ArchivoApoyo = '" + Trim(HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(8).Text)) + "' or ArchivoApoyo2 = '" + Trim(HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(8).Text)) + "'"
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read()
                Total = Total + misRegistros.Item("Total")
                misRegistros.Close()
                objConexion.Close()

                If Total = 0 Then
                    'Borro el archivo que estaba anteriormente porque se pondrá uno nuevo
                    Dim MiArchivo As FileInfo = New FileInfo(ruta & Trim(HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(8).Text)))
                    MiArchivo.Delete()
                    HLarchivo1.Text = ""
                    HLarchivo1.NavigateUrl = ""
                End If
            End If

            If GVplanteamientos.SelectedRow.Cells(10).Text <> "&nbsp;" Then 'Significa que tiene un valor en ese campo
                Total = 0
                miComando.CommandText = "select Count(*) as Total from ApoyoSubtema where ArchivoApoyo = '" + Trim(HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(10).Text)) + "'"
                objConexion.Open()
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read()
                Total = misRegistros.Item("Total")

                misRegistros.Close()
                miComando.CommandText = "select Count(*) as Total from Opcion where ArchivoApoyo = '" + Trim(HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(10).Text)) + "'"
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read()
                Total = Total + misRegistros.Item("Total")

                misRegistros.Close()
                miComando.CommandText = "select Count(*) as Total from Planteamiento where ArchivoApoyo = '" + Trim(HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(10).Text)) + "' or ArchivoApoyo2 = '" + Trim(HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(10).Text)) + "'"
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read()
                Total = Total + misRegistros.Item("Total")
                misRegistros.Close()
                objConexion.Close()

                If Total = 0 Then
                    'Borro el archivo que estaba anteriormente porque se pondrá uno nuevo
                    Dim MiArchivo2 As FileInfo = New FileInfo(ruta & Trim(HttpUtility.HtmlDecode(GVplanteamientos.SelectedRow.Cells(10).Text)))
                    MiArchivo2.Delete()
                    HLarchivo2.Text = ""
                    HLarchivo2.NavigateUrl = ""
                End If
            End If

            GVplanteamientos.DataBind() 'Dejo esta instrucción al final porque si se refresca el GridView antes de borrar el archivo de apoyo, ya no podré saber su nombre
            msgSuccess.show("Éxito", "El registro ha sido borrado así como sus archivos de apoyo si los tenía.")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub


    Protected Sub SDSplanteamientos_Inserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSplanteamientos.Inserted
        msgError.hide()

        Try
            If Trim(DDLtiporespuesta.SelectedValue) = "Abierta" Then
                Dim Correcta As Char
                If CBcalifica.Checked Then
                    Correcta = "" 'Si será revisada por el profesor, entonces dejarla en blanco, por el momento no es correcta
                Else
                    Correcta = "S"
                End If
                'Necesito crearle un registro en la tabla opciones que dará por buena simplemente por contestar el reactivo de respuesta ABIERTA
                'ya que asi puede ser procesada por los algoritmos que son para los reactivos de Opción Múltiple
                Dim nuevoId = e.Command.Parameters("@NuevoId").Value
                SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion(IdPlanteamiento,Redaccion,Correcta,Consecutiva,FechaModif,Modifico,Esconder,SeCalifica) VALUES(" + nuevoId.ToString + ",'Respuesta Abierta','" + Correcta + "','A',getdate(),'" + User.Identity.Name.ToString + "','False','" + CBcalifica.Checked.ToString + "')"
                SDSopciones.Insert()
            ElseIf Trim(DDLtiporespuesta.SelectedValue) = "Abierta Calificada" Then

                ' Necesito crearle un registro en la tabla opciones que dará por mala 
                ' predeterminadamante si no coincide con nonguna de las opciones correctas
                Dim nuevoId = e.Command.Parameters("@NuevoId").Value
                SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion(IdPlanteamiento,Redaccion,Correcta,Consecutiva,FechaModif,Modifico,Esconder,SeCalifica) VALUES(" + nuevoId.ToString + ",'Incorrecta','N','-',getdate(),'" + User.Identity.Name.ToString + "','False','False')"
                SDSopciones.Insert()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub
    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVplanteamientos_PreRender(sender As Object, e As EventArgs) Handles GVplanteamientos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVplanteamientos.Rows.Count > 0 Then
            GVplanteamientos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVplanteamientos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVplanteamientos.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
                        icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If GVplanteamientos.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub GVplanteamientos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVplanteamientos.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVplanteamientos, "Select$" & e.Row.RowIndex).ToString())

            ' convierto el campo de respuesta de modo que se presente con estilos, no como markup
            Dim s As String = Regex.Replace(HttpUtility.HtmlDecode(e.Row.Cells(4).Text), "<[^>]*(>|$)", String.Empty)
            Dim l As Integer = s.Length
            Dim continua As String = ""
            If l > 100 Then
                l = 100
                continua = "...(Continúa)"
            End If
            e.Row.Cells(4).Text = s.Substring(0, l) & continua
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVplanteamientos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVplanteamientos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

End Class
