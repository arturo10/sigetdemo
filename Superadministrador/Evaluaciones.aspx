﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="Evaluaciones.aspx.vb"
    Inherits="superadministrador_Evaluaciones"
    MaintainScrollPositionOnPostback="true"
    ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            width: 900px;
        }

        .style30 {
            font-family: Arial, Helvetica, sans-serif;
        }

        .style21 {
            font-size: x-small;
        }

        .style40 {
        }

        .style41 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style42 {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: small;
        }

        .style44 {
            color: #CC0000;
            font-weight: bold;
            width: 155px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style45 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            height: 34px;
        }

        .style46 {
            height: 34px;
        }

        .style47 {
            height: 34px;
            text-align: right;
            width: 209px;
        }

        .style48 {
            text-align: center;
        }

        .style49 {
            width: 209px;
        }

        .style50 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            width: 209px;
        }

        .style51 {
            text-align: center;
            width: 209px;
        }

        .style52 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
            color: #000066;
            height: 27px;
        }

        .style53 {
            height: 37px;
        }

        .style54 {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: small;
            height: 25px;
            color: #000066;
        }

        .style55 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style56 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: xx-small;
        }

        .titleColumn {
            text-align: right;
        }

        .control_column {
            text-align: left;
        }
        .overlay_top {
            z-index: 10004;
        }
    </style>

    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Crear Actividades (Tareas y Exámenes)
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table>
        <tr>
            <td colspan="2">
                <b style="text-align: left">Seleccione la
                    <asp:Label ID="Label10" runat="server" Text="[ASIGNATURA]" />
                    donde se manejarán las Actividades</b>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Label ID="Label1" runat="server" Text="[CICLO]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Width="300px"
                    CssClass="style41" Height="22px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Label ID="Label2" runat="server" Text="[NIVEL]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Width="300px" Height="22px" CssClass="style41">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Label ID="Label3" runat="server" Text="[GRADO]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion"
                    DataValueField="IdGrado" Width="300px" Height="22px"
                    CssClass="style41">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Label ID="Label4" runat="server" Text="[ASIGNATURA]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSasignaturas" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Width="300px" Height="22px"
                    CssClass="style41">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                Calificación
            </td>
            <td class="control_column">
                <asp:DropDownList ID="ddlCalificacion" runat="server" AutoPostBack="True"
                    DataSourceID="SDScalificaciones" DataTextField="Descripcion"
                    DataValueField="IdCalificacion" Width="300px" Height="22px"
                    CssClass="style41">
                </asp:DropDownList>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <asp:Panel ID="pnlEvaluaciones" runat="server" Visible="false">
                    <table class="style23">
                        <tr>
                            <td colspan="5">
                                <uc1:msgSuccess runat="server" ID="msgSuccess" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <uc1:msgError runat="server" ID="msgError" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <uc1:msgInfo runat="server" ID="msgInfo" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <asp:LinkButton ID="btnNueva" runat="server"
                                    CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                                    <asp:Image ID="imgNueva" runat="server"
                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/add.png"
                                        CssClass="btnIcon"
                                        Height="24" />
                                    <asp:Label ID="lblNueva" runat="server">
                                        Crear
                                    </asp:Label>
                                </asp:LinkButton>
                                &nbsp;
                                <asp:LinkButton ID="btnModificar" runat="server"
                                    Visible="True" CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                                    <asp:Image ID="imgModificar" runat="server"
                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/disk.png"
                                        CssClass="btnIcon"
                                        Height="24" />
                                    <asp:Label ID="lblModificar" runat="server">
                                        Modificar
                                    </asp:Label>
                                </asp:LinkButton>
                                &nbsp;
                                <asp:LinkButton ID="btnEliminar" runat="server"
                                    Visible="True" CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                                    <asp:Image ID="imgEliminar" runat="server"
                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/delete.png"
                                        CssClass="btnIcon"
                                        Height="24" />
                                    <asp:Label ID="lblEliminar" runat="server">
                                        Eliminar
                                    </asp:Label>
                                </asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="style40" colspan="5">
                                <asp:GridView ID="GVevaluaciones" runat="server"
                                    AutoGenerateColumns="False"
                                    AllowSorting="True"

                                    DataKeyNames="IdEvaluacion,Califica, Aleatoria, Instruccion, RepiteIndicador, RepiteRango, RepiteMaximo, EsForo,OpcionesAleatorias,NoActividad"
                                    DataSourceID="SDSevaluaciones"
                                    Width="885px"
                                    Font-Size="X-Small"

                                    CssClass="dataGrid_clear_selectable"
                                    GridLines="None">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="true" ItemStyle-CssClass="selectCell" /><%-- 0 --%>
                                        <asp:BoundField DataField="IdEvaluacion" HeaderText="Id"
                                            Visible="False" ReadOnly="True" SortExpression="IdEvaluacion" /><%-- 1 --%>
                                        <asp:BoundField DataField="ClaveBateria" HeaderText="Clave de la Actividad"
                                            SortExpression="ClaveBateria" /><%-- 2 --%>
                                        <asp:BoundField DataField="ClaveAbreviada" HeaderText="Clave p/Reporte"
                                            SortExpression="ClaveAbreviada" /><%-- 3 --%>
                                        <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                                            SortExpression="Porcentaje"><%-- 4 --%>
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                            HeaderText="Inicia" SortExpression="InicioContestar" /><%-- 5 --%>
                                        <asp:BoundField DataField="FinSinPenalizacion"
                                            DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Máxima"
                                            SortExpression="FinSinPenalizacion" /><%-- 6 --%>
                                        <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                            HeaderText="Termina" SortExpression="FinContestar" /><%-- 7 --%>
                                        <asp:BoundField DataField="Penalizacion" HeaderText="% Penalización"
                                            SortExpression="Penalizacion"><%-- 8 --%>
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" /><%-- 9 --%>
                                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                            SortExpression="Estatus" /><%-- 10 --%>
                                        <asp:BoundField DataField="Aleatoria" HeaderText="Aleatoria"
                                            SortExpression="Aleatoria" /><%-- 11 --%>
                                        <asp:CheckBoxField DataField="SeEntregaDocto" HeaderText="Documento"
                                            SortExpression="SeEntregaDocto" /><%-- 12 --%>
                                        <asp:BoundField DataField="Califica" HeaderText="Califica"
                                            SortExpression="Califica" Visible="False" /><%-- 13 --%>
                                        <asp:CheckBoxField DataField="RepiteAutomatica" HeaderText="Repetición Automática"
                                            SortExpression="RepiteAutomatica" /><%-- 14 --%>
                                        <asp:CheckBoxField DataField="PermiteSegunda" HeaderText="Segunda Vuelta"
                                            SortExpression="PermiteSegunda" /><%-- 15 --%>
                                        <asp:BoundField DataField="FechaModif" HeaderText="Fecha de Modificación"
                                            Visible="False" SortExpression="FechaModif" /><%-- 16 --%>
                                        <asp:BoundField DataField="Modifico" HeaderText="Modificó"
                                            Visible="False" SortExpression="Modifico" /><%-- 17 --%>
                                    </Columns>
                                    <FooterStyle CssClass="footer" />
                                    <PagerStyle CssClass="pager" />
                                    <SelectedRowStyle CssClass="selected" />
                                    <HeaderStyle CssClass="header" />
                                    <AlternatingRowStyle CssClass="altrow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style41">&nbsp;</td>
            <td>&nbsp;</td>
            <td class="style49">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style40">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
            </td>
            <td class="style41">&nbsp;</td>
            <td class="style50">&nbsp;</td>
            <td class="style41">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <asp:Panel ID="pnlDatosEvaluacion" runat="server" CssClass="dialogOverwrite dialogRespuestaAbierta">
        <table>
            <tr>
                <td colspan="2">
                    <asp:Panel ID="pnlDatosEvaluacion_Datos" runat="server" Visible="true">
                        <table>
                            <tr>
                                <td class="titleColumn">Nombre para la Actividad:
                                </td>
                                <td class="control_column">
                                    <asp:TextBox ID="TBclavebateria" runat="server" Width="560px"
                                        MaxLength="200" CssClass="style41"></asp:TextBox>
                                </td>
                                <td class="titleColumn">No.Actividad:
                                </td>
                             <td class="control_column">
                                    <asp:TextBox ID="TBNoActividad" runat="server" Width="30px"
                                        MaxLength="3" TextMode="Number" CssClass="style41"></asp:TextBox>
                                </td>
                                </tr>
                            <tr>
                                <td class="titleColumn">Fechas de disponibilidad:
                                </td>
                                <td class="control_column">
                                    <table>
                                        <tr>
                                            <td>
                                                <span class="style42">Inicio</span><span class="style41"> de 
                                                                    Aplicación</span><br class="style41" />
                                            </td>
                                            <td>
                                                <span class="style41">Fecha </span><span class="style42">máxima</span><span class="style41">para</span>
                                                <br />
                                                <span class="style41"><b style="color: red;">evitar penalización</b></span>
                                            </td>
                                            <td>
                                                <span class="style42">Fin</span><span class="style41"> de
                                                                    Aplicación</span><br class="style41" />
                                                <span class="style41">de la Actividad</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-right: 10px;">
                                                <asp:TextBox ID="tbFechaInicio" runat="server"></asp:TextBox>
                                                <asp:Image ID="Image3" runat="server"
                                                    Title="Antes de ésta fecha no puede contestarse la actividad."
                                                    ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                                                    CssClass="infoPopper" />
                                                <asp:CalendarExtender ID="tbFechaInicio_CalendarExtender" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="tbFechaInicio">
                                                </asp:CalendarExtender>
                                            </td>
                                            <td style="padding-right: 10px;">
                                                <asp:TextBox ID="tbFechaPenalizacion" runat="server"></asp:TextBox>
                                                <asp:Image ID="Image4" runat="server"
                                                    Title=
"Entre esta fecha y el INICIO, se puede contestar la actividad normalmente. 
Desde esta fecha y el FIN, se puede contestar la actividad, pero la calificación obtenida recibirá la penalización definida abajo."
                                                    ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                                                    CssClass="infoPopper" />
                                                <asp:CalendarExtender ID="tbFechaPenalizacion_CalendarExtender" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="tbFechaPenalizacion">
                                                </asp:CalendarExtender>
                                            </td>
                                            <td style="padding-right: 10px;">
                                                <asp:TextBox ID="tbFechaFin" runat="server"></asp:TextBox>
                                                <asp:Image ID="Image5" runat="server"
                                                    Title="Después de esta fecha ya no se podrá realizar la actividad."
                                                    ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                                                    CssClass="infoPopper" />
                                                <asp:CalendarExtender ID="tbFechaFin_CalendarExtender" runat="server" Enabled="True"
                                                    Format="dd/MM/yyyy" TargetControlID="tbFechaFin">
                                                </asp:CalendarExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="titleColumn">
                                                            Porcentaje de la calificación:
                                                        </td>
                                                        <td class="control_column">
                                                            <asp:TextBox ID="TBporcentaje" runat="server" Width="55px"
                                                                Style="margin-left: 0px" CssClass="style41"></asp:TextBox>
                                                            <span class="style41">%</span>
                                                            <asp:Image ID="Image6" runat="server"
                                                                Title=
"Qué porcentaje de la calificación se califica con ésta actividad. 
La suma de todas las Evaluaciones debe ser 100; cada actividad representa una parte de la calificación."
                                                                ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                                                                CssClass="infoPopper" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="titleColumn">Abreviatura para reportes:
                                                        </td>
                                                        <td class="control_column">
                                                            <asp:TextBox ID="TBclaveE" runat="server" Height="22px" MaxLength="8"
                                                                Width="64px" CssClass="style41"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="titleColumn">Tipo
                                                        </td>
                                                        <td class="control_column">
                                                            <asp:DropDownList ID="DDLtipo" runat="server" Width="105px"
                                                                CssClass="style41">
                                                                <asp:ListItem>Aprendizaje</asp:ListItem>
                                                                <asp:ListItem>Evaluación</asp:ListItem>
                                                                <asp:ListItem>Material</asp:ListItem>
                                                                <asp:ListItem>Reactivos</asp:ListItem>
                                                                <asp:ListItem>Documento</asp:ListItem>
                                                                <asp:ListItem>Examen</asp:ListItem>
                                                                <asp:ListItem>Tarea</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="titleColumn">Asignación de fechas por 
                                                        </td>
                                                        <td class="control_column">
                                                            <asp:DropDownList ID="ddlAsignacion" runat="server" Width="108px"
                                                                CssClass="style41">
                                                                <asp:ListItem Value="0">Institución</asp:ListItem>
                                                                <asp:ListItem Value="1">Plantel</asp:ListItem>
                                                                <asp:ListItem Value="2">Alumno</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:Image ID="Image7" runat="server"
                                                                Title="Principalmente para reportes, indica el módo en el que será asignada la actividad a los alumnos. Al verse reportes de avance se muestran asignaciones de acuerdo a éste parámetro."
                                                                ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                                                                CssClass="infoPopper" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="vertical-align: top;">
                                                <table>
                                                    <tr>
                                                        <td class="titleColumn">
                                                            <span class="style41">Porcentaje de </span><span class="style44">penalización</span>
                                                        </td>
                                                        <td class="control_column">
                                                            <asp:TextBox ID="TBpenalizacion" runat="server" Width="55px"
                                                                Style="margin-left: 0px" CssClass="style41"></asp:TextBox>
                                                            <span class="style41">%</span>
                                                            <asp:Image ID="Image8" runat="server"
                                                                Title="El porcentaje que será descontado al resultado cuando se contesta después de la fecha de penalización."
                                                                ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                                                                CssClass="infoPopper" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="titleColumn">Estatus:
                                                        </td>
                                                        <td class="control_column">
                                                            <asp:DropDownList ID="DDLestatus" runat="server" Width="108px"
                                                                CssClass="style41">
                                                                <asp:ListItem>Sin iniciar</asp:ListItem>
                                                                <asp:ListItem>Iniciada</asp:ListItem>
                                                                <asp:ListItem>Terminada</asp:ListItem>
                                                                <asp:ListItem>Cancelada</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="titleColumn">La actividad la captura
                                                        </td>
                                                        <td class="control_column">
                                                            <asp:DropDownList ID="DDLcalifica" runat="server" Width="108px"
                                                                CssClass="style41">
                                                                <asp:ListItem></asp:ListItem>
                                                                <asp:ListItem Value="P">Profesor</asp:ListItem>
                                                                <asp:ListItem Value="C">Coordinador</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="pnlDatosEvaluacion_Dinamica" runat="server" Visible="false">
                        <table>
                            <tr>
                                <td class="titleColumn">
                                    Permite Segunda Vuelta
                                </td>
                                <td class="control_column">
                                    <asp:CheckBox ID="chkSegundavuelta" runat="server" Checked="true" />
                                    <asp:Image ID="Image1" runat="server"
                                        Title=
"Determina si al terminar la primera vuelta el alumno puede intentar corregir sus errores en una segunda vuelta."
                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                                        CssClass="infoPopper" />
                                </td>
                            </tr>
                            <tr>
                                <td class="titleColumn">
                                    Se entrega un archivo
                                </td>
                                <td class="control_column">
                                    <asp:CheckBox ID="CBseentregadocto" runat="server"
                                        AutoPostBack="true"
                                        CssClass="style41" />
                                    <asp:Image ID="Image2" runat="server"
                                        Title=
"Determina si la actividad será de entrega de archivos y presentará controles para ello. 
De activarse no se muestra el botón de inicio de evaluación, por lo que reactivos asignados a subtemas de esta evaluación no estarán disponibles."
                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                                        CssClass="infoPopper" />
                                </td>
                            </tr>
                            <tr>
                                <td class="titleColumn">
                                    Actividad de Foro
                                </td>
                                <td class="control_column">
                                    <asp:CheckBox ID="ChkEsForo" runat="server"
                                        AutoPostBack="true"
                                        CssClass="style41" />
                                    <asp:Image ID="Image12" runat="server"
                                        Title=
"Determina si la actividad será de tipo foro y presentará controles para ello. 
De activarse no se muestra el botón de inicio de evaluación, por lo que reactivos asignados a subtemas de esta evaluación no estarán disponibles."
                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                                        CssClass="infoPopper" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Panel ID="pnlReactivos" runat="server" Visible="true">
                                        <table style="margin-left: 50px; border: 1px solid #999;">
                                            <tr>
                                                <td class="titleColumn">Modo de despliegue de reactivos:
                                                </td>
                                                <td class="control_column">
                                                    <asp:DropDownList ID="ddlAleatoria" runat="server" Width="200px"
                                                        CssClass="style41">
                                                        <asp:ListItem Value="0">Secuencial</asp:ListItem>
                                                        <asp:ListItem Value="1">Aleatorio por Subtema</asp:ListItem>
                                                        <asp:ListItem Value="2" Enabled="false">Aleatorio del Total</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                               
                                            </tr>
                                            <tr>
                                                <td class="titleColumn">Modo de despliegue de Opciones:</td>
                                                 <td class="control_column">
                                                    <asp:DropDownList ID="ddlOpcionAleatoria" runat="server" Width="200px"
                                                        CssClass="style41">
                                                        <asp:ListItem Value="0">Secuencial</asp:ListItem>
                                                        <asp:ListItem Value="1">Aleatorio </asp:ListItem>
      
                                                    </asp:DropDownList>
                                                  </td>
                                              </tr>
                                            <tr>
                                                <td class="titleColumn">Repetición Automática
                                                </td>
                                                <td class="control_column">
                                                    <asp:CheckBox ID="chkRepeticion" runat="server"
                                                        AutoPostBack="true" />
                                                    <asp:Image ID="Image9" runat="server"
                                                        Title="Sistema que habilita nuevamente la actividad desde cero a un alumno que obtenga menos de un resultado definido por un indicador. El resultado anterior se almacena, las respuestas no."
                                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                                                        CssClass="infoPopper" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Panel ID="pnlRepeticion" runat="server" Visible="false">
                                                        <table>
                                                            <tr>
                                                                <td class="titleColumn">Indicador al que estará sujeta la repetición
                                                                </td>
                                                                <td class="control_column">
                                                                    <asp:DropDownList ID="ddlIndicadorRepeticion" runat="server"
                                                                        DataSourceID="SDSIndicadores" DataTextField="Descripcion"
                                                                        DataValueField="IdIndicador" Width="280px" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="titleColumn">Mínimo Resultado para NO repetir
                                                                </td>
                                                                <td class="control_column">
                                                                    <asp:DropDownList ID="ddlResultadoRepeticion" runat="server"
                                                                        DataSourceID="SDSResultados" DataTextField="Color"
                                                                        DataValueField="Selector" Width="280px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="titleColumn">Máximo Número de Repeticiones
                                                                </td>
                                                                <td class="control_column">
                                                                    <asp:TextBox ID="tbRepeticiones" runat="server"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="pnlDatosEvaluacion_Instruccion" runat="server" Visible="false">
                        <table>
                            <tr>
                                <td class="titleColumn">
                                    Instrucción:
                                    <asp:Image ID="Image10" runat="server"
                                        Title="Muestra el contenido definido a continuación al mostrar los materiales de apoyo para la actividad."
                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                                        CssClass="infoPopper" />
                                </td>
                                <td class="control_column">
                                    <div style="width: 700px; height: 320px;">
                                        <asp:TextBox ID="tbInstruccion" runat="server" 
                                            ClientIDMode="Static" 
                                            TextMode="MultiLine"
                                            CssClass="tinymce"></asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnDialogDatos" runat="server"
                        CssClass="choiceButton choiceButton_Left btnThemeBlue btnThemeSlick" Text="Datos" /><!--
                    --><asp:Button ID="btnDialogDinamica" runat="server"
                        CssClass="choiceButton choiceButton btnThemeGrey btnThemeSlick" Text="Dinámica" /><!--
                    --><asp:Button ID="btnDialogInstruccion" runat="server"
                        CssClass="choiceButton choiceButton_Right btnThemeGrey btnThemeSlick" Text="Instrucción" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom: 1px solid #333; min-height: 1em;">
                    <uc1:msgError runat="server" ID="msgErrorDialog" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:LinkButton ID="btnAceptarCrear" runat="server"
                        Visible="false" CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                        <asp:Image ID="imgAceptarCrear" runat="server"
                            ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/accept.png"
                            CssClass="btnIcon"
                            Height="24" />
                        <asp:Label ID="lblAceptarCrear" runat="server">
                        Crear
                        </asp:Label>
                    </asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="btnAceptarModificar" runat="server"
                        Visible="False" CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                        <asp:Image ID="imgAceptarModificar" runat="server"
                            ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/accept.png"
                            CssClass="btnIcon"
                            Height="24" />
                        <asp:Label ID="lblAceptarModificar" runat="server">
                            Guardar Cambios
                        </asp:Label>
                    </asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="btnCancelar" runat="server"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                        <asp:Image ID="imgCalCancelar" runat="server"
                            ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/cancel.png"
                            CssClass="btnIcon"
                            Height="24" />
                        <asp:Label ID="lblCancelar" runat="server">
                            Cancelar
                        </asp:Label>
                    </asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="btnCrearModificar" runat="server"
                        Visible="False" CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                        <asp:Image ID="Image11" runat="server"
                            ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/add.png"
                            CssClass="btnIcon"
                            Height="24" />
                        <asp:Label ID="Label5" runat="server">
                            Crear Nuevo
                        </asp:Label>
                    </asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar] WHERE ([Estatus] = 'Activo') ORDER BY [FechaInicio]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSIndicadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdIndicador], [Descripcion] FROM [Indicador] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT C.IdCalificacion, C.Consecutivo, C.Descripcion 
FROM Calificacion C, Asignatura A 
WHERE ([IdCicloEscolar] = @IdCicloEscolar)  and (A.IdAsignatura = C.IdAsignatura) and (A.IdAsignatura = @IdAsignatura)
ORDER BY C.Consecutivo, C.Descripcion ">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Evaluacion] 
where IdCalificacion = @IdCalificacion
ORDER BY [IdEvaluacion] DESC">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlCalificacion" Name="IdCalificacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdAsignatura], [IdGrado], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>
                <asp:SqlDataSource ID="SDSResultados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="
                    (SELECT 'Rojo - ' + CAST(I.MinRojo As VARCHAR(10)) + '%' AS 'Color', 0 AS 'Selector'
                    FROM Indicador I
                    WHERE I.IdIndicador = @IdIndicador)
                    UNION
                    (SELECT 'Amarillo - ' + CAST(I.MinAmarillo As VARCHAR(10)) + '%' AS 'Color', 1 AS 'Selector'
                    FROM Indicador I
                    WHERE I.IdIndicador = @IdIndicador)
                    UNION
                    (SELECT 'Verde - ' + CAST(I.MinVerde As VARCHAR(10)) + '%' AS 'Color', 2 AS 'Selector'
                    FROM Indicador I
                    WHERE I.IdIndicador = @IdIndicador)
                    UNION
                    (SELECT 'Azul - ' + CAST(I.MinAzul As VARCHAR(10)) + '%' AS 'Color', 3 AS 'Selector'
                    FROM Indicador I
                    WHERE I.IdIndicador = @IdIndicador) ORDER BY Selector">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlIndicadorRepeticion" Name="IdIndicador"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Button ID="HF1" runat="server" Text="HF1" />
                <asp:ModalPopupExtender ID="ModalPopupEvaluacion" runat="server"
                    PopupControlID="pnlDatosEvaluacion" TargetControlID="HF1" BackgroundCssClass="overlay">
                </asp:ModalPopupExtender>
            </td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>

