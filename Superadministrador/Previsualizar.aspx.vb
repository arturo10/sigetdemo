﻿Imports Siget

Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Partial Class superadministrador_Previsualizar
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        TitleLiteral.Text = "Previsualizar Reactivo"

        Dim IdPlant = Session("PlanteamientoSeleccionado")
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        Dim Redaccion As String
        'PLANTEAMIENTO
        miComando = objConexion.CreateCommand
        miComando.CommandText = "SELECT * FROM Planteamiento where idPlanteamiento = " + IdPlant
        Try
            'Abrir la conexión y leo los registros del Planteamiento
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read() 'Leo para poder accesarlos

            If misRegistros.Item("EsconderTexto") Then
                LblPlanteamiento.Visible = False

            Else
                If Not IsDBNull(misRegistros.Item("Redaccion")) Then

                    LblPlanteamiento.Visible = True
                    Redaccion = Replace(HttpUtility.HtmlDecode(misRegistros.Item("Redaccion")), "*salto*", "<BR>")
                    Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")

                    If misRegistros.Item("Consecutivo") > 0 Then
                        LblPlanteamiento.Text = CStr(misRegistros.Item("Consecutivo")) + ".- " + Redaccion
                    Else
                        'OJO: Aqui, en el despliegue para alumnos, tendria que usar un contador que enumere el planteamiento
                        LblPlanteamiento.Text = "#.- " + Redaccion
                    End If
                Else
                    If misRegistros.Item("Consecutivo") > 0 Then
                        LblPlanteamiento.Text = CStr(misRegistros.Item("Consecutivo")) + ".- "
                    Else
                        'OJO: Aqui, en el despliegue para alumnos, tendria que usar un contador que enumere el planteamiento
                        LblPlanteamiento.Text = "#.- "
                    End If

                End If
            End If


            'Saco la Referencia bibliográfica del reactivo (si la hay) que consta de 3 campos
            Dim Referencia As String
            Referencia = ""
            'If (Not IsDBNull(misRegistros.Item("Libro"))) Then
            'Referencia = HttpUtility.HtmlDecode(misRegistros.Item("Libro")) + ", "
            'End If
            'If (Not IsDBNull(misRegistros.Item("Autor"))) Then
            'Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Autor")) + ", "
            'End If
            'If (Not IsDBNull(misRegistros.Item("Editorial"))) Then
            'Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Editorial"))
            'End If
            '***

            If Trim(misRegistros.Item("Libro").ToString <> "") Then
                Referencia = HttpUtility.HtmlDecode(misRegistros.Item("Libro")) + ", "
            End If
            If Trim(misRegistros.Item("Autor").ToString <> "") Then
                Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Autor")) + ", "
            End If
            If Trim(misRegistros.Item("Editorial").ToString <> "") Then
                Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Editorial"))
            End If
            '***
            If Trim(Referencia) <> "" Then
                LblReferencia.Text = "(REFERENCIA: " + Referencia + ")"
            End If

            'Cargo el archivo de apoyo 1, siempre deberán estar en el directorio material
            If (Not IsDBNull(misRegistros.Item("TipoArchivoApoyo"))) And (Not IsDBNull(misRegistros.Item("ArchivoApoyo"))) Then
                If Trim(misRegistros.Item("ArchivoApoyo")).Length > 0 Then 'Esta comparacion la hago aparte del IF superior porque marca error al querer sacar tamaño a valor NULL
                    If Trim(misRegistros.Item("TipoArchivoApoyo")) = "Imagen" Then
                        'ImPlanteamiento.Width = 300
                        'ImPlanteamiento.Height = 300
                        ImPlanteamiento.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                        ImPlanteamiento.Visible = True
                        HLapoyoplanteamiento.Visible = False
                    ElseIf Trim(misRegistros.Item("TipoArchivoApoyo")) = "Video" Or Trim(misRegistros.Item("TipoArchivoApoyo")) = "Audio" Then
                        LblVideo.Text = "<embed src=""" & Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo") + """ width=""280"" height=""227"" autostart=""true"" controller=""true"" align=""center""></embed>"
                        ImPlanteamiento.Visible = False
                        HLapoyoplanteamiento.Visible = False
                    Else
                        'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                        '********PENDIENTE***************
                        HLapoyoplanteamiento.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                        'HLapoyoplanteamiento.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                        HLapoyoplanteamiento.Text = "APOYO: " + Trim(misRegistros.Item("ArchivoApoyo").ToString)
                        HLapoyoplanteamiento.Visible = True
                        ImPlanteamiento.Visible = False
                    End If
                Else
                    ImPlanteamiento.Visible = False
                    HLapoyoplanteamiento.Visible = False
                End If
            End If

            'Cargo el archivo 2 de apoyo si lo hay, siempre deberán estar en el directorio material
            If (Not IsDBNull(misRegistros.Item("TipoArchivoApoyo2"))) And (Not IsDBNull(misRegistros.Item("ArchivoApoyo2"))) Then
                If Trim(misRegistros.Item("ArchivoApoyo2")).Length > 0 Then 'Esta comparacion la hago aparte del IF superior porque marca error al querer sacar tamaño a valor NULL
                    If Trim(misRegistros.Item("TipoArchivoApoyo2")) = "Imagen" Then
                        'ImPlanteamiento2.Width = 300
                        'ImPlanteamiento2.Height = 300
                        ImPlanteamiento2.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo2")
                        ImPlanteamiento2.Visible = True
                        HLapoyoplanteamiento2.Visible = False
                    ElseIf Trim(misRegistros.Item("TipoArchivoApoyo2")) = "Video" Or Trim(misRegistros.Item("TipoArchivoApoyo2")) = "Audio" Then
                        LblVideo2.Text = "<embed src=""" & Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo2") + """ width=""280"" height=""227"" autostart=""true"" controller=""true"" align=""center""></embed>"
                        ImPlanteamiento2.Visible = False
                        HLapoyoplanteamiento2.Visible = False
                    Else
                        'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                        '********PENDIENTE***************
                        HLapoyoplanteamiento2.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo2")
                        'HLapoyoplanteamiento2.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                        HLapoyoplanteamiento2.Text = "APOYO: " + Trim(misRegistros.Item("ArchivoApoyo2").ToString)
                        HLapoyoplanteamiento2.Visible = True
                        ImPlanteamiento2.Visible = False
                    End If
                Else
                    ImPlanteamiento2.Visible = False
                    HLapoyoplanteamiento2.Visible = False
                End If
            End If

            'OPCIONES
            If Trim(misRegistros.Item("TipoRespuesta")) = "Multiple" Then
                PnlOpciones.Visible = True
                misRegistros.Close() 'Cierro la tabla de Planteamiento
                'Algoritmo respuesta multiple
                miComando.CommandText = "SELECT * FROM Opcion where Esconder = 'False' and idPlanteamiento = " + IdPlant + " order by Consecutiva"
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read() 'Leo para poder accesarlos

                'DEBO USAR: (Primero colocar un PlaceHolder y llamarlo "PanelOpciones"
                'Dim TextoOpc1 As New Label
                'TextoOpc1.Text = misRegistros.Item("Redaccion")
                'PanelOpciones.Controls.Add(TextoOpc1) 'ó Controls.AddAt(1, TextoOpc1)
                'Dim TblOpciones As New Table

                Redaccion = Replace(HttpUtility.HtmlDecode(misRegistros.Item("Redaccion")), "*salto*", "<BR>")
                Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                'TextoOpc1.Text = "<font color='blue'>" + CStr(misRegistros.Item("Consecutiva")) + ") </font> " + Redaccion
                TextoOpc1.Text = Redaccion
                Opcion1.Text = CStr(misRegistros.Item("Consecutiva"))
                If Not IsDBNull(misRegistros.Item("TipoArchivoApoyo")) Then
                    If misRegistros.Item("TipoArchivoApoyo") = "Imagen" Then
                        'ImOpc1.Width = 300
                        'ImOpc1.Height = 300
                        ImOpc1.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                        HLapoyoopc1.Visible = False
                    Else
                        'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                        HLapoyoopc1.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                        HLapoyoopc1.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                        ImOpc1.Visible = False
                    End If
                Else
                    ImOpc1.Visible = False
                    HLapoyoopc1.Visible = False
                End If

                If misRegistros.Read() Then  'PASO A OPCION 2
                    Redaccion = Replace(HttpUtility.HtmlDecode(misRegistros.Item("Redaccion")), "*salto*", "<BR>")
                    Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                    'TextoOpc2.Text = "<font color='blue'>" + CStr(misRegistros.Item("Consecutiva")) + ") </font> " + Redaccion
                    TextoOpc2.Text = Redaccion
                    Opcion2.Text = CStr(misRegistros.Item("Consecutiva"))
                    If Not IsDBNull(misRegistros.Item("TipoArchivoApoyo")) Then
                        If misRegistros.Item("TipoArchivoApoyo") = "Imagen" Then
                            'ImOpc2.Width = 300
                            'ImOpc2.Height = 300
                            ImOpc2.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                            HLapoyoopc2.Visible = False
                        Else
                            'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                            HLapoyoopc2.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                            HLapoyoopc2.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                            ImOpc2.Visible = False
                        End If
                    Else
                        ImOpc2.Visible = False
                        HLapoyoopc2.Visible = False
                    End If
                Else
                    ImOpc2.Visible = False
                    HLapoyoopc2.Visible = False
                    Opcion2.Visible = False
                End If

                If misRegistros.Read() Then 'PASO A OPCION 3
                    Redaccion = Replace(HttpUtility.HtmlDecode(misRegistros.Item("Redaccion")), "*salto*", "<BR>")
                    Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                    'TextoOpc3.Text = "<font color='blue'>" + CStr(misRegistros.Item("Consecutiva")) + ") </font> " + Redaccion
                    TextoOpc3.Text = Redaccion
                    Opcion3.Text = CStr(misRegistros.Item("Consecutiva"))
                    If Not IsDBNull(misRegistros.Item("TipoArchivoApoyo")) Then
                        If misRegistros.Item("TipoArchivoApoyo") = "Imagen" Then
                            'ImOpc3.Width = 300
                            'ImOpc3.Height = 300
                            ImOpc3.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                            HLapoyoopc3.Visible = False
                        Else
                            'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                            HLapoyoopc3.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                            HLapoyoopc3.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                            ImOpc3.Visible = False
                        End If
                    Else
                        ImOpc3.Visible = False
                        HLapoyoopc3.Visible = False
                    End If
                Else
                    ImOpc3.Visible = False
                    HLapoyoopc3.Visible = False
                    Opcion3.Visible = False
                End If

                If misRegistros.Read() Then 'PASO A OPCION 4
                    Redaccion = Replace(HttpUtility.HtmlDecode(misRegistros.Item("Redaccion")), "*salto*", "<BR>")
                    Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                    'TextoOpc4.Text = "<font color='blue'>" + CStr(misRegistros.Item("Consecutiva")) + ") </font> " + Redaccion
                    TextoOpc4.Text = Redaccion
                    Opcion4.Text = CStr(misRegistros.Item("Consecutiva"))
                    If Not IsDBNull(misRegistros.Item("TipoArchivoApoyo")) Then
                        If misRegistros.Item("TipoArchivoApoyo") = "Imagen" Then
                            'ImOpc4.Width = 300
                            'ImOpc4.Height = 300
                            ImOpc4.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                            HLapoyoopc4.Visible = False
                        Else
                            'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                            HLapoyoopc4.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                            HLapoyoopc4.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                            ImOpc4.Visible = False
                        End If
                    Else
                        ImOpc4.Visible = False
                        HLapoyoopc4.Visible = False
                    End If
                Else
                    ImOpc4.Visible = False
                    HLapoyoopc4.Visible = False
                    Opcion4.Visible = False
                End If

                If misRegistros.Read() Then 'PASO A OPCION 5
                    Redaccion = Replace(HttpUtility.HtmlDecode(misRegistros.Item("Redaccion")), "*salto*", "<BR>")
                    Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                    'TextoOpc5.Text = "<font color='blue'>" + CStr(misRegistros.Item("Consecutiva")) + ") </font> " + Redaccion
                    TextoOpc5.Text = Redaccion
                    Opcion5.Text = CStr(misRegistros.Item("Consecutiva"))
                    If Not IsDBNull(misRegistros.Item("TipoArchivoApoyo")) Then
                        If misRegistros.Item("TipoArchivoApoyo") = "Imagen" Then
                            'ImOpc5.Width = 300
                            'ImOpc5.Height = 300
                            ImOpc5.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                            HLapoyoopc5.Visible = False
                        Else
                            'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                            HLapoyoopc5.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                            HLapoyoopc5.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                            ImOpc5.Visible = False
                        End If
                    Else
                        ImOpc5.Visible = False
                        HLapoyoopc5.Visible = False
                    End If
                Else
                    ImOpc5.Visible = False
                    HLapoyoopc5.Visible = False
                    Opcion5.Visible = False
                End If

                If misRegistros.Read() Then 'PASO A OPCION 6
                    Redaccion = Replace(HttpUtility.HtmlDecode(misRegistros.Item("Redaccion")), "*salto*", "<BR>")
                    Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                    'TextoOpc6.Text = "<font color='blue'>" + CStr(misRegistros.Item("Consecutiva")) + ") </font> " + Redaccion
                    TextoOpc6.Text = Redaccion
                    Opcion6.Text = CStr(misRegistros.Item("Consecutiva"))
                    If Not IsDBNull(misRegistros.Item("TipoArchivoApoyo")) Then
                        If misRegistros.Item("TipoArchivoApoyo") = "Imagen" Then
                            'ImOpc6.Width = 300
                            'ImOpc6.Height = 300
                            ImOpc6.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                            HLapoyoopc6.Visible = False
                        Else
                            'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                            HLapoyoopc6.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                            HLapoyoopc6.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
                            ImOpc6.Visible = False
                        End If
                    Else
                        ImOpc6.Visible = False
                        HLapoyoopc6.Visible = False
                    End If
                Else
                    ImOpc6.Visible = False
                    HLapoyoopc6.Visible = False
                    Opcion6.Visible = False
                End If
            ElseIf Trim(misRegistros.Item("TipoRespuesta")) = "Abierta Calificada" Then
                PanelAbiertaAutomatica.Visible = True
                tbRespuestaAbiertaAutomatica.Width = misRegistros.Item("Ancho1").ToString()

                ' guardo las respuestas correctas de las opciones en el hfVerifica para comparar sin postback
                If misRegistros.Item("Verifica") Then
                    VerificarAA.Visible = True
                Else
                    VerificarAA.Visible = False
                End If
            Else
                'Aquí iría el Algoritmo cuando el tipo de respuesta es de "Completar"
                '********PENDIENTE***************
                PnlOpciones.Visible = False


            End If

            misRegistros.Close()
            objConexion.Close()

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub
End Class
