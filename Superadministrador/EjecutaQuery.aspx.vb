﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO

Partial Class superadministrador_EjecutaQuery
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GVresultados.Caption = "Resultados de la consulta"

        If Not IsPostBack Then
            TBoperacion.Focus()
        End If
    End Sub

    Protected Sub BtnExecOperacion_Click(sender As Object, e As EventArgs) Handles BtnExecOperacion.Click
        GVresultados.Visible = True
        msgError.hide()
        msgInfo.hide()
        ' verificar password

        If RBcommandType.SelectedValue = 1 Then ' CONSULTA
            Try
                SDSconsulta.SelectCommand = TBoperacion.Text.Trim()
                SDSconsulta.DataBind()
                GVresultados.DataBind()

                msgInfo.show("Filas: " & GVresultados.Rows.Count(), "")
                If GVresultados.Rows.Count() > 0 Then
                    BtnExportar.Visible = True
                End If
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try
        ElseIf RBcommandType.SelectedValue = 2 Then ' OPERACION
            Dim strConexion As String
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objConexion As New SqlConnection(strConexion)
            Dim miComando As SqlCommand
            Dim afectados As Integer
            miComando = objConexion.CreateCommand
            miComando.CommandText = TBoperacion.Text.Trim()
            Try
                objConexion.Open()
                afectados = miComando.ExecuteNonQuery() ' ejecuto operación
                msgInfo.show("Comando ejecutado correctamente. Filas afectadas: " & afectados) ' imprimo mensaje

                objConexion.Close()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                GVresultados.Visible = False
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try
        End If
    End Sub

    Protected Sub RBcommandType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RBcommandType.SelectedIndexChanged
        If RBcommandType.SelectedValue = 1 Then
            BtnExecOperacion.Text = "Ejecutar Consulta"
        Else
            BtnExecOperacion.Text = "Ejecutar Operación"
        End If
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        'Este procedimiento convierte el GridView a Excel eliminando la primera y segunda columna
        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel")
        End If
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVresultados, "Resultado")
    End Sub

    Protected Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        TBoperacion.Text = ""
        msgInfo.hide()
        msgError.hide()
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    ' Métodos de GridViews
    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVresultados_PreRender(sender As Object, e As EventArgs) Handles GVresultados.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVresultados.Rows.Count > 0 Then
            GVresultados.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

End Class
