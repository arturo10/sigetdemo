﻿Imports Siget


Partial Class superadministrador_Equipos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GVequipos.Caption = "<h3>" &
            Config.Etiqueta.EQUIPOS &
            " capturad" & Config.Etiqueta.LETRA_EQUIPO & "s</h3>"

        GVsubequipos.Caption = "<h3>" &
            Config.Etiqueta.SUBEQUIPOS &
            " capturad" & Config.Etiqueta.LETRA_SUBEQUIPO & "s</h3>"

        Label1.Text = Config.Etiqueta.EQUIPOS
        Label2.Text = Config.Etiqueta.SUBEQUIPOS
        Label3.Text = Config.Etiqueta.ALUMNOS
        Label4.Text = Config.Etiqueta.INSTITUCION
        Label5.Text = Config.Etiqueta.EQUIPOS
        Label6.Text = Config.Etiqueta.EQUIPOS
        Label7.Text = Config.Etiqueta.ALUMNOS
        Label8.Text = Config.Etiqueta.EQUIPO
        Label9.Text = Config.Etiqueta.SUBEQUIPOS
        Label10.Text = Config.Etiqueta.ALUMNOS
        Label11.Text = Config.Etiqueta.SUBEQUIPO
        Label12.Text = Config.Etiqueta.SUBEQUIPOS
        Label13.Text = Config.Etiqueta.EQUIPOS
        Label14.Text = Config.Etiqueta.SUBEQUIPOS
    End Sub

    Protected Sub BtnAgregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAgregar.Click
        Try
            If Trim(TBequipo.Text) <> "" Then
                SDSequipos.InsertCommand = "SET dateformat dmy; INSERT INTO Equipo (IdInstitucion,Descripcion, FechaModif, Modifico) VALUES (" + DDLinstitucion.SelectedValue + ",'" + Trim(TBequipo.Text) + "', getdate(), '" & User.Identity.Name & "')"
                SDSequipos.Insert()
                TBequipo.Text = ""
                msgError.hide()
            Else
                msgError.show("No puede ingresar " &
                    Config.Etiqueta.ARTIND_SUBEQUIPO & " " & Config.Etiqueta.EQUIPO &
                    " sin capturar algún nombre de máximo 20 caracteres.")
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnActualizar.Click
        Try
            SDSequipos.UpdateCommand = "SET dateformat dmy; UPDATE Alumno SET Equipo = '" + Trim(TBequipo.Text) + "' WHERE Equipo = '" & GVequipos.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(GVequipos, "Descripción")).Text.Trim() & "'; UPDATE Equipo SET Descripcion = '" + Trim(TBequipo.Text) + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdEquipo = " + GVequipos.SelectedValue.ToString.Trim() 'Me da el IdEquipo porque es el campo clave de la fila seleccionada
            SDSequipos.Update()
            GVequipos.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnQuitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnQuitar.Click
        Try
            SDSequipos.DeleteCommand = "SET dateformat dmy; UPDATE Alumno SET Equipo = '' WHERE Equipo = '" & GVequipos.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(GVequipos, "Descripción")).Text.Trim() & "'; Delete from Equipo where IdEquipo = " + GVequipos.SelectedValue.ToString 'Me da el IdEquipo porque es el campo clave de la fila seleccionada
            SDSequipos.Delete()
            GVequipos.DataBind()
            TBequipo.Text = ""
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVequipos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVequipos.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.EQUIPO

            LnkHeaderText = e.Row.Cells(2).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.INSTITUCION
        End If
    End Sub

    Protected Sub GVsubequipos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVsubequipos.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.SUBEQUIPO

            LnkHeaderText = e.Row.Cells(2).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.INSTITUCION
        End If
    End Sub

    Protected Sub GVequipos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVequipos.SelectedIndexChanged
        'Como no pueden grabarse equipos con la descripcion en blanco, no es necesario que verifique si hay un &nbsp; en el GridView
        TBequipo.Text = GVequipos.SelectedRow.Cells(3).Text
    End Sub

    Protected Sub BtnAgregarS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAgregarS.Click
        Try
            If Trim(TBsubequipo.Text) <> "" Then
                SDSsubequipos.InsertCommand = "SET dateformat dmy; INSERT INTO SubEquipo (IdInstitucion,Descripcion, FechaModif, Modifico) VALUES (" + DDLinstitucion.SelectedValue + ",'" + Trim(TBsubequipo.Text) + "', getdate(), '" & User.Identity.Name & "')"
                SDSsubequipos.Insert()
                TBsubequipo.Text = ""
                msgError.hide()
            Else
                msgError.show("No puede ingresar " &
                    Config.Etiqueta.ARTIND_SUBEQUIPO & " " & Config.Etiqueta.SUBEQUIPO &
                    " sin capturar algún nombre de máximo 20 caracteres.")
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnActualizarS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnActualizarS.Click
        Try
            SDSsubequipos.UpdateCommand = "SET dateformat dmy; UPDATE Alumno SET SubEquipo = '" + Trim(TBsubequipo.Text) + "' WHERE SubEquipo = '" & GVsubequipos.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(GVsubequipos, "Descripción")).Text.Trim() & "'; UPDATE SubEquipo SET Descripcion = '" + Trim(TBsubequipo.Text) + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdSubEquipo = " + GVsubequipos.SelectedValue.ToString 'Me da el IdEquipo porque es el campo clave de la fila seleccionada
            SDSsubequipos.Update()
            GVsubequipos.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnQuitarS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnQuitarS.Click
        Try
            SDSsubequipos.DeleteCommand = "SET dateformat dmy; UPDATE Alumno SET SubEquipo = '' WHERE SubEquipo = '" & GVsubequipos.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(GVsubequipos, "Descripción")).Text.Trim() & "'; Delete from SubEquipo where IdSubEquipo = " + GVsubequipos.SelectedValue.ToString 'Me da el IdEquipo porque es el campo clave de la fila seleccionada
            SDSsubequipos.Delete()
            GVsubequipos.DataBind()
            TBsubequipo.Text = ""
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVsubequipos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVsubequipos.SelectedIndexChanged
        'Como no pueden grabarse subequipos con la descripcion en blanco, no es necesario que verifique si hay un &nbsp; en el GridView
        TBsubequipo.Text = GVsubequipos.SelectedRow.Cells(3).Text
    End Sub

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem("---Elija una " & Config.Etiqueta.INSTITUCION, 0))
    End Sub

    Protected Sub GVsubequipos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVsubequipos.DataBound
        If GVsubequipos.Rows.Count > 0 Then
            BtnActualizarS.Visible = True
            BtnQuitarS.Visible = True
        Else
            BtnActualizarS.Visible = False
            BtnQuitarS.Visible = False
        End If
    End Sub

    Protected Sub GVequipos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVequipos.DataBound
        If GVequipos.Rows.Count > 0 Then
            BtnActualizar.Visible = True
            BtnQuitar.Visible = True
        Else
            BtnActualizar.Visible = False
            BtnQuitar.Visible = False
        End If
    End Sub
End Class
