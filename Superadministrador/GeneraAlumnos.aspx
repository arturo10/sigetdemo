﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="GeneraAlumnos.aspx.vb" 
    Inherits="superadministrador_GeneraAlumnos" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <style type="text/css">

       
              
        .style13
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 51px;
        }
               
              
        .style14
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
        }
       
              
        .style19
        {
            font-size: xx-small;
        }
       
              
        .style15
        {
            width: 100%;
            height: 227px;
        }
               
              
        .style24
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
       
              
        .style25
    {
        font-size: small;
        font-weight: normal;
    }
        .style26
        {
            height: 31px;
        }
       
              
        .style11 {
            width: 100%;
        }

        .style16 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
            height: 52px;
        }
        .auto-style9 {
            height: 16px;
            text-align: center;
        }
        .auto-style4 {
            width: 49px;
            height: 28px;
        }
        .auto-style12 {
            height: 28px;
        }
        .auto-style10 {
            height: 17px;
        }
        .auto-style11 {
            width: 49px;
            height: 17px;
        }
        .auto-style5 {
            height: 16px;
            width: 49px;
            text-align: center;
        }
        .auto-style3 {
            height: 16px;
            font-weight: bold;
            text-align: center;
        }
        .auto-style8 {
            width: 49px;
            text-align: center;
        }
            .auto-style13 {
                height: 17px;
                text-align: center;
            }
       
              
        </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
        <h1>
			Importar <asp:Label ID="Label8" runat="server" Text="[ALUMNOS]" /> &nbsp;y <asp:Label ID="Label9" runat="server" Text="[EQUIPOS]" /> &nbsp;o <asp:Label ID="Label10" runat="server" Text="[SUBEQUIPOS]" />
		</h1>
		Permite importar de archivos de texto los datos de 
                    <asp:Label ID="Label1" runat="server" Text="[EQUIPOS]" />
												, 
												<asp:Label ID="Label2" runat="server" Text="[SUBEQUIPOS]" />
												 y 
												<asp:Label ID="Label3" runat="server" Text="[PLANTELES]" />
												 que se registrarán en el sistema.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
        <table class="style10">
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSusuarios" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [Usuario]"></asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSalumnos" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [Alumno]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="2" 
                    Width="867px" Height="398px" 
                    style="font-family: Arial, Helvetica, sans-serif; font-size: small">
                    <asp:TabPanel ID="TabPanel1" runat="server" HeaderText="TabPanel1">
                        <HeaderTemplate>
                            <asp:Label ID="Label4" runat="server" Text="[EQUIPOS]" />
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table class="style15">
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
                        <HeaderTemplate>
                            <asp:Label ID="Label5" runat="server" Text="[SUBEQUIPOS]" />
                        </HeaderTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel ID="TabPanel3" runat="server" HeaderText="TabPanel3">
                        <HeaderTemplate>
                            <asp:Label ID="Label6" runat="server" Text="[ALUMNOS]" />
                        </HeaderTemplate>
                        <ContentTemplate>
                            <table class="style15">
                                <tr>
                                    <td class="style24" colspan="3">
                                        <strong>El archivo a importar debe ser de texto o csv con los campos separados por el caracter | y el contenido no debe tener apóstrofes (&#39;).<br />El archivo debe incluir todas las columnas aunque alguna(s) no tenga(n) valores (dejarlas en blanco).</strong></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="style11">
                                            <tr>
                                                <td class="style16">
                                                    <table border="0" class="style11">
                                                        <tr>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FF5050">campo con valor obligatorio</td>
                                                            <td class="auto-style4"></td>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FFFF66">campo con valor opcional</td>
                                                            <td class="auto-style12"></td>
                                                            <td class="auto-style12"></td>
                                                            <td class="auto-style12"></td>
                                                            <td class="auto-style12"></td>
                                                            <td class="auto-style12"></td>
                                                            <td class="auto-style12"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style10"></td>
                                                            <td class="auto-style11"></td>
                                                            <td class="auto-style10"></td>
                                                            <td class="auto-style10"></td>
                                                            <td class="auto-style10"></td>
                                                            <td class="auto-style10"></td>
                                                            <td class="auto-style10"></td>
                                                            <td class="auto-style10"></td>
                                                            <td class="auto-style13">Mín [4]</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FF5050;"><b style="background-color: #FF5050">IdGrupo</b></td>
                                                            <td class="auto-style5" style="border: thin solid #000000; background-color: #FF5050;"><b style="background-color: #FF5050">Nombre</b></td>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FF5050;"><b style="background-color: #FF5050">ApePaterno</b></td>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FF5050;"><b style="background-color: #FF5050">ApeMaterno</b></td>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FFFF66;"><b style="background-color: #FFFF66">Matrícula</b></td>
                                                            <td class="auto-style3" style="border: thin solid #000000; background-color: #FFFF66;">Equipo</td>
                                                            <td class="auto-style3" style="border: thin solid #000000; background-color: #FFFF66;">Subequipo</td>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FF5050;"><b style="background-color: #FF5050">Login</b></td>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FF5050;"><b style="background-color: #FF5050">Password</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style8" style="border: thin solid #000000">[int]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">[40]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">[30]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">[30]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">[20]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">[100]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">[20]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">[100]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">&nbsp;[20]</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <span class="style24">Cargue el archivo que contiene los registros para importar 
                                        a la base de datos:</span></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:FileUpload ID="ArchivoCarga" runat="server" Height="22px" Width="265px" />
                                        </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <span class="style14">Se generarán los registros en la tablas de Usuario y 
                                        Alumno. Si no se elige Registrar Usuarios, posteriormente el 
																						<asp:Label ID="Label7" runat="server" Text="[ALUMNO]" /> &nbsp;deberá generar sus cuentas de acceso predefinidas con los datos que se le proporcionarán (Login y Matrícula) para que proceda a su registro y reciba un correo con su password</span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="CBregistrarme" runat="server" 
                                            style="font-weight: 700; font-size: small; font-family: Arial, Helvetica, sans-serif;" 
                                            Text="Registrar Usuarios" Checked="True" />
                                    </td>
                                    <td class="style24" style="text-align: right">
                                        <b>Correo electrónico para registrar usuarios:</b></td>
                                    <td>
                                        <asp:TextBox ID="TBemail" runat="server" Width="260px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="font-size: medium;">
                                        <asp:Button ID="Importar" runat="server" 
																						CssClass="defaultBtn btnThemeBlue btnThemeMedium"
																						Text="Importar" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:TabPanel>
                </asp:TabContainer>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                                        <asp:Label runat="server" ID="LblSalida" style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>

            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="LblMensaje" runat="server" 
                    CssClass="LabelInfoDefault"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:HyperLink ID="HyperLink2" runat="server"
					NavigateUrl="~/" 
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

