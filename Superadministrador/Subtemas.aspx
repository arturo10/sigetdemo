﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="Subtemas.aspx.vb"
    Inherits="superadministrador_Subtemas"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgWarning.ascx" TagPrefix="uc1" TagName="msgWarning" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .dialog1 {
            width: 700px;
        }

        .style12 {
            width: 327px;
            text-align: right;
        }

        .style10 {
            width: 100%;
            height: 499px;
        }

        .style18 {
            height: 14px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style13 {
            height: 154px;
        }

        .style11 {
            width: 96%;
        }

        .style17 {
            font-size: x-small;
        }

        .style16 {
            height: 119px;
        }

        .style15 {
            width: 69%;
        }

        .style19 {
            height: 28px;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
        }

        .style24 {
            width: 327px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style27 {
            height: 3px;
        }

        .style29 {
            width: 90%;
            margin-left: 0px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            width: 136px;
        }

        .style33 {
        }

        .style34 {
        }

        .style42 {
            font-family: Arial;
            font-size: small;
            text-align: right;
        }

        .style43 {
            width: 170px;
        }

        .style44 {
            width: 100%;
        }

        .style45 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style46 {
            height: 38px;
        }

        .style47 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style48 {
            width: 147px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Administración de Subtemas
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style13">
                <table class="style11">
                    <tr>
                        <td colspan="6" class="style27"></td>
                    </tr>
                    <tr>
                        <td colspan="6" class="style19">
                            <asp:Label ID="Mensaje1" runat="server" CssClass="titulo"
                                Style="color: #000066; font-family: Arial, Helvetica, sans-serif;">Capture los siguientes datos:</asp:Label>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td style="text-align: right" class="style32">&nbsp;<asp:Label ID="Label1" runat="server" Text="[NIVEL]" />:</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                                Width="180px">
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td style="text-align: right" class="style32">
                            <asp:Label ID="Label2" runat="server" Text="[GRADO]" />:</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                DataSourceID="SDSgrados" DataTextField="Descripcion"
                                DataValueField="IdGrado" Width="180px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td style="text-align: right" class="style32">
                            <asp:Label ID="Label3" runat="server" Text="[ASIGNATURA]" />:
                        </td>
                        <td style="text-align: left" colspan="4">
                            <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                                DataSourceID="SDSasignaturas" DataTextField="Descripcion"
                                DataValueField="IdAsignatura" Width="383px" Height="22px"
                                Style="margin-left: 0px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td style="text-align: right" class="style32">Temas:</td>
                        <td colspan="5" style="text-align: left">
                            <asp:DropDownList ID="DDLtemas" runat="server" AutoPostBack="True"
                                DataSourceID="SDStemas" DataTextField="NomTema" DataValueField="IdTema"
                                Width="659px" Height="22px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="2">&nbsp;</td>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="2">Número del Subtema</td>
                        <td colspan="4" style="text-align: left">
                            <asp:TextBox ID="TBnumero" runat="server" Width="79px"></asp:TextBox>
                            &nbsp;<span class="style17">(Ej. 2.1, 2.2, etc.)</span></td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="2">Descripción del Subtema</td>
                        <td colspan="4" style="text-align: left">
                            <asp:TextBox ID="TBdescripcion" runat="server" Width="474px" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="2">Competencia que impacta</td>
                        <td colspan="4" style="text-align: left">
                            <asp:DropDownList ID="DDLcompetencias" runat="server" AutoPostBack="True"
                                DataSourceID="SDScompetencias" DataTextField="Descripcion"
                                DataValueField="IdCompetencia" Height="22px" Width="217px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="2">&nbsp;</td>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style12" colspan="2">&nbsp;</td>
                        <td colspan="4">
                            <asp:Button ID="Insertar" runat="server" Text="Insertar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Limpiar" runat="server" Text="Limpiar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Actualizar" runat="server" Text="Actualizar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Eliminar" runat="server" Text="Eliminar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style16">
                <table class="style15">
                    <tr>
                        <td colspan="6">
                            <asp:GridView ID="GVsubtemas" runat="server" AllowPaging="True"
                                AllowSorting="True" AutoGenerateColumns="False" AutoGenerateSelectButton="True"
                                CellPadding="3" DataKeyNames="IdSubtema" DataSourceID="SDSdatosubt"
                                ForeColor="Black" GridLines="Vertical" Style="font-size: small; font-family: Arial, Helvetica, sans-serif;"
                                Width="874px" BackColor="White" BorderColor="#999999"
                                BorderStyle="Solid" BorderWidth="1px"
                                Caption="<h3>Subtemas capturados</h3>">
                                <Columns>
                                    <asp:BoundField DataField="IdCompetencia" HeaderText="Id_Comp."
                                        SortExpression="IdCompetencia" />
                                    <asp:BoundField DataField="Competencia" HeaderText="Competencia"
                                        SortExpression="Competencia" />
                                    <asp:BoundField DataField="IdSubtema" HeaderText="Id_Subtema"
                                        InsertVisible="False" ReadOnly="True" SortExpression="IdSubtema" />
                                    <asp:BoundField DataField="Numero" HeaderText="Número"
                                        SortExpression="Numero" />
                                    <asp:BoundField DataField="Subtema" HeaderText="Subtema"
                                        SortExpression="Subtema" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <uc1:msgSuccess runat="server" ID="msgSuccess" Visible="true"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <uc1:msgError runat="server" ID="msgError" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <td colspan="2">&nbsp;</td>
                        <td colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:Label ID="Mensaje0" runat="server" CssClass="titulo"
                                Style="color: #000066; font-family: Arial, Helvetica, sans-serif;">Cargue material de apoyo para el subtema</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="style34">
                            <table class="style29">
                                <tr>
                                    <td class="style42" colspan="2">Consecutivo</td>
                                    <td style="text-align: left">
                                        <asp:TextBox ID="TBconsecutivo" runat="server" Style="margin-left: 0px"
                                            Width="56px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style42" colspan="2">
                                        Descripción
                                    </td>
                                    <td style="text-align: left;">
                                        <asp:TextBox ID="tbApoyodescripcion" runat="server"
                                            Width="350px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style42" colspan="2">Tipo de archivo</td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLtipoarchivo" runat="server" Width="90px" AutoPostBack="True">
                                            <asp:ListItem>Imagen</asp:ListItem>
                                            <asp:ListItem>PDF</asp:ListItem>
                                            <asp:ListItem>ZIP</asp:ListItem>
                                            <asp:ListItem>Word</asp:ListItem>
                                            <asp:ListItem>FLV</asp:ListItem>
                                            <asp:ListItem>SWF</asp:ListItem>
                                            <asp:ListItem>Excel</asp:ListItem>
                                            <asp:ListItem Value="PP">Power Point</asp:ListItem>
                                            <asp:ListItem>Video</asp:ListItem>
                                            <asp:ListItem>Audio</asp:ListItem>
                                            <asp:ListItem>Link</asp:ListItem>
                                            <asp:ListItem>YouTube</asp:ListItem>
                                            <asp:ListItem>Otro</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:HyperLink ID="HLarchivo" runat="server"
                                            Style="font-family: Arial, Helvetica, sans-serif; font-size: small; color: #000066"
                                            Target="_blank" Visible="False">[HLarchivo]</asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style42" colspan="2">Archivo de apoyo</td>
                                    <td style="text-align: left; width: 100%;">
                                        <asp:AsyncFileUpload ID="fuArchivo" runat="server" CssClass="imageUploaderField"/>
                                        <asp:TextBox ID="TBlink" runat="server" MaxLength="150" Visible="False"
                                            Width="231px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="style43">&nbsp;</td>
                                    <td style="text-align: left">
                                        <asp:Button ID="cargar" runat="server" Text="Cargar"
                                            CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                        &nbsp;
                                        <asp:Button ID="btn_actualizaMaterial" runat="server" Text="Actualizar"
                                            CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="font-size: x-small;">
                                        NOTA: Actualizar sólo modifica consecutivo, descripción y tipo.
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <uc1:msgInfo runat="server" ID="msgInfo" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:GridView ID="GVmaterialapoyo" runat="server" AutoGenerateColumns="False"
                                BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px"
                                Caption="<h3>Material de Apoyo para el Subtema Elegido</h3>"
                                CellPadding="3" DataKeyNames="IdApoyoSubtema" DataSourceID="SDSmaterialapoyo"
                                ForeColor="Black" GridLines="Vertical"
                                Style="font-family: Arial, Helvetica, sans-serif; font-size: small"
                                Width="869px">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True">
                                        <HeaderStyle Width="90px" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="IdApoyoSubtema" HeaderText="IdApoyoSubtema"
                                        InsertVisible="False" ReadOnly="True" SortExpression="IdApoyoSubtema"
                                        Visible="False" />
                                    <asp:BoundField DataField="IdSubtema" HeaderText="IdSubtema"
                                        SortExpression="IdSubtema" Visible="False" />
                                    <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                                        SortExpression="Consecutivo" />
                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripcion"
                                        SortExpression="Descripcion" />
                                    <asp:BoundField DataField="ArchivoApoyo" HeaderText="Archivo de Apoyo"
                                        SortExpression="ArchivoApoyo" />
                                    <asp:BoundField DataField="TipoArchivoApoyo" HeaderText="Tipo de Archivo"
                                        SortExpression="TipoArchivoApoyo" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align: center">
                            <asp:Button ID="Quitar" runat="server" Text="Quitar" Visible="False"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style48">
                            <asp:HyperLink ID="HyperLink2" runat="server"
                                NavigateUrl="~/"
                                CssClass="defaultBtn btnThemeGrey btnThemeWide">
								Regresar&nbsp;al&nbsp;Menú
                            </asp:HyperLink>
                        </td>
                        <td colspan="2">&nbsp;</td>
                        <td colspan="2">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <asp:Panel ID="PnlConfirma" runat="server" CssClass="dialogOverwrite dialog1">
        <table>
            <tr>
                <td colspan="3">
                    <div style="width: 100%; text-align: center; padding-bottom: 10px;">
                        <asp:Image ID="Image1" runat="server"
                            ImageUrl="~/Resources/imagenes/Exclamation.png"
                            Width="50" Height="50" />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding-bottom: 20px;">En el almacén de archivos ya existe un archivo con el mismo nombre:
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding-bottom: 20px;">
                    <asp:HyperLink ID="HLexiste" runat="server"
                        CssClass="hyperlink"
                        Target="_blank">Link</asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding-bottom: 20px;">Puede asignar este mismo archivo al subtema actual, 
                    sobreescribir el archivo antiguo con el archivo recién cargado,
                    o cancelar la operación.
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <uc1:msgWarning runat="server" ID="msgWarning" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="btnDialogAssign" runat="server" Text="Asignar"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                    &nbsp;
                    <asp:Button ID="btnDialogOverwrite" runat="server" Text="Sobreescribir"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                    &nbsp;
                    <asp:Button ID="btnDialogCancel" runat="server" Text="Cancelar"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </asp:Panel>

    <table class="dataSources">
        <tr>
            <td>
            </td>
            <td>
                <asp:SqlDataSource ID="SdsEnlaces" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"></asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSmaterialapoyo" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [ApoyoSubtema] WHERE ([IdSubtema] = @IdSubtema) ORDER BY [Consecutivo]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVsubtemas" Name="IdSubtema"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDStemas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdTema], [IdAsignatura], Cast([Numero] as varchar(8))+ ' ' + [Descripcion] NomTema  FROM [Tema] WHERE ([IdAsignatura] = @IdAsignatura)
order by [Numero],[Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDScompetencias" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Competencia] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSsubtemas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Subtema]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSdatosubt" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT S.IdCompetencia, C.Descripcion Competencia, S.IdSubtema, S.Descripcion Subtema, S.Numero
FROM Subtema S, Competencia C
WHERE (S.IdTema = @IdTema) and (C.IdCompetencia = S.IdCompetencia)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLtemas" Name="IdTema"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdAsignatura], [IdGrado], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>
                <asp:HiddenField ID="HF1" runat="server" />
                <asp:ModalPopupExtender ID="HF1_ModalPopupExtender" runat="server"
                    Enabled="True" PopupControlID="PnlConfirma" TargetControlID="HF1" BackgroundCssClass="overlay">
                </asp:ModalPopupExtender>
            </td>
            <td></td>
        </tr>
    </table>
</asp:Content>

