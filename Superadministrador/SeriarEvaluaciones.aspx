﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="SeriarEvaluaciones.aspx.vb"
    Inherits="superadministrador_SeriarEvaluaciones"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            width: 100%;
        }

        .style23 {
            height: 36px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style24 {
            width: 127px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            color: #000000;
        }

        .style14 {
            width: 133px;
            text-align: right;
        }

        .style13 {
            text-align: right;
        }

        .style15 {
            text-align: right;
            height: 16px;
        }

        .style25 {
            height: 36px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000099;
        }

        .style26 {
            text-align: left;
            height: 16px;
            color: #000099;
            font-weight: bold;
        }

        .style37 {
            height: 36px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000000;
        }

        .style38 {
            text-align: right;
            height: 16px;
            color: #000000;
        }

        .style16 {
            text-align: left;
        }

        .auto-style1 {
            text-align: left;
            width: 112px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Seriar Actividades
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style11">
        <tr>
            <td class="style26">1. Primero elija la Actividad que estará bloqueada:</td>
        </tr>
        <tr>
            <td>
                <table class="estandar">
                    <tr>
                        <td class="style24">
                            <asp:Label ID="Label1" runat="server" Text="[CICLO]" />
                        </td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                                DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                                DataValueField="IdCicloEscolar" Width="322px" Height="22px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style24">
                            <asp:Label ID="Label2" runat="server" Text="[NIVEL]" /></td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                                Height="22px" Width="322px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style24">
                            <asp:Label ID="Label3" runat="server" Text="[GRADO]" /></td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                                Height="22px" Width="322px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style38">
                            <asp:Label ID="Label5" runat="server" Text="[ASIGNATURA]" /></td>
                        <td class="style16" colspan="3">
                            <asp:DropDownList ID="DDLasignaturaSeriadas" runat="server" AutoPostBack="True"
                                DataSourceID="SDSasignaturasSer" DataTextField="Materia"
                                DataValueField="IdAsignatura" Height="22px" Width="650px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style38">Calificaciones</td>
                        <td class="style16" colspan="3">
                            <asp:DropDownList ID="DDLcalificacionseriadas" runat="server" AutoPostBack="True"
                                DataSourceID="SDScalificacionesSer" DataTextField="Cal"
                                DataValueField="IdCalificacion" Height="22px" Width="650px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15" colspan="4">
                            <asp:GridView ID="GVevaluacionesG" runat="server" AllowPaging="True"
                                AllowSorting="True" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3"
                                DataKeyNames="IdEvaluacion" DataSourceID="SDSevaluacionesSer"
                                GridLines="Vertical" Style="font-size: x-small"
                                Caption="<h3>Actividades disponibles:</h3>"
                                ForeColor="Black">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                                        Visible="False" ReadOnly="True" SortExpression="IdEvaluacion" />
                                    <asp:BoundField DataField="ClaveBateria" HeaderText="Nombre de la Actividad"
                                        SortExpression="ClaveBateria" />
                                    <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                                        SortExpression="Porcentaje">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderText="Inicia" SortExpression="InicioContestar" />
                                    <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderText="Termina" SortExpression="FinContestar" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style15" colspan="4">
                            <asp:GridView ID="GVseriaciones" runat="server" AllowPaging="True"
                                AllowSorting="True" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3"
                                DataKeyNames="IdEvaluacion, Minima" DataSourceID="SDSseriaciones"
                                GridLines="Vertical" Style="font-size: x-small"
                                Caption="<h3>Actividades Necesarias para desbloquear esta Evaluación:</h3>"
                                ForeColor="Black">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                                        Visible="False" ReadOnly="True" SortExpression="IdEvaluacion" />
                                    <asp:BoundField DataField="ClaveBateria" HeaderText="Nombre de la Actividad"
                                        SortExpression="ClaveBateria" />
                                    <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                                        SortExpression="Porcentaje">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderText="Inicia" SortExpression="InicioContestar" />
                                    <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderText="Termina" SortExpression="FinContestar" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                    <asp:BoundField DataField="Minima" HeaderText="Calificación Mínima"
                                        SortExpression="Minima" />
                                    <asp:BoundField DataField="FechaModif" HeaderText="Fecha de Modificación"
                                        Visible="False" SortExpression="FechaModif" />
                                    <asp:BoundField DataField="Modifico" HeaderText="Modificó"
                                        Visible="False" SortExpression="Modifico" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <asp:Panel ID="pnlActualizar" runat="server" Visible="true">
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="text-align: left;" colspan="3">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="CHKminimaEdit" runat="server"
                                                            AutoPostBack="true"
                                                            Text="Requerir Calificación Mínima"
                                                            Checked="false" />
                                                    </td>
                                                    <td>
                                                        <asp:Panel ID="PanelMinimaEdit" runat="server"
                                                            Visible="false">
                                                            <table>
                                                                <tr>
                                                                    <td>Calificación Mínima para Continuar:
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="TBminimaEdit" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style1">&nbsp;</td>
                                        <td class="style16">
                                            <asp:Button ID="btnActualizar" runat="server" Text="Actualizar"
                                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                            &nbsp;
                                <asp:Button ID="BtnQuitar" runat="server" Text="Quitar"
                                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                                        </td>
                                        <td class="style16">&nbsp;</td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="style26" colspan="4">2. Ahora elija la actividad que deberá estar terminada para poder iniciar la bloqueada:</td>
                    </tr>
                    <tr>
                        <td class="style24">
                            <asp:Label ID="Label4" runat="server" Text="[ASIGNATURA]" /></td>
                        <td colspan="3" style="text-align: left">
                            <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                                DataSourceID="SDSasignaturas" DataTextField="Materia"
                                DataValueField="IdAsignatura" Height="22px" Width="650px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style24">Calificaciones</td>
                        <td colspan="3" style="text-align: left">
                            <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True"
                                DataSourceID="SDScalificaciones" DataTextField="Cal"
                                DataValueField="IdCalificacion" Height="22px" Width="650px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;" colspan="4">
                            <table style="width: 100%;">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="CHKminima" runat="server"
                                            AutoPostBack="true"
                                            Text="Requerir Calificación Mínima"
                                            Checked="false" />
                                    </td>
                                    <td>
                                        <asp:Panel ID="PanelMinima" runat="server"
                                            Visible="false">
                                            <table>
                                                <tr>
                                                    <td>Calificación Mínima para Continuar:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TBminima" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="style13" colspan="4">
                            <asp:GridView ID="GVevaluacionesDisponibles" runat="server" AllowPaging="True"
                                AllowSorting="True" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3"
                                DataKeyNames="IdEvaluacion" DataSourceID="SDSevaluaciones"
                                GridLines="Vertical" Style="font-size: x-small; text-align: left;"
                                Caption="<h3>Actividades disponibles:</h3>"
                                ForeColor="Black">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                                        Visible="False" ReadOnly="True" SortExpression="IdEvaluacion" />
                                    <asp:BoundField DataField="ClaveBateria" HeaderText="Nombre de la Actividad"
                                        SortExpression="ClaveBateria" />
                                    <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                                        SortExpression="Porcentaje">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderText="Inicia" SortExpression="InicioContestar" />
                                    <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderText="Termina" SortExpression="FinContestar" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">&nbsp;</td>
                        <td class="style16">
                            <asp:Panel ID="pnlSeriar" runat="server" Visible="false">
                                <asp:Button ID="BtnSeriar" runat="server" Text="Seriar"
                                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            </asp:Panel>
                        </td>
                        <td class="style17">&nbsp;</td>
                        <td class="style16">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style16">
                <table>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td class="style16">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar] 
WHERE Estatus = 'Activo'
ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT C.IdCalificacion, C.Descripcion + ' (' + A.Descripcion + ')' as Cal
FROM Calificacion C, Asignatura A
WHERE C.IdCicloEscolar = @IdCicloEscolar
and A.IdAsignatura = C.IdAsignatura and A.IdAsignatura = @IdAsignatura
order by C.Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdGrado,Descripcion FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT A.IdAsignatura, A.Descripcion + ' (' + Ar.Descripcion + ')'  as Materia
FROM Asignatura A, Area Ar
WHERE (A.IdGrado = @IdGrado) and (Ar.IdArea = A.IdArea)
ORDER BY Ar.Descripcion,A.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluacionesSer" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Evaluacion] WHERE ([IdCalificacion] = @IdCalificacion)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcalificacionseriadas" Name="IdCalificacion"
                            PropertyName="SelectedValue" Type="Int64" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignaturasSer" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT A.IdAsignatura, A.Descripcion + ' (' + Ar.Descripcion + ')'  as Materia
FROM Asignatura A, Area Ar
WHERE (A.IdGrado = @IdGrado) and (Ar.IdArea = A.IdArea)
ORDER BY Ar.Descripcion,A.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDScalificacionesSer" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT C.IdCalificacion, C.Descripcion + ' (' + A.Descripcion + ')' as Cal
FROM Calificacion C, Asignatura A
WHERE C.IdCicloEscolar = @IdCicloEscolar
and A.IdAsignatura = C.IdAsignatura and A.IdAsignatura = @IdAsignatura
order by C.Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignaturaSeriadas" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSseriaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="
                SELECT Distinct 
                    E.IdEvaluacion, 
                    E.ClaveBateria, 
                    E.Porcentaje, 
                    E.InicioContestar, 
                    E.FinContestar, 
                    E.Tipo, 
                    E.Estatus, 
                    S.Minima, 
                    S.FechaModif, 
                    S.Modifico 
                FROM 
                    Evaluacion E, 
                    SeriacionEvaluaciones S 
                WHERE 
                    E.IdEvaluacion = S.IdEvaluacionPrevia 
                    AND S.IdEvaluacion = @IdEvaluacion 
                    AND  E.IdEvaluacion IN (SELECT S.IdEvaluacionPrevia FROM SeriacionEvaluaciones S WHERE S.IdEvaluacion = @IdEvaluacion)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVevaluacionesG" Name="IdEvaluacion"
                            PropertyName="SelectedDataKey[0]" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Evaluacion] WHERE ([IdCalificacion] = @IdCalificacion) AND IdEvaluacion <> @IdEvaluacion
                                 AND IdEvaluacion NOT IN (SELECT S.IdEvaluacionPrevia FROM SeriacionEvaluaciones S WHERE S.IdEvaluacion = @IdEvaluacion)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion"
                            PropertyName="SelectedValue" Type="Int64" />
                        <asp:ControlParameter ControlID="GVevaluacionesG" Name="IdEvaluacion"
                            PropertyName="SelectedDataKey[0]" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>

