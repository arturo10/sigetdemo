﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using Siget;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;

public partial class superadministrador_paginaInicio_Default : System.Web.UI.Page
{

  protected void Page_Load(object sender, EventArgs e)
  {
    Siget.Utils.Sesion.sesionAbierta();

    if (!IsPostBack)
    {
      TitleLiteral.Text = "Configura Inicio";

      CargaValores();
    }
  }

  protected void CargaValores()
  {
    if (Siget.Config.PaginaInicio.AvisoPublico != "")
    {
      TxtAviso.Text = Siget.Config.PaginaInicio.AvisoPublico;
      TxtFechaInicioAviso.Text = Siget.Config.PaginaInicio.AvisoPublico_FechaInicio.ToShortDateString();
      TxtFechaFinAviso.Text = Siget.Config.PaginaInicio.AvisoPublico_FechaFin.ToShortDateString();
    }


    DdlTipo_Logo.SelectedValue = Siget.Config.PaginaInicio.LogoCliente_Tipo.ToString();
    DdlTipo_Logo_SelectedIndexChanged(null, null);
    if (Siget.Config.PaginaInicio.LogoCliente_Tipo == 0)
    {
      HlCarga_Logo.Visible = true;
      HlCarga_Logo.Text = Siget.Config.PaginaInicio.LogoCliente_Objetivo;
      HlCarga_Logo.NavigateUrl = Siget.Config.PaginaInicio.LogoCliente_Objetivo;
    }
    else if (Siget.Config.PaginaInicio.LogoCliente_Tipo == 1)
    {
      TxtLink_Logo.Text = Siget.Config.PaginaInicio.LogoCliente_Objetivo;
    }


    DdlTipo_Boton1.SelectedValue = Siget.Config.PaginaInicio.Boton1_Tipo.ToString();
    DdlTipo_Boton1_SelectedIndexChanged(null, null);
    if (Siget.Config.PaginaInicio.Boton1_Tipo == 0)
    {
      HlCarga_Boton1.Visible = true;
      HlCarga_Boton1.Text = Siget.Config.PaginaInicio.Boton1_Objetivo;
      HlCarga_Boton1.NavigateUrl = Siget.Config.PaginaInicio.Boton1_Objetivo;
    }
    else if (Siget.Config.PaginaInicio.Boton1_Tipo == 1)
    {
      TxtLink_Boton1.Text = Siget.Config.PaginaInicio.Boton1_Objetivo;
    }
    TxtNombreBoton1.Text = Siget.Config.PaginaInicio.Boton1_Texto;


    DdlTipo_Boton2.SelectedValue = Siget.Config.PaginaInicio.Boton2_Tipo.ToString();
    DdlTipo_Boton2_SelectedIndexChanged(null, null);
    if (Siget.Config.PaginaInicio.Boton2_Tipo == 0)
    {
      HlCarga_Boton2.Visible = true;
      HlCarga_Boton2.Text = Siget.Config.PaginaInicio.Boton2_Objetivo;
      HlCarga_Boton2.NavigateUrl = Siget.Config.PaginaInicio.Boton2_Objetivo;
    }
    else if (Siget.Config.PaginaInicio.Boton2_Tipo == 1)
    {
      TxtLink_Boton2.Text = Siget.Config.PaginaInicio.Boton2_Objetivo;
    }
    TxtNombreBoton2.Text = Siget.Config.PaginaInicio.Boton2_Texto;


    DdlTipo_Boton3.SelectedValue = Siget.Config.PaginaInicio.Boton3_Tipo.ToString();
    DdlTipo_Boton3_SelectedIndexChanged(null, null);
    if (Siget.Config.PaginaInicio.Boton3_Tipo == 0)
    {
      HlCarga_Boton3.Visible = true;
      HlCarga_Boton3.Text = Siget.Config.PaginaInicio.Boton3_Objetivo;
      HlCarga_Boton3.NavigateUrl = Siget.Config.PaginaInicio.Boton3_Objetivo;
    }
    else if (Siget.Config.PaginaInicio.Boton3_Tipo == 1)
    {
      TxtLink_Boton3.Text = Siget.Config.PaginaInicio.Boton3_Objetivo;
    }


    DdlTipo_Boton4.SelectedValue = Siget.Config.PaginaInicio.Boton4_Tipo.ToString();
    DdlTipo_Boton4_SelectedIndexChanged(null, null);
    if (Siget.Config.PaginaInicio.Boton4_Tipo == 0)
    {
      HlCarga_Boton4.Visible = true;
      HlCarga_Boton4.Text = Siget.Config.PaginaInicio.Boton4_Objetivo;
      HlCarga_Boton4.NavigateUrl = Siget.Config.PaginaInicio.Boton4_Objetivo;
    }
    else if (Siget.Config.PaginaInicio.Boton4_Tipo == 1)
    {
      TxtLink_Boton4.Text = Siget.Config.PaginaInicio.Boton4_Objetivo;
    }
  }

  protected void ActualizaPaneles(DropDownList ddlTipo, PlaceHolder phrLink, PlaceHolder phrCarga)
  {
    if (ddlTipo.SelectedValue == "0")
    {
      phrLink.Visible = false;
      phrCarga.Visible = true;
    }
    else if (ddlTipo.SelectedValue == "1")
    {
      phrLink.Visible = true;
      phrCarga.Visible = false;
    }
    else
    {
      phrLink.Visible = false;
      phrCarga.Visible = false;
    }
  }

  protected void DdlTipo_Logo_SelectedIndexChanged(object sender, EventArgs e)
  {
    ActualizaPaneles(DdlTipo_Logo, PhrLink_Logo, PhrCarga_Logo);
  }

  protected void DdlTipo_Boton1_SelectedIndexChanged(object sender, EventArgs e)
  {
    ActualizaPaneles(DdlTipo_Boton1, PhrLink_Boton1, PhrCarga_Boton1);
  }
  protected void DdlTipo_Boton2_SelectedIndexChanged(object sender, EventArgs e)
  {
    ActualizaPaneles(DdlTipo_Boton2, PhrLink_Boton2, PhrCarga_Boton2);
  }
  protected void DdlTipo_Boton3_SelectedIndexChanged(object sender, EventArgs e)
  {
    ActualizaPaneles(DdlTipo_Boton3, PhrLink_Boton3, PhrCarga_Boton3);
  }
  protected void DdlTipo_Boton4_SelectedIndexChanged(object sender, EventArgs e)
  {
    ActualizaPaneles(DdlTipo_Boton4, PhrLink_Boton4, PhrCarga_Boton4);
  }

  protected void BtnAviso_Click(object sender, EventArgs e)
  {
    msgSuccess.hide();
    msgError.hide();

    if (TxtFechaInicioAviso.Text.Trim() == "")
    {
      msgError.show("Especifique la fecha de inicio del periodo de visibilidad del aviso.");
      return;
    }
    if (TxtFechaFinAviso.Text.Trim() == "")
    {
      msgError.show("Especifique la fecha límite del periodo de visibilidad del aviso.");
      return;
    }

    try
    {
     // BtnAviso.Text = Siget.Utils.DateUtils.Date_to_EndOfDay(TxtFechaFinAviso.Text.Trim());
      PaginaPrincipal_Insert(
        "avisoPublico",
        null,
        null,
        TxtAviso.Text,
        Siget.Utils.DateUtils.Date_to_StartOfDay(TxtFechaInicioAviso.Text.Trim()),
        Siget.Utils.DateUtils.Date_to_EndOfDay(TxtFechaFinAviso.Text.Trim()));

      Siget.Config.PaginaInicio.Vigente = false;
      msgSuccess.show("Éxito", "Se guardó el aviso.");
    }
    catch (Exception ex)
    {
      Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
      msgError.show(Siget.Lang.FileSystem.General.MSG_ERROR["es-mx"], ex.Message.ToString());
    }

    CargaValores();
  }

  protected void BtnGuardar_Logo_Click(object sender, EventArgs e)
  {
    msgSuccess.hide();
    msgError.hide();
    FileUpload upload = FuCarga_Logo;
    TextBox tbEnlace = TxtLink_Logo;
    HyperLink hlEnlace = HlCarga_Logo;
    DropDownList tipo = DdlTipo_Logo;
    string control = "logoCliente";
    string objetivo = null;
    string texto = null;
    string fechaInicia = null;
    string fechaTermina = null;

    InsertaControl(upload,
      tbEnlace,
      hlEnlace,
      tipo,
      control,
      objetivo,
      texto,
      fechaInicia,
      fechaTermina);

    CargaValores();
  }

  protected void BtnGuardar_Boton1_Click(object sender, EventArgs e)
  {
    msgSuccess.hide();
    msgError.hide();

    FileUpload upload = FuCarga_Boton1;
    TextBox tbEnlace = TxtLink_Boton1;
    HyperLink hlEnlace = HlCarga_Boton1;
    DropDownList tipo = DdlTipo_Boton1;
    string control = "boton1";
    string objetivo = null;
    string texto = TxtNombreBoton1.Text;
    string fechaInicia = null;
    string fechaTermina = null;

    InsertaControl(upload,
      tbEnlace,
      hlEnlace,
      tipo,
      control,
      objetivo,
      texto,
      fechaInicia,
      fechaTermina);

    CargaValores();
  }

  protected void BtnGuardar_Boton2_Click(object sender, EventArgs e)
  {
    msgSuccess.hide();
    msgError.hide();

    FileUpload upload = FuCarga_Boton2;
    TextBox tbEnlace = TxtLink_Boton2;
    HyperLink hlEnlace = HlCarga_Boton2;
    DropDownList tipo = DdlTipo_Boton2;
    string control = "boton2";
    string objetivo = null;
    string texto = TxtNombreBoton2.Text;
    string fechaInicia = null;
    string fechaTermina = null;

    InsertaControl(upload,
      tbEnlace,
      hlEnlace,
      tipo,
      control,
      objetivo,
      texto,
      fechaInicia,
      fechaTermina);

    CargaValores();
  }

  protected void BtnGuardar_Boton3_Click(object sender, EventArgs e)
  {
    msgSuccess.hide();
    msgError.hide();

    FileUpload upload = FuCarga_Boton3;
    TextBox tbEnlace = TxtLink_Boton3;
    HyperLink hlEnlace = HlCarga_Boton3;
    DropDownList tipo = DdlTipo_Boton3;
    string control = "boton3";
    string objetivo = null;
    string texto = null;
    string fechaInicia = null;
    string fechaTermina = null;

    InsertaControl(upload,
      tbEnlace,
      hlEnlace,
      tipo,
      control,
      objetivo,
      texto,
      fechaInicia,
      fechaTermina);

    CargaValores();
  }

  protected void BtnGuardar_Boton4_Click(object sender, EventArgs e)
  {
    msgSuccess.hide();
    msgError.hide();

    FileUpload upload = FuCarga_Boton3;
    TextBox tbEnlace = TxtLink_Boton3;
    HyperLink hlEnlace = HlCarga_Boton3;
    DropDownList tipo = DdlTipo_Boton3;
    string control = "boton4";
    string objetivo = null;
    string texto = null;
    string fechaInicia = null;
    string fechaTermina = null;

    InsertaControl(upload,
      tbEnlace,
      hlEnlace,
      tipo,
      control,
      objetivo,
      texto,
      fechaInicia,
      fechaTermina);

    CargaValores();
  }

  /// <summary>
  /// Recibe los controles de los cuales extraer el estado e inserta un contol a partir de ellos
  /// </summary>
  /// <param name="fileUpload"></param>
  /// <param name="textBox"></param>
  /// <param name="hyperlink"></param>
  /// <param name="tipo"></param>
  /// <param name="control"></param>
  /// <param name="objetivo"></param>
  /// <param name="texto"></param>
  /// <param name="fechaInicia"></param>
  /// <param name="fechaTermina"></param>
  protected void InsertaControl(
    FileUpload fileUpload,
    TextBox textBox,
    HyperLink hyperlink,
    DropDownList tipo,
    string control,
    string objetivo,
    string texto,
    string fechaInicia,
    string fechaTermina)
  {

    if (tipo.SelectedValue == "0")
    {
      if (!fileUpload.HasFile)
      {
        if (hyperlink.NavigateUrl != "")
        {
          objetivo = hyperlink.NavigateUrl.Replace(Siget.Config.Global.urlEnlaces, "");
        }
        else
        {
          msgError.show("Selecciona un archivo a cargar.");
          return;
        }
      }
      else
      {
        try
        {
          FileInfo oldFile = new FileInfo(Siget.Config.Global.rutaEnlaces + fileUpload.FileName);
          if (oldFile.Exists)
          {
            oldFile.Delete();
          }

          fileUpload.SaveAs(Siget.Config.Global.rutaEnlaces + fileUpload.FileName);

          hyperlink.Visible = true;
          hyperlink.Text = "Archivo cargado: " + HttpUtility.HtmlDecode(fileUpload.FileName).Trim();
          hyperlink.NavigateUrl = Siget.Config.Global.urlEnlaces + HttpUtility.HtmlDecode(fileUpload.FileName).Trim();

          objetivo = HttpUtility.HtmlDecode(fileUpload.FileName).Trim();
        }
        catch (Exception ex)
        {
          Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
          msgError.show(Siget.Lang.FileSystem.General.MSG_ERROR[Session["Usuario_Idioma"].ToString()], ex.Message.ToString());
        }
      }
    }
    else if (tipo.SelectedValue == "1")
    {
      objetivo = textBox.Text.Trim();
    }

    try
    {
      PaginaPrincipal_Insert(
        control,
        tipo.SelectedValue,
        objetivo,
        texto,
        fechaInicia,
        fechaTermina);

      msgSuccess.show("Éxito", "Se guardó el aviso.");
    }
    catch (Exception ex)
    {
      Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
      msgError.show(Siget.Lang.FileSystem.General.MSG_ERROR["es-mx"], ex.Message.ToString());
    }
  }

  protected void PaginaPrincipal_Insert(
    string control,
    string tipo,
    string objetivo,
    string texto,
    string fechaInicia,
    string fechaTermina)
  {
    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
    {
      conn.Open();
      using (SqlCommand cmd = new SqlCommand("PaginaInicio_Inserta", conn))
      {
        cmd.CommandType = System.Data.CommandType.StoredProcedure;

        cmd.Parameters.Add("@Control ", SqlDbType.VarChar).Value = control;
        if (tipo != null) cmd.Parameters.Add("@Tipo", SqlDbType.SmallInt).Value = tipo;
        if (objetivo != null) cmd.Parameters.Add("@Objetivo", SqlDbType.VarChar).Value = objetivo;
        cmd.Parameters.Add("@Modifico", SqlDbType.VarChar).Value = User.Identity.Name; 
        if (texto != null) cmd.Parameters.Add("@Texto", SqlDbType.VarChar).Value = texto;
        if (fechaInicia != null) cmd.Parameters.Add("@FechaInicia", SqlDbType.SmallDateTime).Value = fechaInicia;
        if (fechaTermina != null) cmd.Parameters.Add("@FechaTermina", SqlDbType.SmallDateTime).Value = fechaTermina;

        cmd.ExecuteNonQuery();

        Siget.Config.PaginaInicio.Vigente = false;
      }
    }
  }
}