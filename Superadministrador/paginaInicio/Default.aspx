﻿<%@ Page Title=""
  Language="C#"
  MasterPageFile="~/Principal.master"
  AutoEventWireup="true"
  CodeFile="Default.aspx.cs"
  Inherits="superadministrador_paginaInicio_Default"
  MaintainScrollPositionOnPostback="true"
  ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
  <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">

  <link id="css2" runat="server" rel="stylesheet"
    media="screen"
    href="Default.css" />

  <script type="text/javascript"
    src="/<%=Siget.Config.Global.NOMBRE_FILESYSTEM%>/Resources/scripts/jquery-1.11.2.min.js"></script>

  <script type="text/javascript"
    src="/<%=Siget.Config.Global.NOMBRE_FILESYSTEM%>/Resources/scripts/tinymce/tinymce.min.js"></script>

  <script type="text/javascript" src="Default.js"></script>

  <style type="text/css">
    .dialog1 {
      width: 700px;
    }
  </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
  <h1>Configuración de la Página de Inicio
  </h1>
  Permite configurar los enlaces y recursos que aparecen en la página de inicio.
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" runat="Server">
  <table style="width: 1000px;">
    <tr>
      <td colspan="4">
        <uc1:msgSuccess runat="server" ID="msgSuccess" />
      </td>
    </tr>
    <tr>
      <td colspan="4">
        <uc1:msgError runat="server" ID="msgError" />
      </td>
    </tr>
    <tr>
      <td colspan="4" class="mainTitle">
        <span>Mensaje de Aviso</span>
      </td>
    </tr>
    <tr>
      <td colspan="4">
        <asp:TextBox ID="TxtAviso" runat="server"
          ClientIDMode="Static"
          TextMode="MultiLine"
          CssClass="tinymce"></asp:TextBox>
      </td>
    </tr>
    <tr>
      <td>
        <span class="subTitle">Fecha de Inicio</span>
        <br />
        <asp:TextBox ID="TxtFechaInicioAviso" runat="server"
          CssClass="wideTextbox"
          placeholder="dd/mm/aaaa"></asp:TextBox>
        <asp:CalendarExtender ID="TxtFechaInicioAviso_CalendarExtender" runat="server"
          Enabled="True"
          Format="dd/MM/yyyy"  
          TargetControlID="TxtFechaInicioAviso">
        </asp:CalendarExtender>
      </td>
      <td>
        <span class="subTitle">Fecha de Fin</span>
        <br />
        <asp:TextBox ID="TxtFechaFinAviso" runat="server"
          CssClass="wideTextbox"
          placeholder="dd/mm/aaaa"></asp:TextBox>
        <asp:CalendarExtender ID="TxtFechaFinAviso_CalendarExtender" runat="server"
          Enabled="True"
          Format="dd/MM/yyyy"
          TargetControlID="TxtFechaFinAviso">
        </asp:CalendarExtender>
      </td>
      <td>
        <asp:Button ID="BtnAviso" runat="server"
          CssClass="defaultBtn btnThemeBlue btnThemeMedium"
          Text="Guardar Aviso" 
          OnClick="BtnAviso_Click" />
      </td>
    </tr>
    <tr>
      <td colspan="4">&nbsp;
      </td>
    </tr>


    <tr>
      <td colspan="4" class="mainTitle">
        <span>Ícono de Cliente</span>
      </td>
    </tr>
    <tr>
      <td>
        <span class="subTitle">Tipo de Enlace</span>
        <br />
        <asp:DropDownList ID="DdlTipo_Logo" runat="server" AutoPostBack="true" 
          CssClass="wideTextbox" 
          OnSelectedIndexChanged="DdlTipo_Logo_SelectedIndexChanged" >
          <asp:ListItem Value="-1">Desactivado</asp:ListItem>
          <asp:ListItem Value="1">Link</asp:ListItem>
          <asp:ListItem Value="0">Archivo</asp:ListItem>
        </asp:DropDownList>
      </td>
      <td colspan="2">
        <asp:PlaceHolder ID="PhrCarga_Logo" runat="server" Visible="false">
          <asp:HyperLink ID="HlCarga_Logo" runat="server" 
            CssClass="hyperlink" 
            Target="_blank" 
            Visible="False">Hyperlink</asp:HyperLink>
          &nbsp;
          <asp:FileUpload ID="FuCarga_Logo" runat="server" />
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="PhrLink_Logo" runat="server" 
          Visible="false">
          <asp:TextBox ID="TxtLink_Logo" runat="server" 
            Width="600" 
            CssClass="wideTextbox"></asp:TextBox>
        </asp:PlaceHolder>
      </td>
      <td>
        <asp:Button ID="BtnGuardar_Logo" runat="server"
          CssClass="defaultBtn btnThemeBlue btnThemeMedium"
          Text="Guardar" 
          OnClick="BtnGuardar_Logo_Click" />
      </td>
    </tr>


    <tr>
      <td colspan="4" class="mainTitle">
        <span>Botón Extremo Izquierdo</span>
      </td>
    </tr>
    <tr>
      <td>
        <span class="subTitle">Tipo de Enlace</span>
        <br />
        <asp:DropDownList ID="DdlTipo_Boton1" runat="server" AutoPostBack="true" 
          CssClass="wideTextbox" 
          OnSelectedIndexChanged="DdlTipo_Boton1_SelectedIndexChanged" >
          <asp:ListItem Value="-1">Desactivado</asp:ListItem>
          <asp:ListItem Value="1">Link</asp:ListItem>
          <asp:ListItem Value="0">Archivo</asp:ListItem>
        </asp:DropDownList>
      </td>
      <td colspan="2">
        <asp:PlaceHolder ID="PhrCarga_Boton1" runat="server" 
          Visible="false">
          <asp:HyperLink ID="HlCarga_Boton1" runat="server" 
            CssClass="hyperlink" 
            Target="_blank" 
            Visible="False">Hyperlink</asp:HyperLink>
          &nbsp;
          <asp:FileUpload ID="FuCarga_Boton1" runat="server" />
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="PhrLink_Boton1" runat="server" 
          Visible="false">
          <asp:TextBox ID="TxtLink_Boton1" runat="server" 
            Width="600" 
            CssClass="wideTextbox"></asp:TextBox>
        </asp:PlaceHolder>
      </td>
      <td>
        <asp:Button ID="BtnGuardar_Boton1" runat="server"
          CssClass="defaultBtn btnThemeBlue btnThemeMedium"
          Text="Guardar" 
          OnClick="BtnGuardar_Boton1_Click" />
      </td>
    </tr>
    <tr>
      <td>
        Texto del Botón
      </td>
      <td colspan="2" style="text-align: left;">
        <asp:TextBox ID="TxtNombreBoton1" runat="server" 
          CssClass="wideTextbox"></asp:TextBox>
      </td>
    </tr>


    <tr>
      <td colspan="4" class="mainTitle">
        <span>Botón Central Izquierdo</span>
      </td>
    </tr>
    <tr>
      <td>
        <span class="subTitle">Tipo de Enlace</span>
        <br />
        <asp:DropDownList ID="DdlTipo_Boton2" runat="server" AutoPostBack="true" 
          CssClass="wideTextbox" 
          OnSelectedIndexChanged="DdlTipo_Boton2_SelectedIndexChanged" >
          <asp:ListItem Value="-1">Desactivado</asp:ListItem>
          <asp:ListItem Value="1">Link</asp:ListItem>
          <asp:ListItem Value="0">Archivo</asp:ListItem>
        </asp:DropDownList>
      </td>
      <td colspan="2">
        <asp:PlaceHolder ID="PhrCarga_Boton2" runat="server" 
          Visible="false">
          <asp:HyperLink ID="HlCarga_Boton2" runat="server" 
            CssClass="hyperlink" 
            Target="_blank" 
            Visible="False">Hyperlink</asp:HyperLink>
          &nbsp;
          <asp:FileUpload ID="FuCarga_Boton2" runat="server" />
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="PhrLink_Boton2" runat="server" 
          Visible="false">
          <asp:TextBox ID="TxtLink_Boton2" runat="server" 
            Width="600" 
            CssClass="wideTextbox"></asp:TextBox>
        </asp:PlaceHolder>
      </td>
      <td>
        <asp:Button ID="BtnGuardar_Boton2" runat="server"
          CssClass="defaultBtn btnThemeBlue btnThemeMedium"
          Text="Guardar" 
          OnClick="BtnGuardar_Boton2_Click" />
      </td>
    </tr>
    <tr>
      <td>
        Texto del Botón
      </td>
      <td colspan="2" style="text-align: left;">
        <asp:TextBox ID="TxtNombreBoton2" runat="server" 
          CssClass="wideTextbox"></asp:TextBox>
      </td>
    </tr>


    <tr>
      <td colspan="4" class="mainTitle">
        <span>Botón Central Derecho</span>
      </td>
    </tr>
    <tr>
      <td>
        <span class="subTitle">Tipo de Enlace</span>
        <br />
        <asp:DropDownList ID="DdlTipo_Boton3" runat="server" AutoPostBack="true" 
          CssClass="wideTextbox" 
          OnSelectedIndexChanged="DdlTipo_Boton3_SelectedIndexChanged" >
          <asp:ListItem Value="-1">Desactivado</asp:ListItem>
          <asp:ListItem Value="1">Link</asp:ListItem>
          <asp:ListItem Value="0">Archivo</asp:ListItem>
        </asp:DropDownList>
      </td>
      <td colspan="2">
        <asp:PlaceHolder ID="PhrCarga_Boton3" runat="server" 
          Visible="false">
          <asp:HyperLink ID="HlCarga_Boton3" runat="server" 
            CssClass="hyperlink" 
            Target="_blank" 
            Visible="False">Hyperlink</asp:HyperLink>
          &nbsp;
          <asp:FileUpload ID="FuCarga_Boton3" runat="server" />
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="PhrLink_Boton3" runat="server" 
          Visible="false">
          <asp:TextBox ID="TxtLink_Boton3" runat="server" 
            Width="600" 
            CssClass="wideTextbox"></asp:TextBox>
        </asp:PlaceHolder>
      </td>
      <td>
        <asp:Button ID="BtnGuardar_Boton3" runat="server"
          CssClass="defaultBtn btnThemeBlue btnThemeMedium"
          Text="Guardar" 
          OnClick="BtnGuardar_Boton3_Click" />
      </td>
    </tr>


    <tr>
      <td colspan="4" class="mainTitle">
        <span>Botón Extremo Derecho</span>
      </td>
    </tr>
    <tr>
      <td>
        <span class="subTitle">Tipo de Enlace</span>
        <br />
        <asp:DropDownList ID="DdlTipo_Boton4" runat="server" AutoPostBack="true" 
          CssClass="wideTextbox" 
          OnSelectedIndexChanged="DdlTipo_Boton4_SelectedIndexChanged" >
          <asp:ListItem Value="-1">Desactivado</asp:ListItem>
          <asp:ListItem Value="1">Link</asp:ListItem>
          <asp:ListItem Value="0">Archivo</asp:ListItem>
        </asp:DropDownList>
      </td>
      <td colspan="2">
        <asp:PlaceHolder ID="PhrCarga_Boton4" runat="server" 
          Visible="false">
          <asp:HyperLink ID="HlCarga_Boton4" runat="server" 
            CssClass="hyperlink" 
            Target="_blank" 
            Visible="False">Hyperlink</asp:HyperLink>
          &nbsp;
          <asp:FileUpload ID="FuCarga_Boton4" runat="server" />
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="PhrLink_Boton4" runat="server" 
          Visible="false">
          <asp:TextBox ID="TxtLink_Boton4" runat="server" 
            Width="600" 
            CssClass="wideTextbox"></asp:TextBox>
        </asp:PlaceHolder>
      </td>
      <td>
        <asp:Button ID="BtnGuardar_Boton4" runat="server"
          CssClass="defaultBtn btnThemeBlue btnThemeMedium"
          Text="Guardar" 
          OnClick="BtnGuardar_Boton4_Click" />
      </td>
    </tr>







    <tr>
      <td colspan="4">
        <asp:HyperLink ID="HyperLink1" runat="server"
          NavigateUrl="~/"
          CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
      </td>
    </tr>
  </table>

  <table class="dataSources">
    <tr>
      <td>
        <asp:SqlDataSource ID="SdsEnlaces" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"></asp:SqlDataSource>
      </td>
      <td>
        <asp:HiddenField ID="HF1" runat="server" />
        <asp:ModalPopupExtender ID="HF1_ModalPopupExtender" runat="server"
          Enabled="True" PopupControlID="PnlConfirma" TargetControlID="HF1" BackgroundCssClass="overlay">
        </asp:ModalPopupExtender>
      </td>
      <td></td>
    </tr>
  </table>
</asp:Content>
