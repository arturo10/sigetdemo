﻿Imports Siget

Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Partial Class superadministrador_CargaReactivos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Button2.Text = "Actualiza " & Config.Etiqueta.EQUIPO & " (TMP)"
    End Sub

    Protected Sub Genera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Genera.Click
        If ArchivoCarga.HasFile Then 'El atributo .HasFile compara si se indico un archivo           
            Try
                msgInfo.show("Espere un momento por favor, la migración puede tardar varios minutos...")
                'Ahora CARGO el archivo
                ArchivoCarga.SaveAs(Server.MapPath("~") + "\superadministrador\" & ArchivoCarga.FileName)
                'Leo el archivo
                Dim archivo_datos As String = Server.MapPath(ArchivoCarga.FileName())
                Dim leer_archivo As StreamReader
                leer_archivo = File.OpenText(archivo_datos)
                Dim LineaLeida, IdSubtema, ConsecutivoP, RedaccionP, TipoRespuesta, Ponderacion, PorcentajeRestarResp, RedaccionO, Correcta, Consecutiva As String

                Dim IdPlanteamiento, I, F, ConsecutivoAnterior As Integer

                ConsecutivoAnterior = 0
                While Not leer_archivo.EndOfStream
                    'Leo el registro completo y ubico cada campo
                    LineaLeida = leer_archivo.ReadLine
                    I = InStr(LineaLeida, ";")
                    F = InStr(I + 1, LineaLeida, ";")
                    IdSubtema = Trim(Mid(LineaLeida, 1, I - 1)) 'CAMPO 1
                    ConsecutivoP = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 2
                    I = F
                    F = InStr(I + 1, LineaLeida, ";")
                    RedaccionP = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 3
                    I = F
                    F = InStr(I + 1, LineaLeida, ";")
                    Ponderacion = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 4
                    I = F
                    F = InStr(I + 1, LineaLeida, ";")
                    PorcentajeRestarResp = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 5
                    I = F
                    F = InStr(I + 1, LineaLeida, ";")
                    TipoRespuesta = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 6
                    I = F
                    F = InStr(I + 1, LineaLeida, ";")
                    Consecutiva = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 7
                    I = F
                    F = InStr(I + 1, LineaLeida, ";")
                    RedaccionO = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 8
                    I = F
                    Correcta = Trim(Mid(LineaLeida, I + 1, Len(LineaLeida))) 'CAMPO 9 (Ultimo)

                    If CInt(ConsecutivoP) <> ConsecutivoAnterior Then
                        'Creo el Planteamiento
                        SDSplanteamientos.InsertCommand = "SET dateformat dmy; INSERT INTO Planteamiento(IdSubtema, Consecutivo, Redaccion, TipoRespuesta, Ponderacion, PorcentajeRestarResp) " + _
                        "VALUES(" + IdSubtema + "," + ConsecutivoP + ",""" + RedaccionP + """,'" + TipoRespuesta + "'," + Ponderacion + "," + PorcentajeRestarResp + ")"
                        SDSplanteamientos.Insert()

                        'Ahora busco el recien ingresado Planteamiento para obtener su ID
                        'Debo mejorar el algoritmo para obtener este ID, ya que si se estuvieran creando Reactivos concurrentemente, no funcionaria
                        Dim strConexion As String
                        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                        Dim objConexion As New SqlConnection(strConexion)
                        Dim miComando As SqlCommand
                        Dim misRegistros As SqlDataReader
                        miComando = objConexion.CreateCommand
                        miComando.CommandText = "SELECT MAX(IdPlanteamiento) IdPlanteamiento FROM PLANTEAMIENTO"
                        objConexion.Open()
                        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                        misRegistros.Read()
                        IdPlanteamiento = misRegistros.Item("IdPlanteamiento")
                        misRegistros.Close()
                        objConexion.Close()
                        ConsecutivoAnterior = CInt(ConsecutivoP)
                    End If

                    'Ahora ingreso los registros de las Opciones
                    SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion(IdPlanteamiento, Redaccion, Correcta, Consecutiva) VALUES (" + _
                    CStr(IdPlanteamiento) + ",""" + RedaccionO + """,""" + Correcta + """,""" + Consecutiva + """)"
                    SDSopciones.Insert()
                End While
                leer_archivo.Close()
                msgSuccess.show("La migración ha terminado con éxito, revise los registros.")
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try
        Else
            msgError.show("Seleccione el archivo que contiene los datos para Migrar.")
        End If

        msgInfo.hide()
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        If ArchivoCarga.HasFile Then 'El atributo .HasFile compara si se indico un archivo           
            Try
                msgInfo.show("Espere un momento por favor, la migración puede tardar varios minutos...")
                'Ahora CARGO el archivo
                ArchivoCarga.SaveAs(Server.MapPath("~") + "\superadministrador\" & ArchivoCarga.FileName)
                'Leo el archivo
                Dim archivo_datos As String = Server.MapPath(ArchivoCarga.FileName())
                Dim leer_archivo As StreamReader
                leer_archivo = File.OpenText(archivo_datos)

                Dim LineaLeida, IdPlanteamiento, Redaccion, Correcta, Consecutiva As String
                Dim I, F, ConsecutivoAnterior As Integer


                ConsecutivoAnterior = 0
                While Not leer_archivo.EndOfStream
                    'Leo el registro completo y ubico cada campo
                    LineaLeida = leer_archivo.ReadLine
                    I = InStr(LineaLeida, ",")
                    F = InStr(I + 1, LineaLeida, ",")
                    IdPlanteamiento = Trim(Mid(LineaLeida, 1, I - 1)) 'CAMPO 1
                    Redaccion = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 2
                    I = F
                    F = InStr(I + 1, LineaLeida, ",")
                    Correcta = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 3
                    I = F
                    Consecutiva = Trim(Mid(LineaLeida, I + 1, Len(LineaLeida))) 'CAMPO 4 (Ultimo)

                    'Ahora ingreso los registros de las Opciones
                    SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion(IdPlanteamiento, Redaccion, Correcta, Consecutiva) VALUES (" + _
                    IdPlanteamiento + ",'" + Redaccion + "','" + Correcta + "','" + Consecutiva + "')"
                    SDSopciones.Insert()
                End While
                leer_archivo.Close()
                msgSuccess.show("La migración ha terminado con éxito, revise los registros.")
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try
        Else
            msgError.show("Seleccione el archivo que contiene los datos para Migrar.")
        End If

        msgInfo.hide()
    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        If ArchivoCarga.HasFile Then 'El atributo .HasFile compara si se indico un archivo           
            Try
                msgInfo.show("Espere un momento por favor, la migración puede tardar varios minutos...")
                'Ahora CARGO el archivo
                ArchivoCarga.SaveAs(Server.MapPath("~") + "\superadministrador\" & ArchivoCarga.FileName)
                'Leo el archivo
                Dim archivo_datos As String = Server.MapPath(ArchivoCarga.FileName())
                Dim leer_archivo As StreamReader
                leer_archivo = File.OpenText(archivo_datos)

                Dim LineaLeida, Matricula, Equipo, Email As String
                Dim I, F, ConsecutivoAnterior As Integer

                ConsecutivoAnterior = 0
                While Not leer_archivo.EndOfStream
                    'Leo el registro completo y ubico cada campo
                    LineaLeida = leer_archivo.ReadLine
                    I = InStr(LineaLeida, ",")
                    F = InStr(I + 1, LineaLeida, ",")
                    Matricula = Trim(Mid(LineaLeida, 1, I - 1)) 'CAMPO 1
                    Equipo = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 2
                    I = F
                    Email = Trim(Mid(LineaLeida, I + 1, Len(LineaLeida))) 'CAMPO 4 (Ultimo)

                    'Ahora ingreso los registros de las Opciones
                    SDSalumnos.InsertCommand = "SET dateformat dmy; UPDATE Alumno SET Equipo = '" + Equipo + "' WHERE Matricula = '" + Matricula + "'"
                    SDSalumnos.Insert()
                End While
                leer_archivo.Close()
                msgSuccess.show("La migración ha terminado con éxito, revise los registros.")
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try
        Else
            msgError.show("Seleccione el archivo que contiene los datos para Migrar.")
        End If

        msgInfo.hide()
    End Sub

    Protected Sub Multiples_Click(sender As Object, e As EventArgs) Handles Multiples.Click
        msgError.hide()
        msgSuccess.hide()

        If ArchivoCarga.HasFile Then 'El atributo .HasFile compara si se indico un archivo  
            Dim leer_archivo As StreamReader

            Try
                msgInfo.show("Espere un momento por favor, la migración puede tardar varios minutos...")
                'Ahora CARGO el archivo
                ArchivoCarga.SaveAs(Server.MapPath("~") + "\superadministrador\" & ArchivoCarga.FileName)
                'Leo el archivo
                Dim archivo_datos As String = Server.MapPath(ArchivoCarga.FileName())
                leer_archivo = File.OpenText(archivo_datos)
                Dim LineaLeida,
                    IdSubtema,
                    ConsecutivoPlanteamiento,
                    RedaccionPlanteamiento,
                    TipoRespuesta,
                    PuntosPonderacion,
                    PorcentajeRestarResp,
                    ReactivoPractica,
                    OcultarRedaccion,
                    OpcionA,
                    OpcionB,
                    OpcionC,
                    OpcionD,
                    OpcionE,
                    OpcionF,
                    Correcta,
                    Tiempo,
                    Minutos,
                    Permite2 As String

                Dim Verifica, ComparaMayusculas, ComparaAcentos, ComparaSoloAlfanumerico As Integer
                Verifica = 0
                ComparaMayusculas = 0
                ComparaAcentos = 0
                ComparaSoloAlfanumerico = 0

                Dim IdPlanteamiento, I, F As Integer

                While Not leer_archivo.EndOfStream
                    'Leo el registro completo
                    LineaLeida = leer_archivo.ReadLine

                    'LineaLeida.Replace(vbNewLine, "*salto*") ' saltos de linea escondidos los remplazo por la clave
                    LineaLeida = LineaLeida.Replace("'", "''") ' apostrofes las remplazo por su equivalente en vb
                    'LineaLeida.Replace("""", "") ' comillas las quito

                    msgInfo.show(LineaLeida)
                    'ubico cada campo en la linea
                    I = InStr(LineaLeida, "|")
                    F = InStr(I + 1, LineaLeida, "|")
                    IdSubtema = Trim(Mid(LineaLeida, 1, I - 1)) 'CAMPO 1
                    ConsecutivoPlanteamiento = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 2
                    I = F
                    F = InStr(I + 1, LineaLeida, "|")
                    RedaccionPlanteamiento = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 3
                    I = F
                    F = InStr(I + 1, LineaLeida, "|")
                    TipoRespuesta = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 4
                    I = F
                    F = InStr(I + 1, LineaLeida, "|")
                    PuntosPonderacion = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 5
                    I = F
                    F = InStr(I + 1, LineaLeida, "|")
                    PorcentajeRestarResp = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 6
                    I = F
                    F = InStr(I + 1, LineaLeida, "|")
                    ReactivoPractica = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 7
                    I = F
                    F = InStr(I + 1, LineaLeida, "|")
                    OcultarRedaccion = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 8
                    I = F
                    F = InStr(I + 1, LineaLeida, "|")
                    Tiempo = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 9
                    I = F
                    F = InStr(I + 1, LineaLeida, "|")
                    Minutos = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 10
                    I = F
                    F = InStr(I + 1, LineaLeida, "|")
                    Permite2 = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 11
                    I = F
                    F = InStr(I + 1, LineaLeida, "|")
                    OpcionA = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 12
                    I = F
                    F = InStr(I + 1, LineaLeida, "|")
                    OpcionB = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 13
                    I = F
                    F = InStr(I + 1, LineaLeida, "|")
                    OpcionC = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 14
                    I = F
                    F = InStr(I + 1, LineaLeida, "|")
                    OpcionD = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 15
                    I = F
                    F = InStr(I + 1, LineaLeida, "|")
                    OpcionE = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 16
                    I = F
                    F = InStr(I + 1, LineaLeida, "|")
                    OpcionF = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 17
                    I = F
                    Correcta = Trim(Mid(LineaLeida, I + 1, Len(LineaLeida))) 'CAMPO 18 (Ultimo)

                    'LblError.Text = LineaLeida & "<br />" &
                    'IdSubtema & "<br />" &
                    'ConsecutivoPlanteamiento & "<br />" &
                    'RedaccionPlanteamiento & "<br />" &
                    'TipoRespuesta & "<br />" &
                    'PuntosPonderacion & "<br />" &
                    'PorcentajeRestarResp & "<br />" &
                    'ReactivoPractica & "<br />" &
                    'OcultarRedaccion & "<br />" &
                    'OpcionA & "<br />" &
                    'OpcionB & "<br />" &
                    'OpcionC & "<br />" &
                    'OpcionD & "<br />" &
                    'OpcionE & "<br />" &
                    'OpcionF & "<br />" &
                    'Correcta

                    'Creo el Planteamiento
                    If (Tiempo = "False") Then
                        SDSplanteamientos.InsertCommand = "SET dateformat dmy; INSERT INTO Planteamiento(" &
                           "IdSubtema, Consecutivo, Redaccion, TipoRespuesta, Ponderacion, PorcentajeRestarResp, Ocultar, EsconderTexto, Tiempo, Permite2, FechaModif, Modifico, Verifica,ComparaMayusculas,ComparaAcentos,ComparaSoloAlfaNumerico,HabilitaRecorder,EliminaPuntuacion) " &
                           "VALUES(" & IdSubtema & "," &
                           ConsecutivoPlanteamiento & ",'" &
                           RedaccionPlanteamiento & "','" &
                           TipoRespuesta & "'," &
                           PuntosPonderacion & "," &
                           PorcentajeRestarResp & ", '" &
                           ReactivoPractica & "', '" &
                           OcultarRedaccion & "', 'False', '" & Permite2 & "', getdate(), '" & User.Identity.Name & "'," &
                           Verifica & "," & ComparaMayusculas & "," & ComparaAcentos & "," & ComparaSoloAlfanumerico & ",'False'" & ",'False'" & ")"
                    Else
                        SDSplanteamientos.InsertCommand = "SET dateformat dmy; INSERT INTO Planteamiento(" &
                            "IdSubtema, Consecutivo, Redaccion, TipoRespuesta, Ponderacion, PorcentajeRestarResp, Ocultar, EsconderTexto, Tiempo, Minutos, Permite2, FechaModif, Modifico,Verifica,ComparaMayusculas,ComparaAcentos,ComparaSoloAlfanumerico,HabilitaRecorder,EliminaPuntuacion ) " &
                            "VALUES(" & IdSubtema & "," &
                            ConsecutivoPlanteamiento & ",'" &
                            RedaccionPlanteamiento & "','" &
                            TipoRespuesta & "'," &
                            PuntosPonderacion & "," &
                            PorcentajeRestarResp & ", '" &
                            ReactivoPractica & "', '" &
                            OcultarRedaccion & "', 'True', '" & Minutos & "', '" & Permite2 & "', getdate(), '" & User.Identity.Name & "'," &
                            Verifica & "," & ComparaMayusculas & "," & ComparaAcentos & "," & ComparaSoloAlfanumerico & ",'False'" & ",'False'" & ")"
                    End If
                    SDSplanteamientos.Insert()

                    'Ahora busco el recien ingresado Planteamiento para obtener su ID
                    'Debo mejorar el algoritmo para obtener este ID, ya que si se estuvieran creando Reactivos concurrentemente, no funcionaria
                    Dim strConexion As String = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                    'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                    Dim objConexion As New SqlConnection(strConexion)
                    Dim miComando As SqlCommand
                    Dim misRegistros As SqlDataReader
                    miComando = objConexion.CreateCommand
                    miComando.CommandText = "SELECT MAX(IdPlanteamiento) IdPlanteamiento FROM Planteamiento"
                    objConexion.Open()
                    misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                    misRegistros.Read()
                    IdPlanteamiento = misRegistros.Item("IdPlanteamiento")
                    misRegistros.Close()
                    objConexion.Close()


                    'Ahora ingreso los registros de las Opciones
                    Dim EsCorrecta As String = "N"
                    If Not OpcionA.Equals("") Then ' Opcion A
                        If Correcta.ToLower.Equals("a") Then
                            EsCorrecta = "S"
                        End If

                        SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion(IdPlanteamiento, Redaccion, Correcta, Consecutiva, Esconder, FechaModif, Modifico) VALUES (" &
                            CStr(IdPlanteamiento) & ",'" & OpcionA & "','" & EsCorrecta & "','A', 'False', getdate(), '" & User.Identity.Name & "')"
                        SDSopciones.Insert()

                        EsCorrecta = "N"
                    End If
                    If Not OpcionB.Equals("") Then ' Opcion B
                        If Correcta.ToLower.Equals("b") Then
                            EsCorrecta = "S"
                        End If

                        SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion(IdPlanteamiento, Redaccion, Correcta, Consecutiva, Esconder, FechaModif, Modifico) VALUES (" &
                            CStr(IdPlanteamiento) & ",'" & OpcionB & "','" & EsCorrecta & "','B', 'False', getdate(), '" & User.Identity.Name & "')"
                        SDSopciones.Insert()

                        EsCorrecta = "N"
                    End If
                    If Not OpcionC.Equals("") Then ' Opcion C
                        If Correcta.ToLower.Equals("c") Then
                            EsCorrecta = "S"
                        End If

                        SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion(IdPlanteamiento, Redaccion, Correcta, Consecutiva, Esconder, FechaModif, Modifico) VALUES (" &
                            CStr(IdPlanteamiento) & ",'" & OpcionC & "','" & EsCorrecta & "','C', 'False', getdate(), '" & User.Identity.Name & "')"
                        SDSopciones.Insert()

                        EsCorrecta = "N"
                    End If
                    If Not OpcionD.Equals("") Then ' Opcion D
                        If Correcta.ToLower.Equals("d") Then
                            EsCorrecta = "S"
                        End If

                        SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion(IdPlanteamiento, Redaccion, Correcta, Consecutiva, Esconder, FechaModif, Modifico) VALUES (" &
                            CStr(IdPlanteamiento) & ",'" & OpcionD & "','" & EsCorrecta & "','D', 'False', getdate(), '" & User.Identity.Name & "')"
                        SDSopciones.Insert()

                        EsCorrecta = "N"
                    End If
                    If Not OpcionE.Equals("") Then ' Opcion E
                        If Correcta.ToLower.Equals("e") Then
                            EsCorrecta = "S"
                        End If

                        SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion(IdPlanteamiento, Redaccion, Correcta, Consecutiva, Esconder, FechaModif, Modifico) VALUES (" &
                            CStr(IdPlanteamiento) & ",'" & OpcionE & "','" & EsCorrecta & "','E', 'False', getdate(), '" & User.Identity.Name & "')"
                        SDSopciones.Insert()

                        EsCorrecta = "N"
                    End If
                    If Not OpcionF.Equals("") Then ' Opcion F
                        If Correcta.ToLower.Equals("f") Then
                            EsCorrecta = "S"
                        End If

                        SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion(IdPlanteamiento, Redaccion, Correcta, Consecutiva, Esconder, FechaModif, Modifico) VALUES (" &
                            CStr(IdPlanteamiento) & ",'" & OpcionF & "','" & EsCorrecta & "','F', 'False', getdate(), '" & User.Identity.Name & "')"
                        SDSopciones.Insert()

                        EsCorrecta = "N"
                    End If

                End While
                leer_archivo.Close()
                msgSuccess.show("La migración ha terminado con éxito, revise los registros.")
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try
        Else
            msgError.show("Seleccione el archivo que contiene los datos para Migrar.")
        End If

        msgInfo.hide()
    End Sub
End Class
