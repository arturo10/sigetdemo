﻿Imports Siget

Imports System.Data.SqlClient

Partial Class superadministrador_Competencias
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString

        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        miComando = objConexion.CreateCommand
        miComando.CommandText = "select Count(IdCompetencia) as Total from Competencia"
        objConexion.Open() 'Abro la conexion
        misRegistros = miComando.ExecuteReader()
        misRegistros.Read()
        If misRegistros.Item("Total") = 0 Then
            SDScompetencia.InsertCommand = "SET dateformat dmy; INSERT INTO Competencia(Descripcion, FechaModif, Modifico) values('DISPONIBLE PARA MODIFICAR', getdate(), '" & User.Identity.Name & "')"
            SDScompetencia.Insert()
        End If
        misRegistros.Close()
        objConexion.Close()
    End Sub
End Class
