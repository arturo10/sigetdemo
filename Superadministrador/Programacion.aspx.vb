﻿Imports Siget

Imports System.IO 'Para que funcione: FileInfo

Partial Class superadministrador_Programacion
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GVprogramacion.Caption = "<h3>Listado de " &
            Config.Etiqueta.GRUPOS &
            " asignados a " &
            Config.Etiqueta.ARTDET_PROFESOR & " " & Config.Etiqueta.PROFESOR &
            " para " & Config.Etiqueta.ARTDET_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA &
            " y " &
            Config.Etiqueta.CICLO &
            " elegid" & Config.Etiqueta.LETRA_CICLO & "s</h3>"

        Label1.Text = Config.Etiqueta.PROFESORES
        Label2.Text = Config.Etiqueta.INSTITUCION
        Label3.Text = Config.Etiqueta.CICLO
        Label4.Text = Config.Etiqueta.NIVEL
        Label5.Text = Config.Etiqueta.GRADO
        Label6.Text = Config.Etiqueta.PLANTEL
        Label7.Text = Config.Etiqueta.PROFESOR
        Label8.Text = Config.Etiqueta.PROFESOR
        Label9.Text = Config.Etiqueta.PLANTEL
        Label10.Text = Config.Etiqueta.GRUPO
        Label11.Text = Config.Etiqueta.ASIGNATURA
        Label12.Text = Config.Etiqueta.GRUPO
        Label13.Text = Config.Etiqueta.PROFESOR
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub DDLprofesor_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLprofesor.DataBound
        DDLprofesor.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.PROFESOR, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija la " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub BtnInsertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnInsertar.Click
        msgError.hide()

        Try
            SDSprogramacion.InsertCommand = "SET dateformat dmy; INSERT INTO Programacion (IdAsignatura,IdProfesor,IdCicloEscolar,IdGrupo, FechaModif, Modifico) VALUES (" + DDLasignatura.SelectedValue + "," + DDLprofesor.SelectedValue + "," + DDLcicloescolar.SelectedValue + "," + DDLgrupo.SelectedValue + ", getdate(), '" & User.Identity.Name & "')"
            SDSprogramacion.Insert()
            GVprogramacion.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEliminar.Click
        msgError.hide()

        Try
            SDSprogramacion.DeleteCommand = "Delete from Programacion WHERE IdProgramacion = " + GVprogramacion.SelectedRow.Cells(1).Text
            SDSprogramacion.Delete()
            GVprogramacion.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVprogramacion_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVprogramacion.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(4).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA

            LnkHeaderText = e.Row.Cells(6).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.GRUPO

            LnkHeaderText = e.Row.Cells(7).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.GRADO
        End If
    End Sub

    Protected Sub GVprogramacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVprogramacion.SelectedIndexChanged
        msgInfo.show("Profesor " + GVprogramacion.SelectedRow.Cells(3).Text &
            " " + GVprogramacion.SelectedRow.Cells(2).Text + " asignado al grupo " + _
            GVprogramacion.SelectedRow.Cells(6).Text + " impartiendo " &
            Config.Etiqueta.ARTDET_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA &
            " de " + GVprogramacion.SelectedRow.Cells(4).Text + ":")
        PnlCargar.Visible = True

        'Checo si ya se le había cargado un archivo de valoración del profesor
        'If Trim(GVprogramacion.SelectedRow.Cells(8).Text).Length > 4 Then
        If Not (GVprogramacion.SelectedRow.Cells(8).Text = "&nbsp;") Then
            'ALGORITMO PARA CARGAR ARCHIVO
            Dim ruta As String
      ruta = Config.Global.rutaCargas
      ruta = ruta + "valoraciones\"
      HLdocumento.Text = Trim(GVprogramacion.SelectedRow.Cells(8).Text)
      HLdocumento.NavigateUrl = Config.Global.urlCargas & "valoraciones/" + Trim(GVprogramacion.SelectedRow.Cells(8).Text)
    Else
      HLdocumento.Text = ""
      HLdocumento.NavigateUrl = ""
    End If

  End Sub

  Protected Sub GVprogramacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVprogramacion.DataBound
    msgInfo.hide() 'Cada que se mueva el grid se inicialice el profesor seleccionado
    If GVprogramacion.Rows.Count > 0 Then
      BtnEliminar.Visible = True
    Else
      BtnEliminar.Visible = False
      PnlCargar.Visible = False
    End If
  End Sub

  Protected Sub BtnCargar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCargar.Click
    msgError.hide()

    'Si ya existe archivo cargado, primero lo elimino y luego lo vuelvo a subir
    'VERIFICO SI LA ACTIVIDAD CONTIENE ARCHIVO PARA SUBIR:
    If (RutaArchivo.HasFile) Then 'El atributo .HasFile compara si se indico un archivo
      Try

        'ALGORITMO PARA CARGAR ARCHIVO
        Dim ruta As String
        ruta = Config.Global.rutaCargas
        ruta = ruta + "valoraciones\"

        'Verifico si ya existe el directorio de valoraciones, sino lo creo
        If Not (Directory.Exists(ruta)) Then
          Dim directorio As DirectoryInfo = Directory.CreateDirectory(ruta)
        End If

        'Lo siguiente es por si ya había subido archivo, lo borre antes de volver a subir uno nuevo.
        If Trim(HLdocumento.Text).Length > 0 Then
          Dim MiArchivo As FileInfo = New FileInfo(ruta & Trim(HLdocumento.Text))
          MiArchivo.Delete()
          HLdocumento.Text = ""
          HLdocumento.NavigateUrl = ""
        End If

        'Ahora inserto el archivo
        Dim MiArchivo2 As FileInfo = New FileInfo(ruta & RutaArchivo.FileName)
        If MiArchivo2.Exists Then
          'Avisa que ya existe, NO LO SUBE pero sí almacena el registro
          msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "YA EXISTE UN ARCHIVO CON ESE NOMBRE EN EL ALMACEN DE ARCHIVOS, CAMBIE EL NOMBRE DE SU ARCHIVO E INTENTE CARGARLO NUEVAMENTE, POR FAVOR.")
        Else
          RutaArchivo.SaveAs(ruta & RutaArchivo.FileName)
          'LblDocumento.Text = RutaArchivo.FileName
          HLdocumento.Text = RutaArchivo.FileName
          HLdocumento.NavigateUrl = Config.Global.urlCargas & "valoraciones/" + RutaArchivo.FileName
          SDSprogramacion.UpdateCommand = "SET dateformat dmy; UPDATE Programacion SET ArchivoValoracion = '" + RutaArchivo.FileName + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdProgramacion = " + GVprogramacion.SelectedRow.Cells(1).Text
          SDSprogramacion.Update()
        End If
      Catch ex As Exception
        Utils.LogManager.ExceptionLog_InsertEntry(ex)
        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      End Try
    Else
      msgError.show("Seleccione el archivo que va a cargar.")
    End If
  End Sub

  Protected Sub BtnQuitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnQuitar.Click
    msgError.hide()

    'VERIFICO SI LA ACTIVIDAD CONTIENE ARCHIVO PARA SUBIR:
    Try
      If Trim(HLdocumento.Text).Length > 0 Then

        Dim ruta As String
        ruta = Config.Global.rutaCargas
        ruta = ruta + "valoraciones\"
        'Borro el archivo:
        Dim MiArchivo As FileInfo = New FileInfo(ruta & Trim(HLdocumento.Text))
        MiArchivo.Delete()
        HLdocumento.Text = ""
        HLdocumento.NavigateUrl = ""

        SDSprogramacion.UpdateCommand = "SET dateformat dmy; UPDATE Programacion SET ArchivoValoracion = '', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdProgramacion = " + GVprogramacion.SelectedRow.Cells(1).Text
        SDSprogramacion.Update()

        msgSuccess.show("'Exito", "El archivo ha sido eliminado.")
      Else
        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "No hay ningún archivo cargado para eliminar.")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

    Protected Sub DDLplantelgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantelgrupo.DataBound
        DDLplantelgrupo.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLplantel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.SelectedIndexChanged
        DDLplantelgrupo.SelectedIndex = DDLplantel.SelectedIndex
    End Sub
End Class
