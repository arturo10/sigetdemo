﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="Mantenimientos.aspx.vb" 
    Inherits="superadministrador_Mantenimientos"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .separatorLine {
            text-align: left;
            min-height: 1em;
        }
        .optionsLeft {
            text-align: right;
        }
        .optionsRight {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>
        Bloqueos para Mantenimiento
    </h1>
    Permite asignar intervalos de bloqueo de acceso por día para todos los usuarios.
    <br /><br /><i>Esta opción sólo debe usarse por el área de sistemas.</i>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <div id="hiddenScripts">
    </div>
    <table>
        <tr>
            <td colspan="2">
                <p>
                    <b>ADVERTENCIA: </b>Un bloqueo por mantenimiento impide el acceso a TODOS los usuarios 
                    (incluido el administrador) para permitir el mantenimiento del sistema.
                    La única manera de desbloquearlo es esperando o desde el sistema mismo.
                </p>
                <p>
                    Si lo que se busca es impedir el acceso a un perfil específico en determinado horario, 
                    busque la opción desde 
                    <br /><b>Menú de Administrador > Accesos > Bloqueos de Acceso</b>.
                </p>
            </td>
        </tr>
        <tr>
            <td class="optionsLeft" style="width: 300px;">
                Día:
            </td>
            <td class="optionsRight">
                <asp:DropDownList ID="DDLDia" runat="server" >
                    <asp:ListItem Value="Lunes" Selected="True">Lunes</asp:ListItem>
                    <asp:ListItem Value="Martes">Martes</asp:ListItem>
                    <asp:ListItem Value="Miercoles">Miércoles</asp:ListItem>
                    <asp:ListItem Value="Jueves">Jueves</asp:ListItem>
                    <asp:ListItem Value="Viernes">Viernes</asp:ListItem>
                    <asp:ListItem Value="Sabado">Sabado</asp:ListItem>
                    <asp:ListItem Value="Domingo">Domingo</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                Los Horarios deben incluir horas (00-23) y minutos (00-59) en formato militar, ej 17:30.
            </td>
        </tr>
        <tr>
            <td class="optionsLeft">
                Hora de Inicio:
            </td>
            <td class="optionsRight">
                <asp:DropDownList ID="InicioHora" runat="server">
                    <asp:ListItem Selected="True">00</asp:ListItem>
                    <asp:ListItem>01</asp:ListItem>
                    <asp:ListItem>02</asp:ListItem>
                    <asp:ListItem>03</asp:ListItem>
                    <asp:ListItem>04</asp:ListItem>
                    <asp:ListItem>05</asp:ListItem>
                    <asp:ListItem>06</asp:ListItem>
                    <asp:ListItem>07</asp:ListItem>
                    <asp:ListItem>08</asp:ListItem>
                    <asp:ListItem>09</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                </asp:DropDownList>:
                <asp:DropDownList ID="InicioMinuto" runat="server">
                    <asp:ListItem Selected="True">00</asp:ListItem>
                    <asp:ListItem>05</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>35</asp:ListItem>
                    <asp:ListItem>40</asp:ListItem>
                    <asp:ListItem>45</asp:ListItem>
                    <asp:ListItem>50</asp:ListItem>
                    <asp:ListItem>55</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="optionsLeft">
                Hora de Fin:
            </td>
            <td class="optionsRight">
                <asp:DropDownList ID="FinHora" runat="server">
                    <asp:ListItem Selected="True">00</asp:ListItem>
                    <asp:ListItem>01</asp:ListItem>
                    <asp:ListItem>02</asp:ListItem>
                    <asp:ListItem>03</asp:ListItem>
                    <asp:ListItem>04</asp:ListItem>
                    <asp:ListItem>05</asp:ListItem>
                    <asp:ListItem>06</asp:ListItem>
                    <asp:ListItem>07</asp:ListItem>
                    <asp:ListItem>08</asp:ListItem>
                    <asp:ListItem>09</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="FinMinuto" runat="server">
                    <asp:ListItem Selected="True">00</asp:ListItem>
                    <asp:ListItem>05</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>35</asp:ListItem>
                    <asp:ListItem>40</asp:ListItem>
                    <asp:ListItem>45</asp:ListItem>
                    <asp:ListItem>50</asp:ListItem>
                    <asp:ListItem>55</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                <asp:Button ID="Insertar" runat="server" 
                    Text="Insertar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
                <asp:Button ID="Eliminar" runat="server" 
                    Text="Eliminar"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GVBloqueos" runat="server"
                    AllowSorting="True"
                    AutoGenerateColumns="True"
                    AutoGenerateSelectButton="True"
                    Caption="<h3>Bloqueos Establecidos</h3>"
                    CellPadding="3"
                    ForeColor="Black"
                    GridLines="Vertical"
                    Height="17px"
                    Style="font-size: x-small; text-align: left;"
                    Width="876px"
                    BackColor="White"
                    BorderColor="#999999"
                    BorderStyle="Solid"
                    BorderWidth="1px">
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <p>
                    Si un usuario está activo cuando llegue una hora de bloqueo, el sistema cierra su sesión sólo cuando ingrese al menú principal de su perfil, y si éste es alumno también cuando ingrese al menú de actividades pendientes, o intente comenzar una actividad.</p>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="separatorLine" colspan="2">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>