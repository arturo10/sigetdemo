﻿Imports Siget

Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Partial Class superadministrador_RegistroProfesor
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GVprofesores.Caption = "<h3>" &
            Config.Etiqueta.PROFESORES &
            " capturad" & Config.Etiqueta.LETRA_PROFESOR & "s</h3>"

        Label1.Text = Config.Etiqueta.PROFESORES
        Label2.Text = Config.Etiqueta.INSTITUCION
        Label3.Text = Config.Etiqueta.PLANTEL
        Label4.Text = Config.Etiqueta.PROFESOR
        Label5.Text = Config.Etiqueta.PROFESOR
        Label6.Text = Config.Etiqueta.PROFESOR
        Label7.Text = Config.Etiqueta.PROFESORES
    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        Try
            If Trim(TBlogin.Text).Length > 0 Then
                'ESTE ALGORITMO NO AFECTA LAS TABLAS DE SEGURIDAD DE USUARIOS A MENOS QUE SE SELECCIONE REGISTRAR EL USUARIO
                'Inserto el login y password (el email que aqui pongo puede o no coincidir con el que pondrá el alumno
                'al momento de registrarse
                SDSusuarios.InsertCommand = "SET dateformat dmy; INSERT INTO Usuario(Login,Password,PerfilASP,Email, FechaModif, Modifico) VALUES('" + Trim(TBlogin.Text) + "','" + Trim(TBpassword.Text) + "','Profesor','" + Trim(TBemail.Text) + "', getdate(), '" & User.Identity.Name & "')"
                SDSusuarios.Insert()

                'Inserto el Profesor pero primero obtengo el IdUsuario recién ingresado
                Dim strConexion As String
                'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                Dim objConexion As New SqlConnection(strConexion)
                Dim miComando As SqlCommand
                Dim misRegistros As SqlDataReader
                objConexion.Open()
                miComando = objConexion.CreateCommand
                'Obtengo el ID del usuario recien creado para usarlo en la tabla Profesor
                miComando.CommandText = "SELECT * FROM USUARIO WHERE Login = '" + Trim(TBlogin.Text) + "'"
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read()
                Dim IdUsuario = misRegistros.Item("IdUsuario").ToString
                misRegistros.Close()
                objConexion.Close()
                If Trim(TBingreso.Text) = "" Then
                    SDSprofesores.InsertCommand = "SET dateformat dmy; INSERT INTO Profesor (IdPlantel,IdUsuario,Clave,Nombre,Apellidos,RFC,Email,Estatus, FechaModif, Modifico) VALUES (" + DDLplantel.SelectedValue + "," + IdUsuario + ",'" + TBclave.Text + "','" + TBnombre.Text + "','" + TBapellidos.Text + "','" + TBrfc.Text + "','" + TBemail.Text + "','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
                Else
                    SDSprofesores.InsertCommand = "SET dateformat dmy; INSERT INTO Profesor (IdPlantel,IdUsuario,Clave,Nombre,Apellidos,RFC,Email,Estatus,FechaIngreso, FechaModif, Modifico) VALUES (" + DDLplantel.SelectedValue + "," + IdUsuario + ",'" + TBclave.Text + "','" + TBnombre.Text + "','" + TBapellidos.Text + "','" + TBrfc.Text + "','" + TBemail.Text + "','" + DDLestatus.SelectedValue + "','" + TBingreso.Text + "', getdate(), '" & User.Identity.Name & "')"
                End If
                SDSprofesores.Insert()

                If CBregistrarme.Checked Then 'Aqui registro al alumno en la base de datos de seguridad
                    'Creo el Usuario
                    Dim status As MembershipCreateStatus

                    ' 17/10/2013 se deseleccionó requiresQuestionAndAnswer="false"  de web config, no es necesario
                    'Dim passwordQuestion As String = ""
                    'Dim passwordAnswer As String = ""
                    'If Membership.RequiresQuestionAndAnswer Then
                    '    passwordQuestion = "Empresa desarrolladora de " & Config.Etiqueta.SISTEMA_CORTO
                    '    passwordAnswer = "Integrant"
                    'End If

                    Try
                        Dim newUser As MembershipUser = Membership.CreateUser(Trim(TBlogin.Text), Trim(TBpassword.Text), _
                                                                       Trim(TBemail.Text), Nothing, _
                                                                       Nothing, True, status)
                        If newUser Is Nothing Then
                            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), GetErrorMessage(status))
                        Else
                            Roles.AddUserToRole(Trim(TBlogin.Text), "Profesor")
                        End If
                    Catch ex As MembershipCreateUserException
                        Utils.LogManager.ExceptionLog_InsertEntry(ex)
                        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
                    End Try
                End If

                msgError.hide()
                Mensaje.Text = "El profesor ha sido creado.<BR>"
                If CBregistrarme.Checked Then
                    Mensaje.Text = Mensaje.Text + "La cuenta de " &
                        Config.Etiqueta.ARTDET_PROFESOR & " " & Config.Etiqueta.PROFESOR &
                        " ya ha sido registrada en la base de datos de seguridad"
                Else
                    Mensaje.Text = Mensaje.Text &
                        Config.Etiqueta.ARTDET_PROFESOR & " " & Config.Etiqueta.PROFESOR &
                        " deberá registrarse con su Matrícula y Login para activar su cuenta. "
                End If
            Else
                msgError.show("Debe proporcionar un nombre de usuario (login) para " &
                    Config.Etiqueta.ARTDET_PROFESOR & " " & Config.Etiqueta.PROFESOR & ".")
                TBlogin.Focus()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            Mensaje.Text = "" 'Limpio primero por si habia un mensaje anterior no se pegue al de ERROR
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Public Function GetErrorMessage(ByVal status As MembershipCreateStatus) As String
        Select Case status
            Case MembershipCreateStatus.DuplicateUserName
                Return "El usuario ya existe, elija otro usuario."
            Case MembershipCreateStatus.DuplicateEmail
                Return "Ya existe un usuario para ese email, por favor escriba un email diferente."
            Case MembershipCreateStatus.InvalidPassword
                Return "El password es inválido, pruebe con otro."
            Case MembershipCreateStatus.InvalidEmail
                Return "El formato del email es inválido, por favor revíselo."
            Case MembershipCreateStatus.InvalidAnswer
                Return "La respuesta secreta es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidQuestion
                Return "La pregunta de recuperación de password es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidUserName
                Return "El nombre de usuario es inválido, corríjalo."
            Case MembershipCreateStatus.ProviderError
                Return "La autentificación falló, contacte al administrador del sistema."
            Case MembershipCreateStatus.UserRejected
                Return "La creación del usuario ha sido cancelada, consulte al administrador."
            Case Else
                Return "Ha ocurrido un error desconocido, verifique o contacte al administrador."
        End Select
    End Function

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_INSTITUCION & " " & Config.Etiqueta.INSTITUCION, 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click
        TBclave.Text = ""
        TBnombre.Text = ""
        TBapellidos.Text = ""
        TBrfc.Text = ""
        TBemail.Text = ""
        TBlogin.Text = ""
        TBpassword.Text = ""
        Mensaje.Text = "Capture los datos"
    End Sub

    Protected Sub GVprofesores_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVprofesores.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(0).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.PROFESOR
        End If
    End Sub
End Class
