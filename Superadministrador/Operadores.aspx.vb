﻿Imports Siget

Imports System.Web.Security.Membership
Imports System.Data.SqlClient
Imports System.Data

Partial Class superadministrador_Operadores
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString

        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        miComando = objConexion.CreateCommand
        miComando.CommandText = "select Count(IdOperador) as Total from Operador"
        objConexion.Open() 'Abro la conexion
        misRegistros = miComando.ExecuteReader()
        misRegistros.Read()
        If misRegistros.Item("Total") = 0 Then
            'DVcoordinadores.DefaultMode = DetailsViewMode.Insert LO HABÍA PUESTO ASI PERO LUEGO NO SE PUEDE QUITAR ESE MODO
            SDSoperadores.InsertCommand = "SET dateformat dmy; INSERT INTO Operador(Nombre, Apellidos, Estatus) values('DISPONIBLE PARA MODIFICAR','DISPONIBLE PARA MODIFICAR','Activo')"
            SDSoperadores.Insert()
        End If
        misRegistros.Close()
        objConexion.Close()
    End Sub

    Protected Sub DVcoordinadores_ItemDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeleteEventArgs) Handles DVoperadores.ItemDeleting
        'Antes de que se borre el registro en la Tabla Coordinador, busco los datos del Usuario:
        Dim IdOperador As String
        IdOperador = Trim(DVoperadores.Rows(0).Cells(1).Text)
        Session("Login") = ""
        Session("IdUsuario") = ""
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        miComando = objConexion.CreateCommand
        Try
            'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
            miComando.CommandText = "select IdUsuario,Login from Usuario where IdUsuario in (select IdUsuario from Operador where IdOperador = " + IdOperador + ")"
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            If misRegistros.Read() Then
                Session("IdUsuario") = misRegistros.Item("IdUsuario").ToString
                Session("Login") = Trim(misRegistros.Item("Login"))
            End If
            misRegistros.Close()
            objConexion.Close()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DVcoordinadores_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles DVoperadores.ItemDeleted
        Try
            'UNA VEZ QUE SE BORRÓ EL REGISTRO DEL COORDINADOR, YA PUEDO BORRAR EL USUARIO Y LO DEMÁS
            If Trim(Session("Login")) <> "" Then
                SDSoperadores.DeleteCommand = "DELETE FROM USUARIO where IdUsuario = " + Session("IdUsuario")
                SDSoperadores.Delete()
                'Elimino Login de la tabla de seguridad
                Membership.DeleteUser(Session("Login"))

                msgError.hide()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

End Class
