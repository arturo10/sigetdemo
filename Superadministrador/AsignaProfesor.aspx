﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="AsignaProfesor.aspx.vb"
    Inherits="superadministrador_AsignaProfesor"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style11 {
            height: 41px;
            text-align: left;
        }

        .style13 {
        }

        .style15 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style16 {
            width: 321px;
        }

        .style18 {
        }

        .style19 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style21 {
            text-align: left;
            width: 361px;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 361px;
            text-align: right;
        }

        .style24 {
            width: 361px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Asignación de
        <asp:Label ID="Label11" runat="server" Text="[PROFESORES]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style11" colspan="3">
                <asp:Label ID="Mensaje" runat="server" CssClass="titulo"
                    Style="font-family: Arial, Helvetica, sans-serif">Capture los datos</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style23">
                <asp:Label ID="Label1" runat="server" Text="[INSTITUCION]" /></td>
            <td colspan="2" style="text-align:left;">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                    DataValueField="IdInstitucion" CssClass="style15" Height="22px"
                    Width="320px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style23">
                <asp:Label ID="Label2" runat="server" Text="[PLANTEL]" />
                al que pertenece el 
								<asp:Label ID="Label3" runat="server" Text="[PROFESOR]" />
            </td>
            <td colspan="2" style="text-align:left;">
                <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                    DataValueField="IdPlantel" CssClass="style15" Height="22px" Width="320px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style19" colspan="3">
                <asp:GridView ID="GVprofesores" runat="server" 
                    AllowPaging="True"
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 
                    DataKeyNames="IdProfesor"
                    DataSourceID="SDSprofesores"
                    Caption="Seleccione al PROFESOR que le asignará la MATERIA y el GRUPO"
                    PageSize="15"

                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small; text-align: left;"
                    BackColor="White" 
                    BorderColor="#999999" 
                    BorderStyle="Solid" 
                    BorderWidth="1px"
                    CellPadding="3" 
                    GridLines="Vertical" 
                    Width="874px" 
                    ForeColor="Black">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="IdProfesor" HeaderText="IdProfesor"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdProfesor" />
                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                            SortExpression="IdPlantel" Visible="False" />
                        <asp:BoundField DataField="IdUsuario" HeaderText="IdUsuario"
                            SortExpression="IdUsuario" Visible="False" />
                        <asp:BoundField DataField="Clave" HeaderText="Clave" SortExpression="Clave" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre"
                            SortExpression="Nombre" />
                        <asp:BoundField DataField="Apellidos" HeaderText="Apellidos"
                            SortExpression="Apellidos" />
                        <asp:BoundField DataField="RFC" HeaderText="RFC" SortExpression="RFC" />
                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email"
                            Visible="False" />
                        <asp:BoundField DataField="FechaIngreso" HeaderText="FechaIngreso"
                            SortExpression="FechaIngreso" Visible="False" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                            SortExpression="Estatus" Visible="False" />
                        <asp:BoundField DataField="FechaModif" HeaderText="FechaModif"
                            SortExpression="FechaModif" Visible="False" />
                        <asp:BoundField DataField="Modifico" HeaderText="Modifico"
                            SortExpression="Modifico" Visible="False" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  select
	                1  id_PROFESOR
	                2  ...
	                3  
	                4  
	                5  
	                6  
	                7  
	                8  
	                9  
	                10 
	                --%>
            </td>
        </tr>
        <tr>
            <td class="style19" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td class="style23">
                <asp:Label ID="Label6" runat="server" Text="[CICLO]" /></td>
            <td colspan="2" style="text-align:left;">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    CssClass="style 15" DataSourceID="SDSciclosescolares"
                    DataTextField="Descripcion" DataValueField="IdCicloEscolar" Height="22px"
                    Width="320px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style23">
                <asp:Label ID="Label7" runat="server" Text="[NIVEL]" /></td>
            <td colspan="2" style="text-align:left;">
                <asp:DropDownList ID="DDLniveles" runat="server" AutoPostBack="True"
                    CssClass="style15" Height="22px" Width="320px" DataSourceID="SDSnivel"
                    DataTextField="Descripcion" DataValueField="IdNivel">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style23">
                <asp:Label ID="Label8" runat="server" Text="[GRADO]" /></td>
            <td colspan="2" style="text-align:left;">
                <asp:DropDownList ID="DDLgrados" runat="server" AutoPostBack="True"
                    CssClass="style15" Height="22px" Width="320px" DataSourceID="SDSgrado"
                    DataTextField="Descripcion" DataValueField="IdGrado">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style23">
                <asp:Label ID="Label9" runat="server" Text="[ASIGNATURA]" /></td>
            <td colspan="2" style="text-align:left;">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    CssClass="style15" DataSourceID="SDSasignaturas" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Height="22px" Width="320px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style23">
                <asp:Label ID="Label4" runat="server" Text="[PLANTEL]" />
                donde se encuentra el 
								<asp:Label ID="Label5" runat="server" Text="[GRUPO]" />
            </td>
            <td colspan="2" style="text-align:left;">
                <asp:DropDownList ID="DDLplantelgrupo" runat="server" AutoPostBack="True"
                    DataSourceID="SDSplantelesgrupo" DataTextField="Descripcion"
                    DataValueField="IdPlantel" CssClass="style15" Height="22px"
                    Width="320px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style23">
                <asp:Label ID="Label10" runat="server" Text="[GRUPO]" /></td>
            <td colspan="2" style="text-align:left;">
                <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                    CssClass="style15" DataSourceID="SDSgrupos" DataTextField="Descripcion"
                    DataValueField="IdGrupo" Height="22px" Width="235px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style21">&nbsp;</td>
            <td colspan="2">
                <asp:Button ID="Asignar" runat="server" Text="Asignar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td class="style18" colspan="3">
                <asp:GridView ID="GVgrupos" runat="server" 
                    AllowPaging="True"
                    AutoGenerateColumns="False" 
                    DataKeyNames="IdProgramacion"
                    DataSourceID="SDSgruposasignados"
                    Caption="ASIGNATURAS y GRUPOS Asignados al PROFESOR Seleccionado"
                    PageSize="15"

                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small; text-align: left;"
                    BackColor="White" 
                    BorderColor="#999999" 
                    BorderStyle="Solid" 
                    BorderWidth="1px"
                    CellPadding="3" 
                    GridLines="Vertical" 
                    Width="872px" 
                    ForeColor="Black">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="IdProgramacion" HeaderText="Id Asignado"
                            InsertVisible="False" SortExpression="IdProgramacion" ReadOnly="True" />
                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo"
                            SortExpression="IdGrupo" InsertVisible="False" ReadOnly="True" />
                        <asp:BoundField DataField="Grupo" HeaderText="Grupo"
                            SortExpression="Grupo" />
                        <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"
                            SortExpression="Asignatura" />
                        <asp:BoundField DataField="Grado" HeaderText="Grado" SortExpression="Grado" />
                        <asp:BoundField DataField="Plantel" HeaderText="Plantel"
                            SortExpression="Plantel" />
                        <asp:BoundField DataField="CicloEscolar" HeaderText="CicloEscolar"
                            SortExpression="CicloEscolar" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
                <%-- sort NO - prerender
	                0  select
	                1  id asignado
	                2  id_GRUPO
	                3  GRUPO
	                4  ASIGNATURA
	                5  GRADO
	                6  PLANTEL
	                7  CICLO
	                --%>
            </td>
        </tr>
        <tr>
            <td class="style13">&nbsp;</td>
            <td class="style13" colspan="2">
                <asp:Button ID="Desasignar" runat="server" Text="Desasignar" Visible="False"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style24">
                <asp:SqlDataSource ID="SDSInstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Institucion]"></asp:SqlDataSource>
            </td>
            <td class="style16">
                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSprofesores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Profesor] WHERE (([IdPlantel] = @IdPlantel) AND ([Estatus] = @Estatus)) ORDER BY [Apellidos], [Nombre]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style24">
                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar] ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>
            </td>
            <td class="style16">
                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdAsignatura, Descripcion
FROM Asignatura A
where IdGrado = @IdGrado
ORDER BY Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrados" Name="IdGrado"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * 
FROM Grupo G
JOIN Asignatura A
ON G.IdGrado = A.IdGrado and A.IdAsignatura = @IdAsignatura
and G.IdPlantel = @IdPlantel and G.IdCicloEscolar = @IdCicloEscolar">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLplantelgrupo" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style24">
                <asp:SqlDataSource ID="SDSnivel" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdNivel, Descripcion 
FROM Nivel WHERE Estatus = 'Activo'
ORDER BY Descripcion"></asp:SqlDataSource>
            </td>
            <td class="style16">
                <asp:SqlDataSource ID="SDSgruposasignados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.IdProgramacion, G.IdGrupo, G.Descripcion Grupo, A.Descripcion Asignatura, Gr.Descripcion Grado, Pl.Descripcion Plantel,C.Descripcion CicloEscolar
from Grupo G,Programacion P, Plantel Pl, CicloEscolar C, Asignatura A, Grado Gr
where P.IdProfesor = @IdProfesor and G.IdGrupo = P.IdGrupo and C.IdCicloEscolar = P.IdCicloEscolar and Pl.IdPlantel = G.IdPlantel and A.IdAsignatura = P.IdAsignatura and Gr.IdGrado = G.IdGrado and Gr.Estatus = 'Activo'
order by C.IdCicloEscolar, Pl.Descripcion, A.Descripcion, Gr.Descripcion, G.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVprofesores" Name="IdProfesor"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSprogramacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Programacion]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style24">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td class="style16">
                <asp:SqlDataSource ID="SDSgrado" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLniveles" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSplantelesgrupo" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

