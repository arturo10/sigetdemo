﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="Grupos.aspx.vb" 
    Inherits="superadministrador_Grupos" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            width: 98%;
        }

        .style21 {
            width: 380px;
        }

        .style19 {
        }

        .style20 {
            height: 8px;
        }

        .style18 {
            height: 3px;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style24 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            color: #000000;
            height: 8px;
            text-align: right;
        }

        .style37 {
            height: 1px;
        }

        .style38 {
            height: 36px;
        }

        .style39 {
            width: 380px;
            height: 36px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Administración de 
								<asp:Label ID="Label1" runat="server" Text="[GRUPOS]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td colspan="3">
                <table class="style11">
                    <tr>
                        <td colspan="2" style="text-align: left" class="style38">
                            <asp:Label ID="Mensaje" runat="server" CssClass="style16"
                                Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #000066; text-align: left">Capture los datos</asp:Label>
                        </td>
                        <td style="text-align: right" class="style39"></td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24">
                            <asp:Label ID="Label2" runat="server" Text="[INSTITUCION]" />
                        </td>
                        <td class="style19" colspan="2" style="text-align: left;">
                            <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                DataValueField="IdInstitucion" Height="22px" Width="400px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24">
                            <asp:Label ID="Label3" runat="server" Text="[PLANTEL]" />
                        </td>
                        <td class="style19" colspan="2" style="text-align: left;">
                            <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                DataValueField="IdPlantel" Height="22px" Width="400px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24">
                            <asp:Label ID="Label4" runat="server" Text="[NIVEL]" />
                        </td>
                        <td class="style19" colspan="2" style="text-align: left;">
                            <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSniveles" DataTextField="Descripcion"
                                DataValueField="IdNivel" Height="22px" Width="400px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24">
                            <asp:Label ID="Label5" runat="server" Text="[GRADO]" />
                        </td>
                        <td class="style19" colspan="2" style="text-align: left;">
                            <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                DataSourceID="SDSgrados" DataTextField="Descripcion"
                                DataValueField="IdGrado" Height="22px" Width="400px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24">
                            <asp:Label ID="Label6" runat="server" Text="[CICLO]" />
                        </td>
                        <td class="style19" colspan="2" style="text-align: left;">
                            <asp:DropDownList ID="DDLcicloescolar" runat="server"
                                DataSourceID="SDSciclosescolares" DataTextField="Ciclo"
                                DataValueField="IdCicloEscolar" Width="400px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24">Descripción del 
							<asp:Label ID="Label7" runat="server" Text="[GRUPO]" />
                        </td>
                        <td class="style19" colspan="2" style="text-align: left;">
                            <asp:TextBox ID="TBdescripcion" runat="server" MaxLength="80" Width="245px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24">
                            Clave para el 
							<asp:Label ID="Label8" runat="server" Text="[GRUPO]" />
                        </td>
                        <td class="style19" colspan="2" style="text-align: left;">
                            <asp:TextBox ID="TBClave" runat="server" MaxLength="15" Width="145px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style17">&nbsp;</td>
                        <td class="style19" colspan="2">
                            <asp:GridView ID="GVgruposcreados" runat="server" 
                                AllowPaging="True"
                                AutoGenerateColumns="False" 
                                DataSourceID="SDSgrupos" 
                                AllowSorting="True"
                                Caption="<h3>GRUPOS creados</h3>"
                                DataKeyNames="IdGrupo,IdCicloEscolar" 

                                BackColor="White" 
                                BorderColor="#999999"
                                BorderStyle="Solid" 
                                BorderWidth="1px" 
                                CellPadding="3"
                                GridLines="Vertical"
                                Style="font-size: small; margin-left: 0px;" 
                                Width="691px" 
                                ForeColor="Black">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True">
                                        <HeaderStyle Width="90px" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="IdGrupo" HeaderText="Id_[Grupo]"
                                        SortExpression="IdGrupo" InsertVisible="False" ReadOnly="True" />
                                    <asp:BoundField DataField="IdGrado" HeaderText="IdGrado"
                                        SortExpression="IdGrado" Visible="False" />
                                    <asp:BoundField DataField="Grupo" HeaderText="[Grupo]"
                                        SortExpression="Grupo" />
                                    <asp:BoundField DataField="Clave" HeaderText="Clave" SortExpression="Clave" />
                                    <asp:BoundField DataField="Grado" HeaderText="[Grado]" SortExpression="Grado" />
                                    <asp:BoundField DataField="IdCicloEscolar" HeaderText="IdCicloEscolar"
                                        InsertVisible="False" SortExpression="IdCicloEscolar" Visible="False" />
                                    <asp:BoundField DataField="Ciclo Escolar" HeaderText="[Ciclo]"
                                        SortExpression="Ciclo Escolar" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                            <%-- sort YES - rowdatabound
	                            0  select
	                            1  id_GRUPO
	                            2  idgrado
	                            3  GRUPO
	                            4  clave
	                            5  GRADO
	                            6  CICLO
	                            7  
	                            8  
	                            9  
	                            10 
	                            --%>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style17">&nbsp;</td>
                        <td class="style19" colspan="2">
                            <asp:Button ID="Insertar" runat="server" Text="Insertar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
							<asp:Button ID="Cancelar" runat="server" Text="Cancelar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                            &nbsp;
							<asp:Button ID="Actualizar" runat="server" Text="Actualizar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
							<asp:Button ID="Eliminar" runat="server" Text="Eliminar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style18">
                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Institucion]"></asp:SqlDataSource>
            </td>
            <td class="style18">
                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td class="style18">
                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select N.IdNivel, N.Descripcion
from Nivel N, Escuela E, Plantel P
where N.IdNivel = E.IdNivel and P.IdPlantel = E.IdPlantel
      and P.IdPlantel = @IdPlantel
Order by Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style37">
                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdCicloEscolar, Descripcion + ' (' + Cast(FechaInicio as varchar(12)) + ' - ' + Cast(FechaFin as varchar(12)) + ')' Ciclo
FROM CicloEscolar
WHERE (Estatus = 'Activo')
order by Descripcion"></asp:SqlDataSource>
            </td>
            <td class="style37">
                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td class="style37">
                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT G.IdGrupo, G.IdGrado, G.Descripcion Grupo, G.Clave, Gr.Descripcion Grado, C.IdCicloEscolar, C.Descripcion as 'Ciclo Escolar'
FROM Grupo G
JOIN CicloEscolar C on C.IdCicloEscolar = G.IdCicloEscolar and G.IdCicloEscolar = @IdCicloEscolar
JOIN Grado Gr
on Gr.IdGrado = G.IdGrado and G.IdPlantel = @IdPlantel and G.IdGrado = @IdGrado
order by Gr.Descripcion, G.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

