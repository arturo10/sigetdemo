﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="Profesores.aspx.vb" 
    Inherits="superadministrador_Profesores" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            width: 440px;
            height: 165px;
        }

        .style29 {
            text-align: left;
            height: 24px;
            margin-left: 80px;
        }

        .style28 {
            width: 204px;
            height: 14px;
            text-align: right;
            font-size: x-small;
        }

        .style14 {
            text-align: left;
        }

        .style25 {
            text-align: center;
        }

        .style23 {
            font-size: x-small;
        }

        .style24 {
            text-align: left;
        }

        .style12 {
            width: 440px;
            height: 76px;
        }

        .style31 {
            width: 440px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style34 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            color: #000000;
            height: 8px;
            text-align: right;
        }

        .style35 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            color: #FFFFFF;
            height: 8px;
            text-align: right;
        }

        .style36 {
            width: 440px;
        }

        .style37 {
            text-align: right;
            height: 24px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Administración de 
								<asp:Label ID="Label1" runat="server" Text="[PROFESORES]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="estandar" style="width: 871px; height: 450px;">
        <tr>
            <td class="style11">
                <table>
                    <tr>
                        <td class="style29" colspan="4">
                            <span class="style28">* En Eliminar borra 
																<asp:Label ID="Label2" runat="server" Text="[PROFESOR]" />
                                , Usuario, Programacion y en 
                            Seguridad</span></td>
                        <td class="style29">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style37" colspan="4">&nbsp;</td>
                        <td class="style29">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style14" colspan="4">
                            <asp:Label ID="Mensaje" runat="server" CssClass="titulo"
                                Style="color: #000066; font-family: Arial, Helvetica, sans-serif;">Capture los datos</asp:Label>
                        </td>
                        <td class="style14">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style17" colspan="4">&nbsp;</td>
                        <td class="style17">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style37">
                            <asp:Label ID="Label3" runat="server" Text="[INSTITUCION]" />
                        </td>
                        <td class="style17" style="text-align: left;">
                            <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                DataValueField="IdInstitucion" Height="22px" Width="260px">
                            </asp:DropDownList>
                        </td>
                        <td class="style37">
                            <asp:Label ID="Label4" runat="server" Text="[PLANTEL]" />
                        </td>
                        <td class="style17" style="text-align: left;">
                            <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                DataValueField="IdPlantel" Height="22px" Width="260px">
                            </asp:DropDownList>
                        </td>
                        <td class="style17">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style37">Matrícula o Clave</td>
                        <td class="style17" style="text-align: left;">
                            <asp:TextBox ID="TBclave" runat="server" MaxLength="15"></asp:TextBox>
                        </td>
                        <td class="style37">Apellidos</td>
                        <td class="style17" style="text-align: left;">
                            <asp:TextBox ID="TBapellidos" runat="server" Width="190px" MaxLength="60"></asp:TextBox>
                        </td>
                        <td class="style17">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style37">Nombre</td>
                        <td class="style17" style="text-align: left;">
                            <asp:TextBox ID="TBnombre" runat="server" MaxLength="40"></asp:TextBox>
                        </td>
                        <td class="style35">&nbsp;</td>
                        <td class="style17">&nbsp;</td>
                        <td class="style17">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style37">RFC</td>
                        <td class="style17" style="text-align: left;">
                            <asp:TextBox ID="TBrfc" runat="server" MaxLength="13"></asp:TextBox>
                        </td>
                        <td class="style37">Email</td>
                        <td class="style17" style="text-align: left;">
                            <asp:TextBox ID="TBemail" runat="server" Width="191px" MaxLength="100"></asp:TextBox>
                        </td>
                        <td class="style17">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style37">Fecha de Ingreso 
                        </td>
                        <td class="style17" style="text-align: left;">
                            <asp:TextBox ID="TBingreso" runat="server" MaxLength="10"></asp:TextBox>
                            <span class="style23">(dd/mm/aaaa)</span></td>
                        <td class="style37">Estatus</td>
                        <td class="style17" style="text-align: left;">
                            <asp:DropDownList ID="DDLestatus" runat="server">
                                <asp:ListItem>Activo</asp:ListItem>
                                <asp:ListItem>Suspendido</asp:ListItem>
                                <asp:ListItem>Baja</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="style17">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style17" colspan="4" style="text-align: center">
                            <asp:Button ID="Insertar" runat="server" Text="Insertar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Limpiar" runat="server" Text="Limpiar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Actualizar" runat="server" Text="Actualizar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                        </td>
                        <td class="style17" style="text-align: center">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style17" colspan="4">
                            <asp:GridView ID="GVprofesores" runat="server"
                                AllowPaging="True"
                                AllowSorting="True" 
                                AutoGenerateColumns="False" 
                                DataKeyNames="IdProfesor" 
                                DataSourceID="SDSprofesores"
                                Caption="<h3>PROFESORES capturados</h3>"
                                PageSize="20"

                                BackColor="White"
                                BorderColor="#999999" 
                                BorderStyle="Solid" 
                                BorderWidth="1px" 
                                CellPadding="3"
                                GridLines="Vertical" 
                                Style="text-align: left; font-size: x-small"
                                Width="836px" 
                                ForeColor="Black">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="IdProfesor" HeaderText="IdProfesor"
                                        InsertVisible="False" ReadOnly="True" SortExpression="IdProfesor" />
                                    <asp:BoundField DataField="Clave" HeaderText="Clave"
                                        SortExpression="Clave" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre"
                                        SortExpression="Nombre" />
                                    <asp:BoundField DataField="Apellidos" HeaderText="Apellidos"
                                        SortExpression="Apellidos" />
                                    <asp:BoundField DataField="RFC" HeaderText="RFC"
                                        SortExpression="RFC" />
                                    <asp:BoundField DataField="Email" HeaderText="Email"
                                        SortExpression="Email" />
                                    <asp:BoundField DataField="Login" HeaderText="Login" SortExpression="Login" />
                                    <asp:BoundField DataField="Contraseña" HeaderText="Contraseña"
                                        SortExpression="Contraseña" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                    <asp:BoundField DataField="FechaIngreso" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderText="Ingreso" SortExpression="FechaIngreso" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                            <%-- sort YES - rowdatabound
	                            0  select
	                            1  Id_PROFESOR
	                            2  ...
	                            3  
	                            4  
	                            5  
	                            6  
	                            7  
	                            8  
	                            9  
	                            10 
	                            --%>
                        </td>
                        <td class="style17">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style25" colspan="4">
                            <asp:Button ID="Eliminar" runat="server" Text="Eliminar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                        </td>
                        <td class="style25">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <uc1:msgError runat="server" ID="msgError" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style12">
                <table>
                    <tr>
                        <td>
                            <asp:SqlDataSource ID="SDSprofesores" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT P.IdProfesor,P.Clave,P.Nombre,P.Apellidos,P.RFC,P.Email,U.Login,U.Password as Contraseña, P.Estatus, P.FechaIngreso
FROM Profesor P
left join Usuario U
on (U.IdUsuario = P.IdUsuario)
WHERE (P.IdPlantel = @IdPlantel) 
order by P.Apellidos, P.Nombre">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td>
                            <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Institucion]"></asp:SqlDataSource>
                        </td>
                        <td>
                            <asp:SqlDataSource ID="SDSplanteles" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style36">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

