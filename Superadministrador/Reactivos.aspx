﻿<%@ Page Title="" 
    Language="C#" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="true" 
    CodeFile="Reactivos.aspx.cs" 
    Inherits="superadministrador_Reactivos"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .titleColumn {
            text-align: right;
        }

        .control_column {
            text-align: left;
        }

        .overlay_top {
            z-index: 10004;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" Runat="Server">

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" Runat="Server">
    <h1>
        Reactivos
    </h1>
    Permite capturar los reactivos y sus opciones de un subtema.
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table>
        <tr>
            <td class="titleColumn">
                <asp:Literal ID="Literal1" runat="server" Text="[NIVEL]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" 
                    DataTextField="Descripcion"
                    DataValueField="IdNivel" 
                    Width="285px" 
                    CssClass="wideTextbox"
                    OnDataBound="DDLnivel_DataBound">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="Nivel_ObtenLista"
                    SelectCommandType="StoredProcedure"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Literal ID="Literal2" runat="server" Text="[GRADO]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" 
                    DataTextField="Descripcion"
                    DataValueField="IdGrado" 
                    Width="285px" 
                    CssClass="wideTextbox"
                    OnDataBound="DDLgrado_DataBound">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                <asp:Literal ID="Literal3" runat="server" Text="[ASIGNATURA]" />
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSasignaturas" 
                    DataTextField="Descripcion"
                    DataValueField="IdAsignatura" 
                    Width="410px"
                    CssClass="wideTextbox"
                    OnDataBound="DDLasignatura_DataBound">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdAsignatura], [IdGrado], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                Tema
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DDLtemas" runat="server" AutoPostBack="True"
                    DataSourceID="SDStemas" 
                    DataTextField="NomTema"
                    DataValueField="IdTema" 
                    Width="410px" 
                    CssClass="wideTextbox"
                    OnDataBound="DDLtemas_DataBound">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SDStemas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdTema], [IdAsignatura], Cast([Numero] as varchar(8))+ ' ' + [Descripcion] NomTema  FROM [Tema] WHERE ([IdAsignatura] = @IdAsignatura)
order by [Numero],[Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
                Subtema
            </td>
            <td class="control_column">
                <asp:DropDownList ID="DDLsubtemas" runat="server" AutoPostBack="True"
                    DataSourceID="SDSsubtemas" 
                    DataTextField="NomSubtema"
                    DataValueField="IdSubtema" 
                    Width="410px"
                    CssClass="wideTextbox"
                    OnDataBound="DDLsubtemas_DataBound" OnSelectedIndexChanged="DDLsubtemas_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SDSsubtemas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdSubtema], [IdTema], Cast([Numero] as varchar(8)) + ' ' + [Descripcion] NomSubtema FROM [Subtema] WHERE ([IdTema] = @IdTema)
order by [Numero],[Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLtemas" Name="IdTema"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="pnlPlanteamientos" runat="server" Visible="false">
                    <table class="style23">
                        <tr>
                            <td colspan="5">
                                <uc1:msgSuccess runat="server" ID="msgSuccess" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <uc1:msgError runat="server" ID="msgError" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <uc1:msgInfo runat="server" ID="msgInfo" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <asp:LinkButton ID="btnNueva" runat="server"
                                    CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                                    <asp:Image ID="imgNueva" runat="server"
                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/add.png"
                                        CssClass="btnIcon"
                                        Height="24" />
                                    <asp:Label ID="lblNueva" runat="server">
                                        Crear
                                    </asp:Label>
                                </asp:LinkButton>
                                &nbsp;
                                <asp:LinkButton ID="btnModificar" runat="server"
                                    Visible="True" CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                                    <asp:Image ID="imgModificar" runat="server"
                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/disk.png"
                                        CssClass="btnIcon"
                                        Height="24" />
                                    <asp:Label ID="lblModificar" runat="server">
                                        Modificar
                                    </asp:Label>
                                </asp:LinkButton>
                                &nbsp;
                                <asp:LinkButton ID="btnEliminar" runat="server"
                                    Visible="True" CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                                    <asp:Image ID="imgEliminar" runat="server"
                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/delete.png"
                                        CssClass="btnIcon"
                                        Height="24" />
                                    <asp:Label ID="lblEliminar" runat="server">
                                        Eliminar
                                    </asp:Label>
                                </asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="style40" colspan="5">
                                <asp:GridView ID="GVplanteamientos" runat="server"
                                    AllowSorting="True" 
                                    AutoGenerateColumns="False" 
                                    CellPadding="3"
                                    PageSize="15"

                                    DataKeyNames="IdPlanteamiento,Libro,Autor,Editorial,EsconderTexto" 
                                    DataSourceID="SDSdatosplant"
                                    Width="890px"

                                    CssClass="dataGrid_clear_selectable"
                                    GridLines="None">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="true" ItemStyle-CssClass="selectCell"/>
                                        <asp:BoundField DataField="IdPlanteamiento" HeaderText="Id-Plant."
                                            InsertVisible="False" ReadOnly="True" SortExpression="IdPlanteamiento" />
                                        <asp:BoundField DataField="IdSubtema" HeaderText="Id-Subtema"
                                            SortExpression="IdSubtema" Visible="False" />
                                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                                            SortExpression="Consecutivo" />
                                        <asp:BoundField DataField="Redaccion" HeaderText="Redacción del planteamiento"
                                            SortExpression="Redaccion" />
                                        <asp:BoundField DataField="TipoRespuesta" HeaderText="Tipo Resp."
                                            SortExpression="TipoRespuesta" />
                                        <asp:BoundField DataField="Ponderacion" HeaderText="Ponderación"
                                            SortExpression="Ponderacion" />
                                        <asp:BoundField DataField="PorcentajeRestarResp" HeaderText="% Restar"
                                            SortExpression="PorcentajeRestarResp" />
                                        <asp:BoundField DataField="ArchivoApoyo" HeaderText="Archivo Apoyo 1"
                                            SortExpression="ArchivoApoyo" />
                                        <asp:BoundField DataField="TipoArchivoApoyo" HeaderText="Tipo de Archivo 1"
                                            SortExpression="TipoArchivoApoyo" />
                                        <asp:BoundField DataField="ArchivoApoyo2" HeaderText="Archivo Apoyo 2"
                                            SortExpression="ArchivoApoyo2" />
                                        <asp:BoundField DataField="TipoArchivoApoyo2" HeaderText="Tipo de Archivo 2"
                                            SortExpression="TipoArchivoApoyo2" />
                                        <asp:BoundField DataField="Libro" HeaderText="Libro" SortExpression="Libro"
                                            Visible="False" />
                                        <asp:BoundField DataField="Autor" HeaderText="Autor" SortExpression="Autor"
                                            Visible="False" />
                                        <asp:BoundField DataField="Editorial" HeaderText="Editorial"
                                            SortExpression="Editorial" Visible="False" />
                                        <asp:BoundField DataField="Ocultar" HeaderText="Ocultar"
                                            SortExpression="Ocultar" />
                                        <asp:BoundField DataField="Tiempo" HeaderText="Tiempo"
                                            SortExpression="Tiempo" />
                                        <asp:BoundField DataField="Minutos" DataFormatString="{0:0.0}"
                                            HeaderText="Minutos" SortExpression="Minutos">
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Permite2" HeaderText="2da. Vuelta" SortExpression="Permite2" />
                                    </Columns>
                                    <FooterStyle CssClass="footer" />
                                    <PagerStyle CssClass="pager" />
                                    <SelectedRowStyle CssClass="selected" />
                                    <HeaderStyle CssClass="header" />
                                    <AlternatingRowStyle CssClass="altrow" />
                                </asp:GridView>
                                <asp:SqlDataSource ID="SDSdatosplant" runat="server"
                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                    SelectCommand="SELECT [IdPlanteamiento], [IdSubtema], [Redaccion], [TipoRespuesta], [Ponderacion], [PorcentajeRestarResp], [ArchivoApoyo], [TipoArchivoApoyo], [ArchivoApoyo2], [TipoArchivoApoyo2], [Consecutivo], [Libro], [Autor], [Editorial], [Ocultar],[Tiempo],[Minutos],[EsconderTexto],[Permite2] FROM [Planteamiento] WHERE ([IdSubtema] = @IdSubtema)">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="DDLsubtemas" Name="IdSubtema"
                                            PropertyName="SelectedValue" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
            </td>
            <td class="control_column">
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
            </td>
            <td class="control_column">
            </td>
        </tr>
        <tr>
            <td class="titleColumn">
            </td>
            <td class="control_column">
            </td>
        </tr>
    </table>
</asp:Content>

