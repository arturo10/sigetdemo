﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="BorraRespuestas.aspx.vb"
    Inherits="superadministrador_BorraRespuestas"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style11 {
            width: 201px;
        }

        .style13 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style14 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style15 {
            width: 201px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: 700;
        }

        .style24 {
            width: 201px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Borrar Respuestas por Fecha
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style23" colspan="3">Indique el rango de las fechas de las respuestas que desea borrar:</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">&nbsp;</td>
            <td class="style13">&nbsp;</td>
            <td class="style14">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style15">&nbsp;</td>
            <td class="style24">Fecha inicial (dd/mm/aaaa):</td>
            <td class="style14" style="text-align: left;">
                <asp:TextBox ID="TBinicial" runat="server"></asp:TextBox>
                <asp:CalendarExtender ID="TBinicial_CalendarExtender" runat="server"
                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TBinicial">
                </asp:CalendarExtender>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style15">&nbsp;</td>
            <td class="style24">Fecha final (dd/mm/aaaa):</td>
            <td class="style14" style="text-align: left;">
                <asp:TextBox ID="TBfinal" runat="server"></asp:TextBox>
                <asp:CalendarExtender ID="TBfinal_CalendarExtender" runat="server"
                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TBfinal">
                </asp:CalendarExtender>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">&nbsp;</td>
            <td class="style13">
                &nbsp;</td>
            <td style="text-align: left;">
                <asp:Button ID="BtnEjecutar" runat="server"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium"
                    Text="Ejecutar" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="2">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

