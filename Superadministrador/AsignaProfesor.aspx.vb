﻿Imports Siget


Partial Class superadministrador_AsignaProfesor
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GVprofesores.Caption = "Seleccione a " &
            Config.Etiqueta.ARTDET_PROFESOR & " " & Config.Etiqueta.PROFESOR &
            " que le asignará " &
            Config.Etiqueta.ARTDET_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA &
            " y " & Config.Etiqueta.ARTDET_ALUMNO & " " & Config.Etiqueta.ALUMNO

        GVgrupos.Caption = Config.Etiqueta.ASIGNATURAS &
            " y " & Config.Etiqueta.GRUPOS &
            " Asignad" & Config.Etiqueta.LETRA_GRUPO & "s a " &
            Config.Etiqueta.ARTDET_PROFESOR & " " & Config.Etiqueta.PROFESOR &
            " Seleccionad" & Config.Etiqueta.LETRA_PROFESOR

        Label1.Text = Config.Etiqueta.INSTITUCION
        Label2.Text = Config.Etiqueta.PLANTEL
        Label3.Text = Config.Etiqueta.PROFESOR
        Label4.Text = Config.Etiqueta.PLANTEL
        Label5.Text = Config.Etiqueta.GRUPO
        Label6.Text = Config.Etiqueta.CICLO
        Label7.Text = Config.Etiqueta.NIVEL
        Label8.Text = Config.Etiqueta.GRADO
        Label9.Text = Config.Etiqueta.ASIGNATURA
        Label10.Text = Config.Etiqueta.GRUPO
        Label11.Text = Config.Etiqueta.PROFESORES
    End Sub

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem("---Elija la " & Config.Etiqueta.INSTITUCION, 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija la " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub Asignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Asignar.Click
        Try
            SDSprogramacion.InsertCommand = "SET dateformat dmy; INSERT INTO Programacion (IdAsignatura,IdProfesor,IdCicloEscolar,IdGrupo,FechaModif, Modifico) VALUES (" + DDLasignatura.SelectedValue + "," + GVprofesores.SelectedValue.ToString + "," + DDLcicloescolar.SelectedValue + "," + DDLgrupo.SelectedValue + ",getdate(), '" & User.Identity.Name & "')"
            SDSprogramacion.Insert()
            GVgrupos.DataBind()
            msgError.hide()
            Mensaje.Text = "El registro ha sido guardado"
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            Mensaje.Text = ""
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Desasignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Desasignar.Click
        Try

            SDSprogramacion.DeleteCommand = "DELETE FROM Comunicacion WHERE IdProgramacion = " + GVgrupos.SelectedValue.ToString 'El campo IdProgramacion lo indico en la propiedad DataKeyNames del GridView
            SDSprogramacion.Delete()
            SDSprogramacion.DeleteCommand = "DELETE FROM ApoyoEvaluacion WHERE IdProgramacion = " + GVgrupos.SelectedValue.ToString 'El campo IdProgramacion lo indico en la propiedad DataKeyNames del GridView
            SDSprogramacion.Delete()
            SDSprogramacion.DeleteCommand = "DELETE FROM Programacion WHERE IdProgramacion = " + GVgrupos.SelectedValue.ToString 'El campo IdProgramacion lo indico en la propiedad DataKeyNames del GridView
            SDSprogramacion.Delete()
            GVgrupos.DataBind()
            msgError.hide()
            Mensaje.Text = "El registro ha sido eliminado"
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            Mensaje.Text = ""
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLniveles_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLniveles.DataBound
        DDLniveles.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrados_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrados.DataBound
        DDLgrados.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub GVgrupos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVgrupos.DataBound
        If GVgrupos.Rows.Count > 0 Then
            Desasignar.Visible = True
        Else
            Desasignar.Visible = False
        End If
    End Sub

    Protected Sub DDLplantelgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantelgrupo.DataBound
        DDLplantelgrupo.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLplantel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.SelectedIndexChanged
        DDLplantelgrupo.SelectedIndex = DDLplantel.SelectedIndex
    End Sub

    Protected Sub GVprofesores_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVprofesores.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.PROFESOR
        End If
    End Sub

    Protected Sub GVgrupos_PreRender(sender As Object, e As EventArgs) Handles GVgrupos.PreRender
        ' Etiquetas de GridView
        GVgrupos.Columns(2).HeaderText = "Id_" & Config.Etiqueta.GRUPO

        GVgrupos.Columns(3).HeaderText = Config.Etiqueta.GRUPO

        GVgrupos.Columns(4).HeaderText = Config.Etiqueta.ASIGNATURA

        GVgrupos.Columns(5).HeaderText = Config.Etiqueta.GRADO

        GVgrupos.Columns(6).HeaderText = Config.Etiqueta.PLANTEL

        GVgrupos.Columns(7).HeaderText = Config.Etiqueta.CICLO
    End Sub
End Class
