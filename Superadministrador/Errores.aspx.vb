﻿Imports Siget

Imports System.Data
Imports System.Xml

Partial Class superadministrador_Errores
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Dim oDs As New DataSet
    oDs.ReadXml(Config.Global.rutaLogExcepciones)
    If oDs.Tables.Count > 0 Then
      GVErrores.DataSource = oDs
      GVErrores.DataBind()
      GVErrores.Visible = True
      Eliminar.Enabled = True
    Else
      GVErrores.Visible = False
      Eliminar.Enabled = False
    End If


  End Sub

  Protected Sub Eliminar_Click(sender As Object, e As EventArgs) Handles Eliminar.Click
    ' primero cargo el documento
    Dim xmldoc As New XmlDocument()
    xmldoc.Load(Config.Global.rutaLogExcepciones)

    'Select main node
    Dim rootNode As XmlNode = xmldoc.SelectSingleNode("/Log")
    While rootNode.HasChildNodes
      rootNode.RemoveAll()
    End While
    xmldoc.Save(Config.Global.rutaLogExcepciones)

    ' actualizo el gridview
    Dim oDs As New DataSet
    oDs.ReadXml(Config.Global.rutaLogExcepciones)
    If oDs.Tables.Count > 0 Then
      GVErrores.DataSource = oDs
      GVErrores.DataBind()
      GVErrores.Visible = True
      Eliminar.Enabled = True
    Else
      GVErrores.Visible = False
      Eliminar.Enabled = False
    End If
  End Sub
End Class
