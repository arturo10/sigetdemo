﻿Imports Siget

Partial Class superadministrador_UsuariosAdministracion
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

    End Sub

    Protected Sub BtnInsertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnInsertar.Click
        Try
            Dim status As MembershipCreateStatus

            ' 17/10/2013 se deseleccionó requiresQuestionAndAnswer="false"  de web config, no es necesario
            '    Dim passwordQuestion As String = ""
            '    Dim passwordAnswer As String = ""
            '    If Membership.RequiresQuestionAndAnswer Then
            '        passwordQuestion = "Empresa desarrolladora de " & Config.Etiqueta.SISTEMA_CORTO
            'passwordAnswer = "Integrant"
            '    End If

            Dim newUser As MembershipUser = Membership.CreateUser(Trim(TBlogin.Text), Trim(TBpassword.Text), _
                                                               Trim(TBemail.Text), Nothing, _
                                                               Nothing, True, status)
            If newUser Is Nothing Then
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), GetErrorMessage(status))
            Else
                Roles.AddUserToRole(Trim(TBlogin.Text), DDLperfil.SelectedValue.ToString)
            End If

            Try
                'Lo pongo aqui por si no se puede generar el usuario:
                SDSadmin.InsertCommand =
                    "SET dateformat dmy; INSERT INTO Usuario(Login,Password,PerfilASP,FechaModif,Modifico" &
                    ") VALUES ('" + Trim(TBlogin.Text) + "','" + Trim(TBpassword.Text) + "','" + DDLperfil.SelectedValue + "',getdate(),'" & User.Identity.Name & "')"
                SDSadmin.Insert()
                SDSadmin.InsertCommand =
                    "SET dateformat dmy; INSERT INTO Admin(Nombre,Apellidos,Email,FechaAlta,Estatus, FechaModif, Modifico, IdUsuario" &
                    ") VALUES ('" + TBnombre.Text + "','" + TBapellidos.Text + "','" + TBemail.Text + "',getdate(),'Activo', getdate(), '" & User.Identity.Name & "', (SELECT IdUsuario FROM Usuario WHERE Login = '" + Trim(TBlogin.Text) + "'))"
                SDSadmin.Insert()
                msgError.hide()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try
            'GVadmin.DataBind()

        Catch ex As MembershipCreateUserException
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Public Function GetErrorMessage(ByVal status As MembershipCreateStatus) As String
        Select Case status
            Case MembershipCreateStatus.DuplicateUserName
                Return "El usuario ya existe, elija otro usuario."
            Case MembershipCreateStatus.DuplicateEmail
                Return "Ya existe un usuario para ese email, por favor escriba un email diferente."
            Case MembershipCreateStatus.InvalidPassword
                Return "El password es inválido, pruebe con otro."
            Case MembershipCreateStatus.InvalidEmail
                Return "El formato del email es inválido, por favor revíselo."
            Case MembershipCreateStatus.InvalidAnswer
                Return "La respuesta secreta es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidQuestion
                Return "La pregunta de recuperación de password es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidUserName
                Return "El nombre de usuario es inválido, corríjalo."
            Case MembershipCreateStatus.ProviderError
                Return "La autentificación falló, contacte al administrador del sistema."
            Case MembershipCreateStatus.UserRejected
                Return "La creación del usuario ha sido cancelada, consulte al administrador."
            Case Else
                Return "Ha ocurrido un error desconocido, verifique o contacte al administrador."
        End Select
    End Function

    Protected Sub BtnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEliminar.Click
        Try
            SDSadmin.DeleteCommand = "DELETE FROM Admin WHERE IdAdmin = " + GVadmin.SelectedRow.Cells(1).Text
            SDSadmin.Delete()
            SDSadmin.DeleteCommand = "DELETE FROM Usuario WHERE Login = '" + GVadmin.SelectedRow.Cells(5).Text & "'"
            SDSadmin.Delete()
            'Ahora borro el usuario de la tabla de seguridad (lo hago después por si falla el borrado del registro anterior)
            Membership.DeleteUser(Trim(HttpUtility.HtmlDecode(GVadmin.SelectedRow.Cells(5).Text)))
            msgError.hide()
            GVadmin.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub
End Class