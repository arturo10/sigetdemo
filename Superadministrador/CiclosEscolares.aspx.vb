﻿Imports Siget


Partial Class coordinador_CiclosEscolares
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.CICLOS
        Label2.Text = Config.Etiqueta.CICLO
        Label3.Text = Config.Etiqueta.CICLO

    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        Try
            SDScicloescolar.InsertCommand = "SET dateformat dmy; INSERT INTO CicloEscolar(IdIndicador,Descripcion,FechaInicio,FechaFin,Estatus,FechaModif, Modifico) VALUES (" + DDLindicador.SelectedValue + ",'" + TBdescripcion.Text + "','" + CalInicio.SelectedDate.ToShortDateString + "','" + CalFin.SelectedDate.ToShortDateString + " 23:59:00','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
            SDScicloescolar.Insert()
            Mensaje.Text = "El registro ha sido guardado"
            GVciclosescolares.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click
        TBdescripcion.Text = ""
        DDLestatus.SelectedValue = "Activo"
        Mensaje.Text = "Capture los datos"
        msgError.hide()
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        Try
            SDScicloescolar.UpdateCommand = "SET dateformat dmy; UPDATE CicloEscolar SET IdIndicador = " + DDLindicador.SelectedValue + ",Descripcion = '" + TBdescripcion.Text + "', FechaInicio = '" + CalInicio.SelectedDate + "', FechaFin = '" + CalFin.SelectedDate + " 23:59:00', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "', Estatus = '" + DDLestatus.SelectedValue + "' WHERE IdCicloEscolar = " + GVciclosescolares.SelectedRow.Cells(1).Text
            SDScicloescolar.Update()
            Mensaje.Text = "El registro ha sido actualizado"
            GVciclosescolares.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVciclosescolares_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVciclosescolares.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.CICLO
        End If
    End Sub

    Protected Sub GVciclosescolares_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVciclosescolares.SelectedIndexChanged
        'La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
        DDLindicador.SelectedValue = GVciclosescolares.SelectedRow.Cells(2).Text
        TBdescripcion.Text = HttpUtility.HtmlDecode(GVciclosescolares.SelectedRow.Cells(3).Text())
        CalInicio.SelectedDate = CDate(GVciclosescolares.SelectedRow.Cells(4).Text)
        CalFin.SelectedDate = CDate(GVciclosescolares.SelectedRow.Cells(5).Text)
        DDLestatus.SelectedValue = Trim(GVciclosescolares.SelectedRow.Cells(8).Text)
        'La liga seleccionar del grid ocupa el index 0
    End Sub

    Protected Sub DDLindicador_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLindicador.DataBound
        DDLindicador.Items.Insert(0, New ListItem("---Elija el Indicador", 0))
    End Sub

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        Try
            SDScicloescolar.DeleteCommand = "Delete from CicloEscolar where IdCicloEscolar = " + GVciclosescolares.SelectedValue.ToString 'Me da el IdCicloEscolar porque es el campo clave de la fila seleccionada
            SDScicloescolar.Delete()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub
End Class
