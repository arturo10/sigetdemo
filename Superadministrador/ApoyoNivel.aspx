﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="ApoyoNivel.aspx.vb"
    Inherits="superadministrador_ApoyoNivel"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>




<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
            height: 450px;
        }

        .style22 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style15 {
            width: 100%;
        }

        .style23 {
        }

        .style25 {
            text-align: left;
        }

        .style28 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style29 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 368px;
        }

        .style30 {
            font-size: x-small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Alta de apoyos académicos para un 
								<asp:Label ID="Label2" runat="server" Text="[NIVEL]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td colspan="3">
                <asp:Label ID="Mensaje1" runat="server" CssClass="titulo"
                    Style="color: #000066; font-family: Arial, Helvetica, sans-serif;">Capture los datos</asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: right" class="style29">
                <asp:Label ID="Label3" runat="server" Text="[NIVEL]" />
                :</td>
            <td class="style25" colspan="2">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion"
                    DataValueField="IdNivel" Width="297px" CssClass="style22"
                    Height="22px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align: right" class="style29">Consecutivo:</td>
            <td class="style25" colspan="2">
                <asp:TextBox ID="TBconsecutivo" runat="server" MaxLength="3" Width="48px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right" class="style29">Descripción del Material:</td>
            <td class="style25" colspan="2">
                <asp:TextBox ID="TBdescripcion" runat="server" MaxLength="150" Width="297px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right" class="style29">Tipo de archivo:</td>
            <td class="style25" colspan="2">
                <asp:DropDownList ID="DDLtipoarchivo" runat="server" Width="100px"
                    AutoPostBack="True">
                    <asp:ListItem>PDF</asp:ListItem>
                    <asp:ListItem>ZIP</asp:ListItem>
                    <asp:ListItem>Word</asp:ListItem>
                    <asp:ListItem>Excel</asp:ListItem>
                    <asp:ListItem Value="PP">Power Point</asp:ListItem>
                    <asp:ListItem>Imagen</asp:ListItem>
                    <asp:ListItem Value="Video"></asp:ListItem>
                    <asp:ListItem>Audio</asp:ListItem>
                    <asp:ListItem Value="Link"></asp:ListItem>
                    <asp:ListItem>FLV</asp:ListItem>
                    <asp:ListItem>SWF</asp:ListItem>
                    <asp:ListItem>YouTube</asp:ListItem>
                </asp:DropDownList>
                <asp:Label ID="Label1" runat="server"
                    Style="color: #000099; font-size: x-small; font-weight: 700; font-family: Arial, Helvetica, sans-serif;"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="text-align: right" class="style29">Cargar material de apoyo:<br />
                <span class="style30">*al insertar se carga el archivo</span></td>
            <td class="style25">
                <asp:AsyncFileUpload ID="RutaArchivo" runat="server" CssClass="imageUploaderField"/>
                <asp:TextBox ID="TBlink" runat="server" MaxLength="150" Visible="False"
                    Width="231px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: right" class="style29">Estatus:</td>
            <td class="style25" colspan="2">
                <asp:DropDownList ID="DDLestatus" runat="server" Width="87px">
                    <asp:ListItem Value="Activo"></asp:ListItem>
                    <asp:ListItem Value="Suspendido">Suspendido</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align: right" class="style29">&nbsp;</td>
            <td class="style23" colspan="2">
                <asp:Button ID="Insertar" runat="server" Text="Insertar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
                <asp:Button ID="Eliminar" runat="server" Text="Eliminar"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                &nbsp;
                <asp:Button ID="Actualizar" runat="server" Text="Actualizar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="font-size: x-small;">
                Nota: Actualizar no modifica el archivo, sólo la información del registro.
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                <table class="style15">
                    <tr>
                        <td colspan="5">
                            <asp:GridView ID="GVapoyosacademicos" runat="server" AllowPaging="True"
                                AllowSorting="True" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3"
                                DataSourceID="SDSApoyoNivel" GridLines="Vertical"
                                Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"
                                Width="882px" DataKeyNames="IdApoyoNivel"
                                Caption="<h3>Apoyos académicos capturados</h3>"
                                ForeColor="Black" PageSize="20">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="IdApoyoNivel" HeaderText="IdApoyo"
                                        SortExpression="IdApoyoNivel" />
                                    <asp:BoundField DataField="IdNivel" HeaderText="IdNivel"
                                        SortExpression="IdNivel" Visible="False" />
                                    <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                                        SortExpression="Consecutivo" />
                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripción"
                                        SortExpression="Descripcion" />
                                    <asp:BoundField DataField="ArchivoApoyo" HeaderText="Archivo de Apoyo"
                                        SortExpression="ArchivoApoyo" />
                                    <asp:BoundField DataField="TipoArchivoApoyo" HeaderText="Tipo"
                                        SortExpression="TipoArchivoApoyo" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: left">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSApoyoNivel" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [ApoyoNivel] WHERE ([IdNivel] = @IdNivel) ORDER BY [Consecutivo]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>

