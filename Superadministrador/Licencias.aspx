﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="Licencias.aspx.vb" 
    Inherits="superadministrador_Licencias" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style10
        {
            width: 100%;
            height: 450px;
        }
        .style11
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
        .style12
        {
            height: 143px;
        }
        .style14
        {
            height: 182px;
        }
        .style15
        {
            font-weight: normal;
            font-family: Arial, Helvetica, sans-serif;
        }
        .style23
        {
            font-family: Arial, Helvetica, sans-serif;
        }
        .auto-style1 {
            width: 200px;
            text-align: right;
        }
        .auto-style2 {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Administración de Licencias
		</h1>
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table style="width: 100%; margin-top: 10px;">
        <tr>
            <td class="auto-style1">
                Clave
            </td>
            <td class="auto-style2">
                <asp:TextBox ID="tbClave" runat="server" Width="300"></asp:TextBox>

            </td>
        </tr>
        <tr>
            <td class="auto-style1">
                Fecha de Expiración
            </td>
            <td class="auto-style2">
                <asp:TextBox ID="tbExpiracion" runat="server" Width="100"></asp:TextBox>
                <asp:CalendarExtender ID="tbExpiracion_CalendarExtender" runat="server"
                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="tbExpiracion"></asp:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">
                Estatus
            </td>
            <td class="auto-style2">
                <asp:DropDownList ID="ddlEstatus" runat="server">
                    <asp:ListItem>Activo</asp:ListItem>
                    <asp:ListItem>Suspendido</asp:ListItem>
                    <asp:ListItem>Baja</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">
                Fecha de Alta
            </td>
            <td class="auto-style2">
                <asp:TextBox ID="tbAlta" runat="server" Width="100"></asp:TextBox>&nbsp;<span class="note">(Opcional)</span>
                <asp:CalendarExtender ID="tbAlta_CalendarExtender" runat="server"
                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="tbAlta"></asp:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center; padding-top: 20px; padding-bottom: 20px;">
                <asp:Button ID="btnCrear" runat="server" Text="Crear"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
				<asp:Button ID="btnEliminar" runat="server" Text="Eliminar"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                &nbsp;
				<asp:Button ID="btnActualizar" runat="server" Text="Actualizar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
        </tr>
    </table>
    <table style="width: 100%;">
        <tr>
            <td>
                <table style="margin-left: auto; margin-right: auto;">
                    <tr>
                        <td>
                            <asp:GridView ID="gvLicencias" runat="server"
                                ClientIDMode="Static" 
                                AllowPaging="False" 
                                AllowSorting="True"
                                AutoGenerateColumns="False" 

                                DataKeyNames="IdLicencia" 
                                DataSourceID="SDSLicencias"

                                CssClass="dataGrid_clear_selectable"
                                GridLines="None">
                                <Columns>
                                    <asp:CommandField 
                                        ShowSelectButton="true" 
                                        SelectText="Seleccionar" 
                                        ItemStyle-CssClass="selectCell" />
                                    <asp:BoundField 
                                        DataField="IdLicencia" 
                                        HeaderText="Id en Sistema" 
                                        SortExpression="IdLicencia" />
                                    <asp:BoundField 
                                        DataField="Clave" 
                                        HeaderText="Clave" 
                                        SortExpression="Clave" />
                                    <asp:BoundField 
                                        DataField="FechaAlta" 
                                        HeaderText="Fecha de Alta (dd/mm/aaaa)" 
                                        SortExpression="FechaAlta" />
                                    <asp:BoundField 
                                        DataField="FechaExpira" 
                                        HeaderText="Fecha de Expiración (dd/mm/aaaa)" 
                                        SortExpression="FechaExpira" />
                                    <asp:BoundField 
                                        DataField="Estatus" 
                                        HeaderText="Estatus" 
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle CssClass="footer" />
                                <PagerStyle CssClass="pager" />
                                <SelectedRowStyle CssClass="selected" />
                                <HeaderStyle CssClass="header" />
                                <AlternatingRowStyle CssClass="altrow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td>
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSLicencias" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM Licencia"></asp:SqlDataSource>
            </td>
            <td>
                
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

