﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="Desbloquea.aspx.vb" 
    Inherits="superadministrador_Desbloquea" 
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style10
        {
            width: 100%;
            height: 399px;
        }
        .style11
        {
            height: 97px;
        }
        .style23
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
        .style24
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 34px;
        }
        .style25
        {
            height: 34px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Desbloquear Cuentas de Usuario
		</h1>
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style10">
        <tr>
            <td class="style24">
                <asp:Label ID="LblActivos" runat="server" 
                    style="font-family: Arial, Helvetica, sans-serif; font-size: small; color: #000066"></asp:Label>
            </td>
            <td class="style25">
                </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSusuarios" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>" SelectCommand="SELECT Count(*) Rehabilitadas FROM [vw_aspnet_MembershipUsers]
where IsLockedOut = 'True'"></asp:SqlDataSource>
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" 
                    CellPadding="3" DataSourceID="SDSusuarios" GridLines="Vertical" 
                    
                    style="font-family: Arial, Helvetica, sans-serif; font-size: x-small; text-align: left" 
                    ForeColor="Black">
                    <Columns>
                        <asp:BoundField DataField="Rehabilitadas" HeaderText="Cuentas deshabilitadas" 
                            ReadOnly="True" SortExpression="Rehabilitadas" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Button1" runat="server" Text="Desbloquear" 
										CssClass="defaultBtn btnThemeBlue btnThemeMedium"/>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style11">
                <asp:Label ID="LblMensaje" runat="server" CssClass="LabelInfoDefault"></asp:Label>
            </td>
            <td class="style11">
            </td>
        </tr>
        <tr>
            <td>
                <asp:HyperLink ID="HyperLink2" runat="server"
					NavigateUrl="~/" 
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
                </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

