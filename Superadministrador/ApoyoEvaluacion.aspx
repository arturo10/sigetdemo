﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="ApoyoEvaluacion.aspx.vb" 
    Inherits="superadministrador_ApoyoEvaluacion" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgWarning.ascx" TagPrefix="uc1" TagName="msgWarning" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            height: 41px;
            text-align: left;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style15 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style19 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
        }

        .style24 {
            width: 361px;
        }

        .style30 {
            font-size: x-small;
        }

        .style31 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: center;
            font-weight: bold;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
            height: 30px;
        }

        .style44 {
            width: 100%;
        }

        .style45 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style46 {
            height: 38px;
        }

        .style47 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style13 {
            text-align: center;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Material para actividades y grupos específicos (como listas de asistencia, notas 
                para el 
								<asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
        , etc.)
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style23">
                <asp:Label ID="Label2" runat="server" Text="[INSTITUCION]" />
            </td>
            <td colspan="2">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                    DataValueField="IdInstitucion" CssClass="style15" Height="22px"
                    Width="320px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style23">
                <asp:Label ID="Label3" runat="server" Text="[CICLO]" />
            </td>
            <td colspan="2">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    CssClass="style15" DataSourceID="SDSciclosescolares"
                    DataTextField="Descripcion" DataValueField="IdCicloEscolar" Height="22px"
                    Width="320px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style23">
                <asp:Label ID="Label4" runat="server" Text="[PLANTEL]" />
            </td>
            <td colspan="2">
                <asp:DropDownList ID="DDLplanteles" runat="server" AutoPostBack="True"
                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                    DataValueField="IdPlantel" CssClass="style15" Height="22px"
                    Width="320px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style23">
                <asp:Label ID="Label5" runat="server" Text="[GRUPO]" />
            </td>
            <td colspan="2">
                <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                    CssClass="style15" DataSourceID="SDSgrupos" DataTextField="Descripcion"
                    DataValueField="IdGrupo" Height="22px" Width="320px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style23">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style23" colspan="3">
                <asp:GridView ID="GVprogramacion" runat="server" 
                    AllowPaging="True"
                    AutoGenerateColumns="False" 
                    PageSize="15"
                    Caption="<h3>PROFESORES y ASIGNATURAS asignadas al GRUPO seleccionado</h3>"
                    AllowSorting="True"

                    DataKeyNames="IdAsignatura,IdProgramacion"
                    DataSourceID="SDSprogramaciones"
                    Width="713px" 

                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small; text-align: left;"
                    BackColor="White" 
                    BorderColor="#999999" 
                    BorderStyle="Solid" 
                    BorderWidth="1px"
                    CellPadding="3" 
                    GridLines="Vertical" 
                    ForeColor="Black">
                    <FooterStyle BackColor="#CCCCCC" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True">
                            <HeaderStyle Width="70px" />
                        </asp:CommandField>
                        <asp:BoundField DataField="IdAsignatura" HeaderText="Id_[Asignatura]"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdAsignatura">
                            <HeaderStyle Width="40px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdProgramacion" HeaderText="Id Programacion"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdProgramacion">
                            <HeaderStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NomProfesor" HeaderText="[Profesor]" ReadOnly="True"
                            SortExpression="NomProfesor" />
                        <asp:BoundField DataField="Asignatura" HeaderText="[Asignatura]"
                            SortExpression="Asignatura" />
                    </Columns>
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  select
	                1  idASIGNATURA
	                2  idprogramacion
	                3  PROFESOR
	                4  ASIGNATURA
	                --%>
            </td>
        </tr>
        <tr>
            <td class="style23">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style19" colspan="3">
                <asp:GridView ID="GVevaluaciones" runat="server" AllowPaging="True"
                    AutoGenerateColumns="False" DataKeyNames="IdEvaluacion"
                    DataSourceID="SDSevaluaciones"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small; text-align: left;"
                    BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px"
                    CellPadding="3" GridLines="Vertical" Width="715px" ForeColor="Black"
                    PageSize="15"
                    Caption="<h3>Actividades creadas para la ASIGNATURA seleccionada</h3>"
                    AllowSorting="True">
                    <FooterStyle BackColor="#CCCCCC" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Evaluacion"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion">
                            <HeaderStyle Width="70px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Actividad" HeaderText="Actividad"
                            SortExpression="Actividad" />
                        <asp:BoundField DataField="Clave para Reportes"
                            HeaderText="Clave para Reportes" SortExpression="Clave para Reportes" />
                        <asp:BoundField DataField="Calificación" HeaderText="Calificación"
                            SortExpression="Calificación" />
                    </Columns>
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style31" colspan="3">Agregue los apoyos</td>
        </tr>
        <tr>
            <td class="style23">Consecutivo</td>
            <td colspan="2" style="text-align: left">
                <asp:TextBox ID="TBconsecutivo" runat="server" MaxLength="3" Width="48px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style23">Descripción del Material&nbsp;
            </td>
            <td colspan="2" style="text-align: left">
                <asp:TextBox ID="TBdescripcion" runat="server" MaxLength="150" Width="297px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style23">&nbsp;</td>
            <td colspan="2">
                <asp:HyperLink ID="HLarchivo" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; color: #000066"
                    Target="_blank" Visible="False">[HLarchivo]</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td class="style23">Cargar material de apoyo<br />
                <span class="style30">*al insertar o actualizar se carga el&nbsp; archivo</span></td>
            <td colspan="2">
                <asp:AsyncFileUpload ID="fuArchivo" runat="server" style="text-align: left" CssClass="imageUploaderField"/>
            </td>
        </tr>
        <tr>
            <td class="style23">
                &nbsp;
            </td>
            <td colspan="2">
                <asp:Button ID="cargar" runat="server" Text="Cargar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
                                <asp:Button ID="BtnEliminar" runat="server" Text="Eliminar"
                                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td class="style23" colspan="3">
                <asp:GridView ID="GVapoyos" runat="server" AllowPaging="True"
                    AllowSorting="True" AutoGenerateColumns="False" BackColor="White"
                    BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3"
                    DataSourceID="SDSapoyoseval" GridLines="Vertical"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small; text-align: left;"
                    Width="882px" DataKeyNames="IdApoyoEvaluacion"
                    Caption="<h3>Apoyos para la Actividad</h3>"
                    ForeColor="Black" PageSize="20">
                    <FooterStyle BackColor="#CCCCCC" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True">
                            <HeaderStyle Width="70px" />
                        </asp:CommandField>
                        <asp:BoundField DataField="IdApoyoEvaluacion" HeaderText="Id Apoyo Evaluacion"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdApoyoEvaluacion">
                            <HeaderStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdProgramacion" HeaderText="Id Programación"
                            SortExpression="IdProgramacion">
                            <HeaderStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Evaluacion"
                            SortExpression="IdEvaluacion">
                            <HeaderStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                            SortExpression="Consecutivo" />
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion"
                            SortExpression="Descripcion" />
                        <asp:BoundField DataField="ArchivoApoyo" HeaderText="ArchivoApoyo"
                            SortExpression="ArchivoApoyo" />
                    </Columns>
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Button ID="Quitar" runat="server" Text="Quitar"
                    Visible="False" CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style24">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td class="style16">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <asp:Panel ID="PnlConfirma" runat="server" CssClass="dialogOverwrite dialog1">
        <table>
            <tr>
                <td colspan="3">
                    <div style="width: 100%; text-align: center; padding-bottom: 10px;">
                        <asp:Image ID="Image1" runat="server"
                            ImageUrl="~/Resources/imagenes/Exclamation.png"
                            Width="50" Height="50" />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding-bottom: 20px;">En el almacén de archivos ya existe un archivo con el mismo nombre:
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding-bottom: 20px;">
                    <asp:HyperLink ID="HLexiste" runat="server"
                        CssClass="hyperlink"
                        Target="_blank">Link</asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding-bottom: 20px;">Puede asignar este mismo archivo al subtema actual, 
                    sobreescribir el archivo antiguo con el archivo recién cargado,
                    o cancelar la operación.
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <uc1:msgWarning runat="server" ID="msgWarning" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="btnDialogAssign" runat="server" Text="Asignar"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                    &nbsp;
                    <asp:Button ID="btnDialogOverwrite" runat="server" Text="Sobreescribir"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                    &nbsp;
                    <asp:Button ID="btnDialogCancel" runat="server" Text="Cancelar"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </asp:Panel>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSInstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Institucion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar] WHERE Estatus = 'Activo' ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdGrupo, Descripcion
FROM Grupo
WHERE IdPlantel = @IdPlantel and IdCicloEscolar = @IdCicloEscolar
order by Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplanteles" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSprogramaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Pr.IdProgramacion, rtrim(P.Apellidos) + ' ' + rtrim(P.Nombre) as NomProfesor, A.IdAsignatura, A.Descripcion as Asignatura
from Profesor P, Asignatura A, Programacion Pr
where Pr.IdCicloEscolar = @IdCicloEscolar and Pr.IdGrupo = @IdGrupo
and A.IdAsignatura = Pr.IdAsignatura and P.IdProfesor = Pr.IdProfesor
order by P.Apellidos, P.Nombre, A.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSapoyoseval" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select *
from ApoyoEvaluacion
where IdProgramacion = @IdProgramacion and IdEvaluacion = @IdEvaluacion
order by Consecutivo">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdProgramacion" SessionField="IdProgramacion" />
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select E.IdEvaluacion, E.ClaveBateria as Actividad, E.ClaveAbreviada as 'Clave para Reportes',C.Descripcion as 'Calificación'
from Evaluacion E, Calificacion C
where C.IdCicloEscolar = @IdCicloEscolar and C.IdAsignatura = @IdAsignatura
and E.idCalificacion = C.IdCalificacion
order by C.Consecutivo, E.InicioContestar">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="GVprogramacion" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="HF1" runat="server" />
                <asp:ModalPopupExtender ID="HF1_ModalPopupExtender" runat="server"
                    Enabled="True" PopupControlID="PnlConfirma" TargetControlID="HF1" BackgroundCssClass="overlay">
                </asp:ModalPopupExtender>
            </td>
        </tr>
    </table>
</asp:Content>

