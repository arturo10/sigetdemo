﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="AgruparReactivos.aspx.vb"
    Inherits="superadministrador_AgruparReactivos"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            width: 100%;
        }

        .style13 {
            width: 127px;
            text-align: right;
        }

        .style14 {
            width: 133px;
            text-align: right;
        }

        .style19 {
            color: #000066;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style15 {
            width: 127px;
            text-align: right;
            height: 16px;
        }

        .style23 {
            height: 36px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style24 {
            width: 127px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style25 {
            height: 16px;
            text-align: left;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style37 {
            text-align: left;
        }

        .style38 {
            text-align: right;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Asignar Reactivos a Actividades
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style11">
        <tr>
            <td>
                <table class="estandar">
                    <tr>
                        <td class="style24">
                            <asp:Label ID="Label1" runat="server" Text="[CICLO]" />
                        </td>
                        <td class="style37">
                            <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                                DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                                DataValueField="IdCicloEscolar" Width="322px" Height="22px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style24">
                            <asp:Label ID="Label2" runat="server" Text="[NIVEL]" />
                        </td>
                        <td class="style37">
                            <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                                Height="22px" Width="322px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style24">
                            <asp:Label ID="Label3" runat="server" Text="[GRADO]" />
                        </td>
                        <td class="style37">
                            <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                                Height="22px" Width="322px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style24">
                            <asp:Label ID="Label4" runat="server" Text="[ASIGNATURA]" />
                        </td>
                        <td colspan="3" style="text-align: left">
                            <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                                DataSourceID="SDSasignaturas" DataTextField="Materia"
                                DataValueField="IdAsignatura" Height="22px" Width="650px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style24">Calificaciones</td>
                        <td colspan="3" style="text-align: left">
                            <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True"
                                DataSourceID="SDScalificaciones" DataTextField="Cal"
                                DataValueField="IdCalificacion" Height="22px" Width="650px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table style="margin-left: auto; margin-right: auto;">
                                <tr>
                                    <td>
                                        <asp:GridView ID="GVevaluaciones" runat="server"
                                            AllowPaging="True"
                                            AllowSorting="True"
                                            AutoGenerateColumns="False"
                                            Caption="<h3>Actividades creadas</h3>"
                                            DataKeyNames="IdEvaluacion, IdCalificacion"
                                            DataSourceID="SDSevaluaciones"
                                            Width="800px"
                                            CssClass="dataGrid_clear_selectable"
                                            GridLines="None">
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                                                <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                                                    Visible="False" ReadOnly="True" SortExpression="IdEvaluacion" />
                                                <asp:BoundField DataField="ClaveBateria" HeaderText="Nombre de la Actividad"
                                                    SortExpression="ClaveBateria" />
                                                <%--<asp:BoundField DataField="Resultado" HeaderText="Resultado"
                                                    SortExpression="Resultado" />--%>
                                                <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                                                    SortExpression="Porcentaje">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                    HeaderText="Inicia" SortExpression="InicioContestar" />
                                                <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                    HeaderText="Termina" SortExpression="FinContestar" />
                                                <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                                <%--<asp:BoundField DataField="UsarReactivos" HeaderText="Reactivos"
                                                    SortExpression="UsarReactivos">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>--%>
                                                <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                    SortExpression="Estatus" />
                                            </Columns>
                                            <FooterStyle CssClass="footer" />
                                            <PagerStyle CssClass="pager" />
                                            <SelectedRowStyle CssClass="selected" />
                                            <HeaderStyle CssClass="header" />
                                            <AlternatingRowStyle CssClass="altrow" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="style13">&nbsp;</td>
                        <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style13">&nbsp;</td>
                        <td colspan="3" class="style19">Ahora asigne los temas que deberán integrar la Actividad seleccionada, y 
                            de los cuáles se
                            tomarán sus reactivos.</td>
                    </tr>
                    <tr>
                        <td class="style13">&nbsp;</td>
                        <td colspan="3">&nbsp;</td>
                    </tr>

                    <tr>
                        <td class="style24">
                            <asp:Label ID="Label5" runat="server" Text="[NIVEL]" />
                            :
                        </td>
                        <td class="style37">
                            <asp:DropDownList ID="ddlNivelesSubtemas" runat="server" AutoPostBack="True"
                                DataSourceID="SDSnivelesSubtemas" DataTextField="Descripcion"
                                DataValueField="IdNivel" Width="270px" Height="22px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style24">
                            <asp:Label ID="Label6" runat="server" Text="[GRADO]" />
                            :
                        </td>
                        <td class="style37">
                            <asp:DropDownList ID="ddlGradosSubtemas" runat="server" AutoPostBack="True"
                                DataSourceID="SDSgradosSubtemas" DataTextField="Descripcion"
                                DataValueField="IdGrado" Width="275px" Height="22px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style24">
                            <asp:Label ID="Label7" runat="server" Text="[ASIGNATURA]" />
                            :
                        </td>
                        <td class="style37">
                            <asp:DropDownList ID="ddlAsignaturasSubtemas" runat="server" AutoPostBack="True"
                                DataSourceID="SDSasignaturasSubtemas" DataTextField="Descripcion"
                                DataValueField="IdAsignatura" Width="270px" Height="22px">
                            </asp:DropDownList>

                        </td>
                        <td class="style14">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style24">Temas:
                        </td>
                        <td class="style37">
                            <asp:DropDownList ID="DDLtemas" runat="server" AutoPostBack="True"
                                DataSourceID="SDStemas" DataTextField="NomTema"
                                DataValueField="IdTema" Width="275px" Height="22px">
                            </asp:DropDownList>

                        </td>
                        <td class="style14">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style24">Subtema:
                        </td>
                        <td>
                            <asp:DropDownList ID="DDLsubtemas" runat="server" AutoPostBack="True"
                                DataSourceID="SDSsubtemas" DataTextField="NomSubtema"
                                DataValueField="IdSubtema" Width="690px" Height="22px">
                            </asp:DropDownList>

                        </td>
                        <td class="style14">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td class="style15"></td>
                        <td class="style16">
                            <asp:Button ID="Agregar" runat="server"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium"
                                Text="Agregar" />
                        </td>
                        <td class="style17"></td>
                        <td class="style16">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <uc1:msgError runat="server" ID="msgError" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">&nbsp;</td>
                        <td class="style16" colspan="3">
                            <asp:GridView ID="GVdetalleevaluacion" runat="server"
                                AllowSorting="True"
                                AutoGenerateColumns="False"
                                Caption="<h3>Subtemas asignados a la Actividad actual:</h3>"
                                ShowFooter="True"
                                DataSourceID="SDSdetalleevaluacion"
                                DataKeyNames="IdDetalleEvaluacion"
                                Width="743px"
                                CssClass="dataGrid_clear_selectable"
                                GridLines="None">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell">
                                        <HeaderStyle Width="60px" />
                                        <ItemStyle Width="60px" />
                                    </asp:CommandField>
                                    <asp:BoundField DataField="IdDetalleEvaluacion" HeaderText="Id Detalle Act."
                                        Visible="False" ReadOnly="True"
                                        SortExpression="IdDetalleEvaluacion">
                                        <HeaderStyle Width="50px" />
                                        <ItemStyle Width="50px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ClaveBateria" HeaderText="Actividad"
                                        SortExpression="ClaveBateria">
                                        <HeaderStyle Width="110px" />
                                        <ItemStyle Width="110px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Tema" HeaderText="Tema" SortExpression="Tema">
                                        <HeaderStyle Width="140px" />
                                        <ItemStyle Width="140px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Subtema" HeaderText="Subtema"
                                        SortExpression="Subtema">
                                        <HeaderStyle Width="140px" />
                                        <ItemStyle Width="140px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Reactivos" HeaderText="Total Reactivos"
                                        SortExpression="Reactivos">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UsarReactivos" HeaderText="Reactivos a Usar"
                                        SortExpression="UsarReactivos">
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FechaModif" HeaderText="Fecha de Modificación"
                                        Visible="False" SortExpression="FechaModif" />
                                    <asp:BoundField DataField="Modifico" HeaderText="Modificó"
                                        Visible="False" SortExpression="Modifico" />
                                </Columns>
                                <FooterStyle CssClass="footer" />
                                <PagerStyle CssClass="pager" />
                                <SelectedRowStyle CssClass="selected" />
                                <HeaderStyle CssClass="header" />
                                <AlternatingRowStyle CssClass="altrow" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">&nbsp;</td>
                        <td class="style16">
                            <asp:Button ID="Quitar" runat="server"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium"
                                Text="Quitar" />
                        </td>
                        <td class="style37" rowspan="2">
                            <asp:ListBox ID="LBusar" runat="server" Width="105px">
                                <asp:ListItem Selected="True" Value="NULL">Todos</asp:ListItem>
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="11">11</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="13">13</asp:ListItem>
                                <asp:ListItem Value="14">14</asp:ListItem>
                                <asp:ListItem Value="15">15</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="17">17</asp:ListItem>
                                <asp:ListItem Value="18">18</asp:ListItem>
                                <asp:ListItem Value="19">19</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                                <asp:ListItem Value="21">21</asp:ListItem>
                                <asp:ListItem Value="22">22</asp:ListItem>
                                <asp:ListItem Value="23">23</asp:ListItem>
                                <asp:ListItem Value="24">24</asp:ListItem>
                                <asp:ListItem Value="25">25</asp:ListItem>
                                <asp:ListItem Value="26">26</asp:ListItem>
                                <asp:ListItem Value="27">27</asp:ListItem>
                                <asp:ListItem Value="28">28</asp:ListItem>
                                <asp:ListItem Value="29">29</asp:ListItem>
                                <asp:ListItem Value="30">30</asp:ListItem>
                                <asp:ListItem Value="31">31</asp:ListItem>
                                <asp:ListItem Value="32">32</asp:ListItem>
                                <asp:ListItem Value="33">33</asp:ListItem>
                                <asp:ListItem Value="34">34</asp:ListItem>
                                <asp:ListItem Value="35">35</asp:ListItem>
                                <asp:ListItem Value="36">36</asp:ListItem>
                                <asp:ListItem Value="37">37</asp:ListItem>
                                <asp:ListItem Value="38">38</asp:ListItem>
                                <asp:ListItem Value="39">39</asp:ListItem>
                                <asp:ListItem Value="40">40</asp:ListItem>
                                <asp:ListItem Value="41">41</asp:ListItem>
                                <asp:ListItem Value="42">42</asp:ListItem>
                                <asp:ListItem Value="43">43</asp:ListItem>
                                <asp:ListItem Value="44">44</asp:ListItem>
                                <asp:ListItem Value="45">45</asp:ListItem>
                                <asp:ListItem Value="46">46</asp:ListItem>
                                <asp:ListItem Value="47">47</asp:ListItem>
                                <asp:ListItem Value="48">48</asp:ListItem>
                                <asp:ListItem Value="49">49</asp:ListItem>
                                <asp:ListItem Value="50">50</asp:ListItem>
                                <asp:ListItem Value="51">51</asp:ListItem>
                                <asp:ListItem Value="52">52</asp:ListItem>
                                <asp:ListItem Value="53">53</asp:ListItem>
                                <asp:ListItem Value="54">54</asp:ListItem>
                                <asp:ListItem Value="55">55</asp:ListItem>
                                <asp:ListItem Value="56">56</asp:ListItem>
                                <asp:ListItem Value="57">57</asp:ListItem>
                                <asp:ListItem Value="58">58</asp:ListItem>
                                <asp:ListItem Value="59">59</asp:ListItem>
                                <asp:ListItem Value="60">60</asp:ListItem>
                                <asp:ListItem Value="61">61</asp:ListItem>
                                <asp:ListItem Value="62">62</asp:ListItem>
                                <asp:ListItem Value="63">63</asp:ListItem>
                                <asp:ListItem Value="64">64</asp:ListItem>
                                <asp:ListItem Value="65">65</asp:ListItem>
                                <asp:ListItem Value="66">66</asp:ListItem>
                                <asp:ListItem Value="67">67</asp:ListItem>
                                <asp:ListItem Value="68">68</asp:ListItem>
                                <asp:ListItem Value="69">69</asp:ListItem>
                                <asp:ListItem Value="70">70</asp:ListItem>
                                <asp:ListItem Value="71">71</asp:ListItem>
                                <asp:ListItem Value="72">72</asp:ListItem>
                                <asp:ListItem Value="73">73</asp:ListItem>
                                <asp:ListItem Value="74">74</asp:ListItem>
                                <asp:ListItem Value="75">75</asp:ListItem>
                                <asp:ListItem Value="76">76</asp:ListItem>
                                <asp:ListItem Value="77">77</asp:ListItem>
                                <asp:ListItem Value="78">78</asp:ListItem>
                                <asp:ListItem Value="79">79</asp:ListItem>
                                <asp:ListItem Value="80">80</asp:ListItem>
                                <asp:ListItem Value="81">81</asp:ListItem>
                                <asp:ListItem Value="82">82</asp:ListItem>
                                <asp:ListItem Value="83">83</asp:ListItem>
                                <asp:ListItem Value="84">84</asp:ListItem>
                                <asp:ListItem Value="85">85</asp:ListItem>
                                <asp:ListItem Value="86">86</asp:ListItem>
                                <asp:ListItem Value="87">87</asp:ListItem>
                                <asp:ListItem Value="88">88</asp:ListItem>
                                <asp:ListItem Value="89">89</asp:ListItem>
                                <asp:ListItem Value="90">90</asp:ListItem>
                                <asp:ListItem Value="91">91</asp:ListItem>
                                <asp:ListItem Value="92">92</asp:ListItem>
                                <asp:ListItem Value="93">93</asp:ListItem>
                                <asp:ListItem Value="94">94</asp:ListItem>
                                <asp:ListItem Value="95">95</asp:ListItem>
                                <asp:ListItem Value="96">96</asp:ListItem>
                                <asp:ListItem Value="97">97</asp:ListItem>
                                <asp:ListItem Value="98">98</asp:ListItem>
                                <asp:ListItem Value="99">99</asp:ListItem>
                                <asp:ListItem Value="100">100</asp:ListItem>
                            </asp:ListBox>
                            &nbsp;
                            <asp:Button ID="BtnAsignar" runat="server" Enabled="False"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium"
                                Text="Asignar" />
                        </td>
                        <td class="style16">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style15">&nbsp;</td>
                        <td class="style38">Cantidad de reactivos a utilizar aleatoriamente del total:</td>
                        <td class="style16">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style15">&nbsp;</td>
                        <td class="style16">&nbsp;</td>
                        <td class="style17">&nbsp;</td>
                        <td class="style16">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style15">
                            <asp:HyperLink ID="HyperLink2" runat="server"
                                NavigateUrl="~/"
                                CssClass="defaultBtn btnThemeGrey btnThemeWide">
								Regresar&nbsp;al&nbsp;Menú
                            </asp:HyperLink>
                        </td>
                        <td class="style16">&nbsp;</td>
                        <td class="style17">&nbsp;</td>
                        <td class="style16">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td></td>
            <td>

                <asp:SqlDataSource ID="SDSdetalleevaluacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select  D.IdDetalleEvaluacion, E.ClaveBateria, 
Cast(T.Numero as varchar(6)) + '.-' +  T.Descripcion Tema, Cast(S.Numero as varchar(6)) + '.-' +  S.Descripcion Subtema,
(select Count(IdPlanteamiento) from Planteamiento where IdSubtema = S.IdSubtema and Ocultar='False') as Reactivos, D.UsarReactivos, D.FechaModif, D.Modifico 
from Evaluacion E, DetalleEvaluacion D, Subtema S, Tema T
where E.IdEvaluacion =@IdEvaluacion and
D.IdEvaluacion = E.IdEvaluacion and
S.IdSubtema = D.IdSubtema and
T.IdTema = S.IdTema
order by Subtema">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar] 
WHERE Estatus = 'Activo'
ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Evaluacion] WHERE ([IdCalificacion] = @IdCalificacion)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion"
                            PropertyName="SelectedValue" Type="Int64" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT C.IdCalificacion, C.Descripcion + ' (' + A.Descripcion + ')' as Cal
FROM Calificacion C, Asignatura A
WHERE C.IdCicloEscolar = @IdCicloEscolar
and A.IdAsignatura = C.IdAsignatura and A.IdAsignatura = @IdAsignatura
order by C.Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdGrado,Descripcion FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT A.IdAsignatura, A.Descripcion + ' (' + Ar.Descripcion + ')'  as Materia
FROM Asignatura A, Area Ar
WHERE (A.IdGrado = @IdGrado) and (Ar.IdArea = A.IdArea)
ORDER BY Ar.Descripcion,A.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSnivelesSubtemas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSgradosSubtemas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlNivelesSubtemas" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSasignaturasSubtemas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdAsignatura], [IdGrado], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlGradosSubtemas" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDStemas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdTema], [IdAsignatura], Cast([Numero] as varchar(8))+ ' ' + [Descripcion] NomTema  FROM [Tema] WHERE ([IdAsignatura] = @IdAsignatura)
order by [Numero],[Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlAsignaturasSubtemas" Name="IdAsignatura"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSsubtemas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdSubtema], [IdTema], Cast([Numero] as varchar(8)) + ' ' + [Descripcion] NomSubtema FROM [Subtema] WHERE ([IdTema] = @IdTema)
                                and IdSubtema NOT IN (SELECT IdSubtema FROM DetalleEvaluacion WHERE IdEvaluacion = @IdEvaluacion)
order by [Numero],[Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLtemas" Name="IdTema"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>

