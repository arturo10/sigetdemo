﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="Buscaprofesor.aspx.vb" 
    Inherits="superadministrador_Buscaprofesor" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style23
        {
            width: 382px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
        .style15
        {
            width: 382px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
        .style19
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
        .style24
        {
            width: 382px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }
        .style18
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }
        .style20
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }
        .style21
        {
            font-size: xx-small;
        }
        .style26
        {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            color: #000066;
            height: 8px;
        }
        .style37
        {
            width: 382px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            color: #000000;
        }
        .style38
        {
            width: 382px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000000;
        }
        </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Buscar un 
								<asp:Label ID="Label1" runat="server" Text="[PROFESOR]" />
		</h1>
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style10">
        <tr>
            <td class="style15">
                &nbsp;</td>
            <td class="style26">
                Capture uno o varios de los datos siguientes para realizar la búsqueda:</td>
            <td>
                &nbsp;</td>
            <td class="style19">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style37">
                Matrícula o Clave</td>
            <td class="style16" style="text-align:left;">
                <asp:TextBox ID="TBclave" runat="server" Width="220px"></asp:TextBox>
            </td>
            <td class="style18">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style37">
                Nombre</td>
            <td class="style16" style="text-align:left;">
                <asp:TextBox ID="TBnombre" runat="server" Width="220px"></asp:TextBox>
            </td>
            <td class="style18">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style37">
                Apellidos</td>
            <td class="style16" style="text-align:left;">
                <asp:TextBox ID="TBapellidos" runat="server" Width="220px"></asp:TextBox>
            </td>
            <td class="style18">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style37">
                Login</td>
            <td class="style16" style="text-align:left;">
                <asp:TextBox ID="TBlogin" runat="server" Width="220px"></asp:TextBox>
            </td>
            <td class="style18">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style22">
                &nbsp;</td>
            <td class="style16">
                <asp:Button ID="BtnBuscar" runat="server" Text="Buscar" 
										CssClass="defaultBtn btnThemeBlue btnThemeMedium"/>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="4">
                <asp:GridView ID="GVprofesores" runat="server" 
                    DataSourceID="SDSbuscar" 
                    Caption="<h3>Listado de PROFESORES encontrados</h3>" 
                    AutoGenerateColumns="false"

                    CellPadding="3" 
                    ForeColor="Black" 
                    style="font-family: Arial, Helvetica, sans-serif; font-size: x-small" 
                    Width="883px" 
                    BackColor="White" 
                    BorderColor="#999999" 
                    BorderStyle="Solid" 
                    BorderWidth="1px" 
                    GridLines="Vertical">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="IdProfesor" HeaderText="Id_[Profesor]"/>
                        <asp:BoundField DataField="Clave" HeaderText="Clave"/>
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre"/>
                        <asp:BoundField DataField="Apellidos" HeaderText="Apellidos"/>
                        <asp:BoundField DataField="Plantel" HeaderText="[Plantel]" />
                        <asp:BoundField DataField="Login" HeaderText="Login" />
                        <asp:BoundField DataField="Password" HeaderText="Password" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
                <%-- sort NO - prerender
	                0  select
	                1  P.IdPROFESOR
	                2  P.Clave
	                3  P.Nombre
	                4  P.Apellidos
	                5  Pl.Descripcion as PLANTEL
	                6  U.Login
	                7  U.Password
                    8  P.Estatus
	                --%>
            </td>
        </tr>
        <tr>
            <td class="style22">
                <asp:SqlDataSource ID="SDSbuscar" runat="server"></asp:SqlDataSource>
            </td>
            <td colspan="2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style23">
                Actualizar password</td>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError1" />
            </td>
        </tr>
        <tr>
            <td class="style24">
                <asp:Label ID="Label2" runat="server" Text="[PROFESOR]" />
								 seleccionado:</td>
            <td colspan="3">
                <asp:Label ID="LblProfesor" runat="server" 
                    
                    
                    
                    style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #000099"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style24">
                Password</td>
            <td colspan="2">
                <asp:TextBox ID="TBpasswordOK" runat="server" CssClass="style20"></asp:TextBox>
            </td>
            <td>
                            &nbsp;</td>
        </tr>
        <tr>
            <td class="style24">
                Email <span class="style21">(solo cuando se genere)</span></td>
            <td colspan="2">
                <asp:TextBox ID="TBemailOK" runat="server" CssClass="style20" Width="258px"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style22">
                &nbsp;</td>
            <td colspan="2">
                <asp:Button ID="BtnActualizar" runat="server" Text="Actualizar" 
										CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
								&nbsp;
                <asp:Button ID="BtnGenerar" runat="server" Text="Generar" 
										CssClass="defaultBtn btnThemeGrey btnThemeMedium"/>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSusuarios" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                                SelectCommand="SELECT * FROM [Usuario]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style22">
				<asp:HyperLink ID="HyperLink2" runat="server"
						NavigateUrl="~/"
						CssClass="defaultBtn btnThemeGrey btnThemeWide">
						Regresar&nbsp;al&nbsp;Menú
				</asp:HyperLink>
            </td>
            <td colspan="2">
                &nbsp;</td>
            <td>
                            &nbsp;</td>
        </tr>
    </table>
</asp:Content>

