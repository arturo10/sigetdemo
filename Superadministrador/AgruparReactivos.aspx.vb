﻿Imports Siget

Partial Class superadministrador_AgruparReactivos
    Inherits System.Web.UI.Page

#Region "Init"

    Protected Sub Page_Load(
                    sender As Object,
                    e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' modifica la visiblidad de la página para usuarios administradores de sistema
        If Roles.IsUserInRole(User.Identity.Name, "sysadmin") Then
            presentaElementosOcultos()
        End If

        ' personaliza las etiquetas de la página
        Label1.Text = Config.Etiqueta.CICLO
        Label2.Text = Config.Etiqueta.NIVEL
        Label3.Text = Config.Etiqueta.GRADO
        Label4.Text = Config.Etiqueta.ASIGNATURA
        Label5.Text = Config.Etiqueta.NIVEL
        Label6.Text = Config.Etiqueta.GRADO
        Label7.Text = Config.Etiqueta.ASIGNATURA
    End Sub

    ' se encarga de mostrar todos los elementos que sólo
    ' deben ser visibles a usuarios privilegiados
    Protected Sub presentaElementosOcultos()
        GVevaluaciones.Columns(1).Visible = True

        GVdetalleevaluacion.Columns(1).Visible = True
        GVdetalleevaluacion.Columns(7).Visible = True
        GVdetalleevaluacion.Columns(8).Visible = True
    End Sub

#End Region
    ' *********************************************************************************************

    Protected Sub GVevaluaciones_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevaluaciones.SelectedIndexChanged
        Session("IdEvaluacion") = GVevaluaciones.SelectedDataKey(0).ToString()
        Session("IdCalificacion") = GVevaluaciones.SelectedDataKey(1).ToString()
        Agregar.Visible = True
        Quitar.Visible = True
    End Sub

    Protected Sub Agregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Agregar.Click
        msgError.hide()

        If Not DDLsubtemas.SelectedValue = 0 Then
            Try
                SDSdetalleevaluacion.InsertCommand = "SET dateformat dmy; INSERT INTO DetalleEvaluacion (IdEvaluacion,IdSubtema, FechaModif, Modifico) VALUES (" + GVevaluaciones.SelectedDataKey(0).ToString() + "," + DDLsubtemas.SelectedValue + ", getdate(),'" & User.Identity.Name & "')"
                SDSdetalleevaluacion.Insert()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try

            DDLsubtemas.DataBind()
            GVdetalleevaluacion.DataBind()
        End If
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLcalificacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacion.DataBound
        DDLcalificacion.Items.Insert(0, New ListItem("---Elija una calificación", 0))
    End Sub

    Protected Sub Quitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Quitar.Click
        msgError.hide()

        Try
            SDSdetalleevaluacion.DeleteCommand = "DELETE FROM DetalleEvaluacion where IdDetalleEvaluacion = " + GVdetalleevaluacion.SelectedDataKey(0).ToString() 'Me da el IdDetalleEvaluacion porque es el campo clave de la fila seleccionada
            SDSdetalleevaluacion.Delete()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
        DDLsubtemas.DataBind()
        GVdetalleevaluacion.DataBind()
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija la " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Dim Suma As UInteger = 0
    Dim SumaUsados As UInteger = 0

    Protected Sub GVdetalleevaluacion_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVdetalleevaluacion.RowDataBound
        msgError.hide()

        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                If Not IsDBNull(DataBinder.Eval(e.Row.DataItem, "Reactivos")) Then
                    Suma += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Reactivos"))
                End If
                If Not IsDBNull(DataBinder.Eval(e.Row.DataItem, "UsarReactivos")) Then
                    SumaUsados += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "UsarReactivos"))
                Else
                    SumaUsados += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Reactivos"))
                End If
            End If

            If e.Row.RowType = DataControlRowType.Footer Then
                e.Row.Cells(4).Text = "TOTAL REACTIVOS:"
                e.Row.Cells(4).HorizontalAlign = HorizontalAlign.Right
                'e.Row.Cells(4).Text = dTotal.ToString("c") 'Si lo uso así queda como moneda (currency: $0.00)

                e.Row.Cells(5).Text = Suma.ToString("0")
                e.Row.Cells(5).HorizontalAlign = HorizontalAlign.Center

                e.Row.Cells(6).Text = SumaUsados.ToString("0")
                e.Row.Cells(6).HorizontalAlign = HorizontalAlign.Center

                e.Row.Font.Bold = True
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVdetalleevaluacion, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    Protected Sub BtnAsignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAsignar.Click
        msgError.hide()

        Try
            Dim usa As Byte
            If LBusar.SelectedValue = "NULL" Then
                usa = 0 'Esto lo hago para que entre en la condición siguiente
            Else
                usa = LBusar.SelectedValue
            End If

            If usa < (GVdetalleevaluacion.SelectedRow.Cells(5).Text.Trim()) Then
                SDSdetalleevaluacion.UpdateCommand = "SET dateformat dmy; UPDATE DetalleEvaluacion SET UsarReactivos = " + LBusar.SelectedValue + ", FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdDetalleEvaluacion = " + GVdetalleevaluacion.SelectedDataKey(0).ToString()
                SDSdetalleevaluacion.Update()
                GVevaluaciones.DataBind()
            Else
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "No puede asignar igual o más reactivos aleatorios del total que tiene actualmente el Subtema.")
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVdetalleevaluacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVdetalleevaluacion.DataBound
        If GVdetalleevaluacion.Rows.Count > 0 Then
            BtnAsignar.Enabled = True
        Else
            BtnAsignar.Enabled = False
        End If
    End Sub

    Protected Sub ddlNivelesSubtemas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlNivelesSubtemas.DataBound
        ddlNivelesSubtemas.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub ddlGradosSubtemas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGradosSubtemas.DataBound
        ddlGradosSubtemas.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub ddlAsignaturasSubtemas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAsignaturasSubtemas.DataBound
        ddlAsignaturasSubtemas.Items.Insert(0, New ListItem("---Elija la " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLtemas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLtemas.DataBound
        DDLtemas.Items.Insert(0, New ListItem("---Elija el Tema", 0))
    End Sub

    Protected Sub DDLsubtemas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLsubtemas.DataBound
        DDLsubtemas.Items.Insert(0, New ListItem("---Elija el Subtema", 0))
    End Sub

    Protected Sub GVdetalleevaluacion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVdetalleevaluacion.SelectedIndexChanged
        If Not GVdetalleevaluacion.SelectedRow.Cells(6).Text = "&nbsp;" Then
            LBusar.SelectedValue = GVdetalleevaluacion.SelectedRow.Cells(6).Text
        Else
            LBusar.SelectedIndex = 0
        End If
    End Sub

    ' *********************************************************************************************

    Protected Sub GVdetalleevaluacion_PreRender(sender As Object, e As EventArgs) Handles GVdetalleevaluacion.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVdetalleevaluacion.Rows.Count > 0 Then
            GVdetalleevaluacion.HeaderRow.TableSection = TableRowSection.TableHeader
            GVdetalleevaluacion.FooterRow.TableSection = TableRowSection.TableFooter
        End If
    End Sub

    Protected Sub GVdetalleevaluacion_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVdetalleevaluacion.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVdetalleevaluacion.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  ' *********************************************************************************************

  Protected Sub GVevaluaciones_PreRender(sender As Object, e As EventArgs) Handles GVevaluaciones.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVevaluaciones.Rows.Count > 0 Then
      GVevaluaciones.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVevaluaciones_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVevaluaciones.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

    Protected Sub GVevaluaciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVevaluaciones, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    ' *********************************************************************************************

#Region "Página"

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVevaluaciones.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(
                                                             GVevaluaciones,
                                                             "Select$" + r.RowIndex.ToString()
                                                             ))

            End If
        Next

        For Each r In GVdetalleevaluacion.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(
                                                             GVdetalleevaluacion,
                                                             "Select$" + r.RowIndex.ToString()
                                                             ))

            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

#End Region

End Class
