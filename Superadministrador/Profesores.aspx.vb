﻿Imports Siget

Imports System.Data
Imports System.IO
Imports System.Data.OleDb 'Este es para MS Access

Partial Class superadministrador_Profesores
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
    Utils.Sesion.sesionAbierta()

    GVprofesores.Caption = "<h3>" &
        Config.Etiqueta.PROFESORES &
        " capturad" & Config.Etiqueta.LETRA_PROFESOR & "s</h3>"

    Label1.Text = Config.Etiqueta.PROFESORES
    Label2.Text = Config.Etiqueta.PROFESOR
    Label3.Text = Config.Etiqueta.INSTITUCION
    Label4.Text = Config.Etiqueta.PLANTEL
  End Sub

    Protected Sub Guardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        Try
            'Lo siguiente lo hago porque meter una fecha en blanco marca error
            If Trim(TBingreso.Text) = "" Then
                SDSprofesores.InsertCommand = "SET dateformat dmy; INSERT INTO Profesor (IdPlantel,Clave,Nombre,Apellidos,RFC,Email,Estatus, FechaModif, Modifico) VALUES (" + DDLplantel.SelectedValue + ",'" + TBclave.Text + "','" + TBnombre.Text + "','" + TBapellidos.Text + "','" + TBrfc.Text + "','" + TBemail.Text + "','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
            Else
                SDSprofesores.InsertCommand = "SET dateformat dmy; INSERT INTO Profesor (IdPlantel,Clave,Nombre,Apellidos,RFC,Email,FechaIngreso,Estatus, FechaModif, Modifico) VALUES (" + DDLplantel.SelectedValue + ",'" + TBclave.Text + "','" + TBnombre.Text + "','" + TBapellidos.Text + "','" + TBrfc.Text + "','" + TBemail.Text + "','" + TBingreso.Text + "','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
            End If
            SDSprofesores.Insert()

            msgError.hide()
            Mensaje.Text = "El registro ha sido guardado"
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            Mensaje.Text = ""
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

  Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click
    TBclave.Text = ""
    TBnombre.Text = ""
    TBapellidos.Text = ""
    TBrfc.Text = ""
    TBemail.Text = ""
    TBingreso.Text = ""
    DDLestatus.ClearSelection()
    Mensaje.Text = "Capture los datos"
  End Sub

  Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
    DDLinstitucion.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_INSTITUCION & " " & Config.Etiqueta.INSTITUCION, 0))
  End Sub

  Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
    DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
  End Sub

  Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
    If Trim(GVprofesores.SelectedRow.Cells(9).Text) <> "Activo" Then
      Try
        Dim objCommand As New SqlClient.SqlCommand("sp_borraprofesor"), Conexion As String = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.Add("@Login", SqlDbType.VarChar, 0)
        objCommand.Parameters("@Login").Value = GVprofesores.SelectedRow.Cells(7).Text
        objCommand.Parameters.Add("@IdProfesor", SqlDbType.Int, 8)
        objCommand.Parameters("@IdProfesor").Value = CInt(GVprofesores.SelectedRow.Cells(1).Text)
        objCommand.CommandTimeout = 3000
        objCommand.Connection = New SqlClient.SqlConnection(Conexion)
        objCommand.Connection.Open()
        ' Crear el DataReader
        Dim Datos As SqlClient.SqlDataReader
        ' Con el método ExecuteReader() del comando se traen los datos
        Datos = objCommand.ExecuteReader()

        Membership.DeleteUser(GVprofesores.SelectedRow.Cells(7).Text.Trim())

        GVprofesores.DataBind()
        'GVAlumnos.DataSource = Datos
        msgError.hide()
        Mensaje.Text = "El registro ha sido eliminado"
      Catch ex As Exception
        Utils.LogManager.ExceptionLog_InsertEntry(ex)
        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        Mensaje.Text = ""
      End Try
    Else
      msgError.show("El " & Config.Etiqueta.PROFESOR & " no puede ser eliminado si está con el estatus de Activo")
      Mensaje.Text = ""
    End If
  End Sub

  Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
    Try
      If Trim(TBingreso.Text) = "" Then
        SDSprofesores.UpdateCommand = "SET dateformat dmy; UPDATE Profesor SET Clave = '" + TBclave.Text + "', Nombre = '" + TBnombre.Text + "', Apellidos = '" + TBapellidos.Text + "', RFC = '" + TBrfc.Text + "', Email = '" + TBemail.Text + "', FechaIngreso = NULL, Estatus = '" + DDLestatus.SelectedValue + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdProfesor = " + GVprofesores.SelectedValue.ToString
        SDSprofesores.Update()
      Else
        SDSprofesores.UpdateCommand = "SET dateformat dmy; UPDATE Profesor SET Clave = '" + TBclave.Text + "', Nombre = '" + TBnombre.Text + "', Apellidos = '" + TBapellidos.Text + "', RFC = '" + TBrfc.Text + "', Email = '" + TBemail.Text + "', FechaIngreso = '" + TBingreso.Text + "', Estatus = '" + DDLestatus.SelectedValue + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdProfesor = " + GVprofesores.SelectedValue.ToString
        SDSprofesores.Update()
      End If
      msgError.hide()
      Mensaje.Text = "El registro ha sido actualizado"
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      Mensaje.Text = ""
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub GVprofesores_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVprofesores.RowDataBound
    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(0)
      LnkHeaderText.Text = "Id_" & Config.Etiqueta.PROFESOR
    End If
  End Sub

  Protected Sub GVprofesores_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVprofesores.SelectedIndexChanged
    'La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
    TBclave.Text = HttpUtility.HtmlDecode(Trim(GVprofesores.SelectedRow.Cells(2).Text))
    TBnombre.Text = HttpUtility.HtmlDecode(Trim(GVprofesores.SelectedRow.Cells(3).Text))
    TBapellidos.Text = HttpUtility.HtmlDecode(Trim(GVprofesores.SelectedRow.Cells(4).Text))
    TBrfc.Text = HttpUtility.HtmlDecode(Trim(GVprofesores.SelectedRow.Cells(5).Text))
    TBemail.Text = HttpUtility.HtmlDecode(Trim(GVprofesores.SelectedRow.Cells(6).Text))
    If Len(Trim(HttpUtility.HtmlDecode(Trim(GVprofesores.SelectedRow.Cells(10).Text)))) > 1 Then 'Para que no ponga el caracter en blanco
      TBingreso.Text = HttpUtility.HtmlDecode(Trim(GVprofesores.SelectedRow.Cells(10).Text))
    Else
      TBingreso.Text = ""
    End If
    DDLestatus.SelectedValue = Trim(GVprofesores.SelectedRow.Cells(9).Text)
  End Sub
End Class
