﻿Imports Siget

Imports System.Data.SqlClient

Partial Class superadministrador_Licencias
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

    End Sub

    Protected Sub gvLicencias_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvLicencias.SelectedIndexChanged
        msgError.hide()
        msgSuccess.hide()

        tbClave.Text = HttpUtility.HtmlDecode(gvLicencias.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(gvLicencias, "Clave")).Text)

        If gvLicencias.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(gvLicencias, "Fecha de Alta (dd/mm/aaaa)")).Text.Equals("&nbsp;") Then
            tbAlta.Text = ""
        Else
            tbAlta.Text = DateTime.Parse(gvLicencias.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(gvLicencias, "Fecha de Alta (dd/mm/aaaa)")).Text).ToShortDateString()
        End If

        If gvLicencias.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(gvLicencias, "Fecha de Expiración (dd/mm/aaaa)")).Text.Equals("&nbsp;") Then
            tbExpiracion.Text = ""
        Else
            tbExpiracion.Text = DateTime.Parse(gvLicencias.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(gvLicencias, "Fecha de Expiración (dd/mm/aaaa)")).Text).ToShortDateString()
        End If

        ddlEstatus.SelectedIndex = ddlEstatus.Items.IndexOf(ddlEstatus.Items.FindByText(Trim(gvLicencias.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(gvLicencias, "Estatus")).Text)))
    End Sub

    ' comprobación básica de los campos en la forma 
    Protected Sub verificaCampos()
        If String.IsNullOrEmpty(Trim(tbClave.Text)) Then
            Throw New System.Exception("Debe definir una clave.")
        End If

        If String.IsNullOrEmpty(Trim(tbExpiracion.Text)) Then
            Throw New System.Exception("Debe indicar una fecha de expiración.")
        End If
    End Sub

    Protected Sub btnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            ' antes de operar verifico los campos
            verificaCampos()

            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("SET dateformat dmy; INSERT INTO Licencia (Clave, FechaAlta, FechaExpira, Estatus, FechaModif, Modifico) VALUES (@Clave, @FechaAlta, @FechaExpira, @Estatus, getdate(), @Modifico)", conn)

                cmd.Parameters.AddWithValue("@Clave", tbClave.Text)
                
                If Trim(tbAlta.Text).Equals("") Then
                    cmd.Parameters.AddWithValue("@FechaAlta", "getdate()")
                Else
                    cmd.Parameters.AddWithValue("@FechaAlta", tbAlta.Text)
                End If

                If Trim(tbExpiracion.Text).Equals("") Then
                    cmd.Parameters.AddWithValue("@FechaExpira", "getdate()")
                Else
                    cmd.Parameters.AddWithValue("@FechaExpira", tbExpiracion.Text)
                End If
                cmd.Parameters.AddWithValue("@Estatus", ddlEstatus.SelectedValue)
                cmd.Parameters.AddWithValue("@Modifico", User.Identity.Name)

                cmd.ExecuteNonQuery()
                cmd.Dispose()
                conn.Close()

                msgSuccess.show("Éxito", "Se creó la licencia " & tbClave.Text)
                gvLicencias.DataBind()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("SET dateformat dmy; DELETE FROM Licencia WHERE IdLicencia = @IdLicencia", conn)

                cmd.Parameters.AddWithValue("@IdLicencia", gvLicencias.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(gvLicencias, "Id en Sistema")).Text)

                cmd.ExecuteNonQuery()
                cmd.Dispose()
                conn.Close()

                msgSuccess.show("Éxito", "Se eliminó la licencia " & gvLicencias.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(gvLicencias, "Clave")).Text)
                gvLicencias.DataBind()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            ' antes de operar verifico los campos
            verificaCampos()

            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("SET dateformat dmy; UPDATE Licencia SET Clave = @Clave, FechaAlta = @FechaAlta, FechaExpira = @FechaExpira, Estatus = @Estatus, FechaModif = getdate(), Modifico = @Modifico WHERE IdLicencia = @IdLicencia", conn)

                cmd.Parameters.AddWithValue("@Clave", tbClave.Text)
                cmd.Parameters.AddWithValue("@FechaAlta", tbAlta.Text)
                cmd.Parameters.AddWithValue("@FechaExpira", tbExpiracion.Text)
                cmd.Parameters.AddWithValue("@Estatus", ddlEstatus.SelectedValue)
                cmd.Parameters.AddWithValue("@Modifico", User.Identity.Name)
                cmd.Parameters.AddWithValue("@IdLicencia", gvLicencias.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(gvLicencias, "Id en Sistema")).Text)

                cmd.ExecuteNonQuery()
                cmd.Dispose()
                conn.Close()

                msgSuccess.show("Éxito", "Se actualizó la licencia " & tbClave.Text)
                gvLicencias.DataBind()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    ' Métodos de GridViews
    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub gvLicencias_PreRender(sender As Object, e As EventArgs) Handles gvLicencias.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If gvLicencias.Rows.Count > 0 Then
            gvLicencias.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub gvLicencias_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles gvLicencias.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If gvLicencias.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub gvLicencias_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvLicencias.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(gvLicencias, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In gvLicencias.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(gvLicencias, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub
End Class
