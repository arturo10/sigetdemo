﻿Imports Siget

Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Partial Class superadministrador_GeneraProg
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.PROFESORES
        Label2.Text = Config.Etiqueta.ASIGNATURAS
        Label3.Text = Config.Etiqueta.GRUPOS
        Label4.Text = Config.Etiqueta.CICLO
        Label5.Text = Config.Etiqueta.ASIGNATURAS
        Label6.Text = Config.Etiqueta.GRUPOS
        Label7.Text = Config.Etiqueta.PROFESORES
        Label8.Text = Config.Etiqueta.CICLO
    End Sub

    Protected Sub Importar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Importar.Click
        If ArchivoCarga.HasFile Then 'El atributo .HasFile compara si se indico un archivo           
            Try
                LblMensaje.Text = "Espere un momento por favor, la migración puede tardar varios minutos..."
                'Ahora CARGO el archivo
                ArchivoCarga.SaveAs(Server.MapPath("~") + "\superadministrador\" & ArchivoCarga.FileName)
                'Leo el archivo
                Dim archivo_datos As String = Server.MapPath(ArchivoCarga.FileName())
                Dim leer_archivo As StreamReader
                leer_archivo = File.OpenText(archivo_datos)
                Dim LineaLeida, IdAsignatura, IdProfesor, IdCicloEscolar, IdGrupo As String
                Dim I, F As Integer

                While Not leer_archivo.EndOfStream
                    'Leo el registro completo y ubico cada campo
                    LineaLeida = leer_archivo.ReadLine
                    I = InStr(LineaLeida, ",")
                    F = InStr(I + 1, LineaLeida, ",")
                    IdAsignatura = Trim(Mid(LineaLeida, 1, I - 1)) 'CAMPO 1
                    IdProfesor = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 2
                    I = F
                    F = InStr(I + 1, LineaLeida, ",")
                    IdCicloEscolar = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 3
                    I = F
                    IdGrupo = Trim(Mid(LineaLeida, I + 1, Len(LineaLeida))) 'CAMPO 4 (Ultimo)

                    'Ahora ingreso el registro del Profesor
                    SDSprogramacion.InsertCommand = "SET dateformat dmy; INSERT INTO Programacion (IdAsignatura, IdProfesor, IdCicloEscolar, IdGrupo, FechaModif) VALUES (" + IdAsignatura + "," + IdProfesor + "," + IdCicloEscolar + "," + IdGrupo + ",getdate())"
                    SDSprogramacion.Insert()
                End While
                'objConexion.Close()
                leer_archivo.Close()
                msgError.hide()
                LblMensaje.Text = "La migración ha terminado con éxito, revise los registros."
                'Elimino el archivo utilizado para la migración
                Dim MiArchivo As FileInfo = New FileInfo(archivo_datos)
                MiArchivo.Delete()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                LblMensaje.Text = ""
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try
        Else
            msgError.show("Seleccione el archivo que contiene los datos para Migrar")
        End If
    End Sub
End Class
