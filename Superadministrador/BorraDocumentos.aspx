﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="BorraDocumentos.aspx.vb" 
    Inherits="superadministrador_BorraDocumentos" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            width: 900px;
        }

        .style30 {
            font-family: Arial, Helvetica, sans-serif;
        }

        .style21 {
            font-size: x-small;
        }

        .style40 {
        }

        .style41 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style42 {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: small;
        }

        .style44 {
            color: #CC0000;
            font-weight: bold;
            width: 155px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style45 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            height: 34px;
        }

        .style46 {
            height: 34px;
        }

        .style47 {
            height: 34px;
            text-align: right;
            width: 209px;
        }

        .style48 {
            text-align: center;
        }

        .style49 {
            width: 209px;
        }

        .style50 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            width: 209px;
        }

        .style51 {
            text-align: center;
            width: 209px;
        }

        .style52 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
            color: #000066;
            height: 27px;
        }

        .style53 {
            height: 37px;
        }

        .style54 {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: small;
            height: 25px;
            color: #000066;
        }

        .style55 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style56 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: xx-small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>
        Borrar Documentos de Actividades
    </h1>
    Permite eliminar los documentos enviados en una actividad específica, incluyendo sus resultados de haber sido calificados.
    <br />
    <br />Precaución: Los archivos eliminados no podrán ser restaurados sin respaldos.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style23">
        <tr>
            <td class="style40">&nbsp;</td>
            <td>&nbsp;</td>
            <td class="style49">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style55">
                <asp:Label ID="Label1" runat="server" Text="[CICLO]" />
            </td>
            <td>
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Width="300px"
                    CssClass="style41" Height="22px">
                </asp:DropDownList>
            </td>
            <td class="style50">
                <asp:Label ID="Label2" runat="server" Text="[NIVEL]" />
            </td>
            <td>
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Width="280px" Height="22px" CssClass="style41">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style55">
                <asp:Label ID="Label3" runat="server" Text="[GRADO]" />
            </td>
            <td>
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion"
                    DataValueField="IdGrado" Width="300px" Height="22px"
                    CssClass="style41">
                </asp:DropDownList>
            </td>
            <td class="style50">
                <asp:Label ID="Label4" runat="server" Text="[ASIGNATURA]" />
            </td>
            <td>
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSasignaturas" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Width="280px" Height="22px"
                    CssClass="style41">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style41">&nbsp;</td>
            <td class="style41">&nbsp;</td>
            <td class="style50">&nbsp;</td>
            <td class="style41">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <asp:Panel ID="pnlCalificacion" runat="server" Visible="false">
        <tr>
            <td class="style52" colspan="5" style="background: #ddd;">
                <b style="text-align: left">Seleccione un periodo de Calificación:</b></td>
        </tr>
        <tr>
            <td colspan="5" style="padding-bottom: 40px; padding-top: 20px;">
                <asp:GridView ID="GVcalificaciones" runat="server"
                    AllowPaging="True"
                    AutoGenerateColumns="False"
                    PageSize="5"

                    DataKeyNames="IdCalificacion,IdIndicador"
                    DataSourceID="SDScalificaciones"
                    Width="959px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdCalificacion" HeaderText="Id_Calificación"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdCalificacion" />
                        <asp:BoundField DataField="IdCicloEscolar" HeaderText="Id_[Ciclo]"
                            SortExpression="IdCicloEscolar" />
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" SortExpression="Descripcion" />
                        <asp:BoundField DataField="Clave" HeaderText="Clave" SortExpression="Clave" />
                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                            SortExpression="Consecutivo">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Materia" HeaderText="[Asignatura]"
                            SortExpression="Materia" />
                        <asp:BoundField DataField="Valor" HeaderText="Valor" SortExpression="Valor">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdIndicador" HeaderText="IdIndicador"
                            SortExpression="IdIndicador" Visible="False" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort NO - prerender
	                0  select
	                1  idcalificacion
	                2  id_CICLO
	                3  descripcion
	                4  clave
	                5  consecutivo
	                6  ASIGNATURA
	                7  valor
	                8  idindicador
                --%>
            </td>
        </tr>
        </asp:Panel>
        <asp:Panel ID="pnlEvaluacion" runat="server" Visible="false">
        <tr>
            <td class="style54" colspan="5" style="background: #ddd;">Ahora seleccione la Actividad cuyos documentos serán eliminados</td>
        </tr>
        <tr>
            <td class="style40" colspan="5" style="padding-bottom: 40px; padding-top: 20px;">
                <asp:GridView ID="GVevaluaciones" runat="server"
                    AutoGenerateColumns="False"
                    AllowSorting="True"

                    DataKeyNames="IdEvaluacion,Califica"
                    DataSourceID="SDSevaluaciones"
                    Width="885px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="true" SelectText="Seleccionar" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion" />
                        <asp:BoundField DataField="IdCalificacion"
                            HeaderText="IdCalificacion"
                            SortExpression="IdCalificacion" Visible="False" />
                        <asp:BoundField DataField="ClaveBateria" HeaderText="Clave de la Actividad"
                            SortExpression="ClaveBateria" />
                        <asp:BoundField DataField="ClaveAbreviada" HeaderText="Clave p/Reporte"
                            SortExpression="ClaveAbreviada" />
                        <%--<asp:BoundField DataField="Resultado" HeaderText="Resultado"
                            SortExpression="Resultado">
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>--%>
                        <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                            SortExpression="Porcentaje">
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                            HeaderText="Inicia" SortExpression="InicioContestar" />
                        <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                            HeaderText="Termina" SortExpression="FinContestar" />
                        <asp:BoundField DataField="FinSinPenalizacion"
                            DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Máxima"
                            SortExpression="FinSinPenalizacion" />
                        <asp:BoundField DataField="Penalizacion" HeaderText="% Penalización"
                            SortExpression="Penalizacion">
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                            SortExpression="Estatus" />
                        <asp:BoundField DataField="Aleatoria" HeaderText="Aleatoria"
                            SortExpression="Aleatoria" />
                        <asp:BoundField DataField="SeEntregaDocto" HeaderText="Documento"
                            SortExpression="SeEntregaDocto" />
                        <asp:BoundField DataField="Califica" HeaderText="Califica"
                            SortExpression="Califica" Visible="False" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        </asp:Panel>
        <tr>
            <td class="style41">&nbsp;</td>
            <td class="style41">&nbsp;</td>
            <td colspan="2">&nbsp;
                                <asp:Button ID="EliminarE" runat="server" Text="Eliminar Documentos"
                                    Visible="False" CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="5">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style41">&nbsp;</td>
            <td>&nbsp;</td>
            <td class="style49">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style40">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
            </td>
            <td class="style41">&nbsp;</td>
            <td class="style50">&nbsp;</td>
            <td class="style41">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar] WHERE ([Estatus] = 'Activo') ORDER BY [FechaInicio]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSIndicadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdIndicador], [Descripcion] FROM [Indicador] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT C.IdCalificacion, C.IdCicloEscolar, C.Clave, C.IdAsignatura, A.Descripcion Materia, C.Consecutivo,C.Descripcion, C.Valor, C.IdIndicador
FROM Calificacion C, Asignatura A
WHERE ([IdCicloEscolar] = @IdCicloEscolar)  and (A.IdAsignatura = C.IdAsignatura) and (A.IdAsignatura = @IdAsignatura)
ORDER BY A.Descripcion, C.Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Evaluacion] 
where IdCalificacion = @IdCalificacion
ORDER BY [IdEvaluacion] DESC">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVCalificaciones" Name="IdCalificacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdAsignatura], [IdGrado], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>

