﻿Imports Siget

Imports System.Data
Imports System.IO

Partial Class superadministrador_BorraRespuestas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

    End Sub

    Protected Sub BtnEjecutar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEjecutar.Click
        msgSuccess.hide()
        msgError.hide()

        Try
            Dim objCommand As New SqlClient.SqlCommand("BorraRespuestas"), Conexion As String = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Parameters.Add("@FechaInicial", SqlDbType.Date, 8)
            objCommand.Parameters("@FechaInicial").Value = CDate(TBinicial.Text)
            objCommand.Parameters.Add("@FechaFinal", SqlDbType.Date, 8)
            objCommand.Parameters("@FechaFinal").Value = CDate(TBfinal.Text)
            objCommand.CommandTimeout = 3000
            objCommand.Connection = New SqlClient.SqlConnection(Conexion)
            objCommand.Connection.Open()
            ' Crear el DataReader
            Dim Datos As SqlClient.SqlDataReader
            ' Con el método ExecuteReader() del comando se traen los datos
            Datos = objCommand.ExecuteReader()
            msgSuccess.show("Éxito", "Se han borrado todos los registros de respuestas entre las fechas dadas. Los registros de las ACTIVIDADES TERMINADAS (calificaciones) permanecen sin cambios.")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
        End Try
    End Sub

End Class
