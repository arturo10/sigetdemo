﻿Imports Siget

Imports System.Data.SqlClient ' comandos sql

Partial Class superadministrador_Evaluaciones
  Inherits System.Web.UI.Page

#Region "Init"

  ' Proceso de inicio de la página
  Protected Sub Page_Load(
                  sender As Object,
                  e As EventArgs) Handles Me.Load
    Utils.Sesion.sesionAbierta()

    ' modifica la visiblidad de la página para usuarios administradores de sistema
    If Roles.IsUserInRole(User.Identity.Name, "sysadmin") Then
      presentaElementosOcultos()
    End If

    ' personaliza las etiquetas de la página
    Label1.Text = Config.Etiqueta.CICLO
    Label2.Text = Config.Etiqueta.NIVEL
    Label3.Text = Config.Etiqueta.GRADO
    Label4.Text = Config.Etiqueta.ASIGNATURA
    Label10.Text = Config.Etiqueta.ASIGNATURA

    ' aquí inicializo la página la primera vez que se carga
    If Not IsPostBack Then
            UserInterface.Include.HtmlEditor(Literal1, 700, 180, 12, "es-mx")
    End If
  End Sub

  ' se encarga de mostrar todos los elementos que sólo
  ' deben ser visibles a usuarios privilegiados
  Protected Sub presentaElementosOcultos()
    GVevaluaciones.Columns(1).Visible = True
    GVevaluaciones.Columns(16).Visible = True
    GVevaluaciones.Columns(17).Visible = True
  End Sub

#End Region
  ' *********************************************************************************************
#Region "Triggers"

#Region "   ddlCicloEscolar"

  Protected Sub DDLcicloescolar_DataBound(
                  ByVal sender As Object,
                  ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
    DDLcicloescolar.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.CICLO, 0))
  End Sub

  Protected Sub DDLcicloescolar_SelectedIndexChanged(
                  sender As Object,
                  e As EventArgs) Handles DDLcicloescolar.SelectedIndexChanged
    ' con cada postback reinicia los avisos de la página
    msgSuccess.hide()
    msgError.hide()
    msgErrorDialog.hide()

    ' al cambiar el ciclo, reinicia los otros controles que dependen de él
    LimpiaCalificacion()
    ' muestra u oculta el panel de evaluaciones
    renderPnlEvaluaciones()
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   ddlNivel"

  Protected Sub DDLnivel_DataBound(
                  ByVal sender As Object,
                  ByVal e As System.EventArgs) Handles DDLnivel.DataBound
    DDLnivel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.NIVEL, 0))
  End Sub

  Protected Sub ddlNivel_SelectedIndexChanged(
                  sender As Object,
                  e As EventArgs) Handles DDLnivel.SelectedIndexChanged
    ' con cada postback reinicia los avisos de la página
    msgSuccess.hide()
    msgError.hide()
    msgErrorDialog.hide()

    ' al cambiar el nivel, reinicia los otros controles que dependen de él
    LimpiaGrado()
    LimpiaAsignatura()
    LimpiaCalificacion()
    LimpiaEvaluacion()
    ' y muestra u oculta el panel de evaluaciones
    renderPnlEvaluaciones()
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   ddlGrado"

  Protected Sub DDLgrado_DataBound(
                  ByVal sender As Object,
                  ByVal e As System.EventArgs) Handles DDLgrado.DataBound
    DDLgrado.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.GRADO, 0))
  End Sub

  Protected Sub ddlGrado_SelectedIndexChanged(
                  sender As Object,
                  e As EventArgs) Handles DDLgrado.SelectedIndexChanged
    ' con cada postback reinicia los avisos de la página
    msgSuccess.hide()
    msgError.hide()
    msgErrorDialog.hide()

    ' al cambiar de grado, reinicia los controles que dependen de el
    LimpiaAsignatura()
    LimpiaCalificacion()
    LimpiaEvaluacion()
    ' y muestra u oculta el panel de evaluaciones
    renderPnlEvaluaciones()
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   ddlAsignatura"

  Protected Sub DDLasignatura_DataBound(
                  ByVal sender As Object,
                  ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
    DDLasignatura.Items.Insert(0, New ListItem("---Elija una " & Config.Etiqueta.ASIGNATURA, 0))
  End Sub

  Protected Sub ddlAsignatura_SelectedIndexChanged(
                  sender As Object,
                  e As EventArgs) Handles DDLasignatura.SelectedIndexChanged
    ' con cada postback reinicia los avisos de la página
    msgSuccess.hide()
    msgError.hide()
    msgErrorDialog.hide()

    ' al cambiar de asignatura, reinicia los controles que dependen de ella
    LimpiaCalificacion()
    LimpiaEvaluacion()
    ' y muestra u oculta el panel de evaluaciones
    renderPnlEvaluaciones()
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   ddlCalificacion"

  Protected Sub ddlCalificacion_DataBound(
                  sender As Object,
                  e As EventArgs) Handles ddlCalificacion.DataBound
    ddlCalificacion.Items.Insert(0, New ListItem("---Elija una Calificación", 0))
  End Sub

  Protected Sub ddlCalificacion_SelectedIndexChanged(
                  sender As Object,
                  e As EventArgs) Handles ddlCalificacion.SelectedIndexChanged
    ' con cada postback reinicia los avisos de la página
    msgSuccess.hide()
    msgError.hide()
    msgErrorDialog.hide()

    ' al cambiar de calificación, reinicia los controles que dependen de ella
    LimpiaEvaluacion()
    ' y muestra u oculta el panel de evaluaciones
    renderPnlEvaluaciones()
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   ddlIndicadorRepeticion"

  Protected Sub ddlIndicadorRepeticion_DataBound(
                  ByVal sender As Object,
                  ByVal e As System.EventArgs) Handles ddlIndicadorRepeticion.DataBound
    ' añado un indicador inválido a la lista de opciones del usuario
    ddlIndicadorRepeticion.Items.Insert(0, New ListItem("---Ninguno", 0))
  End Sub

  Protected Sub ddlIndicadorRepeticion_SelectedIndexChanged(
                  sender As Object,
                  e As EventArgs) Handles ddlIndicadorRepeticion.SelectedIndexChanged
    ModalPopupEvaluacion.Show()
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   ddlResultadoRepeticion"

  Protected Sub ddlResultadoRepeticion_SelectedIndexChanged(
                  sender As Object,
                  e As EventArgs) Handles ddlResultadoRepeticion.SelectedIndexChanged
    ModalPopupEvaluacion.Show()
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   gvEvaluaciones"

  Dim Suma As Decimal = 0

  Protected Sub GVevaluaciones_PreRender(
                  sender As Object,
                  e As EventArgs) Handles GVevaluaciones.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVevaluaciones.Rows.Count > 0 Then
      GVevaluaciones.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVevaluaciones_RowCreated(
                  sender As Object,
                  e As GridViewRowEventArgs) Handles GVevaluaciones.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting
            ' (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVevaluaciones.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GVevaluaciones_RowDataBound(
                  ByVal sender As Object,
                  ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVevaluaciones.RowDataBound
    If (e.Row.Cells.Count > 1) And (e.Row.RowType = DataControlRowType.DataRow) Then  'Antes usaba "(e.Row.RowIndex > -1)" Para que se realice la comparacion cuando haya mas de una columna

      'ANTES USABA:
      'If IsNumeric(Trim(e.Row.Cells(6).Text)) Then 'para que no compare celdas vacias
      'Session("SumaEvaluaciones") = Session("SumaEvaluaciones") + CDbl(Trim(e.Row.Cells(6).Text))
      'End If

      'IMPORTANTE pornerle en el GridView la propiedad ShowFooter="True"
      If e.Row.RowType = DataControlRowType.DataRow Then
        Suma += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Porcentaje"))
                e.Row.Cells(2).Text = HttpUtility.HtmlDecode(e.Row.Cells(2).Text)
        ' muestro la columna de aleatorio
        If e.Row.Cells(11).Text = "0" Then
          e.Row.Cells(11).Text = "Secuencial"
        ElseIf e.Row.Cells(11).Text = "1" Then
          e.Row.Cells(11).Text = "Por Subtema"
        ElseIf e.Row.Cells(11).Text = "2" Then
          e.Row.Cells(11).Text = "Del Total"
        End If

      End If

      GVevaluaciones.Caption = "<font size=""2""><b>Los porcentajes de las Actividades suman: " + Suma.ToString + "%"
      If Suma > 100 Then
        GVevaluaciones.Caption = GVevaluaciones.Caption + "<br>(La SUMA no puede ser MAYOR a 100%)"
      End If
      GVevaluaciones.Caption = GVevaluaciones.Caption + "</b></font>"

    End If

    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick",
                                    Page.ClientScript.GetPostBackClientHyperlink(
                                        GVevaluaciones,
                                        "Select$" & e.Row.RowIndex
                                        ).ToString()
                                    )
    End If
  End Sub

  Protected Sub GVevaluaciones_SelectedIndexChanging(
                  sender As Object,
                  e As GridViewSelectEventArgs) Handles GVevaluaciones.SelectedIndexChanging
    ' revierto la columna de selección de la fila seleccionada
    revertirNombreColumnaSeleccion()

    ' si se seleccionó la misma fila que estaba seleccionada, deselecciónala
    If GVevaluaciones.SelectedIndex = e.NewSelectedIndex Then
      e.NewSelectedIndex = -1
    End If
  End Sub

  Protected Sub GVevaluaciones_SelectedIndexChanged(
                  ByVal sender As Object,
                  ByVal e As System.EventArgs) Handles GVevaluaciones.SelectedIndexChanged
    ' cambio selector "seleccionar" por "deseleccionar" en la fila seleccionada
    cambiarNombreColumnaSeleccion()

    ' si hay una fila seleccionada, muestra los controles de modificación y eliminación
    If Not GVevaluaciones.SelectedIndex = -1 Then
      btnModificar.Visible = True
      btnEliminar.Visible = True
    Else
      btnModificar.Visible = False
      btnEliminar.Visible = False
    End If
  End Sub

  Protected Sub GVevaluaciones_DataBound(
                  ByVal sender As Object,
                  ByVal e As System.EventArgs) Handles GVevaluaciones.DataBound
    ' determina si no hay evaluaciones para informar al usuario
    evaluaCantidadEvaluaciones()
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "  chkRepeticion"

  Protected Sub chkRepeticion_CheckedChanged(
                  sender As Object,
                  e As EventArgs) Handles chkRepeticion.CheckedChanged
    ModalPopupEvaluacion.Show()

    If chkRepeticion.Checked Then
      pnlRepeticion.Visible = True
    Else
      pnlRepeticion.Visible = False
    End If
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "  CBseentregadocto"

  Protected Sub CBseentregadocto_CheckedChanged(
                  sender As Object,
                  e As EventArgs) Handles CBseentregadocto.CheckedChanged
    ModalPopupEvaluacion.Show()

    If CBseentregadocto.Checked Then
      pnlReactivos.Visible = False
      ChkEsForo.Checked = False
    Else
      pnlReactivos.Visible = True
    End If
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "  CBseentregadocto"

  Protected Sub ChkEsForo_CheckedChanged(sender As Object, e As EventArgs) Handles ChkEsForo.CheckedChanged
    ModalPopupEvaluacion.Show()

    If ChkEsForo.Checked Then
      pnlReactivos.Visible = False
      CBseentregadocto.Checked = False
    Else
      pnlReactivos.Visible = True
    End If
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   btnAceptarCrear"

  Protected Sub btnAceptarCrear_Click(
                  sender As Object,
                  e As EventArgs) Handles btnAceptarCrear.Click, btnCrearModificar.Click
    ' con cada postback reinicia los avisos de la página
    msgSuccess.hide()
    msgError.hide()
    msgErrorDialog.hide()

    CreaEvaluacion()
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   btnAceptarModificar"

  Protected Sub btnAceptarModificar_Click(
                  sender As Object,
                  e As EventArgs) Handles btnAceptarModificar.Click
    ' con cada postback reinicia los avisos de la página
    msgSuccess.hide()
    msgError.hide()
    msgErrorDialog.hide()

    ModificaEvaluacion()
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   btnEliminar"

  Protected Sub btnEliminar_Click(
                  sender As Object,
                  e As EventArgs) Handles btnEliminar.Click
    ' con cada postback reinicia los avisos de la página
    msgSuccess.hide()
    msgError.hide()
    msgErrorDialog.hide()

    EliminaEvaluacion()
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   btnCancelar"

  Protected Sub btnCancelar_Click(
                  sender As Object,
                  e As EventArgs) Handles btnCancelar.Click
    ' el popup se esconde por su cuenta con el postback, pero aún así
    ' pongo la instrucción por claridad
    ModalPopupEvaluacion.Hide()

    msgErrorDialog.hide()
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   btnNueva"

  Protected Sub btnNueva_Click(
                  sender As Object,
                  e As EventArgs) Handles btnNueva.Click
    ModalPopupEvaluacion.Show() ' muestra el diálogo
    dialogoMuestraDatos() ' muestra el panel de datos del diálogo
    limpiaDatosEvaluacion() ' llena los datos con los seleccionados

    ' presenta el boton de aceptar modificacíon
    btnAceptarCrear.Visible = True
    btnAceptarModificar.Visible = False
    btnCrearModificar.Visible = False
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   btnModificar"

  Protected Sub btnModificar_Click(
                  sender As Object,
                  e As EventArgs) Handles btnModificar.Click
    ModalPopupEvaluacion.Show() ' muestra el diálogo
    dialogoMuestraDatos() ' muestra el panel de datos del diálogo
    llenaDatosEvaluacion() ' llena los datos con los seleccionados

    ' presenta el boton de aceptar modificacíon
    btnAceptarCrear.Visible = False
    btnAceptarModificar.Visible = True
    btnCrearModificar.Visible = True
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   btnDialogdatos"

  Protected Sub btnDialogDatos_Click(sender As Object, e As EventArgs) Handles btnDialogDatos.Click
    ModalPopupEvaluacion.Show()
    dialogoMuestraDatos()
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   btnDialogDinamica"

  Protected Sub btnDialogDinamica_Click(sender As Object, e As EventArgs) Handles btnDialogDinamica.Click
    ModalPopupEvaluacion.Show()
    dialogoMuestraDinamica()
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   btnDialogInstruccion"

  Protected Sub btnDialogInstruccion_Click(sender As Object, e As EventArgs) Handles btnDialogInstruccion.Click
    ModalPopupEvaluacion.Show()
    dialogoMuestraInstruccion()
  End Sub

#End Region
  ' _____________________________________________________________________________________________
#Region "   msgErrorDialog"

  Protected Sub msgErrorDialog_MessageClosed(sender As Object, e As EventArgs) Handles msgErrorDialog.MessageClosed
    ' si se cierra el mensaje de error en el diálogo, no ocultes el diálogo
    ModalPopupEvaluacion.Show()
  End Sub

#End Region

#End Region
  ' *********************************************************************************************
#Region "Métodos"

  ''' <summary>
  ''' muestra o esconde el panel de evaluaciones 
  ''' dependiendo de la selección de calificaciones
  ''' </summary>
  Protected Sub renderPnlEvaluaciones()
    If ddlCalificacion.SelectedValue = 0 Then
      pnlEvaluaciones.Visible = False
    Else
      pnlEvaluaciones.Visible = True
    End If
  End Sub

  Protected Sub evaluaCantidadEvaluaciones()
    If GVevaluaciones.Rows.Count = 0 AndAlso Not ddlCalificacion.SelectedIndex = 0 Then
      msgInfo.show("Esta calificación aún no tiene actividades")
    Else
      msgInfo.hide()
    End If
  End Sub

  ''' <summary>
  ''' reestablece el texto de la columna de selección de la actual fila seleccionada
  ''' </summary>
  Protected Sub revertirNombreColumnaSeleccion()
    If Not GVevaluaciones.SelectedIndex = -1 Then
      CType(GVevaluaciones.SelectedRow.Cells(0).Controls(0), LinkButton).Text = "Seleccionar"
    End If
  End Sub

  ''' <summary>
  ''' modifica el texto de la columna de selección de la actual fila seleccionada
  ''' </summary>
  Protected Sub cambiarNombreColumnaSeleccion()
    If Not GVevaluaciones.SelectedIndex = -1 Then
      CType(GVevaluaciones.SelectedRow.Cells(0).Controls(0), LinkButton).Text = "Deseleccionar"
    End If
  End Sub

  ''' <summary>
  ''' deselecciona el grado
  ''' </summary>
  Protected Sub LimpiaGrado()
    DDLgrado.SelectedIndex = -1
  End Sub

  ''' <summary>
  ''' deselecciona la asignatura
  ''' </summary>
  Protected Sub LimpiaAsignatura()
    DDLasignatura.SelectedIndex = -1
  End Sub

  ''' <summary>
  ''' deselecciona la calificación
  ''' </summary>
  Protected Sub LimpiaCalificacion()
    ddlCalificacion.SelectedIndex = -1
  End Sub
  Protected Sub LimpiaEvaluacion()
    GVevaluaciones.SelectedIndex = -1
    GVevaluaciones_SelectedIndexChanged(Nothing, Nothing)
  End Sub

  ''' <summary>
  ''' limpia los campos de creación/modificación de evaluaciones
  ''' </summary>
  Protected Sub limpiaDatosEvaluacion()
    msgErrorDialog.hide()

    TBclavebateria.Text = ""
    TBporcentaje.Text = ""
    TBclaveE.Text = ""
    DDLtipo.SelectedIndex = -1
    TBpenalizacion.Text = ""
    DDLestatus.SelectedIndex = -1
    DDLcalifica.SelectedIndex = -1
    ddlAsignacion.SelectedIndex = -1
    CBseentregadocto.Checked = False
    CBseentregadocto_CheckedChanged(Nothing, Nothing)
    ddlAleatoria.SelectedIndex = -1
    chkRepeticion.Checked = False
    chkRepeticion_CheckedChanged(Nothing, Nothing)
    tbRepeticiones.Text = ""
    tbFechaInicio.Text = ""
    tbFechaPenalizacion.Text = ""
    tbFechaFin.Text = ""

    tbInstruccion.Text = ""
  End Sub

  ''' <summary>
  ''' Muestra el panel de datos en el diálogo de creación/modificación
  ''' </summary>
  Protected Sub dialogoMuestraDatos()
    pnlDatosEvaluacion_Datos.Visible = True
    pnlDatosEvaluacion_Dinamica.Visible = False
    pnlDatosEvaluacion_Instruccion.Visible = False
    btnDialogDatos.CssClass = "choiceButton choiceButton_Left btnThemeBlue btnThemeSlick"
    btnDialogDinamica.CssClass = "choiceButton choiceButton btnThemeGrey btnThemeSlick"
    btnDialogInstruccion.CssClass = "choiceButton choiceButton_Right btnThemeGrey btnThemeSlick"
  End Sub

  ''' <summary>
  ''' Muestra el panel de dinámica en el diálogo de creación/modificación
  ''' </summary>
  Protected Sub dialogoMuestraDinamica()
    pnlDatosEvaluacion_Datos.Visible = False
    pnlDatosEvaluacion_Dinamica.Visible = True
    pnlDatosEvaluacion_Instruccion.Visible = False
    btnDialogDatos.CssClass = "choiceButton choiceButton_Left btnThemeGrey btnThemeSlick"
    btnDialogDinamica.CssClass = "choiceButton choiceButton btnThemeBlue btnThemeSlick"
    btnDialogInstruccion.CssClass = "choiceButton choiceButton_Right btnThemeGrey btnThemeSlick"
  End Sub

  ''' <summary>
  ''' Muestra el panel de instrucción en el diálogo de creación/modificación
  ''' </summary>
  Protected Sub dialogoMuestraInstruccion()
    pnlDatosEvaluacion_Datos.Visible = False
    pnlDatosEvaluacion_Dinamica.Visible = False
    pnlDatosEvaluacion_Instruccion.Visible = True
    btnDialogDatos.CssClass = "choiceButton choiceButton_Left btnThemeGrey btnThemeSlick"
    btnDialogDinamica.CssClass = "choiceButton choiceButton btnThemeGrey btnThemeSlick"
    btnDialogInstruccion.CssClass = "choiceButton choiceButton_Right btnThemeBlue btnThemeSlick"
  End Sub

  ''' <summary>
  ''' obtiene los datos de la evaluación seleccionada y 
  ''' llena los campos del formato de creación/modificación con ellos
  ''' </summary>
  ''' <remarks></remarks>
  Protected Sub llenaDatosEvaluacion()
    msgErrorDialog.hide()

    'Aqui me falta mas bien pasar los valores del GVevaluaciones
    TBclavebateria.Text = HttpUtility.HtmlDecode(GVevaluaciones.SelectedRow.Cells(2).Text)
    TBporcentaje.Text = GVevaluaciones.SelectedRow.Cells(4).Text
    TBclaveE.Text = HttpUtility.HtmlDecode(GVevaluaciones.SelectedRow.Cells(3).Text)
    TBpenalizacion.Text = GVevaluaciones.SelectedRow.Cells(8).Text
    tbFechaInicio.Text = GVevaluaciones.SelectedRow.Cells(5).Text.Trim()
    tbFechaPenalizacion.Text = GVevaluaciones.SelectedRow.Cells(6).Text.Trim()
    tbFechaFin.Text = GVevaluaciones.SelectedRow.Cells(7).Text.Trim()
    DDLtipo.SelectedValue = HttpUtility.HtmlDecode(GVevaluaciones.SelectedRow.Cells(9).Text.Trim())
    DDLestatus.SelectedValue = GVevaluaciones.SelectedRow.Cells(10).Text.Trim()
        TBNoActividad.Text = IIf(GVevaluaciones.SelectedDataKey("NoActividad").ToString() <> "&nbsp;", GVevaluaciones.SelectedDataKey("NoActividad").ToString(), String.Empty)
        ddlAleatoria.SelectedValue = GVevaluaciones.SelectedDataKey(2).ToString()
        ddlOpcionAleatoria.SelectedValue = GVevaluaciones.SelectedDataKey(8).ToString()
        If Not IsDBNull(GVevaluaciones.SelectedDataKey(3)) Then
      tbInstruccion.Text = HttpUtility.HtmlDecode(GVevaluaciones.SelectedDataKey(3))
    End If

    'En la siguiente primero rellené las evaluaciones a False cuando actualicé el cambio a la tabla, por eso no necesité la comparación anterior
    CBseentregadocto.Checked = TryCast(GVevaluaciones.SelectedRow.Cells(12).Controls(0), CheckBox).Checked
    CBseentregadocto_CheckedChanged(Nothing, Nothing)

    ChkEsForo.Checked = Boolean.Parse(GVevaluaciones.SelectedDataKey(7).ToString())
    ChkEsForo_CheckedChanged(Nothing, Nothing)

    If GVevaluaciones.SelectedDataKey.Values(1).ToString = "P" Or GVevaluaciones.SelectedDataKey.Values(1).ToString = "C" Then
      DDLcalifica.SelectedValue = GVevaluaciones.SelectedDataKey.Values(1).ToString
    Else
      DDLcalifica.SelectedIndex = 0
    End If

    ' repetición automática
    chkRepeticion.Checked = TryCast(GVevaluaciones.SelectedRow.Cells(14).Controls(0), CheckBox).Checked
    chkRepeticion_CheckedChanged(Nothing, Nothing)
    If chkRepeticion.Checked Then
      ddlIndicadorRepeticion.DataBind()
      ddlIndicadorRepeticion.SelectedValue = GVevaluaciones.SelectedDataKey(4).ToString()
      ddlResultadoRepeticion.DataBind()
      ddlResultadoRepeticion.SelectedValue = GVevaluaciones.SelectedDataKey(5).ToString()
      tbRepeticiones.Text = GVevaluaciones.SelectedDataKey(6).ToString().Trim()
    End If

    chkSegundavuelta.Checked = TryCast(GVevaluaciones.SelectedRow.Cells(15).Controls(0), CheckBox).Checked

    msgError.hide()
  End Sub

#End Region
  ' *********************************************************************************************
#Region "Métodos de Proceso"

  ''' <summary>
  ''' Verifica e indica si los campos de creación/modificación 
  ''' son válidos para crear una evaluación 
  ''' </summary>
  ''' <returns>
  ''' cierto si son válidos, falso de lo contrario
  ''' </returns>
  Protected Function verificaCamposEvaluacion() As Boolean
    If TBporcentaje.Text.Trim() = "" Then
      TBporcentaje.Text = 0
    End If

    If TBclavebateria.Text.Trim() = String.Empty Then
      msgErrorDialog.show("La actividad debe tener un nombre.")
      ModalPopupEvaluacion.Show()
      Return False
    End If

    If TBclaveE.Text.Trim() = String.Empty Then
      msgErrorDialog.show("La calificación debe tener una clave para reportes.")
      ModalPopupEvaluacion.Show()
      Return False
    End If

    If TBpenalizacion.Text.Trim() = "" Then
      TBpenalizacion.Text = 0
    End If

    If chkRepeticion.Checked Then
      If Not Integer.TryParse(tbRepeticiones.Text.Trim(), Nothing) Or Integer.Parse(tbRepeticiones.Text.Trim()) > 100 Then
        msgErrorDialog.show("La actividad debe tener un número máximo de repeticiones menor a 100.")
        ModalPopupEvaluacion.Show()
        Return False
      End If
      If ddlIndicadorRepeticion.SelectedValue = "0" Then
        msgErrorDialog.show("La repetición automática debe estar anclada a un indicador.")
        ModalPopupEvaluacion.Show()
        Return False
      End If
    End If

    If Not Date.TryParse(tbFechaInicio.Text.Trim(), Nothing) Then
      msgErrorDialog.show("La fecha de inicio es inválida.")
      ModalPopupEvaluacion.Show()
      Return False
    End If
    If Not Date.TryParse(tbFechaPenalizacion.Text.Trim(), Nothing) Then
      msgErrorDialog.show("La fecha de penalización es inválida.")
      ModalPopupEvaluacion.Show()
      Return False
    End If
    If Not Date.TryParse(tbFechaFin.Text.Trim(), Nothing) Then
      msgErrorDialog.show("La fecha de fin es inválida.")
      ModalPopupEvaluacion.Show()
      Return False
    End If

    Return True
  End Function

  ''' <summary>
  ''' Intenta crear una evaluación con el formato de creación/modificación
  ''' </summary>
  Protected Sub CreaEvaluacion()
    If verificaCamposEvaluacion() Then
      Try
        SDSevaluaciones.InsertParameters.Clear()
                SDSevaluaciones.InsertCommand =
" SET dateformat dmy; " &
" INSERT INTO " &
" Evaluacion (ClaveBateria, ClaveAbreviada, IdCalificacion, Porcentaje, InicioContestar, " &
"           FinSinPenalizacion, FinContestar, Tipo, Penalizacion, Aleatoria, Estatus, " &
"           SeEntregaDocto, Califica, Instruccion, FechaModif, Modifico, " &
"           RepiteAutomatica, RepiteIndicador, RepiteRango, RepiteMaximo, PermiteSegunda, EsForo," &
            "OpcionesAleatorias,NoActividad) " &
" VALUES (@ClaveBateria, @ClaveAbreviada, @IdCalificacion, @Porcentaje, @InicioContestar, " &
"           @FinSinPenalizacion, @FinContestar, @Tipo, @Penalizacion, @Aleatoria, @Estatus, " &
"           @SeEntregaDocto, @Califica, @Instruccion, getdate(), @Modifico, " &
"           @RepiteAutomatica, @RepiteIndicador, @RepiteRango, @RepiteMaximo, @SegundaVuelta," &
            " @EsForo,@OpcionesAleatorias,@NoActividad)"
                SDSevaluaciones.InsertParameters.Add("ClaveBateria", HttpUtility.HtmlEncode(TBclavebateria.Text.Trim()))
        SDSevaluaciones.InsertParameters.Add("ClaveAbreviada", TBclaveE.Text.Trim())
        SDSevaluaciones.InsertParameters.Add("IdCalificacion", ddlCalificacion.SelectedValue)
        SDSevaluaciones.InsertParameters.Add("Porcentaje", TBporcentaje.Text.Trim())
        SDSevaluaciones.InsertParameters.Add("InicioContestar", tbFechaInicio.Text.Trim())
        SDSevaluaciones.InsertParameters.Add("FinSinPenalizacion", tbFechaPenalizacion.Text.Trim() & " 23:59:00")
        SDSevaluaciones.InsertParameters.Add("FinContestar", tbFechaFin.Text.Trim() & " 23:59:00")
        SDSevaluaciones.InsertParameters.Add("Tipo", DDLtipo.SelectedValue)
        SDSevaluaciones.InsertParameters.Add("Penalizacion", TBpenalizacion.Text)
        SDSevaluaciones.InsertParameters.Add("Aleatoria", ddlAleatoria.SelectedValue)
        SDSevaluaciones.InsertParameters.Add("Estatus", DDLestatus.SelectedValue)
        SDSevaluaciones.InsertParameters.Add("SeEntregaDocto", CBseentregadocto.Checked.ToString)
        SDSevaluaciones.InsertParameters.Add("Califica", DDLcalifica.SelectedValue)
        SDSevaluaciones.InsertParameters.Add("SegundaVuelta", chkSegundavuelta.Checked)
        SDSevaluaciones.InsertParameters.Add("Instruccion", tbInstruccion.Text)
        SDSevaluaciones.InsertParameters.Add("Modifico", User.Identity.Name)
        SDSevaluaciones.InsertParameters.Add("RepiteAutomatica", chkRepeticion.Checked)
                SDSevaluaciones.InsertParameters.Add("EsForo", ChkEsForo.Checked)
                SDSevaluaciones.InsertParameters.Add("OpcionesAleatorias", ddlOpcionAleatoria.SelectedValue)
                SDSevaluaciones.InsertParameters.Add("NoActividad", Trim(TBNoActividad.Text))
                If chkRepeticion.Checked Then
          SDSevaluaciones.InsertParameters.Add("RepiteIndicador", ddlIndicadorRepeticion.SelectedValue)
          SDSevaluaciones.InsertParameters.Add("RepiteRango", ddlResultadoRepeticion.SelectedValue)
          SDSevaluaciones.InsertParameters.Add("RepiteMaximo", Integer.Parse(tbRepeticiones.Text.Trim()))
        Else
          SDSevaluaciones.InsertParameters.Add("RepiteIndicador", Nothing)
          SDSevaluaciones.InsertParameters.Add("RepiteRango", Nothing)
          SDSevaluaciones.InsertParameters.Add("RepiteMaximo", Nothing)
        End If

        SDSevaluaciones.Insert()

        GVevaluaciones.DataBind()
        msgError.hide()
        msgSuccess.show("Éxito", "Se creó la evaluación.")
      Catch ex As Exception
        Utils.LogManager.ExceptionLog_InsertEntry(ex)
        msgErrorDialog.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")),
                            "Ocurrió un error desconocido. Si el problema persiste " &
                            "notifique al administrador del sistema.")
        ModalPopupEvaluacion.Show()
      End Try
    End If
  End Sub

  ''' <summary>
  ''' Intenta modificar una evaluación seleccionada con el formato de creación/modificación
  ''' </summary>
  Protected Sub ModificaEvaluacion()
    If verificaCamposEvaluacion() Then
      Try
        SDSevaluaciones.UpdateParameters.Clear()
                SDSevaluaciones.UpdateCommand =
" SET dateformat dmy; " &
" UPDATE Evaluacion " &
" SET ClaveBateria = @ClaveBateria, ClaveAbreviada = @ClaveAbreviada, IdCalificacion = @IdCalificacion, " &
"   Porcentaje = @Porcentaje, InicioContestar = @InicioContestar, FinSinPenalizacion = @FinSinPenalizacion, " &
"   FinContestar = @FinContestar, Tipo = @Tipo, Penalizacion = @Penalizacion, Aleatoria = @Aleatoria, " &
"   SeEntregaDocto = @SeEntregaDocto, Estatus = @Estatus, Califica = @Califica, FechaModif = getdate(), " &
"   Modifico = @Modifico, Instruccion = @Instruccion, RepiteAutomatica = @RepiteAutomatica, " &
"   RepiteIndicador = @RepiteIndicador, RepiteRango = @RepiteRango, RepiteMaximo = @RepiteMaximo, " &
"   PermiteSegunda = @SegundaVuelta, EsForo = @EsForo, OpcionesAleatorias=@OpcionesAleatorias, NoActividad=@NoActividad " &
" WHERE IdEvaluacion = @IdEvaluacion"
                SDSevaluaciones.UpdateParameters.Add("ClaveBateria", HttpUtility.HtmlEncode(TBclavebateria.Text.Trim()))
        SDSevaluaciones.UpdateParameters.Add("ClaveAbreviada", TBclaveE.Text.Trim())
        SDSevaluaciones.UpdateParameters.Add("IdCalificacion", ddlCalificacion.SelectedValue)
        SDSevaluaciones.UpdateParameters.Add("Porcentaje", TBporcentaje.Text)
        SDSevaluaciones.UpdateParameters.Add("InicioContestar", tbFechaInicio.Text.Trim())
        SDSevaluaciones.UpdateParameters.Add("FinSinPenalizacion", tbFechaPenalizacion.Text.Trim() & " 23:59:00")
        SDSevaluaciones.UpdateParameters.Add("FinContestar", tbFechaFin.Text.Trim() + " 23:59:00")
        SDSevaluaciones.UpdateParameters.Add("Tipo", DDLtipo.SelectedValue)
        SDSevaluaciones.UpdateParameters.Add("Penalizacion", TBpenalizacion.Text)
        SDSevaluaciones.UpdateParameters.Add("Aleatoria", ddlAleatoria.SelectedValue)
        SDSevaluaciones.UpdateParameters.Add("SeEntregaDocto", CBseentregadocto.Checked.ToString)
        SDSevaluaciones.UpdateParameters.Add("Estatus", DDLestatus.SelectedValue)
        SDSevaluaciones.UpdateParameters.Add("Califica", DDLcalifica.SelectedValue)
        SDSevaluaciones.UpdateParameters.Add("SegundaVuelta", chkSegundavuelta.Checked)
        SDSevaluaciones.UpdateParameters.Add("Instruccion", tbInstruccion.Text)
        SDSevaluaciones.UpdateParameters.Add("Modifico", User.Identity.Name)
                SDSevaluaciones.UpdateParameters.Add("RepiteAutomatica", chkRepeticion.Checked)
                SDSevaluaciones.UpdateParameters.Add("NoActividad", Trim(TBNoActividad.Text))
                SDSevaluaciones.UpdateParameters.Add("EsForo", ChkEsForo.Checked)
                SDSevaluaciones.UpdateParameters.Add("OpcionesAleatorias", ddlOpcionAleatoria.SelectedValue)

                If chkRepeticion.Checked Then
                    SDSevaluaciones.UpdateParameters.Add("RepiteIndicador", ddlIndicadorRepeticion.SelectedValue)
                    SDSevaluaciones.UpdateParameters.Add("RepiteRango", ddlResultadoRepeticion.SelectedValue)
                    SDSevaluaciones.UpdateParameters.Add("RepiteMaximo", Integer.Parse(tbRepeticiones.Text.Trim()))
                Else
                    SDSevaluaciones.UpdateParameters.Add("RepiteIndicador", Nothing)
          SDSevaluaciones.UpdateParameters.Add("RepiteRango", Nothing)
          SDSevaluaciones.UpdateParameters.Add("RepiteMaximo", Nothing)
        End If
        SDSevaluaciones.UpdateParameters.Add("IdEvaluacion", GVevaluaciones.SelectedDataKey(0).ToString())

        SDSevaluaciones.Update()

        GVevaluaciones.DataBind()
        msgError.hide()
        msgSuccess.show("Éxito", "Se modificó la evalaución.")
        ' cambio selector "seleccionar" por "deseleccionar" en la fila seleccionada
        cambiarNombreColumnaSeleccion()
      Catch ex As Exception
        Utils.LogManager.ExceptionLog_InsertEntry(ex)
        msgErrorDialog.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")),
                            "Ocurrió un error desconocido. Si el problema persiste " &
                            "notifique al administrador del sistema.")
        ModalPopupEvaluacion.Show()
      End Try
    End If
  End Sub

  ''' <summary>
  ''' Intenta eliminar la evaluación seleccionada
  ''' </summary>
  Protected Sub EliminaEvaluacion()
    Try
      SDSevaluaciones.DeleteParameters.Clear()
      SDSevaluaciones.DeleteCommand = "Delete from Evaluacion WHERE IdEvaluacion = @IdEvaluacion"
      SDSevaluaciones.DeleteParameters.Add("IdEvaluacion", GVevaluaciones.SelectedDataKey(0).ToString())

      SDSevaluaciones.Delete()

      GVevaluaciones.DataBind()

      TBclavebateria.Text = ""
      TBporcentaje.Text = ""
      TBclaveE.Text = ""
      TBpenalizacion.Text = ""
      msgError.hide()
      msgSuccess.show("Éxito", "Se eliminó la evaluación.")
    Catch sqlex As SqlException When sqlex.Number = 547
      Dim s As String = "No se puede borrar la evaluación por que "
      Utils.LogManager.ExceptionLog_InsertEntry(sqlex) ' registra el error
      If sqlex.Message.Contains("EvaluacionPlantel") Then
        msgError.show(s &
                      "tiene fechas asignadas a planteles.")
      ElseIf sqlex.Message.Contains("ApoyoEvaluacion") Then
        msgError.show(s &
                      "tiene apoyos asignados.")
      ElseIf sqlex.Message.Contains("DetalleEvaluacion") Then
        msgError.show(s &
                      "tiene subtemas asignados.")
      ElseIf sqlex.Message.Contains("EvaluacionRepetida") Then
        msgError.show(s &
                      "hay alumnos que la han terminado y están repitiendo.")
      ElseIf sqlex.Message.Contains("EvaluacionTerminada") Then
        msgError.show(s &
                      "hay alumnos que la han terminado.")
      ElseIf sqlex.Message.Contains("DoctoEvaluacion") Then
        msgError.show(s &
                      "hay documentos entregados.")
      ElseIf sqlex.Message.Contains("EvaluacionAlumno") Then
        msgError.show(s &
                      "tiene fechas asignadas a alumnos.")
      ElseIf sqlex.Message.Contains("EvaluacionDemo") Then
        msgError.show(s &
                      "tiene evaluaciones demo.")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

#End Region
  ' *********************************************************************************************
#Region "Página"

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(
                  ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVevaluaciones.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(
                                                     GVevaluaciones,
                                                     "Select$" + r.RowIndex.ToString()
                                                     ))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

#End Region
End Class
