﻿Imports Siget

Partial Class superadministrador_Planteles
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.PLANTELES
        Label2.Text = Config.Etiqueta.INSTITUCION
        Label3.Text = Config.Etiqueta.PLANTEL

        GVDatosPlanteles.Caption = "<h3>" &
            Config.Etiqueta.PLANTELES &
            " capturad" & Config.Etiqueta.LETRA_PLANTEL & "s</h3>"

        'GVDatosPlanteles.HeaderStyle.BackColor = System.Drawing.Color.FromName(Config.Color.TituloPrincipal_Bottom)

    End Sub

    Protected Sub GVDatosPlanteles_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVDatosPlanteles.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(3).Controls(0)
            LnkHeaderText.Text = "Id " & Config.Etiqueta.INSTITUCION
            LnkHeaderText = e.Row.Cells(4).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.INSTITUCION
            LnkHeaderText = e.Row.Cells(5).Controls(0)
            LnkHeaderText.Text = "Id " & Config.Etiqueta.PLANTEL
            LnkHeaderText = e.Row.Cells(6).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.PLANTEL
            LnkHeaderText = e.Row.Cells(7).Controls(0)
            LnkHeaderText.Text = "Clave " & Config.Etiqueta.PLANTEL
        End If
    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        Try
            SDSplanteles.InsertCommand = "SET dateformat dmy; INSERT INTO Plantel (IdLicencia,IdInstitucion,Descripcion,Clave) VALUES (" + DDLlicencia.SelectedValue + "," + DDLinstitucion.SelectedValue + ",'" + TBdescripcion.Text + "','" + TBclave.Text + "')"
            SDSplanteles.Insert()
            Mensaje.Text = "El registro ha sido guardado"
            GVDatosPlanteles.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click
        TBdescripcion.Text = ""
        TBclave.Text = ""
        Mensaje.Text = "Capture los datos"
    End Sub

    Protected Sub GVDatosPlanteles_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDatosPlanteles.SelectedIndexChanged
        'Los campos deben estar con visible=true para poder leer el dato con SelectedRow (el numero depende en como se acomodaron en el GridView en Colección de columnas)
        DDLlicencia.SelectedValue = CInt(GVDatosPlanteles.SelectedRow.Cells(1).Text) 'IdLicencia
        DDLinstitucion.SelectedValue = CInt(GVDatosPlanteles.SelectedRow.Cells(3).Text) 'IdInstitucion
        'La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
        TBdescripcion.Text = HttpUtility.HtmlDecode(GVDatosPlanteles.SelectedRow.Cells(6).Text) 'Descripcion del Plantel
        TBclave.Text = HttpUtility.HtmlDecode(GVDatosPlanteles.SelectedRow.Cells(7).Text) 'Clave del Plantel
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        Try
            SDSplanteles.UpdateCommand = "SET dateformat dmy; UPDATE Plantel SET IdLicencia = " + DDLlicencia.SelectedValue + ",IdInstitucion = " + DDLinstitucion.SelectedValue + ",Descripcion = '" + TBdescripcion.Text + "',Clave = '" + TBclave.Text + "' WHERE IdPlantel = " + GVDatosPlanteles.SelectedValue.ToString 'Me da el IdPlantel porque es el campo clave de la fila seleccionada
            SDSplanteles.Update()
            Mensaje.Text = "El registro ha sido actualizado"
            GVDatosPlanteles.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        Try
            SDSplanteles.DeleteCommand = "Delete from Plantel where IdPlantel = " + GVDatosPlanteles.SelectedValue.ToString 'Me da el IdPlantel porque es el campo clave de la fila seleccionada
            SDSplanteles.Delete()
            GVDatosPlanteles.DataBind()
            'Los campos deben estar con visible=true para poder leer el dato con SelectedRow (el numero depende en como se acomodaron en el GridView en Colección de columnas)

            'NO NECESITO POSICIONAR LOS DDRODOWNLIST PORQUE ES POR ELLOS QUE FILTRO
            '*DDLlicencia.SelectedValue = CInt(GVDatosPlanteles.SelectedRow.Cells(1).Text) 'IdLicencia
            '*DDLinstitucion.SelectedValue = CInt(GVDatosPlanteles.SelectedRow.Cells(3).Text) 'IdInstitucion

            'La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
            TBdescripcion.Text = HttpUtility.HtmlDecode(GVDatosPlanteles.SelectedRow.Cells(6).Text) 'Descripcion del Plantel
            TBclave.Text = HttpUtility.HtmlDecode(GVDatosPlanteles.SelectedRow.Cells(7).Text) 'Clave del Plantel
            Mensaje.Text = "El registro ha sido Eliminado"
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLlicencia_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLlicencia.DataBound
        DDLlicencia.Items.Insert(0, New ListItem("---Elija una Licencia", 0))
    End Sub

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem("---Elija una " & Config.Etiqueta.INSTITUCION, 0))
    End Sub
End Class
