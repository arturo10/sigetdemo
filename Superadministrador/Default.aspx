﻿<%@ Page Title=""
    Language="VB" 
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false" 
    CodeFile="Default.aspx.vb" 
    Inherits="superadministrador_Default" 
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type='text/css'>#toolBarContainer{display: none;}</style> <!-- la toolbar no es necesaria en páginas principales -->

    <!-- Estilos para los menús de inicio con esquema de lista -->
    <link id="MainMenu_List" rel="stylesheet" media="screen" href="../Resources/css/MainMenu_List.css" />

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>
        Menú de Administradores
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <div class="centeringContainer">
        <div class="centered">
            <table class="mainMenuTable borderlessTable">
                <tbody>
                    <tr>
                        <td class="lineSeparator">&nbsp;
                        </td>
                    </tr>
                    <asp:Panel ID="pnlSysadmin" runat="server" Visible="false">
                        <tr class="MenuLineB">
                            <!-- 
                                MENU SECTION
                                 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                                -->
                            <td class="MenuColumn VerticalIndented">
                                <div class="MenuTitleDiv">
                                    <asp:Image ID="Image76" runat="server"
                                        ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/preferences_gear_5.png" />
                                    <br />
                                    Sistemas
                                </div>
                            </td>
                            <td class="OptionsColumn VerticalIndented LeftMenuColumn">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:HyperLink ID="HyperLink61" runat="server"
                                                NavigateUrl="~/superadministrador/EjecutaQuery ">
                                                <div class="mnItem mnIndent1 mnItemLink">
                                                    <asp:Image ID="Image77" runat="server"
                                                        ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                        Width="10" Height="10" />
                                                    Ejecuta Query
                                                </div>
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="OptionsColumn VerticalIndented">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:HyperLink ID="HyperLink63" runat="server"
                                                NavigateUrl="~/superadministrador/Errores ">
											    <div class="mnItem mnIndent2 mnItemLink">
                                                    <asp:Image ID="Image80" runat="server"
                                                        ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                        Width="10" Height="10" />
													    Registro de Errores
											    </div>
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:HyperLink ID="HyperLink35" runat="server"
                                                NavigateUrl="~/superadministrador/LoginStats ">
											    <div class="mnItem mnIndent2 mnItemLink">
                                                    <asp:Image ID="Image50" runat="server"
                                                        ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                        Width="10" Height="10" />
													    Estadísticas de Uso
											    </div>
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="OptionsColumn VerticalIndented">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:HyperLink ID="HyperLink66" runat="server"
                                                NavigateUrl="~/superadministrador/Mantenimientos ">
										    <div class="mnItem mnIndent2 mnItemLink">
                                                    <asp:Image ID="Image84" runat="server"
                                                        ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                        Width="10" Height="10" />
												    Mantenimientos
										    </div>
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </asp:Panel>
                    <tr>
                        <td class="lineSeparator">&nbsp;
                        </td>
                    </tr>
                    <tr class="MenuLineA">
                        <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
                        <td class="MenuColumn VerticalIndented">
                            <div class="MenuTitleDiv">
                                <asp:Image ID="Image7" runat="server"
                                    ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/house_home_building.png" />
                                <br />
                                Entidades
                            </div>
                        </td>
                        <td class="OptionsColumn VerticalIndented LeftMenuColumn">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink6" runat="server"
                                            NavigateUrl="~/superadministrador/Instituciones ">
                                            <div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image16" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                <asp:Label ID="Label1" runat="server" Text="[INSTITUCIONES]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink1" runat="server"
                                            NavigateUrl="~/superadministrador/Planteles ">
                                            <div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image17" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                <asp:Label ID="Label2" runat="server" Text="[PLANTELES]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <div class="mnItem mnIndent1 subtitle ">
                                            <asp:Image ID="Image1" runat="server"
                                                ImageUrl="~/Resources/Customization/imagenes/dot.png" />
                                            <asp:Label ID="Label3" runat="server" Text="[NIVELES]" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink2" runat="server"
                                            NavigateUrl="~/superadministrador/Niveles ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image18" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Crear
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink3" runat="server"
                                            NavigateUrl="~/superadministrador/AsignaNiveles ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image19" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Asignar
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink4" runat="server"
                                            NavigateUrl="~/superadministrador/ApoyoNivel ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image20" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Apoyo Académico
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <div class="mnItem mnIndent1 subtitle ">
                                            <asp:Image ID="Image2" runat="server"
                                                ImageUrl="~/Resources/Customization/imagenes/dot.png" />
                                            <asp:Label ID="Label4" runat="server" Text="[GRADOS]" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink5" runat="server"
                                            NavigateUrl="~/superadministrador/Grados ">
										<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image21" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
												Administrar
										</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="mnItem mnIndent1 subtitle ">
                                            <asp:Image ID="Image3" runat="server"
                                                ImageUrl="~/Resources/Customization/imagenes/dot.png" />
                                            <asp:Label ID="Label5" runat="server" Text="[GRUPOS]" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink7" runat="server"
                                            NavigateUrl="~/superadministrador/Grupos ">
										    <div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image22" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
												    Crear
										    </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink8" runat="server"
                                            NavigateUrl="~/superadministrador/GeneraGrupos ">
										    <div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image23" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
												    Importar
										    </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="lineSeparator">&nbsp;
                        </td>
                    </tr>
                    <tr class="MenuLineB">
                        <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
                        <td class="MenuColumn VerticalIndented">
                            <div class="MenuTitleDiv">
                                <asp:Image ID="Image14" runat="server"
                                    ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/group_users.png" />
                                <br />
                                Plantilla
                            </div>
                        </td>
                        <td class="OptionsColumn VerticalIndented LeftMenuColumn">
                            <table>
                                <tr>
                                    <td>
                                        <div class="mnItem mnIndent1 subtitle ">
                                            <asp:Image ID="Image4" runat="server"
                                                ImageUrl="~/Resources/Customization/imagenes/dot.png" />
                                            <asp:Label ID="Label6" runat="server" Text="[COORDINADORES]" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink9" runat="server"
                                            NavigateUrl="~/superadministrador/Coordinadores ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image24" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Crear
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink10" runat="server"
                                            NavigateUrl="~/superadministrador/AsignaCoordPlantel ">
                                            <div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image25" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                Asignar a
                                                <asp:Label ID="Label7" runat="server" Text="[PLANTEL]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink11" runat="server"
                                            NavigateUrl="~/superadministrador/UsuariosCoordinador ">
										    <div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image26" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
												    Consultar
										    </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <div class="mnItem mnIndent1 subtitle ">
                                            <asp:Image ID="Image5" runat="server"
                                                ImageUrl="~/Resources/Customization/imagenes/dot.png" />
                                            <asp:Label ID="Label8" runat="server" Text="[PROFESORES]" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink12" runat="server"
                                            NavigateUrl="~/superadministrador/RegistroProfesor ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image27" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Registrar
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink13" runat="server"
                                            NavigateUrl="~/superadministrador/Profesores ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image28" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Administrar
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink14" runat="server"
                                            NavigateUrl="~/superadministrador/AsignaProfesor ">
                                            <div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image29" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                Asignar a
                                                <asp:Label ID="Label9" runat="server" Text="[GRUPOS]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink15" runat="server"
                                            NavigateUrl="~/superadministrador/Generausuariosprof ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image30" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Crear Cuentas
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink16" runat="server"
                                            NavigateUrl="~/superadministrador/Buscaprofesor ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image31" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Buscar y Cambiar
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink17" runat="server"
                                            NavigateUrl="~/superadministrador/GeneraProfesores ">
                                            <div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image32" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                Importar
                                                <asp:Label ID="Label10" runat="server" Text="[PROFESORES]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink18" runat="server"
                                            NavigateUrl="~/superadministrador/GeneraProg ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image33" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Importar Programación
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <div class="mnItem mnIndent1 subtitle ">
                                            <asp:Image ID="Image6" runat="server"
                                                ImageUrl="~/Resources/Customization/imagenes/dot.png" />
                                            <asp:Label ID="Label11" runat="server" Text="[ALUMNOS]" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink19" runat="server"
                                            NavigateUrl="~/superadministrador/RegistroAlumnos ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image34" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Registrar
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink20" runat="server"
                                            NavigateUrl="~/superadministrador/Alumnos ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image35" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Administrar
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink21" runat="server"
                                            NavigateUrl="~/superadministrador/GeneraAlumnos ">
                                            <div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image36" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                Importar 
												<asp:Label ID="Label12" runat="server" Text="[ALUMNOS]" />
                                                y 
												<asp:Label ID="Label13" runat="server" Text="[SUBEQUIPOS]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink22" runat="server"
                                            NavigateUrl="~/superadministrador/BuscaAlumno ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image37" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Buscar y Cambiar
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink23" runat="server"
                                            NavigateUrl="~/superadministrador/Equipos ">
                                            <div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image38" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                <asp:Label ID="Label14" runat="server" Text="[EQUIPOS]" />
                                                y 
												<asp:Label ID="Label15" runat="server" Text="[SUBEQUIPOS]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="lineSeparator">&nbsp;
                        </td>
                    </tr>
                    <tr class="MenuLineA">
                        <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
                        <td class="MenuColumn VerticalIndented">
                            <div class="MenuTitleDiv">
                                <asp:Image ID="Image13" runat="server"
                                    ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/clipboard.png" />
                                <br />
                                Académico
                            </div>
                        </td>
                        <td class="OptionsColumn VerticalIndented LeftMenuColumn">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink24" runat="server"
                                            NavigateUrl="~/superadministrador/Competencias ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image39" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Competencias
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink25" runat="server"
                                            NavigateUrl="~/superadministrador/Area ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image40" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Areas o Ciencias
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink26" runat="server"
                                            NavigateUrl="~/superadministrador/Asignaturas ">
                                            <div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image41" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                <asp:Label ID="Label16" runat="server" Text="[ASIGNATURAS]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink27" runat="server"
                                            NavigateUrl="~/superadministrador/Temas ">
										    <div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image42" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
												    Temas
										    </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink28" runat="server"
                                            NavigateUrl="~/superadministrador/Subtemas ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image43" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Subtemas
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <div class="mnItem mnIndent1 subtitle ">
                                            <asp:Image ID="Image8" runat="server"
                                                ImageUrl="~/Resources/Customization/imagenes/dot.png" />
                                            Reactivos
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink29" runat="server"
                                            NavigateUrl="~/superadministrador/Planteamientos ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image44" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Planteamientos
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink30" runat="server"
                                            NavigateUrl="~/superadministrador/Opciones ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image45" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Opciones
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink31" runat="server"
                                            NavigateUrl="~/superadministrador/CargaReactivos ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image46" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Importar
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="lineSeparator">&nbsp;
                        </td>
                    </tr>
                    <tr class="MenuLineB">
                        <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
                        <td class="MenuColumn VerticalIndented">
                            <div class="MenuTitleDiv">
                                <asp:Image ID="Image12" runat="server"
                                    ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/date_calendar.png" />
                                <br />
                                Escolar
                            </div>
                        </td>
                        <td class="OptionsColumn VerticalIndented LeftMenuColumn">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink32" runat="server"
                                            NavigateUrl="~/superadministrador/Indicadores ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image47" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Indicadores
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink33" runat="server"
                                            NavigateUrl="~/superadministrador/CiclosEscolares ">
                                            <div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image48" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                <asp:Label ID="Label17" runat="server" Text="[CICLO]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink34" runat="server"
                                            NavigateUrl="~/superadministrador/Calificaciones ">
                                            <div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image49" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                Calificaciones
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink36" runat="server"
                                            NavigateUrl="~/superadministrador/Evaluaciones ">
										    <div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image51" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
												    Actividades
										    </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink37" runat="server"
                                            NavigateUrl="~/superadministrador/AgruparReactivos ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image52" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Agrupar Reactivos
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink38" runat="server"
                                            NavigateUrl="~/superadministrador/EvaluacionPlantel ">
                                            <div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image53" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                Asignar Fechas a 
												<asp:Label ID="Label19" runat="server" Text="[PLANTELES]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink39" runat="server"
                                            NavigateUrl="~/superadministrador/EvaluacionAlumno ">
                                            <div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image54" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                Asignar Fechas a 
												<asp:Label ID="Label20" runat="server" Text="[ALUMNOS]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink40" runat="server"
                                            NavigateUrl="~/superadministrador/EvaluacionDemo ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image55" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Actividad Demo
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink41" runat="server"
                                            NavigateUrl="~/superadministrador/Programacion ">
                                            <div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image56" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                Carga de Valoración a 
												<asp:Label ID="Label21" runat="server" Text="[PROFESORES]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink42" runat="server"
                                            NavigateUrl="~/superadministrador/SeriarEvaluaciones ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image57" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Seriar Actividades
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink43" runat="server"
                                            NavigateUrl="~/superadministrador/ApoyoEvaluacion ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image58" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Apoyos para Actividades
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="lineSeparator">&nbsp;
                        </td>
                    </tr>
                    <tr class="MenuLineA">
                        <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
                        <td class="MenuColumn VerticalIndented">
                            <div class="MenuTitleDiv">
                                <asp:Image ID="Image11" runat="server"
                                    ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/key_password.png" />
                                <br />
                                Accesos
                            </div>
                        </td>
                        <td class="OptionsColumn VerticalIndented LeftMenuColumn">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink48" runat="server"
                                            NavigateUrl="~/superadministrador/Accesos ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image9" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Accesos
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink44" runat="server"
                                            NavigateUrl="~/superadministrador/Licencias ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image59" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Licencias
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink45" runat="server"
                                            NavigateUrl="~/superadministrador/Desbloquea ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image60" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Desbloquear Usuarios
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink55" runat="server"
                                            NavigateUrl="~/superadministrador/ConfiguraBloqueo ">
                                            <div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image61" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                Tiempos de Bloqueo de Perfiles
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink46" runat="server"
                                            NavigateUrl="~/superadministrador/AlumnosFirmados ">
                                            <div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image62" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                <asp:Label ID="Label22" runat="server" Text="[ALUMNOS]" />
                                                Registrados
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink47" runat="server"
                                            NavigateUrl="~/superadministrador/UsuariosAdministracion ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image63" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Usuarios de Soporte
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <div class="mnItem mnIndent1 subtitle ">
                                            <asp:Image ID="Image15" runat="server"
                                                ImageUrl="~/Resources/Customization/imagenes/dot.png" />
                                            Operadores
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink57" runat="server"
                                            NavigateUrl="~/superadministrador/Operadores ">
											<div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image64" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Crear
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink58" runat="server"
                                            NavigateUrl="~/superadministrador/AsignaOperadorPlantel ">
                                            <div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image65" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
                                                Asignar a
                                                <asp:Label ID="Label23" runat="server" Text="[PLANTEL]" />
                                            </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink59" runat="server"
                                            NavigateUrl="~/superadministrador/UsuariosOperador ">
										    <div class="mnItem mnIndent2 mnItemLink">
                                                <asp:Image ID="Image66" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
												    Consultar
										    </div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="lineSeparator">&nbsp;
                        </td>
                    </tr>
                    <tr class="MenuLineB">
                        <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
                        <td class="MenuColumn VerticalIndented">
                            <div class="MenuTitleDiv">
                                <asp:Image ID="Image10" runat="server"
                                    ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/preferences_settings.png" />
                                <br />
                                Configuración
                            </div>
                        </td>
                        <td class="OptionsColumn VerticalIndented LeftMenuColumn">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink49" runat="server"
                                            NavigateUrl="~/superadministrador/BorraRespuestas ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image68" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Borrar Respuestas
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink60" runat="server"
                                            NavigateUrl="~/superadministrador/BorraDocumentos ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image75" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Borrar Documentos
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink50" runat="server"
                                            NavigateUrl="~/superadministrador/BorraEvaluaciones ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image70" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Borrar Actividades
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink56" runat="server"
                                            NavigateUrl="~/superadministrador/paginaInicio/">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image69" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Página de Inicio
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink62" runat="server"
                                            NavigateUrl="~/superadministrador/ConfiguracionGlobal ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image78" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Configuración General
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink51" runat="server"
                                            NavigateUrl="~/superadministrador/CapturaResultados ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image71" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Capturar Resultados
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>

