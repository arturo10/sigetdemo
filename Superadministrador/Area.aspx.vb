﻿Imports Siget

Imports System.Data.SqlClient

Partial Class superadministrador_Area
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.ASIGNATURAS

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString

        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        miComando = objConexion.CreateCommand
        miComando.CommandText = "select Count(IdArea) as Total from Area"
        objConexion.Open() 'Abro la conexion
        misRegistros = miComando.ExecuteReader()
        misRegistros.Read()
        If misRegistros.Item("Total") = 0 Then
            SDSareas.InsertCommand = "SET dateformat dmy; INSERT INTO Area(Descripcion, Estatus) values('DISPONIBLE PARA MODIFICAR','Activo')"
            SDSareas.Insert()
        End If
        misRegistros.Close()
        objConexion.Close()
    End Sub
End Class
