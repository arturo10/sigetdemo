﻿Imports Siget


Partial Class superadministrador_UsuariosCoordinador
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GVcoordinadores.Caption = Config.Etiqueta.COORDINADORES &
            " asigand" & Config.Etiqueta.LETRA_COORDINADOR & "s a " &
            Config.Etiqueta.ARTDET_PLANTEL & " " & Config.Etiqueta.PLANTEL &
            " elegid" & Config.Etiqueta.LETRA_PLANTEL

        Label1.Text = Config.Etiqueta.COORDINADORES
        Label2.Text = Config.Etiqueta.INSTITUCION
        Label3.Text = Config.Etiqueta.PLANTEL
    End Sub

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_INSTITUCION & " " & Config.Etiqueta.INSTITUCION, 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
    End Sub
End Class
