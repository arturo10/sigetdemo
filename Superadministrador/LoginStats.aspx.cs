﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;
using System.Data.SqlClient;

public partial class superadministrador_LoginStats : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Literal l = (Literal) Master.FindControl("pnlHeader");
        Siget.UserInterface.Include.Chartist(ref l);

        generaListaNavegadores();
        generaListaVersionesNavegador();
        generaListaResolucionX();
        generaListaResolucionY();
        generaListaPlataformas();
    }
    
    protected void generaListaNavegadores()
    {
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.
                                                            ConnectionStrings["sadcomeConnectionString"].
                                                            ConnectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(
"SELECT " +
"	Navegador" +
"	,COUNT(Navegador) AS Conteo" +
" FROM" +
"	InicioSesion" +
" GROUP BY " +
"	Navegador" +
" ORDER BY" +
"	Conteo DESC", conn))
                {
                    using (SqlDataReader results = cmd.ExecuteReader())
                    {

                        while (results.Read())
                        {

                            HfNavegadores.Value += results["Navegador"].ToString() + "," + results["Conteo"].ToString() + "|";
                        }

                        HfNavegadores.Value = HfNavegadores.Value.Substring(0, HfNavegadores.Value.Length - 1);

                    }
                }
            }
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
        }
    }

    protected void generaListaVersionesNavegador()
    {
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.
                                                            ConnectionStrings["sadcomeConnectionString"].
                                                            ConnectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(
"SELECT " +
"	Navegador" +
"	,[Version]" +
"	,COUNT([Version]) AS Conteo" +
" FROM" +
"	InicioSesion" +
" GROUP BY " +
"	Navegador" +
"	,[Version]" +
" ORDER BY " +
"	Navegador" +
"	,[Version] DESC", conn))
                {
                    using (SqlDataReader results = cmd.ExecuteReader())
                    {

                        while (results.Read())
                        {

                            HfVersionesNavegadores.Value += results["Navegador"].ToString() + "_" + results["Version"].ToString() + "," + results["Conteo"].ToString() + "|";
                        }

                        HfVersionesNavegadores.Value = HfVersionesNavegadores.Value.Substring(0, HfVersionesNavegadores.Value.Length - 1);

                    }
                }
            }
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
        }
    }

    protected void generaListaResolucionX()
    {
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.
                                                            ConnectionStrings["sadcomeConnectionString"].
                                                            ConnectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand("InicioSesion_FrecuenciaResolucionX", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    using (SqlDataReader results = cmd.ExecuteReader())
                    {

                        while (results.Read())
                        {

                            HfResX.Value += results["Conteo"].ToString() + "|";
                        }

                        HfResX.Value = HfResX.Value.Substring(0, HfResX.Value.Length - 1);

                    }
                }
            }
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
        }
    }

    protected void generaListaResolucionY()
    {
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.
                                                            ConnectionStrings["sadcomeConnectionString"].
                                                            ConnectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand("InicioSesion_FrecuenciaResolucionY", conn))
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    using (SqlDataReader results = cmd.ExecuteReader())
                    {

                        while (results.Read())
                        {

                            HfResY.Value += results["Conteo"].ToString() + "|";
                        }

                        HfResY.Value = HfResY.Value.Substring(0, HfResY.Value.Length - 1);

                    }
                }
            }
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
        }
    }

    protected void generaListaPlataformas()
    {
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.
                                                            ConnectionStrings["sadcomeConnectionString"].
                                                            ConnectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(
"SELECT " +
"	Plataforma" +
"	,COUNT(Plataforma) AS Conteo" +
" FROM" +
"	InicioSesion" +
" GROUP BY " +
"	Plataforma" +
" ORDER BY" +
"	Conteo DESC", conn))
                {
                    using (SqlDataReader results = cmd.ExecuteReader())
                    {

                        while (results.Read())
                        {

                            HfPlataforma.Value += results["Plataforma"].ToString() + "," + results["Conteo"].ToString() + "|";
                        }

                        HfPlataforma.Value = HfPlataforma.Value.Substring(0, HfPlataforma.Value.Length - 1);

                    }
                }
            }
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
        }
    }

}