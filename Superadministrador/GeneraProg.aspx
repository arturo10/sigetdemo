﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="GeneraProg.aspx.vb" 
    Inherits="superadministrador_GeneraProg" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">



        .style10
        {
            width: 100%;
            height: 430px;
        }
       
              
        .style13
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
               
              
        .style11
        {
            font-family: Arial, Helvetica, sans-serif;
        }
        .style12
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
        .style14
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
        }
       
              
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Generación automática de la Programación de 
								<asp:Label ID="Label1" runat="server" Text="[PROFESORES]" />
								, 
								<asp:Label ID="Label2" runat="server" Text="[ASIGNATURAS]" />
								 y 
								<asp:Label ID="Label3" runat="server" Text="[GRUPOS]" />
								 en 
                un 
								<asp:Label ID="Label4" runat="server" Text="[CICLO]" />
		</h1>
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style10">
        <tr>
            <td class="style11">
                <span class="style12">Cargue el archivo que contiene los registros para importar a la base de datos:<br />
                </span>
                <br class="style12" /><span class="style14">(Deberá tener los campos separados por comas: 
                <b>&nbsp;Idasignatura, Idprofesor, Idciclo, Idgrupo</b>)<br />
                <br />
                <br />Se generarán los registros en la tabla de Programacion, que asigna las 
                <asp:Label ID="Label5" runat="server" Text="ASIGNATURAS	" />
								 y 
										<asp:Label ID="Label6" runat="server" Text="[GRUPOS]" />
										 a los 
										<asp:Label ID="Label7" runat="server" Text="[PROFESORES]" />
										 en un 
										<asp:Label ID="Label8" runat="server" Text="[CICLO]" />
										 determinado</span></td>
        </tr>
        <tr>
            <td class="style11">
                <asp:FileUpload ID="ArchivoCarga" runat="server" Width="505px" />
            </td>
        </tr>
        <tr>
            <td>
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td>
                
                <asp:SqlDataSource ID="SDSprogramacion" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [Programacion]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LblMensaje" runat="server" 
                    CssClass="LabelInfoDefault"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Importar" runat="server" Text="Importar" 
										CssClass="defaultBtn btnThemeBlue btnThemeMedium"/>
            </td>
        </tr>
        <tr>
            <td>
                
                <asp:HyperLink ID="HyperLink2" runat="server"
					NavigateUrl="~/" 
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

