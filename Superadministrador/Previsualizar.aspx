﻿<%@ Page Title=""
  Language="VB"
  MasterPageFile="~/Principal.master"
  AutoEventWireup="false"
  CodeFile="Previsualizar.aspx.vb"
  Inherits="superadministrador_Previsualizar"
  MaintainScrollPositionOnPostback="true" %>

<%@ PreviousPageType VirtualPath="~/superadministrador/Opciones.aspx" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
  <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <style type="text/css">
    .style11 {
      width: 100%;
    }

    .style21 {
    }

    .style16 {
      width: 16px;
    }

    .style22 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: large;
      font-weight: bold;
      color: #0000FF;
      text-align: right;
    }

    .style18 {
      width: 345px;
    }

    .style19 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: x-large;
      font-weight: bold;
    }

    .style23 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: x-small;
      font-weight: bold;
      color: #000099;
      background-color: #CCCCCC;
    }

    .style24 {
      text-align: left;
    }

    .style25 {
      width: 345px;
      text-align: left;
    }

    .style26 {
      font-size: x-small;
      color: #000099;
    }

    .style27 {
      width: 610px;
    }

    .style28 {
      height: 21px;
    }

    .style29 {
      font-family: Arial, Helvetica, sans-serif;
    }

    .style30 {
      width: 42px;
      height: 8px;
    }

    .style31 {
      width: 67px;
      height: 8px;
    }
  </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
  <h1>Previsualización del Reactivo
  </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
  <table class="style11" style="background-color: white;">
    <tr class="titulo">
      <td colspan="2" style="background-color: white;">
        <uc1:msgError runat="server" ID="msgError" />
      </td>
    </tr>
    <tr>
      <td class="style27" style="background-color: white;">
        <asp:Label ID="LblPlanteamiento" runat="server"
          Style="font-family: Arial, Helvetica, sans-serif; font-size: 22px; font-weight: 700; color: #000099;"></asp:Label>
      </td>
      <td>
        <asp:Image ID="ImPlanteamiento" runat="server" Visible="False" />
        <asp:HyperLink ID="HLapoyoplanteamiento" runat="server" Target="_blank"
          CssClass="style26"
          Style="font-family: Arial, Helvetica, sans-serif; font-weight: 700; text-align: left; background-color: #CCCCCC"
          Visible="False">[HLapoyoplanteamiento]</asp:HyperLink>
        <asp:Label ID="LblVideo" runat="server"
          Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
      </td>
    </tr>
    <tr>
      <td class="style27" style="background-color: white;">
        <asp:Image ID="ImPlanteamiento2" runat="server" Visible="False" />
        <asp:HyperLink ID="HLapoyoplanteamiento2" runat="server" Target="_blank"
          CssClass="style26"
          Style="font-family: Arial, Helvetica, sans-serif; font-weight: 700; text-align: left; background-color: #CCCCCC"
          Visible="False">[HLapoyoplanteamiento2]</asp:HyperLink>
        <asp:Label ID="LblVideo2" runat="server"
          Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="style21" colspan="2" style="background-color: white;">
        <asp:Label ID="LblReferencia" runat="server"
          Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
      </td>
    </tr>
    <tr>
      <td colspan="2" style="background-color: #666666">&nbsp;</td>
    </tr>
    <tr>
      <td class="style12" colspan="2">
        <asp:Panel ID="PnlOpciones" runat="server" Visible="false">
          <table id="TblOpciones" class="style11">
            <tr>
              <td class="style30">
                <asp:RadioButton ID="Opcion1" runat="server" GroupName="eleccion"
                  CssClass="style22" Width="50px" TextAlign="Left" />
              </td>
              <td class="style18">
                <asp:Label ID="TextoOpc1" runat="server" CssClass="style19"></asp:Label>
              </td>
              <td class="style31">
                <asp:RadioButton ID="Opcion2" runat="server" GroupName="eleccion"
                  CssClass="style22" Width="50px" TextAlign="Left" />
              </td>
              <td>
                <asp:Label ID="TextoOpc2" runat="server" CssClass="style19"></asp:Label>
              </td>
            </tr>
            <tr>
              <td class="style30">&nbsp;</td>
              <td class="style25">
                <asp:Image ID="ImOpc1" runat="server" />
                <asp:HyperLink ID="HLapoyoopc1" runat="server" Target="_blank"
                  CssClass="style23">[HLapoyoopc1]</asp:HyperLink>
              </td>
              <td class="style31">&nbsp;</td>
              <td class="style24">
                <asp:Image ID="ImOpc2" runat="server" />
                <asp:HyperLink ID="HLapoyoopc2" runat="server" Target="_blank"
                  CssClass="style23">[HLapoyoopc2]</asp:HyperLink>
              </td>
            </tr>
            <tr>
              <td class="style30">
                <asp:RadioButton ID="Opcion3" runat="server" GroupName="eleccion"
                  CssClass="style22" Width="50px" TextAlign="Left" />
              </td>
              <td class="style18">
                <asp:Label ID="TextoOpc3" runat="server" CssClass="style19"></asp:Label>
              </td>
              <td class="style31">
                <asp:RadioButton ID="Opcion4" runat="server" GroupName="eleccion"
                  CssClass="style22" Width="50px" TextAlign="Left" />
              </td>
              <td>
                <asp:Label ID="TextoOpc4" runat="server" CssClass="style19"></asp:Label>
              </td>
            </tr>
            <tr>
              <td class="style30">&nbsp;</td>
              <td class="style25">
                <asp:Image ID="ImOpc3" runat="server" />
                <asp:HyperLink ID="HLapoyoopc3" runat="server" Target="_blank"
                  CssClass="style23">[HLapoyoopc3]</asp:HyperLink>
              </td>
              <td class="style31">&nbsp;</td>
              <td class="style24">
                <asp:Image ID="ImOpc4" runat="server" />
                <asp:HyperLink ID="HLapoyoopc4" runat="server" Target="_blank"
                  CssClass="style23">[HLapoyoopc4]</asp:HyperLink>
              </td>
            </tr>
            <tr>
              <td class="style30">
                <asp:RadioButton ID="Opcion5" runat="server" GroupName="eleccion"
                  CssClass="style22" Width="50px" TextAlign="Left" />
              </td>
              <td class="style18">
                <asp:Label ID="TextoOpc5" runat="server" CssClass="style19"></asp:Label>
              </td>
              <td class="style31">
                <asp:RadioButton ID="Opcion6" runat="server" GroupName="eleccion"
                  CssClass="style22" Width="50px" TextAlign="Left" />
              </td>
              <td>
                <asp:Label ID="TextoOpc6" runat="server" CssClass="style19"></asp:Label>
              </td>
            </tr>
            <tr>
              <td class="style30">&nbsp;</td>
              <td class="style25">
                <asp:Image ID="ImOpc5" runat="server" />
                <asp:HyperLink ID="HLapoyoopc5" runat="server" Target="_blank"
                  CssClass="style23">[HLapoyoopc5]</asp:HyperLink>
              </td>
              <td class="style31">&nbsp;</td>
              <td class="style24">
                <asp:Image ID="ImOpc6" runat="server" />
                <asp:HyperLink ID="HLapoyoopc6" runat="server" Target="_blank"
                  CssClass="style23">[HLapoyoopc6]</asp:HyperLink>
              </td>
            </tr>
            <tr>
              <td class="style30">&nbsp;</td>
              <td class="style18">&nbsp;</td>
              <td class="style31">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td class="style30">&nbsp;</td>
              <td class="style18">
                <input id="Button1" type="button" value="Aceptar respuesta"
                  class="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
								<input id="Button2" type="button" value="Cancelar"
                                  class="defaultBtn btnThemeGrey btnThemeMedium" /></td>
              <td class="style31">&nbsp;                       
              </td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </asp:Panel>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <asp:Panel ID="PanelAbiertaAutomatica" runat="server" Visible="false">
          <table style="margin-left: auto; margin-right: auto;">
            <tr>
              <td>
                <asp:TextBox ID="tbRespuestaAbiertaAutomatica" runat="server"
                  ClientIDMode="Static" CssClass="wideTextbox"></asp:TextBox>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>
                <asp:Button ID="VerificarAA" runat="server" Visible="false"
                  ClientIDMode="AutoID"
                  Enabled="false"
                  Text="Verificar Respuesta"
                  CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
                &nbsp;
                                <asp:Button ID="AceptarAA" runat="server"
                                  Enabled="false"
                                  ClientIDMode="AutoID"
                                  Text="Aceptar Respuesta"
                                  CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
                                <asp:Button ID="CancelarAA" runat="server"
                                  Enabled="false"
                                  Text="   Salir   "
                                  CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
              </td>
            </tr>
          </table>
        </asp:Panel>
      </td>
    </tr>
    <tr>
      <td class="style28" colspan="2">
        <asp:PlaceHolder ID="PanelOpciones" runat="server"></asp:PlaceHolder>
      </td>
    </tr>
    <tr>
      <td class="style12" colspan="2">
        <asp:HyperLink ID="HyperLink5" runat="server"
          NavigateUrl="javascript:history.back()"
          CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
        <br />
      </td>
    </tr>
  </table>
</asp:Content>

