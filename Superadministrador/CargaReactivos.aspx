﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="CargaReactivos.aspx.vb"
    Inherits="superadministrador_CargaReactivos"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>




<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style13 {
            height: 73px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style14 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
        }


        .style15 {
            height: 36px;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>
        Cargar Reactivos
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style13">Cargue el archivo que contiene los registros para importar a la base de datos:<br />
                <br />
                IMPORTANTE: No ejecutar esta función concurrentemente a la creación de Reactivos<br />
            </td>
        </tr>
        <tr>
            <td>
                <span class="style14">
                    Deberá tener los campos separados por pipes (|) <b>:</b>
                    </span>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Image ID="Image1" runat="server" 
                    ImageUrl="~/Resources/imagenes/formato importe planteamientos.png"/>
            </td>
        </tr>
        <tr>
            <td class="style15">
                <asp:FileUpload ID="ArchivoCarga" runat="server" Width="505px" />
            </td>
        </tr>
        <tr>
            <td>
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td>
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td>
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr style=" display: none;">
            <td style="font-size: medium;">
                <asp:Button ID="Genera" runat="server" Text="Importar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium"
                    Enabled="false" />
                &nbsp;
                <asp:Button ID="Button1" runat="server"
                    Text="Migra Opciones emergencia (TMP)"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" 
                    Enabled="false" />
                &nbsp;
                <asp:Button ID="Button2" runat="server" Text="Actualiza EQUIPO (TMP)"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium"
                    Enabled="false" />
            </td>
        </tr>
        <tr>
            <td style="font-size: medium;">
                <asp:Button ID="Multiples" runat="server" 
                    Text="Importar Respuestas de Opción Múltiple"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium"/>
                &nbsp;
                <asp:Button ID="Abiertas" runat="server"
                    Text="Importar Respuestas Abiertas"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" 
                    Enabled="false" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSplanteamientos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Planteamiento]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSopciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Opcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSalumnos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Alumno]"></asp:SqlDataSource>

            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

