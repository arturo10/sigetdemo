﻿Imports Siget

Imports System.Data.SqlClient

Partial Class superadministrador_Instituciones
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

    End Sub

    Protected Sub gvInstituciones_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvInstituciones.SelectedIndexChanged
        msgError.hide()
        msgSuccess.hide()

        tbDescripcion.Text = HttpUtility.HtmlDecode(gvInstituciones.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(gvInstituciones, "Descripción")).Text)

        tbObservaciones.Text = HttpUtility.HtmlDecode(gvInstituciones.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(gvInstituciones, "Observaciones")).Text)

        ddlEstatus.SelectedIndex = ddlEstatus.Items.IndexOf(ddlEstatus.Items.FindByText(Trim(gvInstituciones.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(gvInstituciones, "Estatus")).Text)))
    End Sub

    ' comprobación básica de los campos en la forma 
    Protected Sub verificaCampos()
        If String.IsNullOrEmpty(Trim(tbDescripcion.Text)) Then
            Throw New System.Exception("Debe definir una descripción.")
        End If
    End Sub

    Protected Sub btnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            verificaCampos()

            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("SET dateformat dmy; INSERT INTO Institucion (Descripcion, Observaciones, Estatus, FechaModif, Modifico) VALUES (@Descripcion, @Observaciones, @Estatus, getdate(), @Modifico)", conn)

                cmd.Parameters.AddWithValue("@Descripcion", tbDescripcion.Text)
                cmd.Parameters.AddWithValue("@Observaciones", tbObservaciones.Text)
                cmd.Parameters.AddWithValue("@Estatus", ddlEstatus.SelectedValue)
                cmd.Parameters.AddWithValue("@Modifico", User.Identity.Name)

                cmd.ExecuteNonQuery()
                cmd.Dispose()
                conn.Close()

                msgSuccess.show("Éxito", "Se creó la institución " & tbDescripcion.Text)
                gvInstituciones.DataBind()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("SET dateformat dmy; DELETE FROM Institucion WHERE IdInstitucion = @IdInstitucion", conn)

                cmd.Parameters.AddWithValue("@IdInstitucion", gvInstituciones.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(gvInstituciones, "Id en Sistema")).Text)

                cmd.ExecuteNonQuery()
                cmd.Dispose()
                conn.Close()

                msgSuccess.show("Éxito", "Se eliminó la institución " & gvInstituciones.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(gvInstituciones, "Descripción")).Text)
                gvInstituciones.DataBind()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            verificaCampos()

            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("SET dateformat dmy; UPDATE Institucion SET Descripcion = @Descripcion, Observaciones = @Observaciones, Estatus = @Estatus, FechaModif = getdate(), Modifico = @Modifico WHERE IdInstitucion = @IdInstitucion", conn)

                cmd.Parameters.AddWithValue("@Descripcion", tbDescripcion.Text)
                cmd.Parameters.AddWithValue("@Observaciones", tbObservaciones.Text)
                cmd.Parameters.AddWithValue("@Estatus", ddlEstatus.SelectedValue)
                cmd.Parameters.AddWithValue("@Modifico", User.Identity.Name)
                cmd.Parameters.AddWithValue("@IdInstitucion", gvInstituciones.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(gvInstituciones, "Id en Sistema")).Text)

                cmd.ExecuteNonQuery()
                cmd.Dispose()
                conn.Close()

                msgSuccess.show("Éxito", "Se actualizó la institución " & tbDescripcion.Text)
                gvInstituciones.DataBind()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    ' Métodos de GridViews
    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub gvInstituciones_PreRender(sender As Object, e As EventArgs) Handles gvInstituciones.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If gvInstituciones.Rows.Count > 0 Then
            gvInstituciones.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub gvInstituciones_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles gvInstituciones.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If gvInstituciones.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub gvInstituciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvInstituciones.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(gvInstituciones, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In gvInstituciones.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(gvInstituciones, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub
End Class
