﻿<%@ Page
    Title=""
    Language="C#"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="true"
    CodeFile="LoginStats.aspx.cs"
    Inherits="superadministrador_LoginStats" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">

    <script class="code" type="text/javascript">
        $(document).ready(function () {
            initGraficaNavegadores();
            initGraficaVersionesNavegador();
            initGraficaResolucionX();
            initGraficaResolucionY();
            initGraficaPlataformas();
        });

        function initGraficaNavegadores() {
            var str = $("#HfNavegadores").val().split("|");
            var ticks = [];
            var points = [];

            for (var i = 0; i < str.length; i++) {
                ticks.push(str[i].split(",")[0]);
                points.push(str[i].split(",")[1]);
            }

            var data = {
                // A labels array that can contain any sort of values
                labels: ticks,
                // Our series array that contains series objects or in this case series data arrays
                series: [points]
            };

            // Create a new line chart object where as first parameter we pass in a selector
            // that is resolving to our chart container element. The Second parameter
            // is the actual data object.
            new Chartist.Bar('.fnu', data);
        }

        function initGraficaVersionesNavegador() {
            var str = $("#HfVersionesNavegadores").val().split("|");
            var ticks = [];
            var points = [];

            for (var i = 0; i < str.length; i++) {
                ticks.push(str[i].split(",")[0]);
                points.push(str[i].split(",")[1]);
            }

            var data = {
                // A labels array that can contain any sort of values
                labels: ticks,
                // Our series array that contains series objects or in this case series data arrays
                series: [points]
            };

            // Create a new line chart object where as first parameter we pass in a selector
            // that is resolving to our chart container element. The Second parameter
            // is the actual data object.
            new Chartist.Bar('.dvn', data);
        }

        function initGraficaResolucionX() {
            var points = [];
            var str = $("#HfResX").val().split("|");
            var ticks = ['500', '600', '700', '800', '900', '1000',
                '1100', '1200', '1300', '1400', '1500', '1600', '1700', '1800', '1900', '2000',
                '2100', '2200', '2300', '2400', '2500', '2600', '2700', '2800', '2900', '3000'];

            for (var i = 0; i < str.length; i++) {
                points.push(str[i]);
            }

            var data = {
                // A labels array that can contain any sort of values
                labels: ticks,
                // Our series array that contains series objects or in this case series data arrays
                series: [points]
            };

            // Create a new line chart object where as first parameter we pass in a selector
            // that is resolving to our chart container element. The Second parameter
            // is the actual data object.
            new Chartist.Bar('.resx', data);
        }

        function initGraficaResolucionY() {
            var points = [];
            var str = $("#HfResY").val().split("|");
            var ticks = ['100', '200', '300', '400', '500', '600', '700', '800', '900', '1000',
                '1100', '1200', '1300', '1400', '1500', '1600', '1700', '1800', '1900', '2000',
                '2100', '2200', '2300', '2400', '2500'];

            for (var i = 0; i < str.length; i++) {
                points.push(str[i]);
            }

            var data = {
                // A labels array that can contain any sort of values
                labels: ticks,
                // Our series array that contains series objects or in this case series data arrays
                series: [points]
            };

            // Create a new line chart object where as first parameter we pass in a selector
            // that is resolving to our chart container element. The Second parameter
            // is the actual data object.
            new Chartist.Bar('.resy', data);
        }

        function initGraficaPlataformas() {
            var str = $("#HfPlataforma").val().split("|");
            var ticks = [];
            var points = [];

            for (var i = 0; i < str.length; i++) {
                ticks.push(str[i].split(",")[0]);
                points.push(str[i].split(",")[1]);
            }

            var data = {
                // A labels array that can contain any sort of values
                labels: ticks,
                // Our series array that contains series objects or in this case series data arrays
                series: [points]
            };

            // Create a new line chart object where as first parameter we pass in a selector
            // that is resolving to our chart container element. The Second parameter
            // is the actual data object.
            new Chartist.Bar('.fpu', data);
        }

    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Estadísticas de Inicio de Sesión
    </h1>
    Muestra estadísticas de uso del sistema basadas en información recopilada de cada usuario cuando éste inicia sesión.
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" runat="Server">

    <p>
        <asp:HyperLink ID="HyperLink2" runat="server"
            NavigateUrl="~/"
            CssClass="defaultBtn btnThemeGrey btnThemeWide">
	    Regresar&nbsp;al&nbsp;Menú
        </asp:HyperLink>
    </p>

    Frecuencia de Navegadores Utilizados

    <div class="fnu ct-chart ct-major-eleventh" style="width: 1200px; font-size: 0.75rem;"></div>

    Distribución de Versiones de Navegadores

    <div class="dvn ct-chart ct-major-eleventh" style="width: 1200px; font-size: 0.75rem;"></div>

    Distribución de Resolución Horizontal

    <div class="resx ct-chart ct-major-eleventh" style="width: 1200px; font-size: 0.75rem;"></div>

    Distribución de Resolución Vertical

    <div class="resy ct-chart ct-major-eleventh" style="width: 1200px; font-size: 0.75rem;"></div>

    Frecuencia de Plataformas Utilizadas

    <div class="fpu ct-chart ct-major-eleventh" style="width: 1200px; font-size: 0.75rem;"></div>

    <p>
        <asp:HyperLink ID="HyperLink1" runat="server"
            NavigateUrl="~/"
            CssClass="defaultBtn btnThemeGrey btnThemeWide">
		    Regresar&nbsp;al&nbsp;Menú
        </asp:HyperLink>
    </p>


    <asp:HiddenField ID="HfNavegadores" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HfVersionesNavegadores" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HfResX" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HfResY" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="HfPlataforma" runat="server" ClientIDMode="Static" />
</asp:Content>

