﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="GeneraProfesores.aspx.vb" 
    Inherits="superadministrador_GeneraProfesores" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">

       
              
        .style13
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 43px;
        }
               
              
        .style11
        {
            font-family: Arial, Helvetica, sans-serif;
        }
        .style12
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
        .style14
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
        }
       
              
        .style15
        {
            width: 100%;
        }
               
              
        .style24
        {
            font-size: small;
            font-weight: bold;
            width: 283px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            color: #000000;
            height: 8px;
        }
       
              
        .style18
        {
            width: 279px;
        }
       
              
        .style25
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
       
              
        </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Generación automática de usuarios para 
								<asp:Label ID="Label1" runat="server" Text="[PROFESORES]" />
		</h1>
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style10">
        <tr>
            <td class="style11">
                <span class="style25">Cargue el archivo que contiene los registros para importar a la base de datos:</span><span class="style12"><br />
                </span>
                <br class="style12" /><span class="style14">(Deberá tener los campos separados por comas: 
                <b>&nbsp;Idplantel, Clave, Nombre, Apellidos, Login, Password</b>)<br />*NOTA: al password se le adicionarán automáticamente 3 dígitos aleatorios al 
                final de la cadena<br />
                <br />Se generarán los registros en la tablas de Usuario y profesor. Si no se 
                elige Registrar Usuarios, posteriormente el 
                profesor deberá generar sus cuentas de acceso predefinidas con los datos<br />que se le proporcionarán (Login y 
                Clave) para que proceda a su registro y 
                reciba un correo con su password</span></td>
        </tr>
        <tr>
            <td class="style11">
                <asp:FileUpload ID="ArchivoCarga" runat="server" Width="505px" />
            </td>
        </tr>
        <tr>
            <td class="style11">
                <table class="style15">
                    <tr>
                        <td class="style16">
                            <asp:CheckBox ID="CBregistrarme" runat="server" style="font-weight: 700; font-size: small; font-family: Arial, Helvetica, sans-serif;" 
                                Text="Registrar Usuarios" />
                        </td>
                        <td class="style24">
                            Correo electrónico para registrar usuarios:</td>
                        <td class="style18">
                            <asp:TextBox ID="TBemail" runat="server" Width="260px"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LblSalida" runat="server" 
                    style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSusuarios" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [Usuario]"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSprofesor" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [Profesor]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LblMensaje" runat="server" 
                    CssClass="LabelInfoDefault"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Importar" runat="server" Text="Importar" 
										CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                
                <asp:HyperLink ID="HyperLink2" runat="server"
					NavigateUrl="~/" 
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

