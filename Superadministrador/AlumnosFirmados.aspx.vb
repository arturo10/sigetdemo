﻿Imports Siget

Imports System.IO  'para importar: StringWriter

Partial Class superadministrador_AlumnosFirmados
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.INSTITUCION
        Label3.Text = Config.Etiqueta.ALUMNOS
        Label4.Text = Config.Etiqueta.INSTITUCION
    End Sub

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem("---Elija una " & Config.Etiqueta.INSTITUCION, 0))
    End Sub
    Protected Sub DDLinstitucion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.SelectedIndexChanged, DDLalumnos.SelectedIndexChanged
        Try
            'Para generar el contenido del GridView dinamicamente, es necesario que su propiedad AutoGenerateColumns = True 
            SDSreporte.ConnectionString = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            SDSreporte.SelectCommand = "select A.Matricula, A.ApePaterno, A.ApeMaterno, A.Nombre, G.Descripcion Grupo, P.Descripcion Plantel " + _
                        "from Alumno A, Plantel P, Grupo G where P.IdInstitucion = " + DDLinstitucion.SelectedValue.ToString + _
                        " and A.IdPlantel = P.IdPlantel and A.Estatus = 'Activo' and G.IdGrupo = A.IdGrupo and A.FechaIngreso " + DDLalumnos.SelectedValue.ToString + _
                        " order by P.Descripcion, G.Descripcion, A.ApePaterno, A.ApeMaterno, A.Nombre"
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVsalida_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVsalida.DataBound
        GVsalida.Caption = "<font face=""Arial"" size=3> <b>" + DDLinstitucion.SelectedItem.ToString + "<BR>" + _
                            "Total de " & Config.Etiqueta.ALUMNOS & " " + DDLalumnos.SelectedItem.ToString + ": " + GVsalida.Rows.Count.ToString + _
                            " al " + Date.Today.ToShortDateString + "</b></font>"
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVsalida, "RepRegistro")
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    ' Métodos de GridViews
    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVsalida_PreRender(sender As Object, e As EventArgs) Handles GVsalida.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVsalida.Rows.Count > 0 Then
            GVsalida.HeaderRow.TableSection = TableRowSection.TableHeader
        End If

        ' Etiquetas de GridView
        GVsalida.Columns(4).HeaderText = Config.Etiqueta.GRUPO

        GVsalida.Columns(5).HeaderText = Config.Etiqueta.PLANTEL
    End Sub

End Class
