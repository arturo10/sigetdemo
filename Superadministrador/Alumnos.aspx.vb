﻿Imports Siget

Imports System.Data
Imports System.IO
Imports System.Data.OleDb 'Este es para MS Access
Imports System.Data.SqlClient

Partial Class superadministrador_Alumnos
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Utils.Sesion.sesionAbierta()

    Label1.Text = Config.Etiqueta.ALUMNOS
    Label2.Text = Config.Etiqueta.INSTITUCION
    Label3.Text = Config.Etiqueta.PLANTEL
    Label4.Text = Config.Etiqueta.NIVEL
    Label5.Text = Config.Etiqueta.GRADO
    Label6.Text = Config.Etiqueta.CICLO
    Label7.Text = Config.Etiqueta.GRUPO
    Label8.Text = Config.Etiqueta.ALUMNO
    Label9.Text = Config.Etiqueta.EQUIPO
    Label10.Text = Config.Etiqueta.SUBEQUIPO
    Label11.Text = Config.Etiqueta.ALUMNO
  End Sub

  Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click
    TBnombre.Text = ""
    TBApePaterno.Text = ""
    TBApeMaterno.Text = ""
    TBmatricula.Text = ""
    DDLequipo.Text = ""
    DDLsubequipo.Text = ""
    TBemail.Text = ""
    TBingreso.Text = ""
    DDLestatus.ClearSelection()
    Mensaje.Text = "Capture los datos"
  End Sub

  Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
    Try
      'DESHABILITE EL BOTON DE ESTA FUNCION PORQUE LA CREACION DEL ALUMNO ES EN "REGISTRAR"
      If Trim(TBingreso.Text) = "" Then
        SDSalumnos.InsertCommand = "SET dateformat dmy; INSERT INTO Alumno (IdGrupo,IdPlantel,Nombre,ApePaterno,ApeMaterno,Matricula,Equipo,Subequipo,Email,Estatus,FechaModif, Modifico) VALUES (" + DDLgrupo.SelectedValue + "," + DDLplantel.SelectedValue + ",'" + TBnombre.Text + "','" + TBApePaterno.Text + "','" + TBApeMaterno.Text + "','" + TBmatricula.Text + "','" + DDLequipo.SelectedItem.ToString + "','" + DDLsubequipo.SelectedItem.ToString + "','" + TBemail.Text + "','" + DDLestatus.SelectedValue + "',getdate(),'" & User.Identity.Name & "')"
      Else
        SDSalumnos.InsertCommand = "SET dateformat dmy; INSERT INTO Alumno (IdGrupo,IdPlantel,Nombre,ApePaterno,ApeMaterno,Matricula,Equipo,Subequipo,Email,FechaIngreso,Estatus,FechaModif, Modifico) VALUES (" + DDLgrupo.SelectedValue + "," + DDLplantel.SelectedValue + ",'" + TBnombre.Text + "','" + TBApePaterno.Text + "','" + TBApeMaterno.Text + "','" + TBmatricula.Text + DDLequipo.SelectedItem.ToString + "','" + DDLsubequipo.SelectedItem.ToString + "','" + "','" + TBemail.Text + "','" + TBingreso.Text + "','" + DDLestatus.SelectedValue + "',getdate(),'" & User.Identity.Name & "')"
      End If
      SDSalumnos.Insert()
      Mensaje.Text = "El registro ha sido guardado"
      msgError.hide()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      Mensaje.Text = ""
    End Try
  End Sub

  Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
    DDLinstitucion.Items.Insert(0, New ListItem("---Elija la " & Config.Etiqueta.INSTITUCION, 0))
  End Sub

  Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
    DDLplantel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.PLANTEL, 0))
  End Sub

  Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
    DDLnivel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.NIVEL, 0))
  End Sub

  Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
    DDLgrado.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRADO, 0))
  End Sub

  Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
    DDLgrupo.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRUPO, 0))
  End Sub

  Protected Sub GVAlumnos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVAlumnos.SelectedIndexChanged
    'La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
    TBnombre.Text = HttpUtility.HtmlDecode(Trim(GVAlumnos.SelectedRow.Cells(4).Text))
    TBApePaterno.Text = HttpUtility.HtmlDecode(Trim(GVAlumnos.SelectedRow.Cells(2).Text))
    TBApeMaterno.Text = HttpUtility.HtmlDecode(Trim(GVAlumnos.SelectedRow.Cells(3).Text))
    If GVAlumnos.SelectedRow.Cells(5).Text = "&nbsp;" Then
      TBmatricula.Text = ""
    Else
      TBmatricula.Text = HttpUtility.HtmlDecode(Trim(GVAlumnos.SelectedRow.Cells(5).Text))
    End If
    TBemail.Text = HttpUtility.HtmlDecode(Trim(GVAlumnos.SelectedRow.Cells(6).Text))
    TBingreso.Text = HttpUtility.HtmlDecode(Trim(GVAlumnos.SelectedRow.Cells(7).Text))
    DDLestatus.SelectedValue = Trim(GVAlumnos.SelectedRow.Cells(10).Text)
    'OJO: Si se graba un " " (espacio vacío) en el campo, marca error al seleccionar
    If Trim(GVAlumnos.SelectedRow.Cells(11).Text) = "&nbsp;" Then
      DDLequipo.SelectedValue = ""
    Else
      DDLequipo.SelectedValue = HttpUtility.HtmlDecode(Trim(GVAlumnos.SelectedRow.Cells(11).Text))
    End If
    If Trim(GVAlumnos.SelectedRow.Cells(12).Text) = "&nbsp;" Then
      DDLsubequipo.SelectedValue = ""
    Else
      DDLsubequipo.SelectedValue = Trim(GVAlumnos.SelectedRow.Cells(12).Text)
    End If

  End Sub

  Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
    If Trim(GVAlumnos.SelectedRow.Cells(10).Text) <> "Activo" Then
      Try
        'BORRO EL USUARIO DE LAS BASES DE DATOS
        Dim objCommand As New SqlClient.SqlCommand("EliminaAlumno"), Conexion As String = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.Add("@IdAlumno", SqlDbType.Int, 8)
        objCommand.Parameters("@IdAlumno").Value = CInt(GVAlumnos.SelectedRow.Cells(1).Text)
        objCommand.CommandTimeout = 3000
        objCommand.Connection = New SqlClient.SqlConnection(Conexion)
        objCommand.Connection.Open()
        objCommand.ExecuteNonQuery()

        Membership.DeleteUser(GVAlumnos.SelectedRow.Cells(8).Text.Trim())

        GVAlumnos.DataBind()
        msgError.hide()
        Mensaje.Text = "El " &
        Config.Etiqueta.ALUMNO &
        " ha sido ELIMINADO junto con todo su historial de actividades y respuestas."

        'NO LO USÉ PERO VER SI FUNCIONA MEJOR:
        'SDSborraalumno.DeleteParameters.Add("@Login", GVAlumnos.SelectedRow.Cells(8).Text)
        'SDSborraalumno.DeleteParameters.Add("@IdAlumno", CInt(GVAlumnos.SelectedRow.Cells(1).Text))
        'SDSborraalumno.Delete()
      Catch ex As Exception
        Utils.LogManager.ExceptionLog_InsertEntry(ex)
        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        Mensaje.Text = ""
      End Try
    Else
      msgError.show("El " &
   Config.Etiqueta.ALUMNO &
   " no puede ser eliminado si está con el estatus de Activo. Considere que al eliminarlo se borrará todo su historial de actividades y respuestas.")
      Mensaje.Text = ""
    End If
  End Sub

  Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
    Try
      If Trim(TBingreso.Text) <> "" Then
        SDSalumnos.UpdateCommand = "SET dateformat dmy; UPDATE Alumno SET Nombre = '" + TBnombre.Text + "',ApePaterno = '" + _
                TBApePaterno.Text + "',ApeMaterno = '" + TBApeMaterno.Text + "',Matricula = '" + _
                TBmatricula.Text + "',Equipo = '" + DDLequipo.SelectedItem.ToString + "',Subequipo = '" + DDLsubequipo.SelectedItem.ToString + "',Email = '" + TBemail.Text + "',FechaIngreso = '" + _
                TBingreso.Text + "',Estatus = '" + DDLestatus.SelectedValue.ToString + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name + _
                "' where IdAlumno = " + GVAlumnos.SelectedRow.Cells(1).Text
      Else
        'Marca error si el campo FechaIngreso se ingresa en vacío
        SDSalumnos.UpdateCommand = "SET dateformat dmy; UPDATE Alumno SET Nombre = '" + TBnombre.Text + "',ApePaterno = '" + _
                TBApePaterno.Text + "',ApeMaterno = '" + TBApeMaterno.Text + "',Matricula = '" + TBmatricula.Text + _
                "',Equipo = '" + DDLequipo.SelectedItem.ToString + "',Subequipo = '" + DDLsubequipo.SelectedItem.ToString + "',Email = '" + TBemail.Text + "',FechaIngreso = NULL, Estatus = '" + DDLestatus.SelectedValue.ToString + _
                "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdAlumno = " + GVAlumnos.SelectedRow.Cells(1).Text
      End If
      SDSalumnos.Update()

      Mensaje.Text = "El registro ha sido actualizado"
      GVAlumnos.DataBind()
      msgError.hide()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      Mensaje.Text = ""
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub DDLgrupo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.SelectedIndexChanged
    'LLAMAR A PROCEDIMIENTO: Limpiar_Click
    TBnombre.Text = ""
    TBApePaterno.Text = ""
    TBApeMaterno.Text = ""
    TBmatricula.Text = ""
    TBemail.Text = ""
    TBingreso.Text = ""
    DDLestatus.ClearSelection()
    Mensaje.Text = "Capture los datos"
  End Sub

  Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
    GVactual.Caption = Config.Etiqueta.ALUMNOS & " de " & Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO & " " & DDLgrupo.SelectedItem.Text

    ' añado los estilos, por que el gridview no se exporta predefinidamente con css
    GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
    GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
    GVactual.GridLines = GridLines.Both

    Dim headerText As String
    ' quito la imágen de ordenamiento de los encabezados
    For Each tc As TableCell In GVactual.HeaderRow.Cells
      If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
        headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
        tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
        tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
        tc.Text = headerText ' establezco el texto simple
      End If
    Next

    'Este procedimiento convierte el GridView a Excel eliminando la primera y segunda columna
    If GVactual.Rows.Count.ToString + 1 < 65536 Then
      ' antes de exportar la copia del gridview, en cada fila:
      For Each r As GridViewRow In GVactual.Rows()
        ' elimino la columna de "seleccionar"
        r.Cells.RemoveAt(0)
      Next
      ' ahora quito también la columna de seleccionar, y una del footer para que no se desfase
      GVactual.HeaderRow.Cells.RemoveAt(0)

      Dim sb As StringBuilder = New StringBuilder()
      Dim sw As StringWriter = New StringWriter(sb)
      Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
      Dim pagina As Page = New Page
      Dim form = New HtmlForm
      pagina.EnableEventValidation = False
      pagina.DesignerInitialize()
      pagina.Controls.Add(form)
      form.Controls.Add(GVactual)
      pagina.RenderControl(htw)
      Response.Clear()
      Response.Buffer = True
      Response.ContentType = "application/vnd.ms-excel"
      Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
      Response.Charset = "UTF-8"
      Response.ContentEncoding = Encoding.Default
      Response.Write(sb.ToString())
      Response.End()
    Else
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
    End If
  End Sub

  Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
    Convierte(GVAlumnos, "Grupo-" + HttpUtility.HtmlDecode(DDLgrado.SelectedItem.ToString) + "-" + HttpUtility.HtmlDecode(DDLgrupo.SelectedItem.ToString))
  End Sub

  Protected Sub DDLinstitucion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLinstitucion.SelectedIndexChanged
    'Limpio por si las dudas
    DDLequipo.Items.Clear()
    DDLsubequipo.Items.Clear()

    Dim strConexion As String
    strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
    Dim objConexion As New SqlConnection(strConexion)
    Dim miComando As SqlCommand
    Dim misRegistros As SqlDataReader
    miComando = objConexion.CreateCommand
    'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
    miComando.CommandText = "select * from Equipo where IdInstitucion = " + DDLinstitucion.SelectedValue.ToString + " order by Descripcion desc"
    Try
      objConexion.Open()
      'CARGO LOS EQUIPOS:
      misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
      Do While misRegistros.Read()
        DDLequipo.Items.Insert(0, misRegistros.Item("Descripcion"))
      Loop
      DDLequipo.Items.Insert(0, "")
      'AHORA CARGO LOS SUBEQUIPOS:
      misRegistros.Close()
      miComando.CommandText = "select * from Subequipo where IdInstitucion = " + DDLinstitucion.SelectedValue.ToString + " order by Descripcion desc"
      misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
      Do While misRegistros.Read()
        DDLsubequipo.Items.Insert(0, misRegistros.Item("Descripcion"))
      Loop
      DDLsubequipo.Items.Insert(0, "")
      misRegistros.Close()
      objConexion.Close()
      msgError.hide()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
  ' Métodos de GridViews
  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GVAlumnos_PreRender(sender As Object, e As EventArgs) Handles GVAlumnos.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVAlumnos.Rows.Count > 0 Then
      GVAlumnos.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVAlumnos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVAlumnos.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVAlumnos.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GVAlumnos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVAlumnos.RowDataBound
    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(1)
      LnkHeaderText.Text = "Id_" & Config.Etiqueta.ALUMNO

      LnkHeaderText = e.Row.Cells(11).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.EQUIPO

      LnkHeaderText = e.Row.Cells(12).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.SUBEQUIPO
    End If

    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVAlumnos, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVAlumnos.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVAlumnos, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

End Class
