﻿Imports Siget

Partial Class superadministrador_BorraEvaluaciones
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GValumnosterminados.Caption = "<font size='2'><b>" + Config.Etiqueta.ALUMNOS &
                    " con actividad realizada. Seleccione el resultado que desee eliminar </b></font>"

        Label1.Text = Config.Etiqueta.ALUMNO
        Label2.Text = Config.Etiqueta.INSTITUCION
        Label3.Text = Config.Etiqueta.PLANTEL
        Label4.Text = Config.Etiqueta.NIVEL
        Label5.Text = Config.Etiqueta.GRADO
        Label6.Text = Config.Etiqueta.CICLO
        Label7.Text = Config.Etiqueta.ASIGNATURA
        Label8.Text = Config.Etiqueta.GRUPO
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija una " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLcalificacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacion.DataBound
        DDLcalificacion.Items.Insert(0, New ListItem("---Elija una Calificación", 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub BtnBorrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBorrar.Click
        Try
            SDSevaluacionterminada.DeleteCommand = "DELETE from EvaluacionTerminada WHERE IdAlumno = " + GValumnosterminados.SelectedRow.Cells(1).Text + _
                                                " and IdEvaluacion = " + GVevaluaciones.SelectedRow.Cells(1).Text
            SDSevaluacionterminada.Delete()

            SDSrespuestas.DeleteCommand = "DELETE FROM RespuestaAbierta WHERE IdRespuesta in (select IdRespuesta " + _
                            "from Respuesta where IdAlumno = " + GValumnosterminados.SelectedRow.Cells(1).Text + " and " + _
                            "IdDetalleEvaluacion in (select IdDetalleEvaluacion from DetalleEvaluacion where IdEvaluacion = " + _
                            GVevaluaciones.SelectedRow.Cells(1).Text + "))"
            SDSrespuestas.Delete()

            SDSrespuestas.DeleteCommand = "DELETE FROM Respuesta where IdAlumno = " + GValumnosterminados.SelectedRow.Cells(1).Text + " and " + _
                            "IdDetalleEvaluacion in (select IdDetalleEvaluacion from DetalleEvaluacion where IdEvaluacion = " + _
                            GVevaluaciones.SelectedRow.Cells(1).Text + ")"

            SDSrespuestas.Delete()
            msgSuccess.show("Éxito", "El resultado del " + Config.Etiqueta.ALUMNO + " alumno ha sido eliminado")
            GValumnosterminados.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgSuccess.hide()
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GValumnosterminados_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GValumnosterminados.DataBound
        If GValumnosterminados.Rows.Count > 0 Then
            BtnBorrar.Visible = True
        Else
            BtnBorrar.Visible = False
        End If
    End Sub

    Protected Sub GValumnosterminados_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GValumnosterminados.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.ALUMNO
        End If
    End Sub

End Class
