﻿Imports Siget

Imports System.IO
Imports System.Data.SqlClient

Partial Class superadministrador_Subtemas
    Inherits System.Web.UI.Page

    Public Class GlobalVar
        Public Shared archivoCargado As System.Web.HttpPostedFile
        Public Shared nombreArchivo As String
    End Class

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        TitleLiteral.Text = "Administración de Subtemas"
        Label1.Text = Config.Etiqueta.NIVEL
        Label2.Text = Config.Etiqueta.GRADO
        Label3.Text = Config.Etiqueta.ASIGNATURA
    End Sub

    Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click
        TBnumero.Text = ""
        TBdescripcion.Text = ""

        TBconsecutivo.Text = ""
        msgError.hide()
        msgSuccess.hide()
        msgInfo.hide()
    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        msgError.hide()
        msgSuccess.hide()
        msgInfo.hide()

        Try
            SDSsubtemas.InsertCommand = "SET dateformat dmy; INSERT INTO Subtema (IdTema,IdCompetencia,Descripcion,Numero, FechaModif, Modifico) VALUES (" + DDLtemas.SelectedValue + "," + DDLcompetencias.SelectedValue + ",'" + TBdescripcion.Text + "','" + TBnumero.Text + "', getdate(), '" & User.Identity.Name & "')"
            SDSsubtemas.Insert()
            msgSuccess.show("Éxito", "El registro ha sido guardado. Puede usted cargarle material de apoyo si lo requiere.")
            msgError.hide()
            GVsubtemas.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVsubtemas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVsubtemas.SelectedIndexChanged
        'La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
        TBnumero.Text = GVsubtemas.SelectedRow.Cells(4).Text
        TBdescripcion.Text = HttpUtility.HtmlDecode(GVsubtemas.SelectedRow.Cells(5).Text)
        DDLcompetencias.SelectedValue = CInt(GVsubtemas.SelectedRow.Cells(1).Text)
        HLarchivo.Visible = False
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        msgError.hide()
        msgSuccess.hide()
        msgInfo.hide()

        Try
            SDSsubtemas.UpdateCommand = "SET dateformat dmy; UPDATE Subtema SET Numero = " + TBnumero.Text + ", Descripcion = '" + TBdescripcion.Text + "', IdCompetencia = " + DDLcompetencias.SelectedValue + ", FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdSubTema =" + GVsubtemas.SelectedRow.Cells(3).Text
            SDSsubtemas.Update()
            msgSuccess.show("Éxito", "El registro ha sido actualizado.")
            msgError.hide()
            GVsubtemas.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLtemas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLtemas.SelectedIndexChanged, DDLtemas.TextChanged
        GVsubtemas.DataBind()
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija la " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLcompetencias_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcompetencias.DataBound
        DDLcompetencias.Items.Insert(0, New ListItem("---Elija la Competencia", 0))
    End Sub

    Protected Sub DDLtemas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLtemas.DataBound
        DDLtemas.Items.Insert(0, New ListItem("---Elija el Tema", 0))
    End Sub

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        msgError.hide()
        msgSuccess.hide()
        msgInfo.hide()

        Try
            SDSsubtemas.DeleteCommand = "Delete from Subtema where IdSubtema = " + GVsubtemas.SelectedValue.ToString 'Me da el IdSubtema porque es el campo clave de la fila seleccionada
            SDSsubtemas.Delete()
            GVsubtemas.DataBind()

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub cargar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cargar.Click
        msgError.hide()
        msgSuccess.hide()
        msgInfo.hide()

        Try
            If (fuArchivo.HasFile) Or (Trim(TBlink.Text).Length > 0) Then 'El atributo .HasFile compara si se indico un archivo
                If Trim(TBconsecutivo.Text) = "" Then
                    TBconsecutivo.Text = "0"
                End If

                If DDLtipoarchivo.SelectedValue = "Link" Or DDLtipoarchivo.SelectedValue = "YouTube" Then
                    GlobalVar.nombreArchivo = Trim(TBlink.Text)
                Else
                    GlobalVar.nombreArchivo = fuArchivo.FileName
                End If

                'La siguiente linea la pongo aqui para no repetirla en cada caso, si no se carga el archivo no se aplica
                SDSmaterialapoyo.InsertParameters.Clear()
                SDSmaterialapoyo.InsertCommand = "SET dateformat dmy; INSERT INTO ApoyoSubtema (IdSubTema,Consecutivo, Descripcion,ArchivoApoyo,TipoArchivoApoyo, FechaModif, Modifico) VALUES (@IdSubTema,@Consecutivo,@Descripcion,@ArchivoApoyo,@TipoArchivoApoyo, getdate(), @Modifico)"
                SDSmaterialapoyo.InsertParameters.Add("IdSubTema", GVsubtemas.SelectedValue.ToString)
                SDSmaterialapoyo.InsertParameters.Add("Consecutivo", TBconsecutivo.Text)
                SDSmaterialapoyo.InsertParameters.Add("Descripcion", tbApoyodescripcion.Text.Trim())
                SDSmaterialapoyo.InsertParameters.Add("ArchivoApoyo", GlobalVar.nombreArchivo)
                SDSmaterialapoyo.InsertParameters.Add("TipoArchivoApoyo", DDLtipoarchivo.SelectedValue)
                SDSmaterialapoyo.InsertParameters.Add("Modifico", User.Identity.Name.Trim())

                'Ahora CARGO el archivo solo si NO es un link
                If DDLtipoarchivo.SelectedValue <> "Link" AndAlso DDLtipoarchivo.SelectedValue <> "YouTube" Then
                    Dim ruta As String
          ruta = Config.Global.rutaMaterial

          Dim MiArchivo2 As FileInfo = New FileInfo(ruta & fuArchivo.FileName)

          If MiArchivo2.Exists Then
            'Avisa que ya existe, NO LO SUBE pero sí almacena el registro
            'LblError.Text = "YA EXISTE UN ARCHIVO CON ESE NOMBRE EN EL ALMACEN DE ARCHIVOS, CAMBIE EL NOMBRE DE SU ARCHIVO E INTENTE CARGARLO NUEVAMENTE, POR FAVOR."

            ' guardo el archivo para que no se pierda en el primer postback
            GlobalVar.archivoCargado = fuArchivo.PostedFile

            'Checo si ese archivo no está asignado a otro subtema, si lo está, advierto al usuario
            Dim Total = 0
            Dim strConexion As String
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objConexion As New SqlConnection(strConexion)
            Dim miComando As SqlCommand
            Dim misRegistros As SqlDataReader
            miComando = objConexion.CreateCommand
            miComando.CommandText = "select Count(*) as Total from ApoyoSubtema where ArchivoApoyo = '" + GlobalVar.nombreArchivo + "'"
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            Total = misRegistros.Item("Total")

            misRegistros.Close()
            miComando.CommandText = "select Count(*) as Total from Opcion where ArchivoApoyo = '" + GlobalVar.nombreArchivo + "'"
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            Total = Total + misRegistros.Item("Total")

            misRegistros.Close()
            miComando.CommandText = "select Count(*) as Total from Planteamiento where ArchivoApoyo = '" + GlobalVar.nombreArchivo + "' or ArchivoApoyo2 = '" + GlobalVar.nombreArchivo + "'"
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            Total = Total + misRegistros.Item("Total")
            misRegistros.Close()
            objConexion.Close()

            If Not Total = 0 Then
              ' notifico que está asignado a más subtemas y el archivo no se eliminó
              msgWarning.show("Cuidado", "Este recurso está asignado a otros subtemas/opciones/planteamientos.")
            End If

            HLexiste.Text = Trim(HttpUtility.HtmlDecode(fuArchivo.FileName))
            HLexiste.NavigateUrl = Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(fuArchivo.FileName))
            HF1_ModalPopupExtender.Show()
          Else
            fuArchivo.SaveAs(ruta & fuArchivo.FileName)
            HLarchivo.Visible = True
            HLarchivo.Text = "Archivo cargado: " & Trim(HttpUtility.HtmlDecode(fuArchivo.FileName))
            HLarchivo.NavigateUrl = Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(fuArchivo.FileName))

            SDSmaterialapoyo.Insert()
            SDSmaterialapoyo.DataBind()
            msgSuccess.show("Éxito", "El archivo ha sido cargado")
            msgError.hide()
          End If
        Else
          HLarchivo.Visible = True
          HLarchivo.Text = TBlink.Text
          HLarchivo.NavigateUrl = TBlink.Text
          SDSmaterialapoyo.Insert()
          SDSmaterialapoyo.DataBind()
          msgSuccess.show("Éxito", "El enlace ha sido guardado")
          msgError.hide()
        End If
      Else
        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "No ha seleccionado ningún archivo para cargar.")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub Quitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Quitar.Click
    Try
      SDSmaterialapoyo.DeleteCommand = "Delete from ApoyoSubtema where IdApoyoSubtema = " & GVmaterialapoyo.SelectedDataKey(0)
      SDSmaterialapoyo.Delete()

      If GVmaterialapoyo.SelectedRow.Cells(6).Text <> "Link" AndAlso GVmaterialapoyo.SelectedRow.Cells(6).Text <> "YouTube" Then
        'Checo si ese archivo no está asignado a otro subtema, si lo está, no borro el archivo, solo el registro en la tabla ApoyoSubtema
        Dim Total = 0
        Dim strConexion As String
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        miComando = objConexion.CreateCommand
        miComando.CommandText = "select Count(*) as Total from ApoyoSubtema where ArchivoApoyo = '" + Trim(HttpUtility.HtmlDecode(GVmaterialapoyo.SelectedRow.Cells(5).Text)) + "' and IdApoyoSubtema <> " + GVmaterialapoyo.SelectedValue.ToString
        objConexion.Open()
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        misRegistros.Read()
        Total = misRegistros.Item("Total")

        misRegistros.Close()
        miComando.CommandText = "select Count(*) as Total from Opcion where ArchivoApoyo = '" + Trim(HttpUtility.HtmlDecode(GVmaterialapoyo.SelectedRow.Cells(5).Text)) + "'"
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        misRegistros.Read()
        Total = Total + misRegistros.Item("Total")

        misRegistros.Close()
        miComando.CommandText = "select Count(*) as Total from Planteamiento where ArchivoApoyo = '" + Trim(HttpUtility.HtmlDecode(GVmaterialapoyo.SelectedRow.Cells(5).Text)) + "' or ArchivoApoyo2 = '" + Trim(HttpUtility.HtmlDecode(GVmaterialapoyo.SelectedRow.Cells(5).Text)) + "'"
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        misRegistros.Read()
        Total = Total + misRegistros.Item("Total")
        misRegistros.Close()
        objConexion.Close()

        If Total = 0 Then
          'Elimino el archivo de apoyo que tenga la opción
          'NO NECESITO PORQUE ES DATO FORZOSO: If Trim(HttpUtility.HtmlDecode(GVsubtemas.SelectedRow.Cells(5).Text)).Length > 1 Then 'Significa que tiene un valor en ese campo
          'Y si fuera Link, de todos modos busco borrar algo no vaya a ser que se equivocaron en poner "link" y si es archivo

          Dim ruta As String
          ruta = Config.Global.rutaMaterial

          'Borro el archivo que estaba anteriormente porque se pondrá uno nuevo
          Dim MiArchivo As FileInfo = New FileInfo(ruta & Trim(HttpUtility.HtmlDecode(GVmaterialapoyo.SelectedRow.Cells(5).Text)))
          MiArchivo.Delete()
          HLarchivo.Visible = False
        Else
          ' notifico que está asignado a más subtemas y el archivo no se eliminó
          msgInfo.show("Atención", "El recurso que quitó está asignado a otros subtemas, por lo que no se eliminó físicamente del sistema.")
        End If
      End If

      SDSmaterialapoyo.DataBind() 'Dejo esta instrucción al final porque si se refresca el GridView antes de borrar el archivo de apoyo, ya no podré saber su nombre
      HLarchivo.Text = ""
      HLarchivo.NavigateUrl = ""
      msgError.hide()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub GVmaterialapoyo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmaterialapoyo.SelectedIndexChanged
    If Trim(GVmaterialapoyo.SelectedRow.Cells(6).Text) = "Link" Or Trim(GVmaterialapoyo.SelectedRow.Cells(6).Text) = "YouTube" Then
      fuArchivo.Visible = False
      TBlink.Visible = True
      TBlink.Text = GVmaterialapoyo.SelectedRow.Cells(5).Text

      HLarchivo.Visible = True
      HLarchivo.Text = GVmaterialapoyo.SelectedRow.Cells(5).Text
      HLarchivo.NavigateUrl = GVmaterialapoyo.SelectedRow.Cells(5).Text
    Else
      fuArchivo.Visible = True
      TBlink.Visible = False
      HLarchivo.Visible = True
      HLarchivo.Text = Trim(HttpUtility.HtmlDecode(GVmaterialapoyo.SelectedRow.Cells(5).Text))
      HLarchivo.NavigateUrl = Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(GVmaterialapoyo.SelectedRow.Cells(5).Text))
      TBlink.Text = ""
    End If
    tbApoyodescripcion.Text = HttpUtility.HtmlDecode(GVmaterialapoyo.SelectedRow.Cells(4).Text)
    TBconsecutivo.Text = GVmaterialapoyo.SelectedRow.Cells(3).Text
    DDLtipoarchivo.SelectedValue = GVmaterialapoyo.SelectedRow.Cells(6).Text
  End Sub

  Protected Sub DDLtipoarchivo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLtipoarchivo.SelectedIndexChanged
    If DDLtipoarchivo.SelectedValue = "Link" Or DDLtipoarchivo.SelectedValue = "YouTube" Then
      fuArchivo.Visible = False
      TBlink.Visible = True
      If DDLtipoarchivo.SelectedValue = "Link" Then
        msgInfo.show("Revise", "Si el enlace es a una página fuera del sistema, el link debe incluir dos diagonales al inicio (//). <br />Por ejemplo: //www.google.com.mx")
      Else
        msgInfo.hide()
      End If
    Else
      fuArchivo.Visible = True
      TBlink.Visible = False
    End If
  End Sub

  Protected Sub GVmaterialapoyo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmaterialapoyo.DataBound
    If GVmaterialapoyo.Rows.Count > 0 Then
      Quitar.Visible = True
    Else
      Quitar.Visible = False
    End If
  End Sub

  ' _________________________________________________________________________________________________________________
  ' _________________________________________________________________________________________________________________ DIALOGS

  Protected Sub btnDialogAssign_Click(sender As Object, e As EventArgs) Handles btnDialogAssign.Click
    Try
      HLarchivo.Visible = True
      HLarchivo.Text = "Archivo asignado: " & Trim(HttpUtility.HtmlDecode(GlobalVar.nombreArchivo))
      HLarchivo.NavigateUrl = Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(GlobalVar.nombreArchivo))

      SDSmaterialapoyo.InsertParameters.Clear()
      SDSmaterialapoyo.InsertCommand = "SET dateformat dmy; INSERT INTO ApoyoSubtema (IdSubTema,Consecutivo, Descripcion,ArchivoApoyo,TipoArchivoApoyo, FechaModif, Modifico) VALUES (@IdSubTema,@Consecutivo,@Descripcion,@ArchivoApoyo,@TipoArchivoApoyo, getdate(), @Modifico)"
      SDSmaterialapoyo.InsertParameters.Add("IdSubTema", GVsubtemas.SelectedValue.ToString)
      SDSmaterialapoyo.InsertParameters.Add("Consecutivo", TBconsecutivo.Text)
      SDSmaterialapoyo.InsertParameters.Add("Descripcion", tbApoyodescripcion.Text.Trim())
      SDSmaterialapoyo.InsertParameters.Add("ArchivoApoyo", GlobalVar.nombreArchivo)
      SDSmaterialapoyo.InsertParameters.Add("TipoArchivoApoyo", DDLtipoarchivo.SelectedValue)
      SDSmaterialapoyo.InsertParameters.Add("Modifico", User.Identity.Name.Trim())

      SDSmaterialapoyo.Insert()
      GVmaterialapoyo.DataBind()
      msgSuccess.show("Éxito", "El archivo ha sido asignado.")
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub btnDialogOverwrite_Click(sender As Object, e As EventArgs) Handles btnDialogOverwrite.Click
    Try
      Dim oldFile As FileInfo = New FileInfo(Config.Global.rutaMaterial & GlobalVar.nombreArchivo)

      oldFile.Delete()

      GlobalVar.archivoCargado.SaveAs(Config.Global.rutaMaterial & GlobalVar.nombreArchivo)

      HLarchivo.Visible = True
      HLarchivo.Text = "Archivo asignado: " & Trim(HttpUtility.HtmlDecode(GlobalVar.nombreArchivo))
      HLarchivo.NavigateUrl = Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(GlobalVar.nombreArchivo))

      SDSmaterialapoyo.InsertParameters.Clear()
      SDSmaterialapoyo.InsertCommand = "SET dateformat dmy; INSERT INTO ApoyoSubtema (IdSubTema,Consecutivo, Descripcion,ArchivoApoyo,TipoArchivoApoyo, FechaModif, Modifico) VALUES (@IdSubTema,@Consecutivo,@Descripcion,@ArchivoApoyo,@TipoArchivoApoyo, getdate(), @Modifico)"
      SDSmaterialapoyo.InsertParameters.Add("IdSubTema", GVsubtemas.SelectedValue.ToString)
      SDSmaterialapoyo.InsertParameters.Add("Consecutivo", TBconsecutivo.Text)
      SDSmaterialapoyo.InsertParameters.Add("Descripcion", tbApoyodescripcion.Text.Trim())
      SDSmaterialapoyo.InsertParameters.Add("ArchivoApoyo", GlobalVar.nombreArchivo)
      SDSmaterialapoyo.InsertParameters.Add("TipoArchivoApoyo", DDLtipoarchivo.SelectedValue)
      SDSmaterialapoyo.InsertParameters.Add("Modifico", User.Identity.Name.Trim())

      SDSmaterialapoyo.Insert()
      GVmaterialapoyo.DataBind()
      msgSuccess.show("Éxito", "El archivo ha sido sobreescrito.")
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

    Protected Sub btn_actualizaMaterial_Click(sender As Object, e As EventArgs) Handles btn_actualizaMaterial.Click
        msgError.hide()
        msgSuccess.hide()
        msgInfo.hide()

        Try
            If Trim(TBconsecutivo.Text) = "" Then
                TBconsecutivo.Text = "0"
            End If

            'La siguiente linea la pongo aqui para no repetirla en cada caso, si no se carga el archivo no se aplica
            SDSmaterialapoyo.InsertParameters.Clear()
            SDSmaterialapoyo.InsertCommand = "SET dateformat dmy; UPDATE ApoyoSubtema SET Consecutivo = @Consecutivo, Descripcion = @Descripcion, TipoArchivoApoyo = @TipoArchivoApoyo, FechaModif = getdate(), Modifico = @Modifico WHERE IdApoyoSubtema = @IdApoyoSubtema"
            SDSmaterialapoyo.InsertParameters.Add("IdApoyoSubtema", GVmaterialapoyo.SelectedDataKey(0))
            SDSmaterialapoyo.InsertParameters.Add("Consecutivo", TBconsecutivo.Text)
            SDSmaterialapoyo.InsertParameters.Add("Descripcion", tbApoyodescripcion.Text.Trim())
            SDSmaterialapoyo.InsertParameters.Add("TipoArchivoApoyo", DDLtipoarchivo.SelectedValue)
            SDSmaterialapoyo.InsertParameters.Add("Modifico", User.Identity.Name.Trim())

            SDSmaterialapoyo.Insert()
            SDSmaterialapoyo.DataBind()
            msgSuccess.show("Éxito", "El enlace ha sido guardado")
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub
End Class
