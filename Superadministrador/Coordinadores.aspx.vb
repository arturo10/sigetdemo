﻿Imports Siget

Imports System.Web.Security.Membership
Imports System.Data.SqlClient
Imports System.Data.OleDb 'Este es para MS Access
Imports System.Data

Partial Class superadministrador_Coordinadores
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label2.Text = Config.Etiqueta.COORDINADORES
        Label3.Text = Config.Etiqueta.COORDINADOR

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString

        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        miComando = objConexion.CreateCommand
        miComando.CommandText = "select Count(IdCoordinador) as Total from Coordinador"
        objConexion.Open() 'Abro la conexion
        misRegistros = miComando.ExecuteReader()
        misRegistros.Read()
        If misRegistros.Item("Total") = 0 Then
            'DVcoordinadores.DefaultMode = DetailsViewMode.Insert LO HABÍA PUESTO ASI PERO LUEGO NO SE PUEDE QUITAR ESE MODO
            SDScoordinadores.InsertCommand = "SET dateformat dmy; INSERT INTO Coordinador(Nombre, Apellidos, Estatus) values('DISPONIBLE PARA MODIFICAR','DISPONIBLE PARA MODIFICAR','Activo')"
            SDScoordinadores.Insert()
        End If
        misRegistros.Close()
        objConexion.Close()
    End Sub

    Protected Sub DVcoordinadores_ItemDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeleteEventArgs) Handles DVcoordinadores.ItemDeleting
        'Antes de que se borre el registro en la Tabla Coordinador, busco los datos del Usuario:
        Dim IdCoordinador As String
        IdCoordinador = Trim(DVcoordinadores.Rows(0).Cells(1).Text)
        Session("Login") = ""
        Session("IdUsuario") = ""
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        miComando = objConexion.CreateCommand
        Try
            'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
            miComando.CommandText = "select IdUsuario,Login from Usuario where IdUsuario in (select IdUsuario from Coordinador where IdCoordinador = " + IdCoordinador + ")"
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            If misRegistros.Read() Then
                Session("IdUsuario") = misRegistros.Item("IdUsuario").ToString
                Session("Login") = Trim(misRegistros.Item("Login"))
            End If
            misRegistros.Close()
            objConexion.Close()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DVcoordinadores_ItemDeleted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewDeletedEventArgs) Handles DVcoordinadores.ItemDeleted
        Try
            'UNA VEZ QUE SE BORRÓ EL REGISTRO DEL COORDINADOR, YA PUEDO BORRAR EL USUARIO Y LO DEMÁS
            If Trim(Session("Login")) <> "" Then
                SDScoordinadores.DeleteCommand = "DELETE FROM USUARIO where IdUsuario = " + Session("IdUsuario")
                SDScoordinadores.Delete()
                'Elimino Login de la tabla de seguridad
                Membership.DeleteUser(Session("Login"))


                'BORRO EL USUARIO DE LA BASE DE DATOS DEL BLOG
                'PRIMERO OBTENGO SU ID LEYENDO EL REGISTRO:
                Dim cnn As OleDbConnection
                Dim cmd As OleDbDataAdapter
                Dim dt As DataTable
                Dim str As String
                str = "SELECT * FROM PTusers where usr_User = '" + Session("IdUsuario") + "'"
                cnn = New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + _
                Server.MapPath("..\App_Data\blogbd.mdb") & ";")
                ' Crear el comando
                cmd = New OleDbDataAdapter(str, cnn)
                ' Llenar el DataTable
                dt = New DataTable
                cmd.Fill(dt)

                If dt.Rows.Count > 0 Then
                    Dim id As Integer
                    Dim cmdx As OleDbCommand

                    id = dt.Rows(0).Item("usr_ID")

                    'AQUI ELIMINO LOS REGISTROS DE LAS TABLAS:
                    cnn.Open()
                    str = "DELETE FROM PTposts WHERE usr_ID = " + id.ToString
                    cmdx = New OleDbCommand(str, cnn)
                    cmdx.ExecuteNonQuery()

                    str = "DELETE FROM PTcmts WHERE usr_ID = " + id.ToString
                    cmdx = New OleDbCommand(str, cnn)
                    cmdx.ExecuteNonQuery()

                    str = "DELETE FROM PTusers WHERE usr_ID = " + id.ToString
                    cmdx = New OleDbCommand(str, cnn)
                    cmdx.ExecuteNonQuery()

                End If
                cnn.Close()


                msgError.hide()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub
End Class
