﻿Imports Siget

Imports System.IO

Partial Class superadministrador_BuscaAlumno
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GValumnos.Caption = "<h3>Listado de " &
            Config.Etiqueta.ALUMNOS &
            " encontrad" & Config.Etiqueta.LETRA_ALUMNO & "s</h3>"

        Label1.Text = Config.Etiqueta.ALUMNO
        Label2.Text = Config.Etiqueta.INSTITUCION
        Label3.Text = Config.Etiqueta.ALUMNO
        Label4.Text = Config.Etiqueta.CICLO
        Label5.Text = Config.Etiqueta.PLANTEL
        Label6.Text = Config.Etiqueta.NIVEL
        Label7.Text = Config.Etiqueta.GRADO
        Label8.Text = Config.Etiqueta.GRUPO
        Label9.Text = Config.Etiqueta.ALUMNO
        Label10.Text = Config.Etiqueta.ARTDET_ALUMNO
    End Sub

    Protected Sub BtnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBuscar.Click
        msgError.hide()
        msgSuccess.hide()
        msgInfo.hide()

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Try
            SDSbuscar.ConnectionString = strConexion
            SDSbuscar.SelectCommand = "select Distinct A.IdAlumno, A.Matricula, A.Nombre, A.ApePaterno, A.ApeMaterno, G.IdGrupo, G.Descripcion Grupo, Gr.IdGrado, Gr.Descripcion Grado, N.IdNivel, " + _
                        "N.Descripcion Nivel, P.IdPlantel, P.Descripcion Plantel, U.Login, U.Password, A.Estatus, A.Equipo, A.Subequipo, C.Descripcion Ciclo, A.Email " + _
                        "from Alumno A, Grupo G, Grado Gr, Nivel N, Plantel P, Usuario U, CicloEscolar C " + _
                        "where U.IdUsuario = A.IdUsuario And G.IdGrupo = A.IdGrupo And Gr.IdGrado = G.IdGrado And N.IdNivel = Gr.IdNivel and C.IdCicloEscolar = G.IdCicloEscolar " + _
                        "And P.IdPlantel = G.IdPlantel and P.IdInstitucion = " + DDLinstitucion.SelectedValue + " and A.Matricula like '%" + TBmatricula.Text + "%' and A.Nombre like '%" + TBnombre.Text + "%' and A.ApePaterno like '%" + _
                        TBapepaterno.Text + "%' and A.ApeMaterno like '%" + TBapematerno.Text + "%' and U.Login like '%" + TBlogin.Text + "%' order by A.ApePaterno, A.ApeMaterno, A.Nombre"
            GValumnos.DataBind() 'Es necesario para que se vea el resultado
            msgInfo.show("Se encontraron " + GValumnos.Rows.Count.ToString + " " & Config.Etiqueta.ALUMNOS & " con los datos capturados.")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_GRADO & " " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO, 0))
    End Sub
    'Cambiar este método porque todos los campos cambiaron de nombre 
    Protected Sub BtnCambiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCambiar.Click
        msgError.hide()
        msgError2.hide()
        msgSuccess.hide()
        msgInfo.hide()

        Try
            Dim IdGrupo As Integer
            IdGrupo = CInt(GValumnos.SelectedRow.Cells(6).Text)
            Dim IdGrado As Integer
            IdGrado = CInt(GValumnos.SelectedRow.Cells(6).Text)

            SDSalumnos.UpdateCommand = "SET dateformat dmy; UPDATE Alumno set IdGrupo = " + DDLgrupo.SelectedValue + ", IdPlantel = " + DDLplantel.SelectedValue + ", FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdAlumno = " + GValumnos.SelectedRow.Cells(1).Text + " and IdGrupo = " + IdGrupo.ToString
            SDSalumnos.Update()

            If CBactividades.Checked Then
                'Actualizo todas las evaluaciones que tenga terminadas en el mismo grado, el conjunto de reactivos asignados a un alumno se asignan por el Grado
                SDSevaluacionesterm.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionTerminada set IdGrupo = " + DDLgrupo.SelectedValue + ", IdGrado = " + DDLgrado.SelectedValue + " where IdAlumno = " + GValumnos.SelectedRow.Cells(1).Text + " and IdGrupo = " + IdGrupo.ToString
                SDSevaluacionesterm.Update()
                SDSevaluacionesterm.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionRepetida set IdGrupo = " + DDLgrupo.SelectedValue + ", IdGrado = " + DDLgrado.SelectedValue + " where IdAlumno = " + GValumnos.SelectedRow.Cells(1).Text + " and IdGrupo = " + IdGrupo.ToString
                SDSevaluacionesterm.Update()

                SDSrespuestas.UpdateCommand = "SET dateformat dmy; UPDATE Respuesta set IdGrupo = " + DDLgrupo.SelectedValue + ", IdGrado = " + DDLgrado.SelectedValue + " where IdAlumno = " + GValumnos.SelectedRow.Cells(1).Text + " and IdGrupo = " + IdGrupo.ToString
                SDSrespuestas.Update()

                ''Add this line because it didn´t consider in the change of group
                SDSComentarioEvaluacion.UpdateCommand = "SET dateformat dmy; UPDATE ComentarioEvaluacion set IdGrupo=" + DDLgrupo.SelectedValue + " where IdAlumno = " + GValumnos.SelectedRow.Cells(1).Text + " and IdGrupo = " + IdGrupo.ToString
                SDSComentarioEvaluacion.Update()


                GValumnos.DataBind()
                msgSuccess.show("Éxito", "El " &
                    Config.Etiqueta.GRUPO & " del " &
                    Config.Etiqueta.ALUMNO &
                    " ha sido actualizado. Las evaluaciones terminadas que tuviera este " &
                    Config.Etiqueta.ALUMNO &
                    " también fueron reasignadas al nuevo " &
                    Config.Etiqueta.GRUPO & ".")
            Else
                msgSuccess.show("Éxito", Config.Etiqueta.ARTDET_GRUPO & " " &
                    Config.Etiqueta.GRUPO &
                    " de " &
                    Config.Etiqueta.ARTDET_ALUMNO & " " &
                    Config.Etiqueta.ALUMNO &
                    " ha sido actualizado. Las evaluaciones terminadas quedaron en " &
                    Config.Etiqueta.ARTDET_GRUPO & " " &
                    Config.Etiqueta.GRUPO &
                    " anterior como histórico.")
            End If

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GValumnos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GValumnos.SelectedIndexChanged
        msgInfo.show("Seleccionado", HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(3).Text) + " " + HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(4).Text) + " " + HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(5).Text))
        LblAlumno.Text = HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(3).Text) + " " + HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(4).Text) + " " + HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(5).Text)
        TBpasswordOK.Text = HttpUtility.HtmlDecode(Trim(GValumnos.SelectedRow.Cells(15).Text))
    End Sub


    Protected Sub BtnGenerar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGenerar.Click
        Try
            'Ahora registro el usuario en la base de datos de seguridad
            Dim status As MembershipCreateStatus

            ' 17/10/2013 se deseleccionó requiresQuestionAndAnswer="false"  de web config, no es necesario
            'Dim passwordQuestion As String = ""
            'Dim passwordAnswer As String = ""
            'If Membership.RequiresQuestionAndAnswer Then
            '    passwordQuestion = "Empresa desarrolladora de " &
            '        Config.Etiqueta.SISTEMA_CORTO
            '    passwordAnswer = "Integrant"
            'End If

            Dim newUser As MembershipUser = Membership.CreateUser(Trim(HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(14).Text)), Trim(TBpasswordOK.Text), _
                                                           Trim(TBemailOK.Text), Nothing, _
                                                           Nothing, True, status)

            'La pongo aquí para que no se limpie el error que capturo con la función
            If newUser Is Nothing Then
                msgError2.show("Error", GetErrorMessage(status))
            Else
                Roles.AddUserToRole(TBlogin.Text, "Alumno")
            End If
            SDSusuarios.UpdateCommand = "SET dateformat dmy; UPDATE Usuario set Password = '" + Trim(TBpasswordOK.Text) + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where Login = '" + Trim(HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(14).Text)) + "'"
            SDSusuarios.Update()
            GValumnos.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError2.show("Error", ex.Message.ToString())
        End Try
    End Sub

    Public Function GetErrorMessage(ByVal status As MembershipCreateStatus) As String
        Select Case status
            Case MembershipCreateStatus.DuplicateUserName
                Return "El usuario ya existe, elija Actualizar el password."
            Case MembershipCreateStatus.DuplicateEmail
                Return "Ya existe un usuario para ese email, por favor escriba un email diferente,"
            Case MembershipCreateStatus.InvalidPassword
                Return "El password es inválido, pruebe con otro."
            Case MembershipCreateStatus.InvalidEmail
                Return "El formato del email es inválido, por favor revíselo."
            Case MembershipCreateStatus.InvalidAnswer
                Return "La respuesta secreta es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidQuestion
                Return "La pregunta de recuperación de password es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidUserName
                Return "El nombre de usuario es inválido, corríjalo."
            Case MembershipCreateStatus.ProviderError
                Return "La autentificación falló, contacte al administrador del sistema."
            Case MembershipCreateStatus.UserRejected
                Return "La creación del usuario ha sido cancelada, consulte al administrador."
            Case Else
                Return "Ha ocurrido un error desconocido, verifique o contacte al administrador."
        End Select
    End Function

    Protected Sub BtnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnActualizar.Click
        msgSuccess.hide()
        msgError.hide()
        msgError2.hide()

        Try
            If Trim(TBpasswordOK.Text).Length < 4 Then
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "La contraseña debe de ser de 4 caracteres por lo menos.")
            Else
                'Actualizo password en tabla de seguridad, no puedo cambiar Login
                Dim u As MembershipUser
                u = Membership.GetUser(Trim(HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(14).Text))) 'Obtiene datos del usuario con el LOGIN
                'u.ChangePassword(Trim(HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(15).Text)), Trim(TBpasswordOK.Text)) 'cambia PASSWORD
                u.ChangePassword(u.ResetPassword(), Trim(TBpasswordOK.Text)) 'cambia PASSWORD

                'u.Email = TBemail.Text
                'Membership.UpdateUser(u)
                SDSusuarios.UpdateCommand = "SET dateformat dmy; UPDATE Usuario set Password = '" &
                    Trim(TBpasswordOK.Text) &
                    "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where Login = '" + Trim(HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(14).Text)) + "'"
                SDSusuarios.Update()
                GValumnos.DataBind()
                msgSuccess.show("Éxito", "La contraseña de " & Config.Etiqueta.ARTDET_ALUMNO & " " &
                    Config.Etiqueta.ALUMNO &
                    " ha sido cambiada.")
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError2.show("Error", ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Convierte(GValumnos, "AlumnosEncontrados")
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            'Algoritmo que oculta toda la columna donde aparece el "Seleccionar"
            Dim Cont As Integer
            GVactual.HeaderRow.Cells.Item(0).Visible = False
            For Cont = 0 To GVactual.Rows.Count - 1
                GVactual.Rows(Cont).Cells.Item(0).Visible = False
            Next
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub GValumnos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GValumnos.DataBound
        GValumnos.Caption = "<b>Listado de " &
            Config.Etiqueta.ALUMNOS &
            " encontrados de " &
            Config.Etiqueta.ARTDET_INSTITUCION & " " & Config.Etiqueta.INSTITUCION &
            " " + DDLinstitucion.SelectedItem.ToString + "</b>"
    End Sub

    Protected Sub BtnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLimpiar.Click
        TBmatricula.Text = ""
        TBnombre.Text = ""
        TBapepaterno.Text = ""
        TBapepaterno.Text = ""
        TBlogin.Text = ""

        msgError.hide()
        msgSuccess.hide()
        msgError2.hide()
        msgInfo.hide()
    End Sub

    Protected Sub SDSalumnos_Updating(sender As Object, e As SqlDataSourceCommandEventArgs) Handles SDSalumnos.Updating
        e.Command.CommandTimeout = 0
    End Sub

    Protected Sub SDSevaluacionesterm_Updating(sender As Object, e As SqlDataSourceCommandEventArgs) Handles SDSevaluacionesterm.Updating
        e.Command.CommandTimeout = 0
    End Sub

    Protected Sub SDSrespuestas_Updating(sender As Object, e As SqlDataSourceCommandEventArgs) Handles SDSrespuestas.Updating
        e.Command.CommandTimeout = 0
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    ' Métodos de GridViews
    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GValumnos_PreRender(sender As Object, e As EventArgs) Handles GValumnos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GValumnos.Rows.Count > 0 Then
            GValumnos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If

        ' Etiquetas de GridView
        GValumnos.Columns(1).HeaderText = "Id " & Config.Etiqueta.ALUMNO

        GValumnos.Columns(6).HeaderText = "Id " & Config.Etiqueta.GRUPO

        GValumnos.Columns(7).HeaderText = Config.Etiqueta.GRUPO

        GValumnos.Columns(8).HeaderText = "Id " & Config.Etiqueta.GRADO

        GValumnos.Columns(9).HeaderText = Config.Etiqueta.GRADO

        GValumnos.Columns(10).HeaderText = "Id " & Config.Etiqueta.NIVEL

        GValumnos.Columns(11).HeaderText = Config.Etiqueta.NIVEL

        GValumnos.Columns(12).HeaderText = "Id " & Config.Etiqueta.PLANTEL

        GValumnos.Columns(13).HeaderText = Config.Etiqueta.PLANTEL

        GValumnos.Columns(17).HeaderText = Config.Etiqueta.EQUIPO

        GValumnos.Columns(18).HeaderText = Config.Etiqueta.SUBEQUIPO

        GValumnos.Columns(19).HeaderText = Config.Etiqueta.CICLO
    End Sub

    Protected Sub GValumnos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GValumnos.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GValumnos, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GValumnos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GValumnos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

    Protected Sub BtnRestaurar_Click(sender As Object, e As EventArgs) Handles BtnRestaurar.Click
        Try
            Dim Email As String
            Dim Login = Trim(HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(14).Text))
            If GValumnos.SelectedRow.Cells(20).Text = "&nbsp;" Or Trim(GValumnos.SelectedRow.Cells(20).Text) = "" Then
                Email = "soporte@integrant.com.mx"
            Else
                Email = Trim(HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(20).Text))
            End If

            If Trim(TBpasswordOK.Text).Length < 4 Then
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "La contraseña debe de ser de 4 caracteres por lo menos.")
            Else
                Dim status As MembershipCreateStatus
                Roles.RemoveUserFromRole(Login, "Alumno")
                Membership.DeleteUser(Login)
                Try
                    Dim newUser As MembershipUser = Membership.CreateUser(Login, Trim(TBpasswordOK.Text), Email, Nothing, Nothing, True, status)
                    If newUser Is Nothing Then
                        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), GetErrorMessage(status))
                    Else
                        Roles.AddUserToRole(Trim(TBlogin.Text), "Alumno")
                    End If
                    SDSusuarios.UpdateCommand = "SET dateformat dmy; UPDATE Usuario set Password = '" + Trim(TBpasswordOK.Text) + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where Login = '" + Login + "'"
                    SDSusuarios.Update()
                    GValumnos.DataBind()
                    msgSuccess.show("Éxito", "La contraseña de " & Config.Etiqueta.ARTDET_ALUMNO & " " & Config.Etiqueta.ALUMNO & " ha sido restaurada")
                Catch ex As MembershipCreateUserException
                    Utils.LogManager.ExceptionLog_InsertEntry(ex)
                    msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
                End Try
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError2.show("Error", ex.Message.ToString())
        End Try
    End Sub
End Class
