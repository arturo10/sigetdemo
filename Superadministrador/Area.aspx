﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="Area.aspx.vb" 
    Inherits="superadministrador_Area" 
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style10
        {
            width: 100%;
        }
        .style15
        {
            font-weight: normal;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
        .style16
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
        .style18
        {
            height: 125px;
        }
        .style23
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000000;
            height: 8px;
        }
        .style24
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 155px;
            height: 8px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Administración de Areas o Ciencias de las 
										<asp:Label ID="Label1" runat="server" Text="[ASIGNATURAS]" />
		</h1>
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style10">
        <tr>
            <td>
                <span class="style15">Indique los Estatus: Activo, Suspendido o Baja</span></td>
        </tr>
        <tr>
            <td>
                <asp:DetailsView ID="DVareas" runat="server" AllowPaging="True" 
                    AutoGenerateRows="False" BackColor="White" BorderColor="#999999" 
                    BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataKeyNames="IdArea" 
                    DataSourceID="LDSareas" GridLines="Vertical" Height="50px" 
                    style="font-family: Arial, Helvetica, sans-serif; font-size: small" 
                    Width="622px" ForeColor="Black">
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <Fields>
                        <asp:BoundField DataField="IdArea" HeaderText="Id del Sistema" InsertVisible="False" 
                            ReadOnly="True" SortExpression="IdArea" >
                        <HeaderStyle Width="190px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripción" 
                            SortExpression="Descripcion" />
                        <asp:BoundField DataField="Clave" HeaderText="Clave" SortExpression="Clave" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" 
                            SortExpression="Estatus" />
                        <asp:BoundField DataField="FechaModif" HeaderText="FechaModif" 
                            SortExpression="FechaModif" Visible="False" />
                        <asp:BoundField DataField="Modifico" HeaderText="Modifico" 
                            SortExpression="Modifico" Visible="False" />
                        <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                            ShowInsertButton="True" />
                    </Fields>
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:DetailsView>
            </td>
        </tr>
        <tr>
            <td class="style18">
              <asp:LinqDataSource ID="LDSareas" runat="server"
                ContextTypeName="AreasDataContext" EnableDelete="True" EnableInsert="True"
                EnableUpdate="True" TableName="Areas" EntityTypeName="">
                </asp:LinqDataSource>
                <asp:SqlDataSource ID="SDSareas" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [Area]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
				<asp:HyperLink ID="HyperLink2" runat="server"
						NavigateUrl="~/"
						CssClass="defaultBtn btnThemeGrey btnThemeWide">
						Regresar&nbsp;al&nbsp;Menú
				</asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

