﻿Imports Siget

Imports System.Data.SqlClient

Partial Class superadministrador_EvaluacionAlumno
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        TitleLiteral.Text = "Actividades para " & Config.Etiqueta.ALUMNOS

        GVevalalumno.Caption = "<h3>Relación de " &
            Config.Etiqueta.ALUMNOS &
            " y Actividades asignadas en " &
            Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO &
            " y " &
            Config.Etiqueta.CICLO &
            " seleccionad" & Config.Etiqueta.LETRA_CICLO & "s</h3>"

        Label1.Text = Config.Etiqueta.ALUMNO
        Label2.Text = Config.Etiqueta.CICLO
        Label3.Text = Config.Etiqueta.NIVEL
        Label4.Text = Config.Etiqueta.GRADO
        Label5.Text = Config.Etiqueta.ASIGNATURA
        Label6.Text = Config.Etiqueta.ALUMNO
        Label7.Text = Config.Etiqueta.INSTITUCION
        Label8.Text = Config.Etiqueta.PLANTEL
        Label9.Text = Config.Etiqueta.GRUPO
        Label10.Text = Config.Etiqueta.ALUMNOS
        Literal1.Text = Config.Etiqueta.GRUPO
        Literal2.Text = Config.Etiqueta.LETRA_ALUMNO
        Literal3.Text = Config.Etiqueta.ARTDET_ALUMNOS
        Literal4.Text = Config.Etiqueta.ALUMNOS
        Literal5.Text = Config.Etiqueta.ARTIND_GRUPO
        Literal6.Text = Config.Etiqueta.GRUPO
        Literal7.Text = Config.Etiqueta.LETRA_GRUPO
        Literal8.Text = Config.Etiqueta.ARTDET_GRUPO
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_CICLO & " " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_GRADO & " " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLcalificacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacion.DataBound
        DDLcalificacion.Items.Insert(0, New ListItem("---Elija una Calificación", 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRUPO & " " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub Agregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Agregar.Click
        Try
            Dim I As Integer
            For I = 0 To CBLalumnos.Items.Count - 1
                If CBLalumnos.Items(I).Selected Then
                    SDSevalAlumno.InsertCommand = "SET dateformat dmy; INSERT INTO EvaluacionAlumno (IdEvaluacion,IdAlumno,InicioContestar,FinSinPenalizacion,FinContestar,Estatus, FechaModif, Modifico) VALUES (" + GVevaluaciones.SelectedRow.Cells(1).Text + "," + CBLalumnos.Items(I).Value + ",'" + CalInicioContestar.SelectedDate.ToShortDateString + "','" + CalLimiteSinPenalizacion.SelectedDate.ToShortDateString + " 23:59:00','" + CalFinContestar.SelectedDate.ToShortDateString + " 23:59:00','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
                    SDSevalAlumno.Insert()
                End If
            Next
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
        CBLalumnos.DataBind()
        GVevalalumno.DataBind()
    End Sub

    Protected Sub GVevalalumno_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevalalumno.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.ALUMNO

            LnkHeaderText = e.Row.Cells(2).Controls(0)
            LnkHeaderText.Text = "Nombre de " & Config.Etiqueta.ARTDET_ALUMNO & " " & Config.Etiqueta.ALUMNO

            LnkHeaderText = e.Row.Cells(9).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA
        End If
    End Sub

    Protected Sub GVevalalumno_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevalalumno.SelectedIndexChanged
        Try
            CalInicioContestar.SelectedDate = CDate(GVevalalumno.SelectedRow.Cells(6).Text)
            CalFinContestar.SelectedDate = CDate(GVevalalumno.SelectedRow.Cells(8).Text)
            CalLimiteSinPenalizacion.SelectedDate = CDate(GVevalalumno.SelectedRow.Cells(7).Text)
            'DDLalumno.SelectedValue = CInt(GVevalalumno.SelectedRow.Cells(1).Text) --> MARCARÍA ERROR PORQUE YA NO ESTÁ EN EL DLL PORQUE SE VAN QUITANDO AL AGREGARSE
            DDLestatus.SelectedValue = GVevalalumno.SelectedRow.Cells(10).Text
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVevalalumno_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevalalumno.DataBound
        If GVevalalumno.Rows.Count > 0 Then
            Quitar.Visible = True
            BtnActualizar.Visible = True
            pnlActualizarGrupo.Visible = True
        Else
            Quitar.Visible = False
            BtnActualizar.Visible = False
            pnlActualizarGrupo.Visible = False
        End If
    End Sub

    Protected Sub Quitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Quitar.Click
        Try
            SDSevalAlumno.DeleteCommand = "DELETE FROM EvaluacionAlumno where IdEvaluacion = " + GVevalalumno.SelectedRow.Cells(4).Text + " and IdAlumno = " + GVevalalumno.SelectedRow.Cells(1).Text
            SDSevalAlumno.Delete()
            msgError.hide()
            SDSevalAlumno.DataBind()
            CBLalumnos.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnActualizar.Click
        Try
            SDSevalAlumno.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionAlumno SET InicioContestar='" + CalInicioContestar.SelectedDate.ToShortDateString + "', FinSinPenalizacion='" + CalLimiteSinPenalizacion.SelectedDate.ToShortDateString + " 23:59:00', FinContestar='" + CalFinContestar.SelectedDate.ToShortDateString + " 23:59:00', Estatus='" + DDLestatus.SelectedValue + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdEvaluacion = " + GVevalalumno.SelectedRow.Cells(4).Text + " and IdAlumno = " + GVevalalumno.SelectedRow.Cells(1).Text
            SDSevalAlumno.Update()
            GVevalalumno.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub LinkButton1_Click(sender As Object, e As EventArgs) Handles LinkButton1.Click
        Dim I
        For I = 0 To CBLalumnos.Items.Count - 1
            CBLalumnos.Items(I).Selected = True
        Next
    End Sub

    Protected Sub LinkButton2_Click(sender As Object, e As EventArgs) Handles LinkButton2.Click
        Dim I
        For I = 0 To CBLalumnos.Items.Count - 1
            CBLalumnos.Items(I).Selected = False
        Next
    End Sub

    Protected Sub CBLalumnos_DataBound(sender As Object, e As EventArgs) Handles CBLalumnos.DataBound
        If CBLalumnos.Items.Count > 0 Then
            LinkButton1.Visible = True
            LinkButton2.Visible = True
        Else
            LinkButton1.Visible = False
            LinkButton2.Visible = False
        End If
    End Sub

    ' requiere: 
    '   que se haya seleccionado una actividad en GVevaluaciones y un grupo en DDLgrupo
    '   que se haya seleccionado fechas en los calendarios (CalInicioContestar, CalFinContestar, CalLimiteSinPenalizacion)
    Protected Sub btnActualizarGrupo_Click(sender As Object, e As EventArgs) Handles btnActualizarGrupo.Click
        msgError1.hide()
        msgSuccess1.hide()
        Dim cmd As SqlCommand

        Try
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                cmd = New SqlCommand("SET dateformat dmy; UPDATE EvaluacionAlumno SET InicioContestar='" & CalInicioContestar.SelectedDate.ToShortDateString & "', FinSinPenalizacion='" & CalLimiteSinPenalizacion.SelectedDate.ToShortDateString & " 23:59:00', FinContestar='" & CalFinContestar.SelectedDate.ToShortDateString & " 23:59:00', FechaModif = getdate(), Modifico = '@Modifico' where IdEvaluacion = @IdEvaluacion and IdAlumno IN (SELECT EA.IdAlumno FROM Alumno A, EvaluacionAlumno EA WHERE A.IdAlumno = EA.IdAlumno AND EA.IdEvaluacion = @IDEvaluacion AND A.IdGrupo = @IdGrupo)", conn)

                cmd.Parameters.AddWithValue("@IdEvaluacion", GVevaluaciones.SelectedRow.Cells(1).Text)
                cmd.Parameters.AddWithValue("@IdGrupo", DDLgrupo.SelectedValue)
                cmd.Parameters.AddWithValue("@Modifico", User.Identity.Name)

                cmd.ExecuteNonQuery()
                GVevalalumno.DataBind()
                cmd.Dispose()
                conn.Close()
                msgSuccess1.show("Éxito", "Se actualizaron las fechas para la actividad '" & GVevaluaciones.SelectedRow.Cells(3).Text & "' de " & Config.Etiqueta.ARTDET_ALUMNOS & " " & Config.Etiqueta.ALUMNOS & " de " & Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO & " " & DDLgrupo.SelectedItem.Text & ".")
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError1.show("Error", "Por favor asegúrese de haber seleccionado " & Config.Etiqueta.ARTIND_GRUPO &
                           " " & Config.Etiqueta.GRUPO & ", una evaluación, y fechas de inicio, fin, y penalización. <br />" &
                            ex.Message.ToString())
        End Try
    End Sub

    ' requiere: 
    '   que se haya seleccionado una actividad en GVevaluaciones y un grupo en DDLgrupo
    '   que se haya seleccionado un estado en DDLestatus
    Protected Sub btnActualizarGrupoEstado_Click(sender As Object, e As EventArgs) Handles btnActualizarGrupoEstado.Click
        msgError1.hide()
        msgSuccess1.hide()
        Dim cmd As SqlCommand

        Try
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                cmd = New SqlCommand("SET dateformat dmy; UPDATE EvaluacionAlumno SET Estatus = '" + DDLestatus.SelectedValue + "', FechaModif = getdate(), Modifico = '@Modifico' where IdEvaluacion = @IdEvaluacion and IdAlumno IN (SELECT EA.IdAlumno FROM Alumno A, EvaluacionAlumno EA WHERE A.IdAlumno = EA.IdAlumno AND EA.IdEvaluacion = @IDEvaluacion AND A.IdGrupo = @IdGrupo)", conn)

                cmd.Parameters.AddWithValue("@IdEvaluacion", GVevaluaciones.SelectedRow.Cells(1).Text)
                cmd.Parameters.AddWithValue("@IdGrupo", DDLgrupo.SelectedValue)
                cmd.Parameters.AddWithValue("@Modifico", User.Identity.Name)

                cmd.ExecuteNonQuery()
                GVevalalumno.DataBind()
                cmd.Dispose()
                conn.Close()
                msgSuccess1.show("Éxito", "Se actualizaron los estados para la actividad '" & GVevaluaciones.SelectedRow.Cells(3).Text & "' de " & Config.Etiqueta.ARTDET_ALUMNOS & " " & Config.Etiqueta.ALUMNOS & " de " & Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO & " " & DDLgrupo.SelectedItem.Text & ".")
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError1.show("Error", "Por favor asegúrese de haber seleccionado " & Config.Etiqueta.ARTIND_GRUPO &
                           " " & Config.Etiqueta.GRUPO & ", una evaluación, un estatus de actividad. <br />" &
                            ex.Message.ToString())
        End Try
    End Sub
End Class
