﻿Imports Siget

Imports System.Data.SqlClient

Partial Class superadministrador_ConfiguracionGlobal
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' modifica la visiblidad de la página para usuarios administradores de sistema
        If Roles.IsUserInRole(User.Identity.Name, "sysadmin") Then
            presentaElementosOcultos()
        End If

        If Not IsPostBack() Then
      PageTitle_v1_GENERAL.show("General",
                        Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/server_configuration.png")
      PageTitle_v1_Alumno.show("Alumno",
                        Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/user_student.png")
      PageTitle_v1_Profesor.show("Profesor",
                        Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/user_suit.png")
      PageTitle_v1_Coordinador.show("Coordinador",
                        Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/user_policeman(2).png")
      PageTitle_v1_Administrador.show("Administrador",
                        Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/user_striper.png")

      prepareControls()
    End If
  End Sub

  ' se encarga de mostrar todos los elementos que sólo
  ' deben ser visibles a usuarios privilegiados
  Protected Sub presentaElementosOcultos()
    pnlRecargar.Visible = True
  End Sub

  Protected Sub prepareControls()
    ' una configuración no se guarda en la BD hasta que se cambia las predeterminadas;
    ' por esto cargo primero las predeterminadas, y si existen cargo las de la BD.
    ' El cargar de las BD es redundante, pero asegura que siempre se muestren las
    ' configuraciones almacenadas que permanecerán en un reinicio de aplicación.
    reflectAppliedConfig()

    ' ahora cargo las configuraciones de la base de datos
    refreshConfiguration()
  End Sub

#End Region

#Region "cargado de configuraciones"

  ''' <summary>
  ''' Obtiene las configuraciones aplicadas actualmente en el filesystem desde Global
  ''' y configura los controles de ésta página para reflejar sus estados.
  ''' </summary>
  ''' <remarks></remarks>
  Protected Sub reflectAppliedConfig()

    ' General

    ddlIdiomaGeneral.SelectedValue = Config.Global.Idioma_General

    If Config.Global.DEPLOYED Then
      ddlDEPLOYED.SelectedValue = "true"
    Else
      ddlDEPLOYED.SelectedValue = "false"
    End If

    If Config.Global.MAIL_ACTIVO Then
      ddlMAIL_ACTIVO.SelectedValue = "true"
    Else
      ddlMAIL_ACTIVO.SelectedValue = "false"
    End If

    If Config.Global.GUARDAR_RESPUESTA_ABIERTA Then
      ddlGUARDAR_RESPUESTA_ABIERTA.SelectedValue = "true"
    Else
      ddlGUARDAR_RESPUESTA_ABIERTA.SelectedValue = "false"
    End If

    If Config.Global.FIRMA_PIE_INTEGRANT Then
      ddlFIRMA_PIE_INTEGRANT.SelectedValue = "true"
    Else
      ddlFIRMA_PIE_INTEGRANT.SelectedValue = "false"
    End If

    ' Alumno

    ddlIdiomaAlumno.SelectedValue = Config.Global.Idioma_Alumno

    If Config.Global.ALUMNO_CONTESTAR_RENUNCIAR Then
      ddlALUMNO_CONTESTAR_RENUNCIAR.SelectedValue = "true"
    Else
      ddlALUMNO_CONTESTAR_RENUNCIAR.SelectedValue = "false"
    End If

        If Config.Global.ALUMNO_PENDIENTES_COLUMNA_TIPO Then
            ddlALUMNO_PENDIENTES_COLUMNA_TIPO.SelectedValue = "true"
        Else
            ddlALUMNO_PENDIENTES_COLUMNA_TIPO.SelectedValue = "false"
        End If

        If Config.Global.ALUMNO_PENDIENTES_COLUMNA_ENTREGA Then
            ddlALUMNO_PENDIENTES_COLUMNA_ENTREGA.SelectedValue = "true"
        Else
            ddlALUMNO_PENDIENTES_COLUMNA_ENTREGA.SelectedValue = "false"
        End If


        If Config.Global.ALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION Then
            ddlALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION.SelectedValue = "true"
        Else
            ddlALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION.SelectedValue = "false"
        End If

        If Config.Global.DOCUMENTO_PROTEGIDO Then
            ddlALUMNO_DOCUMENTOPROTEGIDO.SelectedValue = "true"
        Else
            ddlALUMNO_DOCUMENTOPROTEGIDO.SelectedValue = "false"
        End If
        If Config.Global.V_INSCRIBIR_CURSOS Then
            ddlALUMNO_INSCRIBIR_CURSOS.SelectedValue = "true"
        Else
            ddlALUMNO_INSCRIBIR_CURSOS.SelectedValue = "false"
        End If
        If Config.Global.CONTRA_OBLIGATORIA Then
            ddl_ContraObligatoria.SelectedValue = "true"
        Else
            ddl_ContraObligatoria.SelectedValue = "false"
        End If


    ' profesor

    ddlIdiomaProfesor.SelectedValue = Config.Global.Idioma_Profesor

    ddlPROFESOR_CALIFICA_NUMERICO.SelectedValue = Config.Global.PROFESOR_CALIFICA_NUMERICO

    ddlPROFESOR_CALIFICA_DOCUMENTO.SelectedValue = Config.Global.PROFESOR_CALIFICA_DOCUMENTO

    ' coordinador

    ddlIdiomaCoordinador.SelectedValue = Config.Global.Idioma_Coordinador

    ' administrador

    ddlIdiomaAdministrador.SelectedValue = Config.Global.Idioma_Administrador

    ddlModelo.SelectedValue = Config.Global.Modelo_Sistema

    If Config.Global.Login_Screen = 0 Then
      RbLogin_Jp1.Checked = True
    ElseIf Config.Global.Login_Screen = 1 Then
      RbLogin_A1.Checked = True
    End If

  End Sub

  ''' <summary>
  ''' Obtiene las configuraciones guardadas en la base de datos
  ''' y configura los controles de ésta página para reflejar sus estados.
  ''' </summary>
  ''' <remarks>
  ''' Solo configura los controles de las configuraciones encontradas; 
  ''' si no se encuentra una configuración en la base de datos, 
  ''' el control permanece reflejando su estado anterior.
  ''' </remarks>
  Protected Sub refreshConfiguration()
    Try
      Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
        conn.Open()
        Dim cmd As SqlCommand = New SqlCommand("SET dateformat dmy; SELECT * FROM Configuracion", conn)

        Dim results As SqlDataReader = cmd.ExecuteReader()

                While (results.Read())
                    If results.Item("Llave") = Config.Global.Key_Idioma_General Then
                        ddlIdiomaGeneral.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.KEY_DEPLOYED Then
                        ddlDEPLOYED.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.KEY_MAIL_ACTIVO Then
                        ddlMAIL_ACTIVO.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.KEY_GUARDAR_RESPUESTA_ABIERTA Then
                        ddlGUARDAR_RESPUESTA_ABIERTA.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.KEY_ALUMNO_CONTESTAR_RENUNCIAR Then
                        ddlALUMNO_CONTESTAR_RENUNCIAR.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.KEY_ALUMNO_PENDIENTES_COLUMNA_TIPO Then
                        ddlALUMNO_PENDIENTES_COLUMNA_TIPO.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.KEY_ALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION Then
                        ddlALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.KEY_ALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION Then
                        ddlALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.KEY_ALUMNO_PENDIENTES_COLUMNA_ENTREGA Then
                        ddlALUMNO_PENDIENTES_COLUMNA_ENTREGA.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.KEY_PROFESOR_CALIFICA_NUMERICO Then
                        ddlPROFESOR_CALIFICA_NUMERICO.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.KEY_PROFESOR_CALIFICA_DOCUMENTO Then
                        ddlPROFESOR_CALIFICA_DOCUMENTO.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.KEY_FIRMA_PIE_INTEGRANT Then
                        ddlFIRMA_PIE_INTEGRANT.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.Key_Idioma_Alumno Then
                        ddlIdiomaAlumno.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.Key_Idioma_Profesor Then
                        ddlIdiomaProfesor.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.Key_Idioma_Coordinador Then
                        ddlIdiomaCoordinador.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.Key_Idioma_Administrador Then
                        ddlIdiomaAdministrador.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.Key_Modelo_Sistema Then
                        ddlModelo.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.KEY_DOCUMENTO_PROTEGIDO Then
                        ddlALUMNO_DOCUMENTOPROTEGIDO.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.KEY_INSCRIBIR_CURSOS Then
                        ddlALUMNO_INSCRIBIR_CURSOS.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.KEY_CONTRA_OBLIGATORIA Then
                        ddl_ContraObligatoria.SelectedValue = results.Item("Valor")
                    ElseIf results.Item("Llave") = Config.Global.Key_Login_Screen Then
                        Dim r As Integer = Integer.Parse(results.Item("Valor"))
                        If r = 0 Then
                            RbLogin_Jp1.Checked = True
                        ElseIf r = 1 Then
                            RbLogin_A1.Checked = True
                        End If
                    End If

                End While

                cmd.Dispose()
                conn.Close()
            End Using
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

#End Region

#Region "métodos de actualización"


    Protected Sub ddl_ContraObligatoria_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddl_ContraObligatoria.SelectedIndexChanged
        Try
            ' primero almaceno la configuración en la base de datos
            CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.KEY_CONTRA_OBLIGATORIA,
                                                                                                  ddl_ContraObligatoria.SelectedValue,
                                                                                                  User.Identity.Name)

            ' si eso no falla, aplico la configuración al filesystem
            Config.Global.CONTRA_OBLIGATORIA = Boolean.Parse(ddl_ContraObligatoria.SelectedValue)

            prepareControls()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub



    Protected Sub ddlALUMNO_DOCUMENTOPROTEGIDO_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlALUMNO_DOCUMENTOPROTEGIDO.SelectedIndexChanged
        Try
            ' primero almaceno la configuración en la base de datos
            CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.KEY_DOCUMENTO_PROTEGIDO,
                                                                                                  ddlALUMNO_DOCUMENTOPROTEGIDO.SelectedValue,
                                                                                                  User.Identity.Name)

            ' si eso no falla, aplico la configuración al filesystem
            Config.Global.DOCUMENTO_PROTEGIDO = Boolean.Parse(ddlALUMNO_DOCUMENTOPROTEGIDO.SelectedValue)

            prepareControls()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub
    Protected Sub ddlALUMNO_INSCRIBIR_CURSOS_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlALUMNO_INSCRIBIR_CURSOS.SelectedIndexChanged
        Try
            ' primero almaceno la configuración en la base de datos
            CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.KEY_INSCRIBIR_CURSOS,
                                                                                                  ddlALUMNO_INSCRIBIR_CURSOS.SelectedValue,
                                                                                                  User.Identity.Name)

            ' si eso no falla, aplico la configuración al filesystem
            Config.Global.V_INSCRIBIR_CURSOS = Boolean.Parse(ddlALUMNO_INSCRIBIR_CURSOS.SelectedValue)

            prepareControls()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub



    Protected Sub ddlDEPLOYED_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDEPLOYED.SelectedIndexChanged
        Try
            ' primero almaceno la configuración en la base de datos
            CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.KEY_DEPLOYED,
                                                                                                  ddlDEPLOYED.SelectedValue,
                                                                                                  User.Identity.Name)

            ' si eso no falla, aplico la configuración al filesystem
            Config.Global.DEPLOYED = Boolean.Parse(ddlDEPLOYED.SelectedValue)

            prepareControls()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub ddlMAIL_ACTIVO_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMAIL_ACTIVO.SelectedIndexChanged
        Try
            ' primero almaceno la configuración en la base de datos
            CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.KEY_MAIL_ACTIVO,
                                                                                                  ddlMAIL_ACTIVO.SelectedValue,
                                                                                                  User.Identity.Name)

            ' si eso no falla, aplico la configuración al filesystem
            Config.Global.MAIL_ACTIVO = Boolean.Parse(ddlMAIL_ACTIVO.SelectedValue)

            prepareControls()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub ddlGUARDAR_RESPUESTA_ABIERTA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGUARDAR_RESPUESTA_ABIERTA.SelectedIndexChanged
        Try
            ' primero almaceno la configuración en la base de datos
            CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.KEY_GUARDAR_RESPUESTA_ABIERTA,
                                                                                                  ddlGUARDAR_RESPUESTA_ABIERTA.SelectedValue,
                                                                                                  User.Identity.Name)

            ' si eso no falla, aplico la configuración al filesystem
            Config.Global.GUARDAR_RESPUESTA_ABIERTA = Boolean.Parse(ddlGUARDAR_RESPUESTA_ABIERTA.SelectedValue)

            prepareControls()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub ddlFIRMA_PIE_INTEGRANT_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFIRMA_PIE_INTEGRANT.SelectedIndexChanged
        Try
            ' primero almaceno la configuración en la base de datos
            CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.KEY_FIRMA_PIE_INTEGRANT,
                                                                                                  ddlFIRMA_PIE_INTEGRANT.SelectedValue,
                                                                                                  User.Identity.Name)

            ' si eso no falla, aplico la configuración al filesystem
            Config.Global.FIRMA_PIE_INTEGRANT = Boolean.Parse(ddlFIRMA_PIE_INTEGRANT.SelectedValue)

            prepareControls()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub ddlALUMNO_CONTESTAR_RENUNCIAR_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlALUMNO_CONTESTAR_RENUNCIAR.SelectedIndexChanged
        Try
            ' primero almaceno la configuración en la base de datos
            CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.KEY_ALUMNO_CONTESTAR_RENUNCIAR,
                                                                                                  ddlALUMNO_CONTESTAR_RENUNCIAR.SelectedValue,
                                                                                                  User.Identity.Name)

            ' si eso no falla, aplico la configuración al filesystem
            Config.Global.ALUMNO_CONTESTAR_RENUNCIAR = Boolean.Parse(ddlALUMNO_CONTESTAR_RENUNCIAR.SelectedValue)

            prepareControls()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub ddlALUMNO_PENDIENTES_COLUMNA_TIPO_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlALUMNO_PENDIENTES_COLUMNA_TIPO.SelectedIndexChanged
        Try
            ' primero almaceno la configuración en la base de datos
            CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.KEY_ALUMNO_PENDIENTES_COLUMNA_TIPO,
                                                                                                  ddlALUMNO_PENDIENTES_COLUMNA_TIPO.SelectedValue,
                                                                                                  User.Identity.Name)

            ' si eso no falla, aplico la configuración al filesystem
            Config.Global.ALUMNO_PENDIENTES_COLUMNA_TIPO = Boolean.Parse(ddlALUMNO_PENDIENTES_COLUMNA_TIPO.SelectedValue)

            prepareControls()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub ddlALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION.SelectedIndexChanged
        Try
            ' primero almaceno la configuración en la base de datos
            CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.KEY_ALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION,
                                                                                                  ddlALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION.SelectedValue,
                                                                                                  User.Identity.Name)

            ' si eso no falla, aplico la configuración al filesystem
            Config.Global.ALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION = Boolean.Parse(ddlALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION.SelectedValue)

            prepareControls()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub ddlALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION.SelectedIndexChanged
        Try
            ' primero almaceno la configuración en la base de datos
            CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.KEY_ALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION,
                                                                                                  ddlALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION.SelectedValue,
                                                                                                  User.Identity.Name)

            ' si eso no falla, aplico la configuración al filesystem
            Config.Global.ALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION = Boolean.Parse(ddlALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION.SelectedValue)

            prepareControls()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub ddlPROFESOR_CALIFICA_NUMERICO_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPROFESOR_CALIFICA_NUMERICO.SelectedIndexChanged
        Try
            ' primero almaceno la configuración en la base de datos
            CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.KEY_PROFESOR_CALIFICA_NUMERICO,
                                                                                                  ddlPROFESOR_CALIFICA_NUMERICO.SelectedValue,
                                                                                                  User.Identity.Name)

            ' si eso no falla, aplico la configuración al filesystem
            Config.Global.PROFESOR_CALIFICA_NUMERICO = Integer.Parse(ddlPROFESOR_CALIFICA_NUMERICO.SelectedValue)

            prepareControls()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

#End Region

  Protected Sub ddlPROFESOR_CALIFICA_DOCUMENTO_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPROFESOR_CALIFICA_DOCUMENTO.SelectedIndexChanged
    Try
      ' primero almaceno la configuración en la base de datos
      CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.KEY_PROFESOR_CALIFICA_DOCUMENTO,
                                                                                            ddlPROFESOR_CALIFICA_DOCUMENTO.SelectedValue,
                                                                                            User.Identity.Name)

      ' si eso no falla, aplico la configuración al filesystem
      Config.Global.PROFESOR_CALIFICA_DOCUMENTO = Integer.Parse(ddlPROFESOR_CALIFICA_DOCUMENTO.SelectedValue)

      prepareControls()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub lbRecargar_Click(sender As Object, e As EventArgs) Handles lbRecargar.Click
    ' *****************************************************************************************
    ' inicializa configuración
    ' *****************************************************************************************
    Siget.Config.Global_Import.getConfig()

    ' *****************************************************************************************
    ' inicializa configuración de lenguaje
    ' *****************************************************************************************
    Siget.Lang.Lang_Config.InicializaTodosLosLenguajes()

    ' la siguiente línea debe eliminarse al completar la propagación de la ui 5.1.0 a los otros perfiles
    Siget.Config.Etiqueta.importa_Etiquetas_OLD()

    ' *****************************************************************************************
    ' inicializa configuración de colores
    ' *****************************************************************************************
    Siget.Config.Color_Import.configuraColores()

    Response.Redirect("~/superadministrador/ConfiguracionGlobal.aspx")
  End Sub

  Protected Sub ddlIdiomaGeneral_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlIdiomaGeneral.SelectedIndexChanged
    Try
      ' primero almaceno la configuración en la base de datos
      CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.Key_Idioma_General,
                                                                                            ddlIdiomaGeneral.SelectedValue,
                                                                                            User.Identity.Name)

      ' si eso no falla, aplico la configuración al filesystem
      Config.Global.Idioma_General = ddlIdiomaGeneral.SelectedValue

      prepareControls()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

    Protected Sub ddlALUMNO_PENDIENTES_COLUMNA_ENTREGA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlALUMNO_PENDIENTES_COLUMNA_ENTREGA.SelectedIndexChanged
        Try
            ' primero almaceno la configuración en la base de datos
            CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.KEY_ALUMNO_PENDIENTES_COLUMNA_ENTREGA,
                                                                                                  ddlALUMNO_PENDIENTES_COLUMNA_ENTREGA.SelectedValue,
                                                                                                  User.Identity.Name)
            ' si eso no falla, aplico la configuración al filesystem
            Config.Global.ALUMNO_PENDIENTES_COLUMNA_ENTREGA = ddlALUMNO_PENDIENTES_COLUMNA_ENTREGA.SelectedValue
            prepareControls()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub


  Protected Sub ddlIdiomaAlumno_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlIdiomaAlumno.SelectedIndexChanged
    Try
      ' primero almaceno la configuración en la base de datos
      CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.Key_Idioma_Alumno,
                                                                                            ddlIdiomaAlumno.SelectedValue,
                                                                                            User.Identity.Name)

      ' si eso no falla, aplico la configuración al filesystem
      Config.Global.Idioma_Alumno = ddlIdiomaAlumno.SelectedValue

      prepareControls()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub ddlIdiomaProfesor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlIdiomaProfesor.SelectedIndexChanged
    Try
      ' primero almaceno la configuración en la base de datos
      CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.Key_Idioma_Profesor,
                                                                                            ddlIdiomaProfesor.SelectedValue,
                                                                                            User.Identity.Name)

      ' si eso no falla, aplico la configuración al filesystem
      Config.Global.Idioma_Profesor = ddlIdiomaProfesor.SelectedValue

      prepareControls()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub ddlIdiomaCoordinador_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlIdiomaCoordinador.SelectedIndexChanged
    Try
      ' primero almaceno la configuración en la base de datos
      CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.Key_Idioma_Coordinador,
                                                                                            ddlIdiomaCoordinador.SelectedValue,
                                                                                            User.Identity.Name)

      ' si eso no falla, aplico la configuración al filesystem
      Config.Global.Idioma_Coordinador = ddlIdiomaCoordinador.SelectedValue

      prepareControls()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub ddlIdiomaAdministrador_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlIdiomaAdministrador.SelectedIndexChanged
    Try
      ' primero almaceno la configuración en la base de datos
      CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.Key_Idioma_Administrador,
                                                                                            ddlIdiomaAdministrador.SelectedValue,
                                                                                            User.Identity.Name)

      ' si eso no falla, aplico la configuración al filesystem
      Config.Global.Idioma_Administrador = ddlIdiomaAdministrador.SelectedValue

      prepareControls()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub ddlModelo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlModelo.SelectedIndexChanged
    Try
      ' primero almaceno la configuración en la base de datos
      CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.Key_Modelo_Sistema,
                                                                                            ddlModelo.SelectedValue,
                                                                                            User.Identity.Name)

      ' si eso no falla, aplico la configuración al filesystem
      Config.Global.Modelo_Sistema = ddlModelo.SelectedValue

      prepareControls()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub RbLogin_Jp1_CheckedChanged(sender As Object, e As EventArgs) Handles RbLogin_Jp1.CheckedChanged
    updateLoginScreen()
  End Sub

  Protected Sub RbLogin_A1_CheckedChanged(sender As Object, e As EventArgs) Handles RbLogin_A1.CheckedChanged
    updateLoginScreen()
  End Sub

  Protected Sub updateLoginScreen()
    Dim mode As Integer = 0
    If RbLogin_A1.Checked Then
      mode = 1
    End If

    Try
      ' primero almaceno la configuración en la base de datos
      CType(New DataAccess.ConfiguracionDa, DataAccess.ConfiguracionDa).GuardaConfiguracion(Config.Global.Key_Login_Screen,
                                                                                            mode,
                                                                                            User.Identity.Name)

      ' si eso no falla, aplico la configuración al filesystem
      Config.Global.Modelo_Sistema = ddlModelo.SelectedValue
      If RbLogin_Jp1.Checked Then
        Config.Global.Login_Screen = 0
      ElseIf RbLogin_A1.Checked Then
        Config.Global.Login_Screen = 1
      End If

      prepareControls()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

End Class
