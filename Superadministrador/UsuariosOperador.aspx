﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="UsuariosOperador.aspx.vb" 
    Inherits="superadministrador_UsuariosOperador"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style13 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 48px;
        }

        .style11 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style23 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Datos de los Operadores
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style23">
                <asp:Label ID="Label2" runat="server" Text="[INSTITUCION]" />
            </td>
            <td colspan="2">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                    DataValueField="IdInstitucion" Height="22px" Width="300px">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style23">
                <asp:Label ID="Label3" runat="server" Text="[PLANTEL]" />
            </td>
            <td colspan="2">
                <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                    DataValueField="IdPlantel" Height="22px" Width="300px">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style11">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style11" colspan="3">
                <table class="style10">
                    <tr>
                        <td>
                            <asp:GridView ID="GVcoordinadores" runat="server" 
                                AllowPaging="True"
                                AllowSorting="True" 
                                AutoGenerateColumns="False" 
                                Caption="Operadores asigandos al PLANTEL elegido"
                                DataSourceID="SDSlistaoperadores"
                                
                                CellPadding="3" 
                                ForeColor="Black" 
                                GridLines="Vertical"
                                Style="font-family: Arial, Helvetica, sans-serif; font-size: small; text-align: left"
                                Width="866px" 
                                BackColor="White" 
                                BorderColor="#999999" 
                                BorderStyle="Solid"
                                BorderWidth="1px">
                                <Columns>
                                    <asp:BoundField DataField="Clave" HeaderText="Clave" SortExpression="Clave">
                                        <HeaderStyle Width="85px" />
                                        <ItemStyle Width="85px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Apellidos" HeaderText="Apellidos" ReadOnly="True"
                                        SortExpression="Apellidos" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre" ReadOnly="True"
                                        SortExpression="Nombre" />
                                    <asp:BoundField DataField="Login" HeaderText="Login" ReadOnly="True"
                                        SortExpression="Login" />
                                    <asp:BoundField DataField="Contraseña" HeaderText="Contraseña" ReadOnly="True"
                                        SortExpression="Contraseña">
                                        <HeaderStyle Width="85px" />
                                        <ItemStyle Width="85px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style11">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style11">&nbsp;</td>
            <td colspan="2">
                <asp:Label ID="LblError" runat="server"
                    CssClass="LabelErrorDefault"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style11">&nbsp;</td>
            <td>
                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Institucion]"></asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSlistaoperadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select C.Clave,rtrim(C.Apellidos) as Apellidos, rtrim(C.Nombre) as Nombre, rtrim(U.Login) as Login, rtrim(U.Password) as Contraseña, rtrim(C.Email) as Email, C.Estatus
from Operador C, OperadorPlantel CP, Usuario U
where CP.IdPlantel = @IdPlantel and C.IdOperador = CP.IdOperador
and U.IdUsuario = C.IdUsuario
order by C.Apellidos, C.Nombre">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style11">&nbsp;</td>
            <td>
                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

