<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="GeneraUsuarios.aspx.vb" 
    Inherits="superadministrador_GeneraUsuarios" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style10
        {
            width: 100%;
            height: 430px;
        }
       
              
        .style11
        {
            font-family: Arial, Helvetica, sans-serif;
        }
        .style12
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
        .style13
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
        .style14
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
        }
       
              
        .style23
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
       
              
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			
		</h1>Generaci�n autom�tica de Usuarios para 
								<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
								 (YA NO SE USA?)
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style10">
        <tr>
            <td class="style11">
                <span class="style23">Cargue el archivo que contiene los registros para importar a la base de datos:</span><span class="style12"><br />
                </span>
                <br class="style12" />
                <span class="style14">(Deber� tener los campos separados por comas: <b>
                Idplantel, Idgrupo, Nombre, Ap. Paterno, Ap. Materno, Matr�cula, Email</b>)<br />
                <br />
                Se generan los registros en las tablas de Usuario y Alumno, adem�s de las 
                cuentas de acceso en la BD de Seguridad</span></td>
        </tr>
        <tr>
            <td class="style11">
                <asp:FileUpload ID="ArchivoCarga" runat="server" Width="505px" />
            </td>
        </tr>
        <tr>
            <td>
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSusuarios" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [Usuario]"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSalumnos" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [Alumno]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="LblMensaje" runat="server" 
                    CssClass="LabelInfoDefault"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Genera" runat="server" Text="Importar" 
										CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HyperLink ID="HyperLink2" runat="server"
					NavigateUrl="~/" 
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Men�
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

