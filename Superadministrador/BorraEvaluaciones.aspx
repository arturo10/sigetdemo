﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="BorraEvaluaciones.aspx.vb"
    Inherits="superadministrador_BorraEvaluaciones" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }

        .style11 {
            width: 97%;
        }

        .style23 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style26 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 144px;
        }

        .style14 {
            width: 268435520px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style33 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 144px;
            height: 25px;
        }

        .style34 {
            height: 25px;
            text-align: left;
        }

        .style35 {
            width: 268435520px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            height: 25px;
        }

        .style36 {
            width: 155px;
            height: 25px;
        }

        .style25 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
        }

        .style24 {
            width: 268435488px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style13 {
            width: 100%;
            height: 1px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Configuración - Borrar actividades
    </h1>
    Elimina el resultado y todas las respuestas de un 
                <asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
    &nbsp;en una actividad específica.
    Una vez eliminada la actividad, ya no podrá recuperar sus datos.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td>
                <table class="style11">
                    <tr>
                        <td>
                            <table class="style22">
                                <tr>
                                    <td class="style23" colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="style26">&nbsp;</td>
                                    <td style="text-align: left">
                                        <asp:Label ID="Mensaje0" runat="server"
                                            Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #000066; text-align: left;"
                                            Text="Seleccione la Actividad"></asp:Label>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td>
                                    <td class="style16">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="style33">
                                        <span class="estandar" style="text-align: right">
                                            <asp:Label ID="Label2" runat="server" Text="[INSTITUCION]" />
                                        </span></td>
                                    <td class="style34">
                                        <asp:DropDownList ID="DDLinstitucion"
                                            runat="server" AutoPostBack="True" DataSourceID="SDSinstituciones"
                                            DataTextField="Descripcion" DataValueField="IdInstitucion"
                                            Width="350px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style35" style="text-align: right"></td>
                                    <td class="style36"></td>
                                </tr>
                                <tr>
                                    <td class="style26">
                                        <span class="estandar" style="text-align: right">
                                            <asp:Label ID="Label3" runat="server" Text="[PLANTEL]" />
                                        </span></td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                            DataValueField="IdPlantel" Width="350px" Height="22px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td>
                                    <td class="style16">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="style26">
                                        <asp:Label ID="Label4" runat="server" Text="[NIVEL]" />
                                    </td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                                            Width="220px" Height="22px" CssClass="style25">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td>
                                    <td class="style16">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="style26">
                                        <asp:Label ID="Label5" runat="server" Text="[GRADO]" />
                                    </td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSgrados" DataTextField="Descripcion"
                                            DataValueField="IdGrado" Width="220px" Height="22px" CssClass="style25">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td>
                                    <td class="style16">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="style26">
                                        <asp:Label ID="Label6" runat="server" Text="[CICLO]" />
                                    </td>
                                    <td class="style25">
                                        <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                                            DataValueField="IdCicloEscolar" Width="350px" CssClass="style25">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td>
                                    <td class="style24">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="style26">
                                        <asp:Label ID="Label7" runat="server" Text="[ASIGNATURA]" />
                                    </td>
                                    <td class="style25">
                                        <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSasignaturas" DataTextField="Descripcion"
                                            DataValueField="IdAsignatura" Width="350px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td>
                                    <td class="style24">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="style26">Calificación</td>
                                    <td class="style25">
                                        <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True"
                                            DataSourceID="SDScalificaciones" DataTextField="Descripcion"
                                            DataValueField="IdCalificacion" Width="350px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td>
                                    <td class="style24">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="style26">&nbsp;</td>
                                    <td class="style25">&nbsp;</td>
                                    <td class="style14" style="text-align: right">&nbsp;</td>
                                    <td class="style24">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="style26">Actividad para capturar</td>
                                    <td class="style25" colspan="3">
                                        <asp:GridView ID="GVevaluaciones" runat="server"
                                            AutoGenerateColumns="False" AutoGenerateSelectButton="True" CellPadding="3"
                                            DataKeyNames="IdEvaluacion" DataSourceID="SDSevaluaciones"
                                            GridLines="Vertical" Style="font-size: x-small" AllowSorting="True"
                                            ForeColor="Black" Width="830px" BackColor="White" BorderColor="#999999"
                                            BorderStyle="Solid" BorderWidth="1px">
                                            <Columns>
                                                <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion" />
                                                <asp:BoundField DataField="IdCalificacion"
                                                    HeaderText="IdCalificacion"
                                                    SortExpression="IdCalificacion" Visible="False" />
                                                <asp:BoundField DataField="ClaveBateria" HeaderText="Clave de la Actividad"
                                                    SortExpression="ClaveBateria" />
                                                <asp:BoundField DataField="ClaveAbreviada" HeaderText="Clave p/Reporte"
                                                    SortExpression="ClaveAbreviada" />
                                                <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                                                    SortExpression="Porcentaje">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                    HeaderText="Inicia" SortExpression="InicioContestar" />
                                                <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                    HeaderText="Termina" SortExpression="FinContestar" />
                                                <asp:BoundField DataField="FinSinPenalizacion"
                                                    DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Máxima"
                                                    SortExpression="FinSinPenalizacion" />
                                                <asp:BoundField DataField="Penalizacion" HeaderText="% Penalización"
                                                    SortExpression="Penalizacion">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                            </Columns>
                                            <FooterStyle BackColor="#CCCCCC" />
                                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                            <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White"
                                                HorizontalAlign="Center" />
                                            <AlternatingRowStyle BackColor="#CCCCCC" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style26">&nbsp;</td>
                                    <td class="style25" colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="style26">
                                        <span class="estandar" style="text-align: right">
                                            <asp:Label ID="Label8" runat="server" Text="[GRUPO]" />
                                        </span></td>
                                    <td class="style25" colspan="3">
                                        <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSgrupos" DataTextField="Descripcion"
                                            DataValueField="IdGrupo" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style26">&nbsp;</td>
                                    <td class="style25" colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="style26">&nbsp;</td>
                                    <td class="style25" colspan="3">
                                        <asp:GridView ID="GValumnosterminados" runat="server" 
                                            AllowPaging="True"
                                            AllowSorting="True" 
                                            AutoGenerateColumns="False" 
                                            DataKeyNames="IdAlumno" 
                                            DataSourceID="SDSalumnosterminados"
                                            Caption="ALUMNOS con actividad realizada. Seleccione el resultado que desee eliminar" 
                                            PageSize="20" 
                                            
                                            CellPadding="3" 
                                            GridLines="Vertical"
                                            Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small; margin-left: 0px; text-align: left;"
                                            Width="835px"
                                            ForeColor="Black"
                                            BackColor="White" 
                                            BorderColor="#999999" 
                                            BorderStyle="Solid"
                                            BorderWidth="1px">
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" />
                                                <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno" />
                                                <asp:BoundField DataField="ApePaterno" HeaderText="Apellido Paterno"
                                                    SortExpression="ApePaterno" />
                                                <asp:BoundField DataField="ApeMaterno" HeaderText="Apellido Materno"
                                                    SortExpression="ApeMaterno" />
                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre(s)"
                                                    SortExpression="Nombre" />
                                                <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                                                    SortExpression="Matricula" />
                                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                                <asp:BoundField DataField="Resultado" DataFormatString="{0:0.00}"
                                                    HeaderText="Calificación (%)" SortExpression="Resultado" />
                                                <asp:BoundField DataField="FechaTermino" DataFormatString="{0:dd/MM/yyyy}"
                                                    HeaderText="Fecha Terminada" SortExpression="FechaTermino" />
                                            </Columns>
                                            <FooterStyle BackColor="#CCCCCC" />
                                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                            <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                            <AlternatingRowStyle BackColor="#CCCCCC" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style26">&nbsp;</td>
                                    <td class="style25" colspan="3">
                                        <asp:Button ID="BtnBorrar" runat="server" Text="Borrar resultado"
                                            CssClass="defaultBtn btnThemeBlue btnThemeMedium"
                                            Visible="False" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td>
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                            <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Institucion] 
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSplanteles" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion)
ORDER BY [Descripcion]">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSniveles" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="select N.IdNivel, N.Descripcion
from Nivel N, Escuela E, Plantel P
where N.IdNivel = E.IdNivel and P.IdPlantel = E.IdPlantel
      and P.IdPlantel = @IdPlantel
Order by Descripcion">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSgrados" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                            <asp:SqlDataSource ID="SDSgrupos" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Grupo] 
WHERE ([IdGrado] = @IdGrado) and ([IdPlantel] = @IdPlantel) and ([IdCicloEscolar] = @IdCicloEscolar)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                                        PropertyName="SelectedValue" Type="Int32" />
                                    <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                                        PropertyName="SelectedValue" />
                                    <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                                        PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSusuarios" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Usuario]"></asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT C.IdCalificacion, C.IdCicloEscolar, C.Clave, C.IdAsignatura, A.Descripcion Materia, C.Consecutivo,C.Descripcion, C.Valor
FROM Calificacion C, Asignatura A
WHERE ([IdCicloEscolar] = @IdCicloEscolar)  and (A.IdAsignatura = C.IdAsignatura) and (A.IdAsignatura = @IdAsignatura)
ORDER BY A.Descripcion, C.Consecutivo">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                                        PropertyName="SelectedValue" />
                                    <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                                        PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="select IdEvaluacion, IdCalificacion, ClaveBateria, ClaveAbreviada, ClaveAbreviada, Porcentaje, InicioContestar, FinContestar, FinSinPenalizacion, Penalizacion, Tipo
from Evaluacion 
where IdCalificacion = @IdCalificacion and SeEntregaDocto = 'False'
and IdEvaluacion not in  (select IdEvaluacion from EvaluacionPlantel where IdPlantel = @IdPlantel)
UNION
select E.IdEvaluacion, E.IdCalificacion, E.ClaveBateria, E.ClaveAbreviada, E.ClaveAbreviada, E.Porcentaje, P.InicioContestar, P.FinContestar,
P.FinSinPenalizacion, E.Penalizacion, E.Tipo
from Evaluacion E, EvaluacionPlantel P
where E.IdCalificacion = @IdCalificacion and E.IdEvaluacion = P.IdEvaluacion and P.IdPlantel = @IdPlantel and E.SeEntregaDocto = 'False'
order by IdCalificacion, InicioContestar">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion"
                                        PropertyName="SelectedValue" />
                                    <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                                        PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                            <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [CicloEscolar] WHERE ([Estatus] = 'Activo') ORDER BY [FechaInicio]"></asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdAsignatura, IdGrado, Descripcion
from Asignatura
where IdGrado = @IdGrado
order by Descripcion">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                                        PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSalumnosterminados" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT A.IdAlumno, A.Nombre, A.ApePaterno, A.ApeMaterno, A.Matricula, A.Email, E.Resultado, E.FechaTermino
FROM Alumno A, EvaluacionTerminada E
WHERE (A.Estatus = 'Activo' or A.Estatus = 'Suspendido') and
A.IdGrupo = @IdGrupo AND A.IdPlantel = @IdPlantel 
and A.IdAlumno in (select IdAlumno from EvaluacionTerminada where
                                 IdEvaluacion = @IdEvaluacion)
and E.IdAlumno = A.IdAlumno and E.IdEvaluacion = @IdEvaluacion and E.IdGrupo = A.IdGrupo 
and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido')
ORDER BY ApePaterno, ApeMaterno, Nombre">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                                        PropertyName="SelectedValue" Type="Int32" />
                                    <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                                        PropertyName="SelectedValue" Type="Int32" />
                                    <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                                        PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDSevaluacionterminada" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [EvaluacionTerminada]"></asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                            <asp:SqlDataSource ID="SDSrespuestas" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Respuesta]"></asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

