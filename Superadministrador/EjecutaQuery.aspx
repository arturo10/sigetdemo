﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="EjecutaQuery.aspx.vb"
    Inherits="superadministrador_EjecutaQuery"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>



<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
    - Ejecuta Query
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .resizable {
            min-height: 150px;
            min-width: 850px;
            resize: both;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Ejecución de Queries
    </h1>
    Permite ejecutar consultas u operaciones sobre la base de datos del filesystem. 
    <br />
    <br />
    <span style="color: yellow; font-weight: 700;">Úsese con cautela.</span>
    <br />
    <br />
    <i>Esta opción sólo debe usarse por el área de sistemas.</i>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style11">
        <tr>
            <td colspan="4" style="text-align: center;">Ejecutar:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:RadioButtonList ID="RBcommandType" runat="server"
                    AutoPostBack="true"
                    RepeatDirection="Horizontal"
                    RepeatLayout="Flow"
                    Font-Bold="true">
                    <asp:ListItem Selected="true" Value="1">Consulta</asp:ListItem>
                    <asp:ListItem Value="2">Operación</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
            <td colspan="2"  style="text-align: center;">
                <asp:TextBox ID="TBoperacion" runat="server"
                    CssClass="resizable"
                    TextMode="MultiLine"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style26">&nbsp;</td>
            <td colspan="2" style="text-align: center;">
                <asp:Button ID="BtnExecOperacion" runat="server"
                    Text="Ejecutar Consulta"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
                <asp:Button ID="btnClear" runat="server" 
                    Text="Limpiar"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: left;">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
					Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium"/>
                <br />
                <br />
                <asp:GridView ID="GVresultados" runat="server"
                    ShowHeaderWhenEmpty="true"
                    Caption="[Resultados de la consulta]"

                    DataSourceID="SDSconsulta"
                    Min-Width="884px"

                    CssClass="dataGrid_clear" 
                    Font-Size="XX-Small"  
                    GridLines="None">
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: left;">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
    <asp:SqlDataSource ID="SDSconsulta" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"></asp:SqlDataSource>
</asp:Content>

