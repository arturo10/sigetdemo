﻿Imports Siget


Partial Class superadministrador_EvaluacionPlantel
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.PLANTELES
        Label2.Text = Config.Etiqueta.CICLO
        Label3.Text = Config.Etiqueta.NIVEL
        Label4.Text = Config.Etiqueta.GRADO
        Label5.Text = Config.Etiqueta.ASIGNATURA
        Label6.Text = Config.Etiqueta.PLANTELES
        Label7.Text = Config.Etiqueta.INSTITUCION
        Label8.Text = Config.Etiqueta.PLANTEL

        GVevalplantel.Caption = "<h3>Relación de " &
            Config.Etiqueta.PLANTELES &
            " y Actividades asignadas en " &
            Config.Etiqueta.ARTDET_CICLO & " " & Config.Etiqueta.CICLO &
            " seleccionad" & Config.Etiqueta.LETRA_CICLO & "</h3>"
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_CICLO & " " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_GRADO & " " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLcalificacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacion.DataBound
        DDLcalificacion.Items.Insert(0, New ListItem("---Elija una Calificación", 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub Agregar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Agregar.Click
        Try
            SDSevalPlantel.InsertCommand = "SET dateformat dmy; INSERT INTO EvaluacionPlantel (IdEvaluacion,IdPlantel,InicioContestar,FinSinPenalizacion,FinContestar,Estatus, FechaModif, Modifico) VALUES (" + GVevaluaciones.SelectedRow.Cells(1).Text + "," + DDLplantel.SelectedValue + ",'" + CalInicioContestar.SelectedDate.ToShortDateString + "','" + CalLimiteSinPenalizacion.SelectedDate.ToShortDateString + " 23:59:00','" + CalFinContestar.SelectedDate.ToShortDateString + " 23:59:00','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
            SDSevalPlantel.Insert()
            GVevalplantel.DataBind()
            DDLplantel.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Asegúrese de una Calificación Existente <br />" & ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Quitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Quitar.Click
        Try
            SDSevalPlantel.DeleteCommand = "DELETE FROM EvaluacionPlantel where IdEvaluacion = " + GVevalplantel.SelectedRow.Cells(3).Text + " and IdPlantel = " + GVevalplantel.SelectedRow.Cells(1).Text
            SDSevalPlantel.Delete()
            msgError.hide()
            GVevalplantel.DataBind()
            DDLplantel.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVevalplantel_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevalplantel.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.PLANTEL

            LnkHeaderText = e.Row.Cells(2).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.PLANTEL

            LnkHeaderText = e.Row.Cells(8).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA
        End If
    End Sub

    Protected Sub GVevalplantel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevalplantel.SelectedIndexChanged
        Try
            CalInicioContestar.SelectedDate = CDate(GVevalplantel.SelectedRow.Cells(5).Text)
            CalFinContestar.SelectedDate = CDate(GVevalplantel.SelectedRow.Cells(7).Text)
            CalLimiteSinPenalizacion.SelectedDate = CDate(GVevalplantel.SelectedRow.Cells(6).Text)
            'DDLplantel.SelectedValue = CInt(GVevalplantel.SelectedRow.Cells(1).Text) --> MARCARÍA ERROR PORQUE YA NO ESTÁ EN EL DLL PORQUE SE VAN QUITANDO AL AGREGARSE
            DDLestatus.SelectedValue = GVevalplantel.SelectedRow.Cells(9).Text
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        Try
            'ESTE CODIGO NO LO USE PORQUE AL SELECCIONAR NO EXISTIRIA EN EL DDL EL ID YA QUE SE VAN QUITANDO
            SDSevalPlantel.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionPlantel SET Estatus = '" + DDLestatus.SelectedValue + "', InicioContestar = '" + CalInicioContestar.SelectedDate.ToShortDateString + "', FinSinPenalizacion='" + CalLimiteSinPenalizacion.SelectedDate.ToShortDateString + " 23:59:00', FinContestar='" + CalFinContestar.SelectedDate.ToShortDateString + " 23:59:00', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdEvaluacion = " + GVevalplantel.SelectedRow.Cells(3).Text + _
                                           " and IdPlantel = " + GVevalplantel.SelectedRow.Cells(1).Text
            SDSevalPlantel.Update()
            GVevalplantel.DataBind()
            'No puede modificarse el plantel para que luego no se vayan a hacer bolas
            'DDLplantel.SelectedValue = CInt(GVevalplantel.SelectedRow.Cells(1).Text)
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVevalplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevalplantel.DataBound
        If GVevalplantel.Rows.Count > 0 Then
            Quitar.Visible = True
        Else
            Quitar.Visible = False
        End If
    End Sub

End Class
