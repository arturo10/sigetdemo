﻿Imports Siget

Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Partial Class superadministrador_RegistroAlumnos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GridView1.Caption = "<h3>" &
            Config.Etiqueta.ALUMNOS &
            " capturad" & Config.Etiqueta.LETRA_ALUMNO & "s</h3>"

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.INSTITUCION
        Label3.Text = Config.Etiqueta.PLANTEL
        Label4.Text = Config.Etiqueta.NIVEL
        Label5.Text = Config.Etiqueta.GRADO
        Label6.Text = Config.Etiqueta.CICLO
        Label7.Text = Config.Etiqueta.GRUPO
        Label8.Text = Config.Etiqueta.EQUIPO
        Label9.Text = Config.Etiqueta.SUBEQUIPO
        Label10.Text = Config.Etiqueta.ALUMNO

        'AQUI HAGO CICLO PRIMERO PARA LIMPIAR DDL Y LUEGO PARA LLENARLOS CON LOS VALORES DE EQUIPO Y SUBEQUIPO
        If (DDLequipo.Items.Count <= 0) And (DDLsubequipo.Items.Count <= 0) And (DDLinstitucion.SelectedIndex > 0) Then
            'Limpio por si las dudas
            DDLequipo.Items.Clear()
            DDLsubequipo.Items.Clear()

            Dim strConexion As String
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objConexion As New SqlConnection(strConexion)
            Dim miComando As SqlCommand
            Dim misRegistros As SqlDataReader
            miComando = objConexion.CreateCommand
            'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
            miComando.CommandText = "select * from Equipo where IdInstitucion = " + DDLinstitucion.SelectedValue.ToString + " order by Descripcion desc"
            Try
                objConexion.Open()
                'CARGO LOS EQUIPOS:
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                Do While misRegistros.Read()
                    DDLequipo.Items.Insert(0, misRegistros.Item("Descripcion"))
                Loop
                DDLequipo.Items.Insert(0, "")
                'AHORA CARGO LOS SUBEQUIPOS:
                misRegistros.Close()
                miComando.CommandText = "select * from Subequipo where IdInstitucion = " + DDLinstitucion.SelectedValue.ToString + " order by Descripcion desc"
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                Do While misRegistros.Read()
                    DDLsubequipo.Items.Insert(0, misRegistros.Item("Descripcion"))
                Loop
                DDLsubequipo.Items.Insert(0, "")
                misRegistros.Close()
                objConexion.Close()
                msgError.hide()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try
        End If

    End Sub

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_INSTITUCION & " " & Config.Etiqueta.INSTITUCION, 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_GRADO & " " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click
       Siget.Utils.GeneralUtils.CleanAll(Me.Controls)
    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        Try
            If Trim(TBlogin.Text).Length > 0 Then
                SDSusuarios.InsertCommand = "SET dateformat dmy; INSERT INTO Usuario(Login,Password,PerfilASP,Email, FechaModif, Modifico) VALUES('" + Trim(TBlogin.Text) + "','" + Trim(TBpassword.Text) + "','Alumno','" + Trim(TBemail.Text) + "', getdate(), '" & User.Identity.Name & "'); SELECT @NuevoID = @@Identity"
                Dim p As Parameter = New Parameter("NuevoId")
                p.Type = TypeCode.Int16
                p.Direction = Data.ParameterDirection.Output
                p.Size = 8
                SDSusuarios.InsertParameters.Add(p)
                SDSusuarios.Insert()

            Else
                msgError.show("Debe proporcionar un nombre de usuario (login) para " &
                    Config.Etiqueta.ARTDET_ALUMNO & " " & Config.Etiqueta.ALUMNO & ".")
                TBlogin.Focus()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub SDSusuarios_Inserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSusuarios.Inserted


        Try
            Dim IdUsuario = e.Command.Parameters("@NuevoId").Value
            
            SDSalumnos.InsertCommand = "SET dateformat dmy; INSERT INTO Alumno(IdUsuario,IdGrupo,IdPlantel,Nombre,ApePaterno,ApeMaterno,Matricula,Email,Equipo,Subequipo,Estatus,FechaIngreso, FechaModif, Modifico) VALUES (" + IdUsuario.ToString + "," + DDLgrupo.SelectedValue + "," + DDLplantel.SelectedValue + ",'" + Trim(TBnombre.Text) + "','" + Trim(TBApePaterno.Text) + "','" + Trim(TBApeMaterno.Text) + "','" + Trim(TBmatricula.Text) + "','" + Trim(TBemail.Text) + "','" + DDLequipo.SelectedItem.ToString + "','" + DDLsubequipo.SelectedItem.ToString + "','" + DDLestatus.SelectedValue + "',getdate(), getdate(), '" & User.Identity.Name & "')"
                SDSalumnos.Insert()

                Dim status As MembershipCreateStatus

                Try
                    Dim newUser As MembershipUser = Membership.CreateUser(Trim(TBlogin.Text), Trim(TBpassword.Text), _
                                                                   Trim(TBemail.Text), Nothing, _
                                                                   Nothing, True, status)
                    If newUser Is Nothing Then
                        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), GetErrorMessage(status))
                    Else
                        Roles.AddUserToRole(Trim(TBlogin.Text), "Alumno")
                    End If
                Catch ex As MembershipCreateUserException
                    Utils.LogManager.ExceptionLog_InsertEntry(ex)
                    msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
                End Try
            msgError.hide()
            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")), "El Alumno ha sido creado exitosamente")

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub


    Public Function GetErrorMessage(ByVal status As MembershipCreateStatus) As String
        Select Case status
            Case MembershipCreateStatus.DuplicateUserName
                Return "El usuario ya existe, elija otro usuario."
            Case MembershipCreateStatus.DuplicateEmail
                Return "Ya existe un usuario para ese email, por favor escriba un email diferente,"
            Case MembershipCreateStatus.InvalidPassword
                Return "El password es inválido, pruebe con otro."
            Case MembershipCreateStatus.InvalidEmail
                Return "El formato del email es inválido, por favor revíselo."
            Case MembershipCreateStatus.InvalidAnswer
                Return "La respuesta secreta es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidQuestion
                Return "La pregunta de recuperación de password es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidUserName
                Return "El nombre de usuario es inválido, corríjalo."
            Case MembershipCreateStatus.ProviderError
                Return "La autentificación falló, contacte al administrador del sistema."
            Case MembershipCreateStatus.UserRejected
                Return "La creación del usuario ha sido cancelada, consulte al administrador."
            Case Else
                Return "Ha ocurrido un error desconocido, verifique o contacte al administrador."
        End Select
    End Function

    Protected Sub GridView1_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(0).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.ALUMNO

            LnkHeaderText = e.Row.Cells(8).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.EQUIPO

            LnkHeaderText = e.Row.Cells(9).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.SUBEQUIPO
        End If
    End Sub

    Protected Sub SDSalumnos_Inserting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles SDSalumnos.Inserting
        e.Command.CommandTimeout = 0
    End Sub
End Class
