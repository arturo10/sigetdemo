﻿Imports Siget

Partial Class superadministrador_Temas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.NIVEL
        Label2.Text = Config.Etiqueta.GRADO
        Label3.Text = Config.Etiqueta.ASIGNATURA
    End Sub

    Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click, DDLasignatura.SelectedIndexChanged, DDLasignatura.TextChanged
        TBdescripcion.Text = ""
        TBnumero.Text = ""
    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            SDStemas.InsertCommand = "SET dateformat dmy; INSERT INTO Tema (IdAsignatura,Numero,Descripcion, FechaModif, Modifico) VALUES (" + DDLasignatura.SelectedValue + ",'" + TBnumero.Text + "','" + TBdescripcion.Text + "', getdate(), '" & User.Identity.Name & "')"
            SDStemas.Insert()
            msgSuccess.show("Éxito", "El registro ha sido guardado.")
            GVtemas.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVTemas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVtemas.SelectedIndexChanged
        'La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
        TBnumero.Text = GVtemas.SelectedRow.Cells(5).Text
        TBdescripcion.Text = HttpUtility.HtmlDecode(GVtemas.SelectedRow.Cells(4).Text)
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            SDStemas.UpdateCommand = "SET dateformat dmy; UPDATE Tema SET Numero = " + TBnumero.Text + ", Descripcion = '" + TBdescripcion.Text + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdTema =" + GVtemas.SelectedRow.Cells(3).Text
            SDStemas.Update()
            msgSuccess.show("Éxito", "El registro ha sido actualizado.")
            GVtemas.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLnivel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.SelectedIndexChanged, DDLnivel.TextChanged
        'SDSgrados.DataBind()
        DDLgrado.DataBind()
    End Sub

    Protected Sub DDLgrado_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.SelectedIndexChanged, DDLgrado.TextChanged
        'SDSasignaturas.DataBind()
        DDLasignatura.DataBind()
    End Sub


    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija la " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            SDStemas.DeleteCommand = "Delete from Tema where IdTema = " + GVtemas.SelectedValue.ToString 'Me da el IdGrado porque es el campo clave de la fila seleccionada
            SDStemas.Delete()
            GVtemas.DataBind()
            msgSuccess.show("Éxito", "El registro ha sido eliminado.")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub
End Class
