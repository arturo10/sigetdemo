<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="Opciones.aspx.vb"
    Inherits="superadministrador_Opciones"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
            height: 451px;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style21 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style16 {
            width: 461px;
            height: 61px;
        }

        .style11 {
            width: 92%;
        }

        .style20 {
            width: 283px;
            text-align: right;
        }

        .style18 {
            width: 91px;
        }

        .style19 {
            font-size: xx-small;
        }

        .style15 {
            width: 69%;
        }

        .style24 {
            width: 285px;
            text-align: right;
            height: 61px;
        }

        .style25 {
            font-size: x-small;
        }

        .style36 {
            font-weight: normal;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Reactivos - Opciones
    </h1>
    Permite capturar las opciones de respuesta para cada 
                    planteamiento de opci�n m�ltiple.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td style="text-align: right" class="style23">
                <asp:Label ID="Label5" runat="server" Text="[NIVEL]" />:</td>
            <td>
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion"
                    DataValueField="IdNivel" Width="260px" CssClass="style23">
                </asp:DropDownList>
            </td>
            <td style="text-align: right" class="style23">
                <asp:Label ID="Label6" runat="server" Text="[GRADO]" />:</td>
            <td>
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion"
                    DataValueField="IdGrado" Width="370px" CssClass="style23">
                </asp:DropDownList>
            </td>
            <td class="style23">&nbsp;</td>
            <td class="style23">&nbsp;</td>
            <td class="style23">&nbsp;</td>
            <td class="style23">&nbsp;</td>
        </tr>
        <tr>
            <td class="style21">
                <asp:Label ID="Label4" runat="server" Text="[ASIGNATURA]" />:</td>
            <td>
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSasignaturas" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Width="260px"
                    CssClass="style23">
                </asp:DropDownList>
            </td>
            <td style="text-align: right" class="style23">Temas:</td>
            <td>
                <asp:DropDownList ID="DDLtemas" runat="server" AutoPostBack="True"
                    DataSourceID="SDStemas" DataTextField="NomTema"
                    DataValueField="IdTema" Width="370px" CssClass="style23" Height="22px">
                </asp:DropDownList>
            </td>
            <td style="text-align: right" class="style23">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style21">Subtema:</td>
            <td colspan="5">
                <asp:DropDownList ID="DDLsubtemas" runat="server" AutoPostBack="True"
                    DataSourceID="SDSsubtemas" DataTextField="NomSubtema"
                    DataValueField="IdSubtema" Width="725px"
                    CssClass="style23" Height="22px">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="8">
                <table class="estandar">
                    <tr>
                        <td class="style24">Planteamientos capturados</td>
                        <td class="style16">
                            <asp:ListBox ID="LBplanteamiento" runat="server" AutoPostBack="True"
                                DataSourceID="SDSplanteamientos" DataTextField="Texto"
                                DataValueField="IdPlanteamiento" Height="87px" Width="525px"></asp:ListBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <asp:Panel ID="Panel1" runat="server">
                    <table class="style11">
                        <tr>
                            <td colspan="5">
                                <asp:Label ID="Mensaje1" runat="server" CssClass="titulo" Style="color: #000066">Capture los datos</asp:Label>
                            </td>
                        </tr>
                        <tr class="estandar">
                            <td class="style20">&nbsp;</td>
                            <td colspan="4" class="style25">Para ingresar saltos de l�nea en la redacci�n escriba la cadena: *salto* y para 
                                poner espacios en blanco: *_*</td>
                        </tr>
                        <tr class="estandar">
                            <td class="style20">Redacci�n de la Opci�n de Respuesta</td>
                            <td colspan="4">
                                <asp:TextBox ID="TBredaccion" runat="server" MaxLength="100" Rows="3"
                                    TextMode="MultiLine" Width="524px"
                                    onkeypress="k = (document.all) ? event.keyCode : event.which; return (k != 60 && k != 62);"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="estandar">
                            <td class="style20">Es correcta esta opci�n?</td>
                            <td colspan="4">
                                <asp:RadioButtonList ID="RBcorrecta" runat="server"
                                    RepeatDirection="Horizontal" Style="margin-left: 0px">
                                    <asp:ListItem Value="N">No</asp:ListItem>
                                    <asp:ListItem Value="S">Si</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr class="estandar">
                            <td class="style20">
                                <asp:Label ID="Label2" runat="server" Text="(Respuesta posici�n &lt;1&gt;)"
                                    Visible="False"></asp:Label>
                            </td>
                            <td class="style18">
                                <asp:TextBox ID="TBresp1" runat="server" MaxLength="25" Visible="False"></asp:TextBox>
                            </td>
                            <td colspan="2">
                                <asp:Label ID="Label3" runat="server" Text="(Respuesta posici�n &lt;2&gt;)"
                                    Visible="False"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TBresp2" runat="server" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr class="estandar">
                            <td class="style20">Inciso consecutivo (A,B,C,D,E,F)</td>
                            <td colspan="4">
                                <asp:DropDownList ID="DDLconsecutivo" runat="server" Width="50">
                                    <asp:Listitem Value="A">A</asp:Listitem>
                                    <asp:Listitem Value="B">B</asp:Listitem>
                                    <asp:Listitem Value="C">C</asp:Listitem>
                                    <asp:Listitem Value="D">D</asp:Listitem>
                                    <asp:Listitem Value="E">E</asp:Listitem>
                                    <asp:Listitem Value="F">F</asp:Listitem>
                                </asp:DropDownList>
                                &nbsp;<span class="style25">(El reactivo puede tener m�ximo 6 opciones de respuesta)</span></td>
                        </tr>
                        <tr class="estandar">
                            <td class="style20">&nbsp;</td>
                            <td colspan="4">
                                <asp:CheckBox ID="CBesconder" runat="server"
                                    Text="SOLO PARA INTERFAZ DE CAPTURA (No le aparecer�n al ALUMNO, se utiliza principalmente para la opci�n &quot;No Contest�&quot;)"
                                    Style="font-weight: 700" />
                            </td>
                        </tr>
                        <tr class="estandar">
                            <td class="style20">Cargar material de apoyo</td>
                            <td colspan="2">
                                <asp:FileUpload ID="RutaArchivo" runat="server" />
                            </td>
                            <td colspan="2" class="style19">*Al insertar o actualizar se cargar� el archivo</td>
                        </tr>
                        <tr class="estandar">
                            <td class="style20">Tipo de archivo</td>
                            <td colspan="2">
                                <asp:DropDownList ID="DDLtipoarchivo" runat="server" Width="100px">
                                    <asp:ListItem>Imagen</asp:ListItem>
                                    <asp:ListItem>PDF</asp:ListItem>
                                    <asp:ListItem>ZIP</asp:ListItem>
                                    <asp:ListItem>Word</asp:ListItem>
                                    <asp:ListItem>Excel</asp:ListItem>
                                    <asp:ListItem Value="PP">Power Point</asp:ListItem>
                                    <asp:ListItem>Video</asp:ListItem>
                                    <asp:ListItem>Audio</asp:ListItem>
                                </asp:DropDownList>
                                <span class="style19">(Imagenes de m�x. 350x350 px)</span></td>
                            <td colspan="2">
                                <asp:Label ID="Label1" runat="server"
                                    Style="color: #000099; font-size: x-small; font-weight: 700"></asp:Label>
                            </td>
                        </tr>
                        <tr class="estandar">
                            <td class="style20">&nbsp;</td>
                            <td colspan="4">
                                <asp:Button ID="Insertar" runat="server" Text="Insertar"
                                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                &nbsp;
                                <asp:Button ID="Limpiar" runat="server" Text="Limpiar"
                                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                                &nbsp;
                                <asp:Button ID="Actualizar" runat="server" Text="Actualizar"
                                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                &nbsp;
                                <asp:Button ID="Eliminar" runat="server" Text="Eliminar"
                                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                                &nbsp;
                                <asp:Button ID="Previsualizar" runat="server"
                                    PostBackUrl="~/superadministrador/Previsualizar.aspx"
                                    Text="Previsualizar" UseSubmitBehavior="False"
                                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <asp:GridView ID="GVopciones" runat="server" 
                    AllowPaging="True"
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 
                    PageSize="6"
                    Caption="Pude tener hasta 6 opciones por planteamiento" 
                    
                    DataKeyNames="IdOpcion" 
                    DataSourceID="SDSdatosopciones"
                    Width="884px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="true" ItemStyle-CssClass="selectCell"/>
                        <asp:BoundField DataField="IdOpcion" HeaderText="Id-Opcion"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdOpcion" />
                        <asp:BoundField DataField="IdPlanteamiento" HeaderText="IdPlanteamiento"
                            SortExpression="IdPlanteamiento" Visible="False" />
                        <asp:BoundField DataField="Redaccion" HeaderText="Redacci�n"
                            SortExpression="Redaccion" />
                        <asp:BoundField DataField="Correcta" HeaderText="Es correcta?"
                            SortExpression="Correcta" />
                        <asp:BoundField DataField="Resp1" HeaderText="Resp. posici�n 1"
                            SortExpression="Resp1" />
                        <asp:BoundField DataField="Resp2" HeaderText="Resp. posici�n 2"
                            SortExpression="Resp2" />
                        <asp:BoundField DataField="Consecutiva" HeaderText="Consecutivo"
                            SortExpression="Consecutiva" />
                        <asp:BoundField DataField="ArchivoApoyo" HeaderText="Archivo de Apoyo"
                            SortExpression="ArchivoApoyo" />
                        <asp:BoundField DataField="TipoArchivoApoyo" HeaderText="Tipo de Archivo"
                            SortExpression="TipoArchivoApoyo" />
                        <asp:BoundField DataField="Esconder" HeaderText="Solo captura"
                            SortExpression="Esconder" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="8" style="text-align: left;">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Men�
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSopciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Opcion]"></asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSdatosopciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdOpcion], [IdPlanteamiento], [Redaccion], [Correcta], [Resp1], [Resp2], [Consecutiva], [ArchivoApoyo], [TipoArchivoApoyo], [Esconder] FROM [Opcion] WHERE ([IdPlanteamiento] = @IdPlanteamiento)
order by [Consecutiva]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="LBplanteamiento" Name="IdPlanteamiento"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdAsignatura], [IdGrado], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDStemas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdTema], [IdAsignatura], Cast([Numero] as varchar(8))+ ' ' + [Descripcion] NomTema  FROM [Tema] WHERE ([IdAsignatura] = @IdAsignatura)
order by [Numero],[Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSsubtemas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdSubtema], [IdTema], Cast([Numero] as varchar(8)) + ' ' + [Descripcion] NomSubtema FROM [Subtema] WHERE ([IdTema] = @IdTema)
order by [Numero],[Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLtemas" Name="IdTema"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
             <asp:SqlDataSource ID="SDSplanteamientos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT CASE WHEN Redaccion<>'''' THEN (Cast(Consecutivo as varchar(5)) +  '.- '  +  Redaccion) ELSE (Cast(Consecutivo as varchar(5)) +  '.- ')END  Texto, IdSubtema, IdPlanteamiento FROM Planteamiento WHERE (IdSubtema = @IdSubtema)
order by [Consecutivo]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLsubtemas" Name="IdSubtema"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            <td>
              
            </td>
        </tr>
    </table>
</asp:Content>

