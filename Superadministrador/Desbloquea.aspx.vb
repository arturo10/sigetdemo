﻿Imports Siget

Imports System.Web.Security.Membership

Partial Class superadministrador_Desbloquea
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        LblActivos.Text = "En este momento hay " + GetNumberOfUsersOnline.ToString + " usuarios firmados en el sistema."
        LblMensaje.Text = "Hora de último desbloqueo automático: " & Utils.DesbloquearUsuarios.LastExecution
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            SDSusuarios.UpdateCommand = "SET dateformat dmy; UPDATE dbo.aspnet_Membership SET IsLockedOut = 0, FailedPasswordAttemptCount = 0, " + _
                "FailedPasswordAttemptWindowStart = CONVERT( datetime, '17540101', 112 ), FailedPasswordAnswerAttemptCount = 0, " + _
                "FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 ), " + _
                "LastLockoutDate = CONVERT( datetime, '17540101', 112 )"
            SDSusuarios.Update()

            LblMensaje.Text = "Las cuentas de usuarios han sido rehabilitadas"
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            LblMensaje.Text = ex.Message
        End Try
    End Sub

End Class
