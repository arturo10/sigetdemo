﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="Asignaturas.aspx.vb"
    Inherits="superadministrador_Asignaturas"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
            height: 450px;
        }

        .style16 {
            height: 25px;
        }

        .style17 {
            height: 10px;
            text-align: left;
        }

        .style18 {
            height: 28px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style11 {
            height: 27px;
            font-size: small;
        }

        .style13 {
            width: 78%;
        }

        .style14 {
            width: 156px;
            text-align: right;
        }

        .style19 {
            height: 7px;
        }

        .style23 {
            width: 156px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
        }

        .auto-style1 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            width: 70px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Administración de 
								<asp:Label ID="Label1" runat="server" Text="[ASIGNATURAS]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style11" colspan="4">
                <table class="style13">
                    <tr>
                        <td colspan="4">
                            <asp:Label ID="Mensaje" runat="server" CssClass="titulo"
                                Style="font-family: Arial, Helvetica, sans-serif; color: #000066;">Capture los datos</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style24">&nbsp;
														<asp:Label ID="Label2" runat="server" Text="[NIVEL]" />
                            :&nbsp;</td>
                        <td class="estandar" style="text-align: left;">
                            <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                                Width="232px" Height="22px">
                            </asp:DropDownList>
                        </td>
                        <td class="auto-style1">
                            <asp:Label ID="Label3" runat="server" Text="[GRADO]" />:</td>
                        <td class="estandar" style="text-align: left;">
                            <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                DataSourceID="SDSgrados" DataTextField="Descripcion"
                                DataValueField="IdGrado" Width="262px" Height="22px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style23">Área o Ciencia</td>
                        <td colspan="3" style="text-align: left;">
                            <asp:DropDownList ID="DDLarea" runat="server" AutoPostBack="True"
                                DataSourceID="SDSarea" DataTextField="Nombre" DataValueField="IdArea"
                                Height="22px" Width="540px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style23">Nombre de la 
														<asp:Label ID="Label4" runat="server" Text="[ASIGNATURA]" />
                        </td>
                        <td colspan="3" style="text-align: left;">
                            <asp:TextBox ID="TBdescripcion" runat="server" Width="451px" Height="22px"
                                MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Consecutivo <span style="font-size: x-small;">(Opcional)</span>
                        </td>
                        <td colspan="3" style="text-align: left">
                            <asp:TextBox ID="tbConsecutivo" runat="server" Width="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Duración (En Horas) <span style="font-size: x-small;"></span>
                        </td>
                        <td colspan="3" style="text-align: left">
                            <asp:TextBox ID="tbAsignatura" TextMode="Number" runat="server" Width="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Indicador <span style="font-size: x-small;">(opcional)</span>
                        </td>
                        <td colspan="3" style="text-align: left">
                            <asp:DropDownList ID="ddlIndicador" runat="server"
                                DataSourceID="SDSIndicadores" DataTextField="Descripcion"
                                DataValueField="IdIndicador" Width="280px">
                            </asp:DropDownList>
                            <asp:Image ID="Image1" runat="server"
                                Title=
        "Especifica un indicador que será utilizado para clasificar el resultado en conjunto de las actividades de la asignatura."
                                ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                                CssClass="infoPopper" />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="3" style="font-size: x-small; text-align: left;">
                            El consecutivo afecta el orden de aparición en el menú de actividades del <asp:Literal ID="Label5" runat="server">[ALUMNO]</asp:Literal>: 
                            mientras menor sea el consecutivo mayor es la prioridad de aparición. De no haber consecutivo (o entre iguales), se ordenan alfabéticamente.
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style14">&nbsp;</td>
                        <td colspan="3" style="font-size: medium;">
                            <asp:Button ID="Insertar" runat="server" Text="Insertar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Limpiar" runat="server" Text="Limpiar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Actualizar" runat="server" Text="Actualizar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Eliminar" runat="server" Text="Eliminar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style16" colspan="4">
                <asp:GridView ID="GVasignaturas" runat="server"
                    AllowPaging="True"
                    AllowSorting="True"
                    AutoGenerateColumns="False"
                    AutoGenerateSelectButton="True"
                    DataKeyNames="IdAsignatura, IdIndicador,Horas"
                    DataSourceID="SDSdatosasignaturas"
                    CellPadding="3"
                    ForeColor="Black"
                    GridLines="Vertical"
                    Style="font-size: x-small; font-family: Arial, Helvetica, sans-serif;"
                    Width="836px"
                    BackColor="White"
                    BorderColor="#999999"
                    BorderStyle="Solid"
                    BorderWidth="1px">
                    <Columns>
                        <asp:BoundField DataField="IdGrado" HeaderText="Id-Grado"
                            SortExpression="IdGrado" Visible="False" />
                        <asp:BoundField DataField="Grado" HeaderText="Grado" SortExpression="Grado"
                            Visible="False" />
                        <asp:BoundField DataField="IdAsignatura" HeaderText="Id_Asignatura"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdAsignatura" />
                        <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"
                            SortExpression="Asignatura" />
                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                            SortExpression="Consecutivo" />
                        <asp:BoundField DataField="IdArea" HeaderText="Id-Area"
                            SortExpression="IdArea" />
                        <asp:BoundField DataField="Area" HeaderText="Area de Conocimiento"
                            SortExpression="Area" />
                        <asp:BoundField DataField="Indicador" HeaderText="Indicador"
                            SortExpression="Indicador" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  select
	                1  idGrado
	                2  Grado
	                3  Id_ASIGNATURA
	                4  ASIGNATURA
	                5  Idarea
	                6  area
                --%>
            </td>
        </tr>
        <tr>
            <td class="style16">&nbsp;</td>
            <td class="style16">&nbsp;</td>
            <td class="style16">&nbsp;</td>
            <td class="style16">&nbsp;</td>
        </tr>
        <tr>
            <td class="style17" colspan="4">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSarea" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdArea, Descripcion + ' (' + ISNULL(Clave, '') + ')' as Nombre
from Area
where Estatus = 'Activo'
order by Descripcion"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Asignatura]"></asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSdatosasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT A.Horas,A.IdAsignatura, A.IdGrado, G.Descripcion Grado, A.Descripcion Asignatura, A.Consecutivo, A.IdArea, Ar.Descripcion Area, A.IdIndicador, I.Descripcion 'Indicador'
FROM Grado G, Area Ar, Asignatura A LEFT JOIN Indicador I ON A.IdIndicador = I.IdIndicador
WHERE (A.IdGrado = @IdGrado) and (G.IdGrado = A.IdGrado)
and (Ar.IdArea = A.IdArea) 
order by G.Descripcion, A.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>
                <asp:SqlDataSource ID="SDSIndicadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdIndicador], [Descripcion] FROM [Indicador] ORDER BY [Descripcion]"></asp:SqlDataSource>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>

