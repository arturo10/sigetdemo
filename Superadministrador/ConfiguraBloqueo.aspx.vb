﻿Imports Siget

Imports System.Data
Imports System.Xml

Partial Class superadministrador_ConfiguraBloqueo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.INSTITUCION
        msgError.hide()
        msgSuccess.hide()

        Dim oDs As New DataSet
    oDs.ReadXml(Config.Global.rutaBloqueos)
    If oDs.Tables.Count > 0 Then
      GVBloqueos.DataSource = oDs
      GVBloqueos.DataBind()
      GVBloqueos.Visible = True
    Else
      GVBloqueos.Visible = False
    End If
  End Sub

  Protected Sub Insertar_Click(sender As Object, e As EventArgs) Handles Insertar.Click
    ' encontrar el último índice introducido
    Dim indice As Integer
    If GVBloqueos.Rows.Count = 0 Then
      indice = 0
    Else
      indice = GVBloqueos.Rows(GVBloqueos.Rows.Count() - 1).Cells(1).Text
    End If
    ' aumentar al siguiente para dejarlo listo
    indice = indice + 1

    ' primero inserto el nuevo nodo al xml, y luego actualizo el gridview:
    Dim xmldoc As New XmlDocument()
    xmldoc.Load(Config.Global.rutaBloqueos)

    'Select main node
    Dim newXMLNode As XmlNode = xmldoc.SelectSingleNode("/Bloqueos")
    'get the node where you want to insert the data
    Dim childNode As XmlNode = xmldoc.CreateNode(XmlNodeType.Element, "Bl", "")
    'In the below step "name" is your node name and "sree" is your data to insert
    'Dim newAttribute As XmlAttribute = xmldoc.CreateAttribute("name", "sree", "")
    'childNode.Attributes.Append(newAttribute)
    Dim newAttribute As XmlNode = xmldoc.CreateNode(XmlNodeType.Element, "IdBloqueo", "")
    newAttribute.InnerText = indice
    childNode.AppendChild(newAttribute)
    newAttribute = xmldoc.CreateNode(XmlNodeType.Element, "Institucion", "")
    newAttribute.InnerText = DDLInstitucion.SelectedValue
    childNode.AppendChild(newAttribute)
    newAttribute = xmldoc.CreateNode(XmlNodeType.Element, "Dia", "")
    newAttribute.InnerText = DDLDia.SelectedValue
    childNode.AppendChild(newAttribute)
    newAttribute = xmldoc.CreateNode(XmlNodeType.Element, "Perfil", "")
    newAttribute.InnerText = DDLPerfil.SelectedValue
    childNode.AppendChild(newAttribute)
    newAttribute = xmldoc.CreateNode(XmlNodeType.Element, "Inicio", "")
    newAttribute.InnerText = InicioHora.SelectedItem.Text & ":" & InicioMinuto.SelectedItem.Text
    childNode.AppendChild(newAttribute)
    newAttribute = xmldoc.CreateNode(XmlNodeType.Element, "Fin", "")
    newAttribute.InnerText = FinHora.SelectedItem.Text & ":" & FinMinuto.SelectedItem.Text
    childNode.AppendChild(newAttribute)

    newXMLNode.AppendChild(childNode)

    xmldoc.Save(Config.Global.rutaBloqueos)


    ' actualizo el gridview
    Dim oDs As New DataSet
    oDs.ReadXml(Config.Global.rutaBloqueos)
    If oDs.Tables.Count > 0 Then
      GVBloqueos.DataSource = oDs
      GVBloqueos.DataBind()
      GVBloqueos.Visible = True
    Else
      GVBloqueos.Visible = False
    End If
  End Sub

  Protected Sub Eliminar_Click(sender As Object, e As EventArgs) Handles Eliminar.Click
    If GVBloqueos.SelectedIndex <> -1 AndAlso GVBloqueos.SelectedIndex < GVBloqueos.Rows.Count() Then
      ' primero cargo el documento
      Dim xmldoc As New XmlDocument()
      xmldoc.Load(Config.Global.rutaBloqueos)

      ' luego selecciono el nodo que contiene el índice de la fila seleccionada
      Dim node As XmlNode = xmldoc.SelectSingleNode(
          "//IdBloqueo[. = '" & GVBloqueos.SelectedRow.Cells(1).Text & "']" & "/parent::node()/IdBloqueo")
      ' si si se encontró uno (debería)
      If node IsNot Nothing Then
        ' quito el nodo de la estructura
        node.ParentNode.ParentNode.RemoveChild(node.ParentNode)
        ' guardo el archivo nuevo
        xmldoc.Save(Config.Global.rutaBloqueos)
      End If

      ' actualizo el gridview
      Dim oDs As New DataSet
      oDs.ReadXml(Config.Global.rutaBloqueos)
      If oDs.Tables.Count > 0 Then
        GVBloqueos.DataSource = oDs
        GVBloqueos.DataBind()
        GVBloqueos.Visible = True
      Else
        GVBloqueos.Visible = False
      End If
    Else
      msgError.show("Debe seleccionar un elemento a eliminar.")
    End If
  End Sub

End Class
