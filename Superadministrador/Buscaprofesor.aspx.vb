﻿Imports Siget

Partial Class superadministrador_Buscaprofesor
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GVprofesores.Caption = "<h3>Listado de " &
            Config.Etiqueta.PROFESORES &
            " encontrad" & Config.Etiqueta.LETRA_PROFESOR & "s</h3>"

        Label1.Text = Config.Etiqueta.PROFESOR
        Label2.Text = Config.Etiqueta.PROFESOR
    End Sub

    Protected Sub BtnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBuscar.Click
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Try
            SDSbuscar.ConnectionString = strConexion
            SDSbuscar.SelectCommand = "select Distinct P.IdProfesor, P.Clave, P.Nombre, P.Apellidos, Pl.Descripcion as Plantel, U.Login, U.Password, P.Estatus " + _
                        "from Profesor P, Plantel Pl, Usuario U " + _
                        "where U.IdUsuario = P.IdUsuario And Pl.IdPlantel = P.IdPlantel " + _
                        "And P.Clave like '%" + TBclave.Text + "%' and P.Nombre like '%" + TBnombre.Text + "%' and P.Apellidos like '%" + _
                        TBapellidos.Text + "%' and U.Login like '%" + TBlogin.Text + "%' order by P.Apellidos, P.Nombre"
            GVprofesores.DataBind() 'Es necesario para que se vea el resultado
            msgInfo.show("Se encontraron " + GVprofesores.Rows.Count.ToString + " profesores con los datos capturados.")
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            msgInfo.hide()
        End Try
    End Sub

    Protected Sub GVprofesores_PreRender(sender As Object, e As EventArgs) Handles GVprofesores.PreRender
        ' Etiquetas de GridView
        GVprofesores.Columns(1).HeaderText = "Id_" & Config.Etiqueta.PROFESOR

        GVprofesores.Columns(5).HeaderText = Config.Etiqueta.PLANTEL
    End Sub

    Protected Sub GVprofesores_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVprofesores.SelectedIndexChanged
        msgInfo.show("Seleccionado: " + HttpUtility.HtmlDecode(GVprofesores.SelectedRow.Cells(4).Text) + " " + HttpUtility.HtmlDecode(GVprofesores.SelectedRow.Cells(3).Text))
        LblProfesor.Text = HttpUtility.HtmlDecode(GVprofesores.SelectedRow.Cells(4).Text) + " " + HttpUtility.HtmlDecode(GVprofesores.SelectedRow.Cells(3).Text)
        TBpasswordOK.Text = HttpUtility.HtmlDecode(Trim(GVprofesores.SelectedRow.Cells(7).Text))
    End Sub

    Protected Sub BtnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnActualizar.Click
        Try
            'Actualizo password en tabla de seguridad, no puedo cambiar Login
            Dim u As MembershipUser
            u = Membership.GetUser(Trim(HttpUtility.HtmlDecode(GVprofesores.SelectedRow.Cells(6).Text)))
            u.ChangePassword(Trim(HttpUtility.HtmlDecode(GVprofesores.SelectedRow.Cells(7).Text)), Trim(TBpasswordOK.Text))
            'u.Email = TBemail.Text
            'Membership.UpdateUser(u)
            SDSusuarios.UpdateCommand = "SET dateformat dmy; UPDATE Usuario set Password = '" + Trim(TBpasswordOK.Text) + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where Login = '" + Trim(HttpUtility.HtmlDecode(GVprofesores.SelectedRow.Cells(6).Text)) + "'"
            SDSusuarios.Update()
            GVprofesores.DataBind()
            msgError1.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError1.show("Error", ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnGenerar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGenerar.Click
        Try
            'Ahora registro el usuario en la base de datos de seguridad
            Dim status As MembershipCreateStatus

            ' 17/10/2013 se deseleccionó requiresQuestionAndAnswer="false"  de web config, no es necesario
            'Dim passwordQuestion As String = ""
            'Dim passwordAnswer As String = ""
            'If Membership.RequiresQuestionAndAnswer Then
            '    passwordQuestion = "Empresa desarrolladora de " &
            '        Config.Etiqueta.SISTEMA_CORTO
            '    passwordAnswer = "Integrant"
            'End If

            Dim newUser As MembershipUser = Membership.CreateUser(Trim(HttpUtility.HtmlDecode(GVprofesores.SelectedRow.Cells(6).Text)), Trim(TBpasswordOK.Text), _
                                                           Trim(TBemailOK.Text), Nothing, _
                                                           Nothing, True, status)
            msgError1.hide() 'La pongo aquí para que no se limpie el error que capturo con la función
            If newUser Is Nothing Then
                msgError1.show("Error", GetErrorMessage(status))
            Else
                Roles.AddUserToRole(TBlogin.Text, "Profesor")
            End If
            SDSusuarios.UpdateCommand = "SET dateformat dmy; UPDATE Usuario set Password = '" + Trim(TBpasswordOK.Text) + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where Login = '" + Trim(HttpUtility.HtmlDecode(GVprofesores.SelectedRow.Cells(6).Text)) + "'"
            SDSusuarios.Update()
            GVprofesores.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError1.show("Error", ex.Message.ToString())
        End Try
    End Sub

    Public Function GetErrorMessage(ByVal status As MembershipCreateStatus) As String
        Select Case status
            Case MembershipCreateStatus.DuplicateUserName
                Return "El usuario ya existe, elija Actualizar el password."
            Case MembershipCreateStatus.DuplicateEmail
                Return "Ya existe un usuario para ese email, por favor escriba un email diferente,"
            Case MembershipCreateStatus.InvalidPassword
                Return "El password es inválido, pruebe con otro."
            Case MembershipCreateStatus.InvalidEmail
                Return "El formato del email es inválido, por favor revíselo."
            Case MembershipCreateStatus.InvalidAnswer
                Return "La respuesta secreta es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidQuestion
                Return "La pregunta de recuperación de password es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidUserName
                Return "El nombre de usuario es inválido, corríjalo."
            Case MembershipCreateStatus.ProviderError
                Return "La autentificación falló, contacte al administrador del sistema."
            Case MembershipCreateStatus.UserRejected
                Return "La creación del usuario ha sido cancelada, consulte al administrador."
            Case Else
                Return "Ha ocurrido un error desconocido, verifique o contacte al administrador."
        End Select
    End Function

End Class
