﻿Imports Siget

Imports System.IO

Partial Class superadministrador_ApoyoNivel
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label2.Text = Config.Etiqueta.NIVEL
    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        If Trim(TBconsecutivo.Text) = "" Then TBconsecutivo.Text = "0"
        Try
            Dim archivocargado As String

            If RutaArchivo.HasFile Or (Trim(TBlink.Text).Length > 0) Then 'El atributo .HasFile compara si se indico un archivo           
                If DDLtipoarchivo.SelectedValue = "Link" Or DDLtipoarchivo.SelectedValue = "YouTube" Then
                    archivocargado = Trim(TBlink.Text)
                Else
                    archivocargado = RutaArchivo.FileName
                End If
                SDSApoyoNivel.InsertCommand = "SET dateformat dmy; INSERT INTO ApoyoNivel (IdNivel,Consecutivo,Descripcion,ArchivoApoyo,TipoArchivoApoyo,Estatus, FechaModif, Modifico) VALUES (" + _
                DDLnivel.SelectedValue + "," + TBconsecutivo.Text + ",'" + TBdescripcion.Text + "','" + archivocargado + "','" + DDLtipoarchivo.SelectedValue + "','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"

                'Ahora CARGO el archivo solo si NO es un link
                If DDLtipoarchivo.SelectedValue <> "Link" AndAlso DDLtipoarchivo.SelectedValue <> "YouTube" Then
                    Dim ruta As String
          ruta = Config.Global.rutaApoyo

          Dim MiArchivo2 As FileInfo = New FileInfo(ruta & RutaArchivo.FileName)
          If MiArchivo2.Exists Then
            'Avisa que ya existe, NO LO SUBE pero sí almacena el registro
            msgError.show("YA EXISTE UN ARCHIVO CON ESE NOMBRE EN EL ALMACEN DE ARCHIVOS, CAMBIE EL NOMBRE DE SU ARCHIVO E INTENTE CARGARLO NUEVAMENTE, POR FAVOR.")
          Else
            RutaArchivo.SaveAs(ruta & RutaArchivo.FileName)
            SDSApoyoNivel.Insert()
            msgSuccess.show("Éxito", "El registro ha sido guardado.")
            GVapoyosacademicos.DataBind()
            Label1.Text = "Archivo cargado: " & archivocargado
            msgError.hide()
          End If
        Else
          SDSApoyoNivel.Insert()
          msgSuccess.show("Éxito", "El registro ha sido guardado.")
          GVapoyosacademicos.DataBind()
          Label1.Text = "Archivo cargado: " & archivocargado
          msgError.hide()
        End If
      Else 'Significa que NO indicó archivo para cargar
        msgError.show("No ha indicado ningún archivo o link.")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
    DDLnivel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.NIVEL, 0))
  End Sub

  Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
    Try
      SDSApoyoNivel.DeleteCommand = "Delete from ApoyoNivel where IdApoyoNivel = " + GVapoyosacademicos.SelectedRow.Cells(1).Text
      SDSApoyoNivel.Delete()

      If GVapoyosacademicos.SelectedRow.Cells(6).Text <> "Link" AndAlso GVapoyosacademicos.SelectedRow.Cells(6).Text <> "YouTube" Then
        'Elimino el archivo de apoyo que tenga la opción
        If Trim(HttpUtility.HtmlDecode(GVapoyosacademicos.SelectedRow.Cells(5).Text)).Length > 1 Then   'Significa que tiene un valor en ese campo
          Dim ruta As String
          ruta = Config.Global.rutaApoyo

          'Borro el archivo que estaba anteriormente porque se pondrá uno nuevo 
          Dim MiArchivo As FileInfo = New FileInfo(ruta & Trim(HttpUtility.HtmlDecode(GVapoyosacademicos.SelectedRow.Cells(5).Text)))
          MiArchivo.Delete()
        End If
      End If

      GVapoyosacademicos.DataBind() 'Dejo esta instrucción al final porque si se refresca el GridView antes de borrar el archivo de apoyo, ya no podré saber su nombre
      msgSuccess.show("Éxito", "El registro ha sido eliminado, al igual que su archivo de apoyo si lo tenía.")
      Label1.Text = ""

    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgSuccess.hide()
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
    ' Por motivos de optimización conviene solo utilizar actualizar como actualización de info, no carga de archivos

    Try
      SDSApoyoNivel.UpdateCommand = "SET dateformat dmy; UPDATE ApoyoNivel SET Consecutivo = " + TBconsecutivo.Text + ", Descripcion = '" + TBdescripcion.Text + "', Estatus = '" + DDLestatus.SelectedValue + "', TipoArchivoApoyo = '" + DDLtipoarchivo.SelectedValue + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdApoyoNivel =" + GVapoyosacademicos.SelectedRow.Cells(1).Text
      SDSApoyoNivel.Update()
      msgSuccess.show("Éxito", "El registro ha sido actualizado.")
      msgError.hide()
      GVapoyosacademicos.DataBind()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try

    'ESTE CODIGO NO CONVIENE UTILIZARLO
    'Try
    '    Dim archivocargado As String

    '    If RutaArchivo.HasFile Or (Trim(TBlink.Text).Length > 0) Then 'El atributo .HasFile compara si se indico un archivo
    '        If DDLtipoarchivo.SelectedValue = "Link" Then
    '            archivocargado = Trim(TBlink.Text)
    '        Else
    '            archivocargado = RutaArchivo.FileName
    '        End If
    '        SDSApoyoNivel.UpdateCommand = "SET dateformat dmy; UPDATE ApoyoNivel SET Consecutivo = " + TBconsecutivo.Text + ", Descripcion = '" + TBdescripcion.Text + "', Estatus = '" + DDLestatus.SelectedValue + "', ArchivoApoyo = '" + archivocargado + "', TipoArchivoApoyo = '" + DDLtipoarchivo.SelectedValue + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdApoyoNivel =" + GVapoyosacademicos.SelectedRow.Cells(1).Text

    '        'Ahora CARGO el archivo solo si NO es un link
    '        If DDLtipoarchivo.SelectedValue <> "Link" Then
    '            Dim ruta As String
    '            ruta = Config.Global.rutaApoyo

    '            RutaArchivo.SaveAs(ruta & RutaArchivo.FileName)
    '            Label1.Text = "Archivo cargado: " & RutaArchivo.FileName
    '        End If
    '    Else
    '        SDSApoyoNivel.UpdateCommand = "SET dateformat dmy; UPDATE ApoyoNivel SET Consecutivo = " + TBconsecutivo.Text + ", Descripcion = '" + TBdescripcion.Text + "', Estatus = '" + DDLestatus.SelectedValue + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdApoyoNivel =" + GVapoyosacademicos.SelectedRow.Cells(1).Text
    '        'Para eliminar la referencia al archivo de apoyo, agregaría al SET: ArchivoApoyo=NULL, TipoArchivoApoyo=NULL,
    '    End If

    '    SDSApoyoNivel.Update()
    '    Mensaje.Text = "El registro ha sido actualizado"
    '    msgError.hide()
    '    GVapoyosacademicos.DataBind()
    'Catch ex As Exception
    '    Mensaje.Text = ""
    '    msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    'End Try
  End Sub

    Protected Sub GVapoyosacademicos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVapoyosacademicos.SelectedIndexChanged
        If Trim(GVapoyosacademicos.SelectedRow.Cells(4).Text) = "Link" Or Trim(GVapoyosacademicos.SelectedRow.Cells(4).Text) = "YouTube" Then
            RutaArchivo.Visible = False
            TBlink.Visible = True
            TBlink.Text = GVapoyosacademicos.SelectedRow.Cells(4).Text
            Label1.Text = ""
        Else
            RutaArchivo.Visible = True
            TBlink.Visible = False
            Label1.Text = Trim(HttpUtility.HtmlDecode(GVapoyosacademicos.SelectedRow.Cells(4).Text))
            TBlink.Text = ""
        End If

        'La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
        TBconsecutivo.Text = GVapoyosacademicos.SelectedRow.Cells(3).Text
        TBdescripcion.Text = HttpUtility.HtmlDecode(Trim(GVapoyosacademicos.SelectedRow.Cells(4).Text))
        DDLtipoarchivo.SelectedValue = HttpUtility.HtmlDecode(Trim(GVapoyosacademicos.SelectedRow.Cells(6).Text))
        DDLestatus.SelectedValue = HttpUtility.HtmlDecode(Trim(GVapoyosacademicos.SelectedRow.Cells(7).Text))

        msgError.hide()
    End Sub

    Protected Sub DDLtipoarchivo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLtipoarchivo.SelectedIndexChanged
        If DDLtipoarchivo.SelectedValue = "Link" Or DDLtipoarchivo.SelectedValue = "YouTube" Then
            RutaArchivo.Visible = False
            TBlink.Visible = True
            If DDLtipoarchivo.SelectedValue = "Link" Then
                msgInfo.show("Revise", "Si el enlace es a una página fuera del sistema, el link debe incluir dos diagonales al inicio (//). <br />Por ejemplo: //www.google.com.mx")
            Else
                msgInfo.hide()
            End If
        Else
            RutaArchivo.Visible = True
            TBlink.Text = ""
            TBlink.Visible = False
        End If
    End Sub

End Class
