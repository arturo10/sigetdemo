﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="CiclosEscolares.aspx.vb"
    Inherits="coordinador_CiclosEscolares"
    EnableEventValidation="false"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style12 {
            height: 186px;
        }

        .style11 {
            width: 100%;
            height: 468px;
        }

        .style14 {
            width: 115px;
            text-align: right;
        }

        .style15 {
            width: 149px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style16 {
            height: 15px;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 38px;
        }

        .style25 {
            width: 115px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Administración de
        <asp:Label ID="Label1" runat="server" Text="[CICLOS]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style11">
        <tr>
            <td colspan="2">
                <asp:Label ID="Mensaje" runat="server" CssClass="style23"
                    Style="color: #003399">Capture los datos</asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style12" colspan="2">
                <table class="estandar">
                    <tr>
                        <td class="style25">Descripción</td>
                        <td>
                            <asp:TextBox ID="TBdescripcion" runat="server" Width="250px" MaxLength="20"></asp:TextBox>
                        </td>
                        <td class="style15">Indicador</td>
                        <td>
                            <asp:DropDownList ID="DDLindicador" runat="server" AutoPostBack="True"
                                DataSourceID="SDSindicador" DataTextField="Descripcion"
                                DataValueField="IdIndicador" Width="200px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style25">Fecha de Inicio del&nbsp; 
														<asp:Label ID="Label2" runat="server" Text="[CICLO]" />
                        </td>
                        <td>
                            <asp:Calendar ID="CalInicio" runat="server" BackColor="White"
                                BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest"
                                Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px"
                                Width="200px">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#808080" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>
                        </td>
                        <td class="style15">Fecha de Finalización del 
														<asp:Label ID="Label3" runat="server" Text="[CICLO]" /></td>
                        <td>
                            <asp:Calendar ID="CalFin" runat="server" BackColor="White"
                                BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest"
                                Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px"
                                Width="200px">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#808080" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>
                        </td>
                    </tr>
                    <tr>
                        <td class="style25">Estatus</td>
                        <td colspan="3">
                            <asp:DropDownList ID="DDLestatus" runat="server" Height="22px" Width="123px">
                                <asp:ListItem>Activo</asp:ListItem>
                                <asp:ListItem>Suspendido</asp:ListItem>
                                <asp:ListItem>Baja</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style14">&nbsp;</td>
                        <td colspan="3">
                            <asp:Button ID="Insertar" runat="server" Text="Insertar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Limpiar" runat="server" Text="Limpiar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Actualizar" runat="server" Text="Actualizar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Eliminar" runat="server" Text="Eliminar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style16" colspan="2">
                <asp:GridView ID="GVciclosescolares" runat="server"
                    AllowPaging="True"
                    AllowSorting="True"
                    AutoGenerateColumns="False"
                    AutoGenerateSelectButton="True"
                    DataKeyNames="IdCicloEscolar"
                    DataSourceID="SDScicloescolar"

                    BackColor="White"
                    BorderColor="#999999"
                    BorderStyle="Solid"
                    BorderWidth="1px"
                    CellPadding="3"
                    GridLines="Vertical"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"
                    Width="870px"
                    ForeColor="Black">
                    <Columns>
                        <asp:BoundField DataField="IdCicloEscolar" HeaderText="Id Ciclo Escolar"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdCicloEscolar" />
                        <asp:BoundField DataField="IdIndicador" HeaderText="Id_Indicador"
                            SortExpression="IdIndicador" />
                        <asp:BoundField DataField="Descripcion"
                            HeaderText="Descripción" SortExpression="Descripcion" />
                        <asp:BoundField DataField="FechaInicio" DataFormatString="{0:dd/MM/yy}"
                            HeaderText="Inicia el" SortExpression="FechaInicio" />
                        <asp:BoundField DataField="FechaFin" HeaderText="Termina el"
                            SortExpression="FechaFin" DataFormatString="{0:dd/MM/yy}" />
                        <asp:BoundField DataField="FechaModif" HeaderText="FechaModif"
                            SortExpression="FechaModif" Visible="False" />
                        <asp:BoundField DataField="Modifico" HeaderText="Modifico"
                            SortExpression="Modifico" Visible="False" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                            SortExpression="Estatus" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  id_CICLO
	                1  id_indicador
	                2  descripcion
	                3  fechainicio
	                4  termina el
	                5  fechamodif
	                6  modifico
	                7  estatus
	                --%>
            </td>
        </tr>
        <tr>
            <td class="style16">
                <asp:SqlDataSource ID="SDScicloescolar" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdCicloEscolar], [IdIndicador], [Descripcion], [FechaInicio], [FechaFin], [FechaModif], [Modifico], [Estatus] FROM [CicloEscolar]"></asp:SqlDataSource>
            </td>
            <td class="style16">
                <asp:SqlDataSource ID="SDSindicador" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Indicador] ORDER BY [Descripcion]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

