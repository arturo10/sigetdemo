﻿Imports Siget


Partial Class superadministrador_Asignaturas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.ASIGNATURAS
        Label2.Text = Config.Etiqueta.NIVEL
        Label3.Text = Config.Etiqueta.GRADO
        Label4.Text = Config.Etiqueta.ASIGNATURA
        Label5.Text = Config.Etiqueta.ALUMNO
    End Sub

    Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click, DDLgrado.SelectedIndexChanged
        TBdescripcion.Text = ""
        Mensaje.Text = "Capture los datos"
    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
      
        Try
            SDSasignaturas.InsertParameters.Clear()

            SDSasignaturas.InsertCommand = "SET dateformat dmy; INSERT INTO Asignatura (Consecutivo,IdGrado,Descripcion,IdArea, IdIndicador, FechaModif, Modifico,Horas) VALUES (@Consecutivo, @IdGrado,@Descripcion,@IdArea, @IdIndicador, getdate(), @Modifico,@Horas)"
            SDSasignaturas.InsertParameters.Add("Consecutivo", tbConsecutivo.Text.Trim())
            SDSasignaturas.InsertParameters.Add("IdGrado", DDLgrado.SelectedValue)
            SDSasignaturas.InsertParameters.Add("Descripcion", TBdescripcion.Text.Trim())
            SDSasignaturas.InsertParameters.Add("IdArea", DDLarea.SelectedValue)
            SDSasignaturas.InsertParameters.Add("Horas", IIf(IsDBNull(tbAsignatura.Text), tbAsignatura.Text, 0))
            If Not ddlIndicador.SelectedValue = 0 Then
                SDSasignaturas.InsertParameters.Add("IdIndicador", ddlIndicador.SelectedValue)
            Else
                SDSasignaturas.InsertParameters.Add("IdIndicador", Nothing)
            End If
            SDSasignaturas.InsertParameters.Add("Modifico", User.Identity.Name)
            SDSasignaturas.Insert()
            Mensaje.Text = "El registro ha sido guardado"
            GVasignaturas.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVasignaturas_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVasignaturas.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(3).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.ASIGNATURA

            LnkHeaderText = e.Row.Cells(4).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA
        End If
    End Sub

#Region "   ddlIndicador"

    Protected Sub ddlIndicador_DataBound(
                    ByVal sender As Object,
                    ByVal e As System.EventArgs) Handles ddlIndicador.DataBound
        ' añado un indicador inválido a la lista de opciones del usuario
        ddlIndicador.Items.Insert(0, New ListItem("---Ninguno", 0))
    End Sub

#End Region

    Protected Sub GVasignaturas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVasignaturas.SelectedIndexChanged
        'La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
        TBdescripcion.Text = HttpUtility.HtmlDecode(GVasignaturas.SelectedRow.Cells(4).Text).Trim()
        'DDLarea.SelectedValue = HttpUtility.HtmlDecode(GVasignaturas.SelectedRow.Cells(D5).Text)
        DDLarea.SelectedValue = CInt(GVasignaturas.SelectedRow.Cells(6).Text.Trim())
        tbConsecutivo.Text = HttpUtility.HtmlDecode(GVasignaturas.SelectedRow.Cells(5).Text).Trim()
        tbAsignatura.Text = GVasignaturas.SelectedDataKey.Values(2)

        If Not IsDBNull(GVasignaturas.SelectedDataKey(1)) Then
            ddlIndicador.SelectedIndex = ddlIndicador.Items.IndexOf(ddlIndicador.Items.FindByValue(GVasignaturas.SelectedDataKey(1)))
        Else
            ddlIndicador.SelectedValue = 0
        End If
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        Try
            SDSasignaturas.UpdateCommand = "SET dateformat dmy; UPDATE Asignatura SET Consecutivo = @Consecutivo, Descripcion = @Descripcion,IdArea = @IdArea, IdIndicador = @IdIndicador, FechaModif = getdate(), Modifico = @Modifico,Horas=@Horas WHERE IdAsignatura = @IdAsignatura"
            SDSasignaturas.UpdateParameters.Add("Consecutivo", tbConsecutivo.Text.Trim())
            SDSasignaturas.UpdateParameters.Add("IdGrado", DDLgrado.SelectedValue)
            SDSasignaturas.UpdateParameters.Add("Descripcion", TBdescripcion.Text.Trim())
            SDSasignaturas.UpdateParameters.Add("IdArea", DDLarea.SelectedValue)
            SDSasignaturas.UpdateParameters.Add("Modifico", User.Identity.Name)
            SDSasignaturas.UpdateParameters.Add("IdAsignatura", GVasignaturas.SelectedRow.Cells(3).Text.Trim())
            SDSasignaturas.UpdateParameters.Add("Horas", tbAsignatura.Text)
            If Not ddlIndicador.SelectedValue = 0 Then
                SDSasignaturas.UpdateParameters.Add("IdIndicador", ddlIndicador.SelectedValue)
            Else
                SDSasignaturas.UpdateParameters.Add("IdIndicador", Nothing)
            End If
            SDSasignaturas.Update()
            Mensaje.Text = "El registro ha sido actualizado"
            GVasignaturas.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        Try
            SDSasignaturas.DeleteCommand = "Delete from Asignatura where IdAsignatura = " + GVasignaturas.SelectedValue.ToString 'Me da el IdAsignatura porque es el campo clave de la fila seleccionada
            SDSasignaturas.Delete()
            GVasignaturas.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLarea_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLarea.DataBound
        DDLarea.Items.Insert(0, New ListItem("---Elija el Área o Ciencia", 0))
    End Sub

End Class
