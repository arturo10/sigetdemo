﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="Programacion.aspx.vb" 
    Inherits="superadministrador_Programacion" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style11 {
        }

        .style13 {
            width: 205px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 32px;
        }

        .style25 {
            width: 212px;
        }

        .style27 {
            width: 100%;
        }

        .style29 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            width: 392px;
            text-align: right;
            height: 16px;
        }

        .style34 {
            width: 243px;
            height: 16px;
        }

        .style35 {
            height: 16px;
        }

        .style30 {
            width: 392px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
            text-align: center;
        }

        .style28 {
            width: 243px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Carga de un Archivo de Valoración a 
								<asp:Label ID="Label1" runat="server" Text="[PROFESORES]" />
        y
                Programación Académica
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style13">
                <asp:Label ID="Label2" runat="server" Text="[INSTITUCION]" />
            </td>
            <td colspan="2" style="text-align: left;">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    DataSourceID="SDSinstitucion" DataTextField="Descripcion"
                    DataValueField="IdInstitucion" Height="23px" Width="400px">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">
                <asp:Label ID="Label3" runat="server" Text="[CICLO]" />
            </td>
            <td colspan="2" style="text-align: left;">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDScicloescolar" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="23px" Width="400px">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">
                <asp:Label ID="Label4" runat="server" Text="[NIVEL]" />
            </td>
            <td colspan="2" style="text-align: left;">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSnivel" DataTextField="Descripcion" DataValueField="IdNivel"
                    Height="23px" Width="400px">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">
                <asp:Label ID="Label5" runat="server" Text="[GRADO]" />
            </td>
            <td colspan="2" style="text-align: left;">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrado" DataTextField="Descripcion" DataValueField="IdGrado"
                    Height="23px" Width="400px">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">
                <asp:Label ID="Label6" runat="server" Text="[PLANTEL]" />
                donde pertene el 
								<asp:Label ID="Label7" runat="server" Text="[PROFESOR]" />
            </td>
            <td colspan="2" style="text-align: left;">
                <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSplantel" DataTextField="Descripcion"
                    DataValueField="IdPlantel" Height="23px" Width="400px">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">
                <asp:Label ID="Label8" runat="server" Text="[PROFESOR]" />
                asignado</td>
            <td colspan="2" style="text-align: left;">
                <asp:DropDownList ID="DDLprofesor" runat="server" Height="23px" Width="400px"
                    AutoPostBack="True" DataSourceID="SDSprofesor" DataTextField="NombreProf"
                    DataValueField="IdProfesor">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">
                <asp:Label ID="Label9" runat="server" Text="[PLANTEL]" />
                donde está el 
								<asp:Label ID="Label10" runat="server" Text="[GRUPO]" />
            </td>
            <td colspan="2" style="text-align: left;">
                <asp:DropDownList ID="DDLplantelgrupo" runat="server" AutoPostBack="True"
                    DataSourceID="SDSplantelgrupo" DataTextField="Descripcion"
                    DataValueField="IdPlantel" Height="23px" Width="400px">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">
                <asp:Label ID="Label11" runat="server" Text="[ASIGNATURA]" />
            </td>
            <td colspan="2" style="text-align: left;">
                <asp:DropDownList ID="DDLasignatura" runat="server" Height="23px" Width="400px"
                    AutoPostBack="True" DataSourceID="SDSasignatura" DataTextField="Materia"
                    DataValueField="IdAsignatura">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">
                <asp:Label ID="Label12" runat="server" Text="[GRUPO]" />
            </td>
            <td colspan="2" style="text-align: left;">
                <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrupo" DataTextField="Descripcion" DataValueField="IdGrupo"
                    Height="23px" Style="margin-right: 0px" Width="400px">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style11">&nbsp;</td>
            <td>
                <asp:Button ID="BtnInsertar" runat="server" Text="Insertar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
            <td class="style25">&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style11">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td class="style25">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GVprogramacion" runat="server" 
                    AllowPaging="True" 
                    AllowSorting="True"
                    AutoGenerateColumns="False" 
                    DataKeyNames="IdProgramacion"
                    DataSourceID="SDSprogramacion" 
                    Caption="<h3>Listado de GRUPOS asignados al PROFESOR para la ASIGNATURA y CICLO elegidos</h3>"
                    PageSize="20"

                    CellPadding="3" 
                    ForeColor="Black"
                    GridLines="Vertical"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"
                    Width="865px" 
                    BackColor="White" 
                    BorderColor="#999999"
                    BorderStyle="Solid" 
                    BorderWidth="1px">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True">
                            <HeaderStyle Width="65px" />
                            <ItemStyle Width="65px" />
                        </asp:CommandField>
                        <asp:BoundField DataField="IdProgramacion" HeaderText="Id Programación"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdProgramacion">
                            <HeaderStyle Width="30px" />
                            <ItemStyle Width="30px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Apellidos" HeaderText="Apellidos"
                            SortExpression="Apellidos" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre"
                            SortExpression="Nombre" />
                        <asp:BoundField DataField="Asignatura" HeaderText="[Asignatura]"
                            SortExpression="Asignatura" />
                        <asp:BoundField DataField="Area" HeaderText="Area" SortExpression="Area" />
                        <asp:BoundField DataField="Grupo" HeaderText="[Grupo]" SortExpression="Grupo" />
                        <asp:BoundField DataField="Grado" HeaderText="[Grado]" SortExpression="Grado" />
                        <asp:BoundField DataField="Valoración" HeaderText="Valoración"
                            SortExpression="Valoración" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  select
	                1  idprogramacion
	                2  apellidos
	                3  nombre
	                4  ASIGNATURA
	                5  area
	                6  GRUPO
	                7  GRADO
	                8  valoracion
	                --%>
            </td>
        </tr>
        <tr>
            <td class="style11">&nbsp;</td>
            <td class="style11">
                <asp:Button ID="BtnEliminar" runat="server" Text="Eliminar" Visible="False"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
            <td class="style25">&nbsp;</td>
            <td class="style11">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="4">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style11" colspan="4">
                <asp:Panel ID="PnlCargar" runat="server" BackColor="#E3EAEB" Visible="False">
                    <table class="style27">
                        <tr>
                            <td class="style29">Seleccione el archivo que contiene la valoración del 
																<asp:Label ID="Label13" runat="server" Text="[PROFESOR]" />
                                :</td>
                            <td class="style34">
                                <asp:FileUpload ID="RutaArchivo" runat="server" />
                            </td>
                            <td class="style35">
                                <asp:HyperLink ID="HLdocumento" runat="server"
                                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small"
                                    Target="_blank">[HLdocumento]</asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td class="style30">&nbsp;</td>
                            <td class="style28">
                                <asp:Button ID="BtnCargar" runat="server" Text="Cargar"
                                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                &nbsp;
                                <asp:Button ID="BtnQuitar" runat="server" Text="Eliminar"
                                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style11">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td>&nbsp;</td>
            <td class="style25">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSprogramacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Pr.IdProgramacion,P.Apellidos,P.Nombre,A.Descripcion Asignatura,Ar.Descripcion Area, G.Descripcion Grupo, Gr.Descripcion Grado, Pr.ArchivoValoracion as 'Valoración'
from Programacion Pr, Profesor P, Asignatura A, Area Ar, Grupo G, Grado Gr
where Pr.IdCicloEscolar = @IdCicloEscolar and G.IdPlantel = @IdPlantel and Pr.IdProfesor = @IdProfesor and P.IdProfesor = Pr.IdProfesor and P.IdPlantel = G.IdPlantel and Pr.IdGrupo = G.IdGrupo and A.IdGrado = @IdGrado and Pr.IdAsignatura = A.IdAsignatura and A.IdAsignatura = @IdAsignatura and Ar.IdArea = A.IdArea and Gr.IdGrado = A.IdGrado
order by Gr.Descripcion,P.Apellidos,P.Nombre,Ar.Descripcion,A.Descripcion,G.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLprofesor" Name="IdProfesor"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSinstitucion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdInstitucion], [Descripcion] FROM [Institucion] WHERE ([Estatus] = @Estatus) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDScicloescolar" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdCicloEscolar], [Descripcion] FROM [CicloEscolar] WHERE ([Estatus] = @Estatus) ORDER BY [FechaInicio]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSnivel" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] WHERE ([Estatus] = @Estatus) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSgrado" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplantel" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupo" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdGrupo, Descripcion + ' (' + Clave + ')' as Descripcion
from Grupo where IdPlantel = @IdPlantel and
IdGrado = @IdGrado and IdCicloEscolar = @IdCicloEscolar
order by Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantelgrupo" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSprofesor" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdProfesor, Apellidos + ', ' + Nombre + ' (' + Clave + ')' as NombreProf
from Profesor
where IdPlantel = @IdPlantel
order by Apellidos, Nombre">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSasignatura" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select A.IdAsignatura, A.Descripcion + ' (' + Ar.Descripcion + ')' as Materia
from Asignatura A, Area Ar
where A.IdGrado = @IdGrado and Ar.IdArea = A.IdArea
order by Ar.Descripcion, A.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplantelgrupo" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

