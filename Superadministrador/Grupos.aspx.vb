﻿Imports Siget

Partial Class superadministrador_Grupos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GVgruposcreados.Caption = "<h3>" &
            Config.Etiqueta.GRUPOS &
            " cread" & Config.Etiqueta.LETRA_GRUPO & "s</h3>"

        Label1.Text = Config.Etiqueta.GRUPOS
        Label2.Text = Config.Etiqueta.INSTITUCION
        Label3.Text = Config.Etiqueta.PLANTEL
        Label4.Text = Config.Etiqueta.NIVEL
        Label5.Text = Config.Etiqueta.GRADO
        Label6.Text = Config.Etiqueta.CICLO
        Label7.Text = Config.Etiqueta.GRUPO
        Label8.Text = Config.Etiqueta.GRUPO
    End Sub

    Protected Sub SDSniveles_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceSelectingEventArgs) Handles SDSniveles.Selecting
        DDLgrado.ClearSelection()
    End Sub

    Protected Sub Cancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cancelar.Click
        TBdescripcion.Text = ""
        TBClave.Text = ""
        DDLgrado.ClearSelection()
        Mensaje.Text = "Capture los datos"
    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        Try
            SDSgrupos.InsertCommand = "SET dateformat dmy; INSERT INTO Grupo (IdGrado,IdPlantel,IdCicloEscolar,Descripcion, Clave, FechaModif, Modifico) VALUES (" + DDLgrado.SelectedValue + "," + DDLplantel.SelectedValue + "," + DDLcicloescolar.SelectedValue + ", '" + TBdescripcion.Text + "','" + TBClave.Text + "', getdate(), '" & User.Identity.Name & "')"
            SDSgrupos.Insert()
            msgError.hide()
            Mensaje.Text = "El registro ha sido guardado"
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            Mensaje.Text = ""
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_INSTITUCION & " " & Config.Etiqueta.INSTITUCION, 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_GRADO & " " &
            Config.Etiqueta.GRADO & " de " & Config.Etiqueta.ARTDET_GRUPO & " " &
            Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        Try
            SDSgrupos.DeleteCommand = "Delete from Grupo where IdGrupo = " + GVgruposcreados.SelectedValue.ToString 'Me da el IdGrupo porque es el campo que puse como clave de la fila seleccionada en la propiedad DATAKEYNAMES
            SDSgrupos.Delete()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVgruposcreados_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVgruposcreados.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.GRUPO

            LnkHeaderText = e.Row.Cells(3).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.GRUPO

            LnkHeaderText = e.Row.Cells(5).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.GRADO

            LnkHeaderText = e.Row.Cells(6).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.CICLO
        End If
    End Sub

    Protected Sub GVgruposcreados_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVgruposcreados.SelectedIndexChanged
        TBdescripcion.Text = HttpUtility.HtmlDecode(GVgruposcreados.SelectedRow.Cells(3).Text)
        TBClave.Text = HttpUtility.HtmlDecode(GVgruposcreados.SelectedRow.Cells(4).Text)
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        Try
            SDSgrupos.UpdateCommand = "SET dateformat dmy; UPDATE Grupo set Descripcion = '" + TBdescripcion.Text + "',  Clave = '" + TBClave.Text + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdGrupo = " + GVgruposcreados.SelectedValue.ToString 'Me da el IdGrupo porque es el campo que puse como clave de la fila seleccionada en la propiedad DATAKEYNAMES
            SDSgrupos.Update()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija " &
            Config.Etiqueta.ARTIND_CICLO & " " & Config.Etiqueta.CICLO &
            " para " &
            Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO, 0))
    End Sub
End Class
