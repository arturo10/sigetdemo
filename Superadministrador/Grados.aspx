﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="Grados.aspx.vb" 
    Inherits="superadministrador_Grados" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style18 {
            height: 10px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style11 {
            height: 164px;
        }

        .style13 {
        }

        .style19 {
            font-size: x-small;
        }

        .style20 {
            height: 84px;
        }

        .style23 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            color: #000000;
            height: 8px;
            text-align: right;
        }

        .style24 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            color: #000000;
            height: 8px;
            text-align: right;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Administración de
        <asp:Label ID="Label1" runat="server" Text="[GRADOS]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style11">
                <table class="style13">
                    <tr>
                        <td colspan="2" style="text-align: right">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: left">
                            <asp:Label ID="Mensaje" runat="server" CssClass="style16"
                                Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #000066">Capture los datos</asp:Label>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24">
                            <asp:Label ID="Label2" runat="server" Text="[NIVEL]" /></td>
                        <td>
                            <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSniveles" DataTextField="Descripcion"
                                DataValueField="IdNivel" Height="22px" Width="221px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24">Descripción del
                            <asp:Label ID="Label3" runat="server" Text="[GRADO]" /></td>
                        <td>
                            <asp:TextBox ID="TBdescripcion" runat="server" Width="140px"></asp:TextBox>
                            <span class="style19">Ej. Primero, Segundo</span></td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24">Estatus</td>
                        <td>
                            <asp:DropDownList ID="DDLestatus" runat="server" Height="22px" Width="141px">
                                <asp:ListItem>Activo</asp:ListItem>
                                <asp:ListItem>Suspendido</asp:ListItem>
                                <asp:ListItem>Baja</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style23">&nbsp;</td>
                        <td class="style17">
                            <asp:GridView ID="GVgradoscreados" runat="server"
                                AllowPaging="True"
                                AutoGenerateColumns="False"
                                DataKeyNames="IdGrado"
                                DataSourceID="SDSgrados"
                                PageSize="5"
                                CellPadding="3"
                                GridLines="Vertical"
                                CssClass="table table-striped table-bordered"
                                Height="17px"
                                Style="font-size: x-small; text-align: left;">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="IdGrado" HeaderText="Id Grado" InsertVisible="False"
                                        ReadOnly="True" SortExpression="IdGrado" />
                                    <asp:BoundField DataField="IdNivel" HeaderText="IdNivel"
                                        SortExpression="IdNivel" Visible="False" />
                                    <asp:BoundField DataField="Descripcion" HeaderText="Grados creados"
                                        SortExpression="Descripcion" />
                                    <asp:BoundField DataField="FechaModif" HeaderText="FechaModif"
                                        SortExpression="FechaModif" Visible="False" />
                                    <asp:BoundField DataField="Modifico" HeaderText="Modifico"
                                        SortExpression="Modifico" Visible="False" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                            <%-- sort NO - prerender
	                            0  select
	                            1  id_GRADO
	                            2  idnivel
	                            3  GRADOS creados
	                            4  fechamodif
	                            5  modifico
	                            6  estatus
	                            --%>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style23">&nbsp;</td>
                        <td>
                            <asp:Button ID="Insertar" runat="server" Text="Insertar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Eliminar" runat="server" Text="Eliminar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Actualizar" runat="server" Text="Actualizar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Cancelar" runat="server" Text="Cancelar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <uc1:msgError runat="server" ID="msgError" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style20">
                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [IdNivel], [Descripcion], [Estatus], [FechaModif], [Modifico] FROM [Grado] WHERE ([IdNivel] = @IdNivel) ORDER BY [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

