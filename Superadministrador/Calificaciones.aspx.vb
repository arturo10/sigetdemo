﻿Imports Siget

Imports System.Data.SqlClient ' comandos sql

Partial Class superadministrador_Calificaciones
    Inherits System.Web.UI.Page

#Region "Init"

    ' Proceso de inicio de la página
    Protected Sub Page_Load(
                    sender As Object,
                    e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' modifica la visiblidad de la página para usuarios administradores de sistema
        If Roles.IsUserInRole(User.Identity.Name, "sysadmin") Then
            presentaElementosOcultos()
        End If

        ' personaliza las etiquetas de la página
        Label1.Text = Config.Etiqueta.CICLO
        Label2.Text = Config.Etiqueta.NIVEL
        Label3.Text = Config.Etiqueta.GRADO
        Label4.Text = Config.Etiqueta.ASIGNATURA
        Label6.Text = Config.Etiqueta.ASIGNATURA
        Label10.Text = Config.Etiqueta.ASIGNATURA
        Label7.Text = Config.Etiqueta.ASIGNATURAS
    End Sub

    ' se encarga de mostrar todos los elementos que sólo
    ' deben ser visibles a usuarios privilegiados
    Protected Sub presentaElementosOcultos()
        gvCalificaciones.Columns(1).Visible = True
        gvCalificaciones.Columns(7).Visible = True
        gvCalificaciones.Columns(8).Visible = True
    End Sub

#End Region
    ' *********************************************************************************************
#Region "Triggers"

#Region "   ddlCicloEscolar"

    Protected Sub DDLcicloescolar_DataBound(
                    ByVal sender As Object,
                    ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLcicloescolar_SelectedIndexChanged(
                    sender As Object,
                    e As EventArgs) Handles DDLcicloescolar.SelectedIndexChanged
        ' reinicia los avisos de la página
        msgSuccess.hide()
        msgError.hide()
        msgErrorDialog.hide()

        ' al cambiar el ciclo, reinicia los otros controles que dependen de él
        LimpiaCalificacion()
        ' muestra u oculta el panel de calificaciones
        renderPnlCalificaciones()
    End Sub

#End Region
    ' _____________________________________________________________________________________________
#Region "   ddlNivel"

    Protected Sub ddlNivel_DataBound(
                    ByVal sender As Object,
                    ByVal e As System.EventArgs) Handles ddlNivel.DataBound
        ddlNivel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub ddlNivel_SelectedIndexChanged(
                    sender As Object,
                    e As EventArgs) Handles ddlNivel.SelectedIndexChanged
        ' reinicia los avisos de la página
        msgSuccess.hide()
        msgError.hide()
        msgErrorDialog.hide()

        ' al cambiar el nivel, reinicia los otros controles que dependen de él
        LimpiaGrado()
        LimpiaAsignatura()
        LimpiaCalificacion()
        ' y muestra u oculta el panel de calificaciones
        renderPnlCalificaciones()
    End Sub

#End Region
    ' _____________________________________________________________________________________________
#Region "   ddlGrado"

    Protected Sub ddlGrado_DataBound(
                    ByVal sender As Object,
                    ByVal e As System.EventArgs) Handles ddlGrado.DataBound
        ddlGrado.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub ddlGrado_SelectedIndexChanged(
                    sender As Object,
                    e As EventArgs) Handles ddlGrado.SelectedIndexChanged
        ' reinicia los avisos de la página
        msgSuccess.hide()
        msgError.hide()
        msgErrorDialog.hide()

        ' al cambiar de grado, reinicia los controles que dependen de el
        LimpiaAsignatura()
        LimpiaCalificacion()
        ' y muestra u oculta el panel de calificaciones
        renderPnlCalificaciones()
    End Sub

#End Region
    ' _____________________________________________________________________________________________
#Region "   ddlAsignatura"

    Protected Sub ddlAsignatura_DataBound(
                    ByVal sender As Object,
                    ByVal e As System.EventArgs) Handles ddlAsignatura.DataBound
        ddlAsignatura.Items.Insert(0, New ListItem("---Elija una " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub ddlAsignatura_SelectedIndexChanged(
                    sender As Object,
                    e As EventArgs) Handles ddlAsignatura.SelectedIndexChanged
        ' reinicia los avisos de la página
        msgSuccess.hide()
        msgError.hide()
        msgErrorDialog.hide()

        ' al cambiar de asignatura, reinicia los controles que dependen de ella
        LimpiaCalificacion()
        ' y muestra u oculta el panel de calificaciones
        renderPnlCalificaciones()
    End Sub

#End Region
    ' _____________________________________________________________________________________________
#Region "   gvCalificaciones"

    Protected Sub gvCalificaciones_DataBound(
                    sender As Object,
                    e As EventArgs) Handles gvCalificaciones.DataBound
        ' determina si no hay calificaciones para informar al usuario
        evaluaCantidadCalificaciones()
    End Sub

    Protected Sub gvCalificaciones_SelectedIndexChanging(
                    sender As Object,
                    e As GridViewSelectEventArgs) Handles gvCalificaciones.SelectedIndexChanging
        ' revierto la columna de selección de la fila seleccionada
        revertirNombreColumnaSeleccion()

        ' si se seleccionó la misma fila que estaba seleccionada, deselecciónala
        If gvCalificaciones.SelectedIndex = e.NewSelectedIndex Then
            e.NewSelectedIndex = -1
        End If
    End Sub

    Protected Sub gvCalificaciones_SelectedIndexChanged(
                    ByVal sender As Object,
                    ByVal e As System.EventArgs) Handles gvCalificaciones.SelectedIndexChanged
        ' cambio selector "seleccionar" por "deseleccionar" en la fila seleccionada
        cambiarNombreColumnaSeleccion()

        ' si hay una fila seleccionada, muestra los controles de modificación y eliminación
        If Not gvCalificaciones.SelectedIndex = -1 Then
            btnModificar.Visible = True
            btnEliminar.Visible = True
        Else
            btnModificar.Visible = False
            btnEliminar.Visible = False
        End If
    End Sub

    Protected Sub gvCalificaciones_PreRender(
                    sender As Object,
                    e As EventArgs) Handles gvCalificaciones.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If gvCalificaciones.Rows.Count > 0 Then
            gvCalificaciones.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub gvCalificaciones_RowCreated(
                    sender As Object,
                    e As GridViewRowEventArgs) Handles gvCalificaciones.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting
                        ' (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
                        icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If gvCalificaciones.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub gvCalificaciones_RowDataBound(
                    sender As Object,
                    e As GridViewRowEventArgs) Handles gvCalificaciones.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick",
                                          Page.ClientScript.GetPostBackClientHyperlink(
                                              gvCalificaciones,
                                              "Select$" & e.Row.RowIndex
                                              ).ToString()
                                          )
        End If
    End Sub

#End Region
    ' _____________________________________________________________________________________________
#Region "   ddlCalIndicador"

    Protected Sub ddlCalIndicador_DataBound(
                    ByVal sender As Object,
                    ByVal e As System.EventArgs) Handles ddlCalIndicador.DataBound
        ' añado un indicador inválido a la lista de opciones del usuario
        ddlCalIndicador.Items.Insert(0, New ListItem("---Ninguno", 0))
    End Sub

#End Region
    ' _____________________________________________________________________________________________
#Region "   btnAceptarCrear"

    Protected Sub btnAceptarCrear_Click(
                    sender As Object,
                    e As EventArgs) Handles btnAceptarCrear.Click, btnCrearModificar.Click
        ' reinicia los avisos de la página
        msgSuccess.hide()
        msgError.hide()
        msgErrorDialog.hide()

        creaCalificacion() ' intenta crear una calificación
    End Sub

#End Region
    ' _____________________________________________________________________________________________
#Region "   btnAceptarModificar"

    Protected Sub btnAceptarModificar_Click(
                    sender As Object,
                    e As EventArgs) Handles btnAceptarModificar.Click
        ' reinicia los avisos de la página
        msgSuccess.hide()
        msgError.hide()
        msgErrorDialog.hide()

        modificaCalificacion() ' intenta modificar la calificación seleccionada
    End Sub

#End Region
    ' _____________________________________________________________________________________________
#Region "   btnEliminar"

    Protected Sub btnEliminar_Click(
                    sender As Object,
                    e As EventArgs) Handles btnEliminar.Click
        ' reinicia los avisos de la página
        msgSuccess.hide()
        msgError.hide()
        msgErrorDialog.hide()

        eliminaCalificacion() ' intenta eliminar la calificación seleccionada
    End Sub

#End Region
    ' _____________________________________________________________________________________________
#Region "   btnCancelar"

    Protected Sub btnCancelar_Click(
                    sender As Object,
                    e As EventArgs) Handles btnCancelar.Click
        ' el popup se esconde por su cuenta con el postback, pero aún así
        ' pongo la instrucción por claridad
        ModalPopupCalificacion.Hide()

        msgErrorDialog.hide()
    End Sub

#End Region
    ' _____________________________________________________________________________________________
#Region "   btnNueva"

    Protected Sub btnNueva_Click(
                    sender As Object,
                    e As EventArgs) Handles btnNueva.Click
        ModalPopupCalificacion.Show() ' muestra el diálogo de campos
        limpiaDatosCalificaciones() ' limpia los campos del diálogo

        ' muestra el botón de confirmación de modificar
        ' y esconde el de confirmación de creación en el diálogo
        btnAceptarCrear.Visible = True
        btnAceptarModificar.Visible = False
        btnCrearModificar.Visible = False
    End Sub

#End Region
    ' _____________________________________________________________________________________________
#Region "   btnModificar"

    Protected Sub btnModificar_Click(
                    sender As Object,
                    e As EventArgs) Handles btnModificar.Click
        ModalPopupCalificacion.Show() ' muestra el diálogo de campos

        ' llena los datos con la información de la calificación seleccionada
        llenaDatosCalificaciones()

        ' muestra el botón de confirmación de modificar
        ' y esconde el de confirmación de creación en el diálogo
        btnAceptarCrear.Visible = False
        btnAceptarModificar.Visible = True
        btnCrearModificar.Visible = True
    End Sub

#End Region
    ' _____________________________________________________________________________________________
#Region "   msgErrorDialog"

    Protected Sub msgErrorDialog_MessageClosed(
                    sender As Object,
                    e As EventArgs) Handles msgErrorDialog.MessageClosed
        ' si se cierra el mensaje de error en el diálogo, no ocultes el diálogo
        ModalPopupCalificacion.Show()
    End Sub

#End Region

#End Region
    ' *********************************************************************************************
#Region "Métodos Gráficos"

    ''' <summary>
    ''' muestra o esconde el panel de calificaciones 
    ''' dependiendo de la selección de asignaturas
    ''' </summary>
    Protected Sub renderPnlCalificaciones()
        If ddlAsignatura.SelectedValue = 0 Then
            pnlCalificaciones.Visible = False
        Else
            pnlCalificaciones.Visible = True
        End If
    End Sub


    ''' <summary>
    ''' cuenta la cantidad de calificaciones mostradas;
    ''' si no hay, notifica al usuario
    ''' </summary>
    Protected Sub evaluaCantidadCalificaciones()
        If gvCalificaciones.Rows.Count = 0 AndAlso Not ddlAsignatura.SelectedIndex = 0 Then
            msgInfo.show("Esta " & Config.Etiqueta.ASIGNATURA & " aún no tiene calificaciones")
        Else
            msgInfo.hide()
        End If
    End Sub

    ''' <summary>
    ''' reestablece el texto de la columna de selección de la actual fila seleccionada
    ''' </summary>
    Protected Sub revertirNombreColumnaSeleccion()
        If Not gvCalificaciones.SelectedIndex = -1 Then
            CType(gvCalificaciones.SelectedRow.Cells(0).Controls(0), LinkButton).Text = "Seleccionar"
        End If
    End Sub

    ''' <summary>
    ''' modifica el texto de la columna de selección de la actual fila seleccionada
    ''' </summary>
    Protected Sub cambiarNombreColumnaSeleccion()
        If Not gvCalificaciones.SelectedIndex = -1 Then
            CType(gvCalificaciones.SelectedRow.Cells(0).Controls(0), LinkButton).Text = "Deseleccionar"
        End If
    End Sub

    ''' <summary>
    ''' deselecciona el grado
    ''' </summary>
    Protected Sub LimpiaGrado()
        ddlGrado.SelectedIndex = -1
    End Sub

    ''' <summary>
    ''' deselecciona la asignatura
    ''' </summary>
    Protected Sub LimpiaAsignatura()
        ddlAsignatura.SelectedIndex = -1
    End Sub

    ''' <summary>
    ''' deselecciona la calificación
    ''' </summary>
    Protected Sub LimpiaCalificacion()
        gvCalificaciones.SelectedIndex = -1
        gvCalificaciones_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    ''' <summary>
    ''' limpia los campos de creación de calificaciones
    ''' </summary>
    Protected Sub limpiaDatosCalificaciones()
        tbCaldescripcion.Text = ""
        tbCalconsecutivo.Text = ""
        tbCalClave.Text = ""
        tbCalValor.Text = ""
        ddlCalIndicador.SelectedValue = 0
    End Sub

    ''' <summary>
    ''' obtiene los datos de la calificación seleccionada y 
    ''' llena los campos del formato de creación/modificación con ellos
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub llenaDatosCalificaciones()
        'GVevaluaciones.DataBind() no se ocupa porque ya lo puse en un evento
        'Los campos deben estar con visible=true para poder leer el dato con SelectedRow
        ' (el numero depende en como se acomodaron en el GridView en Colección de columnas)
        tbCaldescripcion.Text = HttpUtility.HtmlDecode(gvCalificaciones.SelectedRow.Cells(2).Text).Trim()
        tbCalClave.Text = HttpUtility.HtmlDecode(gvCalificaciones.SelectedRow.Cells(3).Text).Trim()
        tbCalconsecutivo.Text = HttpUtility.HtmlDecode(gvCalificaciones.SelectedRow.Cells(4).Text).Trim()
        If IsNumeric(gvCalificaciones.SelectedRow.Cells(5).Text) Then
            tbCalValor.Text = gvCalificaciones.SelectedRow.Cells(5).Text.Trim()
        Else
            tbCalValor.Text = ""
        End If
        If Not IsDBNull(gvCalificaciones.SelectedDataKey.Values(1)) Then
            ddlCalIndicador.SelectedValue = gvCalificaciones.SelectedDataKey.Values(1).ToString()
        Else
            ddlCalIndicador.SelectedIndex = 0
        End If
    End Sub

#End Region
    ' *********************************************************************************************
#Region "Métodos de proceso"

    ''' <summary>
    ''' Verifica e indica si los campos de creación/modificación 
    ''' son válidos para crear una calificación 
    ''' </summary>
    ''' <returns>
    ''' cierto si son válidos, falso de lo contrario
    ''' </returns>
    Protected Function verificaCamposCalificacion() As Boolean
        If Trim(tbCalValor.Text) = "" Then
            tbCalValor.Text = 0
        End If

        If tbCaldescripcion.Text.Trim() = String.Empty Then
            msgErrorDialog.show("La calificación debe tener una descripción.")
            ModalPopupCalificacion.Show()
            Return False
        End If

        If tbCalClave.Text.Trim() = String.Empty Then
            msgErrorDialog.show("La calificación debe tener una clave para reportes.")
            ModalPopupCalificacion.Show()
            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' Intenta crear una calificación con el formato de creación/modificación
    ''' </summary>
    Protected Sub creaCalificacion()
        If verificaCamposCalificacion() Then
            Try
                SDScalificaciones.InsertParameters.Clear() ' para múltiples intentos esto es importante
                SDScalificaciones.InsertCommand =
"SET dateformat dmy; " &
"INSERT INTO Calificacion (IdCicloEscolar, Descripcion, Clave, Consecutivo, " &
"                           IdAsignatura, Valor, IdIndicador, FechaModif, Modifico) " &
"VALUES (@IdCicloEscolar, @Descripcion, @Clave, @Consecutivo, " &
"                           @IdAsignatura, @Valor, @IdIndicador, getdate(), @Modifico) "
                SDScalificaciones.InsertParameters.Add("IdCicloEscolar", DDLcicloescolar.SelectedValue)
                SDScalificaciones.InsertParameters.Add("Descripcion", tbCaldescripcion.Text.Trim())
                SDScalificaciones.InsertParameters.Add("Clave", tbCalClave.Text.Trim())
                SDScalificaciones.InsertParameters.Add("Consecutivo", tbCalconsecutivo.Text.Trim())
                SDScalificaciones.InsertParameters.Add("IdAsignatura", ddlAsignatura.SelectedValue)
                SDScalificaciones.InsertParameters.Add("Valor", tbCalValor.Text.Trim())
                If ddlCalIndicador.SelectedIndex > 0 Then
                    SDScalificaciones.InsertParameters.Add("IdIndicador", ddlCalIndicador.SelectedValue)
                Else
                    SDScalificaciones.InsertParameters.Add("IdIndicador", Nothing)
                End If
                SDScalificaciones.InsertParameters.Add("Modifico", User.Identity.Name)


                SDScalificaciones.Insert()

                ' deselecciona la fila de la tabla
                gvCalificaciones.SelectedIndex = -1
                gvCalificaciones_SelectedIndexChanged(Nothing, Nothing)

                gvCalificaciones.DataBind()

                msgSuccess.show("Éxito", "Se creó la calificación.")
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgErrorDialog.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")),
                                    "Ocurrió un error desconocido. Si el problema persiste " &
                                    "notifique al administrador del sistema.")
                ModalPopupCalificacion.Show()
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Intenta modificar una calificación seleccionada con el formato de creación/modificación
    ''' </summary>
    Protected Sub modificaCalificacion()
        If verificaCamposCalificacion() Then
            Try
                SDScalificaciones.UpdateParameters.Clear() ' para múltiples intentos esto es importante
                SDScalificaciones.UpdateCommand =
"SET dateformat dmy; " &
"UPDATE Calificacion " &
"SET IdCicloEscolar = @IdCicloEscolar, Descripcion = @Descripcion, Clave = @Clave, " &
"       Consecutivo = @Consecutivo, IdAsignatura = @IdAsignatura, Valor = @Valor, " &
"       IdIndicador = @IdIndicador, FechaModif = getdate(), Modifico = @Modifico " &
"WHERE IdCalificacion = @IdCalificacion"
                SDScalificaciones.UpdateParameters.Add("IdCicloEscolar", DDLcicloescolar.SelectedValue)
                SDScalificaciones.UpdateParameters.Add("Descripcion", tbCaldescripcion.Text.Trim())
                SDScalificaciones.UpdateParameters.Add("Clave", tbCalClave.Text.Trim())
                SDScalificaciones.UpdateParameters.Add("Consecutivo", tbCalconsecutivo.Text.Trim())
                SDScalificaciones.UpdateParameters.Add("IdAsignatura", ddlAsignatura.SelectedValue)
                SDScalificaciones.UpdateParameters.Add("Valor", tbCalValor.Text.Trim())
                If ddlCalIndicador.SelectedIndex > 0 Then
                    SDScalificaciones.UpdateParameters.Add("IdIndicador", ddlCalIndicador.SelectedValue)
                Else
                    SDScalificaciones.UpdateParameters.Add("IdIndicador", Nothing)
                End If
                SDScalificaciones.UpdateParameters.Add("Modifico", User.Identity.Name)
                SDScalificaciones.UpdateParameters.Add("IdCalificacion", gvCalificaciones.SelectedDataKey(0).ToString())

                SDScalificaciones.Update()

                gvCalificaciones.DataBind()
                msgSuccess.show("Éxito", "Se modificó la calificación.")
                ' cambio selector "seleccionar" por "deseleccionar" en la fila seleccionada
                cambiarNombreColumnaSeleccion()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgErrorDialog.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")),
                                    "Ocurrió un error desconocido. Si el problema persiste " &
                                    "notifique al administrador del sistema.")
                ModalPopupCalificacion.Show()
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Intenta eliminar la calificación seleccionada
    ''' </summary>
    Protected Sub eliminaCalificacion()
        Try
            SDScalificaciones.DeleteParameters.Clear() ' para múltiples intentos esto es importante
            SDScalificaciones.DeleteCommand = "Delete from Calificacion WHERE IdCalificacion = @IdCalificacion"
            SDScalificaciones.DeleteParameters.Add("IdCalificacion", gvCalificaciones.SelectedDataKey(0).ToString())

            SDScalificaciones.Delete()

            ' deselecciona la fila de la tabla
            gvCalificaciones.SelectedIndex = -1
            gvCalificaciones_SelectedIndexChanged(Nothing, Nothing)

            gvCalificaciones.DataBind()

            msgSuccess.show("Éxito", "Se eliminó la calificación.")
        Catch sqlex As SqlException When sqlex.Number = 547
            Utils.LogManager.ExceptionLog_InsertEntry(sqlex) ' registra el error
            msgError.show("No se puede borrar la calificación puesto que tiene evaluaciones asignadas.")
        Catch ex As SqlException
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgErrorDialog.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")),
                                "Ocurrió un error desconocido. Si el problema persiste " &
                                "notifique al administrador del sistema.")
        End Try
    End Sub

#End Region
    ' *********************************************************************************************
#Region "Página"

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In gvCalificaciones.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(
                                                             gvCalificaciones,
                                                             "Select$" + r.RowIndex.ToString()
                                                             ))

            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

#End Region

End Class
