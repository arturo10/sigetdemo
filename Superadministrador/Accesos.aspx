﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Principal.master" AutoEventWireup="true" CodeFile="Accesos.aspx.cs" Inherits="Superadministrador_AccesosInfo" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" Runat="Server">

    <div class="bleeding center">
        <div class="panel">
            <label class="label-control lb-lg ">Selecciona el perfil del Sistema</label>
            <asp:DropDownList ID="DDLPerfilSelected" CssClass="gw" AutoPostBack="true" runat="server" 
                OnSelectedIndexChanged="DDLPerfilSelected_SelectedIndexChanged">
                <asp:ListItem Value="Alumno">Alumno</asp:ListItem>
                <asp:ListItem Value="Administrador">Administrador</asp:ListItem>
                <asp:ListItem Value="Profesor">Profesor</asp:ListItem>
                <asp:ListItem Value="Operador">Operador</asp:ListItem>
                <asp:ListItem Value="Coordinador">Coordinador</asp:ListItem>
             </asp:DropDownList>
            </div>
     </div>

    <asp:GridView ID="GVAccesos" runat="server"
                                AllowSorting="True"
                                AutoGenerateColumns="False"
                                DataSourceID="SDSAlumno"
                                Width="857px"
                                Visible="false"
                                CssClass="dataGrid_clear_selectable"
                                GridLines="None">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                                    <asp:BoundField DataField="Username" HeaderText="Login"
                                        InsertVisible="False" ReadOnly="True" SortExpression="Username" />
                                    <asp:BoundField DataField="Nombre_Completo" HeaderText="Nombre Completo"
                                        SortExpression="Nombre_Completo" />
                                    <asp:BoundField DataField="UltimoAcceso" HeaderText="Ultimo Acceso"
                                        SortExpression="UltimoAcceso" />
                                </Columns>
                                <FooterStyle CssClass="footer" />
                                <PagerStyle CssClass="pager" />
                                <SelectedRowStyle CssClass="selected" />
                                <HeaderStyle CssClass="header" />
                                <AlternatingRowStyle CssClass="altrow" />
                            </asp:GridView>

     <asp:GridView ID="GVAccesosAlumno" runat="server"
                                AllowSorting="True"
                                AutoGenerateColumns="False"
                                DataSourceID="SDSAlumno"
                                Width="857px"
                                
                                CssClass="dataGrid_clear_selectable"
                                GridLines="None">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                                    <asp:BoundField DataField="Username" HeaderText="Login"
                                        InsertVisible="False" ReadOnly="True" SortExpression="Username" />
                                    <asp:BoundField DataField="Plantel" HeaderText="Plantel"
                                        SortExpression="Plantel" />
                                    <asp:BoundField DataField="Grupo" HeaderText="Grupo"
                                        SortExpression="GRupo" />
                                    <asp:BoundField DataField="Nombre_Completo" HeaderText="Nombre Completo"
                                        SortExpression="Nombre_Completo" />
                                    <asp:BoundField DataField="UltimoAcceso" HeaderText="Ultimo Acceso"
                                        SortExpression="UltimoAcceso" />
                                </Columns>
                                <FooterStyle CssClass="footer" />
                                <PagerStyle CssClass="pager" />
                                <SelectedRowStyle CssClass="selected" />
                                <HeaderStyle CssClass="header" />
                                <AlternatingRowStyle CssClass="altrow" />
                            </asp:GridView>

             <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide" style="margin-top:20px;"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
             
            <asp:Button ID="BtnExportar" runat="server" style="margin-top:20px;" Text="Exportar a Excel"
                    CssClass="defaultBtn btnThemeGreen btnThemeMedium" OnClick="BtnExportar_Click" />

            <uc1:msgError runat="server" ID="msgError" />

    <asp:SqlDataSource ID="SDSAdmin" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT U.Login AS Username, (A.Apellidos+ SPACE(1)+A.Nombre)AS Nombre_Completo,
                                    (ASP.LastActivityDate-0.20833) AS UltimoAcceso 
                                    FROM Usuario U 
	                                INNER JOIN Admin A  ON A.IdUsuario=U.IdUsuario
	                                INNER JOIN aspnet_Users ASP ON ASP.UserName=U.Login
                                    ORDER BY A.Apellidos, A.Nombre">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SDSAlumno" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT  Pla.Descripcion AS Plantel,G.Descripcion AS Grupo, U.Login AS Username,
	                                (A.ApePaterno+ SPACE(1)+ A.ApeMaterno+ SPACE(1) +A.Nombre)AS Nombre_Completo,
	                                (ASP.LastActivityDate-0.20833) AS UltimoAcceso
                                   FROM Usuario U 
	                             INNER JOIN Alumno A ON A.IdUsuario=U.IdUsuario
	                             INNER JOIN aspnet_Users ASP ON ASP.UserName=U.Login
                                 INNER JOIN Plantel Pla ON A.IdPlantel=Pla.IdPlantel
								 INNER JOIN Grupo G ON A.IdGrupo=G.IdGrupo
                                  ORDER BY A.ApePaterno, A.ApeMaterno,A.Nombre">
    </asp:SqlDataSource>

         <asp:SqlDataSource ID="SDSCoordinador" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT U.Login AS Username,
	                                (C.Apellidos+ SPACE(1)+C.Nombre)AS Nombre_Completo,
	                                (ASP.LastActivityDate-0.20833) AS UltimoAcceso
                                    FROM Usuario U 
	                                INNER JOIN Coordinador C  ON C.IdUsuario=U.IdUsuario
	                                INNER JOIN aspnet_Users ASP ON ASP.UserName=U.Login
                                    ORDER BY C.Apellidos, C.Nombre">
    </asp:SqlDataSource>

      <asp:SqlDataSource ID="SDSProfesor" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT U.Login AS Username,
	                                (P.Apellidos+ SPACE(1)+P.Nombre)AS Nombre_Completo,
	                                (ASP.LastActivityDate-0.20833) AS UltimoAcceso
                                    FROM Usuario U 
	                                INNER JOIN Profesor P  ON P.IdUsuario=U.IdUsuario
	                                INNER JOIN aspnet_Users ASP ON ASP.UserName=U.Login
                                    ORDER BY P.Apellidos, P.Nombre">
    </asp:SqlDataSource>

     <asp:SqlDataSource ID="SDSOperador" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT U.Login AS Username,
	                                (O.Apellidos+ SPACE(1)+O.Nombre)AS Nombre_Completo,
	                                (ASP.LastActivityDate-0.20833) AS UltimoAcceso
                                    FROM  Usuario U 
	                                INNER JOIN Operador O  ON O.IdUsuario=U.IdUsuario
	                                INNER JOIN aspnet_Users ASP ON ASP.UserName=U.Login
                                    ORDER BY O.Apellidos, O.Nombre">
    </asp:SqlDataSource>

</asp:Content>

