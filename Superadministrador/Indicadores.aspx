﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="Indicadores.aspx.vb" 
    Inherits="coordinador_Indicadores" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
    .style11
    {
        width: 100%;
    }
    .style13
    {
            width: 250px;
            text-align: right;
        }
    .style14
    {
        width: 336px;
    }
        .style15
        {
            width: 104px;
        }
        .style16
        {
            color: #0000FF;
        }
        .style17
        {
            color: #00CC00;
        }
        .style18
        {
            color: #D9D900;
        }
        .style19
        {
            width: 223px;
            text-align: right;
            font-weight: bold;
        }
        .style20
        {
            color: #FF0000;
        }
        .style21
        {
            width: 162px;
        }
        .style23
        {
            color: #009900;
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            height: 8px;
        }
        .style24
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
        .style25
        {
            width: 250px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }
        .style27
        {
            width: 223px;
            text-align: right;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
        }
        .style28
        {
            color: #FFFFFF;
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            height: 8px;
        }
        .style29
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 36px;
        }
        .style30
        {
            width: 162px;
            font-family: Arial, Helvetica, sans-serif;
            text-decoration: underline;
            font-weight: bold;
            text-align: center;
        }
        .style32
        {
            width: 234px;
            text-decoration: underline;
            font-weight: bold;
            text-align: center;
        }
        .style34
        {
            color: #000000;
            height: 8px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }
        .style35
        {
            color: #000000;
            height: 8px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
            font-weight: bold;
        }
        .style36
        {
            width: 250px;
            text-align: right;
            font-weight: bold;
            text-decoration: underline;
        }
        .style37
        {
            width: 234px;
            text-align: center;
        }
        .style38
        {
            width: 162px;
            text-align: center;
        }
        .style39
        {
            width: 223px;
            text-align: right;
            font-weight: bold;
            font-family: Arial, Helvetica, sans-serif;
            height: 42px;
        }
        .style40
        {
            width: 162px;
            text-align: center;
            height: 42px;
        }
        .style41
        {
            width: 234px;
            text-align: center;
            height: 42px;
        }
        .style42
        {
            width: 223px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }
        .style43
        {
            width: 223px;
            text-align: center;
            font-weight: bold;
            text-decoration: underline;
            height: 38px;
        }
        .style44
        {
            color: #000000;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }
    		.auto-style1 {
						width: 248px;
						text-align: center;
				}
				.auto-style2 {
						width: 248px;
						text-decoration: underline;
						font-weight: bold;
						text-align: center;
				}
				.auto-style3 {
						width: 248px;
						text-align: center;
						height: 42px;
				}
				.auto-style4 {
						width: 204px;
				}
				.auto-style5 {
						width: 204px;
						font-family: Arial, Helvetica, sans-serif;
						text-decoration: underline;
						font-weight: bold;
						text-align: center;
				}
				.auto-style6 {
						width: 204px;
						text-align: center;
				}
				.auto-style7 {
						width: 204px;
						text-align: center;
						height: 42px;
				}
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Administración de Indicadores para las Evaluaciones
		</h1>
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style11">
        <tr>
            <td colspan="4">
                                <asp:Label ID="Mensaje" runat="server" CssClass="style24" 
                                    style="color: #000099">Capture los datos</asp:Label>
                            </td>
        </tr>
        <tr>
            <td colspan="4">
                <table class="estandar">
                    <tr>
                        <td class="style42">
                            Descripción del Indicador</td>
                        <td class="style14" colspan="2">
                            <asp:TextBox ID="TBdescripcion" runat="server" MaxLength="25" Width="293px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style19">
                            &nbsp;</td>
                        <td class="auto-style4">
                            &nbsp;</td>
                        <td class="auto-style1">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style43" bgcolor="#CCCCCC">
                            Colorímetrica general (sobre 100):</td>
                        <td class="auto-style5" bgcolor="#CCCCCC">
                            Rangos para Promedio:</td>
                        <td class="auto-style2" bgcolor="#CCCCCC">
                            Rangos para Porcentaje de Avance:</span></td>
                    </tr>
                    <tr>
                        <td class="style27">
                            <span class="style16">Mínima Calificación para Azul </span>
                            <br class="style16" />
                            <span class="style16">(Excelente, meta sobrepasada)</span></td>
                        <td class="auto-style6">
                            <asp:TextBox ID="TBazul" runat="server" Width="60px"></asp:TextBox>
                        </td>
                        <td class="auto-style1">
                            <asp:TextBox ID="TBazulPorc" runat="server" Width="60px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style39">
                            <span class="style23">Mínima Calificación para Verde
                            </span>
                            <br class="style23" />
                            <span class="style23">(bien, aceptable, meta alcanzada</span><span 
                                class="style28">)</span></td>
                        <td class="auto-style7">
                            <asp:TextBox ID="TBverde" runat="server" Width="60px"></asp:TextBox>
                        </td>
                        <td class="auto-style3">
                            <asp:TextBox ID="TBverdePorc" runat="server" Width="60px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style27">
                            <span class="style18">Mínima Calificación para Amarillo
                            </span>
                            <br class="style18" />
                            <span class="style18">(se requiere revisión y corregir)</span></td>
                        <td class="auto-style6">
                            <asp:TextBox ID="TBamarillo" runat="server" Width="60px"></asp:TextBox>
                        </td>
                        <td class="auto-style1">
                            <asp:TextBox ID="TBamarilloPorc" runat="server" Width="60px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style27">
                            <span class="style20">Mínima Calificación para Rojo
                            </span>
                            <br class="style20" />
                            <span class="style20">(Mal, inaceptable)</span></td>
                        <td class="auto-style6">
                            <asp:TextBox ID="TBrojo" runat="server" Width="60px"></asp:TextBox>
                        </td>
                        <td class="auto-style1">
                            <asp:TextBox ID="TBrojoPorc" runat="server" Width="60px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style13">
                            &nbsp;</td>
                        <td class="style14" colspan="2">
                                <asp:Button ID="Insertar" runat="server" Text="Insertar" 
																		CssClass="defaultBtn btnThemeBlue btnThemeMedium"/>
														&nbsp;
                                <asp:Button ID="Limpiar" runat="server" Text="Limpiar" 
																		CssClass="defaultBtn btnThemeGrey btnThemeMedium"/>
														&nbsp;
                                <asp:Button ID="Actualizar" runat="server" Text="Actualizar" 
																		CssClass="defaultBtn btnThemeBlue btnThemeMedium"/>
														&nbsp;
                                <asp:Button ID="Eliminar" runat="server" Text="Eliminar" 
																		CssClass="defaultBtn btnThemeGrey btnThemeMedium"/>
                            </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GVindicadores" runat="server" AllowPaging="True" 
                    AllowSorting="True" AutoGenerateColumns="False" 
                    BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" 
                    CellPadding="3" DataKeyNames="IdIndicador" DataSourceID="SDSindicadores" 
                    GridLines="Vertical" PageSize="5" 
                    style="font-family: Arial, Helvetica, sans-serif; font-size: x-small" 
                    Width="846px" ForeColor="Black" 
                    Caption="<h3>Indicadores creados</h3>">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="IdIndicador" HeaderText="IdIndicador" 
                            InsertVisible="False" ReadOnly="True" SortExpression="IdIndicador" 
                            Visible="False" />
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripción" 
                            SortExpression="Descripcion" />
                        <asp:BoundField DataField="MinRojo" HeaderText="Min. Rojo Prom." 
                            SortExpression="MinRojo" />
                        <asp:BoundField DataField="MinAmarillo" HeaderText="Min. Amarillo Prom." 
                            SortExpression="MinAmarillo" />
                        <asp:BoundField DataField="MinVerde" HeaderText="Min. Verde Prom." 
                            SortExpression="MinVerde" />
                        <asp:BoundField DataField="MinAzul" HeaderText="Min. Azul Prom." 
                            SortExpression="MinAzul" />
                        <asp:BoundField DataField="MinRojoPorc" HeaderText="Min. Rojo % Av." 
                            SortExpression="MinRojoPorc" />
                        <asp:BoundField DataField="MinAmarilloPorc" HeaderText="Min. Amarillo % Av." 
                            SortExpression="MinAmarilloPorc" />
                        <asp:BoundField DataField="MinVerdePorc" HeaderText="Min. Verde % Av." 
                            SortExpression="MinVerdePorc" />
                        <asp:BoundField DataField="MinAzulPorc" HeaderText="Min. Azul % Av." 
                            SortExpression="MinAzulPorc" />
                        <asp:BoundField DataField="FechaModif" HeaderText="FechaModif" 
                            SortExpression="FechaModif" Visible="False" />
                        <asp:BoundField DataField="Modifico" HeaderText="Modifico" 
                            SortExpression="Modifico" Visible="False" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="style15">
            </td>
            <td>
                <asp:SqlDataSource ID="SDSindicadores" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [Indicador] ORDER BY [FechaModif] DESC"></asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSrangos" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [Rango] WHERE ([IdIndicador] = @IdIndicador) ORDER BY [Inferior]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVindicadores" Name="IdIndicador" 
                            PropertyName="SelectedValue" Type="Int16" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <table class="style11">
                    <tr>
                        <td class="style29" colspan="4">
                            Opcionalmente puede introducir rangos adicionales para clasificar Calificaciones 
                            de manera específica. Si una Calificación no tiene asignado un Indicador, tomará 
                            la colorímetría general.</td>
                    </tr>
                    <tr>
                        <td class="style44">
                            <span class="estandar" style="text-align: right">Valor Inferior</span></td>
                        <td colspan="3">
                            <asp:TextBox ID="TBinferior" runat="server" Width="90px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style44">
                            <span class="estandar">Valor Superior</span></td>
                        <td colspan="3">
                            <asp:TextBox ID="TBsuperior" runat="server" Width="90px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style44">
                            <span class="estandar">Color para el Rango</span></td>
                        <td>
                            <asp:TextBox ID="TBcolor" runat="server" Width="90px"></asp:TextBox>
                            <asp:ColorPickerExtender ID="TBcolor_ColorPickerExtender" runat="server" 
                                Enabled="True" TargetControlID="TBcolor">
                            </asp:ColorPickerExtender>
                        </td>
                        <td>
                            <asp:Panel ID="PnlColor" runat="server" Height="29px" Width="70px">
                            </asp:Panel>
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style44">
                            <span class="estandar">Categoría</span></td>
                        <td colspan="3">
                            <asp:TextBox ID="TBcategoria" runat="server" MaxLength="20" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="style17">
                            &nbsp;</td>
                        <td colspan="3">
                            <asp:Button ID="BtnInsertar" runat="server" Text="Insertar" 
																CssClass="defaultBtn btnThemeBlue btnThemeMedium"/>
														&nbsp;
                            <asp:Button ID="BtnActualizar" runat="server" Text="Actualizar" 
																CssClass="defaultBtn btnThemeBlue btnThemeMedium"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="style44" colspan="4">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style15" colspan="4">
                <asp:GridView ID="GVrangos" runat="server" 
                    AllowSorting="True" AutoGenerateColumns="False" 
                    BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" 
                    CellPadding="3" DataKeyNames="IdRango" DataSourceID="SDSrangos" 
                    GridLines="Vertical" PageSize="5" 
                    style="font-family: Arial, Helvetica, sans-serif; font-size: small" 
                    Width="648px" ForeColor="Black" 
                    
                                
                                Caption="<h3>Indicadores creados</h3>">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="IdRango" HeaderText="IdRango" InsertVisible="False" 
                            ReadOnly="True" SortExpression="IdRango" />
                        <asp:BoundField DataField="IdIndicador" HeaderText="IdIndicador" 
                            SortExpression="IdIndicador" Visible="False" />
                        <asp:BoundField DataField="Inferior" HeaderText="Inferior" 
                            SortExpression="Inferior" />
                        <asp:BoundField DataField="Superior" HeaderText="Superior" 
                            SortExpression="Superior" />
                        <asp:BoundField DataField="Categoria" HeaderText="Categoria" 
                            SortExpression="Categoria" />
                        <asp:BoundField DataField="Color" HeaderText="Color" SortExpression="Color" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="style17">
                            &nbsp;</td>
                        <td colspan="3">
                            <asp:Button ID="BtnEliminar" runat="server" Text="Eliminar" Visible="False" 
                               CssClass="defaultBtn btnThemeGrey btnThemeMedium"/>
                        </td>
                    </tr>
                </table>
                </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:HyperLink ID="HyperLink2" runat="server"
					NavigateUrl="~/" 
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
                </td>
        </tr>
    </table>
</asp:Content>

