﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="RegistroAlumnos.aspx.vb" 
    Inherits="superadministrador_RegistroAlumnos" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 107%;
            height: 450px;
        }

        .style11 {
            width: 38%;
        }

        .style15 {
            text-align: right;
            width: 89px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
        }

        .style12 {
            text-align: left;
        }

        .style18 {
            text-align: left;
        }

        .style13 {
            width: 100%;
            height: 1px;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            text-align: left;
        }

        .style24 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style26 {
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
            font-size: small;
            font-weight: normal;
        }

        .style27 {
            text-align: left;
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
        }

        .style17 {
            text-align: left;
        }

        .style37 {
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
            font-size: small;
            font-weight: normal;
            width: 47px;
        }

        .style38 {
            height: 35px;
        }

        .style39 {
            width: 66px;
        }

        .style40 {
            text-align: right;
            width: 89px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            height: 20px;
        }

        .style41 {
            width: 66px;
            height: 20px;
        }

        .style42 {
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
            font-size: small;
            font-weight: normal;
            width: 47px;
            height: 20px;
        }

        .style43 {
            text-align: left;
            height: 20px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Registro de 
								<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td>
                <table class="style11">
                    <tr>
                        <td colspan="5" class="style27">* Si no se seleccióna la casilla &quot;Registrar Usuario&quot;, solo 
                            se afectan las tablas de Alumnos y Usuario, después 
                            el Alumno deberá ingresar a generar su 
                            usuario en las tablas de seguridad desde la liga &quot;Registrarme&quot;<br />
                            * Si desea modificar los datos de algún Alumno, entre a la opción PLANTILLA --&gt; 
                            Alumno--&gt;ADMINISTRAR</td>
                    </tr>
                    <tr>
                        <td colspan="5" class="style38" style="text-align: left">
                            <asp:Label ID="Mensaje" runat="server"
                                Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #000099"
                                Text="Capture los datos"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">
                            <asp:Label ID="Label2" runat="server" Text="INSTITICION" />
                            :</td>
                        <td style="text-align: left" class="style39">
                            <asp:DropDownList ID="DDLinstitucion"
                                runat="server" AutoPostBack="True" DataSourceID="SDSinstituciones"
                                DataTextField="Descripcion" DataValueField="IdInstitucion"
                                Width="350px" Height="22px">
                            </asp:DropDownList>
                        </td>
                        <td class="style37">
                            <asp:Label ID="Label3" runat="server" Text="[PLANTEL]" />
                            :</td>
                        <td class="style17" colspan="2">
                            <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                DataValueField="IdPlantel" Width="380px" Height="22px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style40">
                            <asp:Label ID="Label4" runat="server" Text="[NIVEL]" />
                            :</td>
                        <td style="text-align: left" class="style41">
                            <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                                Width="350px" Height="22px">
                            </asp:DropDownList>
                        </td>
                        <td class="style42">
                            <asp:Label ID="Label5" runat="server" Text="[GRADO]" />:</td>
                        <td class="style43" colspan="2">
                            <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                DataSourceID="SDSgrados" DataTextField="Descripcion"
                                DataValueField="IdGrado" Width="380px" Height="22px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">
                            <asp:Label ID="Label6" runat="server" Text="[CICLO]" />:</td>
                        <td style="text-align: left" class="style39">
                            <asp:DropDownList ID="DDLcicloescolar" runat="server"
                                DataSourceID="SDSciclosescolares" DataTextField="Ciclo"
                                DataValueField="IdCicloEscolar" Width="350px" AutoPostBack="True" Height="22px">
                            </asp:DropDownList>
                        </td>
                        <td class="style37">
                            <asp:Label ID="Label7" runat="server" Text="[GRUPO]" />:</td>
                        <td class="style17" colspan="2">
                            <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                                DataSourceID="SDSgrupos" DataTextField="Descripcion"
                                DataValueField="IdGrupo" Width="380px" Height="22px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">Nombre(s):</td>
                        <td class="style18">
                            <asp:TextBox ID="TBnombre" runat="server" MaxLength="40"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">Apellido Paterno:</td>
                        <td class="style18">
                            <asp:TextBox ID="TBApePaterno" runat="server" MaxLength="30"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">Apellido Materno:</td>
                        <td class="style18">
                            <asp:TextBox ID="TBApeMaterno" runat="server" MaxLength="30"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">Matrícula:</td>
                        <td class="style18">
                            <asp:TextBox ID="TBmatricula" runat="server" MaxLength="20"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">
                            <asp:Label ID="Label8" runat="server" Text="[EQUIPO]" />:</td>
                        <td class="style18">
                            <asp:DropDownList ID="DDLequipo" runat="server" Width="145px">
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">
                            <asp:Label ID="Label9" runat="server" Text="[SUBEQUIPO]" />:</td>
                        <td class="style18">
                            <asp:DropDownList ID="DDLsubequipo" runat="server" Width="145px">
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">Email:</td>
                        <td class="style18">
                            <asp:TextBox ID="TBemail" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">Login:</td>
                        <td class="style18">
                            <asp:TextBox ID="TBlogin" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">Password:</td>
                        <td class="style18">
                            <asp:TextBox ID="TBpassword" runat="server" MaxLength="20"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr class="estandar">
                        <td class="style24" colspan="3">Estatus:</td>
                        <td class="style18">
                            <asp:DropDownList ID="DDLestatus" runat="server">
                                <asp:ListItem>Activo</asp:ListItem>
                                <asp:ListItem>Suspendido</asp:ListItem>
                                <asp:ListItem>Baja</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        
                    </tr>
                    <tr>
                        <td class="style12" colspan="2">
                            <asp:HyperLink ID="HyperLink1" runat="server"
                                NavigateUrl="~/"
                                CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
                        </td>
                        <td class="style12" colspan="2">
                            <asp:Button ID="Insertar" runat="server" Text="Insertar"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Limpiar" runat="server" Text="Limpiar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style27" colspan="5">NOTA: Estatus SUSPENDIDO: El 
														<asp:Label ID="Label10" runat="server" Text="[ALUMNO]" />
                            no puede realizar actividades pero sí 
                            aparece en reportes y en funciones de revisión y captura</td>
                    </tr>
                    <tr>
                        <td class="style12" colspan="5">
                            <asp:Label ID="LblMensaje" runat="server"
                                CssClass="LabelInfoDefault"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <uc1:msgError runat="server" ID="msgError" />
                        </td>
                       
                    </tr>
                    <tr>
                         <td colspan="5">
                             <uc1:msgSuccess runat="server" ID="msgSuccess" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style12" colspan="5">
                            <asp:GridView ID="GridView1" runat="server" 
                                AllowPaging="True"
                                AllowSorting="True" 
                                AutoGenerateColumns="False" 
                                DataKeyNames="IdAlumno" 
                                DataSourceID="SDSalumnos" 
                                Caption="<h3>ALUMNOS capturados</h3>"
                                PageSize="30"

                                BackColor="White"
                                BorderColor="#999999" 
                                BorderStyle="Solid" 
                                BorderWidth="1px" 
                                CellPadding="3"
                                GridLines="Vertical"
                                Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small; margin-left: 0px; text-align: left;"
                                Width="919px" 
                                ForeColor="Black">
                                <Columns>
                                    <asp:BoundField DataField="IdAlumno" HeaderText="Id_[Alumno]"
                                        InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno" />
                                    <asp:BoundField DataField="IdUsuario" HeaderText="IdUsuario"
                                        SortExpression="IdUsuario" Visible="False" />
                                    <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo"
                                        SortExpression="IdGrupo" Visible="False" />
                                    <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                                        SortExpression="IdPlantel" Visible="False" />
                                    <asp:BoundField DataField="ApePaterno" HeaderText="Apellido Paterno"
                                        SortExpression="ApePaterno" />
                                    <asp:BoundField DataField="ApeMaterno" HeaderText="Apellido Materno"
                                        SortExpression="ApeMaterno" />
                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre(s)"
                                        SortExpression="Nombre" />
                                    <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                                        SortExpression="Matricula" />
                                    <asp:BoundField DataField="Equipo" HeaderText="[Equipo]" SortExpression="Equipo" />
                                    <asp:BoundField DataField="Subequipo" HeaderText="[Subequipo]"
                                        SortExpression="Subequipo" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                    <asp:BoundField DataField="FechaIngreso" HeaderText="FechaIngreso"
                                        SortExpression="FechaIngreso" Visible="False" />
                                    <asp:BoundField DataField="FechaModif" HeaderText="FechaModif"
                                        SortExpression="FechaModif" Visible="False" />
                                    <asp:BoundField DataField="Modifico" HeaderText="Modifico"
                                        SortExpression="Modifico" Visible="False" />
                                    <asp:BoundField DataField="Login" HeaderText="Login" SortExpression="Login" />
                                    <asp:BoundField DataField="Password" HeaderText="Password"
                                        SortExpression="Password" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                            <%-- sort YES - rowdatabound
	                            0  id_ALUMNO
	                            1  idusuario
	                            2  idgrupo
	                            3  idplantel
	                            4  appat
	                            5  apmat
	                            6  nombre
	                            7  matricula
	                            8  EQUIPO
	                            9  SUBEQUIPO
	                            10 ...
	                            --%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table class="style13">
                    <tr>
                        <td>
                            <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Institucion] ORDER BY [Descripcion]"></asp:SqlDataSource>
                        </td>
                        <td>
                            <asp:SqlDataSource ID="SDSplanteles" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion)
ORDER BY [Descripcion]">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td>
                            <asp:SqlDataSource ID="SDSniveles" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="select N.IdNivel, N.Descripcion
from Nivel N, Escuela E, Plantel P
where N.IdNivel = E.IdNivel and P.IdPlantel = E.IdPlantel
      and P.IdPlantel = @IdPlantel
Order by Descripcion">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td>
                            <asp:SqlDataSource ID="SDSgrados" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:SqlDataSource ID="SDSgrupos" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Grupo] WHERE ([IdGrado] = @IdGrado) and ([IdPlantel] = @IdPlantel)
and ([IdCicloEscolar] = @IdCicloEscolar)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                                        PropertyName="SelectedValue" Type="Int32" />
                                    <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                                        PropertyName="SelectedValue" />
                                    <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                                        PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td>
                            <asp:SqlDataSource ID="SDSalumnos" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT A.*, U.Login, U.Password 
FROM Alumno A, Usuario U
WHERE (([IdGrupo] = @IdGrupo) AND ([IdPlantel] = @IdPlantel)) 
AND U.IdUsuario = A.IdUsuario
ORDER BY [ApePaterno], [ApeMaterno], [Nombre]">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                                        PropertyName="SelectedValue" Type="Int32" />
                                    <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </td>
                        <td>
                            <asp:SqlDataSource ID="SDSusuarios" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Usuario]">
                               
                            </asp:SqlDataSource>
                        </td>
                        <td>
                            <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT IdCicloEscolar, Descripcion + ' (' + Cast(FechaInicio as varchar(12)) + ' - ' + Cast(FechaFin as varchar(12)) + ')' Ciclo
FROM CicloEscolar
WHERE (Estatus = 'Activo')
order by Descripcion"></asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: left">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

