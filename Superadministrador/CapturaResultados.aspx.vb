﻿Imports Siget


Partial Class superadministrador_CapturaResultados
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GVAlumnos.Caption = "<h3>Seleccione al " &
            Config.Etiqueta.ALUMNO &
            "</h3>"

        TitleLiteral.Text = "Capturar Resultados"

        Label1.Text = Config.Etiqueta.CICLO
        Label2.Text = Config.Etiqueta.NIVEL
        Label3.Text = Config.Etiqueta.GRADO
        Label4.Text = Config.Etiqueta.ASIGNATURA
        Label5.Text = Config.Etiqueta.INSTITUCION
        Label6.Text = Config.Etiqueta.PLANTEL
        Label7.Text = Config.Etiqueta.GRUPO
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija una " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLcalificacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacion.DataBound
        DDLcalificacion.Items.Insert(0, New ListItem("---Elija una Calificación", 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub BtnAsignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAsignar.Click
        msgError.hide()

        Try
            'HACER UN UPDATE SI YA EXISTE, OSEA, EL GRID TIENE DATOS
            If GVresultados.Rows.Count > 0 Then
                SDSresultados.UpdateCommand = "SET dateformat dmy; UPDATE EVALUACIONTERMINADA SET Resultado = " + TBresultado.Text + " where IdEvaluacionT = " + GVresultados.SelectedDataKey("IdEvaluacionT").ToString
                SDSresultados.Update()
                GVresultados.DataBind()
            Else
                SDSresultados.InsertCommand = "SET dateformat dmy; INSERT INTO EVALUACIONTERMINADA(IdAlumno,IdGrado,IdGrupo,IdCicloEscolar,IdEvaluacion,Resultado,FechaTermino)" + _
                        " VALUES (" + GVAlumnos.SelectedDataKey("IdAlumno").ToString + "," + DDLgrado.SelectedValue + "," + DDLgrupo.SelectedValue + "," + _
                        DDLcicloescolar.SelectedValue + "," + GVevaluaciones.SelectedDataKey("IdEvaluacion").ToString + "," + _
                        TBresultado.Text + ",getdate())"
                SDSresultados.Insert()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEliminar.Click
        msgError.hide()

        Try
            SDSresultados.DeleteCommand = "DELETE FROM EvaluacionTerminada where IdEvaluacionT = " + GVresultados.SelectedDataKey("IdEvaluacionT")
            SDSresultados.Delete()
            GVresultados.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVresultados_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVresultados.DataBound
        If GVresultados.Rows.Count > 0 Then
            BtnEliminar.Visible = True
        Else
            BtnEliminar.Visible = False
        End If
    End Sub



    Protected Sub GVAlumnos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVAlumnos.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(0).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.ALUMNO

            LnkHeaderText = e.Row.Cells(10).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.EQUIPO

            LnkHeaderText = e.Row.Cells(11).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.SUBEQUIPO
        End If
    End Sub

    

End Class
