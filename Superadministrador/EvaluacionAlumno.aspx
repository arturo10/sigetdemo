﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="EvaluacionAlumno.aspx.vb"
    Inherits="superadministrador_EvaluacionAlumno"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            width: 100%;
        }

        .style28 {
            height: 32px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            text-align: left;
            color: #000000;
        }

        .style29 {
            width: 127px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style14 {
            width: 133px;
            text-align: right;
        }

        .style13 {
            text-align: left;
        }

        .style33 {
            text-align: left;
            font-weight: bold;
            color: #000066;
            height: 24px;
        }

        .style30 {
            width: 127px;
            text-align: right;
            height: 16px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style18 {
            height: 16px;
            text-align: left;
            font-weight: bold;
        }

        .style41 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style15 {
            text-align: right;
            height: 16px;
        }

        .style25 {
            height: 16px;
            text-align: center;
            width: 226px;
        }

        .style26 {
            height: 16px;
            text-align: center;
            width: 466px;
        }

        .style32 {
            height: 16px;
            text-align: center;
            width: 226px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style24 {
            height: 16px;
            text-align: center;
        }

        .style21 {
            font-size: x-small;
        }

        .style23 {
            height: 16px;
            text-align: left;
            font-weight: bold;
            width: 226px;
        }

        .style27 {
            height: 16px;
            text-align: left;
            font-weight: bold;
            width: 466px;
        }

        .style42 {
            height: 32px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            text-align: left;
            color: #000066;
        }

        .style43 {
            text-align: left;
            height: 48px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Asignar fechas específicas de Actividades a un 
								<asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style11">
        <tr>
            <td>
                <table class="estandar">
                    <tr>
                        <td class="style29">
                            <asp:Label ID="Label2" runat="server" Text="[CICLO]" />
                        </td>
                        <td colspan="6" style="text-align: left">
                            <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                                DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                                DataValueField="IdCicloEscolar" Width="322px" Height="22px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14" colspan="5">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style29">
                            <asp:Label ID="Label3" runat="server" Text="[NIVEL]" />
                        </td>
                        <td colspan="6" style="text-align: left">
                            <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                                Height="22px" Width="322px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14" colspan="5">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style29">
                            <asp:Label ID="Label4" runat="server" Text="[GRADO]" />
                        </td>
                        <td colspan="6" style="text-align: left">
                            <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                                Height="22px" Width="322px">
                            </asp:DropDownList>
                        </td>
                        <td class="style14" colspan="5">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style29">
                            <asp:Label ID="Label5" runat="server" Text="[ASIGNATURA]" />
                        </td>
                        <td colspan="12" style="text-align: left">
                            <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                                DataSourceID="SDSasignaturas" DataTextField="Materia"
                                DataValueField="IdAsignatura" Height="22px" Width="650px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style29">Calificación</td>
                        <td colspan="12" style="text-align: left">
                            <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True"
                                DataSourceID="SDScalificaciones" DataTextField="Cal"
                                DataValueField="IdCalificacion" Height="22px" Width="650px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style13">&nbsp;</td>
                        <td colspan="12">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style13">&nbsp;</td>
                        <td colspan="12">
                            <asp:GridView ID="GVevaluaciones" runat="server" AllowPaging="True"
                                AllowSorting="True" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3"
                                DataKeyNames="IdEvaluacion" DataSourceID="SDSevaluaciones"
                                GridLines="Vertical" Style="font-size: x-small"
                                Caption="<h3>Actividades creadas:</h3>"
                                ForeColor="Black">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                                        InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion" />
                                    <asp:BoundField DataField="IdCalificacion" HeaderText="Id Calificación"
                                        SortExpression="IdCalificacion" />
                                    <asp:BoundField DataField="ClaveBateria" HeaderText="Nombre de la Actividad"
                                        SortExpression="ClaveBateria">
                                        <HeaderStyle Width="200px" />
                                        <ItemStyle Width="200px" />
                                    </asp:BoundField>
                                    <%--<asp:BoundField DataField="Resultado" HeaderText="Resultado"
                                        SortExpression="Resultado" />--%>
                                    <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                                        SortExpression="Porcentaje">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderText="Inicia" SortExpression="InicioContestar" />
                                    <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderText="Termina" SortExpression="FinContestar" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="style43" colspan="13">
                            <asp:HyperLink ID="HyperLink1" runat="server"
                                NavigateUrl="~/"
                                CssClass="defaultBtn btnThemeGrey btnThemeWide">
								Regresar&nbsp;al&nbsp;Menú
                            </asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="style42" bgcolor="#CCCCCC" colspan="13">Ahora elija al 
														<asp:Label ID="Label6" runat="server" Text="[ALUMNO]" />
                            que tendrá fechas diferentes a las definidas para la 
                            Actividad</td>
                    </tr>
                    <tr>
                        <td class="style29">
                            <asp:Label ID="Label7" runat="server" Text="[INSTITUCION]" />
                        </td>
                        <td colspan="12" style="text-align: left">
                            <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                DataValueField="IdInstitucion" Height="22px" Width="500px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style30">
                            <asp:Label ID="Label8" runat="server" Text="[PLANTEL]" />
                        </td>
                        <td class="style18" colspan="12">
                            <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                DataValueField="IdPlantel" Height="22px" Width="500px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style30">
                            <asp:Label ID="Label9" runat="server" Text="[GRUPO]" />
                        </td>
                        <td class="style18" colspan="12">
                            <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                                DataSourceID="SDSgrupos" DataTextField="Descripcion" DataValueField="IdGrupo"
                                Width="500px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style30">Estatus</td>
                        <td class="style18" colspan="3">
                            <asp:DropDownList ID="DDLestatus" runat="server" Width="108px"
                                CssClass="style41">
                                <asp:ListItem>Sin iniciar</asp:ListItem>
                                <asp:ListItem>Iniciada</asp:ListItem>
                                <asp:ListItem>Terminada</asp:ListItem>
                                <asp:ListItem>Cancelada</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td class="style18" colspan="3">
                            <asp:LinkButton ID="LinkButton1" runat="server" Style="font-family: Arial, Helvetica, sans-serif; font-size: small" Visible="False">Marcar todos</asp:LinkButton>
                            &nbsp;&nbsp;
                            <asp:LinkButton ID="LinkButton2" runat="server" Style="font-family: Arial, Helvetica, sans-serif; font-size: small" Visible="False">Desmarcar todos</asp:LinkButton>
                        </td>
                        <td class="style18" colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style30">
                            <asp:Label ID="Label10" runat="server" Text="[ALUMNOS]" />
                        </td>
                        <td class="style18" colspan="12">
                            <asp:CheckBoxList ID="CBLalumnos" runat="server"
                                DataSourceID="SDSalumnos" DataTextField="NomAlumno"
                                DataValueField="IdAlumno"
                                Style="background-color: #CCCCCC; font-size: x-small;" Width="500px">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">&nbsp;</td>
                        <td class="style25">&nbsp;</td>
                        <td class="style26">&nbsp;</td>
                        <td class="style32">
                            <b>Inicio</b> de 
                                Aplicación<br />
                            de la Actividad</td>
                        <td class="style25">&nbsp;</td>
                        <td class="style24">
                            Fecha <b>máxima</b> en que se<br />
                            debe realizar la Actividad<br />
                            para evitar penalización</td>
                        <td class="style24" colspan="2">&nbsp;</td>
                        <td class="style24">
                            <b>Fin</b> de
                                Aplicación<br />
                            de la Actividad<b><br class="style21" />
                                <span class="style21">(después de esta fecha ya no<br />
                                    se podrá realizar la actividad)</span></b>
                            </td>
                        <td class="style24">&nbsp;</td>
                        <td class="style24">&nbsp;</td>
                        <td class="style24" colspan="2"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">&nbsp;</td>
                        <td class="style23">&nbsp;</td>
                        <td class="style27">&nbsp;</td>
                        <td class="style23">
                            <asp:Calendar ID="CalInicioContestar" runat="server" BackColor="White"
                                BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest"
                                Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px"
                                Style="font-family: Arial, Helvetica, sans-serif; font-size: xx-small"
                                Width="200px">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#808080" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>
                        </td>
                        <td class="style23">&nbsp;</td>
                        <td class="style18">
                            <asp:Calendar ID="CalLimiteSinPenalizacion" runat="server" BackColor="White"
                                BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest"
                                Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px"
                                Style="font-family: Arial, Helvetica, sans-serif; font-size: xx-small"
                                Width="200px">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#808080" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>
                        </td>
                        <td class="style18" colspan="2">&nbsp;</td>
                        <td class="style18">
                            <asp:Calendar ID="CalFinContestar" runat="server" BackColor="White"
                                BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest"
                                Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px"
                                Style="font-family: Arial, Helvetica, sans-serif; font-size: xx-small"
                                Width="200px">
                                <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                <SelectorStyle BackColor="#CCCCCC" />
                                <WeekendDayStyle BackColor="#FFFFCC" />
                                <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <OtherMonthDayStyle ForeColor="#808080" />
                                <NextPrevStyle VerticalAlign="Bottom" />
                                <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                            </asp:Calendar>
                            
                        </td>
                        <td class="style18">&nbsp;</td>
                        <td class="style18">&nbsp;</td>
                        <td class="style18" colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style15"></td>
                        <td class="style16" colspan="6" style="text-align: right">&nbsp;</td>
                        <td class="style17" colspan="5">
                            <asp:Button ID="Agregar" runat="server"
                                Text="Agregar" CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                        </td>
                        <td class="style16">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="13">
                            <uc1:msgError runat="server" ID="msgError" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style15" colspan="13">
                            <asp:GridView ID="GVevalalumno" runat="server"
                                AllowPaging="True"
                                AllowSorting="True"
                                AutoGenerateColumns="False"
                                DataKeyNames="IdAlumno,IdEvaluacion"
                                DataSourceID="SDSevalAlumno"
                                Caption="<h3>Relación de ALUMNOS y Actividades asignadas en el GRUPO y CICLO seleccionados</h3>"
                                PageSize="20"
                                BackColor="White"
                                BorderColor="#999999"
                                BorderStyle="Solid"
                                BorderWidth="1px"
                                CellPadding="3"
                                GridLines="Vertical"
                                Style="font-size: x-small; text-align: left;"
                                Width="882px"
                                ForeColor="Black">
                                <Columns>
                                    <asp:CommandField ShowSelectButton="True" />
                                    <asp:BoundField DataField="IdAlumno" HeaderText="Id Alumno" ReadOnly="True"
                                        SortExpression="IdAlumno">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Nombre del Alumno" HeaderText="Nombre del Alumno"
                                        ReadOnly="True" SortExpression="Nombre del Alumno" />
                                    <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                                        SortExpression="Matricula" />
                                    <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                                        ReadOnly="True" SortExpression="IdEvaluacion" />
                                    <asp:BoundField DataField="ClaveBateria" HeaderText="Actividad"
                                        SortExpression="ClaveBateria" />
                                    <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderText="Inicia Actividad" SortExpression="InicioContestar" />
                                     <asp:BoundField DataField="FinSinPenalizacion"
                                        DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha máxima"
                                        SortExpression="FinSinPenalizacion" />
                                    <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                        HeaderText="Finaliza Actividad" SortExpression="FinContestar" />
                                    <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"
                                        SortExpression="Asignatura" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle BackColor="#CCCCCC" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>
                            <%-- sort YES - rowdatabound
	                            0  select
	                            1  Id_ALUMNO
	                            2  Nombre del ALUMNO
	                            3  matricula
	                            4  id_actividad
	                            5  actividad
	                            6  inicioContestar
	                            7  finContestar
	                            8  finSinPenalizacion
	                            9  ASIGNATURA
	                            10 estatus
                            --%>
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">&nbsp;</td>
                        <td class="style16" colspan="3">&nbsp;</td>
                        <td class="style16" colspan="3">&nbsp;</td>
                        <td class="style16" colspan="6">
                            <asp:Button ID="Quitar" runat="server" Text="Quitar" Visible="False"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="BtnActualizar" runat="server" Text="Actualizar"
                                Visible="False" CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                        </td>
                    </tr>
                    <tr>
                        <td class="style15">&nbsp;</td>
                        <td class="style16" colspan="3">&nbsp;</td>
                        <td class="style16" colspan="3">&nbsp;</td>
                        <td class="style16" colspan="3">&nbsp;</td>
                        <td class="style16" colspan="3">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; text-align: left;">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlActualizarGrupo" runat="server" Visible="false">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                <div class="contentSeparator" style="width: 500px; margin-left: auto; margin-right: auto;">
                                    Modificar para <asp:Literal ID="Literal8" runat="server" Text="[EL]" />
                                    <asp:Literal ID="Literal1" runat="server" Text="[GRUPO]" /> seleccionado
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width: 500px; margin-left: auto; margin-right: auto;">
                                    <tr>
                                        <td style="text-align: left;">Permite modificar las fechas o el estado de la actividad para tod<asp:Literal ID="Literal2" runat="server" Text="[O]" />s 
                            <asp:Literal ID="Literal3" runat="server" Text="[LOS]" />
                                            <asp:Literal ID="Literal4" runat="server" Text="[ALUMNOS]" />
                                            que tengan la evaluación asignada, de 
                            <asp:Literal ID="Literal5" runat="server" Text="[UN]" />
                                            <asp:Literal ID="Literal6" runat="server" Text="[GRUPO]" />
                                            seleccionad<asp:Literal ID="Literal7" runat="server" Text="[O]" />.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            <asp:Button ID="btnActualizarGrupo" runat="server" Text="Actualizar Fechas"
                                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                            &nbsp;
                                            <asp:Button ID="btnActualizarGrupoEstado" runat="server" Text="Actualizar Estatus"
                                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <uc1:msgError runat="server" ID="msgError1" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <uc1:msgSuccess runat="server" ID="msgSuccess1" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSevalAlumno" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select EA.IdAlumno, A.ApePaterno + ' ' + A.ApeMaterno + ' ' + A.Nombre as 'Nombre del Alumno', A.Matricula, EA.IdEvaluacion, E.ClaveBateria,
EA.InicioContestar, EA.FinContestar, EA.FinSinPenalizacion, Asig.Descripcion as Asignatura, EA.Estatus
from Alumno A, Evaluacion E, EvaluacionAlumno EA, Calificacion C, Asignatura Asig
where C.IdCicloEscolar = @IdCicloEscolar and E.IdCalificacion = C.IdCalificacion
and Asig.IdAsignatura = C.IdAsignatura and A.IdAlumno = EA.IdAlumno and 
E.IdEvaluacion = EA.IdEvaluacion and  A.IdGrupo = @IdGrupo and A.Estatus != 'Baja'
order by A.ApePaterno,A.ApeMaterno,A.Nombre, C.IdAsignatura, E.InicioContestar">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrupo], [Descripcion] FROM [Grupo] WHERE (([IdGrado] = @IdGrado) AND ([IdPlantel] = @IdPlantel)) and ([IdCicloEscolar] = @IdCicloEscolar) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Institucion] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Evaluacion] WHERE ([IdCalificacion] = @IdCalificacion)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion"
                            PropertyName="SelectedValue" Type="Int64" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT C.IdCalificacion, C.Descripcion + ' (' + A.Descripcion + ')' as Cal
FROM Calificacion C, Asignatura A
WHERE C.IdCicloEscolar = @IdCicloEscolar
and A.IdAsignatura = C.IdAsignatura and A.IdAsignatura = @IdAsignatura
order by C.Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdGrado,Descripcion FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT A.IdAsignatura, A.Descripcion + ' (' + Ar.Descripcion + ')'  as Materia
FROM Asignatura A, Area Ar
WHERE (A.IdGrado = @IdGrado) and (Ar.IdArea = A.IdArea)
ORDER BY Ar.Descripcion,A.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="CicloEscolar_ObtenLista_Estatus"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:Parameter Name="Estatus" DefaultValue="Activo"/>
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSalumnos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdAlumno, rtrim(ApePaterno) + ' ' + rtrim(ApeMaterno) + ' ' + rtrim(Nombre) + '   (' + rtrim(Matricula) + ')' as NomAlumno
from Alumno where IdGrupo = @IdGrupo
and IdAlumno not in (select IdAlumno from EvaluacionAlumno where IdEvaluacion = @IdEvaluacion)
                                and Estatus != 'Baja'
order by NomAlumno">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>

