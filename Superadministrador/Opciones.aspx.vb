﻿Imports Siget

Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.IO

Partial Class superadministrador_Opciones
		Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        TitleLiteral.Text = "Opciones de Respuesta"

        Label4.Text = Config.Etiqueta.ASIGNATURA
        Label5.Text = Config.Etiqueta.NIVEL
        Label6.Text = Config.Etiqueta.GRADO
        CBesconder.Text = "SOLO PARA INTERFAZ DE CAPTURA (No le aparecerán al " &
            Config.Etiqueta.ALUMNO &
            ", se utiliza principalmente para la opción &quot;No Contestó&quot;)"
    End Sub

    Public ReadOnly Property PlanteamientoActual() As String
        Get
            Return LBplanteamiento.SelectedValue
        End Get
    End Property

    Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click
        TBredaccion.Text = ""
        TBresp1.Text = ""
        TBresp2.Text = ""
        DDLconsecutivo.SelectedValue = "A"
        CBesconder.Checked = False
    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        msgSuccess.hide()
        msgError.hide()
        Dim Redaccion As String = TBredaccion.Text
        Try

            Redaccion = Replace(Redaccion, "'", "''")


            If RutaArchivo.HasFile Then 'El atributo .HasFile compara si se indico un archivo           
                SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion (IdPlanteamiento,Redaccion,Correcta,Resp1,Resp2,Consecutiva,ArchivoApoyo,TipoArchivoApoyo,FechaModif,Modifico,Esconder) VALUES (" + LBplanteamiento.SelectedValue + ",'" + HttpUtility.HtmlDecode(Redaccion) + "','" + RBcorrecta.SelectedValue + "','" + TBresp1.Text + "','" + TBresp2.Text + "','" + DDLconsecutivo.SelectedValue + "','" + RutaArchivo.FileName + "','" + DDLtipoarchivo.SelectedValue + "',getdate(),'" + User.Identity.Name.ToString + "','" + CBesconder.Checked.ToString + "')"
                'Ahora CARGO el archivo
                Dim ruta As String
                ruta = Config.Global.rutaMaterial

                Try
                    Dim MiArchivo As FileInfo = New FileInfo(ruta & RutaArchivo.FileName)
                    If MiArchivo.Exists Then
                        'Avisa que ya existe, NO LO SUBE pero sí almacena el registro
                        Label1.Text = "El archivo que está intentando cargar ya existía, revise que sea el correcto, de lo contrario, actualice el registro."
                    Else
                        RutaArchivo.SaveAs(ruta & RutaArchivo.FileName)
                        Label1.Text = "Archivo cargado: " & RutaArchivo.FileName
                    End If
                Catch ex As Exception
                    Utils.LogManager.ExceptionLog_InsertEntry(ex)
                    msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
                End Try
            Else
                SDSopciones.InsertCommand = "SET dateformat dmy; INSERT INTO Opcion (IdPlanteamiento,Redaccion,Correcta,Resp1,Resp2,Consecutiva,FechaModif,Modifico,Esconder) VALUES (" + LBplanteamiento.SelectedValue + ",'" + HttpUtility.HtmlDecode(Redaccion) + "','" + RBcorrecta.SelectedValue + "','" + TBresp1.Text + "','" + TBresp2.Text + "','" + DDLconsecutivo.SelectedValue + "',getdate(), '" + User.Identity.Name.ToString + "','" + CBesconder.Checked.ToString + "')"
            End If
            SDSopciones.Insert()
            msgSuccess.show("Éxito", "El registro ha sido guardado.")
            GVopciones.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try
    End Sub

    Protected Sub GVopciones_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVopciones.SelectedIndexChanged
        'La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
        TBredaccion.Text = HttpUtility.HtmlDecode(GVopciones.SelectedRow.Cells(3).Text)
        RBcorrecta.SelectedValue = GVopciones.SelectedRow.Cells(4).Text
        TBresp1.Text = HttpUtility.HtmlDecode(GVopciones.SelectedRow.Cells(5).Text)
        TBresp2.Text = HttpUtility.HtmlDecode(GVopciones.SelectedRow.Cells(6).Text)
        DDLconsecutivo.SelectedValue = GVopciones.SelectedRow.Cells(7).Text.Trim()
        Label1.Text = HttpUtility.HtmlDecode(GVopciones.SelectedRow.Cells(8).Text)
        DDLtipoarchivo.SelectedValue = HttpUtility.HtmlDecode(GVopciones.SelectedRow.Cells(9).Text)
        CBesconder.Checked = GVopciones.SelectedRow.Cells(10).Text 'Cuando agregue el campo Esconder a una tabla que no lo tenga, poner este campo en False para todos los registros
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        msgSuccess.hide()
        msgError.hide()

        Dim Redaccion As String = TBredaccion.Text
        Try
            'Quito los apóstrofes porque marcaría error al momento de construir la siguiente consulta
            Redaccion = Replace(Redaccion, "'", "''")

            If RutaArchivo.HasFile Then 'El atributo .HasFile compara si se indico un archivo
                SDSopciones.UpdateCommand = "SET dateformat dmy; UPDATE Opcion SET Redaccion = '" + HttpUtility.HtmlDecode(Redaccion) + "', Correcta = '" + RBcorrecta.SelectedValue + "', Resp1 = '" + TBresp1.Text + "', Resp2 = '" + TBresp2.Text + "', Consecutiva = '" + DDLconsecutivo.SelectedValue + "', ArchivoApoyo = '" + RutaArchivo.FileName + "', TipoArchivoApoyo = '" + DDLtipoarchivo.SelectedValue + "', FechaModif = getdate(), Modifico = '" + User.Identity.Name.ToString + "', Esconder = '" + CBesconder.Checked.ToString + "' WHERE IdOpcion =" + GVopciones.SelectedRow.Cells(1).Text

                'Ahora CARGO el archivo
                Dim ruta As String
                ruta = Config.Global.rutaMaterial

                Try
                    RutaArchivo.SaveAs(ruta & RutaArchivo.FileName)
                    Label1.Text = "Archivo cargado: " & RutaArchivo.FileName
                Catch ex As Exception
                    Utils.LogManager.ExceptionLog_InsertEntry(ex)
                    msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
                End Try
            Else
                SDSopciones.UpdateCommand = "SET dateformat dmy; UPDATE Opcion SET Redaccion = '" + HttpUtility.HtmlDecode(Redaccion) + "', Correcta = '" + RBcorrecta.SelectedValue + "', Resp1 = '" + TBresp1.Text + "', Resp2 = '" + TBresp2.Text + "', Consecutiva = '" + DDLconsecutivo.SelectedValue + "', FechaModif = getdate(), Modifico = '" + User.Identity.Name.ToString + "', Esconder = '" + CBesconder.Checked.ToString + "' WHERE IdOpcion =" + GVopciones.SelectedRow.Cells(1).Text
            End If
            SDSopciones.Update()
            msgSuccess.show("Éxito", "El registro ha sido actualizado.")
            GVopciones.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

  Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
    DDLnivel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.NIVEL, 0))
  End Sub

  Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
    DDLgrado.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRADO, 0))
  End Sub

  Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
    DDLasignatura.Items.Insert(0, New ListItem("---Elija la " & Config.Etiqueta.ASIGNATURA, 0))
  End Sub

  Protected Sub DDLtemas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLtemas.DataBound
    DDLtemas.Items.Insert(0, New ListItem("---Elija el Tema", 0))
  End Sub

  Protected Sub DDLsubtemas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLsubtemas.DataBound
    DDLsubtemas.Items.Insert(0, New ListItem("---Elija el Subtema", 0))
  End Sub

  Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
    msgSuccess.hide()
    msgError.hide()

    Try
      'NOTA: Se debe de tener cuidado de que al borrar opciones que tienen archivos de apoyo asignados, que no haya otras
      'opciones que hagan referencia a esos mismos archivos porque cuando se borre el primer planteamiento también se borrarán
      'los archivos de apoyo

      SDSopciones.DeleteCommand = "Delete from Opcion where IdOpcion = " + GVopciones.SelectedValue.ToString 'Me da el IdOpcion porque es el campo clave de la fila seleccionada (ademas se autoconfigura en DATAKEYNAMES)
      SDSopciones.Delete()

      'Elimino el archivo de apoyo que tenga la opción
      If GVopciones.SelectedRow.Cells(8).Text <> "&nbsp;" Then 'Significa que tiene un valor en ese campo
        'Antes de borrar los archivos verifico que no estén referenciados en otros registros
        Dim Total = 0
        Dim strConexion As String
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        miComando = objConexion.CreateCommand
        miComando.CommandText = "select Count(*) as Total from ApoyoSubtema where ArchivoApoyo = '" + Trim(HttpUtility.HtmlDecode(GVopciones.SelectedRow.Cells(8).Text)) + "'"
        'LblError.Text = miComando.CommandText
        objConexion.Open()
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        misRegistros.Read()
        Total = misRegistros.Item("Total")

        misRegistros.Close()
        miComando.CommandText = "select Count(*) as Total from Opcion where ArchivoApoyo = '" + Trim(HttpUtility.HtmlDecode(GVopciones.SelectedRow.Cells(8).Text)) + "'"
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        misRegistros.Read()
        Total = Total + misRegistros.Item("Total")

        misRegistros.Close()
        miComando.CommandText = "select Count(*) as Total from Planteamiento where ArchivoApoyo = '" + Trim(HttpUtility.HtmlDecode(GVopciones.SelectedRow.Cells(8).Text)) + "' or ArchivoApoyo2 = '" + Trim(HttpUtility.HtmlDecode(GVopciones.SelectedRow.Cells(8).Text)) + "'"
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        misRegistros.Read()
        Total = Total + misRegistros.Item("Total")
        misRegistros.Close()
        objConexion.Close()

        If Total = 0 Then
          Dim ruta As String
          ruta = Config.Global.rutaMaterial

          'Borro el archivo que estaba anteriormente
          Dim MiArchivo As FileInfo = New FileInfo(ruta & Trim(HttpUtility.HtmlDecode(GVopciones.SelectedRow.Cells(8).Text)))
          MiArchivo.Delete()
        End If
      End If
      GVopciones.DataBind() 'Dejo esta instrucción al final porque si se refresca el GridView antes de borrar el archivo de apoyo, ya no podré saber su nombre
      msgSuccess.show("Éxito", "El registro ha sido eliminado así como su archivo de apoyo si lo tenía.")
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub LBplanteamiento_DataBound(sender As Object, e As EventArgs) Handles LBplanteamiento.DataBound
    For Each litm As ListItem In LBplanteamiento.Items
      ' litm.Text = Regex.Replace(litm.Text, "<[^>]*(>|$)", String.Empty)
      litm.Text = Regex.Replace(HttpUtility.HtmlDecode(litm.Text), "<[^>]*(>|$)", String.Empty)
    Next
  End Sub

  Protected Sub LBplanteamiento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBplanteamiento.SelectedIndexChanged
    msgInfo.hide()
    msgError.hide()
    msgSuccess.hide()

        Dim strConexion As String
        Session("PlanteamientoSeleccionado") = LBplanteamiento.SelectedValue
    'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
    strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
    Dim objConexion As New SqlConnection(strConexion)
    Dim miComando As SqlCommand
    Dim misRegistros As SqlDataReader
    miComando = objConexion.CreateCommand
    Try
      objConexion.Open()
      miComando.CommandText = "Select * from Planteamiento where IdPlanteamiento = " + LBplanteamiento.SelectedValue
      misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
      misRegistros.Read()
      If Trim(misRegistros.Item("TipoRespuesta")) = "Abierta" Then
        Panel1.Visible = False
        GVopciones.Visible = False
        msgInfo.show("Atención", "El reactivo seleccionado es de Respuesta Abierta")
      Else
        Panel1.Visible = True
        GVopciones.Visible = True
      End If
      misRegistros.Close()
      objConexion.Close()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GVopciones_PreRender(sender As Object, e As EventArgs) Handles GVopciones.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVopciones.Rows.Count > 0 Then
      GVopciones.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVopciones_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVopciones.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVopciones.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

    Protected Sub GVopciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVopciones.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVopciones, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVopciones.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVopciones, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

End Class
