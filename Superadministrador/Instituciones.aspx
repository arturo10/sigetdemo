﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="Instituciones.aspx.vb"
    Inherits="superadministrador_Instituciones"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
            height: 450px;
        }

        .style11 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style12 {
            height: 143px;
        }

        .style14 {
            height: 182px;
        }

        .style15 {
            font-weight: normal;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
        }

        .auto-style1 {
            width: 50px;
            text-align: left;
            padding-right: 10px;
        }

        .auto-style2 {
            text-align: left;
        }

        .form_LeftSpacer {
            width: 100px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Administración de Instituciones
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table style="width: 100%; margin-top: 10px;">
        <tr>
            <td class="form_LeftSpacer">&nbsp;</td>
            <td class="auto-style1">Descripción
            </td>
            <td class="auto-style2">
                <asp:TextBox ID="tbDescripcion" runat="server" Width="300" MaxLength="100"></asp:TextBox>

            </td>
        </tr>
        <tr>
            <td style="width: 20px;">&nbsp;</td>
            <td class="auto-style1">Observaciones
            </td>
            <td class="auto-style2">
                <asp:TextBox ID="tbObservaciones" runat="server" Width="100" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 20px;">&nbsp;</td>
            <td class="auto-style1">Estatus
            </td>
            <td class="auto-style2">
                <asp:DropDownList ID="ddlEstatus" runat="server">
                    <asp:ListItem>Activo</asp:ListItem>
                    <asp:ListItem>Suspendido</asp:ListItem>
                    <asp:ListItem>Baja</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: center; padding-top: 20px; padding-bottom: 20px;">
                <asp:Button ID="btnCrear" runat="server" Text="Crear"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
				<asp:Button ID="btnEliminar" runat="server" Text="Eliminar"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                &nbsp;
				<asp:Button ID="btnActualizar" runat="server" Text="Actualizar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
        </tr>
    </table>
    <table style="width: 100%;">
        <tr>
            <td>
                <table style="margin-left: auto; margin-right: auto;">
                    <tr>
                        <td>
                            <asp:GridView ID="gvInstituciones" runat="server"
                                AllowPaging="False"
                                AllowSorting="True"
                                AutoGenerateColumns="False"

                                DataKeyNames="IdInstitucion"
                                DataSourceID="SDSInstituciones"

                                CssClass="dataGrid_clear_selectable"
                                GridLines="None">
                                <Columns>
                                    <asp:CommandField
                                        ShowSelectButton="true"
                                        SelectText="Seleccionar" 
                                        ItemStyle-CssClass="selectCell" />
                                    <asp:BoundField
                                        DataField="IdInstitucion"
                                        HeaderText="Id en Sistema"
                                        SortExpression="IdInstitucion" />
                                    <asp:BoundField 
                                        DataField="Descripcion" 
                                        HeaderText="Descripción"
                                        SortExpression="Descripcion" />
                                    <asp:BoundField 
                                        DataField="Observaciones" 
                                        HeaderText="Observaciones"
                                        SortExpression="Observaciones" />
                                    <asp:BoundField 
                                        DataField="Estatus" 
                                        HeaderText="Estatus"
                                        SortExpression="Estatus" />
                                </Columns>
                                <FooterStyle CssClass="footer" />
                                <PagerStyle CssClass="pager" />
                                <SelectedRowStyle CssClass="selected" />
                                <HeaderStyle CssClass="header" />
                                <AlternatingRowStyle CssClass="altrow" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td>
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSInstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM Institucion"></asp:SqlDataSource>
            </td>
            <td>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>