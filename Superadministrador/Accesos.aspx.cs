﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Text;

public partial class Superadministrador_AccesosInfo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void DDLPerfilSelected_SelectedIndexChanged(object sender, EventArgs e)
    {
        GVAccesos.Visible = true;
        GVAccesosAlumno.Visible = false;

        switch (DDLPerfilSelected.SelectedValue)
        {
            case "Administrador":
                GVAccesos.DataSourceID = "SDSAdmin";
                break;
            case "Coordinador":
                GVAccesos.DataSourceID = "SDSCoordinador";
                break;
            case "Alumno":
                GVAccesos.Visible = false;
                GVAccesosAlumno.Visible = true;
                break;
            case "Profesor":
                GVAccesos.DataSourceID = "SDSProfesor";
                break;
            case "Operador":
                GVAccesos.DataSourceID = "SDSOperador";
                break;
            default:
                GVAccesos.DataSourceID = "SDSAdmin";
                break;
        }
        GVAccesos.DataBind();

    }

    protected void BtnExportar_Click(object sender, EventArgs e)
    {
        if (GVAccesosAlumno.Visible == true)
        {
            Convierte(GVAccesosAlumno, "Accesos Usuarios");
        }
        else
        {
            Convierte(GVAccesos, "Accesos Usuarios");
        }
    }
    protected void Convierte(GridView GVactual, string archivo)
    {
        // añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef");
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Siget.Config.Color.Table_HeaderRow_Background);
        GVactual.GridLines = GridLines.Both;
        GVactual.Columns.RemoveAt(0);
        GVactual.DataBind();
        string headerText = null;


        foreach (TableCell tc in GVactual.HeaderRow.Cells)
        {
            // evita añadir íconos en celdas que no tengan link
            if (tc.HasControls())
            {
                headerText = ((LinkButton)tc.Controls[0]).Text;
                // obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0);
                // quito el ícono de ordenamiento
                // quito el linkbutton de ordenamiento
                tc.Text = headerText;
                // establezco el texto simple
            }
        }

        if (GVactual.Rows.Count + 1 < 65536)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            Page pagina = new Page();
            dynamic form = new HtmlForm();
            pagina.EnableEventValidation = false;
            pagina.DesignerInitialize();
            pagina.Controls.Add(form);
            form.Controls.Add(GVactual);
            pagina.RenderControl(htw);
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(sb.ToString());
            Response.End();
        }
        else
        {
            msgError.show("Demasiados registros para Exportar a Excel.");
        }
    }
}