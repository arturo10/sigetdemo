﻿Imports Siget

Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Partial Class superadministrador_GeneraAlumnos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.EQUIPOS
        Label2.Text = Config.Etiqueta.SUBEQUIPOS
        Label3.Text = Config.Etiqueta.PLANTELES
        Label4.Text = Config.Etiqueta.EQUIPOS
        Label5.Text = Config.Etiqueta.SUBEQUIPOS
        Label6.Text = Config.Etiqueta.ALUMNOS
        Label7.Text = Config.Etiqueta.ALUMNO
        Label8.Text = Config.Etiqueta.ALUMNOS
        Label9.Text = Config.Etiqueta.EQUIPOS
        Label10.Text = Config.Etiqueta.SUBEQUIPOS
    End Sub

    Protected Sub Importar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Importar.Click
        If ArchivoCarga.HasFile Then 'El atributo .HasFile compara si se indico un archivo           
            Try
                Dim Todobien = True
                If CBregistrarme.Checked Then
                    If Trim(TBemail.Text) = "" Then
                        msgError.show("Requiere escribir una cuenta de Email para registrar a los " & Config.Etiqueta.ALUMNOS)
                        Todobien = False
                    End If
                End If
                If Todobien Then
                    LblMensaje.Text = "Espere un momento por favor, la migración puede tardar varios minutos..."
                    'Ahora CARGO el archivo
                    ArchivoCarga.SaveAs(Server.MapPath("~") + "\superadministrador\" & ArchivoCarga.FileName)
                    'Leo el archivo
                    Dim archivo_datos As String = Server.MapPath(ArchivoCarga.FileName())
                    'Dim leer_archivo As StreamReader
                    Dim leer_archivo As StreamReader = New StreamReader(archivo_datos, Encoding.Default, True)
                    'leer_archivo = File.OpenText(archivo_datos)
                    Dim LineaLeida, IdGrupo, Nombre, ApPaterno, ApMaterno, Matricula, Equipo, Subequipo, Login, Password, Grupo, Plantel, Grado As String
                    Dim I, F, IdUsuario, IdPlantel As Integer

                    Dim strConexion As String
                    'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                    strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                    Dim objConexion As New SqlConnection(strConexion)
                    Dim miComando As SqlCommand
                    Dim misRegistros As SqlDataReader
                    objConexion.Open()
                    miComando = objConexion.CreateCommand

                    'Pongo el inicio de una tablita donde iré desplegando los alumnos creados
                    LblSalida.Text = "<table><tr><td><b>NOMBRE</b></td><td><b>APELLIDO PATERNO</b></td><td><b>APELLIDO MATERNO</b></td><td><b>MATRICULA</b></td><td><b>" & Config.Etiqueta.EQUIPO & "</b></td><td><b>" & Config.Etiqueta.SUBEQUIPO & "</b></td><td><b>LOGIN</b></td><td><b>PASSWORD</b></td><td><b>" & Config.Etiqueta.GRUPO & "</b></td><td><b>" & Config.Etiqueta.GRADO & "</b></td><td><b>" & Config.Etiqueta.PLANTEL & "</b></td></tr>"

                    While Not leer_archivo.EndOfStream
                        'Leo el registro completo y ubico cada campo
                        LineaLeida = leer_archivo.ReadLine
                        I = InStr(LineaLeida, "|")
                        F = InStr(I + 1, LineaLeida, "|")
                        IdGrupo = Trim(Mid(LineaLeida, 1, I - 1)) 'CAMPO 1
                        Nombre = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 2
                        I = F
                        F = InStr(I + 1, LineaLeida, "|")
                        ApPaterno = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 3
                        I = F
                        F = InStr(I + 1, LineaLeida, "|")
                        ApMaterno = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 4
                        I = F
                        F = InStr(I + 1, LineaLeida, "|")
                        Matricula = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 5
                        I = F
                        F = InStr(I + 1, LineaLeida, "|")
                        Equipo = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 6
                        I = F
                        F = InStr(I + 1, LineaLeida, "|")
                        Subequipo = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 7
                        I = F
                        F = InStr(I + 1, LineaLeida, "|")
                        Login = Trim(Mid(LineaLeida, I + 1, F - I - 1)) 'CAMPO 8
                        I = F
                        Password = Trim(Mid(LineaLeida, I + 1, Len(LineaLeida))) 'CAMPO 9 (Ultimo)

                        'Ahora ingreso el registro del Usuario
                        'Primero creo una cadena de 3 numeros aleatoria para agregarsela al password
                        '>> Dim aleatorio As Integer
                        '>> Randomize()
                        '>> aleatorio = (Rnd() * 1000) + 1
                        '>> Password = Trim(Password) + CStr(aleatorio)

                        SDSusuarios.InsertCommand = "SET dateformat dmy; INSERT INTO Usuario (Login,Password,PerfilASP,FechaModif) VALUES ('" + Login + "','" + Password + "','Alumno',getdate())"
                        SDSusuarios.Insert()

                        'Ahora busco el recien ingresado Usuario para obtener su ID                  
                        miComando.CommandText = "SELECT * FROM USUARIO WHERE Login = '" + Login + "'"
                        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                        misRegistros.Read()
                        IdUsuario = misRegistros.Item("IdUsuario")
                        misRegistros.Close()

                        'Ahora busco el IdPlantel que le pertenece al Grupo y saco los datos de descripcion del Grado y del Plantel
                        miComando.CommandText = "SELECT G.IdGrupo,G.Descripcion as Grupo,G.IdPlantel,P.Descripcion as Plantel,Gr.Descripcion as Grado FROM GRUPO G, PLANTEL P, GRADO Gr WHERE P.IdPlantel = G.IdPlantel and Gr.IdGrado = G.IdGrado and IdGrupo = " + IdGrupo
                        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                        misRegistros.Read()
                        IdPlantel = misRegistros.Item("IdPlantel")
                        Plantel = misRegistros.Item("Plantel")
                        Grado = misRegistros.Item("Grado")
                        Grupo = misRegistros.Item("Grupo")

                        misRegistros.Close()

                        If CBregistrarme.Checked Then 'AQUI DECIDO SI REGISTRO O NO EL NUEVO USUARIO EN LA TABLA DE SEGURIDAD, ORIGINALMENTE LA FECHA DE INGRESO SE TOMABA COMO LA FECHA EN QUE EL ALUMNO SE AUTOREGISTRABA EN LA BASE DE DATOS DE SEGURIDAD
                            'Ahora ingreso el registro del Alumno, el campo FECHAINGRESO lo tomo para almacenar el momento en que se da de alta en las tablas de seguridad
                            SDSalumnos.InsertCommand = "SET dateformat dmy; INSERT INTO Alumno (IdUsuario,IdGrupo,IdPlantel,Nombre,ApePaterno,ApeMaterno,Matricula,Equipo,Subequipo,Estatus,FechaIngreso,FechaModif) VALUES (" + CStr(IdUsuario) + "," + IdGrupo + "," + CStr(IdPlantel) + ",'" + Nombre + "','" + ApPaterno + "','" + ApMaterno + "','" + Matricula + "','" + Equipo + "','" + Subequipo + "','Activo',getdate(),getdate())"
                            SDSalumnos.Insert()

                            'Ahora registro el usuario en la base de datos de seguridad
                            Dim status As MembershipCreateStatus

                            ' 17/10/2013 se deseleccionó requiresQuestionAndAnswer="false"  de web config, no es necesario
                            'Dim passwordQuestion As String = ""
                            'Dim passwordAnswer As String = ""
                            'If Membership.RequiresQuestionAndAnswer Then
                            '    passwordQuestion = "Empresa desarrolladora de " & Config.Etiqueta.SISTEMA_CORTO
                            '    passwordAnswer = "Integrant"
                            'End If

                            Try
                                Dim newUser As MembershipUser = Membership.CreateUser(Login, Password, _
                                                                               Trim(TBemail.Text), Nothing, _
                                                                               Nothing, True, status)
                                If newUser Is Nothing Then
                                    msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), GetErrorMessage(status))
                                Else
                                    Roles.AddUserToRole(Login, "Alumno")
                                End If
                            Catch ex As MembershipCreateUserException
                                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
                            End Try
                        Else
                            'Ahora ingreso el registro del Alumno, como se decidió no registrarse en la tabla de seguridad, aquí no grabo la FechaIngreso
                            SDSalumnos.InsertCommand = "SET dateformat dmy; INSERT INTO Alumno (IdUsuario,IdGrupo,IdPlantel,Nombre,ApePaterno,ApeMaterno,Matricula,Equipo,Subequipo,Estatus,FechaModif) VALUES (" + CStr(IdUsuario) + "," + IdGrupo + "," + CStr(IdPlantel) + ",'" + Nombre + "','" + ApPaterno + "','" + ApMaterno + "','" + Matricula + "','" + Equipo + "','" + Subequipo + "','Activo',getdate())"
                            SDSalumnos.Insert()
                        End If

                        'Pongo los datos del alumno creado en la pantalla
                        LblSalida.Text = LblSalida.Text + "<tr><td>" + Nombre + "</td><td>" + ApPaterno + "</td><td>" + ApMaterno + "</td><td>" + Matricula + "</td><td>" + Equipo + "</td><td>" + Subequipo + "</td><td>" + Login + "</td><td>" + Password + "</td><td>" + Grupo + "</td><td>" + Grado + "</td><td>" + Plantel + "</td></tr>"
                    End While
                    LblSalida.Text = LblSalida.Text + "</table>"

                    objConexion.Close()
                    leer_archivo.Close()
                    msgError.hide()
                    LblMensaje.Text = "La migración ha terminado con éxito, revise los registros.<BR>"

                    'Elimino el archivo utilizado para la migración
                    Dim MiArchivo As FileInfo = New FileInfo(archivo_datos)
                    MiArchivo.Delete()

                    If CBregistrarme.Checked Then
                        LblMensaje.Text = LblMensaje.Text + "Las cuentas de los " & Config.Etiqueta.ALUMNOS & " migrados ya han sido registradas en la base de datos de seguridad"
                    Else
                        LblMensaje.Text = LblMensaje.Text + "Los " & Config.Etiqueta.ALUMNOS & " migrados deberán registrarse con su Matrícula y Login para activar su cuenta"
                    End If
                End If 'de Todobien
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                LblMensaje.Text = ""
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try
        Else
            msgError.show("Seleccione el archivo que contiene los datos para Migrar")
        End If
    End Sub

    Public Function GetErrorMessage(ByVal status As MembershipCreateStatus) As String
        Select Case status
            Case MembershipCreateStatus.DuplicateUserName
                Return "El usuario ya existe, elija otro usuario."
            Case MembershipCreateStatus.DuplicateEmail
                Return "Ya existe un usuario para ese email, por favor escriba un email diferente."
            Case MembershipCreateStatus.InvalidPassword
                Return "El password es inválido, pruebe con otro."
            Case MembershipCreateStatus.InvalidEmail
                Return "El formato del email es inválido, por favor revíselo."
            Case MembershipCreateStatus.InvalidAnswer
                Return "La respuesta secreta es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidQuestion
                Return "La pregunta de recuperación de password es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidUserName
                Return "El nombre de usuario es inválido, corríjalo."
            Case MembershipCreateStatus.ProviderError
                Return "La autentificación falló, contacte al administrador del sistema."
            Case MembershipCreateStatus.UserRejected
                Return "La creación del usuario ha sido cancelada, consulte al administrador."
            Case Else
                Return "Ha ocurrido un error desconocido, verifique o contacte al administrador."
        End Select
    End Function

End Class
