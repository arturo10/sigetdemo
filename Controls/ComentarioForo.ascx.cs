﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using Siget.Utils;

public partial class Controls_ComentarioForo : System.Web.UI.UserControl
{

  public long IdComentarioEvaluacion;
  public int IdEvaluacion;
  public long IdAlumno = -1;
  public long IdProfesor = -1;
  public long IdEvaluacionT = -1;
  public DateTime Fecha; // fecha en la que fue publicado; aparece a la derecha del nombre completo
  public Boolean EsCalificable = false; // indica si aparecen botones de calificar
  public Boolean EsEditable = false; // indica si aparecen botones de editar/borrar
  public Boolean EsReplicable = false; //indica si aparece el botón para generar una replica para el comentario
  public Boolean hasComments = false; // indica si tiene comentarios anidados o relacionados
  public Boolean isSecondReply = false;
  public string NombreUsuario; // nombre completo que aparece en la parte superior
  public string Comentario; // contenido principal dle comentario
  public int Calificacion = -1; // calificación asignada al comentario por algún profesor
  public string ArchivoAdjunto = ""; // nombre del adjunto que el usuario escribió
  public string ArchivoAdjuntoFisico = ""; // nombre con hash aleatorio para evitar colisiones en el FS
  public string Usuario = ""; // Login del usuario que comentó
  public Panel phrContenedorPadre; // referencia al placeholder que tiene este control


  public void Page_Load(object sender, EventArgs e)
  {
    MuestraPropiedades();
  }

  /// <summary>
  /// Los controles de usuario tienden a cargarse después de la página donde se usan, 
  /// por lo que se necesita un inicializador para mostrar los valores de las propiedades, 
  /// al cargar el control en sí (Page_Load)
  /// </summary>
  public void MuestraPropiedades()
  {
    if (IdProfesor != -1)
    {
      Image1.Attributes.Add("title", Siget.Lang.Etiqueta.Profesor[Session["Usuario_Idioma"].ToString()]);
      PhrProfesorImg.Visible = true;
    }
    else
    {
        if (FileUtils.getUrlProfileImage(MapPath("../Resources/profileImages/"), Usuario) != String.Empty)
            usrImage.ImageUrl = "../Resources/profileImages/" + FileUtils.getUrlProfileImage(MapPath("../Resources/profileImages/"), Usuario);
        else usrImage.ImageUrl = "../Resources/imagenes/blankUser.png";
    }

    // sólo es calificable cuando no es un comentario de profesor
    if (EsCalificable == true && IdAlumno != -1)
      PhrCalificar.Visible = true;

    if (EsEditable == true)
    {
      PhrModificar.Visible = true;
      commTableMain.Attributes.Add("style", "font-size: 0.9em; min-width: 600px; width: 100%; margin-bottom: 10px; background-color: #efefef; padding: 10px; border-left: 5px solid #999;");
    }

    if (hasComments || EsReplicable)
    {
        BtnReplicar.Visible = true;
    }

    if (isSecondReply)
    {
        commTableMain.Attributes.Add("style", "font-size: 0.9em; min-width:500px;width:80% ; margin-bottom: 10px; background-color:#888888; padding: 10px; border-left: 5px solid #999;text-align:right;");
        BtnReplicar.Visible = false;
        containerCommentsNested.Visible = false;
    }
    
    LbNombreUsuario.Text = NombreUsuario;

    LtComentario.Text = Comentario;

    if (Calificacion != -1)
    {
      LbCalificacion.Visible = true;
      LbCalificacion.Text = Calificacion.ToString();
      DdlCalificacion.SelectedValue = Calificacion.ToString();
      PhrModificar.Visible = false;
    }

    LtFecha.Text = Fecha.ToString();

    if (ArchivoAdjunto != "")
    {
      LbAdjunto.Text = ArchivoAdjunto;
      PhrAdjunto.Visible = true;
    }
  }


  public Panel panelComments { get{ return this.PhrCommentsRelated;}}
  public HtmlTable mainPanel { get { return this.commTableMain; } }

  protected void BtnBorrar_Click(object sender, EventArgs e)
  {
    if (ArchivoAdjunto != "")
    {
      try
      {
        FileInfo file = new FileInfo(Siget.Config.Global.rutaAdjuntos_fisica + Usuario + "\\" + ArchivoAdjunto);
        file.Delete();
      }
      catch (Exception ex)
      {
        Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
      }
    }

    try
    {
      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
      {
        conn.Open();
        using (SqlCommand cmd = new SqlCommand("DELETE FROM ComentarioEvaluacion WHERE IdComentarioEvaluacion = @IdComentarioEvaluacion", conn))
        {

          cmd.Parameters.AddWithValue("@IdComentarioEvaluacion", IdComentarioEvaluacion);

          cmd.ExecuteNonQuery();
        }
      }

      // avisa a la página contenedora que se borró un elemento
      System.Web.HttpContext.Current.Session["Comenta_Deleted"] = "";
    }
    catch (Exception ex)
    {
      Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
    }
  }

  protected void BtnEditar_Click(object sender, EventArgs e)
  {
    Session["Comenta_IdComentarioEvaluacion"] = IdComentarioEvaluacion;
    Session["Comenta_Comentario"] = Comentario;
    Session["Comenta_Origen"] = Context.Request.Url.AbsoluteUri;
    Session["Comenta_IdEvaluacion"] = IdEvaluacion;
    Session["Comenta_ArchivoAdjunto"] = ArchivoAdjunto;
    Session["Comenta_ArchivoAdjuntoFisico"] = ArchivoAdjuntoFisico;
    if (IdProfesor != -1)
      Response.Redirect("~/profesor/comentaForo/");
    else
      Response.Redirect("~/alumno/cursos/actividad/comenta/");
  }

  protected void LbAdjunto_Click(object sender, EventArgs e)
  {
    System.IO.FileInfo file = new System.IO.FileInfo(Siget.Config.Global.rutaCargas + Usuario + "\\" + ArchivoAdjuntoFisico);

    if (file.Exists) // set appropriate headers
    {
      Response.Clear();
      Response.AddHeader("Content-Disposition", "attachment; filename=\"" + ArchivoAdjunto + "\"");
      Response.AddHeader("Content-Length", file.Length.ToString());
      Response.ContentType = "application/octet-stream";
      Response.WriteFile(file.FullName);
      // Response.End()
    }
    else
      LbAdjunto.Text = Siget.Lang.FileSystem.Alumno.Mensaje.ERR_ADJUNTO_NO_DISPONIBLE[Session["Usuario_Idioma"].ToString()];
    // nothing in the URL as HTTP GET
  }

  protected void BtnCalificar_Click(object sender, EventArgs e)
  {
    try
    {
      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
      {
        conn.Open();
        using (SqlCommand cmd = new SqlCommand("Califica_ActividadForo", conn))
        {
          cmd.CommandType = System.Data.CommandType.StoredProcedure;

          cmd.Parameters.AddWithValue("@IdComentarioEvaluacion", IdComentarioEvaluacion);
          cmd.Parameters.AddWithValue("@Calificacion", DdlCalificacion.SelectedValue);

          cmd.ExecuteNonQuery();
        }
      }

      System.Web.HttpContext.Current.Session["Comm_Calificacion"] = DdlCalificacion.SelectedValue;
      System.Web.HttpContext.Current.Session["Comm_IdAlumno"] = IdAlumno;
    }
    catch (Exception ex)
    {
      Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
    }
  }
  protected void BtnReplicar_Click(object sender, EventArgs e)
  {
      Session["Comenta_IdComentarioRelacionado"] = IdComentarioEvaluacion;
      Session["Comenta_Comentario"] = Comentario;
      Session["Comenta_Origen"] = Context.Request.Url.AbsoluteUri;
      Session["Comenta_IdEvaluacion"] = IdEvaluacion;
      Session["Comenta_ArchivoAdjunto"] = ArchivoAdjunto;
      Session["Comenta_ArchivoAdjuntoFisico"] = ArchivoAdjuntoFisico;
      if (IdProfesor != -1)
          Response.Redirect("~/profesor/comentaForo/");
      else
          Response.Redirect("~/alumno/cursos/actividad/comenta/");
  }
  protected void containerCommentsNested_Click(object sender, EventArgs e)
  {
      if (PhrCommentsRelated.Visible)
      {
          PhrCommentsRelated.Visible = false;
          ImageNestedButton.ImageUrl = "~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/bullet_toggle_plus.png";
      }
      else {
          ImageNestedButton.ImageUrl = "~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/bullet_toggle_minus.png";
          PhrCommentsRelated.Visible = true;
      }



        
  }
}