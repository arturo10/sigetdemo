﻿<%@ Control Language="VB" 
    AutoEventWireup="false" %>

<asp:Panel ID="pnlControl" runat="server" 
    CssClass="messageContainer" 
    Visible="false">
    <div class="messageBox errorMessage">
        <div class="controlImage">
            <asp:Image ID="Image1" runat="server"
                ImageUrl="~/Resources/imagenes/btnicons/woocons/Stop_2.png" />
        </div>
        <div class="controlText">
            <h3>
                <asp:Literal ID="litTitle" runat="server" />
            </h3>
            <asp:Literal ID="litMessage" runat="server" />
        </div>
        <div class="controlClose">
            <asp:ImageButton ID="closeButton" runat="server" 
                ImageUrl="~/Resources/imagenes/x.png" 
                OnClick="closeButton_Click" />
        </div>
        <div style="clear:both;"></div>
    </div>
</asp:Panel>

<script runat="server">
    ''' <summary>
    ''' Permite manejar el cierre del mensaje desde una página que utilice el control
    ''' </summary>
    Public Event MessageClosed As EventHandler

    ''' <summary>
    ''' Muestra el control utilizando los parámetros como título y cuerpo de mensaje
    ''' </summary>
    ''' <param name="title">Título a mostrar, en mayúsculas</param>
    ''' <param name="message">Contenidos del mensaje</param>
    Public Sub show(
                ByVal title As String,
                ByVal message As String)
        litTitle.Text = title
        litMessage.Text = message
        pnlControl.Visible = True
    End Sub

    ''' <summary>
    ''' Muestra el control utilizando el parámetro cono cuerpo de mensaje, sin título
    ''' </summary>
    ''' <param name="message">Contenidos del mensaje</param>
    Public Sub show(
                ByVal message As String)
        litTitle.Text = ""
        litMessage.Text = message
        pnlControl.Visible = True
    End Sub

    ''' <summary>
    ''' Borra el título y el mensaje, y esconde el control
    ''' </summary>
    Public Sub hide()
        pnlControl.Visible = False
        litMessage.Text = ""
        litTitle.Text = ""
    End Sub

    ''' <summary>
    ''' Devuelve el texto contenido en el mensaje del control
    ''' </summary>
    Public Function getText() As String
        If Not IsNothing(litMessage.Text) Then
            Return litMessage.Text
        Else
            Return ""
        End If
    End Function

    ''' <summary>
    ''' Devuelve el texto contenido en el título del control
    ''' </summary>
    Public Function getTitle() As String
        If Not IsNothing(litTitle.Text) Then
            Return litTitle.Text
        Else
            Return ""
        End If
    End Function
    
    ''' <summary>
    ''' Indica si el control está visible
    ''' </summary>
    Public Function isActive() As Boolean
        If pnlControl.Visible = False Then
            Return False
        Else
            Return True
        End If
    End Function
    
    ''' <summary>
    ''' Esconde el control, y levanta un evento que puede ser manejado
    ''' desde la forma donde se utiliza el control
    ''' </summary>
    Protected Sub closeButton_Click(
                    sender As Object,
                    e As ImageClickEventArgs)
        hide()
        
        RaiseEvent MessageClosed(sender, EventArgs.Empty)
    End Sub
    
</script>