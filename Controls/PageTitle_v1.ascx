﻿<%@ Control Language="VB" 
    AutoEventWireup="false" %>

<asp:Panel ID="pnlControl" runat="server" 
    Visible="false">
    <table style="margin-left: auto; margin-right: auto; margin-top: 10px; margin-bottom: 15px;">
        <tr>
            <td style="text-align: center;">
                <div style="border-bottom: 2px solid #333; padding-bottom: 5px; padding-left: 30px; padding-right: 30px;">
                    <asp:Image ID="titleImage" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/images.png"
                        CssClass="menuBarIcon" width="30"/>
                    <span style="font-size: 16px; font-weight: bold;">
                        <asp:Literal ID="ltTitulo" runat="server" 
                            Text="[Título de Página]" />
                    </span>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>

<script runat="server">

    ''' <summary>
    ''' Muestra el control usando los parámetros como título y dirección de ícono
    ''' </summary>
    ''' <param name="title">Título a mostrar</param>
    ''' <param name="imageVirtualURL">dirección del ícono a utilizar</param>
    Public Sub show(ByRef title As String, ByRef imageVirtualURL As String)
        ltTitulo.Text = title
        titleImage.ImageUrl = imageVirtualURL
        pnlControl.Visible = True
    End Sub

    ''' <summary>
    ''' Permite establecer un nuevo ícono para el control
    ''' </summary>
    ''' <param name="imageVirtualURL">Dirección del ícono a utilizar</param>
    Public Sub setImage(ByRef imageVirtualURL As String)
        titleImage.ImageUrl = imageVirtualURL
    End Sub

    ''' <summary>
    ''' Borra el título, y esconde el control
    ''' </summary>
    Public Sub hide()
        ltTitulo.Text = ""
        pnlControl.Visible = False
    End Sub

    ''' <summary>
    ''' Devuelve el texto contenido en el título del control
    ''' </summary>
    ''' <returns></returns>
    Public Function getTitle() As String
        If Not IsNothing(ltTitulo.Text) Then
            Return ltTitulo.Text
        Else
            Return ""
        End If
    End Function
    
    ''' <summary>
    ''' Indica si el control está visible
    ''' </summary>
    ''' <returns></returns>
    Public Function isActive() As Boolean
        If pnlControl.Visible = False Then
            Return False
        Else
            Return True
        End If
    End Function
    
</script>