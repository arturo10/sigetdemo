﻿Imports Siget


Partial Class EnMantenimiento
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Utils.Bloqueo.sistemaBloqueado() Then
            Response.Redirect("~/Default.aspx")
        End If

        Label1.Text = Config.Etiqueta.SISTEMA_CORTO
    End Sub
End Class
