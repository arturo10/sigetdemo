﻿$(function () {
    $('#ShowPassword').on('click', function () {
        var type = $('#Password').attr('type');
        if ($(this).is(":checked")) {
            $('#Password').attr('type', 'text');
        }
        else {
            $('#Password').attr('type', 'password');
        }
    });
});