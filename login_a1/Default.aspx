﻿<%@ Page Language="C#"
  AutoEventWireup="true"
  CodeFile="Default.aspx.cs"
  Inherits="_Default" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <meta http-equiv="Content-Type" content="text/html; charset=utf- " />
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <meta name="viewport" content="width=device-width" />
  <title></title>

  <link rel="shortcut icon" type="image/x-icon" href="~/Resources/imagenes/favicon.ico" />

  <!-- Estilos específicos de fuentes -->
  <link id="css2" rel="stylesheet" runat="server" media="screen" href="~/Resources/css/Fonts.css" />

  <!-- Estilos específicos de botones -->
  <link id="css3" rel="stylesheet" runat="server" media="screen" href="~/Resources/css/Buttons.css" />
  <link id="Link2" rel="stylesheet" runat="server" media="screen" href="~/Resources/css/Controles.css" />
  <link id="Link1" rel="stylesheet" runat="server" media="screen" href="login.css" />
  <!-- Estilos específicos de grids -->
  <link id="css4" rel="stylesheet" runat="server" media="screen" href="~/Resources/css/DataGrids.css" />

  <!-- Estilos específicos de cuadros de mensajes -->
  <link id="css5" rel="stylesheet" runat="server" media="screen" href="~/Resources/css/MessageBoxes.css" />

  <!-- Estilos específicos de progressbar giratoria -->
  <link id="css6" rel="stylesheet" runat="server" media="screen" href="~/Resources/css/LoadingSpinner.css" />

  <script type="text/javascript" src="../Resources/scripts/css3-mediaqueries.js"></script>

  <link href="~/Resources/css/general_5.1.0_a.css" rel="stylesheet" type="text/css" media="all" />

  <link href="~/Resources/css/alumno_5.1.0_a.css" rel="stylesheet" type="text/css" media="all" />
  <link id="css7" rel="stylesheet" runat="server"  href="~/Resources/css/font-awesome.min.css" />


  <script type="text/javascript" src="../Resources/scripts/jquery-1.11.2.min.js"></script>

  <script type="text/javascript" src="LoginTools.js"></script>
  <asp:Literal ID="ltEstilos" runat="server"></asp:Literal>

  <asp:Literal ID="pnlHeader" runat="server"></asp:Literal>
</head>
<body style="min-width: 1200px;">
  <form id="form1" runat="server">
    <div id="header-wrapper">
      <div id="header" class="container">

        <div id="menuBlock">
          <ul>
            <asp:PlaceHolder ID="phrBoton1" runat="server" Visible="false" >
              <li>
                <asp:HyperLink ID="HlBoton1" runat="server"
                  Target="_blank">
                  <asp:Image ID="Image1" runat="server"
                    ImageUrl="~/Resources/Customization/imagenes/boton1_bare.png"
                    CssClass="menuBarIcon" />
                  <asp:Literal ID="LtBoton1" runat="server">INFORMACIÓN</asp:Literal>
                </asp:HyperLink>
              </li>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phrBoton2" runat="server"  Visible="false" >
              <li>
                <asp:HyperLink ID="HlBoton2" runat="server"
                  Target="_blank">
                  <asp:Image ID="Image2" runat="server"
                    ImageUrl="~/Resources/Customization/imagenes/boton2_bare.png"
                    CssClass="menuBarIcon" />
                  <asp:Literal ID="LtBoton2" runat="server">GUIA INGRESO</asp:Literal>
                </asp:HyperLink>
              </li>
            </asp:PlaceHolder>
            <li>
              <asp:HyperLink ID="HlBoton3" runat="server"  Visible="false"
                Target="_blank">
                <asp:Image ID="Image3" runat="server"
                  ImageUrl="~/Resources/Customization/imagenes/boton3_bare.png"
                  CssClass="menuBarIcon" />
                <asp:Literal ID="LtBoton3" runat="server">GUIA DE USUARIO</asp:Literal>
              </asp:HyperLink>
            </li>
            <li>
              <asp:HyperLink ID="HlBoton4" runat="server"  Visible="false"
                Target="_blank">
                <asp:Image ID="Image4" runat="server"
                  ImageUrl="~/Resources/Customization/imagenes/boton4_bare.png"
                  CssClass="menuBarIcon" />
                <asp:Literal ID="LtBoton4" runat="server">SOPORTE TÉCNICO</asp:Literal>
              </asp:HyperLink>
            </li>
            

          </ul>
        </div>
      </div>
    </div>

    <div class="contenedorPrueba">
      <div style="color: #000; height: 500px; width: 100%; overflow: hidden; max-width: 2500px; min-height: 500px;">
        <asp:Image ID="Image5" runat="server"
          ImageUrl="~/Resources/customization/imagenes/fondoLogin.png"
          CssClass="menuBarIcon" Height="100%" Width="100%" />
        <div id="acessbox">
          <div id="imgPrincipal">
            <asp:HyperLink runat="server" ID="hlClientLogo"
              Target="_blank">
                            <asp:Image runat="server"  
                              style="max-height: 90px;"
                              CssClass="boxlogoEmpresa"
                              ImageUrl="~/Resources/Customization/imagenes/clientLogo_250x70.png" />
            </asp:HyperLink>
          </div>
          <div id="formLogin">
            <asp:TextBox ID="Login" runat="server"
              CssClass="tbLogin tbUsername"></asp:TextBox>

            <asp:TextBox ID="Password" ClientIDMode="Static" runat="server"
              CssClass="tbLogin tbPassword"
              TextMode="Password"></asp:TextBox>

            <div id="areaCheckboxes">
              <asp:CheckBox ID="ShowPassword" ClientIDMode="Static" runat="server"
                CssClass="checkboxLogin" />
              <asp:Label ID="LblMostrarCaracteres" runat="server"
                CssClass="lbChbArea">Mostrar Caracteres</asp:Label>
              <br />
              <asp:CheckBox runat="server" ID="RememberMe" CssClass="checkboxLogin" />
              <asp:Label ID="LblRecordarUsuario" runat="server"
                CssClass="lbChbArea">Recordar Usuario</asp:Label>
            </div>
            <asp:Label ID="LblError" runat="server" CssClass="loginError"></asp:Label>
          </div>
          <asp:Button ID="BtnLogin" runat="server"
            CssClass="btnLoginConnected"
            OnClick="BtnLogin_Click" Text="Iniciar Sesión" />
            <div  id="registerContainer" class="width-register-cont">
                 <asp:HyperLink ID="RegisterRedirect" style="color: black; text-decoration:none;" runat="server" NavigateUrl="../Register">
              <i class="fa fa-pencil-square fa-lg"></i>
                <asp:Literal ID="Literal1" runat="server">Registrarse </asp:Literal>
              </asp:HyperLink>
            </div>
               
        </div>
        <asp:PlaceHolder ID="phrPublicMessage" runat="server" Visible="false">
          <div id="publicMessage">
            <asp:Label ID="LblPublicMessage" runat="server"></asp:Label>
          </div>
        </asp:PlaceHolder>
      </div>
    </div>

    <div class="footer">
      <table style="margin-left: auto; margin-right: auto; font-size: x-small; margin-bottom: 50px; margin-top: 20px; border-top: 1px solid #999;">
        <tr>
          <td style="vertical-align: top;">
            <table>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <div style="width: 30px; height: 30px; display: inline-block;">
                    <asp:Image ID="imgLogoIntegrantFooter" runat="server"
                      ImageUrl="~/Resources/Customization/imagenes/footerLogo.png"
                      Height="30" Width="30" />
                  </div>
                </td>
                <td>
                  <div style="display: inline-block; text-align: left; margin-left: 10px; font-size: x-small;">
                    <span>
                      <asp:Literal ID="lt_rightsReserved" runat="server" Text="[Todos los derechos reservados]" />
                      &#174; 2015<br />
                      <asp:Literal ID="LtProyecto" runat="server">Sistema de Gestión del Talento</asp:Literal>
                      <br />
                      <asp:Literal ID="LtCompany" runat="server">Estrategia y Desarrollo Organizacional S.C.</asp:Literal>
                    </span>
                  </div>
                </td>
                <td>&nbsp;&nbsp;&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>

    </div>
  </form>
</body>
</html>
