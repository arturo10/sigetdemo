﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Configuration;

using Siget;
using System.Text;

public partial class _Default : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (Siget.Utils.Bloqueo.sistemaBloqueado())
    {
      FormsAuthentication.SignOut();
      Response.Redirect("~/EnMantenimiento.aspx");
    }

    // redirige a la página de login configurada
    redirigeLogin();

    if (!IsPostBack)
    {
      // manda el focus a el texbox de username
      Login.Focus();

      if (Request.Cookies["RememberUser"] != null)
      {
        Login.Text = Request.Cookies["RememberUser"].Value;
        RememberMe.Checked = true;
      }
      // Personaliza la página con los colores personalizados
      OutputCSS();

      // aplica lenguaje
      aplicaLenguaje();

      // personaliza enlaces y apariencia configurada
      renderConfig();

      Siget.Utils.DesbloquearUsuarios.Revisa_Desbloquea();
    }
  }

  /// <summary>
  /// Redirige a la página de inicio de sesión que esté configurada en Global.
  /// </summary>
  protected void redirigeLogin()
  {
    if (Siget.Config.Global.Login_Screen == 0)
      Response.Redirect("~/login_jp1/");

    if (User.IsInRole("sysadmin"))
    {
      Session["Rol"] = "sysadmin";        
      Response.Redirect("~/administrador/");
    }
    else if (User.IsInRole("superadministrador"))
    {
      Session["Rol"] = "superadministrador";
      Response.Redirect("~/administrador/");
    }
    else if (User.IsInRole("alumno"))
    {
      Session["Rol"] = "alumno";
      Response.Redirect("~/alumno/");
    }
    else if (User.IsInRole("profesor"))
    {
      Session["Rol"] = "profesor";
      Response.Redirect("~/profesor/");
    }
    else if (User.IsInRole("coordinador"))
    {
      Session["Rol"] = "coordinador";
      Response.Redirect("~/coordinador/");
    }
    else if (User.IsInRole("operador"))
    {
      Session["Rol"] = "operador";
      Response.Redirect("~/operador/");
    }
    else if (User.IsInRole("capturista"))
    {
      Session["Rol"] = "capturista";
      Response.Redirect("~/capturista/");
    }
  }

  /// <summary>
  /// Inyecta los estilos css con los colores configurados en Config.Color
  /// </summary>
  protected void OutputCSS()
  {
  }

  /// <summary>
  /// Aplica el idioma configurado a los controles estáticos de la página
  /// </summary>
  protected void aplicaLenguaje()
  {
    Login.Attributes.Add("placeholder", Siget.Lang.FileSystem.Home.LOGIN_USERNAME[Siget.Config.Global.Idioma_General]);
    Password.Attributes.Add("placeholder", Siget.Lang.FileSystem.Home.LOGIN_PASSWORD[Siget.Config.Global.Idioma_General]);
    LblMostrarCaracteres.Text = Siget.Lang.FileSystem.Home.LOGIN_SHOW_PASSWORD[Siget.Config.Global.Idioma_General];
    LblRecordarUsuario.Text = Siget.Lang.FileSystem.Home.LOGIN_REMEMBER_ME[Siget.Config.Global.Idioma_General];
    BtnLogin.Text = Siget.Lang.FileSystem.Home.LOGIN_BUTTON[Siget.Config.Global.Idioma_General];
    lt_rightsReserved.Text = Siget.Lang.FileSystem.Home.LT_RIGHTS_RESERVED[Siget.Config.Global.Idioma_General];
    LtProyecto.Text = Siget.Lang.Etiqueta.Sistema_Largo[Siget.Config.Global.Idioma_General];
    LtCompany.Text = Siget.Lang.Etiqueta.Nombre_Empresa[Siget.Config.Global.Idioma_General];
    LtBoton3.Text = Siget.Lang.FileSystem.Home.Lt_GuiaUsuario[Siget.Config.Global.Idioma_General];
    LtBoton4.Text = Siget.Lang.FileSystem.Home.Lt_SoporteTecnico[Siget.Config.Global.Idioma_General];
  }

  /// <summary>
  /// Configura la apariencia y enlaces de la página de acuerdo a la configuración almacenada en Config.PaginaInicio
  /// </summary>
  protected void renderConfig()
  {
    if (Siget.Config.PaginaInicio.AvisoPublico != "")
    {
      if (Siget.Config.PaginaInicio.AvisoPublico_FechaInicio < DateTime.Now &&
        Siget.Config.PaginaInicio.AvisoPublico_FechaFin > DateTime.Now)
      {
        LblPublicMessage.Text = Siget.Config.PaginaInicio.AvisoPublico;
        phrPublicMessage.Visible = true;
      }
    }

    if (Siget.Config.PaginaInicio.Boton1_Tipo != -1)
    {
      LtBoton1.Text = Siget.Config.PaginaInicio.Boton1_Texto;
      phrBoton1.Visible = true;
      HlBoton1.NavigateUrl = Siget.Config.PaginaInicio.Boton1_Objetivo;
    }

    if (Siget.Config.PaginaInicio.Boton2_Tipo != -1)
    {
      LtBoton2.Text = Siget.Config.PaginaInicio.Boton2_Texto;
      phrBoton2.Visible = true;
      HlBoton2.NavigateUrl = Siget.Config.PaginaInicio.Boton2_Objetivo;
    }

    if (Siget.Config.PaginaInicio.Boton3_Tipo != -1)
    {
        HlBoton3.Visible = true;
      HlBoton3.NavigateUrl = Siget.Config.PaginaInicio.Boton3_Objetivo;
    }

    if (Siget.Config.PaginaInicio.Boton4_Tipo != -1)
    {
        HlBoton4.Visible = true;
      HlBoton4.NavigateUrl = Siget.Config.PaginaInicio.Boton4_Objetivo;
    }

    if (Siget.Config.PaginaInicio.LogoCliente_Tipo != -1)
    {
      hlClientLogo.NavigateUrl = Siget.Config.PaginaInicio.LogoCliente_Objetivo;
    }

  }

  /// <summary>
  /// Intento de inicio de sesión por el usuario.
  /// </summary>
  protected void BtnLogin_Click(object sender, EventArgs e)
  {
    if (Membership.ValidateUser(Login.Text, Password.Text))
    {
      rememberUser();
      FormsAuthentication.SetAuthCookie(Login.Text, true);
      FormsAuthentication.RedirectFromLoginPage(Login.Text, false);
    }
    else
    {
      LblError.Text = Siget.Lang.FileSystem.Home.LOGIN_FAILURE[Siget.Config.Global.Idioma_General];
      Password.Focus();
    }
  }

  /// <summary>
  /// Establece una cookie para recordar el usuario 
  /// </summary>
  protected void rememberUser()
  {
    if (RememberMe.Checked)
    {
      Response.Cookies["RememberUser"].Value = Login.Text;
      Response.Cookies["RememberUser"].Expires = DateTime.Now.AddDays(30);
    }
    else
    {
      Response.Cookies["RememberUser"].Expires = DateTime.Now.AddDays(-1D);
    }
  }
}