﻿Imports Siget


Partial Class Bloqueo
    Inherits System.Web.UI.MasterPage

    Protected Sub OutputCSS()
    Literal1.Text = "" &
            "<style type='text/css'>" &
                    "body {" &
                            "background-image: url(/" & Config.Global.NOMBRE_FILESYSTEM & "/Resources/Customization/imagenes/watermark.png);" &
                    "}" &
            "</style>"

    Literal2.Text = "" &
            "<style type='text/css'>" &
                    "#title {" &
                            "background: " & Config.Color.TituloPrincipal_Bottom & ";" &
                            "background-image: -webkit-gradient(linear, left top, left bottom, from(" &
                                            Config.Color.TituloPrincipal_Top & "), to(" &
                                            Config.Color.TituloPrincipal_Bottom & ")); /* Saf4+, Chrome */" &
                            "background-image: -webkit-linear-gradient(top, " &
                                            Config.Color.TituloPrincipal_Top & ", " &
                                            Config.Color.TituloPrincipal_Bottom & "); /* Chrome 10+, Saf5.1+ */" &
                            "background-image: -moz-linear-gradient(top, " &
                                            Config.Color.TituloPrincipal_Top & ", " &
                                            Config.Color.TituloPrincipal_Bottom & "); /* FF3.6+ */" &
                            "background-image: -ms-linear-gradient(top, " &
                                            Config.Color.TituloPrincipal_Top & ", " &
                                            Config.Color.TituloPrincipal_Bottom & "); /* IE10 */" &
                            "background-image: -o-linear-gradient(top, " &
                                            Config.Color.TituloPrincipal_Top & ", " &
                                            Config.Color.TituloPrincipal_Bottom & "); /* Opera 11.10+ */" &
                            "background-image: linear-gradient(top, " &
                                            Config.Color.TituloPrincipal_Top & ", " &
                                            Config.Color.TituloPrincipal_Bottom & "); /* W3C */" &
                            "color: white;" &
                    "}" &
            "</style>"

    Literal3.Text = "" &
            "<style type='text/css'>" &
                    ".loginButton {" &
                            "width: 120px;" &
                            "float: left;" &
                            "position: relative;" &
                            "display: inline-block;" &
                            "text-align: center;" &
                            "border: 2px solid #780404;" &
                            "-moz-border-radius: 4px;" &
                            "-webkit-border-radius: 4px;" &
                            "border-radius: 4px;" &
                            "font-weight: 700;" &
                            "cursor: pointer;" &
                            "font-size: 0.8em;" &
                            "padding-bottom: 5px;" &
                            "padding-top: 5px;" &
                            "background: #D83434;" &
                            "background-image: url(" & Config.Global.urlImagenes & "btnLogin_back.png); /* fallback */" &
                            "background-repeat: repeat-x;" &
                            "background-image: -webkit-gradient(linear, left top, left bottom, from(#F85454), to(#D83434)); /* Saf4+, Chrome */" &
                            "background-image: -webkit-linear-gradient(top, #F85454, #D83434); /* Chrome 10+, Saf5.1+ */" &
                            "background-image: -moz-linear-gradient(top, #F85454, #D83434); /* FF3.6+ */" &
                            "background-image: -ms-linear-gradient(top, #F85454, #D83434); /* IE10 */" &
                            "background-image: -o-linear-gradient(top, #F85454, #D83434); /* Opera 11.10+ */" &
                            "background-image: linear-gradient(top, #F85454, #D83434); /* W3C */" &
                    "}" &
                            ".loginButton:hover {" &
                                    "background: #C81414;" &
                                    "background-image: url(" & Config.Global.urlImagenes & "btnLogin_back_over.png); /* fallback */" &
                                    "background-repeat: repeat-x;" &
                                    "background-image: -webkit-gradient(linear, left top, left bottom, from(#E83434), to(#C81414)); /* Saf4+, Chrome */" &
                                    "background-image: -webkit-linear-gradient(top, #E83434, #C81414); /* Chrome 10+, Saf5.1+ */" &
                                    "background-image: -moz-linear-gradient(top, #E83434, #C81414); /* FF3.6+ */" &
                                    "background-image: -ms-linear-gradient(top, #E83434, #C81414); /* IE10 */" &
                                    "background-image: -o-linear-gradient(top, #E83434, #C81414); /* Opera 11.10+ */" &
                                    "background-image: linear-gradient(top, #E83434, #C81414); /* W3C */" &
                            "}" &
            "</style>"
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        LblFecha.Text = DateAndTime.Now
        LabelA.Text = Config.Etiqueta.SISTEMA_CORTO
        OutputCSS()
    End Sub
End Class

