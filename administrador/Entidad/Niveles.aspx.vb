﻿
Imports Siget
Imports System.IO
Imports Siget.Beans

Partial Class administrador_AgruparReactivos
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GVnivelesasignados.Caption = "<h3>" &
            Config.Etiqueta.NIVELES &
            " asignad" & Config.Etiqueta.LETRA_NIVEL & "s</h3>"

        TBFechaMod.Text = DateTime.Now

       
    End Sub

    Protected Sub Asignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Asignar.Click
        Try
            SDSescuelas.InsertCommand = "SET dateformat dmy; INSERT INTO Escuela (IdPlantel,IdNivel) VALUES (" + DDLplantel.SelectedValue + "," + DDLnivel.SelectedValue + ")"
            SDSescuelas.Insert()
            CType(New MessageSuccess(Asignar, Me.GetType, "El registro ha sido guardado"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(Asignar, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try
    End Sub

    Protected Sub GVNivelesGenerales_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVNivelesGenerales.RowCreated
        Dim cociente As Integer = (GVNivelesGenerales.PageCount / 15)
        Dim moduloIndex As Integer = (GVNivelesGenerales.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVNivelesGenerales.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVNivelesGenerales.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVNivelesGenerales.PageIndex
                final = IIf((inicial + 15 <= GVNivelesGenerales.PageCount), inicial + 15, GVNivelesGenerales.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVNivelesGenerales.PageCount, inicial + 15, GVNivelesGenerales.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVNivelesGenerales.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If
    End Sub

    Protected Sub GVNivelesGenerales_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVNivelesGenerales.RowDataBound

        Dim cociente As Integer = (GVNivelesGenerales.PageCount) / 15
        Dim moduloIndex As Integer = (GVNivelesGenerales.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVNivelesGenerales.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVNivelesGenerales.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVNivelesGenerales.PageIndex
            final = IIf((inicial + 15 <= GVNivelesGenerales.PageCount), inicial + 15, GVNivelesGenerales.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVNivelesGenerales.PageCount), inicial + 15, GVNivelesGenerales.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVNivelesGenerales, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVNivelesGenerales, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Institucion"), 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Plantel"), 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.NIVEL & " a asignar", 0))
    End Sub

    Protected Sub Desasignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Desasignar.Click
        Try
            SDSescuelas.DeleteCommand = "Delete from Escuela where IdEscuela = " + GVnivelesasignados.SelectedValue.ToString
            SDSescuelas.Delete()
            GVnivelesasignados.DataBind()
            CType(New MessageSuccess(Asignar, Me.GetType, "El registro ha sido eliminado"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(Asignar, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try
    End Sub

    Protected Sub GVnivelesasignados_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVnivelesasignados.RowCreated

        Dim cociente As Integer = (GVnivelesasignados.PageCount / 15)
        Dim moduloIndex As Integer = (GVnivelesasignados.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVnivelesasignados.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVnivelesasignados.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVnivelesasignados.PageIndex
                final = IIf((inicial + 10 <= GVnivelesasignados.PageCount), inicial + 15, GVnivelesasignados.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVnivelesasignados.PageCount, inicial + 15, GVnivelesasignados.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVnivelesasignados.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub
   
    Protected Sub GVnivelesasignados_OnRowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVnivelesasignados.RowDataBound

        Dim cociente As Integer = (GVnivelesasignados.PageCount) / 15
        Dim moduloIndex As Integer = (GVnivelesasignados.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVnivelesasignados.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVnivelesasignados.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVnivelesasignados.PageIndex
            final = IIf((inicial + 15 <= GVnivelesasignados.PageCount), inicial + 15, GVnivelesasignados.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVnivelesasignados.PageCount), inicial + 15, GVnivelesasignados.PageCount)
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final
                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVnivelesasignados, "Page$" & counter.ToString)
            Next
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVnivelesasignados, "Select$" & e.Row.RowIndex)
        End If
    End Sub

    Protected Sub GVNivelesGenerales_OnRowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVNivelesGenerales.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVNivelesGenerales, "Select$" & e.Row.RowIndex)

        End If
    End Sub

    Protected Sub GVNivelesGenerales_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVNivelesGenerales.SelectedIndexChanged

        ActualizarNivel.Visible = True
        EliminarNivel.Visible = True
        TbDescripcionNivel.Text = HttpUtility.HtmlDecode(GVNivelesGenerales.SelectedDataKey("Descripcion").ToString)
        DDLEstatusNiveles.SelectedValue = HttpUtility.HtmlDecode(GVNivelesGenerales.SelectedDataKey("Estatus").ToString)
        TBFechaMod.Text = HttpUtility.HtmlDecode(GVNivelesGenerales.SelectedDataKey("FechaModif").ToString)
        TBModifico.Text = HttpUtility.HtmlDecode(GVNivelesGenerales.SelectedDataKey("Modifico").ToString)

    End Sub
  
    Protected Sub ActualizarNivel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ActualizarNivel.Click
        ' Por motivos de optimización conviene solo utilizar actualizar como actualización de info, no carga de archivos

        Try
            SDSniveles.UpdateCommand = "SET dateformat dmy; UPDATE Nivel SET Descripcion = '" + TbDescripcionNivel.Text + "', Estatus = '" + DDLEstatusNiveles.SelectedValue.ToString + "',FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdNivel =" + GVNivelesGenerales.SelectedDataKey("IdNivel").ToString
            SDSniveles.Update()
            CType(New MessageSuccess(ActualizarNivel, Me.GetType, "El registro ha sido actualizado."), MessageSuccess).show()
            GVNivelesGenerales.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(ActualizarNivel, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try


    End Sub

    Protected Sub EliminarNivel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles EliminarNivel.Click
        Try
            SDSniveles.DeleteCommand = "SET dateformat dmy; DELETE FROM Nivel WHERE IdNivel =" + GVNivelesGenerales.SelectedDataKey("IdNivel").ToString
            SDSniveles.Delete()
            CType(New MessageSuccess(EliminarNivel, Me.GetType, "El registro ha sido eliminado."), MessageSuccess).show()
            GVNivelesGenerales.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(EliminarNivel, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try


    End Sub

    Protected Sub InsertarNivel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles InsertarNivel.Click
        ' Por motivos de optimización conviene solo utilizar actualizar como actualización de info, no carga de archivos

        Try
            SDSniveles.InsertCommand = "SET dateformat dmy;INSERT INTO Nivel (Descripcion,Estatus,FechaModif,Modifico)  VALUES(@Descripcion,@Estatus,getdate(),@Modifico)"
            SDSniveles.InsertParameters.Add("Descripcion", TbDescripcionNivel.Text)
            SDSniveles.InsertParameters.Add("Estatus", DDLEstatusNiveles.SelectedValue)
            SDSniveles.InsertParameters.Add("Modifico", User.Identity.Name)
            SDSniveles.Insert()
            CType(New MessageSuccess(InsertarNivel, Me.GetType, "El registro ha sido insertado."), MessageSuccess).show()
            GVNivelesGenerales.DataBind()
            DDLNivelGrado.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(InsertarNivel, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try


    End Sub

    Protected Sub GVNivelesGenerales_DataBound(sender As Object, e As EventArgs) Handles GVNivelesGenerales.DataBound

        If GVNivelesGenerales.Rows.Count = 0 Then
            EliminarNivel.Visible = False
            ActualizarNivel.Visible = False
            GVNivelesGenerales.SelectedIndex = -1
        End If
    End Sub

    Protected Sub GVnivelesasignados_DataBound(sender As Object, e As EventArgs) Handles GVnivelesasignados.DataBound
        If GVnivelesasignados.Rows.Count = 0 Then
            Desasignar.Visible = False
            GVnivelesasignados.SelectedIndex = -1
        End If
    End Sub

    Protected Sub GVnivelesasignados_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVnivelesasignados.SelectedIndexChanged
        Desasignar.Visible = True
    End Sub


    Protected Sub Guardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        Try
            SDSgrados.InsertCommand = "SET dateformat dmy; INSERT INTO Grado (IdNivel,Descripcion,Estatus, FechaModif, Modifico) VALUES (" + DDLNivelGrado.SelectedValue + ", '" + TBdescripcion.Text + "','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
            SDSgrados.Insert()
            CType(New MessageSuccess(Insertar, Me.GetType, "El registro ha sido guardado."), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(Insertar, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try
    End Sub


    Protected Sub GVgradoscreados_OnRowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVgradoscreados.RowDataBound

        Dim cociente As Integer = (GVgradoscreados.PageCount) / 15
        Dim moduloIndex As Integer = (GVgradoscreados.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVgradoscreados.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVgradoscreados.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVgradoscreados.PageIndex
            final = IIf((inicial + 15 <= GVgradoscreados.PageCount), inicial + 15, GVgradoscreados.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVgradoscreados.PageCount), inicial + 15, GVgradoscreados.PageCount)
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVgradoscreados, "Page$" & counter.ToString)
            Next
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then

            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVgradoscreados, "Select$" & e.Row.RowIndex)

        End If
    End Sub

    Protected Sub GVgradoscreados_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVgradoscreados.RowCreated

        Dim cociente As Integer = (GVgradoscreados.PageCount / 15)
        Dim moduloIndex As Integer = (GVgradoscreados.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVgradoscreados.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVgradoscreados.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVgradoscreados.PageIndex
                final = IIf((inicial + 10 <= GVgradoscreados.PageCount), inicial + 15, GVgradoscreados.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVgradoscreados.PageCount, inicial + 15, GVgradoscreados.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVgradoscreados.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub DDLNivelGrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLNivelGrado.DataBound
        DDLNivelGrado.Items.Insert(0, New ListItem("---Elija el nivel", 0))
    End Sub

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        Try
            SDSgrados.DeleteCommand = "Delete from Grado where IdGrado = " + GVgradoscreados.SelectedDataKey("IdGrado").ToString 'Me da el IdGrado porque es el campo clave de la fila seleccionada
            SDSgrados.Delete()
            CType(New MessageSuccess(Eliminar, Me.GetType, "El registro ha sido eliminado"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(Eliminar, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try
    End Sub

    Protected Sub GVgradoscreados_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVgradoscreados.SelectedIndexChanged
        Eliminar.Visible = True
        Actualizar.Visible = True
        TBdescripcion.Text = HttpUtility.HtmlDecode(GVgradoscreados.SelectedDataKey("Descripcion").ToString)
        DDLestatus.SelectedValue = GVgradoscreados.SelectedDataKey("Estatus").ToString
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        Try
            SDSgrados.UpdateCommand = "SET dateformat dmy; UPDATE Grado set Descripcion = '" + TBdescripcion.Text + "', Estatus = '" + DDLestatus.SelectedValue + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdGrado = " + GVgradoscreados.SelectedDataKey("IdGrado").ToString    'Me da el IdGrado porque es el campo clave de la fila seleccionada
            SDSgrados.Update()
            CType(New MessageSuccess(Actualizar, Me.GetType, "El registro ha sido actualizado"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(Eliminar, Me.GetType, ex.Message.ToString()), MessageError).show()

        End Try
    End Sub

    Protected Sub GVgradoscreados_DataBound(sender As Object, e As EventArgs) Handles GVgradoscreados.DataBound
        If GVgradoscreados.Rows.Count = 0 Then
            Actualizar.Visible = False
            Eliminar.Visible = False
            GVgradoscreados.SelectedIndex = -1
        End If
    End Sub

  

End Class
