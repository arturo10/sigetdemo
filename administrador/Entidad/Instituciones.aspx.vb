﻿
Imports Siget

Imports System.Data.SqlClient

Partial Class Instituciones
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()
    End Sub

    Protected Sub gvInstituciones_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvInstituciones.SelectedIndexChanged
        msgError.hide()
        msgSuccess.hide()
        TBDescripcionInstitucion.Text = gvInstituciones.SelectedDataKey("Descripcion")
        tbObservaciones.Text = gvInstituciones.SelectedDataKey("Observaciones")
        ddlEstatus.SelectedValue = gvInstituciones.SelectedDataKey("Estatus")
        btnEliminar.Visible = True
        btnActualizar.Visible = True
    End Sub

    ' comprobación básica de los campos en la forma 
    Protected Sub verificaCampos()
        If String.IsNullOrEmpty(Trim(TBDescripcionInstitucion.Text)) Then
            Throw New System.Exception("Debe definir una descripción.")
        End If
    End Sub

    Protected Sub GVDatosPlanteles_OnRowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVDatosPlanteles.RowDataBound

        Dim cociente As Integer = (GVDatosPlanteles.PageCount) / 15
        Dim moduloIndex As Integer = (GVDatosPlanteles.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVDatosPlanteles.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVDatosPlanteles.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVDatosPlanteles.PageIndex
            final = IIf((inicial + 15 <= GVDatosPlanteles.PageCount), inicial + 15, GVDatosPlanteles.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVDatosPlanteles.PageCount), inicial + 15, GVDatosPlanteles.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVDatosPlanteles, "Select$" & e.Row.RowIndex)
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final
                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVDatosPlanteles, "Page$" & counter.ToString)
            Next
        End If
    End Sub
   
    Protected Sub GVDatosPlanteles_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVDatosPlanteles.SelectedIndexChanged
        Actualizar.Visible = True
        Eliminar.Visible = True
        TBdescripcionPlantel.Text = HttpUtility.HtmlDecode(GVDatosPlanteles.SelectedDataKey("Descripcion1").ToString) 'Descripcion del Plantel
    End Sub

    Protected Sub btnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            verificaCampos()

            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("SET dateformat dmy; INSERT INTO Institucion (Descripcion, Observaciones, Estatus, FechaModif, Modifico) VALUES (@Descripcion, @Observaciones, @Estatus, getdate(), @Modifico)", conn)

                cmd.Parameters.AddWithValue("@Descripcion", TBDescripcionInstitucion.Text)
                cmd.Parameters.AddWithValue("@Observaciones", tbObservaciones.Text)
                cmd.Parameters.AddWithValue("@Estatus", ddlEstatus.SelectedValue)
                cmd.Parameters.AddWithValue("@Modifico", User.Identity.Name)

                cmd.ExecuteNonQuery()
                cmd.Dispose()
                conn.Close()

                CType(New MessageSuccess(btnActualizar, Me.GetType, "Se creó la institución " & TBDescripcionInstitucion.Text), MessageSuccess).show()
                gvInstituciones.DataBind()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(btnActualizar, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try
    End Sub

    Protected Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("SET dateformat dmy; DELETE FROM Institucion WHERE IdInstitucion = @IdInstitucion", conn)

                cmd.Parameters.AddWithValue("@IdInstitucion", gvInstituciones.SelectedDataKey("IdInstitucion"))

                cmd.ExecuteNonQuery()
                cmd.Dispose()
                conn.Close()

                CType(New MessageSuccess(btnEliminar, Me.GetType, "Se eliminó la institución " & gvInstituciones.SelectedDataKey("Descripcion")), MessageSuccess).show()

                gvInstituciones.DataBind()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(btnEliminar, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try
    End Sub

    Protected Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click


        Try
            verificaCampos()

            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("SET dateformat dmy; UPDATE Institucion SET Descripcion = @Descripcion, Observaciones = @Observaciones, Estatus = @Estatus, FechaModif = getdate(), Modifico = @Modifico WHERE IdInstitucion = @IdInstitucion", conn)

                cmd.Parameters.AddWithValue("@Descripcion", TBDescripcionInstitucion.Text)
                cmd.Parameters.AddWithValue("@Observaciones", tbObservaciones.Text)
                cmd.Parameters.AddWithValue("@Estatus", ddlEstatus.SelectedValue)
                cmd.Parameters.AddWithValue("@Modifico", User.Identity.Name)
                cmd.Parameters.AddWithValue("@IdInstitucion", gvInstituciones.SelectedDataKey("IdInstitucion"))

                cmd.ExecuteNonQuery()
                cmd.Dispose()
                conn.Close()
                CType(New MessageSuccess(btnActualizar, Me.GetType, "Se actualizó la institución " & TBDescripcionInstitucion.Text), MessageSuccess).show()
                gvInstituciones.DataBind()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(btnActualizar, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    ' Métodos de GridViews
    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub gvInstituciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvInstituciones.RowDataBound

        Dim cociente As Integer = (gvInstituciones.PageCount) / 15
        Dim moduloIndex As Integer = (gvInstituciones.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((gvInstituciones.PageIndex + 1) / 15)
        Dim inicial As Integer
        Dim final As Integer


        If cociente = 0 Then
            inicial = 1
            final = gvInstituciones.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = gvInstituciones.PageIndex
            final = IIf((inicial + 15 <= gvInstituciones.PageCount), inicial + 15, gvInstituciones.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= gvInstituciones.PageCount), inicial + 15, gvInstituciones.PageCount)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(gvInstituciones, "Select$" & e.Row.RowIndex)
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(gvInstituciones, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        Try
            msgError.hide()
            SDSplanteles.UpdateCommand = "SET dateformat dmy; UPDATE Plantel SET IdInstitucion = " + gvInstituciones.SelectedDataKey("IdInstitucion").ToString() + ",Descripcion = '" + TBdescripcionPlantel.Text + "' WHERE IdPlantel = " + GVDatosPlanteles.SelectedValue.ToString 'Me da el IdPlantel porque es el campo clave de la fila seleccionada
            SDSplanteles.Update()

            CType(New MessageSuccess(Actualizar, Me.GetType, "El Registro ha sido actualizado correctamente"), MessageSuccess).show()
            GVDatosPlanteles.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(Actualizar, Me.GetType, ex.Message.ToString()), MessageError).show()


        End Try

    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        Try
            SDSplanteles.InsertCommand = "SET dateformat dmy; INSERT INTO Plantel (IdInstitucion,Descripcion) VALUES (" + gvInstituciones.SelectedDataKey("IdInstitucion").ToString() + ",'" + TBdescripcionPlantel.Text + "')"
            SDSplanteles.Insert()
            CType(New MessageSuccess(Insertar, Me.GetType, "El Registro ha sido insertado correctamente"), MessageSuccess).show()
            GVDatosPlanteles.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(Insertar, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try
    End Sub

    Protected Sub Limpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Limpiar.Click
        Siget.Utils.GeneralUtils.CleanAll(Me.Controls)
    End Sub

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        Try
            SDSplanteles.DeleteCommand = "Delete from Plantel where IdPlantel = " + GVDatosPlanteles.SelectedDataKey("IdPlantel").ToString 'Me da el IdPlantel porque es el campo clave de la fila seleccionada
            SDSplanteles.Delete()
            GVDatosPlanteles.DataBind()

            TBDescripcionInstitucion.Text = HttpUtility.HtmlDecode(GVDatosPlanteles.SelectedDataKey("Descripcion1").ToString) 'Descripcion del Plantel

            CType(New MessageSuccess(Eliminar, Me.GetType, "El Registro ha sido eliminado"), MessageSuccess).show()

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)

            CType(New MessageError(Eliminar, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try
    End Sub

    Protected Sub gvInstituciones_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles gvInstituciones.RowCreated

        Dim cociente As Integer = (gvInstituciones.PageCount / 15)
        Dim moduloIndex As Integer = (gvInstituciones.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((gvInstituciones.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer


        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
                        icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If gvInstituciones.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = gvInstituciones.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = gvInstituciones.PageIndex
                final = IIf((inicial + 10 <= gvInstituciones.PageCount), inicial + 15, gvInstituciones.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= gvInstituciones.PageCount, inicial + 15, gvInstituciones.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = gvInstituciones.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next
        End If

    End Sub

    Protected Sub GVDatosPlanteles_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVDatosPlanteles.RowCreated

        Dim cociente As Integer = (GVDatosPlanteles.PageCount / 15)
        Dim moduloIndex As Integer = (GVDatosPlanteles.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVDatosPlanteles.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer


        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
                        icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If GVDatosPlanteles.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If


        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)


            If cociente = 0 Then
                inicial = 1
                final = GVDatosPlanteles.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVDatosPlanteles.PageIndex
                final = IIf((inicial + 10 <= GVDatosPlanteles.PageCount), inicial + 15, GVDatosPlanteles.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVDatosPlanteles.PageCount, inicial + 15, GVDatosPlanteles.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVDatosPlanteles.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next

        End If

    End Sub

    Protected Sub GVDatosPlanteles_DataBound(sender As Object, e As EventArgs)
        If GVDatosPlanteles.Rows.Count = 0 Then
            Insertar.Visible = False
            Actualizar.Visible = False
            Eliminar.Visible = False
        End If
    End Sub

    Protected Sub gvInstituciones_DataBound(sender As Object, e As EventArgs) Handles gvInstituciones.DataBound

        If gvInstituciones.Rows.Count = 0 Then
            Eliminar.Visible = False
            Actualizar.Visible = False
            gvInstituciones.SelectedIndex = -1
        End If

    End Sub

    Protected Sub GVDatosPlanteles_PreRender(sender As Object, e As EventArgs) Handles GVDatosPlanteles.PreRender

        GVDatosPlanteles.Columns(1).HeaderText = Siget.Lang.Lang_Config.Translate("general", "Institucion")
        GVDatosPlanteles.Columns(1).HeaderText = Siget.Lang.Lang_Config.Translate("general", "Plantel")

    End Sub
End Class
