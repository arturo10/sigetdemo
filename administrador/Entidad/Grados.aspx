﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" 
       EnableEventValidation="false" AutoEventWireup="false" CodeFile="Grados.aspx.vb" Inherits="administrador_AgruparReactivos" %>


<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="row">

        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="widget">
                <div class="widget-header bg-themeprimary">
                    <i class="widget-icon fa fa-arrow-up"></i>
                    <span class="widget-caption"><b>ÁREA DE GRADOS</b></span>
                    <div class="widget-buttons">
                        <a href="#" data-toggle="config">
                            <i class="fa fa-cog"></i>
                        </a>
                        <a href="#" data-toggle="maximize">
                            <i class="fa fa-expand"></i>
                        </a>
                        <a href="#" data-toggle="collapse">
                            <i class="fa fa-minus"></i>
                        </a>
                        <a href="#" data-toggle="dispose">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                    <!--Widget Buttons-->
                </div>
                <!--Widget Header-->
                <div class="widget-body">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="form-horizontal" id="adminGradosForm">
                                <div class="panel panel-default">
                                    

                                    <div class="panel-body">

                                        <div class="form-group">
                                            <asp:Label ID="Label2" CssClass="col-lg-2" runat="server" Text="Nivel" />
                                            <div class="col-lg-8">
                                                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSniveles" DataTextField="Descripcion"
                                                    DataValueField="IdNivel" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label3" CssClass="col-lg-2" runat="server" Text="Descripción" />

                                            <div class="col-lg-8">
                                                <asp:TextBox ID="TBdescripcion" runat="server" CssClass="form-control"></asp:TextBox>
                                                <span class="style19">Ej. Primero, Segundo</span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label1" CssClass="col-lg-2" runat="server" Text="Estatus" />
                                            <div class="col-lg-8">

                                                <asp:DropDownList ID="DDLestatus" runat="server" CssClass="form-control">
                                                    <asp:ListItem>Activo</asp:ListItem>
                                                    <asp:ListItem>Suspendido</asp:ListItem>
                                                    <asp:ListItem>Baja</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="centered">
                                            <div class="table-responsive">
                                                <asp:GridView ID="GVgradoscreados"   runat="server"
                                                    AllowPaging="True" 
                                                    AutoGenerateColumns="False"
                                                    DataKeyNames="IdGrado,Descripcion,Estatus"
                                                    DataSourceID="SDSgrados"
                                                    PageSize="5"
                                                    CellPadding="3"
                                                    GridLines="Vertical"
                                                    CssClass="table table-striped table-bordered"
                                                    Height="17px"
                                                    Style="font-size: x-small; text-align: left;">
                                                    <Columns>

                                                        <asp:BoundField DataField="IdGrado" HeaderText="Id Grado" InsertVisible="False"
                                                            ReadOnly="True" SortExpression="IdGrado" />
                                                        <asp:BoundField DataField="IdNivel" HeaderText="IdNivel"
                                                            SortExpression="IdNivel" Visible="False" />
                                                        <asp:BoundField DataField="Descripcion" HeaderText="Grados creados"
                                                            SortExpression="Descripcion" />
                                                        <asp:BoundField DataField="FechaModif" HeaderText="FechaModif"
                                                            SortExpression="FechaModif" Visible="False" />
                                                        <asp:BoundField DataField="Modifico" HeaderText="Modifico"
                                                            SortExpression="Modifico" Visible="False" />
                                                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                            SortExpression="Estatus" />
                                                    </Columns>
                                                    <PagerTemplate>

                                                        <ul runat="server" id="Pag" class="pagination">
                                                        </ul>
                                                    </PagerTemplate>
                                                    <PagerStyle HorizontalAlign="Center" />
                                                    <SelectedRowStyle CssClass="row-selected" />
                                                    <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <%-- sort NO - prerender
	                            0  select
	                            1  id_GRADO
	                            2  idnivel
	                            3  GRADOS creados
	                            4  fechamodif
	                            5  modifico
	                            6  estatus
                                        --%>
                                        <div class="form-group">
                                            <asp:LinkButton ID="Insertar" runat="server" Text="Insertar"
                                                CssClass="btn btn-labeled btn-palegreen">
                                         <i class="btn-label fa fa-check"></i>Insertar
                                            </asp:LinkButton>

                                            <asp:LinkButton ID="Eliminar" Visible="false" runat="server" Text="Eliminar"
                                                CssClass="btn btn-labeled btn-darkorange shiny">
                                                <i class="btn-label fa fa-remove"></i>Eliminar
                                            </asp:LinkButton>

                                            <asp:LinkButton ID="Actualizar" Visible="false" runat="server" Text="Actualizar"
                                                CssClass="btn btn-labeled btn-palegreen shiny">
                                                <i class="btn-label fa fa-refresh"></i>Actualizar
                                            </asp:LinkButton>

                                        </div>

                                        <div class="col-lg-12">
                                            <uc1:msgError runat="server" ID="msgError" />
                                            <uc1:msgSuccess runat="server" ID="msgSuccess" />
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <!--Widget Body-->
            </div>
            <!--Widget-->
        </div>
    </div>



    <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [IdNivel], [Descripcion], [Estatus], [FechaModif], [Modifico] FROM [Grado] WHERE ([IdNivel] = @IdNivel) ORDER BY [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">
</asp:Content>

