﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Siget.Utils;
using Siget.Lang;
using System.Text.RegularExpressions;


public partial class administrador_Coordinador_CiclosIndicadores : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Siget.Utils.Sesion.sesionAbierta();

    }

    protected void GVciclosescolares_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        //La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
        btnEliminarCiclo.Visible = true;
        btnActualizarCiclo.Visible = true;
        DDLindicador.SelectedValue = GVciclosescolares.SelectedDataKey["IdIndicador"].ToString();
        TBDescripcion.Text = HttpUtility.HtmlDecode(GVciclosescolares.SelectedDataKey["Descripcion"].ToString());
        HFFechaInicio.Value = Convert.ToDateTime(GVciclosescolares.SelectedDataKey["FechaInicio"]).ToString("MM/dd/yyyy");
        HFFechaFin.Value = Convert.ToDateTime(GVciclosescolares.SelectedDataKey["FechaFin"]).ToString("MM/dd/yyyy");
        DDLestatus.SelectedValue = GVciclosescolares.SelectedDataKey["Estatus"].ToString().Trim();
        //La liga seleccionar del grid ocupa el index 0
    }

    protected void btnCrearCicloEscolar_Click(object sender, System.EventArgs e)
    {
        try
        {
            SDScicloescolar.InsertCommand = "SET dateformat dmy; INSERT INTO CicloEscolar(IdIndicador,Descripcion,FechaInicio,FechaFin,Estatus,FechaModif, Modifico) VALUES (" + DDLindicador.SelectedValue + ",'" + TBDescripcion.Text + "','" + Convert.ToDateTime(HFFechaInicioParse.Value).ToString("dd/MM/yyyy") + "','" + Convert.ToDateTime(HFFechaFinParse.Value).ToString("dd/MM/yyyy") + " 23:59:00','" + DDLestatus.SelectedValue + "', getdate(), '" + User.Identity.Name + "')";
            SDScicloescolar.Insert();

            (new MessageSuccess(btnCrearCicloEscolar, this.GetType(), " El registro ha sido creado")).show();
            GVciclosescolares.DataBind();
          
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
            (new MessageError(btnCrearCicloEscolar, this.GetType(), ex.Message.ToString())).show();
        }
    }


    protected void GVciclosescolares_RowCreated(object sender, GridViewRowEventArgs e)
    {
        int cociente = (GVciclosescolares.PageCount / 15);
        int moduloIndex = (GVciclosescolares.PageIndex + 1) % 15;
        int cocienteActual = Convert.ToInt32(Math.Floor(Convert.ToDouble((GVciclosescolares.PageIndex + 1) / 15)));
        int final = 0;
        int inicial = 0;


        if (e.Row.RowType == DataControlRowType.Pager)
        {
            HtmlControl con = (HtmlControl)e.Row.Cells[0].FindControl("Pag");
            if (cociente == 0)
            {
                inicial = 1;
                final = GVciclosescolares.PageCount;
            }
            else if (cociente >= 1 & moduloIndex == 0)
            {
                inicial = GVciclosescolares.PageIndex;
                final = ((inicial + 15 <= GVciclosescolares.PageCount) ? inicial + 15 : GVciclosescolares.PageCount);
            }
            else
            {
                inicial = (cocienteActual * 15 > 0 ? cocienteActual * 15 : 1);
                final = (inicial + 15 <= GVciclosescolares.PageCount ? inicial + 15 : GVciclosescolares.PageCount);
            }


            for (int counter = inicial; counter <= final; counter++)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                HtmlGenericControl page = new HtmlGenericControl("a");

                if (counter == inicial & cocienteActual > 0)
                {
                    HtmlGenericControl span = new HtmlGenericControl("span");
                    span.InnerHtml = "&laquo;";
                    page.Controls.Add(span);
                    page.Attributes.Add("aria-label", "Previous");
                    page.ID = "pagina" + counter.ToString();
                    li.Controls.Add(page);
                    con.Controls.Add(li);

                }
                else
                {
                    if (counter == GVciclosescolares.PageIndex + 1)
                    {
                        li.Attributes["class"] = "active";
                    }
                    page.InnerText = counter.ToString();
                    page.ID = "pagina" + counter.ToString();

                    li.Controls.Add(page);
                    con.Controls.Add(li);
                }
            }


        }

    }


    protected void GVciclosescolares_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        int cociente = (GVciclosescolares.PageCount) / 15;
        int moduloIndex = (GVciclosescolares.PageIndex + 1) % 15;
        int cocienteActual = Convert.ToInt32(Math.Floor(Convert.ToDouble(((GVciclosescolares.PageIndex + 1) / 15))));

        int inicial = 0;
        int final = 0;

        if (cociente == 0)
        {
            inicial = 1;
            final = GVciclosescolares.PageCount;
        }
        else if (cociente >= 1 & moduloIndex == 0)
        {
            inicial = GVciclosescolares.PageIndex;
            final = ((inicial + 15 <= GVciclosescolares.PageCount) ? inicial + 15 : GVciclosescolares.PageCount);
        }
        else
        {
            inicial = (cocienteActual * 15 > 0 ? cocienteActual * 15 : 1);
            final = ((inicial + 15 <= GVciclosescolares.PageCount) ? inicial + 15 : GVciclosescolares.PageCount);
        }


        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVciclosescolares, "Select$" + e.Row.RowIndex));
            string s = Regex.Replace(HttpUtility.HtmlDecode(e.Row.Cells[3].Text), "<[^>]*(>|$)", string.Empty);
            int l = s.Length;
            string continua = "";
            if (l > 100)
            {
                l = 100;
                continua = "...(Continúa)";
            }
            e.Row.Cells[3].Text = s.Substring(0, l) + continua;
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            HtmlControl con = (HtmlControl)e.Row.Cells[0].FindControl("Pag");

            for (int counter = inicial; counter <= final; counter++)
            {
                ((HtmlGenericControl)con.FindControl("pagina" + counter.ToString())).Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GVciclosescolares, "Page$" + counter.ToString());
            }
        }
    }

    protected void btnActualizarCiclo_Click(object sender, System.EventArgs e)
    {
        try
        {
            SDScicloescolar.UpdateCommand = "SET dateformat dmy; UPDATE CicloEscolar SET IdIndicador = " + DDLindicador.SelectedValue + ",Descripcion = '" + TBDescripcion.Text + "', FechaInicio = '" + Convert.ToDateTime(HFFechaInicioParse.Value).ToString("dd/MM/yyyy") + "', FechaFin = '" + Convert.ToDateTime(HFFechaFinParse.Value).ToString("dd/MM/yyyy") + " 23:59:00', FechaModif = getdate(), Modifico = '" + User.Identity.Name + "', Estatus = '" + DDLestatus.SelectedValue + "' WHERE IdCicloEscolar = " + GVciclosescolares.SelectedDataKey["IdCicloEscolar"].ToString();
            SDScicloescolar.Update();

            (new MessageSuccess(btnActualizarCiclo, this.GetType(), " El registro ha sido actualizado")).show();
            GVciclosescolares.DataBind();
            msgError.hide();
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
            (new MessageError(btnActualizarCiclo, this.GetType(), " El registro ha sido borrado")).show();
        }
    }

    protected void btnEliminarCiclo_Click(object sender, System.EventArgs e)
    {
        try
        {
            SDScicloescolar.DeleteCommand = "Delete from CicloEscolar where IdCicloEscolar = " + GVciclosescolares.SelectedDataKey["IdCicloEscolar"].ToString();
            SDScicloescolar.Delete();
            (new MessageSuccess(btnEliminarCiclo, this.GetType(), " El registro ha sido borrado")).show();
            GVciclosescolares.DataBind();
           
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
            (new MessageError(btnEliminarCiclo, this.GetType(), ex.Message.ToString())).show();
        }
    }

    protected void GVciclosescolares_DataBound(object sender, EventArgs e)
    {
        if (GVciclosescolares.Rows.Count == 0)
        {
            btnEliminarCiclo.Visible = false;
            btnActualizarCiclo.Visible = false;
            GVciclosescolares.SelectedIndex = -1;
        }
    }

    protected void GVindicadoresr_RowCreated(object sender, GridViewRowEventArgs e)
    {
        int cociente = (GVindicadores.PageCount / 15);
        int moduloIndex = (GVindicadores.PageIndex + 1) % 15;
        int cocienteActual = Convert.ToInt32(Math.Floor(Convert.ToDouble((GVindicadores.PageIndex + 1) / 15)));
        int final = 0;
        int inicial = 0;


        if (e.Row.RowType == DataControlRowType.Pager)
        {
            HtmlControl con = (HtmlControl)e.Row.Cells[0].FindControl("Pag");
            if (cociente == 0)
            {
                inicial = 1;
                final = GVindicadores.PageCount;
            }
            else if (cociente >= 1 & moduloIndex == 0)
            {
                inicial = GVindicadores.PageIndex;
                final = ((inicial + 15 <= GVindicadores.PageCount) ? inicial + 15 : GVindicadores.PageCount);
            }
            else
            {
                inicial = (cocienteActual * 15 > 0 ? cocienteActual * 15 : 1);
                final = (inicial + 15 <= GVindicadores.PageCount ? inicial + 15 : GVindicadores.PageCount);
            }


            for (int counter = inicial; counter <= final; counter++)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                HtmlGenericControl page = new HtmlGenericControl("a");

                if (counter == inicial & cocienteActual > 0)
                {
                    HtmlGenericControl span = new HtmlGenericControl("span");
                    span.InnerHtml = "&laquo;";
                    page.Controls.Add(span);
                    page.Attributes.Add("aria-label", "Previous");
                    page.ID = "pagina" + counter.ToString();
                    li.Controls.Add(page);
                    con.Controls.Add(li);

                }
                else
                {
                    if (counter == GVindicadores.PageIndex + 1)
                    {
                        li.Attributes["class"] = "active";
                    }
                    page.InnerText = counter.ToString();
                    page.ID = "pagina" + counter.ToString();

                    li.Controls.Add(page);
                    con.Controls.Add(li);
                }
            }


        }

    }

    protected void GVindicadores_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        int cociente = (GVindicadores.PageCount) / 15;
        int moduloIndex = (GVindicadores.PageIndex + 1) % 15;
        int cocienteActual = Convert.ToInt32(Math.Floor(Convert.ToDouble((GVindicadores.PageIndex + 1) / 15)));

        int inicial = 0;
        int final = 0;

        if (cociente == 0)
        {
            inicial = 1;
            final = GVindicadores.PageCount;
        }
        else if (cociente >= 1 & moduloIndex == 0)
        {
            inicial = GVindicadores.PageIndex;
            final = ((inicial + 15 <= GVindicadores.PageCount) ? inicial + 15 : GVindicadores.PageCount);
        }
        else
        {
            inicial = (cocienteActual * 15 > 0 ? cocienteActual * 15 : 1);
            final = ((inicial + 15 <= GVindicadores.PageCount) ? inicial + 15 : GVindicadores.PageCount);
        }


        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVindicadores, "Select$" + e.Row.RowIndex));
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            HtmlControl con = (HtmlControl)e.Row.Cells[0].FindControl("Pag");

            for (int counter = inicial; counter <= final; counter++)
            {
                ((HtmlGenericControl)con.FindControl("pagina" + counter.ToString())).Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GVindicadores, "Page$" + counter.ToString());
            }
        }
    }

    protected void GVindicadores_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        TBIndicadorAmarillo.Text = GVindicadores.SelectedDataKey["MinAmarillo"].ToString().Trim();
        TBIndicadorAmarilloP.Text = GVindicadores.SelectedDataKey["MinAmarilloPorc"].ToString().Trim();
        TBIndicadorAzul.Text = GVindicadores.SelectedDataKey["MinAzul"].ToString().Trim();
        TBIndicadorAzulP.Text = GVindicadores.SelectedDataKey["MinAzulPorc"].ToString().Trim();
        TBIndicadorRojo.Text = GVindicadores.SelectedDataKey["MinRojo"].ToString().Trim();
        TBIndicadorRojoP.Text = GVindicadores.SelectedDataKey["MinRojoPorc"].ToString().Trim();
        TBIndicadorVerde.Text = GVindicadores.SelectedDataKey["MinVerde"].ToString().Trim();
        TBIndicadorVerdeP.Text = GVindicadores.SelectedDataKey["MinVerdePorc"].ToString().Trim();
        TBDescripcionIndicador.Text = GVindicadores.SelectedDataKey["Descripcion"].ToString().Trim();

    }



    protected void btnInsertarIndicador_Click(object sender, System.EventArgs e)
    {
        try
        {
            SDSindicadores.InsertCommand = "SET dateformat dmy; INSERT INTO Indicador (Descripcion,MinAzul,MinVerde,MinAmarillo,MinRojo,MinAzulPorc,MinVerdePorc,MinAmarilloPorc,MinRojoPorc, FechaModif, Modifico) VALUES ('" +
             TBDescripcionIndicador.Text + "'," + TBIndicadorAzul.Text + "," + TBIndicadorVerde.Text + "," +
             TBIndicadorAmarillo.Text + "," + TBIndicadorRojo.Text + "," + TBIndicadorAzulP.Text + "," +
             TBIndicadorVerdeP.Text + "," + TBIndicadorAmarilloP.Text + "," + TBIndicadorRojoP.Text +
             ", getdate(), '" + User.Identity.Name + "')";
            SDSindicadores.Insert();

            (new MessageSuccess(btnInsertarIndicador, this.GetType(), "El registro ha sido guardado")).show();
            GVindicadores.DataBind();
            DDLindicador.DataBind();
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
            (new MessageError(btnInsertarIndicador, this.GetType(), ex.Message.ToString())).show();
        }

    }




    protected void btnEliminarIndicador_Click(object sender, System.EventArgs e)
    {
        try
        {
            SDSindicadores.DeleteCommand = "Delete from Indicador where IdIndicador = " + GVindicadores.SelectedDataKey["IdIndicador"].ToString();
            SDSindicadores.Delete();
            Siget.Utils.GeneralUtils.CleanAll(this.Controls);
            (new MessageSuccess(btnInsertarIndicador, this.GetType(), "El registro ha sido borrado")).show();
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
            (new MessageError(btnInsertarIndicador, this.GetType(), ex.Message.ToString())).show();
        }
    }


    protected void btnActualizarIndicador_Click(object sender, System.EventArgs e)
    {
        try
        {
            SDSindicadores.UpdateCommand = "SET dateformat dmy; UPDATE Indicador SET Descripcion = '" + TBDescripcionIndicador.Text +
                "', MinAzul='" + TBIndicadorAzul.Text + "', MinVerde='" + TBIndicadorVerde.Text + "',MinAmarillo='"
                + TBIndicadorAmarillo.Text + "',MinRojo='" + TBIndicadorRojo.Text + "',MinAzulPorc='" +
                TBIndicadorAzulP.Text + "',MinVerdePorc='" + TBIndicadorVerdeP.Text + "',MinAmarilloPorc='" +
                TBIndicadorAmarilloP.Text + "',MinRojoPorc='" + TBIndicadorRojoP.Text +
                "', FechaModif = getdate(), Modifico = '" + User.Identity.Name + "' WHERE IdIndicador = " +
                GVindicadores.SelectedDataKey["IdIndicador"].ToString();
         
            SDSindicadores.Update();
            (new MessageSuccess(btnActualizarIndicador, this.GetType(), "El registro ha sido actualizado")).show();
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
            (new MessageError(btnActualizarIndicador, this.GetType(), ex.Message.ToString())).show();
        }
    }





}