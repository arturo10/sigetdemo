﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false"   EnableEventValidation="false"  CodeFile="Niveles.aspx.vb" Inherits="administrador_AgruparReactivos" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
        
  
    <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="tabbable">
                        <ul class="nav nav-tabs nav-justified" id="myTab5">
                            <li class="active">
                                <a data-toggle="tab" href="#crearNivel">Crear Nivel
                                </a>
                            </li>

                            <li class="tab-red">
                                <a data-toggle="tab" href="#asignarNivel">Asignar Nivel
                                </a>
                            </li>

                              <li class="tab-blue">
                                <a data-toggle="tab" href="#grados">Grados
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div id="crearNivel" class="tab-pane in active">

                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-xs-12">
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>

                                        <div class="form-horizontal" id="asignarNivelForm">

                                            <div class="col-lg-6">

                                                <div class="form-group">
                                                    <label class="control-label col-lg-2">Descripción</label>
                                                    <div class="col-lg-10">
                                                        <asp:TextBox runat="server" ID="TbDescripcionNivel"
                                                            CssClass="form-control">

                                                        </asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label col-lg-2">Estatus</label>
                                                    <div class="col-lg-10">
                                                        <asp:DropDownList runat="server" ID="DDLEstatusNiveles"
                                                            CssClass="form-control">
                                                            <asp:ListItem Value="Activo"></asp:ListItem>
                                                            <asp:ListItem Value="Suspendido">Suspendido</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="control-label col-lg-2">Fecha Modificación</label>
                                                    <div class="col-lg-10">
                                                        <asp:TextBox ID="TBFechaMod" CssClass="form-control" runat="server" disabled></asp:TextBox>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="control-label col-lg-2">Modifico</label>
                                                    <div class="col-lg-10">
                                                        <asp:TextBox runat="server" CssClass="form-control" ID="TBModifico" disabled>

                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                                 <div class="col-lg-12">
                                                    <asp:LinkButton ID="InsertarNivel" type="submit" runat="server"
                                                        CssClass="btn btn-labeled btn-palegreen inN">
                                                            <i class="btn-label fa fa-check"></i>Insertar
                                                    </asp:LinkButton>

                                                  

                                                    <asp:LinkButton ID="ActualizarNivel" type="submit" runat="server" Text="Actualizar"
                                                        CssClass="btn btn-labeled btn-palegreen inN"  Visible="false">
                                                             <i class="btn-label fa fa-refresh"></i> Actualizar
                                                    </asp:LinkButton>

                                                </div>
                                            </div>


                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                 <div class="table-responsive">
                                                            <asp:GridView ID="GVNivelesGenerales" runat="server"
                                                                AllowPaging="True" 
                                                                AutoGenerateColumns="False"
                                                                DataSourceID="SDSNiveles"
                                                                DataKeyNames="IdNivel,Descripcion, FechaModif, Modifico,Estatus"
                                                                AllowSorting="True"
                                                                PageSize="5"
                                                                Caption="<h1>Niveles existentes</h1>"
                                                                CellPadding="3"
                                                                CssClass="table table-striped table-bordered table-hover"
                                                                Height="17px"
                                                                Style="font-size: x-small; text-align: left;">
                                                                <Columns>

                                                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripcion"
                                                                        SortExpression="Descripcion" />
                                                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus" 
                                                                        SortExpression="Estatus" />
                                                                     <asp:BoundField DataField="FechaModif" HeaderText="FechaModif" 
                                                                        SortExpression="FechaModif" />
                                                                      <asp:BoundField DataField="Modifico" HeaderText="Modifico" 
                                                                        SortExpression="Modifico" />
                                                                       
                                                                </Columns>
                                                                 <PagerTemplate>
                                                                    <ul runat="server" id="Pag" class="pagination">
                                                                    </ul>
                                                                </PagerTemplate>
                                                                <PagerStyle HorizontalAlign="Center" />
                                                                <SelectedRowStyle CssClass="row-selected" />
                                                                <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                            </asp:GridView>
                                                                    </div>
                                                    </div>
                                                <div class="form-group">
                                                      <asp:LinkButton ID="EliminarNivel" type="submit" runat="server" Visible="false"
                                                        CssClass="btn btn-labeled btn-darkorange inN">
                                                            <i class="btn-label fa fa-remove"></i>Eliminar
                                                    </asp:LinkButton>
                                                </div>


                                                </div>


                                             
                                             <div class="col-lg-12">

                                                    <uc1:msgSuccess runat="server" ID="msgSuccess2" />

                                                    <uc1:msgInfo runat="server" ID="msgInfo2" />

                                                    <uc1:msgError runat="server" ID="msgError2" />
                                                </div>

                                        </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                  

                            <div id="asignarNivel" class="tab-pane">

                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-xs-12">
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>
                                                <div class="form-horizontal" >
                                                    <div class="col-lg-6">
                                                        <div class="row">
                                                            <div class="card col-lg-11">
                                                                <div class="card-content ">
                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label3" CssClass="col-lg-3" runat="server" Text="Institución" />
                                                                        <div class="col-lg-8 ">
                                                                            <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                                                                DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                                                                DataValueField="IdInstitucion" CssClass="form-control ">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                        
                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label4" CssClass="col-lg-3" runat="server">
                                                                            <%=Lang_Config.Translate("general","Plantel") %>
                                                                        </asp:Label>
                                                                        <div class="col-lg-8">
                                                                            <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                                                                DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                                                                DataValueField="IdPlantel" CssClass="form-control">
                                                                            </asp:DropDownList>
                                                                            
                                                                        </div>
                                                                    </div>

                                                                    

                                                                    <div class="form-group">

                                                                        <asp:Label ID="Label5" CssClass="col-lg-3" runat="server" Text="Nuevo nivel a asignar" />
                                                                        <div class="col-lg-8">
                                                                            <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                                                                DataSourceID="SDSniveles" DataTextField="Descripcion"
                                                                                DataValueField="IdNivel" CssClass="form-control">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">

                                                                        <asp:LinkButton ID="Asignar" runat="server"
                                                                            CssClass="btn btn-labeled btn-darkorange">
                                                            <i class="btn-label fa fa-plus"></i>Asignar
                                                                        </asp:LinkButton>

                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="row">
                                                                <div class="table-responsive">
                                                            <asp:GridView ID="GVnivelesasignados" runat="server"
                                                                AllowPaging="True"
                                                                AutoGenerateColumns="False" 
                                                                DataSourceID="SDSescuelas" 
                                                                DataKeyNames="IdEscuela"
                                                                AllowSorting="True"
                                                                PageSize="5"
                                                                CellPadding="3"
                                                                CssClass="table table-striped table-bordered table-hover"
                                                                Height="17px"
                                                                Style="font-size: x-small; text-align: left;">
                                                                <Columns>

                                                                    <asp:BoundField DataField="IdPlantel" HeaderText="Id Plantel"
                                                                        InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel">
                                                                        <HeaderStyle HorizontalAlign="Right" Width="90px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Descripcion" HeaderText="Plantel"
                                                                        SortExpression="Descripcion" />
                                                                    <asp:BoundField DataField="IdNivel" HeaderText="Id Nivel" InsertVisible="False"
                                                                        ReadOnly="True" SortExpression="IdNivel">
                                                                        <HeaderStyle HorizontalAlign="Right" Width="70px" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Descripcion1" HeaderText="Nivel asignado"
                                                                        SortExpression="Descripcion1" />
                                                                </Columns>
                                                                <PagerTemplate>
                                                                    <ul runat="server" id="Pag" class="pagination">
                                                                    </ul>
                                                                </PagerTemplate>
                                                                <PagerStyle HorizontalAlign="Center" />
                                                                <SelectedRowStyle CssClass="row-selected" />
                                                                <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                            </asp:GridView>
                                                                    </div>

                                                        </div>
                                                        <div class="col-lg-12">
                                                        <asp:LinkButton ID="Desasignar" Visible="false" runat="server"
                                                            CssClass="btn btn-labeled btn-warning shiny">
                                                              <i class=" btn-label fa fa-minus"></i>Desasignar
                                                        </asp:LinkButton>


                                                    </div>
                                                    </div>
                                                    
                                                    <div class="col-lg-12">

                                                        <div class="form-group">
                                                            <uc1:msgError runat="server" ID="msgError3" />

                                                            <uc1:msgSuccess runat="server" ID="msgSuccess3" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>

                             <div id="grados" class="tab-pane">

                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 col-xs-12">
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>

                                                <div class="form-horizontal" id="adminGradosForm">
                                                    <div class="panel panel-default">


                                                        <div class="panel-body">

                                                            <div class="form-group">
                                                                <asp:Label ID="Label2" CssClass="col-lg-2" runat="server" Text="Nivel" />
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLNivelGrado" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSNivelesGrados" DataTextField="Descripcion"
                                                                        DataValueField="IdNivel" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label1" CssClass="col-lg-2" runat="server" Text="Descripción" />

                                                                <div class="col-lg-8">
                                                                    <asp:TextBox ID="TBdescripcion" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <span class="style19">Ej. Primero, Segundo</span>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <asp:Label ID="Label6" CssClass="col-lg-2" runat="server" Text="Estatus" />
                                                                <div class="col-lg-8">

                                                                    <asp:DropDownList ID="DDLestatus" runat="server" CssClass="form-control">
                                                                        <asp:ListItem>Activo</asp:ListItem>
                                                                        <asp:ListItem>Suspendido</asp:ListItem>
                                                                        <asp:ListItem>Baja</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="centered">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="GVgradoscreados" runat="server"
                                                                        AllowPaging="True"
                                                                        AutoGenerateColumns="False"
                                                                        DataKeyNames="IdGrado,Descripcion,Estatus"
                                                                        DataSourceID="SDSgrados"
                                                                        PageSize="5"
                                                                        CellPadding="3"
                                                                        GridLines="Vertical"
                                                                        CssClass="table table-striped table-bordered"
                                                                        Height="17px"
                                                                        Style="font-size: x-small; text-align: left;">
                                                                        <Columns>

                                                                            <asp:BoundField DataField="IdGrado" HeaderText="Id Grado" InsertVisible="False"
                                                                                ReadOnly="True" SortExpression="IdGrado" />
                                                                            <asp:BoundField DataField="IdNivel" HeaderText="IdNivel"
                                                                                SortExpression="IdNivel" Visible="False" />
                                                                            <asp:BoundField DataField="Descripcion" HeaderText="Grados creados"
                                                                                SortExpression="Descripcion" />
                                                                            <asp:BoundField DataField="FechaModif" HeaderText="FechaModif"
                                                                                SortExpression="FechaModif" Visible="False" />
                                                                            <asp:BoundField DataField="Modifico" HeaderText="Modifico"
                                                                                SortExpression="Modifico" Visible="False" />
                                                                            <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                                                SortExpression="Estatus" />
                                                                        </Columns>
                                                                        <PagerTemplate>

                                                                            <ul runat="server" id="Pag" class="pagination">
                                                                            </ul>
                                                                        </PagerTemplate>
                                                                        <PagerStyle HorizontalAlign="Center" />
                                                                        <SelectedRowStyle CssClass="row-selected" />
                                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                            <%-- sort NO - prerender
	                            0  select
	                            1  id_GRADO
	                            2  idnivel
	                            3  GRADOS creados
	                            4  fechamodif
	                            5  modifico
	                            6  estatus
                                                            --%>
                                                            <div class="form-group">
                                                                <asp:LinkButton ID="Insertar" runat="server" Text="Insertar"
                                                                    CssClass="btn btn-labeled btn-palegreen">
                                         <i class="btn-label fa fa-check"></i>Insertar
                                                                </asp:LinkButton>

                                                                <asp:LinkButton ID="Eliminar" Visible="false" runat="server" Text="Eliminar"
                                                                    CssClass="btn btn-labeled btn-darkorange shiny">
                                                <i class="btn-label fa fa-remove"></i>Eliminar
                                                                </asp:LinkButton>

                                                                <asp:LinkButton ID="Actualizar" Visible="false" runat="server" Text="Actualizar"
                                                                    CssClass="btn btn-labeled btn-palegreen shiny">
                                                <i class="btn-label fa fa-refresh"></i>Actualizar
                                                                </asp:LinkButton>

                                                            </div>

                                                            <div class="col-lg-12">
                                                                <uc1:msgError runat="server" ID="msgError" />
                                                                <uc1:msgSuccess runat="server" ID="msgSuccess" />
                                                            </div>

                                                        </div>
                                                    </div>


                                                </div>

                                            </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                              </div>

                        </div>
                    </div>
                </div>
               
            </div>


  
                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Institucion]"></asp:SqlDataSource>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Nivel]"></asp:SqlDataSource>

                <asp:SqlDataSource ID="SDSescuelas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT E.IdEscuela,P.IdPlantel, P.Descripcion,N.IdNivel, N.Descripcion
FROM Escuela E, Nivel N, Plantel P
where P.IdPlantel = @IdPlantel and E.IdPlantel = P.IdPlantel and E.IdNivel = N.IdNivel
">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>


     <asp:SqlDataSource ID="SDSNivelesGrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>
                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [IdNivel], [Descripcion], [Estatus], [FechaModif], [Modifico] FROM [Grado] WHERE ([IdNivel] = @IdNivel) ORDER BY [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLNivelGrado" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">
</asp:Content>

