﻿<%@ Page Title="" Language="VB"
    MasterPageFile="~/Site.Master"
    AutoEventWireup="false"
    EnableEventValidation="false"
    CodeFile="Instituciones.aspx.vb"
    MaintainScrollPositionOnPostback="true"
    Inherits="Instituciones" %>

<asp:Content runat="server" ContentPlaceHolderID="Title">
</asp:Content>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">


    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat radius-bordered">
            <div class="widget-header bg-themeprimary header-fix">

                <span class="widget-caption"><b>ÁREA PARA ADMINISTRAR <%=Lang_Config.Translate("general", "Plantel").ToUpper()%> Y <%=Lang_Config.Translate("general", "Institucion").ToUpper()%> </b></span>


            </div>

            <div class="widget-body">
                <div class="widget-main ">
                    <div class="tabbable">
                        <ul class="nav nav-tabs tabs-flat" id="myTab11">
                            <li class="active">
                                <a id="AreaInstitucion" data-toggle="tab" href="#institucion">Administrar <%=Lang_Config.Translate("general", "Institucion")%>
                                </a>
                            </li>
                            <li id="AreaPlantel">
                                <a data-toggle="tab" href="#plantel">Administrar <%=Lang_Config.Translate("general", "Plantel")%>
                                </a>
                            </li>

                        </ul>
                        <div class="tab-content tabs-flat">
                            <div id="institucion" class="tab-pane in active">

                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <div class="form-horizontal" id="InstitucionesForm">
                                            <div class="panel panel-default">


                                                <div class="panel-body">

                                                    <div class="col-lg-6 ">
                                                        <div class="form-group">
                                                            <asp:Label ID="Label1" CssClass="col-lg-3" runat="server"
                                                                Text="Descripción" />
                                                            <div class="col-lg-9">
                                                                <asp:TextBox ID="TBDescripcionInstitucion"
                                                                    runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label2" CssClass="col-lg-3 label-control" runat="server" Text="Observaciones" />
                                                            <div class="col-lg-8">
                                                                <span class="input-icon icon-right">
                                                                    <asp:TextBox ID="tbObservaciones" TextMode="MultiLine"
                                                                        runat="server" Rows="7" CssClass="form-control"
                                                                        MaxLength="100"></asp:TextBox>

                                                                </span>
                                                                <p class="help-block">Aquí se colocan datos relevantes referentes a las instituciones</p>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label3" CssClass="col-lg-3" runat="server" Text="Estatus" />
                                                            <div class="col-lg-9">
                                                                <asp:DropDownList ID="ddlEstatus" runat="server"
                                                                    CssClass="form-control">
                                                                    <asp:ListItem Value="Activo">Activo</asp:ListItem>
                                                                    <asp:ListItem Value="Suspendido">Suspendido</asp:ListItem>
                                                                    <asp:ListItem Value="Baja">Baja</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:LinkButton ID="btnCrear" runat="server" type="submit"
                                                                CssClass="btn btn-labeled btn-palegreen">
                                                    <i class="btn-label fa fa-plus"></i>Crear
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">

                                                            <div class="col-lg-12">
                                                                <div class="table-responsive">

                                                                    <asp:GridView ID="gvInstituciones" runat="server"
                                                                        AllowPaging="True"
                                                                        AllowSorting="True"
                                                                        PageSize="5"
                                                                        CellPadding="3"
                                                                        CssClass="table table-striped table-bordered"
                                                                        Height="17px"
                                                                        Style="font-size: x-small; text-align: left;"
                                                                        AutoGenerateColumns="False"
                                                                        DataKeyNames="IdInstitucion,Descripcion,Observaciones,Estatus"
                                                                        DataSourceID="SDSInstituciones">
                                                                        <Columns>
                                                                            <asp:BoundField
                                                                                DataField="Descripcion"
                                                                                HeaderText="Descripción"
                                                                                SortExpression="Descripcion" />
                                                                            <asp:BoundField
                                                                                DataField="Observaciones"
                                                                                HeaderText="Observaciones"
                                                                                SortExpression="Observaciones" />
                                                                            <asp:BoundField
                                                                                DataField="Estatus"
                                                                                HeaderText="Estatus"
                                                                                SortExpression="Estatus" />
                                                                        </Columns>
                                                                        <PagerTemplate>
                                                                            <ul runat="server" id="Pag" class="pagination">
                                                                            </ul>
                                                                        </PagerTemplate>
                                                                        <PagerStyle HorizontalAlign="Center" />
                                                                        <SelectedRowStyle CssClass="row-selected" />
                                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">


                                                        <asp:LinkButton ID="btnEliminar" Visible="false" runat="server"
                                                            CssClass="btn btn-labeled btn-darkorange">
                                                    <i class="btn-label fa fa-remove"></i>Eliminar
                                                        </asp:LinkButton>

                                                        <asp:LinkButton ID="btnActualizar" Visible="false" runat="server" type="submit"
                                                            CssClass="btn btn-labeled btn-palegreen">
                                                    <i class="btn-label fa fa-refresh"></i>Actualizar
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <uc1:msgSuccess runat="server" ID="msgSuccess2" />

                                                    <uc1:msgError runat="server" ID="msgError2" />
                                                </div>

                                            </div>

                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </div>
                            <div id="plantel" class="tab-pane">

                                <asp:UpdatePanel ID="UPPlanteles" runat="server">
                                    <ContentTemplate>
                                        <div class="form-horizontal" id="PlantelesForm">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="col-lg-6 ">
                                                        <div class="form-group">

                                                            <asp:Label ID="Label5" CssClass="col-lg-3" runat="server">
                                                    <%=Lang_Config.Translate("instituciones", "Descripcion_Plantel")%>
                                                            </asp:Label>

                                                            <div class="col-lg-9">
                                                                <asp:TextBox ID="TBdescripcionPlantel" runat="server" Rows="1" TextMode="MultiLine" Style="resize: none;"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">

                                                            <div class="col-lg-12">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="GVDatosPlanteles" runat="server"
                                                                        AllowPaging="True"
                                                                        AllowSorting="True"
                                                                        AutoGenerateColumns="False"
                                                                        DataSourceID="SDSinformacion"
                                                                        DataKeyNames="IdPlantel,IdInstitucion,Descripcion1"
                                                                        PageSize="6"
                                                                        CellPadding="3"
                                                                        CssClass="table table-striped table-bordered"
                                                                        Height="17px"
                                                                        Style="font-size: x-small; text-align: left;">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="Descripcion" HeaderText="Institución"
                                                                                SortExpression="Descripcion" />
                                                                            <asp:BoundField DataField="Descripcion1" HeaderText="Plantel"
                                                                                SortExpression="Descripcion1" />

                                                                        </Columns>
                                                                        <PagerTemplate>

                                                                            <ul class="pagination" runat="server" id="Pag">
                                                                            </ul>

                                                                        </PagerTemplate>
                                                                        <PagerStyle HorizontalAlign="Center" />
                                                                        <SelectedRowStyle CssClass="row-selected" />
                                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />

                                                                    </asp:GridView>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%-- <div class="form-group">
                                </div>
                                                    --%><%--<div class="centered">
                                           
                                        </div>--%>

                                                    <div class="form-group">
                                                        <asp:LinkButton ID="Insertar" runat="server"
                                                            CssClass="btn btn-labeled btn-palegreen" type="submit">
                                                   <i class="btn-label glyphicon glyphicon-ok"></i> Insertar
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="Limpiar" runat="server"
                                                            CssClass="btn btn-labeled btn-yellow">
                                                     <i class="btn-label fa fa-warning"></i> Limpiar
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="Actualizar" runat="server" Visible="false"
                                                            CssClass="btn btn-labeled btn-palegreen" type="submit">
                                                   <i class="btn-label fa fa-refresh"></i> Actualizar
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="Eliminar" runat="server" Visible="false"
                                                            CssClass="btn btn-labeled btn-darkorange">
                                                   <i class="btn-label glyphicon glyphicon-remove"></i> Eliminar
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <uc1:msgSuccess runat="server" ID="msgSuccess" />

                                                    <uc1:msgError runat="server" ID="msgError" />
                                                </div>

                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>














    <asp:SqlDataSource ID="SDScoordinadores" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdCoordinador, Nombre + ' ' + Apellidos Nom
from Coordinador"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSplanteles" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Plantel]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSinstituciones" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Institucion] ORDER BY [Descripcion]"></asp:SqlDataSource>



    <asp:SqlDataSource ID="SDSinformacion" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="select  I.IdInstitucion, I.Descripcion, P.IdPlantel, P.Descripcion
from  Institucion I, Plantel P
where P.IdInstitucion = I.IdInstitucion and I.IdInstitucion = @IdInstitucion
order by I.Descripcion, P.Descripcion">
        <SelectParameters>
            <asp:ControlParameter ControlID="gvInstituciones" Name="IdInstitucion"
                PropertyName="SelectedDataKey(IdInstitucion)" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" runat="Server">
</asp:Content>

