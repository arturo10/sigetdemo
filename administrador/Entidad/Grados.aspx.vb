﻿
Imports Siget

Partial Class administrador_AgruparReactivos
    Inherits System.Web.UI.Page
    Protected Sub Guardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Insertar.Click
        Try
            SDSgrados.InsertCommand = "SET dateformat dmy; INSERT INTO Grado (IdNivel,Descripcion,Estatus, FechaModif, Modifico) VALUES (" + DDLnivel.SelectedValue + ", '" + TBdescripcion.Text + "','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
            SDSgrados.Insert()
            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")), "El registro ha sido guardado")
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())

        End Try
    End Sub


    Protected Sub GVgradoscreados_OnRowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVgradoscreados.RowDataBound

        Dim cociente As Integer = (GVgradoscreados.PageCount) / 15
        Dim moduloIndex As Integer = (GVgradoscreados.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVgradoscreados.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVgradoscreados.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVgradoscreados.PageIndex
            final = IIf((inicial + 15 <= GVgradoscreados.PageCount), inicial + 15, GVgradoscreados.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVgradoscreados.PageCount), inicial + 15, GVgradoscreados.PageCount)
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVgradoscreados, "Page$" & counter.ToString)
            Next
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then

            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVgradoscreados, "Select$" & e.Row.RowIndex)

        End If
    End Sub


    Protected Sub GVgradoscreados_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVgradoscreados.RowCreated

        Dim cociente As Integer = (GVgradoscreados.PageCount / 15)
        Dim moduloIndex As Integer = (GVgradoscreados.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVgradoscreados.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVgradoscreados.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVgradoscreados.PageIndex
                final = IIf((inicial + 10 <= GVgradoscreados.PageCount), inicial + 15, GVgradoscreados.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVgradoscreados.PageCount, inicial + 15, GVgradoscreados.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVgradoscreados.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub



    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija el nivel del Grado", 0))
    End Sub

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Eliminar.Click
        Try
            SDSgrados.DeleteCommand = "Delete from Grado where IdGrado = " + GVgradoscreados.SelectedDataKey("IdGrado").ToString 'Me da el IdGrado porque es el campo clave de la fila seleccionada
            SDSgrados.Delete()
            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")), "El registro ha sido eliminado")
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())

        End Try
    End Sub

    Protected Sub GVgradoscreados_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVgradoscreados.SelectedIndexChanged
        Eliminar.Visible = True
        Actualizar.Visible = True
        TBdescripcion.Text = HttpUtility.HtmlDecode(GVgradoscreados.SelectedDataKey("Descripcion").ToString)
        DDLestatus.SelectedValue = GVgradoscreados.SelectedDataKey("Estatus").ToString
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Actualizar.Click
        Try
            SDSgrados.UpdateCommand = "SET dateformat dmy; UPDATE Grado set Descripcion = '" + TBdescripcion.Text + "', Estatus = '" + DDLestatus.SelectedValue + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where IdGrado = " + GVgradoscreados.SelectedDataKey("IdGrado").ToString    'Me da el IdGrado porque es el campo clave de la fila seleccionada
            SDSgrados.Update()
            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")), "El registro ha sido actualizado")
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())

        End Try
    End Sub

    Protected Sub GVgradoscreados_DataBound(sender As Object, e As EventArgs) Handles GVgradoscreados.DataBound
        If GVgradoscreados.Rows.Count = 0 Then
            Actualizar.Visible = False
            Eliminar.Visible = False
            GVgradoscreados.SelectedIndex = -1
        End If
    End Sub
End Class
