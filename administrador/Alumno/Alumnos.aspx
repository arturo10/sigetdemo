﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false"  ValidateRequest="false"
     CodeFile="alumnos.aspx.vb" Inherits="administrador_AgruparReactivos" EnableEventValidation="false"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>



<asp:Content Runat="server" ContentPlaceHolderID="Title">
 </asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="widget flat radius-bordered">
                <div class="widget-header bg-themeprimary">
                    <span class="widget-caption"><%=Lang_Config.Translate("alumnos", "header_participante")%></span>
                </div>

                <div class="widget-body">
                    <div class="widget-main ">
                        <div class="tabbable">
                            <ul class="nav nav-tabs tabs-flat" id="myTab11">
                                <li id="Home" class="active">
                                    <a data-toggle="tab" href="#home11">Administrar/Registrar
                                    </a>
                                </li>
                                <li>
                                    <a id="Messages" data-toggle="tab" href="#profile11">Buscar y Cambiar
                                    </a>
                                </li>
                                <li>
                                    <a id="Equipos" data-toggle="tab" href="#equiposSubEquipos">
                                        Equipos y Subequipos
                                    </a>
                                </li>
                                 <li>
                                    <a id="ImportaAlumno" data-toggle="tab" href="#importaEquiposSubequipos">
                                        <%=Lang_Config.Translate("alumnos", "header_tab_3")%>
                                    </a>
                                </li>
                                

                            </ul>
                            <div class="tab-content tabs-flat">
                                <div id="home11" class="tab-pane in active">
                                    <div class="form-horizontal" id="classmateForm">
                                          

                                        <div class="panel panel-default">
                                            <div class="panel-heading "  style="text-align: left;">

                                            
                                                    <i class="fa fa-group fa-lg"></i>
                                                    Campos Generales
                                           
                                          
                                            </div>
                                            <asp:UpdatePanel ID="AreaCrearAdministrarAlumnos" runat="server">
                                                <ContentTemplate>

                                                    <div class="panel-body" id="alumnoForm">
                                                        <div class="col-lg-12" style="position: absolute; top: 50%; left: 50%; bottom: 50%;">
                                                            <div class="load-spinner"></div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label2" CssClass="col-lg-2" runat="server">
                                                                    <%=Lang_Config.Translate("general","Institucion") %>

                                                            </asp:Label>
                                                                <div class="col-lg-4">
                                                                    <asp:DropDownList ID="DDLinstitucion"
                                                                        runat="server" AutoPostBack="True" DataSourceID="SDSinstituciones"
                                                                        DataTextField="Descripcion" DataValueField="IdInstitucion" 
                                                                        CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:Label ID="Label3" CssClass="col-lg-2" runat="server" >
                                                                                           <%=Lang_Config.Translate("general","Plantel") %>
                                                                </asp:Label>
                                                                <div class="col-lg-4">
                                                                    <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSplanteles" DataTextField="Descripcion" CssClass="form-control"
                                                                        DataValueField="IdPlantel">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label4" CssClass="col-lg-2" runat="server">
                                                                             <%=Lang_Config.Translate("general","Nivel") %>
                                                                </asp:Label>
                                                                <div class="col-lg-4">
                                                                    <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                                                                        CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:Label ID="Label5" CssClass="col-lg-2" runat="server" >
                                                                                              <%=Lang_Config.Translate("general","Grado") %>
                                                                </asp:Label>
                                                                <div class="col-lg-4">
                                                                    <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSgrados" DataTextField="Descripcion"
                                                                        DataValueField="IdGrado" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label6" CssClass="col-lg-2" runat="server" Text="Ciclo" />
                                                                <div class="col-lg-4">
                                                                    <asp:DropDownList ID="DDLcicloescolar" runat="server"
                                                                        DataSourceID="SDSciclosescolares" DataTextField="Ciclo"
                                                                        DataValueField="IdCicloEscolar" AutoPostBack="True"
                                                                        CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <asp:Label ID="Label7" CssClass="col-lg-2" runat="server" Text="Grupo" />
                                                                <div class="col-lg-4">
                                                                    <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSgrupos" DataTextField="Descripcion"
                                                                        DataValueField="IdGrupo" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-lg-11 col-lg-offset-1">
                                                                    <a data-toggle="modal"
                                                                        class="btn btn-labeled btn-palegreen" data-target=".alumnos-modal">
                                                                        <i class="btn-label glyphicon glyphicon-search"></i> <%=Lang_Config.Translate("alumnos","alumnos_disponibles") %>
                                                                    </a>
                                                                </div>
                                                            </div>


                                                            <asp:Panel runat="server" ID="ObjectAlumno">
                                                                <div class="col-lg-12 col-sm-12 col-xs-12">
                                                                    <div class="widget flat">
                                                                        <div class="widget-header bordered-bottom bordered-platinum">
                                                                            <span class="widget-caption">
                                                                                <label class="col-lg-2 col-xs-3 col-md-">
                                                                                    <i class="fa fa-user fa-lg"></i>
                                                                                </label>

                                                                                          <%=Lang_Config.Translate("general","titulo_objetoAlumno") %>

                                                                        
                                                                            </span>
                                                                            <div class="widget-buttons">
                                                                                <a href="#" data-toggle="maximize">
                                                                                    <i class="fa fa-expand sky"></i>
                                                                                </a>
                                                                                <a href="#" data-toggle="collapse">
                                                                                    <i class="fa fa-minus red"></i>
                                                                                </a>
                                                                            </div>
                                                                            <!--Widget Buttons-->
                                                                        </div>
                                                                        <!--Widget Header-->
                                                                        <div class="widget-body">

                                                                            <div class="form-group">

                                                                                <asp:Label ID="Label1" CssClass="col-lg-2" runat="server" Text="Nombre" />
                                                                                <div class="col-lg-4">
                                                                                    <asp:TextBox ID="TBnombre" runat="server" CssClass="form-control" >
                                                                                    </asp:TextBox>
                                                                                </div>
                                                                                <asp:Label ID="Label12" CssClass="col-lg-2" runat="server" Text="Apellido Paterno" />
                                                                                <div class="col-lg-4">
                                                                                    <asp:TextBox ID="TBApePaterno" runat="server" CssClass="form-control" >
                                                                                    </asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <asp:Label ID="LbApellidoPaterno" CssClass="col-lg-2" runat="server"
                                                                                    Text="Apellido Materno" />
                                                                                <div class="col-lg-4">
                                                                                    <asp:TextBox ID="TBApeMaterno" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                </div>
                                                                                <asp:Label ID="LbNombre" CssClass="col-lg-2" runat="server">

                                                                                    <%=Lang_Config.Translate("alumnos","matricula_alumno")%>
                                                                                    
                                                                                    
                                                                                </asp:Label>
                                                                                <div class="col-lg-4">
                                                                                    <asp:TextBox ID="TBmatricula" runat="server" CssClass="form-control"
                                                                                       ></asp:TextBox>
                                                                                </div>

                                                                            </div>
                                                                            <div class="form-group">
                                                                                <asp:Label ID="Label9" CssClass="col-lg-2" runat="server" Text="Equipo" />
                                                                                <div class="col-lg-4">
                                                                                    <asp:DropDownList ID="DDLequipo" DataSourceID="SDSEquipos" DataValueField="Descripcion" DataTextField="Descripcion" runat="server" CssClass="form-control">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                                <asp:Label ID="Label10" CssClass="col-lg-2" runat="server" Text="Subequipo" />
                                                                                <div class="col-lg-4">
                                                                                    <asp:DropDownList ID="DDLsubequipo" DataSourceID="SDSSubEquipos" DataValueField="Descripcion" DataTextField="Descripcion" runat="server" CssClass="form-control">
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <asp:Label ID="Label13" CssClass="col-lg-2" runat="server" Text="Email" />
                                                                                <div class="col-lg-4">
                                                                                    <asp:TextBox ID="TBemail" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                                                </div>
                                                                                <asp:Label ID="Label14" CssClass="col-lg-2" runat="server" Text="Fecha Ingreso" placeholder="Date" />
                                                                                <div class="col-lg-4">
                                                                                    <asp:TextBox ID="TBingreso" ClientIDMode="Static" runat="server" disabled="true"
                                                                                        CssClass="form-control" ></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <asp:Label ID="Label11" CssClass="col-lg-2" runat="server"><b>Usuario</b></asp:Label>
                                                                                <div class="col-lg-4">
                                                                                    <asp:TextBox ID="TBLogin" runat="server" CssClass="form-control" 
                                                                                       ></asp:TextBox>
                                                                                </div>
                                                                                <asp:Label ID="Label16" CssClass="col-lg-2" runat="server"><b>Contraseña</b></asp:Label>
                                                                                <div class="col-lg-4">
                                                                                    <asp:TextBox ID="TBPassword" 
                                                                                        ClientIDMode="Static" 
                                                                                         runat="server" CssClass="form-control"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <asp:Label ID="Label15" CssClass="col-lg-2" runat="server" Text="Estatus" />
                                                                                <div class="col-lg-10">
                                                                                    <asp:DropDownList ID="DDLestatus" CssClass="form-control" runat="server">
                                                                                        <asp:ListItem>Activo</asp:ListItem>
                                                                                        <asp:ListItem>Suspendido</asp:ListItem>
                                                                                        <asp:ListItem>Baja</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <!--Widget Body-->
                                                                    </div>
                                                                    <!--Widget-->
                                                                </div>
                                                            </asp:Panel>

                                                            <asp:LinkButton ID="Insertar" type="submit" runat="server"
                                                                CssClass="Alcre btn  btn-lg btn-labeled btn-palegreen"
                                                                data-container="body"
                                                                data-titleclass="bordered-warning"
                                                                data-class="black"
                                                                data-toggle="popover-hover"
                                                                data-placement="top"
                                                                data-title="Si inserta..."
                                                                data-content="*No impacta la tabla de usuarios ni las de 
                                                                 seguridad, solo la de alumnos, por eso se deshabilita en esta preagina. 
                                                                    Usar solo para modificar"
                                                                data-original-title="" title="">
                                                                <i class="btn-label glyphicon glyphicon-ok"></i>
                                                                              Insertar
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="Limpiar" runat="server"
                                                                CssClass=" btn  btn-lg btn-labeled btn-blue">
                                                                <i class="btn-label fa fa-eraser"></i>Limpiar
                                                            </asp:LinkButton>

                                                            <asp:LinkButton ID="Actualizar" Visible="false" type="submit" runat="server"
                                                                CssClass="Alcre btn btn-lg btn-labeled btn-palegreen">
                                                                <i class="btn-label fa fa-refresh"></i>Actualizar
                                                            </asp:LinkButton>

                                                           

                                                        </div>
                                                
                                                    <div class="col-lg-12">
                                                        <uc1:msgError runat="server" ID="msgError" />
                                                        <uc1:msgSuccess runat="server" ID="msgSuccess" />
                                                    </div>
                                                    <div>
                                                        <blockquote>
                                                            <small>NOTA:</small>
                                                            <p>
                                                                Estatus <span style="color: red;">SUSPENDIDO</span>: 
                                                                
                                                            <%=Lang_Config.Translate("alumnos","nota_suspendido") %>
                                                            </p>

                                                        </blockquote>

                                                        
                                                    </div>

                                                    <!-- end of body-widget-->
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>

                                <div id="profile11" class="tab-pane">
                                   
                                    
                                <div class="form-horizontal">

                                    <div class="panel panel-default">

                                        <div class="panel-heading bg-themeprimary-lg">

                                            <b>ÁREA PARA BUSCAR PARTICIPANTE</b>
                                        </div>

                                        <div class="panel-body">

                                            <asp:UpdatePanel runat="server">
                                                <ContentTemplate>

                                                    <div class="col-lg-6">
                                                        <div class="widget">
                                                            <div class="widget-header bordered-bottom bordered-themefourthcolor">
                                                                <i class="widget-icon fa fa-tags themefourthcolor"></i>
                                                                <span class="widget-caption themefourthcolor">Área para  búsqueda</span>
                                                            </div>
                                                            <!--Widget Header-->
                                                            <div class="widget-body  no-padding">
                                                                <div class="tickets-container">
                                                                    <div class="form-group">
                                                                         <asp:Label ID="Label17" runat="server" CssClass="col-lg-2 control-label">
                                                                             <%=Lang_Config.Translate("general","Institucion") %>
                                                                         </asp:Label>
                                                                        <div class="col-lg-10">
                                                                            <asp:DropDownList ID="DDLInstitucionCambiar"
                                                                                runat="server" AutoPostBack="True" DataSourceID="SDSinstituciones"
                                                                                DataTextField="Descripcion" DataValueField="IdInstitucion"
                                                                                CssClass="form-control">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                         <asp:Label ID="Label18" runat="server" CssClass="col-lg-2 control-label" Text="Matricula" />
                                                                        <div class="col-lg-10">
                                                                            <asp:TextBox ID="TBMatriculaCambiar" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </div>

                                                                    </div>

                                                                     <div class="form-group">
                                                                         <asp:Label ID="Label19" runat="server" CssClass="col-lg-2 control-label" Text="Nombre" />
                                                                        <div class="col-lg-10">
                                                                            <asp:TextBox ID="TBNombreCambiar" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </div>

                                                                    </div>
                                                                    
                                                                     <div class="form-group">
                                                                         <asp:Label ID="Label20" runat="server" CssClass="col-lg-2 control-label" Text="Apellido Paterno" />
                                                                        <div class="col-lg-10">
                                                                            <asp:TextBox ID="TBApePaternoCambiar" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </div>

                                                                    </div>

                                                                    
                                                                     <div class="form-group">
                                                                         <asp:Label ID="Label21" runat="server" CssClass="col-lg-2 control-label" Text="Apellido Materno" />
                                                                        <div class="col-lg-10">
                                                                            <asp:TextBox ID="TBApeMaternoCambiar" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </div>

                                                                    </div>

                                                                     <div class="form-group">
                                                                         <asp:Label ID="Label22" runat="server" CssClass="col-lg-2 control-label" Text="Login" />
                                                                        <div class="col-lg-10">
                                                                            <asp:TextBox ID="TBLoginCambiar" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        </div>

                                                                    </div>
                                                                    <div class="form-group">
                                                                        <asp:LinkButton ID="BtnBuscar" runat="server" 
                                                                            CssClass="btn btn-labeled btn-palegreen" >
                                                                            <i class="btn-label fa fa-search"></i>Buscar
                                                                        </asp:LinkButton>

                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>



                                                    
                                                                        <div class="col-lg-6">
                                                        <div class="widget">
                                                            <div class="widget-header bordered-bottom bordered-themesecondary">
                                                                <i class="widget-icon fa fa-tags themesecondary"></i>
                                                                <span class="widget-caption themesecondary">Área para cambio de grupo</span>
                                                            </div>
                                                            <!--Widget Header-->
                                                            <div class="widget-body  no-padding">
                                                                <asp:UpdatePanel runat="server">
                                                                    <ContentTemplate>
                                                                <div class="tickets-container">

                                                                    <div class="form-group">
                                                                        <label class="col-lg-2 control-label">Ciclo escolar</label>
                                                                        <div class="col-lg-10">
                                                                            <asp:DropDownList ID="DDLCicloEscolarCambiar" runat="server" AutoPostBack="True"
                                                                                DataSourceID="SDSciclosescolares" DataTextField="Ciclo"
                                                                                DataValueField="IdCicloEscolar"
                                                                                CssClass="form-control" >
                                                                          
                                                                            </asp:DropDownList>
                                                                            
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                           <label class="col-lg-2 control-label"><%=Lang_Config.Translate("general","Plantel") %></label>
                                                                        <div class="col-lg-10">
                                                                            <asp:DropDownList ID="DDLPlantelCambiar" runat="server" AutoPostBack="True"
                                                                                DataSourceID="SDSPlantelesAreaCambiar" DataTextField="Descripcion"
                                    
                                                                                DataValueField="IdPlantel" CssClass="form-control">
                                                                     
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>

                                                                       <div class="form-group">
                                                                           <label class="col-lg-2 control-label"><%=Lang_Config.Translate("general","Nivel") %></label>
                                                                           <div class="col-lg-10">
                                                                               <asp:DropDownList ID="DDLNivelCambiar" runat="server" AutoPostBack="True"
                                                                                   DataSourceID="SDSNivelesAreaCambiar" DataTextField="Descripcion" DataValueField="IdNivel"
                                                                                   CssClass="form-control" >
                                                                                  
                                                                               </asp:DropDownList>
                                                                           </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-lg-2 control-label"><%=Lang_Config.Translate("general","AreaConocimiento") %></label>
                                                                        <div class="col-lg-10">
                                                                                <asp:DropDownList ID="DDLgradoCambiar" runat="server" AutoPostBack="True"
                                                                                    DataSourceID="SDSGradosAreaCambiar" DataTextField="Descripcion"
                                                                                    DataValueField="IdGrado"  CssClass="form-control">
                                                              
                                                                                </asp:DropDownList>
                                                                        </div>
                                                                    </div>


                                                                    <div class="form-group">
                                                                        <label class="col-lg-2 control-label">Grupo</label>
                                                                        <div class="col-lg-10">
                                                                            <asp:DropDownList ID="DDLGrupoCambiar" runat="server" AutoPostBack="True"
                                                                                DataSourceID="SDSGruposCambiar" DataTextField="Descripcion"
                                                                                DataValueField="IdGrupo" CssClass="form-control">
                                                              
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="checkbox">
                                                                            <label>
                                                                                <input id="CBactividades" runat="server" type="checkbox" class="colored-danger" checked="checked">
                                                                                <span class="text">Mover Actividades Terminadas</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <asp:LinkButton ID="BtnCambiar" runat="server" 
                                                                            CssClass="btn btn-labeled btn-darkorange" >
                                                                            <i class="btn-label fa fa-retweet"></i>Cambiar
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                  
                                                                
                                                                </div>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>

                                                      </div>

                                                    <div class="col-lg-12">
                                                        <div class="widget">
                                                            <div class="widget-header bordered-bottom bordered-themefourthcolor">
                                                                <i class="widget-icon fa fa-tags themefourthcolor"></i>
                                                                <span class="widget-caption themefourthcolor"><%=Lang_Config.Translate("alumnos","Area_elegir_alumno") %></span>
                                                            </div>
                                                            <!--Widget Header-->
                                                            <div class="widget-body  no-padding">
                                                                <div class="tickets-container">
                                                                    <div class="table-responsive">
                                                                        <asp:GridView ID="GVAlumnosBuscar" runat="server"
                                                                            
                                                                            AutoGenerateColumns="False"
                                                                            AllowPaging="true"
                                                                            DataSourceID="SDSbuscar"
                                                                            DataKeyNames="IdAlumno,Password,Estatus,Login,Matricula,IdGrupo,IdGrado"
                                                                            PageSize="5"
                                                                            CellPadding="3"
                                                                            CssClass="table table-striped table-bordered"
                                                                            Height="17px"
                                                                            Style="font-size: x-small; text-align: left;">
                                                                            <Columns>
                                                                                <asp:BoundField DataField="IdAlumno" Visible="false" HeaderText="IdAlumno" />
                                                                                <asp:BoundField DataField="Matricula" HeaderText="Matricula" />
                                                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                                                                <asp:BoundField DataField="ApePaterno" HeaderText="ApePaterno" />
                                                                                <asp:BoundField DataField="ApeMaterno" HeaderText="ApeMaterno" />
                                                                                <asp:BoundField DataField="IdGrupo" Visible="false" HeaderText="IdGrupo" />
                                                                                <asp:BoundField DataField="Grupo" HeaderText="Grupo" />
                                                                                <asp:BoundField DataField="IdGrado" Visible="false" HeaderText="IdGrado" />
                                                                                <asp:BoundField DataField="Grado" HeaderText="Grado" />
                                                                                <asp:BoundField DataField="IdNivel" Visible="false" HeaderText="IdNivel" />
                                                                                <asp:BoundField DataField="Nivel" Visible="false" HeaderText="Nivel" />
                                                                                <asp:BoundField DataField="IdPlantel" Visible="false" HeaderText="IdPlantel" />
                                                                                <asp:BoundField DataField="Plantel" HeaderText="Plantel" />
                                                                                <asp:BoundField DataField="Login" HeaderText="Login" />
                                                                                <asp:BoundField DataField="Password" HeaderText="Password" />
                                                                                <asp:BoundField DataField="Estatus" HeaderText="Estatus" />
                                                                                <asp:BoundField DataField="Equipo" Visible="false" HeaderText="Equipo" />
                                                                                <asp:BoundField DataField="Subequipo" Visible="false" HeaderText="Subequipo" />
                                                                                <asp:BoundField DataField="Ciclo" Visible="false" HeaderText="Ciclo" />
                                                                                <asp:BoundField DataField="Email" Visible="false" HeaderText="Email" />
                                                                            </Columns>
                                                                            <PagerTemplate>
                                                                                <ul runat="server" id="Pag" class="pagination">
                                                                                </ul>
                                                                            </PagerTemplate>
                                                                            <PagerStyle HorizontalAlign="Center" />
                                                                            <SelectedRowStyle CssClass="row-selected" />
                                                                            <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                        </asp:GridView>
                                                                    </div>


                                                                </div>
                                                                </div>
                                                            </div>
                                                     
                                                        </div>

                                                          <div class="col-lg-6 col-lg-offset-3">
                                                                <div class="widget">
                                                                    <div class="widget-header bordered-bottom bordered-themesecondary">
                                                                        <i class="widget-icon fa fa-tags themesecondary"></i>
                                                                        <span class="widget-caption themesecondary">Área para cambio de password</span>
                                                                    </div>
                                                                    <!--Widget Header-->
                                                                    <div class="widget-body  no-padding">
                                                                        <div class="tickets-container">
                                                                            <div class="form-group">
                                                                                <label class="col-lg-2 control-label">Password</label>
                                                                                <div class="col-lg-10">
                                                                                    <asp:TextBox runat="server" ID="TBPasswordCambiar" CssClass="form-control"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-lg-2 control-label">Estatus</label>
                                                                                <div class="col-lg-10">
                                                                                    <asp:DropDownList ID="DDLEstatusCambiar" CssClass="form-control" runat="server">
                                                                                        <asp:ListItem>Activo</asp:ListItem>
                                                                                        <asp:ListItem>Suspendido</asp:ListItem>
                                                                                        <asp:ListItem>Baja</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <asp:LinkButton runat="server" ID="BtnActualizarCambiar"
                                                                                    CssClass="btn btn-labeled btn-palegreen shiny">
                                                                                    <i class="btn-label fa fa-refresh"></i>Actualizar
                                                                                </asp:LinkButton>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <uc1:msgSuccess runat="server" ID="msgSuccess2" />
                                                                                <uc1:msgError runat="server" ID="msgError2" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </div>


                                </div>

                                <div id="equiposSubEquipos" class="tab-pane">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <div class="row">

                                         
                                            <div class="form-horizontal" id="EquiposSubequiposForm">
                                                
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label"><%=Lang_Config.Translate("general","Institucion") %></label>
                                                            <div class="col-lg-6">
                                                                <asp:DropDownList ID="DDLInstitucionAlta"
                                                                    runat="server" AutoPostBack="True" DataSourceID="SDSinstituciones"
                                                                    DataTextField="Descripcion" DataValueField="IdInstitucion" CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-12">
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading bg-themeprimary-lg" style="text-align: left;">
                                                                    <i class="fa fa-object-group"></i>
                                                                    Equipos
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <label class="col-lg-2 control-label">
                                                                            Descripcion
                                                                        </label>
                                                                        <div class="col-lg-8">
                                                                            <asp:TextBox ID="TBequipoAlta" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                    <div class="col-lg-2"></div>
                                                                    <div class="col-lg-8">
                                                                        <div class="table-responsive ">
                                                                            <asp:GridView ID="GVequipos" runat="server"
                                                                                AllowPaging="True"
                                                                                AllowSorting="True"
                                                                                AutoGenerateColumns="False"
                                                                                DataKeyNames="IdEquipo,Descripcion"
                                                                                DataSourceID="SDSEquipoAlta"
                                                                                Caption="<h3>EQUIPOS capturados</h3>"
                                                                                PageSize="5"
                                                                                CellPadding="3"
                                                                                CssClass="table table-striped table-bordered"
                                                                                Height="17px"
                                                                                Style="font-size: x-small; text-align: left;">
                                                                                <Columns>
                                                                               
                                                                                    <asp:BoundField DataField="IdEquipo" HeaderText="Id Equipo"
                                                                                        InsertVisible="False" ReadOnly="True" SortExpression="IdEquipo">
                                                                                        <HeaderStyle Width="60px" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="IdInstitucion" HeaderText="Id Institución"
                                                                                        SortExpression="IdInstitucion">
                                                                                        <HeaderStyle Width="90px" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripción" SortExpression="Descripcion" />

                                                                                </Columns>
                                                                                <PagerTemplate>
                                                                                    <ul runat="server" id="Pag" class="pagination">
                                                                                    </ul>
                                                                                </PagerTemplate>
                                                                                <PagerStyle HorizontalAlign="Center" />
                                                                                <SelectedRowStyle CssClass="row-selected" />
                                                                                <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />

                                                                            </asp:GridView>
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                                   
                                                                      <div class="form-group">
                                                                            <asp:LinkButton ID="btnCrearEquipo" runat="server" type="submit"
                                                                            CssClass="btn btn-labeled btn-palegreen shiny" >
                                                                            <i class="btn-label fa fa-plus"></i>Crear
                                                                        </asp:LinkButton>
                                                                            <asp:LinkButton ID="btnActualizarEquipo" type="submit" runat="server" Visible="false"
                                                                            CssClass="btn btn-labeled btn-palegreen" >
                                                                            <i class="btn-label fa fa-retweet"></i>Actualizar
                                                                        </asp:LinkButton>
                                                                            <asp:LinkButton ID="btnEliminarEquipo" runat="server" Visible="false"
                                                                            CssClass="btn btn-labeled btn-darkorange" >
                                                                            <i class="btn-label fa fa-remove"></i>Eliminar
                                                                        </asp:LinkButton>
                                                                    </div>


                                                                </div>

                                                            </div>

                                                        </div>

                                                        <div class="col-lg-12">
                                                               <div class="panel panel-default">
                                                                <div class="panel-heading bg-themeprimary-lg" style="text-align: left;">
                                                                    <i class="fa fa-object-group"></i>
                                                                    SubEquipos
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div class="form-group">
                                                                        <label class="col-lg-2 control-label">
                                                                            Descripción
                                                                        </label>
                                                                        <div class="col-lg-8">
                                                                            <asp:TextBox ID="TBsubequipoAlta" CssClass="form-control" runat="server" MaxLength="20"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="table-responsive">
                                                                            <div class="col-lg-2"></div>
                                                                            <div class="col-lg-8">
                                                                            <asp:GridView ID="GVsubequipos" runat="server"
                                                                                AllowPaging="True" 
                                                                                AllowSorting="True"
                                                                                AutoGenerateColumns="False"
                                                                                Caption="<h3>SUBEQUIPOS capturados</h3>"
                                                                                DataKeyNames="IdSubequipo,Descripcion"
                                                                                DataSourceID="SDSSubquiposAlta"
                                                                                PageSize="5"
                                                                                CellPadding="3"
                                                                                CssClass="table table-striped table-bordered"
                                                                                Height="17px"
                                                                                Style="font-size: x-small; text-align: left;">
                                                                                <Columns>
                                                                                  
                                                                                    <asp:BoundField DataField="IdSubequipo" HeaderText="Id Subequipo"
                                                                                        InsertVisible="False" ReadOnly="True" SortExpression="IdSubequipo">
                                                                                        <HeaderStyle Width="90px" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="IdInstitucion" HeaderText="Id Institución"
                                                                                        SortExpression="IdInstitucion">
                                                                                        <HeaderStyle Width="90px" />
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripción"
                                                                                        SortExpression="Descripcion" />
                                                                                </Columns>
                                                                                <PagerTemplate>
                                                                                    <ul runat="server" id="Pag" class="pagination">
                                                                                    </ul>
                                                                                </PagerTemplate>
                                                                                <PagerStyle HorizontalAlign="Center" />
                                                                                <SelectedRowStyle CssClass="row-selected" />
                                                                                <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                            </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <asp:LinkButton ID="btnCrearSubequipo" runat="server" type="submit"
                                                                            CssClass="btn btn-labeled btn-palegreen shiny">
                                                                            <i class="btn-label fa fa-plus"></i>Crear
                                                                        </asp:LinkButton>
                                                                        <asp:LinkButton ID="btnActualizarSubEquipo"  runat="server" Visible="false" type="submit"
                                                                            CssClass="btn btn-labeled btn-palegreen">
                                                                            <i class="btn-label fa fa-retweet"></i>Actualizar
                                                                        </asp:LinkButton>
                                                                        <asp:LinkButton ID="btnEliminarSubequipo" runat="server" Visible="false"
                                                                            CssClass="btn btn-labeled btn-darkorange">
                                                                            <i class="btn-label fa fa-remove"></i>Eliminar
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                               </div>
                                                        </div>
                                                <div class="col-lg-12">
                                                    <uc1:msgSuccess runat="server" ID="msgSuccess3" />
                                                    <uc1:msgError runat="server" ID="msgError3" />
                                                </div>
                                            </div>

                                            </div>
                                            <!-- End of panel body -->
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>


                                <div id="importaEquiposSubequipos" class="tab-pane">
                                    <asp:UpdatePanel runat="server">
                                       
                                        <ContentTemplate>

                                 
                                       <table class="style15">
                                <tr>
                                    <td class="style24" colspan="3">
                                        <strong>El archivo a importar debe ser de texto o csv con los campos separados por el caracter | y el contenido no debe tener apóstrofes (&#39;).<br />El archivo debe incluir todas las columnas aunque alguna(s) no tenga(n) valores (dejarlas en blanco).</strong></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <table class="style11">
                                            <tr>
                                                <td class="style16">
                                                    <table border="0" class="style11">
                                                        <tr>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FF5050">campo con valor obligatorio</td>
                                                            <td class="auto-style4"></td>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FFFF66">campo con valor opcional</td>
                                                            <td class="auto-style12"></td>
                                                            <td class="auto-style12"></td>
                                                            <td class="auto-style12"></td>
                                                            <td class="auto-style12"></td>
                                                            <td class="auto-style12"></td>
                                                            <td class="auto-style12"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style10"></td>
                                                            <td class="auto-style11"></td>
                                                            <td class="auto-style10"></td>
                                                            <td class="auto-style10"></td>
                                                            <td class="auto-style10"></td>
                                                            <td class="auto-style10"></td>
                                                            <td class="auto-style10"></td>
                                                            <td class="auto-style10"></td>
                                                            <td class="auto-style13">Mín [4]</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FF5050;"><b style="background-color: #FF5050">IdGrupo</b></td>
                                                            <td class="auto-style5" style="border: thin solid #000000; background-color: #FF5050;"><b style="background-color: #FF5050">Nombre</b></td>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FF5050;"><b style="background-color: #FF5050">ApePaterno</b></td>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FF5050;"><b style="background-color: #FF5050">ApeMaterno</b></td>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FFFF66;"><b style="background-color: #FFFF66">Matrícula</b></td>
                                                            <td class="auto-style3" style="border: thin solid #000000; background-color: #FFFF66;">Equipo</td>
                                                            <td class="auto-style3" style="border: thin solid #000000; background-color: #FFFF66;">Subequipo</td>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FF5050;"><b style="background-color: #FF5050">Login</b></td>
                                                            <td class="auto-style9" style="border: thin solid #000000; background-color: #FF5050;"><b style="background-color: #FF5050">Password</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style8" style="border: thin solid #000000">[int]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">[40]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">[30]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">[30]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">[20]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">[100]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">[20]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">[100]</td>
                                                            <td class="auto-style8" style="border: thin solid #000000">&nbsp;[20]</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <span class="style24">Cargue el archivo que contiene los registros para importar 
                                        a la base de datos:</span></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:AsyncFileUpload ID="ArchivoCarga" runat="server" />
                                        </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <span class="style14">Se generarán los registros en la tablas de Usuario y 
                                        <%=Lang_Config.Translate("general","Alumno") %> . Si no se elige Registrar Usuarios, posteriormente el 
																						<%=Lang_Config.Translate("general","Alumno") %> &nbsp;deberá generar sus cuentas de acceso predefinidas con los datos que se le proporcionarán (Login y Matrícula) para que proceda a su registro y reciba un correo con su password</span></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="CBregistrarme" runat="server" 
                                            style="font-weight: 700; font-size: small; font-family: Arial, Helvetica, sans-serif;" 
                                            Text="Registrar Usuarios" Checked="True" />
                                    </td>
                                    <td class="style24" style="text-align: right">
                                        <b>Correo electrónico para registrar usuarios:</b></td>
                                    <td>
                                        <asp:TextBox ID="TBemailAlumno" runat="server" Width="260px"></asp:TextBox>
                                    </td>
                                </tr>
                                           <tr>
                                               <td colspan="3" style="font-size: medium;">
                                                   <asp:Button ID="Importar" runat="server"
                                                       CssClass="defaultBtn btnThemeBlue btnThemeMedium"
                                                       Text="Importar" />
                                               </td>
                                           </tr>
                                       </table>

                                            <div class="row">
                                                <div class="form-horizontal">
                                                    <div class="panel panel-default">
                                                        <div class="form-group">
                                                            <div class="col-lg-8 col-lg-offset-2">

                                                         
                                                            <div class="table-responsive" >
                                                                <asp:GridView ID="GvAlumnosRepetidos" runat="server"
                                                                    
                                                                    Caption="<h2><b>USUARIOS CON PROBLEMA AL DARSE DE ALTA</b></h2>"    
                                                                    CellPadding="3"
                                                                    CssClass="table table-striped table-bordered"
                                                                    Height="17px"
                                                                    Style="font-size: x-small; text-align: left;">
                                                                    <Columns>
                                                                       
                                                                    </Columns>
                                                                   
                                                                    <PagerStyle HorizontalAlign="Center" />
                                                                    <SelectedRowStyle CssClass="row-selected" />
                                                                    <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>


                                                            </div>

                                                                   </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </ContentTemplate>

                                    </asp:UpdatePanel>

                                </div>




                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <table class="dataSources">

        
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSSubEquipos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [SubEquipo] WHERE IdInstitucion=@IdInstitucion ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLInstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32"  />
                  
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                   <asp:SqlDataSource ID="SDSSubquiposAlta" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Subequipo] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucionAlta" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                 <asp:SqlDataSource ID="SDSbuscar" runat="server"></asp:SqlDataSource>
            </td>
            <td>
                   <asp:SqlDataSource ID="SDSevaluacionesterm" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [EvaluacionTerminada]"></asp:SqlDataSource>
            </td>
            <td>
                
                <asp:SqlDataSource ID="SDSrespuestas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Respuesta]"></asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSComentarioEvaluacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [ComentarioEvaluacion]"></asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSEquipos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Equipo] WHERE IdInstitucion=@IdInstitucion ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLInstitucion" Name="IdInstitucion" 
                            PropertyName="SelectedValue" Type="Int32" />
                  
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
             <td>
                <asp:SqlDataSource ID="SDSEquipoAlta" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Equipo] WHERE IdInstitucion=@IdInstitucion ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLInstitucionAlta" Name="IdInstitucion" 
                            PropertyName="SelectedValue" Type="Int32" />
                  
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>

        <tr>
            <td>

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Institucion] ORDER BY [Descripcion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion)
ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
             <td>

                <asp:SqlDataSource ID="SDSPlantelesAreaCambiar" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion)
ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucionCambiar" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select N.IdNivel, N.Descripcion
from Nivel N, Escuela E, Plantel P
where N.IdNivel = E.IdNivel and P.IdPlantel = E.IdPlantel
      and P.IdPlantel = @IdPlantel
Order by Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSNivelesAreaCambiar" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select N.IdNivel, N.Descripcion
from Nivel N, Escuela E, Plantel P
where N.IdNivel = E.IdNivel and P.IdPlantel = E.IdPlantel
      and P.IdPlantel = @IdPlantel
Order by Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLPlantelCambiar" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel) and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
             <td>

                <asp:SqlDataSource ID="SDSGradosAreaCambiar" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel) and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivelCambiar" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grupo] 
WHERE ([IdGrado] = @IdGrado) and ([IdPlantel] = @IdPlantel) and ([IdCicloEscolar] = @IdCicloEscolar)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSGruposCambiar" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grupo] 
WHERE ([IdGrado] = @IdGrado) and ([IdPlantel] = @IdPlantel) and ([IdCicloEscolar] = @IdCicloEscolar)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgradoCambiar" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLplantelCambiar" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolarCambiar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSalumnos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Alumno]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSlistado" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select A.IdAlumno, A.ApePaterno, A.ApeMaterno, A.Nombre, A.Matricula, A.FechaIngreso, A.Email, U.Login, U.Password, A.Estatus, A.Equipo, A.Subequipo
from Alumno A, Usuario U
where A.IdGrupo = @IdGrupo and U.IdUsuario = A.IdUsuario
order by A.ApePaterno, A.ApeMaterno, A.Nombre,U.Login">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdCicloEscolar, Descripcion + ' (' + Cast(FechaInicio as varchar(12)) + ' - ' + Cast(FechaFin as varchar(12)) + ')' Ciclo
FROM CicloEscolar
WHERE (Estatus = 'Activo')
order by Descripcion"></asp:SqlDataSource>

            </td>
            <td>
                       <asp:SqlDataSource ID="SDSusuarios" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Usuario]">
                               
                            </asp:SqlDataSource>
            </td>
        </tr>
    </table>



    <div class="modal fade alumnos-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <asp:UpdatePanel ID="panelAlumnos" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="DDLGrupo" />
                        <asp:PostBackTrigger ControlID="BtnExportar" />

                    </Triggers>
                    <ContentTemplate>
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myLargeModalLabel"><b> <%=Lang_Config.Translate("alumnos","alumnos_disponibles") %> </b></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                            <div class="table-responsive">
                                <asp:GridView ID="GVAlumnos" runat="server"
                                    AllowSorting="True" 
                                    AllowPaging="true" 
                                    AutoGenerateColumns="False"
                                    DataKeyNames="IdAlumno, Nombre, ApePaterno, ApeMaterno,
                                    Matricula,Equipo,SubEquipo,Email,FechaIngreso,Login,Password,Estatus"
                                    DataSourceID="SDSlistado"
                                    PageSize="10"
                                    CellPadding="3"
                                    CssClass="table table-striped table-bordered"
                                    Height="17px"
                                    Style="font-size: x-small; text-align: left;">
                                    <Columns>
                                        <asp:BoundField DataField="IdAlumno" HeaderText="[IdAlumno]" Visible="false"
                                            InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno" />
                                        <asp:BoundField DataField="ApePaterno" HeaderText="Ap. Paterno"
                                            SortExpression="ApePaterno" />
                                        <asp:BoundField DataField="ApeMaterno" HeaderText="Ap. Materno"
                                            SortExpression="ApeMaterno" />
                                        <asp:BoundField DataField="Nombre" HeaderText="Nombre"
                                            SortExpression="Nombre" />
                                        <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                                            SortExpression="Matricula">
                                            <ItemStyle Width="85px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                        <asp:BoundField DataField="FechaIngreso" DataFormatString="{0:dd/MM/yyyy}"
                                            HeaderText="Ingreso" SortExpression="FechaIngreso" />
                                        <asp:BoundField DataField="Login" HeaderText="Login" SortExpression="Login" />
                                        <asp:BoundField DataField="Password" HeaderText="Password"
                                            SortExpression="Password" />
                                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                            SortExpression="Estatus" />
                                        <asp:BoundField DataField="Equipo"  HeaderText="[Equipo]" SortExpression="Equipo" />
                                        <asp:BoundField DataField="Subequipo"  HeaderText="[Subequipo]"
                                            SortExpression="Subequipo" />
                                    </Columns>
                                    <PagerTemplate>
                                        <ul runat="server" id="Pag" class="pagination">
                                        </ul>
                                    </PagerTemplate>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <SelectedRowStyle CssClass="row-selected" />
                                    <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                </asp:GridView>
                                <%-- sort YES - rowdatabound
	                            0  select
	                            1  IdALUMNO
	                            2  appaterno
	                            3  apmaterno
	                            4  nombre
	                            5  matricula
	                            6  email
	                            7  fechaingreso
	                            8  login
	                            9  pass
	                            10 estatus
                                11 EQUIPO
                                12 SUBEQUIPO
        --%>
                            </div>
                            </div>
                            <div class="form-group">
                                <a id="btnCerrarModal" data-dismiss="modal" aria-hidden="true"
                                    class="btn  btn-lg btn-labeled btn-warning">
                                    <i class="btn-label fa fa-minus"></i>Cerrar
                                </a>
                                 <asp:LinkButton ID="Eliminar" Visible="false" runat="server"
                                                                CssClass="btn btn-lg btn-labeled btn-darkorange"
                                                                data-container="body"
                                                                data-titleclass="bordered-warning"
                                                                data-class="black"
                                                                data-toggle="popover-hover"
                                                                data-placement="top"
                                                                data-title="Si Borra..."
                                                                data-content="*Borra todos los registros del usuario, 
                                            incluyendo los de su usuario 
                                            en las tablas de seguridad"
                                                                data-original-title="" title="">
                                             <i class="btn-label fa fa-trash-o"></i>Eliminar
                                                            </asp:LinkButton>
                            </div>
                             <div class="form-group">
                            <asp:LinkButton ID="BtnExportar" runat="server" 
                                CssClass="btn btn-lg btn-labeled btn-palegreen space-margin">
                            <i class="btn-label fa fa-table"></i>
                             Exportar a Excel
                            </asp:LinkButton>
                        </div>

                        <div class="form-group">
                            
                                <uc1:msgSuccess runat="server" ID="msgSuccess4" />
                                <uc1:msgError runat="server" ID="msgError4" />
                         
                        </div>
                        </div>

                       

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">
   
  
         <%= Scripts.Render("~/bundles/input-mask") %>
    <script type="text/javascript">
       
        Sys.Application.add_load(LoadHandler);

        function LoadHandler(sender, args0) {

            $(function () {

                $("#classmateForm").bootstrapValidator();   
            });

        };
    </script>

</asp:Content>

