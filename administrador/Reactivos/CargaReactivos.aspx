﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false" CodeFile="cargareactivos.aspx.vb"  EnableEventValidation="false"
    Inherits="administrador_AgruparReactivos" %>



<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style13 {
            height: 73px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style14 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
        }


        .style15 {
            height: 36px;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }
    </style>


    <div class="row">

        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="widget">
                <div class="widget-header bg-themeprimary">
                    <i class="widget-icon fa fa-arrow-up"></i>
                    <span class="widget-caption"><b>ÁREA PARA IMPORTAR REACTIVOS</b></span>
                    <div class="widget-buttons">
                        <a href="#" data-toggle="config">
                            <i class="fa fa-cog"></i>
                        </a>
                        <a href="#" data-toggle="maximize">
                            <i class="fa fa-expand"></i>
                        </a>
                        <a href="#" data-toggle="collapse">
                            <i class="fa fa-minus"></i>
                        </a>
                        <a href="#" data-toggle="dispose">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                    <!--Widget Buttons-->
                </div>
                <!--Widget Header-->
                <div class="widget-body">
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="form-horizontal" id="adminGradosForm">
                                <div class="panel panel-default">

                                    <div class="panel-body">

                                        <table class="style10">
                                            <tr>
                                                <td class="style13">Cargue el archivo que contiene los registros para importar a la base de datos:<br />
                                                    <br />
                                                    IMPORTANTE: No ejecutar esta función concurrentemente a la creación de Reactivos<br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span class="style14">Deberá tener los campos separados por pipes (|) <b>:</b>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Image ID="Image1" runat="server"
                                                        ImageUrl="~/Resources/imagenes/formato importe planteamientos.png" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style15">
                                                    <asp:AsyncFileUpload ID="ArchivoCarga" runat="server" Width="505px" OnUploadedComplete="ArchivoCarga_UploadedComplete" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <uc1:msgSuccess runat="server" ID="msgSuccess" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <uc1:msgInfo runat="server" ID="msgInfo" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <uc1:msgError runat="server" ID="msgError" />
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td style="font-size: medium;">
                                                    <asp:Button ID="Genera" runat="server" Text="Importar"
                                                        CssClass="defaultBtn btnThemeBlue btnThemeMedium"
                                                        Enabled="false" />
                                                    &nbsp;
                                            <asp:Button ID="Button1" runat="server"
                                                Text="Migra Opciones emergencia (TMP)"
                                                CssClass="defaultBtn btnThemeGrey btnThemeMedium"
                                                Enabled="false" />
                                                    &nbsp;
                                            <asp:Button ID="Button2" runat="server" Text="Actualiza EQUIPO (TMP)"
                                                CssClass="defaultBtn btnThemeGrey btnThemeMedium"
                                                Enabled="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-size: medium;">
                                                    <asp:Button ID="Multiples" runat="server"
                                                        Text="Importar Respuestas de Opción Múltiple"
                                                        CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                                    &nbsp;
                                                <asp:Button ID="Abiertas" runat="server"
                                                    Text="Importar Respuestas Abiertas"
                                                    CssClass="defaultBtn btnThemeBlue btnThemeMedium"
                                                    Enabled="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>

                                        </table>

                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <!--Widget Body-->
            </div>
            <!--Widget-->
        </div>
    </div>


    <asp:SqlDataSource ID="SDSplanteamientos" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Planteamiento]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSopciones" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Opcion]"></asp:SqlDataSource>


    <asp:SqlDataSource ID="SDSalumnos" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Alumno]"></asp:SqlDataSource>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" runat="Server">
</asp:Content>

