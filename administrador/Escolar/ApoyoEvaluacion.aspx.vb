﻿

Imports Siget

Imports System.IO
Imports System.Data.SqlClient
Imports AjaxControlToolkit

Partial Class administrador_ApoyoEvaluacion
    Inherits System.Web.UI.Page


    Public Class GlobalVar
        Public Shared archivoCargado As System.Web.HttpPostedFile
        Public Shared nombreArchivo As String
    End Class

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()


    End Sub

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Institucion"), 0))
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLplanteles_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplanteles.DataBound
        DDLplanteles.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Plantel"), 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub cargar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cargar.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            If (Beans.FileReference.Activator(6)) Then 'El atributo .HasFile compara si se indico un archivo
                If Trim(TBconsecutivo.Text) = "" Then
                    TBconsecutivo.Text = "0"
                End If

                GlobalVar.nombreArchivo = Beans.FileReference.nameOfFiles(6)

                SDSapoyoseval.InsertCommand = "SET dateformat dmy; INSERT INTO ApoyoEvaluacion (IdProgramacion,IdEvaluacion,Consecutivo,Descripcion,ArchivoApoyo, FechaModif, Modifico) VALUES (" + _
                    GVprogramacion.SelectedDataKey.Values(1).ToString + "," + GVevaluaciones.SelectedDataKey.Values(0).ToString + "," + _
                    TBconsecutivo.Text + ",'" + TBdescripcion.Text + "','" + GlobalVar.nombreArchivo + "', getdate(), '" & User.Identity.Name & "')"

                'Ahora CARGO el archivo
                Dim ruta As String
                ruta = Config.Global.rutaApoyo

                Dim MiArchivo2 As FileInfo = New FileInfo(ruta & Beans.FileReference.nameOfFiles(6))

                If MiArchivo2.Exists Then
                    'Avisa que ya existe, NO LO SUBE pero sí almacena el registro
                    'LblError.Text = "YA EXISTE UN ARCHIVO CON ESE NOMBRE EN EL ALMACEN DE ARCHIVOS, CAMBIE EL NOMBRE DE SU ARCHIVO E INTENTE CARGARLO NUEVAMENTE, POR FAVOR."

                    GlobalVar.archivoCargado = Beans.FileReference.Files(6)

                    'Checo si ese archivo no está asignado a otra evaluación; si lo está, advierto al usuario
                    Dim Total = 0
                    Dim strConexion As String
                    strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                    Dim objConexion As New SqlConnection(strConexion)
                    Dim miComando As SqlCommand
                    Dim misRegistros As SqlDataReader
                    miComando = objConexion.CreateCommand
                    miComando.CommandText = "select Count(*) as Total from ApoyoEvaluacion where ArchivoApoyo = '" + GlobalVar.nombreArchivo + "'"
                    objConexion.Open()
                    misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                    misRegistros.Read()
                    Total = misRegistros.Item("Total")
                    misRegistros.Close()
                    objConexion.Close()

                    If Not Total = 0 Then
                        ' notifico que está asignado a más subtemas y el archivo no se eliminó
                        msgWarning.show("Cuidado", "Este recurso está asignado a otras evaluaciones.")
                    End If

                    HLexiste.Text = Trim(HttpUtility.HtmlDecode(fuArchivo.FileName))
                    HLexiste.NavigateUrl = Config.Global.urlApoyo & Trim(HttpUtility.HtmlDecode(fuArchivo.FileName))
                    HF1_ModalPopupExtender.Show()
                Else
                    fuArchivo.SaveAs(ruta & fuArchivo.FileName)
                    HLarchivo.Visible = True
                    HLarchivo.Text = "Archivo cargado: " & Trim(HttpUtility.HtmlDecode(fuArchivo.FileName))
                    HLarchivo.NavigateUrl = Config.Global.urlApoyo & Trim(HttpUtility.HtmlDecode(fuArchivo.FileName))

                    SDSapoyoseval.Insert()
                    GVapoyos.DataBind()
                    msgSuccess.show("Éxito", "El archivo ha sido cargado")
                    msgError.hide()
                End If

            Else
                msgError.show("No ha seleccionado ningún archivo para cargar.")
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVprogramacion_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVprogramacion.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(0)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.ASIGNATURA

            LnkHeaderText = e.Row.Cells(3).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.PROFESOR

            LnkHeaderText = e.Row.Cells(4).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA
        End If
    End Sub

    Protected Sub GVprogramacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVprogramacion.SelectedIndexChanged
        Session("IdProgramacion") = GVprogramacion.SelectedDataKey.Values(1)
    End Sub

    Protected Sub BtnAsignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDialogAssign.Click
        msgError.hide()
        msgSuccess.hide()

        HLarchivo.Visible = True
        HLarchivo.Text = "Archivo asignado: " & Trim(HttpUtility.HtmlDecode(GlobalVar.nombreArchivo))
        HLarchivo.NavigateUrl = Config.Global.urlApoyo & Trim(HttpUtility.HtmlDecode(GlobalVar.nombreArchivo))
        SDSapoyoseval.InsertCommand = "SET dateformat dmy; INSERT INTO ApoyoEvaluacion (IdProgramacion,IdEvaluacion,Consecutivo,Descripcion,ArchivoApoyo, FechaModif, Modifico) VALUES (" + _
                            GVprogramacion.SelectedDataKey.Values(1).ToString + "," + GVevaluaciones.SelectedDataKey.Values(0).ToString + "," + _
                            TBconsecutivo.Text + ",'" + TBdescripcion.Text + "','" + GlobalVar.nombreArchivo + "', getdate(), '" & User.Identity.Name & "')"
        SDSapoyoseval.Insert()
        GVapoyos.DataBind()
        msgSuccess.show("Éxito", "El archivo ha sido asignado")
    End Sub

    Protected Sub btnDialogOverwrite_Click(sender As Object, e As EventArgs) Handles btnDialogOverwrite.Click
        Try
            Dim oldFile As FileInfo = New FileInfo(Config.Global.rutaApoyo & GlobalVar.nombreArchivo)

            oldFile.Delete()

            GlobalVar.archivoCargado.SaveAs(Config.Global.rutaApoyo & GlobalVar.nombreArchivo)

            HLarchivo.Visible = True
            HLarchivo.Text = "Archivo asignado: " & Trim(HttpUtility.HtmlDecode(GlobalVar.nombreArchivo))
            HLarchivo.NavigateUrl = Config.Global.urlApoyo & Trim(HttpUtility.HtmlDecode(GlobalVar.nombreArchivo))
            SDSapoyoseval.InsertCommand = "SET dateformat dmy; INSERT INTO ApoyoEvaluacion (IdProgramacion,IdEvaluacion,Consecutivo,Descripcion,ArchivoApoyo, FechaModif, Modifico) VALUES (" + _
                            GVprogramacion.SelectedDataKey.Values(1).ToString + "," + GVevaluaciones.SelectedDataKey.Values(0).ToString + "," + _
                            TBconsecutivo.Text + ",'" + TBdescripcion.Text + "','" + GlobalVar.nombreArchivo + "', getdate(), '" & User.Identity.Name & "')"
            SDSapoyoseval.Insert()
            GVapoyos.DataBind()
            msgSuccess.show("Éxito", "El archivo ha sido sobreescrito.")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEliminar.Click
        msgError.hide()
        msgSuccess.hide()

        'VERIFICO SI LA ACTIVIDAD CONTIENE ARCHIVO PARA SUBIR:
        Try
            SDSapoyoseval.DeleteCommand = "DELETE FROM ApoyoEvaluacion WHERE IdApoyoEvaluacion = " + GVapoyos.SelectedRow.Cells(1).Text
            SDSapoyoseval.Delete()

            'Checo si ese archivo no está asignado a otra evaluación; si lo está,  no borro el archivo, y advierto al usuario
            Dim Total = 0
            Dim strConexion As String
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objConexion As New SqlConnection(strConexion)
            Dim miComando As SqlCommand
            Dim misRegistros As SqlDataReader
            miComando = objConexion.CreateCommand
            miComando.CommandText = "select Count(*) as Total from ApoyoEvaluacion where ArchivoApoyo = '" + GlobalVar.nombreArchivo + "'"
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            Total = misRegistros.Item("Total")
            misRegistros.Close()
            objConexion.Close()

            If Total = 0 Then
                'Elimino el archivo de apoyo que tenga la opción

                Dim ruta As String
                ruta = Config.Global.rutaMaterial

                'Borro el archivo que estaba anteriormente porque se pondrá uno nuevo
                Dim MiArchivo As FileInfo = New FileInfo(ruta & Trim(HttpUtility.HtmlDecode(GlobalVar.nombreArchivo)))
                MiArchivo.Delete()
                HLarchivo.Visible = False
            Else
                ' notifico que está asignado a más subtemas y el archivo no se eliminó
                msgInfo.show("Atención", "El recurso que quitó está asignado a otras evaluaciones, por lo que no se eliminó físicamente del sistema.")
            End If

            msgSuccess.show("Éxito", "El archivo ha sido desasignado.")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try

    End Sub

    Protected Sub GVapoyos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVapoyos.SelectedIndexChanged
        HLarchivo.Text = GVapoyos.SelectedRow.Cells(6).Text
        HLarchivo.NavigateUrl = Config.Global.urlApoyo & GVapoyos.SelectedRow.Cells(6).Text
    End Sub


    Protected Sub fuArchivo_UploadedComplete(sender As Object, e As AsyncFileUploadEventArgs) Handles fuArchivo.UploadedComplete
        Beans.FileReference.Activator(6) = True
        Beans.FileReference.Files(6) = fuArchivo.PostedFile
        Beans.FileReference.nameOfFiles(6) = fuArchivo.FileName
    End Sub
End Class
