﻿<%@ Page Title="" Language="VB"
    MasterPageFile="~/Site.Master"
    AutoEventWireup="false"
    CodeFile="Programacion.aspx.vb" EnableEventValidation="false"
    ValidateRequest="false"
    Inherits="administrador_AgruparReactivos" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">


    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header">
                <i class="widget-icon fa fa-check"></i>
                <span class="widget-caption">Área para programar maestros específicos a grupos específicos</span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="config">
                        <i class="fa fa-cog"></i>
                    </a>
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <!--Widget Buttons-->
            </div>
            <!--Widget Header-->
            <div class="widget-body">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                                    
                        <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                </div>
                                <div class="panel-body">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Institución</label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSinstitucion" DataTextField="Descripcion"
                                                    DataValueField="IdInstitucion" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Ciclo Escolar</label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDScicloescolar" DataTextField="Descripcion"
                                                    DataValueField="IdCicloEscolar" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Nivel</label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSnivel" DataTextField="Descripcion" DataValueField="IdNivel"
                                                    CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Grado</label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSgrado" DataTextField="Descripcion" DataValueField="IdGrado"
                                                    CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group" data-toggle="tooltip" data-placement="bottom"
                                            title="Selecciona el plantel donde se encuentra el profesor que quieres asignar">
                                            <label class="col-lg-2 control-label">Plantel profesor</label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSplantel" DataTextField="Descripcion"
                                                    DataValueField="IdPlantel" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Profesor asignado</label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLprofesor" runat="server" CssClass="form-control"
                                                    AutoPostBack="True" DataSourceID="SDSprofesor" DataTextField="NombreProf"
                                                    DataValueField="IdProfesor">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group" data-toggle="tooltip" data-placement="bottom"
                                            title="Selecciona el plantel donde se encuentra el grupo que quieres que imparta el profesor">
                                            <label class="col-lg-2 control-label">Plantel grupo</label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLplantelgrupo" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSplantelgrupo" DataTextField="Descripcion"
                                                    DataValueField="IdPlantel" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Asignatura </label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLasignatura" runat="server" CssClass="form-control"
                                                    AutoPostBack="True" DataSourceID="SDSasignatura" DataTextField="Materia"
                                                    DataValueField="IdAsignatura">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Grupo</label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSgrupo" DataTextField="Descripcion" DataValueField="IdGrupo" 
                                                    CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:LinkButton ID="btnCrearProgramacion"  runat="server" Visible="false"
                                                CssClass="btnCrearCal btn  btn-lg btn-palegreen">
                                                                <i class="btn-label fa fa-plus"></i>Insertar Programación
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <div class="table-responsive">
                                            <asp:GridView ID="GVprogramacion" runat="server"
                                                AllowPaging="True" 
                                                AllowSorting="True"
                                                AutoGenerateColumns="False"
                                                DataKeyNames="IdProgramacion"
                                                DataSourceID="SDSprogramacion"
                                                Caption="<h3>Listado de GRUPOS asignados al PROFESOR para la ASIGNATURA y CICLO elegidos</h3>"
                                                PageSize="20"
                                                CssClass="table table-striped table-bordered"
                                                Height="17px"
                                                Style="font-size: x-small; text-align: left;"
                                                CellPadding="3">
                                                <Columns>
                                                    <asp:BoundField DataField="IdProgramacion" Visible="false" HeaderText="Id Programación"
                                                        InsertVisible="False" ReadOnly="True" SortExpression="IdProgramacion">
                                                        <HeaderStyle Width="30px" />
                                                        <ItemStyle Width="30px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Apellidos" HeaderText="Apellidos"
                                                        SortExpression="Apellidos" />
                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre"
                                                        SortExpression="Nombre" />
                                                    <asp:BoundField DataField="Asignatura" HeaderText="[Asignatura]"
                                                        SortExpression="Asignatura" />
                                                    <asp:BoundField DataField="Area" HeaderText="Area" SortExpression="Area" />
                                                    <asp:BoundField DataField="Grupo" HeaderText="[Grupo]" SortExpression="Grupo" />
                                                    <asp:BoundField DataField="Grado" HeaderText="[Grado]" SortExpression="Grado" />
                                                    <asp:BoundField Visible="false" DataField="Valoración" HeaderText="Valoración"
                                                        SortExpression="Valoración" />
                                                </Columns>
                                                <PagerTemplate>
                                                    <ul runat="server" id="Pag" class="pagination">
                                                    </ul>
                                                </PagerTemplate>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <SelectedRowStyle CssClass="row-selected" />
                                                <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:LinkButton ID="btnQuitarProgramacion" runat="server" Visible="false"
                                                CssClass="btnCrearCal btn  btn-lg btn-darkorange">
                                                 <i class="btn-label fa fa-remove"></i>Eliminar Programación
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                         <uc1:msgError runat="server" ID="msgError" />
                                         <uc1:msgInfo runat="server" ID="msgInfo" />
                                         <uc1:msgSuccess runat="server" ID="msgSuccess" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <!--Widget Body-->
        </div>
        <!--Widget-->
    </div>
    <div class="SqlDataSources">
        <asp:SqlDataSource ID="SDSinstitucion" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT [IdInstitucion], [Descripcion] FROM [Institucion] WHERE ([Estatus] = @Estatus) ORDER BY [Descripcion]">
            <SelectParameters>
                <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
           <asp:SqlDataSource ID="SDScicloescolar" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdCicloEscolar], [Descripcion] FROM [CicloEscolar] WHERE ([Estatus] = @Estatus) ORDER BY [FechaInicio]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
       <asp:SqlDataSource ID="SDSnivel" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] WHERE ([Estatus] = @Estatus) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
         <asp:SqlDataSource ID="SDSgrado" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
           <asp:SqlDataSource ID="SDSplantel" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
           <asp:SqlDataSource ID="SDSgrupo" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdGrupo, Descripcion + ' (' + Clave + ')' as Descripcion
from Grupo where IdPlantel = @IdPlantel and
IdGrado = @IdGrado and IdCicloEscolar = @IdCicloEscolar
order by Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantelgrupo" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSprofesor" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdProfesor, Apellidos + ', ' + Nombre + ' (' + Clave + ')' as NombreProf
from Profesor
where IdPlantel = @IdPlantel
order by Apellidos, Nombre">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
           <asp:SqlDataSource ID="SDSasignatura" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select A.IdAsignatura, A.Descripcion + ' (' + Ar.Descripcion + ')' as Materia
from Asignatura A, Area Ar
where A.IdGrado = @IdGrado and Ar.IdArea = A.IdArea
order by Ar.Descripcion, A.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            <asp:SqlDataSource ID="SDSplantelgrupo" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
          <asp:SqlDataSource ID="SDSprogramacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Pr.IdProgramacion,P.Apellidos,P.Nombre,A.Descripcion Asignatura,Ar.Descripcion Area, G.Descripcion Grupo, Gr.Descripcion Grado, Pr.ArchivoValoracion as 'Valoración'
from Programacion Pr, Profesor P, Asignatura A, Area Ar, Grupo G, Grado Gr
where Pr.IdCicloEscolar = @IdCicloEscolar and G.IdPlantel = @IdPlantel and Pr.IdProfesor = @IdProfesor and P.IdProfesor = Pr.IdProfesor and P.IdPlantel = G.IdPlantel and Pr.IdGrupo = G.IdGrupo and A.IdGrado = @IdGrado and Pr.IdAsignatura = A.IdAsignatura and A.IdAsignatura = @IdAsignatura and Ar.IdArea = A.IdArea and Gr.IdGrado = A.IdGrado
order by Gr.Descripcion,P.Apellidos,P.Nombre,Ar.Descripcion,A.Descripcion,G.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLprofesor" Name="IdProfesor"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
    </div>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" runat="Server">
</asp:Content>

