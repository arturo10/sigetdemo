﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" 
    AutoEventWireup="false" CodeFile="CiclosEscolares.aspx.vb" 
        EnableEventValidation="false"
    Inherits="administrador_AgruparReactivos" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:UpdatePanel runat="server">
        <ContentTemplate>

   
    <div class="form-horizontal" id="CiclosEscolaresForm">
        <div class="panel panel-default">
            <div class="panel-heading" style="text-align: left;">

                <div class="col-lg-8">
                    <i class="fa fa-calendar fa-lg"></i>
                <b>ADMINISTRACIÓN DE LOS CICLOS ESCOLARES</b>
                </div>
                <div>
                    <a data-toggle="modal"
                        class="btn btn-labeled btn-palegreen" data-target=".ciclos-modal">
                        <i class="btn-label glyphicon glyphicon-search"></i>Ciclos Disponibles  
                    </a>
                </div>

            
            </div>
            <div class="panel-body">

                <div class="col-lg-6">
                    <div class="col-lg-12">
                        <label class="col-lg-6 ">
                            <b>Inicio de Ciclo Escolar</b>
                        </label>    
                        <label class="col-lg-6 ">
                            <b>Fin de Ciclo  Escolar</b>
                        </label>
                    </div>
                    <div class="col-lg-12">
                                <div class="col-md-6">
                                <div id="datetimePickerInicioCiclo"></div>
                            </div>
                            
                            <div class="col-md-6">
                                <div id="datetimePickerFinCiclo"></div>
                            </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">
                            Indicador
                        </label>
                        <div class="col-lg-10">
                             <asp:DropDownList ID="DDLindicador" runat="server" AutoPostBack="True"
                                DataSourceID="SDSindicador" DataTextField="Descripcion"
                                 CssClass="form-control"
                                DataValueField="IdIndicador" Width="200px">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">
                            Estatus
                        </label>
                        <div class="col-lg-10">
                          <asp:DropDownList  ID="DDLestatus" runat="server" CssClass="form-control">
                                <asp:ListItem>Activo</asp:ListItem>
                                <asp:ListItem>Suspendido</asp:ListItem>
                                <asp:ListItem>Baja</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">
                            Descripción
                        </label>
                        <div class="col-lg-10">
                            <asp:TextBox runat="server" ID="TBDescripcion" CssClass="form-control" TextMode="MultiLine" Rows="5">

                            </asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <asp:LinkButton ID="btnCrearCicloEscolar"  runat="server" type="submit"
                        CssClass="btnCrearCicloEscolar btn  btn-lg btn-labeled btn-palegreen shiny">
                             <i class="btn-label fa fa-eraser"></i>Crear ciclo
                    </asp:LinkButton>

                    <asp:LinkButton ID="btnActualizarCiclo" runat="server" Visible="false" type="submit"
                        CssClass="btn btn-lg btn-labeled btn-palegreen">
                             <i class="btn-label fa fa-refresh"></i>Actualizar ciclo
                    </asp:LinkButton>
                      
                </div>
                <div class="col-lg-12">
                    <uc1:msgsuccess runat="server" id="msgSuccess" />
                    <uc1:msgerror runat="server" id="msgError" />
                </div>

          <asp:HiddenField ClientIDMode="Static" Value="01/01/2015" runat="server" ID="HFFechaInicio" />
          <asp:HiddenField ClientIDMode="Static"  Value="01/01/2015"  runat="server" ID="HFFechaFin" />
          <asp:HiddenField ClientIDMode="Static" Value="01/01/2015" runat="server" ID="HFFechaInicioParse" />
          <asp:HiddenField ClientIDMode="Static"  Value="01/01/2015"  runat="server" ID="HFFechaFinParse" />
       
            </div>


           

        </div>
    </div>
                 </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade ciclos-modal" tabindex="-1" role="dialog"
        aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                         <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myLargeModalLabel"><b>Ciclos escolares disponibles </b></h4>
                        </div>
                        <div class="form-horizontal">
                            <div class="panel panel-default">
                            
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <asp:GridView ID="GVciclosescolares" runat="server"
                                            AllowPaging="True"
                                            AllowSorting="True"
                                            AutoGenerateColumns="False"
                                            DataKeyNames="IdCicloEscolar,IdIndicador, Descripcion,FechaInicio,FechaFin,Estatus"
                                            DataSourceID="SDScicloescolar"
                                            BackColor="White"
                                            BorderColor="#999999"
                                            BorderStyle="Solid"
                                            BorderWidth="1px"
                                            PageSize="6"
                                            CellPadding="3"
                                            CssClass="table table-striped table-bordered"
                                            Height="17px"
                                            Style="font-size: x-small; text-align: left;">
                                            <Columns>
                                                <asp:BoundField DataField="IdCicloEscolar" HeaderText="Id Ciclo Escolar"
                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdCicloEscolar" />
                                                <asp:BoundField DataField="IdIndicador" HeaderText="Id_Indicador"
                                                    SortExpression="IdIndicador" />
                                                <asp:BoundField DataField="Descripcion"
                                                    HeaderText="Descripción" SortExpression="Descripcion" />
                                                <asp:BoundField DataField="FechaInicio" DataFormatString="{0:dd/MM/yy}"
                                                    HeaderText="Inicia el" SortExpression="FechaInicio" />
                                                <asp:BoundField DataField="FechaFin" HeaderText="Termina el"
                                                    SortExpression="FechaFin" DataFormatString="{0:dd/MM/yy}" />
                                                <asp:BoundField DataField="FechaModif" HeaderText="FechaModif"
                                                    SortExpression="FechaModif" Visible="False" />
                                                <asp:BoundField DataField="Modifico" HeaderText="Modifico"
                                                    SortExpression="Modifico" Visible="False" />
                                                <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                    SortExpression="Estatus" />
                                            </Columns>
                                            <PagerTemplate>
                                                <ul runat="server" id="Pag" class="pagination">
                                                </ul>
                                            </PagerTemplate>
                                            <PagerStyle HorizontalAlign="Center" />
                                            <SelectedRowStyle CssClass="row-selected" />
                                            <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                        </asp:GridView>
                                    </div>
                                    <div class="form-group">
                                        <asp:LinkButton ID="btnEliminarCiclo" Visible="false" runat="server"
                                            CssClass="btn btn-lg btn-labeled btn-darkorange">
                             <i class="btn-label fa fa-remove"></i>Eliminar ciclo
                                        </asp:LinkButton>
                                        <asp:LinkButton data-dismiss="modal" ID="btnCancelarCambiosCalificaciones"
                                            runat="server"
                                            CssClass="btn btn-lg btn-labeled btn-warning">
                                                                                            <i class="btn-label fa fa-remove" ></i>Cerrar
                                        </asp:LinkButton>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
                <%-- sort YES - rowdatabound
	                0  id_CICLO
	                1  id_indicador
	                2  descripcion
	                3  fechainicio
	                4  termina el
	                5  fechamodif
	                6  modifico
	                7  estatus
                --%>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <asp:SqlDataSource ID="SDScicloescolar" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT [IdCicloEscolar], [IdIndicador], [Descripcion], [FechaInicio], [FechaFin], [FechaModif], [Modifico], [Estatus] FROM [CicloEscolar]"></asp:SqlDataSource>

           <asp:SqlDataSource ID="SDSindicador" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Indicador] ORDER BY [Descripcion]"></asp:SqlDataSource>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">
    <script type="text/javascript">

        Sys.Application.add_load(LoadHandler);

        function LoadHandler(sender, args0) {
     

            $(function () {
          
                var dateStart= $('#datetimePickerInicioCiclo').datetimepicker({
                    inline: true,
                    sideBySide: false,
                    defaultDate: moment(new Date($('#HFFechaInicio').val()))
                }).
                    on('dp.change', function () {
                        $('#HFFechaInicio').val(moment(new Date(dateStart.data('date'))));
                        $('#HFFechaInicioParse').val(moment(new Date(dateStart.data('date'))).format('DD/MM/YYYY'));
                    });
              
                var dateEnd=$('#datetimePickerFinCiclo').datetimepicker({
                    inline: true,
                    sideBySide: false,
                    defaultDate: moment(new Date($('#HFFechaFin').val()))
                }).
                  on('dp.change', function () {
                      $('#HFFechaFin').val(moment(new Date(dateEnd.data('date'))));
                      $('#HFFechaFinParse').val(moment(new Date(dateEnd.data('date'))).format('DD/MM/YYYY'));

                  });


            });
        }


      
    </script>
</asp:Content>

