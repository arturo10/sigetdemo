﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" 
    AutoEventWireup="false" CodeFile="ApoyoEvaluacion.aspx.vb"
     Inherits="administrador_ApoyoEvaluacion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgWarning.ascx" TagPrefix="uc1" TagName="msgWarning" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content Runat="server" ContentPlaceHolderID="Title">

    Material para actividades y grupos específicos (como listas de asistencia, notas para el Alumno , etc.)
 </asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">




       <div class="row">

        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="widget">
                <div class="widget-header bg-themeprimary">
                    <i class="widget-icon fa fa-arrow-up"></i>
                    <span class="widget-caption"><b>Aquí puedes cargar achivos de apoyo a evaluaciones específicas</b></span>
                    <div class="widget-buttons">
                        <a href="#" data-toggle="config">
                            <i class="fa fa-cog"></i>
                        </a>
                        <a href="#" data-toggle="maximize">
                            <i class="fa fa-expand"></i>
                        </a>
                        <a href="#" data-toggle="collapse">
                            <i class="fa fa-minus"></i>
                        </a>
                        <a href="#" data-toggle="dispose">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                    <!--Widget Buttons-->
                </div>
                <!--Widget Header-->
                <div class="widget-body">
                    <asp:UpdatePanel runat="server" >
                
                        <ContentTemplate>
                            <div class="form-horizontal" id="adminGradosForm">
                                <div class="panel panel-default">
                                   
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <asp:Label ID="Label2" runat="server" CssClass="col-lg-2">
                                                    <%= Lang_Config.Translate("general", "Institucion") %>
                                            </asp:Label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                                    DataValueField="IdInstitucion" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label3" runat="server" CssClass="col-lg-2">
                                                Ciclo
                                            </asp:Label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSciclosescolares"
                                                    DataTextField="Descripcion" DataValueField="IdCicloEscolar"
                                                    CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label4" runat="server" CssClass="col-lg-2" >
                                                  <%= Lang_Config.Translate("general", "Plantel") %>
                                            </asp:Label>
                               
                                          
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLplanteles" runat="server" AutoPostBack="True"
                                                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                                    DataValueField="IdPlantel" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label5" runat="server" CssClass="col-lg-2">
                                                 Grupo
                                            </asp:Label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                                                    CssClass="form-control" DataSourceID="SDSgrupos"
                                                    DataTextField="Descripcion"
                                                    DataValueField="IdGrupo">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-9 col-lg-offset-2">
                                                <div class="table-responsive">
                                                    <asp:GridView ID="GVprogramacion" runat="server"
                                                        AllowPaging="True"
                                                        AutoGenerateColumns="False"
                                                        PageSize="15"
                                                        Caption="<h3>PROFESORES y ASIGNATURAS asignadas al GRUPO seleccionado</h3>"
                                                        AllowSorting="True"
                                                        DataKeyNames="IdAsignatura,IdProgramacion"
                                                        DataSourceID="SDSprogramaciones"
                                                        CellPadding="3"
                                                        GridLines="Vertical"
                                                        CssClass="table table-striped table-bordered"
                                                        Height="13px"
                                                        Style="font-size: x-small; text-align: left;">
                                               
                                                        <Columns>
                                                            <asp:CommandField ShowSelectButton="True">
                                                                <HeaderStyle Width="70px" />
                                                            </asp:CommandField>
                                                            <asp:BoundField DataField="IdAsignatura" HeaderText="Id_[Asignatura]"
                                                                InsertVisible="False" ReadOnly="True" SortExpression="IdAsignatura">
                                                                <HeaderStyle Width="40px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="IdProgramacion" HeaderText="Id Programacion"
                                                                InsertVisible="False" ReadOnly="True" SortExpression="IdProgramacion">
                                                                <HeaderStyle Width="50px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="NomProfesor" HeaderText="[Profesor]" ReadOnly="True"
                                                                SortExpression="NomProfesor" />
                                                            <asp:BoundField DataField="Asignatura" HeaderText="[Asignatura]"
                                                                SortExpression="Asignatura" />
                                                        </Columns>
                                                        <PagerTemplate>

                                                            <ul runat="server" id="Pag" class="pagination">
                                                            </ul>
                                                        </PagerTemplate>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <SelectedRowStyle CssClass="row-selected" />
                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                    </asp:GridView>

                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-9 col-lg-offset-2">
                                                <div class="table-responsive">
                                                    <asp:GridView ID="GVevaluaciones" runat="server" AllowPaging="True"
                                                        AutoGenerateColumns="False" DataKeyNames="IdEvaluacion"
                                                        DataSourceID="SDSevaluaciones"
                                                        CellPadding="3"
                                                        GridLines="Vertical"
                                                        CssClass="table table-striped table-bordered"
                                                        Height="13px"
                                                        Style="font-size: x-small; text-align: left;"
                                                        AllowSorting="True">
                                                        <FooterStyle BackColor="#CCCCCC" />
                                                        <Columns>
                                                            <asp:CommandField ShowSelectButton="True" />
                                                            <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Evaluacion"
                                                                InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion">
                                                                <HeaderStyle Width="70px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Actividad" HeaderText="Actividad"
                                                                SortExpression="Actividad" />
                                                            <asp:BoundField DataField="Clave para Reportes"
                                                                HeaderText="Clave para Reportes" SortExpression="Clave para Reportes" />
                                                            <asp:BoundField DataField="Calificación" HeaderText="Calificación"
                                                                SortExpression="Calificación" />
                                                        </Columns>
                                                        <PagerTemplate>

                                                            <ul runat="server" id="Pag" class="pagination">
                                                            </ul>
                                                        </PagerTemplate>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <SelectedRowStyle CssClass="row-selected" />
                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                    </asp:GridView>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-7 col-lg-offset-3">
                                          <div class="panel panel-default">
                                              <div class="panel-body">
                                                  <div class="col-lg-offset-2 col-lg-offset-8">

                                                 
                                                  <div class="form-group">
                                                     
                                                      <label class="col-lg-2 control-label">Consecutivo</label>
                                                      <div class="col-lg-4">
                                                            <asp:TextBox TextMode="Number" ID="TBconsecutivo" Max="0" runat="server" MaxLength="3" CssClass="form-control" ></asp:TextBox>
                                                      </div>
                                                     
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="col-lg-2 control-label">Descripción del material</label>
                                                      <div class="col-lg-4">
                                                             <asp:TextBox ID="TBdescripcion" runat="server" MaxLength="150" CssClass="form-control"></asp:TextBox>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">

                                                      <asp:HyperLink ID="HLarchivo" runat="server"
                                                          Style="font-family: Arial, Helvetica, sans-serif; font-size: small; color: #000066"
                                                          Target="_blank" Visible="False">[HLarchivo]</asp:HyperLink>

                                                      Cargar material de apoyo
                                                            *al insertar o actualizar se carga el&nbsp; archivo
         
                                                 <asp:AsyncFileUpload ID="fuArchivo"  runat="server" Style="text-align: left" CssClass="imageUploaderField" />

                                                  </div>
                                                  <div class="form-group">
                                                      <asp:Button ID="cargar" runat="server" Text="Cargar"
                                                          CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                                      &nbsp;
                                                    <asp:Button ID="BtnEliminar" runat="server" Text="Eliminar"
                                                        CssClass="defaultBtn btnThemeGrey btnThemeMedium" />


                                                      <uc1:msgSuccess runat="server" ID="msgSuccess" />

                                                  </div>
                                                  </div>
                                              </div>
                                          </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="table-responsive">
                                                
                <asp:GridView ID="GVapoyos" runat="server" AllowPaging="True"
                    AllowSorting="True" AutoGenerateColumns="False" BackColor="White"
                    BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3"
                    DataSourceID="SDSapoyoseval" GridLines="Vertical"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small; text-align: left;"
                    Width="882px" DataKeyNames="IdApoyoEvaluacion"
                    Caption="<h3>Apoyos para la Actividad</h3>"
                    ForeColor="Black" PageSize="20">
                    <FooterStyle BackColor="#CCCCCC" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True">
                            <HeaderStyle Width="70px" />
                        </asp:CommandField>
                        <asp:BoundField DataField="IdApoyoEvaluacion" HeaderText="Id Apoyo Evaluacion"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdApoyoEvaluacion">
                            <HeaderStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdProgramacion" HeaderText="Id Programación"
                            SortExpression="IdProgramacion">
                            <HeaderStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Evaluacion"
                            SortExpression="IdEvaluacion">
                            <HeaderStyle Width="50px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                            SortExpression="Consecutivo" />
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion"
                            SortExpression="Descripcion" />
                        <asp:BoundField DataField="ArchivoApoyo" HeaderText="ArchivoApoyo"
                            SortExpression="ArchivoApoyo" />
                    </Columns>
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#005290" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#424242" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>


                                                <asp:Button ID="Quitar" runat="server" Text="Quitar"
                                                    Visible="False" CssClass="defaultBtn btnThemeGrey btnThemeMedium" />

                                                <uc1:msgInfo runat="server" ID="msgInfo" />
                                                <uc1:msgError runat="server" ID="msgError" />

                                            </div>
                                        </div>







                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <!--Widget Body-->
            </div>
            <!--Widget-->
        </div>
    </div>









    
             
     
   
      
     
          
           
       


    <asp:Panel ID="PnlConfirma" runat="server" CssClass="dialogOverwrite dialog1">
        <table>
            <tr>
                <td colspan="3">
                    <div style="width: 100%; text-align: center; padding-bottom: 10px;">
                        <asp:Image ID="Image1" runat="server"
                            ImageUrl="~/Resources/imagenes/Exclamation.png"
                            Width="50" Height="50" />
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding-bottom: 20px;">En el almacén de archivos ya existe un archivo con el mismo nombre:
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding-bottom: 20px;">
                    <asp:HyperLink ID="HLexiste" runat="server"
                        CssClass="hyperlink"
                        Target="_blank">Link</asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="padding-bottom: 20px;">Puede asignar este mismo archivo al subtema actual, 
                    sobreescribir el archivo antiguo con el archivo recién cargado,
                    o cancelar la operación.
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <uc1:msgWarning runat="server" ID="msgWarning" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="btnDialogAssign" runat="server" Text="Asignar"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                    &nbsp;
                    <asp:Button ID="btnDialogOverwrite" runat="server" Text="Sobreescribir"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                    &nbsp;
                    <asp:Button ID="btnDialogCancel" runat="server" Text="Cancelar"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </asp:Panel>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSInstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Institucion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar] WHERE Estatus = 'Activo' ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdGrupo, Descripcion
FROM Grupo
WHERE IdPlantel = @IdPlantel and IdCicloEscolar = @IdCicloEscolar
order by Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplanteles" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSprogramaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Pr.IdProgramacion, rtrim(P.Apellidos) + ' ' + rtrim(P.Nombre) as NomProfesor, A.IdAsignatura, A.Descripcion as Asignatura
from Profesor P, Asignatura A, Programacion Pr
where Pr.IdCicloEscolar = @IdCicloEscolar and Pr.IdGrupo = @IdGrupo
and A.IdAsignatura = Pr.IdAsignatura and P.IdProfesor = Pr.IdProfesor
order by P.Apellidos, P.Nombre, A.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSapoyoseval" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select *
from ApoyoEvaluacion
where IdProgramacion = @IdProgramacion and IdEvaluacion = @IdEvaluacion
order by Consecutivo">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdProgramacion" SessionField="IdProgramacion" />
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select E.IdEvaluacion, E.ClaveBateria as Actividad, E.ClaveAbreviada as 'Clave para Reportes',C.Descripcion as 'Calificación'
from Evaluacion E, Calificacion C
where C.IdCicloEscolar = @IdCicloEscolar and C.IdAsignatura = @IdAsignatura
and E.idCalificacion = C.IdCalificacion
order by C.Consecutivo, E.InicioContestar">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="GVprogramacion" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="HF1" runat="server" />
                <asp:ModalPopupExtender ID="HF1_ModalPopupExtender" runat="server"
                    Enabled="True" PopupControlID="PnlConfirma" TargetControlID="HF1" BackgroundCssClass="overlay">
                </asp:ModalPopupExtender>
            </td>
        </tr>
    </table>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">
</asp:Content>

