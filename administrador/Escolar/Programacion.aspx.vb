﻿Imports Siget
Partial Class administrador_AgruparReactivos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()
    End Sub
    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLprofesor_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLprofesor.DataBound
        DDLprofesor.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.PROFESOR, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija la " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub GVprogramacion_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVprogramacion.RowCreated

        Dim cociente As Integer = (GVprogramacion.PageCount / 15)
        Dim moduloIndex As Integer = (GVprogramacion.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVprogramacion.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVprogramacion.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVprogramacion.PageIndex
                final = IIf((inicial + 15 <= GVprogramacion.PageCount), inicial + 15, GVprogramacion.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVprogramacion.PageCount, inicial + 15, GVprogramacion.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVprogramacion.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVprogramacion_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVprogramacion.RowDataBound


        Dim cociente As Integer = (GVprogramacion.PageCount) / 15
        Dim moduloIndex As Integer = (GVprogramacion.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVprogramacion.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVprogramacion.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVprogramacion.PageIndex
            final = IIf((inicial + 15 <= GVprogramacion.PageCount), inicial + 15, GVprogramacion.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVprogramacion.PageCount), inicial + 15, GVprogramacion.PageCount)
        End If

        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(3).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA

            LnkHeaderText = e.Row.Cells(5).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.GRUPO

            LnkHeaderText = e.Row.Cells(6).Controls(0)
            LnkHeaderText.Text = Config.Etiqueta.GRADO
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVprogramacion, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVprogramacion, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVprogramacion_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVprogramacion.SelectedIndexChanged
        btnQuitarProgramacion.Visible = True
    End Sub

    Protected Sub GVprogramacion_DataBound(sender As Object, e As EventArgs) Handles GVprogramacion.DataBound
        If GVprogramacion.Rows.Count = 0 Then
            btnQuitarProgramacion.Visible = False
            GVprogramacion.SelectedIndex = -1
        End If
    End Sub

    Protected Sub DDLgrupo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLgrupo.SelectedIndexChanged
        btnCrearProgramacion.Visible = True
    End Sub

    Protected Sub DDLgrupo_DataBound(sender As Object, e As EventArgs) Handles DDLgrupo.DataBound
        If DDLgrupo.Items.Count = 0 Then
            btnCrearProgramacion.Visible = False
        End If
        DDLgrupo.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub btnCrearProgramacion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrearProgramacion.Click
        msgError.hide()

        Try
            SDSprogramacion.InsertCommand = "SET dateformat dmy; INSERT INTO Programacion (IdAsignatura,IdProfesor,IdCicloEscolar,IdGrupo, FechaModif, Modifico) VALUES (" +
                DDLasignatura.SelectedValue + "," + DDLprofesor.SelectedValue + "," +
                DDLcicloescolar.SelectedValue + "," + DDLgrupo.SelectedValue + ", getdate(), '" &
                User.Identity.Name & "')"
            SDSprogramacion.Insert()
            GVprogramacion.DataBind()
            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")), "La programación ha sido agregada con éxito")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub btnQuitarProgramacion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnQuitarProgramacion.Click
        msgError.hide()

        Try
            SDSprogramacion.DeleteCommand = "Delete from Programacion WHERE IdProgramacion = " + GVprogramacion.SelectedDataKey("IdProgramacion").ToString
            SDSprogramacion.Delete()
            GVprogramacion.DataBind()
            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")), "La programación ha sido elminado con éxito")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub
End Class
