﻿<%@ Page Title="Calificaciones" Language="VB"
    MasterPageFile="~/Site.Master" AutoEventWireup="false" ValidateRequest="false"
    CodeFile="calificaciones.aspx.vb" Inherits="administrador_AgruparReactivos" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">


    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12 resize-mobile">
            <div class="widget flat radius-bordered">
                <div class="widget-header bg-themeprimary">
                    <span class="widget-caption">ÁREA PARA CALIFICACIONES</span>
                </div>

                <div class="widget-body">
                    <div class="widget-main ">
                        <div class="tabbable">
                            <ul class="nav nav-tabs tabs-flat" id="myTab11">
                                <li id="Cal" class="active">
                                    <a data-toggle="tab" href="#calificaciones">Calificaciones
                                    </a>
                                </li>
                                <li>
                                    <a id="Eval" data-toggle="tab" href="#evaluaciones">Actividades
                                    </a>
                                </li>
                                <li>
                                    <a id="Equipos" data-toggle="tab" href="#agruparReactivos">Agrupar reactivos
                                    </a>
                                </li>
                                <li>
                                    <a id="FechasP" data-toggle="tab" href="#fechasPlanteles">      
                                         <%= Lang_Config.Translate("calificaciones", "Fechas_planteles")%>
                                    </a>
                                </li>
                                <li>
                                    <a id="FechasA" data-toggle="tab" href="#fechasAlumnos">
                                        <%= Lang_Config.Translate("calificaciones", "Fechas_alumnos")%>
                                    </a>
                                </li>
                                <li>
                                    <a id="SeriarA" data-toggle="tab" href="#seriarActividades">Seriar Actividades
                                    </a>
                                </li>
                                <li>
                                    <a id="ActividadDemo" data-toggle="tab" href="#actividadPractica">Actividad Demo
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content tabs-flat">

                                <div id="calificaciones" class="tab-pane in active">
                                    <asp:UpdatePanel ID="PanelCalificaciones" runat="server">
                                        <ContentTemplate>
                                            <div class="form-horizontal" id="calificacionesgralForm">
                                                  <div class="col-lg-12" style="position:absolute;top:50%;left:50%;bottom:50%;">
                                                <div class="load-spinner"></div>
                                            </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" style="text-align: left;">
                                                    </div>

                                                    <div class="panel-body">

                                                        <div class="col-lg-6">
                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                    Ciclo
                                                                </label>

                                                                <div class="col-lg-10">
                                                                    <asp:DropDownList ID="DDLcicloescolarCalificaciones"
                                                                        runat="server" AutoPostBack="True" Style="width: 100%;"
                                                                        DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                                                                        DataValueField="IdCicloEscolar" AppendDataBoundItems="false"
                                                                        CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>

                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                   <%= Lang_Config.Translate("general", "Nivel")%>
                                                                </label>

                                                                <div class="col-lg-10">
                                                                    <asp:DropDownList ID="DDLNivelCalificaciones" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSniveles" DataTextField="Descripcion"
                                                                        DataValueField="IdNivel"  AppendDataBoundItems="false"
                                                                        CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>

                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                      <%= Lang_Config.Translate("general", "Grado")%>
                                                                </label>

                                                                <div class="col-lg-10">
                                                                    <asp:DropDownList ID="DDLGradoCalificaciones" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSgrados" DataTextField="Descripcion"
                                                                        DataValueField="IdGrado" AppendDataBoundItems="false"
                                                                        CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>

                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                    <%= Lang_Config.Translate("general", "Asignatura")%>
                                                                </label>

                                                                <div class="col-lg-10">
                                                                    <asp:DropDownList ID="DDLAsignaturaCalificaciones" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSasignaturas" DataTextField="Descripcion" style="width:100%;"
                                                                        DataValueField="IdAsignatura" AppendDataBoundItems="false"
                                                                        CssClass="DDLAC selectable">
                                                                    </asp:DropDownList>
                                                                </div>

                                                            </div>

                                                            <div class="form-group">
                                                                <asp:LinkButton ID="btnCrearCalificaciones" data-toggle="modal" data-backdrop="static"
                                                                     data-target=".calificaciones-modal" runat="server"
                                                                    CssClass="btnCrearCal btn  btn-lg btn-labeled btn-blue">
                                                                <i class="btn-label fa fa-plus"></i>Redactar calificación
                                                                </asp:LinkButton>


                                                             
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="col-lg-12" id="spin">
                                                            </div>
                                                        </div>

                                                        
                                                        <div class="col-lg-6">
                                                             <div class="form-group">
                                                            <div class="table-responsive">
                                                                <asp:GridView ID="gvCalificaciones" runat="server"
                                                                    ShowFooter="true"
                                                                    AllowPaging="True"
                                                                    PageSize="6"
                                                                    AllowSorting="true"
                                                                    AutoGenerateColumns="False"
                                                                    DataKeyNames="IdCalificacion,IdIndicador,Descripcion,Clave,Consecutivo,Valor,DescripcionIndicador"
                                                                    DataSourceID="SDScalificaciones"
                                                                    CellPadding="3"
                                                                    CssClass="table table-striped table-bordered"
                                                                    Height="17px"
                                                                    Style="font-size: x-small; text-align: left;">
                                                                    <Columns>
                                                                       

                                                                        <asp:BoundField DataField="IdCalificacion" HeaderText="Id"
                                                                            Visible="False" ReadOnly="True" SortExpression="IdCalificacion" />
                                                                        <asp:BoundField
                                                                            DataField="Descripcion"
                                                                            HeaderText="Descripcion"
                                                                            SortExpression="Descripcion" />
                                                                        <asp:BoundField
                                                                            DataField="Clave"
                                                                            HeaderText="Clave"
                                                                            SortExpression="Clave" />
                                                                        <asp:BoundField
                                                                            DataField="Consecutivo"
                                                                            HeaderText="Consecutivo"
                                                                            SortExpression="Consecutivo">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Valor" HeaderText="% de la Asignatura" SortExpression="Valor">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DescripcionIndicador" HeaderText="Indicador"
                                                                            SortExpression="DescripcionIndicador" />
                                                                        <asp:BoundField DataField="FechaModif" HeaderText="Fecha de Modificación"
                                                                            Visible="False" SortExpression="FechaModif" />
                                                                        <asp:BoundField DataField="Modifico" HeaderText="Modificó"
                                                                            Visible="False" SortExpression="Modifico" />
                                                                    </Columns>
                                                                    <PagerTemplate>
                                                                        <ul runat="server" id="Pag" class="pagination">
                                                                        </ul>
                                                                    </PagerTemplate>
                                                                    <PagerStyle HorizontalAlign="Center" />
                                                                    <SelectedRowStyle CssClass="row-selected" />
                                                                    <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </div>
                                                             </div>
                                                            <div class="form-group">
                                                                <asp:LinkButton data-toggle="modal" ID="btnModificarCalificaciones" 
                                                                    data-target=".calificaciones-modal" runat="server"
                                                                    CssClass="btn btn-lg btn-labeled btn-palegreen" Visible="false">
                                                                <i class="btn-label fa fa-refresh" ></i>Modificar
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnEliminarCalificaciones" Visible="false" runat="server" 
                                                                    type="submit"
                                                                    CssClass="btn btn-lg btn-labeled btn-darkorange">
                                                                <i class="btn-label fa fa-remove"></i>Eliminar
                                                                </asp:LinkButton>
                                                            </div>

                                                        </div>
                                                        <div class="col-lg-12">
                                                            <uc1:msgError runat="server" ID="msgError" />
                                                            <uc1:msgInfo runat="server" ID="msgInfoCalificaciones" />
                                                            <uc1:msgSuccess runat="server" ID="msgSuccess" />
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End of Update Panel Part of scores-->

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <!-- End of first tab -->
                                <div id="evaluaciones" class="tab-pane">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <div class="form-horizontal">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-12" id="spinEval">
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" >
                                                        ACTIVIDADES PARA LA CALIFICACIÓN:
                                                            <b> <asp:Label runat="server" ID="calificacionSelected" 
                                                                 CssClass="lsize-st" >
                                                             </asp:Label>
                                                        </b>
                                                    </div>
                                                    <div class="panel-body">

                                                       
                                                        <div class="form-group">
                                                            <div class="table-responsive">
                                                                <asp:GridView ID="GVevaluaciones" runat="server"
                                                                    AutoGenerateColumns="False"
                                                                    AllowPaging="true"
                                                                    
                                                                    AllowSorting="True"
                                                                    DataKeyNames="IdEvaluacion,ClaveBateria,
                                                                 ClaveAbreviada,Porcentaje,InicioContestar,FinSinPenalizacion,
                                                                  Aleatoria, Instruccion, RepiteIndicador,FinContestar,Tipo,Estatus
                                                                 ,SeEntregaDocto,Califica,RepiteRango, RepiteMaximo, EsForo,RepiteAutomatica,
                                                                 PermiteSegunda,OpcionesAleatorias,NoActividad,Penalizacion,CerrarCaptura"
                                                                    DataSourceID="SDSevaluaciones"
                                                                    CellPadding="3"
                                                                    ShowFooter="True"
                                                                    CssClass="table table-striped table-bordered"
                                                                    Height="17px"
                                                                    Style="font-size: x-small; text-align: left;">
                                                                    <Columns>

                                                                         <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox runat="server"  />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                      
                                                                        <asp:BoundField DataField="IdEvaluacion" HeaderText="Id"
                                                                            Visible="False" ReadOnly="True" SortExpression="IdEvaluacion" />
                                                                      
                                                                        <asp:BoundField DataField="ClaveBateria" HeaderText="Clave de la Actividad"
                                                                            SortExpression="ClaveBateria" />
                                                                      
                                                                        <asp:BoundField DataField="ClaveAbreviada" HeaderText="Clave p/Reporte"
                                                                            SortExpression="ClaveAbreviada" />
                                                                   
                                                                        <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                                                                            SortExpression="Porcentaje"><%-- 4 --%>
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                                            HeaderText="Inicia" SortExpression="InicioContestar" />
                                                                        <%-- 5 --%>
                                                                        <asp:BoundField DataField="FinSinPenalizacion"
                                                                            DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Máxima"
                                                                            SortExpression="FinSinPenalizacion" />
                                                                        <%-- 6 --%>
                                                                        <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                                            HeaderText="Termina" SortExpression="FinContestar" />
                                                                        <%-- 7 --%>
                                                                        <asp:BoundField DataField="Penalizacion" HeaderText="% Penalización"
                                                                            SortExpression="Penalizacion"><%-- 8 --%>
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                                                        <%-- 9 --%>
                                                                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                                            SortExpression="Estatus" />
                                                                        <%-- 10 --%>
                                                                        <asp:BoundField DataField="Aleatoria" HeaderText="Aleatoria"
                                                                            SortExpression="Aleatoria" />
                                                                        <%-- 11 --%>
                                                                        <asp:CheckBoxField DataField="SeEntregaDocto" HeaderText="Documento"
                                                                            SortExpression="SeEntregaDocto" />
                                                                        <%-- 12 --%>
                                                                        <asp:BoundField DataField="Califica" HeaderText="Califica"
                                                                            SortExpression="Califica" Visible="False" />
                                                                        <%-- 13 --%>
                                                                        <asp:CheckBoxField DataField="RepiteAutomatica" HeaderText="Repetición Automática"
                                                                            SortExpression="RepiteAutomatica" />
                                                                        <%-- 14 --%>
                                                                        <asp:CheckBoxField DataField="PermiteSegunda" HeaderText="Segunda Vuelta"
                                                                            SortExpression="PermiteSegunda" />
                                                                        <%-- 15 --%>
                                                                        <asp:BoundField DataField="FechaModif" HeaderText="Fecha de Modificación"
                                                                            Visible="False" SortExpression="FechaModif" />
                                                                        <%-- 16 --%>
                                                                        <asp:BoundField DataField="Modifico" HeaderText="Modificó"
                                                                            Visible="False" SortExpression="Modifico" />
                                                                        <%-- 17 --%>
                                                                    </Columns>
                                                                    <PagerTemplate>
                                                                        <ul runat="server" id="Pag" class="pagination">
                                                                        </ul>
                                                                    </PagerTemplate>
                                                                    <PagerStyle HorizontalAlign="Center" />
                                                                    <SelectedRowStyle CssClass="row-selected" />
                                                                    <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:LinkButton ID="btnCrearEvaluacion" Visible="false" runat="server" data-toggle="modal"
                                                                data-target=".evaluaciones-modal"
                                                                CssClass="btnCrearCal btn  btn-lg btn-labeled btn-palegreen shiny">
                                                                                                <i class="btn-label fa fa-plus"></i>Redactar Actividad
                                                            </asp:LinkButton>
                                                            <a data-toggle="modal" visible="false"
                                                                data-target=".evaluaciones-modal"
                                                                id="btnModificarEvaluacion"
                                                                runat="server"
                                                                class="btn btn-lg btn-labeled btn-palegreen ">
                                                                <i class="btn-label fa fa-remove"></i>Modificar
                                                            </a>
                                                            <asp:LinkButton ID="btnEliminarEvaluacion" Visible="false" data-dismiss="modal" runat="server"
                                                                CssClass="btn btn-lg btn-labeled btn-darkorange">
                                                                                                <i class="btn-label fa fa-plus"></i>Eliminar
                                                            </asp:LinkButton>
                                                        </div>
                                                          <div class="form-group pull-right">
                                                                <label class="col-lg-2 control-label">
                                                                    Estatus Actividad
                                                                </label>
                                                                <div class="col-lg-4">
                                                                     <asp:DropDownList ID="DDLEstatusTodos" runat="server"
                                                                        CssClass="form-control">
                                                                        <asp:ListItem>Sin iniciar</asp:ListItem>
                                                                        <asp:ListItem>Iniciada</asp:ListItem>
                                                                        <asp:ListItem>Terminada</asp:ListItem>
                                                                        <asp:ListItem>Cancelada</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                             <div class="col-lg-6">
                                                                  <asp:LinkButton ID="btnActualizarTodosEstatus"  runat="server" 
                                                                    
                                                                CssClass=" btn btn-labeled btn-palegreen">
                                                                                                <i class="btn-label fa fa-refresh"></i>Actualizar todos los estatus
                                                                   
                                                                 </asp:LinkButton>
                                                             </div>
                                                            
                                                            </div>
                                                        <div class="col-lg-12">
                                                            <uc1:msgError runat="server" ID="msgError3" />
                                                            <uc1:msgSuccess runat="server" ID="msgSuccess3" />
                                                             <uc1:msgSuccess runat="server" ID="msgInfoEvaluaciones" />
                                                        </div>
                                                        




                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <!--    End of second tab-->

                                <div id="agruparReactivos" class="tab-pane">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>

                                            <div class="form-horizontal" id="agruparReactivosForm">
                                                <div class="col-lg-12" style="position: absolute; top: 50%; left: 50%; bottom: 50%;">
                                                    <div class="load-spinner"></div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                       ASIGNE SUBTEMAS A LA ACTIVIDAD:
                                                            <b> <asp:Label runat="server"
                                                                CssClass="lsize-st" ID="EvaluacionSelected"></asp:Label></b>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="col-lg-6">

                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                    <%=Lang_Config.Translate("general", "Nivel")%>
                                                                </label>
                                                                <div class="col-lg-10">
                                                                    <asp:DropDownList ID="ddlNivelesSubtemas" runat="server"
                                                                        AutoPostBack="True" AppendDataBoundItems="false"
                                                                        DataSourceID="SDSnivelesSubtemas" DataTextField="Descripcion"
                                                                        DataValueField="IdNivel" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                     <%=Lang_Config.Translate("general", "Grado")%>
                                                                </label>
                                                                <div class="col-lg-10">
                                                                    <asp:DropDownList ID="ddlGradosSubtemas" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSgradosSubtemas" DataTextField="Descripcion" AppendDataBoundItems="false"
                                                                        DataValueField="IdGrado" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                           <%=Lang_Config.Translate("general", "Asignatura")%>
                                                                </label>
                                                                <div class="col-lg-10">
                                                                    <asp:DropDownList ID="ddlAsignaturasSubtemas"
                                                                        runat="server" AutoPostBack="True" AppendDataBoundItems="false"
                                                                        DataSourceID="SDSasignaturasSubtemas" DataTextField="Descripcion"
                                                                        DataValueField="IdAsignatura" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                    Temas
                                                                </label>
                                                                <div class="col-lg-10">
                                                                    <asp:DropDownList ID="DDLtemas" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDStemas" DataTextField="NomTema" AppendDataBoundItems="false"
                                                                        DataValueField="IdTema" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                    Subtemas
                                                                </label>
                                                                <div class="col-lg-10">
                                                                    <asp:DropDownList ID="DDLsubtemas" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSsubtemas" DataTextField="NomSubtema" AppendDataBoundItems="false"
                                                                        DataValueField="IdSubtema" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:LinkButton ID="btnAgregarTemas" Visible="false" runat="server" type="submit"
                                                                    CssClass="btn btn-lg btn-labeled btn-palegreen">
                                                            <i class="btn-label fa fa-plus"></i>Agregar
                                                                </asp:LinkButton>
                                                            </div>


                                                        </div>


                                                        <div class="col-lg-6">
                                                            <div class="table-responsive">
                                                                <asp:GridView ID="GVdetalleevaluacion" runat="server"
                                                                    AllowSorting="True"
                                                                    AutoGenerateColumns="False"
                                                                    Caption="<h3>Subtemas asignados </h3>"
                                                                    ShowFooter="True"
                                                                    DataSourceID="SDSdetalleevaluacion"
                                                                    DataKeyNames="IdDetalleEvaluacion,Reactivos"
                                                                    CellPadding="3"
                                                                    CssClass="table table-striped table-bordered"
                                                                    Height="17px"
                                                                    Style="font-size: x-small; text-align: left;">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="IdDetalleEvaluacion" HeaderText="Id Detalle Act."
                                                                            Visible="False" ReadOnly="True"
                                                                            SortExpression="IdDetalleEvaluacion">
                                                                            <HeaderStyle Width="50px" />
                                                                            <ItemStyle Width="50px" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="ClaveBateria" HeaderText="Actividad"
                                                                            SortExpression="ClaveBateria">
                                                                            <HeaderStyle Width="110px" />
                                                                            <ItemStyle Width="110px" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Tema" HeaderText="Tema" SortExpression="Tema">
                                                                            <HeaderStyle Width="140px" />
                                                                            <ItemStyle Width="140px" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Subtema" HeaderText="Subtema"
                                                                            SortExpression="Subtema">
                                                                            <HeaderStyle Width="140px" />
                                                                            <ItemStyle Width="140px" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Reactivos" HeaderText="Total Reactivos"
                                                                            SortExpression="Reactivos">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="UsarReactivos" HeaderText="Reactivos a Usar"
                                                                            SortExpression="UsarReactivos">
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="FechaModif" HeaderText="Fecha de Modificación"
                                                                            Visible="False" SortExpression="FechaModif" />
                                                                        <asp:BoundField DataField="Modifico" HeaderText="Modificó"
                                                                            Visible="False" SortExpression="Modifico" />
                                                                    </Columns>
                                                                    <PagerTemplate>
                                                                        <ul runat="server" id="Pag" class="pagination">
                                                                        </ul>
                                                                    </PagerTemplate>
                                                                    <PagerStyle HorizontalAlign="Center" />
                                                                    <SelectedRowStyle CssClass="row-selected" />
                                                                    <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-lg-3 col-lg-offset-4 control-label"><b>Cantidad de reactivos a utilizar</b></label>
                                                                <div class="col-lg-2">
                                                                    <asp:TextBox TextMode="Number" min="0" ID="LBusar" CssClass="form-control" runat="server">   
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <asp:LinkButton ID="btnQuitarTema" Visible="false" Style="margin: 4px;" runat="server" type="submit"
                                                                    CssClass="btn btn-lg btn-labeled btn-warning">
                                                            <i class="btn-label fa fa-remove"></i>Quitar Subtema
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnAsignarNoReactivos" Visible="false" Style="margin: 4px;" type="submit" 
                                                                    runat="server"
                                                                    CssClass="btn btn-lg btn-labeled btn-palegreen shiny">
                                                            <i class="btn-label fa fa-plus"></i>Asignar No.Reactivos
                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <uc1:msgError runat="server" ID="msgError4" />
                                                            <uc1:msgSuccess runat="server" ID="msgSuccess4" />
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <!--End of third tab -->

                                <div id="fechasPlanteles" class="tab-pane">
                                    <asp:UpdatePanel ID="Panelplanteles" runat="server">
                                        <ContentTemplate>
                                            <div class="form-horizontal" id="fechasPlantelesForm">
                                                  <div class="col-lg-12" style="position:absolute;top:50%;left:50%;bottom:50%;">
                                                <div class="load-spinner"></div>
                                            </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        ASIGNAR FECHAS ESPECIFICAS DE ACTIVIDADES A SUCURSALES
                                                 
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                    Institución
                                                                </label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLinstitucionFechasPlanteles" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                                                        DataValueField="IdInstitucion" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                    Sucursal
                                                                </label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLPlantelFechasPlanteles" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSplanteles" DataTextField="Descripcion" AppendDataBoundItems="false"
                                                                        
                                                                        DataValueField="IdPlantel" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                    Estatus
                                                                </label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLEstatusActividadFP" runat="server"
                                                                        CssClass="form-control">
                                                                        <asp:ListItem>Sin iniciar</asp:ListItem>
                                                                        <asp:ListItem>Iniciada</asp:ListItem>
                                                                        <asp:ListItem>Terminada</asp:ListItem>
                                                                        <asp:ListItem>Cancelada</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-lg-4 control-label">Inicio de la Aplicación de la Actividad</label>
                                                                <label class="col-lg-4 control-label">Fecha máxima para evitar penalización</label>
                                                                <label class="col-lg-4 control-label">Fin de la aplicación de la Actividad</label>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-lg-4">
                                                                    <asp:TextBox ID="TBInicioPlanteles" runat="server" CssClass="form-control datepickerGlobal"
                                                                        data-date-format="dd/mm/yyyy" />
                                                                </div>


                                                                <div class="col-lg-4">
                                                                    <asp:TextBox ID="TBPenalizacionPlanteles" runat="server" CssClass="form-control datepickerGlobal"
                                                                        data-date-format="dd/mm/yyyy" />
                                                                </div>

                                                                <div class="col-lg-4">
                                                                    <asp:TextBox ID="TBFinPlanteles" runat="server" CssClass="form-control datepickerGlobal"
                                                                        data-date-format="dd/mm/yyyy" />
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:LinkButton ID="btnAgregarFP"  runat="server" type="submit"
                                                                    CssClass="btn btn-lg btn-labeled btn-palegreen">
                                                                    <i class="btn-label fa fa-plus"></i>Agregar 
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnActualizarFP" Visible="false" 
                                                                    data-dismiss="modal" runat="server" type="submit"
                                                                    CssClass="btn btn-lg btn-labeled btn-palegreen">
                                                                    <i class="btn-label fa fa-refresh"></i>Actualizar 
                                                                </asp:LinkButton>
                                                            </div>
                                                       


                                                            <div class="col-lg-12">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="GVevalplantel" runat="server"
                                                                        AllowPaging="True" 
                                                                        AllowSorting="True"
                                                                   
                                                                        AutoGenerateColumns="False"
                                                                        DataKeyNames="IdPlantel,IdEvaluacion,InicioContestar,FinSinPenalizacion,FinContestar,Estatus"
                                                                        DataSourceID="SDSevalPlantel"
                                                                        Caption="<h3 style=font-size:16px;font-weight:bold !important; >Relación de PLANTELES y Actividades asignadas en el CICLO seleccionado</h3>"
                                                                        PageSize="5"
                                                                        CellPadding="3"
                                                                        CssClass="table table-striped table-bordered"
                                                                        Height="14px"
                                                                        Style="font-size: xx-small; text-align: left;">
                                                                        <Columns>

                                                                            <asp:BoundField DataField="IdPlantel" HeaderText="Id_[Plantel]" ReadOnly="True"
                                                                                SortExpression="IdPlantel" Visible="false">
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="Plantel" HeaderText="[Plantel]"
                                                                                SortExpression="Plantel">
                                                                                <HeaderStyle Width="190px" />
                                                                                <ItemStyle Width="190px" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="IdEvaluacion" Visible="false" HeaderText="Id Actividad"
                                                                                ReadOnly="True" SortExpression="IdEvaluacion" />
                                                                            <asp:BoundField DataField="ClaveBateria" HeaderText="Actividad"
                                                                                SortExpression="ClaveBateria">
                                                                                <HeaderStyle Width="200px" />
                                                                                <ItemStyle Width="200px" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="InicioContestar" HeaderText="Inicia Actividad"
                                                                                SortExpression="InicioContestar" DataFormatString="{0:dd/MM/yyyy}" />
                                                                            <asp:BoundField DataField="FinSinPenalizacion" HeaderText="Fecha Máxima"
                                                                                SortExpression="FinSinPenalizacion" DataFormatString="{0:dd/MM/yyyy}" />
                                                                            <asp:BoundField DataField="FinContestar" HeaderText="Finaliza Actividad"
                                                                                SortExpression="FinContestar" DataFormatString="{0:dd/MM/yyyy}" />

                                                                            <asp:BoundField DataField="Asignatura" HeaderText="[Asignatura]"
                                                                                SortExpression="Asignatura" />
                                                                            <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                                                SortExpression="Estatus" />
                                                                        </Columns>
                                                                        <PagerTemplate>
                                                                            <ul runat="server" id="Pag" class="pagination">
                                                                            </ul>
                                                                        </PagerTemplate>
                                                                        <PagerStyle HorizontalAlign="Center" />
                                                                        <SelectedRowStyle CssClass="row-selected" />
                                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                    </asp:GridView>
                                                                </div>
                                                                <div class="form-group">
                                                                    <asp:LinkButton ID="btnQuitarAsignacionFP" Visible="false" 
                                                                        runat="server" type="submit"
                                                                        CssClass="btn btn-lg btn-labeled btn-darkorange">
                                                                    <i class="btn-label fa fa-remove"></i>Eliminar asignación
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                                  <div class="form-group">
                                                                      <asp:LinkButton ID="btnActualizarFechasPlantelesGeneral" 
                                                                         type="submit" runat="server"
                                                                        CssClass="btn btn-lg btn-labeled btn-palegreen">
                                                                    <i class="btn-label fa fa-refresh"></i>Actualizar Fechas y Estatus a todos
                                                                    </asp:LinkButton>
                                                                </div>

                                                            <div class="form-group">
                                                                <uc1:msgError runat="server" ID="msgError5" />
                                                                <uc1:msgSuccess runat="server" ID="msgSuccess5" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <!-- End of fourth tab -->

                                <div id="fechasAlumnos" class="tab-pane">
                                    <asp:UpdatePanel ID="PanelFechasAlumnos" runat="server">
                                        <ContentTemplate>
                                            <div class="form-horizontal" id="fechasAlumnosForm">
                                                  <div class="col-lg-12" style="position:absolute;top:50%;left:50%;bottom:50%;">
                                                <div class="load-spinner"></div>
                                            </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                    </div>
                                                    <div class="panel-body">
                                                        <div class="col-lg-12">


                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                    Institución
                                                                </label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLinstitucionFechasAlumnos" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSinstituciones" DataTextField="Descripcion" AppendDataBoundItems="false"
                                                                        DataValueField="IdInstitucion" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                    Sucursal
                                                                </label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLplantelFechaAlumnos" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSPlantelesFA" DataTextField="Descripcion" AppendDataBoundItems="false"
                                                                        DataValueField="IdPlantel" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                    Grupo
                                                                </label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLgrupoFechasAlumnos" runat="server" AutoPostBack="True" AppendDataBoundItems="false"
                                                                        DataSourceID="SDSgrupos" DataTextField="Descripcion" DataValueField="IdGrupo"
                                                                        CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">
                                                                    Estatus
                                                                </label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="ddlEstatusFechasAlumnos" runat="server"
                                                                        CssClass="form-control">
                                                                        <asp:ListItem>Sin iniciar</asp:ListItem>
                                                                        <asp:ListItem>Iniciada</asp:ListItem>
                                                                        <asp:ListItem>Terminada</asp:ListItem>
                                                                        <asp:ListItem>Cancelada</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-lg-12">


                                                                <div class="form-group">
                                                                    <label class="col-lg-2 col-xs-2 col-md-2 control-label">Participantes:</label>
                                                                    <div class="col-lg-8 col-xs-10 col-md-10">

                                                                        <asp:ListBox runat="server"  style="width:100%;" ID="CBLalumnos" DataSourceID="SDSalumnos" SelectionMode="Multiple"
                                                                            DataTextField="NomAlumno" DataValueField="IdAlumno" CssClass="multipleSelectable"></asp:ListBox>
                                                                        <%-- <asp:CheckBoxList RepeatColumns="2" ID="CBLalumnos" runat="server"
                                                                             DataSourceID="SDSalumnos" DataTextField="NomAlumno"
                                                                             DataValueField="IdAlumno"
                                                                             Style="background-color: #CCCCCC; font-size:12px; text-align: left; margin: 4px !important;">
                                                                         </asp:CheckBoxList>--%>
                                                                    </div>
                                                                    <div class="col-lg-2">
                                                                        <div class="checkbox">
                                                                            <label>
                                                                                <input id="ckSelectAlumnosAll"  type="checkbox" class="colored-success" >
                                                                                <span class="text">Selecciona todos los participantes</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <label class="col-lg-4 control-label">Inicio de la Aplicación de la Actividad</label>
                                                                    <label class="col-lg-4 control-label">Fecha máxima para evitar penalización</label>
                                                                    <label class="col-lg-4 control-label">Fin de la aplicación de la Actividad</label>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-lg-4">
                                                                        <asp:TextBox ID="TBFechaInicioFechasAlumnos" runat="server" CssClass="form-control datepickerGlobal"
                                                                            data-date-format="dd/mm/yyyy" />
                                                                    </div>
                                                                    <div class="col-lg-4">
                                                                        <asp:TextBox ID="TBFechaPenalizacionFechasAlumnos" runat="server" CssClass="form-control datepickerGlobal"
                                                                            data-date-format="dd/mm/yyyy" />
                                                                    </div>
                                                                    <div class="col-lg-4">
                                                                        <asp:TextBox ID="TBFechaFinFechasAlumnos" runat="server" CssClass="form-control datepickerGlobal"
                                                                            data-date-format="dd/mm/yyyy" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <asp:LinkButton ID="btnAgregarFechasAlumnos" runat="server"
                                                                        type="submit"
                                                                        CssClass="btn btn-lg btn-labeled btn-palegreen">
                                                                    <i class="btn-label fa fa-plus"></i>Agregar 
                                                                    </asp:LinkButton>
                                                                    <asp:LinkButton ID="btnActualizarFechasAlumnos" 
                                                                        Visible="false" type="submit" runat="server"
                                                                        CssClass="btn btn-lg btn-labeled btn-palegreen">
                                                                    <i class="btn-label fa fa-refresh"></i>Actualizar 
                                                                    </asp:LinkButton>
                                                                </div>
                                                              
                                                            </div>

                                                            <div class="col-lg-12">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="GVevalalumno" runat="server"
                                                                        AllowPaging="True" 
                                                                        AllowSorting="True"
                                                                        AutoGenerateColumns="False"
                                                                        DataKeyNames="IdAlumno,IdEvaluacion,InicioContestar,FinSinPenalizacion,FinContestar,Estatus"
                                                                        DataSourceID="SDSevalAlumno"
                                                                        Caption="<h3 style=font-size:16px;font-weight:bold !important;>Relación de ALUMNOS y Actividades asignadas en el GRUPO y CICLO seleccionados</h3>"
                                                                        PageSize="5"
                                                                        CellPadding="3"
                                                                        CssClass="table table-striped table-bordered"
                                                                        Height="14px"
                                                                        Style="font-size: xx-small; text-align: left;">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="IdAlumno" HeaderText="Id Alumno" ReadOnly="True"
                                                                                SortExpression="IdAlumno">
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="Nombre del Alumno" HeaderText="Nombre del Alumno"
                                                                                ReadOnly="True" SortExpression="Nombre del Alumno" />
                                                                            <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                                                                                SortExpression="Matricula" />
                                                                            <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                                                                                ReadOnly="True" SortExpression="IdEvaluacion" />
                                                                            <asp:BoundField DataField="ClaveBateria" HeaderText="Actividad"
                                                                                SortExpression="ClaveBateria" />
                                                                            <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                                                HeaderText="Inicia Actividad" SortExpression="InicioContestar" />
                                                                            <asp:BoundField DataField="FinSinPenalizacion"
                                                                                DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha máxima"
                                                                                SortExpression="FinSinPenalizacion" />
                                                                            <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                                                HeaderText="Finaliza Actividad" SortExpression="FinContestar" />
                                                                            <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"
                                                                                SortExpression="Asignatura" />
                                                                            <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                                                SortExpression="Estatus" />
                                                                        </Columns>
                                                                        <PagerTemplate>
                                                                            <ul runat="server" id="Pag" class="pagination">
                                                                            </ul>
                                                                        </PagerTemplate>
                                                                        <PagerStyle HorizontalAlign="Center" />
                                                                        <SelectedRowStyle CssClass="row-selected" />
                                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                    </asp:GridView>
                                                                </div>
                                                                <div class="form-group">
                                                                    <asp:LinkButton ID="btnEliminarFechasAlumnos" Visible="false" 
                                                                         type="submit" runat="server"
                                                                        CssClass="btn btn-lg btn-labeled btn-darkorange">
                                                                    <i class="btn-label fa fa-remove"></i>Eliminar asignación
                                                                    </asp:LinkButton>
                                                                </div>


                                                            </div>

                                                            
                                                                  <div class="form-group">
                                                                      <asp:LinkButton ID="btnActualizarFechasAlumnosGeneral" 
                                                                         type="submit" runat="server"
                                                                        CssClass="btn btn-lg btn-labeled btn-palegreen">
                                                                    <i class="btn-label fa fa-refresh"></i>Actualizar Fechas y Estatus a todos
                                                                    </asp:LinkButton>
                                                                </div>

                                                            <div class="form-group">
                                                                <uc1:msgError runat="server" ID="msgError6" />
                                                                <uc1:msgSuccess runat="server" ID="msgSuccess6" />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <!-- End of fifth tab-->

                                <div id="seriarActividades" class="tab-pane">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <div class="form-horizontal">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">ÁREA PARA SERIAR LA ACTIVIDAD:<b> <asp:Label runat="server" 
                                                                         CssClass="lsize-st" ID="Evaluacion_Selected_SeriarArea"></asp:Label></b></div>
                                                    <div class="panel-body">

                                                        <div class="form-group">
                                                             <label class="col-lg-10">
                                                                        <b>1.Condicionantes para abrir la actividad:</b>
                                                                        <asp:Label runat="server" 
                                                                         CssClass="lsize-st" ID="Evaluacion_Selected_ActNecesarias"></asp:Label>
                                                                    </label>  
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-lg-9  col-lg-offset-2">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="GVseriaciones" runat="server" AllowPaging="True"
                                                                        AllowSorting="True" AutoGenerateColumns="False"
                                                                        CellPadding="3"
                                                                        DataKeyNames="IdEvaluacion, Minima"
                                                                        DataSourceID="SDSseriaciones"
                                                                        CssClass="table table-striped table-bordered"
                                                                        Height="14px"
                                                                        Style="font-size: xx-small; text-align: left;">
                                                                        <Columns>

                                                                            <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                                                                                Visible="False" ReadOnly="True" SortExpression="IdEvaluacion" />
                                                                            <asp:BoundField DataField="ClaveBateria" HeaderText="Nombre de la Actividad"
                                                                                SortExpression="ClaveBateria" />
                                                                            <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                                                                                SortExpression="Porcentaje">
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                                                HeaderText="Inicia" SortExpression="InicioContestar" />
                                                                            <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                                                HeaderText="Termina" SortExpression="FinContestar" />
                                                                            <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                                                            <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                                                SortExpression="Estatus" />
                                                                            <asp:BoundField DataField="Minima" HeaderText="Calificación Mínima"
                                                                                SortExpression="Minima" />
                                                                            <asp:BoundField DataField="FechaModif" HeaderText="Fecha de Modificación"
                                                                                Visible="False" SortExpression="FechaModif" />
                                                                            <asp:BoundField DataField="Modifico" HeaderText="Modificó"
                                                                                Visible="False" SortExpression="Modifico" />
                                                                        </Columns>
                                                                        <PagerTemplate>
                                                                            <ul runat="server" id="Pag" class="pagination">
                                                                            </ul>
                                                                        </PagerTemplate>
                                                                        <PagerStyle HorizontalAlign="Center" />
                                                                        <SelectedRowStyle CssClass="row-selected" />
                                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>

                                                                <div class="form-group">
                                                                   
                                                                     <asp:Panel ID="AreaMinimaActSeriadas" Visible="false" runat="server">
                                                                        <div class="col-lg-8">
                                                                            <div class="checkbox cbMinimaEdit">
                                                                                <label>
                                                                                    <input id="CHKminimaEdit" runat="server"  type="checkbox" class="colored-success CBCalMinima"/>
                                                                                    <span class="text">Requerir calificación minima</span>
                                                                                    <asp:TextBox ID="TBminimaEdit" TextMode="Number"  style="display:none;" runat="server" CssClass="form-control TBMinimaEdit" />
                                                                                </label>
                                                                                  
                                                                            </div
                                                                        </div>
                                                                   </asp:Panel>
                                                                    </div>


                                                        <div class="form-group">
                                                            <div class="col-lg-9 col-lg-offset-2">
                                                                <asp:LinkButton ID="btnQuitarSeriacion" Visible="false" runat="server"
                                                                    CssClass="btn btn-lg btn-labeled btn-darkorange">
                                                                    <i class="btn-label fa fa-remove"></i>Quitar seriación
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnActualizarSeriacion" Visible="false" runat="server"
                                                                    CssClass="btn btn-lg btn-labeled btn-palegreen">
                                                                    <i class="btn-label fa fa-remove"></i>Actualizar Calificación mínima
                                                                </asp:LinkButton>
                                                            </div>
                                                        </div>


                                                        <div class="horizontal-space"></div>

                                                        <div class="form-group">
                                                                <hr />
                                                            
                                                        <div class="form-group">
                                                            <label class="col-lg-10">
                                                                <b>2. Ahora elija la actividad que deberá estar terminada para poder iniciar la bloqueada:

                                                                </b>
                                                            </label>
                                                        </div>
                                                            <label class="col-lg-2 control-label">
                                                                Curso
                                                            </label>
                                                            <div class="col-lg-10">
                                                                <asp:DropDownList ID="DDLasignaturaSeriarAct" runat="server" AutoPostBack="True"
                                                                    DataSourceID="SDSAsignaturasSeriarAct" DataTextField="Materia" 
                                                                    DataValueField="IdAsignatura" CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                         <div class="form-group">
                                                            <label class="col-lg-2 control-label">
                                                                  Calificaciones
                                                            </label>
                                                            <div class="col-lg-10">
                                                                <asp:DropDownList ID="DDLcalificacionSeriarAct" runat="server" AutoPostBack="True"
                                                                    DataSourceID="SDSCalificacionesSeriarAct" DataTextField="Cal" 
                                                                    DataValueField="IdCalificacion" CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <asp:Panel ID="panelInsertarMinima"  runat="server">
                                                            <div class="col-lg-8">
                                                                <div class="checkbox cbMinimaEdit">
                                                                    <label>
                                                                        <input id="CHKminima" runat="server"  type="checkbox" class="colored-success CBCalMinimaInsertar"/>
                                                                        <span class="text">Requerir calificación minima</span>
                                                                        <asp:TextBox ID="TBminima" TextMode="Number"  style="display:none;" runat="server" CssClass="form-control TBMinimaInsertar" />
                                                                    </label>
                                                                                  
                                                                </div
                                                            </div>
                                                        </asp:Panel>
                                                        </div>
                                                        <div class="form-group">
                                                           <div class="col-lg-9 col-lg-offset-2">
                                                           <div class="table-responsive">
                                                                <asp:GridView ID="GVevaluacionesDisponibles" runat="server" AllowPaging="True"
                                                                    AllowSorting="True" AutoGenerateColumns="False" 
                                                                    DataKeyNames="IdEvaluacion" DataSourceID="SDSEvaluacionesParaSeriar"
                                                                    CssClass="table table-striped table-bordered"
                                                                    Height="14px"
                                                                    Style="font-size: xx-small; text-align: left;"
                                                                    Caption="<h3>Actividades disponibles:</h3>">
                                                                    <Columns>
                                                                      
                                                                        <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                                                                            Visible="False" ReadOnly="True" SortExpression="IdEvaluacion" />
                                                                        <asp:BoundField DataField="ClaveBateria" HeaderText="Nombre de la Actividad"
                                                                            SortExpression="ClaveBateria" />
                                                                        <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                                                                            SortExpression="Porcentaje">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                                            HeaderText="Inicia" SortExpression="InicioContestar" />
                                                                        <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                                            HeaderText="Termina" SortExpression="FinContestar" />
                                                                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                                                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                                            SortExpression="Estatus" />
                                                                    </Columns>
                                                                    <PagerTemplate>
                                                                        <ul runat="server" id="Pag" class="pagination">
                                                                        </ul>
                                                                    </PagerTemplate>
                                                                    <PagerStyle HorizontalAlign="Center" />
                                                                    <SelectedRowStyle CssClass="row-selected" />
                                                                    <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                           </div>  
                                                               </div>  
                                                               
                                                                   
                                                       
                                                        </div>
                                                        <div class="form-group">
                                                              <asp:LinkButton ID="btnSeriarActividad" Visible="false" runat="server"
                                                                CssClass="btn btn-lg btn-labeled btn-palegreen">
                                                                    <i class="btn-label fa fa-plus"></i>Seriar
                                                            </asp:LinkButton>
                                                        </div>

                                                         <div class="form-group">
                                                                <uc1:msgError runat="server" ID="msgError8" />
                                                                <uc1:msgSuccess runat="server" ID="msgSuccess8" />
                                                            </div>
                                                       


                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                         

                                <div id="actividadPractica" class="tab-pane">
                                    <asp:UpdatePanel ID="PanelActividadPractica" runat="server">
                                        <ContentTemplate>
                                            <div class="form-horizontal">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        ÁREA PARA ASIGNAR REACTIVOS A EVALUACIONES DEMO
                                                       </div>
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <div class="col-lg-11">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="GVseleccionados" runat="server" AutoGenerateColumns="False"
                                                                        Caption="<h3>Reactivos seleccionados para la actividad demo</h3>"
                                                                        DataKeyNames="IdPlanteamiento" DataSourceID="SDSevaluaciondemo"
                                                                        CellPadding="3" 
                                                                        CssClass="table table-striped table-bordered"
                                                                        Height="14px" 
                                                                        Style="font-size: xx-small; text-align: left;">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="IdPlanteamiento" HeaderText="IdPlanteamiento"
                                                                                InsertVisible="False" ReadOnly="True" SortExpression="IdPlanteamiento"
                                                                                Visible="False" />
                                                                            <asp:BoundField DataField="Secuencia" HeaderText="Secuencia"
                                                                                SortExpression="Secuencia" />
                                                                            <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                                                                                SortExpression="Consecutivo" />
                                                                            <asp:BoundField DataField="Reactivo" HeaderText="Reactivo"
                                                                                SortExpression="Reactivo" />
                                                                            <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                                                            <asp:BoundField DataField="Archivo de Apoyo 1" HeaderText="Archivo de Apoyo 1"
                                                                                SortExpression="Archivo de Apoyo 1" />
                                                                            <asp:BoundField DataField="Archivo de Apoyo 2" HeaderText="Archivo de Apoyo 2"
                                                                                SortExpression="Archivo de Apoyo 2" />
                                                                        </Columns>
                                                                        <PagerTemplate>
                                                                            <ul runat="server" id="Pag" class="pagination">
                                                                            </ul>
                                                                        </PagerTemplate>
                                                                        <PagerStyle HorizontalAlign="Center" />
                                                                        <SelectedRowStyle CssClass="row-selected" />
                                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>

                                                          

                                                        </div>

                                                        <div class="btn-group">
                                                            <asp:LinkButton ID="btnQuitarReactivoDemo" Visible="false" data-dismiss="modal" runat="server"
                                                                CssClass="btn btn-lg btn-labeled btn-darkorange">
                                                                    <i class="btn-label fa fa-remove"></i>Quitar reactivo
                                                            </asp:LinkButton>
                                                        </div>

                                                        <div class="horizontal-space"></div>

                                                        <div class="form-group">
                                                            <label class="col-lg-3 control-label">
                                                                Subtemas asignados a la actividad
                                                            </label>
                                                            <div class="col-lg-9">
                                                                <asp:DropDownList ID="DDLSubtemasADemo" runat="server"
                                                                    AutoPostBack="True" AppendDataBoundItems="false"
                                                                    DataSourceID="SDSSubtemasAD" DataTextField="NomSubtema"
                                                                    DataValueField="IdSubtema" CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                         <label id="LbReactivosDemo" class="col-lg-2 control-label" runat="server">Listado de reactivos Demo</label>
                                                            <div class="col-lg-10">
                                                            <asp:CheckBoxList ID="CBLreactivos" runat="server"  Font-Bold="true"
                                                                BackColor="#CCCCCC" DataSourceID="SDSreactivos" DataTextField="Reactivo"
                                                                DataValueField="IdPlanteamiento">
                                                            </asp:CheckBoxList>
                                                            </div>
                                                        </div>
                                                         <div class="btn-group">
                                                            <asp:LinkButton ID="btnAgregarReactivo" Visible="false"  runat="server"
                                                                CssClass="btn btn-lg btn-labeled btn-palegreen">
                                                                    <i class="btn-label fa fa-plus"></i>Agregar reactivo
                                                            </asp:LinkButton>

                                                        </div>
                                                         <div class="form-group">
                                                                <uc1:msgError runat="server" ID="msgError7" />
                                                                <uc1:msgSuccess runat="server" ID="msgSuccess7" />
                                                            </div>

                                                    </div>

                                                    
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Start Modal of Cal-->

    <div class="modal fade calificaciones-modal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <asp:UpdatePanel ID="CalificacionesModal" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="myLargeModalLabel"><b>Redactar calificación </b></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal" id="registrationForm"
                                data-bv-message="This value is not valid"
                                data-bv-feedbackicons-valid="glyphicon glyphicon-ok"
                                data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"
                                data-bv-feedbackicons-validating="glyphicon glyphicon-refresh">

                                <div class="panel panel-default">

                                    <div class="panel-body">
                                        

                                        <div class="col-lg-12">

                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">
                                                    Nombre Calificación
                                                </label>
                                                <div class="col-lg-4">
                                                    <asp:TextBox ID="tbCaldescripcion" runat="server"
                                                        data-bv-notempty="true"
                                                        data-bv-notempty-message="La calificación debe tener un nombre"
                                                        MaxLength="400" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <label class="col-lg-2 control-label">
                                                    Abreviatura
                                                </label>
                                                <div class="col-lg-2">
                                                    <asp:TextBox ID="tbCalClave" runat="server"
                                                        data-bv-notempty="true"
                                                        data-bv-notempty-message="La abreviatura no puede estar vacía"
                                                        data-bv-stringlength="true"
                                                        data-bv-stringlength-message="Debe ser menor a 6 caracteres"
                                                        data-bv-stringlength-max="6"
                                                        CssClass="form-control"></asp:TextBox>
                                                </div>

                                            </div>
                                            <div class="form-group">

                                                <label class="col-lg-2 control-label">
                                                    Porcentaje curso
                                                </label>
                                                <div class="col-lg-2">
                                                    <asp:TextBox ID="tbCalValor" TextMode="Number" runat="server"
                                                        CssClass="form-control"
                                                        data-bv-notempty="true"
                                                        data-bv-notempty-message="El porcentaje no puede estar vacío"
                                                        data-bv-lessthan="true"
                                                        data-bv-lessthan-value="100"
                                                        data-bv-lessthan-message="No puede ser mayor a 100%"></asp:TextBox>
                                                </div>
                                                <label class="col-lg-2 control-label">
                                                    Consecutivo (opcional)
                                                </label>
                                                <div class="col-lg-2">
                                                    <asp:TextBox ID="tbCalconsecutivo" TextMode="Number" runat="server"
                                                        CssClass="form-control"></asp:TextBox>
                                                </div>

                                                    <label class="col-lg-2 control-label">
                                                    Número Dias (AutoRegistro)
                                                </label>
                                                <div class="col-lg-2">
                                                    <asp:TextBox  ID="TBNumeroDiasAutoRegistro" TextMode="Number" runat="server"
                                                         CssClass="form-control">
                                                    </asp:TextBox>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-lg-4 control-label">
                                                    Indicador específico (opcional)
                                                </label>
                                                <div class="col-lg-8">
                                                    <asp:DropDownList ID="ddlCalIndicador" runat="server"
                                                        DataSourceID="SDSIndicadores" DataTextField="Descripcion"
                                                        DataValueField="IdIndicador" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                            
                                            </div>

                                            <div class="form-group">
                                                <asp:LinkButton ID="btnGuardarCambiosCalificaciones" data-backdrop="static" type="submit" runat="server"
                                                    CssClass="btnCrearCal btn  btn-lg btn-labeled btn-palegreen shiny" Visible="false">
                                                                                                <i class="btn-label fa fa-refresh"></i>Guardar cambios
                                                </asp:LinkButton>
                                                <asp:LinkButton data-dismiss="modal" ID="btnCancelarCambiosCalificaciones"
                                                    runat="server"
                                                    CssClass="btn btn-lg btn-labeled btn-darkorange">
                                                                                            <i class="btn-label fa fa-remove" ></i>Cancelar
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnCrearNuevoCalificaciones" data-backdrop="static" type="submit" value="Validate" runat="server"
                                                    CssClass="btn btn-lg btn-labeled btn-palegreen">
                                               <i class="btn-label fa fa-plus"></i>Crear Nuevo
                                                </asp:LinkButton>
                                            </div>

                                            <div class="form-group">
                                                <uc1:msgError runat="server" ID="msgErrorDialog" />
                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- End Modal Of Cal-->
    <!-- Start Modal evaluation-->

    <div class="modal fade evaluaciones-modal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel2"><b>Redactar Actividad </b></h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <div class="tabbable">
                                <ul class="nav nav-tabs" id="tabLayout" >
                                    <li class="active">
                                        <a data-toggle="tab" href="#datos">Datos
                                        </a>
                                    </li>

                                    <li class="tab-red">
                                        <a data-toggle="tab" href="#dinamica">Dinámica
                                        </a>
                                    </li>

                                    <li class="tab-blue">
                                        <a data-toggle="tab" href="#instrucciones">Instrucción
                                        </a>
                                    </li>

                                      <li class="tab-red ">
                                        <a  data-toggle="tab" href="#cargaImagenes">Carga de imágenes
                                        </a>
                                    </li>


                                </ul>

                                <div class="tab-content">

                                    <div id="datos" class="tab-pane in active">
                                        <asp:UpdatePanel ID="PanelEvaluacionesModal" runat="server">

                                            <ContentTemplate>
                                                <div class="form-horizontal">
                                                    <div class="panel panel-default">

                                                        <div class="panel-body" id="datosForm">

                                                            <div class="col-lg-12" style="position: absolute; top: 50%; left: 50%; bottom: 50%;">
                                                                <div class="load-spinner"></div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">Nombre Actividad</label>
                                                                <div class="col-lg-6">
                                                                    <asp:TextBox ID="TBclavebateria" runat="server"
                                                                        CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                                <label class="col-lg-2 control-label">No. Actividad</label>
                                                                <div class="col-lg-2">
                                                                    <asp:TextBox ID="TBNoActividad" runat="server"
                                                                        MaxLength="3" TextMode="Number" CssClass="form-control">

                                                                    </asp:TextBox>
                                                                </div>

                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">Fechas Disponibilidad:</label>

                                                                <div class="col-lg-3">
                                                                    <label class="col-lg-4 control-label">Fecha  Inicio:</label>
                                                                    <div class="col-lg-8">
                                                                        <asp:TextBox ID="tbFechaInicio" class="datepickerGlobal form-control"
                                                                            data-date-format="dd/mm/yyyy" runat="server"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <label class="col-lg-5 control-label">Fecha penalización:</label>
                                                                    <div class="col-lg-7">
                                                                        <asp:TextBox ID="tbFechaPenalizacion" class="datepickerGlobal form-control"
                                                                            data-date-format="dd/mm/yyyy" runat="server"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <label class="col-lg-3 control-label">Fecha limite:</label>
                                                                    <div class="col-lg-9">
                                                                        <asp:TextBox ID="tbFechaFin" class="datepickerGlobal form-control"
                                                                            data-date-format="dd/mm/yyyy" runat="server"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-lg-3 control-label">Porcentaje de la calificación</label>
                                                                <div class="col-lg-3">
                                                                    <asp:TextBox ID="TBporcentaje" TextMode="Number" min="0" runat="server"
                                                                        CssClass="form-control"></asp:TextBox>

                                                                </div>

                                                                <label class="col-lg-2 contro-label">
                                                                    Porcentaje de penalización
                                                                </label>
                                                                <div class="col-lg-2">
                                                                    <asp:TextBox ID="TBpenalizacion" runat="server" 
                                                                        CssClass="form-control"></asp:TextBox>   
                                                                  
                                                                </div> 
                                                                 <div class="col-lg-1" style="padding-left:0px;text-align:left;">
                                                                     %
                                                                </div>
                                                                
                                                            
                                                            </div>


                                                            <div class="form-group">
                                                              
                                                                <label class="col-lg-2">Estatus</label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLestatus" runat="server"
                                                                        CssClass="form-control">
                                                                        <asp:ListItem>Sin iniciar</asp:ListItem>
                                                                        <asp:ListItem>Iniciada</asp:ListItem>
                                                                        <asp:ListItem>Terminada</asp:ListItem>
                                                                        <asp:ListItem>Cancelada</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">Tipo</label>
                                                                <div class="col-lg-4">
                                                                    <asp:DropDownList ID="DDLtipo" runat="server"
                                                                        CssClass="form-control">
                                                                        <asp:ListItem>Aprendizaje</asp:ListItem>
                                                                        <asp:ListItem>Evaluación</asp:ListItem>
                                                                        <asp:ListItem>Material</asp:ListItem>
                                                                        <asp:ListItem>Reactivos</asp:ListItem>
                                                                        <asp:ListItem>Documento</asp:ListItem>
                                                                        <asp:ListItem>Examen</asp:ListItem>
                                                                        <asp:ListItem>Tarea</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <label class="col-lg-2 control-label">La actividad la captura</label>
                                                                <div class="col-lg-4">
                                                                    <asp:DropDownList ID="DDLcalifica" runat="server"
                                                                        CssClass="form-control">
                                                                        <asp:ListItem></asp:ListItem>
                                                                        <asp:ListItem Value="P">Profesor</asp:ListItem>
                                                                        <asp:ListItem Value="C">Coordinador</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                            </div>
                                                            <div class="form-group">
                                                    
                                                                  <div class="checkbox">
                                                                            <label>
                                                                                <input id="CBCerrarCaptura" runat="server" type="checkbox" class="colored-success">
                                                                                <span class="text">Cerrar Captura</span>
                                                                            </label>
                                                                        </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div id="dinamica" class="tab-pane">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">

                                            <ContentTemplate>
                                                <div class="form-horizontal">
                                                    <div class="panel panel-default">

                                                        <div class="panel-body" id="dinamicaForm">
                                                            <div class="col-lg-12" style="position: absolute; top: 50%; left: 50%; bottom: 50%;">
                                                                <div class="load-spinner"></div>
                                                            </div>
                                                            <div class="col-lg-3">

                                                                <div class="form-group">

                                                                    <div class="col-lg-12">
                                                                        <div class="checkbox">
                                                                            <label>
                                                                                <input id="chkSegundavuelta" runat="server" type="checkbox" class="colored-success" checked="checked">
                                                                                <span class="text">Permite segunda vuelta</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">

                                                                    <div class="col-lg-12">
                                                                        <div class="checkbox">
                                                                            <label>
                                                                                <input id="CBseentregadocto" runat="server" type="checkbox" class="colored-success">
                                                                                <span class="text">Se entrega documento</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">

                                                                    <div class="col-lg-12">
                                                                        <div class="checkbox">
                                                                            <label>
                                                                                <input id="ChkEsForo" runat="server" type="checkbox" class="colored-success">
                                                                                <span class="text">La actividad es foro</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-9 areaOpcionaVuelta">

                                                                <div class="form-group" id="DespliegueRepeticion">


                                                                    <div class="form-group">
                                                                        <label class="col-lg-5 control-label">
                                                                            Modo de despliegue de reactivos
                                                                        </label>
                                                                        <div class="col-lg-7">
                                                                            <asp:DropDownList ID="ddlAleatoria" AutoPostBack="true" runat="server"
                                                                                CssClass="form-control">
                                                                                <asp:ListItem Value="0">Secuencial</asp:ListItem>
                                                                                <asp:ListItem Value="1">Aleatorio por Subtema</asp:ListItem>
                                                                                <asp:ListItem Value="2" Enabled="false">Aleatorio del Total</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="col-lg-5 control-label">
                                                                            Modo de despliegue de opciones
                                                                        </label>
                                                                        <div class="col-lg-7">
                                                                            <asp:DropDownList ID="ddlOpcionAleatoria" AutoPostBack="true" runat="server"
                                                                                CssClass="form-control">
                                                                                <asp:ListItem Value="0">Secuencial</asp:ListItem>
                                                                                <asp:ListItem Value="1">Aleatorio </asp:ListItem>

                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>


                                                                    <div class="form-group">
                                                                        <div class="checkbox">
                                                                            <label>
                                                                                <input id="chkRepeticion" runat="server" type="checkbox" class="colored-success">
                                                                                <span class="text">Repetición automática</span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <div id="panelRepeticion" class="form-group" style="display: none;" runat="server">
                                                                        <div class="form-group">
                                                                            <label class="col-lg-5 control-label">
                                                                                Indicador al que estará sujeta la repetición
                                                                            </label>
                                                                            <div class="col-lg-7">
                                                                                <asp:DropDownList ID="ddlIndicadorRepeticion" runat="server"
                                                                                    DataSourceID="SDSIndicadores" DataTextField="Descripcion"
                                                                                    DataValueField="IdIndicador" CssClass="form-control"
                                                                                    AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="col-lg-5 control-label">
                                                                                Minímo resultado para no repetir
                                                                            </label>
                                                                            <div class="col-lg-7">

                                                                                <asp:DropDownList ID="ddlResultadoRepeticion" runat="server"
                                                                                    DataSourceID="SDSResultados" DataTextField="Color"
                                                                                    DataValueField="Selector" CssClass="form-control">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="col-lg-5 control-label">
                                                                                Máximo número de repeticiones
                                                                            </label>
                                                                            <div class="col-lg-7">
                                                                                <asp:TextBox ID="tbRepeticiones" CssClass="form-control" runat="server"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div id="instrucciones" class="tab-pane">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">

                                            <ContentTemplate>
                                                <div class="row">
                                                    <asp:TextBox ID="ckEditor" runat="server"
                                                        ClientIDMode="Static"
                                                        TextMode="MultiLine"></asp:TextBox>
                                                </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div id="cargaImagenes" class="tab-pane">

                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">

                                            <ContentTemplate>
                                                <div class="form-horizontal">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body" id="formCargaImagenes">
                                                             <div class="col-lg-12" style="position: absolute; top: 50%; left: 50%; bottom: 50%;">
                                                                <div class="load-spinner"></div>
                                                            </div>



                                                            <div class="form-group">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="gvCargaImagenes" runat="server" AllowPaging="True"
                                                                        AllowSorting="True" AutoGenerateColumns="False"
                                                                        DataKeyNames="IdApoyoP,NombreApoyo" DataSourceID="SDSApoyosPlanteamientos"
                                                                        CssClass="table table-striped table-bordered"
                                                                        Height="14px"
                                                                        Style="font-size: xx-small; text-align: left;"
                                                                        Caption="<h3>Apoyos Actuales:</h3>">
                                                                        <Columns>
                                                                            <asp:BoundField DataField="IdApoyoP" HeaderText="IdApoyoP"
                                                                                Visible="False" ReadOnly="True" SortExpression="IdApoyoP" />
                                                                            <asp:BoundField DataField="NombreApoyo" HeaderText="Nombre del Apoyo"
                                                                                SortExpression="NombreApoyo" />
                                                                        </Columns>
                                                                        <PagerTemplate>
                                                                            <ul runat="server" id="Pag" class="pagination">
                                                                            </ul>
                                                                        </PagerTemplate>
                                                                        <PagerStyle HorizontalAlign="Center" />
                                                                        <SelectedRowStyle CssClass="row-selected" />
                                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-lg-8">
                                                                    <asp:AsyncFileUpload Style="width: 200px !important;" runat="server" ID="ArchivoApoyoPlanteamiento"
                                                                        Width=" 800" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <asp:LinkButton ID="btnUploadFileQuestions" runat="server"
                                                                    CssClass="btnCrearCal btn  btn-lg btn-labeled btn-success">
                                                             <i class="btn-label fa fa-upload"></i>Subir Archivo
                                                                </asp:LinkButton>
                                                                <asp:LinkButton ID="btnDeleteFileQuestion"
                                                                    runat="server" Visible="false"
                                                                    CssClass="btn btn-lg btn-labeled btn-darkorange">
                                                      <i class="btn-label fa fa-remove" ></i>Eliminar
                                                                </asp:LinkButton>
                                                            </div>
                                                            <div class="form-group">
                                                                <uc1:msgInfo runat="server" ID="msgInfo" />
                                                                <uc1:msgError runat="server" ID="msgError9" />
                                                                <uc1:msgSuccess runat="server" ID="msgSuccess9" />
                                                            </div>
                                                                 
                                                        </div>
                                                    </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                </div>
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">

                                    <ContentTemplate>
                                        <div class="form-group">
                                            <asp:LinkButton ID="btnActualizarEvaluacion" runat="server" type="submit"
                                                CssClass="btnCrearCal btn  btn-lg btn-labeled btn-palegreen shiny btnActEval">
                                            <i class="btn-label fa fa-refresh"></i>Guardar cambios
                                            </asp:LinkButton>
                                            <asp:LinkButton data-dismiss="modal" ID="btnCerrar"
                                                runat="server"
                                                CssClass="btn btn-lg btn-labeled btn-darkorange btnCerrarEval">
                                                      <i class="btn-label fa fa-remove" ></i>Cancelar
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnCrearnuevaEvaluacion" runat="server" type="submit"
                                                CssClass="btn btn-lg btn-labeled btn-palegreen btnCrearNueEval">
                                                <i class="btn-label fa fa-plus"></i>Crear Nuevo
                                            </asp:LinkButton>
                                        </div>
                                        <div class="form-group">
                                            <uc1:msgError runat="server" ID="msgErrorDialog2" />

                                            <uc1:msgSuccess runat="server" ID="msgSuccess2" />
                                        </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="horizontal-space"></div>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- End Modal evaluation-->

    <div class="sqlDatasources">
        <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT * FROM [CicloEscolar] WHERE ([Estatus] = 'Activo') ORDER BY [FechaInicio]"></asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSniveles" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>

     <asp:SqlDataSource ID="SDSApoyosPlanteamientos" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT * FROM ApoyoPlanteamiento"></asp:SqlDataSource>
    

        <asp:SqlDataSource ID="SDSasignaturas" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT [IdAsignatura], [IdGrado], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) ORDER BY [Descripcion]">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDLgradoCalificaciones" Name="IdGrado"
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSgrados" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlNivelCalificaciones" Name="IdNivel"
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="SDScalificaciones" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT C.IdCalificacion, C.IdCicloEscolar, C.Clave, C.IdAsignatura, A.Descripcion Materia, C.Consecutivo,C.Descripcion, C.Valor, C.IdIndicador, I.Descripcion AS DescripcionIndicador, C.FechaModif, C.Modifico
FROM Calificacion C LEFT JOIN Indicador I ON C.IdIndicador = I.IdIndicador, Asignatura A
WHERE ([IdCicloEscolar] = @IdCicloEscolar)  and (A.IdAsignatura = C.IdAsignatura) and (A.IdAsignatura = @IdAsignatura)
ORDER BY C.Consecutivo, C.Descripcion">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDLcicloescolarCalificaciones" Name="IdCicloEscolar"
                    PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="DDLasignaturaCalificaciones" Name="IdAsignatura"
                    PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSIndicadores" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT [IdIndicador], [Descripcion] FROM [Indicador] ORDER BY [Descripcion]"></asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSCalificacionesCal" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT C.IdCalificacion, C.Consecutivo, C.Descripcion 
FROM Calificacion C, Asignatura A 
WHERE ([IdCicloEscolar] = @IdCicloEscolar)  and (A.IdAsignatura = C.IdAsignatura) and (A.IdAsignatura = @IdAsignatura)
ORDER BY C.Consecutivo, C.Descripcion ">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDLcicloescolarCalificaciones" Name="IdCicloEscolar"
                    PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="DDLAsignaturaCalificaciones" Name="IdAsignatura"
                    PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT * FROM [Evaluacion] 
where IdCalificacion = @IdCalificacion
ORDER BY [IdEvaluacion] DESC">
            <SelectParameters>
                <asp:ControlParameter ControlID="gvCalificaciones" Name="IdCalificacion"
                    PropertyName="SelectedDataKey('IdCalificacion')" />
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSResultados" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="
                    (SELECT 'Rojo - ' + CAST(I.MinRojo As VARCHAR(10)) + '%' AS 'Color', 0 AS 'Selector'
                    FROM Indicador I
                    WHERE I.IdIndicador = @IdIndicador)
                    UNION
                    (SELECT 'Amarillo - ' + CAST(I.MinAmarillo As VARCHAR(10)) + '%' AS 'Color', 1 AS 'Selector'
                    FROM Indicador I
                    WHERE I.IdIndicador = @IdIndicador)
                    UNION
                    (SELECT 'Verde - ' + CAST(I.MinVerde As VARCHAR(10)) + '%' AS 'Color', 2 AS 'Selector'
                    FROM Indicador I
                    WHERE I.IdIndicador = @IdIndicador)
                    UNION
                    (SELECT 'Azul - ' + CAST(I.MinAzul As VARCHAR(10)) + '%' AS 'Color', 3 AS 'Selector'
                    FROM Indicador I
                    WHERE I.IdIndicador = @IdIndicador) ORDER BY Selector">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlIndicadorRepeticion" Name="IdIndicador"
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSdetalleevaluacion" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select  D.IdDetalleEvaluacion, E.ClaveBateria, 
Cast(T.Numero as varchar(10)) + '.-' +  T.Descripcion Tema, Cast(S.Numero as varchar(10)) + '.-' +  S.Descripcion Subtema,
(select Count(IdPlanteamiento) from Planteamiento where IdSubtema = S.IdSubtema and Ocultar='False') as Reactivos, D.UsarReactivos, D.FechaModif, D.Modifico 
from Evaluacion E, DetalleEvaluacion D, Subtema S, Tema T
where E.IdEvaluacion =@IdEvaluacion and
D.IdEvaluacion = E.IdEvaluacion and
S.IdSubtema = D.IdSubtema and
T.IdTema = S.IdTema
order by Subtema">
            <SelectParameters>
                <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                    PropertyName="SelectedDataKey('IdEvaluacion')" />
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSnivelesSubtemas" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSgradosSubtemas" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlNivelesSubtemas" Name="IdNivel"
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSasignaturasSubtemas" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT [IdAsignatura], [IdGrado], [Descripcion] FROM [Asignatura] WHERE ([IdGrado] = @IdGrado) ORDER BY [Descripcion]">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlGradosSubtemas" Name="IdGrado"
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="SDStemas" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT [IdTema], [IdAsignatura], Cast([Numero] as varchar(8))+ ' ' + [Descripcion] NomTema  FROM [Tema] WHERE ([IdAsignatura] = @IdAsignatura)
order by [Numero],[Descripcion]">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlAsignaturasSubtemas" Name="IdAsignatura"
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSsubtemas" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT [IdSubtema], [IdTema], Cast([Numero] as varchar(8)) + ' ' + [Descripcion] NomSubtema FROM [Subtema] WHERE ([IdTema] = @IdTema)
                                and IdSubtema NOT IN (SELECT IdSubtema FROM DetalleEvaluacion WHERE IdEvaluacion = @IdEvaluacion)
order by [Numero],[Descripcion]">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDLtemas" Name="IdTema"
                    PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                    PropertyName="SelectedDataKey('IdEvaluacion')" />
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSSubtemasAD" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select S.IdSubtema, CAST(S.Numero as varchar(8)) + '.-' + S.Descripcion + '  (' + T.Descripcion + ')' as NomSubtema
from DetalleEvaluacion D, Subtema S, Tema T
where D.IdEvaluacion = @IdEvaluacion and 
S.IdSubtema = D.IdSubtema
and T.IdTema = S.IdTema
order by T.Numero, S.Numero">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedDataKey('IdEvaluacion')" />
                    </SelectParameters>
                </asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSinstituciones" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT * FROM [Institucion] ORDER BY [Descripcion]"></asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSplanteles" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>">
           
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSevalPlantel" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"  ></asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSevalAlumno" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
            >
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSgrupos" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT [IdGrupo], [Descripcion] FROM [Grupo] WHERE (([IdGrado] = @IdGrado) AND ([IdPlantel] = @IdPlantel)) and ([IdCicloEscolar] = @IdCicloEscolar) ORDER BY [Descripcion]">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDLGradoCalificaciones" Name="IdGrado"
                    PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="DDLplantelFechaAlumnos" Name="IdPlantel"
                    PropertyName="SelectedValue" Type="Int32" />
                <asp:ControlParameter ControlID="DDLcicloescolarCalificaciones" Name="IdCicloEscolar"
                    PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>


        <asp:SqlDataSource ID="SDSPlantelesFA" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDLinstitucionFechasAlumnos" Name="IdInstitucion"
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSalumnos" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>">
        </asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSreactivos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdPlanteamiento, cast(Consecutivo as varchar(3)) + '.- ' + ISNULL(Redaccion,'-') + ' ('  + ISNULL(ArchivoApoyo,'-') + ')' as Reactivo, ArchivoApoyo, ArchivoApoyo2  FROM Planteamiento
WHERE IdSubtema = @IdSubtema and Ocultar = 1
and IdPlanteamiento not in (select IdPlanteamiento from EvaluacionDemo 
where IdEvaluacion = @IdEvaluacion)
ORDER BY Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLSubtemasADemo" Name="IdSubtema"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedDataKey('IdEvaluacion')" />
                    </SelectParameters>
                </asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSevaluaciondemo" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.IdPlanteamiento, E.Consecutivo as Secuencia, P.Consecutivo, P.Redaccion as Reactivo, P.TipoRespuesta as Tipo, 
ArchivoApoyo as 'Archivo de Apoyo 1', ArchivoApoyo2 as 'Archivo de Apoyo 2'
from Planteamiento P, EvaluacionDemo E
where P.IdPlanteamiento = E.IdPlanteamiento
and E.IdEvaluacion = @IdEvaluacion
order by E.COnsecutivo, P.Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedDataKey('IdEvaluacion')" />
                    </SelectParameters>
                </asp:SqlDataSource>
        <asp:SqlDataSource ID="SDSseriaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="
                SELECT Distinct 
                    E.IdEvaluacion, 
                    E.ClaveBateria, 
                    E.Porcentaje, 
                    E.InicioContestar, 
                    E.FinContestar, 
                    E.Tipo, 
                    E.Estatus, 
                    S.Minima, 
                    S.FechaModif, 
                    S.Modifico 
                FROM 
                    Evaluacion E, 
                    SeriacionEvaluaciones S 
                WHERE 
                    E.IdEvaluacion = S.IdEvaluacionPrevia 
                    AND S.IdEvaluacion = @IdEvaluacion 
                    AND  E.IdEvaluacion IN (SELECT S.IdEvaluacionPrevia FROM SeriacionEvaluaciones S WHERE S.IdEvaluacion = @IdEvaluacion)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="gvEvaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedDataKey('IdEvaluacion')" />
                    </SelectParameters>
                </asp:SqlDataSource>

         <asp:SqlDataSource ID="SDSAsignaturasSeriarAct" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT A.IdAsignatura, A.Descripcion + ' (' + Ar.Descripcion + ')'  as Materia
FROM Asignatura A, Area Ar
WHERE (A.IdGrado = @IdGrado) and (Ar.IdArea = A.IdArea)
ORDER BY Ar.Descripcion,A.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLGradoCalificaciones" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

        
                <asp:SqlDataSource ID="SDSCalificacionesSeriarAct" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT C.IdCalificacion, C.Descripcion + ' (' + A.Descripcion + ')' as Cal
FROM Calificacion C, Asignatura A
WHERE C.IdCicloEscolar = @IdCicloEscolar
and A.IdAsignatura = C.IdAsignatura and A.IdAsignatura = @IdAsignatura
order by C.Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolarCalificaciones" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignaturaSeriarAct" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
          <asp:SqlDataSource ID="SDSevaluacionesSer" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM SeriacionEvaluaciones ">
                   
                </asp:SqlDataSource>

          <asp:SqlDataSource ID="SDSEvaluacionesParaSeriar" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Evaluacion] WHERE ([IdCalificacion] = @IdCalificacion) AND IdEvaluacion <> @IdEvaluacion AND IdEvaluacion NOT IN (SELECT S.IdEvaluacionPrevia FROM SeriacionEvaluaciones S WHERE S.IdEvaluacion = @IdEvaluacion)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcalificacionSeriarAct" Name="IdCalificacion"
                            PropertyName="SelectedValue" Type="Int64" />
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedDataKey('IdEvaluacion')" />
                    </SelectParameters>
                </asp:SqlDataSource>

    </div>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" runat="Server">

    <script type="text/javascript" async
  src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_CHTML">
</script>
    <script type="text/javascript">

        Sys.Application.add_load(LoadHandler);

        function LoadHandler(sender, args0) {

            $(function () {
                
                $("#registrationForm").bootstrapValidator();

                $('.selectable').select2();

                $(".multipleSelectable").select2({
                    placeholder: "--Selecciona un elemento--",
                    allowClear: true
                });
                var editor = CKEDITOR.replace('ckEditor', {
                    language: 'es',
                    height: 250,
                });

                editor.on('change', function (evt) {
                    $('#ckEditor').val(evt.editor.getData());
                });

                $('.DDLAC').on('change', function (event) {

                    var opts = {
                        lines: 10, // The number of lines to draw
                        length: 7, // The length of each line
                        width: 6, // The line thickness
                        radius: 15, // The radius of the inner circle
                        corners: 1, // Corner roundness (0..1)
                        rotate: 0, // The rotation offset
                        color: '#000', // #rgb or #rrggbb
                        speed: 1, // Rounds per second
                        trail: 60, // Afterglow percentage
                        shadow: false, // Whether to render a shadow
                        hwaccel: false, // Whether to use hardware acceleration
                        className: 'spinner', // The CSS class to assign to the spinner
                        zIndex: 2e9,
                        top: 50
                    };
                    var target = document.getElementById('spin');
                    var spinner;
                    spinner = new Spinner(opts).spin(target);
                   

                });


            });
        }


    </script>

</asp:Content>


