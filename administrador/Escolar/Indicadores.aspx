﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false" 
    CodeFile="Indicadores.aspx.vb" Inherits="administrador_AgruparReactivos" EnableEventValidation="false" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:UpdatePanel runat="server">
        <ContentTemplate>


            <div class="form-horizontal" id="IndicadoresForm">

                <div class="panel panel-default">

                    <div class="panel-heading">

                        <b>INDICADORES CREADOS</b>
                    </div>

                    <div class="panel-body">

                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="table-responsive">
                                    <asp:GridView ID="GVindicadores" runat="server" AllowPaging="True"
                                        AllowSorting="True" AutoGenerateColumns="False"
                                        BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px"
                                        DataKeyNames="IdIndicador,Descripcion,MinRojo,MinAmarillo,MinVerde,MinAzul,MinRojoPorc,
                     MinAmarilloPorc,MinVerdePorc,MinAzulPorc"
                                        DataSourceID="SDSindicadores"
                                        CellPadding="3"
                                        CssClass="table table-striped table-bordered"
                                        Height="17px"
                                        Style="font-size: x-small; text-align: left;">
                                        <Columns>

                                            <asp:BoundField DataField="IdIndicador" HeaderText="IdIndicador"
                                                InsertVisible="False" ReadOnly="True" SortExpression="IdIndicador"
                                                Visible="False" />
                                            <asp:BoundField DataField="Descripcion" HeaderText="Descripción"
                                                SortExpression="Descripcion" />
                                            <asp:BoundField DataField="MinRojo" HeaderText="Min. Rojo Prom."
                                                SortExpression="MinRojo" />
                                            <asp:BoundField DataField="MinAmarillo" HeaderText="Min. Amarillo Prom."
                                                SortExpression="MinAmarillo" />
                                            <asp:BoundField DataField="MinVerde" HeaderText="Min. Verde Prom."
                                                SortExpression="MinVerde" />
                                            <asp:BoundField DataField="MinAzul" HeaderText="Min. Azul Prom."
                                                SortExpression="MinAzul" />
                                            <asp:BoundField DataField="MinRojoPorc" HeaderText="Min. Rojo % Av."
                                                SortExpression="MinRojoPorc" />
                                            <asp:BoundField DataField="MinAmarilloPorc" HeaderText="Min. Amarillo % Av."
                                                SortExpression="MinAmarilloPorc" />
                                            <asp:BoundField DataField="MinVerdePorc" HeaderText="Min. Verde % Av."
                                                SortExpression="MinVerdePorc" />
                                            <asp:BoundField DataField="MinAzulPorc" HeaderText="Min. Azul % Av."
                                                SortExpression="MinAzulPorc" />
                                            <asp:BoundField DataField="FechaModif" HeaderText="FechaModif"
                                                SortExpression="FechaModif" Visible="False" />
                                            <asp:BoundField DataField="Modifico" HeaderText="Modifico"
                                                SortExpression="Modifico" Visible="False" />
                                        </Columns>
                                        <PagerTemplate>
                                            <ul runat="server" id="Pag" class="pagination">
                                            </ul>
                                        </PagerTemplate>
                                        <PagerStyle HorizontalAlign="Center" />
                                        <SelectedRowStyle CssClass="row-selected" />
                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 wi-padd">
            <div class="orders-container">
                <div class="orders-header back-12">
                    <h6>COLORIMETRÍA GENERAL</h6>
                </div>
                <ul class="orders-list">
                    <li class="order-item">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 item-left">
                                <div class="item-booker">Mínima Calificacion Azul</div>
                                <div class="item-time">
                                    <i class="fa fa-check"></i>
                                    <span>(Excelente,Meta Sobrepasada)</span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 item-right">
                                <div class="item-price">
                                    <svg width="30" height="30">
                                        <rect width="30" height="30" style="fill: #0000FF; stroke-width: 3; stroke: #0000FF;" />
                                    </svg>
                                </div>
                            </div>
                              
                        </div>
                        
                    </li>
                    <li class="order-item">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 item-left">
                                <div class="item-booker">Mínima Calificación para Verde </div>
                                <div class="item-check">
                                    <i class="fa fa-check"></i>
                                    <span>(bien, aceptable, meta alcanzada)	</span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 item-right">
                                <div class="item-price">
                                     <svg width="30" height="30">
                                        <rect width="30" height="30" style="fill:  #00CC00; stroke-width: 3; stroke: #00CC00;" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                       
                    </li>
                    <li class="order-item">
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 item-left">
                                <div class="item-booker">Mínima Calificación para Amarillo </div>
                                <div class="item-time">
                                    <i class="fa fa-remove"></i>
                                    <span>(se requiere revisión y corregir)	</span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 item-right">
                                <div class="item-price">
                                  <svg width="30" height="30">
                                        <rect width="30" height="30" style="fill: #D9D900; stroke-width: 3; stroke: #D9D900;" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                      
                    </li>
                     <li class="order-item">
                        <div class="row ">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 item-left">
                                <div class="item-booker">Mínima Calificación para Rojo </div>
                                <div class="item-time">
                                    <i class="fa fa-remove"></i>
                                    <span>(Mal, inaceptable)	</span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 item-right">
                                <div class="item-price">
                                  <svg width="30" height="30">
                                        <rect width="30" height="30" style="fill: #FF0000; stroke-width: 3; stroke: #FF0000;" />
                                    </svg>
                                </div>
                            </div>
                        </div>
                      
                    </li>
                   
                </ul>
                
            </div>
        </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 wi-padd">
            <div class="orders-container">
                <div class="orders-header back-12">
                    <h6>RANGOS PARA PROMEDIO</h6>
                </div>
                <ul class="orders-list">
                    <li class="order-item">
                        <div class="row he-68"  >
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item-left ">
                                <div class="form-group">
                                
                                    <div class="col-lg-9">
                                        <asp:TextBox runat="server"
                                            ClientIDMode="Static"
                                            ID="TBIndicadorAzul"
                                            CssClass="form-control slider-azul"></asp:TextBox>

                                    </div>
                                    <div class="col-lg-3">
                                        <label id="slider-value-azul">0</label> 
                                    </div>

                                </div>
                            </div>
                        
                              
                        </div>
                    
                    </li>
                    <li class="order-item">
                        <div class="row he-68">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item-left">
                                 <div class="form-group">
                                
                                    <div class="col-lg-9">
                                        <asp:TextBox runat="server"
                                            ClientIDMode="Static"
                                            ID="TBIndicadorVerde"
                                            CssClass="form-control slider-verde"></asp:TextBox>

                                    </div>
                                    <div class="col-lg-3">
                                        <label id="slider-value-verde">0</label> 
                                    </div>

                                </div>
                            </div>
                           
                        </div>
                       
                    </li>
                    <li class="order-item">
                        <div class="row he-68">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item-left">
                            <div class="form-group">
                                
                                    <div class="col-lg-9">
                                        <asp:TextBox runat="server"
                                            ClientIDMode="Static"
                                            ID="TBIndicadorAmarillo"
                                            CssClass="form-control slider-amarillo"></asp:TextBox>

                                    </div>
                                    <div class="col-lg-3">
                                        <label id="slider-value-amarillo">0</label> 
                                    </div>

                                </div>
                                 </div>
                        </div>
                     
                    </li>
                    <li class="order-item">
                        <div class="row he-68">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item-left">
                                <div class="form-group">
                                
                                    <div class="col-lg-9">
                                        <asp:TextBox runat="server"
                                            ClientIDMode="Static"
                                            ID="TBIndicadorRojo"
                                            CssClass="form-control slider-rojo"></asp:TextBox>

                                    </div>
                                    <div class="col-lg-3">
                                        <label id="slider-value-rojo">0</label> 
                                    </div>

                                </div>
                            </div>
                        </div>
                      
                    </li>
                   
                </ul>
                
            </div>
        </div>




          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 wi-padd">
            <div class="orders-container" >
                <div class="orders-header back-12">
                    <h6>RANGOS PARA PORCENTAJE DE AVANCES</h6>
                </div>
                <ul class="orders-list">
                    <li class="order-item">
                        <div class="row he-68">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item-left">
                            <div class="form-group">
                                
                                    <div class="col-lg-9">
                                        <asp:TextBox runat="server"
                                            ClientIDMode="Static"
                                            ID="TBIndicadorAzulP"
                                            CssClass="form-control slider-azulp"></asp:TextBox>

                                    </div>
                                    <div class="col-lg-3">
                                        <label id="slider-value-azulp">0</label> %
                                    </div>

                                </div>
                              </div>
                        </div>
                      
                    </li>
                    <li class="order-item">
                        <div class="row he-68">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item-left">
                                <div class="form-group">
                                
                                    <div class="col-lg-9">
                                        <asp:TextBox runat="server"
                                            ClientIDMode="Static"
                                            ID="TBIndicadorVerdeP"
                                            CssClass="form-control slider-verdep"></asp:TextBox>

                                    </div>
                                    <div class="col-lg-3">
                                        <label id="slider-value-verdep">0</label> %
                                    </div>

                                </div>
                            </div>
                           
                        </div>
                      
                    </li>
                    <li class="order-item">
                        <div class="row he-68">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item-left">
                               <div class="form-group">
                                
                                    <div class="col-lg-9">
                                        <asp:TextBox runat="server"
                                            ClientIDMode="Static"
                                            ID="TBIndicadorAmarilloP"
                                            CssClass="form-control slider-amarillop"></asp:TextBox>

                                    </div>
                                    <div class="col-lg-3">
                                        <label id="slider-value-amarillop">0</label> %
                                    </div>

                                </div>


                            </div>
                           
                        </div>
                        
                    </li>
                    <li class="order-item">
                        <div class="row he-68">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item-left">
                                <div class="form-group">
                                
                                    <div class="col-lg-9">
                                        <asp:TextBox runat="server"
                                            ClientIDMode="Static"
                                            ID="TBIndicadorRojoP"
                                            CssClass="form-control slider-rojop"></asp:TextBox>

                                    </div>
                                    <div class="col-lg-3">
                                        <label id="slider-value-rojop">0</label> %
                                    </div>

                                </div>
                            </div>
                           
                        </div>
                      
                    </li>
                   
                </ul>
                
            </div>
        </div>

         <div class="col-lg-12">



             <div class="form-group">
                 <label class="col-lg-3 control-label">
                     Descripción Indicador
                 </label>

                 <div class="col-lg-6">
                     <asp:TextBox runat="server" ClientIDMode="Static"
                         ID="TBDescripcionIndicador"
                         class="form-control" TextMode="MultiLine" Rows="5"> 
                         
                     </asp:TextBox>
                 </div>

             </div>

         </div>
         <div class="col-lg-12">
             <asp:LinkButton ID="btnInsertarIndicador" runat="server" type="submit"
                 CssClass="btn  btn-lg btn-labeled btn-palegreen shiny">
               <i class="btn-label fa fa-plus"></i>Insertar
             </asp:LinkButton>

             <asp:LinkButton ID="btnActualizarIndicador" runat="server" type="submit"
                 CssClass="btn btn-lg btn-labeled btn-palegreen">
                   <i class="btn-label fa fa-refresh"></i>Actualizar
             </asp:LinkButton>

             
             <asp:LinkButton ID="btnEliminarIndicador" runat="server"
                 CssClass="btn btn-lg btn-labeled btn-darkorange">
                   <i class="btn-label fa fa-remove"></i>Eliminar
             </asp:LinkButton>
             
             </div>

                        <div class="col-lg-12">
                            <uc1:msgerror runat="server" id="msgError" />
                            <uc1:msgsuccess runat="server" id="msgSuccess" />
                        </div>
         </div>
         
         </div>
     </div>
               </ContentTemplate>
    </asp:UpdatePanel>

    <div class="sqlDataSources">
          <asp:SqlDataSource ID="SDSindicadores" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [Indicador] ORDER BY [FechaModif] DESC"></asp:SqlDataSource>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">
     <%:Scripts.Render("~/bundles/slider") %> 
    
    <script>
        Sys.Application.add_load(LoadHandler);

        function LoadHandler(sender, args0) {

            $(function () {
                var scores = [];

                for (var i = 0; i <= 100; i++) {
                    scores.push(i);
                }

                $("#TBIndicadorAzul").simpleSlider({
                    highlight: true,
                    allowedValues: scores
                });

                $("#TBIndicadorAzulP").simpleSlider({
                    highlight: true,
                    allowedValues: scores
                });
              
                $("#TBIndicadorVerde").simpleSlider({
                    highlight: true,
                    allowedValues: scores
                });

                $("#TBIndicadorVerdeP").simpleSlider({
                    highlight: true,
                    allowedValues: scores
                });

                $("#TBIndicadorAmarillo").simpleSlider({
                    highlight: true,
                    allowedValues: scores
                });
                $("#TBIndicadorAmarilloP").simpleSlider({
                    highlight: true,
                    allowedValues: scores
                });
                $("#TBIndicadorRojo").simpleSlider({
                    highlight: true,
                    allowedValues: scores
                });
                $("#TBIndicadorRojoP").simpleSlider({
                    highlight: true,
                    allowedValues: scores
                });


                //Area para actualizar los datos en caso de un postback

                $('#slider-value-azul').text($("#TBIndicadorAzul").val());
                $('#slider-value-azulp').text($("#TBIndicadorAzulP").val());
                $('#slider-value-verde').text($("#TBIndicadorVerde").val());
                $('#slider-value-verdep').text($("#TBIndicadorVerdeP").val());
                $('#slider-value-amarillo').text($("#TBIndicadorAmarillo").val());
                $('#slider-value-amarillop').text($("#TBIndicadorAmarilloP").val());
                $('#slider-value-rojo').text($("#TBIndicadorRojo").val());
                $('#slider-value-rojop').text($("#TBIndicadorRojoP").val());
                //fin del area de actualización
 
              
                $(".slider-azul").bind("slider:changed", function (event, data) {
                    $('#slider-value-azul').text(data.value);
                });

                $(".slider-azulp").bind("slider:changed", function (event, data) {
                    $('#slider-value-azulp').text(data.value);
                });


                $(".slider-verde").bind("slider:changed", function (event, data) {
                    $('#slider-value-verde').text(data.value);
                });


                $(".slider-verdep").bind("slider:changed", function (event, data) {
                    $('#slider-value-verdep').text(data.value);
                });

                $(".slider-amarillo").bind("slider:changed", function (event, data) {
                    $('#slider-value-amarillo').text(data.value);
                });


                $(".slider-amarillop").bind("slider:changed", function (event, data) {
                    $('#slider-value-amarillop').text(data.value);
                });

                $(".slider-rojo").bind("slider:changed", function (event, data) {
                    $('#slider-value-rojo').text(data.value);
                });


                $(".slider-rojop").bind("slider:changed", function (event, data) {
                    $('#slider-value-rojop').text(data.value);
                });

               
            });
        }
    </script>
</asp:Content>

