﻿
Imports Siget

Partial Class administrador_AgruparReactivos
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()
    End Sub


    Protected Sub GVindicadoresr_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVindicadores.RowCreated

        Dim cociente As Integer = (GVindicadores.PageCount / 15)
        Dim moduloIndex As Integer = (GVindicadores.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVindicadores.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVindicadores.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVindicadores.PageIndex
                final = IIf((inicial + 15 <= GVindicadores.PageCount), inicial + 15, GVindicadores.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVindicadores.PageCount, inicial + 15, GVindicadores.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVindicadores.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub


    Protected Sub GVindicadores_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVindicadores.RowDataBound


        Dim cociente As Integer = (GVindicadores.PageCount) / 15
        Dim moduloIndex As Integer = (GVindicadores.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVindicadores.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVindicadores.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVindicadores.PageIndex
            final = IIf((inicial + 15 <= GVindicadores.PageCount), inicial + 15, GVindicadores.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVindicadores.PageCount), inicial + 15, GVindicadores.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVindicadores, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVindicadores, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVindicadores_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVindicadores.SelectedIndexChanged

        TBIndicadorAmarillo.Text = Trim(GVindicadores.SelectedDataKey("MinAmarillo"))
        TBIndicadorAmarilloP.Text = Trim(GVindicadores.SelectedDataKey("MinAmarilloPorc"))
        TBIndicadorAzul.Text = Trim(GVindicadores.SelectedDataKey("MinAzul"))
        TBIndicadorAzulP.Text = Trim(GVindicadores.SelectedDataKey("MinAzulPorc"))
        TBIndicadorRojo.Text = Trim(GVindicadores.SelectedDataKey("MinRojo"))
        TBIndicadorRojoP.Text = Trim(GVindicadores.SelectedDataKey("MinRojoPorc"))
        TBIndicadorVerde.Text = Trim(GVindicadores.SelectedDataKey("MinVerde"))
        TBIndicadorVerdeP.Text = Trim(GVindicadores.SelectedDataKey("MinVerdePorc"))
        TBDescripcionIndicador.Text = Trim(GVindicadores.SelectedDataKey("Descripcion"))

    End Sub


    Protected Sub btnInsertarIndicador_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInsertarIndicador.Click
        Try
            SDSindicadores.InsertCommand = "SET dateformat dmy; INSERT INTO Indicador (Descripcion,MinAzul,MinVerde,MinAmarillo,MinRojo,MinAzulPorc,MinVerdePorc,MinAmarilloPorc,MinRojoPorc, FechaModif, Modifico) VALUES ('" +
                TBDescripcionIndicador.Text + "'," + TBIndicadorAzul.Text + "," + TBIndicadorVerde.Text + "," +
                TBIndicadorAmarillo.Text + "," + TBIndicadorRojo.Text + "," + TBIndicadorAzulP.Text + "," +
                TBIndicadorVerdeP.Text + "," + TBIndicadorAmarilloP.Text + "," + TBIndicadorRojoP.Text +
                ", getdate(), '" & User.Identity.Name & "')"
            SDSindicadores.Insert()

            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")), "El registro ha sido guardado")

            GVindicadores.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)

            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub btnEliminarIndicador_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarIndicador.Click
        Try
            SDSindicadores.DeleteCommand = "Delete from Indicador where IdIndicador = " + GVindicadores.SelectedDataKey("IdIndicador").ToString
            SDSindicadores.Delete()
            Utils.GeneralUtils.CleanAll(Me.Controls)

            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")), "El registro ha sido borrado")

            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)

            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub btnActualizarIndicador_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarIndicador.Click
        Try
            SDSindicadores.UpdateCommand = "SET dateformat dmy; UPDATE Indicador SET Descripcion = '" + TBDescripcionIndicador.Text + "', MinAzul='" + TBIndicadorAzul.Text + "', MinVerde='" + TBIndicadorVerde.Text + "',MinAmarillo='" + TBIndicadorAmarillo.Text + "',MinRojo='" + TBIndicadorRojo.Text + "',MinAzulPorc='" + TBIndicadorAzulP.Text + "',MinVerdePorc='" + TBIndicadorVerdeP.Text + "',MinAmarilloPorc='" + TBIndicadorAmarilloP.Text + "',MinRojoPorc='" + TBIndicadorRojoP.Text + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' WHERE IdIndicador = " + GVindicadores.SelectedDataKey("IdIndicador").ToString 'Me da el IdIndicador porque es el campo clave de la fila seleccionada
            SDSindicadores.Update()
            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")), "El registro ha sido actualizado")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)

            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

End Class
