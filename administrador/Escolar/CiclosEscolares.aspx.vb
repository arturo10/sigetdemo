﻿Imports Siget

Partial Class administrador_AgruparReactivos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

    End Sub

    Protected Sub GVciclosescolares_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVciclosescolares.SelectedIndexChanged
        'La utilidad HttpUtility.HtmlDecode() es necesaria para que convierta bien los caracteres especiales y no marque error
        btnEliminarCiclo.Visible = True
        btnActualizarCiclo.Visible = True
        DDLindicador.SelectedValue = GVciclosescolares.SelectedDataKey("IdIndicador")
        TBDescripcion.Text = HttpUtility.HtmlDecode(GVciclosescolares.SelectedDataKey("Descripcion"))
        HFFechaInicio.Value = CDate(GVciclosescolares.SelectedDataKey("FechaInicio")).ToString("MM/dd/yyyy")
        HFFechaFin.Value = CDate(GVciclosescolares.SelectedDataKey("FechaFin")).ToString("MM/dd/yyyy")
        DDLestatus.SelectedValue = Trim(GVciclosescolares.SelectedDataKey("Estatus"))
        'La liga seleccionar del grid ocupa el index 0
    End Sub

    Protected Sub btnCrearCicloEscolar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrearCicloEscolar.Click
        Try
            SDScicloescolar.InsertCommand = "SET dateformat dmy; INSERT INTO CicloEscolar(IdIndicador,Descripcion,FechaInicio,FechaFin,Estatus,FechaModif, Modifico) VALUES (" + DDLindicador.SelectedValue + ",'" + TBDescripcion.Text + "','" + CDate(HFFechaInicioParse.Value).ToString("dd/MM/yyyy") + "','" + CDate(HFFechaFinParse.Value).ToString("dd/MM/yyyy") + " 23:59:00','" + DDLestatus.SelectedValue + "', getdate(), '" & User.Identity.Name & "')"
            SDScicloescolar.Insert()
            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")), " El registro ha sido creado")
            GVciclosescolares.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVciclosescolares_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVciclosescolares.RowCreated

        Dim cociente As Integer = (GVciclosescolares.PageCount / 15)
        Dim moduloIndex As Integer = (GVciclosescolares.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVciclosescolares.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVciclosescolares.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVciclosescolares.PageIndex
                final = IIf((inicial + 15 <= GVciclosescolares.PageCount), inicial + 15, GVciclosescolares.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVciclosescolares.PageCount, inicial + 15, GVciclosescolares.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVciclosescolares.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVciclosescolares_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVciclosescolares.RowDataBound


        Dim cociente As Integer = (GVciclosescolares.PageCount) / 15
        Dim moduloIndex As Integer = (GVciclosescolares.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVciclosescolares.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVciclosescolares.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVciclosescolares.PageIndex
            final = IIf((inicial + 15 <= GVciclosescolares.PageCount), inicial + 15, GVciclosescolares.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVciclosescolares.PageCount), inicial + 15, GVciclosescolares.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVciclosescolares, "Select$" & e.Row.RowIndex))
            Dim s As String = Regex.Replace(HttpUtility.HtmlDecode(e.Row.Cells(3).Text), "<[^>]*(>|$)", String.Empty)
            Dim l As Integer = s.Length
            Dim continua As String = ""
            If l > 100 Then
                l = 100
                continua = "...(Continúa)"
            End If
            e.Row.Cells(3).Text = s.Substring(0, l) & continua
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVciclosescolares, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub btnActualizarCiclo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarCiclo.Click
        Try
            SDScicloescolar.UpdateCommand = "SET dateformat dmy; UPDATE CicloEscolar SET IdIndicador = " + DDLindicador.SelectedValue + ",Descripcion = '" + TBDescripcion.Text + "', FechaInicio = '" + CDate(HFFechaInicioParse.Value).ToString("dd/MM/yyyy") + "', FechaFin = '" + CDate(HFFechaFinParse.Value).ToString("dd/MM/yyyy") + " 23:59:00', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "', Estatus = '" + DDLestatus.SelectedValue + "' WHERE IdCicloEscolar = " + GVciclosescolares.SelectedDataKey("IdCicloEscolar").ToString
            SDScicloescolar.Update()
            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")), " El registro ha sido actualizado")
            GVciclosescolares.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub btnEliminarCiclo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarCiclo.Click
        Try
            SDScicloescolar.DeleteCommand = "Delete from CicloEscolar where IdCicloEscolar = " + GVciclosescolares.SelectedDataKey("IdCicloEscolar").ToString 'Me da el IdCicloEscolar porque es el campo clave de la fila seleccionada
            SDScicloescolar.Delete()
            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")), " El registro ha sido borrado")
            GVciclosescolares.DataBind()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVciclosescolares_DataBound(sender As Object, e As EventArgs)
        If GVciclosescolares.Rows.Count = 0 Then
            btnEliminarCiclo.Visible = False
            btnActualizarCiclo.Visible = False
            GVciclosescolares.SelectedIndex = -1
        End If
    End Sub
End Class
