﻿Imports Siget
Imports System.Data
Imports System.IO
Imports System.Data.OleDb 'Este es para MS Access
Imports System.Data.SqlClient
Imports Siget.Entity


Partial Class administrador_AgruparReactivos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()
       
        Dim strConexion As String

        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        SDSbuscar.ConnectionString = strConexion
        SDSbuscar.SelectCommand = "select Distinct P.IdProfesor, P.Clave, P.Nombre, P.Apellidos, Pl.Descripcion as Plantel, U.Login, U.Password, P.Estatus " + _
                    "from Profesor P, Plantel Pl, Usuario U " + _
                    "where U.IdUsuario = P.IdUsuario And Pl.IdPlantel = P.IdPlantel " + _
                    "And P.Clave like '%" + TBClaveBuscar.Text + "%' and P.Nombre like '%" + TBNombreBuscar.Text + "%' and P.Apellidos like '%" + _
                    TBApellidosBuscar.Text + "%' and U.Login like '%" + TBLoginBuscar.Text + "%' order by P.Apellidos, P.Nombre"
        GVProfesoresBuscar.DataBind()

    End Sub

    Protected Sub GVgrupos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVgrupos.RowDataBound



        Dim cociente As Integer = (GVgrupos.PageCount) / 15
        Dim moduloIndex As Integer = (GVgrupos.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVgrupos.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVgrupos.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVgrupos.PageIndex
            final = IIf((inicial + 15 <= GVgrupos.PageCount), inicial + 15, GVgrupos.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVgrupos.PageCount), inicial + 15, GVgrupos.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVgrupos, "Select$" & e.Row.RowIndex)
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVgrupos, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVProfesoresBuscar_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVProfesoresBuscar.RowDataBound

        Dim cociente As Integer = (GVProfesoresBuscar.PageCount) / 15
        Dim moduloIndex As Integer = (GVProfesoresBuscar.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVProfesoresBuscar.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVProfesoresBuscar.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVProfesoresBuscar.PageIndex
            final = IIf((inicial + 15 <= GVProfesoresBuscar.PageCount), inicial + 15, GVProfesoresBuscar.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVProfesoresBuscar.PageCount), inicial + 15, GVProfesoresBuscar.PageCount)
        End If



        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVProfesoresBuscar, "Select$" & e.Row.RowIndex)
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVProfesoresBuscar, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVprofesores_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVprofesores.RowDataBound

        Dim cociente As Integer = (GVprofesores.PageCount) / 15
        Dim moduloIndex As Integer = (GVprofesores.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVprofesores.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVprofesores.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVprofesores.PageIndex
            final = IIf((inicial + 15 <= GVprofesores.PageCount), inicial + 15, GVprofesores.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVprofesores.PageCount), inicial + 15, GVprofesores.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVprofesores, "Select$" & e.Row.RowIndex)
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVprofesores, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVProfesoresBuscar_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVProfesoresBuscar.RowCreated
        Dim cociente As Integer = (GVProfesoresBuscar.PageCount / 15)
        Dim moduloIndex As Integer = (GVProfesoresBuscar.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVProfesoresBuscar.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVProfesoresBuscar.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVProfesoresBuscar.PageIndex
                final = IIf((inicial + 10 <= GVProfesoresBuscar.PageCount), inicial + 15, GVProfesoresBuscar.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVProfesoresBuscar.PageCount, inicial + 15, GVProfesoresBuscar.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVProfesoresBuscar.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVprofesores_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVprofesores.RowCreated
        Dim cociente As Integer = (GVprofesores.PageCount / 15)
        Dim moduloIndex As Integer = (GVprofesores.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVprofesores.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVprofesores.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVprofesores.PageIndex
                final = IIf((inicial + 10 <= GVprofesores.PageCount), inicial + 15, GVprofesores.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVprofesores.PageCount, inicial + 15, GVprofesores.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVprofesores.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVgrupos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVgrupos.RowCreated
        Dim cociente As Integer = (GVgrupos.PageCount / 15)
        Dim moduloIndex As Integer = (GVgrupos.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVgrupos.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVgrupos.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVgrupos.PageIndex
                final = IIf((inicial + 10 <= GVgrupos.PageCount), inicial + 15, GVgrupos.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVgrupos.PageCount, inicial + 15, GVgrupos.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVgrupos.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVprofesores_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVprofesores.SelectedIndexChanged

        TBnombre.Text = HttpUtility.HtmlDecode(CStr(GVprofesores.SelectedDataKey("Nombre"))) 'IdLicencia
        TBapellidos.Text = HttpUtility.HtmlDecode(CStr(GVprofesores.SelectedDataKey("Apellidos"))) 'IdInstitucion
        DDLplantel.SelectedValue = GVprofesores.SelectedDataKey("IdPlantel").ToString
        TBlogin.Text = HttpUtility.HtmlDecode(CStr(GVprofesores.SelectedDataKey("Login")))
        TBpassword.Text = HttpUtility.HtmlDecode(CStr(GVprofesores.SelectedDataKey("Password")))
        TBemail.Text = HttpUtility.HtmlDecode(CStr(GVprofesores.SelectedDataKey("Email")))
        DDLestatus.SelectedValue = HttpUtility.HtmlDecode(CStr(GVprofesores.SelectedDataKey("Estatus")))
        TBlogin.Enabled = False
        EliminarProfesor.Visible = True
        ActualizarProfesor.Visible = True
        profesorSelected.Text = GVprofesores.SelectedDataKey("Nombre") + " " + GVprofesores.SelectedDataKey("Apellidos")

    End Sub

    Protected Sub Insertar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles InsertarProfesor.Click

        Dim repositoryProfesor As IRepository(Of Profesor) = New ProfesorRepository()
        Dim profesor As Profesor = New Profesor()
        Dim usuario As Usuario = New Usuario()
        Dim status As MembershipCreateStatus
        Dim newUser As MembershipUser
       
        Try
            If Trim(TBlogin.Text).Length > 0 Then

                With usuario
                    .Login = TBlogin.Text
                    .Password = TBpassword.Text
                    .PerfilASP = "Profesor"
                    .Email = IIf(TBemail.Text.Trim() <> String.Empty, TBemail.Text, String.Empty)
                    .FechaModif = DateTime.Now
                    .Modifico = User.Identity.Name
                End With
                With profesor
                    .Usuario = usuario
                    .IdPlantel = DDLplantel.SelectedValue
                    .Nombre = TBnombre.Text
                    .Apellidos = TBapellidos.Text
                    .FechaIngreso = DateTime.Now
                    .Email = IIf(TBemail.Text <> String.Empty, TBemail.Text, String.Empty)
                    .Estatus = DDLestatus.SelectedValue
                    .FechaModif = DateTime.Now
                    .Modifico = User.Identity.Name
                End With


                newUser = Membership.CreateUser(Trim(TBlogin.Text), Trim(TBpassword.Text), _
                                                                   Trim(TBemail.Text), Nothing, _
                                                                   Nothing, True, status)
                If newUser Is Nothing Then
                    msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), GetErrorMessage(status))
                Else
                    Roles.AddUserToRole(Trim(TBlogin.Text), "Profesor")
                    repositoryProfesor.Add(profesor)
                    CType(New MessageSuccess(GVprofesores, Me.GetType, "El profesor se ha creado exitosamente"), MessageSuccess).show()
                    GVprofesores.DataBind()
                End If
                
            Else
                CType(New MessageError(GVprofesores, Me.GetType, "Debe proporcionar un nombre de usuario (login) para " &
                    Config.Etiqueta.ARTDET_PROFESOR & " " & Config.Etiqueta.PROFESOR & "."), MessageError).show()
 
                TBlogin.Focus()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVprofesores, Me.GetType, "Verifique los campos"), MessageError).show()

        End Try
    End Sub

    Public Function GetErrorMessage(ByVal status As MembershipCreateStatus) As String
        Select Case status
            Case MembershipCreateStatus.DuplicateUserName
                Return "El usuario ya existe, elija otro usuario."
            Case MembershipCreateStatus.DuplicateEmail
                Return "Ya existe un usuario para ese email, por favor escriba un email diferente."
            Case MembershipCreateStatus.InvalidPassword
                Return "El password es inválido, pruebe con otro."
            Case MembershipCreateStatus.InvalidEmail
                Return "El formato del email es inválido, por favor revíselo."
            Case MembershipCreateStatus.InvalidAnswer
                Return "La respuesta secreta es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidQuestion
                Return "La pregunta de recuperación de password es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidUserName
                Return "El nombre de usuario es inválido, corríjalo."
            Case MembershipCreateStatus.ProviderError
                Return "La autentificación falló, contacte al administrador del sistema."
            Case MembershipCreateStatus.UserRejected
                Return "La creación del usuario ha sido cancelada, consulte al administrador."
            Case Else
                Return "Ha ocurrido un error desconocido, verifique o contacte al administrador."
        End Select
    End Function

    Protected Sub Eliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles EliminarProfesor.Click
        If Trim(GVprofesores.SelectedDataKey("Estatus")) <> "Activo" Then
            Try
                Dim repositoryProfesor As IRepository(Of Profesor) = New ProfesorRepository()
                Dim repositoryUsuario As IRepository(Of Usuario) = New UsuarioRepository()
                Dim profesor As Profesor = repositoryProfesor.FindById(GVprofesores.SelectedDataKey("IdProfesor"))
                Dim usuario As Usuario = repositoryUsuario.FindById(profesor.Usuario.IdUsuario)
                repositoryProfesor.Delete(profesor)
                repositoryUsuario.Delete(usuario)
                Membership.DeleteUser(GVprofesores.SelectedDataKey("Login"))
                GVprofesores.DataBind()
                CType(New MessageSuccess(GVprofesores, Me.GetType, "El registro se ha eliminado"), MessageSuccess).show()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                CType(New MessageError(GVprofesores, Me.GetType, "Verifique los campos"), MessageError).show()
            End Try
        Else
            CType(New MessageError(GVprofesores, Me.GetType, "El " & Config.Etiqueta.PROFESOR & " no puede ser eliminado si está con el estatus de Activo"), MessageError).show()
        End If
    End Sub

    Protected Sub Actualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ActualizarProfesor.Click

        Try
            Dim repositoryProfesor As IRepository(Of Profesor) = New ProfesorRepository()
            Dim profesor As Profesor = repositoryProfesor.FindById(GVprofesores.SelectedDataKey("IdProfesor"))
            Dim userUpdate As MembershipUser

            With profesor.Usuario
                .Password = TBpassword.Text
                .Email = IIf(TBemail.Text.Trim() <> String.Empty, TBemail.Text, String.Empty)
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
            End With
            With profesor
                .Nombre = TBnombre.Text
                .Apellidos = TBapellidos.Text()
                .Email = IIf(TBemail.Text <> String.Empty, TBemail.Text, String.Empty)
                .Estatus = DDLestatus.SelectedValue
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
            End With

            userUpdate = Membership.GetUser(Trim(HttpUtility.HtmlDecode(GVprofesores.SelectedDataKey("Login").ToString)))
            userUpdate.ChangePassword(userUpdate.ResetPassword(), Trim(TBpassword.Text))
            repositoryProfesor.Update(profesor)
            CType(New MessageSuccess(GVprofesores, Me.GetType, "El registro se ha actualizado"), MessageSuccess).show()
            GVprofesores.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVprofesores, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub Asignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Asignar.Click
        Try
            SDSprogramacion.InsertCommand = "SET dateformat dmy; INSERT INTO Programacion (IdAsignatura,IdProfesor,IdCicloEscolar,IdGrupo,FechaModif, Modifico) VALUES (" + DDLasignatura.SelectedValue + "," + GVprofesores.SelectedDataKey("IdProfesor").ToString + "," + DDLcicloescolar.SelectedValue + "," + DDLgrupo.SelectedValue + ",getdate(), '" & User.Identity.Name & "')"
            SDSprogramacion.Insert()
            GVgrupos.DataBind()
            CType(New MessageSuccess(GVprofesores, Me.GetType, "El registro  ha sido guardado"), MessageSuccess).show()

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVprofesores, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub Desasignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Desasignar.Click
        Try

            SDSprogramacion.DeleteCommand = "DELETE FROM Comunicacion WHERE IdProgramacion = " + GVgrupos.SelectedDataKey("IdProgramacion").ToString 'El campo IdProgramacion lo indico en la propiedad DataKeyNames del GridView
            SDSprogramacion.Delete()
            SDSprogramacion.DeleteCommand = "DELETE FROM ApoyoEvaluacion WHERE IdProgramacion = " + GVgrupos.SelectedDataKey("IdProgramacion").ToString 'El campo IdProgramacion lo indico en la propiedad DataKeyNames del GridView
            SDSprogramacion.Delete()
            SDSprogramacion.DeleteCommand = "DELETE FROM Programacion WHERE IdProgramacion = " + GVgrupos.SelectedDataKey("IdProgramacion").ToString 'El campo IdProgramacion lo indico en la propiedad DataKeyNames del GridView
            SDSprogramacion.Delete()
            GVgrupos.DataBind()
            CType(New MessageSuccess(GVprofesores, Me.GetType, "El registro se ha eliminado"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVprofesores, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub DDLniveles_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLniveles.DataBound
        DDLniveles.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Nivel"), 0))

        If DDLniveles.Items.Count = 2 Then
            DDLniveles.SelectedIndex = 1
            DDLgrados.DataBind()
        End If
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Asignatura"), 0))

        If DDLasignatura.Items.Count = 2 Then
            DDLasignatura.SelectedIndex = 1
            DDLplantelgrupo.DataBind()
        End If
    End Sub

    Protected Sub DDLinstitucion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLinstitucion.DataBound
        DDLinstitucion.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Institucion"), 0))

        If DDLinstitucion.Items.Count = 2 Then
            DDLinstitucion.SelectedIndex = 1
            DDLplantel.DataBind()
        End If
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Plantel"), 0))

        If DDLplantel.Items.Count = 2 Then
            DDLplantel.SelectedIndex = 1
            GVprofesores.DataBind()
        End If

    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Grupo"), 0))
        If DDLgrupo.Items.Count = 2 Then
            DDLgrupo.SelectedIndex = 1
            GVgrupos.DataBind()
        End If

    End Sub

    Protected Sub DDLgrados_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrados.DataBound
        DDLgrados.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Grado"), 0))

        If DDLgrados.Items.Count = 2 Then
            DDLgrados.SelectedIndex = 1
            DDLasignatura.DataBind()
        End If
    End Sub

    Protected Sub GVgrupos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVgrupos.DataBound
        If GVgrupos.Rows.Count > 0 Then
            Desasignar.Visible = True
        Else
            Desasignar.Visible = False
        End If
    End Sub

    Protected Sub DDLplantelgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantelgrupo.DataBound
        DDLplantelgrupo.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Plantel"), 0))
        If DDLplantelgrupo.Items.Count = 2 Then
            DDLplantelgrupo.SelectedIndex = 1
            DDLgrupo.DataBind()
        End If

    End Sub

    Protected Sub DDLplantel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.SelectedIndexChanged
        DDLplantelgrupo.SelectedIndex = DDLplantel.SelectedIndex

    End Sub



    Protected Sub BtnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBuscar.Click
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Try
            SDSbuscar.ConnectionString = strConexion
            SDSbuscar.SelectCommand = "select Distinct P.IdProfesor, P.Clave, P.Nombre, P.Apellidos, Pl.Descripcion as Plantel, U.Login, U.Password, P.Estatus " + _
                        "from Profesor P, Plantel Pl, Usuario U " + _
                        "where U.IdUsuario = P.IdUsuario And Pl.IdPlantel = P.IdPlantel " + _
                        "And P.Clave like '%" + TBClaveBuscar.Text + "%' and P.Nombre like '%" + TBNombreBuscar.Text + "%' and P.Apellidos like '%" + _
                        TBApellidosBuscar.Text + "%' and U.Login like '%" + TBLoginBuscar.Text + "%' order by P.Apellidos, P.Nombre"
            GVProfesoresBuscar.DataBind()
            If GVProfesoresBuscar.Rows.Count > 0 Then
                CType(New MessageSuccess(BtnBuscar, Me.GetType, "Se han encontrado " + CType(SDSbuscar.Select(DataSourceSelectArguments.Empty), DataView).Count().ToString() + " varios resultados"), MessageSuccess).show()
            Else
                CType(New MessageError(BtnBuscar, Me.GetType, "No se han encontrado resultados"), MessageError).show()
            End If

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(BtnBuscar, Me.GetType, "Verifique los campos"), MessageError).show()
            msgInfo.hide()
        End Try
    End Sub

    Protected Sub GVProfesoresBuscarSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVProfesoresBuscar.SelectedIndexChanged
        msgInfo.show("Seleccionado: " & HttpUtility.HtmlDecode(GVProfesoresBuscar.SelectedDataKey("Apellidos").ToString))
        TBpasswordOK.Text = HttpUtility.HtmlDecode(Trim(GVProfesoresBuscar.SelectedDataKey("Password").ToString))
    End Sub

    Protected Sub BtnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnActualizar.Click
        Try
            'Actualizo password en tabla de seguridad, no puedo cambiar Login
            Dim u As MembershipUser
            u = Membership.GetUser(Trim(HttpUtility.HtmlDecode(GVProfesoresBuscar.SelectedDataKey("Login").ToString)))
            u.ChangePassword(Trim(HttpUtility.HtmlDecode(GVProfesoresBuscar.SelectedDataKey("Password").ToString)), Trim(TBpasswordOK.Text))
            'u.Email = TBemail.Text
            'Membership.UpdateUser(u)
            SDSusuarios.UpdateCommand = "SET dateformat dmy; UPDATE Usuario set Password = '" + Trim(TBpasswordOK.Text) + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where Login = '" + Trim(HttpUtility.HtmlDecode(GVProfesoresBuscar.SelectedDataKey("Login").ToString)) + "'"
            SDSusuarios.Update()
            GVProfesoresBuscar.DataBind()
            CType(New MessageSuccess(GVprofesores, Me.GetType, "Se ha actualizado correctamente"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVprofesores, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub BtnGenerar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGenerar.Click
        Try
            'Ahora registro el usuario en la base de datos de seguridad
            Dim status As MembershipCreateStatus

            Dim newUser As MembershipUser = Membership.CreateUser(Trim(HttpUtility.HtmlDecode(GVProfesoresBuscar.SelectedRow.Cells(6).Text)), Trim(TBpasswordOK.Text), _
                                                           Trim(TBemailOK.Text), Nothing, _
                                                           Nothing, True, status)
            msgError2.hide() 'La pongo aquí para que no se limpie el error que capturo con la función
            If newUser Is Nothing Then
                msgError1.show("Error", GetErrorMessage(status))
            Else
                Roles.AddUserToRole(TBlogin.Text, "Profesor")
            End If
            SDSusuarios.UpdateCommand = "SET dateformat dmy; UPDATE Usuario set Password = '" + Trim(TBpasswordOK.Text) + "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where Login = '" + Trim(HttpUtility.HtmlDecode(GVProfesoresBuscar.SelectedRow.Cells(6).Text)) + "'"
            SDSusuarios.Update()
            GVProfesoresBuscar.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVprofesores, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
    End Sub

    Protected Sub CrearNuevoProfesor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CrearNuevoProfesor.Click
        Siget.Utils.GeneralUtils.CleanAll(CrearAdministrarProfesor.Controls)
        TBlogin.Enabled = True
        EliminarProfesor.Visible = False
        ActualizarProfesor.Visible = False
        GVprofesores.SelectedIndex = -1
    End Sub

    Protected Sub GVprofesores_DataBound(sender As Object, e As EventArgs)
        If GVprofesores.Rows.Count = 0 Then
            EliminarProfesor.Visible = False
            ActualizarProfesor.Visible = False
            GVprofesores.SelectedIndex = -1
        End If
    End Sub

    Protected Sub GVgrupos_PreRender(sender As Object, e As EventArgs) Handles GVgrupos.PreRender
        GVgrupos.Columns(1).HeaderText = Lang_Config.Translate("general", "Asignatura")
        GVgrupos.Columns(3).HeaderText = Lang_Config.Translate("general", "Plantel")
    End Sub
End Class
