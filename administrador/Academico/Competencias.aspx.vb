﻿
Imports Siget.Entity
Imports Siget

Partial Class administrador_Competencias
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()
        TBFechaModificacionCompetencia.Text = DateTime.Now
    End Sub

    Protected Sub GVCompetencias_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVCompetencias.RowCreated
        Dim cociente As Integer = (GVCompetencias.PageCount / 15)
        Dim moduloIndex As Integer = (GVCompetencias.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVCompetencias.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVCompetencias.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVCompetencias.PageIndex
                final = IIf((inicial + 15 <= GVCompetencias.PageCount), inicial + 15, GVCompetencias.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVCompetencias.PageCount, inicial + 15, GVCompetencias.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVCompetencias.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If
    End Sub

    Protected Sub GVAreas_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVAreas.RowCreated
        Dim cociente As Integer = (GVAreas.PageCount / 15)
        Dim moduloIndex As Integer = (GVAreas.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVAreas.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVAreas.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVAreas.PageIndex
                final = IIf((inicial + 15 <= GVAreas.PageCount), inicial + 15, GVAreas.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVAreas.PageCount, inicial + 15, GVAreas.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVAreas.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If
    End Sub

    Protected Sub GVasignaturas_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVasignaturas.RowCreated
        Dim cociente As Integer = (GVasignaturas.PageCount / 15)
        Dim moduloIndex As Integer = (GVasignaturas.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVasignaturas.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVasignaturas.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVasignaturas.PageIndex
                final = IIf((inicial + 15 <= GVasignaturas.PageCount), inicial + 15, GVasignaturas.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVasignaturas.PageCount, inicial + 15, GVasignaturas.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVasignaturas.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If
    End Sub

    Protected Sub GVCompetencias_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVCompetencias.RowDataBound


        Dim cociente As Integer = (GVCompetencias.PageCount) / 15
        Dim moduloIndex As Integer = (GVCompetencias.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVCompetencias.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVCompetencias.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVCompetencias.PageIndex
            final = IIf((inicial + 15 <= GVCompetencias.PageCount), inicial + 15, GVCompetencias.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVCompetencias.PageCount), inicial + 15, GVCompetencias.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVCompetencias, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVCompetencias, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVAreas_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVAreas.RowDataBound


        Dim cociente As Integer = (GVAreas.PageCount) / 15
        Dim moduloIndex As Integer = (GVAreas.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVAreas.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVAreas.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVAreas.PageIndex
            final = IIf((inicial + 15 <= GVAreas.PageCount), inicial + 15, GVAreas.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVAreas.PageCount), inicial + 15, GVAreas.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVAreas, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVAreas, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVasignaturass_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVasignaturas.RowDataBound


        Dim cociente As Integer = (GVasignaturas.PageCount) / 15
        Dim moduloIndex As Integer = (GVasignaturas.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVasignaturas.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVasignaturas.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVasignaturas.PageIndex
            final = IIf((inicial + 15 <= GVasignaturas.PageCount), inicial + 15, GVasignaturas.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVasignaturas.PageCount), inicial + 15, GVasignaturas.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVasignaturas, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVasignaturas, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVCompetencias_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVCompetencias.SelectedIndexChanged
        TBNombreCompetencia.Text = GVCompetencias.SelectedDataKey("Descripcion").ToString
        TBClasificacionCompetencia.Text = GVCompetencias.SelectedDataKey("Area").ToString
        TBFechaModificacionCompetencia.Text = GVCompetencias.SelectedDataKey("FechaModif").ToString

    End Sub

    Protected Sub GVAreas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVAreas.SelectedIndexChanged
        TBClaveArea.Text = GVAreas.SelectedDataKey("Clave").ToString
        TBDescripcionArea.Text = GVAreas.SelectedDataKey("Descripcion").ToString
        DDLEstatusArea.SelectedValue = GVAreas.SelectedDataKey("Estatus").ToString

    End Sub

    Protected Sub GVasignaturas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVasignaturas.SelectedIndexChanged

        TBNombreAsignatura.Text = HttpUtility.HtmlDecode(GVasignaturas.SelectedDataKey("Asignatura").ToString).Trim()
        DDLAreaAsignatura.SelectedValue = CInt(GVasignaturas.SelectedDataKey("IdArea").ToString)
        TBConsecutivoAsignatura.Text = HttpUtility.HtmlDecode(GVasignaturas.SelectedDataKey("Consecutivo").ToString)
        TBHorasAsignatura.Text = GVasignaturas.SelectedDataKey("Horas").ToString


        If Not IsDBNull(GVasignaturas.SelectedDataKey("IdIndicador").ToString) Then
            DDLIndicadorAsignatura.SelectedIndex = DDLIndicadorAsignatura.Items.IndexOf(DDLIndicadorAsignatura.Items.FindByValue(GVasignaturas.SelectedDataKey("IdIndicador").ToString))
        Else
            DDLIndicadorAsignatura.SelectedValue = 0
        End If

    End Sub

    Protected Sub ddlIndicador_DataBound(
                ByVal sender As Object,
                ByVal e As System.EventArgs) Handles DDLIndicadorAsignatura.DataBound
        ' añado un indicador inválido a la lista de opciones del usuario
        DDLIndicadorAsignatura.Items.Insert(0, New ListItem("---Ninguno", 0))
    End Sub

    Protected Sub btnEliminarCompetencias_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarCompetencias.Click
        Try
            Dim CompetenciasRepository As IRepository(Of Competencia) = New CompetenciasRepository()
            Dim competencia As Competencia = CompetenciasRepository.FindById(GVCompetencias.SelectedDataKey("IdCompetencia").ToString)
            CompetenciasRepository.Delete(competencia)
            CType(New MessageSuccess(btnEliminarCompetencias, Me.GetType, "El registro ha sido eliminado."), MessageSuccess).show()
            GVCompetencias.DataBind()
        Catch ex As Exception
            CType(New MessageError(btnEliminarCompetencias, Me.GetType, ex.Message.ToString()), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try

    End Sub

    Protected Sub btnEliminarArea_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarArea.Click
        Try
            Dim AreaRepository As IRepository(Of Area) = New AreaRepository()
            Dim area As Area = AreaRepository.FindById(GVAreas.SelectedDataKey("IdArea").ToString)
            AreaRepository.Delete(area)
            CType(New MessageSuccess(btnEliminarArea, Me.GetType, "El registro ha sido eliminado."), MessageSuccess).show()
            GVAreas.DataBind()
        Catch ex As Exception

            CType(New MessageError(btnEliminarArea, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try

    End Sub

    Protected Sub btnEliminarAsignatura_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarAsignatura.Click
        Try
            Dim AsignaturaRepository As IRepository(Of Asignatura) = New AsignaturaRepository()
            Dim asignatura As Asignatura = AsignaturaRepository.FindById(GVasignaturas.SelectedDataKey("IdAsignatura").ToString)
            AsignaturaRepository.Delete(asignatura)
            CType(New MessageSuccess(btnEliminarArea, Me.GetType, "El registro ha sido eliminado."), MessageSuccess).show()
            GVasignaturas.DataBind()
        Catch ex As Exception
            CType(New MessageError(btnEliminarArea, Me.GetType, ex.Message.ToString()), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try

    End Sub

    Protected Sub btnActualizarCompetencias_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarCompetencias.Click
        Try
            Dim CompetenciasRepository As IRepository(Of Competencia) = New CompetenciasRepository()
            Dim competencia As Competencia = CompetenciasRepository.FindById(GVCompetencias.SelectedDataKey("IdCompetencia").ToString)

            With competencia
                .Area = TBClasificacionCompetencia.Text
                .Descripcion = TBNombreCompetencia.Text
                .FechaModif = Date.Now.ToString
                .Modifico = User.Identity.Name

            End With
            TBFechaModificacionCompetencia.Text = competencia.FechaModif.ToString
            CompetenciasRepository.Update(competencia)
            CType(New MessageSuccess(btnEliminarArea, Me.GetType, "El registro ha sido actualizado."), MessageSuccess).show()
            GVCompetencias.DataBind()
        Catch ex As Exception
            CType(New MessageError(btnEliminarArea, Me.GetType, ex.Message.ToString()), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try


    End Sub

    Protected Sub btnActualizarArea_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarArea.Click
        Try
            Dim AreaRepository As IRepository(Of Area) = New AreaRepository()
            Dim area As Area = AreaRepository.FindById(GVAreas.SelectedDataKey("IdArea").ToString)

            With area
                .Clave = TBClaveArea.Text
                .Descripcion = TBDescripcionArea.Text
                .Estatus = DDLEstatusArea.SelectedValue
                .FechaModif = Date.Now.ToString
                .Modifico = User.Identity.Name
            End With

            AreaRepository.Update(area)
            CType(New MessageSuccess(btnActualizarArea, Me.GetType, "El registro ha sido actualizado."), MessageSuccess).show()
            GVAreas.DataBind()
        Catch ex As Exception
            CType(New MessageError(btnActualizarArea, Me.GetType, ex.Message.ToString()), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try


    End Sub

    Protected Sub btnActualizarAsignatura_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarAsignatura.Click
        Try
            Dim AsignaturaRepository As IRepository(Of Asignatura) = New AsignaturaRepository()
            Dim asignatura As Asignatura = AsignaturaRepository.FindById(GVasignaturas.SelectedDataKey("IdAsignatura").ToString)

            If Not DDLgradoAsignatura.SelectedValue = 0 Then
                asignatura.IdIndicador = CShort(DDLIndicadorAsignatura.SelectedValue)
            Else
                asignatura.IdIndicador = Nothing
            End If
            With asignatura
                .Descripcion = TBNombreAsignatura.Text
                .FechaModif = Date.Now.ToString
                .Modifico = User.Identity.Name
                .Horas = IIf(TBHorasAsignatura.Text <> "", CDbl(TBHorasAsignatura.Text), Nothing)
                .Consecutivo = TBConsecutivoAsignatura.Text
                .IdArea = DDLAreaAsignatura.SelectedValue.ToString
                .IdGrado = DDLgradoAsignatura.SelectedValue.ToString
                .IdIndicador = IIf(DDLIndicadorAsignatura.SelectedValue <> "0", CShort(DDLIndicadorAsignatura.SelectedValue), Nothing)
            End With

            AsignaturaRepository.Update(asignatura)
            GVasignaturas.DataBind()
            CType(New MessageSuccess(btnActualizarAsignatura, Me.GetType, "El registro ha sido actualizado."), MessageSuccess).show()
        Catch ex As Exception
            CType(New MessageError(btnActualizarAsignatura, Me.GetType, ex.Message.ToString()), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try


    End Sub

    Protected Sub btnCrearCompetencias_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrearCompetencias.Click

        Try
            Dim competencia As Competencia = New Competencia()
            Dim CompetenciasRepository As IRepository(Of Competencia) = New CompetenciasRepository()
            With competencia
                .Area = TBClasificacionCompetencia.Text
                .Descripcion = TBNombreCompetencia.Text
                .FechaModif = Date.Now.ToString
                .Modifico = User.Identity.Name
            End With
            CompetenciasRepository.Add(competencia)
            CType(New MessageSuccess(btnActualizarAsignatura, Me.GetType, "El registro ha sido creado."), MessageSuccess).show()
            GVCompetencias.DataBind()
        Catch ex As Exception
            CType(New MessageError(btnActualizarAsignatura, Me.GetType, ex.Message.ToString()), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try

    End Sub

    Protected Sub btnCrearArea_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrearArea.Click

        Try
            Dim area As Area = New Area()
            Dim AreaRepository As IRepository(Of Area) = New AreaRepository()
            With area
                .Clave = TBClaveArea.Text
                .Descripcion = TBDescripcionArea.Text
                .Estatus = DDLEstatusArea.SelectedValue
                .FechaModif = Date.Now.ToString
                .Modifico = User.Identity.Name
            End With
            AreaRepository.Add(area)
            CType(New MessageSuccess(btnCrearArea, Me.GetType, "El registro ha sido creado."), MessageSuccess).show()
            DDLAreaAsignatura.DataBind()
            GVAreas.DataBind()
        Catch ex As Exception
            CType(New MessageError(btnCrearArea, Me.GetType, ex.Message.ToString()), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try

    End Sub

    Protected Sub btnCrearAsignatura_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrearAsignatura.Click

        Try
            Dim asignatura As Asignatura = New Asignatura()
            Dim AsignaturaRepository As IRepository(Of Asignatura) = New AsignaturaRepository()
         

            With asignatura
                .Descripcion = TBNombreAsignatura.Text
                .FechaModif = Date.Now.ToString
                .Modifico = User.Identity.Name
                .Horas = IIf(Trim(TBHorasAsignatura.Text) = String.Empty, Nothing, CDbl(TBHorasAsignatura.Text))
                .Consecutivo = TBConsecutivoAsignatura.Text
                .IdArea = DDLAreaAsignatura.SelectedValue.ToString
                .IdGrado = DDLgradoAsignatura.SelectedValue.ToString
                .IdIndicador = IIf(DDLIndicadorAsignatura.SelectedValue <> "0", CShort(DDLIndicadorAsignatura.SelectedValue), Nothing)
            End With

            AsignaturaRepository.Add(asignatura)
            CType(New MessageSuccess(btnCrearAsignatura, Me.GetType, "El registro ha sido creado."), MessageSuccess).show()
            GVasignaturas.DataBind()
        Catch ex As Exception
            CType(New MessageError(btnCrearAsignatura, Me.GetType, ex.Message.ToString()), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try

    End Sub

    Protected Sub DDLNivelAsignatura_DataBound(sender As Object, e As EventArgs) Handles DDLNivelAsignatura.DataBound

        DDLNivelAsignatura.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Nivel"), 0))
    End Sub

    Protected Sub DDLgradoAsignatura_DataBound(sender As Object, e As EventArgs) Handles DDLgradoAsignatura.DataBound

        DDLgradoAsignatura.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Grado"), 0))
    End Sub

    Protected Sub DDLAreaAsignaturaDataBound(sender As Object, e As EventArgs) Handles DDLAreaAsignatura.DataBound

        DDLAreaAsignatura.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_AreaConocimiento"), 0))
    End Sub
End Class
