﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" 
    AutoEventWireup="false"  EnableEventValidation="false"
    CodeFile="Competencias.aspx.vb" Inherits="administrador_Competencias" %>

<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">


    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat radius-bordered">
            <div class="widget-header bg-themeprimary">
               <span class="widget-caption"><b>ÁREA DE COMPETENCIAS, ÁREAS Y <%=Lang_Config.Translate("general","Asignatura").ToUpper() %>S</b></span>
            </div>

            <div class="widget-body">
                <div class="widget-main ">
                    <div class="tabbable">
                        <ul class="nav nav-tabs tabs-flat" id="myTab11">
                            <li id="Competencias" class="active">
                                <a data-toggle="tab" href="#createCompetencias">Crear y administrar las competencias 
                                </a>
                            </li>
                            <li>
                                <a id="AreasOCiencias" data-toggle="tab" href="#areasCiencias">Áreas o Ciencias
                                </a>
                            </li>
                            <li>
                                <a id="Asignaturas" data-toggle="tab" href="#asignaturas"><%=Lang_Config.Translate("general", "Asignatura")%>
                                </a>
                            </li>

                        </ul>
                        <div class="tab-content tabs-flat">
                            <div id="createCompetencias" class="tab-pane in active">

                                <div class="form-horizontal">

                                    <div class="panel panel-default">

                                          <asp:UpdatePanel runat="server">
                                                <ContentTemplate>
                                        <div class="panel-body" id="competenciasForm">
                                          

                                            <div class="col-lg-6" >
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">
                                                        Nombre de la Competencia
                                                    </label>
                                                    <div class="col-lg-9">
                                                           <asp:TextBox runat="server"
                                                                ID="TBNombreCompetencia" CssClass="form-control">
                                                           </asp:TextBox>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-lg-3 control-label">
                                                        Clasificación de la Competencia
                                                    </label>
                                                    <div class="col-lg-9">
                                                           <asp:TextBox runat="server"
                                                                ID="TBClasificacionCompetencia" CssClass="form-control">
                                                           </asp:TextBox>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <label class="col-lg-3 control-label">
                                                        Fecha de Modificación
                                                    </label>
                                                    <div class="col-lg-9">
                                                           <asp:TextBox runat="server"
                                                                ID="TBFechaModificacionCompetencia" CssClass="form-control" disabled>
                                                           </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-lg-6">
                                                <div class="table-responsive">
                                                    <asp:GridView ID="GVCompetencias" runat="server"
                                                        AutoGenerateColumns="False"
                                                        AllowPaging="true"
                                                        DataSourceID="SDSCompetencias"
                                                        DataKeyNames="IdCompetencia,Descripcion,Area,FechaModif"
                                                        PageSize="5"
                                                        CellPadding="3"
                                                        CssClass="table table-striped table-bordered"
                                                        Height="17px"
                                                        Style="font-size: x-small; text-align: left;">
                                                        <Columns>
                                                            <asp:BoundField DataField="IdCompetencia" Visible="false" HeaderText="IdAlumno" />
                                                            <asp:BoundField DataField="Descripcion" HeaderText="Nombre de la Competencia" />
                                                            <asp:BoundField DataField="Area" HeaderText="Clasificación " />
                                                            <asp:BoundField DataField="FechaModif" HeaderText="Fecha modificación" />
                                                           
                                                        </Columns>
                                                        <PagerTemplate>
                                                            <ul runat="server" id="Pag" class="pagination">
                                                            </ul>
                                                        </PagerTemplate>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <SelectedRowStyle CssClass="row-selected" />
                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                    </asp:GridView>
                                                </div>
                                            </div>


                                            <div class="col-lg-12">
                                                <asp:LinkButton ID="btnCrearCompetencias" runat="server"  type="submit"
                                                    CssClass="btn btn-lg btn-labeled btn-palegreen space-margin">
                                                        <i class="btn-label fa fa-plus"></i>
                                                            Crear Competencia
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnActualizarCompetencias" runat="server"  type="submit"
                                                    CssClass="btn btn-lg btn-labeled btn-palegreen space-margin shiny">
                                                        <i class="btn-label fa fa-refresh"></i>
                                                            Actualizar Competencia
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="btnEliminarCompetencias" runat="server" 
                                                    CssClass="btn btn-lg btn-labeled btn-darkorange space-margin">
                                                        <i class="btn-label fa fa-remove"></i>
                                                            Eliminar Competencia
                                                </asp:LinkButton>
                                            </div>
                                                    <div class="col-lg-12">
                                                         <uc1:msgSuccess runat="server" ID="msgSuccess" />
                                                         <uc1:msgError runat="server" ID="msgError" />
                                                    </div>

                                               
                                        </div>
                                                       </ContentTemplate>
                                            </asp:UpdatePanel>
                                    </div>
                                </div>
                               
                            </div>
                             <div id="areasCiencias" class="tab-pane">

                                 <div class="form-horizontal">

                                     <div class="panel panel-default">

                                          <asp:UpdatePanel runat="server">
                                                 <ContentTemplate>
                                         <div class="panel-body" id="areasCienciasForm">
                                           
                                                     <div class="col-lg-6">
                                                         <div class="form-group">
                                                             <asp:Label CssClass="col-lg-2 control-label" runat="server">Descripción</asp:Label>
                                                             <div class="col-lg-10">
                                                                 <asp:TextBox CssClass="form-control" runat="server" ID="TBDescripcionArea">
                                                                 </asp:TextBox>
                                                             </div>
                                                         </div>

                                                         <div class="form-group">
                                                             <asp:Label CssClass="col-lg-2 control-label" runat="server">Clave</asp:Label>
                                                             <div class="col-lg-10">
                                                                 <asp:TextBox runat="server" ID="TBClaveArea" CssClass="form-control">
                                                                 </asp:TextBox>
                                                             </div>
                                                         </div>

                                                         <div class="form-group">
                                                             <asp:Label CssClass="col-lg-2 control-label" runat="server">
                                                                Estatus
                                                             </asp:Label>
                                                             <div class="col-lg-10">
                                                                 <asp:DropDownList ID="DDLEstatusArea" CssClass="form-control" runat="server">
                                                                     <asp:ListItem>Activo</asp:ListItem>
                                                                     <asp:ListItem>Suspendido</asp:ListItem>
                                                                     <asp:ListItem>Baja</asp:ListItem>
                                                                 </asp:DropDownList>
                                                             </div>
                                                         </div>

                                                     </div>

                                                       <div class="col-lg-6">
                                                            <div class="table-responsive">
                                                    <asp:GridView ID="GVAreas" runat="server"
                                                        AutoGenerateColumns="False"
                                                        AllowPaging="true"
                                                        DataSourceID="SDSAreas"
                                                        DataKeyNames="IdArea,Descripcion,Clave,Estatus"
                                                        PageSize="5"
                                                        CellPadding="3"
                                                        CssClass="table table-striped table-bordered"
                                                        Height="17px"
                                                        Style="font-size: x-small; text-align: left;">
                                                        <Columns>
                                                            <asp:BoundField DataField="IdArea" Visible="false" HeaderText="IdArea" />
                                                            <asp:BoundField DataField="Descripcion" HeaderText="Nombre del Área" />
                                                            <asp:BoundField DataField="Clave" HeaderText="Clave " />
                                                            <asp:BoundField DataField="Estatus" HeaderText= "Estatus" />
                                                        </Columns>
                                                        <PagerTemplate>
                                                            <ul runat="server" id="Pag" class="pagination">
                                                            </ul>
                                                        </PagerTemplate>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <SelectedRowStyle CssClass="row-selected" />
                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                    </asp:GridView>
                                                </div>
                                                     </div>

                                                     <div class="col-lg-12">
                                                         <asp:LinkButton ID="btnCrearArea" runat="server"  type="submit"
                                                             CssClass="btn btn-lg btn-labeled btn-palegreen space-margin">
                                                        <i class="btn-label fa fa-plus"></i>
                                                            Crear Area
                                                         </asp:LinkButton>
                                                         <asp:LinkButton ID="btnActualizarArea" runat="server" type="submit"
                                                             CssClass="btn btn-lg btn-labeled btn-palegreen space-margin shiny">
                                                        <i class="btn-label fa fa-refresh"></i>
                                                            Actualizar Area
                                                         </asp:LinkButton>
                                                         <asp:LinkButton ID="btnEliminarArea" runat="server"
                                                             CssClass="btn btn-lg btn-labeled btn-darkorange space-margin">
                                                        <i class="btn-label fa fa-remove"></i>
                                                            Eliminar Area
                                                         </asp:LinkButton>

                                                     </div>

                                                     <div class="col-lg-12">
                                                         <uc1:msgSuccess runat="server" ID="msgSuccess2" />
                                                         <uc1:msgError runat="server" ID="msgError2" />
                                                     </div>

                                         </div>

                                                 </ContentTemplate>
                                          </asp:UpdatePanel>
                                     </div>
                                 </div>
                             </div>


                            <div id="asignaturas" class="tab-pane">


                                <div class="form-horizontal">

                                    <div class="panel panel-default">

                                      

                                           <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                            <div class="panel-body" id="asignaturasForm">
                                           
                                                        <div class="col-lg-6">

                                                            <div class="form-group">
                                                                <asp:Label CssClass="col-lg-2 control-label" runat="server">
                                                                Nivel
                                                                </asp:Label>
                                                                <div class="col-lg-4">
                                                                    <asp:DropDownList runat="server" ID="DDLNivelAsignatura"
                                                                        CssClass="form-control" AutoPostBack="True" 
                                                                        DataSourceID="SDSniveles" DataTextField="Descripcion"
                                                                         DataValueField="IdNivel">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                 <asp:Label CssClass="col-lg-2 control-label" runat="server">
                                                                <%=Lang_Config.Translate("general", "Grado")%>
                                                                </asp:Label>
                                                                <div class="col-lg-4">
                                                                    <asp:DropDownList runat="server" 
                                                                          DataSourceID="SDSgrados" DataTextField="Descripcion"
                                                                        DataValueField="IdGrado"  AutoPostBack="true"
                                                                        ID="DDLgradoAsignatura" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <asp:Label CssClass="col-lg-2 control-label" runat="server">
                                                               <%= Lang_Config.Translate("general", "AreaConocimiento")%>
                                                                </asp:Label>
                                                                <div class="col-lg-4">
                                                                    <asp:DropDownList runat="server" ID="DDLAreaAsignatura" CssClass="form-control"
                                                                        AutoPostBack="True"
                                                                        DataSourceID="SDSarea" DataTextField="Nombre" DataValueField="IdArea">
                                                                    </asp:DropDownList>
                                                                </div>

                                                                 <asp:Label CssClass="col-lg-2 control-label" runat="server">
                                                                Nombre <%= Lang_Config.Translate("general", "Asignatura")%>
                                                                </asp:Label>
                                                                <div class="col-lg-4">
                                                                    <asp:TextBox runat="server" ID="TBNombreAsignatura" CssClass="form-control">
                                                                    </asp:TextBox>
                                                                </div>
                                                            </div>

                                                             <div class="form-group">
                                                             <asp:Label CssClass="col-lg-2 control-label" runat="server">
                                                                Consecutivo
                                                             </asp:Label>
                                                             <div class="col-lg-4">
                                                                 <asp:TextBox TextMode="Number" runat="server" ID="TBConsecutivoAsignatura" CssClass="form-control">
                                                                 </asp:TextBox>
                                                             </div>
                                                                 <asp:Label data-toggle="tooltip"
                                                                     data-placement="bottom" title="Especificar en horas" CssClass="col-lg-2 control-label" runat="server">
                                                                Duración (en días)
                                                             </asp:Label>
                                                             <div class="col-lg-4">
                                                                 <asp:TextBox min="0" runat="server" ID="TBHorasAsignatura" CssClass="form-control">
                                                                 </asp:TextBox>
                                                             </div>
                                                         </div>
                                                            <div class="form-group">
                                                                <asp:Label  data-toggle="tooltip"
                                                                     data-placement="bottom" title="Este campo es opcional" CssClass="col-lg-2 control-label" runat="server">Indicador</asp:Label>
                                                                <div class="col-lg-10">
                                                                    <asp:DropDownList runat="server" ID="DDLIndicadorAsignatura"
                                                                        DataSourceID="SDSIndicadores" DataTextField="Descripcion"
                                                                        DataValueField="IdIndicador" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                         </div>


                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="table-responsive">
                                                                <asp:GridView ID="GVasignaturas" runat="server"
                                                                    AllowPaging="True"
                                                                    AllowSorting="True"
                                                                    AutoGenerateColumns="False"
                                                                    DataKeyNames="IdAsignatura, IdIndicador,Horas,Asignatura,IdArea,Consecutivo,Area,Indicador"
                                                                    DataSourceID="SDSdatosasignaturas"
                                                                    PageSize="5"
                                                                    CellPadding="3"
                                                                    CssClass="table table-striped table-bordered"
                                                                    Height="17px"
                                                                    Style="font-size: x-small; text-align: left;">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Grado" HeaderText="Grado" SortExpression="Grado"
                                                                            Visible="False" />
                                                                        <asp:BoundField DataField="IdAsignatura" Visible="false" HeaderText="Id_Asignatura"
                                                                            InsertVisible="False" ReadOnly="True" SortExpression="IdAsignatura" />
                                                                        <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"
                                                                            SortExpression="Asignatura" />
                                                                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                                                                            SortExpression="Consecutivo" />
                                                                        <asp:BoundField DataField="IdArea" Visible="false" HeaderText="Id-Area"
                                                                            SortExpression="IdArea" />
                                                                        <asp:BoundField DataField="Area" HeaderText="Area de Conocimiento"
                                                                            SortExpression="Area" />
                                                                        <asp:BoundField DataField="Indicador" HeaderText="Indicador"
                                                                            SortExpression="Indicador" />
                                                                    </Columns>
                                                                    <PagerTemplate>
                                                                        <ul runat="server" id="Pag" class="pagination">
                                                                        </ul>
                                                                    </PagerTemplate>
                                                                    <PagerStyle HorizontalAlign="Center" />
                                                                    <SelectedRowStyle CssClass="row-selected" />
                                                                    <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </div>

                                                        </div>

                                                        <div class="col-lg-12">
                                                             <asp:LinkButton ID="btnCrearAsignatura"    runat="server"  type="submit"
                                                             CssClass=" btn btn-lg btn-labeled btn-palegreen space-margin">
                                                        <i class="btn-label fa fa-plus"></i>
                                                            <%=Lang_Config.Translate("asignaturas","Insertar_asignatura") %>
                                                         </asp:LinkButton>
                                                         <asp:LinkButton ID="btnActualizarAsignatura" runat="server"  type="submit"
                                                             CssClass="btn btn-lg btn-labeled btn-palegreen space-margin shiny">
                                                        <i class="btn-label fa fa-refresh"></i>
                                                                 <%=Lang_Config.Translate("asignaturas","Actualizar_asignatura") %>
                                                         </asp:LinkButton>
                                                         <asp:LinkButton ID="btnEliminarAsignatura" runat="server"
                                                             CssClass="btn btn-lg btn-labeled btn-darkorange space-margin">
                                                        <i class="btn-label fa fa-remove"></i>
                                                                <%=Lang_Config.Translate("asignaturas", "Eliminar_asignatura")%>
                                                         </asp:LinkButton>

                                                        </div>
                                                <div class="col-lg-12">
                                                    <uc1:msgSuccess runat="server" ID="msgSuccess3" />
                                                    <uc1:msgError runat="server" ID="msgError3" />
                                                </div>

                                            </div>
                                                    </ContentTemplate>
                                           </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:SqlDataSource ID="SDSCompetencias" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Competencia]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SDSAreas" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Area]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSdatosasignaturas" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT A.Horas,A.IdAsignatura, A.IdGrado, G.Descripcion Grado, A.Descripcion Asignatura, A.Consecutivo, A.IdArea, Ar.Descripcion Area, A.IdIndicador, I.Descripcion 'Indicador'
FROM Grado G, Area Ar, Asignatura A LEFT JOIN Indicador I ON A.IdIndicador = I.IdIndicador
WHERE (A.IdGrado = @IdGrado) and (G.IdGrado = A.IdGrado)
and (Ar.IdArea = A.IdArea) 
order by G.Descripcion, A.Descripcion">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLgradoAsignatura" Name="IdGrado"
                
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
    
                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivelAsignatura" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
    <asp:SqlDataSource ID="SDSniveles" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SDSarea" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdArea, Descripcion + ' (' + ISNULL(Clave, '') + ')' as Nombre
from Area
where Estatus = 'Activo'
order by Descripcion"></asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSIndicadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdIndicador], [Descripcion] FROM [Indicador] ORDER BY [Descripcion]"></asp:SqlDataSource>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">
   
</asp:Content>

