﻿
Imports Siget
Imports Siget.Entity
Imports System.IO
Imports System.Data.SqlClient
Imports System.Data


Partial Class administrador_AgruparReactivos
    Inherits System.Web.UI.Page

#Region "LOAD"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()
    End Sub
#End Region

#Region "UPLOAD COMPLETE"
    Protected Sub FileUploadComplete(ByVal sender As Object, ByVal e As EventArgs) Handles FileUploadSubtema.UploadedComplete
        Beans.FileReference.Files.Insert(0, FileUploadSubtema.PostedFile)
        Beans.FileReference.nameOfFiles.Insert(0, FileUploadSubtema.FileName)
        Beans.FileReference.Activator.Insert(0, True)
    End Sub

#End Region

#Region "ROW_CREATED"

    Protected Sub GVtemas_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVtemas.RowCreated
        Dim cociente As Integer = (GVtemas.PageCount / 15)
        Dim moduloIndex As Integer = (GVtemas.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVtemas.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVtemas.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVtemas.PageIndex
                final = IIf((inicial + 15 <= GVtemas.PageCount), inicial + 15, GVtemas.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVtemas.PageCount, inicial + 15, GVtemas.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVtemas.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If
    End Sub

    Protected Sub GVsubtemas_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVsubtemas.RowCreated
        Dim cociente As Integer = (GVsubtemas.PageCount / 15)
        Dim moduloIndex As Integer = (GVsubtemas.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVsubtemas.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVsubtemas.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVtemas.PageIndex
                final = IIf((inicial + 15 <= GVsubtemas.PageCount), inicial + 15, GVsubtemas.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVsubtemas.PageCount, inicial + 15, GVsubtemas.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVsubtemas.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If
    End Sub

    Protected Sub GVApoyoSubtema_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVApoyoSubtema.RowCreated
        Dim cociente As Integer = (GVApoyoSubtema.PageCount / 15)
        Dim moduloIndex As Integer = (GVApoyoSubtema.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVApoyoSubtema.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVApoyoSubtema.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVApoyoSubtema.PageIndex
                final = IIf((inicial + 15 <= GVApoyoSubtema.PageCount), inicial + 15, GVApoyoSubtema.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVApoyoSubtema.PageCount, inicial + 15, GVApoyoSubtema.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVApoyoSubtema.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If
    End Sub


#End Region

#Region "ROW_DATABOUND"
    Protected Sub GVtemas_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVtemas.RowDataBound

        Dim cociente As Integer = (GVtemas.PageCount) / 15
        Dim moduloIndex As Integer = (GVtemas.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVtemas.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVtemas.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVtemas.PageIndex
            final = IIf((inicial + 15 <= GVtemas.PageCount), inicial + 15, GVtemas.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVtemas.PageCount), inicial + 15, GVtemas.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVtemas, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVtemas, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVApoyoSubtema_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVApoyoSubtema.RowDataBound

        Dim cociente As Integer = (GVApoyoSubtema.PageCount) / 15
        Dim moduloIndex As Integer = (GVApoyoSubtema.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVApoyoSubtema.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVApoyoSubtema.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVApoyoSubtema.PageIndex
            final = IIf((inicial + 15 <= GVApoyoSubtema.PageCount), inicial + 15, GVApoyoSubtema.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVApoyoSubtema.PageCount), inicial + 15, GVApoyoSubtema.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVApoyoSubtema, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVApoyoSubtema, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVsubtemas_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVsubtemas.RowDataBound


        Dim cociente As Integer = (GVsubtemas.PageCount) / 15
        Dim moduloIndex As Integer = (GVsubtemas.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVsubtemas.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVsubtemas.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVsubtemas.PageIndex
            final = IIf((inicial + 15 <= GVsubtemas.PageCount), inicial + 15, GVsubtemas.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVsubtemas.PageCount), inicial + 15, GVsubtemas.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVsubtemas, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVsubtemas, "Page$" & counter.ToString)
            Next
        End If
    End Sub
#End Region

#Region "SELECTED_INDEX_CHANGED"
    Protected Sub GVtemas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVtemas.SelectedIndexChanged
        Siget.Utils.GeneralUtils.CleanAll(ApoyoSubtemaPanel.Controls)
        btnActualizarTema.Visible = True
        btnEliminarTema.Visible = True
        DDLAsignaturaTemas.SelectedValue = GVtemas.SelectedDataKey("IdAsignatura").ToString
        TBNoTema.Text = GVtemas.SelectedDataKey("Numero").ToString
        TBDescripcionTemas.Text = GVtemas.SelectedDataKey("NomTema").ToString
        TemaSelected.Text = GVtemas.SelectedDataKey("NomTema")
    End Sub

    Protected Sub GVApoyoSubtema_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVApoyoSubtema.SelectedIndexChanged
        btnEliminarApoyoSubtema.Visible = True
        TBConsecutivoApoyoSub.Text = IIf(Not IsDBNull(GVApoyoSubtema.SelectedDataKey("Consecutivo")), GVApoyoSubtema.SelectedDataKey("Consecutivo"), String.Empty)
        DDLTipoArchivoApoyoSub.SelectedValue = GVApoyoSubtema.SelectedDataKey("TipoArchivoApoyo")

        If Not IsDBNull(GVApoyoSubtema.SelectedDataKey("Descripcion")) Then
            TBDescripcionApoyoSub.Text = GVApoyoSubtema.SelectedDataKey("Descripcion")
        End If
        If GVApoyoSubtema.SelectedDataKey("TipoArchivoApoyo") = "Link" Or GVApoyoSubtema.SelectedDataKey("TipoArchivoApoyo") = "YouTube" Then
            FileUploadSubtema.Visible = False
            msgInfo.show("Revise", "Si el enlace es a una página fuera del sistema, el link debe incluir dos diagonales al inicio (//). <br />Por ejemplo: //www.google.com.mx")
            TBlink.Visible = True
            TBlink.Text = HttpUtility.HtmlDecode(GVApoyoSubtema.SelectedDataKey("ArchivoApoyo"))
            HLArchivoCargadoSubtemas.Text = HttpUtility.HtmlDecode(GVApoyoSubtema.SelectedDataKey("ArchivoApoyo").ToString.Replace("\\", "\"))
            HLArchivoCargadoSubtemas.NavigateUrl = HttpUtility.HtmlDecode(GVApoyoSubtema.SelectedDataKey("ArchivoApoyo").ToString.Replace("\\", "\"))
        Else
            FileUploadSubtema.Visible = True
            TBlink.Visible = False
            HLArchivoCargadoSubtemas.NavigateUrl = Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(GVApoyoSubtema.SelectedDataKey("ArchivoApoyo")))
            HLArchivoCargadoSubtemas.Text = HttpUtility.HtmlDecode(GVApoyoSubtema.SelectedDataKey("ArchivoApoyo"))

            msgInfo.hide()
        End If


    End Sub

    Protected Sub GVsubtemas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVsubtemas.SelectedIndexChanged
        Siget.Utils.GeneralUtils.CleanAll(ApoyoSubtemaPanel.Controls)
        btnActualizarSubtema.Visible = True
        btnEliminarSubtema.Visible = True
        btnApoyosSubtemas.Visible = True
        TBNumeroSubtema.Text = GVsubtemas.SelectedDataKey("Numero").ToString
        DDLCompetenciaSubtema.SelectedValue = GVsubtemas.SelectedDataKey("IdCompetencia").ToString
        TBDescripcionSubtema.Text = GVsubtemas.SelectedDataKey("Subtema").ToString
    End Sub

    Protected Sub DDLTipoArchivoApoyoSub_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DDLTipoArchivoApoyoSub.SelectedIndexChanged
        If DDLTipoArchivoApoyoSub.SelectedValue = "Link" Or DDLTipoArchivoApoyoSub.SelectedValue = "YouTube" Then
            FileUploadSubtema.Visible = False
            msgInfo.show("Revise", "Si el enlace es a una página fuera del sistema, el link debe incluir dos diagonales al inicio (//). <br />Por ejemplo: //www.google.com.mx")
            TBlink.Visible = True
            HLArchivoCargadoSubtemas.Text = String.Empty
            HLArchivoCargadoSubtemas.NavigateUrl = String.Empty
        Else
            FileUploadSubtema.Visible = True
            TBlink.Visible = False
            msgInfo.hide()
        End If

    End Sub
#End Region

#Region "DELETE BUTTONS"
    Protected Sub btnEliminarTema_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarTema.Click
        Try
            Dim temaRepository As IRepository(Of Tema) = New TemaRepository()
            Dim tema As Tema = temaRepository.FindById(GVtemas.SelectedDataKey("IdTema").ToString)
            temaRepository.Delete(tema)
            CType(New MessageSuccess(GVtemas, Me.GetType, "El registro ha sido eliminado"), MessageSuccess).show()
            GVtemas.DataBind()
        Catch ex As Exception
            CType(New MessageError(GVtemas, Me.GetType, "Por favor revisa de nuevo los campos que has llenado"), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try

    End Sub

    Protected Sub btnEliminarSubtema_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarSubtema.Click
        Try
            Dim subtemaRepository As IRepository(Of Subtema) = New SubtemaRepository()
            Dim subtema As Subtema = subtemaRepository.FindById(GVsubtemas.SelectedDataKey("IdSubtema").ToString)
            subtemaRepository.Delete(subtema)
            CType(New MessageSuccess(GVtemas, Me.GetType, "El registro ha sido eliminado"), MessageSuccess).show()
            GVsubtemas.DataBind()
        Catch ex As Exception
            CType(New MessageError(GVtemas, Me.GetType, "Por favor revisa de nuevo los campos que has llenado"), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try

    End Sub

    Protected Sub btnEliminarApoyoSubtema_Click(sender As Object, e As EventArgs)

        Dim flag As Boolean = False
        Try

            If Utils.FileUtils.isThisFilesWithMultipleAssociations(GVApoyoSubtema.SelectedDataKey("ArchivoApoyo")) Then
                CType(New MessageInfo(btnEliminarApoyoSubtema, Me.GetType(), "El archivo fisico no se ha borrado pero si el registro. Este recurso está asignado a otros subtemas/opciones/planteamientos. "), MessageInfo).show()
                flag = True
            Else
                File.Delete(Siget.Config.Global.rutaMaterial & GVApoyoSubtema.SelectedDataKey("ArchivoApoyo"))
            End If

            SDSDatosApoyosSubtemas.DeleteCommand = "DELETE FROM ApoyoSubtema where IdAPoyoSubtema=" + GVApoyoSubtema.SelectedDataKey("IdApoyoSubtema").ToString
            SDSDatosApoyosSubtemas.Delete()
            Siget.Utils.GeneralUtils.CleanAll(ApoyoSubtemaPanel.Controls)
            Siget.Utils.Library.recalculateStructure()

            If Not flag Then
                CType(New MessageSuccess(GVtemas, Me.GetType, "El registro ha sido eliminado junto con su archivo de apoyo"), MessageSuccess).show()
            End If

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVtemas, Me.GetType, "El registro no se ha eliminado"), MessageError).show()

        End Try

    End Sub
#End Region

#Region "UPDATE BUTTONS"
    Protected Sub btnActualizarTema_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarTema.Click
        Try
            Dim temaRepository As IRepository(Of Tema) = New TemaRepository()
            Dim tema As Tema = temaRepository.FindById(GVtemas.SelectedDataKey("IdTema").ToString)

            With tema
                .IdAsignatura = DDLAsignaturaTemas.SelectedValue
                .Descripcion = TBDescripcionTemas.Text
                .Numero = TBNoTema.Text
                .FechaModif = Date.Now.ToString
                .Modifico = User.Identity.Name
            End With
            temaRepository.Update(tema)
            CType(New MessageSuccess(GVtemas, Me.GetType, "El registro ha sido actualizado"), MessageSuccess).show()
            GVtemas.DataBind()
        Catch ex As Exception
            CType(New MessageError(GVtemas, Me.GetType, "Por favor revisa de nuevo los campos que has llenado"), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try


    End Sub

    Protected Sub btnActualizarSubtema_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarSubtema.Click
        Try
            Dim subtemaRepository As IRepository(Of Subtema) = New SubtemaRepository()
            Dim subtema As Subtema = subtemaRepository.FindById(GVsubtemas.SelectedDataKey("IdSubtema").ToString)

            With subtema
                .IdCompetencia = DDLCompetenciaSubtema.SelectedValue
                .Descripcion = TBDescripcionSubtema.Text
                .Numero = TBNumeroSubtema.Text
                .FechaModif = Date.Now.ToString
                .Modifico = User.Identity.Name
            End With
            subtemaRepository.Update(subtema)
            CType(New MessageSuccess(GVtemas, Me.GetType, "El registro ha sido actualizado"), MessageSuccess).show()
            GVsubtemas.DataBind()
        Catch ex As Exception
            CType(New MessageError(GVtemas, Me.GetType, "Por favor revisa de nuevo los campos que has llenado"), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try


    End Sub

    Protected Sub btnActualizarApoyoSub_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarApoyoSub.Click
        Dim archivo As HttpPostedFile
        Dim nombreArchivo As String
        Dim ruta As String = Config.Global.rutaMaterial
        Try
            
            SDSDatosApoyosSubtemas.UpdateCommand = "UPDATE ApoyoSubtema SET Consecutivo=@Consecutivo,Descripcion=@Descripcion,ArchivoApoyo=@ArchivoApoyo,TipoArchivoApoyo=@Tipo, Modifico=@Modifico WHERE IdApoyoSubtema=@IdApoyoSubtema"
            SDSDatosApoyosSubtemas.UpdateParameters.Add("Consecutivo", TBConsecutivoApoyoSub.Text)
            SDSDatosApoyosSubtemas.UpdateParameters.Add("Descripcion", TBDescripcionApoyoSub.Text)
            SDSDatosApoyosSubtemas.UpdateParameters.Add("Tipo", DDLTipoArchivoApoyoSub.SelectedValue)
            SDSDatosApoyosSubtemas.UpdateParameters.Add("Modifico", User.Identity.Name.ToString())
            SDSDatosApoyosSubtemas.UpdateParameters.Add("IdApoyoSubtema", GVApoyoSubtema.SelectedDataKey("IdApoyoSubtema"))

            If (Beans.FileReference.Activator(0)) Then
                archivo = Beans.FileReference.Files(0)
                nombreArchivo = Beans.FileReference.nameOfFiles(0)

                If File.Exists(Config.Global.urlMaterial & GVApoyoSubtema.SelectedDataKey("ArchivoApoyo")) Then

                    Dim Total = 0
                    Dim strConexion As String
                    strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                    Dim objConexion As New SqlConnection(strConexion)
                    Dim miComando As SqlCommand
                    Dim misRegistros As SqlDataReader
                    miComando = objConexion.CreateCommand
                    miComando.CommandText = "select Count(*) as Total from ApoyoSubtema where ArchivoApoyo = '" + nombreArchivo + "'"
                    objConexion.Open()
                    misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                    misRegistros.Read()
                    Total = misRegistros.Item("Total")

                    misRegistros.Close()
                    miComando.CommandText = "select Count(*) as Total from Opcion where ArchivoApoyo = '" + nombreArchivo + "'"
                    misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                    misRegistros.Read()
                    Total = Total + misRegistros.Item("Total")

                    misRegistros.Close()
                    miComando.CommandText = "select Count(*) as Total from Planteamiento where ArchivoApoyo = '" + nombreArchivo + "' or ArchivoApoyo2 = '" + nombreArchivo + "'"
                    misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                    misRegistros.Read()
                    Total = Total + misRegistros.Item("Total")
                    misRegistros.Close()
                    objConexion.Close()

                    If Not Total = 0 Then
                        msgWarning3.show("Cuidado", "Este recurso está asignado a otros subtemas/opciones/planteamientos.")
                    End If

                    HLArchivoCargadoSubtemas.Text = nombreArchivo
                    HLArchivoCargadoSubtemas.NavigateUrl = Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(nombreArchivo))
                    SDSDatosApoyosSubtemas.UpdateParameters.Add("ArchivoApoyo", GVApoyoSubtema.SelectedDataKey("ArchivoApoyo"))
                Else
                    archivo.SaveAs(ruta & nombreArchivo)
                    Beans.FileReference.Activator(0) = False
                    HLArchivoCargadoSubtemas.Visible = True
                    HLArchivoCargadoSubtemas.Text = "Archivo cargado: " & Trim(HttpUtility.HtmlDecode(nombreArchivo))
                    HLArchivoCargadoSubtemas.NavigateUrl = Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(nombreArchivo))
                    SDSDatosApoyosSubtemas.UpdateParameters.Add("ArchivoApoyo", nombreArchivo)
                End If
            ElseIf Trim(DDLTipoArchivoApoyoSub.SelectedValue) = "Link" Or Trim(DDLTipoArchivoApoyoSub.SelectedValue) = "Youtube" Then
                SDSDatosApoyosSubtemas.UpdateParameters.Add("ArchivoApoyo", TBlink.Text)
                HLArchivoCargadoSubtemas.Text = HttpUtility.HtmlDecode(TBlink.Text.Replace("\\", "\"))
                HLArchivoCargadoSubtemas.NavigateUrl = HttpUtility.HtmlDecode(TBlink.Text.Replace("\\", "\"))
            Else
                SDSDatosApoyosSubtemas.UpdateParameters.Add("ArchivoApoyo", GVApoyoSubtema.SelectedDataKey("ArchivoApoyo"))
            End If
            SDSDatosApoyosSubtemas.Update()
            GVApoyoSubtema.DataBind()
            Siget.Utils.Library.recalculateStructure()
            CType(New MessageSuccess(btnActualizarApoyoSub, Me.GetType, "El registro se ha actualizado correctamente"), MessageSuccess).show()
        Catch ex As Exception
            CType(New MessageError(btnActualizarApoyoSub, Me.GetType, "Por favor revisa de nuevo los campos que has llenado"), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try
    End Sub
#End Region

#Region "INSERTIONS BUTTONS"
    Protected Sub btnCrearTema_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrearTema.Click
        Try
            Dim tema As Tema = New Tema()
            Dim temaRepository As IRepository(Of Tema) = New TemaRepository()
            With tema
                .IdAsignatura = DDLAsignaturaTemas.SelectedValue
                .Descripcion = TBDescripcionTemas.Text
                .Numero = TBNoTema.Text
                .FechaModif = Date.Now.ToString
                .Modifico = User.Identity.Name
            End With
            temaRepository.Add(tema)
            CType(New MessageSuccess(GVtemas, Me.GetType, "El registro ha sido creado"), MessageSuccess).show()
            GVtemas.DataBind()
        Catch ex As Exception
            CType(New MessageError(GVtemas, Me.GetType, "Por favor revisa de nuevo los campos que has llenado"), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try

    End Sub

    Protected Sub btnCrearSubtema_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrearSubtema.Click
        Try
            Dim subtema As Subtema = New Subtema()
            Dim subtemaRepository As IRepository(Of Subtema) = New SubtemaRepository()
            With subtema
                .IdTema = GVtemas.SelectedDataKey("IdTema").ToString
                .IdCompetencia = DDLCompetenciaSubtema.SelectedValue
                .Descripcion = TBDescripcionSubtema.Text
                .Numero = TBNumeroSubtema.Text
                .FechaModif = Date.Now.ToString
                .Modifico = User.Identity.Name
            End With
            subtemaRepository.Add(subtema)
            CType(New MessageSuccess(GVsubtemas, Me.GetType, "El registro ha sido creado"), MessageSuccess).show()
            GVsubtemas.DataBind()
        Catch ex As Exception
            CType(New MessageError(GVsubtemas, Me.GetType, "Por favor revisa de nuevo los campos que has llenado"), MessageError).show()
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try

    End Sub

    Protected Sub btnCargarApoyoSub_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCargarApoyoSub.Click

        Dim archivo As HttpPostedFile
        Dim nombreArchivo As String


        Try
            If Beans.FileReference.Activator.Count > 0 Then
                SDSmaterialapoyo.InsertParameters.Clear()
                SDSmaterialapoyo.InsertCommand = "SET dateformat dmy; INSERT INTO ApoyoSubtema (IdSubTema,Consecutivo, Descripcion,ArchivoApoyo,TipoArchivoApoyo, FechaModif, Modifico) VALUES (@IdSubTema,@Consecutivo,@Descripcion,@ArchivoApoyo,@TipoArchivoApoyo, getdate(), @Modifico)"
                SDSmaterialapoyo.InsertParameters.Add("IdSubTema", GVsubtemas.SelectedValue.ToString)
                SDSmaterialapoyo.InsertParameters.Add("Consecutivo", TBConsecutivoApoyoSub.Text)
                SDSmaterialapoyo.InsertParameters.Add("Descripcion", TBDescripcionApoyoSub.Text.Trim())
                SDSmaterialapoyo.InsertParameters.Add("TipoArchivoApoyo", DDLTipoArchivoApoyoSub.SelectedValue)
                SDSmaterialapoyo.InsertParameters.Add("Modifico", User.Identity.Name.Trim())

                If (Beans.FileReference.Activator(0)) Then
                    archivo = Beans.FileReference.Files(0)
                    nombreArchivo = Beans.FileReference.nameOfFiles(0)

                    If DDLTipoArchivoApoyoSub.SelectedValue <> "Link" AndAlso DDLTipoArchivoApoyoSub.SelectedValue <> "YouTube" Then
                        Dim ruta As String
                        ruta = Config.Global.rutaMaterial

                        Dim MiArchivo2 As FileInfo = New FileInfo(ruta & nombreArchivo)

                        If MiArchivo2.Exists Then
                            Dim Total = 0
                            Dim strConexion As String
                            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                            Dim objConexion As New SqlConnection(strConexion)
                            Dim miComando As SqlCommand
                            Dim misRegistros As SqlDataReader
                            miComando = objConexion.CreateCommand
                            miComando.CommandText = "select Count(*) as Total from ApoyoSubtema where ArchivoApoyo = '" + nombreArchivo + "'"
                            objConexion.Open()
                            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                            misRegistros.Read()
                            Total = misRegistros.Item("Total")

                            misRegistros.Close()
                            miComando.CommandText = "select Count(*) as Total from Opcion where ArchivoApoyo = '" + nombreArchivo + "'"
                            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                            misRegistros.Read()
                            Total = Total + misRegistros.Item("Total")

                            misRegistros.Close()
                            miComando.CommandText = "select Count(*) as Total from Planteamiento where ArchivoApoyo = '" + nombreArchivo + "' or ArchivoApoyo2 = '" + nombreArchivo + "'"
                            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                            misRegistros.Read()
                            Total = Total + misRegistros.Item("Total")
                            misRegistros.Close()
                            objConexion.Close()

                            If Not Total = 0 Then
                                msgWarning3.show("Cuidado", "Este recurso está asignado a otros subtemas/opciones/planteamientos.")
                            End If

                            HLArchivoCargadoSubtemas.Text = nombreArchivo
                            HLArchivoCargadoSubtemas.NavigateUrl = Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(nombreArchivo))

                        Else
                            archivo.SaveAs(ruta & nombreArchivo)
                            Beans.FileReference.Activator(0) = False
                            HLArchivoCargadoSubtemas.Visible = True
                            HLArchivoCargadoSubtemas.Text = "Archivo cargado: " & Trim(HttpUtility.HtmlDecode(nombreArchivo))
                            HLArchivoCargadoSubtemas.NavigateUrl = Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(nombreArchivo))
                        End If
                        SDSmaterialapoyo.InsertParameters.Add("ArchivoApoyo", nombreArchivo)
                        SDSmaterialapoyo.Insert()
                        SDSmaterialapoyo.DataBind()
                        CType(New MessageSuccess(GVsubtemas, Me.GetType, "El archivo ha sido guardado"), MessageSuccess).show()
                    End If
                    Beans.FileReference.Activator(0) = False
                Else
                    nombreArchivo = TBlink.Text
                    SDSmaterialapoyo.InsertParameters.Add("ArchivoApoyo", nombreArchivo)
                    SDSmaterialapoyo.Insert()
                    CType(New MessageSuccess(GVsubtemas, Me.GetType, "El enlace ha sido guardado"), MessageSuccess).show()
                    SDSmaterialapoyo.DataBind()
                End If
            Else
                CType(New MessageError(GVsubtemas, Me.GetType, "No ha seleccionado ningún archivo para cargar."), MessageError).show()
            End If
            GVApoyoSubtema.DataBind()
            Siget.Utils.Library.recalculateStructure()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GVsubtemas, Me.GetType, "Por favor revisa de nuevo los campos que has llenado"), MessageError).show()
        End Try

    End Sub
#End Region

#Region "DATABOUND"
    Protected Sub GVtemas_DataBound(sender As Object, e As EventArgs) Handles GVtemas.DataBound
        If GVtemas.Rows.Count = 0 Then
            btnEliminarTema.Visible = False
            btnActualizarTema.Visible = False

            GVtemas.SelectedIndex = -1
        End If
    End Sub

    Protected Sub GVsubtemas_DataBound(sender As Object, e As EventArgs) Handles GVsubtemas.DataBound
        GVsubtemas.Caption = "<h3>" + Lang_Config.Translate("temas", "Header_subtemas") + "</h3>"
        If GVtemas.Rows.Count = 0 Then
            btnApoyosSubtemas.Visible = False
            GVsubtemas.SelectedIndex = -1
        End If
    End Sub

    Protected Sub DDLNivelTemas_DataBound(sender As Object, e As EventArgs) Handles DDLNivelTemas.DataBound
        DDLNivelTemas.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Nivel"), "0"))
        If DDLNivelTemas.Items.Count = 2 Then
            DDLNivelTemas.SelectedIndex = 1
            DDLGradoTemas.DataBind()
        End If
    End Sub

    Protected Sub DDLGradoTemas_DataBound(sender As Object, e As EventArgs) Handles DDLGradoTemas.DataBound
        DDLGradoTemas.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Grado"), "0"))
        If DDLGradoTemas.Items.Count = 2 Then
            DDLGradoTemas.SelectedIndex = 1
            DDLAsignaturaTemas.DataBind()
        End If
    End Sub

    Protected Sub DDLAsignaturaTemas_DataBound(sender As Object, e As EventArgs) Handles DDLAsignaturaTemas.DataBound
        DDLAsignaturaTemas.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Asignatura"), "0"))
        If DDLAsignaturaTemas.Items.Count = 2 Then
            DDLAsignaturaTemas.SelectedIndex = 1
            GVtemas.DataBind()
        End If
    End Sub
    Protected Sub GVApoyoSubtema_DataBound(sender As Object, e As EventArgs)
        If GVApoyoSubtema.Rows.Count = 0 Then
            btnEliminarApoyoSubtema.Visible = False
            GVApoyoSubtema.SelectedIndex = -1
        End If
    End Sub
#End Region

#Region "PRE RENDER"
    Protected Sub GVasignaturas_PreRender(sender As Object, e As EventArgs) Handles GVtemas.PreRender
        GVtemas.Columns(3).HeaderText = Lang_Config.Translate("temas", "Tema")
        GVtemas.Columns(4).HeaderText = Lang_Config.Translate("temas", "Numero_tema")
    End Sub

    Protected Sub GVsubtemas_PreRender(sender As Object, e As EventArgs) Handles GVsubtemas.PreRender
        GVsubtemas.Columns(2).HeaderText = Lang_Config.Translate("temas", "Competencia")
        GVsubtemas.Columns(3).HeaderText = Lang_Config.Translate("temas", "Numero_tema")
        GVsubtemas.Columns(4).HeaderText = Lang_Config.Translate("temas", "Subtema")
    End Sub




    Protected Sub GVApoyoSubtema_PreRender(sender As Object, e As EventArgs) Handles GVApoyoSubtema.PreRender
        GVApoyoSubtema.Caption = Lang_Config.Translate("temas", "Header_apoyo_subtema")

    End Sub
#End Region

#Region "PAGEINDEX_CHANGED"

    Protected Sub GVtemas_PageIndexChanged(sender As Object, e As EventArgs) Handles GVtemas.PageIndexChanged
        GVtemas.SelectedIndex = -1
        Siget.Utils.GeneralUtils.CleanAll(PanelTemas.Controls)
        btnActualizarTema.Visible = False
        btnEliminarTema.Visible = False
    End Sub

    Protected Sub GVsubtemas_PageIndexChanged(sender As Object, e As EventArgs) Handles GVsubtemas.PageIndexChanged
        GVsubtemas.SelectedIndex = -1
        Siget.Utils.GeneralUtils.CleanAll(PanelSubtemas.Controls)
        btnActualizarSubtema.Visible = False
        btnEliminarSubtema.Visible = False
        btnApoyosSubtemas.Visible = False
    End Sub


#End Region

#Region "INDEX_CHANGING"
    Protected Sub GVtemas_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles GVtemas.SelectedIndexChanging
        If e.NewSelectedIndex = CType(sender, GridView).SelectedIndex Then
            GVtemas.SelectedIndex = -1
            e.Cancel = True
            Siget.Utils.GeneralUtils.CleanAll(PanelTemas.Controls)
            btnActualizarTema.Visible = False
            btnEliminarTema.Visible = False
        End If
    End Sub


    Protected Sub GVsubtemas_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles GVsubtemas.SelectedIndexChanging
        If e.NewSelectedIndex = CType(sender, GridView).SelectedIndex Then
            GVsubtemas.SelectedIndex = -1
            e.Cancel = True
            Siget.Utils.GeneralUtils.CleanAll(PanelSubtemas.Controls)
            btnActualizarSubtema.Visible = False
            btnEliminarSubtema.Visible = False
            btnApoyosSubtemas.Visible = False
        End If
    End Sub
#End Region

End Class
