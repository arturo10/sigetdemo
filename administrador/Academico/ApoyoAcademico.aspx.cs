﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data;
using Siget.Config;
using Siget.Utils;
using Siget.Beans;
using Siget.Utils;
using Siget.Lang;


public partial class administrador_Academico_ApoyoAcademico : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Sesion.sesionAbierta();
   
    }

    protected void GVapoyosacademicos_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        int cociente = (GVapoyosacademicos.PageCount) / 15;
        int moduloIndex = (GVapoyosacademicos.PageIndex + 1) % 15;
        int cocienteActual = Convert.ToInt16(Math.Floor(Convert.ToDouble((GVapoyosacademicos.PageIndex + 1) / 15)));

        int inicial = 0;
        int final = 0;

        if (cociente == 0)
        {
            inicial = 1;
            final = GVapoyosacademicos.PageCount;
        }
        else if (cociente >= 1 & moduloIndex == 0)
        {
            inicial = GVapoyosacademicos.PageIndex;
            final = ((inicial + 15 <= GVapoyosacademicos.PageCount) ? inicial + 15 : GVapoyosacademicos.PageCount);
        }
        else
        {
            inicial = (cocienteActual * 15 > 0 ? cocienteActual * 15 : 1);
            final = ((inicial + 15 <= GVapoyosacademicos.PageCount) ? inicial + 15 : GVapoyosacademicos.PageCount);
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            HtmlControl con = (HtmlControl)e.Row.Cells[0].FindControl("Pag");
            for (int counter = inicial; counter <= final; counter++)
            {
                ((HtmlGenericControl)con.FindControl("pagina" + counter.ToString())).Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GVapoyosacademicos, "Page$" + counter.ToString());
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GVapoyosacademicos, "Select$" + e.Row.RowIndex);

        }
    }

    protected void GVapoyosacademicos_RowCreated(object sender, GridViewRowEventArgs e)
    {
        int cociente = (GVapoyosacademicos.PageCount / 15);
        int moduloIndex = (GVapoyosacademicos.PageIndex + 1) % 15;
        int cocienteActual = Convert.ToInt32(Math.Floor(Convert.ToDouble((GVapoyosacademicos.PageIndex + 1) / 15)));
        int final = 0;
        int inicial = 0;


        if (e.Row.RowType == DataControlRowType.Pager)
        {
            HtmlControl con = (HtmlControl)e.Row.Cells[0].FindControl("Pag");
            if (cociente == 0)
            {
                inicial = 1;
                final = GVapoyosacademicos.PageCount;
            }
            else if (cociente >= 1 & moduloIndex == 0)
            {
                inicial = GVapoyosacademicos.PageIndex;
                final = ((inicial + 10 <= GVapoyosacademicos.PageCount) ? inicial + 15 : GVapoyosacademicos.PageCount);
            }
            else
            {
                inicial = (cocienteActual * 15 > 0 ? cocienteActual * 15 : 1);
                final = (inicial + 15 <= GVapoyosacademicos.PageCount ? inicial + 15 : GVapoyosacademicos.PageCount);
            }


            for (int counter = inicial; counter <= final; counter++)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                HtmlGenericControl page = new HtmlGenericControl("a");

                if (counter == inicial & cocienteActual > 0)
                {
                    HtmlGenericControl span = new HtmlGenericControl("span");
                    span.InnerHtml = "&laquo;";
                    page.Controls.Add(span);
                    page.Attributes.Add("aria-label", "Previous");
                    page.ID = "pagina" + counter.ToString();
                    li.Controls.Add(page);
                    con.Controls.Add(li);

                }
                else
                {
                    if (counter == GVapoyosacademicos.PageIndex + 1)
                    {
                        li.Attributes["class"] = "active";
                    }
                    page.InnerText = counter.ToString();
                    page.ID = "pagina" + counter.ToString();

                    li.Controls.Add(page);
                    con.Controls.Add(li);
                }
            }


        }

    }

    protected void Insertar_Click(object sender, System.EventArgs e)
    {
        if (string.IsNullOrEmpty(TBconsecutivo.Text.Trim()))
            TBconsecutivo.Text = "0";
        try
        {
            string archivocargado = string.Empty;
            string urlApoyo = Global.urlApoyo;
            HttpPostedFile archivo = default(HttpPostedFile);


            if (FileReference.Activator.Count() > 0)
            {
                if (FileReference.Activator[0])
                {
                    archivocargado = FileReference.nameOfFiles[0];
                    archivo = FileReference.Files[0];

                    if (DDLtipoarchivo.SelectedValue != "Link" && DDLtipoarchivo.SelectedValue != "YouTube")
                    {
                        string ruta = null;
                        ruta = Global.rutaApoyo;

                        FileInfo MiArchivo2 = new FileInfo(ruta + FileReference.nameOfFiles[0]);
                        if (MiArchivo2.Exists)
                        {
                            //Avisa que ya existe, NO LO SUBE pero sí almacena el registro
                            (new MessageError(DDLtipoarchivo, this.GetType(), 
                                "YA EXISTE UN ARCHIVO CON ESE NOMBRE EN EL ALMACEN DE ARCHIVOS, CAMBIE EL NOMBRE DE SU ARCHIVO E INTENTE CARGARLO NUEVAMENTE, POR FAVOR." + FileReference.nameOfFiles[0])).show();
                        }
                        else
                        {
                            SDSApoyoNivel.InsertCommand = "SET dateformat dmy; INSERT INTO ApoyoNivel (IdNivel,Consecutivo,Descripcion,ArchivoApoyo,TipoArchivoApoyo,Estatus, FechaModif, Modifico) VALUES (" + DDLNivelApoyo.SelectedValue + "," + TBconsecutivo.Text + ",'" + TBdescripcion.Text + "','" + archivocargado + "','" + DDLtipoarchivo.SelectedValue + "','" + DDLestatus.SelectedValue + "', getdate(), '" + User.Identity.Name + "')";
                            archivo.SaveAs(ruta + FileReference.nameOfFiles[0]);
                            SDSApoyoNivel.Insert();

                            (new MessageSuccess(DDLtipoarchivo, this.GetType(),
                              "El registro ha sido guardado.")).show();
                            GVapoyosacademicos.DataBind();
                            LBArchivoApoyoNivel.Text = "Archivo cargado: " + archivocargado;
                            LBArchivoApoyoNivel.NavigateUrl = urlApoyo + archivocargado;

                        }

                    }
                    else
                    {
                        archivocargado = TBlink.Text;
                        SDSApoyoNivel.InsertCommand = "SET dateformat dmy; INSERT INTO ApoyoNivel (IdNivel,Consecutivo,Descripcion,ArchivoApoyo,TipoArchivoApoyo,Estatus, FechaModif, Modifico) VALUES (" + DDLNivelApoyo.SelectedValue + "," + TBconsecutivo.Text + ",'" + TBdescripcion.Text + "','" + archivocargado + "','" + DDLtipoarchivo.SelectedValue + "','" + DDLestatus.SelectedValue + "', getdate(), '" + User.Identity.Name + "')";
                        LBArchivoApoyoNivel.Text = "Link cargado:" + archivocargado;
                        LBArchivoApoyoNivel.NavigateUrl = archivocargado;
                        SDSApoyoNivel.Insert();
                        (new MessageSuccess(DDLtipoarchivo, this.GetType(),
                          "El registro ha sido guardado.")).show();
                        GVapoyosacademicos.DataBind();

                    }
                    FileReference.Activator[0] = false;
                 
                }
                else
                {
                     (new MessageError(DDLtipoarchivo, this.GetType(),
                              "No ha indicado ningún archivo o link.")).show();
                }

            }
        }
        catch (Exception ex)
        {
            LogManager.ExceptionLog_InsertEntry(ex);
            (new MessageError(DDLtipoarchivo, this.GetType(),
                               "Contacte al administrador.")).show();
        }
    }

    protected void Eliminar_Click(object sender, System.EventArgs e)
    {
        try
        {
            SDSApoyoNivel.DeleteCommand = "Delete from ApoyoNivel where IdApoyoNivel = " + GVapoyosacademicos.SelectedDataKey["IdApoyoNivel"].ToString();
            SDSApoyoNivel.Delete();


            if (GVapoyosacademicos.SelectedDataKey["TipoArchivoApoyo"].ToString() != "Link" && GVapoyosacademicos.SelectedDataKey["TipoArchivoApoyo"].ToString() != "YouTube")
            {
                if (HttpUtility.HtmlDecode(GVapoyosacademicos.SelectedDataKey["ArchivoApoyo"].ToString()).Trim().Length > 1)
                {
                    string ruta = null;
                    ruta = Global.rutaApoyo;
                    FileInfo MiArchivo = new FileInfo(ruta +HttpUtility.HtmlDecode(GVapoyosacademicos.SelectedDataKey["ArchivoApoyo"].ToString().Trim()));
                    MiArchivo.Delete();
                    LBArchivoApoyoNivel.Text = string.Empty;
                    LBArchivoApoyoNivel.NavigateUrl = string.Empty;
                }
            }

            GVapoyosacademicos.DataBind();
            
            (new MessageSuccess(DDLtipoarchivo, this.GetType(),
                        "El registro ha sido eliminado, al igual que su archivo de apoyo si lo tenía.")).show();
           
        }
        catch (Exception ex)
        {
            LogManager.ExceptionLog_InsertEntry(ex);
            (new MessageError(GVapoyosacademicos, this.GetType(),
                             "Contacte al administrador.")).show();
        }
    }

    protected void Actualizar_Click(object sender, System.EventArgs e)
    {
        try
        {
            SDSApoyoNivel.UpdateCommand = "SET dateformat dmy; UPDATE ApoyoNivel SET Consecutivo = " + TBconsecutivo.Text + ", Descripcion = '" + TBdescripcion.Text + "', Estatus = '" + DDLestatus.SelectedValue + "', TipoArchivoApoyo = '" + DDLtipoarchivo.SelectedValue + "', FechaModif = getdate(), Modifico = '" + User.Identity.Name + "' WHERE IdApoyoNivel =" + GVapoyosacademicos.SelectedDataKey["IdApoyoNivel"].ToString();
            SDSApoyoNivel.Update();
            (new MessageSuccess(GVapoyosacademicos, this.GetType(),
                           "El registro ha sido actualizado.")).show();
            GVapoyosacademicos.DataBind();
        }
        catch (Exception ex)
        {
            LogManager.ExceptionLog_InsertEntry(ex);
            (new MessageError(GVapoyosacademicos, this.GetType(),
                             "Contacte al administrador.")).show();
        }


    }

    protected void DDLtipoarchivo_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (DDLtipoarchivo.SelectedValue == "Link" | DDLtipoarchivo.SelectedValue == "YouTube")
        {
            RutaArchivo.Visible = false;
            TBlink.Visible = true;
            LBArchivoApoyoNivel.Text = string.Empty;
            LBArchivoApoyoNivel.NavigateUrl = string.Empty;
            if (DDLtipoarchivo.SelectedValue == "Link")
            {
                msgInfo1.show("Revise", "Si el enlace es a una página fuera del sistema, el link debe incluir dos diagonales al inicio (//). <br />Por ejemplo: //www.google.com.mx");
            }
            else
            {
                msgInfo1.hide();
            }

        }
        else
        {
            RutaArchivo.Visible = true;
            TBlink.Text = "";
            TBlink.Visible = false;
        }
    }

    protected void GVapoyosacademicos_DataBound(object sender, EventArgs e)
    {
        if (GVapoyosacademicos.Rows.Count == 0)
        {
            Eliminar.Visible = false;
            Actualizar.Visible = false;
            GVapoyosacademicos.SelectedIndex = -1;
        }
    }

    protected void DDLNivelApoyo_DataBound(object sender, System.EventArgs e)
    {
        DDLNivelApoyo.Items.Insert(0, new ListItem("---Elija el " + Lang_Config.Translate("general","Nivel"), "0"));
    }

    protected void RutaArchivo_UploadedComplete(object sender, System.EventArgs e)
    {
       FileReference.Files.Insert(0,RutaArchivo.PostedFile);
       FileReference.nameOfFiles.Insert(0,RutaArchivo.FileName);
       FileReference.Activator.Insert(0,true);
    }
     
    protected void GVapoyosacademicos_SelectedIndexChanged(object sender, System.EventArgs e) 
    {
        string ruta = Global.urlApoyo;

        Eliminar.Visible = true;
        Actualizar.Visible = true;

        if (GVapoyosacademicos.SelectedDataKey["TipoArchivoApoyo"].ToString().Trim() == "Link" ||
            GVapoyosacademicos.SelectedDataKey["TipoArchivoApoyo"].ToString().Trim() == "YouTube")
        {

            RutaArchivo.Visible = false;
            TBlink.Visible = true;
            TBlink.Text = GVapoyosacademicos.SelectedDataKey["ArchivoApoyo"].ToString();
            LBArchivoApoyoNivel.Text = String.Empty;
            LBArchivoApoyoNivel.NavigateUrl = String.Empty;
        }
        else
        {
            RutaArchivo.Visible = true;
            TBlink.Visible = false;
            LBArchivoApoyoNivel.Text = GVapoyosacademicos.SelectedDataKey["ArchivoApoyo"].ToString().Trim();
            LBArchivoApoyoNivel.NavigateUrl = ruta + GVapoyosacademicos.SelectedDataKey["ArchivoApoyo"].ToString().Trim();
            TBlink.Text = "";
        }


        TBconsecutivo.Text = GVapoyosacademicos.SelectedDataKey["Consecutivo"].ToString();
        TBdescripcion.Text = HttpUtility.HtmlDecode(GVapoyosacademicos.SelectedDataKey["Descripcion"].ToString().Trim());
        DDLtipoarchivo.SelectedValue = HttpUtility.HtmlDecode(GVapoyosacademicos.SelectedDataKey["TipoArchivoApoyo"].ToString().Trim());
        DDLestatus.SelectedValue = HttpUtility.HtmlDecode(GVapoyosacademicos.SelectedDataKey["Estatus"].ToString().Trim());

    }



    protected void GVapoyosacademicos_PageIndexChanged(object sender, EventArgs e)
    {
          GVapoyosacademicos.SelectedIndex = -1;
          Siget.Utils.GeneralUtils.CleanAll(PanelApoyosAcademicos.Controls);
          Eliminar.Visible = false;
          Actualizar.Visible = false;
    }
    protected void GVapoyosacademicos_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
          if( e.NewSelectedIndex == ((GridView)sender).SelectedIndex) 
          {
              GVapoyosacademicos.SelectedIndex = -1;
                  e.Cancel=true;
              Eliminar.Visible=false;
              Actualizar.Visible = false;
              Siget.Utils.GeneralUtils.CleanAll(PanelApoyosAcademicos.Controls);
          }
        
    }
}   