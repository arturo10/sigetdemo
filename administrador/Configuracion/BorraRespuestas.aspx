﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false"  EnableEventValidation="false"
    CodeFile="borrarespuestas.aspx.vb" Inherits="administrador_AgruparReactivos" %>


<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat radius-bordered">
            <div class="widget-header bg-themeprimary">
                <span class="widget-caption">Área para la administración de reactivos y opciones </span>
            </div>

            <div class="widget-body">
                <div class="widget-main ">
                    <div class="tabbable">
                        <ul class="nav nav-tabs tabs-flat" id="myTab11">
                      
                            <li>
                                <a id="BorradoActividades" data-toggle="tab" href="#borrarActividades">Borrar actividades
                                </a>
                            </li>

                        </ul>
                        <div class="tab-content tabs-flat">
                  

                            <div id="borrarActividades" class="tab-pane active">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <div class="form-horizontal">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">
                                                              <%=Lang_Config.Translate("general", "Institucion")%>  
                                                            </label>
                                                            <div class="col-lg-10">
                                                                <asp:DropDownList ID="DDLinstitucionBorrarActividades"
                                                                    runat="server" AutoPostBack="True" DataSourceID="SDSinstituciones"
                                                                    
                                                                    DataTextField="Descripcion" DataValueField="IdInstitucion"
                                                                   CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">
                                                                 <%=Lang_Config.Translate("general", "Plantel")%>  
                                                            </label>
                                                            <div class="col-lg-10">
                                                                <asp:DropDownList ID="DDLplantelBorrarActividades" runat="server" AutoPostBack="True"
                                                                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                                                    DataValueField="IdPlantel" CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="col-lg-2 control-label">
                                                                       <%=Lang_Config.Translate("general", "Nivel")%>  
                                                            </label>
                                                            <div class="col-lg-10">
                                                                <asp:DropDownList ID="DDLNivelBorrarActividades" runat="server" AutoPostBack="True"
                                                                    DataSourceID="SDSNivelesBorrarActividades" DataTextField="Descripcion" 
                                                                    DataValueField="IdNivel"
                                                                    CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="col-lg-2 control-label">
                                                                <%=Lang_Config.Translate("general", "AreaConocimiento")%>  
                                                            </label>
                                                             <div class="col-lg-10">
                                                                 <asp:DropDownList ID="DDLGradoBorrarActividades" runat="server" AutoPostBack="True"
                                                                     DataSourceID="SDSGradosBorrarActividades" DataTextField="Descripcion"
                                                                     DataValueField="IdGrado" CssClass="form-control">
                                                                 </asp:DropDownList>
                                                             </div>
                                                         </div>
                                                         <div class="form-group">
                                                            <label class="col-lg-2 control-label">
                                                                Ciclo Escolar
                                                            </label>
                                                             <div class="col-lg-10">
                                                                 <asp:DropDownList ID="DDLCicloBorrarActividades" runat="server" AutoPostBack="True"
                                                                     DataSourceID="SDSCiclosEscolaresBorrarActividades" DataTextField="Descripcion"
                                                                     DataValueField="IdCicloEscolar" CssClass="form-control">
                                                                 </asp:DropDownList>
                                                             </div>
                                                         </div>
                                                         <div class="form-group">
                                                            <label class="col-lg-2 control-label">
                                                                  <%=Lang_Config.Translate("general", "Asignatura")%>  
                                                            </label>
                                                             <div class="col-lg-10">
                                                                 <asp:DropDownList ID="DDLAsignaturaBorrarActividades" runat="server" AutoPostBack="True"
                                                                     DataSourceID="SDSAsignaturasBorrarActividades" DataTextField="Descripcion"
                                                                     DataValueField="IdAsignatura" CssClass="form-control">
                                                                 </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="col-lg-2 control-label">
                                                                Calificación
                                                            </label>
                                                             <div class="col-lg-10">
                                                                 <asp:DropDownList ID="DDLcalificacionBorrarActividades" runat="server" AutoPostBack="True"
                                                                     DataSourceID="SDSCalificacionesBorrarActividades" DataTextField="Descripcion"
                                                                     DataValueField="IdCalificacion" CssClass="form-control">
                                                                 </asp:DropDownList>
                                                             </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Grupo</label>
                                                            <div class="col-lg-10">
                                                                <asp:DropDownList ID="DDLgrupoBorrarActividades" runat="server" AutoPostBack="True"
                                                                    DataSourceID="SDSgrupos" DataTextField="Descripcion"
                                                                    DataValueField="IdGrupo" CssClass="form-control">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                         
                                                    </div>
                                                    <div class="col-lg-6">
                                                        
                                                        <div class="form-group">
                                                            <asp:Label runat="server" ID="LbSeleccionaActividadBorrar" Visible="false">Actividad para capturar</asp:Label>
                                                        </div>
                                                        <div class="form-group">

                                                            <div class="table-responsive">
                                                                <asp:GridView ID="gvEvaluacionesBorrarActividades" runat="server"
                                                                                  
                                                                    AutoGenerateColumns="False"  CellPadding="3" PageSize="5"
                                                                    DataKeyNames="IdEvaluacion" DataSourceID="SDSEvaluacionesBorrarActividades"
                                                                    AllowPaging="true" AllowSorting="true" CssClass="table table-striped table-bordered"
                                                                    Height="17px"
                                                                    Style="font-size: x-small; text-align: left;">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad" Visible="false"
                                                                            InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion" />
                                                                        <asp:BoundField DataField="IdCalificacion"
                                                                            HeaderText="IdCalificacion"
                                                                            SortExpression="IdCalificacion" Visible="False" />
                                                                        <asp:BoundField DataField="ClaveBateria" HeaderText="Clave de la Actividad"
                                                                            SortExpression="ClaveBateria" />
                                                                        <asp:BoundField DataField="ClaveAbreviada" HeaderText="Clave p/Reporte"
                                                                            SortExpression="ClaveAbreviada" />
                                                                        <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                                                                            SortExpression="Porcentaje">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                                            HeaderText="Inicia" SortExpression="InicioContestar" />
                                                                        <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                                            HeaderText="Termina" SortExpression="FinContestar" />
                                                                        <asp:BoundField DataField="FinSinPenalizacion"
                                                                            DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Máxima"
                                                                            SortExpression="FinSinPenalizacion" />
                                                                        <asp:BoundField DataField="Penalizacion" HeaderText="% Penalización"
                                                                            SortExpression="Penalizacion">
                                                                            <ItemStyle HorizontalAlign="Right" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                                                    </Columns>
                                                                    <PagerTemplate>
                                                                        <ul runat="server" id="Pag" class="pagination">
                                                                        </ul>
                                                                    </PagerTemplate>
                                                                    <PagerStyle HorizontalAlign="Center" />
                                                                    <SelectedRowStyle CssClass="row-selected" />
                                                                    <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="table-responsive">
                                                        <asp:GridView ID="GValumnosterminados" runat="server"
                                                            AllowPaging="True" 
                                                            AllowSorting="True"
                                                            AutoGenerateColumns="False"
                                                            DataKeyNames="IdAlumno"
                                                            DataSourceID="SDSalumnosterminados"
                                              
                                                       
                                                            Caption="ALUMNOS con actividad realizada. Seleccione el resultado que desee eliminar"
                                                            PageSize="5"
                                                            CssClass="table table-striped table-bordered"
                                                            Height="17px"
                                                            Style="font-size: x-small; text-align: left;">
                                                            <Columns>
                                                               
                                                                <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno" />
                                                                <asp:BoundField DataField="ApePaterno" HeaderText="Apellido Paterno"
                                                                    SortExpression="ApePaterno" />
                                                                <asp:BoundField DataField="ApeMaterno" HeaderText="Apellido Materno"
                                                                    SortExpression="ApeMaterno" />
                                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre(s)"
                                                                    SortExpression="Nombre" />
                                                                <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                                                                    SortExpression="Matricula" />
                                                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                                                <asp:BoundField DataField="Resultado" DataFormatString="{0:0.00}"
                                                                    HeaderText="Calificación (%)" SortExpression="Resultado" />
                                                                <asp:BoundField DataField="FechaTermino" DataFormatString="{0:dd/MM/yyyy}"
                                                                    HeaderText="Fecha Terminada" SortExpression="FechaTermino" />
                                                            </Columns>
                                                            <PagerTemplate>
                                                                <ul runat="server" id="Pag" class="pagination">
                                                                </ul>
                                                            </PagerTemplate>
                                                            <PagerStyle HorizontalAlign="Center" />
                                                            <SelectedRowStyle CssClass="row-selected" />
                                                            <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                        </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <asp:LinkButton ID="btnBorrarActividades" Visible="false" runat="server"
                                                            CssClass="btn btn-lg btn-labeled btn-darkorange">
                                                <i class="btn-label fa fa-remove"></i>Eliminar Actividades
                                                        </asp:LinkButton>
                                                    </div>
                                                    <div class="form-group">
                                              <uc1:msgerror runat="server" id="msgError5" />

                                              <uc1:msgsuccess runat="server" id="msgSuccess5" />
                                          </div>

                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                        </div>


                    </div>

                </div>

            </div>

        </div>
    </div>


    <div class="datasource">
         
       
        
              
             
         
             <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Institucion] 
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion]"></asp:SqlDataSource>

        
                            <asp:SqlDataSource ID="SDSplanteles" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion)
ORDER BY [Descripcion]">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLinstitucionBorrarActividades" Name="IdInstitucion"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>

       <asp:SqlDataSource ID="SDSNivelesBorrarActividades" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="select N.IdNivel, N.Descripcion
from Nivel N, Escuela E, Plantel P
where N.IdNivel = E.IdNivel and P.IdPlantel = E.IdPlantel
      and P.IdPlantel = @IdPlantel
Order by Descripcion">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLplantelBorrarActividades" Name="IdPlantel"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>

         <asp:SqlDataSource ID="SDSGradosBorrarActividades" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLnivelBorrarActividades" Name="IdNivel"
                                        PropertyName="SelectedValue" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
            <asp:SqlDataSource ID="SDSCiclosEscolaresBorrarActividades" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [CicloEscolar] WHERE ([Estatus] = 'Activo') ORDER BY [FechaInicio]"></asp:SqlDataSource>
         
            <asp:SqlDataSource ID="SDSAsignaturasBorrarActividades" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdAsignatura, IdGrado, Descripcion
from Asignatura
where IdGrado = @IdGrado
order by Descripcion">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLgradoBorrarActividades" Name="IdGrado"
                                        PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>

          <asp:SqlDataSource ID="SDSCalificacionesBorrarActividades" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT C.IdCalificacion, C.IdCicloEscolar, C.Clave, C.IdAsignatura, A.Descripcion Materia, C.Consecutivo,C.Descripcion, C.Valor
FROM Calificacion C, Asignatura A
WHERE ([IdCicloEscolar] = @IdCicloEscolar)  and (A.IdAsignatura = C.IdAsignatura) and (A.IdAsignatura = @IdAsignatura)
ORDER BY A.Descripcion, C.Consecutivo">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLcicloBorrarActividades" Name="IdCicloEscolar"
                                        PropertyName="SelectedValue" />
                                    <asp:ControlParameter ControlID="DDLAsignaturaBorrarActividades" Name="IdAsignatura"
                                        PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>

         <asp:SqlDataSource ID="SDSEvaluacionesBorrarActividades" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="select IdEvaluacion, IdCalificacion, ClaveBateria, ClaveAbreviada, ClaveAbreviada, Porcentaje, InicioContestar, FinContestar, FinSinPenalizacion, Penalizacion, Tipo
from Evaluacion 
where IdCalificacion = @IdCalificacion and SeEntregaDocto = 'False'
and IdEvaluacion not in  (select IdEvaluacion from EvaluacionPlantel where IdPlantel = @IdPlantel)
UNION
select E.IdEvaluacion, E.IdCalificacion, E.ClaveBateria, E.ClaveAbreviada, E.ClaveAbreviada, E.Porcentaje, P.InicioContestar, P.FinContestar,
P.FinSinPenalizacion, E.Penalizacion, E.Tipo
from Evaluacion E, EvaluacionPlantel P
where E.IdCalificacion = @IdCalificacion and E.IdEvaluacion = P.IdEvaluacion and P.IdPlantel = @IdPlantel and E.SeEntregaDocto = 'False'
order by IdCalificacion, InicioContestar">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLcalificacionBorrarActividades" Name="IdCalificacion"
                                        PropertyName="SelectedValue" />
                                    <asp:ControlParameter ControlID="DDLplantelBorrarActividades" Name="IdPlantel"
                                        PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>

         <asp:SqlDataSource ID="SDSgrupos" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Grupo] 
WHERE ([IdGrado] = @IdGrado) and ([IdPlantel] = @IdPlantel) and ([IdCicloEscolar] = @IdCicloEscolar)">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLgradoBorrarActividades" Name="IdGrado"
                                        PropertyName="SelectedValue" Type="Int32" />
                                    <asp:ControlParameter ControlID="DDLplantelBorrarActividades" Name="IdPlantel"
                                        PropertyName="SelectedValue" />
                                    <asp:ControlParameter ControlID="DDLCicloBorrarActividades" Name="IdCicloEscolar"
                                        PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>

        
                            <asp:SqlDataSource ID="SDSalumnosterminados" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT A.IdAlumno, A.Nombre, A.ApePaterno, A.ApeMaterno, A.Matricula, A.Email, E.Resultado, E.FechaTermino
FROM Alumno A, EvaluacionTerminada E
WHERE (A.Estatus = 'Activo' or A.Estatus = 'Suspendido') and
A.IdGrupo = @IdGrupo AND A.IdPlantel = @IdPlantel 
and A.IdAlumno in (select IdAlumno from EvaluacionTerminada where
                                 IdEvaluacion = @IdEvaluacion)
and E.IdAlumno = A.IdAlumno and E.IdEvaluacion = @IdEvaluacion and E.IdGrupo = A.IdGrupo 
and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido')
ORDER BY ApePaterno, ApeMaterno, Nombre">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLgrupoBorrarActividades" Name="IdGrupo"
                                        PropertyName="SelectedValue" Type="Int32" />
                                    <asp:ControlParameter ControlID="DDLplantelBorrarActividades" Name="IdPlantel"
                                        PropertyName="SelectedValue" Type="Int32" />
                                    <asp:ControlParameter ControlID="gvEvaluacionesBorrarActividades" Name="IdEvaluacion"
                                        PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>

               <asp:SqlDataSource ID="SDSevaluacionterminada" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [EvaluacionTerminada]"></asp:SqlDataSource>

     <asp:SqlDataSource ID="SDSrespuestas" runat="server"
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                                SelectCommand="SELECT * FROM [Respuesta]"></asp:SqlDataSource>
         </div>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">
</asp:Content>

