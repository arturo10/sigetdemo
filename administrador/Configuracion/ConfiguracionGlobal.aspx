﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false" CodeFile="ConfiguracionGlobal.aspx.vb" Inherits="administrador_AgruparReactivos" %>


<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">




    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="widget flat radius-bordered">
                <div class="widget-header bg-themeprimary">
                    <span class="widget-caption">CONFIGURACIÓN DEL FILESYSTEM</span>
                </div>

                <div class="widget-body">
                    <div class="widget-main ">
                        <div class="tabbable">
                            <ul class="nav nav-tabs tabs-flat" id="myTab11">
                                <li id="Cal" class="active">
                                    <a data-toggle="tab" href="#ConfigGeneral">  
                                        <i class="fa fa-cogs"></i> 
                                        General
                                    </a>
                                </li>
                                <li>
                                    <a id="LConfigGeneral" data-toggle="tab" href="#ConfigAlumno">
                                        <i class="flaticon-people"></i> 
                                        <%=Lang_Config.Translate("general", "Alumno")%>
                                    </a>
                                </li>
                                <li>
                                    <a id="LConfigProfesor" data-toggle="tab" href="#ConfigProfesor">
                                     <i class="flaticon-education"></i> 
                                            <%=Lang_Config.Translate("general", "Profesor")%>
                                    </a>
                                </li>
                                <li>
                                    <a id="LConfigCoordinador" data-toggle="tab" href="#ConfigCoordinador">
                                          <i class="flaticon-technology"></i> 
                                        Coordinador
                                    </a>
                                </li>
                                <li>
                                    <a id="LConfigAdministrador" data-toggle="tab" href="#ConfigAdministrador">
                                    <i class="flaticon-people-1"></i> 
                                        Administrador
                                    </a>
                                </li>

                            </ul>
                            <div class="tab-content tabs-flat">

                                <div id="ConfigGeneral" class="tab-pane in active">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <div class="form-horizontal">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" style="text-align: left;">
                                                    </div>

                                                    <div class="panel-body">

                                                        
                                                        <div class="col-lg-6">

                                                        <div class ="form-group">

                                                            <ul class="col-lg-12" style="list-style:none;">
                                                                <li>
                                                                    <label class="col-lg-4  control-label">
                                                                        <b>Modelo
                                                                        </b>
                                                                    </label>
                                                                    <div class="col-lg-8">
                                                                        <asp:DropDownList ID="ddlModelo" CssClass="form-control" runat="server" AutoPostBack="true">
                                                                            <asp:ListItem Value="0">Escuela</asp:ListItem>
                                                                            <asp:ListItem Value="1">Empresa</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                          <h6 class="col-lg-12 control-label" style="text-align:center;">El esquema de trabajo de la aplicación.</h6>
                                                                    </div>
                                                                </li>
                                                              
                                                            </ul>
                                                            </div>

                                                         <div class ="form-group">

                                                            <ul class="col-lg-12" style="list-style:none;">
                                                                <li>
                                                                    <label class="col-lg-4 control-label">
                                                                        <b>Idioma
                                                                        </b>
                                                                    </label>
                                                                    <div class="col-lg-8">
                                                                        <asp:DropDownList ID="ddlIdiomaGeneral" runat="server" CssClass="form-control" AutoPostBack="true">
                                                                            <asp:ListItem Value="es-mx">Español</asp:ListItem>
                                                                            <asp:ListItem Value="en-us">English</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                         
                                                                         <h6 class="col-lg-12 control-label" style="text-align: center;">  
                                                                             El idioma de las páginas públicas (como la página de Inicio).
                                                                      </h6>
                                                                    </div>
                                                         
                                                                </li>
                                                            </ul>
                                                            </div>

                                                         <div class ="form-group">

                                                            <ul class="col-lg-12" style="list-style:none;">
                                                                <li>
                                                                    <label class="col-lg-4  control-label">
                                                                        <b> Habilitación de Registro de Errores
                                                                        </b>
                                                                    </label>
                                                                    <div class="col-lg-8">
                                                                        <asp:DropDownList ID="ddlDEPLOYED" runat="server" CssClass="form-control" AutoPostBack="true">
                                                                            <asp:ListItem Value="true">Habilitado</asp:ListItem>
                                                                            <asp:ListItem Value="false">Deshabilitado</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;">Controla si la aplicación almacena o no información de cada error encontrado. 
                                                                          Los errores se almacenan en el registro de errores de cada aplicación.</h6>
                                                                        </div>

                                                               
                                                                </li>
                                                              
                                                            </ul>
                                                            </div>
                                                   

                                                    <div class="form-group">

                                                        <ul class="col-lg-12" style="list-style: none;">
                                                            <li>
                                                                <label class="col-lg-4 control-label">
                                                                    <b>Envío de Correos Electrónicos
                                                                    </b>
                                                                </label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="ddlMAIL_ACTIVO" runat="server"
                                                                        AutoPostBack="true" CssClass="form-control">
                                                                        <asp:ListItem Value="true">Habilitado</asp:ListItem>
                                                                        <asp:ListItem Value="false">Deshabilitado</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <h6 class="col-lg-12 control-label" style="text-align: center;">Controla si la aplicación almacena o no información de cada error encontrado. 
                                                                          Controla si la aplicación envía o no correos electrónicos en las funcionalidades 
                                                                        de la aplicación que así lo permiten, como durante el envío de mensajes.</h6>
                                                                </div>

                                                            </li>

                                                        </ul>
                                                    </div>
                                                    </div>
            
                                                        <div class="col-lg-6">
                                                            <div class="form-group">

                                                                <ul class="col-lg-12" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-4 control-label">
                                                                            <b>   Permite Guardar Respuestas Abiertas
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-8">
                                                                            <asp:DropDownList ID="ddlGUARDAR_RESPUESTA_ABIERTA" runat="server"
                                                                                AutoPostBack="true" CssClass="form-control">
                                                                                <asp:ListItem Value="true">Habilitado</asp:ListItem>
                                                                                <asp:ListItem Value="false">Deshabilitado</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;">
                                                                         Determina si se muestra el botón de "guardar respuesta" al contestar Respuestas Abiertas, así como el checkbox "Mostrar Borradores" al calificar respuestas abiertas.</h6>
                                                                        </div>

                                                                    </li>

                                                                </ul>
                                                            </div>

                                                            <div class="form-group">

                                                                <ul class="col-lg-12" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-4 control-label">
                                                                            <b>Firma Integrant en Pies de página
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-8">
                                                                            <asp:DropDownList ID="ddlFIRMA_PIE_INTEGRANT" runat="server"
                                                                                CssClass="form-control"
                                                                                 AutoPostBack="true">
                                                                                <asp:ListItem Value="true">Visible</asp:ListItem>
                                                                                <asp:ListItem Value="false">Invisible</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;"> 
                                                                                Determina si se muestra información de Integrant en el pie de las páginas de la aplicación.</h6>
                                                                        </div>

                                                                    </li>

                                                                </ul>
                                                            </div>


                                                            <div class="form-group">

                                                                <asp:RadioButton ID="RbLogin_Jp1" runat="server" AutoPostBack="true"
                                                                    GroupName="Login" Checked="true"
                                                                    Style="display: inline-block; vertical-align: middle;" />
                                                                <asp:Image ID="Image1" runat="server"
                                                                    CssClass="login_thumb"
                                                                    ImageUrl="~/Resources/imagenes/login_jp1_thumb.png"
                                                                    Style="display: inline-block; vertical-align: middle;" />
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <asp:RadioButton ID="RbLogin_A1" runat="server" AutoPostBack="true"
                                                                GroupName="Login"
                                                                Style="display: inline-block; vertical-align: middle;" />
                                                                <asp:Image ID="Image2" runat="server"
                                                                    CssClass="login_thumb"
                                                                    ImageUrl="~/Resources/imagenes/login_a1_thumb.png"
                                                                    Style="display: inline-block; vertical-align: middle;" />
                                                            </div>

                                                       
                                                        </div>
                                                        <div class="form-group">
                                                               <uc1:msgError runat="server" ID="msgError" />
                                                        </div>
                                                         

                                                    </div>
                                                </div>
                                            </div>


                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <div id="ConfigAlumno" class="tab-pane ">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <div class="form-horizontal">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" style="text-align: left;">
                                                    </div>

                                                    <div class="panel-body">

                                                        <div class="col-lg-6">
                                                            <div class="form-group">

                                                                <ul class="col-lg-12" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-4 control-label" style="text-align:center;">
                                                                            <b>   Idioma
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-8">
                                                                            <asp:DropDownList ID="ddlIdiomaAlumno"
                                                                                runat="server"
                                                                                AutoPostBack="true"
                                                                                CssClass="form-control">
                                                                                <asp:ListItem Value="es-mx">Español</asp:ListItem>
                                                                                <asp:ListItem Value="en-us">English</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;">
                                                                            El idioma para este perfil.
                                                                        </div>

                                                                    </li>

                                                                </ul>
                                                            </div>

                                                                    <div class="form-group">

                                                                <ul class="col-lg-12" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-4 control-label" style="text-align:center;">
                                                                            <b>Visibilidad del botón de renuncia
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-8">
                                                                            <asp:DropDownList ID="ddlALUMNO_CONTESTAR_RENUNCIAR"
                                                                                CssClass="form-control" runat="server" AutoPostBack="true">
                                                                                <asp:ListItem Value="true">Visible</asp:ListItem>
                                                                                <asp:ListItem Value="false">Invisible</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;">
                                                                            Controla la visibilidad del botón de renuncia en el perfil del alumno durante la realización de una actividad.
                                                                        </div>

                                                                    </li>

                                                                </ul>
                                                                    </div>

                                                            <div class="form-group">

                                                                <ul class="col-lg-12" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-4 control-label" style="text-align:center;">
                                                                            <b>  Visibilidad de columna tipo de actividad
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-8">
                                                                            <asp:DropDownList ID="ddlALUMNO_PENDIENTES_COLUMNA_TIPO" runat="server" 
                                                                                AutoPostBack="true" CssClass="form-control">
                                                                                <asp:ListItem Value="true">Visible</asp:ListItem>
                                                                                <asp:ListItem Value="false">Invisible</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;">
                                                                               Determina si en la página de pendientes del alumno aparece la columna de tipo de actividad.
                                                                        </div>

                                                                    </li>

                                                                </ul>
                                                            </div>

                                                            <div class="form-group">

                                                                <ul class="col-lg-12" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-4 control-label" style="text-align:center;">
                                                                            <b>   Visibilidad de columna fecha límite sin penalización
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-8">
                                                                            <asp:DropDownList ID="ddlALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION"
                                                                                 runat="server" AutoPostBack="true" CssClass="form-control">
                                                                                <asp:ListItem Value="true">Visible</asp:ListItem>
                                                                                <asp:ListItem Value="false">Invisible</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;">
                                                                             Determina si en la página de pendientes del alumno aparece la columna de fecha de penalización.
                                                                        </div>

                                                                    </li>

                                                                </ul>
                                                            </div>

                                                                    <div class="form-group">

                                                                <ul class="col-lg-12" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-5 control-label" style="text-align:center;">
                                                                            <b> Visibilidad de columna Entrega de documento
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-7">
                                                                            <asp:DropDownList ID="ddlALUMNO_PENDIENTES_COLUMNA_ENTREGA" runat="server"
                                                                                 AutoPostBack="true" CssClass="form-control">
                                                                                <asp:ListItem Value="true">Visible</asp:ListItem>
                                                                                <asp:ListItem Value="false">Invisible</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;">
                                                                           Determina si en la página de pendientes del alumno aparece la columna de entrega
                                                                        </div>

                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">

                                                              <div class="form-group">

                                                                <ul class="col-lg-12" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-5 control-label"  style="text-align:center;">
                                                                            <b>Visibilidad de columna fecha porcentaje de penalización
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-7">
                                                                            <asp:DropDownList ID="ddlALUMNO_PENDIENTES_COLUMNA_PORCENTAJEPENALIZACION"
                                                                                 runat="server" AutoPostBack="true" CssClass="form-control">
                                                                                <asp:ListItem Value="true">Visible</asp:ListItem>
                                                                                <asp:ListItem Value="false">Invisible</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;"> 
                                                                                Determina si en la página de pendientes del alumno aparece la columna de porcentaje de penalización.
                                                                        </div>

                                                                    </li>

                                                                </ul>
                                                            </div>


                                                             <div class="form-group">

                                                                <ul class="col-lg-12" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-5 control-label" style="text-align:center;">
                                                                            <b>Proteger Documentos Contra Descarga
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-7">
                                                                            <asp:DropDownList ID="ddlALUMNO_DOCUMENTOPROTEGIDO" runat="server"
                                                                                CssClass="form-control" AutoPostBack="true">
                                                                                <asp:ListItem Value="true">Protegido</asp:ListItem>
                                                                                <asp:ListItem Value="false">Sin Proteger</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;">
                                                                            Determina si los documentos en pdf estan protegidos contra descarga.
                                                                        </div>

                                                                    </li>

                                                                </ul>
                                                            </div>



                                                             <div class="form-group">

                                                                <ul class="col-lg-12" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-5 control-label" style="text-align:center;">
                                                                            <b> Visibilidad para Inscribir Cursos
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-7">
                                                                            <asp:DropDownList ID="ddlALUMNO_INSCRIBIR_CURSOS" runat="server"
                                                                                 AutoPostBack="true" CssClass="form-control">
                                                                                <asp:ListItem Value="true">Visible</asp:ListItem>
                                                                                <asp:ListItem Value="false">No Visible</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;"> 
                                                                                Determina la visibilidad del área de inscripción de Cursos
                                                                        </div>

                                                                    </li>

                                                                </ul>
                                                            </div>



                                                             <div class="form-group">

                                                                <ul class="col-lg-12" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-5 control-label" style="text-align:center;">
                                                                            <b> Cambiar contraseña obligatoria al entrar por primera vez 
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-7">
                                                                            <asp:DropDownList ID="ddl_ContraObligatoria" runat="server"
                                                                                 AutoPostBack="true" CssClass="form-control">
                                                                                <asp:ListItem Value="true">Obligatoria</asp:ListItem>
                                                                                <asp:ListItem Value="false">No Obligatoria</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;"> 
                                                                                Determina si un alumno debe de cambiar obligatoriamente su contraseña
                                                                        </div>

                                                                    </li>

                                                                </ul>
                                                            </div>
                                                        </div>
                                               


                                                    </div>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <div id="ConfigProfesor" class="tab-pane">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <div class="form-horizontal">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" style="text-align: left;">
                                                    </div>

                                                    <div class="panel-body">


                                                        
                                                             <div class="form-group">

                                                                <ul class="col-lg-7 col-lg-offset-3" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-4 control-label" style="text-align:center;">
                                                                            <b>  Idioma
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-5">
                                                                            <asp:DropDownList ID="ddlIdiomaProfesor" runat="server"
                                                                                CssClass="form-control" AutoPostBack="true">
                                                                                <asp:ListItem Value="es-mx">Español</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;"> 
                                                                                El idioma para este perfil.
                                                                        </div>

                                                                    </li>

                                                                </ul>
                                                            </div>

                                                         <div class="form-group">

                                                                <ul class="col-lg-7 col-lg-offset-3" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-4 control-label" style="text-align: center;">
                                                                            <b>Escala de calificaciones para respuestas abiertas
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-5">
                                                                            <asp:DropDownList ID="ddlPROFESOR_CALIFICA_NUMERICO"
                                                                                 runat="server" AutoPostBack="true" CssClass="form-control">
                                                                                <asp:ListItem Value="0">0 - 100</asp:ListItem>
                                                                                <asp:ListItem Value="1">Bien - Mal</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;">
                                                                              Determina si en las opciones de calificar respuesta abierta del profesor aparecen calificaciones 0-100 o escalas bien-mal.
                                                                        </div>

                                                                    </li>

                                                                </ul>
                                                            </div>


                                                         <div class="form-group">

                                                                <ul class="col-lg-7 col-lg-offset-3" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-4 control-label" style="text-align:center;">
                                                                            <b> Escala de calificaciones para documentos
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-5">
                                                                            <asp:DropDownList ID="ddlPROFESOR_CALIFICA_DOCUMENTO"  CssClass="form-control"
                                                                                 runat="server" AutoPostBack="true">
                                                                                <asp:ListItem Value="0">0 - 100</asp:ListItem>
                                                                                <asp:ListItem Value="1">Competente - No Competente</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;"> 
                                                                              Determina si en las opciones de calificar documento del profesor aparecen calificaciones 0-100 o escalas competente-nocompetente
                                                                        </div>

                                                                    </li>

                                                                </ul>
                                                            </div>


                                                    </div>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <div id="ConfigCoordinador" class="tab-pane">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <div class="form-horizontal">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" style="text-align: left;">
                                                    </div>

                                                    <div class="panel-body">

                                                                   <div class="form-group">

                                                                <ul class="col-lg-7 col-lg-offset-3" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-4 control-label" style="text-align:center;">
                                                                            <b> Idioma
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-5">
                                                                            <asp:DropDownList ID="ddlIdiomaCoordinador" runat="server" CssClass="form-control"
                                                                                 AutoPostBack="true">
                                                                                <asp:ListItem Value="es-mx">Español</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;">
                                                                                    El idioma para este perfil.
                                                                        </div>

                                                                    </li>

                                                                </ul>
                                                            </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                                <div id="ConfigAdministrador" class="tab-pane">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <div class="form-horizontal">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" style="text-align: left;">
                                                    </div>

                                                    <div class="panel-body">

                                                                   <div class="form-group">
                                                                <ul class="col-lg-7 col-lg-offset-3" style="list-style: none;">
                                                                    <li>
                                                                        <label class="col-lg-4 control-label" style="text-align:center;">
                                                                            <b>Idioma
                                                                            </b>
                                                                        </label>
                                                                        <div class="col-lg-5">
                                                                            <asp:DropDownList ID="ddlIdiomaAdministrador" runat="server" AutoPostBack="true"
                                                                                CssClass="form-control">
                                                                                <asp:ListItem Value="es-mx">Español</asp:ListItem>
                                                                                <asp:ListItem Value="en-us">Inglés</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <h6 class="col-lg-12 control-label" style="text-align: center;"> 
                                                                            El idioma para los perfiles Administrador, Operador y Capturista.

                                                                    </li>

                                                                </ul>
                                                            </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" runat="Server">
</asp:Content>

