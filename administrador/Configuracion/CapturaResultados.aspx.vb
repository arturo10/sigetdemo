﻿Imports Siget

Partial Class administrador_AgruparReactivos
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GVAlumnos.Caption = "<h3>Seleccione al " &
            Config.Etiqueta.ALUMNO &
            "</h3>"

    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Nivel"), 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Grado"), 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Asignatura"), 0))
    End Sub

    Protected Sub DDLcalificacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacion.DataBound
        DDLcalificacion.Items.Insert(0, New ListItem("---Elija una Calificación", 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Plantel"), 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija un " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub BtnAsignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAsignar.Click
        msgError.hide()

        Try
            'HACER UN UPDATE SI YA EXISTE, OSEA, EL GRID TIENE DATOS
            If GVresultados.Rows.Count > 0 Then
                SDSresultados.UpdateCommand = "SET dateformat dmy; UPDATE EVALUACIONTERMINADA SET Resultado = " + TBresultado.Text + " where IdEvaluacionT = " + GVresultados.SelectedDataKey("IdEvaluacionT").ToString()
                SDSresultados.Update()
                GVresultados.DataBind()
                CType(New MessageSuccess(BtnAsignar, Me.GetType, "La calificación ha sido actualizada"), MessageSuccess).show()

            Else
                SDSresultados.InsertCommand = "SET dateformat dmy; INSERT INTO EVALUACIONTERMINADA(IdAlumno,IdGrado,IdGrupo,IdCicloEscolar,IdEvaluacion,Resultado,FechaTermino)" + _
                        " VALUES (" + GVAlumnos.SelectedDataKey("IdAlumno").ToString + "," + DDLgrado.SelectedValue + "," + DDLgrupo.SelectedValue + "," + _
                        DDLcicloescolar.SelectedValue + "," + GVevaluaciones.SelectedDataKey("IdEvaluacion").ToString + "," + _
                        TBresultado.Text + ",getdate())"
                SDSresultados.Insert()
                CType(New MessageSuccess(BtnAsignar, Me.GetType, "La calificación ha sido asignada"), MessageSuccess).show()
            End If
            GVAlumnos.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(BtnAsignar, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try
    End Sub

    Protected Sub BtnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEliminar.Click
        msgError.hide()

        Try
            SDSresultados.DeleteCommand = "DELETE FROM EvaluacionTerminada where IdEvaluacionT = " + GVresultados.SelectedDataKey("IdEvaluacionT").ToString
            SDSresultados.Delete()
            GVresultados.DataBind()
            CType(New MessageSuccess(BtnAsignar, Me.GetType, "La calificación ha sido eliminada"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(BtnAsignar, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try
    End Sub

    Protected Sub GVresultados_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVresultados.DataBound
        If GVresultados.Rows.Count > 0 Then
            BtnEliminar.Visible = True
        Else
            BtnEliminar.Visible = False
        End If
    End Sub

   
    Protected Sub GVAlumnos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVAlumnos.RowDataBound


        Dim cociente As Integer = (GVAlumnos.PageCount) / 15
        Dim moduloIndex As Integer = (GVAlumnos.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVAlumnos.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVAlumnos.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVAlumnos.PageIndex
            final = IIf((inicial + 15 <= GVAlumnos.PageCount), inicial + 15, GVAlumnos.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVAlumnos.PageCount), inicial + 15, GVAlumnos.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVAlumnos, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVAlumnos, "Page$" & counter.ToString)
            Next
        End If


     
    End Sub


    Protected Sub GVevaluaciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowDataBound


        Dim cociente As Integer = (GVevaluaciones.PageCount) / 15
        Dim moduloIndex As Integer = (GVevaluaciones.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVevaluaciones.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVevaluaciones.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVevaluaciones.PageIndex
            final = IIf((inicial + 15 <= GVevaluaciones.PageCount), inicial + 15, GVevaluaciones.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVevaluaciones.PageCount), inicial + 15, GVevaluaciones.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVevaluaciones, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVevaluaciones, "Page$" & counter.ToString)
            Next
        End If
    End Sub


    Protected Sub GVresultados_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVresultados.RowDataBound


        Dim cociente As Integer = (GVresultados.PageCount) / 15
        Dim moduloIndex As Integer = (GVresultados.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVresultados.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVresultados.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVresultados.PageIndex
            final = IIf((inicial + 15 <= GVresultados.PageCount), inicial + 15, GVresultados.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVresultados.PageCount), inicial + 15, GVresultados.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVresultados, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVresultados, "Page$" & counter.ToString)
            Next
        End If
    End Sub



    Protected Sub GVAlumnos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVAlumnos.RowCreated

        Dim cociente As Integer = (GVAlumnos.PageCount / 15)
        Dim moduloIndex As Integer = (GVAlumnos.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVAlumnos.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVAlumnos.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVAlumnos.PageIndex
                final = IIf((inicial + 15 <= GVAlumnos.PageCount), inicial + 15, GVAlumnos.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVAlumnos.PageCount, inicial + 15, GVAlumnos.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVAlumnos.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub


    Protected Sub GVevaluaciones_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowCreated

        Dim cociente As Integer = (GVevaluaciones.PageCount / 15)
        Dim moduloIndex As Integer = (GVevaluaciones.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVevaluaciones.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVevaluaciones.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVevaluaciones.PageIndex
                final = IIf((inicial + 15 <= GVevaluaciones.PageCount), inicial + 15, GVevaluaciones.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVevaluaciones.PageCount, inicial + 15, GVevaluaciones.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVevaluaciones.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub



    Protected Sub GVresultados_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVresultados.RowCreated

        Dim cociente As Integer = (GVresultados.PageCount / 15)
        Dim moduloIndex As Integer = (GVresultados.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVresultados.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVresultados.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVresultados.PageIndex
                final = IIf((inicial + 15 <= GVresultados.PageCount), inicial + 15, GVresultados.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVresultados.PageCount, inicial + 15, GVresultados.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVresultados.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

End Class
