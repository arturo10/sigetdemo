﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master"
    AutoEventWireup="true" CodeFile="Inicio.aspx.cs" ValidateRequest="false"
    Inherits="administrador_Configuracion_Inicio" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>



<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="form-horizontal">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading  bg-themeprimary" style="color: white;"><b>Mensaje de Aviso</b> </div>
                                    <div class="panel-body">

                                        <div class="col-lg-8">

                                            <asp:TextBox runat="server" ID="editorPaginaInicio" ClientIDMode="Static" TextMode="MultiLine">
                                            </asp:TextBox>

                                        </div>
                                        <div class="col-lg-4">
                                            <label class="col-lg-8 label-control">
                                                Fecha Inicio:
                                            </label>
                                            <div class="form-group">
                                                <asp:TextBox runat="server" ID="FechaInicioMensaje" CssClass="form-control datepickerGlobal"
                                                    data-date-format="dd/mm/yyyy"></asp:TextBox>
                                            </div>
                                            <label class="col-lg-8 label-control">
                                                Fecha Final:
                                            </label>
                                            <div class="form-group">
                                                <asp:TextBox ID="FechaFinalMensaje" runat="server" CssClass="form-control datepickerGlobal"
                                                    data-date-format="dd/mm/yyyy"></asp:TextBox>
                                            </div>
                                        
                                            <div class="form-group">
                                                   <asp:LinkButton ID="btnSaveMessage" runat="server" OnClick="btnSaveMessage_Click"
                                                                                    CssClass="btnCrearCal btn  btn-lg btn-labeled btn-success">
                                                                             <i class="btn-label fa fa-check"></i>Guardar aviso 
                                                                                </asp:LinkButton>
                                            </div>



                                        </div>
                                        <div class="form-group">
                                            <uc1:msgInfo runat="server" ID="msgInfo1" />
                                            <uc1:msgError runat="server" ID="msgError1" />
                                            <uc1:msgSuccess runat="server" ID="msgSuccess1" />
                                        </div>
                                </div>
                            </div>

                            </div>


                               <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading  bg-themeprimary" style="color: white;"><b>Configuración Cliente</b> </div>
                                    <div class="panel-body">

                                          <div class="col-lg-12">

                                                <div class="dd dd-draghandle bordered">
                                                    <ol class="dd-list">
                                                        <li class="dd-item dd2-item" data-id="13">
                                                            <div class="dd-handle dd2-handle">
                                                                <i class=" fa fa-arrow-left "></i>

                                                                <i class="drag-icon fa fa-arrows-alt "></i>
                                                            </div>

                                                            <div class="dd2-content">
                                                                <b>Ícono del cliente</b>
                                                            </div>
                                                        </li>
                                                        <li>

                                                            <div class="row">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-body">
                                                                        <div class="form-horizontal">
                                                                            <div class="form-group">
                                                                                <label class="col-lg-3 control-label">Tipo de enlace</label>
                                                                                <div class="col-lg-6">
                                                                                    <asp:DropDownList ID="DdlTipo_Logo" runat="server" AutoPostBack="true"
                                                                                        OnSelectedIndexChanged="DdlTipo_Logo_SelectedIndexChanged"
                                                                                        CssClass="form-control">
                                                                                        <asp:ListItem Value="-1">Desactivado</asp:ListItem>
                                                                                        <asp:ListItem Value="1">Link (Activo)</asp:ListItem>
                                                                                        <asp:ListItem Value="0">Archivo (Activo) </asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                    <asp:Panel runat="server" ID="PCarga_Logo" Visible="false">
                                                                                        <asp:HyperLink ID="HlCarga_Logo" runat="server"
                                                                                            CssClass="hyperlink"
                                                                                            Target="_blank"
                                                                                            Visible="False">Hyperlink</asp:HyperLink>

                                                                                        <asp:AsyncFileUpload Style="width: 50% !important;"
                                                                                            OnUploadedComplete="FuCarga_Logo_UploadedComplete"
                                                                                            runat="server" ID="FuCarga_Logo"
                                                                                            Width="800" />

                                                                                    </asp:Panel>

                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">

                                                                                <div class="col-lg-12">
                                                                                    <asp:Panel runat="server" Visible="false" ID="PLink_Logo">
                                                                                        <asp:TextBox ID="TxtLink_Logo" runat="server"
                                                                                            CssClass="form-control"></asp:TextBox>
                                                                                    </asp:Panel>
                                                                                </div>

                                                                            </div>
                                                                           
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ol>
                                                </div>
                                            </div>


                                          <div class="form-group">
                                                   <asp:LinkButton ID="btnGuardarIconoCliente" runat="server" OnClick="btnGuardarIconoCliente_Click"
                                                                                    CssClass="btnCrearCal btn  btn-lg btn-labeled btn-success">
                                                                             <i class="btn-label fa fa-check"></i>Guardar ícono del cliente
                                                                                </asp:LinkButton>
                                            </div>


                                        </div>
                                    </div>
                                   </div>




                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading  bg-themeprimary" style="color: white;"><b>Configuración de botones</b> </div>
                                    <div class="panel-body">

                                        <div class="col-lg-6">

                                            <div class="dd dd-draghandle bordered">
                                                <ol class="dd-list">
                                                    <li class="dd-item dd2-item" data-id="13">
                                                        <div class="dd-handle dd2-handle">
                                                            <i class=" fa fa-arrow-left "></i>

                                                            <i class="drag-icon fa fa-arrows-alt "></i>
                                                        </div>

                                                        <div class="dd2-content">
                                                            <b>Botón Extremo Izquierdo</b>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <img src="<%=Siget.Config.Global.urlBase%>/Resources/Customization/imagenes/botonEI.png" class="col-lg-12 col-xs-12 col-md-12 col-sm-12" />

                                                        </div>
                                                        <div class="row">
                                                            <div class="panel panel-default">
                                                                <div class="panel-body">


                                                                    <div class="form-horizontal">
                                                                        <div class="form-group">
                                                                            <label class="col-lg-2 control-label">Tipo de enlace</label>
                                                                            <div class="col-lg-4">
                                                                                <asp:DropDownList ID="DdlTipo_Boton1" runat="server" AutoPostBack="true"
                                                                                    OnSelectedIndexChanged="DdlTipo_Boton1_SelectedIndexChanged"
                                                                                    CssClass="form-control">
                                                                                  <asp:ListItem Value="-1">Desactivado</asp:ListItem>
                                                                                        <asp:ListItem Value="1">Link (Activo)</asp:ListItem>
                                                                                        <asp:ListItem Value="0">Archivo (Activo) </asp:ListItem>
                                                                                </asp:DropDownList>


                                                                            </div>

                                                                            <div class="col-lg-6">
                                                                                <asp:Panel runat="server" Visible="false" ID="PLink_Boton1">
                                                                                    <asp:TextBox ID="TxtLink_Boton1" runat="server" placeholder="Introduce el link aquí"
                                                                                        CssClass="form-control"></asp:TextBox>
                                                                                </asp:Panel>

                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <asp:Panel runat="server" ID="PCarga_Boton1" Visible="false">
                                                                                <asp:HyperLink ID="HlCarga_Boton1" runat="server"
                                                                                    CssClass="hyperlink"
                                                                                    Target="_blank"
                                                                                    Visible="False">Hyperlink</asp:HyperLink>

                                                                                <asp:AsyncFileUpload Style="width: 50% !important;" runat="server"
                                                                                    ID="FuCarga_Boton1"
                                                                                    OnUploadedComplete="FuCarga_Boton1_UploadedComplete"
                                                                                    CssClass="col-lg-offset-1"
                                                                                    Width="500" />

                                                                            </asp:Panel>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="col-lg-2 control-label">Texto del botón</label>
                                                                            <div class="col-lg-6">
                                                                                <asp:TextBox ID="TxtNombreBoton1" runat="server"
                                                                                    CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>

                                                    <li class="dd-item dd2-item" data-id="14">
                                                        <div class="dd-handle dd2-handle">
                                                            <i class="normal-icon fa fa-align-center"></i>

                                                            <i class="drag-icon fa fa-arrows-alt "></i>
                                                        </div>
                                                        <div class="dd2-content"><b>Botón Central Izquierdo</b></div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <img src="<%=Siget.Config.Global.urlBase%>/Resources/Customization/imagenes/botonCI.png"
                                                                class="col-lg-12 col-xs-12 col-md-12 col-sm-12" />

                                                        </div>
                                                        <div class="row">
                                                            <div class="panel panel-default">
                                                                <div class="panel-body">


                                                                    <div class="form-horizontal">
                                                                        <div class="form-group">
                                                                            <label class="col-lg-2 control-label">Tipo de enlace</label>
                                                                            <div class="col-lg-4">

                                                                                <asp:DropDownList ID="DdlTipo_Boton2" runat="server" AutoPostBack="true"
                                                                                    CssClass="form-control"
                                                                                    OnSelectedIndexChanged="DdlTipo_Boton2_SelectedIndexChanged">
                                                                                  <asp:ListItem Value="-1">Desactivado</asp:ListItem>
                                                                                        <asp:ListItem Value="1">Link (Activo)</asp:ListItem>
                                                                                        <asp:ListItem Value="0">Archivo (Activo) </asp:ListItem>
                                                                                </asp:DropDownList>


                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <asp:Panel runat="server" Visible="false" ID="PLink_Boton2">
                                                                                    <asp:TextBox ID="TxtLink_Boton2" runat="server" placeholder="Introduce el link aquí"
                                                                                        CssClass="form-control "></asp:TextBox>
                                                                                </asp:Panel>
                                                                            </div>


                                                                        </div>

                                                                        <div class="form-group">
                                                                            <asp:Panel runat="server" ID="PCarga_Boton2" Visible="false">
                                                                                <asp:HyperLink ID="HlCarga_Boton2" runat="server"
                                                                                    CssClass="hyperlink"
                                                                                    Target="_blank"
                                                                                    Visible="False">Hyperlink</asp:HyperLink>
                                                                                <asp:AsyncFileUpload Style="width: 50% !important;" CssClass="col-lg-offset-1"
                                                                                    OnUploadedComplete="FuCarga_Boton2_UploadedComplete"
                                                                                    runat="server" ID="FuCarga_Boton2"
                                                                                    Width="500" />
                                                                            </asp:Panel>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label class="col-lg-2 control-label">Texto del botón</label>
                                                                            <div class="col-lg-6">
                                                                                <asp:TextBox ID="TxtNombreBoton2" runat="server"
                                                                                    CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ol>
                                            </div>


                                        </div>
                                        <div class="col-lg-6">

                                            <div class="dd dd-draghandle bordered">
                                                <ol class="dd-list">
                                                    <li class="dd-item dd2-item" data-id="15">
                                                        <div class="dd-handle dd2-handle">
                                                            <i class="normal-icon fa fa-align-center "></i>

                                                            <i class="drag-icon fa fa-arrows-alt "></i>
                                                        </div>
                                                        <div class="dd2-content"><b>Botón Central Derecho</b></div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <img src="<%=Siget.Config.Global.urlBase%>/Resources/Customization/imagenes/botonCD.png" class="col-lg-12 col-xs-12 col-md-12 col-sm-12" />
                                                        </div>
                                                        <div class="row">
                                                            <div class="panel panel-default">
                                                                <div class="panel-body">
                                                                    <div class="form-horizontal">
                                                                        <div class="form-group">
                                                                            <label class="col-lg-2 control-label">Tipo de enlace</label>
                                                                            <div class="col-lg-4">
                                                                                <asp:DropDownList ID="DdlTipo_Boton3" runat="server" AutoPostBack="true"
                                                                                    CssClass="form-control" OnSelectedIndexChanged="DdlTipo_Boton3_SelectedIndexChanged">
                                                                                  <asp:ListItem Value="-1">Desactivado</asp:ListItem>
                                                                                        <asp:ListItem Value="1">Link (Activo)</asp:ListItem>
                                                                                        <asp:ListItem Value="0">Archivo (Activo) </asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <asp:Panel runat="server" Visible="false" ID="PLink_Boton3">
                                                                                    <asp:TextBox ID="TxtLink_Boton3" runat="server" placeholder="Introduce el link aquí"
                                                                                        CssClass="form-control "></asp:TextBox>
                                                                                </asp:Panel>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">

                                                                            <asp:Panel runat="server" ID="PCarga_Boton3" Visible="false">
                                                                                <asp:HyperLink ID="HlCarga_Boton3" runat="server"
                                                                                    CssClass="hyperlink"
                                                                                    Target="_blank"
                                                                                    Visible="False">Hyperlink</asp:HyperLink>
                                                                                <asp:AsyncFileUpload Style="width: 50% !important;" runat="server"
                                                                                    OnUploadedComplete="FuCarga_Boton3_UploadedComplete"
                                                                                    ID="FuCarga_Boton3" CssClass="col-lg-offset-1"
                                                                                    Width="500" />
                                                                            </asp:Panel>


                                                                        </div>


                                                                        <div class="form-group">
                                                                            <label class="col-lg-2 control-label">Texto del botón</label>
                                                                            <div class="col-lg-6">
                                                                                <asp:TextBox ID="TxtNombreBoton3" runat="server"
                                                                                    CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="dd-item dd2-item" data-id="15">
                                                        <div class="dd-handle dd2-handle">
                                                            <i class="normal-icon fa fa-arrow-right "></i>

                                                            <i class="drag-icon fa fa-arrows-alt "></i>
                                                        </div>
                                                        <div class="dd2-content"><b>Botón Extremo Derecho</b></div>
                                                    </li>
                                                    <li>
                                                        <div class="row">
                                                            <img src="<%=Siget.Config.Global.urlBase%>/Resources/Customization/imagenes/botonED.png" class="col-lg-12 col-xs-12 col-md-12 col-sm-12" />
                                                        </div>
                                                        <div class="row">
                                                            <div class="panel panel-default">
                                                                <div class="panel-body">

                                                                    <div class="form-horizontal">
                                                                        <div class="form-group">
                                                                            <label class="col-lg-2 control-label">Tipo de enlace</label>
                                                                            <div class="col-lg-4">
                                                                                <asp:DropDownList ID="DdlTipo_Boton4" runat="server" AutoPostBack="true"
                                                                                     OnSelectedIndexChanged="DdlTipo_Boton4_SelectedIndexChanged"
                                                                                    CssClass="form-control">
                                                                                 <asp:ListItem Value="-1">Desactivado</asp:ListItem>
                                                                                        <asp:ListItem Value="1">Link (Activo)</asp:ListItem>
                                                                                        <asp:ListItem Value="0">Archivo (Activo) </asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="col-lg-6">
                                                                                <asp:Panel runat="server" Visible="false" ID="PLink_Boton4">
                                                                                    <asp:TextBox ID="TxtLink_Boton4" runat="server" placeholder="Introduce el link aquí"
                                                                                        CssClass="form-control "></asp:TextBox>
                                                                                </asp:Panel>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <asp:Panel runat="server" ID="PCarga_Boton4" Visible="false">
                                                                                <asp:HyperLink ID="HlCarga_Boton4" runat="server"
                                                                                    CssClass="hyperlink"
                                                                                    Target="_blank"
                                                                                    Visible="False">Hyperlink</asp:HyperLink>
                                                                                <asp:AsyncFileUpload Style="width: 50% !important;" runat="server"
                                                                                    OnUploadedComplete="FuCarga_Boton4_UploadedComplete"
                                                                                    ID="FuCarga_Boton4" CssClass="col-lg-offset-1"
                                                                                    Width="500" />
                                                                            </asp:Panel>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-lg-2 control-label">Texto del botón</label>
                                                                            <div class="col-lg-6">
                                                                                <asp:TextBox ID="TxtNombreBoton4" runat="server"
                                                                                    CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>

                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <asp:LinkButton ID="btnSaveChanges" runat="server" OnClick="btnSaveChanges_Click"
                                    CssClass="btnCrearCal btn  btn-lg btn-labeled btn-success">
                                <i class="btn-label fa fa-check"></i>Guardar configuraciones de botones
                                </asp:LinkButton>

                            </div>

                            <div class="col-lg-12">
                                <uc1:msgInfo runat="server" ID="msgInfo" />
                                <uc1:msgError runat="server" ID="msgError" />
                                <uc1:msgSuccess runat="server" ID="msgSuccess" />
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScriptContent" runat="Server">
    <script type="text/javascript">


        Sys.Application.add_load(LoadHandler);

        function LoadHandler(sender, args0) {

            $(function () {

                var editorPaginaInicio = CKEDITOR.replace('editorPaginaInicio', {
                    language: 'es',
                    height: 250
                });

                editorPaginaInicio.on('change', function (evt) {
                    $('#editorPaginaInicio').val(evt.editor.getData());
                });

            });
        }




    </script>

</asp:Content>

