﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false" CodeFile="paginaInicio.aspx.vb" Inherits="administrador_Configuracion_paginaInicio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="row">
        <div class="form-horizontal">
            <div class="panel panel-default">
                <div class="panel-body">



                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading  bg-themeprimary" style="color:white;"><b>Mensaje de Aviso</b> </div>
                            <div class="panel-body">

                                <div class="col-lg-8">

                                <asp:TextBox runat="server" ID="editorPaginaInicio" ClientIDMode="Static" TextMode="MultiLine">
                                </asp:TextBox>
                                
                                </div>
                                <div class="col-lg-4">
                                    <label class="col-lg-8 label-control">
                                           Fecha Inicio:
                                    </label>
                                    <div class="form-group">
                                          <asp:TextBox runat="server" ID="FechaInicioMensaje" CssClass="form-control datepickerGlobal" 
                                      data-date-format="dd/mm/yyyy"></asp:TextBox>
                                    </div>
                                     <label class="col-lg-8 label-control">
                                           Fecha Final:
                                    </label>
                                     <div class="form-group">
                                          <asp:TextBox ID="FechaFinalMensaje"  runat="server" CssClass="form-control datepickerGlobal" 
                                      data-date-format="dd/mm/yyyy"></asp:TextBox>
                                    </div>
                                  
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading  bg-themeprimary" style="color:white;"><b>Configuración de botones</b> </div>
                        <div class="panel-body">

                            <div class="col-lg-6">

                                <div class="dd dd-draghandle bordered">
                                    <ol class="dd-list">
                                        <li class="dd-item dd2-item" data-id="13">
                                            <div class="dd-handle dd2-handle">
                                                <i class=" fa fa-arrow-left "></i>

                                                <i class="drag-icon fa fa-arrows-alt "></i>
                                            </div>

                                            <div class="dd2-content">
                                                <b>Botón Extremo Izquierdo</b>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="row">
                                                <img src="<%=Siget.Config.Global.urlBase%>/Resources/Customization/imagenes/botonEI.png" class="col-lg-12 col-xs-12 col-md-12 col-sm-12" />

                                            </div>
                                            <div class="row">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">


                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">Tipo de enlace</label>
                                                                <div class="col-lg-4">
                                                                    <asp:DropDownList ID="DropDownList112" runat="server" AutoPostBack="true"
                                                                        CssClass="form-control">
                                                                        <asp:ListItem Value="-1">Desactivado</asp:ListItem>
                                                                        <asp:ListItem Value="1">Link</asp:ListItem>
                                                                        <asp:ListItem Value="0">Archivo</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="dd-item dd2-item" data-id="14">
                                            <div class="dd-handle dd2-handle">
                                                <i class="normal-icon fa fa-align-center"></i>

                                                <i class="drag-icon fa fa-arrows-alt "></i>
                                            </div>
                                            <div class="dd2-content"><b>Botón Central Izquierdo</b></div>
                                        </li>
                                        <li>
                                            <div class="row">
                                                <img src="<%=Siget.Config.Global.urlBase%>/Resources/Customization/imagenes/botonCI.png" class="col-lg-12 col-xs-12 col-md-12 col-sm-12" />

                                            </div>
                                            <div class="row">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">


                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">Tipo de enlace</label>
                                                                <div class="col-lg-4">

                                                                    <asp:DropDownList ID="DropDownList21" runat="server" AutoPostBack="true"
                                                                        CssClass="form-control">
                                                                        <asp:ListItem Value="-1">Desactivado</asp:ListItem>
                                                                        <asp:ListItem Value="1">Link</asp:ListItem>
                                                                        <asp:ListItem Value="0">Archivo</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>



                                    </ol>
                                </div>


                            </div>
                            <div class="col-lg-6">

                                <div class="dd dd-draghandle bordered">
                                    <ol class="dd-list">
                                        <li class="dd-item dd2-item" data-id="15">
                                            <div class="dd-handle dd2-handle">
                                                <i class="normal-icon fa fa-align-center "></i>

                                                <i class="drag-icon fa fa-arrows-alt "></i>
                                            </div>
                                            <div class="dd2-content"><b>Botón Central Derecho</b></div>
                                        </li>
                                        <li>
                                            <div class="row">
                                                <img src="<%=Siget.Config.Global.urlBase%>/Resources/Customization/imagenes/botonCD.png" class="col-lg-12 col-xs-12 col-md-12 col-sm-12" />
                                            </div>
                                            <div class="row">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">Tipo de enlace</label>
                                                                <div class="col-lg-4">
                                                                    <asp:DropDownList ID="DdlTipo_Logo" runat="server" AutoPostBack="true"
                                                                        CssClass="form-control">
                                                                        <asp:ListItem Value="-1">Desactivado</asp:ListItem>
                                                                        <asp:ListItem Value="1">Link</asp:ListItem>
                                                                        <asp:ListItem Value="0">Archivo</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="dd-item dd2-item" data-id="15">
                                            <div class="dd-handle dd2-handle">
                                                <i class="normal-icon fa fa-arrow-right "></i>

                                                <i class="drag-icon fa fa-arrows-alt "></i>
                                            </div>
                                            <div class="dd2-content"><b>Botón Extremo Derecho</b></div>
                                        </li>
                                        <li>
                                            <div class="row">
                                                <img src="<%=Siget.Config.Global.urlBase%>/Resources/Customization/imagenes/botonED.png" class="col-lg-12 col-xs-12 col-md-12 col-sm-12" />
                                            </div>
                                            <div class="row">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">

                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <label class="col-lg-2 control-label">Tipo de enlace</label>
                                                                <div class="col-lg-4">
                                                                    <asp:DropDownList ID="DropDownList12" runat="server" AutoPostBack="true"
                                                                        CssClass="form-control">
                                                                        <asp:ListItem Value="-1">Desactivado</asp:ListItem>
                                                                        <asp:ListItem Value="1">Link</asp:ListItem>
                                                                        <asp:ListItem Value="0">Archivo</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                       
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>

                </div>
            </div>
        </div>
    </div>







</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScriptContent" Runat="Server">
    <script type="text/javascript">

        
        Sys.Application.add_load(LoadHandler);

        function LoadHandler(sender, args0) {

            $(function () {

                var editorPaginaInicio = CKEDITOR.replace('editorPaginaInicio', {
                    language: 'es',
                    height: 250
                });

                editorPaginaInicio.on('change', function (evt) {
                    $('#editorPaginaInicio').val(evt.editor.getData());
                });

            });
        }
                


      
    </script>
</asp:Content>

