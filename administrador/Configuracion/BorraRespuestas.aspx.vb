﻿Imports Siget
Imports System.Data.SqlClient
Imports System.Data
Imports System.IO

Partial Class administrador_AgruparReactivos
    Inherits System.Web.UI.Page



#Region "LOAD"

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

    End Sub



#End Region
   
#Region "DATABOUND"

    Protected Sub DDLinstitucionBorrarActividades_DataBound(sender As Object, e As EventArgs) Handles DDLinstitucionBorrarActividades.DataBound
        DDLinstitucionBorrarActividades.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Institucion"), 0))
        If DDLinstitucionBorrarActividades.Items.Count = 2 Then
            DDLinstitucionBorrarActividades.SelectedIndex = 1
        End If
        DDLplantelBorrarActividades.DataBind()
    End Sub

    Protected Sub DDLplantelBorrarActividades_DataBound(sender As Object, e As EventArgs) Handles DDLplantelBorrarActividades.DataBound
        DDLplantelBorrarActividades.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Plantel"), 0))
        If DDLplantelBorrarActividades.Items.Count = 2 Then
            DDLplantelBorrarActividades.SelectedIndex = 1

        End If
        DDLNivelBorrarActividades.DataBind()
        gvEvaluacionesBorrarActividades.DataBind()
    End Sub

    Protected Sub DDLNivelBorrarActividades_DataBound(sender As Object, e As EventArgs) Handles DDLNivelBorrarActividades.DataBound
        DDLNivelBorrarActividades.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Nivel"), 0))
        If DDLNivelBorrarActividades.Items.Count = 2 Then
            DDLNivelBorrarActividades.SelectedIndex = 1
        End If
        DDLGradoBorrarActividades.DataBind()
    End Sub

    Protected Sub DDLGradoBorrarActividades_DataBound(sender As Object, e As EventArgs) Handles DDLGradoBorrarActividades.DataBound
        DDLGradoBorrarActividades.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_AreaConocimiento"), 0))
        If DDLGradoBorrarActividades.Items.Count = 2 Then
            DDLGradoBorrarActividades.SelectedIndex = 1
        End If
        DDLCicloBorrarActividades.DataBind()
        DDLgrupoBorrarActividades.DataBind()
        DDLAsignaturaBorrarActividades.DataBind()
    End Sub

    Protected Sub DDLCicloBorrarActividades_DataBound(sender As Object, e As EventArgs) Handles DDLCicloBorrarActividades.DataBound
        DDLCicloBorrarActividades.Items.Insert(0, New ListItem("---Elija el " & Config.Etiqueta.CICLO, 0))
        If DDLCicloBorrarActividades.Items.Count = 2 Then
            DDLCicloBorrarActividades.SelectedIndex = 1
        End If
    End Sub

    Protected Sub DDLAsignaturaBorrarActividades_DataBound(sender As Object, e As EventArgs) Handles DDLAsignaturaBorrarActividades.DataBound
        DDLAsignaturaBorrarActividades.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Asignatura"), 0))
        If DDLAsignaturaBorrarActividades.Items.Count = 2 Then
            DDLAsignaturaBorrarActividades.SelectedIndex = 1
        End If
        DDLcalificacionBorrarActividades.DataBind()
    End Sub



    Protected Sub DDLCalificacionBorrarActividades_DataBound(sender As Object, e As EventArgs) Handles DDLcalificacionBorrarActividades.DataBound
        DDLcalificacionBorrarActividades.Items.Insert(0, New ListItem("--Elija una calificación", 0))
        If DDLcalificacionBorrarActividades.Items.Count = 2 Then
            DDLcalificacionBorrarActividades.SelectedIndex = 1
        End If
        gvEvaluacionesBorrarActividades.DataBind()
    End Sub


    Protected Sub DDLgrupoBorrarActividades_DataBound(sender As Object, e As EventArgs) Handles DDLgrupoBorrarActividades.DataBound
        DDLgrupoBorrarActividades.Items.Insert(0, New ListItem(Lang_Config.Translate("general", "Opcion_Grupo"), 0))
        If DDLgrupoBorrarActividades.Items.Count = 2 Then
            DDLgrupoBorrarActividades.SelectedIndex = 1
        End If
        GValumnosterminados.DataBind()
    End Sub


#End Region

#Region "ROW_DATA_BOUND"



    Protected Sub GValumnosterminados_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GValumnosterminados.RowDataBound


        Dim cociente As Integer = (GValumnosterminados.PageCount) / 15
        Dim moduloIndex As Integer = (GValumnosterminados.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GValumnosterminados.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GValumnosterminados.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GValumnosterminados.PageIndex
            final = IIf((inicial + 15 <= GValumnosterminados.PageCount), inicial + 15, GValumnosterminados.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GValumnosterminados.PageCount), inicial + 15, GValumnosterminados.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GValumnosterminados, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GValumnosterminados, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub gvEvaluacionesBorrarActividades_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvEvaluacionesBorrarActividades.RowDataBound


        Dim cociente As Integer = (gvEvaluacionesBorrarActividades.PageCount) / 15
        Dim moduloIndex As Integer = (gvEvaluacionesBorrarActividades.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((gvEvaluacionesBorrarActividades.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = gvEvaluacionesBorrarActividades.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = gvEvaluacionesBorrarActividades.PageIndex
            final = IIf((inicial + 15 <= gvEvaluacionesBorrarActividades.PageCount), inicial + 15, gvEvaluacionesBorrarActividades.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= gvEvaluacionesBorrarActividades.PageCount), inicial + 15, gvEvaluacionesBorrarActividades.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(gvEvaluacionesBorrarActividades, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(gvEvaluacionesBorrarActividades, "Page$" & counter.ToString)
            Next
        End If
    End Sub

#End Region
   
#Region "ROW_CREATED"


    Protected Sub GValumnosterminados_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GValumnosterminados.RowCreated

        Dim cociente As Integer = (GValumnosterminados.PageCount / 15)
        Dim moduloIndex As Integer = (GValumnosterminados.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GValumnosterminados.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GValumnosterminados.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GValumnosterminados.PageIndex
                final = IIf((inicial + 15 <= GValumnosterminados.PageCount), inicial + 15, GValumnosterminados.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GValumnosterminados.PageCount, inicial + 15, GValumnosterminados.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GValumnosterminados.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub gvEvaluacionesBorrarActividades_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles gvEvaluacionesBorrarActividades.RowCreated

        Dim cociente As Integer = (gvEvaluacionesBorrarActividades.PageCount / 15)
        Dim moduloIndex As Integer = (gvEvaluacionesBorrarActividades.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((gvEvaluacionesBorrarActividades.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = gvEvaluacionesBorrarActividades.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = gvEvaluacionesBorrarActividades.PageIndex
                final = IIf((inicial + 15 <= gvEvaluacionesBorrarActividades.PageCount), inicial + 15, gvEvaluacionesBorrarActividades.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= gvEvaluacionesBorrarActividades.PageCount, inicial + 15, gvEvaluacionesBorrarActividades.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = gvEvaluacionesBorrarActividades.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub


#End Region

#Region "DELETE"


    Protected Sub BtnBorrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBorrarActividades.Click
        Try
            'un archivo con el mismo nombre de otro usuario, entonces si le restringe el subirlo --> ESTO YA NO APLICARIA?
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()

                Using cmd As SqlCommand = New SqlCommand("EliminaRespuestas")
                    cmd.Connection = conn
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.AddWithValue("IdEvaluacion", gvEvaluacionesBorrarActividades.SelectedDataKey("IdEvaluacion"))
                    cmd.Parameters.AddWithValue("IdAlumno", GValumnosterminados.SelectedDataKey("IdAlumno").ToString())
                    cmd.ExecuteNonQuery()
                End Using
            End Using


            CType(New MessageSuccess(GValumnosterminados, Me.GetType, "El resultado del " + Config.Etiqueta.ALUMNO + " alumno ha sido eliminado"), MessageSuccess).show()
            GValumnosterminados.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(GValumnosterminados, Me.GetType, "Verifique los campos"), MessageError).show()
        End Try
    End Sub




#End Region
   
#Region "SELECTED_INDEX_CHANGED"

    Protected Sub GValumnosterminados_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GValumnosterminados.SelectedIndexChanged
        btnBorrarActividades.Visible = True
    End Sub

    Protected Sub GValumnosterminados_DataBound(sender As Object, e As EventArgs) Handles GValumnosterminados.DataBound
        If GValumnosterminados.Rows.Count = 0 Then
            btnBorrarActividades.Visible = False
            GValumnosterminados.SelectedIndex = -1
        Else
            btnBorrarActividades.Visible = True
        End If

    End Sub


#End Region

    Protected Sub gvEvaluacionesBorrarActividades_PageIndexChanged(sender As Object, e As EventArgs) Handles gvEvaluacionesBorrarActividades.PageIndexChanged
        gvEvaluacionesBorrarActividades.SelectedIndex = -1
    End Sub

    Protected Sub GValumnosterminados_PageIndexChanged(sender As Object, e As EventArgs) Handles GValumnosterminados.PageIndexChanged
        GValumnosterminados.SelectedIndex = -1
    End Sub

End Class
