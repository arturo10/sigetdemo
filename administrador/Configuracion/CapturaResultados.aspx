﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false" CodeFile="CapturaResultados.aspx.vb" Inherits="administrador_AgruparReactivos" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="widget flat radius-bordered">
                <div class="widget-header bg-themeprimary">
                    <span class="widget-caption">Captura resultados de <%=Lang_Config.Translate("general","Alumno") %>s</span>
                </div>

                <div class="widget-body">
                    <div class="widget-main ">
                        <div class="tabbable">
                            <ul class="nav nav-tabs tabs-flat" id="myTab11">
                                <li id="Cal" class="active">
                                    <a data-toggle="tab" href="#AreaSeleccionaActividad">  
                       
                                        Selecciona la actividad 
                                    </a>
                                </li>
                                <li>
                                    <a id="LSeleccionaAlumno" data-toggle="tab" href="#AreaSeleccionaAlumno">
                                      
                                        Selecciona al <%=Lang_Config.Translate("general", "Alumno")%>
                                    </a>
                                </li>

                                



                            </ul>
                            <div class="tab-content tabs-flat">

                                <div id="AreaSeleccionaActividad" class="tab-pane in active">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <div class="form-horizontal">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">

                                                        <div class="col-lg-5">
                                                           <div class="form-group">
                                                                <asp:Label ID="Label1" runat="server" CssClass="col-lg-3 control-label" >
                                                                    Ciclo
                                                                </asp:Label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                                                                        DataValueField="IdCicloEscolar" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <asp:Label ID="Label2" runat="server"  CssClass="col-lg-3 control-label">
                                                                    Nivel
                                                                </asp:Label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                                                                        CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <asp:Label ID="Label3" runat="server"  CssClass="col-lg-3 control-label" >
                                                                    <%=Lang_Config.Translate("general","Grado") %>
                                                                </asp:Label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                                                                        CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <asp:Label ID="Label4" runat="server"  CssClass="col-lg-3 control-label" >
                                                                     <%=Lang_Config.Translate("general","Asignatura") %>
                                                                </asp:Label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSasignaturas" DataTextField="Materia"
                                                                        DataValueField="IdAsignatura" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-lg-3 control-label">Calificación</label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDScalificaciones" DataTextField="Cal"
                                                                        DataValueField="IdCalificacion" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="col-lg-7">

                                                            <div class="form-group">
                                                                <div class="col-lg-12">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="GVevaluaciones" runat="server" AllowPaging="True"
                                                                        AllowSorting="True" AutoGenerateColumns="False"
                                                                        DataKeyNames="IdEvaluacion" DataSourceID="SDSevaluaciones"
                                                                        GridLines="Vertical" PageSize="5"
                                                                        CellPadding="3"
                                                                        CssClass="table table-striped table-bordered"
                                                                        Height="17px"
                                                                        Style="font-size: x-small; text-align: left;">
                                                                        <Columns>
                                                                           
                                                                            <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                                                                                InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion" />
                                                                            <asp:BoundField DataField="IdCalificacion" HeaderText="Id Calificacion"
                                                                                SortExpression="IdCalificacion" />
                                                                            <asp:BoundField DataField="ClaveBateria" HeaderText="Nombre de la Actividad"
                                                                                SortExpression="ClaveBateria" />
                                                                            <%--<asp:BoundField DataField="Resultado" HeaderText="Resultado" 
                                        SortExpression="Resultado" />--%>
                                                                            <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                                                                                SortExpression="Porcentaje">
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                                                HeaderText="Inicia" SortExpression="InicioContestar" />
                                                                            <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                                                HeaderText="Termina" SortExpression="FinContestar" />
                                                                            <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                                                            <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                                                SortExpression="Estatus" />
                                                                        </Columns>
                                                                        <PagerTemplate>
                                                                            <ul runat="server" id="Pag" class="pagination">
                                                                            </ul>
                                                                        </PagerTemplate>
                                                                        <PagerStyle HorizontalAlign="Center" />
                                                                        <SelectedRowStyle CssClass="row-selected" />
                                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                    </asp:GridView>
                                                                </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>


                                <div id="AreaSeleccionaAlumno" class="tab-pane">
                                    <asp:UpdatePanel runat="server">
                                        <ContentTemplate>
                                            <div class="form-horizontal">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                
                                                           <div class="form-group">
                                                                <asp:Label ID="Label5" runat="server" CssClass="col-lg-2 control-label">
                                                                    Institución
                                                                </asp:Label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                                                        DataValueField="IdInstitucion" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                               <asp:Label ID="Label6" runat="server"  CssClass="col-lg-2 control-label" >
                                                                     <%=Lang_Config.Translate("general","Plantel") %>
                                                               </asp:Label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                                                        DataValueField="IdPlantel" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>


                                                            <div class="form-group">
                                                                <asp:Label ID="Label7" runat="server"  CssClass="col-lg-2 control-label" >
                                                                    Grupo
                                                                </asp:Label>
                                                                <div class="col-lg-8">
                                                                    <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                                                                        DataSourceID="SDSgrupos" DataTextField="Descripcion" DataValueField="IdGrupo"
                                                                        CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                     
                                                            <div class="form-group">
                                                                <div class="table-responsive col-lg-10 col-lg-offset-1">
                                                                    <asp:GridView ID="GVAlumnos" runat="server"
                                                                        AllowSorting="True"
                                                                        AutoGenerateColumns="False"
                                                                        Caption="<h3>Seleccione al ALUMNO</h3>"
                                                                        DataKeyNames="IdAlumno"
                                                                        DataSourceID="SDSlistado"
                                                                        PageSize="5"
                                                                        CellPadding="3"
                                                                        CssClass="table table-striped table-bordered"
                                                                        Height="17px"
                                                                        Style="font-size: x-small; text-align: left;">
                                                                        <Columns>
                                                                           
                                                                            <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                                                                                InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno" />
                                                                            <asp:BoundField DataField="ApePaterno" HeaderText="Ap. Paterno"
                                                                                SortExpression="ApePaterno" />
                                                                            <asp:BoundField DataField="ApeMaterno" HeaderText="Ap. Materno"
                                                                                SortExpression="ApeMaterno" />
                                                                            <asp:BoundField DataField="Nombre" HeaderText="Nombre"
                                                                                SortExpression="Nombre" />
                                                                            <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                                                                                SortExpression="Matricula">
                                                                                <ItemStyle Width="85px" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" Visible="false" />
                                                                            <asp:BoundField DataField="FechaIngreso" DataFormatString="{0:dd/MM/yyyy}"
                                                                                HeaderText="Ingreso" SortExpression="FechaIngreso" Visible="false" />
                                                                            <asp:BoundField DataField="Login" HeaderText="Login" SortExpression="Login" Visible="false"/>
                                                                            <asp:BoundField DataField="Password" HeaderText="Password" Visible="false"
                                                                                SortExpression="Password" />
                                                                            <asp:BoundField DataField="Estatus" HeaderText="Estatus" Visible="false"
                                                                                SortExpression="Estatus" />
                                                                            <asp:BoundField DataField="Equipo" HeaderText="Equipo" SortExpression="Equipo" />
                                                                            <asp:BoundField DataField="Subequipo" HeaderText="Subequipo"
                                                                                SortExpression="Subequipo" />
                                                                        </Columns>
                                                                        <PagerTemplate>
                                                                                <ul runat="server" id="Pag" class="pagination">
                                                                                </ul>
                                                                            </PagerTemplate>
                                                                            <PagerStyle HorizontalAlign="Center" />
                                                                            <SelectedRowStyle CssClass="row-selected" />
                                                                            <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-lg-2 col-lg-offset-3 control-label">Capture los resultados</label>
                                                                <div class="col-lg-2">
                                                                    <asp:TextBox ID="TBresultado" runat="server" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Button ID="BtnAsignar" runat="server" Text="Asignar"
                                                                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                                            </div>
                                                            <div class="form-group">
                                                                  <uc1:msgSuccess runat="server" ID="msgSuccess" />
                                                                  <uc1:msgError runat="server" ID="msgError" />
                                                            </div>
                                                  
                                                        <hr />
                                                        
                                                        <div class="form-group">
                                                            <div class="col-lg-8 col-lg-offset-2">
                                                                <div class="table-responsive">
                                                                    <asp:GridView ID="GVresultados" runat="server" AllowSorting="True"
                                                                        AutoGenerateColumns="False" DataKeyNames="IdEvaluacionT"
                                                                        DataSourceID="SDSresultados" PageSize="5"
                                                                        CellPadding="3"
                                                                        CssClass="table table-striped table-bordered"
                                                                        Height="17px"
                                                                        Style="font-size: x-small; text-align: left;"
                                                                        Caption="<h3>Resultado registrado</h3>">
                                                                        <Columns>
                                                                           
                                                                            <asp:BoundField DataField="IdEvaluacionT" HeaderText="IdEvaluacionT"
                                                                                InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacionT" />
                                                                            <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                                                                                SortExpression="IdAlumno" />
                                                                            <asp:BoundField DataField="IdGrado" HeaderText="IdGrado"
                                                                                SortExpression="IdGrado" Visible="False" />
                                                                            <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo"
                                                                                SortExpression="IdGrupo" />
                                                                            <asp:BoundField DataField="IdCicloEscolar" HeaderText="IdCicloEscolar"
                                                                                SortExpression="IdCicloEscolar" />
                                                                            <asp:BoundField DataField="IdEvaluacion" HeaderText="IdActividad"
                                                                                SortExpression="IdEvaluacion" />
                                                                            <asp:BoundField DataField="TotalReactivos" HeaderText="TotalReactivos"
                                                                                SortExpression="TotalReactivos" Visible="False" />
                                                                            <asp:BoundField DataField="ReactivosContestados"
                                                                                HeaderText="ReactivosContestados" SortExpression="ReactivosContestados"
                                                                                Visible="False" />
                                                                            <asp:BoundField DataField="PuntosPosibles" HeaderText="PuntosPosibles"
                                                                                SortExpression="PuntosPosibles" Visible="False" />
                                                                            <asp:BoundField DataField="PuntosAcertados" HeaderText="PuntosAcertados"
                                                                                SortExpression="PuntosAcertados" Visible="False" />
                                                                            <asp:BoundField DataField="Resultado" HeaderText="Resultado"
                                                                                SortExpression="Resultado" />
                                                                            <asp:BoundField DataField="FechaTermino" DataFormatString="{0:dd/MM/yyyy}"
                                                                                HeaderText="Fecha Contestado" SortExpression="FechaTermino">
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                        <PagerTemplate>
                                                                            <ul runat="server" id="Pag" class="pagination">
                                                                            </ul>
                                                                        </PagerTemplate>
                                                                        <PagerStyle HorizontalAlign="Center" />
                                                                        <SelectedRowStyle CssClass="row-selected" />
                                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                    </asp:GridView>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Button ID="BtnEliminar" runat="server" Text="Eliminar" Visible="False"
                                                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                                                        </div>
                                                       
                                                             <div class="form-group">
                                                                  <uc1:msgSuccess runat="server" ID="msgSuccess1" />
                                                                  <uc1:msgError runat="server" ID="msgError1" />
                                                            </div>
                                             
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>







                    
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>



    <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [CicloEscolar] 
WHERE Estatus = 'Activo'
ORDER BY [Descripcion]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSniveles" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT [IdNivel], [Descripcion] FROM [Nivel] ORDER BY [Descripcion]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSgrados" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT IdGrado,Descripcion FROM [Grado] WHERE ([IdNivel] = @IdNivel)
and Estatus = 'Activo'">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSasignaturas" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT A.IdAsignatura, A.Descripcion + ' (' + Ar.Descripcion + ')'  as Materia
FROM Asignatura A, Area Ar
WHERE (A.IdGrado = @IdGrado) and (Ar.IdArea = A.IdArea)
ORDER BY Ar.Descripcion,A.Descripcion">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SDScalificaciones" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT C.IdCalificacion, C.Descripcion + ' (' + A.Descripcion + ')' as Cal
FROM Calificacion C, Asignatura A
WHERE C.IdCicloEscolar = @IdCicloEscolar
and A.IdAsignatura = C.IdAsignatura and A.IdAsignatura = @IdAsignatura
order by C.Consecutivo">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSinstituciones" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [Institucion] ORDER BY [Descripcion]"></asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSplanteles" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE ([IdInstitucion] = @IdInstitucion) ORDER BY [Descripcion]">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:SqlDataSource ID="SDSgrupos" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT [IdGrupo], [Descripcion] FROM [Grupo] WHERE (([IdGrado] = @IdGrado) AND ([IdPlantel] = @IdPlantel)) and ([IdCicloEscolar] = @IdCicloEscolar) ORDER BY [Descripcion]">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>


    <asp:SqlDataSource ID="SDSlistado" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select A.IdAlumno, A.ApePaterno, A.ApeMaterno, A.Nombre, A.Matricula, A.FechaIngreso, A.Email, U.Login, U.Password, A.Estatus, A.Equipo, A.Subequipo
from Alumno A, Usuario U
where A.IdGrupo = @IdGrupo and U.IdUsuario = A.IdUsuario
order by A.ApePaterno, A.ApeMaterno, A.Nombre,U.Login">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>


    <asp:SqlDataSource ID="SDSresultados" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT * FROM [EvaluacionTerminada] WHERE (([IdAlumno] = @IdAlumno) AND ([IdEvaluacion] = @IdEvaluacion) AND ([IdGrupo] = @IdGrupo))">
        <SelectParameters>
            <asp:ControlParameter ControlID="GVAlumnos" Name="IdAlumno"
                PropertyName="SelectedValue" Type="Int64" />
            <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

        <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT * FROM [Evaluacion] WHERE ([IdCalificacion] = @IdCalificacion)">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion"
                    PropertyName="SelectedValue" Type="Int64" />
            </SelectParameters>
        </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">

    


</asp:Content>

