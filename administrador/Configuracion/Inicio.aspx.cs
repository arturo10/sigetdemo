﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using Siget.Beans;
using System.IO;

public partial class administrador_Configuracion_Inicio : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Siget.Utils.Sesion.sesionAbierta();

        if (!IsPostBack)
        {
           
            CargaValores();
        }
    }

    protected void DdlTipo_Logo_SelectedIndexChanged(object sender, EventArgs e) 
    {
        ActualizaPaneles(DdlTipo_Logo, PLink_Logo, PCarga_Logo);
    }

    protected void DdlTipo_Boton1_SelectedIndexChanged(object sender, EventArgs e)
    {
        ActualizaPaneles(DdlTipo_Boton1, PLink_Boton1, PCarga_Boton1);
    }

    protected void DdlTipo_Boton2_SelectedIndexChanged(object sender, EventArgs e)
    {
        ActualizaPaneles(DdlTipo_Boton2, PLink_Boton2, PCarga_Boton2);
    }

    protected void DdlTipo_Boton3_SelectedIndexChanged(object sender, EventArgs e)
    {
        ActualizaPaneles(DdlTipo_Boton3, PLink_Boton3, PCarga_Boton3);
    }

    protected void DdlTipo_Boton4_SelectedIndexChanged(object sender, EventArgs e)
    {
        ActualizaPaneles(DdlTipo_Boton4, PLink_Boton4, PCarga_Boton4);
    }

    protected void ActualizaPaneles(DropDownList ddlTipo, Panel phrLink, Panel phrCarga)
    {
        if (ddlTipo.SelectedValue == "0")
        {
            phrLink.Visible = false;
            phrCarga.Visible = true;
        }
        else if (ddlTipo.SelectedValue == "1")
        {
            phrLink.Visible = true;
            phrCarga.Visible = false;
        }
        else
        {
            phrLink.Visible = false;
            phrCarga.Visible = false;
        }
    }

    protected void CargaValores()
    {
        if (Siget.Config.PaginaInicio.AvisoPublico != "")
        {
            editorPaginaInicio.Text = Siget.Config.PaginaInicio.AvisoPublico;
            FechaInicioMensaje.Text = Siget.Config.PaginaInicio.AvisoPublico_FechaInicio.ToShortDateString();
            FechaFinalMensaje.Text = Siget.Config.PaginaInicio.AvisoPublico_FechaFin.ToShortDateString();
        }


        DdlTipo_Logo.SelectedValue = Siget.Config.PaginaInicio.LogoCliente_Tipo.ToString();
        DdlTipo_Logo_SelectedIndexChanged(null, null);
        if (Siget.Config.PaginaInicio.LogoCliente_Tipo == 0)
        {
            HlCarga_Logo.Visible = true;
            HlCarga_Logo.Text = Siget.Config.PaginaInicio.LogoCliente_Objetivo;
            HlCarga_Logo.NavigateUrl = Siget.Config.PaginaInicio.LogoCliente_Objetivo;
        }
        else if (Siget.Config.PaginaInicio.LogoCliente_Tipo == 1)
        {
            TxtLink_Logo.Text = Siget.Config.PaginaInicio.LogoCliente_Objetivo;
        }


        DdlTipo_Boton1.SelectedValue = Siget.Config.PaginaInicio.Boton1_Tipo.ToString();
        DdlTipo_Boton1_SelectedIndexChanged(null, null);
        if (Siget.Config.PaginaInicio.Boton1_Tipo == 0)
        {
            HlCarga_Boton1.Visible = true;
            HlCarga_Boton1.Text = Siget.Config.PaginaInicio.Boton1_Objetivo;
            HlCarga_Boton1.NavigateUrl = Siget.Config.PaginaInicio.Boton1_Objetivo;
        }
        else if (Siget.Config.PaginaInicio.Boton1_Tipo == 1)
        {
            TxtLink_Boton1.Text = Siget.Config.PaginaInicio.Boton1_Objetivo;
        }
        TxtNombreBoton1.Text = Siget.Config.PaginaInicio.Boton1_Texto;


        DdlTipo_Boton2.SelectedValue = Siget.Config.PaginaInicio.Boton2_Tipo.ToString();
        DdlTipo_Boton2_SelectedIndexChanged(null, null);
        if (Siget.Config.PaginaInicio.Boton2_Tipo == 0)
        {
            HlCarga_Boton2.Visible = true;
            HlCarga_Boton2.Text = Siget.Config.PaginaInicio.Boton2_Objetivo;
            HlCarga_Boton2.NavigateUrl = Siget.Config.PaginaInicio.Boton2_Objetivo;
        }
        else if (Siget.Config.PaginaInicio.Boton2_Tipo == 1)
        {
            TxtLink_Boton2.Text = Siget.Config.PaginaInicio.Boton2_Objetivo;
        }
        TxtNombreBoton2.Text = Siget.Config.PaginaInicio.Boton2_Texto;


        DdlTipo_Boton3.SelectedValue = Siget.Config.PaginaInicio.Boton3_Tipo.ToString();
        DdlTipo_Boton3_SelectedIndexChanged(null, null);
        if (Siget.Config.PaginaInicio.Boton3_Tipo == 0)
        {
            HlCarga_Boton3.Visible = true;
            HlCarga_Boton3.Text = Siget.Config.PaginaInicio.Boton3_Objetivo;
            HlCarga_Boton3.NavigateUrl = Siget.Config.PaginaInicio.Boton3_Objetivo;
        }
        else if (Siget.Config.PaginaInicio.Boton3_Tipo == 1)
        {
            TxtLink_Boton3.Text = Siget.Config.PaginaInicio.Boton3_Objetivo;
        }


        DdlTipo_Boton4.SelectedValue = Siget.Config.PaginaInicio.Boton4_Tipo.ToString();
        DdlTipo_Boton4_SelectedIndexChanged(null, null);
        if (Siget.Config.PaginaInicio.Boton4_Tipo == 0)
        {
            HlCarga_Boton4.Visible = true;
            HlCarga_Boton4.Text = Siget.Config.PaginaInicio.Boton4_Objetivo;
            HlCarga_Boton4.NavigateUrl = Siget.Config.PaginaInicio.Boton4_Objetivo;
        }
        else if (Siget.Config.PaginaInicio.Boton4_Tipo == 1)
        {
            TxtLink_Boton4.Text = Siget.Config.PaginaInicio.Boton4_Objetivo;
        }
    }

    protected void btnSaveChanges_Click(object sender, EventArgs e)
    {
        List<string> nameOfControls = new List<string>{ "boton1", "boton2", "boton3", "boton4" };
        List<string> nameOfTypes = new List<string> {  DdlTipo_Boton1.SelectedValue, 
                                                      DdlTipo_Boton2.SelectedValue, 
                                                      DdlTipo_Boton3.SelectedValue,
                                                      DdlTipo_Boton4.SelectedValue };
        List<string> textOfLinks = new List<string>{  TxtLink_Boton1.Text, 
                                                      TxtLink_Boton2.Text, TxtLink_Boton3.Text,
                                                      TxtLink_Boton4.Text};
        List<HyperLink> hyperlinksOfControls = new List<HyperLink>{ HlCarga_Boton1, 
                                                      HlCarga_Boton2, HlCarga_Boton3,
                                                      HlCarga_Boton4};
        List<string> textOfButtons = new List<string>{TxtNombreBoton1.Text,TxtNombreBoton2.Text, 
                                                      TxtNombreBoton3.Text, TxtNombreBoton4.Text};
   

        for (int i = 0; i < 4; i++)
        {

            InsertaControl(
             (6+i),
             textOfLinks[i],
             hyperlinksOfControls[i],
             nameOfTypes[i],
             nameOfControls[i],
             null,
             textOfButtons[i],
             null,
             null);
        }
        CargaValores();

    }


    protected void InsertaControl(
   int position,
   string textLink,
   HyperLink hyperlink,
   string tipo,
   string control,
   string objetivo,
   string texto,
   string fechaInicia,
   string fechaTermina)
    {

        if (tipo == "0")
        {
            if (!FileReference.Activator[position])
            {
                if (hyperlink.NavigateUrl != "")
                {
                    objetivo = hyperlink.NavigateUrl.Replace(Siget.Config.Global.urlEnlaces, "");
                }
                else
                {
                    msgError.show("Selecciona un archivo a cargar.");
                  
                }
            }
            else
            {
                try
                {
                    FileInfo oldFile = new FileInfo(Siget.Config.Global.rutaEnlaces + FileReference.nameOfFiles[position]);
                    if (oldFile.Exists)
                    {
                        oldFile.Delete();
                    }

                    FileReference.Files[position].SaveAs(Siget.Config.Global.rutaEnlaces + FileReference.nameOfFiles[position]);

                    hyperlink.Visible = true;
                    hyperlink.Text = "Archivo cargado: " + HttpUtility.HtmlDecode(FileReference.nameOfFiles[position]).Trim();
                    hyperlink.NavigateUrl = Siget.Config.Global.urlEnlaces + HttpUtility.HtmlDecode(FileReference.nameOfFiles[position]).Trim();

                    objetivo = HttpUtility.HtmlDecode(FileReference.nameOfFiles[position]).Trim();
                }
                catch (Exception ex)
                {
                    Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
                    msgError.show(Siget.Lang.FileSystem.General.MSG_ERROR[Session["Usuario_Idioma"].ToString()], ex.Message.ToString()); 
                }
            }
        }
        else if (tipo == "1")
        {
            objetivo = textLink.Trim();
        }

        try
        {
            PaginaPrincipal_Insert(
              control,
              tipo,
              objetivo,
              texto,
              fechaInicia,
              fechaTermina);
           
             (new MessageSuccess(btnSaveChanges, this.GetType(), "Se guardaron las configuraciones.")).show();
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
            (new MessageError(btnSaveChanges, this.GetType(), ex.Message.ToString())).show();
        }
    }


    protected void PaginaPrincipal_Insert(
          string control,
          string tipo,
          string objetivo,
          string texto,
          string fechaInicia,
          string fechaTermina)
    {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand("PaginaInicio_Inserta", conn))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.Add("@Control ", SqlDbType.VarChar).Value = control;
                if (tipo != null) cmd.Parameters.Add("@Tipo", SqlDbType.SmallInt).Value = tipo;
                if (objetivo != null) cmd.Parameters.Add("@Objetivo", SqlDbType.VarChar).Value = objetivo;
                cmd.Parameters.Add("@Modifico", SqlDbType.VarChar).Value = User.Identity.Name;
                if (texto != null) cmd.Parameters.Add("@Texto", SqlDbType.VarChar).Value = texto;
                if (fechaInicia != null) cmd.Parameters.Add("@FechaInicia", SqlDbType.SmallDateTime).Value = fechaInicia;
                if (fechaTermina != null) cmd.Parameters.Add("@FechaTermina", SqlDbType.SmallDateTime).Value = fechaTermina;

                cmd.ExecuteNonQuery();

                Siget.Config.PaginaInicio.Vigente = false;
            }
        }
    }
    protected void FuCarga_Logo_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        FileReference.Files.Insert(5, FuCarga_Logo.PostedFile);
        FileReference.nameOfFiles.Insert(5, FuCarga_Logo.FileName);
        FileReference.Activator.Insert(5, true);
    }

    protected void FuCarga_Boton1_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        FileReference.Files.Insert(6, FuCarga_Boton1.PostedFile);
        FileReference.nameOfFiles.Insert(6, FuCarga_Boton1.FileName);
        FileReference.Activator.Insert(6, true);
    }

    protected void FuCarga_Boton2_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        FileReference.Files.Insert(7, FuCarga_Boton2.PostedFile);
        FileReference.nameOfFiles.Insert(7, FuCarga_Boton2.FileName);
        FileReference.Activator.Insert(7, true);
    }

    protected void FuCarga_Boton3_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        FileReference.Files.Insert(8, FuCarga_Boton3.PostedFile);
        FileReference.nameOfFiles.Insert(8, FuCarga_Boton3.FileName);
        FileReference.Activator.Insert(8, true);
    }

    protected void FuCarga_Boton4_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        FileReference.Files.Insert(9, FuCarga_Boton4.PostedFile);
        FileReference.nameOfFiles.Insert(9, FuCarga_Boton4.FileName);
        FileReference.Activator.Insert(9, true);
    }
    protected void btnSaveMessage_Click(object sender, EventArgs e)
    {
      

        if (FechaInicioMensaje.Text.Trim() == "")
        {
            msgError1.show("Especifique la fecha de inicio del periodo de visibilidad del aviso.");
            return;
        }
        if (FechaFinalMensaje.Text.Trim() == "")
        {
            msgError1.show("Especifique la fecha límite del periodo de visibilidad del aviso.");
            return;
        }

        try
        {
            // BtnAviso.Text = Siget.Utils.DateUtils.Date_to_EndOfDay(TxtFechaFinAviso.Text.Trim());
            PaginaPrincipal_Insert(
              "avisoPublico",
              null,
              null,
              editorPaginaInicio.Text,
              Siget.Utils.DateUtils.Date_to_StartOfDay(FechaInicioMensaje.Text.Trim()),
              Siget.Utils.DateUtils.Date_to_EndOfDay(FechaFinalMensaje.Text.Trim()));
            Siget.Config.PaginaInicio.Vigente = false;

            (new MessageSuccess(FechaInicioMensaje, this.GetType(), "Se guardó el aviso.")).show();
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
            (new MessageError(btnSaveChanges, this.GetType(), ex.Message.ToString())).show();
        }

        CargaValores();
    }
    protected void btnGuardarIconoCliente_Click(object sender, EventArgs e)
    {
        try
        {
            InsertaControl(5, TxtLink_Logo.Text, HlCarga_Logo, DdlTipo_Logo.SelectedValue, "logoCliente", null, "", null, null);
        }
        catch (Exception ex)
        {
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
            (new MessageError(btnSaveChanges, this.GetType(), ex.Message.ToString())).show();     
        }
    }
}