﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" MaintainScrollPositionOnPostback="true"
        EnableEventValidation="false" AutoEventWireup="false" CodeFile="coordinador.aspx.vb" Inherits="administrador_AgruparReactivos" %>


<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat radius-bordered">
            <div class="widget-header bg-themeprimary">
                <span class="widget-caption"><b>ÁREA DE COORDINADORES</b></span>
            </div>

            <div class="widget-body">
                <div class="widget-main ">
                    <div class="tabbable">
                        <ul class="nav nav-tabs tabs-flat" id="myTab11">
                            <li id="Crear" class="active">
                                <a data-toggle="tab" href="#crearAdministrar">Crear y administrar
                                </a>
                            </li>
                            <li>
                                <a id="Messages" data-toggle="tab" href="#asignarPlanteles"><%=Lang_Config.Translate("coordinador", "Header_coordinador")%>  
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content tabs-flat">
                            <div id="crearAdministrar" class="tab-pane in active">
                                <asp:UpdatePanel ID="AreaAdministracionCoordinadores" runat="server">
                                    <ContentTemplate>
                                        <div class="form-horizontal" id="coordinatorForm">
                                            <div class="col-lg-12" style="position: absolute; top: 50%; left: 50%; bottom: 50%;">
                                                            <div class="load-spinner"></div>
                                                        </div>
                                            <div class="panel panel-default">
                                                
                                                <div class="panel-body">

                                                    <div class="col-lg-6 ">
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Nombre</label>
                                                            <div class="col-lg-10">
                                                                <asp:TextBox ID="TBNombre" runat="server"
                                                                    CssClass="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Apellidos</label>
                                                            <div class="col-lg-10">
                                                                <asp:TextBox ID="TBApellidos" runat="server"
                                                                    CssClass="form-control">

                                                                </asp:TextBox>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                           
                                                            <label class="col-lg-2 control-label">Estatus</label>
                                                            <div class="col-lg-8">
                                                                <asp:DropDownList ID="DDLEstatus" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="Activo">Activo</asp:ListItem>
                                                                    <asp:ListItem Value="Baja">Baja</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Correo Electronico</label>
                                                            <div class="col-lg-4">
                                                                <asp:TextBox type="email" ID="TBCorreoElectronico"
                                                                    runat="server" CssClass="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                            <label class="col-lg-2 control-label">Usuario:</label>
                                                            <div class="col-lg-4">
                                                                <asp:TextBox ID="TBUsername" runat="server"
                                                                    CssClass="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>



                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Contraseña:</label>
                                                            <div class="col-lg-10">
                                                                <asp:TextBox ID="TBPassword"
                                                                    runat="server" CssClass="form-control"></asp:TextBox>

                                                            </div>
                                                        </div>
                                                      


                                                    </div>

                                                    <div class="col-lg-6 ">

                                                        <div class="table-responsive">

                                                            <asp:GridView ID="GVCoordinadores" runat="server"
                                                                AllowPaging="True" 
                                                                AllowSorting="True"
                                                                AutoGenerateColumns="False"
                                                                DataSourceID="SDSCoordinadores"
                                                                DataKeyNames="IdCoordinador,Nombre, Apellidos,Clave,FechaIngreso,Estatus,Email,Login,Password"
                                                                PageSize="10"
                                                                CellPadding="3"
                                                                CssClass="table table-striped table-bordered"
                                                                Height="17px"
                                                                Style="font-size: x-small; text-align: left;">
                                                                <Columns>
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Nombre"
                                                                        SortExpression="Nombre" />
                                                                    <asp:BoundField DataField="Apellidos" HeaderText="Apellidos"
                                                                        SortExpression="Apellidos" />
                                                                    <asp:BoundField DataField="Clave" Visible="false" HeaderText="Clave"
                                                                        SortExpression="Clave" />
                                                                    <asp:BoundField DataField="FechaIngreso" HeaderText="Fecha de Ingreso"
                                                                        SortExpression="FechaIngreso" />
                                                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                                        SortExpression="Estatus" />
                                                                </Columns>
                                                                <PagerTemplate>
                                                                    <ul runat="server" id="Pag" class="pagination">
                                                                    </ul>
                                                                </PagerTemplate>
                                                                <PagerStyle HorizontalAlign="Center" />
                                                                <SelectedRowStyle CssClass="row-selected" />
                                                                <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />

                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <asp:LinkButton ID="Insertar" runat="server" 
                                                                CssClass="Cin btn btn-labeled btn-palegreen" type="submit">
                                                                 <i class="btn-label glyphicon glyphicon-plus"></i> Insertar
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="CrearNuevo" runat="server"
                                                                CssClass="btn btn-labeled btn-yellow">
                                                                 <i class="btn-label fa fa-warning"></i> Limpiar
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="Actualizar" Visible="false" runat="server" 
                                                                CssClass="Cin btn btn-labeled btn-palegreen" type="submit">
                                                                <i class="btn-label fa fa-refresh"></i> Actualizar
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="Eliminar" Visible="false" runat="server" 
                                                                type="submit"
                                                                CssClass="btn btn-labeled btn-darkorange">
                                                                <i class="btn-label glyphicon glyphicon-remove"></i> Eliminar
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <uc1:msgSuccess runat="server" ID="msgSuccess1" />
                                                        <uc1:msgError runat="server" ID="msgError1" />
                                                    </div>

                                                </div>

                                            </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                        </div>

                        <div id="asignarPlanteles" class="tab-pane">


                            <div class="form-horizontal">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <div class="panel panel-default">

                                            <div class="panel-heading  bg-themeprimary-lg"><b>ÁREA PARA ASIGNAR <%=Lang_Config.Translate("general","Plantel") %>S A <asp:Label runat="server" CssClass="size-st" ID="coordinadorSelected"></asp:Label></b></div>

                                            <div class="panel-body">

                                                <div class="form-group">
                                                    <asp:Label ID="Label3" CssClass="col-lg-2 control-label" runat="server">

                                                        Elija la <%=Lang_Config.Translate("general","Institucion") %>
                                                    </asp:Label>
                                                    <div class="col-lg-10">

                                                        <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                                            DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                                            DataValueField="IdInstitucion" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                             

                                                <div class="col-lg-6">
                                                       <label><h1><%=Lang_Config.Translate("coordinador","Sub_header_plantel_asignar_1") %>
                                                       </h1></label>
                                                    <div class="table-reponsive">
                                                        <asp:GridView ID="GVPlanteles" runat="server"
                                                            AllowPaging="True" 
                                                            AllowSorting="True"
                                                            AutoGenerateColumns="False"
                                                            DataKeyNames="IdPlantel"
                                                            DataSourceID="SDSPlanteles"
                                           
                                                            PageSize="10"
                                                            CellPadding="3"
                                                            CssClass="table table-striped table-bordered"
                                                            Height="17px"
                                                            Style="font-size: x-small; text-align: left;">
                                                            <Columns>

                                                                <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel" Visible="false"
                                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel" />
                                                                <asp:BoundField DataField="IdLicencia" HeaderText="IdLicencia" Visible="false"
                                                                    SortExpression="IdLicencia" />
                                                                <asp:BoundField DataField="IdInstitucion" HeaderText="IdInstitucion" Visible="false"
                                                                    SortExpression="IdInstitucion" />
                                                                <asp:BoundField DataField="Descripcion" HeaderText="Plantel"
                                                                    SortExpression="Descripcion" />
                                                            </Columns>
                                                            <PagerTemplate>
                                                                <ul runat="server" id="Pag" class="pagination">
                                                                </ul>
                                                            </PagerTemplate>
                                                            <PagerStyle HorizontalAlign="Center" />
                                                            <SelectedRowStyle CssClass="row-selected" />
                                                            <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                        </asp:GridView>
                                                        <%-- sort YES - rowdatabound
	                                                        0  select
	                                                        1  idPLANTEL
	                                                        2  idlicencia
	                                                        3  idINSTITUCION
	                                                        4  descripcion
                                                        --%>
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:LinkButton ID="Asignar" Visible="false" runat="server"
                                                            CssClass="btn btn-lg btn-labeled btn-palegreen shiny space-margin">
                                                                 <i class="btn-label fa fa-plus"></i>Asignar
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                      <label><h1><%=Lang_Config.Translate("coordinador","Sub_header_plantel_asignar_2") %></h1></label>
                                                    <div class="table-responsive">
                                                        <asp:GridView ID="GVDatosPlanteles" runat="server"
                                                            AllowPaging="True"
                                                            AllowSorting="True"
                                                            AutoGenerateColumns="False"
                                                            DataSourceID="SDSinformacion"
                                                            DataKeyNames="IdPlantel,IdCoordinador"
                                                            Caption="<h1>"
                                                            PageSize="10"
                                                            CellPadding="3"
                                                            CssClass="table table-striped table-bordered"
                                                            Height="17px"
                                                            Style="font-size: x-small; text-align: left;">
                                                            <Columns>
                                                             
                                                               
                                                                <asp:BoundField DataField="IdInstitucion" Visible="false" HeaderText="Id-Inst."
                                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdInstitucion" />
                                                                <asp:BoundField DataField="Descripcion" Visible="false" HeaderText="Institución"
                                                                    SortExpression="Descripcion" />
                                                                <asp:BoundField DataField="IdCoordinador" Visible="false" HeaderText="Id-Coord."
                                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdCoordinador" />
                                                                <asp:BoundField DataField="NomCoordinador" Visible="false" HeaderText="Coordinador"
                                                                    ReadOnly="True" SortExpression="NomCoordinador" />
                                                                <asp:BoundField DataField="IdPlantel" Visible="false" HeaderText="Descripción Plantel"
                                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel" />
                                                                <asp:BoundField DataField="Descripcion1" HeaderText="Plantel"
                                                                    SortExpression="Descripcion1" />
                                                            </Columns>
                                                            <PagerTemplate>
                                                                <ul runat="server" id="Pag" class="pagination">
                                                                </ul>
                                                            </PagerTemplate>
                                                            <PagerStyle HorizontalAlign="Center" />
                                                            <SelectedRowStyle CssClass="row-selected" />
                                                            <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                        </asp:GridView>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:LinkButton ID="Desasignar" runat="server"
                                                            CssClass="btn btn-lg btn-labeled btn-darkorange space-margin"
                                                            Text="Desasignar">
                                                                <i class="btn-label fa fa-minus"></i>Desasignar
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <uc1:msgSuccess runat="server" ID="msgSuccess" />
                                                    <uc1:msgError runat="server" ID="msgError" />
                                                </div>
                                            </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>


    </div>




                <asp:SqlDataSource ID="SDSinformacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select  I.IdInstitucion, I.Descripcion, C.IdCoordinador, C.Apellidos+', '+Nombre+ ' (' + C.Clave+ ')' NomCoordinador, P.IdPlantel, P.Descripcion
from  Institucion I, Coordinador C, Plantel P, CoordinadorPlantel CP
where  P.IdInstitucion = I.IdInstitucion and P.IdPlantel = CP.IdPlantel and C.IdCoordinador = CP.IdCoordinador and C.IdCoordinador=@IdCoordinador
order by I.Descripcion, P.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVCoordinadores" Name="IdCoordinador"
                            PropertyName="SelectedDataKey(IdCoordinador)" />
                    </SelectParameters>
                </asp:SqlDataSource>

         

                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdInstitucion], [Descripcion] FROM [Institucion] ORDER BY [Descripcion]"></asp:SqlDataSource>

           
                <asp:SqlDataSource ID="SDSPlanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE [IdInstitucion] = @IdInstitucion AND IdPlantel NOT IN (SELECT IdPlantel FROM CoordinadorPlantel WHERE IdCoordinador = @IdCoordinador) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="GVCoordinadores" Name="IdCoordinador"
                            PropertyName="SelectedDataKey.Values[IdCoordinador]" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

  
                <asp:SqlDataSource ID="SDSCoordPlantel" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CoordinadorPlantel]"></asp:SqlDataSource>






    <asp:SqlDataSource ID="SDSCoordinadores" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="SELECT C.*,U.Login,U.Password FROM  Coordinador C JOIN Usuario U ON C.IdUsuario=U.IdUsuario"></asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">

    <script type="text/javascript">
      
      

    </script>


</asp:Content>

