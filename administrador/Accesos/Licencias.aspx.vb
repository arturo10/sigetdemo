﻿Imports Siget
Imports System.Data.SqlClient
Partial Class administrador_AgruparReactivos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()
    End Sub


    Protected Sub gvLicencias_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles gvLicencias.RowCreated

        Dim cociente As Integer = (gvLicencias.PageCount / 15)
        Dim moduloIndex As Integer = (gvLicencias.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((gvLicencias.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = gvLicencias.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = gvLicencias.PageIndex
                final = IIf((inicial + 15 <= gvLicencias.PageCount), inicial + 15, gvLicencias.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= gvLicencias.PageCount, inicial + 15, gvLicencias.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = gvLicencias.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub
    Protected Sub gvLicencias_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvLicencias.RowDataBound


        Dim cociente As Integer = (gvLicencias.PageCount) / 15
        Dim moduloIndex As Integer = (gvLicencias.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((gvLicencias.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = gvLicencias.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = gvLicencias.PageIndex
            final = IIf((inicial + 15 <= gvLicencias.PageCount), inicial + 15, gvLicencias.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= gvLicencias.PageCount), inicial + 15, gvLicencias.PageCount)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(gvLicencias, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(gvLicencias, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub gvLicencias_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvLicencias.SelectedIndexChanged
        btnEliminarLicencia.Visible = True
        btnActualizarLicencia.Visible = True

        tbClave.Text = gvLicencias.SelectedDataKey("Clave")
        tbExpiracion.Text = CDate(gvLicencias.SelectedDataKey("FechaExpira").ToString).ToShortDateString
        tbAlta.Text = CDate(gvLicencias.SelectedDataKey("FechaAlta").ToString).ToShortDateString
        ddlEstatus.SelectedValue = gvLicencias.SelectedDataKey("Estatus")

    End Sub

    Protected Sub gvLicencias_DataBound(sender As Object, e As EventArgs) Handles gvLicencias.DataBound
        If gvLicencias.Rows.Count = 0 Then
            btnEliminarLicencia.Visible = False
            btnActualizarLicencia.Visible = False
            gvLicencias.SelectedIndex = -1
        End If
    End Sub


    Protected Sub btnCrearNuevaLicencia_Click(sender As Object, e As EventArgs) Handles btnCrearNuevaLicencia.Click
        msgError.hide()
        msgSuccess.hide()

        Try

            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("SET dateformat dmy; INSERT INTO Licencia (Clave, FechaAlta, FechaExpira, Estatus, FechaModif, Modifico) VALUES (@Clave, @FechaAlta, @FechaExpira, @Estatus, getdate(), @Modifico)", conn)

                cmd.Parameters.AddWithValue("@Clave", tbClave.Text)

                If Trim(tbAlta.Text).Equals("") Then
                    cmd.Parameters.AddWithValue("@FechaAlta", "getdate()")
                Else
                    cmd.Parameters.AddWithValue("@FechaAlta", tbAlta.Text)
                End If

                If Trim(tbExpiracion.Text).Equals("") Then
                    cmd.Parameters.AddWithValue("@FechaExpira", "getdate()")
                Else
                    cmd.Parameters.AddWithValue("@FechaExpira", tbExpiracion.Text)
                End If
                cmd.Parameters.AddWithValue("@Estatus", ddlEstatus.SelectedValue)
                cmd.Parameters.AddWithValue("@Modifico", User.Identity.Name)

                cmd.ExecuteNonQuery()
                cmd.Dispose()
                conn.Close()

                msgSuccess.show("Éxito", "Se creó la licencia " & tbClave.Text)
                gvLicencias.DataBind()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub btnActualizarLicencia_Click(sender As Object, e As EventArgs) Handles btnActualizarLicencia.Click
        msgError.hide()
        msgSuccess.hide()

        Try
        
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("SET dateformat dmy; UPDATE Licencia SET Clave = @Clave, FechaAlta = @FechaAlta, FechaExpira = @FechaExpira, Estatus = @Estatus, FechaModif = getdate(), Modifico = @Modifico WHERE IdLicencia = @IdLicencia", conn)

                cmd.Parameters.AddWithValue("@Clave", tbClave.Text)
                cmd.Parameters.AddWithValue("@FechaAlta", tbAlta.Text)
                cmd.Parameters.AddWithValue("@FechaExpira", tbExpiracion.Text)
                cmd.Parameters.AddWithValue("@Estatus", ddlEstatus.SelectedValue)
                cmd.Parameters.AddWithValue("@Modifico", User.Identity.Name)
                cmd.Parameters.AddWithValue("@IdLicencia", gvLicencias.SelectedDataKey("IdLicencia").ToString)

                cmd.ExecuteNonQuery()
                cmd.Dispose()
                conn.Close()

                msgSuccess.show("Éxito", "Se actualizó la licencia " & tbClave.Text)
                gvLicencias.DataBind()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub btnEliminarLicencia_Click(sender As Object, e As EventArgs) Handles btnEliminarLicencia.Click
        msgError.hide()
        msgSuccess.hide()

        Try
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("SET dateformat dmy; DELETE FROM Licencia WHERE IdLicencia = @IdLicencia", conn)

                cmd.Parameters.AddWithValue("@IdLicencia", gvLicencias.SelectedDataKey("IdLicencia").ToString)

                cmd.ExecuteNonQuery()
                cmd.Dispose()
                conn.Close()

                msgSuccess.show("Éxito", "Se eliminó la licencia " & gvLicencias.SelectedDataKey("Clave"))
                gvLicencias.DataBind()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub
End Class
