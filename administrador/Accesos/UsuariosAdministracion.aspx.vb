﻿Imports Siget
Imports Siget.Entity

Partial Class administrador_AgruparReactivos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()
    End Sub


    Protected Sub GVadmin_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVadmin.RowCreated

        Dim cociente As Integer = (GVadmin.PageCount / 15)
        Dim moduloIndex As Integer = (GVadmin.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVadmin.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVadmin.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVadmin.PageIndex
                final = IIf((inicial + 15 <= GVadmin.PageCount), inicial + 15, GVadmin.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVadmin.PageCount, inicial + 15, GVadmin.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVadmin.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub

    Protected Sub GVcoordinadores_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVcoordinadores.RowCreated

        Dim cociente As Integer = (GVcoordinadores.PageCount / 15)
        Dim moduloIndex As Integer = (GVcoordinadores.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVcoordinadores.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVcoordinadores.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVcoordinadores.PageIndex
                final = IIf((inicial + 15 <= GVcoordinadores.PageCount), inicial + 15, GVcoordinadores.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVcoordinadores.PageCount, inicial + 15, GVcoordinadores.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVcoordinadores.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub
    Protected Sub GVPlanteles_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVPlanteles.RowCreated

        Dim cociente As Integer = (GVPlanteles.PageCount / 15)
        Dim moduloIndex As Integer = (GVPlanteles.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVPlanteles.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVPlanteles.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVPlanteles.PageIndex
                final = IIf((inicial + 15 <= GVPlanteles.PageCount), inicial + 15, GVPlanteles.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVPlanteles.PageCount, inicial + 15, GVPlanteles.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVPlanteles.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub
    Protected Sub GVDatosPlanteles_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVDatosPlanteles.RowCreated

        Dim cociente As Integer = (GVDatosPlanteles.PageCount / 15)
        Dim moduloIndex As Integer = (GVDatosPlanteles.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVDatosPlanteles.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVDatosPlanteles.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVDatosPlanteles.PageIndex
                final = IIf((inicial + 15 <= GVDatosPlanteles.PageCount), inicial + 15, GVDatosPlanteles.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVDatosPlanteles.PageCount, inicial + 15, GVDatosPlanteles.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVDatosPlanteles.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub
    Protected Sub GVadmin_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVadmin.RowDataBound


        Dim cociente As Integer = (GVadmin.PageCount) / 15
        Dim moduloIndex As Integer = (GVadmin.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVadmin.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVadmin.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVadmin.PageIndex
            final = IIf((inicial + 15 <= GVadmin.PageCount), inicial + 15, GVadmin.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVadmin.PageCount), inicial + 15, GVadmin.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVadmin, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVadmin, "Page$" & counter.ToString)
            Next
        End If
    End Sub
    Protected Sub GVPlanteles_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVPlanteles.RowDataBound


        Dim cociente As Integer = (GVPlanteles.PageCount) / 15
        Dim moduloIndex As Integer = (GVPlanteles.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVPlanteles.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVPlanteles.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVPlanteles.PageIndex
            final = IIf((inicial + 15 <= GVPlanteles.PageCount), inicial + 15, GVPlanteles.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVPlanteles.PageCount), inicial + 15, GVPlanteles.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVPlanteles, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVPlanteles, "Page$" & counter.ToString)
            Next
        End If
    End Sub
    Protected Sub GVcoordinadores_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVcoordinadores.RowDataBound


        Dim cociente As Integer = (GVcoordinadores.PageCount) / 15
        Dim moduloIndex As Integer = (GVcoordinadores.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVcoordinadores.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVcoordinadores.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVcoordinadores.PageIndex
            final = IIf((inicial + 15 <= GVcoordinadores.PageCount), inicial + 15, GVcoordinadores.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVcoordinadores.PageCount), inicial + 15, GVcoordinadores.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVcoordinadores, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVcoordinadores, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVDatosPlanteles_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVDatosPlanteles.RowDataBound


        Dim cociente As Integer = (GVDatosPlanteles.PageCount) / 15
        Dim moduloIndex As Integer = (GVDatosPlanteles.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVDatosPlanteles.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVDatosPlanteles.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVDatosPlanteles.PageIndex
            final = IIf((inicial + 15 <= GVDatosPlanteles.PageCount), inicial + 15, GVDatosPlanteles.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVcoordinadores.PageCount), inicial + 15, GVcoordinadores.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVDatosPlanteles, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVDatosPlanteles, "Page$" & counter.ToString)
            Next
        End If
    End Sub

    Protected Sub GVadmin_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVadmin.SelectedIndexChanged
        btnEliminarUsuarioSoporte.Visible = True
        TBnombreUsuariosSoporte.Text = IIf(IsDBNull(GVadmin.SelectedDataKey("Nombre")), String.Empty, GVadmin.SelectedDataKey("Nombre"))
        TBapellidosUsuariosSoporte.Text = IIf(IsDBNull(GVadmin.SelectedDataKey("Apellidos")), String.Empty, GVadmin.SelectedDataKey("Apellidos"))
        TBemailUsuariosSoporte.Text = IIf(IsDBNull(GVadmin.SelectedDataKey("Email")), String.Empty, GVadmin.SelectedDataKey("Email"))
        TBloginUsuariosSoporte.Text = GVadmin.SelectedDataKey("Login")
        TBpasswordUsuariosSoporte.Text = GVadmin.SelectedDataKey("Password")
        DDLperfilUsuariosSoporte.SelectedValue = GVadmin.SelectedDataKey("PerfilASP")
    End Sub

    Public Function GetErrorMessage(ByVal status As MembershipCreateStatus) As String
        Select Case status
            Case MembershipCreateStatus.DuplicateUserName
                Return "El usuario ya existe, elija otro usuario."
            Case MembershipCreateStatus.DuplicateEmail
                Return "Ya existe un usuario para ese email, por favor escriba un email diferente."
            Case MembershipCreateStatus.InvalidPassword
                Return "El password es inválido, pruebe con otro."
            Case MembershipCreateStatus.InvalidEmail
                Return "El formato del email es inválido, por favor revíselo."
            Case MembershipCreateStatus.InvalidAnswer
                Return "La respuesta secreta es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidQuestion
                Return "La pregunta de recuperación de password es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidUserName
                Return "El nombre de usuario es inválido, corríjalo."
            Case MembershipCreateStatus.ProviderError
                Return "La autentificación falló, contacte al administrador del sistema."
            Case MembershipCreateStatus.UserRejected
                Return "La creación del usuario ha sido cancelada, consulte al administrador."
            Case Else
                Return "Ha ocurrido un error desconocido, verifique o contacte al administrador."
        End Select
    End Function

    Protected Sub btnCrearUsuarioSoporte_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrearUsuarioSoporte.Click
        Dim status As MembershipCreateStatus
        Dim adminRepository As IRepository(Of Admin) = New AdminRepository()
        Dim admin As Admin = New Admin()
        Dim usuario As Usuario = New Usuario()

        Try
            Dim newUser As MembershipUser = Membership.CreateUser(Trim(TBloginUsuariosSoporte.Text), Trim(TBpasswordUsuariosSoporte.Text), _
                                                             Trim(TBemailUsuariosSoporte.Text), Nothing, _
                                                             Nothing, True, status)
            If newUser Is Nothing Then
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), GetErrorMessage(status))
            Else
                Roles.AddUserToRole(Trim(TBloginUsuariosSoporte.Text), DDLperfilUsuariosSoporte.SelectedValue.ToString)
            End If

            With usuario
                .Login = TBloginUsuariosSoporte.Text
                .Password = TBpasswordUsuariosSoporte.Text
                .PerfilASP = DDLperfilUsuariosSoporte.SelectedValue
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
                .FT = False
            End With

            With admin
                .Usuario = usuario
                .Nombre = TBnombreUsuariosSoporte.Text
                .Apellidos = TBapellidosUsuariosSoporte.Text
                .Email = TBemailUsuariosSoporte.Text
                .FechaAlta = DateTime.Now
                .Estatus = "Activo"
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
            End With

            adminRepository.Add(admin)
            CType(New MessageSuccess(btnCrearUsuarioSoporte, Me.GetType, "El registro se insertó correctamente"), MessageSuccess).show()
            GVadmin.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(btnCrearUsuarioSoporte, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try

    End Sub
    Protected Sub btnEliminarUsuarioSoporte_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarUsuarioSoporte.Click
        msgSuccess.hide()
        msgError.hide()
        Dim adminRepository As IRepository(Of Admin) = New AdminRepository()
        Dim usuarioRepository As IRepository(Of Usuario) = New UsuarioRepository()
        Dim admin As Admin = New Admin()
        Dim usuario As Usuario = New Usuario()
        Dim IdUsuario As Integer

        Try

            admin = adminRepository.FindById(GVadmin.SelectedDataKey("IdAdmin"))
            IdUsuario = admin.Usuario.IdUsuario
            usuario = usuarioRepository.FindById(IdUsuario)
            adminRepository.Delete(admin)
            usuarioRepository.Delete(usuario)
            Membership.DeleteUser(Trim(HttpUtility.HtmlDecode(GVadmin.SelectedDataKey("Login"))))
            CType(New MessageSuccess(btnCrearUsuarioSoporte, Me.GetType, "El registro se eliminó correctamente"), MessageSuccess).show()
            GVadmin.DataBind()

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(btnCrearUsuarioSoporte, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try

    End Sub
    Protected Sub GVadmin_DataBound(sender As Object, e As EventArgs) Handles GVadmin.DataBound
        If GVadmin.Rows.Count = 0 Then
            btnEliminarUsuarioSoporte.Visible = False
        End If
    End Sub

    Protected Sub GVcoordinadores_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVcoordinadores.SelectedIndexChanged
        btnEliminarOperador.Visible = True
        btnActualizarOperador.Visible = True

        TBnombreOperador.Text = GVcoordinadores.SelectedDataKey("Nombre")
        TBApellidosOperador.Text = GVcoordinadores.SelectedDataKey("Apellidos")
        TBCodigoAsignadoOperador.Text = GVcoordinadores.SelectedDataKey("Clave")
        TBCorreoOperador.Text = GVcoordinadores.SelectedDataKey("Email")
        TBLoginOperador.Text = GVcoordinadores.SelectedDataKey("Login")
        TBPasswordOperador.Text = GVcoordinadores.SelectedDataKey("Contraseña")

    End Sub

    Protected Sub GVcoordinadores_DataBound(sender As Object, e As EventArgs)
        If GVcoordinadores.Rows.Count = 0 Then
            btnEliminarOperador.Visible = False
            btnActualizarOperador.Visible = False
        End If
    End Sub

    Protected Sub btnCrearOperador_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCrearOperador.Click
        Dim status As MembershipCreateStatus
        Dim operadorRepository As IRepository(Of Operador) = New OperadorRepository()
        Dim operador As Operador = New Operador()
        Dim usuario As Usuario = New Usuario()

        msgSuccess2.hide()
        msgError2.hide()
        Try
            Dim newUser As MembershipUser = Membership.CreateUser(Trim(TBLoginOperador.Text), Trim(TBPasswordOperador.Text), _
                                                             Trim(TBCorreoOperador.Text), Nothing, _
                                                             Nothing, True, status)
            If newUser Is Nothing Then
                msgError2.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), GetErrorMessage(status))
            Else
                Roles.AddUserToRole(Trim(TBLoginOperador.Text), "Operador")
            End If

            With usuario
                .Login = TBLoginOperador.Text
                .Password = TBPasswordOperador.Text
                .PerfilASP = "Operador"
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
                .FT = False
            End With

            With operador
                .Usuario = usuario
                .Clave = TBCodigoAsignadoOperador.Text
                .FechaIngreso = DateTime.Now
                .Nombre = TBnombreOperador.Text
                .Apellidos = TBApellidosOperador.Text
                .Email = TBCorreoOperador.Text
                .Estatus = DDLEstatusOperador.SelectedValue
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
            End With

            operadorRepository.Add(operador)
            CType(New MessageSuccess(btnCrearUsuarioSoporte, Me.GetType, "El registro se insertó correctamente"), MessageSuccess).show()
            GVcoordinadores.DataBind()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(btnCrearOperador, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try

    End Sub

    Protected Sub btnEliminarOperador_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEliminarOperador.Click

        Dim operadorRepository As IRepository(Of Operador) = New OperadorRepository()
        Dim operador As Operador = New Operador()
        Dim usuario As Usuario = New Usuario()
        Dim usuarioRepository As IRepository(Of Usuario) = New UsuarioRepository()
        Dim IdUsuario As Integer
        Try

            operador = operadorRepository.FindById(GVcoordinadores.SelectedDataKey("IdOperador"))
            IdUsuario = operador.Usuario.IdUsuario
            usuario = usuarioRepository.FindById(IdUsuario)
            operadorRepository.Delete(operador)
            usuarioRepository.Delete(usuario)
            Membership.DeleteUser(Trim(HttpUtility.HtmlDecode(GVcoordinadores.SelectedDataKey("Login"))))
            CType(New MessageSuccess(btnEliminarOperador, Me.GetType, "El registro se eliminó correctamente"), MessageSuccess).show()
            GVcoordinadores.DataBind()

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(btnEliminarOperador, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try

    End Sub

    Protected Sub btnActualizarOperador_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActualizarOperador.Click
        Dim userUpdate As MembershipUser
        Dim operadorRepository As IRepository(Of Operador) = New OperadorRepository()
        Dim operador As Operador = New Operador()
        Dim usuario As Usuario = New Usuario()
        Dim usuarioRepository As IRepository(Of Usuario) = New UsuarioRepository()

        msgSuccess2.hide()
        msgError2.hide()
        Try


            operador = operadorRepository.FindById(GVcoordinadores.SelectedDataKey("IdOperador"))
            With operador.Usuario
                .Login = TBLoginOperador.Text
                .Password = TBPasswordOperador.Text
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
                .FT = False
            End With

            With operador
                .Clave = TBCodigoAsignadoOperador.Text
                .FechaIngreso = DateTime.Now
                .Nombre = TBnombreOperador.Text
                .Apellidos = TBApellidosOperador.Text
                .Email = TBCorreoOperador.Text
                .Estatus = DDLEstatusOperador.SelectedValue
                .FechaModif = DateTime.Now
                .Modifico = User.Identity.Name
            End With

            operadorRepository.Update(operador)
            userUpdate = Membership.GetUser(Trim(HttpUtility.HtmlDecode(GVcoordinadores.SelectedDataKey("Login").ToString)))
            userUpdate.ChangePassword(userUpdate.ResetPassword(), Trim(TBPasswordOperador.Text))
            CType(New MessageSuccess(btnActualizarOperador, Me.GetType, "El registro se eliminó correctamente"), MessageSuccess).show()
            GVcoordinadores.DataBind()

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(btnActualizarOperador, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try

    End Sub

    Protected Sub GVDatosPlanteles_DataBound(sender As Object, e As EventArgs) Handles GVDatosPlanteles.DataBound

        If GVDatosPlanteles.Rows.Count = 0 Then
            btnDesasignarOperador.Visible = False
            GVDatosPlanteles.SelectedIndex = -1
        End If

    End Sub

    Protected Sub GVPlanteles_DataBound(sender As Object, e As EventArgs) Handles GVPlanteles.DataBound
        If GVPlanteles.Rows.Count = 0 Then
            btnAsignarOperador.Visible = False
            GVPlanteles.SelectedIndex = -1
        End If
    End Sub

    Protected Sub GVPlanteles_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVPlanteles.SelectedIndexChanged
        btnAsignarOperador.Visible = True
    End Sub

    Protected Sub GVDatosPlanteles_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GVDatosPlanteles.SelectedIndexChanged
        btnDesasignarOperador.Visible = True
    End Sub

    Protected Sub Asignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAsignarOperador.Click
        Try
            SDSOpePlantel.InsertCommand = "SET dateformat dmy; INSERT INTO OperadorPlantel (IdOperador,IdPlantel,FechaModif, Modifico) VALUES (" + _
                GVcoordinadores.SelectedValue.ToString + "," + GVPlanteles.SelectedValue.ToString + ",getdate(), '" & User.Identity.Name & "')"
            SDSOpePlantel.Insert()
            GVDatosPlanteles.DataBind()
            GVPlanteles.DataBind()
            CType(New MessageSuccess(btnActualizarOperador, Me.GetType, "El Plantel ha sido asignado al Operador."), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(btnActualizarOperador, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try
    End Sub

    Protected Sub Desasignar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDesasignarOperador.Click
        Try
            SDSOpePlantel.DeleteCommand = "DELETE FROM OperadorPlantel WHERE IdPlantel = " + GVDatosPlanteles.SelectedDataKey(0).ToString + " and IdOperador = " + GVDatosPlanteles.SelectedDataKey(1).ToString
            SDSOpePlantel.Delete()
            GVDatosPlanteles.DataBind()
            GVPlanteles.DataBind()

            CType(New MessageSuccess(btnActualizarOperador, Me.GetType, Config.Etiqueta.ARTDET_PLANTEL & " " & Config.Etiqueta.PLANTEL &
                " ha sido desasignad" & Config.Etiqueta.LETRA_PLANTEL & " del Operador"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(btnActualizarOperador, Me.GetType, "Elija el registro con los datos a desasignar."), MessageError).show()
        End Try
    End Sub

End Class
