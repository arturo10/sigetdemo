﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Siget.Entity;
using System.Web.Security;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;


public partial class administrador_Accesos_Autorizaciones : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void GVAutorizaciones_RowCreated(object sender, GridViewRowEventArgs e)
    {
        int cociente = (GVAutorizaciones.PageCount / 15);
        int moduloIndex = (GVAutorizaciones.PageIndex + 1) % 15;
        int cocienteActual = Convert.ToInt32( Math.Floor(Convert.ToDecimal((GVAutorizaciones.PageIndex + 1) / 15)));
        int final = 0;
        int inicial = 0;


        if (e.Row.RowType == DataControlRowType.Pager)
        {
            HtmlControl con = (HtmlControl)e.Row.Cells[0].FindControl("Pag");
            if (cociente == 0)
            {
                inicial = 1;
                final = GVAutorizaciones.PageCount;
            }
            else if (cociente >= 1 & moduloIndex == 0)
            {
                inicial = GVAutorizaciones.PageIndex;
                final = ((inicial + 15 <= GVAutorizaciones.PageCount) ? inicial + 15 : GVAutorizaciones.PageCount);
            }
            else
            {
                inicial = (cocienteActual * 15 > 0 ? cocienteActual * 15 : 1);
                final = (inicial + 15 <= GVAutorizaciones.PageCount ? inicial + 15 : GVAutorizaciones.PageCount);
            }


            for (int counter = inicial; counter <= final; counter++)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                HtmlGenericControl page = new HtmlGenericControl("a");

                if (counter == inicial & cocienteActual > 0)
                {
                    HtmlGenericControl span = new HtmlGenericControl("span");
                    span.InnerHtml = "&laquo;";
                    page.Controls.Add(span);
                    page.Attributes.Add("aria-label", "Previous");
                    page.ID = "pagina" + counter.ToString();
                    li.Controls.Add(page);
                    con.Controls.Add(li);

                }
                else
                {
                    if (counter == GVAutorizaciones.PageIndex + 1)
                    {
                        li.Attributes["class"] = "active";
                    }
                    page.InnerText = counter.ToString();
                    page.ID = "pagina" + counter.ToString();

                    li.Controls.Add(page);
                    con.Controls.Add(li);
                }
            }
        }

    }

    protected void GVAutorizaciones_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        int cociente = (GVAutorizaciones.PageCount) / 15;
        int moduloIndex = (GVAutorizaciones.PageIndex + 1) % 15;
        int cocienteActual = Convert.ToInt32(Math.Floor(Convert.ToDecimal((GVAutorizaciones.PageIndex + 1) / 15)));

        int inicial = 0;
        int final = 0;

        if (cociente == 0)
        {
            inicial = 1;
            final = GVAutorizaciones.PageCount;
        }
        else if (cociente >= 1 & moduloIndex == 0)
        {
            inicial = GVAutorizaciones.PageIndex;
            final = ((inicial + 15 <= GVAutorizaciones.PageCount) ? inicial + 15 : GVAutorizaciones.PageCount);
        }
        else
        {
            inicial = (cocienteActual * 15 > 0 ? cocienteActual * 15 : 1);
            final = ((inicial + 15 <= GVAutorizaciones.PageCount) ? inicial + 15 : GVAutorizaciones.PageCount);
        }


        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVAutorizaciones, "Select$" + e.Row.RowIndex));
        //}

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            HtmlControl con = (HtmlControl)e.Row.Cells[0].FindControl("Pag");

            for (int counter = inicial; counter <= final; counter++)
            {
                ((HtmlGenericControl)con.FindControl("pagina" + counter.ToString())).Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(GVAutorizaciones, "Page$" + counter.ToString());
            }
        }
    }

    protected void btnAceptarAlumno_Click(object sender, EventArgs e)
    {
        IRepository<Alumno> alumnoRepository = new AlumnoRepository();
        Boolean atLeastOne = false;
        try
        {
            foreach (GridViewRow row in GVAutorizaciones.Rows)
            {

                if (((CheckBox)row.Cells[0].Controls[1]).Checked)
                {
                    Alumno alumno = alumnoRepository.FindById(Convert.ToInt32(GVAutorizaciones.DataKeys[row.RowIndex].Values["IdAlumno"]));
                    alumno.Estatus = "Activo";
                    alumnoRepository.Update(alumno);
                    atLeastOne = true;
                }
            }
            if (atLeastOne)
                MsgSuccess.show("ÉXITO", "Se ha realizado exitosamente la operación");
            else MsgInfo.show("ATENCIÓN", "No se seleccionó ningún para autorizar");
        }
        catch (Exception ex)
        {
            MsgError.show("ERROR", ex.Message.ToString());
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
        }


        
        GVAutorizaciones.DataBind();
    }
    protected void btnRechazarAlumno_Click(object sender, EventArgs e)
    {
        IRepository<Alumno> alumnoRepository = new AlumnoRepository();
        IRepository<Usuario> usuarioRepository = new UsuarioRepository();
        SqlParameter output = new SqlParameter("@isSuccesful", SqlDbType.Int);
        output.Direction = ParameterDirection.Output;
        SqlCommand command;
        Boolean atLeastOne = false;

        try
        {
            foreach (GridViewRow row in GVAutorizaciones.Rows)
            {
                if (((CheckBox)row.Cells[0].Controls[1]).Checked)
                {

                    using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
                    {
                        conn.Open();
                        command = new SqlCommand("DeleteRegisterUser", conn);
                        command.Parameters.Add("@IdAlumno", SqlDbType.Int).Value = Convert.ToInt32(GVAutorizaciones.DataKeys[row.RowIndex].Values["IdAlumno"]);
                        command.Parameters.Add(output);
                        command.CommandType = CommandType.StoredProcedure;
                        command.ExecuteNonQuery();
                    }


                    if (output.Value.ToString() == "1")
                    {
                        Membership.DeleteUser(GVAutorizaciones.DataKeys[row.RowIndex].Values["Login"].ToString());
                        MsgSuccess.show("ÉXITO", "Se ha realizado exitosamente la operación");
                    }
                    else
                    {
                        MsgError.show("ERROR", "Contacte al administrador ");
                    }

                }
            }
        }
        catch (Exception ex)
        {
            MsgError.show("ERROR", ex.Message.ToString());
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
        }

        GVAutorizaciones.DataBind();
    }
    protected void GVAutorizaciones_DataBound(object sender, EventArgs e)
    {
        if (GVAutorizaciones.Rows.Count == 0)
        {
            btnAceptarAlumno.Visible = false;
            btnRechazarAlumno.Visible = false;
        }   
    }
}