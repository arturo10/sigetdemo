﻿Imports Siget
Imports System.Data
Imports System.Xml
Imports System.Web.Security.Membership

Partial Class administrador_AgruparReactivos
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        LblActivos.Text = "En este momento hay " + GetNumberOfUsersOnline.ToString + " usuarios firmados en el sistema."
        msgInfo2.show("Hora de último desbloqueo automático: " & Utils.DesbloquearUsuarios.LastExecution)

        Dim oDs As New DataSet
        oDs.ReadXml(Config.Global.rutaBloqueos)
        If oDs.Tables.Count > 0 Then
            GVBloqueos.DataSource = oDs
            GVBloqueos.DataBind()
            GVBloqueos.Visible = True
        Else
            GVBloqueos.Visible = False
        End If
    End Sub


    Protected Sub Insertar_Click(sender As Object, e As EventArgs) Handles btnCrearBloqueo.Click
        ' encontrar el último índice introducido
        Dim indice As Integer
     
        If GVBloqueos.Rows.Count = 0 Then
            indice = 0
        Else
            indice = CInt(GVBloqueos.SelectedRow().Cells(0).Text)
        End If
        ' aumentar al siguiente para dejarlo listo
        indice = indice + 1
        ' primero inserto el nuevo nodo al xml, y luego actualizo el gridview:
        Dim xmldoc As New XmlDocument()
        xmldoc.Load(Config.Global.rutaBloqueos)

        'Select main node
        Dim newXMLNode As XmlNode = xmldoc.SelectSingleNode("/Bloqueos")
        'get the node where you want to insert the data
        Dim childNode As XmlNode = xmldoc.CreateNode(XmlNodeType.Element, "Bl", "")
        Dim newAttribute As XmlNode = xmldoc.CreateNode(XmlNodeType.Element, "IdBloqueo", "")
        newAttribute.InnerText = indice
        childNode.AppendChild(newAttribute)
        newAttribute = xmldoc.CreateNode(XmlNodeType.Element, "Institucion", "")
        newAttribute.InnerText = DDLInstitucion.SelectedValue
        childNode.AppendChild(newAttribute)
        newAttribute = xmldoc.CreateNode(XmlNodeType.Element, "Dia", "")
        newAttribute.InnerText = DDLDia.SelectedValue
        childNode.AppendChild(newAttribute)
        newAttribute = xmldoc.CreateNode(XmlNodeType.Element, "Perfil", "")
        newAttribute.InnerText = DDLPerfil.SelectedValue
        childNode.AppendChild(newAttribute)
        newAttribute = xmldoc.CreateNode(XmlNodeType.Element, "Inicio", "")
        newAttribute.InnerText = TBInicioHora.Text
        childNode.AppendChild(newAttribute)
        newAttribute = xmldoc.CreateNode(XmlNodeType.Element, "Fin", "")
        newAttribute.InnerText = TBFinHora.Text
        childNode.AppendChild(newAttribute)

        newXMLNode.AppendChild(childNode)

        xmldoc.Save(Config.Global.rutaBloqueos)
        msgSuccess.show("Éxito", "Se ha agregado el bloqueo")

        Dim oDs As New DataSet
        oDs.ReadXml(Config.Global.rutaBloqueos)
        If oDs.Tables.Count > 0 Then
            GVBloqueos.DataSource = oDs
            GVBloqueos.DataBind()
            GVBloqueos.Visible = True
        Else
            GVBloqueos.Visible = False
        End If
    End Sub

    Protected Sub Eliminar_Click(sender As Object, e As EventArgs) Handles btnEliminarBloqueo.Click
        If GVBloqueos.SelectedIndex <> -1 AndAlso GVBloqueos.SelectedIndex < GVBloqueos.Rows.Count() Then
            ' primero cargo el documento
            Dim xmldoc As New XmlDocument()
            xmldoc.Load(Config.Global.rutaBloqueos)

            ' luego selecciono el nodo que contiene el índice de la fila seleccionada
            Dim node As XmlNode = xmldoc.SelectSingleNode(
                "//IdBloqueo[. = '" & GVBloqueos.SelectedRow.Cells(0).Text & "']" & "/parent::node()/IdBloqueo")
            ' si si se encontró uno (debería)
            If node IsNot Nothing Then
                ' quito el nodo de la estructura
                node.ParentNode.ParentNode.RemoveChild(node.ParentNode)
                ' guardo el archivo nuevo
                xmldoc.Save(Config.Global.rutaBloqueos)
                msgSuccess.show("Éxito", "Se ha eliminado el bloqueo")
            End If

            ' actualizo el gridview
            Dim oDs As New DataSet
            oDs.ReadXml(Config.Global.rutaBloqueos)
            If oDs.Tables.Count > 0 Then
                GVBloqueos.DataSource = oDs
                GVBloqueos.DataBind()
                GVBloqueos.Visible = True
            Else
                GVBloqueos.Visible = False
            End If
        Else
            msgError.show("Debe seleccionar un elemento a eliminar.")
        End If
    End Sub

    Protected Sub btndesbloquearCuentas_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndesbloquearCuentas.Click
        Try
            SDSusuarios.UpdateCommand = "SET dateformat dmy; UPDATE dbo.aspnet_Membership SET IsLockedOut = 0, FailedPasswordAttemptCount = 0, " + _
                "FailedPasswordAttemptWindowStart = CONVERT( datetime, '17540101', 112 ), FailedPasswordAnswerAttemptCount = 0, " + _
                "FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, '17540101', 112 ), " + _
                "LastLockoutDate = CONVERT( datetime, '17540101', 112 )"
            SDSusuarios.Update()
            CType(New MessageSuccess(btndesbloquearCuentas, Me.GetType, "Las cuentas de usuarios han sido rehabilitadas"), MessageSuccess).show()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            CType(New MessageError(btndesbloquearCuentas, Me.GetType, ex.Message.ToString()), MessageError).show()
        End Try
    End Sub

    Protected Sub GVBloqueos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVBloqueos.RowCreated

        Dim cociente As Integer = (GVBloqueos.PageCount / 15)
        Dim moduloIndex As Integer = (GVBloqueos.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVBloqueos.PageIndex + 1) / 15)
        Dim final As Integer
        Dim inicial As Integer

        If e.Row.RowType = DataControlRowType.Pager Then

            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            If cociente = 0 Then
                inicial = 1
                final = GVBloqueos.PageCount
            ElseIf cociente >= 1 And moduloIndex = 0 Then
                inicial = GVBloqueos.PageIndex
                final = IIf((inicial + 15 <= GVBloqueos.PageCount), inicial + 15, GVBloqueos.PageCount)
            Else
                inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
                final = IIf(inicial + 15 <= GVBloqueos.PageCount, inicial + 15, GVBloqueos.PageCount)
            End If

            For counter As Integer = inicial To final

                Dim li As HtmlGenericControl = New HtmlGenericControl("li")
                Dim page As HtmlGenericControl = New HtmlGenericControl("a")

                If counter = inicial And cocienteActual > 0 Then
                    Dim span As HtmlGenericControl = New HtmlGenericControl("span")
                    span.InnerHtml = "&laquo;"
                    page.Controls.Add(span)
                    page.Attributes.Add("aria-label", "Previous")
                    page.ID = "pagina" & counter.ToString
                    li.Controls.Add(page)
                    con.Controls.Add(li)

                Else
                    If counter = GVBloqueos.PageIndex + 1 Then
                        li.Attributes("class") = "active"
                    End If
                    page.InnerText = counter.ToString
                    page.ID = "pagina" & counter.ToString

                    li.Controls.Add(page)
                    con.Controls.Add(li)
                End If
            Next


        End If

    End Sub
    Protected Sub GVBloqueos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVBloqueos.RowDataBound


        Dim cociente As Integer = (GVBloqueos.PageCount) / 15
        Dim moduloIndex As Integer = (GVBloqueos.PageIndex + 1) Mod 15
        Dim cocienteActual As Integer = Math.Floor((GVBloqueos.PageIndex + 1) / 15)

        Dim inicial As Integer
        Dim final As Integer

        If cociente = 0 Then
            inicial = 1
            final = GVBloqueos.PageCount
        ElseIf cociente >= 1 And moduloIndex = 0 Then
            inicial = GVBloqueos.PageIndex
            final = IIf((inicial + 15 <= GVBloqueos.PageCount), inicial + 15, GVBloqueos.PageCount)
        Else
            inicial = IIf(cocienteActual * 15 > 0, cocienteActual * 15, 1)
            final = IIf((inicial + 15 <= GVBloqueos.PageCount), inicial + 15, GVBloqueos.PageCount)
        End If


        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVBloqueos, "Select$" & e.Row.RowIndex))
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim con As HtmlControl = CType(e.Row.Cells(0).FindControl("Pag"), HtmlControl)
            For counter As Integer = inicial To final

                CType(con.FindControl("pagina" & counter.ToString), HtmlGenericControl).Attributes("onclick") = Page.ClientScript.GetPostBackClientHyperlink(GVBloqueos, "Page$" & counter.ToString)
            Next
        End If
    End Sub
End Class
