﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" 
    AutoEventWireup="false" CodeFile="Licencias.aspx.vb" 
    Inherits="administrador_AgruparReactivos" EnableEventValidation="false" %>

<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header">
                <i class="widget-icon fa fa-check"></i>
                <span class="widget-caption">Área para crear licencias</span>
                <div class="widget-buttons">
                    <a href="#" data-toggle="config">
                        <i class="fa fa-cog"></i>
                    </a>
                    <a href="#" data-toggle="maximize">
                        <i class="fa fa-expand"></i>
                    </a>
                    <a href="#" data-toggle="collapse">
                        <i class="fa fa-minus"></i>
                    </a>
                    <a href="#" data-toggle="dispose">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
                <!--Widget Buttons-->
            </div>
            <!--Widget Header-->
            <div class="widget-body">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>

                        <div class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading"></div>
                                <div class="panel-body">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Clave</label>
                                            <div class="col-lg-10">
                                                <asp:TextBox ID="tbClave" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Fecha expiración</label>
                                            <div class="col-lg-10">
                                                <asp:TextBox ID="tbExpiracion" runat="server"
                                                    data-date-format="dd/mm/yyyy" CssClass="datepickerGlobal form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Fecha Alta (Opcional)</label>
                                            <div class="col-lg-10">
                                                <asp:TextBox ID="tbAlta" runat="server"
                                                    data-date-format="dd/mm/yyyy" CssClass="datepickerGlobal form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Estatus</label>
                                            <div class="col-lg-10">
                                                <asp:DropDownList ID="ddlEstatus" runat="server" CssClass="form-control">
                                                    <asp:ListItem>Activo</asp:ListItem>
                                                    <asp:ListItem>Suspendido</asp:ListItem>
                                                    <asp:ListItem>Baja</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                              <asp:LinkButton ID="btnCrearNuevaLicencia"  runat="server"  type="submit"
                                                CssClass="btnCrearCal btn  btn-lg btn-palegreen">
                                             <i class="btn-label fa fa-plus"></i>Crear licencia
                                            </asp:LinkButton>
                                              <asp:LinkButton ID="btnActualizarLicencia"  runat="server" Visible="false"
                                                CssClass="btnCrearCal btn  btn-lg btn-palegreen">
                                             <i class="btn-label fa fa-refresh"></i>Actualizar licencia
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="table-responsive">
                                            <asp:GridView ID="gvLicencias" runat="server"
                                                AllowPaging="true"
                                                AllowSorting="True"
                                                AutoGenerateColumns="False"
                                                DataKeyNames="IdLicencia,Clave,FechaAlta,FechaExpira,Estatus"
                                                DataSourceID="SDSLicencias"
                                                PageSize="5"
                                                CellPadding="3"
                                                CssClass="table table-striped table-bordered"
                                                Height="14px"
                                                Style="font-size: xx-small; text-align: left;">
                                                <Columns>

                                                    <asp:BoundField
                                                        DataField="IdLicencia"
                                                        HeaderText="Id en Sistema"
                                                        SortExpression="IdLicencia" />
                                                    <asp:BoundField
                                                        DataField="Clave"
                                                        HeaderText="Clave"
                                                        SortExpression="Clave" />
                                                    <asp:BoundField
                                                        DataField="FechaAlta"
                                                        HeaderText="Fecha de Alta (dd/mm/aaaa)"
                                                        SortExpression="FechaAlta" />
                                                    <asp:BoundField
                                                        DataField="FechaExpira"
                                                        HeaderText="Fecha de Expiración (dd/mm/aaaa)"
                                                        SortExpression="FechaExpira" />
                                                    <asp:BoundField
                                                        DataField="Estatus"
                                                        HeaderText="Estatus"
                                                        SortExpression="Estatus" />
                                                </Columns>
                                                <PagerTemplate>
                                                    <ul runat="server" id="Pag" class="pagination">
                                                    </ul>
                                                </PagerTemplate>
                                                <PagerStyle HorizontalAlign="Center" />
                                                <SelectedRowStyle CssClass="row-selected" />
                                                <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                            </asp:GridView>
                                        </div>
                                         <div class="form-group">
                                            <asp:LinkButton ID="btnEliminarLicencia"  runat="server" Visible="false"
                                                CssClass="btnCrearCal btn  btn-lg btn-warning">
                                                                <i class="btn-label fa fa-remove"></i>Eliminar licencia
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                   
                                    <div class="col-lg-12">
                                        <uc1:msgSuccess runat="server" ID="msgSuccess" />
                                        <uc1:msgError runat="server" ID="msgError" />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

 <asp:SqlDataSource ID="SDSLicencias" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM Licencia"></asp:SqlDataSource>








</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">

    <script type="text/javascript">

        Sys.Application.add_load(LoadHandler);

        function LoadHandler(sender, args0) {

            $(function () {
                $('.datepickerGlobal').datepicker();
            });
        }


    </script>
</asp:Content>

