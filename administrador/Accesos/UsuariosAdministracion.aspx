﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="UsuariosAdministracion.aspx.vb" Inherits="administrador_AgruparReactivos" %>

<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat radius-bordered">
            <div class="widget-header bg-themeprimary">
                <span class="widget-caption">Área para la administración de reactivos y opciones </span>
            </div>

            <div class="widget-body">
                <div class="widget-main ">
                    <div class="tabbable">
                        <ul class="nav nav-tabs tabs-flat" id="myTab11">
                            <li id="UsuariosSoporte" class="active">
                                <a data-toggle="tab" href="#crearUsuariosSoporte">Usuarios de soporte
                                </a>
                            </li>
                            <li>
                                <a id="UsuariosOperadores" data-toggle="tab" href="#crearOperadores">Crear operadores
                                </a>
                            </li>
                            <li>
                                <a id="AsignarOperadores" data-toggle="tab" href="#asignarOperadores">Asignar usuarios de soporte
                                </a>
                            </li>

                        </ul>
                        <div class="tab-content tabs-flat">


                            <div id="crearUsuariosSoporte" class="tab-pane in active">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <div class="form-horizontal">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                </div>
                                                <div class="panel-body">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Nombre</label>
                                                            <div class="col-lg-10">
                                                                <asp:TextBox ID="TBnombreUsuariosSoporte" runat="server" MaxLength="50" CssClass="form-control">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Apellidos</label>
                                                            <div class="col-lg-10">
                                                                <asp:TextBox ID="TBapellidosUsuariosSoporte" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Correo electrónico</label>
                                                            <div class="col-lg-10">
                                                                <asp:TextBox ID="TBemailUsuariosSoporte" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Login</label>
                                                            <div class="col-lg-10">
                                                                <asp:TextBox ID="TBloginUsuariosSoporte" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Password</label>
                                                            <div class="col-lg-10">
                                                                <asp:TextBox ID="TBpasswordUsuariosSoporte" runat="server" MaxLength="20" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Perfil</label>
                                                            <div class="col-lg-10">
                                                                <asp:DropDownList ID="DDLperfilUsuariosSoporte" runat="server" CssClass="form-control"
                                                                    AutoPostBack="True">
                                                                    <asp:ListItem Value="superadministrador ">Superadministrador </asp:ListItem>
                                                                    <asp:ListItem Value="capturista" Selected="True">Capturista</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <asp:LinkButton ID="btnCrearUsuarioSoporte" runat="server"
                                                                CssClass="btn btn-lg btn-labeled btn-palegreen space-margin">
                                                        <i class="btn-label fa fa-plus"></i>
                                                            Insertar
                                                            </asp:LinkButton>

                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <div class="table-responsive">
                                                                <asp:GridView ID="GVadmin" runat="server" AllowPaging="True"
                                                                    AllowSorting="True" AutoGenerateColumns="False" Caption="Cuentas creadas"
                                                                    CellPadding="3"
                                                                    DataKeyNames="IdAdmin,Nombre,Apellidos,Email,Login,Password,PerfilASP" DataSourceID="SDSadmin"
                                                                    CssClass="table table-striped table-bordered"
                                                                    Height="17px"
                                                                    Style="font-size: x-small; text-align: left;">
                                                                    <Columns>

                                                                        <asp:BoundField DataField="IdAdmin" HeaderText="IdAdmin" InsertVisible="False"
                                                                            ReadOnly="True" SortExpression="IdAdmin" />
                                                                        <asp:BoundField DataField="Nombre" HeaderText="Nombre"
                                                                            SortExpression="Nombre" />
                                                                        <asp:BoundField DataField="Apellidos" HeaderText="Apellidos"
                                                                            SortExpression="Apellidos" />
                                                                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                                                        <asp:BoundField DataField="Login" HeaderText="Login" SortExpression="Login" />
                                                                        <asp:BoundField DataField="Password" HeaderText="Password"
                                                                            SortExpression="Password" />
                                                                        <asp:BoundField DataField="PerfilASP" HeaderText="Perfil"
                                                                            SortExpression="PerfilASP" />
                                                                        <asp:BoundField DataField="FechaAlta" DataFormatString="{00:dd/MM/yyyy}"
                                                                            HeaderText="Fecha Alta" SortExpression="FechaAlta" />
                                                                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                                            SortExpression="Estatus" />
                                                                    </Columns>
                                                                    <PagerTemplate>
                                                                        <ul runat="server" id="Pag" class="pagination">
                                                                        </ul>
                                                                    </PagerTemplate>
                                                                    <PagerStyle HorizontalAlign="Center" />
                                                                    <SelectedRowStyle CssClass="row-selected" />
                                                                    <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </div>
                                                            <asp:LinkButton ID="btnEliminarUsuarioSoporte" runat="server" Visible="false"
                                                                CssClass="btn btn-lg btn-labeled btn-darkorange space-margin">
                                                        <i class="btn-label fa fa-remove"></i>
                                                            Eliminar
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12">
                                                        <uc1:msgSuccess runat="server" ID="msgSuccess" />

                                                        <uc1:msgError runat="server" ID="msgError" />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>


                            <div id="asignarOperadores" class="tab-pane">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <div class="form-horizontal">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <label class="col-lg-2 control-label">
                                                            Institución
                                                        </label>
                                                        <div class="col-lg-6">
                                                            <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                                                                DataSourceID="SDSinstituciones" DataTextField="Descripcion"
                                                                DataValueField="IdInstitucion" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <div class="table-responsive">
                                                                <div class="col-lg-10">
                                                        <asp:GridView ID="GVPlanteles" runat="server"
                                                            AllowPaging="True"
                                                            AllowSorting="True"
                                                            AutoGenerateColumns="False"
                                                            DataKeyNames="IdPlantel"
                                                            DataSourceID="SDSPlanteles"
                                                            Caption="<h3>Seleccione el PLANTEL que desea asignar al Operador elegido, debe asignar uno por uno</h3>"
                                                            PageSize="15" 
                                                            CssClass="table table-striped table-bordered"
                                                            Height="17px"
                                                            Style="font-size: x-small; text-align: left;">
                                                            <Columns>
                                                               
                                                                <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel" />
                                                                <asp:BoundField DataField="IdLicencia" HeaderText="IdLicencia"
                                                                    SortExpression="IdLicencia" />
                                                                <asp:BoundField DataField="IdInstitucion" HeaderText="IdInstitucion"
                                                                    SortExpression="IdInstitucion" />
                                                                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion"
                                                                    SortExpression="Descripcion" />

                                                            </Columns>
                                                              <PagerTemplate>
                                                                        <ul runat="server" id="Pag" class="pagination">
                                                                        </ul>
                                                                    </PagerTemplate>
                                                                    <PagerStyle HorizontalAlign="Center" />
                                                            <SelectedRowStyle CssClass="row-selected" />
                                                            <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />

                                                        </asp:GridView>
                                                                </div>
                                                            </div>
                                                            <asp:LinkButton ID="btnAsignarOperador" runat="server" Visible="false"
                                                                CssClass="btn btn-lg btn-labeled btn-palegreen space-margin">
                                                        <i class="btn-label fa fa-plus"></i>
                                                            Asignar
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <div class="table-responsive">
                                                                <div class="col-lg-10">
                                                                <asp:GridView ID="GVDatosPlanteles" runat="server"
                                                                    AllowPaging="True" 
                                                                    AllowSorting="True"
                                                                    AutoGenerateColumns="False"
                                                                    DataSourceID="SDSinformacion"
                                                                    DataKeyNames="IdPlantel,IdOperador"
                                                                    Caption="<h3>Listado de PLANTELES asignados a Operadores de la INSTITUCION elegida</h3>"
                                                                    PageSize="10"
                                                                    CssClass="table table-striped table-bordered"
                                                                    Height="17px"
                                                                    Style="font-size: x-small; text-align: left;"
                                                                    CellPadding="3">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="IdLicencia" HeaderText="Id_Licencia"
                                                                            SortExpression="IdLicencia" />
                                                                        <asp:BoundField DataField="Clave" HeaderText="Licencia"
                                                                            SortExpression="Clave" />
                                                                        <asp:BoundField DataField="IdInstitucion" HeaderText="Id-Inst."
                                                                            InsertVisible="False" ReadOnly="True" SortExpression="IdInstitucion" />
                                                                        <asp:BoundField DataField="Descripcion" HeaderText="Institución"
                                                                            SortExpression="Descripcion" />
                                                                        <asp:BoundField DataField="IdOperador" HeaderText="Id-Op."
                                                                            InsertVisible="False" ReadOnly="True" SortExpression="IdOperador" />
                                                                        <asp:BoundField DataField="NomOperador" HeaderText="Operador"
                                                                            ReadOnly="True" SortExpression="NomCoordinador" />
                                                                        <asp:BoundField DataField="IdPlantel" HeaderText="Id-Plantel"
                                                                            InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel" />
                                                                        <asp:BoundField DataField="Descripcion1" HeaderText="Plantel"
                                                                            SortExpression="Descripcion1" />
                                                                    </Columns>
                                                                  <PagerTemplate>
                                                                        <ul runat="server" id="Pag" class="pagination">
                                                                        </ul>
                                                                    </PagerTemplate>
                                                                    <PagerStyle HorizontalAlign="Center" />
                                                                    <SelectedRowStyle CssClass="row-selected" />
                                                                    <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                                </div>
                                                            </div>
                                                            <asp:LinkButton ID="btnDesasignarOperador" runat="server" Visible="false"
                                                                CssClass="btn btn-lg btn-labeled btn-darkorange space-margin">
                                                        <i class="btn-label fa fa-remove"></i>
                                                            Desasignar
                                                            </asp:LinkButton>

                                                            
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-12">
                                                          <uc1:msgSuccess runat="server" ID="msgSuccess3" />
                                                         <uc1:msgError runat="server" ID="msgError3" />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <div id="crearOperadores" class="tab-pane">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <div class="form-horizontal">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                </div>
                                                <div class="panel-body">
                                                    <div class="col-lg-6">
                                                         
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Nombre</label>
                                                            <div class="col-lg-10">
                                                                <asp:TextBox runat="server" ID="TBnombreOperador"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Apellidos</label>
                                                            <div class="col-lg-10">
                                                                 <asp:TextBox runat="server" ID="TBApellidosOperador"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Código asignado</label>
                                                            <div class="col-lg-10">
                                                                 <asp:TextBox runat="server" ID="TBCodigoAsignadoOperador"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="form-group">
                                                            <label class="col-lg-2 control-label">Correo Electrónico</label>
                                                            <div class="col-lg-4">
                                                                 <asp:TextBox runat="server" ID="TBCorreoOperador"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                            <label class="col-lg-2 control-label">Estatus</label>
                                                             <div class="col-lg-4">
                                                                 <asp:DropDownList ID="DDLEstatusOperador" CssClass="form-control" runat="server">
                                                                     <asp:ListItem>Activo</asp:ListItem>
                                                                     <asp:ListItem>Suspendido</asp:ListItem>
                                                                     <asp:ListItem>Baja</asp:ListItem>
                                                                 </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                          <div class="form-group">
                                                            <label class="col-lg-2 control-label">Login</label>
                                                            <div class="col-lg-10">
                                                                 <asp:TextBox runat="server" ID="TBLoginOperador"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                          <div class="form-group">
                                                            <label class="col-lg-2 control-label">Contraseña</label>
                                                            <div class="col-lg-10">
                                                                 <asp:TextBox runat="server" ID="TBPasswordOperador"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                         <div class="form-group">
                                                            <label class="col-lg-2 control-label">Confirma contraseña</label>
                                                            <div class="col-lg-10">
                                                                 <asp:TextBox runat="server" ID="TBConfirmaPassword"
                                                                    CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-12">
                                                            <asp:LinkButton ID="btnCrearOperador" runat="server" 
                                                                CssClass="btn btn-lg btn-labeled btn-palegreen shiny space-margin">
                                                        <i class="btn-label fa fa-plus"></i>
                                                            Crear
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="btnActualizarOperador" runat="server" Visible="false"
                                                                CssClass="btn btn-lg btn-labeled btn-palegreen space-margin">
                                                        <i class="btn-label fa fa-refresh"></i>
                                                            Actualizar
                                                            </asp:LinkButton>
                                                        </div>

                                                    </div>
                                                    <div class="col-lg-6">
                                                        
                                                        <div class="form-group">
                                                            <div class="table-responsive">
                                                                <asp:GridView ID="GVcoordinadores" runat="server"
                                                                    AllowPaging="True"
                                                                    AllowSorting="True"
                                                                    AutoGenerateColumns="False"
                                                                    Caption="Operadores asigandos al PLANTEL elegido"
                                                                    DataSourceID="SDSlistaoperadores"
                                                                    CssClass="table table-striped table-bordered"
                                                                    DataKeyNames="IdOperador,Clave,Apellidos,Nombre,Login,Contraseña,Email,Estatus"
                                                                    Height="17px" 
                                                                    Style="font-size: x-small; text-align: left;">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Clave" HeaderText="Clave" SortExpression="Clave">
                                                                            <HeaderStyle Width="85px" />
                                                                            <ItemStyle Width="85px" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Apellidos" HeaderText="Apellidos" ReadOnly="True"
                                                                            SortExpression="Apellidos" />
                                                                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" ReadOnly="True"
                                                                            SortExpression="Nombre" />
                                                                        <asp:BoundField DataField="Login" HeaderText="Login" ReadOnly="True"
                                                                            SortExpression="Login" />
                                                                        <asp:BoundField DataField="Contraseña" HeaderText="Contraseña" ReadOnly="True"
                                                                            SortExpression="Contraseña">
                                                                            
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                                                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                                            SortExpression="Estatus" />
                                                                    </Columns>
                                                                    <PagerTemplate>
                                                                        <ul runat="server" id="Pag" class="pagination">
                                                                        </ul>
                                                                    </PagerTemplate>
                                                                    <PagerStyle HorizontalAlign="Center" />
                                                                    <SelectedRowStyle CssClass="row-selected" />
                                                                    <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </div>
                                                             <asp:LinkButton ID="btnEliminarOperador" runat="server" Visible="false"
                                                                CssClass="btn btn-lg btn-labeled btn-darkorange space-margin">
                                                        <i class="btn-label fa fa-remove"></i>
                                                            Eliminar
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                   
                                                      <div class="col-lg-12">
                                                        <uc1:msgSuccess runat="server" ID="msgSuccess2" />
                                                        <uc1:msgError runat="server" ID="msgError2" />
                                                    </div>


                                                </div>

                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <asp:SqlDataSource ID="SDSadmin" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
        SelectCommand="
                    SELECT 
                        A.[IdAdmin], 
                        A.[Nombre], 
                        A.[Apellidos], 
                        A.[Email], 
                        U.[Login], 
                        A.[FechaAlta], 
                        U.[PerfilASP], 
                        U.[Password], 
                        A.[Estatus] 
                    FROM 
                        [Admin] A
                        ,Usuario U
                    WHERE 
                        (U.PerfilASP = @PerfilASP) 
                        AND A.IdUsuario = U.IdUsuario
                    ORDER BY A.[Apellidos], A.[Nombre], A.[FechaAlta]">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLperfilUsuariosSoporte" DefaultValue="Superadministrador"
                Name="PerfilASP" PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
      

    <asp:SqlDataSource ID="SDSlistaoperadores" runat="server"
        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select  C.IdOperador, C.Clave,rtrim(C.Apellidos) as Apellidos, rtrim(C.Nombre) as Nombre, rtrim(U.Login) as Login, rtrim(U.Password) as Contraseña, rtrim(C.Email) as Email, C.Estatus
from Operador C, Usuario U
where U.IdUsuario = C.IdUsuario
order by C.Apellidos, C.Nombre
">
    </asp:SqlDataSource>

          <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdInstitucion], [Descripcion] FROM [Institucion] ORDER BY [Descripcion]"></asp:SqlDataSource>

    
                <asp:SqlDataSource ID="SDSPlanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdPlantel], [IdLicencia], [IdInstitucion], [Descripcion] FROM [Plantel] WHERE [IdInstitucion] = @IdInstitucion AND IdPlantel NOT IN (SELECT IdPlantel FROM OperadorPlantel WHERE IdOperador = @IdOperador) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="GVcoordinadores" Name="IdOperador"
                            PropertyName="SelectedDataKey.Values[IdOperador]" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
      <asp:SqlDataSource ID="SDSinformacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select P.IdLicencia, L.Clave, I.IdInstitucion, I.Descripcion, C.IdOperador, C.Apellidos+', '+Nombre+ ' (' + C.Clave+ ')' NomOperador, P.IdPlantel, P.Descripcion
from Licencia L, Institucion I, Operador C, Plantel P, OperadorPlantel CP
where P.IdLicencia = L.IdLicencia and P.IdInstitucion = I.IdInstitucion and P.IdPlantel = CP.IdPlantel and C.IdOperador = CP.IdOperador and I.IdInstitucion = @IdInstitucion
order by I.Descripcion, P.Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLinstitucion" Name="IdInstitucion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

       <asp:SqlDataSource ID="SDSOpePlantel" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CoordinadorPlantel]"></asp:SqlDataSource>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" runat="Server">
</asp:Content>

