﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false"
     CodeFile="AlumnosFirmados.aspx.vb" Inherits="administrador_AgruparReactivos"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content Runat="server" ContentPlaceHolderID="Title">
    Reporte de 
								<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    registrados y no registrados de una
        <asp:Label ID="Label4" runat="server" Text="[INSTITUCION]" />
    
 </asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">


    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="widget">
                <div class="widget-header">
                    <i class="widget-icon fa fa-check"></i>
                    <span class="widget-caption">Alumnos Firmados</span>
                    <div class="widget-buttons">
                        <a href="#" data-toggle="config">
                            <i class="fa fa-cog"></i>
                        </a>
                        <a href="#" data-toggle="maximize">
                            <i class="fa fa-expand"></i>
                        </a>
                        <a href="#" data-toggle="collapse">
                            <i class="fa fa-minus"></i>
                        </a>
                        <a href="#" data-toggle="dispose">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                    <!--Widget Buttons-->
                </div>
                <!--Widget Header-->
                <div class="widget-body">
                    <div class="form-horizontal ">
                        <div class="panel panel-default">
                            <div class="panel-heading"></div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <asp:Label ID="Label2" CssClass="col-lg-2" runat="server" Text="[INSTITUCION]" />

                                    <div class="col-lg-10">
                                        <asp:DropDownList ID="DDLinstitucion" runat="server" CssClass="form-control"
                                            DataSourceID="SDSinstitucion" DataTextField="Descripcion"
                                            DataValueField="IdInstitucion" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                     <asp:Label ID="Label3" CssClass="col-lg-2" runat="server" Text="Alumnos a desplegar" />
                                                   
                                    <div class="col-lg-10">
                                        <asp:DropDownList ID="DDLalumnos" runat="server" CssClass="form-control"
                                            AutoPostBack="True">
                                            <asp:ListItem Value="IS NOT NULL">Registrados</asp:ListItem>
                                            <asp:ListItem Value="IS NULL">Sin registrarse</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <asp:LinkButton ID="BtnExportar" runat="server" 
                        CssClass="btn btn-lg btn-labeled btn-palegreen space-margin">
                        <i class="btn-label fa fa-table"></i>
                        Exportar a Excel
                    </asp:LinkButton>

                    <asp:GridView ID="GVsalida" runat="server"
                        AutoGenerateColumns="false"
                        PageSize="100"
                        DataSourceID="SDSreporte"
                        Width="722px"
                        CssClass="dataGrid_clear"
                        GridLines="None">
                        <Columns>
                            <asp:BoundField DataField="Matricula" HeaderText="Matricula" />
                            <asp:BoundField DataField="ApePaterno" HeaderText="ApePaterno" />
                            <asp:BoundField DataField="ApeMaterno" HeaderText="ApeMaterno" />
                            <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                            <asp:BoundField DataField="Grupo" HeaderText="Grupo" />
                            <asp:BoundField DataField="Plantel" HeaderText="Plantel" />
                        </Columns>
                        <FooterStyle CssClass="footer" />
                        <PagerStyle CssClass="pager" />
                        <SelectedRowStyle CssClass="selected" />
                        <HeaderStyle CssClass="header" />
                        <AlternatingRowStyle CssClass="altrow" />
                    </asp:GridView>
                    <%-- sort NO - prerender
	                0  A.Matricula
	                1  A.ApePaterno
	                2  A.ApeMaterno
	                3  A.Nombre
	                4  G.Descripcion GRUPO
	                5  P.Descripcion PLANTEL
                    --%>

                    <uc1:msgError runat="server" ID="msgError" />
                </div>
                <!--Widget Body-->
            </div>
            <!--Widget-->
        </div>
    </div>







    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSinstitucion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdInstitucion, Descripcion, Estatus
from Institucion
where Estatus = 'Activo'
order by Descripcion"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSreporte" runat="server"></asp:SqlDataSource>

            </td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">
</asp:Content>

