﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Site.Master" EnableEventValidation="false"
    AutoEventWireup="false" CodeFile="Desbloquea.aspx.vb" Inherits="administrador_AgruparReactivos" %>


<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">

    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget flat radius-bordered">
            <div class="widget-header bg-themeprimary">
                <span class="widget-caption">Área para bloqueo y desbloqueo </span>
            </div>

            <div class="widget-body">
                <div class="widget-main ">
                    <div class="tabbable">
                        <ul class="nav nav-tabs tabs-flat" id="tabInit">
                              <li class="active">
                                <a id="desbloqueoG" data-toggle="tab" href="#desbloqueoGeneral">Desbloqueo general
                                </a>
                            </li>
                            <li id="bloqueosPerfil" >
                                <a data-toggle="tab" href="#bloqueos">Configuración de bloqueos por perfil
                                </a>
                            </li>
                          
                        </ul>
                        <div class="tab-content tabs-flat">
                            <div id="desbloqueoGeneral" class="tab-pane in active ">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>


                                        <div class="form-horizontal">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <asp:Label ID="LblActivos" runat="server" CssClass="col-lg-5 control-label"
                                                            Style="font-family: Arial, Helvetica, sans-serif; font-size: small; color: #000066"></asp:Label>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-lg-6">
                                                            <div class="table-responsive">
                                                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False"
                                                                    CellPadding="3" DataSourceID="SDSusuarios" CssClass="table table-striped table-bordered"
                                                                    Height="25px"
                                                                    Style="font-size: x-small; text-align: left;">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="Rehabilitadas" HeaderText="Cuentas deshabilitadas"
                                                                            ReadOnly="True" SortExpression="Rehabilitadas" />
                                                                    </Columns>
                                                                    <PagerTemplate>
                                                                        <ul runat="server" id="Pag" class="pagination">
                                                                        </ul>
                                                                    </PagerTemplate>
                                                                    <PagerStyle HorizontalAlign="Center" />
                                                                    <SelectedRowStyle CssClass="row-selected" />
                                                                    <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <asp:LinkButton ID="btndesbloquearCuentas" runat="server"
                                                            CssClass="btnCrearPlanteamiento btn btn-lg btn-labeled btn-palegreen space-margin">
                                                        <i class=" btn-label fa fa-plus"></i>
                                                            Desbloquear
                                                        </asp:LinkButton>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <uc1:msgInfo runat="server" ID="msgInfo2" />
                                                        <uc1:msgSuccess runat="server" ID="msgSuccess2" />

                                                        <uc1:msgError runat="server" ID="msgError2" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                            <div id="bloqueos" class="tab-pane">
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                <div class="form-horizontal">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                        </div>
                                        <div class="panel-body">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">
                                                        Institución
                                                    </label>
                                                    <div class="col-lg-10">
                                                        <asp:DropDownList ID="DDLInstitucion" runat="server"
                                                            AutoPostBack="True"
                                                            DataSourceID="SDSinstituciones"
                                                            DataTextField="Descripcion"
                                                            DataValueField="Descripcion"
                                                            CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">
                                                        Día
                                                    </label>
                                                    <div class="col-lg-10">
                                                        <asp:DropDownList ID="DDLDia" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="Lunes" Selected="True">Lunes</asp:ListItem>
                                                            <asp:ListItem Value="Martes">Martes</asp:ListItem>
                                                            <asp:ListItem Value="Miercoles">Miércoles</asp:ListItem>
                                                            <asp:ListItem Value="Jueves">Jueves</asp:ListItem>
                                                            <asp:ListItem Value="Viernes">Viernes</asp:ListItem>
                                                            <asp:ListItem Value="Sabado">Sabado</asp:ListItem>
                                                            <asp:ListItem Value="Domingo">Domingo</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Perfil</label>
                                                    <div class="col-lg-10">
                                                        <asp:DropDownList ID="DDLPerfil" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="Alumno" Selected="True">Alumno</asp:ListItem>
                                                            <asp:ListItem Value="Profesor">Profesor</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Hora de inicio:</label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <asp:TextBox runat="server" ID="TBInicioHora" CssClass="form-control timePickerInicioDesbloqueo"></asp:TextBox>
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-clock-o"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-2 control-label">Hora de fin:</label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <asp:TextBox runat="server" ID="TBFinHora" CssClass="form-control timePickerFinDesbloqueo"></asp:TextBox>
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-clock-o"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <asp:LinkButton ID="btnCrearBloqueo" runat="server"
                                                        CssClass="btnCrearPlanteamiento btn btn-lg btn-labeled btn-palegreen space-margin">
                                                        <i class=" btn-label fa fa-plus"></i>
                                                            Insertar
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnEliminarBloqueo" runat="server"
                                                        CssClass="btn btn-lg btn-labeled btn-darkorange space-margin shiny">
                                                        <i class="btn-label fa fa-remove"></i>
                                                            Eliminar
                                                    </asp:LinkButton>

                                                </div>
                                                
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="table-responsive">
                                                        <asp:GridView ID="GVBloqueos" runat="server"
                                                            AllowSorting="True"
                                                            AutoGenerateColumns="True"
                                                            Caption="<h3>Bloqueos Establecidos</h3>"
                                                            CssClass="table table-striped table-bordered"
                                                            Height="17px"
                                                            Style="font-size: x-small; text-align: left;">
                                                            <PagerTemplate>
                                                                <ul runat="server" id="Pag" class="pagination">
                                                                </ul>
                                                            </PagerTemplate>
                                                            <PagerStyle HorizontalAlign="Center" />
                                                            <SelectedRowStyle CssClass="row-selected" />
                                                            <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <blockquote>
                                                        <small>NOTA:</small>
                                                        <p>
                                                            Si un usuario está activo cuando llegue una hora de bloqueo,
                                                          el sistema cierra su sesión sólo cuando ingrese al menú 
                                                         principal de su perfil, y si éste es alumno también cuando
                                                          ingrese al menú de actividades pendientes.
                                                        </p>

                                                    </blockquote>
                                                </div>
                                                <uc1:msgSuccess runat="server" ID="msgSuccess" />

                                                <uc1:msgError runat="server" ID="msgError" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


      <asp:SqlDataSource ID="SDSinstituciones" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
            SelectCommand="SELECT [IdInstitucion], [Descripcion] FROM [Institucion] ORDER BY [Descripcion]">
        </asp:SqlDataSource>
    <asp:SqlDataSource ID="SDSusuarios" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>" SelectCommand="SELECT Count(*) Rehabilitadas FROM [vw_aspnet_MembershipUsers]
where IsLockedOut = 'True'"></asp:SqlDataSource>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PageScriptContent" Runat="Server">

</asp:Content>

