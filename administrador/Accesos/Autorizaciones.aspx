﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Autorizaciones.aspx.cs" Inherits="administrador_Accesos_Autorizaciones" %>



<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">



    <asp:UpdatePanel runat="server" ID="UPAutorizaciones">
        <ContentTemplate>


            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12">
                    <div class="widget">
                        <div class="widget-header bg-themeprimary">
                            <i class="widget-icon fa fa-check"></i>
                            <span class="widget-caption">Alumnos Firmados</span>
                            <div class="widget-buttons">
                                <a href="#" data-toggle="config">
                                    <i class="fa fa-cog"></i>
                                </a>
                                <a href="#" data-toggle="maximize">
                                    <i class="fa fa-expand"></i>
                                </a>
                                <a href="#" data-toggle="collapse">
                                    <i class="fa fa-minus"></i>
                                </a>
                                <a href="#" data-toggle="dispose">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                            <!--Widget Buttons-->
                        </div>
                        <!--Widget Header-->
                        <div class="widget-body">
                            <div class="form-horizontal">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <b>Autorizaciones Pendientes</b>
                                    </div>
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <div class="col-lg-8 col-lg-offset-2">


                                                <div class="table-responsive">
                                                    <asp:GridView ID="GVAutorizaciones" runat="server"
                                                        AutoGenerateColumns="False" OnDataBound="GVAutorizaciones_DataBound"
                                                        AllowPaging="true" 
                                                        DataSourceID="SDSAutorizacion"
                                                        DataKeyNames="IdAlumno,Login"
                                                        PageSize="5" OnRowDataBound="GVAutorizaciones_RowDataBound"
                                                        OnRowCreated="GVAutorizaciones_RowCreated"
                                                        CellPadding="3"
                                                        CssClass="table table-striped table-bordered"
                                                        Height="17px"
                                                        Style="font-size: x-small; text-align: left;">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                         
                                                                <ItemTemplate >
                                                                    <asp:CheckBox runat="server"  />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="IdAlumno" Visible="false" HeaderText="IdAlumno" />
                                                            <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                                                            <asp:BoundField DataField="Asignaturas" HeaderText="Asignaturas" />
                                                        </Columns>
                                                        <PagerTemplate>
                                                            <ul runat="server" id="Pag" class="pagination">
                                                            </ul>
                                                        </PagerTemplate>
                                                        <PagerStyle HorizontalAlign="Center" />
                                                        <SelectedRowStyle CssClass="row-selected" />
                                                        <HeaderStyle BackColor="#121212" Font-Bold="True" ForeColor="White" />
                                                    </asp:GridView>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                               <asp:LinkButton ID="btnAceptarAlumno" runat="server"  OnClick="btnAceptarAlumno_Click"
                                                        CssClass="btnCrearPlanteamiento btn btn-lg btn-labeled btn-palegreen space-margin">
                                                        <i class=" btn-label fa fa-plus"></i>
                                                            Aceptar Alumno
                                                    </asp:LinkButton>
                                               <asp:LinkButton ID="btnRechazarAlumno" runat="server"  OnClick="btnRechazarAlumno_Click"
                                                        CssClass="btn btn-lg btn-labeled btn-darkorange space-margin shiny">
                                                        <i class="btn-label fa fa-remove"></i>
                                                            Rechazar Alumno
                                               </asp:LinkButton>
                                        </div>
                                        <div class="form-group">
                                            <uc1:msgSuccess runat="server" ID="MsgSuccess" />
                                            <uc1:msgError runat="server" ID="MsgError" />
                                            <uc1:msgInfo runat="server" ID="MsgInfo" />
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>






    <asp:SqlDataSource ID="SDSAutorizacion" runat="server"
            ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
           SelectCommandType="StoredProcedure"
            SelectCommand="GetRegisterUser">
        </asp:SqlDataSource>




</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageScriptContent" Runat="Server">
</asp:Content>

