﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO

Partial Class profesor_MensajeP
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        If Session("Envio_Previous").Equals("LeerMensajeSalidaP") Then
            HyperLink2.NavigateUrl = "~/profesor/LeerMensajeSalidaP.aspx"
            LBresponder.Visible = False
            LBresponderC.Visible = False
        Else
            HyperLink2.NavigateUrl = "~/profesor/LeerMensajeEntradaP.aspx"
        End If

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            aplicaLenguaje()

            initPageData()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = "Leer Mensaje"

        If Session("Envio_Previous").Equals("LeerMensajeSalidaP") Then
            lt_hintAlumno.Text = Config.Etiqueta.ALUMNO & " Destinatario"
            lt_hintCoordinador.Text = Config.Etiqueta.COORDINADOR & " Destinatario"

            If Session("Envio_IdComunicacion") <> "0" Then
                lt_titulo.Text = "Mensaje Enviado a " & Config.Etiqueta.ALUMNO
            ElseIf Session("Envio_IdComunicacionCP") <> "0" Then
                lt_titulo.Text = "Mensaje Enviado a " & Config.Etiqueta.COORDINADOR
            End If
        Else
            lt_hintAlumno.Text = Config.Etiqueta.ALUMNO & " que Envía"
            lt_hintCoordinador.Text = Config.Etiqueta.COORDINADOR & " que Envía"

            If Session("Envio_IdComunicacion") <> "0" Then
                lt_titulo.Text = "Mensaje Recibido de " & Config.Etiqueta.ALUMNO
            ElseIf Session("Envio_IdComunicacionCP") <> "0" Then
                lt_titulo.Text = "Mensaje Recibido de " & Config.Etiqueta.COORDINADOR
            End If
        End If
        lt_hintAsignatura.Text = Config.Etiqueta.ASIGNATURA
        lt_hintPlantel.Text = Config.Etiqueta.PLANTEL
    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub initPageData()
        'Hago consulta para sacar los datos del mensaje
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        miComando = objConexion.CreateCommand

        Try
            If Session("Envio_IdComunicacion") <> "0" Then
                pnlMensajeCoordinador.Visible = False
                'PRIMERO VEO SI EL MENSAJE FUE ENVIADO POR UN ALUMNO (si viene de alumno session("IdComunicacionCP")="0", y si viene de coordinador session("IdComunicacion")="0"
                miComando.CommandText = "select C.IdComunicacion, C.IdProgramacion, C.Fecha, C.Asunto, C.IdAlumno, A.Nombre + ' ' + A.ApePaterno + ' ' + A.ApeMaterno as 'Alumno', A.Email," + _
                        " C.Contenido, C.Fecha, C.Estatus, C.ArchivoAdjunto, C.ArchivoAdjuntoFisico, C.CarpetaAdjunto from Comunicacion C, Alumno A " + _
                        "where C.IdComunicacion = " + Session("Envio_IdComunicacion") + " and A.IdAlumno = C.IdAlumno"

                'Abrir la conexión y leo los registros del Planteamiento
                objConexion.Open()
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read() 'Leo para poder accesarlos, a fuerzas debe haber un registro

                ltEnvia.Text = HttpUtility.HtmlDecode(misRegistros.Item("Alumno"))
                ltFechaEnvio.Text = HttpUtility.HtmlDecode(misRegistros.Item("Fecha"))
                If Not IsDBNull(misRegistros.Item("Asunto")) Then
                    ltAsunto.Text = HttpUtility.HtmlDecode(misRegistros.Item("Asunto"))
                End If
                ltContenido.Text = HttpUtility.HtmlDecode(misRegistros.Item("Contenido"))
                ltAsignatura.Text = Session("Envio_Materia")

                'Los siguientes valores los utilizaré si le dan responder al mensaje
                hfIdAlumno.Value = misRegistros.Item("IdAlumno")
                hfIdProgramacion.Value = misRegistros.Item("IdProgramacion")
                If IsDBNull(misRegistros.Item("Email")) Then 'Si intento asignar un valor Nulo a una variable, marca error
                    hfEmail.Value = ""
                Else
                    hfEmail.Value = misRegistros.Item("Email")
                End If

                If Not IsDBNull(misRegistros.Item("ArchivoAdjunto")) Then
                    lbAdjunto.Visible = True
                    lt_hintAdjunto.Visible = True
                    hfAdjuntoFisico.Value = misRegistros.Item("ArchivoAdjuntoFisico")
                    lbAdjunto.Text = misRegistros.Item("ArchivoAdjunto")
                    hfCarpetaAdjunto.Value = misRegistros.Item("CarpetaAdjunto")
                Else
                    lbAdjunto.Visible = False
                    lbAdjunto.Text = ""
                    lt_hintAdjunto.Visible = False
                End If

                'Modificar el mensaje a "Leido" en caso de que este como "Nuevo"
                If misRegistros.Item("Estatus") <> "Leido" AndAlso Not Session("Envio_Previous").Equals("LeerMensajeSalidaP") Then
                    SDSmensaje.UpdateCommand = "SET dateformat dmy; UPDATE Comunicacion set Estatus = 'Leido' where IdComunicacion = " + Session("Envio_IdComunicacion")
                    SDSmensaje.Update()
                End If

                misRegistros.Close()
            ElseIf Session("Envio_IdComunicacionCP") <> "0" Then
                pnlMensajeAlumno.Visible = False
                'AHORA CHECO SI EL MENSAJE VIENE DE UN COORDINADOR
                miComando.CommandText = "select C.IdComunicacionCP, C.Fecha, C.Asunto, Coor.IdCoordinador, C.IdProfesor, Coor.Nombre + ' ' + Coor.Apellidos as 'Coordinador', Coor.Email," + _
                        "C.Contenido, C.Fecha, C.EstatusP, C.ArchivoAdjunto, C.ArchivoAdjuntoFisico, C.CarpetaAdjunto from ComunicacionCP C, Coordinador Coor " + _
                        "where C.IdComunicacionCP = " + Session("Envio_IdComunicacionCP") + " and Coor.IdCoordinador = C.IdCoordinador"

                'Abrir la conexión y leo los registros del Planteamiento
                objConexion.Open()
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read() 'Leo para poder accesarlos
                ltEnviaC.Text = HttpUtility.HtmlDecode(misRegistros.Item("Coordinador"))
                ltFechaEnvioC.Text = HttpUtility.HtmlDecode(misRegistros.Item("Fecha"))
                If Not IsDBNull(misRegistros.Item("Asunto")) Then
                    ltAsuntoC.Text = HttpUtility.HtmlDecode(misRegistros.Item("Asunto"))
                End If
                ltContenidoC.Text = HttpUtility.HtmlDecode(misRegistros.Item("Contenido"))
                hfIdProfesor.Value = HttpUtility.HtmlDecode(misRegistros.Item("IdProfesor"))
                ltPlantel.Text = Session("Envio_Plantel") 'Viene de LeerMensajeEntradaP.aspx

                'Los siguientes valores los utilizaré si le dan responder al mensaje
                hfIdCoordinador.Value = misRegistros.Item("IdCoordinador")

                If IsDBNull(misRegistros.Item("Email")) Then 'Si intento asignar un valor Nulo a una variable, marca error
                    hfEmail.Value = ""
                Else
                    hfEmail.Value = misRegistros.Item("Email")
                End If

                If Not IsDBNull(misRegistros.Item("ArchivoAdjunto")) Then
                    lbAdjuntoC.Visible = True
                    lt_hintAdjuntoC.Visible = True
                    hfAdjuntoFisico.Value = misRegistros.Item("ArchivoAdjuntoFisico")
                    lbAdjuntoC.Text = misRegistros.Item("ArchivoAdjunto")
                    hfCarpetaAdjunto.Value = misRegistros.Item("CarpetaAdjunto")
                Else
                    lbAdjuntoC.Visible = False
                    lbAdjuntoC.Text = ""
                    lt_hintAdjuntoC.Visible = False
                End If

                'Modificar el mensaje a "Leido" en caso de que este como "Nuevo"
                If misRegistros.Item("EstatusP") <> "Leido" AndAlso Not Session("Envio_Previous").Equals("LeerMensajeSalidaP") Then
                    SDSmensaje.UpdateCommand = "SET dateformat dmy; UPDATE ComunicacionCP set EstatusP = 'Leido' where IdComunicacionCP = " + Session("Envio_IdComunicacionCP")
                    SDSmensaje.Update()
                End If

                misRegistros.Close()

            End If

            objConexion.Close()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

#End Region

    Protected Sub LBeliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBeliminar.Click
        Dim eliminado As Boolean = False

        Try
            If CBeliminar.Checked Then
                'Para eliminarlo solo es necesario darlo de baja, y si lo está desplegando es que no está dado de baja
                SDSmensaje.UpdateCommand = "SET dateformat dmy; UPDATE Comunicacion set Estatus = 'Baja' where IdComunicacion = " + Session("Envio_IdComunicacion")
                SDSmensaje.Update()

                'A diferencia de cuando el Profesor envía un adjunto a los alumnos que no borro el archivo, aquí si lo borro porque normalmente un alumno manda a un solo profesor
                'Elimino el archivo adjunto para no acumular basura
                'If Trim(lbAdjunto.Text).Length > 0 Then
                '    Dim ruta As String
        '    ruta = Config.Global.rutaMaterial
        '    ruta = ruta + "adjuntos\" + Session("LoginAlumno") + "\"

        '    Dim MiArchivo As FileInfo = New FileInfo(ruta & lbAdjunto.Text)
        '    MiArchivo.Delete()
        'End If

        eliminado = True
      Else
        msgError.show("Debe marcar el mensaje para poder eliminarlo.")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try

    If eliminado Then
      If Session("Envio_Previous").Equals("LeerMensajeSalidaP") Then
        Response.Redirect("LeerMensajeSalidaP.aspx")
      Else
        Response.Redirect("LeerMensajeEntradaP.aspx")
      End If
    End If
  End Sub

  Protected Sub LBresponder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBresponder.Click
    Session("Envio_Asunto") = ltAsunto.Text
    Session("Envio_IdAlumno") = hfIdAlumno.Value
    Session("Envio_Nombre") = ltEnvia.Text 'NombreAlumno objetivo
    Session("Envio_Asignatura") = ltAsignatura.Text
    Session("Envio_Fecha") = ltFechaEnvio.Text
    Session("Envio_Email") = hfEmail.Value
    Session("Envio_IdProgramacion") = hfIdProgramacion.Value
    Response.Redirect("ResponderMensajeP.aspx")
  End Sub

  Protected Sub LBresponderC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBresponderC.Click
    Session("Envio_Asunto") = ltAsuntoC.Text
    Session("Envio_IdCoordinador") = hfIdCoordinador.Value
    Session("IdProfesor") = hfIdProfesor.Value
    Session("Envio_Nombre") = ltEnviaC.Text 'NombreCoordinador objetivo
    Session("Envio_Plantel") = ltPlantel.Text
    Session("Envio_Fecha") = ltFechaEnvioC.Text
    Session("Envio_Email") = hfEmail.Value
    Response.Redirect("ResponderMensajeP.aspx")
  End Sub

  Protected Sub LBreenviar_Click(sender As Object, e As EventArgs) Handles LBreenviar.Click
    Session("Reenvio") = "A"
    Session("Envio_Asunto") = ltAsunto.Text
    Session("Envio_Fecha") = ltFechaEnvio.Text
    Session("Envio_Contenido") = ltContenido.Text
    Session("Envio_AdjuntoNombre") = lbAdjunto.Text
    Session("Envio_AdjuntoFisico") = hfAdjuntoFisico.Value
    Session("Envio_Carpeta") = hfCarpetaAdjunto.Value
    Response.Redirect("EnviarMensajeP.aspx")
  End Sub

  Protected Sub LBreenviarC_Click(sender As Object, e As EventArgs) Handles LBreenviarC.Click
    Session("Reenvio") = "C"
    Session("Envio_Asunto") = ltAsuntoC.Text
    Session("Envio_Fecha") = ltFechaEnvioC.Text
    Session("Envio_Contenido") = ltContenidoC.Text
    Session("Envio_AdjuntoNombre") = lbAdjuntoC.Text
    Session("Envio_AdjuntoFisico") = hfAdjuntoFisico.Value
    Session("Envio_Carpeta") = hfCarpetaAdjunto.Value
    Response.Redirect("EnviarMensajeP.aspx")
  End Sub

  Protected Sub LBeliminarC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBeliminarC.Click
    Dim eliminado As Boolean = False

    Try
      If CBeliminarC.Checked Then
        'Para eliminarlo solo es necesario darlo de baja, y si lo está desplegando es que no está dado de baja
        SDSmensaje.UpdateCommand = "SET dateformat dmy; UPDATE ComunicacionCP set EstatusP = 'Baja' where IdComunicacionCP = " + Session("Envio_IdComunicacionCP") 'Viene de LeerMensajeC.aspx
        SDSmensaje.Update()

        'A diferencia de cuando el Profesor envía un adjunto a los alumnos que no borro el archivo, aquí si lo borro porque normalmente un alumno manda a un solo profesor
        'Elimino el archivo adjunto para no acumular basura
        'If Trim(lbAdjunto.Text).Length > 0 Then
        '    Dim ruta As String
        '    ruta = Config.Global.rutaMaterial
        '    ruta = ruta + "adjuntos\" + Session("LoginCoordinador") + "\"   'Viene de LeerMensajeP.aspx

        '    Dim MiArchivo As FileInfo = New FileInfo(ruta & lbAdjunto.Text)
        '    MiArchivo.Delete()
        'End If

        eliminado = True
      Else
        msgError.show("Debe marcar el mensaje para poder eliminarlo.")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try

    If eliminado Then
      If Session("Envio_Previous").Equals("LeerMensajeSalidaP") Then
        Response.Redirect("LeerMensajeSalidaP.aspx")
      Else
        Response.Redirect("LeerMensajeEntradaP.aspx")
      End If
    End If
  End Sub

#Region "adjuntos"

  Protected Sub lbAdjunto_Click(sender As Object, e As EventArgs) Handles lbAdjunto.Click
    Dim file As System.IO.FileInfo =
        New System.IO.FileInfo(Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value + "\" & hfAdjuntoFisico.Value)

    If file.Exists Then 'set appropriate headers
      Response.Clear()
      Response.AddHeader("Content-Disposition", "attachment; filename=""" & lbAdjunto.Text & """")
      Response.AddHeader("Content-Length", file.Length.ToString())
      Response.ContentType = "application/octet-stream"
      Response.WriteFile(file.FullName)
      'Response.End()
    Else
      msgError.show("Ha ocurrido un error con el archivo adjunto: éste ya no está disponible.")
    End If 'nothing in the URL as HTTP GET
  End Sub

  Protected Sub lbAdjuntoC_Click(sender As Object, e As EventArgs) Handles lbAdjuntoC.Click
    Dim file As System.IO.FileInfo =
        New System.IO.FileInfo(Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value + "\" & hfAdjuntoFisico.Value)

    If file.Exists Then 'set appropriate headers
      Response.Clear()
      Response.AddHeader("Content-Disposition", "attachment; filename=" & lbAdjuntoC.Text)
      Response.AddHeader("Content-Length", file.Length.ToString())
      Response.ContentType = "application/octet-stream"
      Response.WriteFile(file.FullName)
      'Response.End()
    Else
      msgError.show("Ha ocurrido un error con el archivo adjunto: éste ya no está disponible.")
    End If 'nothing in the URL as HTTP GET
  End Sub

#End Region

End Class
