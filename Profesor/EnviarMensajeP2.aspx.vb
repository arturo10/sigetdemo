﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO

Partial Class profesor_EnviarMensajeP
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            UserInterface.Include.HtmlEditor(CType(Master.FindControl("pnlHeader"), Literal), 665, 200, 12, Session("Usuario_Idioma").ToString())

            aplicaLenguaje()

            initPageData()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.GRUPOS
        Label3.Text = Config.Etiqueta.COORDINADORES
        Label4.Text = Config.Etiqueta.PLANTEL
        Label5.Text = Config.Etiqueta.ARTDET_ALUMNOS
        Label6.Text = Config.Etiqueta.LETRA_GRUPO
        Label7.Text = Config.Etiqueta.ARTDET_COORDINADORES
    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub initPageData()
        ' necesito obtener los datos del profesor para usarse al enviar el mensaje
        Try
            LblNombre.Text = "MENSAJE PARA " + Session("NomAlumno")
            HLregresar.NavigateUrl = "~/profesor/" + Session("Regresar")

            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()

                Dim cmd As SqlCommand = New SqlCommand("SELECT P.IdProfesor, P.Nombre, P.Apellidos FROM Usuario U, Profesor P where U.Login = @Login and P.IdUsuario = U.IdUsuario", conn)
                cmd.Parameters.AddWithValue("@Login", Trim(User.Identity.Name))

                Dim results As SqlDataReader = cmd.ExecuteReader()
                results.Read()
                Session("NombreProfesor") = results.Item("Nombre") + " " + results.Item("Apellidos")
                hfCarpetaAdjunto.Value = User.Identity.Name.Trim()

                results.Close()
                cmd.Dispose()
                conn.Close()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

#End Region

    Protected Sub BtnEnviar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEnviar.Click
        'Hago ciclo para leer CheckBoxList y por cada alumno: 
        'voy grabando el mensaje y enviándole un correo si este tiene email registrado
        'Envío un Email al usuario con sus datos de acceso
        If (Trim(TBasunto.Text).Length > 0) And (Trim(TBcontenido.Text).Length) > 0 Then
            'Recorro todo el CheckBoxList para leer los alumnos seleccionados
            Try
                'Las siguientes variables las necesitaré para buscar Email del alumno y mandarle correo
                Dim strConexion As String
                strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                Dim objConexion As New SqlConnection(strConexion)
                Dim miComando As SqlCommand
                Dim misRegistros As SqlDataReader
                miComando = objConexion.CreateCommand
                Dim Email As String
                Dim correo As New System.Net.Mail.MailMessage


                'Ahora checo si el alumno tiene Email registrado, si es así, también le mando un correo
                miComando.CommandText = "select * from Alumno where IdAlumno = " + Session("IdAlumno").ToString
                'Abrir la conexión y leo los registros del Planteamiento
                objConexion.Open()
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read() 'Leo para poder accesarlo

                If IsDBNull(misRegistros.Item("Email")) Then 'Si intento asignar un valor Nulo a una variable, marca error
                    Email = ""
                Else
                    Email = misRegistros.Item("Email")
                End If

                misRegistros.Close()
                objConexion.Close()

                'Si el alumno está seleccionado, inserto el mensaje para él [Sentido (E=Envia, E=Recibe - desde el punto de vista del profesor)][Estatus (Leido, Nuevo, Baja)]
                SDScomunicacion.InsertCommand = "SET dateformat dmy; INSERT INTO Comunicacion(IdProgramacion,Sentido,IdAlumno,Asunto,Contenido,Fecha,ArchivoAdjunto,ArchivoAdjuntoFisico,CarpetaAdjunto,EmailEnviado,Estatus, EstatusA)" &
                    " VALUES(@IdProgramacion, 'E', @IdAlumno, @Asunto, @Contenido, getdate(), @NombreAdjunto, @RutaAdjunto, @CarpetaAdjunto, @EmailEnviado, 'Nuevo', 'Nuevo')"
                SDScomunicacion.InsertParameters.Add("IdProgramacion", Session("IdProgramacion").ToString)
                SDScomunicacion.InsertParameters.Add("IdAlumno", Session("IdAlumno").ToString)
                SDScomunicacion.InsertParameters.Add("Asunto", TBasunto.Text)
                SDScomunicacion.InsertParameters.Add("Contenido", TBcontenido.Text)
                SDScomunicacion.InsertParameters.Add("NombreAdjunto", lbAdjunto.Text)
                SDScomunicacion.InsertParameters.Add("RutaAdjunto", hfAdjuntoFisico.Value)
                SDScomunicacion.InsertParameters.Add("CarpetaAdjunto", hfCarpetaAdjunto.Value)
                SDScomunicacion.InsertParameters.Add("EmailEnviado", Email)
                SDScomunicacion.Insert()

                If Trim(Email).Length > 1 Then
          If Config.Global.MAIL_ACTIVO Then
            Utils.Correo.EnviaCorreo(Email.Trim(),
                                     Config.Global.NOMBRE_FILESYSTEM & " - Mensaje en " & Config.Etiqueta.SISTEMA_CORTO,
                                     "Tiene un nuevo mensaje en " & Config.Global.NOMBRE_FILESYSTEM & " que le ha enviado " + Session("NombreProfesor"),
                                     False)
          End If
        End If

        msgSuccess.show("Éxito", "Se ha enviado el mensaje.")
        msgError.hide()
        TBasunto.Text = ""
        TBcontenido.Text = ""
        lbAdjunto.Text = ""
      Catch ex As Exception
        Utils.LogManager.ExceptionLog_InsertEntry(ex)
        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        msgSuccess.hide()
      End Try
    Else
      msgError.show("Indique el asunto y el contenido de su mensaje.")
      msgSuccess.hide()
    End If
  End Sub

#Region "adjuntos"

  Protected Sub BtnAdjuntar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdjuntar.Click
    Try  'Cuando se adjunta el archivo, se borra el nombre del FileUpload
      If FUadjunto.HasFile Then
        Dim ruta As String = Config.Global.rutaAdjuntos_fisica & Trim(User.Identity.Name) & "\" 'Esta ruta contempla un directorio como alumno nombrado como su login

        'Verifico si ya existe el directorio del alumno, si no, lo creo
        If Not (Directory.Exists(ruta)) Then
          Dim directorio As DirectoryInfo = Directory.CreateDirectory(ruta)
        End If

        'Subo archivo
        Dim now As String = Date.Now.Year.ToString & "_" & Date.Now.Month.ToString & "_" & Date.Now.Day.ToString & "_" & Date.Now.Hour.ToString & "_" & Date.Now.Minute.ToString & "_" & Date.Now.Second.ToString & "_" & Date.Now.Millisecond.ToString
        hfAdjuntoFisico.Value = now & FUadjunto.FileName
        FUadjunto.SaveAs(ruta & hfAdjuntoFisico.Value)
        lbAdjunto.Text = FUadjunto.FileName

        msgError.hide()
        msgSuccess.show("Se ha adjuntado el archivo.")
      Else
        msgError.show("No ha seleccionado un archivo para adjuntar.")
        msgSuccess.hide()
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      msgSuccess.hide()
    End Try
  End Sub

  Protected Sub lbAdjunto_Click(sender As Object, e As EventArgs) Handles lbAdjunto.Click
    Dim file As System.IO.FileInfo =
        New System.IO.FileInfo(Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value + "\" & hfAdjuntoFisico.Value) '-- if the file exists on the server

    If file.Exists Then 'set appropriate headers
      Response.Clear()
      Response.AddHeader("Content-Disposition", "attachment; filename=""" & lbAdjunto.Text & """")
      Response.AddHeader("Content-Length", file.Length.ToString())
      Response.ContentType = "application/octet-stream"
      Response.WriteFile(file.FullName)
      'Response.End()
    Else
      msgError.show("Ha ocurrido un error con el archivo adjunto: éste ya no está disponible.")
    End If 'nothing in the URL as HTTP GET
  End Sub

  Protected Sub BtnQuitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnQuitar.Click
    quitarAdjunto()
  End Sub

  Protected Sub quitarAdjunto()
    Try
      If hfAdjuntoFisico.Value.Length > 0 Then
        Dim ruta As String = Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value & "\" 'Esta ruta contempla un directorio como alumno nombrado como su login

        ' borro el archivo que estaba anteriormente porque se pondrá uno nuevo
        Dim adjunto As FileInfo = New FileInfo(ruta & hfAdjuntoFisico.Value)
        adjunto.Delete()

        lbAdjunto.Text = ""
        hfAdjuntoFisico.Value = ""
        msgError.hide()
        msgSuccess.show("Se ha eliminado el archivo.")
      Else
        msgError.show("No ha adjuntado ningún archivo todavía.")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgSuccess.hide()
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

#End Region

    Protected Sub BtnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLimpiar.Click
        TBasunto.Text = ""
        TBcontenido.Text = ""
        ' al limpiar se limpia el adjunto, y debe eliminarse el archivo
        quitarAdjunto()
        ' se ocultan los mensajes al final puesto que al quitar el archivo pueden mostrarse
        msgSuccess.hide()
        msgError.hide()
    End Sub

End Class
