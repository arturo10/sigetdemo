﻿
tinymce.init({
  language:   'es',
  selector: '.tinymce',
  theme: 'modern',
  width: 800,
  height: 200,
  resize: false,
  plugins: [
        'advlist autolink link image lists charmap preview hr',
        'searchreplace wordcount visualblocks visualchars insertdatetime nonbreaking',
        'contextmenu directionality paste textcolor'
  ],
  menubar: '',
  toolbar1: 'undo redo | fontselect | fontsizeselect',
  toolbar2: 'bold italic | forecolor backcolor | preview',
  setup: function (ed) {
      ed.on('init', function () {
          this.getDoc().body.style.fontSize = '12px';
      });
  }
});

function goBack() {
  history.back(1);
}