﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Profesor_comentaForo_Default : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    HfBackCounter.Value = (int.Parse(HfBackCounter.Value)-1).ToString();
    HlBack.NavigateUrl = "javascript:history.go(" + HfBackCounter.Value + ");";

    if (!Siget.Utils.Sesion.sesionAbierta())
    {
      Session["Nota"] = Siget.Lang.FileSystem.General.Msg_SuSesionExpiro[Session["Usuario_Idioma"].ToString()].ToString();
      Response.Redirect("~/");
    }

    // protección en contra de intentos ilegales
    if (Session["Comenta_IdEvaluacion"] == null)
    {
      Session["Nota"] = Siget.Lang.FileSystem.Alumno.Cursos.msg_evaluacion_no_identificada[Session["Usuario_Idioma"].ToString()].ToString();
      Response.Redirect("~/");
    }

    if (!IsPostBack)
    {
      presentaInstruccion();

      if (Session["Comenta_IdComentarioEvaluacion"] != null)
      {
        TxtComentario.Text = Session["Comenta_Comentario"].ToString();
        if (Session["Comenta_ArchivoAdjunto"].ToString() != "")
        {
          LbAdjunto.Text = Session["Comenta_ArchivoAdjunto"].ToString();
          HfAdjuntoFisico.Value = Session["Comenta_ArchivoAdjuntoFisico"].ToString();
          BtnQuitarAdjunto.Visible = true;
        }
        BtnGuardarCambios.Visible = true;
        BtnNuevoComentario.Visible = false;
      }
    }
  }

  protected void presentaInstruccion()
  {
    try
    {
      using (SqlConnection conn = new SqlConnection(ConfigurationManager
        .ConnectionStrings["sadcomeConnectionString"]
        .ConnectionString))
      {
        conn.Open();
        using (SqlCommand cmd = new SqlCommand("SELECT Instruccion FROM Evaluacion WHERE IdEvaluacion = @IdEvaluacion", conn))
        {
          cmd.Parameters.AddWithValue("@IdEvaluacion", Session["Comenta_IdEvaluacion"].ToString());

          using (SqlDataReader result = cmd.ExecuteReader())
          {
            while (result.Read())
            {
              pnlInstruccion.Visible = true;
              ltInstruccion.Text = result["Instruccion"].ToString();
            }
          }
        }
      }
    }
    catch (Exception ex)
    {
      Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
    }
  }

  protected void BtnNuevoComentario_Click(object sender, EventArgs e)
  {
    Boolean success = false;

    // todo quitar un archivo si session archivo es algo y lbarchivo no
    if (FuAdjunto.HasFile)
    {
      string ruta = Siget.Config.Global.rutaCargas + User.Identity.Name + "\\"; // Esta ruta contempla un directorio como alumno nombrado como su login

      //Verifico si ya existe el directorio del alumno, si no, lo creo
      if (!(Directory.Exists(ruta)))
      {
        DirectoryInfo directorio = Directory.CreateDirectory(ruta);
      }

      // obtengo un valor con el instante actual para diferenciar el archivo físico en disco
      string now =
        System.DateTime.Now.Year.ToString() + "_" +
        System.DateTime.Now.Month.ToString() + "_" +
        System.DateTime.Now.Day.ToString() + "_" +
        System.DateTime.Now.Hour.ToString() + "_" +
        System.DateTime.Now.Minute.ToString() + "_" +
        System.DateTime.Now.Second.ToString() + "_" +
        System.DateTime.Now.Millisecond.ToString();

      // guardo el archivo
      HfAdjuntoFisico.Value = now + FuAdjunto.FileName;
      FuAdjunto.SaveAs(ruta + HfAdjuntoFisico.Value);
      LbAdjunto.Text = FuAdjunto.FileName; // necesario para el parametro del insert a DB
    }

    try
    {
      using (SqlConnection conn = new SqlConnection(ConfigurationManager
        .ConnectionStrings["sadcomeConnectionString"]
        .ConnectionString))
      {
        conn.Open();
        using (SqlCommand cmd = new SqlCommand(
          "INSERT INTO ComentarioEvaluacion (" +
          "   IdEvaluacion" +
          "   ,IdProfesor" +
          "   ,IdGrupo" +
          "   ,Fecha" +
          "   ,Comentario" +
          "   ,ArchivoAdjunto" +
          "   ,ArchivoAdjuntoFisico) " +
          "VALUES (" +
          "   @IdEvaluacion" +
          "   ,@IdProfesor" +
          "   ,@IdGrupo" +
          "   ,getdate()" +
          "   ,@Comentario" +
          "   ,@ArchivoAdjunto" +
          "   ,@ArchivoAdjuntoFisico)", conn))
        {
          cmd.Parameters.AddWithValue("@IdEvaluacion", Session["Comenta_IdEvaluacion"].ToString());
          cmd.Parameters.AddWithValue("@IdProfesor", Session["Usuario_IdProfesor"].ToString());
          cmd.Parameters.AddWithValue("@IdGrupo", Session["Comenta_IdGrupo"].ToString());
          cmd.Parameters.AddWithValue("@Comentario", TxtComentario.Text);
          cmd.Parameters.AddWithValue("@ArchivoAdjunto", LbAdjunto.Text);
          cmd.Parameters.AddWithValue("@ArchivoAdjuntoFisico", HfAdjuntoFisico.Value);

          cmd.ExecuteNonQuery();
        }
      }

      success = true;
    }
    catch (Exception ex)
    {
      Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
    }

    if (success)
    {
      Session["Comenta_Nuevo"] = "";
      Session["Comenta_Guardado"] = ""; // para que muestre el mensaje de editado
      Response.Redirect("~/profesor/calificaForo/");
    }
  }

  protected void BtnCancelar_Click(object sender, EventArgs e)
  {
    if (Session["Comenta_IdComentarioEvaluacion"] != null)
    {
      Session.Remove("Comenta_IdComentarioEvaluacion");
    }
    Response.Redirect(Session["Comenta_Origen"].ToString());
  }

  protected void BtnGuardarCambios_Click(object sender, EventArgs e)
  {
    Boolean success = false;

    // quitar el archivo si antes tenía y lo quitó
    if (LbAdjunto.Text == "" && Session["Comenta_ArchivoAdjunto"].ToString() != "")
    {
      try
      {
        FileInfo file = new FileInfo(Siget.Config.Global.rutaCargas + User.Identity.Name.Trim() + "\\" + Session["Comenta_ArchivoAdjuntoFisico"].ToString());
        file.Delete();
      }
      catch (Exception ex)
      {
        Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
      }
    }

    if (FuAdjunto.HasFile)
    {
      string ruta = Siget.Config.Global.rutaCargas + User.Identity.Name + "\\"; // Esta ruta contempla un directorio como alumno nombrado como su login

      if (LbAdjunto.Text != "")
      {
        // si hay un archivo antiguo, borralo antes de cargar el nuevo
        try
        {
          FileInfo file = new FileInfo(Siget.Config.Global.rutaCargas + User.Identity.Name.Trim() + "\\" + LbAdjunto.Text);
          file.Delete();
        }
        catch (Exception ex)
        {
          Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
        }
      }

      //Verifico si ya existe el directorio del alumno, si no, lo creo
      if (!(Directory.Exists(ruta)))
      {
        DirectoryInfo directorio = Directory.CreateDirectory(ruta);
      }

      // obtengo un valor con el instante actual para diferenciar el archivo físico en disco
      string now =
        System.DateTime.Now.Year.ToString() + "_" +
        System.DateTime.Now.Month.ToString() + "_" +
        System.DateTime.Now.Day.ToString() + "_" +
        System.DateTime.Now.Hour.ToString() + "_" +
        System.DateTime.Now.Minute.ToString() + "_" +
        System.DateTime.Now.Second.ToString() + "_" +
        System.DateTime.Now.Millisecond.ToString();

      // guardo el archivo
      HfAdjuntoFisico.Value = now + FuAdjunto.FileName;
      FuAdjunto.SaveAs(ruta + HfAdjuntoFisico.Value);
      LbAdjunto.Text = FuAdjunto.FileName; // necesario para el parametro del insert a DB
    }

    try
    {
      using (SqlConnection conn = new SqlConnection(ConfigurationManager
        .ConnectionStrings["sadcomeConnectionString"]
        .ConnectionString))
      {
        conn.Open();
        using (SqlCommand cmd = new SqlCommand("UPDATE ComentarioEvaluacion SET Comentario =  @Comentario, ArchivoAdjunto = @ArchivoAdjunto, ArchivoAdjuntoFisico = @ArchivoAdjuntoFisico WHERE IdComentarioEvaluacion = @IdComentarioEvaluacion", conn))
        {
          cmd.Parameters.AddWithValue("@IdComentarioEvaluacion", Session["Comenta_IdComentarioEvaluacion"].ToString());
          cmd.Parameters.AddWithValue("@Comentario", TxtComentario.Text);
          if (LbAdjunto.Text == "")
          {
            cmd.Parameters.AddWithValue("@ArchivoAdjunto", DBNull.Value);
            cmd.Parameters.AddWithValue("@ArchivoAdjuntoFisico", DBNull.Value);
          }
          else
          {
            cmd.Parameters.AddWithValue("@ArchivoAdjunto", LbAdjunto.Text);
            cmd.Parameters.AddWithValue("@ArchivoAdjuntoFisico", HfAdjuntoFisico.Value);
          }

          cmd.ExecuteNonQuery();
        }
      }

      success = true;
    }
    catch (Exception ex)
    {
      Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
    }

    if (success)
    {
      Session["Comenta_Nuevo"] = ""; // para que recargue el estado de la página
      Session["Comenta_Editado"] = ""; // para que muestre el mensaje de editado
      Session.Remove("Comenta_IdComentarioEvaluacion");
      Response.Redirect("~/profesor/calificaForo/");
    }

  }

  protected void LbAdjunto_Click(object sender, EventArgs e)
  {
    System.IO.FileInfo file = new System.IO.FileInfo(Siget.Config.Global.rutaCargas + User.Identity.Name.Trim() + "\\" + HfAdjuntoFisico.Value.Trim());

    if (file.Exists) // set appropriate headers
    {
      Response.Clear();
      Response.AddHeader("Content-Disposition", "attachment; filename=\"" + LbAdjunto.Text + "\"");
      Response.AddHeader("Content-Length", file.Length.ToString());
      Response.ContentType = "application/octet-stream";
      Response.WriteFile(file.FullName);
      // Response.End()
    }
    else
      LbAdjunto.Text = Siget.Lang.FileSystem.Alumno.Mensaje.ERR_ADJUNTO_NO_DISPONIBLE[Session["Usuario_Idioma"].ToString()];
    // nothing in the URL as HTTP GET
  }

  protected void BtnQuitarAdjunto_Click(object sender, EventArgs e)
  {
    LbAdjunto.Text = "";
    HfAdjuntoFisico.Value = "";
    BtnQuitarAdjunto.Visible = false;
  }
}