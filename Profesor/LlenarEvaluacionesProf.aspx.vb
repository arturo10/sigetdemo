﻿Imports Siget
Imports Siget.DataAccess

Imports System.Data.SqlClient

Partial Class superadministrador_LlenarEvaluaciones
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = Trim(User.Identity.Name)

        GValumnos.Caption = "<h3>" &
            Config.Etiqueta.ALUMNOS &
            " con Actividad Pendiente de Realizar. Seleccione " & Config.Etiqueta.ARTIND_ALUMNO & "</h3>"

        GValumnosterminados.Caption = "<h3>" &
            Config.Etiqueta.ALUMNOS &
            " con Actividad Realizada</h3>"
        TitleLiteral.Text = "Capturar Actividad"

        Label1.Text = Config.Etiqueta.ALUMNO
        Label2.Text = Config.Etiqueta.CICLO
        Label3.Text = Config.Etiqueta.NIVEL
        Label4.Text = Config.Etiqueta.GRADO
        Label5.Text = Config.Etiqueta.PLANTEL
        Label6.Text = Config.Etiqueta.GRUPO
        Label7.Text = Config.Etiqueta.ASIGNATURA
        Label8.Text = Config.Etiqueta.ALUMNO
        Label9.Text = Config.Etiqueta.ARTIND_ALUMNO

        If (Request.QueryString.Count > 0) Then
            Dim Veces = CInt(Request.QueryString("Otro").ToString) 'Esta variable la uso para que solo se acomoden automáticamente
            Session("Entra") = Session("Entra") + Veces            'los combobox cuando regreso de la página FormatoCaptura y no
        End If                                                     'cada que se regresque la página porque marcan error los SDS
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        'DDLcicloescolar.Items.Insert(0, New ListItem("---Elija un Ciclo Escolar", 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLcalificacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacion.DataBound
        DDLcalificacion.Items.Insert(0, New ListItem("---Elija una Calificación", 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRUPO & " " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub GValumnos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GValumnos.SelectedIndexChanged
        'Las siguientes variables las necesitaré cuando regrese a esta página a capturar otro Alumno para mover los combobox
        Session("IdAsignatura") = DDLasignatura.SelectedValue
        Session("IdCicloEscolar") = DDLcicloescolar.SelectedValue
        Session("IdNivel") = DDLnivel.SelectedValue
        Session("IdGrado") = DDLgrado.SelectedValue
        Session("IdPlantel") = DDLplantel.SelectedValue
        Session("IdCalificacion") = DDLcalificacion.SelectedValue
        Session("IndiceEvaluacion") = GVevaluaciones.SelectedIndex

        Dim strConexion As String
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misReactivos As SqlDataReader
        miComando = objConexion.CreateCommand
        Try
            'NO SIRVE: miComando.CommandText = "select * from Evaluacion where IdEvaluacion = " + Session("IdEvaluacion")
            miComando.CommandText = "select distinct IdEvaluacion, IdCalificacion, ClaveBateria, ClaveAbreviada, Porcentaje, InicioContestar, FinSinPenalizacion, FinContestar, Tipo, Penalizacion, Estatus, Aleatoria, SeEntregaDocto, Califica " + _
                        "from Evaluacion where IdEvaluacion not in (select IdEvaluacion from EvaluacionPlantel where IdEvaluacion = " + GVevaluaciones.SelectedDataKey.Values(0).ToString + " and IdPlantel = " + GValumnos.SelectedDataKey.Values(1).ToString + ") and IdEvaluacion = " + GVevaluaciones.SelectedDataKey.Values(0).ToString + _
                        " UNION " + _
                        "select distinct E.IdEvaluacion, E.IdCalificacion, E.ClaveBateria, E.ClaveAbreviada, E.Porcentaje, P.InicioContestar, P.FinSinPenalizacion, P.FinContestar, E.Tipo, E.Penalizacion, E.Estatus, E.Aleatoria, E.SeEntregaDocto, E.Califica " + _
                        "from Evaluacion E, EvaluacionPlantel P where E.IdEvaluacion = " + GVevaluaciones.SelectedDataKey.Values(0).ToString + " and P.IdEvaluacion = E.IdEvaluacion and P.IdPlantel = " + GValumnos.SelectedDataKey.Values(1).ToString
            objConexion.Open() 'Abro la conexion
            misReactivos = miComando.ExecuteReader()
            misReactivos.Read()

            'PRIMERO VERIFICO SI ESTÁ SERIADA LA ACTIVIDAD
            If Not CType(New SeriacionEvaluacionesDa, SeriacionEvaluacionesDa).VerificaActividadesPrevias(misReactivos.Item("IdEvaluacion").ToString, GValumnos.SelectedDataKey.Values(0).ToString) Then
                ' si la verificación falla, el mensaje de error queda en Session("Nota")
                msgError.show(Session("Nota"))
                Session("Nota") = ""
                objConexion.Close()
                misReactivos.Close()
                'NO PUEDE CONTINUAR Y LO MANDO DE REGRESO PORQUE NO HA TERMINADO LA ACTIVIDAD SERIADA
            Else
                Session("Nota") = ""
                objConexion.Close()
                misReactivos.Close()
                'CODIGO PARA IR A CONTESTAR LA ACTIVIDAD:
                Session("IdEvaluacion") = GVevaluaciones.SelectedDataKey.Values(0).ToString
                Session("IdAlumno") = GValumnos.SelectedDataKey.Values(0).ToString
                Session("Titulo") = "Captura de respuestas para <font color=BLUE>" + GValumnos.SelectedRow.Cells(8).Text + _
                                    " " + GValumnos.SelectedRow.Cells(6).Text + " " + GValumnos.SelectedRow.Cells(7).Text + _
                                    " </font> en la Actividad <font color=BLUE>" + GVevaluaciones.SelectedRow.Cells(4).Text + "</font>"
                Session("Entra") = 0 'Esta variable la uso para que funcionen los cambios de valores en los ComboBox cuando regrese a capturar otro
                Response.Redirect("FormatoCapturaProf.aspx")
            End If

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub


    'Los siguientes componentes deben estar seguidos en la misma columna si no, no agarra el cambio automatico

    Protected Sub DDLgrupo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.SelectedIndexChanged
        Session("IdGrupo") = DDLgrupo.SelectedValue 'Esta variable la necesitare para cuando regrese a capturar otro
    End Sub

    Protected Sub SDSasignaturas_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSasignaturas.Selected
        If Request.QueryString.Count > 0 And Session("Entra") = 1 Then
            DDLasignatura.SelectedValue = Session("IdAsignatura")
        End If
    End Sub

    Protected Sub SDScalificaciones_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDScalificaciones.Selected
        Try
            If (Request.QueryString.Count > 0) And Session("Entra") = 1 Then
                DDLcalificacion.SelectedValue = Session("IdCalificacion")
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub SDSevaluaciones_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSevaluaciones.Selected
        Try
            If (Request.QueryString.Count > 0) And Session("Entra") = 1 Then
                GVevaluaciones.SelectedIndex = Session("IndiceEvaluacion")
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVevaluaciones_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevaluaciones.DataBound
        If (GVevaluaciones.Rows.Count = 0) And (DDLcalificacion.SelectedIndex > 0) Then
            msgInfo.show("No tiene actividades asignadas para capturar.")
        Else
            msgInfo.hide()
        End If
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRADO & " " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub SDSgrados_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSgrados.Selected
        If Request.QueryString.Count > 0 And Session("Entra") = 1 Then
            DDLgrado.SelectedValue = Session("IdGrado")
        End If
    End Sub

    Protected Sub SDSplanteles_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSplanteles.Selected
        If Request.QueryString.Count > 0 And Session("Entra") = 1 Then
            DDLplantel.SelectedValue = Session("IdPlantel")
        End If
    End Sub

    Protected Sub SDSgrupos_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSgrupos.Selected
        If Request.QueryString.Count > 0 And Session("Entra") = 1 Then
            DDLgrupo.SelectedValue = Session("IdGrupo")
        End If
    End Sub

    Protected Sub SDSciclosescolares_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSciclosescolares.Selected
        If Request.QueryString.Count > 0 And Session("Entra") = 1 Then
            DDLcicloescolar.SelectedValue = Session("IdCicloEscolar")
        End If
    End Sub

    Protected Sub SDSniveles_Selected(sender As Object, e As SqlDataSourceStatusEventArgs) Handles SDSniveles.Selected
        If Request.QueryString.Count > 0 And Session("Entra") = 1 Then
            DDLnivel.SelectedValue = Session("IdNivel")
        End If
    End Sub

    Protected Sub GValumnosterminados_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GValumnosterminados.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.EQUIPO
        End If
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVevaluaciones_PreRender(sender As Object, e As EventArgs) Handles GVevaluaciones.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVevaluaciones.Rows.Count > 0 Then
            GVevaluaciones.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVevaluaciones_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVevaluaciones.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GVevaluaciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowDataBound
    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVevaluaciones, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVevaluaciones.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVevaluaciones, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    For Each r In GValumnos.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GValumnos, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GValumnos_PreRender(sender As Object, e As EventArgs) Handles GValumnos.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GValumnos.Rows.Count > 0 Then
      GValumnos.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GValumnos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GValumnos.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GValumnos.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GValumnos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GValumnos.RowDataBound
    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GValumnos, "Select$" & e.Row.RowIndex).ToString())
    End If

    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(5).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.EQUIPO
    End If
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GValumnosterminados_PreRender(sender As Object, e As EventArgs) Handles GValumnosterminados.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GValumnosterminados.Rows.Count > 0 Then
      GValumnosterminados.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GValumnosterminados_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GValumnosterminados.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GValumnosterminados.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

End Class
