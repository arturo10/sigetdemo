﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="CalificaRespAbiertaNueva.aspx.vb"
    Inherits="profesor_CalificaRespAbiertaNueva"
    MaintainScrollPositionOnPostback="true"
    ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content6" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content7" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-family: Arial;
            font-size: small;
            font-weight: bold;
            height: 38px;
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style33 {
            height: 34px;
            text-align: left;
        }

        .style32 {
            height: 22px;
            width: 479px;
            font-weight: normal;
            font-size: small;
        }

        .style34 {
            height: 19px;
        }

        .style35 {
            font-family: Arial;
            font-size: small;
            font-weight: bold;
            height: 21px;
        }

        .style37 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000066;
        }

        .auto-style1 {
            text-align: left;
        }

        .pnlResp {
            min-width: 880px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content8" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Evaluaciones - Calificar Nuevas Respuestas Abiertas
    </h1>
    Permite calificar respuestas abiertas de 
    <asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    que aún no han sido calificadas, para 
    <asp:Label ID="Label4" runat="server" Text="[UNA]" />
    <asp:Label ID="Label6" runat="server" Text="[ASIGNATURA]" />
    específica.
</asp:Content>

<asp:Content ID="Content10" ContentPlaceHolderID="Contenedor" runat="Server">
    <table>
        <tr>
            <td class="style24" colspan="3">
                <asp:Label ID="Label2" runat="server" Text="[CICLO]" />
            </td>
            <td colspan="5" style="text-align: left">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px"
                    Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="3"></td>
            <td colspan="2"></td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="3" class="style24" style="vertical-align: top;">
                <asp:Label ID="Label5" runat="server" Text="[GRADO]" /></td>
            <td colspan="4" style="text-align: left; vertical-align: top;" class="auto-style1">
                <asp:DropDownList ID="DDLGrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion"
                    DataValueField="IdGrado" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="style24" style="vertical-align: top;">
                <asp:Label ID="Label3" runat="server" Text="[ASIGNATURA]" /></td>
            <td colspan="5" style="text-align: left; vertical-align: top;" class="auto-style1">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSasignatura" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="style24">&nbsp;</td>
            <td colspan="2"></td>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="8" class="style34">
                <asp:GridView ID="GValumnos0" runat="server" 
                    AllowSorting="True"
                    AutoGenerateColumns="False" 
                    Caption="ARCHIVOS DE APOYO PARA EL PROFESOR"
                    
                    DataSourceID="SDSapoyos" 
                    Width="393px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                            SortExpression="Consecutivo">
                            <HeaderStyle Width="60px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripción"
                            SortExpression="Descripcion">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ArchivoApoyo" HeaderText="ArchivoApoyo"
                            SortExpression="ArchivoApoyo" Visible="False" />
                        <asp:HyperLinkField DataNavigateUrlFields="ArchivoApoyo"
                            DataNavigateUrlFormatString="~/Resources/apoyo/{0}" DataTextField="ArchivoApoyo"
                            HeaderText="Archivo" Target="_blank" ItemStyle-CssClass="columnaHyperlink">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:HyperLinkField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="8">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td colspan="2"></td>
            <td colspan="2">Mostrar:&nbsp;
                <asp:DropDownList ID="ddlEstado" runat="server" AutoPostBack="true"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: medium">
                    <asp:ListItem Value="Enviado" Selected="true">Envíos</asp:ListItem>
                    <asp:ListItem Value="Borrador">Borradores</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td colspan="2"></td>
            <td colspan="2">Respuestas por página:&nbsp;
                <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: medium">
                    <asp:ListItem Value="5" Selected="true">5</asp:ListItem>
                    <asp:ListItem Value="10">10</asp:ListItem>
                    <asp:ListItem Value="20">20</asp:ListItem>
                    <asp:ListItem Value="50">50</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td colspan="2"></td>
        </tr>
        <tr>
            <td colspan="8">
                <asp:GridView ID="GVrespuestas" runat="server"
                    AllowSorting="True"
                    AllowPaging="true"
                    PageSize="5"
                    AutoGenerateColumns="False"
                    Caption="<h3>Respuestas Abiertas</h3>"

                    DataKeyNames="IdRespuesta, SeCalifica, IdAlumno, IdRespuestaA, IdProfesor, IdGrupo, IdGrado, IdPlantel, IdProgramacion, IdCalificacion, IdEvaluacion"
                    DataSourceID="SDSrespuestas"
                    Width="1000"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell"/>

                        <asp:BoundField DataField="Ponderacion" HeaderText="Ponderacion"
                            SortExpression="Ponderacion"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />

                        <asp:BoundField DataField="ClaveBateria" HeaderText="Evaluación"
                            SortExpression="ClaveBateria" />
                        <asp:BoundField DataField="Descripcion" HeaderText="Calificación"
                            SortExpression="Descripcion" />

                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                            SortExpression="Consecutivo" />
                        <asp:BoundField DataField="Reactivo" HeaderText="Reactivo"
                            SortExpression="Reactivo" />
                        <asp:BoundField DataField="Respuesta" HeaderText="Respuesta"
                            SortExpression="Respuesta" />
                        <asp:BoundField DataField="Calificacion" HeaderText="Calificación"
                            SortExpression="Calificacion"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />

                        <asp:BoundField DataField="Grupo" HeaderText="Grupo"
                            SortExpression="Grupo" />
                        <asp:BoundField DataField="Grado" HeaderText="Grado"
                            SortExpression="Grado"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="Nivel" HeaderText="Nivel"
                            SortExpression="Nivel" />
                        <asp:BoundField DataField="Plantel" HeaderText="Plantel"
                            SortExpression="Plantel" />

                        <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                            SortExpression="Matricula"></asp:BoundField>
                        <asp:BoundField DataField="NomAlumno" HeaderText="Nombre del Alumno" ReadOnly="True"
                            SortExpression="NomAlumno"></asp:BoundField>
                        
                        <asp:BoundField DataField="Respuesta" HeaderText="MarkupRespuesta"
                            SortExpression="Respuesta"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        
                        <asp:BoundField DataField="Reactivo" HeaderText="MarkupReactivo"
                            SortExpression="Reactivo"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
                    0  select
                    1  consecutivo
                    2  reactivo
                    3  idrespuesta
                    4  respuesta
                    5  matricula
                    6  NOMBRE DEL ALUMNO
                --%>
            </td>
        </tr>
        <tr>
            <td colspan="8" class="style34">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td colspan="8" style="text-align: left">
                <asp:HiddenField ID="hfReturn" runat="server" Value="0" />
                <asp:HyperLink ID="LblRegresar" runat="server"
                    NavigateUrl="~/profesor/CalificaRespuestaAbierta.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
        </tr>
    </table>

    <asp:Panel ID="PnlResp" runat="server" CssClass="dialogOverwrite dialogRespuestaAbierta">
        <div class="style37" style="margin-bottom: 20px;">
            Contenido de la Respuesta
        </div>
        <div style="text-align: left; border: 2px solid #444; padding: 10px; width: 766px; max-height: 300px; overflow-y: scroll;">
            <asp:Literal ID="TBrespuesta" runat="server"></asp:Literal>
        </div>
        <div style="margin-top: 20px;">
            <asp:DropDownList ID="DDLcalifica" runat="server"
                Style="font-family: Arial, Helvetica, sans-serif; font-size: medium">
            </asp:DropDownList>
            &nbsp;
            <asp:Button ID="BtnCalificar" runat="server" Text="Calificar"
                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            &nbsp;
            <asp:Button ID="btnDialogClose" runat="server" Text="Cerrar"
                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            <div style="width:30%;text-align:center;">
                <label><b>Número de palabras de la respuesta:</b></label>
                <asp:Label runat="server" ID="NoPalabrasRespuesta" ></asp:Label>
            </div>
        </div>
       
    </asp:Panel>

    <%-- DATA SOURCES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - --%>

    <table class="dataSources" style="width: 100%;">
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select distinct P.IdProfesor, G.IdGrupo, G.Descripcion as Grupo, Gr.IdGrado, Gr.Descripcion Grado, N.Descripcion as Nivel, Pl.IdPlantel, Pl.Descripcion as Plantel, Pr.IdProgramacion
from Usuario U, Profesor P, Grupo G, Grado Gr, Nivel N, Plantel Pl, Programacion Pr
where U.Login = @Login and P.IdUsuario = U.IdUsuario and Pr.IdProfesor = P.IdProfesor
and Pr.IdCicloEscolar = @IdCicloEscolar and G.IdGrupo = Pr.IdGrupo and Pr.IdAsignatura = @IdAsignatura and 
 Pl.IdPlantel = G.IdPlantel and Gr.IdGrado = G.IdGrado and N.IdNivel = Gr.IdNivel and G.IdCicloEscolar = @IdCicloEscolar
order by Pl.Descripcion, N.Descripcion, Gr.Descripcion, G.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>
            </td>
            <td>
                <%-- selecciona los grados de los grupos de las programaciones del profesor en ese ciclo escolar --%>
                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select distinct Gr.IdGrado, Gr.Descripcion
from Usuario U, Profesor P, Grupo G, Grado Gr, Nivel N, Plantel Pl, Programacion Pr
where U.Login = @Login and P.IdUsuario = U.IdUsuario and Pr.IdProfesor = P.IdProfesor
and Pr.IdCicloEscolar = @IdCicloEscolar and G.IdGrupo = Pr.IdGrupo and 
 Pl.IdPlantel = G.IdPlantel and Gr.IdGrado = G.IdGrado and N.IdNivel = Gr.IdNivel and G.IdCicloEscolar = @IdCicloEscolar
order by Gr.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSrespuestas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="
select distinct 
	/* Sección de Respuestas */
	P.Consecutivo, 
	P.Redaccion as Reactivo, 
	A.Calificacion, 
	R.IdRespuesta, 
	A.Redaccion as Respuesta,
	Al.Matricula, 
	Al.Nombre + ' ' + Al.ApePaterno + ' ' + Al.ApeMaterno as NomAlumno, 
	O.SeCalifica, 
	Al.IdAlumno, 
	P.Ponderacion, 
	A.IdRespuestaA,

	/* Sección de Grupo y Grado */
	Pro.IdProfesor, 
	G.IdGrupo, 
	G.Descripcion as Grupo, 
	Gr.IdGrado, 
	Gr.Descripcion Grado, 
	N.Descripcion as Nivel, 
	Pl.IdPlantel, 
	Pl.Descripcion as Plantel, 
	Pr.IdProgramacion,
	
	/* Sección de Calificación */
	Cal.IdCalificacion, 
	Cal.Descripcion,
	
	/* Sección de Actividad */
	Ev.IdEvaluacion, 
	Ev.ClaveBateria

from 
	/* Sección de Respuestas */
	Planteamiento P, 
	Respuesta R, 
	RespuestaAbierta A, 
	Alumno Al, 
	Opcion O,
	
	/* Sección de Grupo y Grado */
	Usuario U, 
	Profesor Pro, 
	Grupo G, 
	Grado Gr, 
	Nivel N, 
	Plantel Pl, 
	Programacion Pr,
	
	/* Sección de Calificación */
	Calificacion Cal,
	
	/* Sección de Actividad */
	Evaluacion Ev
where 
	/* Sección de Respuestas */
	R.IdDetalleEvaluacion in 
		(select 
			IdDetalleEvaluacion 
		from 
			DetalleEvaluacion 
		where 
			IdEvaluacion = Ev.IdEvaluacion
		) 
	and R.IdGrupo = G.IdGrupo
	and A.IdRespuesta = R.IdRespuesta 
	and P.IdPlanteamiento = R.IdPlanteamiento 
	and P.TipoRespuesta = 'Abierta' 
	and A.Calificacion IS NULL 
	and R.IdCicloEscolar = @IdCicloEscolar 
	and Al.IdAlumno = R.IdAlumno 
	and O.IdOpcion = R.IdOpcion 
	and P.IdPlanteamiento = O.IdPlanteamiento
	and Al.IdAlumno in 
		(SELECT 
			IdAlumno 
		FROM 
			EvaluacionTerminada 
		WHERE 
			IdEvaluacion = Ev.IdEvaluacion
		)
		
	/* Sección de Grupo y Grado */
	and U.Login = @Login 
	and Pro.IdUsuario = U.IdUsuario 
	and Pr.IdProfesor = Pro.IdProfesor
	and Pr.IdCicloEscolar = @IdCicloEscolar 
	and G.IdGrupo = Pr.IdGrupo 
	and Pr.IdAsignatura = @IdAsignatura 
	and Pl.IdPlantel = G.IdPlantel 
	and Gr.IdGrado = G.IdGrado 
	and N.IdNivel = Gr.IdNivel 
	and G.IdCicloEscolar = @IdCicloEscolar
	
	/* Sección de Calificación */
	and Cal.IdCicloEscolar = @IdCicloEscolar 
	AND Cal.IdAsignatura = @IdAsignatura
	
	/* Sección de Actividad */
	and Ev.IdCalificacion = Cal.IdCalificacion

    and A.Estado = @Estado
order by 
    Ev.IdEvaluacion ASC,
	P.Consecutivo ASC, 
	NomAlumno">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="ddlEstado" Name="Estado"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSapoyos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Distinct Consecutivo,Descripcion,ArchivoApoyo from
ApoyoEvaluacion
where IdProgramacion = @IdProgramacion and
IdEvaluacion = @IdEvaluacion
order by Consecutivo">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdProgramacion" SessionField="IdProgramacion" />
                        <asp:ControlParameter ControlID="GVrespuestas" Name="IdEvaluacion" PropertyName="SelectedDataKey.Values[IdEvaluacion]" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSactividades" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdEvaluacion], [ClaveBateria] FROM [Evaluacion] WHERE ([IdCalificacion] = @IdCalificacion) ORDER BY [InicioContestar], [ClaveBateria]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVrespuestas" Name="IdCalificacion"
                            PropertyName="SelectedValue" Type="Int64" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdCalificacion], [Descripcion] FROM [Calificacion] WHERE (([IdCicloEscolar] = @IdCicloEscolar) AND ([IdAsignatura] = @IdAsignatura)) ORDER BY [Consecutivo]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSasignatura" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdAsignatura, Descripcion 
FROM Asignatura WHERE IdGrado = @IdGrado
AND IdAsignatura in (select IdAsignatura from Programacion where IdProfesor in (select P.IdProfesor from Profesor P, Usuario U where U.Login = @Login and P.IdUsuario = U.IdUsuario))
ORDER BY IdArea">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLGrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDScalculacalificacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Calificacion]"></asp:SqlDataSource>
            </td>
            <td></td>
            <td>
                <asp:HiddenField ID="HF1" runat="server" />
                <asp:ModalPopupExtender ID="HF1_ModalPopupExtender" runat="server"
                    Enabled="True" PopupControlID="PnlResp" TargetControlID="HF1" BackgroundCssClass="overlay">
                </asp:ModalPopupExtender>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>
