﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="CalificaActividad.aspx.vb"
    Inherits="profesor_CalificaActividad"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>




<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style13 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
            height: 42px;
        }

        .style14 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            text-align: left;
        }

        .style15 {
            font-family: Arial, Helvetica, sans-serif;
        }

        .style16 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style25 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            height: 8px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }

        .style33 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            height: 42px;
        }

        .style34 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            text-align: left;
            height: 36px;
        }

        .style35 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            height: 36px;
        }

        .style37 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            height: 15px;
            width: 268435584px;
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Asigna Calificación a Actividades
    </h1>
    Permite asignar calificaciones a actividades de un
    <asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />.
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td style="text-align: left" colspan="6">Nota: Predeterminadamente el sistema califica automáticamente las actividades: 
                para que usted asigne calificaciones, la actividad debe estar configurada
                en especial para ello. Para más información consulte a su administrador.
            </td>
        </tr>
        <tr>
            <td class="style13" colspan="4">
                <asp:Label ID="Label5" runat="server" Text="[CICLO]" />
            </td>
            <td class="style37" colspan="2">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px"
                    Width="330px">
                </asp:DropDownList>
            </td>
            <td class="style33"></td>
        </tr>
        <tr>
            <td class="style15" colspan="6" style="text-align: left;">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td class="style35"></td>
        </tr>
        <tr>
            <td class="style14" colspan="6">Elija
                <asp:Label ID="Label11" runat="server" Text="[UN]" />
                <asp:Label ID="Label6" runat="server" Text="[GRUPO]" />
                de
                <asp:Label ID="Label12" runat="server" Text="[LOS]" />
                asignad<asp:Label ID="Label13" runat="server" Text="[O]" />s a su cuenta:</td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style15" colspan="7">
                <asp:GridView ID="GVdatos" runat="server"
                    AllowPaging="True"
                    AllowSorting="True"
                    AutoGenerateColumns="False"

                    Width="880px"
                    DataSourceID="SDSgrupos"
                    DataKeyNames="IdGrupo,IdGrado"
                    
                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo"
                            SortExpression="IdGrupo"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="IdProfesor" HeaderText="IdProfesor"
                            SortExpression="IdProfesor"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="Grupo" HeaderText="Grupo" SortExpression="Grupo" />
                        <asp:BoundField DataField="IdGrado" HeaderText="IdGrado"
                            SortExpression="IdGrado"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="Grado" HeaderText="Grado" SortExpression="Grado" />
                        <asp:BoundField DataField="Nivel" HeaderText="Nivel"
                            SortExpression="Nivel" />
                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                            SortExpression="IdPlantel"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="Plantel" HeaderText="Plantel"
                            SortExpression="Plantel" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
                    0  select
                    1  idgrupo
                    2  idprofesor
                    3  GRUPO
                    4  idgrado
                    5  GRADO
                    6  NIVEL
                    7  idplantel
                    8  PLANTEL
                --%>
            </td>
        </tr>
        <tr>
            <td class="style11">
                &nbsp;</td>
            <td class="style11">
                &nbsp;</td>
            <td class="style11" colspan="3">
                &nbsp;</td>
            <td class="style11" colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style25">&nbsp;</td>
            <td class="style25" colspan="2">
                <asp:Label ID="Label7" runat="server" Text="[ASIGNATURA]" />
                que desea calificar:</td>
            <td class="style11" colspan="2">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSmaterias" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Width="330px">
                </asp:DropDownList>
            </td>
            <td class="style11" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style15" colspan="7">
                <asp:GridView ID="GVactividades" runat="server"
                    AutoGenerateColumns="false"
                    AllowSorting="True"
                    Caption="<h2>Actividades para calificar de la [ASIGNATURA] Seleccionada<h2>"

                    Width="880px"
                    DataSourceID="SDSdetalleactividades"
                    DataKeyNames="IdEvaluacion"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="IdEvaluacion" HeaderText="IdEvaluacion"
                            SortExpression="IdEvaluacion"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="IdCalificacion" HeaderText="IdCalificacion"
                            SortExpression="IdCalificacion"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="ClaveBateria" HeaderText="Nombre"
                            SortExpression="ClaveBateria" />
                        <asp:BoundField DataField="Porcentaje" HeaderText="Porcentaje"
                            SortExpression="Porcentaje" />
                        <asp:BoundField DataField="InicioContestar" HeaderText="Inicio"
                            SortExpression="Inicio" />
                        <asp:BoundField DataField="FinSinPenalizacion" HeaderText="Fin Sin Penalizacion"
                            SortExpression="IdGrado" />
                        <asp:BoundField DataField="FinContestar" HeaderText="Fin"
                            SortExpression="IdGrado" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo"
                            SortExpression="Tipo" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="7">&nbsp;
            </td>
        </tr>
        <tr>
            <td class="style15" colspan="7">
                <asp:GridView ID="GValumnos" runat="server"
                    AllowSorting="True"
                    Caption="<h2>Calificaciones de la Actividad Seleccionada<h2>"
                    AutoGenerateColumns="false"

                    Width="880px"
                    DataSourceID="SDSalumnos"
                    DataKeyNames="IdAlumno"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                            SortExpression="IdAlumno"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="NomAlumno" HeaderText="Nombre"
                            SortExpression="NomAlumno" />
                        <asp:BoundField DataField="Resultado" HeaderText="Calificación"
                            SortExpression="Resultado" />
                        <asp:BoundField DataField="Matricula" HeaderText="Matricula"
                            SortExpression="Matricula" />
                        <asp:BoundField DataField="Equipo" HeaderText="Equipo"
                            SortExpression="Equipo" />
                        <asp:BoundField DataField="Subequipo" HeaderText="Subequipo"
                            SortExpression="Subequipo" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="7" style="text-align: center;">
                <asp:Panel ID="pnlCalifica" runat="server" Visible="false" >
                    <asp:DropDownList ID="DDLcalifica" runat="server"
                        Style="font-family: Arial, Helvetica, sans-serif; font-size: medium">
                        <asp:ListItem Value="100">100</asp:ListItem>
                        <asp:ListItem Value="95">95</asp:ListItem>
                        <asp:ListItem Value="90">90</asp:ListItem>
                        <asp:ListItem Value="85">85</asp:ListItem>
                        <asp:ListItem Value="80">80</asp:ListItem>
                        <asp:ListItem Value="75">75</asp:ListItem>
                        <asp:ListItem Value="70">70</asp:ListItem>
                        <asp:ListItem Value="65">65</asp:ListItem>
                        <asp:ListItem Value="60">60</asp:ListItem>
                        <asp:ListItem Value="55">55</asp:ListItem>
                        <asp:ListItem Value="50">50</asp:ListItem>
                        <asp:ListItem Value="45">45</asp:ListItem>
                        <asp:ListItem Value="40">40</asp:ListItem>
                        <asp:ListItem Value="35">35</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>
                        <asp:ListItem Value="25">25</asp:ListItem>
                        <asp:ListItem Value="20">20</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="0">0</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;
                <asp:Button ID="BtnCalificar" runat="server" Text="Calificar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:HyperLink ID="HyperLink2" runat="server" 
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar&nbsp;al&nbsp;Menú</asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select distinct P.IdProfesor, G.IdGrupo, G.Descripcion as Grupo, Gr.IdGrado, Gr.Descripcion Grado, N.Descripcion as Nivel, Pl.IdPlantel, Pl.Descripcion as Plantel
from Usuario U, Profesor P, Grupo G, Grado Gr, Nivel N, Plantel Pl, Programacion Pr
where U.Login = @Login and P.IdUsuario = U.IdUsuario and Pr.IdProfesor = P.IdProfesor
and Pr.IdCicloEscolar = @IdCicloEscolar and G.IdGrupo = Pr.IdGrupo and
 Pl.IdPlantel = G.IdPlantel and Gr.IdGrado = G.IdGrado and N.IdNivel = Gr.IdNivel
and G.IdCicloEscolar = @IdCicloEscolar
order by Pl.Descripcion, N.Descripcion, Gr.Descripcion, G.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSdetalleactividades" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT E.* FROM Evaluacion E, Calificacion C, Asignatura A, Grado GRA, Grupo GRU 
                    WHERE GRU.IdGrupo = @IdGrupo AND A.IdAsignatura = @IdAsignatura AND C.IdCicloEscolar = @IdCicloEscolar AND
                    A.IdAsignatura = C.IdAsignatura AND A.IdGrado = GRA.IdGrado AND GRA.IdGrado = GRU.IdGrado AND C.IdCalificacion = E.IdCalificacion AND E.Califica = 'P'">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:SessionParameter Name="IdGrupo" SessionField="IdGrupo" Type="Int32" />
                        <asp:SessionParameter Name="IdAsignatura" SessionField="IdAsignatura"
                            Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSmaterias" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdAsignatura, Descripcion
from Asignatura
where IdAsignatura in (select IdAsignatura from Programacion where IdProfesor in (select IdProfesor from Profesor Pr, Usuario U where U.Login = @Login and Pr.IdUsuario = U.IdUsuario) and IdGrupo =@idGrupo and IdCicloEscolar = @IdCicloEscolar )
order by descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="GVdatos" Name="idGrupo"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSalumnos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="
                    SELECT A.IdAlumno, A.Nombre + ' ' + A.ApePaterno + ' ' + A.ApeMaterno as NomAlumno, A.Matricula, A.Equipo, A.Subequipo, '' As Resultado FROM Alumno A, Grupo G WHERE A.Estatus = 'Activo' and A.IdGrupo = G.IdGrupo 
                    AND G.IdGrupo = @IdGrupo">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="GVdatos" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="GVactividades" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>
