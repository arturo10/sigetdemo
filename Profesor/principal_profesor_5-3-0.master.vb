﻿Imports Siget

Imports System.Data.SqlClient

Partial Class principal_profesor_5_3_0
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            UserInterface.Include.JQuery(pnlHeader)
            UserInterface.Include.JQuery_UI(pnlHeader)

            ' aplica lenguaje necesita controles inicializados de initPageData
            aplicaLenguaje()

            OutputCss()
        End If

        ' cargo enlaces de controles configurables
        hlClientLogo.NavigateUrl = Config.PaginaInicio.LogoCliente_Objetivo

        ' Cuenta mensajes entrantes: si hay mensajes, muestra el globo con el número en el menú superior
        Dim msgCount As Integer = DataAccess.ComunicacionDa.CuentaMensajesProfesor(Session("Usuario_IdProfesor"))
        If msgCount > 0 Then
            ' imprimo el señalador de mensajes
            estableceMensajes(msgCount)
            ' e incluyo las funcioens para sacudirlo
            OutputShaker()
        End If

        ' oculto la firma si está configurada
    If Not Config.Global.FIRMA_PIE_INTEGRANT Then
      imgLogoIntegrantFooter.Visible = False
      hlIntegrant.Visible = False
      ltCompany.Visible = False
      hlCreditos.Visible = False
    End If
    End Sub

    Protected Sub aplicaLenguaje()
        LblFecha.Text = DateAndTime.Now

        If HttpContext.Current.User.Identity.IsAuthenticated Then
            ltLogoutButton.Text = Lang.FileSystem.Alumno.Master_Principal.LT_LOGOUT_BUTTON.Item(Session("Usuario_Idioma"))
            ltUserName.Text = HttpContext.Current.User.Identity.Name.Trim()
        Else
            ltLogoutButton.Text = Lang.FileSystem.Alumno.Master_Principal.LT_LOGOUT_BUTTON_UNAUTHORIZED.Item(Session("Usuario_Idioma"))
            ltUserName.Text = ""
        End If

        lt_cursos.Text = Lang.FileSystem.Alumno.Master_Principal.LT_CURSOS.Item(Session("Usuario_Idioma"))
        lt_comunicacion.Text = Lang.FileSystem.Alumno.Master_Principal.LT_COMUNICACION.Item(Session("Usuario_Idioma"))
        lt_biblioteca.Text = Lang.FileSystem.Alumno.Master_Principal.LT_BIBLIOTECA.Item(Session("Usuario_Idioma"))
        lt_blog.Text = Lang.FileSystem.Alumno.Master_Principal.LT_BLOG.Item(Session("Usuario_Idioma"))
        lt_rightsReserved.Text = Lang.FileSystem.Alumno.Master_Principal.LT_RIGHTS_RESERVED.Item(Session("Usuario_Idioma"))
        hlCreditos.Text = Lang.FileSystem.Alumno.Master_Principal.HL_CREDITOS.Item(Session("Usuario_Idioma"))
    End Sub

    ' Permite establecer el número de mensajes recibidos en el menú superior; 
    ' de lo contrario su contenedor <asp:Label> está visible = false
    Public Sub estableceMensajes(ByRef numero As Integer)
        lblNotificaComunicacion.Text = numero.ToString()
        lblNotificaComunicacion.Visible = True
    End Sub

    ' imprimo los estilos variables generales
    Protected Sub OutputCss()
        Dim s As StringBuilder = New StringBuilder()
        s.Append("<style type='text/css'>")
        ' formatos generales
        s.Append("  .dataSources, .hiddenFields {")
        s.Append("    display: none;")
        s.Append("  }")
        ' formatos de grids
        s.Append("  .dataGrid_clear .header, .dataGrid_clear_selectable .header {")
        s.Append("    background-color: " & Config.Color.Table_HeaderRow_Background & ";")
        s.Append("    color: " & Config.Color.Table_HeaderRow_Color & ";")
        s.Append("  }")
        s.Append("    .dataGrid_clear .header a:link, .dataGrid_clear_selectable .header a:link,")
        s.Append("    .dataGrid_clear .header a:hover, .dataGrid_clear_selectable .header a:hover,")
        s.Append("    .dataGrid_clear .header a:visited, .dataGrid_clear_selectable .header a:visited, ")
        s.Append("    .dataGrid_clear .header a:active, .dataGrid_clear_selectable .header a:active {")
        s.Append("      color: " & Config.Color.Table_HeaderRow_Color & ";")
        s.Append("    }")
        s.Append("  .dataGrid_clear_selectable tbody tr:hover {")
        s.Append("    background-color: " & Config.Color.Table_SelectableRowHover_Background & ";")
        s.Append("  }")
        s.Append("  .dataGrid_clear .selected, .dataGrid_clear_selectable .selected {")
        s.Append("    background-color: " & Config.Color.Table_SelectedRow_Background & ";")
        s.Append("  }")
        s.Append("  .dataGrid_clear .selected:hover, .dataGrid_clear_selectable .selected:hover {")
        s.Append("    background-color: " & Config.Color.Table_SelectedRowHover_Background & ";")
        s.Append("  }")
        s.Append("</style>")
        ltEstilos.Text += s.ToString()
    End Sub

    ' imprimo los scripts para sacudir el indicador de mensajes
    Protected Sub OutputShaker()
        Dim s As StringBuilder = New StringBuilder()
        s.Append("<script type=""text/javascript"">")
        s.Append("    (function shakeMessages() {")
        s.Append("        $.when(")
        s.Append("            $(""#lblNotificaComunicacion"").effect(""shake"", { distance: 5 })")
        s.Append("        ).done(function () {")
        s.Append("            setTimeout(shakeMessages, 5000);")
        s.Append("        });")
        s.Append("    }());")
        s.Append("</script>")
        pnlHeader.Text += s.ToString()
    End Sub

    ' Ésta propiedad es necesaria para inyectar estilos y scripts en el encabezado de la página principal
    ' para poder referenciarse (Master.ltHeader) a ésta página maestra en las páginas hijas, 
    ' incluir el inicio de la página de diseño de cada una:
    ' <%@ MasterType VirtualPath="~/master_examplefile.master"  %>
    Public Property ltHeader() As Literal
        Get
            Return pnlHeader ' reporto el literal en el head
        End Get

        Set(value As Literal)
            ' no es necesaria
        End Set
    End Property

End Class

