﻿Imports Siget


Partial Class profesor_CalificaDocumento
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = Trim(User.Identity.Name)

        GVgrupos.Caption = "<h3>" &
            Config.Etiqueta.GRUPOS &
            " que tiene asignad" & Config.Etiqueta.LETRA_GRUPO & "s actualmente</h3>"

        Label6.Text = Config.Etiqueta.ARTDET_ALUMNOS
        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.GRUPOS
        Label7.Text = Config.Etiqueta.LETRA_GRUPO
        Label3.Text = Config.Etiqueta.PROFESOR
        Label4.Text = Config.Etiqueta.ALUMNOS
        Label5.Text = Config.Etiqueta.CICLO
    End Sub

    Protected Sub GVgrupos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVgrupos.SelectedIndexChanged
        Session("IdGrupo") = GVgrupos.SelectedDataKey.Values(0).ToString
        Session("IdGrado") = GVgrupos.SelectedDataKey.Values(1).ToString
        Session("IdPlantel") = GVgrupos.SelectedDataKey.Values(2).ToString
        GVactividades.Caption = "<font size=""2""><b>Actividades que requieren la Revisión de un ARCHIVO DE ENTREGA de " &
            Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO &
            " " + GVgrupos.SelectedRow.Cells(3).Text + _
            " de " & Config.Etiqueta.ARTDET_PLANTEL & " " & Config.Etiqueta.PLANTEL &
            " " + GVgrupos.SelectedRow.Cells(8).Text + "<BR>" + _
            " - " + GVgrupos.SelectedRow.Cells(6).Text + " - " + GVgrupos.SelectedRow.Cells(5).Text + "</b></font>"
    End Sub

    Protected Sub GVactividades_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVactividades.SelectedIndexChanged
        Session("IdEvaluacion") = GVactividades.SelectedDataKey.Values(0).ToString()
        Session("IdAsignatura") = GVactividades.SelectedDataKey.Values(1).ToString() 'La voy a necesitar en EnviarmensajeP2.aspx
        Session("Titulo") = "Alumnos del " & Config.Etiqueta.GRUPO & " " + GVgrupos.SelectedRow.Cells(3).Text + " que han realizado la Actividad " + GVactividades.SelectedRow.Cells(2).Text + " en la " & Config.Etiqueta.ASIGNATURA & " " + GVactividades.SelectedRow.Cells(6).Text
        Session("Evaluacion") = GVactividades.SelectedRow.Cells(2).Text
        Session("Asignatura") = GVactividades.SelectedRow.Cells(6).Text
        Session("IdCicloEscolar") = DDLcicloescolar.SelectedValue
        'Session("IdGrupo") e Session("IdPlantel") ya están cargadas en memoria
        Response.Redirect("CalificaDoctoAlumnos.aspx")
    End Sub

    Protected Sub GVgrupos_PreRender(sender As Object, e As EventArgs) Handles GVgrupos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVgrupos.Rows.Count > 0 Then
            GVgrupos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVgrupos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVgrupos.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVgrupos.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GVgrupos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVgrupos.RowDataBound
    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(3).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.GRUPO

      LnkHeaderText = e.Row.Cells(5).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.GRADO

      LnkHeaderText = e.Row.Cells(6).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.NIVEL

      LnkHeaderText = e.Row.Cells(8).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.PLANTEL
    End If

    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVgrupos, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVgrupos.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVgrupos, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    For Each r In GVactividades.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVactividades, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

  Protected Sub GVactividades_PreRender(sender As Object, e As EventArgs) Handles GVactividades.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVactividades.Rows.Count > 0 Then
      GVactividades.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVactividades_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVactividades.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVactividades.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

    Protected Sub GVactividades_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVactividades.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(6).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA
        End If

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVactividades, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub
End Class
