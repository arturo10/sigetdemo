﻿Imports Siget

'Imports System.Data
'Imports System.Data.OleDb
Imports System.Data.SqlClient

Partial Class profesor_VerReactivosEv
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        TitleLiteral.Text = "Ver Reactivos"

        Label1.Text = Config.Etiqueta.CICLO
        Label2.Text = Config.Etiqueta.NIVEL
        Label3.Text = Config.Etiqueta.GRADO
        Label4.Text = Config.Etiqueta.ASIGNATURA

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString

        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim miNivel As SqlDataReader
        miComando = objConexion.CreateCommand
        'Obtengo el Nivel al que pertenece el Alumno
        Dim Usuario = User.Identity.Name
        miComando.CommandText = "SELECT P.IdPlantel, P.IdProfesor FROM Profesor P, Usuario U " + _
                        "WHERE U.Login = '" + Usuario + "' and P.IdUsuario = U.IdUsuario"
        Try
            objConexion.Open() 'Abro la conexion
            miNivel = miComando.ExecuteReader() 'Creo conjunto de registros
            miNivel.Read()
            Session("IdPlantel") = miNivel.Item("IdPlantel")
            Session("IdProfesor") = miNivel.Item("IdProfesor")
            miNivel.Close()
            objConexion.Close()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            'miNivel.Close() AQUI MARCA ERROR POR EL AMBITO DE LA EXCEPCION
            objConexion.Close()
        End Try
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        'DDLcicloescolar.Items.Insert(0, New ListItem("---Elija un Ciclo Escolar", 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRADO & " " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLcalificacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacion.DataBound
        DDLcalificacion.Items.Insert(0, New ListItem("---Elija una Calificación", 0))
    End Sub

    Protected Sub DDLevaluacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLevaluacion.DataBound
        DDLevaluacion.Items.Insert(0, New ListItem("---Elija una Tarea/Examen/Proyecto", 0))
    End Sub

    Protected Sub DDLevaluacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLevaluacion.SelectedIndexChanged
        'PRIMERO LOS REACTIVOS DE OPCION MULTIPLE
        Dim strConexion As String
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misPlanteamientos As SqlDataReader
        miComando = objConexion.CreateCommand
        objConexion.Open()
        Dim Redaccion As String

        'Consulta para sacar todos los reactivos de respuesta múltiple que tiene la actividad que se acaba de contestar
        miComando.CommandText = "select P.Consecutivo,  P.Redaccion as Planteamiento, " + _
                        "P.ArchivoApoyo, P.TipoArchivoApoyo, P.ArchivoApoyo2, P.TipoArchivoApoyo2, O.Consecutiva as OpcionCorrecta, O.Redaccion as Respuesta, O.ArchivoApoyo as ArchivoRespuesta, S.Numero as NoSubtema, S.Descripcion Subtema, P.TipoRespuesta " + _
                        "from Planteamiento P, Opcion O, Subtema S, DetalleEvaluacion DE " + _
                        "where DE.IdEvaluacion = " + DDLevaluacion.SelectedValue.ToString + " and " + _
                        "S.IdSubtema = DE.IdSubtema and P.IdSubtema = S.IdSubtema and " + _
                        "O.IdPlanteamiento = P.IdPlanteamiento and P.Ocultar = 0 and O.Correcta = 'S' and P.TipoRespuesta = 'Multiple' UNION " + _
                        "select P.Consecutivo,  P.Redaccion as Planteamiento, " + _
                        "P.ArchivoApoyo, P.TipoArchivoApoyo, P.ArchivoApoyo2, P.TipoArchivoApoyo2, O.Consecutiva as OpcionCorrecta, O.Redaccion as Respuesta, O.ArchivoApoyo as ArchivoRespuesta, S.Numero as NoSubtema, S.Descripcion Subtema, P.TipoRespuesta " + _
                        "from Planteamiento P, Opcion O, Subtema S, DetalleEvaluacion DE " + _
                        "where DE.IdEvaluacion = " + DDLevaluacion.SelectedValue.ToString + " and " + _
                        "S.IdSubtema = DE.IdSubtema and P.IdSubtema = S.IdSubtema and " + _
                        "O.IdPlanteamiento = P.IdPlanteamiento and P.Ocultar = 0 and P.TipoRespuesta = 'Abierta' " + _
                        "order by P.Consecutivo"

        misPlanteamientos = miComando.ExecuteReader() 'Creo conjunto de registros

        If misPlanteamientos.HasRows Then
            msgInfo.hide()
            'Ciclo para ir avazando reactivo por reactivo y sacando sus opciones o textbox para respuesta abierta y agregándolos al panel
            PnlReactivos.Controls.Add(New LiteralControl("<Table align='left' border='1' bordercolor = '#CCCCCC'>")) 'Agrego el inicio de la Tabla
            Dim Cont As Byte 'Esta variable la usaré para pintar los renglones
            Cont = 0
            Dim color As String

            'AGREGO EL TITULO (ES UNA TABLA DE 9 COLUMNAS)
            PnlReactivos.Controls.Add(New LiteralControl("<tr bgcolor = 'white' align='center'><font face='Arial' color='black' size='2'><b> " + _
                            Trim(DDLnivel.SelectedItem.ToString) + " - " + Trim(DDLgrado.SelectedItem.ToString) + " - " + Trim(DDLasignatura.SelectedItem.ToString) + "<BR>" + _
                            Trim(DDLcalificacion.SelectedItem.ToString) + " - " + Trim(DDLevaluacion.SelectedItem.ToString) + _
                            "</b></font></tr>"))


            'AGREGO LOS ENCABEZADOS DE LAS TABLAS
            PnlReactivos.Controls.Add(New LiteralControl("<tr  bgcolor = '#424242'><td width='2%' align='center'><font face='Arial' color='white' size='1'>CONS.</font></td><td width='15%' align='center'><font face='Arial' color='white' size='1'>PLANTEAMIENTO</font></td><td width='20%' align='center'><font face='Arial' color='white' size='1'>APOYO 1</font></td><td width='20%' align='center'><font face='Arial' color='white' size='1'>APOYO 2</font></td><td width='5%' align='center'><font face='Arial' color='white' size='1'>OPC. CORRECTA</font></td><td width='10%' align='center'><font face='Arial' color='white' size='1'>RESPUESTA</font></td><td width='20%' align='center'><font face='Arial' color='white' size='1'>APOYO RESPUESTA</font></td><td width='3%' align='center'><font face='Arial' color='white' size='1'>NUM. SUBTEMA</font></td><td width='10%' align='center'><font face='Arial' color='white' size='1'>SUBTEMA</font></td></tr>"))
            Do While misPlanteamientos.Read()
                Cont += 1
                If Cont Mod 2 = 0 Then
                    color = "#E3EAEB"
                Else
                    color = "#CCCCCC"
                End If
                'AQUI DEBEN DE IR PARA QUE SE REGENERE Y SE AGREGUE COMO NUEVOS OBJETOS EN EL PANEL
                Dim Etiq1 As Label = New Label 'Consecutivo
                Dim Etiq2 As Label = New Label 'Planeamiento
                Dim Imag1 As Image = New Image 'ArchivoApoyo
                Dim Link1 As HyperLink = New HyperLink 'ArchivoApoyo
                Dim Imag2 As Image = New Image 'ArchivoApoyo2
                Dim Link2 As HyperLink = New HyperLink 'ArchivoApoyo2
                Dim Etiq3 As Label = New Label 'OpcionCorrecta
                Dim Etiq4 As Label = New Label 'Respuesta (redacción)
                Dim Imag3 As Image = New Image 'ArchivoRespuesta
                Dim Link3 As HyperLink = New HyperLink 'ArchivoRespuesta
                Dim Etiq5 As Label = New Label 'NoSubtema
                Dim Etiq6 As Label = New Label 'Subtema

                'AGREGO EL CONSECUTIVO DEL PLANTEAMIENTO
                Etiq1.Text = "<font face = 'Arial' size='1'>" + misPlanteamientos.Item("Consecutivo").ToString + "</font>"
                PnlReactivos.Controls.Add(New LiteralControl("<tr  bgcolor = '" + color + "'><td width='5%'>"))
                PnlReactivos.Controls.Add(Etiq1)
                PnlReactivos.Controls.Add(New LiteralControl("</td>"))

                'AGREGO EL PLANTEAMIENTO
                If IsDBNull(misPlanteamientos.Item("Planteamiento")) Then
                    Redaccion = ""
                Else
                    Redaccion = Replace(HttpUtility.HtmlDecode(misPlanteamientos.Item("Planteamiento")), "*salto*", "<BR>")
                    Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                End If


                Etiq2.Text = "<font face = 'Arial' size='1'>" + Redaccion + "</font>"
                PnlReactivos.Controls.Add(New LiteralControl("<td width='15%'>"))
                PnlReactivos.Controls.Add(Etiq2)
                PnlReactivos.Controls.Add(New LiteralControl("</td>"))

                'AGREGO EL ARCHIVO DE APOYO 1 (SI HAY) --> El valor del campo no puede ser "Ninguno", en ese caso queda como NULL
                If IsDBNull(misPlanteamientos.Item("TipoArchivoApoyo")) Then
                    PnlReactivos.Controls.Add(New LiteralControl("<td width='15%' align='center'></td>"))
                Else
                    PnlReactivos.Controls.Add(New LiteralControl("<td width='15%' align='center'>"))
                    If misPlanteamientos.Item("TipoArchivoApoyo") = "Imagen" Then
            Imag1.ImageUrl = Config.Global.urlMaterial & misPlanteamientos.Item("ArchivoApoyo")
            PnlReactivos.Controls.Add(Imag1)
          Else
            Link1.Text = "<font face = 'Arial' size='1'>" + misPlanteamientos.Item("ArchivoApoyo") + "</font>"
            Link1.Target = "_blank"
            Link1.NavigateUrl = Config.Global.urlMaterial & misPlanteamientos.Item("ArchivoApoyo")
            PnlReactivos.Controls.Add(Link1)
          End If
          PnlReactivos.Controls.Add(New LiteralControl("</td>"))
        End If

        'AGREGO EL ARCHIVO DE APOYO 2 (SI HAY) --> El valor del campo no puede ser "Ninguno", en ese caso queda como NULL
        If IsDBNull(misPlanteamientos.Item("TipoArchivoApoyo2")) Then
          PnlReactivos.Controls.Add(New LiteralControl("<td width='15%'></td>"))
        Else
          PnlReactivos.Controls.Add(New LiteralControl("<td width='15%' align='center'>"))
          If misPlanteamientos.Item("TipoArchivoApoyo2") = "Imagen" Then
            Imag2.ImageUrl = Config.Global.urlMaterial & misPlanteamientos.Item("ArchivoApoyo2")
            PnlReactivos.Controls.Add(Imag2)
          Else
            Link2.Text = "<font face = 'Arial' size='1'>" + misPlanteamientos.Item("ArchivoApoyo2") + "</font>"
            Link2.Target = "_blank"
            Link2.NavigateUrl = Config.Global.urlMaterial & misPlanteamientos.Item("ArchivoApoyo2")
            PnlReactivos.Controls.Add(Link2)
          End If
          PnlReactivos.Controls.Add(New LiteralControl("</td>"))
        End If

        'AGREGO LA OPCION CORRECTA
        If Trim(misPlanteamientos.Item("TipoRespuesta")) = "Multiple" Then
          Etiq3.Text = "<font face = 'Arial' size='1'>" + misPlanteamientos.Item("OpcionCorrecta").ToString + "</font>"
          PnlReactivos.Controls.Add(New LiteralControl("<td width='5%' align='center'>"))
          PnlReactivos.Controls.Add(Etiq3)
          PnlReactivos.Controls.Add(New LiteralControl("</td>"))
        Else
          PnlReactivos.Controls.Add(New LiteralControl("<td width='5%' align='center'></td>"))
        End If

        'AGREGO LA RESPUESTA
        Etiq4.Text = "<font face = 'Arial' size='1'>" + misPlanteamientos.Item("Respuesta").ToString + "</font>"
        PnlReactivos.Controls.Add(New LiteralControl("<td width='15%' align='center'>"))
        PnlReactivos.Controls.Add(Etiq4)
        PnlReactivos.Controls.Add(New LiteralControl("</td>"))

        'AGREGO EL ARCHIVO DE APOYO DE LA RESPUESTA (SI HAY) --> El valor del campo no puede ser "Ninguno", en ese caso queda como NULL
        If IsDBNull(misPlanteamientos.Item("ArchivoRespuesta")) Then
          PnlReactivos.Controls.Add(New LiteralControl("<td width='15%'></td>"))
        Else
          PnlReactivos.Controls.Add(New LiteralControl("<td width='15%' align='center'>"))
          If misPlanteamientos.Item("ArchivoRespuesta") = "Imagen" Then
            Imag3.ImageUrl = Config.Global.urlMaterial & misPlanteamientos.Item("ArchivoRespuesta")
            PnlReactivos.Controls.Add(Imag3)
          Else
            Link3.Text = "<font face = 'Arial' size='1'>" + misPlanteamientos.Item("ArchivoRespuesta") + "</font>"
            Link3.Target = "_blank"
            Link3.NavigateUrl = Config.Global.urlMaterial & misPlanteamientos.Item("ArchivoRespuesta")
            PnlReactivos.Controls.Add(Link3)
          End If
          PnlReactivos.Controls.Add(New LiteralControl("</td>"))
        End If

        'AGREGO NUM. DE SUBTEMA
        Etiq5.Text = "<font face = 'Arial' size='1'>" + misPlanteamientos.Item("NoSubtema").ToString + "</font>"
        PnlReactivos.Controls.Add(New LiteralControl("<td width='5%' align='center'>"))
        PnlReactivos.Controls.Add(Etiq5)
        PnlReactivos.Controls.Add(New LiteralControl("</td>"))

        'AGREGO EL SUBTEMA
        Etiq6.Text = "<font face = 'Arial' size='1'>" + misPlanteamientos.Item("Subtema").ToString + "</font>"
        PnlReactivos.Controls.Add(New LiteralControl("<td width='10%' align='center'>"))
        PnlReactivos.Controls.Add(Etiq6)
        PnlReactivos.Controls.Add(New LiteralControl("</td></tr>"))
      Loop 'del While misPlanteamientos.Read()
      PnlReactivos.Controls.Add(New LiteralControl("</Table>"))
      msgError.hide()
    Else
      msgInfo.show("No existen reactivos para mostrar.")
    End If
    misPlanteamientos.Close()
    objConexion.Close()
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GVreactivosDemo_PreRender(sender As Object, e As EventArgs) Handles GVreactivosDemo.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVreactivosDemo.Rows.Count > 0 Then
      GVreactivosDemo.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVreactivosDemo_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVreactivosDemo.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVreactivosDemo.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub
End Class
