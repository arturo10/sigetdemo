﻿Imports Siget

Imports System.Data
Imports System.IO
Imports System.Data.SqlClient

Partial Class profesor_AvanceActividadesProf
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = Trim(User.Identity.Name)

        GVdatos.Caption = "<h3>Seleccione algun" & Config.Etiqueta.LETRA_GRUPO & " de " &
            Config.Etiqueta.ARTDET_GRUPOS & " " & Config.Etiqueta.GRUPOS &
            " que tiene asignad" & Config.Etiqueta.LETRA_GRUPO & "s</h3>"

        TitleLiteral.Text = "Avance - Realizadas"

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label8.Text = Config.Etiqueta.ARTIND_GRUPO
        Label7.Text = Config.Etiqueta.GRUPO
        Label9.Text = Config.Etiqueta.LETRA_ALUMNO
        Label2.Text = Config.Etiqueta.PROFESOR
        Label10.Text = Config.Etiqueta.ARTIND_ASIGNATURA
        Label3.Text = Config.Etiqueta.ASIGNATURA
        Label11.Text = Config.Etiqueta.ARTDET_GRUPOS
        Label12.Text = Config.Etiqueta.LETRA_GRUPO
        Label4.Text = Config.Etiqueta.GRUPOS
        Label5.Text = Config.Etiqueta.CICLO
        Label6.Text = Config.Etiqueta.ASIGNATURA
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub GVdatos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(1)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.PROFESOR

            LnkHeaderText = e.Row.Cells(2).Controls(1)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.GRUPO

            LnkHeaderText = e.Row.Cells(3).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.GRUPO

            LnkHeaderText = e.Row.Cells(4).Controls(1)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.GRADO

            LnkHeaderText = e.Row.Cells(5).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.GRADO

            LnkHeaderText = e.Row.Cells(6).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.NIVEL

            LnkHeaderText = e.Row.Cells(7).Controls(1)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.PLANTEL

            LnkHeaderText = e.Row.Cells(8).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.PLANTEL
        End If

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVdatos, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    Protected Sub GVdatos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVdatos.SelectedIndexChanged
        Session("IdGrado") = GVdatos.SelectedDataKey.Values(1).ToString
    End Sub

    Protected Sub DDLasignatura_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.SelectedIndexChanged
        If DDLasignatura.SelectedValue > 0 Then
            GVreporte.Visible = True
        Else
            GVreporte.Visible = False
        End If
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVreporte, "AvanceActividadesGrupo")
    End Sub

    Protected Sub GVreporte_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVreporte.DataBound
        GVreporte.Caption = "<font size = ""2""><b>Avance en las actividades de " &
            Config.Etiqueta.ARTDET_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA &
            " " + DDLasignatura.SelectedItem.ToString + "</b></font>"

        ' Etiquetas de GridView
        GVreporte.Columns(0).HeaderText = Config.Etiqueta.GRUPO
        GVreporte.Columns(0).SortExpression = "Grupo"
    End Sub

    Protected Sub gvLicencias_PreRender(sender As Object, e As EventArgs) Handles GVdatos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVdatos.Rows.Count > 0 Then
            GVdatos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub gvLicencias_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If GVdatos.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVdatos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVdatos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub
End Class
