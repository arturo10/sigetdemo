﻿Imports Siget

Imports System.Data.SqlClient

Partial Class profesor_Default
    Inherits System.Web.UI.Page

    Protected Sub OutputCSS()
    Literal1.Text = "" &
            "<style type='text/css'>" &
                    ".btn {" &
                            "background-image: url(" & Config.Global.urlImagenes & "btn_back.png); /* fallback */" &
                    "}" &
                                    ".btn:hover {" &
                                            "background-image: url(" & Config.Global.urlImagenes & "btn_back_over.png); /* fallback */" &
                                    "}" &
            "</style>"

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        'UserInterface.Include.JQuery(CType(Master.FindControl("pnlHeader"), Literal))

        ' esto debe ir antes que cualquier operación que utilice base de datos
        If Utils.Bloqueo.sistemaBloqueado() Then
            Utils.Sesion.CierraSesion()
            Response.Redirect("~/EnMantenimiento.aspx")
        End If
        TitleLiteral.Text = "Menú Principal"


        ' Si el perfil de PROFESOR está bloqueado en este momento, cierra sesión y notifica
        Try
            Dim strCon As String
            strCon = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objCon As New SqlConnection(strCon)
            Dim comm As SqlCommand
            Dim reg As SqlDataReader
            comm = objCon.CreateCommand
            objCon.Open()
            comm.CommandText = "SELECT I.Descripcion FROM Profesor A, Usuario U, Plantel P, Institucion I WHERE U.Login = '" + User.Identity.Name + "' and A.IdUsuario = U.IdUsuario and P.IdPlantel = A.IdPlantel AND I.IdInstitucion = P.IdInstitucion"
            reg = comm.ExecuteReader() 'Creo conjunto de registros
            Dim inst As String = ""
            If reg.HasRows() Then
                reg.Read()
                inst = reg.Item("Descripcion")
            End If
            reg.Close()
            objCon.Close()
            If Utils.Bloqueo.profesorBloqueado(inst) Then
                FormsAuthentication.SignOut()
                Response.Redirect("~/BloqueadoProfesor.aspx")
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try

        Label1.Text = Config.Etiqueta.PROFESORES
        Label2.Text = Config.Etiqueta.ASIGNATURA
        OutputCSS()

        'Despliego los Avisos si los hay
        Dim strConexion1 As String
        strConexion1 = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion1 As New SqlConnection(strConexion1)
        Dim miComando1 As SqlCommand
        Dim misRegistros1 As SqlDataReader
        miComando1 = objConexion1.CreateCommand
        objConexion1.Open()
        miComando1.CommandText = "select * from Mensaje where FechaInicia <= getdate() and FechaTermina >= getdate() order by Posicion"
        misRegistros1 = miComando1.ExecuteReader() 'Creo conjunto de registros
        If misRegistros1.HasRows Then
            While misRegistros1.Read()
                Select Case misRegistros1.Item("Posicion")
                    Case 1
                        LblMensaje1.Text = misRegistros1.Item("Contenido")
                    Case 2
                        LblMensaje2.Text = misRegistros1.Item("Contenido")
                    Case 3
                        LblMensaje3.Text = misRegistros1.Item("Contenido")
                    Case 4
                        LblMensaje4.Text = misRegistros1.Item("Contenido")
                    Case 5
                        LblMensaje5.Text = misRegistros1.Item("Contenido")
                    Case 6
                        LblMensaje6.Text = misRegistros1.Item("Contenido")
                End Select
            End While
        Else
            LblMensaje1.Text = "No hay avisos vigentes."
        End If
        misRegistros1.Close()
        objConexion1.Close()

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader

        miComando = objConexion.CreateCommand
        miComando.CommandText = "SELECT P.* FROM Usuario U, Profesor P where U.Login = '" + User.Identity.Name + "' and P.IdUsuario = U.IdUsuario"
        '   Try
        'Abrir la conexión y leo los registros del Alumno
        objConexion.Open()
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        misRegistros.Read() 'Leo para poder accesarlos
        Dim IdProfesor = misRegistros.Item("IdProfesor")
        'Cierro y ahora buscaré si tiene mensajes (El profesor E=Envía o R=Recibe
        misRegistros.Close()

        miComando.CommandText = "select Count(IdComunicacion) as Total from Comunicacion C, Programacion P where C.IdProgramacion = P.IdProgramacion " + _
                                                        " and P.IdProfesor = " + IdProfesor.ToString + " and C.Sentido = 'R' and C.Estatus = 'Nuevo'"

        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        misRegistros.Read() 'Leo para poder accesarlos
        If misRegistros.Item("Total") > 0 Then
            HLmensaje.Text = "Tiene " + misRegistros.Item("Total").ToString + " NUEVO(S) mensaje(s) de " & Config.Etiqueta.ALUMNOS & " en su bandeja."
        Else
            HLmensaje.Text = ""
        End If

        misRegistros.Close()
        miComando.CommandText = "select Count(IdComunicacionCP) as Total from ComunicacionCP where IdProfesor = " + _
                                        IdProfesor.ToString + " and Sentido = 'E' and EstatusP = 'Nuevo'"
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        misRegistros.Read() 'Leo para poder accesarlos
        If misRegistros.Item("Total") > 0 Then
            HLmensaje.Text = HLmensaje.Text + "<BR>Tiene " + misRegistros.Item("Total").ToString + " NUEVO(S) mensaje(s) de " & Config.Etiqueta.COORDINADORES & " en su bandeja."

        End If
        misRegistros.Close()


        'Las siguientes variables de Aplicación las lee el archivo App_Code/PTBlogTemplate/UserFunctions.vb
        'La quize poner en If Menu3.SelectedItem.Value = "Blog" Then pero no funcionó
        Application("Login") = Trim(User.Identity.Name)
        miComando.CommandText = "select Password from Usuario where Login = '" + Trim(User.Identity.Name) + "'"
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        misRegistros.Read()
        Application("Passwd") = misRegistros.Item("Password")
        misRegistros.Close()

        objConexion.Close()
        msgError.hide()
        '   Catch ex As Exception
        ' msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        '    End Try
    End Sub

End Class
