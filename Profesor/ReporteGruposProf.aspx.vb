﻿Imports Siget

'Imports System.Web.Security
'Imports System.Data
'Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.IO

Partial Class profesor_ReporteGruposProf
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        TitleLiteral.Text = "Resultados por Asignatura"

        GVsalida.Caption = "<h3>Listado de " &
            Config.Etiqueta.ALUMNOS & " de " &
            Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO &
            " seleccionado.</h3>"

        GVgrupos.Caption = "<h3>Seleccione " &
            Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO &
            " que desee consultar.</h3>"

        Label1.Text = Config.Etiqueta.ALUMNO
        Label2.Text = Config.Etiqueta.ASIGNATURA
        Label3.Text = Config.Etiqueta.ALUMNO
        Label4.Text = Config.Etiqueta.ASIGNATURA
        Label5.Text = Config.Etiqueta.ASIGNATURA
        Label6.Text = Config.Etiqueta.GRUPOS
        Label7.Text = Config.Etiqueta.CICLO
        Label8.Text = Config.Etiqueta.ASIGNATURA
        Label9.Text = Config.Etiqueta.ARTIND_ASIGNATURA
        Label10.Text = Config.Etiqueta.ARTDET_ASIGNATURA
        Label11.Text = Config.Etiqueta.PLANTEL
        Label12.Text = Config.Etiqueta.ARTIND_CICLO
        Label13.Text = Config.Etiqueta.ARTIND_PLANTEL

        'Inicializo las siguientes variables por si vuelve a entrar que el grid GVsalida no presenta datos
        If GVgrupos.SelectedIndex < 0 Then 'Cuando no hay nada seleccionado en un grid, el Index es -1
            Session("IdGrupo") = 0
            Session("IdAsignatura") = 0
        End If


        Dim Usuario = User.Identity.Name
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader

        miComando = objConexion.CreateCommand
        'Con obtener el IdGrado es suficiente para rastrear el nivel ya que son
        'valores unicos en la tabla
        miComando.CommandText = "SELECT P.IdProfesor, P.Nombre, P.Apellidos FROM Usuario U, Profesor P where U.Login = '" + Usuario + "' and P.IdUsuario = U.IdUsuario"
        Try
            'Abrir la conexión y leo los registros
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read() 'Leo para poder accesarlos
            LblNombre.Text = Config.Etiqueta.PROFESOR & ": " + misRegistros.Item("Nombre") + " " + misRegistros.Item("Apellidos")
            'Estos valores se pasarán al siguiente script (Contestar.aspx) SE SIGUEN OCUPANDO?
            Session("IdProfesor") = misRegistros.Item("IdProfesor")
            misRegistros.Close()
            objConexion.Close()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVgrupos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVgrupos.SelectedIndexChanged
        Try
            GVsalida.Caption = "<b>Resultados de " &
                Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO &
                " " + GVgrupos.SelectedRow.Cells(2).Text + " de " &
                Config.Etiqueta.ARTDET_PLANTEL & " " & Config.Etiqueta.PLANTEL &
                " " + GVgrupos.SelectedRow.Cells(7).Text &
                " En " & Config.Etiqueta.ARTDET_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA &
                " " + GVgrupos.SelectedRow.Cells(4).Text + "<br>Reporte emitido el " + Date.Today.ToShortDateString + "</b>"

            'Para obtener el porcentaje máximo posible
            Session("IdAsignatura") = GVgrupos.SelectedDataKey.Values(1)
            Session("IdGrupo") = GVgrupos.SelectedDataKey.Values(0)
            'Primero obtengo las calificaciones
            Dim strConexion As String
            'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objConexion As New SqlConnection(strConexion)
            Dim miComando As SqlCommand
            Dim misRegistros As SqlDataReader
            miComando = objConexion.CreateCommand
            objConexion.Open()


            'CUENTO LAS CUANTAS CALIFICACIONES HAY
            miComando.CommandText = " select Count(Distinct C.IdCalificacion) as Total from Calificacion C,Asignatura A" + _
                " where A.IdAsignatura = " + GVgrupos.SelectedDataKey.Values(1).ToString + " and C.IdAsignatura = A.IdAsignatura and" + _
                " C.IdCicloEscolar = " + DDLcicloescolar.SelectedValue
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            Session("TotalC") = misRegistros.Item("Total")

            misRegistros.Close()
            objConexion.Close()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        'Este procedimiento convierte el GridView a Excel eliminando la primer columna
        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            'Algoritmo que oculta toda la columna donde aparece el "Seleccionar"
            Dim Cont As Integer
            GVactual.HeaderRow.Cells.Item(0).Visible = False
            For Cont = 0 To GVactual.Rows.Count - 1
                GVactual.Rows(Cont).Cells.Item(0).Visible = False
            Next
            'Algoritmo que oculta la columna IdAlumn
            GVactual.HeaderRow.Cells.Item(1).Visible = False
            For Cont = 0 To GVactual.Rows.Count - 1
                GVactual.Rows(Cont).Cells.Item(1).Visible = False
            Next

            ' quito la imágen de ordenamiento de los encabezados
            For Each tc As TableCell In GVactual.HeaderRow.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    tc.Controls.RemoveAt(0)
                End If
            Next

            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVsalida, "RepAlumnos")
    End Sub

    Protected Sub GVsalida_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVsalida.SelectedIndexChanged
        'Variables que necesito pasar
        Session("NomAlumno") = GVsalida.SelectedRow.Cells(4).Text 'Campo Alumnos
        Session("IdAlumno") = GVsalida.SelectedDataKey.Values(0).ToString 'GVsalida.SelectedRow.Cells(1).Text
        'Session("IdAsignatura") = GVgrupos.SelectedDataKey.Values(1).ToString Esta ya la tengo desde el GVgrupos
        Session("IdCicloEscolar") = DDLcicloescolar.SelectedValue.ToString
        Session("IdEvaluacionT") = 0 'Esto lo hago para eliminar el valor en memoria y que
        'si regresa la pagina y vuelve a avanzar, no se carge el grid de respuestas
        Response.Redirect("DetalleRespuestas.aspx")
    End Sub

    Protected Sub GVsalida_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVsalida.RowDataBound
        Try
            If e.Row.Cells.Count > 5 Then 'Cuenta el total de COLUMNAS que hay en el grid,  GValumnos.Columns.Count NO funciona
                '1)PRIMERO OBTENGO LOS RANGOS DE LOS INDICADORES PARA EL SEMAFORO ROJO, AMARILLO Y VERDE
                Dim strConexion As String
                'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                Dim objConexion As New SqlConnection(strConexion)
                Dim miComando As SqlCommand
                Dim misRegistros As SqlDataReader

                miComando = objConexion.CreateCommand
                'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
                miComando.CommandText = "select I.MinAmarillo, I.MinVerde, I.MinAzul from Indicador I, CicloEscolar C where " + _
                                        "I.IdIndicador = C.IdIndicador and C.IdCicloEscolar = " + DDLcicloescolar.SelectedValue
                Dim MinAmarillo, MinVerde, MinAzul As Decimal

                objConexion.Open()
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read()
                MinAmarillo = misRegistros.Item("MinAmarillo")
                MinVerde = misRegistros.Item("MinVerde")
                MinAzul = misRegistros.Item("MinAzul")
                misRegistros.Close()

                Dim Quita As Byte
                If Session("TotalC") > 1 Then
                    Quita = 4
                Else
                    Quita = 2
                End If

                '2)HAGO UN CICLO QUE VAYA DE 1 AL TOTAL DE COLUMNAS (EL INDICE DE LAS CELDAS(COLUMNAS) INICIA EN 0 (CELLS(0)= )
                If (e.Row.Cells.Count > 1) And (e.Row.RowType = DataControlRowType.DataRow) Then  'Para que se realice la comparacion cuando haya mas de una columna y que esta sea de datos (no de Encabezado) ANTES TENÍA: And (e.Row.RowIndex > -1) Then
                    For I As Byte = 6 To e.Row.Cells.Count - 1 'Inicio a partir de la sexta columna a comparar
                        If IsNumeric(Trim(e.Row.Cells(I).Text)) Then 'para que no compare celdas vacias
                            If (CDbl(e.Row.Cells(I).Text) < MinAmarillo) Then
                                e.Row.Cells(I).BackColor = Drawing.Color.LightSalmon
                            ElseIf (CDbl(e.Row.Cells(I).Text) < MinVerde) Then
                                e.Row.Cells(I).BackColor = Drawing.Color.Yellow
                            ElseIf (CDbl(e.Row.Cells(I).Text) < MinAzul) Then
                                e.Row.Cells(I).BackColor = Drawing.Color.LightGreen
                            Else 'Significa que es mayor o igual al MinAzul:
                                e.Row.Cells(I).BackColor = Drawing.Color.LightBlue
                            End If
                        End If
                    Next

                    '3)AHORA VERIFICO CALIFICACION POR CALIFICACION SI TIENEN UN INDICADOR ESPECÍFICO PARA PINTARLA EN BASE A SU COLOR
                    Dim TC, Cuenta As Byte
                    TC = e.Row.Cells.Count
                    Cuenta = 5 'La columna 5 es la primera de las calificaciones acumuladas, luego aparecen cada 2 columnas
                    Do While (Cuenta < (TC - Quita))
                        misRegistros.Close()
                        'Obtengo los datos de la Calificacion para ver si tiene asignado un Indicador específico
                        miComando.CommandText = "select * from Calificacion where IdCalificacion = " + e.Row.Cells(Cuenta).Text
                        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                        misRegistros.Read()

                        'Verifico si tiene asignado algun Indicador Específico
                        Dim Ind = 0
                        If Not IsDBNull(misRegistros.Item("IdIndicador")) Then
                            Ind = misRegistros.Item("IdIndicador")
                        End If

                        'Si tiene un indicador, obtengo los Rangos para pintar la celda
                        If Ind > 0 Then
                            misRegistros.Close()
                            miComando.CommandText = "select * from Rango where IdIndicador=" + Ind.ToString
                            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                            Do While misRegistros.Read()
                                'Aquí sucede que una actividad no realizada no aparece 0, sino espacio en blanco, por tanto tengo que hacer lo siguiente
                                Dim Valor As Decimal
                                If e.Row.Cells(Cuenta + 1).Text = "&nbsp;" Then
                                    Valor = 0
                                Else
                                    Valor = CDbl(e.Row.Cells(Cuenta + 1).Text)
                                End If

                                'Voy leyendo Rango por Rango y pinto comparando pintando donde está la celda con el valor de la calificación, que es la que está a la derecha del IdCalificacion
                                If (Valor >= misRegistros.Item("Inferior")) And (Valor <= misRegistros.Item("Superior")) And e.Row.Cells(Cuenta + 1).Text <> "&nbsp;" Then
                                    e.Row.Cells(Cuenta + 1).BackColor = Drawing.ColorTranslator.FromHtml("#" + misRegistros.Item("Color"))
                                    Exit Do
                                End If
                            Loop
                        End If
                        Cuenta += 3
                    Loop
                End If ' del if (e.Row.Cells.Count > 1) And (e.Row.RowType = DataControlRowType.DataRow) Then
                misRegistros.Close()
                objConexion.Close()


                '4)Escondo la columna del IdAlumno
                e.Row.Cells(1).Visible = False

                '5)Ahora oculto todos los IdCalificacion
                Dim TotCol, Cont As Byte
                TotCol = e.Row.Cells.Count
                Cont = 5 'La columna 5 es el Primero de los IdCalificacion, luego aparecen cada 2 columnas
                Do While (Cont < (TotCol - Quita))
                    e.Row.Cells(Cont).Visible = False
                    Cont += 3
                Loop

                '6)Lo siguiente es para ocultar o mostrar las columnas de calificaciones acumuladas. Quize usar un checkbox pero no hace el postback
                If Session("TotalC") > 1 Then
                    Quita = 2
                Else
                    Quita = 0
                End If
                If RBacumuladas.SelectedValue = "O" Then
                    TotCol = e.Row.Cells.Count
                    Cont = 6 'La columna 6 es la primera de las calificaciones acumuladas, luego aparecen cada 3 columnas
                    Do While (Cont < (TotCol - Quita))
                        e.Row.Cells(Cont).Visible = False
                        Cont += 3
                    Loop
                    If Session("TotalC") > 1 Then
                        e.Row.Cells(TotCol - Quita).Visible = False
                    End If
                End If
            End If 'del if e.Row.Cells.Count > 5
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
        End Try

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(2).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.EQUIPO

            LnkHeaderText = e.Row.Cells(4).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.ALUMNO
        End If

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVsalida, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    Protected Sub RBacumuladas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RBacumuladas.SelectedIndexChanged
        GVsalida.DataBind()
    End Sub

    Protected Sub GVsalida_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVsalida.DataBound
        If GVsalida.Rows.Count > 0 Then
            BtnExportar.Visible = True
        Else
            BtnExportar.Visible = False
        End If
    End Sub

    Protected Sub GVindicadores_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVindicadores.RowDataBound
        If (e.Row.Cells.Count > 1) And (e.Row.RowType = DataControlRowType.DataRow) Then
            e.Row.Cells(0).BackColor = Drawing.ColorTranslator.FromHtml("#" + e.Row.Cells(1).Text)
        End If
        e.Row.Cells(1).Visible = False
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVgrupos_PreRender(sender As Object, e As EventArgs) Handles GVgrupos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVgrupos.Rows.Count > 0 Then
            GVgrupos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If

        ' Etiquetas de GridView
        GVgrupos.Columns(2).HeaderText = Config.Etiqueta.GRUPO
        GVgrupos.Columns(2).SortExpression = "Grupo"

        GVgrupos.Columns(4).HeaderText = Config.Etiqueta.ASIGNATURA
        GVgrupos.Columns(4).SortExpression = "Asignatura"

        GVgrupos.Columns(5).HeaderText = Config.Etiqueta.GRADO
        GVgrupos.Columns(5).SortExpression = "Grado"

        GVgrupos.Columns(7).HeaderText = Config.Etiqueta.PLANTEL
        GVgrupos.Columns(7).SortExpression = "Plantel"

        GVgrupos.Columns(9).HeaderText = Config.Etiqueta.CICLO
        GVgrupos.Columns(9).SortExpression = "Ciclo"
    End Sub

    Protected Sub GVgrupos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVgrupos.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVgrupos, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVgrupos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVgrupos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        For Each r In GVsalida.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVsalida, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

    Protected Sub GVsalida_PreRender(sender As Object, e As EventArgs) Handles GVsalida.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVsalida.Rows.Count > 0 Then
            GVsalida.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVsalida_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVsalida.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If GVsalida.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub

End Class
