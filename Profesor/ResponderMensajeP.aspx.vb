﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO

Partial Class profesor_ResponderMensajeP
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            UserInterface.Include.HtmlEditor(CType(Master.FindControl("pnlHeader"), Literal), 665, 200, 12, Session("Usuario_Idioma").ToString())

            aplicaLenguaje()

            initPageData()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = "Responder Mensaje"

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.GRUPOS
        Label3.Text = Config.Etiqueta.ARTDET_ALUMNOS
        Label4.Text = Config.Etiqueta.LETRA_ALUMNO
        Label5.Text = Config.Etiqueta.ARTDET_COORDINADORES
        Label6.Text = Config.Etiqueta.COORDINADORES
        Label7.Text = Config.Etiqueta.PLANTEL
    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub initPageData()
        ' necesito obtener los datos del profesor para usarse al enviar el mensaje
        Try
            If Session("Envio_Previous").Equals("LeerMensajeSalidaP") Then
                HyperLink2.NavigateUrl = "~/profesor/LeerMensajeSalidaP.aspx"
            Else
                HyperLink2.NavigateUrl = "~/profesor/LeerMensajeEntradaP.aspx"
            End If

            LblNombre.Text = Session("Envio_Nombre")
            Dim asunto As String
            asunto = "RESP: " + Session("Envio_Asunto") + " [" + Session("Envio_Fecha") + "]"
            TBasunto.Text = Mid(asunto, 1, 100)

            If Session("Envio_IdComunicacion") <> "0" Then
                LblInfo.Text = Session("Envio_Asignatura")
            ElseIf Session("Envio_IdComunicacionCP") <> "0" Then
                LblInfo.Text = Session("Envio_Plantel")
            End If

            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()

                Dim cmd As SqlCommand = New SqlCommand("SELECT P.IdProfesor, P.Nombre, P.Apellidos FROM Usuario U, Profesor P where U.Login = @Login and P.IdUsuario = U.IdUsuario", conn)
                cmd.Parameters.AddWithValue("@Login", Trim(User.Identity.Name))

                Dim results As SqlDataReader = cmd.ExecuteReader()
                results.Read()
                hfIdProfesor.Value = results.Item("IdProfesor")
                Session("NombreProfesor") = results.Item("Nombre") + " " + results.Item("Apellidos")
                hfCarpetaAdjunto.Value = User.Identity.Name.Trim()

                results.Close()
                cmd.Dispose()
                conn.Close()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

#End Region

#Region "adjuntos"

    Protected Sub BtnAdjuntar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdjuntar.Click
        Try  'Cuando se adjunta el archivo, se borra el nombre del FileUpload
            If FUadjunto.HasFile Then
        Dim ruta As String = Config.Global.rutaAdjuntos_fisica & User.Identity.Name.Trim() & "\" 'Esta ruta contempla un directorio como alumno nombrado como su login

        'Verifico si ya existe el directorio del alumno, si no, lo creo
        If Not (Directory.Exists(ruta)) Then
          Dim directorio As DirectoryInfo = Directory.CreateDirectory(ruta)
        End If

        'Subo archivo
        Dim now As String = Date.Now.Year.ToString & "_" & Date.Now.Month.ToString & "_" & Date.Now.Day.ToString & "_" & Date.Now.Hour.ToString & "_" & Date.Now.Minute.ToString & "_" & Date.Now.Second.ToString & "_" & Date.Now.Millisecond.ToString
        hfAdjuntoFisico.Value = now & FUadjunto.FileName
        hfCarpetaAdjunto.Value = User.Identity.Name.Trim()
        FUadjunto.SaveAs(ruta & hfAdjuntoFisico.Value)
        lbAdjunto.Text = FUadjunto.FileName

        msgError.hide()
        msgSuccess.show("Se ha adjuntado el archivo.")
      Else
        msgError.show("No ha seleccionado un archivo para adjuntar.")
        msgSuccess.hide()
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      msgSuccess.hide()
    End Try
  End Sub

  Protected Sub lbAdjunto_Click(sender As Object, e As EventArgs) Handles lbAdjunto.Click
    Dim file As System.IO.FileInfo =
        New System.IO.FileInfo(Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value + "\" & hfAdjuntoFisico.Value) '-- if the file exists on the server

    If file.Exists Then 'set appropriate headers
      Response.Clear()
      Response.AddHeader("Content-Disposition", "attachment; filename=""" & lbAdjunto.Text & """")
      Response.AddHeader("Content-Length", file.Length.ToString())
      Response.ContentType = "application/octet-stream"
      Response.WriteFile(file.FullName)
      'Response.End()
    Else
      msgError.show("Ha ocurrido un error con el archivo adjunto: éste ya no está disponible.")
    End If 'nothing in the URL as HTTP GET
  End Sub

  Protected Sub BtnQuitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnQuitar.Click
    quitarAdjunto()
  End Sub

  Protected Sub quitarAdjunto()
    Try
      If hfAdjuntoFisico.Value.Length > 0 Then
        Dim ruta As String = Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value & "\" 'Esta ruta contempla un directorio como alumno nombrado como su login

        ' borro el archivo que estaba anteriormente porque se pondrá uno nuevo
        Dim adjunto As FileInfo = New FileInfo(ruta & hfAdjuntoFisico.Value)
        adjunto.Delete()

        lbAdjunto.Text = ""
        hfAdjuntoFisico.Value = ""
        msgError.hide()
        msgSuccess.show("Se ha eliminado el archivo.")
      Else
        msgError.show("No ha adjuntado ningún archivo todavía.")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgSuccess.hide()
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

#End Region

  Protected Sub BtnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLimpiar.Click
    TBcontenido.Text = ""
    ' al limpiar se limpia el adjunto, y debe eliminarse el archivo
    quitarAdjunto()
    ' se ocultan los mensajes al final puesto que al quitar el archivo pueden mostrarse
    msgSuccess.hide()
    msgError.hide()
  End Sub

  Protected Sub BtnEnviar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEnviar.Click
    Try
      If Session("Envio_IdComunicacion") <> "0" Then
        '[Sentido (E=Envia, E=Recibe - desde el punto de vista del profesor)][Estatus (Leido, Nuevo, Baja)]
        SDScomunicacion.InsertCommand = "SET dateformat dmy; INSERT INTO Comunicacion(IdProgramacion,Sentido,IdAlumno,Asunto,Contenido,Fecha,ArchivoAdjunto,ArchivoAdjuntoFisico,CarpetaAdjunto,EmailEnviado,Estatus,EstatusA)" &
                        " VALUES(@IdProgramacion, 'E', @IdAlumno, @Asunto, @Contenido, getdate(), @NombreAdjunto, @RutaAdjunto, @CarpetaAdjunto, @EmailEnviado, 'Nuevo', 'Nuevo')"
        SDScomunicacion.InsertParameters.Add("IdProgramacion", Session("Envio_IdProgramacion").ToString)
        SDScomunicacion.InsertParameters.Add("IdAlumno", Session("Envio_IdAlumno").ToString)
        SDScomunicacion.InsertParameters.Add("Asunto", TBasunto.Text)
        SDScomunicacion.InsertParameters.Add("Contenido", TBcontenido.Text)
        SDScomunicacion.InsertParameters.Add("NombreAdjunto", lbAdjunto.Text)
        SDScomunicacion.InsertParameters.Add("RutaAdjunto", hfAdjuntoFisico.Value)
        SDScomunicacion.InsertParameters.Add("CarpetaAdjunto", hfCarpetaAdjunto.Value)
        SDScomunicacion.InsertParameters.Add("EmailEnviado", Session("Envio_Email"))
        SDScomunicacion.Insert()
      ElseIf Session("Envio_IdComunicacionCP") <> "0" Then
        '[Sentido (E=Envia, E=Recibe - desde el punto de vista del profesor)][Estatus (Leido, Nuevo, Baja)]
        SDScomunicacion.InsertCommand = "SET dateformat dmy; INSERT INTO ComunicacionCP(IdCoordinador,IdProfesor,Sentido,Asunto,Contenido,Fecha,ArchivoAdjunto,ArchivoAdjuntoFisico,CarpetaAdjunto,EmailEnviado,Estatus,EstatusP)" &
                        " VALUES(@IdCoordinador, @IdProfesor, 'R', @Asunto, @Contenido, getdate(), @NombreAdjunto, @RutaAdjunto, @CarpetaAdjunto, @EmailEnviado, 'Nuevo', 'Nuevo')"
        SDScomunicacion.InsertParameters.Add("IdCoordinador", Session("Envio_IdCoordinador").ToString)
        SDScomunicacion.InsertParameters.Add("IdProfesor", hfIdProfesor.Value)
        SDScomunicacion.InsertParameters.Add("Asunto", TBasunto.Text)
        SDScomunicacion.InsertParameters.Add("Contenido", TBcontenido.Text)
        SDScomunicacion.InsertParameters.Add("NombreAdjunto", lbAdjunto.Text)
        SDScomunicacion.InsertParameters.Add("RutaAdjunto", hfAdjuntoFisico.Value)
        SDScomunicacion.InsertParameters.Add("CarpetaAdjunto", hfCarpetaAdjunto.Value)
        SDScomunicacion.InsertParameters.Add("EmailEnviado", Session("Envio_Email"))
        SDScomunicacion.Insert()
      End If

      If Trim(Session("Envio_Email")).Length > 1 Then
        If Config.Global.MAIL_ACTIVO Then
          Utils.Correo.EnviaCorreo(Trim(Session("Envio_Email")),
                                   Config.Global.NOMBRE_FILESYSTEM & " - Mensaje en " & Config.Etiqueta.SISTEMA_CORTO,
                                   "Tiene un nuevo mensaje en " & Config.Global.NOMBRE_FILESYSTEM & " que le ha enviado " + Session("NombreProfesor"),
                                   False)
        End If
      End If
      msgSuccess.show("Éxito", "Se ha enviado el mensaje.")
      msgError.hide()
      TBasunto.Text = ""
      TBcontenido.Text = ""
      lbAdjunto.Text = ""
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

End Class
