<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="CalificaDoctoAlumnos.aspx.vb"
    Inherits="profesor_CalificaDoctoAlumnos" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<%@ Register src="../Controls/msgSuccess.ascx" tagname="msgSuccess" tagprefix="uc1" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            width: 100%;
            height: 240px;
        }

        .style30 {
            height: 24px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
            font-weight: bold;
        }

        .style24 {
            height: 107px;
        }

        .style21 {
            height: 2px;
        }

        .style20 {
            height: 6px;
        }

        .style31 {
            width: 155px;
            height: 24px;
        }

        .style32 {
            width: 700px;
        }

        .style34 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
        }

        .style38 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
        }

        .style40 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 84px;
            text-align: right;
            color: #000066;
        }

        .style41 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            color: #000066;
        }

        .style42 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 490px;
        }

        .style43 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            color: #000066;
            width: 128px;
        }

        .style44 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 57px;
        }

        .style45 {
            height: 20px;
        }

        .style46 {
            width: 630px;
            font-weight: normal;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Evaluaciones - Calificar Documento
    </h1>
    Permite calificar una actividad de Entrega de Archivo de 
                    los 
												<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    de sus 
												<asp:Label ID="Label2" runat="server" Text="[GRUPOS]" />
    asignados como 
												<asp:Label ID="Label3" runat="server" Text="[PROFESOR]" />.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style11">
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style20" colspan="4">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td class="style30" colspan="4">
                <asp:Panel ID="PnlCalifica" runat="server" BackColor="#CCCCCC" Visible="False">
                    <table align="center" class="style32" border="1">
                        <tr>
                            <td class="style43" style="background-color: white;">
                                <asp:Label ID="Label4" runat="server" Text="[ALUMNO]" />
                                seleccionad<asp:Label ID="Label6" runat="server" Text="[O]" /></td>
                            <td class="style38" colspan="2" style="background-color: white;">
                                <asp:Label ID="LblAlumno" runat="server" Style="font-weight: 700"></asp:Label>
                            </td>
                            <td class="style34" style="background-color: white;">
                                <asp:ImageButton ID="IBmensaje" runat="server"
                                    AlternateText="Enviar mensaje al alumno"
                                    DescriptionUrl="Enviar mensaje al alumno" ImageUrl="~/Resources/imagenes/mensaje.jpg"
                                    Style="text-align: right" />
                            </td>
                        </tr>
                        <tr>
                            <td class="style43" style="background-color: white;">
                                <asp:Label ID="Label5" runat="server" Text="[ASIGNATURA]" />
                            </td>
                            <td class="style38" colspan="3" style="background-color: white;">
                                <asp:Label ID="LblAsignatura" runat="server" Style="font-weight: 700"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style43" style="background-color: white;">Actividad</td>
                            <td class="style38" colspan="3" style="background-color: white;">
                                <asp:Label ID="LblActividad" runat="server" Style="font-weight: 700"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style43" style="background-color: white;">Calificación </td>
                            <td class="style38" colspan="3" style="background-color: white;">
                                <asp:Label ID="LblCalificacion" runat="server" Style="font-weight: 700"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style43" style="background-color: white;">Archivo entregado</td>
                            <td class="style42" style="background-color: white;">
                                <asp:HyperLink ID="HLarchivo" runat="server"
                                    Style="font-weight: 700; font-size: medium;" Target="_blank">[HLarchivo]</asp:HyperLink>
                            </td>
                            <td class="style40" style="background-color: white;">Calificación</td>
                            <td class="style44" style="background-color: white;">
                                <asp:DropDownList ID="DDLcalifica" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style41" colspan="3" style="background-color: white;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td>
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <asp:TextBox ID="tbRetro" runat="server" TextMode="MultiLine" Width="425" Rows="5"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left;">
                                                        <asp:Button ID="btnGuardaRetro" runat="server" 
                                                            CssClass="defaultBtn btnThemeGrey btnThemeSlick" Text="Guardar Retroalimentación" />
                                                        <br />
                                                        <br />
                                                        <asp:Button ID="btnEliminaRetro" runat="server" 
                                                            CssClass="defaultBtn btnThemeGrey btnThemeSlick" Text="Eliminar Retroalimentación" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="style44" style="vertical-align: top;">
                                <asp:Button ID="BtnGuardar" runat="server" Text="Guardar Calificación"
                                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                <br />
                                <br />
                                <asp:Button ID="BtnQuitar" runat="server" 
                                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" Text="Quitar Calificación" />
                            </td>
                        </tr>
                        <tr>
                            <td class="style41" colspan="4">
                                <asp:GridView ID="GValumnos0" runat="server" 
                                    AllowSorting="True"
                                    AutoGenerateColumns="False" 
                                    Caption="APOYOS PARA LA ACTIVIDAD"

                                    DataSourceID="SDSapoyos" 
                                    Width="618px"

                                    CssClass="dataGrid_clear"
                                    GridLines="None">
                                    <Columns>
                                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                                            SortExpression="Consecutivo">
                                            <HeaderStyle Width="60px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripción"
                                            SortExpression="Descripcion">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ArchivoApoyo" HeaderText="ArchivoApoyo"
                                            SortExpression="ArchivoApoyo" Visible="False" />
                                        <asp:HyperLinkField DataNavigateUrlFields="ArchivoApoyo"
                                            DataNavigateUrlFormatString="~/Resources/apoyo/{0}" DataTextField="ArchivoApoyo"
                                            HeaderText="Archivo" Target="_blank" ItemStyle-CssClass="columnaHyperlink">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:HyperLinkField>
                                    </Columns>
                                    <FooterStyle CssClass="footer" />
                                    <PagerStyle CssClass="pager" />
                                    <SelectedRowStyle CssClass="selected" />
                                    <HeaderStyle CssClass="header" />
                                    <AlternatingRowStyle CssClass="altrow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style30" colspan="4">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style24" colspan="4">
                <asp:GridView ID="GVdatos" runat="server"
                    AutoGenerateColumns="False"
                    AllowSorting="True"

                    DataKeyNames="IdAlumno,Login, Retroalimentacion"
                    DataSourceID="SDSalumnosterminaron" 
                    Width="883px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno"
                            Visible="False" />
                        <asp:BoundField DataField="Login" HeaderText="Usuario" SortExpression="Login"
                            Visible="False" />
                        <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                            SortExpression="Matricula" />
                        <asp:BoundField DataField="Nombre Alumno" HeaderText="Nombre del [ALUMNO]"
                            ReadOnly="True" SortExpression="Nombre Alumno" />
                        <asp:BoundField DataField="Grupo" HeaderText="[GRUPO]" SortExpression="Grupo" />
                        <asp:BoundField DataField="Calificacion" HeaderText="Resultado"
                            SortExpression="Calificacion">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Documento" HeaderText="Archivo Entregado"
                            SortExpression="Documento" />
                        <asp:BoundField DataField="FechaEntregado" HeaderText="Fecha de Entrega" SortExpression="FechaEntregado"/>
                        <asp:BoundField DataField="Equipo" HeaderText="[EQUIPO]" SortExpression="Equipo" />
                        <asp:BoundField DataField="Retroalimentacion" HeaderText="Retroalimentación" SortExpression="Retroalimentacion" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- 
                    sort YES - rowdatabound
                    --%>
            </td>
        </tr>
        <tr>
            <td class="style21" colspan="2">
                &nbsp;</td>
            <td class="style21" colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style20" colspan="4">
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/profesor/CalificaDocumento.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSalumnosterminaron" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select A.IdAlumno, U.Login, A.Matricula, A.ApePaterno + ' ' + A.ApeMaterno + ', ' + A.Nombre as 'Nombre Alumno',
    DE.FechaEntregado, DE.Retroalimentacion, G.Descripcion as Grupo, DE.Calificacion, DE.Documento, A.Equipo
from Alumno A
join Usuario U on U.IdUsuario = A.IdUsuario
left join DoctoEvaluacion DE on DE.IdEvaluacion = @IdEvaluacion and A.IdAlumno = DE.IdAlumno and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido')
join Grupo G on G.IdGrupo = A.IdGrupo and G.IdPlantel = @IdPlantel and G.IdGrupo = @IdGrupo
order by G.Descripcion, A.ApePaterno, A.ApeMaterno, A.Nombre">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdEvaluacion" SessionField="IdEvaluacion" />
                        <asp:SessionParameter DefaultValue="" Name="IdPlantel"
                            SessionField="IdPlantel" />
                        <asp:SessionParameter Name="IdGrupo" SessionField="IdGrupo" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSapoyos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Distinct Consecutivo,Descripcion,ArchivoApoyo from
ApoyoEvaluacion
where IdProgramacion = @IdProgramacion and
IdEvaluacion = @IdEvaluacion
order by Consecutivo">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdProgramacion" SessionField="IdProgramacion" />
                        <asp:SessionParameter Name="IdEvaluacion" SessionField="IdEvaluacion" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSdoctoevaluacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [DoctoEvaluacion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluacionterminada" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [EvaluacionTerminada]"></asp:SqlDataSource>

            </td>
        </tr>
    </table>
</asp:Content>

