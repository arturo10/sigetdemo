﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="LeerMensajeEntradaP.aspx.vb"
    Inherits="profesor_LeerMensajeEntradaP"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            height: 34px;
        }

        .style33 {
            width: 10px;
            font-weight: normal;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Comunicación - Leer Mensajes Recibidos
    </h1>
    Permite revisar los mensajes recibidos de sus 
												<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    y 
												<asp:Label ID="Label2" runat="server" Text="[COORDINADORES]" />.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td colspan="2">
                <asp:GridView ID="GVmensajes" runat="server"
                    AllowSorting="True"
                    AutoGenerateColumns="False"
                    Caption="<h3>Mensajes de Alumnos</h3>"

                    DataKeyNames="IdComunicacion,Login"
                    DataSourceID="SDSmensajes"
                    Width="882px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdComunicacion" HeaderText="IdComunicacion"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdComunicacion"
                            Visible="False" />
                        <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha" />
                        <asp:BoundField DataField="Asunto" HeaderText="Asunto"
                            SortExpression="Asunto" />
                        <asp:BoundField DataField="Alumno que Envía" HeaderText="Alumno que Envía"
                            ReadOnly="True" SortExpression="Alumno que Envía" />
                        <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"
                            SortExpression="Asignatura" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                            SortExpression="Estatus" />
                        <asp:BoundField DataField="Login" HeaderText="Login" SortExpression="Login"
                            Visible="False" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  select
	                1  idcomunicacion
	                2  fecha
	                3  asunto
	                4  ALUMNO QUE ENVIA
	                5  ASIGNATURA
	                6  estatus
	                7  login
                --%>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GVmensajesC" runat="server"
                    AllowSorting="True"
                    AutoGenerateColumns="False"
                    Caption="<h3>Mensajes de Coordinadores</h3>"

                    DataSourceID="SDSmensajes2"
                    DataKeyNames="IdComunicacionCP,Login"
                    Width="882px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" >
                            <HeaderStyle Width="90px" />
                        </asp:CommandField>
                        <asp:BoundField DataField="IdComunicacionCP" HeaderText="IdComunicacionCP" SortExpression="IdComunicacionCP"
                            Visible="False" InsertVisible="False" ReadOnly="True" />
                        <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha">
                            <HeaderStyle Width="130px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Asunto" HeaderText="Asunto"
                            SortExpression="Asunto" />
                        <asp:BoundField DataField="Coordinador que Envía" HeaderText="Coordinador que Envía"
                            ReadOnly="True" SortExpression="Coordinador que Envía" />
                        <asp:BoundField DataField="Plantel" HeaderText="Plantel"
                            SortExpression="Plantel" />
                        <asp:BoundField DataField="EstatusP" HeaderText="Estatus"
                            SortExpression="EstatusP" />
                        <asp:BoundField DataField="Login" HeaderText="Login" SortExpression="Login"
                            Visible="False" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  select
	                1  idcomunicacioncp
	                2  fecha
	                3  asunto
	                4  COORDINADOR QUE ENVIA
	                5  PLANTEL
	                6  estatus
	                7  login
                --%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSmensajes" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select C.IdComunicacion, C.Fecha, C.Asunto, A.Nombre + ' ' + A.ApePaterno + ' ' + A.ApeMaterno as 'Alumno que Envía', M.Descripcion as Asignatura, C.Estatus, U.Login
from Comunicacion C, Alumno A, Programacion P, Asignatura M, Usuario U
where P.IdProfesor = @IdProfesor and C.IdProgramacion = P.IdProgramacion
and M.IdAsignatura = P.IdAsignatura and A.IdAlumno = C.IdAlumno
and C.Estatus &lt;&gt; 'Baja' and C.Sentido = 'R' and U.IdUsuario = A.IdUsuario
order by C.Fecha DESC">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdProfesor" SessionField="IdProfesor" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSmensajes2" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select C.IdComunicacionCP, C.Fecha, C.Asunto, Coord.Nombre + ' ' + Coord.Apellidos as 'Coordinador que Envía', Pl.Descripcion as Plantel, C.EstatusP, U.Login
from ComunicacionCP C, Profesor P, Coordinador Coord, Usuario U, Plantel Pl
where P.IdProfesor = (select IdProfesor from Profesor,Usuario where Usuario.Login=@Login and Profesor.IdUsuario=Usuario.Idusuario)
and C.IdProfesor = P.IdProfesor and Coord.IdCoordinador =  C.IdCoordinador and U.IdUsuario = Coord.IdUsuario
and C.EstatusP &lt;&gt; 'Baja' and C.Sentido = 'E' and Pl.IdPlantel = P.IdPlantel
order by C.Fecha DESC">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>

