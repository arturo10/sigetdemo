﻿Imports Siget

Imports System.IO
Imports System.Data.SqlClient

Partial Class profesor_CalificaDoctoAlumnos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.GRUPOS
        Label3.Text = Config.Etiqueta.PROFESOR
        Label4.Text = Config.Etiqueta.ALUMNO
        Label5.Text = Config.Etiqueta.ASIGNATURA
        Label6.Text = Config.Etiqueta.LETRA_ALUMNO

        If Not IsPostBack Then
      If Config.Global.PROFESOR_CALIFICA_DOCUMENTO = 0 Then
        DDLcalifica.Items.Add(New ListItem("100", 100))
        DDLcalifica.Items.Add(New ListItem("95", 95))
        DDLcalifica.Items.Add(New ListItem("90", 90))
        DDLcalifica.Items.Add(New ListItem("85", 85))
        DDLcalifica.Items.Add(New ListItem("80", 80))
        DDLcalifica.Items.Add(New ListItem("75", 75))
        DDLcalifica.Items.Add(New ListItem("70", 70))
        DDLcalifica.Items.Add(New ListItem("65", 65))
        DDLcalifica.Items.Add(New ListItem("60", 60))
        DDLcalifica.Items.Add(New ListItem("55", 55))
        DDLcalifica.Items.Add(New ListItem("50", 50))
        DDLcalifica.Items.Add(New ListItem("45", 45))
        DDLcalifica.Items.Add(New ListItem("40", 40))
        DDLcalifica.Items.Add(New ListItem("35", 35))
        DDLcalifica.Items.Add(New ListItem("30", 30))
        DDLcalifica.Items.Add(New ListItem("25", 25))
        DDLcalifica.Items.Add(New ListItem("20", 20))
        DDLcalifica.Items.Add(New ListItem("15", 15))
        DDLcalifica.Items.Add(New ListItem("10", 10))
        DDLcalifica.Items.Add(New ListItem("5", 5))
        DDLcalifica.Items.Add(New ListItem("0", 0))
      ElseIf Config.Global.PROFESOR_CALIFICA_DOCUMENTO = 1 Then
        DDLcalifica.Items.Add(New ListItem("Aún NO competente", 50))
        DDLcalifica.Items.Add(New ListItem("Competente", 100))
      End If
    End If
  End Sub

  Protected Sub GVdatos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVdatos.DataBound
    GVdatos.Caption = "<font size=""2""><b>" + Session("Titulo") + "</b></font>"
  End Sub

  Protected Sub GVdatos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVdatos.RowDataBound
    '1)PRIMERO OBTENGO LOS RANGOS DE LOS INDICADORES PARA EL SEMAFORO ROJO, AMARILLO Y VERDE
    Dim strConexion As String
    'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
    strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
    Dim objConexion As New SqlConnection(strConexion)
    Dim miComando As SqlCommand
    Dim misRegistros As SqlDataReader

    miComando = objConexion.CreateCommand
    'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
    miComando.CommandText = "select I.MinAmarillo, I.MinVerde, I.MinAzul from Indicador I, CicloEscolar C where " + _
                            "I.IdIndicador = C.IdIndicador and C.IdCicloEscolar = " + Session("IdCicloEscolar").ToString
    Dim MinAmarillo, MinVerde, MinAzul As Decimal
    Try
      objConexion.Open()
      misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
      misRegistros.Read()
      MinAmarillo = misRegistros.Item("MinAmarillo")
      MinVerde = misRegistros.Item("MinVerde")
      MinAzul = misRegistros.Item("MinAzul")
      misRegistros.Close()
      objConexion.Close()
      msgError.hide()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
    End Try

    '2)HAGO UN CICLO QUE VAYA DE 1 AL TOTAL DE COLUMNAS (EL INDICE DE LAS CELDAS(COLUMNAS) INICIA EN 0 (CELLS(0)= )
    'La parte de la condición: (e.Row.RowIndex > -1) es porque en este caso que comparo fechas estaba pintando tambien el encabezado (fila -1), que tambien comparaba
    If (e.Row.Cells.Count > 1) And (e.Row.RowIndex > -1) Then  'Para que se realice la comparacion cuando haya mas de una columna
      Dim I = 4 'Este es el número de columna del Grid que compara con los indicadores
      If IsNumeric(Trim(e.Row.Cells(I).Text)) Then 'para que no compare celdas vacias
        If (CDbl(e.Row.Cells(I).Text) < MinAmarillo) Then
          e.Row.Cells(I).BackColor = Drawing.Color.LightSalmon
        ElseIf (CDbl(e.Row.Cells(I).Text) < MinVerde) Then
          e.Row.Cells(I).BackColor = Drawing.Color.Yellow
        ElseIf (CDbl(e.Row.Cells(I).Text) < MinAzul) Then
          e.Row.Cells(I).BackColor = Drawing.Color.LightGreen
        Else 'Significa que es mayor o igual al MinAzul:
          e.Row.Cells(I).BackColor = Drawing.Color.LightBlue
        End If
      End If
    End If

    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(4).Controls(1)
      LnkHeaderText.Text = "Nombre de " & Config.Etiqueta.ARTDET_ALUMNO & " " & Config.Etiqueta.ALUMNO

      LnkHeaderText = e.Row.Cells(5).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.GRUPO

      LnkHeaderText = e.Row.Cells(9).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.EQUIPO
    End If

    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVdatos, "Select$" & e.Row.RowIndex).ToString())

      ' convierto el campo de retroalimentacion de modo que se presente con estilos, no como markup
      Dim s As String = Regex.Replace(HttpUtility.HtmlDecode(e.Row.Cells(10).Text), "<[^>]*(>|$)", String.Empty)
      Dim l As Integer = s.Length
      Dim continua As String = ""
      If l > 100 Then
        l = 100
        continua = "...(Continúa)"
      End If
      e.Row.Cells(10).Text = s.Substring(0, l) & continua
    End If
  End Sub

  Protected Sub GVdatos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVdatos.SelectedIndexChanged
    msgSuccess.hide()
    PnlCalifica.Visible = True
    LblAlumno.Text = GVdatos.SelectedRow.Cells(4).Text
    LblAsignatura.Text = Session("Asignatura")
    LblActividad.Text = Session("Evaluacion")
    tbRetro.Text = GVdatos.SelectedDataKey(2).ToString()

    LblCalificacion.Text = Trim(GVdatos.SelectedRow.Cells(6).Text)
    HLarchivo.Text = HttpUtility.HtmlDecode(GVdatos.SelectedRow.Cells(7).Text)
    'El directorio donde graba cada alumno tiene como nombre su Login
    HLarchivo.NavigateUrl = Config.Global.urlCargas & GVdatos.SelectedDataKey.Values(1).ToString + "/" + HttpUtility.HtmlDecode(GVdatos.SelectedRow.Cells(7).Text)
    LblCalificacion.Text = Trim(GVdatos.SelectedRow.Cells(6).Text)
    If Trim(GVdatos.SelectedRow.Cells(6).Text).Length <= 3 Then 'Si es mayor a 3 es que no tiene nada el campo
      Try
        DDLcalifica.SelectedValue = GVdatos.SelectedRow.Cells(6).Text
      Catch ex As Exception
        Utils.LogManager.ExceptionLog_InsertEntry(ex)
      End Try
    End If

    'ALGORITMO PARA BUSCAR EN DOCTOEVALUACION SI YA EXISTÍAN LOS DATOS DE CALIFICACION AL DOCUMENTO

    'BUSCO APOYOS:
    'Verifico si la actividad seleccionada tiene material de apoyo
    Dim strConexion As String
    strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
    Dim objConexion As New SqlConnection(strConexion)
    Dim miComando As SqlCommand
    Dim misRegistros As SqlDataReader
    miComando = objConexion.CreateCommand
    miComando.CommandText = "select Distinct IdProgramacion,IdEvaluacion from ApoyoEvaluacion where IdProgramacion in (select distinct IdProgramacion from Programacion where IdAsignatura = " + _
        Session("IdAsignatura").ToString + " and IdCicloEscolar = " + Session("IdCicloEscolar").ToString + " and IdGrupo = " + _
        Session("IdGrupo").ToString + ") and IdEvaluacion = " + Session("IdEvaluacion").ToString
    Try
      objConexion.Open()
      misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
      If misRegistros.Read() Then
        Session("IdProgramacion") = misRegistros.Item("IdProgramacion")
        Session("IdEvaluacion") = misRegistros.Item("IdEvaluacion")
        misRegistros.Close()
        objConexion.Close()
      Else
        misRegistros.Close()
        objConexion.Close()
      End If
      msgError.hide()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try

  End Sub

  Protected Sub BtnGuardar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGuardar.Click
    msgSuccess.hide()
    If GVdatos.SelectedRow.Cells(7).Text = "&nbsp;" Then
      msgError.show("No puede calificar si no hay archivo entregado.")
    Else
      msgError.hide()
      Dim strConexion As String
      'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
      strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
      Dim objConexion As New SqlConnection(strConexion)
      Dim miComando As SqlCommand
      Dim misRegistros As SqlDataReader

      miComando = objConexion.CreateCommand
      miComando.CommandText = "select * from EvaluacionTerminada where IdEvaluacion = " + Session("IdEvaluacion").ToString + " and IdAlumno = " + GVdatos.SelectedDataKey.Values(0).ToString
      Try
        'Abrir la conexión y leo los registros del Planteamiento
        objConexion.Open()
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros            
        If misRegistros.Read() Then 'Leo para poder accesarlos
          SDSevaluacionterminada.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionTerminada set Resultado = " + DDLcalifica.SelectedValue.ToString + ", FechaTermino = cast(getdate() as smalldatetime) where IdEvaluacionT = " + misRegistros.Item("IdEvaluacionT").ToString
          SDSevaluacionterminada.Update()
        Else
          misRegistros.Close()
          miComando.CommandText = "select * from DetalleEvaluacion where IdEvaluacion = " + Session("IdEvaluacion").ToString
          misRegistros = miComando.ExecuteReader()
          misRegistros.Read()

          SDSevaluacionterminada.InsertCommand = "SET dateformat dmy; INSERT INTO EvaluacionTerminada(IdAlumno,IdGrado,IdGrupo,IdCicloEscolar,IdEvaluacion,TotalReactivos,ReactivosContestados,PuntosPosibles,PuntosAcertados,Resultado,FechaTermino) " + _
                          "VALUES(" + GVdatos.SelectedDataKey.Values(0).ToString + "," + Session("IdGrado").ToString + "," + _
                              Session("IdGrupo").ToString + "," + Session("IdCicloEscolar") + "," + _
                              Session("IdEvaluacion").ToString + ",0,0,100," + DDLcalifica.SelectedValue.ToString + "," + DDLcalifica.SelectedValue.ToString + ",cast(getdate() as smalldatetime))"
          SDSevaluacionterminada.Insert()

        End If
        'Ahora pongo la calificacion en el registro de DoctoEvaluacion (que debe existir)
        SDSdoctoevaluacion.UpdateCommand = "SET dateformat dmy; UPDATE DoctoEvaluacion set Calificacion = " + DDLcalifica.SelectedValue.ToString + " where IdEvaluacion = " + Session("IdEvaluacion").ToString + " and IdAlumno = " + GVdatos.SelectedDataKey.Values(0).ToString
        SDSdoctoevaluacion.Update()
        misRegistros.Close()
        objConexion.Close()
        GVdatos.DataBind()
        LblCalificacion.Text = Trim(GVdatos.SelectedRow.Cells(6).Text)
        msgSuccess.show("Éxito", "Se ha guardado la calificación. El archivo que acaba Ud. de calificar ya no puede ser modificado. Si requiere hacerlo, deberá QUITAR la calificación")
      Catch ex As Exception
        Utils.LogManager.ExceptionLog_InsertEntry(ex)
        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      End Try
    End If
  End Sub

  Protected Sub IBmensaje_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles IBmensaje.Click
    Dim strConexion As String
    'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
    strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
    Dim objConexion As New SqlConnection(strConexion)
    Dim miComando As SqlCommand
    Dim misRegistros As SqlDataReader
    Try
      miComando = objConexion.CreateCommand
      'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
      miComando.CommandText = "select IdProgramacion from Programacion where IdProfesor = (select P.IdProfesor from Profesor P, Usuario U where U.Login = '" + _
                      Session("Login") + "' and P.IdUsuario = U.Idusuario) " + _
                      " and IdAsignatura = " + Session("IdAsignatura").ToString + " and IdCicloEscolar = " + Session("IdCicloEscolar").ToString
      objConexion.Open()
      misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
      misRegistros.Read() 'Leo para poder accesarlo
      'Esta variable la necesito por si quiero mandar un mensaje al alumno
      Session("IdProgramacion") = misRegistros.Item("IdProgramacion")
      misRegistros.Close()
      objConexion.Close()
      Session("IdAlumno") = GVdatos.SelectedDataKey.Values(0).ToString
      Session("NomAlumno") = GVdatos.SelectedRow.Cells(4).Text
      Session("Regresar") = "CalificaDoctoAlumnos.aspx"
      Response.Redirect("EnviarMensajeP2.aspx")
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub BtnQuitar_Click(sender As Object, e As EventArgs) Handles BtnQuitar.Click
    msgSuccess.hide()
    If GVdatos.SelectedRow.Cells(6).Text = "&nbsp;" Then
      msgError.show("No ha calificado aún este documento.")
    Else
      msgError.hide()

      Dim strConexion As String
      'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
      strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
      Dim objConexion As New SqlConnection(strConexion)
      Dim miComando As SqlCommand
      Dim misRegistros As SqlDataReader

      miComando = objConexion.CreateCommand
      miComando.CommandText = "select * from EvaluacionTerminada where IdEvaluacion = " + Session("IdEvaluacion").ToString + " and IdAlumno = " + GVdatos.SelectedDataKey.Values(0).ToString
      Try
        'Abrir la conexión y leo los registros del Planteamiento
        objConexion.Open()
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros            
        If misRegistros.Read() Then 'Leo para poder accesarlos
          SDSevaluacionterminada.DeleteCommand = "DELETE FROM EvaluacionTerminada where IdEvaluacionT = " + misRegistros.Item("IdEvaluacionT").ToString
          SDSevaluacionterminada.Delete()
        End If
        'Ahora pongo la calificacion en el registro de DoctoEvaluacion (que debe existir)
        SDSdoctoevaluacion.UpdateCommand = "SET dateformat dmy; UPDATE DoctoEvaluacion set Calificacion = NULL where IdEvaluacion = " + Session("IdEvaluacion").ToString + " and IdAlumno = " + GVdatos.SelectedDataKey.Values(0).ToString
        SDSdoctoevaluacion.Update()
        misRegistros.Close()
        objConexion.Close()
        GVdatos.DataBind()
        LblCalificacion.Text = ""
        msgSuccess.show("Éxito", "Acaba de quitar la calificación al documento, por tanto, ya puede ser modificado.")
      Catch ex As Exception
        Utils.LogManager.ExceptionLog_InsertEntry(ex)
        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      End Try
    End If
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GVdatos_PreRender(sender As Object, e As EventArgs) Handles GVdatos.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVdatos.Rows.Count > 0 Then
      GVdatos.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVdatos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVdatos.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVdatos.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVdatos, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    For Each r In GValumnos0.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GValumnos0, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

  Protected Sub GValumnos0_PreRender(sender As Object, e As EventArgs) Handles GValumnos0.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GValumnos0.Rows.Count > 0 Then
      GValumnos0.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GValumnos0_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GValumnos0.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GValumnos0.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

    Protected Sub btnGuardaRetro_Click(sender As Object, e As EventArgs) Handles btnGuardaRetro.Click
        'Hago consulta para sacar los datos del mensaje
        Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
            conn.Open()

            Dim cmd As SqlCommand = New SqlCommand("UPDATE DoctoEvaluacion SET Retroalimentacion = @Retroalimentacion WHERE IdEvaluacion = @IdEvaluacion and IdAlumno = @IdAlumno", conn)
            cmd.Parameters.AddWithValue("@Retroalimentacion", tbRetro.Text.Trim())
            cmd.Parameters.AddWithValue("@IdEvaluacion", Session("IdEvaluacion").ToString)
            cmd.Parameters.AddWithValue("@IdAlumno", GVdatos.SelectedDataKey.Values(0).ToString)
            cmd.ExecuteNonQuery()

            GVdatos.DataBind()
            msgSuccess.show("Éxito", "Se guardó la retroalimentación.")

            cmd.Dispose()
            conn.Close()
        End Using
    End Sub

    Protected Sub btnEliminaRetro_Click(sender As Object, e As EventArgs) Handles btnEliminaRetro.Click
        'Hago consulta para sacar los datos del mensaje
        Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
            conn.Open()

            Dim cmd As SqlCommand = New SqlCommand("UPDATE DoctoEvaluacion SET Retroalimentacion = NULL WHERE IdEvaluacion = @IdEvaluacion and IdAlumno = @IdAlumno", conn)
            cmd.Parameters.AddWithValue("@IdEvaluacion", Session("IdEvaluacion").ToString)
            cmd.Parameters.AddWithValue("@IdAlumno", GVdatos.SelectedDataKey.Values(0).ToString)
            cmd.ExecuteNonQuery()

            GVdatos.DataBind()
            msgSuccess.show("Éxito", "Se eliminó la retroalimentación.")

            cmd.Dispose()
            conn.Close()
        End Using
    End Sub

End Class
