﻿Imports Siget

Imports System.Data.SqlClient

Partial Class capturista_FormatoCaptura
    Inherits System.Web.UI.Page

    Public Class MisVariablesGlobales
        'EJEMPLO: Public Shared MiGlobal As String = "Valor Fijo"
        Public Shared EsAbierta As Boolean
        Public Shared Calificacion As String
        Public Shared RespAbierta As String
    End Class

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.ALUMNO
        Label2.Text = Config.Etiqueta.ARTIND_ALUMNO
        TitleLiteral.Text = "Capturar Actividad"

        'Esto lo pongo aquí para cargarlo en el Boton Renunciar
        BtnTerminar.Attributes.Add("onclick", "return confirm('¿Está Ud. seguro que desea TERMINAR esta Actividad? Se calculará la calificación solo con los reactivos capturados.');")


        LblTitulo.Text = Session("Titulo")
        '1)Ciclo para agregar todos los reactivos (eleccion y abiertos) de la actividad, agregandolos por cada fila de una tabla
        '    Tengo que identificar cada uno de los ids de los reactivos y de las respuestas
        Dim strConexion As String
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misPlanteamientos As SqlDataReader
        miComando = objConexion.CreateCommand
        objConexion.Open()
        Dim Redaccion As String

        'Consulta para sacar todos los reactivos, se contestarán todos o ninguno 
        '(checar que la evaluacion no esté terminada para desplegarlos)
        miComando.CommandText = "select P.IdPlanteamiento, P.Consecutivo, P.Redaccion, P.TipoRespuesta " + _
                        "from Planteamiento P, DetalleEvaluacion D, Subtema S " + _
                        "where D.IdEvaluacion = " + Session("IdEvaluacion").ToString + " " + _
                        "and P.IdSubtema = D.IdSubtema and P.Ocultar = 0 and S.IdSubtema = D.IdSubtema " + _
                        "and D.IdEvaluacion not in " + _
                        "(select IdEvaluacion from EvaluacionTerminada where IdEvaluacion = D.IdEvaluacion and IdAlumno = " + _
                        Session("IdAlumno").ToString + ") order by S.Numero, P.Consecutivo"
        misPlanteamientos = miComando.ExecuteReader()   'Creo conjunto de registros
        'misPlanteamientos.Read()

        Dim objConexion2 As New SqlConnection(strConexion)
        Dim miComando2 As SqlCommand
        Dim misOpciones As SqlDataReader
        miComando2 = objConexion2.CreateCommand
        objConexion2.Open()
        If misPlanteamientos.HasRows Then
            msgInfo.hide()
            'Ciclo para ir avazando reactivo por reactivo y sacando sus opciones o textbox para respuesta abierta y agregándolos al panel
            PnlCaptura.Controls.Add(New LiteralControl("<Table align='left' border='1' bordercolor = '#CCCCCC'>")) 'Agrego el inicio de la Tabla
            Dim Cont As Byte 'Esta variable la usaré para pintar los renglones
            Cont = 0
            Dim color As String
            Do While misPlanteamientos.Read()
                Cont += 1
                If Cont Mod 2 = 0 Then
                    color = "#E3EAEB"
                Else
                    color = "#CCCCCC"
                End If
                Dim Etiq1 As Label = New Label 'AQUI DEBE DE IR PARA QUE SE REGENERE Y SE AGREGUE COMO NUEVA EN EL PANEL

                'Agrego al panel el Planteamiento y las opciones múltiples
                Etiq1.ID = "Pla" + misPlanteamientos.Item("IdPlanteamiento").ToString   'Le Pongo nombre al Planteamiento

                Redaccion = Replace(HttpUtility.HtmlDecode(misPlanteamientos.Item("Redaccion")), "*salto*", "<BR>")
                Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")

                Etiq1.Text = "<font face = 'Arial' size='2'>" + misPlanteamientos.Item("Consecutivo").ToString + ".-" + Redaccion + "</font>"
                PnlCaptura.Controls.Add(New LiteralControl("<tr  bgcolor = '" + color + "'><td width='40%'>"))
                PnlCaptura.Controls.Add(Etiq1)
                PnlCaptura.Controls.Add(New LiteralControl("</td>"))

                miComando2.CommandText = "select IdOpcion, Consecutiva, Redaccion, ArchivoApoyo, TipoArchivoApoyo, Correcta " + _
                                                "from Opcion where IdPlanteamiento = " + misPlanteamientos.Item("IdPlanteamiento").ToString + _
                                                " order by Consecutiva"
                misOpciones = miComando2.ExecuteReader

                If Trim(misPlanteamientos.Item("TipoRespuesta")) = "Multiple" Then
                    Do While misOpciones.Read()
                        Dim rb1 As Control = New RadioButton 'AQUI DEBE DE IR PARA QUE SE REGENERE Y SE AGREGUE COMO NUEVA EN EL PANEL
                        'REDACCION DEL PLANTEAMIENTO Y LAS OPCIONES
                        rb1.ID = "RB" + misOpciones.Item("IdOpcion").ToString

                        Redaccion = Replace(HttpUtility.HtmlDecode(misOpciones.Item("Redaccion")), "*salto*", "<BR>")
                        Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")

                        'no es necesario poner la redacción de la respuesta en la captura: CType(rb1, RadioButton).Text = "<font face = ""Arial"" size=""3"">" + misOpciones.Item("Consecutiva").ToString + ") " + misOpciones.Item("Redaccion").ToString + "</font>"
                        CType(rb1, RadioButton).Text = "<font face = ""Arial"" size=""2"">" + misOpciones.Item("Consecutiva").ToString + ")&nbsp;" + Redaccion + "</font>"
                        CType(rb1, RadioButton).GroupName = "Reac" + misPlanteamientos.Item("IdPlanteamiento").ToString

                        'ANTES: PnlCaptura.Controls.Add(New LiteralControl("<td width='6%'>"))
                        'Ahora para que al dar click sobre la celda se seleccione el Radiobutton:
                        PnlCaptura.Controls.Add(New LiteralControl("<td onclick='document.getElementById(""ctl00_Contenedor_" + rb1.ClientID + """).checked=true;'>"))
                        PnlCaptura.Controls.Add(rb1)
                        PnlCaptura.Controls.Add(New LiteralControl("</td>"))
                    Loop
                    PnlCaptura.Controls.Add(New LiteralControl("</tr>"))
                ElseIf Trim(misPlanteamientos.Item("TipoRespuesta")) = "Abierta" Then
                    misOpciones.Read() 'Los planteamientos de respuesta abierta tienen un registro en la tabla Opcion
                    Dim TB1 As New TextBox 'AQUI DEBE DE IR PARA QUE SE REGENERE Y SE AGREGUE COMO NUEVA EN EL PANEL
                    TB1.ID = "TB" + misOpciones.Item("IdOpcion").ToString
                    'TB1.width = 400 ESTO NO LO ACEPTA CUANDO DICE ...As Control ...
                    TB1.Width = 350
                    TB1.MaxLength = 400
                    PnlCaptura.Controls.Add(New LiteralControl("<td width='40%'>"))
                    PnlCaptura.Controls.Add(TB1)

                    PnlCaptura.Controls.Add(New LiteralControl("</td><td>"))
                    Dim DDL1 As New DropDownList
                    'DDL1.Items.Add(New ListItem("Correcta", "S"))
                    'DDL1.Items.Add(New ListItem("Incorrecta", "N"))
                    'DDL1.Items.Add(New ListItem("Media", "M"))
          If Config.Global.PROFESOR_CALIFICA_DOCUMENTO = 0 Then
            DDL1.Items.Add(New ListItem("100", 100))
            DDL1.Items.Add(New ListItem("95", 95))
            DDL1.Items.Add(New ListItem("90", 90))
            DDL1.Items.Add(New ListItem("85", 85))
            DDL1.Items.Add(New ListItem("80", 80))
            DDL1.Items.Add(New ListItem("75", 75))
            DDL1.Items.Add(New ListItem("70", 70))
            DDL1.Items.Add(New ListItem("65", 65))
            DDL1.Items.Add(New ListItem("60", 60))
            DDL1.Items.Add(New ListItem("55", 55))
            DDL1.Items.Add(New ListItem("50", 50))
            DDL1.Items.Add(New ListItem("45", 45))
            DDL1.Items.Add(New ListItem("40", 40))
            DDL1.Items.Add(New ListItem("35", 35))
            DDL1.Items.Add(New ListItem("30", 30))
            DDL1.Items.Add(New ListItem("25", 25))
            DDL1.Items.Add(New ListItem("20", 20))
            DDL1.Items.Add(New ListItem("15", 15))
            DDL1.Items.Add(New ListItem("10", 10))
            DDL1.Items.Add(New ListItem("5", 5))
            DDL1.Items.Add(New ListItem("0", 0))
          ElseIf Config.Global.PROFESOR_CALIFICA_DOCUMENTO = 1 Then
            'DDLcalifica.Items.Add(New ListItem("-", 100))
            DDL1.Items.Add(New ListItem("Aún NO competente", 50))
            DDL1.Items.Add(New ListItem("Competente", 100))
          End If
                    PnlCaptura.Controls.Add(DDL1)

                    PnlCaptura.Controls.Add(New LiteralControl("</td></tr>"))
                End If
                misOpciones.Close()
            Loop 'del While misPlanteamientos.Read()
            PnlCaptura.Controls.Add(New LiteralControl("</Table>"))
            msgError.hide()
        Else
            msgInfo.show("No existen reactivos para capturar.")
        End If
        misPlanteamientos.Close()
        objConexion.Close()
    End Sub

    Protected Sub BtnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAceptar.Click
        'Primero hago ciclo para asegurarme de que se hayan tecleado todas las respuestas
        Dim I = 0
        Dim ValorEncontrado = True 'Es necesario que inicie en True para que ingrese al ciclo y no se interrumpa de inmediato
        Dim DatosCompletos = True 'Inicia en verdadero, si sale del ciclo sin cambiar su valor es que todos los datos están completos

        msgError.hide()
        Do While (I < PnlCaptura.Controls.Count)
            If (PnlCaptura.Controls.Item(I).GetType.Name = "Label") And Not (ValorEncontrado) Then
                DatosCompletos = False
                Exit Do 'Interrumpe completamente el ciclo
            Else
                If (PnlCaptura.Controls.Item(I).GetType.Name = "Label") Then
                    ValorEncontrado = False
                End If
            End If

            If PnlCaptura.Controls.Item(I).GetType.Name = "TextBox" Then 'Este no importa que lo hayan dejado vacío
                ValorEncontrado = True
            Else
                If PnlCaptura.Controls.Item(I).GetType.Name = "RadioButton" Then
                    If CType(PnlCaptura.Controls.Item(I), RadioButton).Checked Then
                        ValorEncontrado = True
                    End If
                End If
            End If
            I = I + 1
        Loop

        If DatosCompletos And ValorEncontrado Then
            Dim Cont As Byte 'Esta variable la usaré para pintar los renglones
            Cont = 0
            Dim color As String
            I = 0
            'Los datos están completos, desplegaré la previsualización
            PnlRespuestas.Controls.Add(New LiteralControl("<Table align='left' border='1' bordercolor = '#CCCCCC'>")) 'Agrego el inicio de la Tabla
            PnlRespuestas.Controls.Add(New LiteralControl("<tr><td width='60%'><font face = 'Arial' size='2'><b>PLANTEAMIENTO</b></font></td><td width='40%'><font face = 'Arial' size='2'><b>RESPUESTA CAPTURADA</b></font></td></tr>"))
            Do While (I < PnlCaptura.Controls.Count)
                If (PnlCaptura.Controls.Item(I).GetType.Name = "Label") Then
                    'Lo siguiente debe quedar aquí adentro para que no cambie con la lectura de los radiobuttons o textbox
                    Cont += 1
                    If Cont Mod 2 = 0 Then
                        color = "#E3EAEB"
                    Else
                        color = "#CCCCCC"
                    End If

                    'Ahora inserto el planteamiento
                    Dim Etiq2 As Label = New Label 'AQUI DEBE DE IR PARA QUE SE REGENERE Y SE AGREGUE COMO NUEVA EN EL PANEL
                    Etiq2.ID = PnlCaptura.Controls.Item(I).ID 'Se compone de Pla + IdPlanteamiento
                    Etiq2.Text = "<font face = 'Arial' size='2'>" + CType(PnlCaptura.Controls.Item(I), Label).Text + "</font>"
                    PnlRespuestas.Controls.Add(New LiteralControl("<tr  bgcolor = '" + color + "'><td width='60%'>"))
                    PnlRespuestas.Controls.Add(Etiq2)
                    PnlRespuestas.Controls.Add(New LiteralControl("</td>"))

                ElseIf PnlCaptura.Controls.Item(I).GetType.Name = "TextBox" Then
                    Dim Etiq4 As Label = New Label 'AQUI DEBE DE IR PARA QUE SE REGENERE Y SE AGREGUE COMO NUEVA EN EL PANEL
                    Etiq4.ID = PnlCaptura.Controls.Item(I).ID 'Se compone de TB + IdOpcion
                    Etiq4.Text = CType(PnlCaptura.Controls.Item(I), TextBox).Text
                    PnlRespuestas.Controls.Add(New LiteralControl("<td width='40%'>"))
                    PnlRespuestas.Controls.Add(Etiq4)

                    PnlRespuestas.Controls.Add(New LiteralControl("</td><td>"))
                    Dim Etiq4cal As Label = New Label
                    Etiq4cal.ID = PnlCaptura.Controls.Item(I).ID + "cal" 'Se compone de TB + IdOpcion
                    Etiq4cal.Text = CType(PnlCaptura.Controls.Item(I + 2), DropDownList).SelectedValue
                    PnlRespuestas.Controls.Add(Etiq4cal)

                    PnlRespuestas.Controls.Add(New LiteralControl("</td></tr>")) 'Aquí va porque cuando ponga respuesta, cambia planteamiento
                ElseIf PnlCaptura.Controls.Item(I).GetType.Name = "RadioButton" Then
                    If CType(PnlCaptura.Controls.Item(I), RadioButton).Checked Then
                        Dim Etiq3 As Label = New Label 'AQUI DEBE DE IR PARA QUE SE REGENERE Y SE AGREGUE COMO NUEVA EN EL PANEL
                        Etiq3.ID = PnlCaptura.Controls.Item(I).ID 'Se compone de RB + IdOpcion
                        Etiq3.Text = CType(PnlCaptura.Controls.Item(I), RadioButton).Text
                        PnlRespuestas.Controls.Add(New LiteralControl("<td width='40%'>"))
                        PnlRespuestas.Controls.Add(Etiq3)
                        PnlRespuestas.Controls.Add(New LiteralControl("</td></tr>")) 'Aquí va porque cuando ponga respuesta, cambia planteamiento
                    End If
                End If

                I += 1
            Loop

            PnlRespuestas.Controls.Add(New LiteralControl("</Table>")) 'Agrego el final de la Tabla
            msgInfo.show("REVISE LAS RESPUESTAS CAPTURADAS ANTES DE ACEPTAR LA CAPTURA")
            BtnAceptarResp.Visible = True
            LblM.Visible = True
        Else
            msgError.show("Omitió la respuesta de alguno(s) de los reactivos, revise cuidadosamente y complete la captura.")
        End If
    End Sub

    Protected Sub BtnAceptarResp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAceptarResp.Click
        'Aquí coloco algoritmo para insertar respuestas en la base de datos (Utilizar el Panel PnlCaptura porque el segundo
        'desaparece antes de que lo pueda leer)  Plaxxxxx, RBxxxx, TBxxxxx, y tener en cuenta el IdEvaluacion y IdAlumno
        'Y por tanto, no se debe modificar los elementos de PnlCaptura si se van a aceptar las respuestas (pornerlo flotante luego)

        Dim IdOpcion As String = "",
            Valor As String = "",
            Acertada As String = "",
            IdGrado As String = "",
            IdGrupo As String = "",
            IdPlanteamiento As String = "",
            IdCicloEscolar As String = "",
            IdDetalleEvaluacion As String = ""
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        miComando = objConexion.CreateCommand
        objConexion.Open()
        'De script anterior vienen: Session("IdEvaluacion") y Session("IdAlumno")

        Dim I = 0
        Do While (I < PnlCaptura.Controls.Count) 'Este ciclo recorre todos los elementos del Panel, por tanto, cada que obtenga un par de IdPlanteamiento e IdOpcion, debo guardarlos
            IdOpcion = ""  'Esto es necesario para que se limpie la opción en cada vuelta
            Session("RespAbierta") = "" 'Porque si no entraría de nuevo al SDSrespuestasabiertas_Inserted
            If (PnlCaptura.Controls.Item(I).GetType.Name = "Label") Then
                IdPlanteamiento = Mid(PnlCaptura.Controls.Item(I).ID, 4, Len(PnlCaptura.Controls.Item(I).ID) - 3) 'Pla[####]                
            ElseIf (PnlCaptura.Controls.Item(I).GetType.Name = "RadioButton") Or (PnlCaptura.Controls.Item(I).GetType.Name = "TextBox") Then
                'usé la función MID(cadena, inicio[, longitud])
                If (PnlCaptura.Controls.Item(I).GetType.Name = "RadioButton") Then
                    If CType(PnlCaptura.Controls.Item(I), RadioButton).Checked Then
                        IdOpcion = Mid(PnlCaptura.Controls.Item(I).ID, 3, Len(PnlCaptura.Controls.Item(I).ID) - 2) 'RB[####]                    
                    End If
                Else 'Es TextBox
                    IdOpcion = Mid(PnlCaptura.Controls.Item(I).ID, 3, Len(PnlCaptura.Controls.Item(I).ID) - 2) 'TB[####]
                    Valor = CType(PnlCaptura.Controls.Item(I + 2), DropDownList).SelectedValue
                    MisVariablesGlobales.Calificacion = Valor 'Este valor lo pasaré a la hora de capturar el registro en RespuestaAbierta
                End If

                If Trim(IdOpcion) <> "" Then
                    'ALGORITMO PARA ALMACENAR LA RESPUESTA DADA (guardo el par de IdPlanteamiento e IdOpcion
                    'PASO 1) Obtengo los datos para ingresar la respuesta
                    miComando.CommandText = "SELECT A.IdAlumno, G.IdGrupo, G.IdGrado FROM Alumno A, Grupo G where A.IdAlumno = " + Session("IdAlumno") + " and G.IdGrupo = A.IdGrupo"
                    Try
                        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                        misRegistros.Read() 'Leo para poder accesarlos
                        'IdAlumno ya lo tengo
                        IdGrado = CStr(misRegistros.Item("IdGrado"))
                        IdGrupo = CStr(misRegistros.Item("IdGrupo"))
                        'IdPlanteamiento  ya lo tengo
                        'Consecutiva = Aqui no se necesita saberlo porque tengo el IdOpcion

                        misRegistros.Close()
                        miComando.CommandText = "SELECT C.IdCicloEscolar, D.IdDetalleEvaluacion FROM Evaluacion E, DetalleEvaluacion D, " + _
                                                " Calificacion C where E.IdEvaluacion = " + Session("IdEvaluacion") + _
                                                " and D.IdEvaluacion = E.IdEvaluacion and C.IdCalificacion = E.IdCalificacion"
                        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                        misRegistros.Read() 'Leo para poder accesarlos

                        IdCicloEscolar = CStr(misRegistros.Item("IdCicloEscolar"))
                        IdDetalleEvaluacion = CStr(misRegistros.Item("IdDetalleEvaluacion"))
                        'Cierro el objeto para utilizarlo en otra conexion, no es necesario
                        'cerrar objConexion
                        misRegistros.Close()

                        'PASO 2) Obtengo si la Opcion es correcta (EN EL CASO DE RESPUESTAS ABIERTAS, SE REGISTRA COMO UNA ÚNICA RESPUESTA CORRECTA
                        miComando.CommandText = "select P.TipoRespuesta, O.IdOpcion, O.Consecutiva, O.Correcta from Opcion O, Planteamiento P where P.IdPlanteamiento = O.IdPlanteamiento and O.idPlanteamiento = " + IdPlanteamiento.ToString + " and O.IdOpcion = " + IdOpcion.ToString
                        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                        misRegistros.Read() 'Leo para poder accesarlos
                        'IdOpcion ya
                        If Not (PnlCaptura.Controls.Item(I).GetType.Name = "TextBox") Then
                            Acertada = misRegistros.Item("Correcta") 'S o N
                            'En caso contrario que sea TextBox, ya tengo el valor de S,N o M
                        End If
                        misRegistros.Close()

                        'PASO 3) Ingreso la respuesta
                        If (PnlCaptura.Controls.Item(I).GetType.Name = "TextBox") Then
                            MisVariablesGlobales.EsAbierta = True
                            MisVariablesGlobales.RespAbierta = CType(PnlCaptura.Controls.Item(I), TextBox).Text 'Tiene que ir antes del Insert
                            If CDec(Valor) < 60.0 Then 'Esto es clave para poner S o N e respuestas abiertas
                                Acertada = "N"
                            Else
                                Acertada = "S"
                            End If
                            SDSrespuestas.InsertCommand = "SET dateformat dmy; INSERT INTO Respuesta (IdPlanteamiento,IdOpcion,IdAlumno,IdGrado,IdGrupo,IdCicloEscolar,IdDetalleEvaluacion,Intento,Acertada,FechaContestada) VALUES (" + _
                                                         IdPlanteamiento + "," + IdOpcion + "," + Session("IdAlumno") + "," + IdGrado + "," + IdGrupo + "," + IdCicloEscolar + "," + IdDetalleEvaluacion + ",1,'" + Acertada + "',getdate()); SELECT @NuevoIdresp = @@Identity"
                            SDSrespuestas.Insert()
                        Else 'Significa que es RadioButton y está checado
                            MisVariablesGlobales.EsAbierta = False
                            SDSrespuestas.InsertCommand = "SET dateformat dmy; INSERT INTO Respuesta (IdPlanteamiento,IdOpcion,IdAlumno,IdGrado,IdGrupo,IdCicloEscolar,IdDetalleEvaluacion,Intento,Acertada,FechaContestada) VALUES (" + _
                                                         IdPlanteamiento + "," + IdOpcion + "," + Session("IdAlumno") + "," + IdGrado + "," + IdGrupo + "," + IdCicloEscolar + "," + IdDetalleEvaluacion + ",1,'" + Acertada + "',getdate())"
                            SDSrespuestas.Insert()
                        End If
                    Catch ex As Exception
                        Utils.LogManager.ExceptionLog_InsertEntry(ex)
                        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
                    End Try
                    '**************TERMINA ALGORITMO**************
                End If 'del Trim(IdOpcion) <> ""
            End If 'del If (PnlCaptura.Controls.Item(I).GetType.Name = "Label") Then
            I += 1
        Loop

        'ALGORITMO PARA CALCULAR LA CALIFICACIÓN
        Try
            'PRIMERO VEO CUANTOS SON LOS PUNTOS POSIBLES QUE PODRÍA SACAR
            miComando.CommandText = "select Sum(P.Ponderacion) Puntos, Count(Ponderacion) Cant " + _
                "from Respuesta R, Planteamiento P, DetalleEvaluacion D " + _
                "where (R.IdPlanteamiento = P.IdPlanteamiento) and P.Ocultar = 0 " + _
                "and R.IdAlumno = " + Session("IdAlumno") + " and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion " + _
                "and D.IdEvaluacion  = " + Session("IdEvaluacion")
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            Dim TotalPuntos = misRegistros.Item("Puntos")
            Dim TotalReactivos = misRegistros.Item("Cant")
            misRegistros.Close()

            'CHECO CUANTOS PUNTOS SUMO EL ALUMNO CORRECTAMENTE EN LA PRIMERA VUELTA
            miComando.CommandText = "select Sum(P.Ponderacion) Puntos, Count(Ponderacion) Cant " + _
                "from Respuesta R, Planteamiento P, DetalleEvaluacion D " + _
                "where (R.IdPlanteamiento = P.IdPlanteamiento) and P.TipoRespuesta = 'Multiple' and P.Ocultar = 0 " + _
                "and R.IdAlumno = " + Session("IdAlumno") + " and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion " + _
                "and D.IdEvaluacion  = " + Session("IdEvaluacion") + " and R.Acertada = 'S' and R.Intento = 1"
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()

            Dim PuntosOk, ReactivosAcertados
            If IsDBNull(misRegistros.Item("Puntos")) Then
                PuntosOk = 0
                ReactivosAcertados = 0
            Else
                PuntosOk = misRegistros.Item("Puntos")
                ReactivosAcertados = misRegistros.Item("Cant")
            End If
            misRegistros.Close()

            'AHORA CHECO LOS PUNTOS QUE OBTUVO EN LA SEGUNDA VUELTA
            miComando.CommandText = "select Sum(Ponderacion - (P.Ponderacion * P.PorcentajeRestarResp)/100) Puntos, Count(Ponderacion) Cant " + _
                "from Respuesta R, Planteamiento P, DetalleEvaluacion D " + _
                "where (R.IdPlanteamiento = P.IdPlanteamiento) and P.TipoRespuesta = 'Multiple' and P.Ocultar = 0 " + _
                "and R.IdAlumno = " + Session("IdAlumno") + " and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion " + _
                "and D.IdEvaluacion  = " + Session("IdEvaluacion") + " and R.Acertada = 'S' and R.Intento = 2"
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()

            If (Not IsDBNull(misRegistros.Item("Puntos"))) Then  '(misRegistros.Item("Cant") > 0) no es necesario
                PuntosOk = PuntosOk + misRegistros.Item("Puntos")
                ReactivosAcertados = ReactivosAcertados + misRegistros.Item("Cant")
            End If
            misRegistros.Close()


            'AHORA CHECO LOS PUNTOS QUE OBTUVO A TRAVÉS DE RESPUESTAS ABIERTAS QUE SE CONSIDERARON ACERTADAS
            miComando.CommandText = "select Sum(P.Ponderacion * (RA.Calificacion/100)) Puntos, Count(Ponderacion) Cant " + _
                "from Respuesta R, Planteamiento P, DetalleEvaluacion D, RespuestaAbierta RA " + _
                "where (R.IdPlanteamiento = P.IdPlanteamiento) and (RA.IdRespuesta = R.IdRespuesta) and P.TipoRespuesta = 'Abierta' and P.Ocultar = 0 " + _
                "and R.IdAlumno = " + Session("IdAlumno") + " and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion " + _
                "and D.IdEvaluacion  = " + Session("IdEvaluacion") + " and R.Acertada = 'S'"
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            If misRegistros.Read() Then
                If (Not IsDBNull(misRegistros.Item("Puntos"))) Then  '(misRegistros.Item("Cant") > 0) no es necesario
                    PuntosOk = PuntosOk + misRegistros.Item("Puntos")
                    ReactivosAcertados = ReactivosAcertados + misRegistros.Item("Cant")
                End If
            End If
            misRegistros.Close()
            'AHORA CHECO LOS PUNTOS QUE OBTUVO A TRAVÉS DE RESPUESTAS ABIERTAS QUE SE CONSIDERARON ERRONEAS
            miComando.CommandText = "select Sum(P.Ponderacion * (RA.Calificacion/100)) Puntos, Count(Ponderacion) Cant " + _
                "from Respuesta R, Planteamiento P, DetalleEvaluacion D, RespuestaAbierta RA " + _
                "where (R.IdPlanteamiento = P.IdPlanteamiento) and (RA.IdRespuesta = R.IdRespuesta) and P.TipoRespuesta = 'Abierta' and P.Ocultar = 0 " + _
                "and R.IdAlumno = " + Session("IdAlumno") + " and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion " + _
                "and D.IdEvaluacion  = " + Session("IdEvaluacion") + " and R.Acertada = 'N'"
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            If misRegistros.Read() Then
                If (Not IsDBNull(misRegistros.Item("Puntos"))) Then  '(misRegistros.Item("Cant") > 0) no es necesario
                    PuntosOk = PuntosOk + misRegistros.Item("Puntos")
                    ReactivosAcertados = ReactivosAcertados + misRegistros.Item("Cant")
                End If
            End If
            misRegistros.Close()


            'NUEVO, ESTO LO AGREGUÉ PARA QUE CONTARA LA CANTIDAD DE REACTIVOS Y PUNTOS TOTALES
            miComando.CommandText = "select ISNULL(Sum(P.Ponderacion),0.0) Puntos, ISNULL(Count(P.Ponderacion),0.0) Cant " + _
               " from Planteamiento P, DetalleEvaluacion D " + _
               " where(D.IdEvaluacion = " + Session("IdEvaluacion") + ")" + _
               " and P.IdSubtema = D.IdSubtema and P.Ocultar = 0"
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            TotalPuntos = misRegistros.Item("Puntos")
            TotalReactivos = misRegistros.Item("Cant")



            'CALCULO EL RESULTADO
            Dim Resultado = (PuntosOk / TotalPuntos) * 100

            'Inserto el registro de la evaluacion terminada
            SDSevaluacionesterminadas.InsertCommand = "SET dateformat dmy; INSERT INTO EvaluacionTerminada(IdAlumno,IdGrado,IdGrupo,IdCicloEscolar,IdEvaluacion,TotalReactivos,ReactivosContestados,PuntosPosibles,PuntosAcertados,Resultado,FechaTermino) " + _
            "VALUES(" + Session("IdAlumno") + "," + IdGrado + "," + _
                IdGrupo + "," + IdCicloEscolar + "," + _
                Session("IdEvaluacion") + "," + CStr(TotalReactivos) + "," + _
                CStr(ReactivosAcertados) + "," + CStr(FormatNumber(TotalPuntos, 2)) + "," + _
                CStr(FormatNumber(PuntosOk, 2)) + "," + CStr(FormatNumber(Resultado, 2)) + ",getdate())"
            'puedo usar tambien: String.Format("{0:c}", Resultado) para que salga con simbolo y todo
            SDSevaluacionesterminadas.Insert()
            'TERMINA ALGORITMO
            objConexion.Close()

            msgInfo.show("La calificación obtenida es: " + CStr(FormatNumber(Resultado, 2)))
            BtnAceptarResp.Visible = False
            LblM.Visible = False

            PnlCaptura.Visible = False
            BtnAceptar.Visible = False
            BtnCancelar.Visible = False
            BtnTerminar.Visible = False
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnTerminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnTerminar.Click
        msgError.hide()
        Dim Cont As Byte 'Esta variable la usaré para pintar los renglones
        Cont = 0
        Dim color As String
        Dim I = 0
        'Los datos están completos, desplegaré la previsualización
        PnlRespuestas.Controls.Add(New LiteralControl("<Table align='left' border='1' bordercolor = '#CCCCCC'>")) 'Agrego el inicio de la Tabla
        PnlRespuestas.Controls.Add(New LiteralControl("<tr><td width='60%'><font face = 'Arial' size='2'><b>PLANTEAMIENTO</b></font></td><td width='40%'><font face = 'Arial' size='2'><b>RESPUESTA CAPTURADA</b></font></td></tr>"))
        Do While (I < PnlCaptura.Controls.Count)
            If (PnlCaptura.Controls.Item(I).GetType.Name = "Label") Then
                'Lo siguiente debe quedar aquí adentro para que no cambie con la lectura de los radiobuttons o textbox
                Cont += 1
                If Cont Mod 2 = 0 Then
                    color = "#E3EAEB"
                Else
                    color = "#CCCCCC"
                End If

                'Ahora inserto el planteamiento
                Dim Etiq2 As Label = New Label 'AQUI DEBE DE IR PARA QUE SE REGENERE Y SE AGREGUE COMO NUEVA EN EL PANEL
                Etiq2.ID = PnlCaptura.Controls.Item(I).ID 'Se compone de Pla + IdPlanteamiento
                Etiq2.Text = "<font face = 'Arial' size='2'>" + CType(PnlCaptura.Controls.Item(I), Label).Text + "</font>"
                PnlRespuestas.Controls.Add(New LiteralControl("<tr  bgcolor = '" + color + "'><td width='60%'>"))
                PnlRespuestas.Controls.Add(Etiq2)
                PnlRespuestas.Controls.Add(New LiteralControl("</td>"))

            ElseIf PnlCaptura.Controls.Item(I).GetType.Name = "TextBox" Then
                Dim Etiq4 As Label = New Label 'AQUI DEBE DE IR PARA QUE SE REGENERE Y SE AGREGUE COMO NUEVA EN EL PANEL
                Etiq4.ID = PnlCaptura.Controls.Item(I).ID 'Se compone de TB + IdOpcion
                Etiq4.Text = CType(PnlCaptura.Controls.Item(I), TextBox).Text
                PnlRespuestas.Controls.Add(New LiteralControl("<td width='40%'>"))
                PnlRespuestas.Controls.Add(Etiq4)

                PnlRespuestas.Controls.Add(New LiteralControl("</td><td>"))
                Dim Etiq4cal As Label = New Label
                Etiq4cal.ID = PnlCaptura.Controls.Item(I).ID + "cal" 'Se compone de TB + IdOpcion
                Etiq4cal.Text = CType(PnlCaptura.Controls.Item(I + 2), DropDownList).SelectedValue
                PnlRespuestas.Controls.Add(Etiq4cal)

                PnlRespuestas.Controls.Add(New LiteralControl("</td></tr>")) 'Aquí va porque cuando ponga respuesta, cambia planteamiento
            ElseIf PnlCaptura.Controls.Item(I).GetType.Name = "RadioButton" Then
                If CType(PnlCaptura.Controls.Item(I), RadioButton).Checked Then
                    Dim Etiq3 As Label = New Label 'AQUI DEBE DE IR PARA QUE SE REGENERE Y SE AGREGUE COMO NUEVA EN EL PANEL
                    Etiq3.ID = PnlCaptura.Controls.Item(I).ID 'Se compone de RB + IdOpcion
                    Etiq3.Text = CType(PnlCaptura.Controls.Item(I), RadioButton).Text
                    PnlRespuestas.Controls.Add(New LiteralControl("<td width='40%'>"))
                    PnlRespuestas.Controls.Add(Etiq3)
                    PnlRespuestas.Controls.Add(New LiteralControl("</td></tr>")) 'Aquí va porque cuando ponga respuesta, cambia planteamiento
                End If
            End If
            I += 1
        Loop
        PnlRespuestas.Controls.Add(New LiteralControl("</Table>")) 'Agrego el final de la Tabla
        msgInfo.show("REVISE LAS RESPUESTAS CAPTURADAS ANTES DE ACEPTAR LA CAPTURA")
        BtnAceptarResp.Visible = True
        LblM.Visible = True
    End Sub

    Protected Sub SDSrespuestas_Inserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSrespuestas.Inserted
        'Este procedimiento se ejecuta cada que se inserta un registro en la tabla Respuesta
        If MisVariablesGlobales.EsAbierta Then
            Dim nuevoId = e.Command.Parameters("@NuevoIdresp").Value
            SDSrespuestasabiertas.InsertCommand = "SET dateformat dmy; INSERT INTO RespuestaAbierta(IdRespuesta,Redaccion,Calificacion,Estado) VALUES(" + nuevoId.ToString + ",'" + Session("RespAbierta") + "'," + MisVariablesGlobales.Calificacion + ",'Enviado')"
            SDSrespuestasabiertas.Insert()
        End If
    End Sub
End Class
