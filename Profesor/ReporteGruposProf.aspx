﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="ReporteGruposProf.aspx.vb"
    Inherits="profesor_ReporteGruposProf"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style11 {
            height: 25px;
            text-align: left;
        }

        .style12 {
            text-align: left;
            height: 24px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style24 {
            width: 143px;
            height: 24px;
        }

        .style21 {
            width: 273px;
        }

        .style13 {
            width: 143px;
        }

        .style14 {
            text-align: center;
        }

        .style22 {
            text-align: left;
            width: 324px;
        }

        .style26 {
            height: 25px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style28 {
            height: 31px;
        }

        .style29 {
            height: 24px;
        }

        .style30 {
            height: 47px;
            text-align: left;
        }

        .style31 {
            height: 30px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Resultados -
        <asp:Label ID="Label8" runat="server" Text="[ASIGNATURA]" />
    </h1>
    Presenta los resultados por 
	<asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
     de 
    <asp:Label ID="Label9" runat="server" Text="[UNA]" /> 
	<asp:Label ID="Label2" runat="server" Text="[ASIGNATURA]" />
     específica mostrando la calificación promedio y la acumulada. Permite ampliar el detalle por 
	<asp:Label ID="Label3" runat="server" Text="[ALUMNO]" />
     para consultar las respuestas dadas a cada actividad perteneciente a 
    <asp:Label ID="Label10" runat="server" Text="[LA]" /> 
	<asp:Label ID="Label4" runat="server" Text="[ASIGNATURA]" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr class="titulo">
            <td class="style30" colspan="6">Resultados por 
								<asp:Label ID="Label5" runat="server" Text="[ASIGNATURA]" />
                de sus 
								<asp:Label ID="Label6" runat="server" Text="[GRUPOS]" />
            </td>
        </tr>
        <tr class="titulo">
            <td class="style11" colspan="6">
                <asp:Label ID="LblNombre" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #000099"></asp:Label>
            </td>
        </tr>
        <tr class="titulo">
            <td class="style26" colspan="2">Seleccione 
                <asp:Label ID="Label12" runat="server" Text="[UN]" /> 
								<asp:Label ID="Label7" runat="server" Text="[CICLO]" />
            </td>
            <td class="style11" colspan="2">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Width="355px">
                </asp:DropDownList>
            </td>
            <td class="style11" colspan="2">&nbsp;</td>
        </tr>
        <tr class="titulo">
            <td class="style26" colspan="2">Seleccione 
                <asp:Label ID="Label13" runat="server" Text="[UN]" /> 
								<asp:Label ID="Label11" runat="server" Text="[PLANTEL]" />
            </td>
            <td class="style11" colspan="2">
                <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                    DataValueField="IdPlantel" Width="355px">
                </asp:DropDownList>
            </td>
            <td class="style11" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: left;">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td class="style24" colspan="2"></td>
            <td class="style29"></td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="6">
                <asp:GridView ID="GVgrupos" runat="server"
                    AllowPaging="True"
                    AutoGenerateColumns="False"
                    AllowSorting="false"
                    Caption="<h3>Seleccione el GRUPO que desee consultar.</h3>"
                    PageSize="20"

                    DataSourceID="SDSgrupos"
                    DataKeyNames="IdGrupo,IdAsignatura,IdPlantel,IdCicloEscolar"
                    Width="800px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo" InsertVisible="False"
                            ReadOnly="True" SortExpression="IdGrupo" Visible="False" />
                        <asp:BoundField DataField="Grupo" HeaderText="Grupo" SortExpression="Grupo" />
                        <asp:BoundField DataField="IdAsignatura" HeaderText="IdAsignatura"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdAsignatura"
                            Visible="False" />
                        <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"
                            SortExpression="Asignatura" />
                        <asp:BoundField DataField="Grado" HeaderText="Grado" SortExpression="Grado" />
                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel"
                            Visible="False" />
                        <asp:BoundField DataField="Plantel" HeaderText="Plantel"
                            SortExpression="Plantel" Visible="false"/>
                        <asp:BoundField DataField="IdCicloEscolar" HeaderText="IdCicloEscolar"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdCicloEscolar"
                            Visible="False" />
                        <asp:BoundField DataField="CicloEscolar" HeaderText="CicloEscolar"
                            SortExpression="CicloEscolar" Visible="False" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="3">
                <asp:GridView ID="GVmaximos" runat="server" 
                    AutoGenerateColumns="False"

                    DataSourceID="SDSmaximascal" 
                    Width="366px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Calificacion" HeaderText="Calificación"
                            SortExpression="Calificacion" />
                        <asp:BoundField DataField="Maximo" HeaderText="% Acumulado Máximo Posible" ReadOnly="True"
                            SortExpression="Maximo" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
            <td class="style14" colspan="3">
                <asp:GridView ID="GVindicadores" runat="server" 
                    AutoGenerateColumns="False"
                    Caption="INDICADORES ESPECÍFICOS" 

                    DataSourceID="SDSindicadores" 
                    DataKeyNames="Color"
                    Width="237px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Categoria" HeaderText="Categoria"
                            SortExpression="Categoria" />
                        <asp:BoundField DataField="Color" HeaderText="Color" SortExpression="Color" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="6">
                <asp:RadioButtonList ID="RBacumuladas" runat="server" AutoPostBack="True"
                    RepeatDirection="Horizontal"
                    Style="font-weight: 700; color: #000066; font-size: small">
                    <asp:ListItem Value="M">Mostrar acumuladas</asp:ListItem>
                    <asp:ListItem Value="O" Selected="True">Ocultar acumuladas</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="6">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="6">
                <asp:GridView ID="GVsalida" runat="server"
                    AllowSorting="True"
                    Caption="<h3>Listado de ALUMNOS del GRUPO seleccionado.</h3>"

                    Width="890px"
                    DataSourceID="SDSmateriaalumno"
                    DataKeyNames="IdAlumno"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell">
                            <HeaderStyle Width="70px" />
                        </asp:CommandField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="6">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td class="style22">
                &nbsp;</td>
            <td class="style21" colspan="2">
                &nbsp;</td>
            <td class="style13" colspan="2">
            </td>
            <td class="style15">&nbsp;</td>
        </tr>
        <tr>
            <td class="style22">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td class="style21" colspan="2">
            </td>
            <td class="style13" colspan="2">
                &nbsp;</td>
            <td class="style15">&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSmateriaalumno" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SP_promediomateriaalumno" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:SessionParameter Name="IdAsignatura" SessionField="IdAsignatura"
                            Type="Int32" />
                        <asp:SessionParameter Name="IdGrupo" SessionField="IdGrupo" Type="Int32" />
                        <asp:SessionParameter Name="TotalC" SessionField="TotalC" Type="Int16" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select G.IdGrupo, G.Descripcion Grupo, A.IdAsignatura, A.Descripcion Asignatura, Gr.Descripcion Grado, Pl.IdPlantel, Pl.Descripcion Plantel, C.IdCicloEscolar, C.Descripcion CicloEscolar
from Grupo G,Programacion P, Plantel Pl, CicloEscolar C, Asignatura A, Grado Gr
where P.IdProfesor = @IdProfesor and G.IdGrupo = P.IdGrupo and C.IdCicloEscolar = @IdCicloEscolar and P.IdCicloEscolar = C.IdCicloEscolar and Pl.IdPlantel = G.IdPlantel and A.IdAsignatura = P.IdAsignatura and Gr.IdGrado = G.IdGrado and G.IdCicloEscolar = @IdCicloEscolar AND Pl.IdPlantel = @IdPlantel
order by C.IdCicloEscolar, Pl.Descripcion, A.Descripcion, Gr.Descripcion, G.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdProfesor" SessionField="IdProfesor" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>
                <%-- selecciona solo los ciclos escolares activos de los grupos que tiene asignados --%>
                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT DISTINCT C.IdCicloEscolar, C.Descripcion FROM CicloEscolar C, Grupo G, Programacion P 
WHERE (C.Estatus = @Estatus) and C.IdCicloEscolar = G.IdCicloEscolar and G.IdGrupo = P.IdGrupo and P.IdProfesor = @IdProfesor
ORDER BY C.Descripcion">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                        <asp:SessionParameter Name="IdProfesor" Type="Int32" SessionField="IdProfesor" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <%-- selecciona solo los planteles de los grupos asignados al profesor en el ciclo seleccionado --%>
                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select DISTINCT Pl.IdPlantel, Pl.Descripcion 
from Grupo G, Programacion P, Plantel Pl, CicloEscolar C, Asignatura A, Grado Gr
where P.IdProfesor = @IdProfesor and G.IdGrupo = P.IdGrupo and C.IdCicloEscolar = @IdCicloEscolar and P.IdCicloEscolar = C.IdCicloEscolar and Pl.IdPlantel = G.IdPlantel and A.IdAsignatura = P.IdAsignatura and Gr.IdGrado = G.IdGrado and G.IdCicloEscolar = @IdCicloEscolar
order by Pl.Descripcion">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:SessionParameter Name="IdProfesor" Type="Int32" SessionField="IdProfesor" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSindicadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT DISTINCT Categoria, Color
FROM         Rango
WHERE     IdIndicador in 
(select IdIndicador from Calificacion where IdAsignatura = @IdAsignatura and IdCicloEscolar = @IdCicloEscolar)">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdAsignatura" SessionField="IdAsignatura" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSmaximascal" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Cal.Descripcion as Calificacion, (select SUM(Porcentaje) from Evaluacion where IdCalificacion = Cal.IdCalificacion and InicioContestar &lt;= getdate()) as Maximo
from Calificacion Cal
where Cal.IdCalificacion in (select Distinct C.IdCalificacion from Calificacion C, Evaluacion E  where (C.IdAsignatura = @IdAsignatura and C.IdCicloEscolar = @IdCicloEscolar) and
(E.IdCalificacion = C.IdCalificacion ))
order by Cal.Consecutivo">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdAsignatura" SessionField="IdAsignatura" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

