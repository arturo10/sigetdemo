﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="EstatusAlumnos.aspx.vb"
    Inherits="profesor_EstatusAlumnos"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            width: 100%;
            height: 240px;
        }

        .style22 {
            height: 36px;
        }

        .style28 {
            height: 42px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style27 {
            height: 42px;
        }

        .style29 {
            height: 12px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
        }

        .style14 {
            color: #000099;
            height: 27px;
        }

        .style24 {
            height: 107px;
        }

        .style21 {
            height: 2px;
        }

        .style20 {
            height: 6px;
        }

        .style30 {
            height: 28px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
            font-weight: bold;
        }

        .style31 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Avance - Actividades
    </h1>
    Permite consultar el avance del total de 
	<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    de 
    <asp:Label ID="Label6" runat="server" Text="[LOS]" /> 
	<asp:Label ID="Label2" runat="server" Text="[GRUPOS]" />
    que tiene asignad<asp:Label ID="Label7" runat="server" Text="[O]" />s como 
	<asp:Label ID="Label3" runat="server" Text="[PROFESOR]" />
    que han realizado una actividad específica.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style11">
        <tr>
            <td class="style28">Tareas que desea consultar su avance:</td>
            <td class="style27">
                <asp:DropDownList ID="DDLconsultar" runat="server" AutoPostBack="True"
                    Height="22px" Width="190px">
                    <asp:ListItem Value="Terminadas" Selected="True">Terminadas</asp:ListItem>
                    <asp:ListItem Value="Iniciadas">Iniciadas</asp:ListItem>
                    <asp:ListItem Value="Pendientes">Pendientes</asp:ListItem>
                </asp:DropDownList>
                <span class="style31">*</span></td>
        </tr>
        <tr>
            <td class="style29" colspan="2">*Las tareas INICIADAS son las que 
                <asp:Label ID="Label8" runat="server" Text="[EL]" /> 
				<asp:Label ID="Label4" runat="server" Text="[ALUMNO]" /> 
                empezó a realizar pero no ha 
                terminado todos los reactivos, o que ya subió el archivo de entrega pero 
                <asp:Label ID="Label9" runat="server" Text="[EL]" /> 
                <asp:Label ID="Label5" runat="server" Text="[PROFESOR]" /> 
                no lo ha revisado, y&nbsp;las PENDIENTES son las que no ha contestado ningún reactivo 
                ni ha subido el archivo de entrega.</td>
        </tr>
        <tr>
            <td class="style14" colspan="2"
                style="font-family: Arial, Helvetica, sans-serif; font-size: small">
                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="EstatusGrupo.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td class="style24" colspan="2">
                <asp:GridView ID="GVdatos" runat="server"
                    AllowSorting="True"

                    DataSourceID="SDSterminadas"
                    Width="883px"

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
                    TERMINADAS
	                0  A.Matricula
	                1  A.ApePaterno + ' ' + A.ApeMaterno + ', ' + A.Nombre as 'Nombre Alumno'
	                2  G.Descripcion as Grupo
	                3  ET.TotalReactivos as 'Total Reactivos'
	                4  ET.ReactivosContestados as 'R. Acertados'
	                5  ET.PuntosPosibles as 'Puntos Posibles'
	                6  ET.PuntosAcertados as 'P. Obtenidos'
	                7  ET.Resultado

                    INICIADAS
	                0  A.Matricula
	                1  A.ApePaterno + ' ' + A.ApeMaterno + ', ' + A.Nombre as Alumno
	                2  G.Descripcion Grupo
	                3  Count(IdRespuesta) as Respuestas
	                4  (select Count(IdPlanteamiento) from Evaluacion E, Calificacion C, " + _
                            "Tema T, Subtema S, Planteamiento P where E.IdEvaluacion = " + Session("IdEvaluacion") + _
                            " and C.IdCalificacion = E.IdCalificacion and T.IdAsignatura = C.IdAsignatura " + _
                            "and S.IdTema = T.IdTema and P.IdSubtema = S.IdSubtema and P.Ocultar = 0) as 'Total R.' 

                    PENDIENTES
	                0  A.Matricula
	                1  A.ApePaterno + ' ' + A.ApeMaterno + ', ' + A.Nombre as 'Nombre Alumno'
	                2  G.Descripcion as Grupo
                --%>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td class="style21" colspan="2">
                <asp:Button ID="Button1" runat="server" Text="Exportar a Excel"
                    CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td class="style21" colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style20" colspan="2">
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="EstatusGrupo.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSterminadas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select A.Matricula, A.ApePaterno + ' ' + A.ApeMaterno + ', ' + A.Nombre as 'Nombre Alumno', 
                        G.Descripcion as Grupo, ET.TotalReactivos as 'Total Reactivos', 
                        ET.ReactivosContestados as 'R. Acertados', ET.PuntosPosibles as 'Puntos Posibles', 
                        ET.PuntosAcertados as 'P. Obtenidos', ET.Resultado from Alumno A 
                        join EvaluacionTerminada ET on ET.IdEvaluacion = @IdEvaluacion and A.IdAlumno = ET.IdAlumno and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido') and ET.IdGrupo = @IdGrupo 
                        join Grupo G on G.IdGrupo = ET.IdGrupo and G.IdPlantel = @IdPlantel 
                        order by G.Descripcion, A.ApePaterno, A.ApeMaterno, A.Nombre">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdEvaluacion" SessionField="IdEvaluacion" />
                        <asp:SessionParameter Name="IdPlantel" SessionField="IdPlantel" />
                        <asp:SessionParameter Name="IdGrupo" SessionField="IdGrupo" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSiniciadas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select A.Matricula, A.ApePaterno + ' ' + A.ApeMaterno + ', ' + A.Nombre as Alumno, 
                        G.Descripcion Grupo, Count(IdRespuesta) as Respuestas, 
                        (select Count(IdPlanteamiento) from Evaluacion E, Calificacion C, 
                        Tema T, Subtema S, Planteamiento P where E.IdEvaluacion = @IdEvaluacion
                         and C.IdCalificacion = E.IdCalificacion and T.IdAsignatura = C.IdAsignatura 
                        and S.IdTema = T.IdTema and P.IdSubtema = S.IdSubtema and P.Ocultar = 0) as 'Total R.' 
                        from Alumno A, Grupo G, Evaluacion E, Calificacion C, Asignatura Asig, Respuesta R, DetalleEvaluacion D 
                        where E.IdEvaluacion = @IdEvaluacion and C.IdCalificacion = E.IdCalificacion and 
                        Asig.IdAsignatura = C.IdAsignatura and Asig.IdGrado = @IdGrado and 
                        G.IdGrado = Asig.IdGrado And G.IdPlantel = @IdPlantel
                         and A.IdGrupo = G.IdGrupo and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido') and A.IdAlumno not in 
                        (select distinct IdAlumno from EvaluacionTerminada 
                        where IdEvaluacion = @IdEvaluacion and IdAlumno = A.IdAlumno) 
                        and D.IdEvaluacion = E.IdEvaluacion and 
                        R.IdAlumno = A.IdAlumno and R.IdGrupo = @IdGrupo And R.IdDetalleEvaluacion = D.IdDetalleEvaluacion 
                        group by A.Matricula, A.ApePaterno, A.ApeMaterno, A.Nombre, G.Descripcion 
                         UNION 
                        select A.Matricula, A.ApePaterno + ' ' + A.ApeMaterno + ', ' + A.Nombre as Alumno, 
                        G.Descripcion Grupo, '' as Respuestas, '' as 'Total R.' 
                        from Alumno A, Grupo G, Evaluacion E, Calificacion C, Asignatura Asig 
                        where E.IdEvaluacion = @IdEvaluacion and C.IdCalificacion = E.IdCalificacion and 
                        Asig.IdAsignatura = C.IdAsignatura and Asig.IdGrado = @IdGrado and 
                        G.IdGrado = Asig.IdGrado And G.IdPlantel = @IdPlantel
                         and A.IdGrupo = G.IdGrupo and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido') and 
                        A.IdAlumno in (select D.IdAlumno from DoctoEvaluacion D where D.IdEvaluacion = @IdEvaluacion and D.IdAlumno not in (select IdAlumno from EvaluacionTerminada where IdEvaluacion = @IdEvaluacion)) 
                        group by A.Matricula, A.ApePaterno, A.ApeMaterno, A.Nombre, G.Descripcion 
                        order by Grupo, Alumno">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdEvaluacion" SessionField="IdEvaluacion" />
                        <asp:SessionParameter Name="IdPlantel" SessionField="IdPlantel" />
                        <asp:SessionParameter Name="IdGrupo" SessionField="IdGrupo" />
                        <asp:SessionParameter Name="IdGrado" SessionField="IdGrado" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSpendientes" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select A.Matricula, A.ApePaterno + ' ' + A.ApeMaterno + ', ' + A.Nombre as 'Nombre Alumno',G.Descripcion as Grupo 
                        from Alumno A, Grupo G, Evaluacion E, Calificacion C, Asignatura Asig 
                        where E.IdEvaluacion = @IdEvaluacion and C.IdCalificacion = E.IdCalificacion and 
                        Asig.IdAsignatura = C.IdAsignatura and Asig.IdGrado = @IdGrado and 
                        G.IdGrado = Asig.IdGrado And G.IdPlantel = @IdPlantel
                         and A.IdGrupo = G.IdGrupo and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido') and G.IdGrupo = @IdGrupo and A.IdAlumno not in 
                        (select distinct IdAlumno from Respuesta R, DetalleEvaluacion D 
                        where(D.IdEvaluacion = @IdEvaluacion And R.IdDetalleEvaluacion = D.IdDetalleEvaluacion) 
                        and R.IdAlumno = A.IdAlumno) and A.IdAlumno not in 
                        (select IdAlumno from DoctoEvaluacion where IdEvaluacion = @IdEvaluacion) 
                        group by A.Matricula, A.ApePaterno, A.ApeMaterno, A.Nombre, G.Descripcion 
                        order by G.Descripcion, A.ApePaterno, A.ApeMaterno, A.Nombre">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdEvaluacion" SessionField="IdEvaluacion" />
                        <asp:SessionParameter Name="IdPlantel" SessionField="IdPlantel" />
                        <asp:SessionParameter Name="IdGrupo" SessionField="IdGrupo" />
                        <asp:SessionParameter Name="IdGrado" SessionField="IdGrado" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

