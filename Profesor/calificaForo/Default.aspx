﻿<%@ Page Title=""
  Language="C#"
  MasterPageFile="~/Principal.master"
  AutoEventWireup="true"
  CodeFile="Default.aspx.cs"
  Inherits="Profesor_calificaForo_Default"
  MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<%@ Register Src="~/Controls/ComentarioForo.ascx" TagPrefix="uc1" TagName="ComentarioForo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
  <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
  <style type="text/css">
    .style23 {
      font-family: Arial;
      font-size: small;
      font-weight: bold;
      height: 38px;
    }

    .style24 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: small;
      text-align: right;
    }

    .style33 {
      height: 34px;
      text-align: left;
    }

    .style32 {
      height: 22px;
      width: 479px;
      font-weight: normal;
      font-size: small;
    }

    .style34 {
      height: 19px;
    }

    .style35 {
      font-family: Arial;
      font-size: small;
      font-weight: bold;
      height: 21px;
    }

    .style37 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: small;
      font-weight: bold;
      color: #000066;
    }

    .auto-style1 {
      width: 268435312px;
    }

    .auto-style2 {
      text-align: left;
    }
  </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
  <h1>Evaluaciones - Calificar Comentarios de Foro
  </h1>
  Permite desplegar todas los comentarios escritos por 
    <asp:Label ID="Label4" runat="server" Text="[LOS]" />
  <asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
  en una actividad de foro, y calificarlas.
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" runat="Server">

  <table class="style10">
    <tr>
      <td class="style24" colspan="2">
        <asp:Label ID="Label2" runat="server" Text="[CICLO]" />
      </td>
      <td colspan="4" style="text-align: left">
        <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
          DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
          DataValueField="IdCicloEscolar"
          CssClass="wideTextbox"
          Width="350px">
        </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td colspan="2" class="style24" style="vertical-align: top;">
        <asp:Label ID="Label5" runat="server" Text="[GRADO]" /></td>
      <td colspan="4" style="text-align: left; vertical-align: top;" class="auto-style1">
        <asp:DropDownList ID="DDLGrado" runat="server" AutoPostBack="True"
          DataSourceID="SDSgrados" DataTextField="Descripcion"
          CssClass="wideTextbox"
          DataValueField="IdGrado" Width="350px">
        </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td colspan="2" class="style24" style="vertical-align: top;">
        <asp:Label ID="Label3" runat="server" Text="[ASIGNATURA]" /></td>
      <td colspan="4" style="text-align: left; vertical-align: top;" class="auto-style1">
        <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
          DataSourceID="SDSasignatura" DataTextField="Descripcion"
          CssClass="wideTextbox"
          DataValueField="IdAsignatura" Width="350px" OnDataBound="DDLasignatura_DataBound">
        </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td colspan="2" class="style24">Calificación</td>
      <td colspan="4" style="text-align: left" class="auto-style1">
        <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True"
          DataSourceID="SDScalificaciones" DataTextField="Descripcion"
          CssClass="wideTextbox"
          DataValueField="IdCalificacion" Width="350px" OnDataBound="DDLcalificacion_DataBound">
        </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td colspan="2" class="style24">Actividad</td>
      <td colspan="4" style="text-align: left;" class="auto-style1">
        <asp:DropDownList ID="DDLactividad" runat="server" AutoPostBack="True"
          DataSourceID="SDSactividades" DataTextField="ClaveBateria"
          CssClass="wideTextbox"
          DataValueField="IdEvaluacion" Width="350px" OnSelectedIndexChanged="DDLactividad_SelectedIndexChanged" OnDataBound="DDLactividad_DataBound">
        </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td colspan="6">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="6">
        <asp:GridView ID="GVgrupos" runat="server"
          AllowPaging="True"
          AllowSorting="True"
          AutoGenerateColumns="False"
          Caption="<h3>GRUPOS que tiene asignados actualmente</h3>"
          DataSourceID="SDSgrupos"
          Width="873px"
          DataKeyNames="IdGrupo,IdGrado,IdPlantel"
          CssClass="dataGrid_clear_selectable"
          OnSelectedIndexChanged="GVgrupos_SelectedIndexChanged"
          GridLines="None">
          <Columns>
            <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
            <asp:BoundField DataField="IdProfesor" HeaderText="IdProfesor"
              InsertVisible="False" ReadOnly="True" SortExpression="IdProfesor"
              Visible="False" />
            <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo" InsertVisible="False"
              ReadOnly="True" SortExpression="IdGrupo" Visible="True" />
            <asp:BoundField DataField="Grupo" HeaderText="[GRUPO]" SortExpression="Grupo" />
            <asp:BoundField DataField="IdGrado" HeaderText="IdGrado"
              SortExpression="IdGrado" InsertVisible="False" ReadOnly="True"
              Visible="False" />
            <asp:BoundField DataField="Grado" HeaderText="[GRADO]" SortExpression="Grado" />
            <asp:BoundField DataField="Nivel" HeaderText="[NIVEL]"
              SortExpression="Nivel" />
            <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
              InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel"
              Visible="False" />
            <asp:BoundField DataField="Plantel" HeaderText="[PLANTEL]"
              SortExpression="Plantel" />
          </Columns>
          <FooterStyle CssClass="footer" />
          <PagerStyle CssClass="pager" />
          <SelectedRowStyle CssClass="selected" />
          <HeaderStyle CssClass="header" />
          <AlternatingRowStyle CssClass="altrow" />
        </asp:GridView>
        <%-- 
                    sort YES - rowdatabound
        --%>
      </td>
    </tr>
    <tr>
      <td colspan="6">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2">
        Filtrar comentarios de 
      </td>
      <td colspan="4">
        <asp:DropDownList ID="DdlAlumnos" runat="server" AutoPostBack="True"
          DataSourceID="SDSAlumnos" 
          DataTextField="Nombre"
          DataValueField="IdAlumno" 
          Width="350px" 
          CssClass="wideTextbox"
          OnDataBound="DdlAlumnos_DataBound">
        </asp:DropDownList>
      </td>
    </tr>
    <tr>
      <td colspan="6">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2">
        <asp:Button ID="BtnConsultar" runat="server" 
          Text="Ver Comentarios"
          OnClick="BtnConsultar_Click"
          CssClass="defaultBtn btnThemeBlue btnThemeWide" />
      </td>
      <td colspan="2" style="text-align: center;">
        <asp:Button ID="BtnResumen" runat="server" 
          Text="Ver Resumen de Calificaciones"
          OnClick="BtnResumen_Click"
          CssClass="defaultBtn btnThemeGrey btnThemeWide" />
      </td>
      <td colspan="2" style="text-align: right;">
        <asp:Button ID="BtnComentar" runat="server" 
          Text="Comentar"
          OnClick="BtnComentar_Click"
          CssClass="defaultBtn btnThemeGrey btnThemeWide" />
      </td>
    </tr>
    <tr>
      <td colspan="6">
        <uc1:msgInfo runat="server" ID="msgInfo" />
      </td>
    </tr>
    <tr>
      <td colspan="6">
        <uc1:msgSuccess runat="server" ID="msgSuccess" />
      </td>
    </tr>
    <tr>
      <td colspan="6">
        <uc1:msgError runat="server" ID="msgError" />
      </td>
    </tr>
    <tr>
      <td colspan="6">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="6" style="font-size: 0.9em;">
        <asp:Panel ID="PnlComentarios" runat="server"></asp:Panel>
      </td>
    </tr>
    <tr>
      <td colspan="6">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="3" style="text-align: left">
        <asp:HyperLink ID="HyperLink1" runat="server"
          NavigateUrl="~/"
          CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
        </asp:HyperLink>
      </td>
      <td colspan="3" style="text-align: left">&nbsp;</td>
    </tr>
  </table>

  <asp:Panel ID="pnlMessage" runat="server" CssClass="dialogOverwrite dialogRespuestaAbierta">
    <table>
      <tr>
        <td style="text-align: center;">
          <asp:Label ID="lblMensajeInicio" runat="server" CssClass="LabelInfoDefault"></asp:Label>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <asp:LinkButton ID="btnCerrarDialogo" runat="server"
            CssClass="defaultBtn btnThemeGrey btnThemeMedium">
            <asp:Image ID="imgAceptarCrear" runat="server"
              ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/accept.png"
              CssClass="btnIcon"
              Height="24" />
            <asp:Label ID="lblCerrarDialogo" runat="server">
                            Cerrar
            </asp:Label>
          </asp:LinkButton>
        </td>
      </tr>
    </table>
  </asp:Panel>

  <table class="dataSources">
    <tr>
      <td>

        <asp:SqlDataSource ID="SDSgrupos" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="
                  select distinct P.IdProfesor, G.IdGrupo, G.Descripcion as Grupo, Gr.IdGrado, Gr.Descripcion Grado, N.Descripcion as Nivel, Pl.IdPlantel, Pl.Descripcion as Plantel
from Profesor P, Grupo G, Grado Gr, Nivel N, Plantel Pl, Programacion Pr
where P.IdProfesor = @IdProfesor and Pr.IdProfesor = P.IdProfesor
and Pr.IdCicloEscolar = @IdCicloEscolar and G.IdGrupo = Pr.IdGrupo and
 Pl.IdPlantel = G.IdPlantel and Gr.IdGrado = G.IdGrado and N.IdNivel = Gr.IdNivel
and G.IdCicloEscolar = @IdCicloEscolar
order by Pl.Descripcion, N.Descripcion, Gr.Descripcion, G.Descripcion">
          <SelectParameters>
            <asp:SessionParameter Name="IdProfesor" SessionField="Usuario_IdProfesor" />
            <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
              PropertyName="SelectedValue" />
          </SelectParameters>
        </asp:SqlDataSource>

      </td>
      <td>

        <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

      </td>
      <td>

        <%-- selecciona los grados de los grupos de las programaciones del profesor en ese ciclo escolar --%>
        <asp:SqlDataSource ID="SDSgrados" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="select distinct Gr.IdGrado, Gr.Descripcion
from Usuario U, Profesor P, Grupo G, Grado Gr, Nivel N, Plantel Pl, Programacion Pr
where U.Login = @Login and P.IdUsuario = U.IdUsuario and Pr.IdProfesor = P.IdProfesor
and Pr.IdCicloEscolar = @IdCicloEscolar and G.IdGrupo = Pr.IdGrupo and 
 Pl.IdPlantel = G.IdPlantel and Gr.IdGrado = G.IdGrado and N.IdNivel = Gr.IdNivel and G.IdCicloEscolar = @IdCicloEscolar
order by Gr.Descripcion">
          <SelectParameters>
            <asp:SessionParameter Name="Login" SessionField="Login" />
            <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
              PropertyName="SelectedValue" />
          </SelectParameters>
        </asp:SqlDataSource>

      </td>
      <td></td>
    </tr>
    <tr>
      <td>

        <asp:SqlDataSource ID="SDSasignatura" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT IdAsignatura, Descripcion 
FROM Asignatura WHERE IdGrado = @IdGrado
AND IdAsignatura in (select IdAsignatura from Programacion where IdProfesor in (select P.IdProfesor from Profesor P, Usuario U where U.Login = @Login and P.IdUsuario = U.IdUsuario))
ORDER BY IdArea">
          <SelectParameters>
            <asp:ControlParameter ControlID="DDLGrado" Name="IdGrado"
              PropertyName="SelectedValue" />
            <asp:SessionParameter Name="Login" SessionField="Login" />
          </SelectParameters>
        </asp:SqlDataSource>

      </td>
      <td>

        <asp:SqlDataSource ID="SDScalificaciones" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT [IdCalificacion], [Descripcion] FROM [Calificacion] WHERE (([IdCicloEscolar] = @IdCicloEscolar) AND ([IdAsignatura] = @IdAsignatura)) ORDER BY [Consecutivo]">
          <SelectParameters>
            <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
              PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
              PropertyName="SelectedValue" Type="Int32" />
          </SelectParameters>
        </asp:SqlDataSource>

      </td>
      <td>

        <asp:SqlDataSource ID="SDSactividades" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT [IdEvaluacion], [ClaveBateria] FROM [Evaluacion] WHERE ([IdCalificacion] = @IdCalificacion) AND EsForo = 'true' ORDER BY [InicioContestar], [ClaveBateria]">
          <SelectParameters>
            <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion"
              PropertyName="SelectedValue" Type="Int64" />
          </SelectParameters>
        </asp:SqlDataSource>

      </td>
      <td>
        <asp:Button ID="HF1" runat="server" Text="HF1" />
        <asp:ModalPopupExtender ID="ModalPopupMessage" runat="server"
          PopupControlID="pnlMessage" TargetControlID="HF1" BackgroundCssClass="overlay">
        </asp:ModalPopupExtender>
      </td>
    </tr>
    <tr>
      <td>
        
        <asp:SqlDataSource ID="SDSAlumnos" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="
          SELECT
            IdAlumno, 
            ApePaterno + ' ' + ApeMaterno + ', ' + Nombre AS Nombre
          FROM 
            Alumno
          WHERE
            IdGrupo = @IdGrupo
          ">
          <SelectParameters>
            <asp:ControlParameter ControlID="GVgrupos" Name="IdGrupo"
              PropertyName="SelectedDataKey.Values[IdGrupo]" />
          </SelectParameters>
        </asp:SqlDataSource>

      </td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </table>
</asp:Content>

