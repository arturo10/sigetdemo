﻿<%@ Page Title=""
  Language="C#"
  MasterPageFile="~/Principal.master"
  AutoEventWireup="true"
  CodeFile="Default.aspx.cs"
  Inherits="Profesor_calificaForo_resumen_Default"
  MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
  <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
  <style type="text/css">
    .style11 {
      width: 100%;
    }

    .style23 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: small;
      font-weight: bold;
    }

    .style32 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: small;
      font-weight: normal;
      text-align: right;
      height: 25px;
      width: 419px;
    }
  </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
  <h1>Evaluaciones - Resumen de Calificaciones de Foro
  </h1>
  Permite consultar las calificaciones de un grupo en una actividad de foro.
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" runat="Server">
  <table class="style11">
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td colspan="4">
        <asp:GridView ID="GVdatos" runat="server"
          AllowSorting="True"
          Width="869px"
          DataSourceID="SDSdatos"
          CssClass="dataGrid_clear"
          GridLines="None">
          <FooterStyle CssClass="footer" />
          <PagerStyle CssClass="pager" />
          <SelectedRowStyle CssClass="selected" />
          <HeaderStyle CssClass="header" />
          <AlternatingRowStyle CssClass="altrow" />
        </asp:GridView>
        <%-- sort YES - rowdatabound
                    0  select
                    1  idevaluacion
                    2  E.ClaveBateria as 'Evaluacion (Actividad)'
                    3  E.ClaveAbreviada as 'Abreviación'
                    4  tipo
                    5  C.Descripcion 'Periodo Califica'
                    6  A.Descripcion Asignatura
        --%>
      </td>
    </tr>
    <tr>
      <td colspan="4"></td>
    </tr>
    <tr>
      <td colspan="4">
        <uc1:msgError runat="server" ID="msgError" />
      </td>
    </tr>
    <tr>
      <td colspan="4">
        <asp:HyperLink ID="HlBack" runat="server" NavigateUrl="javascript:history.go(-1);"
          CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
      </td>
    </tr>
  </table>

  <table class="dataSources">
    <tr>
      <td>

        <asp:SqlDataSource ID="SDSdatos" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="Consulta_ForoResumen"
          SelectCommandType="StoredProcedure">
          <SelectParameters>
            <asp:ControlParameter ControlID="HfIdEvaluacion" Name="IdEvaluacion"
              PropertyName="Value" Type="Int64" />
            <asp:ControlParameter ControlID="HfIdGrupo" Name="IdGrupo"
              PropertyName="Value" Type="Int64" />
          </SelectParameters>
        </asp:SqlDataSource>

      </td>
      <td>
        <asp:HiddenField ID="HfBackCounter" runat="server" Value="0" />
      </td>
      <td>
        <asp:HiddenField ID="HfIdEvaluacion" runat="server" />
      </td>
      <td>
        <asp:HiddenField ID="HfIdGrupo" runat="server" />
      </td>
    </tr>
  </table>
</asp:Content>

