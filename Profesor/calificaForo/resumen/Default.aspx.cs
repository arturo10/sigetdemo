﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Profesor_calificaForo_resumen_Default : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    HfBackCounter.Value = (int.Parse(HfBackCounter.Value) - 1).ToString();
    HlBack.NavigateUrl = "javascript:history.go(" + HfBackCounter.Value + ");";

    if (Session["Resumen_IdEvaluacion"] != null)
    {
      // prepara los campos 
      HfIdEvaluacion.Value = Session["Resumen_IdEvaluacion"].ToString();
      HfIdGrupo.Value = Session["Resumen_IdGrupo"].ToString();
      GVdatos.DataBind();
      GVdatos.Caption = "Calificaciones de " +
        Siget.Lang.Etiqueta.ArtDet_Alumnos[Session["Usuario_Idioma"].ToString()] + " " + Siget.Lang.Etiqueta.Alumnos[Session["Usuario_Idioma"].ToString()] +
        " de " + Siget.Lang.Etiqueta.ArtDet_Grupo[Session["Usuario_Idioma"].ToString()] + " " + Siget.Lang.Etiqueta.Grupo[Session["Usuario_Idioma"].ToString()] +
        " " + Session["Resumen_Grupo"].ToString() + ", en la actividad " + Session["Resumen_Evaluacion"].ToString();
    }

  }
}