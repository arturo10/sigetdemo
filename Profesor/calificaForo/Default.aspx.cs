﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Profesor_calificaForo_Default : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    Session["Login"] = User.Identity.Name;

    if (Session["Comenta_Nuevo"] != null)
    {
      // si el profesor comentó en un foro, 
      // reestablezco el estado de la página a la actividad en donde comentó
      DDLcicloescolar.DataBind();
      DDLcicloescolar.SelectedValue = Session["Comenta_IdCiclo"].ToString();
      //SDSgrados.DataBind();
      DDLGrado.DataBind();
      DDLGrado.SelectedValue = Session["Comenta_IdGrado"].ToString();
      DDLasignatura.DataBind();
      DDLasignatura.SelectedValue = Session["Comenta_IdAsignatura"].ToString();
      DDLcalificacion.DataBind();
      DDLcalificacion.SelectedValue = Session["Comenta_IdCalificacion"].ToString();
      DDLactividad.DataBind();
      DDLactividad.SelectedValue = Session["Comenta_IdEvaluacion"].ToString();
      GVgrupos.DataBind();
      GVgrupos.PageIndex = int.Parse(Session["Comenta_GvPage"].ToString());
      GVgrupos.SelectedIndex = int.Parse(Session["Comenta_GvRow"].ToString());

      Session.Remove("Comenta_Nuevo");
    }

    CargaComentarios();

    // cargo en sesión el estado de la página para regresar a ella si comenta o edita
    Session["Comenta_IdCiclo"] = DDLcicloescolar.SelectedValue;
    Session["Comenta_IdGrado"] = DDLGrado.SelectedValue;
    Session["Comenta_IdAsignatura"] = DDLasignatura.SelectedValue;
    Session["Comenta_IdCalificacion"] = DDLcalificacion.SelectedValue;
    Session["Comenta_IdEvaluacion"] = DDLactividad.SelectedValue;
    if (GVgrupos.SelectedIndex != -1)
    {
      Session["Comenta_GvPage"] = GVgrupos.PageIndex;
      Session["Comenta_GvRow"] = GVgrupos.SelectedIndex;
      Session["Comenta_IdGrupo"] = GVgrupos.SelectedDataKey[0];
    }
  }

  protected void Page_PreRender(object sender, EventArgs e)
  {
    // si al recargar la página encuentro que calificó a alguien, establece la calificación
    // en todos los comentarios, y borra la sesión que lo indica
    if (Session["Comm_Calificacion"] != null)
    {
      foreach (UserControl uc in PnlComentarios.Controls)
      {
        Controls_ComentarioForo c = uc as Controls_ComentarioForo;
        if (c != null)
        {
          if (c.IdAlumno == long.Parse(Session["Comm_IdAlumno"].ToString()))
          {
            c.Calificacion = int.Parse(Session["Comm_Calificacion"].ToString());
            c.MuestraPropiedades();
          }
        }
      }

      Session.Remove("Comm_Calificacion");
      Session.Remove("Comm_IdAlumno");
    }

    if (Session["Comenta_Deleted"] != null)
    {
      // al borrar un comentario, el comentario se elimina de la base de datos, del boton de borrarpero el evento click() 
      // se ejecuta despues del onLoad , donde se cargan los comentarios, por lo que el cambio no se ve reflejado.
      // (borra de la base de datos después de presentar los comentarios; habrá uno fantasma)
      // por lo tanto, muestro un diálogo de aviso de borrado (con postback) para que el usuario recargue la página y 
      // se ejecute onload() cargando los comentarios ya actualizados.
      // No se puede borrar un comentario de la lista de PnlComentarios.Controls por que no se guarda en el viewstate,
      // y si alguien borra un comentario y edita otro inmediatamente después, al editar se referencia a un comentario erróneo
      // que podría estar una posición adelante del comentario borrado.
      Session.Remove("Comenta_Deleted");
      lblMensajeInicio.Text = Siget.Lang.FileSystem.Alumno.Actividad.Msg_ComentarioBorrado[Session["Usuario_Idioma"].ToString()];
      ModalPopupMessage.Show();
    }

    if (Session["Comenta_Editado"] != null)
    {
      Session.Remove("Comenta_Editado");
      lblMensajeInicio.Text = "Los cambios se guardaron correctamente.";
      ModalPopupMessage.Show();
    }

    if (Session["Comenta_Guardado"] != null)
    {
      Session.Remove("Comenta_Guardado");
      lblMensajeInicio.Text = "Se publicó exitosamente el comentario.";
      ModalPopupMessage.Show();
    }
  }

  /// <summary>
  /// Despliega y configura una lista de controles ComentarioForo con los comentarios existentes de la evaluación.
  /// </summary>
  /// <remarks></remarks>
  protected void CargaComentarios()
  {
    Boolean hasComments = false;
    msgInfo.hide();
   
    if (GVgrupos.SelectedIndex != -1 && DDLactividad.SelectedValue != "0")
    {
      try
      {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
        {
          conn.Open();
          using (SqlCommand cmd = new SqlCommand("Consulta_ForoComentarios", conn))
          {
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
           
            cmd.Parameters.AddWithValue("@IdEvaluacion", DDLactividad.SelectedValue);
            cmd.Parameters.AddWithValue("@IdGrupo", GVgrupos.SelectedDataKey[0]);
            if (DdlAlumnos.SelectedValue == "0")
                cmd.Parameters.AddWithValue("@IdAlumno", DBNull.Value);
            else
              cmd.Parameters.AddWithValue("@IdAlumno", DdlAlumnos.SelectedValue);
              

            using (SqlDataReader results = cmd.ExecuteReader())
            {
              ASP.controls_comentarioforo_ascx comentario = default(ASP.controls_comentarioforo_ascx);

              while (results.Read())
              {
                hasComments = true;

                comentario = new ASP.controls_comentarioforo_ascx();

                comentario.IdComentarioEvaluacion = int.Parse(results["IdComentarioEvaluacion"].ToString());
                comentario.IdEvaluacion = int.Parse(results["IdEvaluacion"].ToString());

                if (results["IdProfesor"] != DBNull.Value)
                {
                  comentario.IdProfesor = int.Parse(results["IdProfesor"].ToString());
                  if (results["IdProfesor"].ToString() == Session["Usuario_IdProfesor"].ToString())
                    comentario.EsEditable = true;
                }
                else
                {
                  comentario.IdAlumno = int.Parse(results["IdAlumno"].ToString());
                  comentario.EsCalificable = true;
                }

                if (results["ArchivoAdjunto"] != DBNull.Value)
                {
                  comentario.ArchivoAdjunto = results["ArchivoAdjunto"].ToString();
                  comentario.ArchivoAdjuntoFisico = results["ArchivoAdjuntoFisico"].ToString();
                  comentario.Usuario = results["Login"].ToString();
                }

                comentario.NombreUsuario = results["Nombre"].ToString().Trim();
                comentario.Fecha = DateTime.Parse(results["Fecha"].ToString());
                if (results["Calificacion"] != DBNull.Value)
                {
                  comentario.Calificacion = Int16.Parse(results["Calificacion"].ToString());
                  comentario.IdEvaluacionT = long.Parse(results["IdEvaluacionT"].ToString());
                }
                comentario.Comentario = results["Comentario"].ToString();

                PnlComentarios.Controls.Add(comentario);
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
      }
    }

    if (!hasComments) { }
      msgInfo.show("No hay comentarios en ésta actividad No comments");
  }

  protected void DDLactividad_SelectedIndexChanged(object sender, EventArgs e)
  {
    PnlComentarios.Controls.Clear();
  }
  protected void DDLasignatura_DataBound(object sender, EventArgs e)
  {
    DDLasignatura.Items.Insert(0, new ListItem("---Elija " + Siget.Config.Etiqueta.ARTIND_ASIGNATURA + " " + Siget.Config.Etiqueta.ASIGNATURA, "0"));
  }
  protected void DDLcalificacion_DataBound(object sender, EventArgs e)
  {
    DDLcalificacion.Items.Insert(0, new ListItem("---Elija una Calificación", "0"));
  }
  protected void DDLactividad_DataBound(object sender, EventArgs e)
  {
    if (DDLactividad.Items.Count == 0)
      DDLactividad.Items.Insert(0, new ListItem("No hay actividades tipo foro", "0"));
    else
      DDLactividad.Items.Insert(0, new ListItem("---Elija una Actividad", "0"));
  }
  protected void GVgrupos_SelectedIndexChanged(object sender, EventArgs e)
  {
    PnlComentarios.Controls.Clear();
    CargaComentarios();
  }

  protected void BtnConsultar_Click(object sender, EventArgs e)
  {

  }

  protected void BtnComentar_Click(object sender, EventArgs e)
  {
    Response.Redirect("~/Profesor/comentaForo/");
  }

  protected void BtnResumen_Click(object sender, EventArgs e)
  {
    Session["Resumen_Evaluacion"] = DDLactividad.SelectedItem.Text;
    Session["Resumen_IdEvaluacion"] = DDLactividad.SelectedValue;
    Session["Resumen_Grupo"] = GVgrupos.SelectedRow.Cells[3].Text.Trim();
    Session["Resumen_IdGrupo"] = GVgrupos.SelectedDataKey[0];
    Response.Redirect("~/Profesor/calificaForo/resumen/");
  }
  protected void DdlAlumnos_DataBound(object sender, EventArgs e)
  {
    DdlAlumnos.Items.Insert(0, new ListItem("---Todos", "0"));
  }
}