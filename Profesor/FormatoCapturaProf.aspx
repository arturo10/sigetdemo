<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="FormatoCapturaProf.aspx.vb"
    Inherits="capturista_FormatoCaptura"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style11 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            text-align: center;
        }

        .style40 {
            text-align: right;
        }

        .style41 {
            height: 29px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }

        .style42 {
            width: 624px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Evaluaciones - Capturar Actividad Individual
    </h1>
    Permite capturar las respuestas que hizo 
    <asp:Label ID="Label2" runat="server" Text="[UN]" />
    <asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
    en una actividad de opci�n m�ltiple o respuesta abierta.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td colspan="4" class="style41">
                <asp:Label ID="LblTitulo" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="4">
                <asp:Panel ID="PnlRespuestas" runat="server" BackColor="#E3EAEB"
                    HorizontalAlign="Left">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="4">
                <asp:Button ID="BtnAceptarResp" runat="server" OnClientClick="this.disabled=true"
                    Text="  Aceptar Respuestas  " UseSubmitBehavior="False"
                    Visible="False" CssClass="defaultBtn btnThemeBlue btnThemeWide" />
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="4">
                <asp:Label ID="LblM" runat="server" Text="Si alguna de las respuestas superiores fue capturada err�neamente, puede corregir en la parte inferior. Si todo esta correcto de clic al bot�n Aceptar Respuestas." Visible="False"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td class="style11" colspan="4">CAPTURAR LAS RESPUESTAS</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:Panel ID="PnlCaptura" runat="server" BackColor="#E3EAEB"
                    HorizontalAlign="Left">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="text-align: right" colspan="2" class="style42">
                <asp:Button ID="BtnAceptar" runat="server" OnClientClick="this.disabled=true"
                    Text="Aceptar" UseSubmitBehavior="False"
                    CssClass="defaultBtn btnThemeBlue btnThemeWide" />
                &nbsp;
                            <asp:Button ID="BtnCancelar" runat="server"
                                PostBackUrl="~/capturista/LlenarEvaluaciones.aspx" Text="Cancelar"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
            <td style="text-align: right" colspan="2">
                <asp:Button ID="BtnTerminar" runat="server" 
                    BackColor="#990000"
                    CssClass="defaultBtn btnThemeMedium" ForeColor="White" Text="Renunciar" />
            </td>
        </tr>
        <tr>
            <td class="style40">
                &nbsp;</td>
            <td class="style40" colspan="2">
                &nbsp;</td>
            <td class="style40">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Men�
                </asp:HyperLink>
            </td>
            <td class="style40" colspan="3">
                <asp:LinkButton ID="LinkButton1" runat="server"
                    PostBackUrl="LlenarEvaluacionesProf.aspx?Otro=1"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium">Capturar otro</asp:LinkButton>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSrespuestas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Respuesta]">
                    <InsertParameters>
                        <asp:Parameter Direction="Output" Name="NuevoIdresp" Size="8" Type="Int32" />
                    </InsertParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSrespuestasabiertas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [RespuestaAbierta]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluacionesterminadas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [EvaluacionTerminada]"></asp:SqlDataSource>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

