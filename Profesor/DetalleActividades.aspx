<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="DetalleActividades.aspx.vb"
    Inherits="profesor_DetalleActividades"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
        <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style13 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
            height: 42px;
        }

        .style14 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            text-align: left;
        }

        .style15 {
            font-family: Arial, Helvetica, sans-serif;
            text-align: left;
        }

        .style16 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style25 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            height: 8px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }

        .style33 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            height: 42px;
        }

        .style34 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            text-align: left;
            height: 36px;
        }

        .style35 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            height: 36px;
        }

        .style37 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            height: 15px;
            width: 268435584px;
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Resultados - Actividades
    </h1>
    Presenta una matriz con el detalle de los resultados obtenidos en las actividades de 
    <asp:Label ID="Label8" runat="server" Text="[UNA]" /> 
	<asp:Label ID="Label1" runat="server" Text="[ASIGNATURA]" />
     de tod<asp:Label ID="Label14" runat="server" Text="[O]" />s 
    <asp:Label ID="Label9" runat="server" Text="[LOS]" /> 
	<asp:Label ID="Label2" runat="server" Text="[ALUMNOS]" />
     de cada 
	<asp:Label ID="Label3" runat="server" Text="[GRUPO]" />
     que tenga asignad<asp:Label ID="Label10" runat="server" Text="[O]" /> como 
	<asp:Label ID="Label4" runat="server" Text="[PROFESOR]" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style13" colspan="4">
                <asp:Label ID="Label5" runat="server" Text="[CICLO]" />
            </td>
            <td class="style37" colspan="2">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px"
                    Width="330px">
                </asp:DropDownList>
            </td>
            <td class="style33"></td>
        </tr>
        <tr>
            <td class="style25">&nbsp;</td>
            <td class="style25" colspan="2">
                <asp:Label ID="Label7" runat="server" Text="[ASIGNATURA]" />
                que desea revisar</td>
            <td class="style11" colspan="2">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSmaterias" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Width="330px">
                </asp:DropDownList>
            </td>
            <td class="style11" colspan="2">&nbsp;</td>
        </tr>
        

        <tr>
            <td class="style15" colspan="6">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Men�
                </asp:HyperLink>
            </td>
            <td class="style35"></td>
        </tr>
        <tr>
            <td class="style14" colspan="6">Elija <asp:Label ID="Label11" runat="server" Text="[UN]" />
                <asp:Label ID="Label6" runat="server" Text="[GRUPO]" />
                 de <asp:Label ID="Label12" runat="server" Text="[LOS]" /> 
                asignad<asp:Label ID="Label13" runat="server" Text="[O]" />s a su cuenta:</td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7">
                <asp:GridView ID="GVdatos" runat="server"
                    AllowPaging="True"
                    AllowSorting="True"
                    AutoGenerateColumns="False"

                    Width="880px"
                    DataSourceID="SDSgrupos"
                    DataKeyNames="IdGrupo,IdGrado,IdPlantel"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo" InsertVisible="False"
                            ReadOnly="True" SortExpression="IdGrupo" Visible="False" />
                        <asp:BoundField DataField="IdProfesor" HeaderText="IdProfesor"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdProfesor"
                            Visible="False" />
                        <asp:BoundField DataField="Grupo" HeaderText="Grupo" SortExpression="Grupo" />
                        <asp:BoundField DataField="IdGrado" HeaderText="IdGrado"
                            SortExpression="IdGrado" InsertVisible="False" ReadOnly="True"
                            Visible="False" />
                        <asp:BoundField DataField="Grado" HeaderText="Grado" SortExpression="Grado" />
                        <asp:BoundField DataField="Nivel" HeaderText="Nivel"
                            SortExpression="Nivel" />
                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel"
                            Visible="False" />
                        <asp:BoundField DataField="Plantel" HeaderText="Plantel"
                            SortExpression="Plantel" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
                    0  select
                    1  idgrupo
                    2  idprofesor
                    3  GRUPO
                    4  idgrado
                    5  GRADO
                    6  NIVEL
                    7  idplantel
                    8  PLANTEL
                --%>
            </td>
        </tr>
        <tr>
            <td class="style16" colspan="3">&nbsp;</td>
            <td class="style11" colspan="2">&nbsp;</td>
            <td class="style11" colspan="2">&nbsp;</td>
        </tr>
        
        <tr>
            <td colspan="7">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style15" colspan="7">
                <asp:GridView ID="GValumnos" runat="server"
                    AllowSorting="True"

                    Width="880px"
                    DataSourceID="SDSdetalleactividades"

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                
            </td>
        </tr>
        <tr>
            <td colspan="7" style="font-size: small;">
                <asp:Panel ID="pnlNota" runat="server" Visible="false">
                    Nota: * significa que la actividad fue realizada pero no ha sido calificada.
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style15">&nbsp;</td>
            <td colspan="5">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style15" colspan="6" style="text-align: left;">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Men�
                </asp:HyperLink>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select distinct P.IdProfesor, G.IdGrupo, G.Descripcion as Grupo, Gr.IdGrado, Gr.Descripcion Grado, N.Descripcion as Nivel, Pl.IdPlantel, Pl.Descripcion as Plantel
from Usuario U, Profesor P, Grupo G, Grado Gr, Nivel N, Plantel Pl, Programacion Pr
where U.Login = @Login and P.IdUsuario = U.IdUsuario and Pr.IdProfesor = P.IdProfesor
and Pr.IdCicloEscolar = @IdCicloEscolar and G.IdGrupo = Pr.IdGrupo and
 Pl.IdPlantel = G.IdPlantel and Gr.IdGrado = G.IdGrado and N.IdNivel = Gr.IdNivel
and G.IdCicloEscolar = @IdCicloEscolar
order by Pl.Descripcion, N.Descripcion, Gr.Descripcion, G.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSdetalleactividades" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SP_resultadoactividades2" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:SessionParameter Name="RolIndicador" SessionField="RolIndicador"  />
                       <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:SessionParameter Name="IdPlantel" SessionField="IdPlantel_Profesor"
                            Type="Int32" />
                        <asp:SessionParameter Name="IdAsignatura" SessionField="IdAsignatura"
                            Type="Int32" />
                        <asp:SessionParameter Name="IdGrupo" SessionField="IdGrupo"
                            Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSmaterias" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdAsignatura, Descripcion
from Asignatura
where IdAsignatura in (select IdAsignatura from Programacion where IdProfesor in (select IdProfesor from Profesor Pr, Usuario U where U.Login = @Login and Pr.IdUsuario = U.IdUsuario) and IdCicloEscolar = @IdCicloEscolar )
order by descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            
        </tr>
        <tr>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

