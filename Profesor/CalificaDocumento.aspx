﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="CalificaDocumento.aspx.vb"
    Inherits="profesor_CalificaDocumento"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 34px;
        }

        .style13 {
            width: 219px;
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
        }

        .style16 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            text-align: left;
        }

        .style24 {
            height: 35px;
        }

        .style46 {
            width: 630px;
            font-weight: normal;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Evaluaciones - Calificar Documento
    </h1>
    Permite calificar una actividad de Entrega de Archivo de 
    <asp:Label ID="Label6" runat="server" Text="[LOS]" /> 
	<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
     de sus 
	<asp:Label ID="Label2" runat="server" Text="[GRUPOS]" />
    asignad<asp:Label ID="Label7" runat="server" Text="[O]" />s como 
	<asp:Label ID="Label3" runat="server" Text="[PROFESOR]" />.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td colspan="4" class="style23">Calificar documentos entregados por 
								<asp:Label ID="Label4" runat="server" Text="[ALUMNOS]" />
            </td>
        </tr>
        <tr>
            <td class="style13">
                &nbsp;</td>
            <td class="style13">
                <asp:Label ID="Label5" runat="server" Text="[CICLO]" />
            </td>
            <td class="estandar">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px" Width="323px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13">
                &nbsp;</td>
            <td class="style13">
                Estado de Actividades a mostrar
            </td>
            <td class="estandar">
                <asp:DropDownList ID="DdlEstatusActividad" runat="server" AutoPostBack="True"
                    Width="323px">
                    <asp:ListItem Value="Sin iniciar">Sin iniciar</asp:ListItem>
                    <asp:ListItem Value="Iniciada">Iniciada</asp:ListItem>
                    <asp:ListItem Value="Terminada">Terminada</asp:ListItem>
                    <asp:ListItem Value="Cancelada">Cancelada</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: left;">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
                <div style="float:right;">
                <asp:HyperLink ID="HyperLink2" runat="server" 
                    NavigateUrl="~/profesor/CalificaDocumentoNuevo.aspx"
                    CssClass="defaultBtn btnThemeBlue btnThemeWide">
                    Buscar Documentos Nuevos
                </asp:HyperLink>
                </div>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style15" colspan="3">
                <asp:GridView ID="GVgrupos" runat="server" 
                    AllowPaging="True"
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 
                    Caption="<h3>GRUPOS que tiene asignados actualmente</h3>"
                    
                    DataSourceID="SDSgrupos" 
                    Width="873px" 
                    DataKeyNames="IdGrupo,IdGrado,IdPlantel"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdProfesor" HeaderText="IdProfesor"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdProfesor"
                            Visible="False" />
                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo" InsertVisible="False"
                            ReadOnly="True" SortExpression="IdGrupo" Visible="False" />
                        <asp:BoundField DataField="Grupo" HeaderText="[GRUPO]" SortExpression="Grupo" />
                        <asp:BoundField DataField="IdGrado" HeaderText="IdGrado"
                            SortExpression="IdGrado" InsertVisible="False" ReadOnly="True"
                            Visible="False" />
                        <asp:BoundField DataField="Grado" HeaderText="[GRADO]" SortExpression="Grado" />
                        <asp:BoundField DataField="Nivel" HeaderText="[NIVEL]"
                            SortExpression="Nivel" />
                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel"
                            Visible="False" />
                        <asp:BoundField DataField="Plantel" HeaderText="[PLANTEL]"
                            SortExpression="Plantel" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- 
                    sort YES - rowdatabound
                    --%>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style11" colspan="4">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="4">
                <asp:GridView ID="GVactividades" runat="server"
                    AutoGenerateColumns="False" 
                    AllowPaging="True" 
                    AllowSorting="True" 
                    PageSize="15"

                    DataKeyNames="IdEvaluacion,IdAsignatura"
                    DataSourceID="SDSdatos" 
                    Width="873px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdEvaluacion" HeaderText="IdEvaluacion"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion"
                            Visible="False" />
                        <asp:BoundField DataField="Evaluacion (Actividad)"
                            HeaderText="Evaluacion (Actividad)" SortExpression="Evaluacion (Actividad)" />
                        <asp:BoundField DataField="Abreviación" HeaderText="Abreviación"
                            SortExpression="Abreviación" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                        <asp:BoundField DataField="Periodo Califica" HeaderText="Periodo Califica"
                            SortExpression="Periodo Califica" />
                        <asp:BoundField DataField="Asignatura" HeaderText="[ASIGNATURA]"
                            SortExpression="Asignatura" />
                        <asp:BoundField DataField="IdAsignatura" HeaderText="IdAsignatura"
                            InsertVisible="False" SortExpression="IdAsignatura" Visible="False" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- 
                    sort YES - rowdatabound
                    --%>
            </td>
        </tr>
        <tr>
            <td class="style24" colspan="2">
                &nbsp;</td>
            <td class="style24"></td>
            <td class="style24"></td>
        </tr>
        <tr>
            <td class="style15" colspan="2">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select distinct P.IdProfesor, G.IdGrupo, G.Descripcion as Grupo, Gr.IdGrado, Gr.Descripcion Grado, N.Descripcion as Nivel, Pl.IdPlantel, Pl.Descripcion as Plantel
from Usuario U, Profesor P, Grupo G, Grado Gr, Nivel N, Plantel Pl, Programacion Pr
where U.Login = @Login and P.IdUsuario = U.IdUsuario and Pr.IdProfesor = P.IdProfesor
and Pr.IdCicloEscolar = @IdCicloEscolar and G.IdGrupo = Pr.IdGrupo and
 Pl.IdPlantel = G.IdPlantel and Gr.IdGrado = G.IdGrado and N.IdNivel = Gr.IdNivel
and G.IdCicloEscolar = @IdCicloEscolar
order by Pl.Descripcion, N.Descripcion, Gr.Descripcion, G.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSdatos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select E.IdEvaluacion, E.ClaveBateria as 'Evaluacion (Actividad)', E.ClaveAbreviada as 'Abreviación', E.Tipo, C.Descripcion 'Periodo Califica', A.Descripcion Asignatura, A.IdAsignatura
from Calificacion C, Evaluacion E, Asignatura A, Grado G, Escuela Es
     where G.IdGrado = @IdGrado
     and Es.IdNivel = G.IdNivel and Es.IdPlantel = @IdPlantel
     and A.IdGrado = G.IdGrado and C.IdAsignatura = A.IdAsignatura and A.IdAsignatura
     in (select IdAsignatura from Programacion where IdProfesor in (select IdProfesor from Profesor Pr, Usuario U where U.Login = @Login and Pr.IdUsuario = U.IdUsuario) 
and IdGrupo =@IdGrupo) and C.IdCicloEscolar = @IdCicloEscolar
and E.IdCalificacion = C.IdCalificacion and E.SeEntregaDocto = 'True'
                    and E.Estatus = 'Sin iniciar'
order by C.Consecutivo, E.InicioContestar">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdGrado" SessionField="IdGrado" />
                        <asp:SessionParameter Name="IdPlantel" SessionField="IdPlantel" />
                        <asp:SessionParameter DefaultValue="" Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="GVgrupos" DefaultValue="" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                      
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

