﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="CalificaRespuestaAbierta.aspx.vb"
    Inherits="profesor_CalificaRespuestaAbierta"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-family: Arial;
            font-size: small;
            font-weight: bold;
            height: 38px;
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style33 {
            height: 34px;
            text-align: left;
        }

        .style32 {
            height: 22px;
            width: 479px;
            font-weight: normal;
            font-size: small;
        }

        .style34 {
            height: 19px;
        }

        .style35 {
            font-family: Arial;
            font-size: small;
            font-weight: bold;
            height: 21px;
        }

        .style37 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000066;
        }

        .auto-style1 {
            width: 268435312px;
        }

        .auto-style2 {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Evaluaciones - Calificar Respuestas Abiertas
    </h1>
    Permite desplegar todas las respuestas abiertas escritas por 
    <asp:Label ID="Label4" runat="server" Text="[LOS]" />
    <asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    en una actividad específica, y calificarlas.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style24" colspan="2">
                <asp:Label ID="Label2" runat="server" Text="[CICLO]" />
            </td>
            <td colspan="4" style="text-align: left">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px"
                    Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="6" class="style33">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
                <div style="float: right;">
                    <asp:HyperLink ID="HyperLink2" runat="server"
                        NavigateUrl="~/profesor/CalificaRespAbiertaNueva.aspx"
                        CssClass="defaultBtn btnThemeBlue btnThemeWide">
                    Buscar Respuestas Nuevas
                    </asp:HyperLink>
                </div>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2" class="style24" style="vertical-align: top;">
                <asp:Label ID="Label5" runat="server" Text="[GRADO]" /></td>
            <td colspan="4" style="text-align: left; vertical-align: top;" class="auto-style1">
                <asp:DropDownList ID="DDLGrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion"
                    DataValueField="IdGrado" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="style24" style="vertical-align: top;">
                <asp:Label ID="Label3" runat="server" Text="[ASIGNATURA]" /></td>
            <td colspan="4" style="text-align: left; vertical-align: top;" class="auto-style1">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSasignatura" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;
            </td>
            <td colspan="2" class="auto-style2">Mostrar:&nbsp;
                <asp:DropDownList ID="ddlEstado" runat="server" AutoPostBack="true"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: medium">
                    <asp:ListItem Value="Enviado" Selected="true">Envíos</asp:ListItem>
                    <asp:ListItem Value="Borrador">Borradores</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="GVdatos" runat="server"
                    AllowPaging="True"
                    AllowSorting="True"
                    AutoGenerateColumns="False"
                    Caption="<h3>Seleccione alguno de los GRUPOS que tiene asignados</h3>"

                    DataSourceID="SDSgrupos"
                    DataKeyNames="IdProfesor,IdGrupo,IdGrado,IdPlantel,IdProgramacion"
                    Width="873px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell">
                            <HeaderStyle Width="90px" />
                        </asp:CommandField>
                        <asp:BoundField DataField="IdProfesor" HeaderText="IdProfesor" InsertVisible="False"
                            ReadOnly="True" SortExpression="IdProfesor" Visible="False" />
                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdGrupo"
                            Visible="False" />
                        <asp:BoundField DataField="Grupo" HeaderText="Grupo" SortExpression="Grupo">
                            <HeaderStyle Width="100px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdGrado" HeaderText="IdGrado"
                            SortExpression="IdGrado" InsertVisible="False" ReadOnly="True"
                            Visible="False" />
                        <asp:BoundField DataField="Grado" HeaderText="Grado" SortExpression="Grado" InsertVisible="False" ReadOnly="True"
                            Visible="False" />
                        <asp:BoundField DataField="Nivel" HeaderText="Nivel"
                            SortExpression="Nivel" />
                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel"
                            Visible="False" />
                        <asp:BoundField DataField="Plantel" HeaderText="Plantel"
                            SortExpression="Plantel" />
                        <asp:BoundField DataField="IdProgramacion" HeaderText="IdProgramacion" InsertVisible="False" ReadOnly="True" SortExpression="IdProgramacion" Visible="False" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
                    0  select
                    1  idgrupo
                    2  idprofesor
                    3  GRUPO
                    4  idgrado
                    5  GRADO
                    6  NIVEL
                    7  idplantel
                    8  PLANTEL
                --%>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="style24">Calificación</td>
            <td colspan="4" style="text-align: left" class="auto-style1">
                <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True"
                    DataSourceID="SDScalificaciones" DataTextField="Descripcion"
                    DataValueField="IdCalificacion" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="style24">Actividad</td>
            <td colspan="4" style="text-align: left;" class="auto-style1">
                <asp:DropDownList ID="DDLactividad" runat="server" AutoPostBack="True"
                    DataSourceID="SDSactividades" DataTextField="ClaveBateria"
                    DataValueField="IdEvaluacion" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="6" class="style34">
                <asp:GridView ID="GValumnos0" runat="server" 
                    AllowSorting="True"
                    AutoGenerateColumns="False" 
                    Caption="ARCHIVOS DE APOYO PARA EL PROFESOR"

                    DataSourceID="SDSapoyos" 
                    Width="393px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                            SortExpression="Consecutivo">
                            <HeaderStyle Width="60px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripción"
                            SortExpression="Descripcion">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ArchivoApoyo" HeaderText="ArchivoApoyo"
                            SortExpression="ArchivoApoyo" Visible="False" />
                        <asp:HyperLinkField DataNavigateUrlFields="ArchivoApoyo"
                            DataNavigateUrlFormatString="~/Resources/apoyo/{0}" DataTextField="ArchivoApoyo"
                            HeaderText="Archivo" Target="_blank" ItemStyle-CssClass="columnaHyperlink">
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:HyperLinkField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="GVrespuestas" runat="server"
                    AllowSorting="True"
                    AutoGenerateColumns="False"
                    Caption="<h3>Respuestas Abiertas</h3>"

                    DataKeyNames="IdRespuesta,SeCalifica"
                    DataSourceID="SDSrespuestas"
                    Width="885px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField SelectText="Calificar" ShowSelectButton="True" ItemStyle-CssClass="selectCell">
                            <HeaderStyle Width="50px" />
                        </asp:CommandField>
                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                            SortExpression="Consecutivo">
                            <HeaderStyle Width="60px" HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Reactivo" HeaderText="Reactivo"
                            SortExpression="Reactivo">
                            <HeaderStyle Width="150px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdRespuesta" HeaderText="IdRespuesta"
                            SortExpression="IdRespuesta"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="Respuesta" HeaderText="Respuesta"
                            SortExpression="Respuesta" />
                        <asp:BoundField DataField="Calificacion" HeaderText="Calificación"
                            SortExpression="Calificacion" />
                        <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                            SortExpression="Matricula">
                            <HeaderStyle Width="60px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NomAlumno" HeaderText="Nombre del Alumno" ReadOnly="True"
                            SortExpression="NomAlumno">
                            <HeaderStyle Width="140px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                            SortExpression="IdAlumno"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="SeCalifica" HeaderText="SeCalifica"
                            SortExpression="SeCalifica"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="Ponderacion" HeaderText="Ponderacion"
                            SortExpression="Ponderacion"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="IdRespuestaA" HeaderText="IdRespuestaA"
                            SortExpression="IdRespuestaA"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        
                        <asp:BoundField DataField="Respuesta" HeaderText="MarkupRespuesta"
                            SortExpression="Respuesta"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        
                        <asp:BoundField DataField="Reactivo" HeaderText="MarkupReactivo"
                            SortExpression="Reactivo"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
                    0  select
                    1  consecutivo
                    2  reactivo
                    3  idrespuesta
                    4  respuesta
                    5  matricula
                    6  NOMBRE DEL ALUMNO
                --%>
            </td>
        </tr>
        <tr>
            <td colspan="6" class="style34">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: left">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td colspan="3" style="text-align: left">&nbsp;</td>
        </tr>
    </table>

    <asp:Panel ID="PnlResp" runat="server" CssClass="dialogOverwrite dialogRespuestaAbierta">
        <div class="style37" style="margin-bottom: 20px;">
            Contenido de la Respuesta
        </div>
        <div style="text-align: left; border: 2px solid #444; padding: 10px; width: 766px; max-height: 300px; overflow-y: scroll;">
            <asp:Literal ID="TBrespuesta" runat="server"></asp:Literal>
        </div>
        <div style="margin-top: 20px;">
            <asp:DropDownList ID="DDLcalifica" runat="server"
                Style="font-family: Arial, Helvetica, sans-serif; font-size: medium">
            </asp:DropDownList>
            &nbsp;
            <asp:Button ID="BtnCalificar" runat="server" Text="Calificar"
                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            &nbsp;
            <asp:Button ID="btnDialogClose" runat="server" Text="Cerrar"
                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />

              <div style="width:30%;text-align:center;">
                <label><b>Número de palabras de la respuesta:</b></label>
                <asp:Label runat="server" ID="NoPalabrasRespuesta" ></asp:Label>
            </div>
        </div>
    </asp:Panel>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select distinct P.IdProfesor, G.IdGrupo, G.Descripcion as Grupo, Gr.IdGrado, Gr.Descripcion Grado, N.Descripcion as Nivel, Pl.IdPlantel, Pl.Descripcion as Plantel, Pr.IdProgramacion
from Usuario U, Profesor P, Grupo G, Grado Gr, Nivel N, Plantel Pl, Programacion Pr
where U.Login = @Login and P.IdUsuario = U.IdUsuario and Pr.IdProfesor = P.IdProfesor
and Pr.IdCicloEscolar = @IdCicloEscolar and G.IdGrupo = Pr.IdGrupo and Pr.IdAsignatura = @IdAsignatura and 
 Pl.IdPlantel = G.IdPlantel and Gr.IdGrado = G.IdGrado and N.IdNivel = Gr.IdNivel and G.IdCicloEscolar = @IdCicloEscolar
order by Pl.Descripcion, N.Descripcion, Gr.Descripcion, G.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

                <%-- selecciona los grados de los grupos de las programaciones del profesor en ese ciclo escolar --%>
                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select distinct Gr.IdGrado, Gr.Descripcion
from Usuario U, Profesor P, Grupo G, Grado Gr, Nivel N, Plantel Pl, Programacion Pr
where U.Login = @Login and P.IdUsuario = U.IdUsuario and Pr.IdProfesor = P.IdProfesor
and Pr.IdCicloEscolar = @IdCicloEscolar and G.IdGrupo = Pr.IdGrupo and 
 Pl.IdPlantel = G.IdPlantel and Gr.IdGrado = G.IdGrado and N.IdNivel = Gr.IdNivel and G.IdCicloEscolar = @IdCicloEscolar
order by Gr.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSrespuestas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.Consecutivo, P.Redaccion as Reactivo, A.Calificacion, R.IdRespuesta, A.Redaccion as Respuesta,
Al.Matricula, Al.Nombre + ' ' + Al.ApePaterno + ' ' + Al.ApeMaterno as NomAlumno, O.SeCalifica, Al.IdAlumno, P.Ponderacion, A.IdRespuestaA
from Planteamiento P, Respuesta R, RespuestaAbierta A, Alumno Al, Opcion O
where R.IdDetalleEvaluacion in 
(select IdDetalleEvaluacion from DetalleEvaluacion where IdEvaluacion = @IdEvaluacion)
and R.IdGrupo = @IdGrupo and A.IdRespuesta = R.IdRespuesta and 
P.IdPlanteamiento = R.IdPlanteamiento and P.TipoRespuesta = 'Abierta' and
R.IdCicloEscolar = @IdCicloEscolar and Al.IdAlumno = R.IdAlumno and O.IdOpcion = R.IdOpcion and P.IdPlanteamiento = O.IdPlanteamiento
and Al.IdAlumno in (SELECT IdAlumno FROM EvaluacionTerminada WHERE IdEvaluacion = @IdEvaluacion) and A.Estado = @Estado
order by P.Consecutivo, NomAlumno">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLactividad" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="GVdatos" Name="IdGrupo"
                            PropertyName="SelectedDataKey.Values(1)" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="ddlEstado" Name="Estado"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSasignatura" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdAsignatura, Descripcion 
FROM Asignatura WHERE IdGrado = @IdGrado
AND IdAsignatura in (select IdAsignatura from Programacion where IdProfesor in (select P.IdProfesor from Profesor P, Usuario U where U.Login = @Login and P.IdUsuario = U.IdUsuario))
ORDER BY IdArea">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLGrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdCalificacion], [Descripcion] FROM [Calificacion] WHERE (([IdCicloEscolar] = @IdCicloEscolar) AND ([IdAsignatura] = @IdAsignatura)) ORDER BY [Consecutivo]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSactividades" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdEvaluacion], [ClaveBateria] FROM [Evaluacion] WHERE ([IdCalificacion] = @IdCalificacion) ORDER BY [InicioContestar], [ClaveBateria]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion"
                            PropertyName="SelectedValue" Type="Int64" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSapoyos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Distinct Consecutivo,Descripcion,ArchivoApoyo from
ApoyoEvaluacion
where IdProgramacion = @IdProgramacion and
IdEvaluacion = @IdEvaluacion
order by Consecutivo">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdProgramacion" SessionField="IdProgramacion" />
                        <asp:ControlParameter ControlID="DDLactividad" Name="IdEvaluacion" PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDScalculacalificacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Calificacion]"></asp:SqlDataSource>
            </td>
            <td></td>
            <td>
                <asp:HiddenField ID="HF1" runat="server" />
                <asp:ModalPopupExtender ID="HF1_ModalPopupExtender" runat="server"
                    Enabled="True" PopupControlID="PnlResp" TargetControlID="HF1" BackgroundCssClass="overlay">
                </asp:ModalPopupExtender>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>

