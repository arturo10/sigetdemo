﻿Imports Siget

Imports System.IO
Imports System.Data.SqlClient
Imports System.Data

Partial Class coordinador_ActividadesDocto
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = Trim(User.Identity.Name)
        Session("Exportar") = False
        TitleLiteral.Text = "Actividades con Documento"

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.GRUPO
        Label3.Text = Config.Etiqueta.ASIGNATURA
        Label4.Text = Config.Etiqueta.INSTITUCION
        Label5.Text = Config.Etiqueta.CICLO
        Label6.Text = Config.Etiqueta.PLANTEL
        Label7.Text = Config.Etiqueta.NIVEL
        Label8.Text = Config.Etiqueta.GRADO
        Label9.Text = Config.Etiqueta.GRUPO
        Label10.Text = Config.Etiqueta.ASIGNATURA
     
        Try
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("SELECT P.IdProfesor, P.Nombre, P.Apellidos FROM Usuario U, Profesor P where U.Login = @Login and P.IdUsuario = U.IdUsuario", conn)

                cmd.Parameters.AddWithValue("@Login", User.Identity.Name)

                Dim results As SqlDataReader = cmd.ExecuteReader()
                results.Read()
                Session("IdProfesor") = results.Item("IdProfesor")

                results.Close()
                cmd.Dispose()
                conn.Close()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try
    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRADO & " " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRUPO & " " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css

        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both
        GVactual.HeaderRow.Visible = True

        Dim headerText As String
        '' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GValumnos, "DetalleActividadesDoctos")
    End Sub

    Protected Sub GValumnos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GValumnos.RowDataBound

        If (e.Row.Cells.Count <= 5) And (DDLasignatura.SelectedIndex > 0) Then
            msgInfo.show("Est" & Config.Etiqueta.LETRA_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA & " no tiene actividades de entrega de documento.")
        Else
            msgInfo.hide()
            GValumnos.Caption = "<B> Listado de " & Config.Etiqueta.ALUMNOS &
                " de " & Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO &
                " " + DDLgrupo.SelectedItem.ToString +
                " con el resultado de las Actividades de ENTREGA DE ARCHIVO correspondientes a " &
                Config.Etiqueta.ARTDET_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA & " " + _
                    DDLasignatura.SelectedItem.ToString + " en " &
                    Config.Etiqueta.ARTDET_CICLO & " " & Config.Etiqueta.CICLO &
                    " " + DDLcicloescolar.SelectedItem.ToString + "<br>" + _
                    "Reporte emitido el " + Date.Today.ToShortDateString + "</B>"
            msgError.hide()
        End If
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(4).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.EQUIPO

        End If
    End Sub
    
    Protected Sub GValumnos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GValumnos.DataBound
        If GValumnos.Rows.Count > 0 Then
            BtnExportar.Visible = True
        Else
            BtnExportar.Visible = False
        End If
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GValumnos_PreRender(sender As Object, e As EventArgs) Handles GValumnos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables

        

        If GValumnos.Rows.Count > 0 Then

            'Dim row As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
            '' GValumnos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GValumnos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GValumnos.RowCreated


        If e.Row.RowType = DataControlRowType.Header Then

            ' GValumnos.HeaderRow.TableSection = TableRowSection.TableHeader
            Dim dv As DataView = CType(SDSdetalleactividades.Select(DataSourceSelectArguments.Empty), DataView)
            If dv.Table.Columns.Count > 6 Then
                Dim row As New GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert)
                row.Attributes.Add("style", "border-bottom:1px solid #bbb;")
                row.Font.Bold = True

                row.Font.Size = 14
                Dim cellAlumno As New TableCell()
                cellAlumno.HorizontalAlign = HorizontalAlign.Center
                cellAlumno.Text = "Datos del Alumno"
                cellAlumno.ColumnSpan = 6
                row.Cells.Add(cellAlumno)
                Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                    conn.Open()
                    Dim cmd As SqlCommand = New SqlCommand("SELECT C.Descripcion AS Descripcion,COUNT(E.IdEvaluacion) AS Conteo from Calificacion C" +
                                            " INNER JOIN Evaluacion E ON E.IdCalificacion=C.IdCalificacion" +
                                            " WHERE C.IdAsignatura=" + DDLasignatura.SelectedValue + " AND E.SeEntregaDocto='True'" +
                                            " GROUP BY C.Descripcion", conn)
                    Dim results As SqlDataReader = cmd.ExecuteReader()
                    While results.Read()
                        Dim cell As New TableCell()
                        cell.HorizontalAlign = HorizontalAlign.Center
                        cell.Text = results.Item("Descripcion")
                        cell.ColumnSpan = results.Item("Conteo")
                        row.Cells.Add(cell)
                    End While
                    results.Close()
                    cmd.Dispose()
                End Using
                GValumnos.Controls(0).Controls.AddAt(0, row)
            End If


            End If
           

            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Try
                        Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)

                        If Not IsNothing(lb) Then
                            ' quito el link button para reañadirlo después del link
                            tc.Controls.RemoveAt(0)
                            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en firefox)
                            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                            ' creo el ícono de sorting
                            Dim icon As Image = New Image()
                            icon.ImageUrl = Config.Global.urlImagenes & "btnicons/sortable.png"
                            ' ésta propiedad es importante para que se cargue a la izquierda
                            div.Attributes.Add("style", "float: left; position: absolute;")
                            ' añado el ícono al div
                            div.Controls.Add(icon)
                            ' añado el div al header
                            tc.Controls.Add(div)
                            ' reañado el link
                            tc.Controls.Add(lb)
                            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                            lb.Attributes.Add("style", "float: left; margin-left: 15px;text-decoration:none;")

                            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                            ' para asignarle un resaltado con css
                            If GValumnos.SortExpression = lb.CommandArgument Then
                                lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration:none;")
                            End If
                        End If
                    Catch Exception As Exception

                    End Try
                End If
            Next

    End Sub

    Protected Sub DDLasignatura_SelectedIndexChanged(sender As Object, e As EventArgs)
        If sender.SelectedValue = "0" And DDLgrupo.SelectedValue <> "0" Then
            GValumnos.Controls(0).Controls.RemoveAt(0)
        End If
    End Sub
End Class
