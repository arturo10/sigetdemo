﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="EstatusGrupo.aspx.vb"
    Inherits="profesor_EstatusGrupo"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            width: 100%;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Avance - Actividades
    </h1>
    Permite consultar el avance del total de 
	<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    de 
    <asp:Label ID="Label4" runat="server" Text="[LOS]" /> 
	<asp:Label ID="Label2" runat="server" Text="[GRUPOS]" />
    que tiene asignad<asp:Label ID="Label5" runat="server" Text="[O]" />s como 
	<asp:Label ID="Label3" runat="server" Text="[PROFESOR]" />
    que han realizado una actividad específica.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style11">
        <tr>
            <td>&nbsp;</td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GVdatos" runat="server"
                    AllowSorting="True"
                    Width="869px"

                    DataKeyNames="IdEvaluacion"
                    DataSourceID="SDSdatos"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
                    0  select
                    1  idevaluacion
                    2  E.ClaveBateria as 'Evaluacion (Actividad)'
                    3  E.ClaveAbreviada as 'Abreviación'
                    4  tipo
                    5  C.Descripcion 'Periodo Califica'
                    6  A.Descripcion Asignatura
                --%>
            </td>
        </tr>
        <tr>
            <td colspan="4">

            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/profesor/RevisaGrupo.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSdatos" runat="server"></asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

