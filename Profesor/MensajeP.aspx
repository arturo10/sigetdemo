﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="MensajeP.aspx.vb"
    Inherits="profesor_MensajeP"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .leftSpacer1 {
            width: 15px;
        }

        .rightSpacer1 {
            width: 15px;
        }

        .labelcolumn {
            text-align: right;
            width: 140px;
            font-size: small;
        }

        .contentColumn {
            border: 1px outset #bbb;
            padding: 2px;
            font-size: small;
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>
        <asp:Literal ID="lt_titulo" runat="server"></asp:Literal>
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <asp:Panel ID="pnlMensajeAlumno" runat="server">
        <table style="width: 100%;">
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_hintAlumno" runat="server">Alumno</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltEnvia" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_hintAsignatura" runat="server">Asignatura</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltAsignatura" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">Fecha de Envío
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltFechaEnvio" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">Asunto
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltAsunto" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">Contenido
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltContenido" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_hintAdjunto" runat="server">Archivo&nbsp;Adjunto</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:LinkButton ID="lbAdjunto" runat="server"
                        CssClass="LabelInfoDefault"></asp:LinkButton>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">&nbsp;</td>
                <td style="text-align: left;">
                    <asp:CheckBox ID="CBeliminar" runat="server"
                        Style="font-family: Arial, Helvetica, sans-serif; font-size: small"
                        Text="Marcar para eliminar este mensaje" />
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center;">
                    <asp:LinkButton ID="LBresponder" runat="server"
                        CssClass="defaultBtn btnThemeBlue btnThemeMedium">Responder</asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="LBeliminar" runat="server"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium">Eliminar</asp:LinkButton>
                    &nbsp;
                <asp:LinkButton ID="LBreenviar" runat="server"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium">Reenviar</asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel ID="pnlMensajeCoordinador" runat="server">
        <table style="width: 100%;">
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_hintCoordinador" runat="server">Coordinador</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltEnviaC" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_hintPlantel" runat="server">Plantel</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltPlantel" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">Fecha de Envío
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltFechaEnvioC" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">Asunto
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltAsuntoC" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">Contenido
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltContenidoC" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_hintAdjuntoC" runat="server">Archivo&nbsp;Adjunto</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:LinkButton ID="lbAdjuntoC" runat="server"
                        CssClass="LabelInfoDefault"></asp:LinkButton>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">&nbsp;</td>
                <td style="text-align: left;">
                    <asp:CheckBox ID="CBeliminarC" runat="server"
                        Style="font-family: Arial, Helvetica, sans-serif; font-size: small"
                        Text="Marcar para eliminar este mensaje" />
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center;">
                    <asp:LinkButton ID="LBresponderC" runat="server"
                        CssClass="defaultBtn btnThemeBlue btnThemeMedium">Responder</asp:LinkButton>
                    &nbsp;
                <asp:LinkButton ID="LBeliminarC" runat="server"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium">Eliminar</asp:LinkButton>
                    &nbsp;
                <asp:LinkButton ID="LBreenviarC" runat="server"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium">Reenviar</asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <table style="width: 100%;">
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
    </table>

    <table style="width: 100%;">
        <tr>
            <td colspan="4" style="text-align: left">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="javascript:history.go(-1);"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSmensaje" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [ComunicacionCP]"></asp:SqlDataSource>
            </td>
            <td>
                <asp:HiddenField ID="hfIdProgramacion" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfIdCoordinador" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfEmail" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hfAdjuntoFisico" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfIdAlumno" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfCarpetaAdjunto" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfIdProfesor" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
