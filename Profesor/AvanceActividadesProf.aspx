﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="AvanceActividadesProf.aspx.vb"
    Inherits="profesor_AvanceActividadesProf"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-family: Arial;
            font-size: small;
            font-weight: bold;
            height: 38px;
            color: #000000;
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Avance - Realizadas
    </h1>
    Permite consultar el avance del total de 
	<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    de 
    <asp:Label ID="Label8" runat="server" Text="[UN]" />
    <asp:Label ID="Label7" runat="server" Text="[GRUPO]" />
    que tiene asignad<asp:Label ID="Label9" runat="server" Text="[O]" />s como 
	<asp:Label ID="Label2" runat="server" Text="[PROFESOR]" />
    que han realizado todas las actividades de 
    <asp:Label ID="Label10" runat="server" Text="[UNA]" />
    <asp:Label ID="Label3" runat="server" Text="[ASIGNATURA]" />.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style23" colspan="6">Detalle en el Avance de una Actividad Realizada. Puede seleccionar 
                <asp:Label ID="Label11" runat="server" Text="[LOS]" />
                <asp:Label ID="Label4" runat="server" Text="[GRUPOS]" />
                que tiene asignad<asp:Label ID="Label12" runat="server" Text="[O]" />s.</td>
        </tr>
        <tr>
            <td class="style24" colspan="3">
                <asp:Label ID="Label5" runat="server" Text="[CICLO]" />
            </td>
            <td colspan="3">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px"
                    Width="330px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="GVdatos" runat="server" 
                    AllowPaging="True"
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 
                    Caption="<h3>Seleccione alguno de los GRUPOS que tiene asignados</h3>"
                    
                    DataSourceID="SDSgrupos" 
                    DataKeyNames="IdGrupo,IdGrado,IdPlantel" 
                    Width="873px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell">
                            <HeaderStyle Width="90px" />
                        </asp:CommandField>
                        <asp:BoundField DataField="IdProfesor" HeaderText="IdProfesor"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdProfesor"
                            Visible="False" />
                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo" InsertVisible="False"
                            ReadOnly="True" SortExpression="IdGrupo" Visible="False" />
                        <asp:BoundField DataField="Grupo" HeaderText="[GRUPO]" SortExpression="Grupo" />
                        <asp:BoundField DataField="IdGrado" HeaderText="IdGrado"
                            SortExpression="IdGrado" InsertVisible="False" ReadOnly="True"
                            Visible="False" />
                        <asp:BoundField DataField="Grado" HeaderText="[GRADO]" SortExpression="Grado" />
                        <asp:BoundField DataField="Nivel" HeaderText="[NIVEL]"
                            SortExpression="Nivel" />
                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel"
                            Visible="False" />
                        <asp:BoundField DataField="Plantel" HeaderText="[PLANTEL]"
                            SortExpression="Plantel" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%--
                    sorting YES - rowdatabound
                --%>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="4">
                &nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" class="style24">
                <asp:Label ID="Label6" runat="server" Text="[ASIGNATURA]" /></td>
            <td colspan="3">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSasignatura" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Height="22px" Width="330px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="GVreporte" runat="server"
                    AutoGenerateColumns="False"
                    Visible="False"

                    DataSourceID="SDSdetalleactividadesgrupo" 
                    Width="869px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Grupo" HeaderText="[GRUPO]" SortExpression="Grupo" />
                        <asp:BoundField DataField="Iniciadas" HeaderText="Iniciadas" ReadOnly="True"
                            SortExpression="Iniciadas" />
                        <asp:BoundField DataField="Pendientes" HeaderText="Pendientes" ReadOnly="True"
                            SortExpression="Pendientes" />
                        <asp:BoundField DataField="Terminadas" HeaderText="Terminadas" ReadOnly="True"
                            SortExpression="Terminadas" />
                        <asp:BoundField DataField="EXCELENTE" HeaderText="EXCELENTE" ReadOnly="True"
                            SortExpression="EXCELENTE">
                            <ItemStyle BackColor="LightBlue" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BIEN" HeaderText="BIEN" ReadOnly="True"
                            SortExpression="BIEN">
                            <ItemStyle BackColor="LightGreen" />
                        </asp:BoundField>
                        <asp:BoundField DataField="REGULAR" HeaderText="REGULAR" ReadOnly="True"
                            SortExpression="REGULAR">
                            <ItemStyle BackColor="Yellow" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MAL" HeaderText="MAL" ReadOnly="True"
                            SortExpression="MAL">
                            <ItemStyle BackColor="LightSalmon" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%--
                    sort NO - databound 
                --%>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="GVindicadores" runat="server" 
                    AutoGenerateColumns="False"
                    Caption="<h3>ESCALA DE VALORES</h3>" 

                    DataSourceID="SDSindicadores" 
                    Width="400px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="MinAzul" HeaderText="Mínimo para EXCELENTE"
                            SortExpression="MinAzul">
                            <ItemStyle HorizontalAlign="Right" BackColor="LightBlue" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinVerde" HeaderText="Mínimo para BIEN"
                            SortExpression="MinVerde">
                            <ItemStyle HorizontalAlign="Right" BackColor="LightGreen" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinAmarillo" HeaderText="Mínimo para REGULAR"
                            SortExpression="MinAmarillo">
                            <ItemStyle HorizontalAlign="Right" BackColor="Yellow" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MinRojo" HeaderText="Mínimo para MAL"
                            SortExpression="MinRojo">
                            <ItemStyle HorizontalAlign="Right" BackColor="LightSalmon" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="6">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: left;">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select distinct P.IdProfesor, G.IdGrupo, G.Descripcion as Grupo, Gr.IdGrado, Gr.Descripcion Grado, N.Descripcion as Nivel, Pl.IdPlantel, Pl.Descripcion as Plantel
from Usuario U, Profesor P, Grupo G, Grado Gr, Nivel N, Plantel Pl, Programacion Pr
where U.Login = @Login and P.IdUsuario = U.IdUsuario and Pr.IdProfesor = P.IdProfesor
and Pr.IdCicloEscolar = @IdCicloEscolar and G.IdGrupo = Pr.IdGrupo and
 Pl.IdPlantel = G.IdPlantel and Gr.IdGrado = G.IdGrado and N.IdNivel = Gr.IdNivel
and G.IdCicloEscolar = @IdCicloEscolar
order by Pl.Descripcion, N.Descripcion, Gr.Descripcion, G.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignatura" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdAsignatura, Descripcion 
FROM Asignatura WHERE IdGrado = @IdGrado
AND IdAsignatura in (select IdAsignatura from Programacion where IdProfesor in (select P.IdProfesor from Profesor P, Usuario U where U.Login = @Login and P.IdUsuario = U.IdUsuario))
ORDER BY IdArea">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdGrado" SessionField="IdGrado" Type="Int32" />
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSdetalleactividadesgrupo" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="sp_detalleactividadesgrupo" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="GVdatos" Name="IdGrupo"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSindicadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT MinRojo, MinAmarillo, MinVerde, MinAzul FROM Indicador, CicloEscolar
where Indicador.IdIndicador = CicloEscolar.IdIndicador and IdCicloEscolar = @IdCicloEscolar">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

