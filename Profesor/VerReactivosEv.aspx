﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="VerReactivosEv.aspx.vb" 
    Inherits="profesor_VerReactivosEv" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style10
        {
            width: 85%;
            height: 86px;
        }
        .style11
        {
        }
        .style14
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }
        .style21
        {
            width: 200px;
        }
        .style24
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 28%;
        }
        .style25
        {
            width: 28%;
        }
        .style26
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            height: 39px;
            font-weight: bold;
        }
        .style27
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            height: 21px;
            font-weight: bold;
        }
        .style36
        {
            font-weight: normal;
            font-size: small;
        }
        .style37
        {
            width: 25%;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            font-weight: normal;
        }
        </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Consultas - Ver Reactivos
		</h1>
		Permite ver los reactivos que componen una actividad 
                    específica que no sea de Entrega de Archivo.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style10">
        <tr>
            <td class="style26" colspan="3">
                Elija los siguientes datos para desplegar los reactivos 
                correspondientes y sus respuestas</td>
        </tr>
        <tr>
            <td class="style37">
                <asp:Label ID="Label1" runat="server" Text="[CICLO]" />
						</td>
            <td class="style21">
                            <asp:DropDownList ID="DDLcicloescolar" runat="server" 
                                    DataSourceID="SDSciclosescolares" DataTextField="Ciclo" 
                                    DataValueField="IdCicloEscolar" Width="375px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
            <td class="style24">
            </td>
        </tr>
        <tr>
            <td class="style37">
                <asp:Label ID="Label2" runat="server" Text="[NIVEL]" />
						</td>
            <td class="style21">
                                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True" 
                                DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel" 
                                    Width="375px" Height="22px" CssClass="style14">
                            </asp:DropDownList>
                                </td>
            <td class="style24">
            </td>
        </tr>
        <tr>
            <td class="style37">
                <asp:Label ID="Label3" runat="server" Text="[GRADO]" />
						</td>
            <td class="style21">
                                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True" 
                                    DataSourceID="SDSgrados" DataTextField="Descripcion" 
                                    DataValueField="IdGrado" Width="250px" Height="22px" 
                    CssClass="style14">
                                </asp:DropDownList>
                        </td>
            <td class="style24">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style37">
                <asp:Label ID="Label4" runat="server" Text="[ASIGNATURA]" /></td>
            <td class="style21">
                                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True" 
                                    DataSourceID="SDSasignaturas" DataTextField="Materia" 
                                    DataValueField="IdAsignatura" Width="375px" Height="22px" 
                    CssClass="style14">
                                </asp:DropDownList>
                           </td>
            <td class="style24">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style37">
                Calificacion</td>
            <td class="style21">
                <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True" 
                    CssClass="style14" DataSourceID="SDScalificaciones" 
                    DataTextField="Calificacion" DataValueField="IdCalificacion" Height="22px" 
                    Width="375px">
                </asp:DropDownList>
            </td>
            <td class="style24">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style37">
                Actividad</td>
            <td class="style21">
                <asp:DropDownList ID="DDLevaluacion" runat="server" AutoPostBack="True" 
                    CssClass="style14" DataSourceID="SDSevaluaciones" DataTextField="Tarea" 
                    DataValueField="IdEvaluacion" Height="22px" Width="375px">
                </asp:DropDownList>
            </td>
            <td class="style24">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style11">
                &nbsp;</td>
            <td class="style21">
                &nbsp;</td>
            <td class="style25">
                    &nbsp;</td>
        </tr>
        <tr>
            <td class="style11" colspan="3">
                            <asp:Panel ID="PnlReactivos" runat="server" BackColor="#E3EAEB" 
                                HorizontalAlign="Left">
                            </asp:Panel>
                        </td>
        </tr>
        <tr>
            <td class="style11" colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style11" colspan="3">
                <asp:GridView ID="GVreactivosDemo" runat="server" 
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 
                    Caption="<h3>Reactivos para la Actividad de Práctica (demo)</h3>"

                    DataSourceID="SDSreactivosdemo" 
                    Width="897px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo" 
                            SortExpression="Consecutivo" />
                        <asp:BoundField DataField="Planteamiento" 
                            HeaderText="Planteamiento del reactivo" SortExpression="Planteamiento" />
                        <asp:BoundField DataField="ArchivoPlanteamiento" 
                            HeaderText="ArchivoPlanteamiento" SortExpression="ArchivoPlanteamiento" 
                            Visible="False" />
                        <asp:HyperLinkField DataNavigateUrlFields="ArchivoPlanteamiento" 
                            DataNavigateUrlFormatString="~/Resources/material/{0}" 
                            DataTextField="ArchivoPlanteamiento" HeaderText="Archivo del Planteamiento" />
                        <asp:BoundField DataField="OpcionCorrecta" HeaderText="Opc. Correcta" 
                            SortExpression="OpcionCorrecta" />
                        <asp:BoundField DataField="Respuesta" HeaderText="Respuesta" 
                            SortExpression="Respuesta" />
                        <asp:BoundField DataField="ArchivoRespuesta" HeaderText="ArchivoRespuesta" 
                            SortExpression="ArchivoRespuesta" Visible="False" />
                        <asp:HyperLinkField DataNavigateUrlFields="ArchivoRespuesta" 
                            DataNavigateUrlFormatString="~/Resources/material/{0}" DataTextField="ArchivoRespuesta" 
                            HeaderText="Archivo de la Respuesta" />
                        <asp:BoundField DataField="NoSubtema" HeaderText="No. Subt." 
                            SortExpression="NoSubtema" />
                        <asp:BoundField DataField="Subtema" HeaderText="Descripción del Subtema" 
                            SortExpression="Subtema" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td style="text-align: left;">
                <asp:HyperLink ID="HyperLink3" runat="server"
					NavigateUrl="~/"
					CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
				</asp:HyperLink>
            </td>
            <td class="style21">
                &nbsp;</td>
            <td class="style25">
                &nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                        SelectCommand="SELECT IdCicloEscolar, Descripcion + ' (' + Cast(FechaInicio as varchar(10)) + ' - ' + Cast(FechaFin as varchar(10)) + ')' Ciclo
FROM CicloEscolar
WHERE (Estatus = 'Activo')
order by Descripcion"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    
                        SelectCommand="SELECT N.IdNivel, N.Descripcion 
FROM Nivel N, Escuela E
WHERE E.IdPlantel = @IdPlantel and N.IdNivel = E.IdNivel and (N.Estatus = 'Activo')">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdPlantel" SessionField="IdPlantel" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                    <asp:SqlDataSource ID="SDSgrados" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                        SelectCommand="SELECT * FROM [Grado] WHERE ([IdNivel] = @IdNivel) and (Estatus = 'Activo')
order by IdGrado">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel" 
                                PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>

            </td>
            <td>

                            <asp:SqlDataSource ID="SDScalificaciones" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                                SelectCommand="SELECT IdCalificacion, Descripcion + ' (' + C.Clave + ')' Calificacion
FROM Calificacion C
WHERE IdCicloEscolar = @IdCicloEscolar  and IdAsignatura = @IdAsignatura
ORDER BY Consecutivo">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar" 
                                        PropertyName="SelectedValue" Type="Int32" />
                                    <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura" 
                                        PropertyName="SelectedValue" />
                                </SelectParameters>
                            </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                                <asp:SqlDataSource ID="SDSevaluaciones" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                                    SelectCommand="select IdEvaluacion, ClaveBateria + ' (' + ClaveAbreviada + ')' Tarea
from Evaluacion
where IdCalificacion = @IdCalificacion and SeEntregaDocto = 'False'
order by IdEvaluacion">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion" 
                                            PropertyName="SelectedValue" />
                                    </SelectParameters>
                                </asp:SqlDataSource>

            </td>
            <td>

                    <asp:SqlDataSource ID="SDSasignaturas" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                        SelectCommand="SELECT distinct A.IdAsignatura, A.Descripcion + ' (' + Ar.Descripcion + ')' Materia
from Asignatura A, Programacion P, Area Ar
where A.IdGrado = @IdGrado and P.IdAsignatura = A.IdAsignatura and Ar.IdArea = A.IdArea
and P.IdProfesor = @IdProfesor
order by Materia, A.IdAsignatura">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado" 
                                PropertyName="SelectedValue" Type="Int32" />
                            <asp:SessionParameter Name="IdProfesor" SessionField="IdProfesor" />
                        </SelectParameters>
                    </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSreactivosdemo" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.Consecutivo,  P.Redaccion as Planteamiento, 
P.ArchivoApoyo as ArchivoPlanteamiento, O.Consecutiva as OpcionCorrecta, O.Redaccion as Respuesta, O.ArchivoApoyo as ArchivoRespuesta, S.Numero as NoSubtema, S.Descripcion Subtema
from Planteamiento P, Opcion O, Subtema S, DetalleEvaluacion DE 
where DE.IdEvaluacion = @IdEvaluacion and 
S.IdSubtema = DE.IdSubtema and P.IdSubtema = S.IdSubtema and 
O.IdPlanteamiento = P.IdPlanteamiento and P.Ocultar = 1 and O.Correcta = 'S' and P.TipoRespuesta = 'Multiple'
UNION
select P.Consecutivo,  P.Redaccion as Planteamiento, 
P.ArchivoApoyo as ArchivoPlanteamiento, O.Consecutiva as OpcionCorrecta, O.Redaccion as Respuesta, O.ArchivoApoyo as ArchivoRespuesta, S.Numero as NoSubtema, S.Descripcion Subtema
from Planteamiento P, Opcion O, Subtema S, DetalleEvaluacion DE 
where DE.IdEvaluacion = @IdEvaluacion and
S.IdSubtema = DE.IdSubtema and P.IdSubtema = S.IdSubtema and 
O.IdPlanteamiento = P.IdPlanteamiento and P.Ocultar = 1 and P.TipoRespuesta = 'Abierta'
order by P.Consecutivo
">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLevaluacion" Name="IdEvaluacion" 
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

