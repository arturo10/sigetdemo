﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="BuscaAlumnoProf.aspx.vb"
    Inherits="profesor_BuscaAlumnoProf"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            width: 208px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style15 {
            width: 208px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style19 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000066;
        }

        .style18 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            width: 717px;
        }

        .style20 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style21 {
            font-size: xx-small;
        }

        .style26 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            color: #000066;
            height: 8px;
        }

        .style37 {
            width: 208px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            color: #000000;
        }

        .style38 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            color: #000000;
            width: 717px;
        }

        .style39 {
            width: 208px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000000;
        }

        .style40 {
            width: 717px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Buscar un 
								<asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style37">
                <asp:Label ID="Label2" runat="server" Text="[INSTITUCION]" />
            </td>
            <td class="style16" colspan="3" style="text-align: left;">
                <asp:DropDownList ID="DDLinstitucion"
                    runat="server" AutoPostBack="True" DataSourceID="SDSinstituciones"
                    DataTextField="Descripcion" DataValueField="IdInstitucion"
                    Width="720px" Height="24px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style37">&nbsp;</td>
            <td class="style16" rowspan="2">Capture uno o varios de los datos siguientes para hacer la búsqueda:</td>
            <td class="style38">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style37">&nbsp;</td>
            <td class="style38">
                &nbsp;</td>
            <td style="text-align: left;">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style37">Matrícula</td>
            <td class="style16" style="text-align: left;">
                <asp:TextBox ID="TBmatricula" runat="server" Width="400px"></asp:TextBox>
            </td>
            <td class="style38">
                &nbsp;</td>
            <td style="text-align: left;">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style37">Nombre</td>
            <td class="style16" style="text-align: left;">
                <asp:TextBox ID="TBnombre" runat="server" Width="400px"></asp:TextBox>
            </td>
            <td class="style38">
                &nbsp;</td>
            <td style="text-align: left;">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style37">Apellido Paterno</td>
            <td class="style16" style="text-align: left;">
                <asp:TextBox ID="TBapepaterno" runat="server" Width="400px"></asp:TextBox>
            </td>
            <td class="style38">
                &nbsp;</td>
            <td style="text-align: left;">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style37">Apellido Materno</td>
            <td class="style16" style="text-align: left;">
                <asp:TextBox ID="TBapematerno" runat="server" Width="400px"></asp:TextBox>
            </td>
            <td class="style38">
                &nbsp;</td>
            <td style="text-align: left;">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style37">Login</td>
            <td class="style16" style="text-align: left;">
                <asp:TextBox ID="TBlogin" runat="server" Width="400px"></asp:TextBox>
            </td>
            <td class="style18">&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style22">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
																Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td class="style16">
                <asp:Button ID="BtnBuscar" runat="server" Text="Buscar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
                <asp:Button ID="BtnLimpiar" runat="server" Text="Limpiar"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
            <td class="style40">&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        </table>
        <table>
        <tr>
            <td>
                <asp:GridView ID="GValumnos" runat="server"
                    Caption="<h3>Listado de ALUMNOS encontrados</h3>" 
                    AutoGenerateColumns="false"

                    DataSourceID="SDSbuscar" 
                    Width="896px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True"  ItemStyle-CssClass="selectCell"/>
                        <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="Matricula" HeaderText="Matricula" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                        <asp:BoundField DataField="ApePaterno" HeaderText="ApePaterno" />
                        <asp:BoundField DataField="ApeMaterno" HeaderText="ApeMaterno" />
                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="Grupo" HeaderText="Grupo" />
                        <asp:BoundField DataField="IdGrado" HeaderText="IdGrado"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="Grado" HeaderText="Grado" />
                        <asp:BoundField DataField="IdNivel" HeaderText="IdNivel"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="Nivel" HeaderText="Nivel" />
                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="Plantel" HeaderText="Plantel" />
                        <asp:BoundField DataField="Login" HeaderText="Login" />
                        <asp:BoundField DataField="Password" HeaderText="Password" />
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" />
                        <asp:BoundField DataField="Equipo" HeaderText="Equipo" />
                        <asp:BoundField DataField="Subequipo" HeaderText="Subequipo" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort 
	                0  select
	                1  Id_ALUMNO
	                2  Matricula
	                3  Nombre
	                4  ApePaterno
	                5  ApeMaterno
	                6  Id_GRUPO
	                7  GRUPO
	                8  Id_GRADO
	                9  GRADO
	                10 Id_NIVEL
                    11 NIVEL
                    12 Id_PLANTEL
                    13 PLANTEL
                    14 Login
                    15 Password
                    16 Estatus
                    17 EQUIPO
                    18 SUBEQUIPO
	                --%>
            </td>
        </tr>
        </table>
        <table>
        <tr>
            <td class="style22">
                <asp:Button ID="Button1" runat="server" Text="Exportar a Excel"
                    CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style22">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style23">Actualizar password</td>
            <td colspan="2">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style37">
                <asp:Label ID="Label9" runat="server" Text="[ALUMNO]" />
                seleccionado:</td>
            <td colspan="2">
                <asp:Label ID="LblAlumno" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #000099"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style37">Nueva Contraseña:</td>
            <td colspan="2">
                <asp:TextBox ID="TBpasswordOK" runat="server" CssClass="style20"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style22">&nbsp;</td>
            <td colspan="2">
                <asp:Button ID="BtnActualizar" runat="server" Text="Actualizar"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
            <tr>
                <td colspan="4">
                    <uc1:msgError runat="server" ID="msgError2" />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    &nbsp;
                </td>
            </tr>
        <tr>
            <td class="style22">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
																Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td colspan="2">&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>

    <table>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSbuscar" runat="server"></asp:SqlDataSource>

            </td>
            <td>
                <asp:SqlDataSource ID="SDSusuarios" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Usuario]"></asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="
                    SELECT DISTINCT I.* FROM Institucion I, Plantel Pl, Grupo G, Programacion Pr, Profesor P 
                    WHERE P.IdProfesor = @IdProfesor AND P.IdProfesor = Pr.IdProfesor AND Pr.IdGrupo = G.IdGrupo AND G.IdPlantel = Pl.IdPlantel AND Pl.IdInstitucion = I.IdInstitucion
                    ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdProfesor" SessionField="IdProfesor" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>

