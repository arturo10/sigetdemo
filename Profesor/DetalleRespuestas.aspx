﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="DetalleRespuestas.aspx.vb"
    Inherits="profesor_DetalleRespuestas"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style13 {
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 229px;
        }

        .style19 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 229px;
            font-weight: bold;
        }

        .style21 {
            width: 229px;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 229px;
            text-align: right;
        }

        .style25 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 229px;
            font-weight: bold;
        }

        .style26 {
            width: 76px;
        }

        .style27 {
            width: 352px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }

        .style37 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000066;
            text-align: center;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Resultados -
        <asp:Label ID="Label6" runat="server" Text="[ASIGNATURA]" />
    </h1>
    Presenta los resultados por 
	<asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
    de 
    <asp:Label ID="Label7" runat="server" Text="[UNA]" />
    <asp:Label ID="Label2" runat="server" Text="[ASIGNATURA]" />
    específic<asp:Label ID="Label8" runat="server" Text="[A]" />, mostrando la calificación promedio y la acumulada. 
	<br />
    Permite ampliar el detalle por 
	<asp:Label ID="Label3" runat="server" Text="[ALUMNO]" />
    para consultar las respuestas dadas a cada actividad perteneciente a 
    <asp:Label ID="Label9" runat="server" Text="[LA]" />
    <asp:Label ID="Label4" runat="server" Text="[ASIGNATURA]" />.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style24">&nbsp;</td>
            <td class="style24">
                <asp:Label ID="Label5" runat="server" Text="[ALUMNO]" /></td>
            <td colspan="2" class="style27">
                <asp:Label ID="LblAlumno" runat="server"
                    Style="font-weight: 700; color: #000099; font-family: Arial, Helvetica, sans-serif; font-size: small"></asp:Label>
            </td>
            <td colspan="2">
                <asp:ImageButton ID="IBmensaje" runat="server"
                    AlternateText="Enviar mensaje al alumno"
                    DescriptionUrl="Enviar mensaje al alumno" ImageUrl="~/Resources/imagenes/mensaje.jpg"
                    Style="text-align: right" />
            </td>
        </tr>
        <tr>
            <td class="style24">&nbsp;</td>
            <td class="style24">Periodos de evaluación</td>
            <td colspan="4">
                <asp:DropDownList ID="DDLcalificacion" runat="server"
                    DataSourceID="SDScalificaciones" DataTextField="Descrip"
                    DataValueField="IdCalificacion" Height="22px"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small"
                    Width="335px" AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style14" colspan="2">
                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/profesor/ReporteGruposProf.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="6">
                <asp:GridView ID="GVevaluaciones" runat="server"
                    AutoGenerateColumns="False"
                    AllowSorting="True"
                    PageSize="6"
                    Caption="<h3>Listado de Actividades.</h3>"

                    DataSourceID="SDSevaluaciones"
                    Width="885px"
                    DataKeyNames="IdEvaluacionT,PuntosPosibles,FinSinPenalizacion,Penalizacion"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdEvaluacion" HeaderText="IdEvaluacion"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion"
                            Visible="False" />
                        <asp:BoundField DataField="IdEvaluacionT" HeaderText="IdEvaluacionT"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacionT"
                            Visible="False" />
                        <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                            SortExpression="IdAlumno" Visible="False" />
                        <asp:BoundField DataField="IdCalificacion" HeaderText="IdCalificacion"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdCalificacion"
                            Visible="False" />
                        <asp:BoundField DataField="ClaveBateria" HeaderText="Clave"
                            SortExpression="ClaveBateria" />
                        <asp:BoundField DataField="Descripcion" HeaderText="Actividad"
                            SortExpression="Descripcion" />
                        <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"
                            SortExpression="Asignatura" />
                        <asp:BoundField DataField="PeriodoE" HeaderText="Periodo Evaluación"
                            SortExpression="PeriodoE" />
                        <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                            HeaderText="Inicia" SortExpression="InicioContestar" Visible="False" />
                        <asp:BoundField DataField="FinSinPenalizacion"
                            DataFormatString="{0:dd/MM/yyyy}" HeaderText="FinSinPenalizacion"
                            SortExpression="FinSinPenalizacion" Visible="False" />
                        <asp:BoundField DataField="Penalizacion" HeaderText="Penalizacion"
                            SortExpression="Penalizacion" Visible="False" />
                        <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                            HeaderText="Límite" SortExpression="FinContestar" Visible="False" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                        <asp:BoundField DataField="TotalReactivos" HeaderText="Tot. Reactivos"
                            SortExpression="TotalReactivos" />
                        <asp:BoundField DataField="ReactivosContestados" HeaderText="Reac. Correctos"
                            SortExpression="ReactivosContestados" />
                        <asp:BoundField DataField="PuntosAcertados" HeaderText="Puntos"
                            SortExpression="PuntosAcertados" />
                        <asp:BoundField DataField="Resultado" HeaderText="Resultado"
                            SortExpression="Resultado" />
                        <asp:BoundField DataField="IdGrado" HeaderText="IdGrado"
                            SortExpression="IdGrado" Visible="False" />
                        <asp:BoundField DataField="Peso" HeaderText="Peso (%)" SortExpression="Peso" />
                        <asp:BoundField DataField="FechaTermino" DataFormatString="{0:dd/MM/yyyy}"
                            HeaderText="Terminó" SortExpression="FechaTermino" />
                        <asp:BoundField DataField="PuntosPosibles" HeaderText="PuntosPosibles"
                            SortExpression="PuntosPosibles" Visible="False" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td class="style25" colspan="2">Detalle de respuestas</td>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="GVrespuestas" runat="server" 
                    AutoGenerateColumns="False" 
                    Caption="<h3>Respuestas de Opción Múltiple</h3>"

                    Width="885px" 
                    DataSourceID="SDSrespuestas"

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                            SortExpression="Consecutivo" />
                        <asp:BoundField DataField="Planteamiento" HeaderText="Planteamiento"
                            SortExpression="Planteamiento" />
                        <asp:BoundField DataField="ArchivoPlanteamiento"
                            HeaderText="ArchivoPlanteamiento" SortExpression="ArchivoPlanteamiento"
                            Visible="False" />
                        <asp:HyperLinkField DataNavigateUrlFields="ArchivoPlanteamiento"
                            DataNavigateUrlFormatString="~/Resources/material/{0}"
                            DataTextField="ArchivoPlanteamiento" HeaderText="Archivo Planteamiento"
                            Target="_blank" />
                        <asp:BoundField DataField="Ponderacion" HeaderText="Ponderación"
                            SortExpression="Ponderacion" />
                        <asp:BoundField DataField="Consecutiva" HeaderText="Inciso Contestado"
                            SortExpression="Consecutiva" />
                        <asp:BoundField DataField="Opcion" HeaderText="Opción"
                            SortExpression="Opcion" />
                        <asp:BoundField DataField="ArchivoOpcion" HeaderText="ArchivoOpcion"
                            SortExpression="ArchivoOpcion" Visible="False" />
                        <asp:HyperLinkField DataNavigateUrlFields="ArchivoOpcion"
                            DataNavigateUrlFormatString="~/Resources/material/{0}" DataTextField="ArchivoOpcion"
                            HeaderText="Archivo Opción" Target="_blank" />
                        <asp:BoundField DataField="Correcta" HeaderText="Es Correcta?"
                            SortExpression="Correcta" />
                        <asp:BoundField DataField="Intento" HeaderText="Intento"
                            SortExpression="Intento" />
                        <asp:BoundField DataField="Inciso Correcto" HeaderText="Inciso Correcto"
                            SortExpression="Inciso Correcto" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="GVrespuestasAbiertas" runat="server"
                    AutoGenerateColumns="False" 
                    Caption="<h3>Respuestas Abiertas</h3>"
                    AllowSorting="True" 

                    DataKeyNames="IdRespuesta,SeCalifica"
                    DataSourceID="SDSrespuestasA" 
                    Width="885px" 

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField SelectText="Calificar" ShowSelectButton="False"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer">
                            <HeaderStyle Width="50px" />
                        </asp:CommandField>
                        <asp:BoundField DataField="IdRespuesta" HeaderText="IdRespuesta"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdRespuesta"
                            Visible="False" />
                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                            SortExpression="Consecutivo" />
                        <asp:BoundField DataField="Planteamiento" HeaderText="Planteamiento"
                            SortExpression="Planteamiento" />
                        <asp:BoundField DataField="ArchivoPlanteamiento"
                            HeaderText="ArchivoPlanteamiento" SortExpression="ArchivoPlanteamiento"
                            Visible="False" />
                        <asp:HyperLinkField DataNavigateUrlFields="ArchivoPlanteamiento"
                            DataNavigateUrlFormatString="~/Resources/material/{0}"
                            DataTextField="ArchivoPlanteamiento" HeaderText="Archivo Planteamiento" />
                        <asp:BoundField DataField="Ponderacion" HeaderText="Ponderacion"
                            SortExpression="Ponderacion" />
                        <asp:BoundField DataField="Redaccion" HeaderText="Respuesta"
                            SortExpression="Redaccion" />
                        <asp:BoundField DataField="Calificacion" HeaderText="Calificación"
                            SortExpression="Calificacion" />
                        <asp:BoundField DataField="Intento" HeaderText="Intento"
                            SortExpression="Intento" />
                        <asp:BoundField DataField="SeCalifica" HeaderText="SeCalifica"
                            SortExpression="SeCalifica" Visible="False" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:Panel ID="PnlResp" runat="server" Width="881px" Visible="false">
                    <%-- 20/11/2013 se separa función de calificar: este panel es permanentemente invisible --%>
                    <table class="style10" style="display:none;">
                        <tr>
                            <td class="style37" colspan="5">Contenido de la respuesta</td>
                        </tr>
                        <tr>
                            <td style="text-align: center" colspan="5">
                                <asp:TextBox ID="TBrespuesta" runat="server" Height="206px"
                                    Style="text-align: left" TextMode="MultiLine" Width="632px" ReadOnly="True"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="style21"></td>
                            <td class="style26">&nbsp;
                            </td>
                            <td colspan="2">&nbsp;</td>
                            <td>
                                <asp:DropDownList ID="DDLcalifica" runat="server"
                                    Style="font-family: Arial, Helvetica, sans-serif; font-size: medium">
                                    <asp:ListItem Value="S">Correcta</asp:ListItem>
                                    <asp:ListItem Value="N">Incorrecta</asp:ListItem>
                                    <asp:ListItem Value="M">Media</asp:ListItem>
                                </asp:DropDownList>
                                &nbsp;
                                <asp:Button ID="BtnCalificar" runat="server" Text="Calificar"
                                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Button ID="BtnExportarA" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style21" colspan="2">
                &nbsp;</td>
            <td class="style26">
                &nbsp;</td>
            <td colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style21" colspan="6">
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/profesor/ReporteGruposProf.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdCalificacion, Consecutivo, Descripcion + ' (' + Clave + ')' as Descrip
from Calificacion where IdAsignatura = @IdAsignatura
and IdCicloEscolar = @IdCicloEscolar order by Consecutivo">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdAsignatura" SessionField="IdAsignatura" />
                        <asp:SessionParameter Name="IdCicloEscolar" SessionField="IdCicloEscolar" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSrespuestasA" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select R.IdRespuesta, P.Consecutivo, P.Redaccion Planteamiento, P.ArchivoApoyo ArchivoPlanteamiento,P.Ponderacion, RA.Redaccion, R.Acertada, R.Intento, O.SeCalifica, RA.Calificacion
from EvaluacionTerminada ET, Respuesta R, DetalleEvaluacion DE, 
Planteamiento P, Opcion O, RespuestaAbierta RA
where ET.IdEvaluacionT = @IdEvaluacionT and DE.IdEvaluacion = ET.IdEvaluacion
and R.IdDetalleEvaluacion = DE.IdDetalleEvaluacion and O.IdOpcion = R.IdOpcion
and P.IdPlanteamiento = O.IdPlanteamiento and R.IdAlumno = ET.IdAlumno
and P.TipoRespuesta = 'Abierta' and RA.IdRespuesta = R.IdRespuesta 
order by P.Consecutivo">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdEvaluacionT" SessionField="IdEvaluacionT" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select E.IdEvaluacion, ET.IdEvaluacionT, ET.IdAlumno, C.IdCalificacion, E.ClaveBateria, C.Descripcion, C.Descripcion PeriodoE, A.Descripcion Asignatura, E.InicioContestar, E. FinSinPenalizacion, E.Penalizacion, E.FinContestar, E.Tipo, ET.TotalReactivos, ET.ReactivosContestados, ET.PuntosAcertados, ET.Resultado,ET.IdGrado, ET.FechaTermino, E.Porcentaje as Peso, ET.PuntosPosibles
from EvaluacionTerminada ET, Evaluacion E, Calificacion C, Asignatura A
where ET.IdAlumno = @IdAlumno
and E.IdEvaluacion = ET.IdEvaluacion and E.IdCalificacion = @IdCalificacion
and C.IdCalificacion = E.IdCalificacion
and A.IdAsignatura = C.IdAsignatura
and E.Estatus &lt;&gt; 'Cancelada'
order by A.Descripcion, C.Consecutivo, E.InicioContestar">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdAlumno" SessionField="IdAlumno" />
                        <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSrespuestas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.Consecutivo, P.Redaccion Planteamiento, P.ArchivoApoyo ArchivoPlanteamiento,P.Ponderacion, O.Consecutiva,O.Redaccion Opcion, O.ArchivoApoyo ArchivoOpcion,O.Correcta, R.Intento, 
(select top(1) Consecutiva from Opcion where Correcta = 'S' and IdPlanteamiento = P.IdPlanteamiento) as 'Inciso Correcto'
from EvaluacionTerminada ET, Respuesta R, DetalleEvaluacion DE, 
Planteamiento P, Opcion O
where ET.IdEvaluacionT = @IdEvaluacionT and DE.IdEvaluacion = ET.IdEvaluacion
and R.IdDetalleEvaluacion = DE.IdDetalleEvaluacion and O.IdOpcion = R.IdOpcion
and P.IdPlanteamiento = O.IdPlanteamiento and R.IdAlumno = ET.IdAlumno
and P.TipoRespuesta = 'Multiple'
order by P.Consecutivo">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdEvaluacionT" SessionField="IdEvaluacionT" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
    </table>
</asp:Content>

