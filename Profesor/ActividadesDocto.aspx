﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="ActividadesDocto.aspx.vb" 
    Inherits="coordinador_ActividadesDocto" 
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 35px;
        }

        .style13 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
        }

        .style15 {
            font-family: Arial, Helvetica, sans-serif;
        }

        .style36 {
            font-weight: normal;
            font-size: small;
        }

        .style37 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            height: 23px;
        }
        .estandar {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Consultas - Actividades con Documento
    </h1>
    Muestra a los 
	<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    de un 
	<asp:Label ID="Label2" runat="server" Text="[GRUPO]" />
    en una tabla con el 
    detalle de los resultados de todas las actividades de Entrega de Archivo de una 
    <asp:Label ID="Label3" runat="server" Text="[ASIGNATURA]" />
    .
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label4" runat="server" Text="[INSTITUCION]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLinstitucion" runat="server" AutoPostBack="True"
                    Height="22px" Width="350px" DataSourceID="SDSinstituciones"
                    DataTextField="Descripcion" DataValueField="IdInstitucion">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label5" runat="server" Text="[CICLO]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px"
                    Width="350px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label6" runat="server" Text="[PLANTEL]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSplanteles" DataTextField="Descripcion"
                    DataValueField="IdPlantel" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label7" runat="server" Text="[NIVEL]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                    DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                    Height="22px" Width="270px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label8" runat="server" Text="[GRADO]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                    Height="22px" Width="270px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label9" runat="server" Text="[GRUPO]" />
            </td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                    DataSourceID="SDSgrupos" DataTextField="Descripcion" DataValueField="IdGrupo"
                    Width="270px">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td class="style13" colspan="3">
                <asp:Label ID="Label10" runat="server" Text="[ASIGNATURA]" />
                que contiene actividades de entrega de documento</td>
            <td class="estandar" colspan="2">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSmaterias" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Width="350px" OnSelectedIndexChanged="DDLasignatura_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td class="estandar">&nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td>
                &nbsp;</td><asp:TableRow>
       
    </asp:TableRow>
            <td colspan="2">
                &nbsp;</td>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style15" colspan="6">
                <asp:GridView ID="GValumnos" runat="server" 
                    AllowSorting="True" 
                    DataSourceID="SDSdetalleactividades" 
                    Width="880px" 
                    CssClass="dataGrid_clear"
                    GridLines="None"
       >
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                

                <%-- sort YES - rowdatabound
                    'select A.,A. as "",A. as "",A.,A.,A.'
	                0  Matricula
	                1  Apellido Paterno
	                2  Apellido Materno
	                3  Nombre
	                4  EQUIPO
	                5  Estatus
	                6  ... dinamicas
	                --%>
            </td>
        </tr>
        <tr>
            <td class="style15">
                &nbsp;</td>
            <td colspan="4">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    Visible="False" CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style15">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
										Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td colspan="4">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>
                <%--  --%>
                <asp:SqlDataSource ID="SDSinstituciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="
                    SELECT DISTINCT I.* FROM Institucion I, Plantel Pl, Grupo G, Programacion Pr, Profesor P 
                    WHERE P.IdProfesor = @IdProfesor AND P.IdProfesor = Pr.IdProfesor AND Pr.IdGrupo = G.IdGrupo AND G.IdPlantel = Pl.IdPlantel AND Pl.IdInstitucion = I.IdInstitucion
                    ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdProfesor" Type="Int32" SessionField="IdProfesor" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>
                
                <%-- selecciona solo los ciclos escolares activos de los grupos que tiene asignados --%>
                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT DISTINCT C.IdCicloEscolar, C.Descripcion FROM CicloEscolar C, Grupo G, Programacion P 
WHERE (C.Estatus = @Estatus) and C.IdCicloEscolar = G.IdCicloEscolar and G.IdGrupo = P.IdGrupo and P.IdProfesor = @IdProfesor
ORDER BY C.Descripcion">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                        <asp:SessionParameter Name="IdProfesor" Type="Int32" SessionField="IdProfesor" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>
                
                <%-- selecciona solo los planteles de los grupos asignados al profesor en el ciclo seleccionado --%>
                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select DISTINCT Pl.IdPlantel, Pl.Descripcion 
from Grupo G, Programacion P, Plantel Pl, CicloEscolar C, Asignatura A, Grado Gr
where P.IdProfesor = @IdProfesor and G.IdGrupo = P.IdGrupo and C.IdCicloEscolar = @IdCicloEscolar and P.IdCicloEscolar = C.IdCicloEscolar and Pl.IdPlantel = G.IdPlantel and A.IdAsignatura = P.IdAsignatura and Gr.IdGrado = G.IdGrado and G.IdCicloEscolar = @IdCicloEscolar
order by Pl.Descripcion">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:SessionParameter Name="IdProfesor" Type="Int32" SessionField="IdProfesor" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <%-- selecciona sólo las materias del profesor --%>
                <asp:SqlDataSource ID="SDSmaterias" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT IdAsignatura, Descripcion 
FROM Asignatura WHERE IdGrado = @IdGrado
AND IdAsignatura in (select IdAsignatura from Programacion where IdProfesor = @IdProfesor and IdCicloEscolar = @IdCicloEscolar)
                    AND IdAsignatura in (select C.IdAsignatura from Calificacion C, Evaluacion E 
where C.IdCicloEscolar = @IdCicloEscolar and E.IdCalificacion = C.IdCalificacion and SeEntregaDocto = 'True')
ORDER BY IdArea">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                        <asp:SessionParameter Name="IdProfesor" Type="Int32" SessionField="IdProfesor" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE (([IdNivel] = @IdNivel) AND ([Estatus] = @Estatus)) ORDER BY [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrupo], [Descripcion] FROM [Grupo] WHERE (([IdGrado] = @IdGrado) AND ([IdPlantel] = @IdPlantel)) and ([IdCicloEscolar] = @IdCicloEscolar)
                    AND IdGrupo IN (SELECT IdGrupo FROM Programacion WHERE IdProfesor = @IdProfesor)
ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:SessionParameter Name="IdProfesor" Type="Int32" SessionField="IdProfesor" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSdetalleactividades" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SP_resultadoactividadesdocto"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Distinct N.IdNivel, N.Descripcion
from Nivel N, Escuela E
where E.IdPlantel = @IdPlantel
and N.IdNivel = E.IdNivel and (N.Estatus = 'Activo')
order by N.IdNivel">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
    </table>
</asp:Content>

