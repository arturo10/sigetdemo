﻿<%@ Page Title=""
  Language="VB"
  MasterPageFile="~/Principal.master"
  AutoEventWireup="false"
  CodeFile="Default.aspx.vb"
  Inherits="profesor_Default"
  MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
  <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <style type='text/css'>
    #toolBarContainer {
      display: none;
    }
  </style>
  <!-- la toolbar no es necesaria en páginas principales -->

  <asp:Literal ID="Literal1" runat="server" />

  <style type="text/css">
    #menusCol {
      display: inline-block;
      float: left;
      width: 170px;
      overflow: hidden;
      clear: both;
      border-right-color: #808080;
      border-right-width: 1px;
      border-right-style: solid;
    }

    #optionsCol {
      width: 715px;
      display: inline-block;
      position: relative;
      clear: none;
    }

      #optionsCol a:link,
      #optionsCol a:hover,
      #optionsCol a:visited,
      #optionsCol a:active {
        text-decoration: none;
        color: #000000;
      }

    .optionSection {
      display: none;
    }

    .menuP {
      display: inline-block;
      width: 160px;
      min-height: 1.3em;
      float: left;
      position: relative;
      padding-top: 10px;
      padding-bottom: 10px;
      padding-left: 5px;
      padding-right: 5px;
      text-align: center;
      font-weight: normal;
      cursor: pointer;
      border-bottom: solid;
      border-bottom-width: 1px;
      border-color: #888;
    }

      .menuP:hover {
        background-color: #CCDDEE;
      }

    .subMenuLine {
      width: 710px;
    }

    .subMenu {
      margin-left: 180px;
      min-width: 350px;
      width: 350px;
      color: #000000;
      text-align: center;
      border-bottom-width: 2px;
      border-bottom-style: solid;
      border-bottom-color: #888;
      font-weight: 600;
      font-size: 1.2em;
    }

    .btnLine {
      position: relative;
      display: inline-block;
      float: left;
      clear: left;
      width: 710px;
      max-width: 710px;
    }

    .btn {
      display: inline-block;
      margin-bottom: 0;
      font-size: 14px;
      line-height: 21px;
      text-align: center;
      vertical-align: middle;
      cursor: pointer;
      -moz-border-radius: 4px;
      -webkit-border-radius: 4px;
      border-radius: 4px;
      border: 1px solid #bbb;
      width: 210px;
      height: 100px;
      margin: 10px 10px 10px 10px;
      background: #F3F3F3;
      /* background-image al inicio */
      background-repeat: repeat-x;
      background-image: -webkit-gradient(linear, left top, left bottom, from(#F3F3F3), to(#E1E1E1)); /* Saf4+, Chrome */
      background-image: -webkit-linear-gradient(top, #F3F3F3, #E1E1E1); /* Chrome 10+, Saf5.1+ */
      background-image: -moz-linear-gradient(top, #F3F3F3, #E1E1E1); /* FF3.6+ */
      background-image: -ms-linear-gradient(top, #F3F3F3, #E1E1E1); /* IE10 */
      background-image: -o-linear-gradient(top, #F3F3F3, #E1E1E1); /* Opera 11.10+ */
      background-image: linear-gradient(top, #F3F3F3, #E1E1E1); /* W3C */
      font-weight: 600;
    }

      .btn:hover {
        background: #D1D1D1;
        /* background-image al inicio */
        background-repeat: repeat-x;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#C3C3C3), to(#D1D1D1)); /* Saf4+, Chrome */
        background-image: -webkit-linear-gradient(top, #C3C3C3, #D1D1D1); /* Chrome 10+, Saf5.1+ */
        background-image: -moz-linear-gradient(top, #C3C3C3, #D1D1D1); /* FF3.6+ */
        background-image: -ms-linear-gradient(top, #C3C3C3, #D1D1D1); /* IE10 */
        background-image: -o-linear-gradient(top, #C3C3C3, #D1D1D1); /* Opera 11.10+ */
        background-image: linear-gradient(top, #C3C3C3, #D1D1D1); /* W3C */
        border: 1px solid #444444;
      }


    .messageDiv {
      display: block;
      float: right;
      min-width: 280px;
      max-width: 400px;
    }

    .message {
      font-size: 1.1em;
      display: inline-block;
      float: left;
      position: relative;
      clear: both;
      margin-top: 15px;
      font-weight: 500;
      color: /*1*/ #000000;
      font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
    }

    .msgTitle {
      width: 100%;
      border-bottom: solid;
      border-bottom-width: 2px;
      border-bottom-color: #888;
      font-size: 1.1em;
      font-weight: 800;
    }
  </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
  <script>
    $(document).ready(function () {
      $('#mn0').click(function (event) { showOptions('#opc0', '#mn0') });
      $('#mn1').click(function (event) { showOptions('#opc1', '#mn1') });
      $('#mn2').click(function (event) { showOptions('#opc2', '#mn2') });
      $('#mn3').click(function (event) { showOptions('#opc3', '#mn3') });
      $('#mn4').click(function (event) { showOptions('#opc4', '#mn4') });
      $('#mn5').click(function (event) { showOptions('#opc5', '#mn5') });

      $('#opc0').css('display', 'block');
      $('#mn0').css('background-color', '#CCDDEE');
    });

    function showOptions(id, sender) {
      if ($('#opc0').css('display') != 'none') {
        $('#opc0').hide(300, function () { $(id).show(300); });
        $('#mn0').css('background-color', '');
      }
      else if ($('#opc1').css('display') != 'none') {
        $('#opc1').hide(300, function () { $(id).show(300); });
        $('#mn1').css('background-color', '');
      }
      else if ($('#opc2').css('display') != 'none') {
        $('#opc2').hide(300, function () { $(id).show(300); });
        $('#mn2').css('background-color', '');
      }
      else if ($('#opc3').css('display') != 'none') {
        $('#opc3').hide(300, function () { $(id).show(300); });
        $('#mn3').css('background-color', '');
      }
      else if ($('#opc4').css('display') != 'none') {
        $('#opc4').hide(300, function () { $(id).show(300); });
        $('#mn4').css('background-color', '');
      }
      else if ($('#opc5').css('display') != 'none') {
        $('#opc5').hide(300, function () { $(id).show(300); });
        $('#mn5').css('background-color', '');
      }
      else {
        $(id).show(300);
      }
      $(sender).css('background-color', '#CCDDEE');
    }
  </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
  <h1>Menú de
				<asp:Label ID="Label1" runat="server" Text="[PROFESORES]" />
  </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
  <div id="menusCol">
    <div id="mn0" class="menuP">
      <span class="menuPtext">Inicio</span>
    </div>
    <div id="mn1" class="menuP">
      <span class="menuPtext">Avance</span>
    </div>
    <div id="mn2" class="menuP">
      <span class="menuPtext">Resultados</span>
    </div>
    <div id="mn3" class="menuP">
      <span class="menuPtext">Evaluaciones</span>
    </div>
    <div id="mn4" class="menuP">
      <span class="menuPtext">Comunicación</span>
    </div>
    <div id="mn5" class="menuP">
      <span class="menuPtext">Consultas</span>
    </div>
  </div>
  <div id="optionsCol">
    <div id="opc0" class="optionSection">
      <div style="width: 100%; float: left; clear: both; text-align: center; margin-top: 20px; margin-bottom: 20px;">
        <asp:HyperLink ID="HLmensaje" runat="server"
          NavigateUrl="~/profesor/LeerMensajeEntradaP "
          Style="text-decoration: underline; font-family: Verdana, Arial, sans-serif; font-size: medium; color: #000066; font-weight: 700">Tiene mensajes pendientes de Leer</asp:HyperLink>
      </div>
      <div style="width: 100%; float: left; clear: both; text-align: center;">
        <uc1:msgError runat="server" ID="msgError" />
      </div>
      <div style="width: 100%; float: left; clear: both;">
        <div class="messageDiv">
          <div class="msgTitle">Avisos:</div>
          <asp:Label ID="LblMensaje1" runat="server" CssClass="message"></asp:Label>
          <asp:Label ID="LblMensaje2" runat="server" CssClass="message"></asp:Label>
          <asp:Label ID="LblMensaje3" runat="server" CssClass="message"></asp:Label>
          <asp:Label ID="LblMensaje4" runat="server" CssClass="message"></asp:Label>
          <asp:Label ID="LblMensaje5" runat="server" CssClass="message"></asp:Label>
          <asp:Label ID="LblMensaje6" runat="server" CssClass="message"></asp:Label>
        </div>
        <a class="btn" href="DatosProfesor ">
          <asp:Image ID="Image21" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/contact_card.png"
            Width="50" Height="50" />
          <br />
          Mis Datos
        </a>
      </div>
    </div>

    <div id="opc1" class="optionSection">
      <div class="btnLine">
        <asp:HyperLink CssClass="btn" ID="HyperLink2" runat="server"
          NavigateUrl="~/profesor/RevisaGrupo ">
          <asp:Image ID="Image1" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/monotone/monotone_graph_chart.png"
            Width="50" Height="50" />
          <br />
          Actividades
        </asp:HyperLink>
        <asp:HyperLink CssClass="btn" ID="HyperLink1" runat="server"
          NavigateUrl="~/profesor/AvanceActividadesProf ">
          <asp:Image ID="Image2" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/monotone/monotone_pen_write.png"
            Width="50" Height="50" />
          <br />
          Contestadas
        </asp:HyperLink>
      </div>
    </div>

    <div id="opc2" class="optionSection">
      <div class="btnLine">
        <asp:HyperLink CssClass="btn" ID="HyperLink17" runat="server"
          NavigateUrl="~/profesor/ReporteGruposProf ">
          <asp:Image ID="Image17" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/graph.png"
            Width="50" Height="50" />
          <br />
          <asp:Label ID="Label2" runat="server" Text="[ASIGNATURA]" />
        </asp:HyperLink><asp:HyperLink CssClass="btn" ID="HyperLink4" runat="server"
          NavigateUrl="~/profesor/DetalleActividades ">
          <asp:Image ID="Image3" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/finish_flag.png"
            Width="50" Height="50" />
          <br />
          Actividades
        </asp:HyperLink><asp:HyperLink CssClass="btn" ID="HyperLink5" runat="server"
          NavigateUrl="~/profesor/ResultadoReactivosProf ">
          <asp:Image ID="Image4" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/rulers.png"
            Width="50" Height="50" />
          <br />
          Reactivos
        </asp:HyperLink></div><div class="btnLine">
        <asp:HyperLink CssClass="btn" ID="HyperLink16" runat="server"
          NavigateUrl="~/profesor/ConsultaValoraciones ">
          <asp:Image ID="Image16" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/champion.png"
            Width="50" Height="50" />
          <br />
          Mis Valoraciones
        </asp:HyperLink></div></div><div id="opc3" class="optionSection">
      <div class="btnLine">
        <asp:HyperLink CssClass="btn" ID="HyperLink3" runat="server"
          NavigateUrl="~/profesor/CalificaRespuestaAbierta ">
          <asp:Image ID="Image5" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/monotone/monotone_check_yes.png"
            Width="50" Height="50" />
          <br />
          Calificar Respuestas
                    <br />
          Abiertas
        </asp:HyperLink><asp:HyperLink CssClass="btn" ID="HyperLink7" runat="server"
          NavigateUrl="~/profesor/CalificaDocumento ">
          <asp:Image ID="Image7" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/invoice_document_file.png"
            Width="50" Height="50" />
          <br />
          Calificar Documento
        </asp:HyperLink><asp:HyperLink CssClass="btn" ID="HyperLink10" runat="server"
          NavigateUrl="~/profesor/calificaForo/">
          <asp:Image ID="Image10" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/talk_speak_chat.png"
            Width="50" Height="50" />
          <br />
          Calificar Foro
        </asp:HyperLink></div><div class="btnLine">
        <asp:HyperLink CssClass="btn" ID="HyperLink6" runat="server"
          NavigateUrl="~/profesor/LlenarEvaluacionesProf ">
          <asp:Image ID="Image6" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/excel_spreasheet.png"
            Width="50" Height="50" />
          <br />
          Capturar Actividad
        </asp:HyperLink><asp:HyperLink CssClass="btn" ID="HyperLink18" runat="server"
          NavigateUrl="~/profesor/CalificaActividad ">
          <asp:Image ID="Image18" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/monotone/monotone_tag_round_check.png"
            Width="50" Height="50" />
          <br />
          Asigna Calificación
                    <br />
          a Actividad
        </asp:HyperLink></div></div><div id="opc4" class="optionSection">
      <div class="btnLine">
        <asp:HyperLink CssClass="btn" ID="HyperLink8" runat="server"
          NavigateUrl="~/profesor/EnviarMensajeP ">
          <asp:Image ID="Image8" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/monotone/monotone_email.png"
            Width="50" Height="50" />
          <br />
          Enviar Mensajes
        </asp:HyperLink><asp:HyperLink CssClass="btn" ID="HyperLink20" runat="server"
          NavigateUrl="~/profesor/LeerMensajeEntradaP ">
          <asp:Image ID="Image20" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/inbox_2.png"
            Width="50" Height="50" />
          <br />
          Mensajes Recibidos
        </asp:HyperLink><asp:HyperLink CssClass="btn" ID="HyperLink9" runat="server"
          NavigateUrl="~/profesor/LeerMensajeSalidaP ">
          <asp:Image ID="Image9" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/inbox_3.png"
            Width="50" Height="50" />
          <br />
          Mensajes Enviados
        </asp:HyperLink></div></div><div id="opc5" class="optionSection">
      <div class="btnLine">
        <asp:HyperLink CssClass="btn" ID="HyperLink12" runat="server"
          NavigateUrl="~/profesor/ListaApoyoNivelProf ">
          <asp:Image ID="Image12" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/books_multiple.png"
            Width="50" Height="50" />
          <br />
          Biblioteca de Recursos
        </asp:HyperLink><asp:HyperLink CssClass="btn" ID="HyperLink13" runat="server"
          NavigateUrl="~/profesor/VerReactivosEv ">
          <asp:Image ID="Image13" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/list.png"
            Width="50" Height="50" />
          <br />
          Ver Reactivos
        </asp:HyperLink><asp:HyperLink CssClass="btn" ID="HyperLink14" runat="server"
          NavigateUrl="~/profesor/MuestraApoyos ">
          <asp:Image ID="Image14" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/paper_clip_attachment.png"
            Width="50" Height="50" />
          <br />
          Material de Temas
        </asp:HyperLink></div><div class="btnLine">
        <asp:HyperLink CssClass="btn" ID="HyperLink15" runat="server"
          NavigateUrl="~/profesor/RespuestasAbiertas ">
          <asp:Image ID="Image15" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/monotone/monotone_microphone_record.png"
            Width="50" Height="50" />
          <br />
          Respuestas Abiertas
        </asp:HyperLink><asp:HyperLink CssClass="btn" ID="HyperLink19" runat="server"
          NavigateUrl="~/profesor/BuscaAlumnoProf ">
          <asp:Image ID="Image19" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/search_2.png"
            Width="50" Height="50" />
          <br />
          Buscar Alumnos
        </asp:HyperLink><asp:HyperLink CssClass="btn" ID="HyperLink21" runat="server"
          NavigateUrl="~/profesor/ActividadesDocto ">
          <asp:Image ID="Image22" runat="server"
            ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/invoice_document_file.png"
            Width="50" Height="50" />
          <br />
          Actividades Con Documento
        </asp:HyperLink></div></div><div style="clear: both;"></div>
  </div>
</asp:Content>

