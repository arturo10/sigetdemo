﻿Imports Siget

Imports System.IO
Imports System.Data.SqlClient

Partial Class profesor_BuscaAlumnoProf
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        GValumnos.Caption = "<h3>Listado de " &
            Config.Etiqueta.ALUMNOS &
            " encontrad" & Config.Etiqueta.LETRA_ALUMNO & "s</h3>"

        Label1.Text = Config.Etiqueta.ALUMNO
        Label2.Text = Config.Etiqueta.INSTITUCION
        Label9.Text = Config.Etiqueta.ALUMNO

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader

        miComando = objConexion.CreateCommand
        miComando.CommandText = "SELECT P.* FROM Usuario U, Profesor P where U.Login = '" + User.Identity.Name + "' and P.IdUsuario = U.IdUsuario"
        '   Try
        'Abrir la conexión y leo los registros del Alumno
        objConexion.Open()
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        misRegistros.Read() 'Leo para poder accesarlos
        Session("IdProfesor") = misRegistros.Item("IdProfesor")
        'Cierro y ahora buscaré si tiene mensajes (El profesor E=Envía o R=Recibe
        misRegistros.Close()
        objConexion.Close()
    End Sub

    Protected Sub BtnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBuscar.Click
        msgError.hide()
        msgSuccess.hide()
        msgInfo.hide()

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Try
            SDSbuscar.ConnectionString = strConexion ' modifiqué el query siguiente para qeu solo busque alumnos de grupos asignados al profesor
            SDSbuscar.SelectCommand = "select Distinct A.IdAlumno, A.Matricula, A.Nombre, A.ApePaterno, A.ApeMaterno, G.IdGrupo, G.Descripcion Grupo, Gr.IdGrado, Gr.Descripcion Grado, N.IdNivel, " + _
                        "N.Descripcion Nivel, P.IdPlantel, P.Descripcion Plantel, U.Login, U.Password, A.Estatus, A.Equipo, A.Subequipo " + _
                        "from Alumno A, Grupo G, Grado Gr, Nivel N, Plantel P, Usuario U " + _
                        "where U.IdUsuario = A.IdUsuario And G.IdGrupo = A.IdGrupo And Gr.IdGrado = G.IdGrado And N.IdNivel = Gr.IdNivel " + _
                        "And P.IdPlantel = G.IdPlantel and P.IdInstitucion = " + DDLinstitucion.SelectedValue + " and A.Matricula like '%" + TBmatricula.Text + "%' and A.Nombre like '%" + TBnombre.Text + "%' and A.ApePaterno like '%" + _
                        TBapepaterno.Text + "%' and A.ApeMaterno like '%" + TBapematerno.Text + "%' and U.Login like '%" + TBlogin.Text + "%'" &
                        " AND A.IdGrupo IN (SELECT IdGrupo From Programacion WHERE IdProfesor = " & Session("IdProfesor") & ") order by A.ApePaterno, A.ApeMaterno, A.Nombre"
            GValumnos.DataBind() 'Es necesario para que se vea el resultado
            msgInfo.show("Se encontraron " + GValumnos.Rows.Count.ToString + " " & Config.Etiqueta.ALUMNOS & " con los datos capturados.")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GValumnos_PreRender(sender As Object, e As EventArgs) Handles GValumnos.PreRender
        ' Etiquetas de GridView
        GValumnos.Columns(1).HeaderText = "Id_" & Config.Etiqueta.ALUMNO

        GValumnos.Columns(6).HeaderText = "Id_" & Config.Etiqueta.GRUPO

        GValumnos.Columns(7).HeaderText = Config.Etiqueta.GRUPO

        GValumnos.Columns(8).HeaderText = "Id_" & Config.Etiqueta.GRADO

        GValumnos.Columns(9).HeaderText = Config.Etiqueta.GRADO

        GValumnos.Columns(10).HeaderText = "Id_" & Config.Etiqueta.NIVEL

        GValumnos.Columns(11).HeaderText = Config.Etiqueta.NIVEL

        GValumnos.Columns(12).HeaderText = "Id_" & Config.Etiqueta.PLANTEL

        GValumnos.Columns(13).HeaderText = Config.Etiqueta.PLANTEL

        GValumnos.Columns(17).HeaderText = Config.Etiqueta.EQUIPO

        GValumnos.Columns(18).HeaderText = Config.Etiqueta.SUBEQUIPO

        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GValumnos.Rows.Count > 0 Then
            GValumnos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GValumnos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GValumnos.SelectedIndexChanged
        msgInfo.show("Seleccionado", HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(3).Text) + " " + HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(4).Text) + " " + HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(5).Text))
        LblAlumno.Text = HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(3).Text) + " " + HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(4).Text) + " " + HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(5).Text)
    End Sub

    Public Function GetErrorMessage(ByVal status As MembershipCreateStatus) As String
        Select Case status
            Case MembershipCreateStatus.DuplicateUserName
                Return "El usuario ya existe, elija Actualizar el password."
            Case MembershipCreateStatus.DuplicateEmail
                Return "Ya existe un usuario para ese email, por favor escriba un email diferente,"
            Case MembershipCreateStatus.InvalidPassword
                Return "El password es inválido, pruebe con otro."
            Case MembershipCreateStatus.InvalidEmail
                Return "El formato del email es inválido, por favor revíselo."
            Case MembershipCreateStatus.InvalidAnswer
                Return "La respuesta secreta es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidQuestion
                Return "La pregunta de recuperación de password es inválida, pruebe con otra."
            Case MembershipCreateStatus.InvalidUserName
                Return "El nombre de usuario es inválido, corríjalo."
            Case MembershipCreateStatus.ProviderError
                Return "La autentificación falló, contacte al administrador del sistema."
            Case MembershipCreateStatus.UserRejected
                Return "La creación del usuario ha sido cancelada, consulte al administrador."
            Case Else
                Return "Ha ocurrido un error desconocido, verifique o contacte al administrador."
        End Select
    End Function

    Protected Sub BtnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnActualizar.Click
        msgSuccess.hide()
        msgError.hide()
        msgError2.hide()

        Try
            'Actualizo password en tabla de seguridad, no puedo cambiar Login
            Dim u As MembershipUser
            u = Membership.GetUser(Trim(HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(14).Text))) 'Obtiene datos del usuario con el LOGIN
            'u.ChangePassword(Trim(HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(15).Text)), Trim(TBpasswordOK.Text)) 'cambia PASSWORD
            u.ChangePassword(u.ResetPassword(), Trim(TBpasswordOK.Text)) 'cambia PASSWORD

            'u.Email = TBemail.Text
            'Membership.UpdateUser(u)
            SDSusuarios.UpdateCommand = "SET dateformat dmy; UPDATE Usuario set Password = '" &
                Trim(TBpasswordOK.Text) &
                "', FechaModif = getdate(), Modifico = '" & User.Identity.Name & "' where Login = '" + Trim(HttpUtility.HtmlDecode(GValumnos.SelectedRow.Cells(14).Text)) + "'"
            SDSusuarios.Update()
            GValumnos.DataBind()
            msgSuccess.show("Éxito", "La contraseña de " & Config.Etiqueta.ARTDET_ALUMNO & " " &
                Config.Etiqueta.ALUMNO &
                " ha sido cambiada.")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError2.show("Error", ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Convierte(GValumnos, "AlumnosEncontrados")
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            'Algoritmo que oculta toda la columna donde aparece el "Seleccionar"
            Dim Cont As Integer
            GVactual.HeaderRow.Cells.Item(0).Visible = False
            For Cont = 0 To GVactual.Rows.Count - 1
                GVactual.Rows(Cont).Cells.Item(0).Visible = False
            Next
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub GValumnos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GValumnos.DataBound
        GValumnos.Caption = "<b>Listado de " &
            Config.Etiqueta.ALUMNOS &
            " encontrados de " &
            Config.Etiqueta.ARTDET_INSTITUCION & " " & Config.Etiqueta.INSTITUCION &
            " " + DDLinstitucion.SelectedItem.ToString + "</b>"
    End Sub

    Protected Sub BtnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLimpiar.Click
        TBmatricula.Text = ""
        TBnombre.Text = ""
        TBapepaterno.Text = ""
        TBapepaterno.Text = ""
        TBlogin.Text = ""

        msgError.hide()
        msgSuccess.hide()
        msgError2.hide()
        msgInfo.hide()
    End Sub

    Protected Sub GValumnos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GValumnos.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GValumnos, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GValumnos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GValumnos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub
End Class
