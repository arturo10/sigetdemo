﻿Imports Siget

Imports System.IO
Imports System.Data.SqlClient

Partial Class profesor_EstatusAlumnos
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.GRUPOS
        Label3.Text = Config.Etiqueta.PROFESOR
        Label4.Text = Config.Etiqueta.ALUMNO
        Label5.Text = Config.Etiqueta.PROFESOR
        Label6.Text = Config.Etiqueta.ARTDET_GRUPOS
        Label7.Text = Config.Etiqueta.LETRA_GRUPO
        Label8.Text = Config.Etiqueta.ARTDET_ALUMNO
        Label9.Text = Config.Etiqueta.ARTDET_PROFESOR
    End Sub

    Protected Sub DDLconsultar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLconsultar.SelectedIndexChanged
        GVdatos.Sort("", SortDirection.Ascending)
        GVdatos.Columns.Clear()
        GVdatos.DataBind()

        'Observar si no llega a dar problema el que esto se controle con variables de sesion, de ser
        'asi puedo grabar en campos ocultos los valores para que al seleccionar leerlos de nuevo y pasarlos al siguiente script
        Try
            'Para generar el contenido del GridView dinamicamente, es necesario que su propiedad AutoGenerateColumns = True 
            Select Case DDLconsultar.SelectedValue
                Case "Terminadas" 'Alumnos que contestaron la Tarea/Examen/proyecto Completa
                    GVdatos.DataSourceID = "SDSterminadas"
                Case "Iniciadas" 'Alumnos que iniciaron a contestar la Tarea/Examen/proyecto pero que no la han terminado o que subieronn archivo de entrega pero el profesor no se los ha revisado
                    GVdatos.DataSourceID = "SDSiniciadas"
                Case "Pendientes" 'Alumnos que no han iniciado a contestar la Tarea/Examen/proyecto
                    GVdatos.DataSourceID = "SDSpendientes"
            End Select
            GVdatos.DataBind() 'Es necesario para que se actualice (refresque) el Grid
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgInfo.hide()
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        If GVactual.Rows.Count.ToString + 1 < 65536 Then
            Dim sb As StringBuilder = New StringBuilder()
            Dim sw As StringWriter = New StringWriter(sb)
            Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
            Dim pagina As Page = New Page
            Dim form = New HtmlForm
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            form.Controls.Add(GVactual)
            pagina.RenderControl(htw)
            Response.Clear()
            Response.Buffer = True
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
            Response.Charset = "UTF-8"
            Response.ContentEncoding = Encoding.Default
            Response.Write(sb.ToString())
            Response.End()
        Else
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Convierte(GVdatos, DDLconsultar.SelectedItem.ToString)
    End Sub

    Protected Sub GVdatos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVdatos.DataBound
        GVdatos.Caption = "<B>" + Session("Plantel").ToString + " <BR> " + Session("Nivel").ToString + _
                " - " + Session("Grado").ToString + "</B> - [" + _
                Session("Evaluacion").ToString + " - Total de " & Config.Etiqueta.ALUMNOS & ": " + _
                CStr(GVdatos.Rows.Count) + "] <BR>" + "Evaluaciones " &
                DDLconsultar.SelectedItem.ToString + " al " + Date.Today.ToShortDateString

        If GVdatos.Rows.Count = 0 Then
            msgInfo.show("No hay tareas " + DDLconsultar.SelectedItem.ToString)
        Else
            msgInfo.hide()
        End If
    End Sub

    Protected Sub GVdatos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVdatos.RowDataBound
        '1)PRIMERO OBTENGO LOS RANGOS DE LOS INDICADORES PARA EL SEMAFORO ROJO, AMARILLO Y VERDE
        If Trim(DDLconsultar.Text) = "Terminadas" Then
            Dim strConexion As String
            'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objConexion As New SqlConnection(strConexion)
            Dim miComando As SqlCommand
            Dim misRegistros As SqlDataReader

            miComando = objConexion.CreateCommand
            'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
            miComando.CommandText = "select I.MinAmarillo, I.MinVerde, I.MinAzul from Indicador I, CicloEscolar C where " + _
                                    "I.IdIndicador = C.IdIndicador and C.IdCicloEscolar = " + Session("IdCicloEscolar").ToString
            Dim MinAmarillo, MinVerde, MinAzul As Decimal
            Try
                objConexion.Open()
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                misRegistros.Read()
                MinAmarillo = misRegistros.Item("MinAmarillo")
                MinVerde = misRegistros.Item("MinVerde")
                MinAzul = misRegistros.Item("MinAzul")
                misRegistros.Close()
                objConexion.Close()
                msgError.hide()
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
            End Try

            '2)HAGO UN CICLO QUE VAYA DE 1 AL TOTAL DE COLUMNAS (EL INDICE DE LAS CELDAS(COLUMNAS) INICIA EN 0 (CELLS(0)= )
            'La parte de la condición: (e.Row.RowIndex > -1) es porque en este caso que comparo fechas estaba pintando tambien el encabezado (fila -1), que tambien comparaba
            If (e.Row.Cells.Count > 6) And (e.Row.RowIndex > -1) Then  'Para que se realice la comparacion cuando haya mas de una columna
                Dim I = 7 'Este es el número de columna del Grid que compara con los indicadores
                If IsNumeric(Trim(e.Row.Cells(I).Text)) Then 'para que no compare celdas vacias
                    If (CDbl(e.Row.Cells(I).Text) < MinAmarillo) Then
                        e.Row.Cells(I).BackColor = Drawing.Color.LightSalmon
                    ElseIf (CDbl(e.Row.Cells(I).Text) < MinVerde) Then
                        e.Row.Cells(I).BackColor = Drawing.Color.Yellow
                    ElseIf (CDbl(e.Row.Cells(I).Text) < MinAzul) Then
                        e.Row.Cells(I).BackColor = Drawing.Color.LightGreen
                    Else 'Significa que es mayor o igual al MinAzul:
                        e.Row.Cells(I).BackColor = Drawing.Color.LightBlue
                    End If
                End If
            End If
        End If

        ' Etiquetas de GridView
        ' en este caso son 3 selects diferentes, pero ambos tienen los 3 mismos campos iniciales,
        ' y son los que llevan etiquetas, así que no uso un "Select Case DDLconsultar.SelectedValue"
        ' para chekar etiquetas para cada consulta, ojo con eso.
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(1)
            LnkHeaderText.Text = "Nombre " & Config.Etiqueta.ALUMNO

            LnkHeaderText = e.Row.Cells(2).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.GRUPO
        End If
    End Sub

    Protected Sub GVdatos_PreRender(sender As Object, e As EventArgs) Handles GVdatos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVdatos.Rows.Count > 0 Then
            GVdatos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVdatos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If GVdatos.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub
End Class
