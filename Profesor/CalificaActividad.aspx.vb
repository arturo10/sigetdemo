﻿Imports Siget

Imports System.Data.SqlClient

Partial Class profesor_CalificaActividad
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = Trim(User.Identity.Name)

        TitleLiteral.Text = "Asignar Calificación a Actividades"

        GVactividades.Caption = "<h2>Actividades para calificar de la " & Config.Etiqueta.ASIGNATURA & " Seleccionada<h2>"

        Label1.Text = Config.Etiqueta.ALUMNO
        'Label2.Text = Config.Etiqueta.ALUMNOS
        'Label3.Text = Config.Etiqueta.GRUPO
        'Label4.Text = Config.Etiqueta.PROFESOR
        Label5.Text = Config.Etiqueta.CICLO
        Label6.Text = Config.Etiqueta.GRUPO
        Label7.Text = Config.Etiqueta.ASIGNATURA
        'Label8.Text = Config.Etiqueta.ARTIND_ASIGNATURA
        'Label9.Text = Config.Etiqueta.ARTDET_ALUMNOS
        'Label10.Text = Config.Etiqueta.LETRA_PROFESOR
        Label11.Text = Config.Etiqueta.ARTIND_GRUPO
        Label12.Text = Config.Etiqueta.ARTDET_GRUPOS
        Label13.Text = Config.Etiqueta.LETRA_GRUPO
        'Label14.Text = Config.Etiqueta.LETRA_ALUMNO
    End Sub

    Protected Sub GVdatos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(3).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.GRUPO

            LnkHeaderText = e.Row.Cells(5).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.GRADO

            LnkHeaderText = e.Row.Cells(6).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.NIVEL

            LnkHeaderText = e.Row.Cells(8).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.PLANTEL
        End If

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVdatos, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    Protected Sub GVdatos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVdatos.SelectedIndexChanged
        Session("IdAsignatura") = 0 'Inicializo para que vuelva a elegir una materia
        Session("IdGrupo") = CInt(GVdatos.SelectedDataKey.Values(0).ToString)
        Session("IdGrado") = CInt(GVdatos.SelectedDataKey.Values(1).ToString)
        DDLasignatura.SelectedIndex = -1
        GVactividades.SelectedIndex = -1
        GValumnos.SelectedIndex = -1
    End Sub


    Protected Sub DDLasignatura_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.SelectedIndexChanged
        Session("IdAsignatura") = DDLasignatura.SelectedValue
        GVactividades.SelectedIndex = -1
        GValumnos.SelectedIndex = -1


        'If GValumnos.Rows.Count > 0 Then
        '    GValumnos.Caption = "<B> Listado de " &
        ' Config.Etiqueta.ALUMNOS &
        ' " de " & Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO &
        ' " " + GVdatos.SelectedRow.Cells(3).Text +
        ' " con el resultado de las Actividades realizadas en " &
        ' Config.Etiqueta.ARTDET_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA &
        ' " " + DDLasignatura.SelectedItem.ToString +
        ' " en " & Config.Etiqueta.ARTDET_CICLO & " " & Config.Etiqueta.CICLO &
        ' " " + DDLcicloescolar.SelectedItem.ToString + "<br>" + _
        '    "Reporte emitido el " + Date.Today.ToShortDateString + "</B>"
        'End If
    End Sub

    Protected Sub DDLcicloescolar_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.SelectedIndexChanged
        'Inicializo para que se vuelvan a elegir los argumentos
        Session("IdAsignatura") = 0
        Session("IdGrupo") = 0
        Session("IdGrado") = 0
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        'DDLcicloescolar.Items.Insert(0, New ListItem("---Elija un Ciclo Escolar", 0))
        'Inicializo para que se vuelvan a elegir los argumentos
        Session("IdAsignatura") = 0
        Session("IdGrupo") = 0
        Session("IdGrado") = 0
    End Sub

    Protected Sub GVactividades_DataBound(sender As Object, e As EventArgs) Handles GVactividades.DataBound
        msgError.hide()
        msgInfo.hide()

        If DDLasignatura.SelectedIndex > 0 Then
            If GVactividades.Rows.Count < 1 Then
                msgInfo.show("No hay actividades configuradas para calificarse para la " & Config.Etiqueta.ASIGNATURA & " seleccionada.")
            End If
        End If
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub GValumnos_DataBound(sender As Object, e As EventArgs) Handles GValumnos.DataBound
        Dim strConexion As String = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim connection As New SqlConnection(strConexion)
        Dim command As SqlCommand
        Dim results As SqlDataReader
        command = connection.CreateCommand

        ' para cada alumno de el gridview
        For Each actual As GridViewRow In GValumnos.Rows
            ' intenta obtener el resultado de la evaluación seleccionada, si lo hay, 
            ' y asignalo a su campo de calificación. No pude hacer esto desde el SqlDataSource 
            ' por que no necesariamente los alumnos tienen registros en EvaluacionTerminada
            ' para esta actividad, y un join alteraría el orden de los registros
            command.CommandText = "SELECT Resultado FROM EvaluacionTerminada WHERE IdAlumno = " & actual.Cells(1).Text.Trim() & " AND IdEvaluacion = " & GVactividades.SelectedDataKey(0).ToString().Trim()
            connection.Open()
            results = command.ExecuteReader()
            If results.Read() Then
                actual.Cells(3).Text = results.Item("Resultado")
            Else
                actual.Cells(3).Text = ""
            End If
            connection.Close()
            results.Close()
        Next
    End Sub

    Protected Sub BtnCalificar_Click(sender As Object, e As EventArgs) Handles BtnCalificar.Click
        Try
            pnlCalifica.Visible = True

            Dim empty As Boolean = True
            Try
                Decimal.Parse(GValumnos.SelectedRow.Cells(3).Text)
                empty = False
            Catch ex As Exception
                empty = True
            End Try

            If empty Then
                SDSalumnos.InsertCommand = "SET DATEFORMAT dmy; INSERT INTO EvaluacionTerminada " &
                    " (IdAlumno, IdGrado, IdGrupo, IdCicloEscolar, IDEvaluacion, Resultado, FechaTermino) VALUES (" &
                GValumnos.SelectedDataKey(0).ToString().Trim() & ", " &
                GVdatos.SelectedDataKey(1).ToString().Trim() & ", " &
                GVdatos.SelectedDataKey(0).ToString.Trim() & ", " &
                DDLcicloescolar.SelectedValue.ToString().Trim() & ", " &
                GVactividades.SelectedDataKey(0).ToString().Trim() & ", " &
                DDLcalifica.SelectedValue & ", getDate())"
                SDSalumnos.Insert()
                SDSalumnos.DataBind()
                GValumnos.DataBind()
            Else
                SDSalumnos.UpdateCommand = "SET DATEFORMAT dmy; UPDATE EvaluacionTerminada SET Resultado = " & DDLcalifica.SelectedValue &
                    " WHERE IdEvaluacion = " & GVactividades.SelectedDataKey(0).ToString().Trim() &
                    " AND IdAlumno = " & GValumnos.SelectedDataKey(0).ToString().Trim() &
                    " AND IdGrado = " & GVdatos.SelectedDataKey(1).ToString().Trim() &
                    " AND IdGrupo = " & GVdatos.SelectedDataKey(0).ToString.Trim() &
                    " AND IdCicloEscolar = " & DDLcicloescolar.SelectedValue.ToString().Trim()
                SDSalumnos.Update()
                SDSalumnos.DataBind()
                GValumnos.DataBind()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GValumnos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles GValumnos.SelectedIndexChanged
        Try
            pnlCalifica.Visible = True

            Dim empty As Boolean = True
            Try
                Decimal.Parse(GValumnos.SelectedRow.Cells(3).Text)
                empty = False
            Catch ex As Exception
                empty = True
            End Try

            If Not empty Then
                DDLcalifica.SelectedIndex = DDLcalifica.Items.IndexOf(DDLcalifica.Items.FindByValue(Int(Decimal.Parse(GValumnos.SelectedRow.Cells(3).Text))))
            Else
                DDLcalifica.SelectedIndex = 0
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVdatos_PreRender(sender As Object, e As EventArgs) Handles GVdatos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVdatos.Rows.Count > 0 Then
            GVdatos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVdatos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVdatos.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVdatos.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVdatos, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    For Each r In GVactividades.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVactividades, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    For Each r In GValumnos.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GValumnos, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

  Protected Sub GVactividades_PreRender(sender As Object, e As EventArgs) Handles GVactividades.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVactividades.Rows.Count > 0 Then
      GVactividades.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVactividades_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVactividades.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVactividades.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GVactividades_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVactividades.RowDataBound
    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(3).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.MATRICULA

      LnkHeaderText = e.Row.Cells(4).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.EQUIPO

      LnkHeaderText = e.Row.Cells(5).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.SUBEQUIPO
    End If

    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVactividades, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  Protected Sub GValumnos_PreRender(sender As Object, e As EventArgs) Handles GValumnos.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GValumnos.Rows.Count > 0 Then
      GValumnos.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GValumnos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GValumnos.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GValumnos.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

    Protected Sub GValumnos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GValumnos.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GValumnos, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub
End Class
