﻿Imports Siget


Partial Class profesor_EstatusGrupo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.GRUPOS
        Label3.Text = Config.Etiqueta.PROFESOR
        Label4.Text = Config.Etiqueta.ARTDET_GRUPOS
        Label5.Text = Config.Etiqueta.LETRA_GRUPO

        'Observar si no llega a dar problema el que esto se controle con variables de sesion, de ser
        'asi puedo grabar en campos ocultos los valores para que al seleccionar leerlos de nuevo y pasarlos al siguiente script
        Try
            'Para generar el contenido del GridView dinamicamente, es necesario que su propiedad AutoGenerateColumns = True 
            SDSdatos.ConnectionString = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            GVdatos.Caption = "<font size=""2""><b>Actividades de " &
                Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO &
                " " + Session("Grupo") + " de " &
                Config.Etiqueta.ARTDET_PLANTEL & " " & Config.Etiqueta.PLANTEL &
                " " + Session("Plantel").ToString + " <BR> " + Session("Nivel").ToString + " - " + Session("Grado").ToString + "</b></font>"

            'Filtro desde el Plantel porque puede haber un plantel que no maneje cierto grado
            'Y no es necesario que filtre aqui el IdGrupo porque si la tarea existe en el Grado debe tenerla el Grupo, aunque es necesario que materia tiene asignada en la tabla Programacion
            SDSdatos.SelectCommand = "select E.IdEvaluacion, E.ClaveBateria as 'Evaluacion (Actividad)', E.ClaveAbreviada as 'Abreviación', " + _
                    "E.Tipo, C.Descripcion 'Periodo Califica', A.Descripcion Asignatura " + _
                    "from Calificacion C, Evaluacion E, Asignatura A, Grado G, Escuela Es " + _
                    "where G.IdGrado = " + Session("IdGrado").ToString + _
                    " and Es.IdNivel = G.IdNivel and Es.IdPlantel = " + Session("IdPlantel").ToString + _
                    " and A.IdGrado = G.IdGrado and C.IdAsignatura = A.IdAsignatura and A.IdAsignatura " + _
                    "in (select IdAsignatura from Programacion where IdProfesor in (select IdProfesor from Profesor Pr, Usuario U where U.Login = '" + _
                    Trim(User.Identity.Name) + "' and Pr.IdUsuario = U.IdUsuario) and IdGrupo = " + Session("IdGrupo").ToString + ")" + _
                    " and C.IdCicloEscolar = " + Session("IdCicloEscolar").ToString + _
                    " and e.IdCalificacion = C.IdCalificacion order by C.Consecutivo, E.InicioContestar"

            ' msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVdatos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVdatos.SelectedIndexChanged
        Session("IdEvaluacion") = GVdatos.SelectedDataKey.Values(0).ToString()
        Session("Evaluacion") = GVdatos.SelectedRow.Cells(2).Text
        Response.Redirect("EstatusAlumnos.aspx")
    End Sub

    Protected Sub GVdatos_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVdatos.RowDataBound
        'Con lo siguiente oculto la columna IdEvaluacion
        'OJO, EL GRIDVIEW DEBE TENER DESHABILITADA LA OPCION DE HABILITAR PAGINACION
        e.Row.Cells(1).Visible = False
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(6).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA
        End If

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVdatos, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    Protected Sub GVdatos_PreRender(sender As Object, e As EventArgs) Handles GVdatos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVdatos.Rows.Count > 0 Then
            GVdatos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVdatos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If GVdatos.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVdatos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVdatos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub
End Class
