﻿Imports Siget

Partial Class profesor_ListaApoyoNivelProf
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = User.Identity.Name

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.NIVEL
        Label3.Text = Config.Etiqueta.NIVEL
        Label4.Text = Config.Etiqueta.NIVEL
        Label5.Text = Config.Etiqueta.ARTIND_NIVEL
        Label6.Text = Config.Etiqueta.NIVEL
        Label7.Text = Config.Etiqueta.ARTDET_ALUMNOS
        Label8.Text = Config.Etiqueta.ARTDET_NIVEL
        Label9.Text = Config.Etiqueta.ARTDET_NIVEL
    End Sub

    Protected Sub DDLniveles_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLniveles.DataBound
        DDLniveles.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTDET_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub GVapoyos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVapoyos.DataBound
        If GVapoyos.Rows.Count = 0 And DDLniveles.SelectedIndex > 0 Then
            LblMensaje.Text = "No existen apoyos registrados en este " & Config.Etiqueta.NIVEL & "."
        Else
            LblMensaje.Text = ""
        End If
    End Sub

    Protected Sub GVapoyos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVapoyos.SelectedIndexChanged
        LblMensaje.Text = "Reproduciendo: " + Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(3).Text))
        PnlLink.Visible = False
        PnlDespliega.Controls.Add(New LiteralControl("<Table align='left' border='1' bordercolor = '#CCCCCC'>")) 'Agrego el inicio de la Tabla

        If Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(6).Text)) = "Imagen" Then
      PnlDespliega.Controls.Add(New LiteralControl("<IMG SRC=""" & Config.Global.urlApoyo & Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)) + """>"))
    ElseIf Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(6).Text)) = "Video" Or Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(6).Text)) = "Audio" Then
      'Acepta videos en formato .mpg y .avi hasta ahorita probados
      PnlLink.Visible = True
      HLarchivo.Text = HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)
      HLarchivo.NavigateUrl = Config.Global.urlApoyo & HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)
      HLarchivo.Target = "_blank"
      PnlDespliega.Controls.Add(New LiteralControl("<embed src=""" & Config.Global.urlApoyo & Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)) + """ width=""280"" height=""227"" autostart=""true"" controller=""true"" align=""center""></embed>"))
      'NO: Response.Write("<script>window.open('../apoyo/" + HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text) + "','_blank');</script>")
    ElseIf Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(6).Text)) = "FLV" Then
      'ASÍ FUNCIONABA ANTES CON EL FLVPLAYER: PnlDespliega.Controls.Add(New LiteralControl("<object type=""application/x-shockwave-flash"" width=""600"" height=""450"" wmode=""transparent"" data=""../flvplayer.swf?file=apoyo/prueba.flv&autoStart=true""><param name=""movie"" value=""../flvplayer.swf?file=apoyo/prueba.flv&autoStart=true"" /><param name=""wmode"" value=""transparent"" /></object>"))
      'PnlDespliega.Controls.Add(New LiteralControl("<object type=""application/x-shockwave-flash"" width=""700"" height=""520"" wmode=""transparent"" data=""../flvplayer.swf?file=material/" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + "&autoStart=true""><param name=""movie"" value=""../flvplayer.swf?file=material/" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + "&autoStart=true"" /><param name=""wmode"" value=""transparent"" /></object>"))
      PnlDespliega.Controls.Add(New LiteralControl("<script type=""text/javascript"" src=""../usuarios/flash/jwplayer.js""></script><div id=""container"">Cargando el video ...</div><script type=""text/javascript""> jwplayer(""container"").setup({ flashplayer: ""../usuarios/flash/player.swf"", file: """ & Config.Global.urlApoyo & Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)) + """, autostart: true, height: 420, width: 680 }); </script>"))
    ElseIf Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(6).Text)) = "SWF" Then
      PnlDespliega.Controls.Add(New LiteralControl("<object classid=""clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"" width=""680"" height=""420""><param name=""allowScriptAccess"" value=""never""/><param name=""allowNetworking"" value=""internal"" /><param name=""movie"" value=""" & Config.Global.urlApoyo & Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)) + """/><param name=""wmode"" value=""transparent"" /><embed type=""application/x-shockwave-flash"" allowScriptAccess=""never"" allowNetworking=""internal"" src=""" & Config.Global.urlApoyo & Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)) + """ width=""680"" height=""420"" wmode=""transparent""/></object>"))
    ElseIf Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(6).Text)) = "Link" Then
      'PnlDespliega.Controls.Add(New LiteralControl("<b><font face=""Arial"" size=3><a href=""" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + """ target=""_blank"">" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + "</a></font></b>"))
      PnlLink.Visible = True
      HLarchivo.Text = HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)
      HLarchivo.NavigateUrl = HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)
      HLarchivo.Target = "_blank"
      Response.Write("<script>window.open('" + HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text) + "','_blank');</script>")

    ElseIf Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(6).Text)) = "YouTube" Then
      Dim VideoID As String = Regex.Match(GVapoyos.SelectedRow.Cells(4).Text, "youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)").Groups(1).Value

      PnlDespliega.Controls.Add(New LiteralControl("<object><param name='movie' value='http://www.youtube.com/v/" & VideoID & "'></param><param name='wmode' value='transparent'></param><param name='allowFullScreen' value='true'></param><embed src='http://www.youtube.com/v/" & VideoID & "' type='application/x-shockwave-flash' allowfullscreen='true' wmode='transparent' width='640' height='390'></embed></object>"))

    Else
      'Si llega aquí es que es una liga a un documento:
      'PnlDespliega.Controls.Add(New LiteralControl("<b><font face=""Arial"" size=3><a href=""../material/" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + """ target=""_blank"">" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + "</a></font></b>"))
      PnlLink.Visible = True
      HLarchivo.Text = HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)
      HLarchivo.NavigateUrl = Config.Global.urlApoyo & HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)
      HLarchivo.Target = "_blank"
      Response.Write("<script>window.open('" & Config.Global.urlApoyo & HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text) + "','_blank','_blank','scrollbars=yes,resizable=yes');</script>")
    End If
    PnlDespliega.Controls.Add(New LiteralControl("</Table>"))
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GVapoyos_PreRender(sender As Object, e As EventArgs) Handles GVapoyos.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVapoyos.Rows.Count > 0 Then
      GVapoyos.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVapoyos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVapoyos.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVapoyos.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

    Protected Sub GVapoyos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVapoyos.RowDataBound
        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVapoyos, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVapoyos.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVapoyos, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

End Class
