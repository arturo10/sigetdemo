﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO
Imports System.Globalization


Partial Class profesor_CalificaRespAbiertaNueva
  Inherits System.Web.UI.Page

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Utils.Sesion.sesionAbierta()

    Session("Login") = Trim(User.Identity.Name)

    TitleLiteral.Text = "Califica Respuestas Abiertas"

    If Not IsPostBack Then
      If Config.Global.PROFESOR_CALIFICA_NUMERICO = 0 Then
        DDLcalifica.Items.Add(New ListItem("100", 100))
        DDLcalifica.Items.Add(New ListItem("95", 95))
        DDLcalifica.Items.Add(New ListItem("90", 90))
        DDLcalifica.Items.Add(New ListItem("85", 85))
        DDLcalifica.Items.Add(New ListItem("80", 80))
        DDLcalifica.Items.Add(New ListItem("75", 75))
        DDLcalifica.Items.Add(New ListItem("70", 70))
        DDLcalifica.Items.Add(New ListItem("65", 65))
        DDLcalifica.Items.Add(New ListItem("60", 60))
        DDLcalifica.Items.Add(New ListItem("55", 55))
        DDLcalifica.Items.Add(New ListItem("50", 50))
        DDLcalifica.Items.Add(New ListItem("45", 45))
        DDLcalifica.Items.Add(New ListItem("40", 40))
        DDLcalifica.Items.Add(New ListItem("35", 35))
        DDLcalifica.Items.Add(New ListItem("30", 30))
        DDLcalifica.Items.Add(New ListItem("25", 25))
        DDLcalifica.Items.Add(New ListItem("20", 20))
        DDLcalifica.Items.Add(New ListItem("15", 15))
        DDLcalifica.Items.Add(New ListItem("10", 10))
        DDLcalifica.Items.Add(New ListItem("5", 5))
        DDLcalifica.Items.Add(New ListItem("0", 0))
      ElseIf Config.Global.PROFESOR_CALIFICA_NUMERICO = 1 Then
        DDLcalifica.Items.Add(New ListItem("Correcta", 100))
        DDLcalifica.Items.Add(New ListItem("Media", 80))
        DDLcalifica.Items.Add(New ListItem("Incorrecta", 50))
      End If
    End If

    Label1.Text = Config.Etiqueta.ALUMNOS
    Label2.Text = Config.Etiqueta.CICLO
    Label3.Text = Config.Etiqueta.ASIGNATURA
    Label4.Text = Config.Etiqueta.ARTIND_ASIGNATURA
    Label6.Text = Config.Etiqueta.ASIGNATURA

    ' tuve que establecer un hidden field para contar el número de postbacks,
    ' para poder regresar a la página de CalificaRespuestaAbierta desde donde 
    ' se llego aqui, conservando los valores de ésta.
    If IsNothing(hfReturn.Value) Then
      hfReturn.Value = "0"
    End If
    hfReturn.Value = (Integer.Parse(hfReturn.Value) + -1) & ""
    LblRegresar.NavigateUrl = "javascript:window.history.go(document.getElementById('" & hfReturn.ClientID & "').value)"
  End Sub

  Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
    DDLasignatura.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA, 0))
  End Sub

  Protected Sub DDLasignatura_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.SelectedIndexChanged
    msgSuccess.hide()
    msgError.hide()
    msgInfo.hide()

    GVrespuestas.SelectedIndex = -1
  End Sub

  Protected Sub GVrespuestas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVrespuestas.DataBound
    If GVrespuestas.Rows.Count > 0 Then
      GVrespuestas.Caption = "<font size=""2""><b>Respuestas Abiertas para " &
          Config.Etiqueta.ARTDET_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA &
          " " + DDLasignatura.SelectedItem.Text + "</b></font>"
      msgSuccess.hide()
      msgInfo.hide()
      BtnExportar.Visible = True
    Else
      If (DDLasignatura.SelectedIndex > 0) And (Not msgSuccess.isActive()) Then
        msgInfo.show("Aún no hay nuevas respuestas de " & Config.Etiqueta.ALUMNOS & " para esta " & Config.Etiqueta.ASIGNATURA & ".")
      End If
      BtnExportar.Visible = False
    End If
  End Sub

  Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
    ' añado los estilos, por que el gridview no se exporta predefinidamente con css
    GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
    GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
    GVactual.GridLines = GridLines.Both

    Dim headerText As String
    ' quito la imágen de ordenamiento de los encabezados
    For Each tc As TableCell In GVactual.HeaderRow.Cells
      If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
        headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
        tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
        tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
        tc.Text = headerText ' establezco el texto simple
      End If
    Next

    'Este procedimiento convierte el GridView a Excel eliminando la primera y segunda columna
    Try
      ' antes de exportar la copia del gridview, en cada fila:
      For Each r As GridViewRow In GVactual.Rows()

        ' paso el markup limpio completo a la columna de respuesta
        r.Cells(Utils.Tables.GetColumnIndexByName(GVactual, "Respuesta")).Text =
            Regex.Replace(HttpUtility.HtmlDecode(r.Cells(Utils.Tables.GetColumnIndexByName(GVactual, "MarkupRespuesta")).Text), "<[^>]*(>|$)", String.Empty)

        ' elimino la columna de markup, y de "seleccionar"
        r.Cells.RemoveAt(Utils.Tables.GetColumnIndexByName(GVactual, "MarkupRespuesta"))
        r.Cells.RemoveAt(Utils.Tables.GetColumnIndexByName(GVactual, "MarkupReactivo"))
        r.Cells.RemoveAt(0)
      Next
      ' ahora quito también las columnas de seleccionar y markup en el encabezado
      GVactual.HeaderRow.Cells.RemoveAt(Utils.Tables.GetColumnIndexByName(GVactual, "MarkupReactivo"))
      GVactual.HeaderRow.Cells.RemoveAt(Utils.Tables.GetColumnIndexByName(GVactual, "MarkupRespuesta"))
      GVactual.HeaderRow.Cells.RemoveAt(0)

      If GVactual.Rows.Count.ToString + 1 < 65536 Then
        Dim sb As StringBuilder = New StringBuilder()
        Dim sw As StringWriter = New StringWriter(sb)
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        Dim pagina As Page = New Page
        Dim form = New HtmlForm
        pagina.EnableEventValidation = False
        pagina.DesignerInitialize()
        pagina.Controls.Add(form)
        form.Controls.Add(GVactual)
        pagina.RenderControl(htw)
        Response.Clear()
        Response.Buffer = True
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
        Response.Charset = "UTF-8"
        Response.ContentEncoding = Encoding.Default
        Response.Write(sb.ToString())
        Response.End()
      Else
        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
    Convierte(GVrespuestas, "RespuestasAbiertas")
  End Sub

  Protected Sub GVrespuestas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVrespuestas.SelectedIndexChanged
        msgSuccess.hide()
        msgError.hide()
        msgInfo.hide()

        Try
            If GVrespuestas.SelectedIndex <> -1 Then
                TBrespuesta.Text = HttpUtility.HtmlDecode(GVrespuestas.SelectedRow.Cells(Utils.Tables.GetColumnIndexByName(GVrespuestas, "MarkupRespuesta")).Text)
                DDLcalifica.SelectedIndex = 0
                Dim array() As String = Regex.Replace(HttpUtility.HtmlDecode(TBrespuesta.Text), "<[^>\s]*(>|$)", String.Empty).Split(" ").ToArray()
                Dim list As List(Of String) = New List(Of String)()
                Dim val As String = ""

                For m As Integer = 0 To array.Length - 1
                    If Not array(m).Trim() = "" Then
                        list.Add(array(m))
                    End If
                Next
                NoPalabrasRespuesta.Text = list.Count()
                HF1_ModalPopupExtender.Show()
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

  Protected Sub BtnCalificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCalificar.Click
    msgSuccess.hide()
    msgError.hide()
    msgInfo.hide()

    ' los borradores no pueden ser calificados por el profesor
    If ddlEstado.SelectedValue = "Borrador" Then
      msgError.show("No se pueden calificar respuestas que aún no han sido enviadas.")
      Return
    End If

    Try
      '[S]i --> (CORRETA) = 100  [M]edia --> = 80  [N]o --> (INCORRETA) = 50
      If GVrespuestas.SelectedDataKey.Values(1).ToString = "True" Then 'Compruebo que la respuesta abierta esta configurada para ser revisada por el Profesor (SeCalifica = 'True')
        'AQUI CALIFICO LA RESPUESTA ABIERTA DEL ALUMNO QUE ESTÉ SELECCIONADA EN EL GRID
        'SI ES DE OPCION MULTIPLE LA RESPUESTA ES ACERTADA (S) O NO (N)
        'SI ES ABIERTA, ENTONCES SI NO LA VA A CALIFICAR EL PROFESOR QUEDA COMO ACERTADA (S) PERO SI SI LA VA A CALIFICAR QUEDA EN BLANCO EL CAMPO "ACERTADA"
        '1) MODIFICO EL CAMPO "PUNTOSACERTADOS" Y "RESULTADO" DE LA TABLA EVALUACIONTERMINADA
        'Dim PuntosA, Ponderacion, PuntosPosibles, Penalizacion As Decimal
        Dim PuntosPosibles As Decimal
        'Dim FinSinPenalizacion, termino As Date --> YA NO SE NECESITAN
        Dim FinSinPenalizacion As Date
        Dim ReactivosOK, IdEvaluacionT As Integer

        ' obten los datos de la actividad
        Try
          Dim strCon As String
          strCon = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
          Dim objCon As New SqlConnection(strCon)
          Dim comm As SqlCommand
          Dim reg As SqlDataReader
          comm = objCon.CreateCommand
          objCon.Open()
          comm.CommandText = "select E.IdEvaluacion, ET.IdEvaluacionT, ET.IdAlumno, C.IdCalificacion, E.ClaveBateria, C.Descripcion, C.Descripcion PeriodoE, A.Descripcion Asignatura, E.InicioContestar, E. FinSinPenalizacion, E.Penalizacion, E.FinContestar, E.Tipo, ET.TotalReactivos, ET.ReactivosContestados, ET.PuntosAcertados, ET.Resultado,ET.IdGrado, ET.FechaTermino, E.Porcentaje as Peso, ET.PuntosPosibles" &
              " from EvaluacionTerminada ET, Evaluacion E, Calificacion C, Asignatura A" &
              " where ET.IdAlumno = " & GVrespuestas.SelectedDataKey().Values(2).ToString &
              " And E.IdEvaluacion = ET.IdEvaluacion and E.IdCalificacion = " & GVrespuestas.SelectedDataKey().Values(9).ToString &
              " and C.IdCalificacion = E.IdCalificacion" &
              " and A.IdAsignatura = C.IdAsignatura" &
              " and E.Estatus <> 'Cancelada' And E.IdEvaluacion = " & GVrespuestas.SelectedDataKey().Values(10).ToString
          reg = comm.ExecuteReader() 'Creo conjunto de registros
          Dim inst As String = ""
          If reg.HasRows() Then
            reg.Read()
            'PuntosA = CDec(reg.Item("PuntosAcertados"))
            ReactivosOK = CInt(reg.Item("ReactivosContestados"))
            PuntosPosibles = CDec(reg.Item("PuntosPosibles"))
            'no lo necesito: Resultado = CDec(GVevaluaciones.SelectedRow.Cells(17).Text)
            'Ponderacion = CDec(GVrespuestas.SelectedRow.Cells(1).Text)
            FinSinPenalizacion = CDate(reg.Item("FinSinPenalizacion"))
            'Penalizacion = CDec(reg.Item("Penalizacion"))
            'termino = CDate(reg.Item("FechaTermino"))
            IdEvaluacionT = CInt(reg.Item("IdEvaluacionT"))
          End If
          reg.Close()
          objCon.Close()
        Catch ex As Exception
          Utils.LogManager.ExceptionLog_InsertEntry(ex)
          msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try

        ' obten la fecha de finalización sin penalizacion.
        ' puesto que puede haber tres tipos de asignaciones, se hace en una consulta aparte,
        ' para obtener la fecha sin penalización de la evaluación pertinente
        Try
          Dim strCon As String
          strCon = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
          Dim objCon As New SqlConnection(strCon)
          Dim comm As SqlCommand
          Dim reg As SqlDataReader
          comm = objCon.CreateCommand
          objCon.Open()
          comm.CommandText = "select distinct IdEvaluacion, IdCalificacion, ClaveBateria, ClaveAbreviada, Porcentaje, InicioContestar, FinSinPenalizacion, FinContestar, Tipo, Penalizacion, Estatus, Aleatoria, SeEntregaDocto, Califica " + _
              "from Evaluacion where IdEvaluacion not in (select IdEvaluacion from EvaluacionPlantel where IdEvaluacion = " + GVrespuestas.SelectedDataKey().Values(10).ToString + " and IdPlantel = (SELECT IdPlantel FROM Alumno WHERE IdAlumno = " & GVrespuestas.SelectedDataKey().Values(2).ToString & ")) and IdEvaluacion = " + GVrespuestas.SelectedDataKey().Values(10).ToString + _
              "and IdEvaluacion not in (select IdEvaluacion from EvaluacionAlumno where IdEvaluacion = " + GVrespuestas.SelectedDataKey().Values(10).ToString + " and IdAlumno = " + GVrespuestas.SelectedDataKey().Values(2).ToString + ")" + _
              " UNION " + _
              "select distinct E.IdEvaluacion, E.IdCalificacion, E.ClaveBateria, E.ClaveAbreviada, E.Porcentaje, P.InicioContestar, P.FinSinPenalizacion, P.FinContestar, E.Tipo, E.Penalizacion, P.Estatus, E.Aleatoria, E.SeEntregaDocto, E.Califica " + _
              "from Evaluacion E, EvaluacionPlantel P where E.IdEvaluacion = " + GVrespuestas.SelectedDataKey().Values(10).ToString + " and P.IdEvaluacion = E.IdEvaluacion and P.IdPlantel = (SELECT IdPlantel FROM Alumno WHERE IdAlumno = " & GVrespuestas.SelectedDataKey().Values(2).ToString & ")" + _
              " and E.IdEvaluacion not in (select IdEvaluacion from EvaluacionAlumno where IdEvaluacion = " + GVrespuestas.SelectedDataKey().Values(10).ToString + " and IdAlumno = " + GVrespuestas.SelectedDataKey().Values(2).ToString + ")" + _
              " UNION " + _
              "select distinct E.IdEvaluacion, E.IdCalificacion, E.ClaveBateria, E.ClaveAbreviada, E.Porcentaje, A.InicioContestar, A.FinSinPenalizacion, A.FinContestar, E.Tipo, E.Penalizacion, A.Estatus, E.Aleatoria, E.SeEntregaDocto, E.Califica " + _
              "from Evaluacion E, EvaluacionAlumno A where E.IdEvaluacion = " + GVrespuestas.SelectedDataKey().Values(10).ToString + " and A.IdEvaluacion = E.IdEvaluacion and A.IdAlumno = " + GVrespuestas.SelectedDataKey().Values(2).ToString
          reg = comm.ExecuteReader() 'Creo conjunto de registros
          Dim inst As String = ""
          If reg.HasRows() Then
            reg.Read()
            FinSinPenalizacion = CDate(reg.Item("FinSinPenalizacion"))
          End If
          reg.Close()
          objCon.Close()
        Catch ex As Exception
          Utils.LogManager.ExceptionLog_InsertEntry(ex)
          msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try

        Dim Calcula = "S"
        '1) MODIFICO EL CAMPO "ACERTADA" DE LA TABLA RESPUESTA
        ' esto ya no será necesario para la calificación de la actividad, pero sí para las caritas en ListarReactivos.aspx
        Dim acertada As String
        If DDLcalifica.SelectedValue < 60 Then
          acertada = "N"
        Else
          acertada = "S"
        End If
        SDSrespuestas.UpdateCommand = "SET dateformat dmy; UPDATE Respuesta set Acertada = '" & acertada & "' where IdRespuesta = " + GVrespuestas.SelectedDataKey().Values(0).ToString
        SDSrespuestas.Update()

        'RECALCULAR CALIFICACION. Si el campo Acertada estába vacío significa que todavía no se califica, por lo que entonces solo puedo
        'sumarle los puntos de la Ponderacion que tiene el reactivo, si tiene el valor de "S" es que ya tiene sumados los puntos por
        'tanto si cambio a "N" debo restárselos

        'Si la actividad se contestó después de la "FechaSinPenalización" se le descontará la penalización a esos puntos
        'If termino > FinSinPenalizacion Then
        '    Ponderacion = Ponderacion - (Ponderacion * Penalizacion / 100)
        'End If
              
        Dim empty As Boolean = True
        Try
          Decimal.Parse(GVrespuestas.SelectedRow.Cells(5).Text)
          empty = False
        Catch ex As Exception
          empty = True
        End Try

        Try
          '**************************OJO ESTE ES EL ALGORITMO ERA EL ANTERIOR PARA RECALCULAR LA CALIFICACION, PERO OCASIONABA CALIFICACIONES ARRIBA DE 100
          'If Not empty Then
          '    ' si ya estaba calificado, se le descuenta la calificacion pasada
          '    PuntosA = PuntosA - Ponderacion * (CDec(GVrespuestas.SelectedRow.Cells(5).Text) / 100.0)
          '    ' luego se le aumenta a la nueva calificación
          '    PuntosA = PuntosA + Ponderacion * (CDec(DDLcalifica.SelectedValue) / 100.0)
          'Else
          '    ' si no estaba calificado, se suma la ponderación por la calificación
          '    Ponderacion = Ponderacion * (CDec(DDLcalifica.SelectedValue) / 100.0)
          '    PuntosA = PuntosA + Ponderacion
          '    ReactivosOK = ReactivosOK + 1
          'End If

          'Recalculo el resultado
          ' Resultado = (PuntosA / PuntosPosibles) * 100
          '****************************
          SDSrespuestas.UpdateCommand = "SET dateformat dmy; UPDATE RespuestaAbierta set Calificacion = " & CDec(DDLcalifica.SelectedValue) & " where IdRespuestaA = " + GVrespuestas.SelectedDataKey().Values(3).ToString
          SDSrespuestas.Update()

                    SDSrespuestas.UpdateCommand = "SET dateformat dmy; UPDATE RespuestaAbierta set modifico = '" & User.Identity.Name & "',fechaModificacion=" & DateTime.Now.ToString("dd/MM/yyyy") & " where IdRespuestaA = " + GVrespuestas.SelectedDataKey().Values(3).ToString
                    SDSrespuestas.Update()
          'Actualizo los Puntos Acertados y el Resultado en EVALUACIONTERMINADA --> ahora lo hago desde un procedimiento almacenado
          'SDSrespuestas.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionTerminada set PuntosAcertados = " & CStr(FormatNumber(PuntosA, 2)) & ", Resultado = " + CStr(FormatNumber(Resultado, 2)) + ", ReactivosContestados = " + ReactivosOK.ToString + " where IdEvaluacionT = " & IdEvaluacionT
          'SDSrespuestas.Update()

          '************************NUEVO ALGORITMO PARA ACTUALIZAR LA CALIFICACION (USO UN STOREPROCEDURE)
                    SDScalculacalificacion.UpdateCommand = "exec SP_recalculaCalificacion @IdAlumno=" + GVrespuestas.SelectedDataKey.Values(2).ToString + _
              ", @IdEvaluacion=" + GVrespuestas.SelectedDataKey.Values(10).ToString + _
              ", @FinSinPenalizacion='" + FinSinPenalizacion.ToString("dd/MM/yyyy") + "'" + _
                                  ", @PuntosPosibles=" + PuntosPosibles.ToString + ", @ReactivosOK=" + ReactivosOK.ToString + ", @IdEvaluacionT=" + IdEvaluacionT.ToString
                    SDScalculacalificacion.Update()
          '***************************************************************
          GVrespuestas.DataBind()
          msgSuccess.show("Éxito", "Se calificó la respuesta.")

          GVrespuestas.SelectedIndex = -1
          TBrespuesta.Text = ""
          DDLcalifica.SelectedIndex = 0
        Catch ex As Exception
                    Utils.LogManager.ExceptionLog_InsertEntry(ex)

          msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
      Else
        msgError.show("Este reactivo no puede ser calificado por " &
            Config.Etiqueta.ARTDET_PROFESOR & " " & Config.Etiqueta.PROFESOR & ".")
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página.<br />" & ex.Message.ToString())
    End Try
  End Sub

  Protected Sub ddlPageSize_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPageSize.SelectedIndexChanged
    GVrespuestas.PageSize = ddlPageSize.SelectedValue
  End Sub

  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

  Protected Sub GVrespuestas_PreRender(sender As Object, e As EventArgs) Handles GVrespuestas.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVrespuestas.Rows.Count > 0 Then
      GVrespuestas.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVrespuestas_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVrespuestas.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVrespuestas.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GVrespuestas_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVrespuestas.RowDataBound
    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(8).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.GRUPO

      LnkHeaderText = e.Row.Cells(9).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.GRADO

      LnkHeaderText = e.Row.Cells(10).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.NIVEL

      LnkHeaderText = e.Row.Cells(11).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.PLANTEL

      LnkHeaderText = e.Row.Cells(12).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.MATRICULA
    End If

    If e.Row.RowType = DataControlRowType.DataRow Then
      ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVrespuestas, "Select$" & e.Row.RowIndex).ToString())

      ' convierto el campo de respuesta de modo que se presente con estilos, no como markup
      Dim s As String = Regex.Replace(HttpUtility.HtmlDecode(e.Row.Cells(Utils.Tables.GetColumnIndexByName(GVrespuestas, "Respuesta")).Text), "<[^>]*(>|$)", String.Empty)
      Dim l As Integer = s.Length
      Dim continua As String = ""
      If l > 100 Then
        l = 100
        continua = "...(Continúa)"
      End If
      e.Row.Cells(Utils.Tables.GetColumnIndexByName(GVrespuestas, "Respuesta")).Text = s.Substring(0, l) & continua

      ' convierto el campo de planteamiento de modo que se presente con estilos, no como markup
      s = Regex.Replace(HttpUtility.HtmlDecode(e.Row.Cells(Utils.Tables.GetColumnIndexByName(GVrespuestas, "Reactivo")).Text), "<[^>]*(>|$)", String.Empty)
      l = s.Length
      continua = ""
      If l > 100 Then
        l = 100
        continua = "...(Continúa)"
      End If
      e.Row.Cells(Utils.Tables.GetColumnIndexByName(GVrespuestas, "Reactivo")).Text = s.Substring(0, l) & continua
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVrespuestas.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVrespuestas, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    For Each r In GValumnos0.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GValumnos0, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

  Protected Sub GValumnos0_PreRender(sender As Object, e As EventArgs) Handles GValumnos0.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GValumnos0.Rows.Count > 0 Then
      GValumnos0.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GValumnos0_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GValumnos0.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GValumnos0.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GValumnos0_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GValumnos0.RowDataBound
    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GValumnos0, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub
End Class
