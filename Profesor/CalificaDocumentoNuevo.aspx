﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="CalificaDocumentoNuevo.aspx.vb"
    Inherits="profesor_CalificaDocumentoNuevo" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            width: 100%;
            height: 240px;
        }

        .style30 {
            height: 24px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
            font-weight: bold;
        }

        .style24 {
            height: 107px;
        }

        .style21 {
            height: 2px;
        }

        .style20 {
            height: 6px;
        }

        .style31 {
            width: 155px;
            height: 24px;
        }

        .style32 {
            width: 630px;
        }

        .style34 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
        }

        .style38 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
        }

        .style40 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 84px;
            text-align: right;
            color: #000066;
        }

        .style41 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            color: #000066;
        }

        .style42 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 490px;
        }

        .style43 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            color: #000066;
            width: 128px;
        }

        .style44 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 57px;
        }

        .style45 {
            height: 20px;
        }

        .style46 {
            width: 630px;
            font-weight: normal;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Evaluaciones - Calificar Documentos Nuevos
    </h1>
    Permite calificar archivos entregados que aún no han sido calificados, de
	<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
    de sus
    <asp:Label ID="Label2" runat="server" Text="[GRUPOS]" />
    asignad<asp:Label ID="Label7" runat="server" Text="[O]" />s como 
	<asp:Label ID="Label3" runat="server" Text="[PROFESOR]" />.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style11">
        <tr>
            <td class="style30">&nbsp;</td>
            <td class="style31" colspan="2">&nbsp;</td>
            <td class="style31">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" 
                    AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" 
                    DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" 
                    Height="22px" 
                    Width="323px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="style30" colspan="4">
                <asp:Panel ID="PnlCalifica" runat="server" BackColor="#CCCCCC" Visible="False">
                    <table align="center" class="style32" border="1">
                        <tr>
                            <td class="style43" style="background-color: white;">
                                <asp:Label ID="Label4" runat="server" Text="[ALUMNO]" />
                                seleccionad<asp:Label ID="Label6" runat="server" Text="[O]" /></td>
                            <td class="style38" colspan="2" style="background-color: white;">
                                <asp:Label ID="LblAlumno" runat="server" Style="font-weight: 700"></asp:Label>
                            </td>
                            <td class="style34" style="background-color: white;">
                                <asp:ImageButton ID="IBmensaje" runat="server"
                                    AlternateText="Enviar mensaje al alumno"
                                    DescriptionUrl="Enviar mensaje al alumno" ImageUrl="~/Resources/imagenes/mensaje.jpg"
                                    Style="text-align: right" />
                            </td>
                        </tr>
                        <tr>
                            <td class="style43" style="background-color: white;">
                                <asp:Label ID="Label5" runat="server" Text="[ASIGNATURA]" />
                            </td>
                            <td class="style38" colspan="3" style="background-color: white;">
                                <asp:Label ID="LblAsignatura" runat="server" Style="font-weight: 700"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style43" style="background-color: white;">Actividad</td>
                            <td class="style38" colspan="3" style="background-color: white;">
                                <asp:Label ID="LblActividad" runat="server" Style="font-weight: 700"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style43" style="background-color: white;">Archivo entregado</td>
                            <td class="style42" style="background-color: white;">
                                <asp:HyperLink ID="HLarchivo" runat="server"
                                    Style="font-weight: 700; font-size: medium;" Target="_blank">[HLarchivo]</asp:HyperLink>
                            </td>
                            <td class="style40" style="background-color: white;">Calificación</td>
                            <td class="style44" style="background-color: white;">
                                <asp:DropDownList ID="DDLcalifica" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style41" colspan="3" style="background-color: white;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td>
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <asp:TextBox ID="tbRetro" runat="server" TextMode="MultiLine" Width="425" Rows="5"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left;">
                                                        <asp:Button ID="btnGuardaRetro" runat="server" 
                                                            CssClass="defaultBtn btnThemeGrey btnThemeSlick" Text="Guardar Retroalimentación" />
                                                        <br /><br />
                                                        <asp:Button ID="btnEliminaRetro" runat="server" 
                                                            CssClass="defaultBtn btnThemeGrey btnThemeSlick" Text="Eliminar Retroalimentación" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="style44" style="vertical-align: top;">
                                <asp:Button ID="BtnGuardar" runat="server" Text="Guardar Calificación"
                                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                <br />
                                <br />
                                <asp:Button ID="BtnQuitar" runat="server" 
                                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" Text="Quitar Calificación" />
                            </td>
                        </tr>
                        <tr>
                            <td class="style41" colspan="4">
                                <asp:GridView ID="GValumnos0" runat="server" 
                                    AllowSorting="True"
                                    AutoGenerateColumns="False" 
                                    Caption="APOYOS PARA LA ACTIVIDAD"

                                    DataSourceID="SDSapoyos" 
                                    Width="618px"

                                    CssClass="dataGrid_clear"
                                    GridLines="None">
                                    <Columns>
                                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                                            SortExpression="Consecutivo">
                                            <HeaderStyle Width="60px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripción"
                                            SortExpression="Descripcion">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ArchivoApoyo" HeaderText="ArchivoApoyo"
                                            SortExpression="ArchivoApoyo" Visible="False" />
                                        <asp:HyperLinkField DataNavigateUrlFields="ArchivoApoyo"
                                            DataNavigateUrlFormatString="~/Resources/apoyo/{0}" DataTextField="ArchivoApoyo"
                                            HeaderText="Archivo" Target="_blank" ItemStyle-CssClass="columnaHyperlink">
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:HyperLinkField>
                                    </Columns>
                                    <FooterStyle CssClass="footer" />
                                    <PagerStyle CssClass="pager" />
                                    <SelectedRowStyle CssClass="selected" />
                                    <HeaderStyle CssClass="header" />
                                    <AlternatingRowStyle CssClass="altrow" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style30" colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td class="style24" colspan="4">
                <asp:GridView ID="GVdatos" runat="server"
                    AllowSorting="True"
                    AutoGenerateColumns="False"

                    DataSourceID="SDSalumnosterminaron" 
                    DataKeyNames="IdAlumno, Login, IdEvaluacion, IdAsignatura, IdGrupo, IdGrado, Retroalimentacion"
                    Width="883px" 

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="Evaluacion (Actividad)" HeaderText="Evaluacion (Actividad)"
                            SortExpression="Evaluacion (Actividad)" />
                        <asp:BoundField DataField="Abreviación" HeaderText="Abreviación"
                            SortExpression="Abreviación" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo"
                            SortExpression="Tipo" />
                        <asp:BoundField DataField="Periodo Califica" HeaderText="Periodo Califica"
                            SortExpression="Periodo Califica" />
                        <asp:BoundField DataField="Asignatura" HeaderText="[Asignatura]"
                            SortExpression="Asignatura" />
                        <asp:BoundField DataField="Matricula" HeaderText="[Matrícula]"
                            SortExpression="Matricula" />
                        <asp:BoundField DataField="Nombre Alumno" HeaderText="Nombre del [ALUMNO]"
                            ReadOnly="True" SortExpression="Nombre Alumno" />
                        <asp:BoundField DataField="Nivel" HeaderText="[NIVEL]" SortExpression="Nivel" />
                        <asp:BoundField DataField="Grado" HeaderText="[GRADO]" SortExpression="Grado" />
                        <asp:BoundField DataField="Grupo" HeaderText="[GRUPO]" SortExpression="Grupo" />
                        <asp:BoundField DataField="Documento" HeaderText="Archivo Entregado"
                            SortExpression="Documento" />
                        <asp:BoundField DataField="FechaEntregado" HeaderText="Fecha de Entrega" SortExpression="FechaEntregado"/>
                        <asp:BoundField DataField="Equipo" HeaderText="[EQUIPO]" SortExpression="Equipo" />
                        <asp:BoundField DataField="Retroalimentacion" HeaderText="Retroalimentación" SortExpression="Retroalimentacion" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- 
                    sort YES - rowdatabound
                --%>
            </td>
        </tr>
        <tr>
            <td class="style21" colspan="2">&nbsp;</td>
            <td class="style21" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style20" colspan="4">
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/profesor/CalificaDocumento.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSalumnosterminaron" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="
SELECT 
	E.IdEvaluacion, 
	E.ClaveBateria as 'Evaluacion (Actividad)', 
	E.ClaveAbreviada as 'Abreviación', 
	E.Tipo, 
	C.Descripcion 'Periodo Califica', 
	Asi.Descripcion Asignatura, 
	Asi.IdAsignatura,

	G.IdGrupo,
	G.Descripcion Grupo,
	Gr.IdGrado,
	Gr.Descripcion Grado,
	N.Descripcion Nivel,
	P.IdPlantel,
	P.Descripcion Plantel, 

	A.IdAlumno, 
	A.Matricula, 
	A.ApePaterno + ' ' + A.ApeMaterno + ', ' + A.Nombre as 'Nombre Alumno',
	DE.Documento, 
    DE.FechaEntregado,
    DE.Retroalimentacion,
	A.Equipo,
    U.Login
FROM 
	Calificacion C, 
	Evaluacion E, 
	Asignatura Asi,
	Grado Gr,
	(SELECT DISTINCT 
		G.IdGrupo, 
		G.IdGrado, 
		G.IdPlantel, 
		G.IdCicloEscolar 
	FROM 
		Usuario U, 
		Profesor P, 
		Grupo G, 
		Programacion Pr 
	WHERE 
		U.Login = @Login 
        AND U.IdUsuario = P.IdUsuario
		AND Pr.IdProfesor = P.IdProfesor 
		AND Pr.IdCicloEscolar = @IdCicloEscolar 
		AND G.IdGrupo = Pr.IdGrupo 
		AND G.IdCicloEscolar = @IdCicloEscolar) AS GP,
	Grupo G,
	Nivel N,
	Plantel P,
	Alumno A,
	DoctoEvaluacion DE, 
    Usuario U
WHERE 
	Gr.IdGrado = GP.IdGrado
	AND Asi.IdGrado = Gr.IdGrado 
	AND C.IdAsignatura = Asi.IdAsignatura 
	AND Asi.IdAsignatura IN (select IdAsignatura from Programacion where IdProfesor in (select IdProfesor from Profesor Pr, Usuario U where U.Login = @Login and Pr.IdUsuario = U.IdUsuario) 
    and IdGrupo IN (SELECT DISTINCT 
		G.IdGrupo
	FROM 
		Usuario U, 
		Profesor P, 
		Grupo G, 
		Programacion Pr 
	WHERE 
		U.Login = @Login 
        AND U.IdUsuario = P.IdUsuario
		AND Pr.IdProfesor = P.IdProfesor 
		AND Pr.IdCicloEscolar = @IdCicloEscolar 
		AND G.IdGrupo = Pr.IdGrupo 
		AND G.IdCicloEscolar = @IdCicloEscolar)) 
	and C.IdCicloEscolar = GP.IdCicloEscolar
	and E.IdCalificacion = C.IdCalificacion 
	and E.SeEntregaDocto = 'True'
	and G.IdGrupo = GP.IdGrupo
	AND Gr.IdNivel = N.IdNivel
	AND GP.IdPlantel = P.IdPlantel
	AND E.IdEvaluacion = DE.IdEvaluacion
	AND A.IdAlumno = DE.IdAlumno
	and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido')
	AND A.IdGrupo = G.IdGrupo
    AND A.IdUsuario = U.IdUsuario
    AND DE.Calificacion IS NULL">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSapoyos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Distinct Consecutivo,Descripcion,ArchivoApoyo from
ApoyoEvaluacion
where IdProgramacion = @IdProgramacion and
IdEvaluacion = @IdEvaluacion
order by Consecutivo">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdProgramacion" SessionField="IdProgramacion" />
                        <asp:SessionParameter Name="IdEvaluacion" SessionField="IdEvaluacion" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSdoctoevaluacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [DoctoEvaluacion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluacionterminada" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [EvaluacionTerminada]"></asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>
                    <asp:SqlDataSource ID="SDScomunicacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Comunicacion]"></asp:SqlDataSource>

            </td>
            <td></td>
        </tr>
    </table>
</asp:Content>

