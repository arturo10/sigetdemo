﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="MuestraApoyos.aspx.vb"
    Inherits="profesor_MuestraApoyos"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            text-align: center;
            height: 22px;
        }

        .style12 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: medium;
            font-weight: bold;
        }

        .style29 {
            width: 176px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style30 {
            height: 32px;
        }

        .style31 {
            text-align: left;
            height: 36px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style32 {
            text-align: left;
            height: 11px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style36 {
            font-weight: normal;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Consultas - Material de Temas
    </h1>
    Permite consultar los apoyos didácticos que tiene un tema específico y que se le presentan a 
    <asp:Label ID="Label4" runat="server" Text="[EL]" /> 
	<asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
    en las actividades relacionadas con 
                    este tema.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style31" colspan="5">Elija el tema del que desea consultar los apoyos:</td>
        </tr>
        <tr>
            <td class="style11" colspan="5">
                <table class="style10">
                    <tr>
                        <td class="style29">
                            <asp:Label ID="Label2" runat="server" Text="[GRUPO]" />
                        </td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="DDLgrados" runat="server" AutoPostBack="True"
                                DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                                Height="22px" Width="320px">
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style29">
                            <asp:Label ID="Label3" runat="server" Text="[ASIGNATURA]" /></td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="DDLasignaturas" runat="server" AutoPostBack="True"
                                DataSourceID="SDSmaterias" DataTextField="Descripcion"
                                DataValueField="IdAsignatura" Height="22px" Width="320px">
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style29">Tema</td>
                        <td style="text-align: left">
                            <asp:DropDownList ID="DDLtemas" runat="server" AutoPostBack="True"
                                DataSourceID="SDStemas" DataTextField="Descripcion" DataValueField="IdTema"
                                Height="22px" Width="320px">
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="#CCCCCC" class="style11" colspan="5">
                <span class="style12">Material de Consulta para el Tema elegido.</span></td>
        </tr>
        <tr>
            <td class="style11" colspan="5"></td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:GridView ID="GVmaterial" runat="server"
                    AllowPaging="True"
                    AllowSorting="True"
                    AutoGenerateColumns="False"
                    Caption="<h3>Seleccione el material de apoyo que desea consultar</h3>"

                    DataSourceID="SDSapoyos"
                    DataKeyNames="archivoMaterial"
                    Width="884px"
                    
                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"
                            SortExpression="Asignatura" />
                        <asp:BoundField DataField="NumTema" HeaderText="No. Tema"
                            SortExpression="NumTema" Visible="False" />
                        <asp:BoundField DataField="Tema" HeaderText="Tema" SortExpression="Tema" />
                        <asp:BoundField DataField="Numsubtema" HeaderText="No. Subtema"
                            SortExpression="Numsubtema" Visible="False" />
                        <asp:BoundField DataField="Subtema" HeaderText="Subtema"
                            SortExpression="Subtema" />
                        <asp:BoundField DataField="Material" HeaderText="Material"
                            SortExpression="Material"
                            HeaderStyle-CssClass="hiddenContainer" ItemStyle-CssClass="hiddenContainer" />
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabound
	                0  select
	                1  ASIGNATURA
	                2  numtema
	                3  tema
	                4  numsubtema
	                5  subtema
	                6  material
	                7  tipo
	                8  
	                9  
	                10 
                --%>
            </td>
        </tr>
        <tr>
            <td colspan="5" style="text-align: center" class="style30">
                <asp:Label ID="LblMensaje" runat="server"
                    CssClass="LabelInfoDefault"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:Panel ID="PnlDespliega" runat="server" BackColor="#E3EAEB">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select Distinct G.IdGrado, G.Descripcion
from Grado G, Programacion P, Asignatura A
where A.IdAsignatura = P.IdAsignatura and
G.IdGrado = A.IdGrado and P.IdProfesor in 
(select P.IdProfesor from Profesor P, Usuario U
where U.Login = @Login and P.IdUsuario = U.IdUsuario)">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSmaterias" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdAsignatura, Descripcion
from Asignatura
where IdGrado = @IdGrado
order by Descripcion">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrados" Name="IdGrado"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDStemas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdTema, Descripcion
from Tema
where IdAsignatura = @IdAsignatura
order by numero">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLasignaturas" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSapoyos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT distinct A.Descripcion AS Asignatura, T.Numero AS NumTema, T.Descripcion AS Tema, S.Numero AS Numsubtema, S.Descripcion AS Subtema, M.Consecutivo, M.Descripcion AS Material, M.ArchivoApoyo AS archivoMaterial, M.TipoArchivoApoyo AS Tipo
FROM Tema T, Subtema S, Asignatura A, ApoyoSubtema M
WHERE S.IdTema = @IdTema and T.IdTema = S.IdTema
and A.IdAsignatura = T.IdAsignatura and M.IdSubtema = S.IdSubtema
ORDER BY T.Numero, S.Numero, M.Consecutivo, A.Descripcion, T.Descripcion, S.Descripcion, M.ArchivoApoyo, M.TipoArchivoApoyo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLtemas" Name="IdTema"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>

