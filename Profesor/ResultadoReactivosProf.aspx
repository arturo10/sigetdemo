﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="ResultadoReactivosProf.aspx.vb"
    Inherits="profesor_ResultadoReactivosProf"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-family: Arial;
            font-size: small;
            font-weight: bold;
            height: 38px;
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }

        .style33 {
            height: 34px;
        }

        .style34 {
            font-family: Arial;
            font-size: small;
            font-weight: bold;
            height: 26px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Resultados - Reactivos
    </h1>
    Muestra la cantidad de aciertos y errores de cada reactivo que compone una actividad específica en sus 
	<asp:Label ID="Label1" runat="server" Text="[GRUPOS]" />
    asignad<asp:Label ID="Label7" runat="server" Text="[O]" />s
    , permitiendo ampliar el detalle con una matriz donde cruza cada 
	<asp:Label ID="Label2" runat="server" Text="[ALUMNO]" />
    con cada reactivo.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style24" colspan="3">
                <asp:Label ID="Label3" runat="server" Text="[CICLO]" /></td>
            <td colspan="3">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px"
                    Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="6" class="style33">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:GridView ID="GVdatos" runat="server"
                    AllowPaging="True"
                    AllowSorting="True"
                    AutoGenerateColumns="False"
                    Caption="<h3>Seleccione alguno de los GRUPOS que tiene asignados</h3>"

                    DataSourceID="SDSgrupos"
                    DataKeyNames="IdGrupo,IdGrado,IdPlantel"
                    Width="873px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="IdProfesor" HeaderText="IdProfesor"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdProfesor"
                            Visible="False" />
                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo" InsertVisible="False"
                            ReadOnly="True" SortExpression="IdGrupo" Visible="False" />
                        <asp:BoundField DataField="Grupo" HeaderText="Grupo" SortExpression="Grupo" />
                        <asp:BoundField DataField="IdGrado" HeaderText="IdGrado"
                            SortExpression="IdGrado" InsertVisible="False" ReadOnly="True"
                            Visible="False" />
                        <asp:BoundField DataField="Grado" HeaderText="Grado" SortExpression="Grado" />
                        <asp:BoundField DataField="Nivel" HeaderText="Nivel"
                            SortExpression="Nivel" />
                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel"
                            Visible="False" />
                        <asp:BoundField DataField="Plantel" HeaderText="Plantel"
                            SortExpression="Plantel" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%-- sort YES - rowdatabind
	                0  select
	                1  idprofesor
	                2  idgrupo
	                3  GRUPO
	                4  idgrado
	                5  GRADO
	                6  NIVEL
	                7  idplantel
	                8  PLANTEL
                --%>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="4">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" class="style24">
                <asp:Label ID="Label4" runat="server" Text="[ASIGNATURA]" /></td>
            <td colspan="3">
                <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                    DataSourceID="SDSasignatura" DataTextField="Descripcion"
                    DataValueField="IdAsignatura" Height="22px" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="style24">Calificación</td>
            <td colspan="3">
                <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True"
                    DataSourceID="SDScalificaciones" DataTextField="Descripcion"
                    DataValueField="IdCalificacion" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="style24">Actividad</td>
            <td colspan="3">
                <asp:DropDownList ID="DDLactividad" runat="server" AutoPostBack="True"
                    DataSourceID="SDSactividades" DataTextField="ClaveBateria"
                    DataValueField="IdEvaluacion" Width="350px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="6"
                style="font-family: Arial, Helvetica, sans-serif; font-size: small">
                <asp:GridView ID="GVreporte" runat="server"
                    AllowSorting="True" 
                    AutoGenerateColumns="False" 

                    DataSourceID="SDSreactivos"
                    Width="883px"

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="No. Subtema" HeaderText="No. Subtema"
                            SortExpression="No. Subtema" />
                        <asp:BoundField DataField="Subtema" HeaderText="Subtema"
                            SortExpression="Subtema" />
                        <asp:BoundField DataField="No. Reactivo" HeaderText="No. Reactivo"
                            SortExpression="No. Reactivo" />
                        <asp:BoundField DataField="Reactivo" HeaderText="Reactivo"
                            SortExpression="Reactivo" />
                        <asp:BoundField DataField="Respuestas Acertadas"
                            HeaderText="Respuestas Acertadas" ReadOnly="True"
                            SortExpression="Respuestas Acertadas">
                            <ItemStyle BackColor="#99FF66" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Respuestas Erróneas"
                            HeaderText="Respuestas Erróneas" ReadOnly="True"
                            SortExpression="Respuestas Erróneas">
                            <ItemStyle BackColor="#FF9999" />
                        </asp:BoundField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: center">
                <asp:HyperLink ID="LinkDetalle" runat="server"
                    CssClass="defaultBtn btnThemeBlue btnThemeWide"
                    NavigateUrl="~/profesor/ReactivosAlumnosProf.aspx"
                    Visible="False">
                    Ver detalle de 
                    <asp:Label ID="Label5" runat="server" Text="[ALUMNOS]" />
                    del 
                    <asp:Label ID="Label6" runat="server" Text="[GRUPO]" />
                </asp:HyperLink></td></tr><tr>
            <td colspan="6">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink></td></tr></table><table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="select distinct P.IdProfesor, G.IdGrupo, G.Descripcion as Grupo, Gr.IdGrado, Gr.Descripcion Grado, N.Descripcion as Nivel, Pl.IdPlantel, Pl.Descripcion as Plantel
from Usuario U, Profesor P, Grupo G, Grado Gr, Nivel N, Plantel Pl, Programacion Pr
where U.Login = @Login and P.IdUsuario = U.IdUsuario and Pr.IdProfesor = P.IdProfesor
and Pr.IdCicloEscolar = @IdCicloEscolar and G.IdGrupo = Pr.IdGrupo and
 Pl.IdPlantel = G.IdPlantel and Gr.IdGrado = G.IdGrado and N.IdNivel = Gr.IdNivel
and G.IdCicloEscolar = @IdCicloEscolar
order by Pl.Descripcion, N.Descripcion, Gr.Descripcion, G.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSreactivos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select S.Numero as 'No. Subtema', S.Descripcion as Subtema, P.Consecutivo as 'No. Reactivo', P.Redaccion as Reactivo,
(select COUNT(R.Acertada)
from Respuesta R, Grupo G, Alumno A where R.Acertada = 'S'
and R.IdPlanteamiento = P.IdPlanteamiento and R.IdDetalleEvaluacion = DE.IdDetalleEvaluacion and A.IdAlumno = R.IdAlumno
and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido') and G.IdGrupo = R.IdGrupo and G.IdGrupo = @IdGrupo) as 'Respuestas Acertadas',
(select COUNT(R.Acertada)
from Respuesta R, Grupo G, Alumno A where R.Acertada = 'N'
and R.IdPlanteamiento = P.IdPlanteamiento and R.IdDetalleEvaluacion = DE.IdDetalleEvaluacion and A.IdAlumno = R.IdAlumno
and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido') and G.IdGrupo = R.IdGrupo and G.IdGrupo = @IdGrupo) as 'Respuestas Erróneas'
from DetalleEvaluacion DE, Tema T, Subtema S, Planteamiento P
where T.IdAsignatura = @IdAsignatura and S.IdTema = T.IdTema and S.IdSubtema = DE.IdSubtema and
DE.IdEvaluacion = @IdEvaluacion and P.IdSubtema = S.IdSubtema and P.Ocultar = 0
order by S.Numero, P.Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVdatos" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLactividad" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSasignatura" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT IdAsignatura, Descripcion 
FROM Asignatura WHERE IdGrado = @IdGrado
AND IdAsignatura in (select IdAsignatura from Programacion where IdProfesor in (select P.IdProfesor from Profesor P, Usuario U where U.Login = @Login and P.IdUsuario = U.IdUsuario))
ORDER BY IdArea">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdGrado" SessionField="IdGrado" Type="Int32" />
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdCalificacion], [Descripcion] FROM [Calificacion] WHERE (([IdCicloEscolar] = @IdCicloEscolar) AND ([IdAsignatura] = @IdAsignatura)) ORDER BY [Consecutivo]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSactividades" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdEvaluacion], [ClaveBateria] FROM [Evaluacion] WHERE ([IdCalificacion] = @IdCalificacion) ORDER BY [InicioContestar], [ClaveBateria]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion"
                            PropertyName="SelectedValue" Type="Int64" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>

