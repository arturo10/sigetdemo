﻿Imports Siget

Imports System.IO

Partial Class profesor_ReactivosAlumnosProf
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        TitleLiteral.Text = "Resultados por Reactivo"

        Label1.Text = Config.Etiqueta.GRUPOS
        Label2.Text = Config.Etiqueta.ALUMNO
        Label3.Text = Config.Etiqueta.LETRA_GRUPO
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        'Este procedimiento convierte el GridView a Excel eliminando la primera y segunda columna
        Try
            If GVactual.Rows.Count.ToString + 1 < 65536 Then
                Dim sb As StringBuilder = New StringBuilder()
                Dim sw As StringWriter = New StringWriter(sb)
                Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
                Dim pagina As Page = New Page
                Dim form = New HtmlForm
                pagina.EnableEventValidation = False
                pagina.DesignerInitialize()
                pagina.Controls.Add(form)
                form.Controls.Add(GVactual)
                pagina.RenderControl(htw)
                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
                Response.Charset = "UTF-8"
                Response.ContentEncoding = Encoding.Default
                Response.Write(sb.ToString())
                Response.End()
            Else
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVreporte, "ReactivosAlumnos")
    End Sub

    Protected Sub GVreporte_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVreporte.DataBound
        GVreporte.Caption = "<font size=""2""><b>Detalle de reactivos contestados de " &
            Config.Etiqueta.ARTDET_GRUPO & " " & Config.Etiqueta.GRUPO &
            " " + Session("Titulo") + "</b></font>"
    End Sub

    Protected Sub GVreporte_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVreporte.RowDataBound
        If (e.Row.Cells.Count > 1) And (e.Row.RowIndex > -1) Then  'Para que se realice la comparacion cuando haya mas de una columna en el Grid
            For I As Byte = 3 To e.Row.Cells.Count - 1 'Inicio a partir de la tercer columna a comparar
                If Trim(e.Row.Cells(I).Text) = "S" Then
                    e.Row.Cells(I).BackColor = Drawing.Color.LightGreen
                End If
                If Trim(e.Row.Cells(I).Text) = "N" Then 'para que no compare celdas vacias
                    e.Row.Cells(I).BackColor = Drawing.Color.LightSalmon
                End If
            Next
        End If
    End Sub
End Class
