﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="LlenarEvaluacionesProf.aspx.vb"
    Inherits="superadministrador_LlenarEvaluaciones"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="System.Web.DynamicData, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.DynamicData" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
            height: 450px;
        }

        .style11 {
            width: 97%;
        }

        .style22 {
            width: 90%;
        }

        .style14 {
            width: 268435520px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
        }

        .style16 {
            width: 268435488px;
        }

        .style24 {
            width: 268435488px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            height: 37px;
        }

        .style25 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: left;
        }

        .style26 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 66px;
        }

        .style27 {
            height: 39px;
        }

        .style28 {
            height: 36px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 900px;
        }

        .style33 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 66px;
            height: 37px;
        }

        .style34 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            height: 37px;
            text-align: left;
        }

        .style35 {
            width: 268435520px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            height: 37px;
        }

        .style37 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 96px;
        }

        .style38 {
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            width: 96px;
            height: 15px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Evaluaciones - Capturar actividad
    </h1>
    Permite capturar las respuestas que hizo 
    <asp:Label ID="Label9" runat="server" Text="[UN]" />
    <asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
    en una actividad de opción múltiple o respuesta abierta.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td style="text-align: center" class="style28" colspan="5">
                <asp:Label ID="Mensaje0" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #000066; text-align: left;">Seleccione los siguientes datos para realizar la Evaluación del 
					<asp:Label ID="Label8" runat="server" Text="[ALUMNO]" />:</asp:Label></td></tr><tr>
            <td colspan="5">
                <table class="style11">
                    <tr>
                        <td>
                            <table class="style22">
                                <tr>
                                    <td class="style37">
                                        <asp:Label ID="Label2" runat="server" Text="[CICLO]" /></td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                                            DataValueField="IdCicloEscolar" Width="450px" CssClass="style25"
                                            Height="22px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td><td class="style16">&nbsp;</td></tr><tr>
                                    <td class="style37">
                                        <asp:Label ID="Label3" runat="server" Text="[NIVEL]" /></td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLnivel" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSniveles" DataTextField="Descripcion" DataValueField="IdNivel"
                                            Height="22px" Width="450px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td><td class="style16">&nbsp;</td></tr><tr>
                                    <td class="style37">
                                        <asp:Label ID="Label4" runat="server" Text="[GRADO]" /></td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLgrado" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSgrados" DataTextField="Descripcion" DataValueField="IdGrado"
                                            Height="22px" Width="450px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td><td class="style16">&nbsp;</td></tr><tr>
                                    <td class="style37">
                                        <asp:Label ID="Label5" runat="server" Text="[PLANTEL]" /></td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLplantel" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSplanteles" DataTextField="Descripcion"
                                            DataValueField="IdPlantel" Height="22px" Width="450px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td><td class="style16">&nbsp;</td></tr><tr>
                                    <td class="style37">
                                        <span class="estandar" style="text-align: right">
                                            <asp:Label ID="Label6" runat="server" Text="[GRUPO]" /></span></td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLgrupo" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSgrupos" DataTextField="Descripcion"
                                            DataValueField="IdGrupo" Width="450px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td><td class="style16">&nbsp;</td></tr><tr>
                                    <td class="style37">
                                        <asp:Label ID="Label7" runat="server" Text="[ASIGNATURA]" /></td>
                                    <td style="text-align: left">
                                        <asp:DropDownList ID="DDLasignatura" runat="server" AutoPostBack="True"
                                            DataSourceID="SDSasignaturas" DataTextField="Descripcion"
                                            DataValueField="IdAsignatura" Width="450px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td><td class="style16">&nbsp;</td></tr><tr>
                                    <td class="style37">Calificación</td><td style="text-align: left">
                                        <asp:DropDownList ID="DDLcalificacion" runat="server" AutoPostBack="True"
                                            DataSourceID="SDScalificaciones" DataTextField="Descripcion"
                                            DataValueField="IdCalificacion" Width="450px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="style14" style="text-align: right">&nbsp;</td><td class="style16">&nbsp;</td></tr><tr>
                                    <td colspan="2" style="text-align: left;">
                                        <asp:HyperLink ID="HyperLink3" runat="server"
                                            NavigateUrl="~/"
                                            CssClass="defaultBtn btnThemeGrey btnThemeWide">
											Regresar&nbsp;al&nbsp;Menú
                                        </asp:HyperLink></td><td class="style35" style="text-align: right"></td>
                                    <td class="style24"></td>
                                </tr>
                                <tr>
                                    <td class="style37">Actividad para capturar</td><td class="style25" colspan="3">
                                        <asp:GridView ID="GVevaluaciones" runat="server"
                                            AutoGenerateColumns="False" 
                                            AllowSorting="True"
                                            
                                            DataKeyNames="IdEvaluacion" 
                                            DataSourceID="SDSevaluaciones"
                                            Width="830px" 

                                            CssClass="dataGrid_clear_selectable"
                                            GridLines="None">
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="true" ItemStyle-CssClass="selectCell" />
                                                <asp:BoundField DataField="IdEvaluacion" HeaderText="Id Actividad"
                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion"
                                                    Visible="False" />
                                                <asp:BoundField DataField="IdCalificacion"
                                                    HeaderText="IdCalificacion"
                                                    SortExpression="IdCalificacion" Visible="False" />
                                                <asp:BoundField DataField="ClaveBateria" HeaderText="Clave de la Actividad"
                                                    SortExpression="ClaveBateria" />
                                                <asp:BoundField DataField="ClaveAbreviada" HeaderText="Clave p/Reporte"
                                                    SortExpression="ClaveAbreviada" />
                                                <asp:BoundField DataField="Porcentaje" HeaderText="% de la Calificación"
                                                    SortExpression="Porcentaje">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="InicioContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                    HeaderText="Inicia" SortExpression="InicioContestar" />
                                                <asp:BoundField DataField="FinContestar" DataFormatString="{0:dd/MM/yyyy}"
                                                    HeaderText="Termina" SortExpression="FinContestar" />
                                                <asp:BoundField DataField="FinSinPenalizacion"
                                                    DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Máxima"
                                                    SortExpression="FinSinPenalizacion" />
                                                <asp:BoundField DataField="Penalizacion" HeaderText="% Penalización"
                                                    SortExpression="Penalizacion">
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Tipo" HeaderText="Tipo" SortExpression="Tipo" />
                                            </Columns>
                                            <FooterStyle CssClass="footer" />
                                            <PagerStyle CssClass="pager" />
                                            <SelectedRowStyle CssClass="selected" />
                                            <HeaderStyle CssClass="header" />
                                            <AlternatingRowStyle CssClass="altrow" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style37">&nbsp;</td><td class="style25" colspan="3">&nbsp;</td></tr><tr>
                                    <td class="style37">&nbsp;</td><td class="style25" colspan="3">
                                        <asp:GridView ID="GValumnos" runat="server"
                                            AllowPaging="True"
                                            AllowSorting="True"
                                            AutoGenerateColumns="False"
                                            Caption="<h3>ALUMNOS con Actividad Pendiente de Contestar. Seleccione Alguno</h3>"
                                            PageSize="20"

                                            DataKeyNames="IdAlumno,IdPlantel"
                                            DataSourceID="SDSalumnos"
                                            Width="835px"

                                            CssClass="dataGrid_clear_selectable"
                                            GridLines="None">
                                            <Columns>
                                                <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                                                <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno"
                                                    Visible="False" />
                                                <asp:BoundField DataField="IdUsuario" HeaderText="IdUsuario"
                                                    SortExpression="IdUsuario" Visible="False" />
                                                <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo"
                                                    SortExpression="IdGrupo" Visible="False" />
                                                <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                                                    SortExpression="IdPlantel" Visible="False" />
                                                <asp:BoundField DataField="Equipo" HeaderText="Equipo"
                                                    SortExpression="Equipo" />
                                                <asp:BoundField DataField="ApePaterno" HeaderText="Apellido Paterno"
                                                    SortExpression="ApePaterno" />
                                                <asp:BoundField DataField="ApeMaterno" HeaderText="Apellido Materno"
                                                    SortExpression="ApeMaterno" />
                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre(s)"
                                                    SortExpression="Nombre" />
                                                <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                                                    SortExpression="Matricula" />
                                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                                <asp:BoundField DataField="FechaIngreso" HeaderText="FechaIngreso"
                                                    SortExpression="FechaIngreso" Visible="False" />
                                                <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                    SortExpression="Estatus" />
                                                <asp:BoundField DataField="FechaModif" HeaderText="FechaModif"
                                                    SortExpression="FechaModif" Visible="False" />
                                                <asp:BoundField DataField="Modifico" HeaderText="Modifico"
                                                    SortExpression="Modifico" Visible="False" />
                                            </Columns>
                                            <FooterStyle CssClass="footer" />
                                            <PagerStyle CssClass="pager" />
                                            <SelectedRowStyle CssClass="selected" />
                                            <HeaderStyle CssClass="header" />
                                            <AlternatingRowStyle CssClass="altrow" />
                                        </asp:GridView>
                                        <%-- sort YES - rowdatabound
	                                0  select
	                                1  idalumno
	                                2  idusuario
	                                3  idgrupo
	                                4  idplantel
	                                5  EQUIPO
	                                6  apepaterno
	                                7  apematerno
	                                8  nombre
	                                9  matricula
	                                10 email
                                    11 fechaingreso
                                    12 estatus
                                    13 fechamodif
                                    14 modifico
                                        --%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style37">&nbsp;</td><td class="style25" colspan="3">&nbsp;</td></tr><tr>
                                    <td colspan="4">
                                        <uc1:msgInfo runat="server" ID="msgInfo" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <uc1:msgError runat="server" ID="msgError" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style37">&nbsp;</td><td class="style25" colspan="3">
                                        <asp:GridView ID="GValumnosterminados" runat="server"
                                            AllowPaging="True"
                                            AllowSorting="True"
                                            AutoGenerateColumns="False"
                                            Caption="<h3>ALUMNOS con Actividad Contestada</h3>" 
                                            PageSize="20"

                                            DataKeyNames="IdAlumno"
                                            DataSourceID="SDSalumnosterminados"
                                            Width="835px"

                                            CssClass="dataGrid_clear"
                                            GridLines="None">
                                            <Columns>
                                                <asp:BoundField DataField="IdAlumno" HeaderText="IdAlumno"
                                                    InsertVisible="False" ReadOnly="True" SortExpression="IdAlumno"
                                                    Visible="False" />
                                                <asp:BoundField DataField="Equipo" HeaderText="Equipo"
                                                    SortExpression="Equipo" />
                                                <asp:BoundField DataField="ApePaterno" HeaderText="Apellido Paterno"
                                                    SortExpression="ApePaterno" />
                                                <asp:BoundField DataField="ApeMaterno" HeaderText="Apellido Materno"
                                                    SortExpression="ApeMaterno" />
                                                <asp:BoundField DataField="Nombre" HeaderText="Nombre(s)"
                                                    SortExpression="Nombre" />
                                                <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                                                    SortExpression="Matricula" />
                                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                                                <asp:BoundField DataField="Resultado" DataFormatString="{0:0.00}"
                                                    HeaderText="Calificación (%)" SortExpression="Resultado" />
                                                <asp:BoundField DataField="FechaTermino" DataFormatString="{0:dd/MM/yyyy}"
                                                    HeaderText="Fecha Terminada" SortExpression="FechaTermino" />
                                            </Columns>
                                            <FooterStyle CssClass="footer" />
                                            <PagerStyle CssClass="pager" />
                                            <SelectedRowStyle CssClass="selected" />
                                            <HeaderStyle CssClass="header" />
                                            <AlternatingRowStyle CssClass="altrow" />
                                        </asp:GridView>
                                        <%-- sort YES - rowdatabound
	                                        0  idalumno
	                                        1  EQUIPO
	                                        2  appaterno
	                                        3  apmaterno
	                                        4  nombre
	                                        5  matricula
	                                        6  email
	                                        7  resultado
	                                        8  fechatermino
                                        --%>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="5">&nbsp; </td></tr><tr>
            <td style="text-align: left">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink></td><td style="text-align: left">&nbsp;</td><td style="text-align: left">&nbsp;</td><td style="text-align: left">&nbsp;</td><td style="text-align: left">&nbsp;</td></tr><tr>
            <td colspan="5">&nbsp;</td></tr></table><table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSalumnos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Alumno] WHERE ([IdGrupo] = @IdGrupo) 
and IdAlumno not in (select IdAlumno from EvaluacionTerminada where
                                 IdEvaluacion = @IdEvaluacion) and (Estatus = 'Activo' or Estatus = 'Suspendido')
ORDER BY [ApePaterno], [ApeMaterno], [Nombre]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSusuarios" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Usuario]"></asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDScalificaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT C.IdCalificacion, C.IdCicloEscolar, C.Clave, C.IdAsignatura, A.Descripcion Materia, C.Consecutivo,C.Descripcion, C.Valor
FROM Calificacion C, Asignatura A
WHERE ([IdCicloEscolar] = @IdCicloEscolar)  and (A.IdAsignatura = C.IdAsignatura) and (A.IdAsignatura = @IdAsignatura)
ORDER BY A.Descripcion, C.Consecutivo">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLasignatura" Name="IdAsignatura"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar] WHERE ([Estatus] = 'Activo') ORDER BY [FechaInicio]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSasignaturas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdAsignatura, IdGrado, Descripcion
from Asignatura
where IdAsignatura in (select IdAsignatura from Programacion 
where IdProfesor = (select P.IdProfesor from Profesor P, Usuario U where U.Login = @Login and P.IdUsuario = U.IdUsuario) and IdGrupo = @IdGrupo and IdCicloEscolar = @IdCicloEscolar)
order by Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSalumnosterminados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT A.IdAlumno, A.Nombre, A.ApePaterno, A.ApeMaterno, A.Matricula, A.Email, E.Resultado, E.FechaTermino,A.Equipo
FROM Alumno A, EvaluacionTerminada E
WHERE A.IdGrupo = @IdGrupo and 
A.IdAlumno in (select IdAlumno from EvaluacionTerminada where
                                 IdEvaluacion = @IdEvaluacion)
and E.IdAlumno = A.IdAlumno and E.IdEvaluacion = @IdEvaluacion and E.IdGrupo = A.IdGrupo 
and (A.Estatus = 'Activo' or A.Estatus = 'Suspendido')
ORDER BY ApePaterno, ApeMaterno, Nombre">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="GVevaluaciones" Name="IdEvaluacion"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdPlantel, Descripcion
from Plantel where IdPlantel in
(SELECT distinct G.IdPlantel
FROM Grupo G, Programacion P
WHERE P.IdProfesor = (select P.IdProfesor from Profesor P, Usuario U where U.Login = @Login and P.IdUsuario = U.IdUsuario) and
G.IdGrupo = P.IdGrupo and P.IdCicloEscolar = @IdCicloEscolar
and G.IdGrado = @IdGrado)
order by Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSniveles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select distinct N.IdNivel, N.Descripcion
from Nivel N, Escuela E where
N.IdNivel = E.IdNivel and E.IdPlantel in
(SELECT distinct G.IdPlantel
FROM Grupo G, Programacion P
WHERE P.IdProfesor = (select P.IdProfesor from Profesor P, Usuario U where U.Login = @Login and P.IdUsuario = U.IdUsuario) and
G.IdGrupo = P.IdGrupo and P.IdCicloEscolar = @IdCicloEscolar)
order by N.IdNivel">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSgrados" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdGrado], [Descripcion] FROM [Grado] WHERE (([IdNivel] = @IdNivel) AND ([Estatus] = @Estatus)) ORDER BY [IdGrado]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLnivel" Name="IdNivel"
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT distinct G.IdGrupo, G.Descripcion
FROM Grupo G, Programacion P
WHERE P.IdProfesor = (select P.IdProfesor from Profesor P, Usuario U where U.Login = @Login and P.IdUsuario = U.IdUsuario) and
G.IdGrupo = P.IdGrupo and P.IdCicloEscolar = @IdCicloEscolar
and G.IdGrado = @IdGrado and G.IdPlantel = @IdPlantel
and G.IdCicloEscolar = @IdCicloEscolar
order by G.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLgrado" Name="IdGrado"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLplantel" Name="IdPlantel"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluaciones" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="select IdEvaluacion, IdCalificacion, ClaveBateria, ClaveAbreviada, ClaveAbreviada, Porcentaje, InicioContestar, FinContestar, FinSinPenalizacion, Penalizacion, Tipo
from Evaluacion 
where IdCalificacion = @IdCalificacion and Califica = 'P' and SeEntregaDocto = 'False'
and IdEvaluacion not in (select IdEvaluacion from EvaluacionPlantel where IdPlantel in (select IdPlantel from Grupo where IdGrupo = @IdGrupo))
UNION
select E.IdEvaluacion, E.IdCalificacion, E.ClaveBateria, E.ClaveAbreviada, E.ClaveAbreviada, E.Porcentaje, P.InicioContestar, P.FinContestar,
P.FinSinPenalizacion, E.Penalizacion, E.Tipo
from Evaluacion E, EvaluacionPlantel P
where E.IdCalificacion = @IdCalificacion and E.Califica = 'P' and E.SeEntregaDocto = 'False' and
E.IdEvaluacion = P.IdEvaluacion and P.IdPlantel in (select IdPlantel from Grupo where IdGrupo = @IdGrupo)
order by IdCalificacion, InicioContestar">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLcalificacion" Name="IdCalificacion"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="DDLgrupo" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

                <asp:SqlDataSource ID="SDSreactivos" runat="server"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</asp:Content>
