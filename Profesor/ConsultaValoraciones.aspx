﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="ConsultaValoraciones.aspx.vb"
    Inherits="profesor_ConsultaValoraciones"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            font-family: Arial;
            font-size: small;
            font-weight: bold;
            height: 22px;
            color: #000000;
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Resultados - Mis Valoraciones
    </h1>
    Permite consultar los archivos donde se encuentra su valoración como 
	<asp:Label ID="Label1" runat="server" Text="[PROFESOR]" />
     en 
    <asp:Label ID="Label6" runat="server" Text="[UN]" /> 
	<asp:Label ID="Label2" runat="server" Text="[GRUPO]" />
    , 
	<asp:Label ID="Label3" runat="server" Text="[ASIGNATURA]" />
     y 
	<asp:Label ID="Label4" runat="server" Text="[CICLO]" />
     específic<asp:Label ID="Label7" runat="server" Text="[O]" />s.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td class="style24" colspan="2">
                <asp:Label ID="Label5" runat="server" Text="[CICLO]" />
            </td>
            <td colspan="2" style="text-align: left">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px"
                    Width="330px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="GVdatos" runat="server"
                    AllowSorting="True"
                    AutoGenerateColumns="False"
                    Caption="<h3>GRUPOS y ASIGNATURAS asignados en el CICLO elegido</h3>"

                    DataSourceID="SDSgrupos"
                    DataKeyNames="IdProfesor,IdGrupo,IdGrado,IdPlantel" 
                    Width="884px"

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="IdProfesor" HeaderText="IdProfesor"
                            InsertVisible="False" ReadOnly="True" SortExpression="IdProfesor"
                            Visible="False" />
                        <asp:BoundField DataField="Asignatura" HeaderText="[ASIGNATURA]"
                            SortExpression="Asignatura" />
                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo"
                            SortExpression="IdGrupo" InsertVisible="False" ReadOnly="True"
                            Visible="False" />
                        <asp:BoundField DataField="Grupo" HeaderText="[GRUPO]"
                            SortExpression="Grupo" />
                        <asp:BoundField DataField="IdGrado" HeaderText="IdGrado"
                            SortExpression="IdGrado" InsertVisible="False" ReadOnly="True"
                            Visible="False" />
                        <asp:BoundField DataField="Grado" HeaderText="[GRADO]"
                            SortExpression="Grado" />
                        <asp:BoundField DataField="Nivel" HeaderText="[NIVEL]" SortExpression="Nivel" />
                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                            SortExpression="IdPlantel" InsertVisible="False" ReadOnly="True"
                            Visible="False" />
                        <asp:BoundField DataField="Plantel" HeaderText="[PLANTEL]"
                            SortExpression="Plantel" />
                        <asp:BoundField DataField="ArchivoValoracion" HeaderText="ArchivoValoracion"
                            SortExpression="ArchivoValoracion" Visible="False" />
                        <asp:HyperLinkField DataNavigateUrlFields="ArchivoValoracion"
                            DataNavigateUrlFormatString="~/Resources/cargas/valoraciones/{0}"
                            DataTextField="ArchivoValoracion" HeaderText="Archivo con la Valoración"
                            ItemStyle-CssClass="columnaHyperlink"
                            Target="_blank">
                            <ItemStyle Font-Size="Small" />
                        </asp:HyperLinkField>
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
                <%--
                    sort YES - rowdatabound
                     --%>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" style="text-align: left">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar]
WHERE [Estatus] = 'Activo'
ORDER BY [Descripcion], [FechaInicio] DESC"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select distinct P.IdProfesor, A.Descripcion as Asignatura, G.IdGrupo, G.Descripcion as Grupo, Gr.IdGrado, Gr.Descripcion Grado, N.Descripcion as Nivel, Pl.IdPlantel, Pl.Descripcion as Plantel, Pr.ArchivoValoracion
from Usuario U, Profesor P, Grupo G, Grado Gr, Nivel N, Plantel Pl, Asignatura A, Programacion Pr where U.Login = @Login and P.IdUsuario = U.IdUsuario and Pr.IdProfesor = P.IdProfesor
and Pr.IdCicloEscolar = @IdCicloEscolar and G.IdGrupo = Pr.IdGrupo and
 Pl.IdPlantel = G.IdPlantel and Gr.IdGrado = G.IdGrado and N.IdNivel = Gr.IdNivel
and A.IdAsignatura = Pr.IdAsignatura and G.IdCicloEscolar = @IdCicloEscolar
order by Pl.Descripcion, N.Descripcion, Gr.Descripcion, G.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

