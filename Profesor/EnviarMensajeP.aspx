﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="EnviarMensajeP.aspx.vb"
    Inherits="profesor_EnviarMensajeP"
    MaintainScrollPositionOnPostback="true"
    ValidateRequest="false" %>

<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .style11 {
            height: 25px;
            text-align: left;
        }

        .style26 {
            height: 42px;
            text-align: left;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style23 {
            text-align: left;
            height: 27px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style24 {
            width: 143px;
            height: 5px;
        }

        .style25 {
            height: 5px;
        }

        .style21 {
            width: 273px;
        }

        .style13 {
            width: 143px;
        }

        .style28 {
            height: 14px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style29 {
            height: 25px;
            text-align: right;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style30 {
            height: 42px;
        }

        .style31 {
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
        }

        .style32 {
            width: 10px;
        }

        .style33 {
            width: 10px;
            font-weight: normal;
            font-size: small;
        }

        .style34 {
            text-align: left;
            height: 5px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #000000;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Comunicación - Enviar Mensajes
    </h1>
    Permite enviar mensajes a 
    <asp:Label ID="Label9" runat="server" Text="[LOS]" /> 
	<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
     de sus 
	<asp:Label ID="Label2" runat="server" Text="[GRUPOS]" />
     asignad<asp:Label ID="Label10" runat="server" Text="[O]" />s o a 
    <asp:Label ID="Label11" runat="server" Text="[LOS]" /> 
	<asp:Label ID="Label3" runat="server" Text="[COORDINADORES]" />
     de su 
	<asp:Label ID="Label4" runat="server" Text="[PLANTEL]" />.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr class="titulo">
            <td class="style26" colspan="5">
                <asp:Label ID="LblNombre" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #000099"></asp:Label>
            </td>
            <td class="style30" colspan="2"></td>
        </tr>
        <tr class="titulo">
            <td class="style29" colspan="2">Seleccione 
                <asp:Label ID="Label8" runat="server" Text="[UN]" /> 
                <asp:Label ID="Label5" runat="server" Text="[CICLO]" />
            </td>
            <td class="style11" colspan="3">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Width="355px">
                </asp:DropDownList>
            </td>
            <td class="style11" colspan="2">&nbsp;</td>
        </tr>
        <tr class="titulo">
            <td class="style29" colspan="2">Enviar mensaje a</td>
            <td class="style11" colspan="3">
                <asp:DropDownList ID="DDLenvia" runat="server" Width="150px"
                    AutoPostBack="True">
                    <asp:ListItem Value="A" Selected="True">Alumno</asp:ListItem>
                    <asp:ListItem Value="C">Coordinador</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="style11" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style34" colspan="4"></td>
            <td class="style24" colspan="2"></td>
            <td class="style25"></td>
        </tr>
        <tr>
            <td colspan="7">
                <asp:Panel ID="PnlAlumnos" runat="server">
                    <table>
                        <tr>
                            <td>
                                <asp:GridView ID="GVgrupos" runat="server"
                                    AllowSorting="false"
                                    AllowPaging="True"
                                    AutoGenerateColumns="False"
                                    PageSize="8"
                                    Caption="<h3>Seleccione el GRUPO al que pertenece el o los ALUMNOS.</h3>"

                                    DataKeyNames="IdGrupo,IdAsignatura,IdPlantel,IdCicloEscolar,IdProgramacion"
                                    DataSourceID="SDSgrupos"
                                    Width="860px"

                                    CssClass="dataGrid_clear_selectable"
                                    GridLines="None">
                                    <Columns>
                                        <asp:CommandField ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                                        <asp:BoundField DataField="IdGrupo" HeaderText="IdGrupo" InsertVisible="False"
                                            ReadOnly="True" SortExpression="IdGrupo" Visible="False" />
                                        <asp:BoundField DataField="Grupo" HeaderText="Grupo" SortExpression="Grupo" />
                                        <asp:BoundField DataField="IdAsignatura" HeaderText="IdAsignatura"
                                            InsertVisible="False" ReadOnly="True" SortExpression="IdAsignatura"
                                            Visible="False" />
                                        <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"
                                            SortExpression="Asignatura" />
                                        <asp:BoundField DataField="Grado" HeaderText="Grado" SortExpression="Grado" />
                                        <asp:BoundField DataField="IdPlantel" HeaderText="IdPlantel"
                                            InsertVisible="False" ReadOnly="True" SortExpression="IdPlantel"
                                            Visible="False" />
                                        <asp:BoundField DataField="Plantel" HeaderText="Plantel"
                                            SortExpression="Plantel" />
                                        <asp:BoundField DataField="IdCicloEscolar" HeaderText="IdCicloEscolar"
                                            InsertVisible="False" ReadOnly="True" SortExpression="IdCicloEscolar"
                                            Visible="False" />
                                        <asp:BoundField DataField="CicloEscolar" HeaderText="CicloEscolar"
                                            SortExpression="CicloEscolar" />
                                        <asp:BoundField DataField="IdProgramacion" HeaderText="IdProgramacion"
                                            InsertVisible="False" SortExpression="IdProgramacion" Visible="False" />
                                    </Columns>
                                    <FooterStyle CssClass="footer" />
                                    <PagerStyle CssClass="pager" />
                                    <SelectedRowStyle CssClass="selected" />
                                    <HeaderStyle CssClass="header" />
                                    <AlternatingRowStyle CssClass="altrow" />
                                </asp:GridView>
                                <%-- sort NO - prerender -
                                    0  Select
                                    1  IdGrupo
                                    2  GRUPO
                                    3  IdAsignatura
                                    4  ASIGNATURA
                                    5  GRADO
                                    6  IdPlantel
                                    7  PLANTEL
                                    8  IdCicloEscolar
                                    9  CICLO
                                    10 IdProgramacion
                                --%>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="text-align: center; font-weight: bold;">Seleccione <asp:Label ID="Label12" runat="server" Text="[EL]" /> o 
                                <asp:Label ID="Label13" runat="server" Text="[LOS]" /> 
                                <asp:Label ID="Label6" runat="server" Text="[ALUMNOS]" />
                                 a l<asp:Label ID="Label14" runat="server" Text="[O]" />s que enviará el mensaje:</td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <asp:LinkButton ID="LinkButton1" runat="server"
                                    CssClass="defaultBtn btnThemeGrey btnThemeSlick">Marcar todos</asp:LinkButton>
                                &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="LinkButton2" runat="server"
                                    CssClass="defaultBtn btnThemeGrey btnThemeSlick">Desmarcar todos</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="CBLalumnos" runat="server" DataSourceID="SDSalumnos"
                                    DataTextField="nombrecompleto" DataValueField="IdAlumno"
                                    Style="background-color: #efefef; text-align: left; margin-left: auto; margin-right: auto;" Width="494px">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style14" colspan="7">
                <asp:Panel ID="PnlCoordinador" runat="server" Visible="False">
                    <table style="width: 100%;">
                        <tr>
                            <td style="text-align: center;">
                                <b>Seleccione <asp:Label ID="Label15" runat="server" Text="[EL]" /> o 
                                    <asp:Label ID="Label16" runat="server" Text="[LOS]" /> 
                                    <asp:Label ID="Label7" runat="server" Text="[COORDINADORES]" />
                                    a l<asp:Label ID="Label17" runat="server" Text="[O]" />s que enviará el mensaje:</b></td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                    <asp:LinkButton ID="LinkButton3" runat="server"
                                    CssClass="defaultBtn btnThemeGrey btnThemeSlick">Marcar todos</asp:LinkButton>
                                    &nbsp;&nbsp;&nbsp;
                                <asp:LinkButton ID="LinkButton4" runat="server"
                                    CssClass="defaultBtn btnThemeGrey btnThemeSlick">Desmarcar todos</asp:LinkButton>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="CBLcoordinadores" runat="server"
                                    DataSourceID="SDScoordinadores" DataTextField="nombrecompleto"
                                    DataValueField="IdCoordinador"
                                    Style="background-color: #efefef; text-align: left; margin-left: auto;margin-right: auto;"
                                    Width="494px">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="2">Asunto</td>
            <td class="style14" colspan="3">
                <asp:TextBox ID="TBasunto" runat="server" MaxLength="100" Width="665px"></asp:TextBox>
            </td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="2">Contenido</td>
            <td class="style14" colspan="3">
                <asp:TextBox ID="TBcontenido" runat="server" 
                    ClientIDMode="Static" 
                    TextMode="MultiLine"
                    CssClass="tinymce"></asp:TextBox>
            </td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="2">Adjuntar archivo<br />(solo uno)</td>
            <td class="style32">
                <asp:FileUpload ID="FUadjunto" runat="server" />
            </td>
            <td class="style14">
                <asp:Button ID="BtnAdjuntar" runat="server" Text="Adjuntar"
                    CssClass="defaultBtn btnThemeGrey btnThemeSlick" />
                &nbsp;
                <asp:Button ID="BtnQuitar" runat="server" Text="Quitar"
                    CssClass="defaultBtn btnThemeGrey btnThemeSlick" />
            </td>
            <td class="style14" colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="2"></td>
            <td class="style14" colspan="5" style="text-align: left">
                <asp:LinkButton ID="lbAdjunto" runat="server" CssClass="LabelInfoDefault"></asp:LinkButton>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="2">&nbsp;</td>
            <td class="style14" colspan="3">&nbsp;</td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="2">&nbsp;</td>
            <td class="style14" colspan="3">
                <asp:Button ID="BtnEnviar" runat="server" Text="Enviar mensaje"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
                <asp:Button ID="BtnLimpiar" runat="server" Text="Limpiar"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style22">
                &nbsp;</td>
            <td class="style21" colspan="3">
                &nbsp;</td>
            <td class="style13" colspan="2">
                &nbsp;</td>
            <td class="style15">&nbsp;</td>
        </tr>
        <tr>
            <td class="style22" style="text-align: right">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="~/"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide"> Regresar&nbsp;al&nbsp;Menú </asp:HyperLink>
            </td>
            <td class="style21" colspan="3">&nbsp;</td>
            <td class="style13" colspan="2">
                &nbsp;</td>
            <td class="style15">&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSalumnos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdAlumno, ApePaterno + ' ' + ApeMaterno + ', ' + Nombre + ' (' + Matricula + ')' as nombrecompleto
from Alumno
where (Estatus = 'Activo' or Estatus = 'Suspendido') and IdGrupo = @IdGrupo
order by ApePaterno, ApeMaterno, Nombre">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GVgrupos" Name="IdGrupo"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSgrupos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select G.IdGrupo, G.Descripcion Grupo, A.IdAsignatura, A.Descripcion Asignatura, Gr.Descripcion Grado, Pl.IdPlantel, Pl.Descripcion Plantel, C.IdCicloEscolar, C.Descripcion CicloEscolar,P.IdProgramacion
from Grupo G,Programacion P, Plantel Pl, CicloEscolar C, Asignatura A, Grado Gr
where P.IdProfesor = @IdProfesor and G.IdGrupo = P.IdGrupo and C.IdCicloEscolar = @IdCicloEscolar and P.IdCicloEscolar = C.IdCicloEscolar and Pl.IdPlantel = G.IdPlantel and A.IdAsignatura = P.IdAsignatura and Gr.IdGrado = G.IdGrado and G.IdCicloEscolar = @IdCicloEscolar
order by C.IdCicloEscolar, Pl.Descripcion, A.Descripcion, Gr.Descripcion, G.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdProfesor" SessionField="IdProfesor" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloEscolar"
                            PropertyName="SelectedValue" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT [IdCicloEscolar], [Descripcion] FROM [CicloEscolar] WHERE ([Estatus] = @Estatus) ORDER BY [Descripcion]">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="Activo" Name="Estatus" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDScoordinadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select C.IdCoordinador, C.Nombre + ' ' + C.Apellidos + ' (' + P.Descripcion + ')' as nombrecompleto
from Coordinador C, Plantel P, CoordinadorPlantel CP
where CP.IdPlantel = (select IdPlantel from Profesor, Usuario where Usuario.Login = @Login and Profesor.IdUsuario = Usuario.IdUsuario)
and C.IdCoordinador = CP.IdCoordinador and P.IdPlantel = CP.IdPlantel and C.Estatus = 'Activo'
order by P.Descripcion,C.Apellidos,C.Nombre">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
        </tr>
        <tr>
            <td>

                <asp:SqlDataSource ID="SDScomunicacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Comunicacion]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSplanteles" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select distinct P.IdPlantel,P.Descripcion,P.Municipio,P.Estado
from Plantel P, Profesor Pr, Usuario U
where U.Login = @Login and Pr.IdUsuario = U.IdUsuario
and P.IdPlantel = Pr.IdPlantel
order by P.Estado, P.Descripcion">
                    <SelectParameters>
                        <asp:SessionParameter Name="Login" SessionField="Login" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDScomunicacionCP" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [ComunicacionCP]"></asp:SqlDataSource>

            </td>
            <td>
                <asp:HiddenField ID="hfAdjuntoFisico" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hfCarpetaAdjunto" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfIsReenvio" runat="server" Value="False" />
            </td>
        </tr>
    </table>
</asp:Content>

