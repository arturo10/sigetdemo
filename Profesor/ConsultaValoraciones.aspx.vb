﻿Imports Siget

Imports System.Data.SqlClient

Partial Class profesor_ConsultaValoraciones
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = Trim(User.Identity.Name)

        GVdatos.Caption = "<h3>" &
            Config.Etiqueta.GRUPOS &
            " y " & Config.Etiqueta.ASIGNATURAS &
            " asignados en " &
            Config.Etiqueta.ARTDET_CICLO & " " & Config.Etiqueta.CICLO &
            " elegido</h3>"

        Label1.Text = Config.Etiqueta.PROFESOR
        Label6.Text = Config.Etiqueta.ARTIND_GRUPO
        Label2.Text = Config.Etiqueta.GRUPO
        Label3.Text = Config.Etiqueta.ASIGNATURA
        Label4.Text = Config.Etiqueta.CICLO
        Label7.Text = Config.Etiqueta.LETRA_CICLO
        Label5.Text = Config.Etiqueta.CICLO

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader

        miComando = objConexion.CreateCommand
        'Con obtener el IdGrado es suficiente para rastrear el nivel ya que son
        'valores unicos en la tabla
        miComando.CommandText = "SELECT P.IdProfesor, P.Nombre, P.Apellidos FROM Usuario U, Profesor P where U.Login = '" + User.Identity.Name + "' and P.IdUsuario = U.IdUsuario"
        Try
            'Abrir la conexión y leo los registros
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read() 'Leo para poder accesarlos
            Session("Profesor") = misRegistros.Item("Nombre") + " " + misRegistros.Item("Apellidos")
            misRegistros.Close()
            objConexion.Close()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVdatos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVdatos.DataBound
        If (GVdatos.Rows.Count <= 0) Then
            msgInfo.show("Ud. no tiene " & Config.Etiqueta.GRUPOS &
                " y " & Config.Etiqueta.ASIGNATURAS & " asignados.")
        Else
            msgInfo.hide()
            GVdatos.Caption = "<font size=""2""><b>" & Config.Etiqueta.GRUPOS &
                " y " & Config.Etiqueta.ASIGNATURAS & " asignados en " &
                Config.Etiqueta.ARTDET_CICLO & " " & Config.Etiqueta.CICLO &
                " " + DDLcicloescolar.SelectedItem.ToString + "<br>para " &
                Config.Etiqueta.ARTDET_PROFESOR & " " & Config.Etiqueta.PROFESOR &
                " " + Session("Profesor") + "</b></font>"
        End If
    End Sub

    Protected Sub GVdatos_PreRender(sender As Object, e As EventArgs) Handles GVdatos.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVdatos.Rows.Count > 0 Then
            GVdatos.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVdatos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
                        ' ésta propiedad es importante para que se cargue a la izquierda
                        div.Attributes.Add("style", "float: left; position: absolute;")
                        ' añado el ícono al div
                        div.Controls.Add(icon)
                        ' añado el div al header
                        tc.Controls.Add(div)
                        ' reañado el link
                        tc.Controls.Add(lb)
                        ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
                        lb.Attributes.Add("style", "float: left; margin-left: 15px;")

                        ' reviso si la expresión de ordenado del gridview es igual al comando del link,
                        ' para asignarle un resaltado con css
                        If GVdatos.SortExpression = lb.CommandArgument Then
                            lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub GVdatos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVdatos.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA

            LnkHeaderText = e.Row.Cells(3).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.GRUPO

            LnkHeaderText = e.Row.Cells(5).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.GRADO

            LnkHeaderText = e.Row.Cells(6).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.NIVEL

            LnkHeaderText = e.Row.Cells(8).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.PLANTEL
        End If
    End Sub
End Class
