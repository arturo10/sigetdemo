﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="ListaAlumnosProf.aspx.vb"
    Inherits="profesor_ListaAlumnosProf"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style11 {
            height: 33px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style21 {
            width: 273px;
        }

        .style13 {
            width: 143px;
        }

        .style23 {
            height: 21px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }
        .style22 {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Resultados - Reactivos - Listado de
        <asp:Label ID="Label3" runat="server" Text="[ALUMNOS]" />
    </h1>
    Muestra la cantidad de aciertos y errores de cada reactivo que compone una actividad específica en sus 
	<asp:Label ID="Label1" runat="server" Text="[GRUPOS]" />
    asignad<asp:Label ID="Label4" runat="server" Text="[O]" />s
    , permitiendo ampliar el detalle con una matriz donde cruza cada 
	<asp:Label ID="Label2" runat="server" Text="[ALUMNO]" />
    con cada reactivo.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr class="estandar">
            <td class="style14" colspan="5">
                <asp:GridView ID="GVreporte" runat="server"
                    AllowSorting="True"
                    AutoGenerateColumns="False" 

                    DataSourceID="SDSlistaalumnos" 
                    Width="838px"

                    CssClass="dataGrid_clear"
                    GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Consecutivo" HeaderText="Consecutivo"
                            ReadOnly="True" SortExpression="Consecutivo">
                            <HeaderStyle Width="90px" />
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Matricula" HeaderText="Matrícula"
                            SortExpression="Matricula" />
                        <asp:BoundField DataField="ApePaterno" HeaderText="Apellido Paterno"
                            SortExpression="ApePaterno" />
                        <asp:BoundField DataField="ApeMaterno" HeaderText="Apellido Materno"
                            SortExpression="ApeMaterno" />
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre(s)"
                            SortExpression="Nombre" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style14">
                <asp:Button ID="BtnExportar" runat="server" Text="Exportar a Excel"
                    CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
            </td>
            <td class="style14">
                <asp:HyperLink ID="HyperLink4" runat="server"
                    NavigateUrl="~/profesor/ResultadoReactivosProf.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium">Volver al inicio</asp:HyperLink>
            </td>
            <td class="style14" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="5">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style22">
                &nbsp;</td>
            <td class="style21" colspan="2">&nbsp;</td>
            <td class="style13">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style22">
                <asp:HyperLink ID="HyperLink5" runat="server"
                    NavigateUrl="javascript:history.back()"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
            <td class="style21" colspan="2">&nbsp;</td>
            <td class="style13">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSlistaalumnos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT Rank() OVER(ORDER BY ApePaterno, ApeMaterno, Nombre) as Consecutivo, Matricula, ApePaterno, ApeMaterno, Nombre
FROM Alumno WHERE (IdGrupo = @IdGrupo) AND (Estatus = 'Activo' or Estatus = 'Suspendido') 
order by Consecutivo">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdGrupo" SessionField="IdGrupo" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>

            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

