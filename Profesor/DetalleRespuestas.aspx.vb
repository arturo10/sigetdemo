﻿Imports Siget

Imports System.IO
Imports System.Data.SqlClient

Partial Class profesor_DetalleRespuestas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        LblAlumno.Text = Session("NomAlumno")

        Label1.Text = Config.Etiqueta.ALUMNO
        Label2.Text = Config.Etiqueta.ASIGNATURA
        Label3.Text = Config.Etiqueta.ALUMNO
        Label4.Text = Config.Etiqueta.ASIGNATURA
        Label5.Text = Config.Etiqueta.ALUMNO
        Label6.Text = Config.Etiqueta.ASIGNATURA
        Label7.Text = Config.Etiqueta.ARTIND_ASIGNATURA
        Label8.Text = Config.Etiqueta.LETRA_ASIGNATURA
        Label9.Text = Config.Etiqueta.ARTDET_ASIGNATURA
    End Sub

    Protected Sub GVevaluaciones_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevaluaciones.SelectedIndexChanged
        TBrespuesta.Text = ""
        PnlResp.Visible = False
        'El SelectedValue.ToString lo lee aunque este oculto mientras este en la propiedad DataKeyName del GridView. Si solo hay un campo 
        'en DatakeyName es el que lee por default, si pongo varios campos los podré leer con GVevaluaciones.SelectedDataKey.Values(0).ToString
        Session("IdEvaluacionT") = GVevaluaciones.SelectedValue.ToString
        GVrespuestas.DataBind()
        'Lo siguiente es importante que esté despues del DataBind() anterior para que calcule el .Rows.Count
        GVrespuestas.Caption = "<b>Respuestas de opción múltiple para la actividad: " +
            GVevaluaciones.SelectedRow.Cells(6).Text +
            " <br>contestadas por " & Config.Etiqueta.ARTDET_ALUMNO & " " & Config.Etiqueta.ALUMNO & ": " + _
            Session("NomAlumno") + "</b><br>" + CStr(GVrespuestas.Rows.Count) + " Reactivos"
        If GVrespuestas.Rows.Count <= 0 Then
            BtnExportar.Visible = False
        Else
            BtnExportar.Visible = True
        End If


        GVrespuestasAbiertas.DataBind()
        'Lo siguiente es importante que esté despues del DataBind() anterior para que calcule el .Rows.Count
        GVrespuestasAbiertas.Caption = "<b>Respuestas abiertas para la actividad: " +
            GVevaluaciones.SelectedRow.Cells(6).Text +
            " <br>contestadas por " & Config.Etiqueta.ARTDET_ALUMNO & " " & Config.Etiqueta.ALUMNO & ": " + _
        Session("NomAlumno") + "</b><br>" + CStr(GVrespuestasAbiertas.Rows.Count) + " Reactivos"
        If GVrespuestasAbiertas.Rows.Count <= 0 Then
            BtnExportarA.Visible = False
        Else
            BtnExportarA.Visible = True
        End If
    End Sub

    Protected Sub Convierte(ByVal GVactual As GridView, ByVal archivo As String)
        ' añado los estilos, por que el gridview no se exporta predefinidamente con css
        GVactual.AlternatingRowStyle.BackColor = System.Drawing.ColorTranslator.FromHtml("#efefef")
        GVactual.HeaderStyle.BackColor = System.Drawing.ColorTranslator.FromHtml(Config.Color.Table_HeaderRow_Background)
        GVactual.GridLines = GridLines.Both

        Dim headerText As String
        ' quito la imágen de ordenamiento de los encabezados
        For Each tc As TableCell In GVactual.HeaderRow.Cells
            If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                headerText = CType(tc.Controls(1), LinkButton).Text ' obtengo el texto del linkbutton de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el ícono de ordenamiento
                tc.Controls.RemoveAt(0) ' quito el linkbutton de ordenamiento
                tc.Text = headerText ' establezco el texto simple
            End If
        Next

        Try
            'Para usar este procedimiento es necesario importar System.IO
            If GVactual.Rows.Count.ToString + 1 < 65536 Then
                Dim sb As StringBuilder = New StringBuilder()
                Dim sw As StringWriter = New StringWriter(sb)
                Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
                Dim pagina As Page = New Page
                Dim form = New HtmlForm
                pagina.EnableEventValidation = False
                pagina.DesignerInitialize()
                pagina.Controls.Add(form)
                form.Controls.Add(GVactual)
                pagina.RenderControl(htw)
                Response.Clear()
                Response.Buffer = True
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Disposition", "attachment;filename=" + archivo + ".xls")
                Response.Charset = "UTF-8"
                Response.ContentEncoding = Encoding.Default
                Response.Write(sb.ToString())
                Response.End()
            Else
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "Demasiados registros para Exportar a Excel.")
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
        End Try
    End Sub

    Protected Sub BtnExportar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportar.Click
        Convierte(GVrespuestas, "Respuestas de Opcion " + HttpUtility.HtmlDecode(Session("NomAlumno")))
    End Sub

    Protected Sub DDLcalificacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacion.DataBound
        DDLcalificacion.Items.Insert(0, New ListItem("---Elija un Periodo de Evaluación", 0))
    End Sub

    Protected Sub GVevaluaciones_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevaluaciones.DataBound
        PnlResp.Visible = False
        GVrespuestas.DataBind()
        If DDLcalificacion.SelectedValue > 0 Then
            If GVevaluaciones.Rows.Count <= 0 Then
                msgInfo.show("No hay actividades en este periodo de evaluación.")
            Else
                msgInfo.hide()
            End If
        End If
    End Sub

    Protected Sub BtnExportarA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExportarA.Click
        Convierte(GVrespuestasAbiertas, "Respuestas Abiertas " + HttpUtility.HtmlDecode(Session("NomAlumno")))
    End Sub

    Protected Sub GVevaluaciones_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVevaluaciones.RowDataBound
        '1)PRIMERO OBTENGO LOS RANGOS DE LOS INDICADORES PARA EL SEMAFORO ROJO, AMARILLO Y VERDE
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader

        miComando = objConexion.CreateCommand
        'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
        miComando.CommandText = "select I.MinAmarillo, I.MinVerde, I.MinAzul from Indicador I, CicloEscolar C where " + _
                                "I.IdIndicador = C.IdIndicador and C.IdCicloEscolar = " + Session("IdCicloEscolar").ToString
        Dim MinAmarillo, MinVerde, MinAzul As Decimal
        Try
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read()
            MinAmarillo = misRegistros.Item("MinAmarillo")
            MinVerde = misRegistros.Item("MinVerde")
            MinAzul = misRegistros.Item("MinAzul")
            misRegistros.Close()
            objConexion.Close()
            msgError.hide()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
        End Try

        '2)HAGO UN CICLO QUE VAYA DE 1 AL TOTAL DE COLUMNAS (EL INDICE DE LAS CELDAS(COLUMNAS) INICIA EN 0 (CELLS(0)= )
        'La parte de la condición: (e.Row.RowIndex > -1) es porque en este caso que comparo fechas estaba pintando tambien el encabezado (fila -1), que tambien comparaba
        If (e.Row.Cells.Count > 1) And (e.Row.RowIndex > -1) Then  'Para que se realice la comparacion cuando haya mas de una columna
            Dim I = 17
            If IsNumeric(Trim(e.Row.Cells(I).Text)) Then 'para que no compare celdas vacias
                If (CDbl(e.Row.Cells(I).Text) < MinAmarillo) Then
                    e.Row.Cells(I).BackColor = Drawing.Color.LightSalmon
                ElseIf (CDbl(e.Row.Cells(I).Text) < MinVerde) Then
                    e.Row.Cells(I).BackColor = Drawing.Color.Yellow
                ElseIf (CDbl(e.Row.Cells(I).Text) < MinAzul) Then
                    e.Row.Cells(I).BackColor = Drawing.Color.LightGreen
                Else 'Significa que es mayor o igual al MinAzul:
                    e.Row.Cells(I).BackColor = Drawing.Color.LightBlue
                End If
            End If
        End If

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(7).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA
        End If

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVevaluaciones, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub

    Protected Sub BtnCalificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCalificar.Click
        Try
            '[S]i --> (CORRETA) = 100  [M]edia --> = 80  [N]o --> (INCORRETA) = 50
            If GVrespuestasAbiertas.SelectedDataKey.Values(1).ToString = "True" Then 'Compruebo que la respuesta abierta esta configurada para ser revisada por el Profesor (SeCalifica = 'True')
                'AQUI CALIFICO LA RESPUESTA ABIERTA DEL ALUMNO QUE ESTÉ SELECCIONADA EN EL GRID
                'SI ES DE OPCION MULTIPLE LA RESPUESTA ES ACERTADA (S) O NO (N)
                'SI ES ABIERTA, ENTONCES SI NO LA VA A CALIFICAR EL PROFESOR QUEDA COMO ACERTADA (S) PERO SI SI LA VA A CALIFICAR QUEDA EN BLANCO EL CAMPO "ACERTADA"
                '1) MODIFICO EL CAMPO "PUNTOSACERTADOS" Y "RESULTADO" DE LA TABLA EVALUACIONTERMINADA
                Dim PuntosA, Resultado, Ponderacion, PuntosPosibles, Penalizacion As Decimal
                Dim FinSinPenalizacion As Date
                Dim ReactivosOK As Integer
                PuntosA = CDec(GVevaluaciones.SelectedRow.Cells(16).Text)
                ReactivosOK = CInt(GVevaluaciones.SelectedRow.Cells(15).Text)
                PuntosPosibles = CDec(GVevaluaciones.SelectedDataKey.Values(1).ToString)
                'no lo necesito: Resultado = CDec(GVevaluaciones.SelectedRow.Cells(17).Text)
                Ponderacion = CDec(GVrespuestasAbiertas.SelectedRow.Cells(6).Text)
                FinSinPenalizacion = CDate(GVevaluaciones.SelectedDataKey.Values(2).ToString)
                Penalizacion = CDec(GVevaluaciones.SelectedDataKey.Values(3).ToString)
                Dim Calcula = "S"
                '1) MODIFICO EL CAMPO "ACERTADA" DE LA TABLA RESPUESTA
                SDSrespuestasA.UpdateCommand = "SET dateformat dmy; UPDATE Respuesta set Acertada = '" + Trim(DDLcalifica.SelectedValue) + "' where IdRespuesta = " + GVrespuestasAbiertas.SelectedValue.ToString()
                SDSrespuestasA.Update()

                'RECALCULAR CALIFICACION. Si el campo Acertada estába vacío significa que todavía no se califica, por lo que entonces solo puedo
                'sumarle los puntos de la Ponderacion que tiene el reactivo, si tiene el valor de "S" es que ya tiene sumados los puntos por
                'tanto si cambio a "N" debo restárselos

                'Si la actividad se contestó después de la "FechaSinPenalización" se le descontará la penalización a esos puntos
                If CDate(GVevaluaciones.SelectedRow.Cells(20).Text) > FinSinPenalizacion Then
                    Ponderacion = Ponderacion - (Ponderacion * Penalizacion / 100)
                End If

                If ((Trim(DDLcalifica.SelectedValue) = "M") And (GVrespuestasAbiertas.SelectedRow.Cells(8).Text = "N")) Then
                    PuntosA = PuntosA - Ponderacion * 0.5 'Si es Mal y se pasa a Media, se le descuenta el 50% y se aumenta el 80%
                    PuntosA = PuntosA + Ponderacion * 0.8
                    ReactivosOK = ReactivosOK + 1 'Aunque está medio bien la cuento como un reactivo acertado                
                ElseIf ((Trim(DDLcalifica.SelectedValue) = "M") And (GVrespuestasAbiertas.SelectedRow.Cells(8).Text = "S")) Then
                    Ponderacion = Ponderacion * 0.2 'Si de Bien pasa a Media, se le descontará el 20% 
                    PuntosA = PuntosA - Ponderacion
                    'NO SE MUEVE EL TOTAL DE REACTIVOS OK: ReactivosOK = ReactivosOK + 1 
                ElseIf ((Trim(DDLcalifica.SelectedValue) = "M") And (Trim(GVrespuestasAbiertas.SelectedRow.Cells(8).Text) <> "S" And Trim(GVrespuestasAbiertas.SelectedRow.Cells(8).Text) <> "N" And Trim(GVrespuestasAbiertas.SelectedRow.Cells(8).Text) <> "M")) Then 'GVrespuestasAbiertas.SelectedRow.Cells(8).Text = "&nbsp;" NO FUNCIONÓ PARA VACÍO
                    Ponderacion = Ponderacion * 0.8 'Si no había sido calificado y se pone Medio, se suma el 80%
                    PuntosA = PuntosA + Ponderacion
                    ReactivosOK = ReactivosOK + 1
                ElseIf (Trim(DDLcalifica.SelectedValue) = "S") And (GVrespuestasAbiertas.SelectedRow.Cells(8).Text = "N") Then
                    PuntosA = PuntosA + Ponderacion * 0.5 'Si de estar mal, se pone Bien, Se suma el 50% de la ponderación del reactivo
                    ReactivosOK = ReactivosOK + 1
                ElseIf ((Trim(DDLcalifica.SelectedValue) = "S") And (Trim(GVrespuestasAbiertas.SelectedRow.Cells(8).Text) <> "S" And Trim(GVrespuestasAbiertas.SelectedRow.Cells(8).Text) <> "N" And Trim(GVrespuestasAbiertas.SelectedRow.Cells(8).Text) <> "M")) Then 'GVrespuestasAbiertas.SelectedRow.Cells(8).Text = "&nbsp;" NO FUNCIONÓ
                    PuntosA = PuntosA + Ponderacion 'Si de estar no calicado, se pone Bien, Se suma toda la ponderación del reactivo porque se califica correcto el reactivo
                    ReactivosOK = ReactivosOK + 1
                ElseIf ((Trim(DDLcalifica.SelectedValue) = "S") And (GVrespuestasAbiertas.SelectedRow.Cells(8).Text = "M")) Then
                    Ponderacion = Ponderacion * 0.2 'Si es Media y la pongo bien, se le aumenta el 20%
                    PuntosA = PuntosA + Ponderacion
                    'NO SE MUEVE EL TOTAL DE REACTIVOS OK: ReactivosOK = ReactivosOK + 1 
                ElseIf (Trim(DDLcalifica.SelectedValue) = "N") And (GVrespuestasAbiertas.SelectedRow.Cells(8).Text = "S") Then
                    PuntosA = PuntosA - Ponderacion * 0.5 'Se resta el 50% de la ponderación del reactivo porque de estar bien, se puso mal
                    ReactivosOK = ReactivosOK - 1
                ElseIf (Trim(DDLcalifica.SelectedValue) = "N") And (Trim(GVrespuestasAbiertas.SelectedRow.Cells(8).Text) <> "S" And Trim(GVrespuestasAbiertas.SelectedRow.Cells(8).Text) <> "N" And Trim(GVrespuestasAbiertas.SelectedRow.Cells(8).Text) <> "M") Then 'GVrespuestasAbiertas.SelectedRow.Cells(8).Text = "&nbsp;" NO FUNCIONÓ
                    Ponderacion = Ponderacion * 0.5 'Si se califica mal estando sin calificar, se suma un 50% 
                    PuntosA = PuntosA + Ponderacion
                    'NO SE MUEVE EL TOTAL DE REACTIVOS OK: ReactivosOK = ReactivosOK + 1 
                ElseIf ((Trim(DDLcalifica.SelectedValue) = "N") And (GVrespuestasAbiertas.SelectedRow.Cells(8).Text = "M")) Then
                    PuntosA = PuntosA - Ponderacion * 0.8 'Si de Media pasa a Mal se le descontará el 80% y luego le pongo el 50%
                    PuntosA = PuntosA + Ponderacion * 0.5
                    ReactivosOK = ReactivosOK - 1 'Se le quita un reactivo bien
                ElseIf (Trim(DDLcalifica.SelectedValue) = "") And (GVrespuestasAbiertas.SelectedRow.Cells(8).Text = "S") Then
                    PuntosA = PuntosA - Ponderacion 'Se resta toda la ponderación del reactivo porque de estar bien, se puso que no se ha calificado
                    ReactivosOK = ReactivosOK - 1
                ElseIf ((Trim(DDLcalifica.SelectedValue) = "") And (GVrespuestasAbiertas.SelectedRow.Cells(8).Text = "M")) Then
                    Ponderacion = Ponderacion * 0.8 'Si de Media pasa a No calificada se le descontará el 80% 
                    PuntosA = PuntosA - Ponderacion
                    ReactivosOK = ReactivosOK - 1 'Se le quita un reactivo bien
                ElseIf ((Trim(DDLcalifica.SelectedValue) = "") And (GVrespuestasAbiertas.SelectedRow.Cells(8).Text = "N")) Then
                    Ponderacion = Ponderacion * 0.5 'Si de Mal pasa a No calificada, se le resta el 50%
                    PuntosA = PuntosA - Ponderacion
                    'NO SE MUEVE EL TOTAL DE REACTIVOS OK: ReactivosOK = ReactivosOK - 1 'Se le quita un reactivo bien
                Else
                    'Y en caso que no entre a ninguna opción anterior, sería que se eligió la misma opción que ya estaba
                    Calcula = "N"
                End If

                If Calcula = "S" Then
                    'Recalculo el resultado
                    Resultado = (PuntosA / PuntosPosibles) * 100

                    'Actualizo los Puntos Acertados y el Resultado en EVALUACIONTERMINADA
                    SDSevaluaciones.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionTerminada set PuntosAcertados = " + CStr(FormatNumber(PuntosA, 2)) + ", Resultado = " + CStr(FormatNumber(Resultado, 2)) + ", ReactivosContestados = " + ReactivosOK.ToString + " where IdEvaluacionT = " + GVevaluaciones.SelectedDataKey.Values(0).ToString
                    SDSevaluaciones.Update()
                End If
                '3) REFRESCO EL GRIDVIEW GVEVALUACIONES Y GVRESPUESTASABIERTAS    
                GVevaluaciones.DataBind()
                GVrespuestasAbiertas.DataBind()
                msgError.hide()
                msgInfo.hide()
            Else
                msgError.show("Este reactivo no puede ser modificado por " &
                    Config.Etiqueta.ARTDET_PROFESOR & " " & Config.Etiqueta.PROFESOR & ".")
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), "De refrescar (F5) a esta página. " & ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVrespuestasAbiertas_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVrespuestasAbiertas.RowDataBound
        'Pinta de amarillo las filas de los reactivos que no han sido calificados
        If (Trim(DataBinder.Eval(e.Row.DataItem, "Acertada")) = "") And (e.Row.RowType = DataControlRowType.DataRow) Then
            e.Row.BackColor = Drawing.Color.LightYellow
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(Utils.Tables.GetColumnIndexByName(GVrespuestasAbiertas, "Respuesta")).Text = Regex.Replace(HttpUtility.HtmlDecode(e.Row.Cells(Utils.Tables.GetColumnIndexByName(GVrespuestasAbiertas, "Respuesta")).Text), "<[^>]*(>|$)", String.Empty)
        End If
    End Sub

    Protected Sub GVrespuestasAbiertas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVrespuestasAbiertas.DataBound
        If GVrespuestasAbiertas.Rows.Count > 0 Then
            DDLcalifica.Visible = True
            BtnCalificar.Visible = True
        Else
            DDLcalifica.Visible = False
            BtnCalificar.Visible = False
        End If
    End Sub

    Protected Sub GVrespuestasAbiertas_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVrespuestasAbiertas.SelectedIndexChanged
        ' 20/11/2013 se separa la función de calificar, el gridview ya no muestra selector - esta función es inaccesible
        Try
            PnlResp.Visible = True
            'Para leer bien el campo, grabo el texto solo con trim() y lo leo con HttpUtility.HtmlDecode(
            TBrespuesta.Text = HttpUtility.HtmlDecode(GVrespuestasAbiertas.SelectedRow.Cells(7).Text)
            'OJO: Revisar esto
            If Trim(GVrespuestasAbiertas.SelectedRow.Cells(8).Text) = "S" Or _
                Trim(GVrespuestasAbiertas.SelectedRow.Cells(8).Text) = "N" Or _
                Trim(GVrespuestasAbiertas.SelectedRow.Cells(8).Text) = "M" Then
                DDLcalifica.SelectedValue = Trim(GVrespuestasAbiertas.SelectedRow.Cells(8).Text)
            Else
                'No puede leer del GridView el espacio vacío
                DDLcalifica.SelectedIndex = 0
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub IBmensaje_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles IBmensaje.Click
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        Try
            miComando = objConexion.CreateCommand
            'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
            miComando.CommandText = "select IdProgramacion from Programacion where IdProfesor = (select P.IdProfesor from Profesor P, Usuario U where U.Login = '" + _
                            User.Identity.Name + "' and P.IdUsuario = U.Idusuario) " + _
                            " and IdAsignatura = " + Session("IdAsignatura").ToString + " and IdCicloEscolar = " + Session("IdCicloEscolar").ToString
            'LblError.Text = miComando.CommandText
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read() 'Leo para poder accesarlo
            'Esta variable la necesito por si quiero mandar un mensaje al alumno
            Session("IdProgramacion") = misRegistros.Item("IdProgramacion")
            misRegistros.Close()
            objConexion.Close()
            'Session("IdAsignatura"), Session("IdCicloEscolar"), Session("IdAlumno") y Session("NomAlumno") viene de ReporteGruposProf.aspx
            Session("Regresar") = "DetalleRespuestas.aspx"
            Response.Redirect("EnviarMensajeP2.aspx")
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVrespuestas_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVrespuestas.DataBound
        '    If GVevaluaciones.SelectedIndex < 0 Then
        If DDLcalificacion.SelectedValue > 0 Then
            If (GVrespuestas.Rows.Count < 1) And (GVrespuestasAbiertas.Rows.Count < 1) Then
                msgInfo.show("Esta ACTIVIDAD no tiene asignados reactivos de opción MÚLTIPLE.")
            End If
        End If
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVevaluaciones_PreRender(sender As Object, e As EventArgs) Handles GVevaluaciones.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVevaluaciones.Rows.Count > 0 Then
            GVevaluaciones.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVevaluaciones_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVevaluaciones.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVevaluaciones.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVevaluaciones, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

  Protected Sub GVrespuestasAbiertas_PreRender(sender As Object, e As EventArgs) Handles GVrespuestasAbiertas.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVrespuestasAbiertas.Rows.Count > 0 Then
      GVrespuestasAbiertas.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVrespuestasAbiertas_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVrespuestasAbiertas.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVrespuestasAbiertas.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub


End Class
