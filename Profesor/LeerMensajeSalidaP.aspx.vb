﻿Imports Siget

Imports System.Data.SqlClient

Partial Class profesor_LeerMensajeSalidaP
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        Session("Login") = User.Identity.Name
        TitleLiteral.Text = "Buzón de Mensajes"

        Label1.Text = Config.Etiqueta.ALUMNOS
        Label2.Text = Config.Etiqueta.COORDINADORES

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader

        miComando = objConexion.CreateCommand
        'Con obtener el IdGrado es suficiente para rastrear el nivel ya que son
        'valores unicos en la tabla
        miComando.CommandText = "SELECT P.IdProfesor, P.Nombre, P.Apellidos FROM Usuario U, Profesor P where U.Login = '" + Session("Login") + "' and P.IdUsuario = U.IdUsuario"
        Try
            'Abrir la conexión y leo los registros del Planteamiento
            objConexion.Open()
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            misRegistros.Read() 'Leo para poder accesarlos
            Session("IdProfesor") = misRegistros.Item("IdProfesor")

            misRegistros.Close()
            objConexion.Close()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVmensajes_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmensajes.DataBound
        If GVmensajes.Rows.Count <> 0 Then
            GVmensajes.Caption = "<h3>(" & GVmensajes.Rows.Count & ") Mensajes para " & Config.Etiqueta.ALUMNOS & "</h3>"
        End If
    End Sub

    Protected Sub GVmensajes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmensajes.SelectedIndexChanged
        Session("Envio_IdComunicacion") = GVmensajes.SelectedDataKey.Values(0).ToString
        Session("Envio_IdComunicacionCP") = "0"
        Session("Envio_LoginAlumno") = Trim(GVmensajes.SelectedDataKey.Values(1).ToString)
        Session("Envio_Materia") = GVmensajes.SelectedRow.Cells(5).Text
        Session("Envio_Profesor") = HttpUtility.HtmlDecode(GVmensajes.Caption)
        Session("Envio_Previous") = "LeerMensajeSalidaP"
        Response.Redirect("MensajeP.aspx")
    End Sub

    Protected Sub GVmensajesC_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmensajesC.DataBound
        If GVmensajesC.Rows.Count <> 0 Then
            GVmensajesC.Caption = "<h3>(" & GVmensajesC.Rows.Count & ") Mensajes para " & Config.Etiqueta.COORDINADORES & "</h3>"
        End If
    End Sub

    Protected Sub GVmensajesC_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmensajesC.SelectedIndexChanged
        Session("Envio_IdComunicacionCP") = GVmensajesC.SelectedDataKey.Values(0).ToString
        Session("Envio_IdComunicacion") = "0"
        Session("Envio_LoginCoordinador") = Trim(GVmensajesC.SelectedDataKey.Values(1).ToString)
        Session("Envio_Plantel") = GVmensajesC.SelectedRow.Cells(5).Text
        Session("Envio_Previous") = "LeerMensajeSalidaP"
        Response.Redirect("MensajeP.aspx")
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVmensajes_PreRender(sender As Object, e As EventArgs) Handles GVmensajes.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVmensajes.Rows.Count > 0 Then
            GVmensajes.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVmensajes_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVmensajes.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVmensajes.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GVmensajes_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVmensajes.RowDataBound
    'Los ESTATUS que se manejaran son Nuevo, Leido, Baja (los mensajes de baja ya no aparecen)
    If (DataBinder.Eval(e.Row.DataItem, "EstatusA") = "Leido" Or DataBinder.Eval(e.Row.DataItem, "EstatusA") = "Baja") And (e.Row.RowIndex > -1) Then
      e.Row.Cells(Utils.Tables.GetColumnIndexByName(GVmensajes, "Estatus")).BackColor = Drawing.Color.GreenYellow
    End If

    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(4).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.ALUMNO & " que Recibe"

      LnkHeaderText = e.Row.Cells(5).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.ASIGNATURA
    End If

    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVmensajes, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    If GVmensajes.Rows.Count = 0 AndAlso GVmensajesC.Rows.Count = 0 Then
      msgInfo.show("No ha enviado ningún mensaje.")
    Else
      msgInfo.hide()
    End If

    Dim r As GridViewRow
    For Each r In GVmensajes.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVmensajes, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    For Each r In GVmensajesC.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVmensajesC, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

  Protected Sub GVmensajesC_PreRender(sender As Object, e As EventArgs) Handles GVmensajesC.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVmensajesC.Rows.Count > 0 Then
      GVmensajesC.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVmensajesC_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVmensajesC.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVmensajesC.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

    Protected Sub GVmensajesC_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVmensajesC.RowDataBound
        'Los ESTATUS que se manejaran son Nuevo, Leido, Baja (los mensajes de baja ya no aparecen)
        If (DataBinder.Eval(e.Row.DataItem, "Estatus") = "Leido" Or DataBinder.Eval(e.Row.DataItem, "Estatus") = "Baja") And (e.Row.RowIndex > -1) Then
            e.Row.Cells(Utils.Tables.GetColumnIndexByName(GVmensajesC, "Estatus")).BackColor = Drawing.Color.GreenYellow
        End If

        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(4).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.COORDINADOR & " que Recibe"

            LnkHeaderText = e.Row.Cells(5).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.PLANTEL
        End If

        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVmensajesC, "Select$" & e.Row.RowIndex).ToString())
        End If
    End Sub
End Class
