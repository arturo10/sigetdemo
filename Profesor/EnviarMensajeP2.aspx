﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="EnviarMensajeP2.aspx.vb"
    Inherits="profesor_EnviarMensajeP"
    MaintainScrollPositionOnPostback="true"
    ValidateRequest="false" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .style26 {
            height: 34px;
            text-align: left;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style21 {
            width: 273px;
        }

        .style13 {
            width: 143px;
        }

        .style28 {
            height: 24px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style30 {
            height: 34px;
        }

        .style31 {
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
        }

        .style32 {
            width: 10px;
        }

        .style33 {
            width: 10px;
            font-weight: normal;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Comunicación - Enviar Mensajes
    </h1>
    Permite enviar mensajes a 
    <asp:Label ID="Label5" runat="server" Text="[LOS]" /> 
	<asp:Label ID="Label1" runat="server" Text="[ALUMNOS]" />
     de sus 
	<asp:Label ID="Label2" runat="server" Text="[GRUPOS]" />
     asignad<asp:Label ID="Label6" runat="server" Text="[O]" />s o a 
    <asp:Label ID="Label7" runat="server" Text="[LOS]" /> 
	<asp:Label ID="Label3" runat="server" Text="[COORDINADORES]" />
     de su 
	<asp:Label ID="Label4" runat="server" Text="[PLANTEL]" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr class="titulo">
            <td class="style26" colspan="5">
                <asp:Label ID="LblNombre" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #000099"></asp:Label>
            </td>
            <td class="style30" colspan="2"></td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="2">Asunto</td>
            <td class="style14" colspan="3">
                <asp:TextBox ID="TBasunto" runat="server" MaxLength="100" Width="665px"></asp:TextBox>
            </td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="2">Contenido</td>
            <td class="style14" colspan="3">
                <asp:TextBox ID="TBcontenido" runat="server" 
                    ClientIDMode="Static" 
                    TextMode="MultiLine"
                    CssClass="tinymce"></asp:TextBox>
            </td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="2">Adjuntar archivo (solo uno)</td>
            <td class="style32">
                <asp:FileUpload ID="FUadjunto" runat="server" />
            </td>
            <td class="style14">
                <asp:Button ID="BtnAdjuntar" runat="server" Text="Adjuntar"
                    CssClass="defaultBtn btnThemeGrey btnThemeSlick" />
                &nbsp;
                <asp:Button ID="BtnQuitar" runat="server" Text="Quitar"
                    CssClass="defaultBtn btnThemeGrey btnThemeSlick" />
            </td>
            <td class="style14" colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="2"></td>
            <td class="style14" colspan="5" style="text-align: left">
                <asp:LinkButton ID="lbAdjunto" runat="server" CssClass="LabelInfoDefault"></asp:LinkButton>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="2">&nbsp;</td>
            <td class="style14" colspan="3">&nbsp;</td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="7">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="2">&nbsp;</td>
            <td class="style14" colspan="3">
                <asp:Button ID="BtnEnviar" runat="server" Text="Enviar mensaje"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
                <asp:Button ID="BtnLimpiar" runat="server" Text="Limpiar"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style22">&nbsp;</td>
            <td class="style21" colspan="3">&nbsp;</td>
            <td class="style13" colspan="2">&nbsp;</td>
            <td class="style15">&nbsp;</td>
        </tr>
        <tr>
            <td class="style22" style="text-align: right">
                <asp:HyperLink ID="HLregresar" runat="server" 
                    NavigateUrl="~/profesor/CalificaDoctoAlumnos.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
            <td class="style21" colspan="3">&nbsp;</td>
            <td class="style13" colspan="2">&nbsp;</td>
            <td class="style15">&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDScomunicacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Comunicacion]"></asp:SqlDataSource>

            </td>
            <td>
                <asp:HiddenField ID="hfAdjuntoFisico" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfCarpetaAdjunto" runat="server" />
            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

