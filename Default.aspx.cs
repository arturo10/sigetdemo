﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/*
 * La aplicación debe poder soportar múltiples estilos de inicio de sesión;
 * para lograrlo la página de inicio se utiliza como redirección a la página
 * de inicio de sesión que esté configurada en Global.
 */

public partial class _Default : System.Web.UI.Page
{
  /// <summary>
  /// Indicador principal de la versión del programa.
  /// </summary>
  private const String programVersion = "5.3.0";

  protected void Page_Load(object sender, EventArgs e)
  {
    LtVersion.Text = programVersion;

    // evita la redirección de forms authentication
    deshabilitaReturnUrl();

    // redirige a la página de login configurada
    redirigeLogin();
  }

  /// <summary>
  /// Deshabilita la funcionalidad de ReturnUrl al expirar la sesión de un usuario.
  /// </summary>
  /// <remarks>
  /// Ocasionaba errores al originarse desde una página que necesita variables de sesión o estado 
  /// para desplegar información, como reportes.
  /// 
  /// Cuando un usuario hace una petición a una página mientras su sesión expiró, forms 
  /// authentication envía a la página de login (definida en web.config) estableciendo la página 
  /// originaria como ReturnUrl en el URL; al reinciar sesión se envía a la página originaria (1). 
  /// 
  /// (1) http://msdn.microsoft.com/en-us/library/ka5ffkce%28v=vs.110%29.aspx
  /// </remarks>
  protected void deshabilitaReturnUrl()
  {
    if (Request.QueryString["ReturnUrl"] != null)
      Response.Redirect("~/");
  }

  /// <summary>
  /// Redirige a la página de inicio de sesión que esté configurada en Global.
  /// </summary>
  protected void redirigeLogin()
  {
    if (Siget.Config.Global.Login_Screen == 0)
      Response.Redirect("~/login_jp1/");
    else if (Siget.Config.Global.Login_Screen == 1)
      Response.Redirect("~/login_a1/");
  }
}