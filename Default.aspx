﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        <asp:Literal ID="LtVersion" runat="server" />
    </title>

    <link rel="shortcut icon" type="image/x-icon" href="~/Resources/imagenes/favicon.ico" />
    
    <style type="text/css">
        html, body, #form1 {
            width: 100%; 
        }

        #centerContainer {
            font-family: 'Courier New';
            font-size: 0.7em;
            width: 300px;
            min-width: 300px;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="centerContainer">
            <br />
            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.--.&nbsp;
            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_____&nbsp;)&nbsp;&nbsp;`>--._,-.&nbsp;
            <br />&nbsp;&nbsp;&nbsp;&nbsp;,-'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`'--''`--<.&nbsp;&nbsp;\&nbsp;
            <br />&nbsp;&nbsp;,'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`.'&nbsp;
            <br />&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\&nbsp;
            <br />|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_,--''--..&nbsp;&nbsp;&nbsp;|&nbsp;_&nbsp;
            <br />|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;,;._|(&nbsp;\,-.&nbsp;
            <br />'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;;&nbsp;&nbsp;;&nbsp;)//&nbsp;&nbsp;_>&nbsp;'&nbsp;/&nbsp;
            <br />&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:'`,((&nbsp;&nbsp;(__,*/-.&nbsp;
            <br />&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\&nbsp;&nbsp;.&nbsp;&nbsp;`'&nbsp;&nbsp;\`.&nbsp;&nbsp;.|,&nbsp;\_)&nbsp;
            <br />&nbsp;&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;`----'&nbsp;)&nbsp;\&nbsp;&nbsp;/'-'&nbsp;
            <br />&nbsp;&nbsp;&nbsp;`._&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;|/--.&nbsp;
            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_;:-'.-'--.___,,,<,,.-/&nbsp;&nbsp;&nbsp;`&nbsp;
            <br />&nbsp;&nbsp;&nbsp;,'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\&nbsp;&nbsp;&nbsp;&nbsp;,-.&nbsp;\'._--/._&nbsp;
            <br />&nbsp;<'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`.__,'o\`-&nbsp;&nbsp;&nbsp;/&nbsp;_)_)&nbsp;
            <br />&nbsp;,|`.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\&nbsp;o\&nbsp;&nbsp;&nbsp;|_||,-.&nbsp;
            <br />&nbsp;`.'(`--./&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`-'&nbsp;&nbsp;&nbsp;|&nbsp;,|_)`&nbsp;
            <br />&nbsp;&nbsp;&nbsp;`._'(_/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`-'&nbsp;\&nbsp;
            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`-.._______,,..,--'&nbsp;&nbsp;/&nbsp;
            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;|&nbsp;
            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(--....___/)`-P,.&nbsp;
            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;do----,oooood8P"\.&nbsp;
            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d8888P"&nbsp;&nbsp;_,888oo88'&nbsp;
            <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;`'`P888888P"'&nbsp;
        </div>
    </form>
</body>
</html>
