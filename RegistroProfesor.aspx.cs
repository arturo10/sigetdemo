﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security; //este contine la funcion Roles

public partial class RegistroProfesor : System.Web.UI.Page
{  //originalmente este script era unico y desde aqui se podian crear los usuarios de todos
   //los perfiles, pero como se equivocaban al elegir el perfil, terminé creando un script para c/u
   
    protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
    {
        try
        {
        // Crea un profile vacío para el usuario recien creado
        ProfileCommon p = (ProfileCommon)ProfileCommon.Create(CreateUserWizard2.UserName, true);

        //Propaga algunas propiedades en off del usuario creado por el wizard
        p.IdEntidad = Int32.Parse(((DropDownList)CreateUserWizard2.CreateUserStep.ContentTemplateContainer.FindControl("Profesor")).SelectedValue);
        p.DDLtipo = ((DropDownList)CreateUserWizard2.CreateUserStep.ContentTemplateContainer.FindControl("DDLtipo")).SelectedValue;

        //Guarda el profile (debe ser hecho desde que nosotros explicitamente lo creamos)
        p.Save();
        }
        catch (Exception ex)
        {
            Lblerror.Text = ex.Message;
        }
    }

    //El evento "activate" se dispara cuando el usuario presiona "Next" en el CreateUserWizard
    public void AssignUserToRoles_Activate(object sender, EventArgs e)
    {
        try
        {
        // Databind list of roles in the role manager system to a listbox in the wizard
        AvailableRoles.DataSource = Roles.GetAllRoles();
        AvailableRoles.DataBind();
        }
        catch (Exception ex)
        {
            Lblerror.Text = ex.Message;
        }
    }

    // Deactivate event fires when user hits "next" in the CreateUserWizard 
    public void AssignUserToRoles_Deactivate(object sender, EventArgs e)
    {
        try
        {
        // Add user to all selected roles from the roles listbox
        //   for (int i = 0; i < AvailableRoles.Items.Count; i++)
        //   {
            //       if (AvailableRoles.Items[i].Selected == true)
            //0=Alumno, 1=Capturista, 2=Coordinador, 3=Operador,4=profesor, 5=superadministrador
        //**Agrego el usuario al RoleGroup Alumno
        Roles.AddUserToRole(CreateUserWizard2.UserName, AvailableRoles.Items[4].Value);

        //**Agrego el login del usuario creado a la tabla Profesor
        SDSusuarios.InsertCommand = "SET dateformat dmy; INSERT INTO Usuario (Login,PerfilASP,Email) VALUES ('" + CreateUserWizard2.UserName + "','" + ((DropDownList)CreateUserWizard2.CreateUserStep.ContentTemplateContainer.FindControl("DDLtipo")).SelectedValue + "','" + CreateUserWizard2.Email + "')";
        SDSusuarios.Insert();
        //**Amarro el usuario recien creado con el Profesor
        SDSprofesores.UpdateCommand = "SET dateformat dmy; UPDATE Profesor set IdUsuario = (select IdUsuario from Usuario where login = '" + CreateUserWizard2.UserName + "') where IdProfesor = " + ((DropDownList)CreateUserWizard2.CreateUserStep.ContentTemplateContainer.FindControl("Profesor")).SelectedValue;
        SDSprofesores.Update();

        //   }
        }
        catch (Exception ex)
        {
            Lblerror.Text = ex.Message;
        }
    }
}
