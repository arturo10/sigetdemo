﻿<%@ Page Language="VB"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="_Default" %>

<!DOCTYPE html>

<!--
		> El logo del cliente debe de ser de 350x100 máximo
		
		Color de los avisos importantes
		/*1*/mintcream

		-->

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
    </title>

    <link rel="shortcut icon" type="image/x-icon" href="~/Resources/imagenes/favicon.ico" />

    <asp:Literal ID="Literal1" runat="server" />

    <!--[if gt IE 6]> <!-- Ejecuta lo siguiente en cualquier cosa que no sea IE < 7 -->
    <style type="text/css">
        /* Sistema de Centrado del cuerpo*/

        .centered {
            margin-left: auto;
            margin-right: auto;
            display: table;
        }
    </style>
    <!-- <![endif]-->

    <!--[if lt IE 7]>
		<style type="text/css" >
				/* Sistema de Centrado del cuerpo*/

				.centered{
						display: inline;
						zoom: 1;
				}

				.centeringContainer{
						text-align: center;
				}
		</style>
		<![endif]-->

    <!-- Estilos específicos de botones -->
    <link id="css3" rel="stylesheet" runat="server" media="screen" href="~/Resources/css/Buttons.css" />

    <!--
				Estilos Generales
				- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
				-->
    <style type="text/css">
        /* Eliminación de márgenes globales */

        body {
            background-color: /*1*/ #f5f5f5;
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        }

        html, body {
            margin: 0px;
            padding: 0px;
            border: 0px;
        }

        /* Definición el Cuerpo */

        #outerBodyContainer {
            min-width: 900px;
        }

        #content {
            margin-top: 10px;
            min-height: 465px;
            width: 900px;
            border: 1px solid #888;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            margin-bottom: 5px;
            display: block;
            background-color: white;
            background-repeat: no-repeat;
            max-height: 700px;
            overflow: hidden;
        }

        #footer {
            border-spacing: 0;
            border-collapse: collapse;
            padding: 0;
            font-family: Arial, Helvetica, sans-serif;
            font-size: xx-small;
            margin-bottom: 50px;
            width: 900px;
            /* center this container if #content is widened */
            margin-left: auto;
            margin-right: auto;
        }

            #footer a:link,
            #footer a:hover,
            #footer a:visited,
            #footer a:active {
                color: #333;
            }

        #clientLogoDiv {
            height: 100px;
            width: 350px;
            max-width: 350px;
            max-height: 100px;
            overflow: hidden;
            margin-left: 10px;
            margin-top: 10px;
        }

        #loginDiv {
            width: 500px;
            padding-left: 50px;
            vertical-align: top;
        }

        .loginControl {
            font-size: 0.8em;
            color: #333333;
            font-family: Verdana;
        }

        .loginButton {
            -webkit-border-radius: 3px;
            border: 1px solid #bbb;
            font-size: 1em;
        }

            .loginButton:hover {
                border: 1px solid #000;
                background-color: #a0a0a0;
            }

        #messageDiv {
            max-width: 300px;
            overflow: visible;
            padding-left: 10px;
            padding-top: 10px;
            padding-left: 10px;
            width: 286px;
            vertical-align: top;
        }

        #optionsDiv {
            width: 600px;
            padding-top: 350px;
        }

        .optionButton {
            display: inline-block;
            position: relative;
            clear: none;
            margin-left: 40px;
            cursor: pointer;
        }

        .message {
            font-size: 1.1em;
            display: inline-block;
            float: left;
            position: relative;
            clear: both;
            margin-top: 15px;
            font-weight: 500;
            color: /*1*/ mintcream;
            font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
        }

        .loginTitle {
            font-weight: 600;
        }

        .errorMessage {
            text-align: center;
            color: Red;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 1.1em;
            font-weight: 600;
        }
    </style>

    <asp:Literal ID="Literal2" runat="server"></asp:Literal>

    <%-- Screen Resolution Detection --%>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#HfWidth").val( $(window).width() );
            $("#HfHeight").val( $(window).height() );

        });
    </script>
</head>
<body>
    <div class="centeringContainer">
        <div class="centered">
            <table class="borderlessTable">
                <tr>
                    <td id="outerBodyContainer" style="min-width: 900px; border-spacing: 0px;">
                        <form id="form2" runat="server">
                            <table id="content" class="borderlessTable">
                                <tr>
                                    <td>
                                        <!-- START CONTENT CONTAINER -->
                                        <table class="borderlessTable">
                                            <tr>
                                                <td>
                                                    <table class="borderlessTable">
                                                        <tr>
                                                            <td id="clientLogoDiv">
                                                                <asp:HyperLink ID="hlClientLogo" runat="server" Target="_blank">
                                                                    <asp:Image ID="Image2" runat="server"
                                                                        ImageUrl="~/Resources/Customization/imagenes/clientLogo.png"
                                                                        Height="100" />
                                                                </asp:HyperLink>
                                                            </td>
                                                            <td id="loginDiv">
                                                                <asp:Login ID="LoginPpal" runat="server"
                                                                    CssClass="loginControl"
                                                                    DestinationPageUrl="~/">
                                                                    <LayoutTemplate>
                                                                        <table>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="UserNameLabel"
                                                                                            runat="server"
                                                                                            AssociatedControlID="UserName" CssClass="loginTitle">
																	                        [Usuario]
                                                                                        </asp:Label>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Label ID="PasswordLabel"
                                                                                            runat="server"
                                                                                            AssociatedControlID="Password" CssClass="loginTitle">
																	                        [Contraseña]
                                                                                        </asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:TextBox ID="UserName" runat="server" CssClass="loginTextBox"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server"
                                                                                            ControlToValidate="UserName" ErrorMessage="[El nombre de usuario es obligatorio]"
                                                                                            ToolTip="[El nombre de usuario es obligatorio]" ValidationGroup="LoginPpal">*</asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="loginTextBox"></asp:TextBox>
                                                                                        <asp:TextBox ID="Password2" runat="server" Visible="False" CssClass="loginTextBox"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server"
                                                                                            ControlToValidate="Password" ErrorMessage="[La contraseña es obligatoria]"
                                                                                            ToolTip="[La contraseña es obligatoria]" ValidationGroup="LoginPpal">*</asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:Button ID="LoginButton" runat="server" CommandName="Login"
                                                                                            CssClass="defaultBtn btnThemeGrey btnThemeSlick"
                                                                                            Text="[Iniciar Sesión]" ValidationGroup="LoginPpal" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="chkRememberMe" runat="server"
                                                                                            AutoPostBack="true"
                                                                                            OnCheckedChanged="chkRememberMe_CheckedChanged"
                                                                                            Text="[Recordar Usuario]" Style="font-size: 0.8em;" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:CheckBox ID="CBmostrar" runat="server" AutoPostBack="True"
                                                                                            OnCheckedChanged="CBmostrar_CheckedChanged"
                                                                                            Text="[Mostrar Caracteres]" Style="font-size: 0.8em;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        <div class="errorMessage">
                                                                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                                        </div>
                                                                    </LayoutTemplate>
                                                                </asp:Login>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table class="borderlessTable">
                                                        <tr>
                                                            <td id="messageDiv">
                                                                <asp:Label ID="LblMensaje1" runat="server" CssClass="message"></asp:Label>
                                                            </td>
                                                            <td id="optionsDiv">
                                                                <div class="optionButton">
                                                                    <asp:HyperLink ID="hlButton1" runat="server"
                                                                        Target="_blank">
                                                                        <asp:Image ID="Image3" runat="server"
                                                                            ImageUrl="~/Resources/Customization/imagenes/boton1.png" />
                                                                    </asp:HyperLink>
                                                                </div>
                                                                <div class="optionButton">
                                                                    <asp:HyperLink ID="hlButton2" runat="server"
                                                                        Target="_blank">
                                                                        <asp:Image ID="Image4" runat="server"
                                                                            ImageUrl="~/Resources/Customization/imagenes/boton2.png" />
                                                                    </asp:HyperLink>
                                                                </div>
                                                                <div class="optionButton">
                                                                    <asp:HyperLink ID="hlButton3" runat="server"
                                                                        Target="_blank">
                                                                        <asp:Image ID="Image5" runat="server"
                                                                            ImageUrl="~/Resources/Customization/imagenes/boton3.png" />
                                                                    </asp:HyperLink>
                                                                </div>
                                                                <div class="optionButton">
                                                                  
                                                                  <asp:HyperLink ID="hlButton4" runat="server"
                                                                        Target="_blank">
                                                                        <asp:Image ID="Image6" runat="server"
                                                                            ImageUrl="~/Resources/Customization/imagenes/boton4.png" />
                                                                    </asp:HyperLink>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- END CONTENT CONTAINER -->
                                    </td>
                                </tr>
                            </table>
                            <table id="footer" class="borderlessTable">
                                <tr>
                                    <td>
                                        <table style="display: table; float: right; font-size: 13px; font-weight: bold;">
                                            <tr>
                                                <td>
                                                    <asp:Image ID="Image7" runat="server"
                                                        ImageUrl="~/Resources/imagenes/small_lock.png" />
                                                </td>
                                                <td>
                                                    <asp:HyperLink ID="hl_privacyNotice" runat="server"
                                                        NavigateUrl="../../doctos/AvisoPrivacidad.pdf"
                                                        Target="_blank">
                                                        [Aviso de Privacidad]
                                                    </asp:HyperLink>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="borderlessTable" style="display: table; margin-left: auto; margin-right: auto;">
                                            <tr>
                                                <td style="text-align: center;">
                                                    <span style="font-size: 1.3em; font-weight: bold; letter-spacing: 2em;">
                                                        <asp:Label ID="LabelA" runat="server" Text="Label" />
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div style="width: 30px; height: 30px; display: inline-block;">
                                                        <asp:Image ID="imgLogoIntegrantFooter" runat="server"
                                                            ImageUrl="~/Resources/imagenes/integrantLogo.png"
                                                            Height="30" Width="30" />
                                                    </div>
                                                    <div style="display: inline-block; text-align: left; margin-left: 10px;">
                                                        <asp:Label ID="LblFecha" runat="server" Style="color: #000000;" />
                                                        <span>
                                                            <br />
                                                            <asp:Literal ID="lt_rightsReserved" runat="server" Text="[Todos los derechos reservados]" />
                                                            <asp:HyperLink ID="hlIntegrant" runat="server" 
                                                                NavigateUrl="//www.integrant.com.mx"
                                                                Target="_blank">Salmar - Integrant</asp:HyperLink>
                                                            &#174; 2014<br />
                                                            <asp:Literal ID="ltCompany" runat="server">Estrategia y Desarrollo Organizacional S.C.</asp:Literal>
                                                        </span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <%-- Screen Resolution Detection --%>
                            <asp:HiddenField ID="HfWidth" runat="server" />
                            <asp:HiddenField ID="HfHeight" runat="server" />
                        </form>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
