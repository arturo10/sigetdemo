﻿<%@ Control
  Language="C#"
  AutoEventWireup="true"
  CodeFile="ComentarioForo.ascx.cs"
  Inherits="Controls_ComentarioForo" %>

<asp:PlaceHolder ID="PhrControl" runat="server">
  <table id="commTableMain" runat="server" style="font-size: 0.9em; min-width: 600px; width: 100%; margin-bottom: 10px; background-color: #efefef; padding: 10px;">
    <tr>
      <td style="vertical-align: top; width: 40px;">
        <asp:Image ID="usrImage" runat="server" ImageUrl="~/Resources/imagenes/blankUser.png" Width="40" />
        <asp:PlaceHolder ID="PhrProfesorImg" runat="server" Visible="false">
          <br />
          <asp:Image ID="Image1" runat="server" 
            ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/user_policeman(2).png" 
            Width="32" style="margin-left: 5px; margin-top: 8px;" />
        </asp:PlaceHolder>
      </td>
      <td style="vertical-align: top;">
        <table style="width: 100%;">
          <tr>
            <td>
              <table style="width: 100%;">
                <tr>
                  <td style="padding-left: 40px; font-weight: bold;">
                    <asp:Label ID="LbNombreUsuario" runat="server">Nombre de Usuario</asp:Label>
                  </td>
                  <td style="text-align: right;">
                    <asp:Literal ID="LtFecha" runat="server">dd/mm/aaaa hh:mm:ss</asp:Literal>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="padding-left: 10px; padding-right: 10px;">
              <asp:Literal ID="LtComentario" runat="server"><p>Comentario de usuario... Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi quis ipsum rutrum, dapibus lectus ut, lobortis dui. Vivamus at dolor dapibus, molestie leo vel, aliquet odio. Cras sed semper lacus. Morbi interdum lectus nibh. Cras in quam vitae augue aliquet egestas. Donec pretium diam leo, non tincidunt mauris porttitor id. Donec vel arcu vel arcu congue aliquam. Nulla non elit dignissim mauris gravida convallis sit amet quis nulla. Morbi eu porta elit. Etiam non risus ullamcorper, facilisis felis in, dapibus augue.</p></asp:Literal>
            </td>
          </tr>
          <tr>
            <td>
              <table style="width: 100%;">
                <tr>
                  <asp:PlaceHolder ID="PhrAdjunto" runat="server" Visible="false">
                    <td>
                      <asp:LinkButton ID="LbAdjunto" runat="server"
                        OnClick="LbAdjunto_Click"
                        CssClass="LabelInfoDefault"></asp:LinkButton>
                    </td>
                  </asp:PlaceHolder>
                  <asp:PlaceHolder ID="PhrCalificar" runat="server" Visible="false">
                    <td style="text-align: right;">
                      <asp:Label ID="LbCalificacion" runat="server" CssClass="resultadoAsignatura" style='
                        display: inline-block;
                        margin-left: 90px;
                        padding-top: 5px;
                        padding-bottom: 5px;
                        padding-left: 7px;
                        padding-right: 7px;

                        font-weight: bold;
                        background: #c8c8c8;

                        border-radius: 4px; margin-left: 0px; font-size: 20px; background-color: #C5C9CE;' 
                        Visible="false">95</asp:Label>
                      &nbsp;
                      <asp:DropDownList ID="DdlCalificacion" runat="server" CssClass="wideTextbox">
                        <asp:ListItem Value="100">100</asp:ListItem>
                        <asp:ListItem Value="95">95</asp:ListItem>
                        <asp:ListItem Value="90">90</asp:ListItem>
                        <asp:ListItem Value="85">85</asp:ListItem>
                        <asp:ListItem Value="80">80</asp:ListItem>
                        <asp:ListItem Value="75">75</asp:ListItem>
                        <asp:ListItem Value="70">70</asp:ListItem>
                        <asp:ListItem Value="65">65</asp:ListItem>
                        <asp:ListItem Value="60">60</asp:ListItem>
                        <asp:ListItem Value="55">55</asp:ListItem>
                        <asp:ListItem Value="50">50</asp:ListItem>
                        <asp:ListItem Value="45">45</asp:ListItem>
                        <asp:ListItem Value="40">40</asp:ListItem>
                        <asp:ListItem Value="35">35</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>
                        <asp:ListItem Value="25">25</asp:ListItem>
                        <asp:ListItem Value="20">20</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                        <asp:ListItem Value="0">0</asp:ListItem>
                      </asp:DropDownList>
                      &nbsp;
                      <asp:Button ID="BtnCalificar" runat="server" Text="Calificar" CssClass="defaultBtn btnThemeSemiSlick btnThemeGrey" OnClick="BtnCalificar_Click" />
                    </td>
                  </asp:PlaceHolder>
                  <asp:PlaceHolder ID="PhrModificar" runat="server" Visible="false">
                    <td style="text-align: right;">
                      <asp:Button ID="BtnEditar" runat="server" Text="Editar" CssClass="defaultBtn btnThemeSemiSlick btnThemeGrey" OnClick="BtnEditar_Click" />
                      &nbsp;
                      <asp:Button ID="BtnBorrar" runat="server" Text="Borrar" CssClass="defaultBtn btnThemeSemiSlick btnThemeGrey" OnClick="BtnBorrar_Click" />
                    </td>
                  </asp:PlaceHolder>
                </tr>
              </table>
            </td>

          </tr>
        </table>
      </td>
    </tr>
  </table>
</asp:PlaceHolder>