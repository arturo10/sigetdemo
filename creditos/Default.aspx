﻿<%@ Page Language="VB"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="creditos_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>
        <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" /></title>

    <link rel="shortcut icon" type="image/x-icon" href="~/Resources/imagenes/favicon.ico" />

    <link href="~/Resources/css/general_5.1.0_a.css" rel="stylesheet" type="text/css" media="all" />

    <!-- Estilos específicos de botones -->
    <link id="css3" rel="stylesheet" runat="server" media="screen" href="~/Resources/css/Buttons.css" />

    <style>
        body {
            background: #f5f5f5;
        }

        .creditBox {
            color: #000;
            display: inline-block;
            text-align: inherit;
            background: #fff;
            border: 1px solid #333;
            padding: 30px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
        }

            .creditBox a {
                color: #4070D0;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="display: block; margin: 30px auto 0 auto; text-align: center;">
            <div style="display: inline-block;">
                <asp:Image ID="Image1" runat="server"
                    ImageUrl="~/Resources/imagenes/LogoIntegrantVertical.png" />
            </div>
            <br />
            <div class="creditBox">
                <p>
                    <asp:Literal ID="lt_desarrollo" runat="server" />
                    <asp:HyperLink ID="HyperLink2" runat="server"
                        NavigateUrl="//www.integrant.com.mx"
                        Target="_blank">
                        Integrant
                    </asp:HyperLink>
                </p>
                <p>
                    <asp:Literal ID="lt_iconos" runat="server" />
                    <asp:HyperLink ID="HyperLink1" runat="server"
                        NavigateUrl="//www.fatcow.com/free-icons"
                        Target="_blank">
                        FatCow Web Hosting
                    </asp:HyperLink>
                </p>
                <p>
                    <asp:Literal ID="lt_privacy" runat="server" />
                    <asp:HyperLink ID="HyperLink5" runat="server"
                        NavigateUrl="/doctos/AvisoPrivacidad.pdf"
                        Target="_blank">
                        <asp:Literal ID="lt_privacy_link" runat="server" />
                    </asp:HyperLink>
                </p>
            </div>
            <br />
            <div style="display: inline-block; margin-top: 50px;">
                <asp:HyperLink ID="HyperLink3" runat="server"
                    NavigateUrl="javascript:history.go(-1);"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
                    <asp:Image ID="Image3" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/bullet_back_2.png"
                        CssClass="btnIcon" />
                    <asp:Literal ID="lt_goBack" runat="server" />
                </asp:HyperLink>
            </div>
        </div>
    </form>
</body>
</html>
