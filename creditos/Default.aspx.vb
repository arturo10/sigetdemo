﻿Imports Siget


Partial Class creditos_Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            aplicaLenguaje()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = Lang.FileSystem.Creditos.LT_TITULO.Item(Session("Usuario_Idioma"))
        lt_desarrollo.Text = Lang.FileSystem.Creditos.LT_DESARROLLO.Item(Session("Usuario_Idioma"))
        lt_iconos.Text = Lang.FileSystem.Creditos.LT_ICONOS.Item(Session("Usuario_Idioma"))
        lt_privacy.Text = Lang.FileSystem.Creditos.LT_PRIVACY.Item(Session("Usuario_Idioma"))
        lt_privacy_link.Text = Lang.FileSystem.Creditos.LT_PRIVACY_LINK.Item(Session("Usuario_Idioma"))
        lt_goBack.Text = Lang.FileSystem.Creditos.LT_GO_BACK.Item(Session("Usuario_Idioma"))
    End Sub
End Class
