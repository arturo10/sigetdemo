﻿<%@ Page Title="" 
    Language="C#" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="true" 
    CodeFile="RegistroProfesor.aspx.cs" 
    Inherits="RegistroProfesor" %>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">

        .style12
    {
        width: 100%;
            height: 450px;
        }
    .style13
    {
            width: 74px;
        }
    .style14
    {
            width: 642px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style12">
        <tr>
            <td colspan="3">
                <span class="titulo">El usuario que registre es el que deberá usar para 
                ingresar al sistema.</span></td>
        </tr>
        <tr>
            <td class="style13">
                &nbsp;</td>
            <td class="style14">
                <asp:CreateUserWizard ID="CreateUserWizard2" runat="server" BackColor="#E3EAEB" 
                    BorderColor="#E6E2D8" BorderStyle="Solid" BorderWidth="1px" 
                    Font-Names="Verdana" Font-Size="0.8em" Width="322px" 
                    oncreateduser="CreateUserWizard1_CreatedUser" CancelButtonText="Cancelar." 
                    CompleteSuccessText="La cuenta se ha creado correctamente. Ud. ya ha sido ingresado al sistema." 
                    ContinueButtonText="Continuar." CreateUserButtonText="Crear Usuario" 
                    DuplicateEmailErrorMessage="La dirección de correo que ha especificado ya está en uso. Especifique una diferente" 
                    DuplicateUserNameErrorMessage="Especifique un nombre de usuario diferente, el que capturó ya existe" 
                    FinishCompleteButtonText="Finalizar." FinishPreviousButtonText="Anterior." 
                    InvalidAnswerErrorMessage="Especifique una respuesta de seguridad diferente" 
                    InvalidEmailErrorMessage="Especifique una dirección de correo electrónico válida" 
                    InvalidPasswordErrorMessage="Longitud mínima de la contraseña: {0}. Se requieren caracteres no alfanuméricos (como &amp; # + ? - /): {1}." 
                    InvalidQuestionErrorMessage="Especifique una pregunta de seguridad diferente" 
                    StartNextButtonText="Siguiente." StepNextButtonText="Siguiente." 
                    StepPreviousButtonText="Anterior." 
                    UnknownErrorMessage="La cuenta no se ha creado. Inténtelo de nuevo" 
                    ContinueDestinationPageUrl="~/Default.aspx">
                    <SideBarStyle BackColor="#1C5E55" Font-Size="0.9em" VerticalAlign="Top" />
                    <SideBarButtonStyle ForeColor="White" />
                    <ContinueButtonStyle BackColor="White" BorderColor="#C5BBAF" 
                        BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" 
                        ForeColor="#1C5E55" />
                    <NavigationButtonStyle BackColor="White" BorderColor="#C5BBAF" 
                        BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" 
                        ForeColor="#1C5E55" />
                    <HeaderStyle BackColor="#666666" BorderColor="#E6E2D8" BorderStyle="Solid" 
                        BorderWidth="2px" Font-Bold="True" Font-Size="0.9em" ForeColor="White" 
                        HorizontalAlign="Center" />
                    <CreateUserButtonStyle BackColor="White" BorderColor="#C5BBAF" 
                        BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" 
                        ForeColor="#1C5E55" />
                    <TitleTextStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <StepStyle BorderWidth="0px" />
                    <WizardSteps>
                        <asp:CreateUserWizardStep runat="server" >
                            <ContentTemplate>
                                <table border="0" style="font-family:Verdana;font-size:100%;width:641px;">
                                    <tr>
                                        <td align="center" colspan="2" 
                                            style="color:White;background-color:#336699; font-weight:bold;">
                                            Regístrese para obtener una nueva cuenta de usuario</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Nombre de usuario:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="UserName" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" 
                                                ControlToValidate="UserName" 
                                                ErrorMessage="El nombre de usuario es obligatorio." 
                                                ToolTip="El nombre de usuario es obligatorio." 
                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            Profesor</td>
                                        <td>
                                            <asp:DropDownList ID="Profesor" runat="server" AutoPostBack="True" 
                                                DataSourceID="SDSprofesores" DataTextField="NomProfesor" DataValueField="IdProfesor" 
                                                Width="300px">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                ControlToValidate="Profesor" 
                                                ErrorMessage="Indique un coordinador." 
                                                ToolTip="Indicar un coordinador asociado a este usuario es obligatorio." 
                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Contraseña:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" 
                                                ControlToValidate="Password" ErrorMessage="La contraseña es obligatoria." 
                                                ToolTip="La contraseña es obligatoria." ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:Label ID="ConfirmPasswordLabel" runat="server" 
                                                AssociatedControlID="ConfirmPassword">Confirmar contraseña:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" 
                                                ControlToValidate="ConfirmPassword" 
                                                ErrorMessage="Confirmar contraseña es obligatorio." 
                                                ToolTip="Confirmar contraseña es obligatorio." 
                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">Correo electrónico:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Email" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="EmailRequired" runat="server" 
                                                ControlToValidate="Email" ErrorMessage="El correo electrónico es obligatorio." 
                                                ToolTip="El correo electrónico es obligatorio." 
                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                                ControlToValidate="Email" Display="Dynamic" 
                                                ErrorMessage="Este campo es obligatorio"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                                ControlToValidate="Email" Display="Dynamic" 
                                                ErrorMessage="*El formato no es correcto" 
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:Label ID="QuestionLabel" runat="server" AssociatedControlID="Question">Pregunta de seguridad:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Question" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="QuestionRequired" runat="server" 
                                                ControlToValidate="Question" 
                                                ErrorMessage="La pregunta de seguridad es obligatoria." 
                                                ToolTip="La pregunta de seguridad es obligatoria." 
                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer">Respuesta de seguridad:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Answer" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" 
                                                ControlToValidate="Answer" 
                                                ErrorMessage="La respuesta de seguridad es obligatoria." 
                                                ToolTip="La respuesta de seguridad es obligatoria." 
                                                ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: right;">
                                            <asp:Label ID="LabelTipoU" runat="server" AssociatedControlID="DDLtipo">Tipo de Usuario:</asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DDLtipo" runat="server">
                                                <asp:ListItem>Profesor</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                                    ControlToValidate="DDLtipo" 
                                                    ErrorMessage="El tipo de usuario debe ser Coordinador" 
                                                    ToolTip="El tipo de usuario debe ser Coordinador" 
                                                    ValidationGroup="CreateUserWizard1">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
                                            <asp:CompareValidator ID="PasswordCompare" runat="server" 
                                                ControlToCompare="Password" ControlToValidate="ConfirmPassword" 
                                                Display="Dynamic" 
                                                ErrorMessage="Contraseña y Confirmar contraseña deben coincidir." 
                                                ValidationGroup="CreateUserWizard1"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2" style="color:Red;">
                                            <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:CreateUserWizardStep>
                        <asp:WizardStep runat="server" ID="wsAssignUserToRoles" AllowReturn="False" Title="Step 2: Assign User To Roles"
                                OnActivate="AssignUserToRoles_Activate" OnDeactivate="AssignUserToRoles_Deactivate">
                            <table>
                                <tr>
                                    <td>
                                        Los siguientes son los Roles disponibles para el Usuario (se asignará el de Coordinador):</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:ListBox ID="AvailableRoles" runat="server" SelectionMode="Multiple" 
                                            Height="104px" Width="264px" Enabled="False"></asp:ListBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        <asp:CompleteWizardStep runat="server" />
                    </WizardSteps>
                </asp:CreateUserWizard>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style13">
                &nbsp;</td>
            <td class="style14">
                <asp:Label ID="Lblerror" runat="server" 
                    style="font-family: Arial, Helvetica, sans-serif; font-size: small; color: #FF0000"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style13">
                &nbsp;</td>
            <td class="style14">
                <asp:SqlDataSource ID="SDSprofesores" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="select IdProfesor, Apellidos + ',  ' + Nombre+ ' (' + Clave + ')' NomProfesor
from Profesor
where IdUsuario is null
order by Apellidos, Nombre"></asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSusuarios" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                        SelectCommand="SELECT * FROM [Usuario]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style13">
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Login.aspx" 
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
            </td>
            <td class="style14">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

