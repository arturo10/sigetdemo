﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="Default2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

  <style type="text/css">
      .compTable td, .compTable th {
        border: 1px solid #333;
        padding: 5px;
      }
  </style>
</head>
<body>
    <form id="form1" runat="server">
      <table class="compTable">
        <thead>
          <tr>
            <th>
              Respuesta del Usuario
            </th>
            <th>
              Opción Correcta
            </th>
            <th>
              Compara Mayúsculas
            </th>
            <th>
              Compara Acentos
            </th>
            <th>
              Compara Sólo Números y Letras
            </th>
            <th>
              Resultado de Comparación
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <asp:Label ID="Label11" runat="server">HólA</asp:Label>
            </td>
            <td>
              <asp:Label ID="Label12" runat="server">hola</asp:Label>
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td>
              <%= (Siget.Utils.StringUtils.ComparaRespuesta(Label11.Text, Label12.Text, false, false, false) == 0) ? "igual" : "diferente" %>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Label ID="Label5" runat="server">HólA</asp:Label>
            </td>
            <td>
              <asp:Label ID="Label6" runat="server">hola.</asp:Label>
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td>
              <%= (Siget.Utils.StringUtils.ComparaRespuesta(Label5.Text, Label6.Text, false, false, false) == 0) ? "igual" : "diferente" %>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Label ID="Label1" runat="server">The river ganges is the second longest river in - india. it runs from the himalayas to the bay of bengal.</asp:Label>
            </td>
            <td>
              <asp:Label ID="Label2" runat="server">The River Ganges is The second longest river in - India. It runs from The Himalayas to the Bay of Bengal.</asp:Label>
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td>
              <%= (Siget.Utils.StringUtils.ComparaRespuesta(Label1.Text, Label2.Text, false, false, false) == 0) ? "igual" : "diferente" %>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Label ID="Label21" runat="server">dont12</asp:Label>
            </td>
            <td>
              <asp:Label ID="Label22" runat="server">d*"#$&%(/)ont+12</asp:Label>
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #D4FF6A">
              Si
            </td>
            <td>
              <%= (Siget.Utils.StringUtils.ComparaRespuesta(Label21.Text, Label22.Text, false, false, true) == 0) ? "igual" : "diferente" %>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Label ID="Label3" runat="server">dont'´^^`.,:;-</asp:Label>
            </td>
            <td>
              <asp:Label ID="Label4" runat="server">dont</asp:Label>
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #D4FF6A">
              Si
            </td>
            <td>
              <%= (Siget.Utils.StringUtils.ComparaRespuesta(Label3.Text, Label4.Text, false, false, true) == 0) ? "igual" : "diferente" %>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Label ID="Label31" runat="server">hóla</asp:Label>
            </td>
            <td>
              <asp:Label ID="Label32" runat="server">hola</asp:Label>
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #D4FF6A">
              Si
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td>
              <%= (Siget.Utils.StringUtils.ComparaRespuesta(Label31.Text, Label32.Text, false, true, false) == 0) ? "igual" : "diferente" %>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Label ID="Label7" runat="server">hólâ</asp:Label>
            </td>
            <td>
              <asp:Label ID="Label8" runat="server">hólâ</asp:Label>
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #D4FF6A">
              Si
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td>
              <%= (Siget.Utils.StringUtils.ComparaRespuesta(Label7.Text, Label8.Text, false, true, false) == 0) ? "igual" : "diferente" %>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Label ID="Label51" runat="server">Benito Juárez</asp:Label>
            </td>
            <td>
              <asp:Label ID="Label52" runat="server">benito juárez</asp:Label>
            </td>
            <td style="background-color: #D4FF6A">
              Si
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td>
              <%= (Siget.Utils.StringUtils.ComparaRespuesta(Label51.Text, Label52.Text, true, false, false) == 0) ? "igual" : "diferente" %>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Label ID="Label9" runat="server">Benito Juárez</asp:Label>
            </td>
            <td>
              <asp:Label ID="Label10" runat="server">Benito Juarez</asp:Label>
            </td>
            <td style="background-color: #D4FF6A">
              Si
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td>
              <%= (Siget.Utils.StringUtils.ComparaRespuesta(Label9.Text, Label10.Text, true, false, false) == 0) ? "igual" : "diferente" %>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Label ID="Label41" runat="server"></asp:Label>
            </td>
            <td>
              <asp:Label ID="Label42" runat="server"></asp:Label>
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #D4FF6A">
              Si
            </td>
            <td style="background-color: #D4FF6A">
              Si
            </td>
            <td>
              <%= (Siget.Utils.StringUtils.ComparaRespuesta(Label41.Text, Label42.Text, false, false, false) == 0) ? "igual" : "diferente" %>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Label ID="Label61" runat="server"></asp:Label>
            </td>
            <td>
              <asp:Label ID="Label62" runat="server"></asp:Label>
            </td>
            <td style="background-color: #D4FF6A">
              Si
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td style="background-color: #D4FF6A">
              Si
            </td>
            <td>
              <%= (Siget.Utils.StringUtils.ComparaRespuesta(Label61.Text, Label62.Text, false, false, false) == 0) ? "igual" : "diferente" %>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Label ID="Label71" runat="server"></asp:Label>
            </td>
            <td>
              <asp:Label ID="Label72" runat="server"></asp:Label>
            </td>
            <td style="background-color: #D4FF6A">
              Si
            </td>
            <td style="background-color: #D4FF6A">
              Si
            </td>
            <td style="background-color: #FFCCCC">
              
            </td>
            <td>
              <%= (Siget.Utils.StringUtils.ComparaRespuesta(Label71.Text, Label72.Text, false, false, false) == 0) ? "igual" : "diferente" %>
            </td>
          </tr>
          <tr>
            <td>
              <asp:Label ID="Label81" runat="server">the river ganges is the second longest river in india it runs from the himalayas to the bay of bengal</asp:Label>
            </td>
            <td>
              <asp:Label ID="Label82" runat="server">The River Ganges is The second longest river in India. It runs from The Himalayas to the Bay of Bengal</asp:Label>
            </td>
            <td style="background-color: #D4FF6A">
              Si
            </td>
            <td style="background-color: #D4FF6A">
              Si
            </td>
            <td style="background-color: #D4FF6A">
              Si
            </td>
            <td>
              <%= (Siget.Utils.StringUtils.ComparaRespuesta(Label81.Text, Label82.Text, false, false, true) == 0) ? "igual" : "diferente" %>
            </td>
          </tr>
        </tbody>
      </table>
    </form>
</body>
</html>
