﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Bloqueo.master" 
    AutoEventWireup="false" 
    CodeFile="Oops.aspx.vb" 
    Inherits="Oops" 
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" Runat="Server">
    <div style="width: 100%; text-align: center;"><h1>
        Oops!
    </h1></div>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" Runat="Server">
    <br /><div style="width: 100%; text-align: center;">
        <asp:Image ID="Image1" runat="server" ImageUrl="~/Resources/imagenes/dead.png"/>
    </div><br />
    <p>
        Lo sentimos, ha ocurrido un error en la aplicación. 
    </p>
    <p>
        Si persiste esta situación por favor repórtelo a su administrador, haremos lo posible por corregirlo a la brevedad.
    </p>
    <br />
    <asp:HyperLink ID="HyperLink5" runat="server"
		NavigateUrl="javascript:history.back()"
		CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
</asp:Content>

