﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Bloqueo.master" 
    AutoEventWireup="false" 
    CodeFile="BloqueadoProfesor.aspx.vb" 
    Inherits="BloqueadoProfesor"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .returnLink a:link,
        .returnLink a:hover,
        .returnLink a:visited,
        .returnLink a:active {
            color: #444;
            font-weight: 600;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" Runat="Server">
    <h1>Horario Restringido</h1>
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" Runat="Server">
    <br />
    Lo sentimos, el sistema está configurado para no permitir acceso a 
    <asp:Label ID="Label1" runat="server" Text="[PROFESORES]" />
     en este momento. 
    <br />
    Intenta más tarde, o pregunta a tus 
    <asp:Label ID="Label2" runat="server" Text="[COORDINADORES]" />
     los horarios de acceso.
    <br />
    <br />
    <div class="returnLink">
        <asp:HyperLink ID="HyperLink1" runat="server"
            NavigateUrl="Default.aspx" >
		    Regresar&nbsp;a&nbsp;la&nbsp;Página&nbsp;Principal
        </asp:HyperLink>
    </div>
    <br />
</asp:Content>

