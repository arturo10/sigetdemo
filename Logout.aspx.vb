﻿Imports Siget


Partial Class Logout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.CierraSesion()
        Response.Redirect("~/Default.aspx", False)
    End Sub

End Class
