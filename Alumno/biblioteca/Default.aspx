﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/alumno/principal_alumno_5-1-0.master"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="alumno_biblioteca_Default" %>

<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/PageTitle_v1.ascx" TagPrefix="uc1" TagName="PageTitle_v1" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%-- Aquí imprimo los estilos variables de ésta página específica --%>
    <asp:Literal ID="ltEstilos" runat="server" />

 

    
        
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
   
     <link href="http://vjs.zencdn.net/5.9.2/video-js.css" rel="stylesheet">

  
    <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1" />
    <div style="margin-left: auto; margin-right: auto; display: block;margin-top:40px;">
        <div style="margin-left: auto; margin-right: auto; display: block;">
            <div class="row">
                <div class="form-horizontal">
                    <div class="col-md-11 col-md-offset-1">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class=" col-lg-2 control-label">Búsqueda</label>
                                <div class="col-lg-4">
                                        <input type="text" id="textFilter" class="form-control"/>
                                </div>
                                <button type="submit" id="btnSearch" class="btn btn-success">Buscar</button>
                            </div>
                            <div class="form-group">
                                <div id="jstree_demo_div"></div>
                            </div>
                        </div>
                        <div id="containerPreview" style="min-height:300px;text-align:center;" class="col-md-8" >
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</asp:Content>


