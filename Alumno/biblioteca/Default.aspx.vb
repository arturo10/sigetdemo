﻿Imports Siget

Imports System.Data
Imports System.Data.OleDb
Imports System.Data.SqlClient

Partial Class alumno_biblioteca_Default
    Inherits System.Web.UI.Page

    '#Region "init"

    '    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '        Utils.Sesion.sesionAbierta()

    '        ' esto debe ir antes que cualquier operación que utilice base de datos
    '        If Utils.Bloqueo.sistemaBloqueado() Then
    '            Utils.Sesion.CierraSesion()
    '            Response.Redirect("~/EnMantenimiento.aspx")
    '        End If

    '        ' aquí inicializo la página la primera vez que se carga
    '        If Not IsPostBack Then
    '            ' limpio las variables de la sesión de contestado si están definidas
    '            Utils.Sesion.LimpiaSesionContestado()

    '            initPageData()

    '            ' aplica lenguaje necesita controles inicializados de initPageData
    '            aplicaLenguaje()

    '            OutputCss()
    '        End If
    '    End Sub

    '    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    '    Protected Sub aplicaLenguaje()
    '        TitleLiteral.Text = Lang.FileSystem.Alumno.Biblioteca.LT_TITULO.Item(Session("Usuario_Idioma"))

    '    PageTitle_v1.show(Lang.FileSystem.Alumno.Biblioteca.APOYOS_NIVEL.Item(Session("Usuario_Idioma")) & " " & hf_nivel.Value,
    '                      Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/resources.png")
    '    lt_mostrar.Text = Lang.FileSystem.Alumno.Biblioteca.LT_MOSTRAR.Item(Session("Usuario_Idioma"))

    '    DDLtipoarchivo.Items(0).Text = Lang.FileSystem.Alumno.Biblioteca.DDL_ARCHIVO_SELECCIONE.Item(Session("Usuario_Idioma"))
    '    DDLtipoarchivo.Items(1).Text = Lang.FileSystem.Alumno.Biblioteca.DDL_ARCHIVO_TODOS.Item(Session("Usuario_Idioma"))
    '    DDLtipoarchivo.Items(7).Text = Lang.FileSystem.Alumno.Biblioteca.DDL_ARCHIVO_IMAGEN.Item(Session("Usuario_Idioma"))
    '    DDLtipoarchivo.Items(11).Text = Lang.FileSystem.Alumno.Biblioteca.DDL_ARCHIVO_ANIMACION.Item(Session("Usuario_Idioma"))
    '    GVapoyos.Caption = Lang.FileSystem.Alumno.Biblioteca.GV_APOYOS_CAPTION.Item(Session("Usuario_Idioma"))

    '    lt_backupLink.Text = Lang.FileSystem.Alumno.Biblioteca.LT_BACKUP_LINK.Item(Session("Usuario_Idioma"))
    '  End Sub

    '  ' inicializo los datos que necesito para los controles de la página
    '  Protected Sub initPageData()
    '    Dim strConexion As String
    '    'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
    '    strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString

    '    Dim objConexion As New SqlConnection(strConexion)
    '    Dim miComando As SqlCommand
    '    Dim miNivel As SqlDataReader
    '    miComando = objConexion.CreateCommand
    '    'Obtengo el Nivel al que pertenece el Alumno
    '    Dim Usuario = User.Identity.Name
    '    miComando.CommandText = "SELECT N.IdNivel, N.Descripcion FROM Alumno A, Grupo G1, Grado G2, Nivel N, Usuario U " + _
    '                    "WHERE U.Login = '" + Usuario + "' and A.IdUsuario = U.IdUsuario and G1.IdGrupo = A.IdGrupo " + _
    '                    "and G2.IdGrado = G1.IdGrado and N.IdNivel = G2.IdNivel"
    '    Try
    '      objConexion.Open() 'Abro la conexion
    '      miNivel = miComando.ExecuteReader() 'Creo conjunto de registros
    '      miNivel.Read()
    '      Session("IdNivel") = miNivel.Item("IdNivel")
    '      hf_nivel.Value = miNivel.Item("Descripcion").ToString()

    '      miNivel.Close()
    '      objConexion.Close()
    '      msgError.hide()
    '    Catch ex As Exception
    '      Utils.LogManager.ExceptionLog_InsertEntry(ex)
    '      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    '      'miNivel.Close() AQUI MARCA ERROR POR EL AMBITO DE LA EXCEPCION
    '      objConexion.Close()
    '    End Try
    '  End Sub

    '  Protected Sub OutputCss()
    '    Dim s As StringBuilder = New StringBuilder()
    '    s.Append("<style type='text/css'>")
    '    s.Append("  #menu .current_area_biblioteca a {") ' esta clase css debe corresponder a la clase del boton a resaltar
    '    s.Append("    border-bottom: 5px solid " & Config.Color.MenuBar_SelectedOption_BorderColor & ";")
    '    s.Append("  }")
    '    s.Append("</style>")
    '    ltEstilos.Text += s.ToString()
    '  End Sub

    '#End Region

    '  Protected Sub GVapoyos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVapoyos.SelectedIndexChanged
    '    msgInfo.show(Lang.FileSystem.Alumno.Biblioteca.REPRODUCIENDO.Item(Session("Usuario_Idioma")) & ": " + Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(3).Text)))
    '    PnlLink.Visible = False
    '    PnlDespliega.Controls.Add(New LiteralControl("<Table align='left' border='1' bordercolor = '#CCCCCC'>")) 'Agrego el inicio de la Tabla

    '    If Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(6).Text)) = "Imagen" Then
    '      PnlDespliega.Controls.Add(New LiteralControl("<IMG SRC=""" & Config.Global.urlApoyo & Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)) + """>"))
    '    ElseIf Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(6).Text)) = "Video" Or Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(6).Text)) = "Audio" Then
    '      'Acepta videos en formato .mpg y .avi hasta ahorita probados
    '      PnlLink.Visible = True
    '      HLarchivo.Text = HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)
    '      HLarchivo.NavigateUrl = Config.Global.urlApoyo & HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)
    '      HLarchivo.Target = "_blank"
    '      PnlDespliega.Controls.Add(New LiteralControl("<embed src=""" & Config.Global.urlApoyo & Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)) + """ width=""280"" height=""227"" autostart=""true"" controller=""true"" align=""center""></embed>"))
    '      'NO: Response.Write("<script>window.open('" & Config.Global.urlApoyo & HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text) + "','_blank');</script>")
    '    ElseIf Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(6).Text)) = "FLV" Then
    '      'ASÍ FUNCIONABA ANTES CON EL FLVPLAYER: PnlDespliega.Controls.Add(New LiteralControl("<object type=""application/x-shockwave-flash"" width=""600"" height=""450"" wmode=""transparent"" data=""/" & Config.Global.NOMBRE_FILESYSTEM & "/flvplayer.swf?file=Resources/apoyo/prueba.flv&autoStart=true""><param name=""movie"" value=""/" & Config.Global.NOMBRE_FILESYSTEM & "/flvplayer.swf?file=apoyo/prueba.flv&autoStart=true"" /><param name=""wmode"" value=""transparent"" /></object>"))
    '      'PnlDespliega.Controls.Add(New LiteralControl("<object type=""application/x-shockwave-flash"" width=""700"" height=""520"" wmode=""transparent"" data=""/" & Config.Global.NOMBRE_FILESYSTEM & "/flvplayer.swf?file=material/" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + "&autoStart=true""><param name=""movie"" value=""/" & Config.Global.NOMBRE_FILESYSTEM & "/flvplayer.swf?file=material/" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + "&autoStart=true"" /><param name=""wmode"" value=""transparent"" /></object>"))
    '      PnlDespliega.Controls.Add(New LiteralControl("<table style='margin-left: auto; margin-right: auto;'><tr><td><script type=""text/javascript"" src=""/" & Config.Global.NOMBRE_FILESYSTEM & "/usuarios/flash/jwplayer.js""></script><div id=""container"">Cargando el video ...</div><script type=""text/javascript""> jwplayer(""container"").setup({ flashplayer: ""/" & Config.Global.NOMBRE_FILESYSTEM & "/usuarios/flash/player.swf"", file: """ & Config.Global.urlApoyo & Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)) + """, autostart: true, height: 420, width: 680 }); </script></td></tr></table>"))
    '    ElseIf Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(6).Text)) = "SWF" Then
    '      PnlDespliega.Controls.Add(New LiteralControl("<object classid=""clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"" width=""680"" height=""420""><param name=""allowScriptAccess"" value=""never""/><param name=""allowNetworking"" value=""internal"" /><param name=""movie"" value=""" & Config.Global.urlApoyo & Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)) + """/><param name=""wmode"" value=""transparent"" /><embed type=""application/x-shockwave-flash"" allowScriptAccess=""never"" allowNetworking=""internal"" src=""" & Config.Global.urlApoyo & Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)) + """ width=""680"" height=""420"" wmode=""transparent""/></object>"))
    '    ElseIf Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(6).Text)) = "Link" Then
    '      'PnlDespliega.Controls.Add(New LiteralControl("<b><font face=""Arial"" size=3><a href=""" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + """ target=""_blank"">" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + "</a></font></b>"))
    '      PnlLink.Visible = True
    '      HLarchivo.Text = HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)
    '      HLarchivo.NavigateUrl = HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)
    '      HLarchivo.Target = "_blank"
    '      Response.Write("<script>window.open('" + HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text) + "','_blank');</script>")

    '    ElseIf Trim(HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(6).Text)) = "YouTube" Then
    '      Dim VideoID As String = Regex.Match(GVapoyos.SelectedRow.Cells(4).Text, "youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)").Groups(1).Value

    '      PnlDespliega.Controls.Add(New LiteralControl("<object><param name='movie' value='http://www.youtube.com/v/" & VideoID & "'></param><param name='wmode' value='transparent'></param><param name='allowFullScreen' value='true'></param><embed src='http://www.youtube.com/v/" & VideoID & "' type='application/x-shockwave-flash' allowfullscreen='true' wmode='transparent' width='640' height='390'></embed></object>"))

    '    Else
    '      'Si llega aquí es que es una liga a un documento:
    '      'PnlDespliega.Controls.Add(New LiteralControl("<b><font face=""Arial"" size=3><a href=""/" & Config.Global.NOMBRE_FILESYSTEM & "/material/" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + """ target=""_blank"">" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + "</a></font></b>"))
    '      PnlLink.Visible = True
    '      HLarchivo.Text = HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)
    '      HLarchivo.NavigateUrl = Config.Global.urlApoyo & HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text)
    '      HLarchivo.Target = "_blank"
    '      'Response.Write("<script>window.open('" & Config.Global.urlApoyo & HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text) + "','_blank');</script>")

    '      Response.Write("<script>window.open('" & Config.Global.urlApoyo & HttpUtility.HtmlDecode(GVapoyos.SelectedRow.Cells(4).Text) + "','_blank','scrollbars=yes,resizable=yes');</script>")
    '    End If
    '    PnlDespliega.Controls.Add(New LiteralControl("</Table>"))
    '  End Sub

    '  Protected Sub GVapoyos_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVapoyos.DataBound
    '    If (GVapoyos.Rows.Count <= 0) And (DDLtipoarchivo.SelectedIndex > 0) Then
    '      msgInfo.show(Lang.FileSystem.Alumno.Biblioteca.NO_HAY_RECURSOS.Item(Session("Usuario_Idioma")))
    '    Else
    '      msgInfo.hide()
    '    End If
    '  End Sub

    '  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
    '  ' Métodos de GridViews
    '  ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    '  Protected Sub GVapoyos_PreRender(sender As Object, e As EventArgs) Handles GVapoyos.PreRender
    '    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    '    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    '    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    '    If GVapoyos.Rows.Count > 0 Then
    '      GVapoyos.HeaderRow.TableSection = TableRowSection.TableHeader
    '    End If
    '  End Sub

    '  Protected Sub GVapoyos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVapoyos.RowCreated
    '    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    '    If e.Row.RowType = DataControlRowType.Header Then
    '      For Each tc As TableCell In e.Row.Cells
    '        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
    '          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
    '          If Not IsNothing(lb) Then
    '            ' quito el link button para reañadirlo después del link
    '            tc.Controls.RemoveAt(0)
    '            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
    '            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
    '            ' creo el ícono de sorting
    '            Dim icon As Image = New Image()
    '            icon.ImageUrl = Config.Global.urlImagenes & "btnicons/sortable.png"
    '            ' ésta propiedad es importante para que se cargue a la izquierda
    '            div.Attributes.Add("style", "float: left; position: absolute;")
    '            ' añado el ícono al div
    '            div.Controls.Add(icon)
    '            ' añado el div al header
    '            tc.Controls.Add(div)
    '            ' reañado el link
    '            tc.Controls.Add(lb)
    '            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
    '            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

    '            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
    '            ' para asignarle un resaltado con css
    '            If GVapoyos.SortExpression = lb.CommandArgument Then
    '              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
    '            End If
    '          End If
    '        End If
    '      Next
    '    End If
    '  End Sub

    '    Protected Sub GVapoyos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVapoyos.RowDataBound
    '        ' Etiquetas de GridView
    '        If e.Row.RowType = DataControlRowType.Header Then
    '            Dim LnkHeaderText As LinkButton = e.Row.Cells(2).Controls(1)
    '            LnkHeaderText.Text = Lang.FileSystem.Alumno.Biblioteca.GVAPOYOS_HEADER_CONSECUTIVO.Item(Session("Usuario_Idioma"))

    '            LnkHeaderText = e.Row.Cells(3).Controls(1)
    '            LnkHeaderText.Text = Lang.FileSystem.Alumno.Biblioteca.GVAPOYOS_HEADER_DESCRIPCION.Item(Session("Usuario_Idioma"))

    '            LnkHeaderText = e.Row.Cells(4).Controls(1)
    '            LnkHeaderText.Text = Lang.FileSystem.Alumno.Biblioteca.GVAPOYOS_HEADER_ARCHIVO.Item(Session("Usuario_Idioma"))

    '            LnkHeaderText = e.Row.Cells(6).Controls(1)
    '            LnkHeaderText.Text = Lang.FileSystem.Alumno.Biblioteca.GVAPOYOS_HEADER_TIPO.Item(Session("Usuario_Idioma"))

    '        ElseIf e.Row.RowType = DataControlRowType.DataRow Then
    '            Dim LnkSelectText As LinkButton = e.Row.Cells(0).Controls(0)
    '            LnkSelectText.Text = Lang.FileSystem.Alumno.Biblioteca.GV_SELECCIONAR.Item(Session("Usuario_Idioma"))
    '        End If

    '        ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVapoyos, "Select$" & e.Row.RowIndex).ToString())
    '        End If
    '    End Sub

    '    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    '    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    '    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    '    ' para que no falle la validación de integridad del control.
    '    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    '    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    '        Dim r As GridViewRow
    '        For Each r In GVapoyos.Rows()
    '            If r.RowType = DataControlRowType.DataRow Then
    '                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVapoyos, "Select$" + r.RowIndex.ToString()))
    '            End If
    '        Next

    '        MyBase.Render(writer) ' necesario por default
    '    End Sub
End Class
