<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/alumno/comunicacion_alumno_5-1-0.master"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="alumno_comunicacion_mensaje_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/PageTitle_v1.ascx" TagPrefix="uc1" TagName="PageTitle_v1" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder_comunicacion" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head_comunicacion" runat="Server">
    <%-- Aqu� imprimo los estilos variables de �sta p�gina espec�fica --%>
    <asp:Literal ID="ltEstilos" runat="server" />

    <style type="text/css">
        .leftSpacer1 {
            width: 15px;
        }

        .rightSpacer1 {
            width: 15px;
        }

        .labelcolumn {
            text-align: right;
            width: 140px;
            font-size: small;
        }

        .contentColumn {
            border: 1px outset #bbb;
            padding: 2px;
            font-size: small;
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor_comunicacion" runat="Server">

    <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1" />

    <asp:Panel ID="pnlMensajeProfesor" runat="server">
        <table style="width: 100%;">
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_hintProfesor" runat="server">[Profesor]</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltEnvia" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_hintAsignatura" runat="server">[Asignatura]</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltAsignatura" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_profesor_hint_fecha" runat="server">[Fecha de Env�o]</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltFechaEnvio" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_profesor_hint_asunto" runat="server">[Asunto]</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltAsunto" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_profesor_hint_contenido" runat="server">[Contenido]</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltContenido" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_hintAdjunto" runat="server">[Archivo&nbsp;Adjunto]</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:LinkButton ID="lbAdjunto" runat="server"
                        CssClass="LabelInfoDefault"></asp:LinkButton>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">&nbsp;</td>
                <td style="text-align: left;">
                    <asp:CheckBox ID="CBeliminar" runat="server"
                        Style="font-family: Arial, Helvetica, sans-serif; font-size: small"
                        Text="[Marcar para eliminar este mensaje]" />
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center;">
                    <asp:LinkButton ID="LBresponder" runat="server"
                        CssClass="defaultBtn btnThemeBlue btnThemeMedium">
                        <asp:Image ID="Image3" runat="server" 
                            ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/email_go.png" 
                            CssClass="btnIcon" />
                        <asp:Literal ID="lt_profe_btn_responder" runat="server">[Responder]</asp:Literal>
                    </asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="LBeliminar" runat="server"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                        <asp:Image ID="Image1" runat="server" 
                            ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/email_delete.png" 
                            CssClass="btnIcon" />
                        <asp:Literal ID="lt_profe_btn_eliminar" runat="server">[Eliminar]</asp:Literal>
                    </asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="LBreenviar" runat="server"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                        <asp:Image ID="Image2" runat="server" 
                            ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/email_forward.png" 
                            CssClass="btnIcon" />
                        <asp:Literal ID="lt_profe_btn_reenviar" runat="server">[Reenviar]</asp:Literal>
                    </asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel ID="pnlMensajeCoordinador" runat="server">
        <table style="width: 100%;">
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_hintCoordinador" runat="server">[Coordinador]</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltEnviaC" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_hintPlantel" runat="server">[Plantel]</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltPlantel" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_coordinador_hint_fecha" runat="server">[Fecha de Env�o]</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltFechaEnvioC" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_coordinador_hint_asunto" runat="server">[Asunto]</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltAsuntoC" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_coordinador_hint_contenido" runat="server">[Contenido]</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:Literal ID="ltContenidoC" runat="server"></asp:Literal>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">
                    <asp:Literal ID="lt_hintAdjuntoC" runat="server">[Archivo&nbsp;Adjunto]</asp:Literal>
                </td>
                <td class="contentColumn">
                    <asp:LinkButton ID="lbAdjuntoC" runat="server"
                        CssClass="LabelInfoDefault"></asp:LinkButton>
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td class="leftSpacer1">&nbsp;</td>
                <td class="labelcolumn">&nbsp;</td>
                <td style="text-align: left;">
                    <asp:CheckBox ID="CBeliminarC" runat="server"
                        Style="font-family: Arial, Helvetica, sans-serif; font-size: small"
                        Text="[Marcar para eliminar este mensaje]" />
                </td>
                <td class="rightSpacer1">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center;">
                    <asp:LinkButton ID="LBresponderC" runat="server"
                        CssClass="defaultBtn btnThemeBlue btnThemeMedium">
                        <asp:Image ID="Image4" runat="server" 
                            ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/email_go.png" 
                            CssClass="btnIcon" />
                        <asp:Literal ID="lt_coord_btn_responder" runat="server">[Responder]</asp:Literal>
                    </asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="LBeliminarC" runat="server"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                            <asp:Image ID="Image5" runat="server" 
                                ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/email_delete.png" 
                                CssClass="btnIcon" />
                        <asp:Literal ID="lt_coord_btn_eliminar" runat="server">[Eliminar]</asp:Literal>
                    </asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="LBreenviarC" runat="server"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                        <asp:Image ID="Image6" runat="server" 
                            ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/email_forward.png" 
                            CssClass="btnIcon" />
                        <asp:Literal ID="lt_coord_btn_reenviar" runat="server">[Reenviar]</asp:Literal>
                    </asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <table style="width: 100%;">
        <tr>
            <td colspan="4">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
    </table>

    <table style="width: 100%;">
        <tr>
            <td colspan="4" style="text-align: left">
                <asp:HyperLink ID="HyperLink2" runat="server"
                    NavigateUrl="javascript:history.go(-1);"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
                    <asp:Image ID="Image7" runat="server" 
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/bullet_back_2.png" 
                        CssClass="btnIcon" />
                    <asp:Literal ID="lt_btn_regresar" runat="server">[Regresar]</asp:Literal>
                </asp:HyperLink>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSmensaje" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [ComunicacionCP]"></asp:SqlDataSource>
            </td>
            <td>
                <asp:HiddenField ID="hfIdAlumno" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfIdProgramacion" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfEmail" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hfAdjuntoFisico" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfIdCoordinador" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfCarpetaAdjunto" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>
