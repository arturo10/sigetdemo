﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO

Partial Class alumno_comunicacion_mensaje_Default
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        If Session("Envio_Previous").Equals("LeerMensajeSalidaA") Then
            HyperLink2.NavigateUrl = "~/alumno/comunicacion/salida/"
            LBresponder.Visible = False
            LBresponderC.Visible = False
        Else
            HyperLink2.NavigateUrl = "~/alumno/comunicacion/entrada/"
        End If

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            aplicaLenguaje()

            initPageData()

            OutputCss()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = Lang.FileSystem.Alumno.Mensaje.LT_TITULO.Item(Session("Usuario_Idioma"))

        If Session("Envio_Previous").Equals("LeerMensajeSalidaA") Then
            lt_hintProfesor.Text = Lang.FileSystem.Alumno.Mensaje.LT_HINT_PROFESOR_RECIBE.Item(Session("Usuario_Idioma"))
            lt_hintCoordinador.Text = Lang.FileSystem.Alumno.Mensaje.LT_HINT_COORDINADOR_RECIBE.Item(Session("Usuario_Idioma"))

            If Session("Envio_IdComunicacion") <> "0" Then
        PageTitle_v1.show(Lang.FileSystem.Alumno.Mensaje.LT_TITULO_PAGINA_SALIDA_PROFESOR.Item(Session("Usuario_Idioma")),
                          Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/email_open.png")
        'lt_titulo.Text = "Mensaje Enviado a " & Lang.Config.Etiqueta.PROFESOR
      ElseIf Session("Envio_IdComunicacionCA") <> "0" Then
        PageTitle_v1.show(Lang.FileSystem.Alumno.Mensaje.LT_TITULO_PAGINA_SALIDA_COORDINADOR.Item(Session("Usuario_Idioma")),
                          Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/email_open.png")
        'lt_titulo.Text = "Mensaje Enviado a " & Lang.Config.Etiqueta.COORDINADOR
      End If
    Else
      lt_hintProfesor.Text = Lang.FileSystem.Alumno.Mensaje.LT_HINT_PROFESOR_ENVIA.Item(Session("Usuario_Idioma"))
      lt_hintCoordinador.Text = Lang.FileSystem.Alumno.Mensaje.LT_HINT_COORDINADOR_ENVIA.Item(Session("Usuario_Idioma"))

      If Session("Envio_IdComunicacion") <> "0" Then
        PageTitle_v1.show(Lang.FileSystem.Alumno.Mensaje.LT_TITULO_PAGINA_ENTRADA_PROFESOR.Item(Session("Usuario_Idioma")),
                          Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/email_open.png")
        'lt_titulo.Text = "Mensaje Recibido de " & Lang.Config.Etiqueta.PROFESOR
      ElseIf Session("Envio_IdComunicacionCA") <> "0" Then
        PageTitle_v1.show(Lang.FileSystem.Alumno.Mensaje.LT_TITULO_PAGINA_ENTRADA_COORDINADOR.Item(Session("Usuario_Idioma")),
                          Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/email_open.png")
        'lt_titulo.Text = "Mensaje Recibido de " & Lang.Config.Etiqueta.COORDINADOR
      End If
    End If

    lt_hintAsignatura.Text = Lang.Etiqueta.Asignatura.Item(Session("Usuario_Idioma").ToString())
    lt_profesor_hint_fecha.Text = Lang.FileSystem.Alumno.Mensaje.LT_HINT_FECHA.Item(Session("Usuario_Idioma"))
    lt_profesor_hint_asunto.Text = Lang.FileSystem.Alumno.Mensaje.LT_HINT_ASUNTO.Item(Session("Usuario_Idioma"))
    lt_profesor_hint_contenido.Text = Lang.FileSystem.Alumno.Mensaje.LT_HINT_CONTENIDO.Item(Session("Usuario_Idioma"))
    lt_hintAdjunto.Text = Lang.FileSystem.Alumno.Mensaje.LT_HINT_ADJUNTO.Item(Session("Usuario_Idioma"))
    CBeliminar.Text = Lang.FileSystem.Alumno.Mensaje.CBELIMINAR.Item(Session("Usuario_Idioma"))
    lt_profe_btn_responder.Text = Lang.FileSystem.Alumno.Mensaje.LT_BTN_RESPONDER.Item(Session("Usuario_Idioma"))
    lt_profe_btn_eliminar.Text = Lang.FileSystem.Alumno.Mensaje.LT_BTN_ELIMINAR.Item(Session("Usuario_Idioma"))
    lt_profe_btn_reenviar.Text = Lang.FileSystem.Alumno.Mensaje.LT_BTN_REENVIAR.Item(Session("Usuario_Idioma"))

    lt_hintPlantel.Text = Lang.Etiqueta.Plantel.Item(Session("Usuario_Idioma").ToString())
    lt_coordinador_hint_fecha.Text = Lang.FileSystem.Alumno.Mensaje.LT_HINT_FECHA.Item(Session("Usuario_Idioma"))
    lt_coordinador_hint_asunto.Text = Lang.FileSystem.Alumno.Mensaje.LT_HINT_ASUNTO.Item(Session("Usuario_Idioma"))
    lt_coordinador_hint_contenido.Text = Lang.FileSystem.Alumno.Mensaje.LT_HINT_CONTENIDO.Item(Session("Usuario_Idioma"))
    lt_hintAdjuntoC.Text = Lang.FileSystem.Alumno.Mensaje.LT_HINT_ADJUNTO.Item(Session("Usuario_Idioma"))
    CBeliminarC.Text = Lang.FileSystem.Alumno.Mensaje.CBELIMINAR.Item(Session("Usuario_Idioma"))
    lt_coord_btn_responder.Text = Lang.FileSystem.Alumno.Mensaje.LT_BTN_RESPONDER.Item(Session("Usuario_Idioma"))
    lt_coord_btn_eliminar.Text = Lang.FileSystem.Alumno.Mensaje.LT_BTN_ELIMINAR.Item(Session("Usuario_Idioma"))
    lt_coord_btn_reenviar.Text = Lang.FileSystem.Alumno.Mensaje.LT_BTN_REENVIAR.Item(Session("Usuario_Idioma"))

    lt_btn_regresar.Text = Lang.FileSystem.Alumno.Mensaje.LT_BTN_REGRESAR.Item(Session("Usuario_Idioma"))
  End Sub

  ' inicializo los datos que necesito para los controles de la página
  Protected Sub initPageData()
    Try
      If Session("Envio_IdComunicacion") <> "0" Then ' MENSAJE DE PROFESOR
        pnlMensajeCoordinador.Visible = False ' oculto el panel no necesitado

        'Hago consulta para sacar los datos del mensaje
        Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
          conn.Open()

          Dim cmd As SqlCommand = New SqlCommand("select C.Fecha, C.Asunto, C.IdProgramacion, C.IdAlumno, P.Nombre + ' ' + P.Apellidos as 'Profesor', P.Email, " &
              "A.Descripcion as Asignatura, C.Contenido, C.Fecha, C.EstatusA, C.ArchivoAdjunto, C.ArchivoAdjuntoFisico, C.CarpetaAdjunto from Comunicacion C, Profesor P, Programacion Pr, Asignatura A " &
              "where C.IdComunicacion = @IdComunicacion and Pr.IdProgramacion = C.IdProgramacion and P.IdProfesor = Pr.IdProfesor And A.IdAsignatura = Pr.IdAsignatura", conn)
          cmd.Parameters.AddWithValue("@IdComunicacion", Session("Envio_IdComunicacion"))

          Dim results As SqlDataReader = cmd.ExecuteReader()
          results.Read()
          ' obtengo el contenido del mensaje
          ltEnvia.Text = HttpUtility.HtmlDecode(results.Item("Profesor"))
          ltAsignatura.Text = HttpUtility.HtmlDecode(results.Item("Asignatura"))
          ltFechaEnvio.Text = HttpUtility.HtmlDecode(results.Item("Fecha"))
          If Not IsDBNull(results.Item("Asunto")) Then
            ltAsunto.Text = HttpUtility.HtmlDecode(results.Item("Asunto"))
          End If
          ltContenido.Text = HttpUtility.HtmlDecode(results.Item("Contenido"))
          If Not IsDBNull(results.Item("ArchivoAdjunto")) Then
            lbAdjunto.Visible = True
            lt_hintAdjunto.Visible = True
            'Verifico si ya existe el directorio del alumno, si no, lo creo
            lbAdjunto.Text = results.Item("ArchivoAdjunto")
            hfAdjuntoFisico.Value = results.Item("ArchivoAdjuntoFisico")
            hfCarpetaAdjunto.Value = results.Item("CarpetaAdjunto")
          Else
            lbAdjunto.Visible = False
            lt_hintAdjunto.Visible = False
          End If

          'Los siguientes valores los utilizaré si le dan responder al mensaje
          hfIdAlumno.Value = results.Item("IdAlumno")
          hfIdProgramacion.Value = results.Item("IdProgramacion")
          If IsDBNull(results.Item("Email")) Then 'Si intento asignar un valor Nulo a una variable, marca error
            hfEmail.Value = ""
          Else
            hfEmail.Value = results.Item("Email")
          End If

          'Modificar el mensaje a "Leido" en caso de que este como "Nuevo"
          If results.Item("EstatusA") <> "Leido" AndAlso Not Session("Envio_Previous").Equals("LeerMensajeSalidaA") Then
            SDSmensaje.UpdateCommand = "SET dateformat dmy; UPDATE Comunicacion set Estatus = 'Leido', EstatusA = 'Leido' where IdComunicacion = @IdComunicacion"
            SDSmensaje.UpdateParameters.Add("IdComunicacion", Session("Envio_IdComunicacion"))
            SDSmensaje.Update()
          End If

          results.Close()
          cmd.Dispose()
          conn.Close()
        End Using
      ElseIf Session("Envio_IdComunicacionCA") <> "0" Then ' MENSAJE PARA COORDINADOR
        pnlMensajeProfesor.Visible = False ' oculto el panel no necesitado

        'Hago consulta para sacar los datos del mensaje
        Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
          conn.Open()

          Dim cmd As SqlCommand = New SqlCommand("select CA.Fecha, CA.Asunto, CA.IdCoordinador, CA.IdAlumno, C.Nombre + ' ' + C.Apellidos as 'Coordinador', C.Email, P.Descripcion as Plantel, CA.Contenido, CA.EstatusA,CA.ArchivoAdjunto, CA.ArchivoAdjuntoFisico, CA.CarpetaAdjunto " &
              "FROM ComunicacionCA CA, Coordinador C, Plantel P, Alumno A " &
              "WHERE CA.IdComunicacionCA = @IdComunicacionCA AND CA.IdCoordinador = C.IdCoordinador AND CA.IdAlumno = A.IdAlumno AND A.IdPlantel = P.IdPlantel", conn)
          cmd.Parameters.AddWithValue("@IdComunicacionCA", Session("Envio_IdComunicacionCA"))

          Dim results As SqlDataReader = cmd.ExecuteReader()
          results.Read()
          ' obtengo el contenido del mensaje
          ltEnviaC.Text = HttpUtility.HtmlDecode(results.Item("Coordinador"))
          ltPlantel.Text = HttpUtility.HtmlDecode(results.Item("Plantel"))
          ltFechaEnvioC.Text = HttpUtility.HtmlDecode(results.Item("Fecha"))

          If Not IsDBNull(results.Item("Asunto")) Then
            ltAsuntoC.Text = HttpUtility.HtmlDecode(results.Item("Asunto"))
          End If
          ltContenidoC.Text = HttpUtility.HtmlDecode(results.Item("Contenido"))
          If Not IsDBNull(results.Item("ArchivoAdjunto")) Then
            lbAdjuntoC.Visible = True
            lt_hintAdjuntoC.Visible = True
            lbAdjuntoC.Text = results.Item("ArchivoAdjunto")
            hfAdjuntoFisico.Value = results.Item("ArchivoAdjuntoFisico")
            hfCarpetaAdjunto.Value = results.Item("CarpetaAdjunto")
          Else
            lbAdjuntoC.Visible = False
            lt_hintAdjuntoC.Visible = False
          End If

          'Los siguientes valores los utilizaré si le dan responder al mensaje
          hfIdAlumno.Value = results.Item("IdAlumno")
          hfIdCoordinador.Value = results.Item("IdCoordinador")
          If IsDBNull(results.Item("Email")) Then 'Si intento asignar un valor Nulo a una variable, marca error
            hfEmail.Value = ""
          Else
            hfEmail.Value = results.Item("Email")
          End If

          'Modificar el mensaje a "Leido" en caso de que este como "Nuevo"
          If results.Item("EstatusA") <> "Leido" AndAlso Not Session("Envio_Previous").Equals("LeerMensajeSalidaA") Then
            SDSmensaje.UpdateCommand = "SET dateformat dmy; UPDATE ComunicacionCA set Estatus = 'Leido', EstatusA = 'Leido' where IdComunicacionCA = @IdComunicacionCA"
            SDSmensaje.UpdateParameters.Add("IdComunicacionCA", Session("Envio_IdComunicacionCA"))
            SDSmensaje.Update()
          End If

          results.Close()
          cmd.Dispose()
          conn.Close()
        End Using

      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  ' imprimo los estilos variables de ésta página
  Protected Sub OutputCss()
    Dim s As StringBuilder = New StringBuilder()
    s.Append("<style type='text/css'>")
    s.Append("  #menu .current_area_comunicacion a {") ' esta clase css debe corresponder a la clase del boton a resaltar
    s.Append("    border-bottom: 5px solid " & Config.Color.MenuBar_SelectedOption_BorderColor & ";")
    s.Append("  }")
    s.Append("</style>")
    ltEstilos.Text += s.ToString()
  End Sub

#End Region

#Region "eliminar"

  Protected Sub LBeliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBeliminar.Click
    Dim eliminado As Boolean = False

    Try
      If CBeliminar.Checked Then
        'Para eliminarlo solo es necesario darlo de baja, y si lo está desplegando es que no está dado de baja
        SDSmensaje.UpdateCommand = "SET dateformat dmy; UPDATE Comunicacion set EstatusA = 'Baja' where IdComunicacion = @IdComunicacion"
        SDSmensaje.UpdateParameters.Add("IdComunicacion", Session("Envio_IdComunicacion"))
        SDSmensaje.Update()
        SDSmensaje.UpdateParameters.Clear()

        'SIEMPRE NO LO ELIMINO PORQUE EL MISMO ARCHIVO LO PUEDEN ESTAR RECIBIENDO VARIOS ALUMNOS:
        'Elimino el archivo adjunto para no acumular basura
        'If Trim(lbAdjunto.Text).Length > 0 Then
        'Dim ruta As String
        'ruta = Config.Global.rutaMaterial
        'ruta = ruta + "adjuntos\" + Session("LoginProfesor") + "\" 'Esta ruta contempla un directorio como alumno nombrado como su login
        'Dim MiArchivo As FileInfo = New FileInfo(ruta & lbAdjunto.Text)
        'MiArchivo.Delete()
        'End If

        eliminado = True
      Else
        msgError.show(Lang.FileSystem.Alumno.Mensaje.ERR_MARCAR_PARA_ELIMINAR.Item(Session("Usuario_Idioma")))
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try

    If eliminado Then
      If Session("Envio_Previous").Equals("LeerMensajeSalidaA") Then
        Response.Redirect("~/alumno/comunicacion/salida/")
      Else
        Response.Redirect("~/alumno/comunicacion/entrada/")
      End If
    End If
  End Sub

  Protected Sub LBeliminarC_Click(sender As Object, e As EventArgs) Handles LBeliminarC.Click
    Dim eliminado As Boolean = False

    Try
      If CBeliminarC.Checked Then
        'Para eliminarlo solo es necesario darlo de baja, y si lo está desplegando es que no está dado de baja
        SDSmensaje.UpdateCommand = "SET dateformat dmy; UPDATE ComunicacionCA set EstatusA = 'Baja' where IdComunicacionCA = @IdComunicacionCA"
        SDSmensaje.UpdateParameters.Add("IdComunicacionCA", Session("Envio_IdComunicacionCA"))
        SDSmensaje.Update()
        SDSmensaje.UpdateParameters.Clear()

        'SIEMPRE NO LO ELIMINO PORQUE EL MISMO ARCHIVO LO PUEDEN ESTAR RECIBIENDO VARIOS ALUMNOS:
        'Elimino el archivo adjunto para no acumular basura
        'If Trim(lbAdjunto.Text).Length > 0 Then
        'Dim ruta As String
        'ruta = Config.Global.rutaMaterial
        'ruta = ruta + "adjuntos\" + Session("LoginProfesor") + "\" 'Esta ruta contempla un directorio como alumno nombrado como su login
        'Dim MiArchivo As FileInfo = New FileInfo(ruta & lbAdjunto.Text)
        'MiArchivo.Delete()
        'End If

        eliminado = True
      Else
        msgError.show(Lang.FileSystem.Alumno.Mensaje.ERR_MARCAR_PARA_ELIMINAR.Item(Session("Usuario_Idioma")))
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try

    If eliminado Then
      If Session("Envio_Previous").Equals("LeerMensajeSalidaA") Then
        Response.Redirect("~/alumno/comunicacion/salida/")
      Else
        Response.Redirect("~/alumno/comunicacion/entrada/")
      End If
    End If
  End Sub

#End Region

#Region "adjuntos"

  Protected Sub lbAdjunto_Click(sender As Object, e As EventArgs) Handles lbAdjunto.Click
    Dim file As System.IO.FileInfo =
        New System.IO.FileInfo(Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value + "\" & hfAdjuntoFisico.Value)

    If file.Exists Then 'set appropriate headers
      Response.Clear()
      Response.AddHeader("Content-Disposition", "attachment; filename=""" & lbAdjunto.Text & """")
      Response.AddHeader("Content-Length", file.Length.ToString())
      Response.ContentType = "application/octet-stream"
      Response.WriteFile(file.FullName)
      'Response.End()
    Else
      msgError.show(Lang.FileSystem.Alumno.Mensaje.ERR_ADJUNTO_NO_DISPONIBLE.Item(Session("Usuario_Idioma")))
    End If 'nothing in the URL as HTTP GET
  End Sub

  Protected Sub lbAdjuntoC_Click(sender As Object, e As EventArgs) Handles lbAdjuntoC.Click
    Dim file As System.IO.FileInfo =
        New System.IO.FileInfo(Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value + "\" & hfAdjuntoFisico.Value)

    If file.Exists Then 'set appropriate headers
      Response.Clear()
      Response.AddHeader("Content-Disposition", "attachment; filename=" & lbAdjuntoC.Text)
      Response.AddHeader("Content-Length", file.Length.ToString())
      Response.ContentType = "application/octet-stream"
      Response.WriteFile(file.FullName)
      'Response.End()
    Else
      msgError.show(Lang.FileSystem.Alumno.Mensaje.ERR_ADJUNTO_NO_DISPONIBLE.Item(Session("Usuario_Idioma")))
    End If 'nothing in the URL as HTTP GET
  End Sub

#End Region

#Region "responder"

    Protected Sub LBresponder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBresponder.Click
        Session("Responder") = "P"
        Session("Envio_IdProgramacion") = hfIdProgramacion.Value
        Session("Envio_IdAlumno") = hfIdAlumno.Value
        Session("Envio_NombreProfesor") = ltEnvia.Text
        Session("Envio_Email") = hfEmail.Value
        Session("Envio_Asunto") = ltAsunto.Text
        Session("Envio_Asignatura") = ltAsignatura.Text
        Session("Envio_Fecha") = ltFechaEnvio.Text
        Response.Redirect("~/alumno/comunicacion/responder/")
    End Sub

    Protected Sub LBresponderC_Click(sender As Object, e As EventArgs) Handles LBresponderC.Click
        Session("Responder") = "C"
        Session("Envio_IdCoordinador") = hfIdCoordinador.Value
        Session("Envio_IdAlumno") = hfIdAlumno.Value
        Session("Envio_NombreCoordinador") = ltEnviaC.Text
        Session("Envio_Email") = hfEmail.Value
        Session("Envio_Asunto") = ltAsuntoC.Text
        Session("Envio_Plantel") = ltPlantel.Text
        Session("Envio_Fecha") = ltFechaEnvioC.Text
        Response.Redirect("~/alumno/comunicacion/responder/")
    End Sub

#End Region

    Protected Sub LBreenviar_Click(sender As Object, e As EventArgs) Handles LBreenviar.Click
        Session("Reenvio") = "P"
        Session("Envio_Asunto") = ltAsunto.Text
        Session("Envio_Fecha") = ltFechaEnvio.Text
        Session("Envio_Contenido") = ltContenido.Text
        Session("Envio_AdjuntoNombre") = lbAdjunto.Text
        Session("Envio_AdjuntoFisico") = hfAdjuntoFisico.Value
        Session("Envio_Carpeta") = hfCarpetaAdjunto.Value
        Response.Redirect("~/alumno/comunicacion/enviar/")
    End Sub

    Protected Sub LBreenviarC_Click(sender As Object, e As EventArgs) Handles LBreenviarC.Click
        Session("Reenvio") = "C"
        Session("Envio_Asunto") = ltAsuntoC.Text
        Session("Envio_Fecha") = ltFechaEnvioC.Text
        Session("Envio_Contenido") = ltContenidoC.Text
        Session("Envio_AdjuntoNombre") = lbAdjuntoC.Text
        Session("Envio_AdjuntoFisico") = hfAdjuntoFisico.Value
        Session("Envio_Carpeta") = hfCarpetaAdjunto.Value
        Response.Redirect("~/alumno/comunicacion/enviar/")
    End Sub
End Class
