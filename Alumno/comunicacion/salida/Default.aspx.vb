﻿Imports Siget

Imports System.Data.SqlClient

Partial Class alumno_comunicacion_salida_Default
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            aplicaLenguaje()

            initPageData()

            OutputCss()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = Lang.FileSystem.Alumno.Salida.LT_TITULO.Item(Session("Usuario_Idioma"))
    PageTitle_v1.show(Lang.FileSystem.Alumno.Salida.LT_TITULO_PAGINA.Item(Session("Usuario_Idioma")),
                      Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/account_level_filtering.png")
  End Sub

  ' inicializo los datos que necesito para los controles de la página
  Protected Sub initPageData()
    Try
      'obtengo el id del usuario para cargar sus mensajes
      ' no lo hago desde el sds con el login, por que las tablas de alumnos pueden ser muy grandes y la consulta sería dos veces
      Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
        conn.Open()

        Dim cmd As SqlCommand = New SqlCommand("SELECT A.IdAlumno, A.Nombre + ' ' + A.ApePaterno + ' ' + A.ApeMaterno AS NomAlumno FROM Usuario U, Alumno A where U.Login = @Login and A.IdUsuario = U.IdUsuario", conn)
        cmd.Parameters.AddWithValue("@Login", Trim(User.Identity.Name))

        Dim results As SqlDataReader = cmd.ExecuteReader()
        results.Read()
        Session("LeerMensajeSalidaA_IdAlumno") = results.Item("IdAlumno").ToString()
        Session("Envio_NombreAlumno") = results.Item("NomAlumno").ToString() ' se usará al enviar mails al responder mensajes

        results.Close()
        cmd.Dispose()
        conn.Close()
      End Using
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub OutputCss()
    Dim s As StringBuilder = New StringBuilder()
    s.Append("<style type='text/css'>")
    s.Append("  #menu .current_area_comunicacion a {") ' esta clase css debe corresponder a la clase del boton a resaltar
    s.Append("    border-bottom: 5px solid " & Config.Color.MenuBar_SelectedOption_BorderColor & ";")
    s.Append("  }")
    s.Append("  #menusCol .current_page_salida {")
    s.Append("    background: " & Config.Color.MenuPendientes_Subtitle & ";")
    s.Append("  }")
    s.Append("</style>")
    ltEstilos.Text += s.ToString()
  End Sub

#End Region

#Region "GVmensajesProfesores"

  Protected Sub GVmensajesProfesores_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmensajesProfesores.DataBound
    If GVmensajesProfesores.Rows.Count <> 0 Then
      GVmensajesProfesores.Caption = "<h3>(" & GVmensajesProfesores.Rows.Count & ") " & Lang.FileSystem.Alumno.Salida.GVMENSAJESPROFESORES_CAPTION.Item(Session("Usuario_Idioma")) & "</h3>"
    End If
  End Sub

  Protected Sub GVmensajesProfesores_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmensajesProfesores.RowDataBound
    'Los ESTATUS que se manejaran son Nuevo, Leido, Baja (los mensajes de baja ya no aparecen)
    If (DataBinder.Eval(e.Row.DataItem, "Estatus") = "Leido" Or DataBinder.Eval(e.Row.DataItem, "Estatus") = "Baja") And (e.Row.RowIndex > -1) Then
      e.Row.Cells(5).BackColor = Drawing.Color.GreenYellow
    End If

    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(1)
      LnkHeaderText.Text = Lang.FileSystem.Alumno.Salida.GV_FECHA.Item(Session("Usuario_Idioma"))

      LnkHeaderText = e.Row.Cells(2).Controls(1)
      LnkHeaderText.Text = Lang.FileSystem.Alumno.Salida.GV_ASUNTO.Item(Session("Usuario_Idioma"))

      LnkHeaderText = e.Row.Cells(3).Controls(1)
      LnkHeaderText.Text = Lang.FileSystem.Alumno.Salida.GVMENSAJESPROFESORES_PROFE_RECIBE.Item(Session("Usuario_Idioma"))

      LnkHeaderText = e.Row.Cells(4).Controls(1)
      LnkHeaderText.Text = Lang.Etiqueta.Asignatura.Item(Session("Usuario_Idioma"))

      LnkHeaderText = e.Row.Cells(5).Controls(1)
      LnkHeaderText.Text = Lang.FileSystem.Alumno.Salida.GV_ESTATUS.Item(Session("Usuario_Idioma"))
    End If

    If (DataBinder.Eval(e.Row.DataItem, "Estatus") = "Leido") And (e.Row.RowIndex > -1) Then
      e.Row.Cells(5).Text = Lang.FileSystem.Alumno.Salida.GV_ESTATUS_LEIDO.Item(Session("Usuario_Idioma"))
    ElseIf (DataBinder.Eval(e.Row.DataItem, "Estatus") = "Nuevo") And (e.Row.RowIndex > -1) Then
      e.Row.Cells(5).Text = Lang.FileSystem.Alumno.Salida.GV_ESTATUS_NUEVO.Item(Session("Usuario_Idioma"))
    ElseIf (DataBinder.Eval(e.Row.DataItem, "Estatus") = "Baja") And (e.Row.RowIndex > -1) Then
      e.Row.Cells(5).Text = Lang.FileSystem.Alumno.Salida.GV_ESTATUS_BAJA.Item(Session("Usuario_Idioma"))
    End If

    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Text = Lang.FileSystem.Alumno.Salida.GV_SELECT.Item(Session("Usuario_Idioma"))
    End If

    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVmensajesProfesores, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  Protected Sub GVmensajesProfesores_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmensajesProfesores.SelectedIndexChanged
    Session("Envio_IdComunicacion") = GVmensajesProfesores.SelectedDataKey.Values(0).ToString
    Session("Envio_IdComunicacionCA") = "0"
    Session("Envio_Previous") = "LeerMensajeSalidaA"
    Session("Envio_LoginProfesor") = GVmensajesProfesores.SelectedDataKey.Values(1).ToString ' necesario para accesar al adjunto
    Response.Redirect("~/alumno/comunicacion/mensaje/")
  End Sub

  Protected Sub GVmensajesProfesores_PreRender(sender As Object, e As EventArgs) Handles GVmensajesProfesores.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVmensajesProfesores.Rows.Count > 0 Then
      GVmensajesProfesores.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVmensajesProfesores_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVmensajesProfesores.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVmensajesProfesores.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

#End Region

#Region "GVmensajesCoordinadores"

  Protected Sub GVmensajesCoordinadores_DataBound(sender As Object, e As EventArgs) Handles GVmensajesCoordinadores.DataBound
    If GVmensajesCoordinadores.Rows.Count <> 0 Then
      GVmensajesCoordinadores.Caption = "<h3>(" & GVmensajesCoordinadores.Rows.Count & ") " & Lang.FileSystem.Alumno.Salida.GVMENSAJESCOORDINADORES_CAPTION.Item(Session("Usuario_Idioma")) & "</h3>"
    End If
  End Sub

  Protected Sub GVmensajesCoordinadores_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVmensajesCoordinadores.RowDataBound
    'Los ESTATUS que se manejaran son Nuevo, Leido, Baja (los mensajes de baja ya no aparecen)
    If (DataBinder.Eval(e.Row.DataItem, "Estatus") = "Leido" Or DataBinder.Eval(e.Row.DataItem, "Estatus") = "Baja") And (e.Row.RowIndex > -1) Then
      e.Row.Cells(5).BackColor = Drawing.Color.GreenYellow
    End If

    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(1)
      LnkHeaderText.Text = Lang.FileSystem.Alumno.Salida.GV_FECHA.Item(Session("Usuario_Idioma"))

      LnkHeaderText = e.Row.Cells(2).Controls(1)
      LnkHeaderText.Text = Lang.FileSystem.Alumno.Salida.GV_ASUNTO.Item(Session("Usuario_Idioma"))

      LnkHeaderText = e.Row.Cells(3).Controls(1)
      LnkHeaderText.Text = Lang.FileSystem.Alumno.Salida.GVMENSAJESCOORDINADORES_COORD_RECIBE.Item(Session("Usuario_Idioma"))

      LnkHeaderText = e.Row.Cells(4).Controls(1)
      LnkHeaderText.Text = Lang.Etiqueta.Plantel.Item(Session("Usuario_Idioma"))

      LnkHeaderText = e.Row.Cells(5).Controls(1)
      LnkHeaderText.Text = Lang.FileSystem.Alumno.Salida.GV_ESTATUS.Item(Session("Usuario_Idioma"))
    End If

    If (DataBinder.Eval(e.Row.DataItem, "Estatus") = "Leido") And (e.Row.RowIndex > -1) Then
      e.Row.Cells(5).Text = Lang.FileSystem.Alumno.Salida.GV_ESTATUS_LEIDO.Item(Session("Usuario_Idioma"))
    ElseIf (DataBinder.Eval(e.Row.DataItem, "Estatus") = "Nuevo") And (e.Row.RowIndex > -1) Then
      e.Row.Cells(5).Text = Lang.FileSystem.Alumno.Salida.GV_ESTATUS_NUEVO.Item(Session("Usuario_Idioma"))
    ElseIf (DataBinder.Eval(e.Row.DataItem, "Estatus") = "Baja") And (e.Row.RowIndex > -1) Then
      e.Row.Cells(5).Text = Lang.FileSystem.Alumno.Salida.GV_ESTATUS_BAJA.Item(Session("Usuario_Idioma"))
    End If

    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Text = Lang.FileSystem.Alumno.Salida.GV_SELECT.Item(Session("Usuario_Idioma"))
    End If

    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVmensajesCoordinadores, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  Protected Sub GVmensajesCoordinadores_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmensajesCoordinadores.SelectedIndexChanged
    Session("Envio_IdComunicacion") = "0"
    Session("Envio_IdComunicacionCA") = GVmensajesCoordinadores.SelectedDataKey.Values(0).ToString
    Session("Envio_Previous") = "LeerMensajeSalidaA"
    Session("Envio_LoginCoordinador") = GVmensajesCoordinadores.SelectedDataKey.Values(1).ToString ' necesario para accesar al adjunto
    Response.Redirect("~/alumno/comunicacion/mensaje/")
  End Sub

  Protected Sub GVmensajesCoordinadores_PreRender(sender As Object, e As EventArgs) Handles GVmensajesCoordinadores.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVmensajesCoordinadores.Rows.Count > 0 Then
      GVmensajesCoordinadores.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVmensajesCoordinadores_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVmensajesCoordinadores.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVmensajesCoordinadores.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

#End Region

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        If GVmensajesProfesores.Rows.Count = 0 AndAlso GVmensajesCoordinadores.Rows.Count = 0 Then
            msgInfo.show(Lang.FileSystem.Alumno.Salida.MSG_NO_HAY_MENSAJES.Item(Session("Usuario_Idioma")))
        Else
            msgInfo.hide()
        End If

        Dim r As GridViewRow
        For Each r In GVmensajesProfesores.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVmensajesProfesores, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        For Each r In GVmensajesCoordinadores.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVmensajesCoordinadores, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub
End Class
