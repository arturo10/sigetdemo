﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/alumno/comunicacion_alumno_5-1-0.master"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="alumno_comunicacion_salida_Default" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/PageTitle_v1.ascx" TagPrefix="uc1" TagName="PageTitle_v1" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder_comunicacion" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head_comunicacion" runat="Server">
    <%-- Aquí imprimo los estilos variables de ésta página específica --%>
    <asp:Literal ID="ltEstilos" runat="server" />

    <style type="text/css">
        .style23 {
            font-size: small;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            height: 34px;
        }

        .style33 {
            width: 10px;
            font-weight: normal;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor_comunicacion" runat="Server">

    <uc1:PageTitle_v1 runat="server" id="PageTitle_v1" />

    <table style="width: 100%;">
        <tr>
            <td>
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td>
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GVmensajesProfesores" runat="server" 
                    AllowSorting="True"
                    AutoGenerateColumns="False" 
                    Caption="<h3>[Mensajes para Profesores]</h3>" 

                    DataKeyNames="IdComunicacion,Login" 
                    DataSourceID="SDSmensajesProfesores" 
                    Width="882px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField SelectText="[Leer]" ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="Fecha" HeaderText="[Fecha]" SortExpression="Fecha" />
                        <asp:BoundField DataField="Asunto" HeaderText="[Asunto]"
                            SortExpression="Asunto" />
                        <asp:BoundField DataField="Profesor que Recibe" HeaderText="[Profesor Destinatario]"
                            ReadOnly="True" SortExpression="Profesor que Recibe" />
                        <asp:BoundField DataField="Asignatura" HeaderText="[Asignatura]"
                            SortExpression="Asignatura" />
                        <asp:BoundField DataField="Estatus" HeaderText="[Estatus]"
                            SortExpression="Estatus" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GVmensajesCoordinadores" runat="server" 
                    AllowSorting="True"
                    AutoGenerateColumns="False" 
                    Caption="<h3>[Mensajes para Coordinadores]</h3>" 

                    DataKeyNames="IdComunicacionCA,Login" 
                    DataSourceID="SDSmensajesCoordinadores" 
                    Width="882px"

                    CssClass="dataGrid_clear_selectable"
                    GridLines="None">
                    <Columns>
                        <asp:CommandField SelectText="[Leer]" ShowSelectButton="True" ItemStyle-CssClass="selectCell" />
                        <asp:BoundField DataField="Fecha" HeaderText="[Fecha]" SortExpression="Fecha" />
                        <asp:BoundField DataField="Asunto" HeaderText="[Asunto]"
                            SortExpression="Asunto" />
                        <asp:BoundField DataField="Coordinador que Recibe" HeaderText="[Coordinador Destinatario]"
                            ReadOnly="True" SortExpression="Coordinador que Recibe" />
                        <asp:BoundField DataField="Plantel" HeaderText="[Plantel]"
                            SortExpression="Plantel" />
                        <asp:BoundField DataField="Estatus" HeaderText="[Estatus]"
                            SortExpression="Estatus" />
                    </Columns>
                    <FooterStyle CssClass="footer" />
                    <PagerStyle CssClass="pager" />
                    <SelectedRowStyle CssClass="selected" />
                    <HeaderStyle CssClass="header" />
                    <AlternatingRowStyle CssClass="altrow" />
                </asp:GridView>
            </td>
        </tr>
    </table>

    <table>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSmensajesProfesores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="
select C.IdComunicacion, C.Fecha, C.Asunto, P.Nombre + ' ' + P.Apellidos as 'Profesor que Recibe', A.Descripcion as Asignatura, C.Estatus, U.Login
from Comunicacion C, Profesor P, Programacion Pr, Asignatura A, Usuario U
where C.IdAlumno = @IdAlumno and C.EstatusA &lt;&gt; 'Baja' and C.Sentido = 'R'
and Pr.IdProgramacion = C.IdProgramacion and P.IdProfesor = Pr.IdProfesor
and A.IdAsignatura = Pr.IdAsignatura and U.IdUsuario = P.IdUsuario
order by C.Fecha DESC">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdAlumno" SessionField="LeerMensajeSalidaA_IdAlumno" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSmensajesCoordinadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="
select CA.IdComunicacionCA, CA.Fecha, CA.Asunto, C.Nombre + ' ' + C.Apellidos as 'Coordinador que Recibe', CA.Estatus, U.Login, P.Descripcion As Plantel
from ComunicacionCA CA, Coordinador C, Usuario U, Plantel P, CoordinadorPlantel CP, Alumno A
where CA.IdAlumno = @IdAlumno and CA.EstatusA &lt;&gt; 'Baja' and CA.Sentido = 'R'
and U.IdUsuario = C.IdUsuario and CA.IdCoordinador = C.IdCoordinador 
and CP.IdPlantel = P.IdPlantel and C.IdCoordinador = CP.IdCoordinador 
and A.IdAlumno = CA.IdAlumno AND A.IdPlantel = P.IdPlantel 
order by CA.Fecha DESC">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdAlumno" SessionField="LeerMensajeSalidaA_IdAlumno" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

