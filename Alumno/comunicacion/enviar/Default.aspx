﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/alumno/comunicacion_alumno_5-1-0.master"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="alumno_comunicacion_enviar_Default"
    ValidateRequest="false" %>

<%-- Se necesita esta propiedad para acceder directamente a propiedades de la página maestra --%>
<%@ MasterType VirtualPath="~/alumno/comunicacion_alumno_5-1-0.master" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/PageTitle_v1.ascx" TagPrefix="uc1" TagName="PageTitle_v1" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder_comunicacion" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head_comunicacion" runat="Server">
    <%-- Aquí imprimo los estilos variables de ésta página específica --%>
    <asp:Literal ID="ltEstilos" runat="server" />

    <style type="text/css">
        .limitedSelect1 {
            width: 100px;
        }

        .style23 {
            height: 36px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
        }

        .style25 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            height: 33px;
        }

        .style26 {
            height: 33px;
            text-align: left;
        }

        .style33 {
            width: 10px;
            font-weight: normal;
            font-size: small;
        }
        .auto-style1 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            height: 33px;
            width: 120px;
        }
        .auto-style2 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            text-align: right;
            width: 120px;
        }
        .auto-style3 {
            width: 120px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor_comunicacion" runat="Server">

    <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1" />

    <table class="style10">
        <tr>
            <td class="auto-style1">
                <asp:Literal ID="lit_hintAlumno" runat="server" Text="[Alumno]" />
            </td>
            <td class="style26" colspan="3">
                <asp:Label ID="LblNombre" runat="server"
                    CssClass="LabelInfoDefault"></asp:Label></td>
            <td class="style26"></td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Literal ID="lit_hintCiclo" runat="server" Text="[Ciclo Escolar]" />
            </td>
            <td colspan="3" style="text-align: left">
                <asp:DropDownList ID="DDLcicloescolar" runat="server" AutoPostBack="True"
                    DataSourceID="SDSciclosescolares" DataTextField="Descripcion"
                    DataValueField="IdCicloEscolar" Height="22px" Width="351px">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">
                <asp:Literal ID="lit_hintDestino" runat="server">[Enviar mensaje a]</asp:Literal></td>
            <td colspan="3" style="text-align: left">
                <asp:DropDownList ID="ddlDestino" runat="server"
                    Width="150px"
                    AutoPostBack="True">
                    <asp:ListItem Value="P" Selected="True">[Profesores]</asp:ListItem>
                    <asp:ListItem Value="C">[Coordinadores]</asp:ListItem>
                </asp:DropDownList></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td colspan="3">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2" style="vertical-align: top; padding-top: 1em;"><asp:Literal ID="lit_hintDestinatario" runat="server">[Destinatario]</asp:Literal></td>
            <td colspan="4">
                <asp:Panel ID="pnlProfesores" runat="server" Visible="true">
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="4" style="text-align: right">
                                <table>
                                    <tr>
                                        <td>
                                            <table style="float: right;">
                                                <tr>
                                                    <td style="text-align: left;">
                                                        <asp:Literal ID="lt_search_profe_nombre" runat="server">[Nombre]</asp:Literal>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <asp:Literal ID="lt_search_profe_apellidos" runat="server">[Apellidos]</asp:Literal>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <asp:Literal ID="lt_search_profe_asignatura" runat="server">[Asignatura]</asp:Literal>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="tbBuscaProfesorNombre" runat="server" Width="140"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbBuscaProfesorApellidos" runat="server" Width="140"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbBuscaProfesorAsignatura" runat="server" Width="140"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="btnBuscaProfesor" runat="server" 
                                                            CssClass="defaultBtn btnThemeGrey btnThemeSlick" >
                                                            <asp:Image ID="Image1" runat="server"
                                                                ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/search_accounts.png"
                                                                CssClass="btnIcon" />
                                                            <asp:Literal ID="lt_btnBuscaProfes" runat="server">[Buscar]</asp:Literal>
                                                        </asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="btnLimpiaBusquedaProfesor" runat="server" 
                                                            CssClass="defaultBtn btnThemeGrey btnThemeSlick" >
                                                            <asp:Image ID="Image5" runat="server"
                                                                ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/draw_eraser.png"
                                                                CssClass="btnIcon" />
                                                            <asp:Literal ID="lt_btnLimpiabusquedaProfes" runat="server">[Limpiar Búsqueda]</asp:Literal>
                                                        </asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td colspan="5">
                                                        <uc1:msgInfo runat="server" ID="msgInfoProfesores" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:GridView ID="GVprofesores" runat="server"
                                                            AllowSorting="True"
                                                            AutoGenerateColumns="False"
                                                            DataSourceID="SDSprofesores"
                                                            DataKeyNames="IdProfesor,IdProgramacion"
                                                            CssClass="dataGrid_clear_selectable"
                                                            GridLines="None"
                                                            Width="750px">
                                                            <Columns>
                                                                <asp:CommandField SelectText="[Seleccionar]" ShowSelectButton="True" ItemStyle-CssClass="selectCell limitedSelect1" />
                                                                <asp:BoundField DataField="Profesor" HeaderText="Profesor" ReadOnly="True"
                                                                    SortExpression="Profesor" />
                                                                <asp:BoundField DataField="Asignatura" HeaderText="Asignatura"
                                                                    SortExpression="Asignatura" />
                                                            </Columns>
                                                            <FooterStyle CssClass="footer" />
                                                            <PagerStyle CssClass="pager" />
                                                            <SelectedRowStyle CssClass="selected" />
                                                            <HeaderStyle CssClass="header" />
                                                            <AlternatingRowStyle CssClass="altrow" />
                                                        </asp:GridView>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlCoordinadores" runat="server" Visible="false">
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="4" style="text-align: right">
                                <table>
                                    <tr>
                                        <td>
                                            <table style="float: right;">
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td style="text-align: left;">
                                                        <asp:Literal ID="lt_search_coord_nombre" runat="server">[Nombre]</asp:Literal>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <asp:Literal ID="lt_search_coord_apellidos" runat="server">[Apellidos]</asp:Literal>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/Resources/imagenes/btnIcons/woocons/Search.png" Height="24" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbBuscaCoordinadorNombre" runat="server" Width="140"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="tbBuscaCoordinadorApellidos" runat="server" Width="140"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="btnBuscaCoordinador" runat="server" 
                                                            CssClass="defaultBtn btnThemeGrey btnThemeSlick" >
                                                            <asp:Image ID="Image6" runat="server"
                                                                ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/search_accounts.png"
                                                                CssClass="btnIcon" />
                                                            <asp:Literal ID="lt_buscaCoords" runat="server">[Buscar]</asp:Literal>
                                                        </asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="btnLimpiaBusquedaCoordinador" runat="server" 
                                                            CssClass="defaultBtn btnThemeGrey btnThemeSlick" >
                                                            <asp:Image ID="Image7" runat="server"
                                                                ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/draw_eraser.png"
                                                                CssClass="btnIcon" />
                                                            <asp:Literal ID="lt_limpiaBusquedaCoords" runat="server">[Limpiar Búsqueda]</asp:Literal>
                                                        </asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td colspan="5">
                                                        <uc1:msgInfo runat="server" ID="MsgInfoCoordinadores" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <asp:GridView ID="GVcoordinadores" runat="server"
                                                            AllowSorting="True"
                                                            AutoGenerateColumns="False"
                                                            DataSourceID="SDScoordinadores"
                                                            DataKeyNames="IdCoordinador"
                                                            CssClass="dataGrid_clear_selectable"
                                                            GridLines="None"
                                                            Width="600px">
                                                            <Columns>
                                                                <asp:CommandField SelectText="[Seleccionar]" ShowSelectButton="True" ItemStyle-CssClass="selectCell limitedSelect1" />
                                                                <asp:BoundField DataField="Coordinador" HeaderText="Coordinador" ReadOnly="True"
                                                                    SortExpression="Coordinador" />
                                                            </Columns>
                                                            <FooterStyle CssClass="footer" />
                                                            <PagerStyle CssClass="pager" />
                                                            <SelectedRowStyle CssClass="selected" />
                                                            <HeaderStyle CssClass="header" />
                                                            <AlternatingRowStyle CssClass="altrow" />
                                                        </asp:GridView>
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="5">&nbsp; </td>
        </tr>
        <tr>
            <td class="auto-style2"><asp:Literal ID="lt_hintAsunto" runat="server">[Asunto]</asp:Literal></td>
            <td colspan="3">
                <asp:TextBox ID="TBasunto" runat="server" MaxLength="100" Width="750px"></asp:TextBox></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2" style="vertical-align: top;"><asp:Literal ID="lt_hintContenido" runat="server">[Contenido]</asp:Literal></td>
            <td colspan="3">
                <asp:TextBox ID="TBmensaje" runat="server"
                    ClientIDMode="Static"
                    TextMode="MultiLine"
                    CssClass="tinymce"></asp:TextBox></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="5">&nbsp; </td>
        </tr>
        <tr>
            <td class="auto-style2"><asp:Literal ID="lt_hintAdjuntar" runat="server">[Adjuntar archivo<br />(solo uno)]</asp:Literal></td>
            <td>
                <asp:FileUpload ID="FUadjunto" runat="server" />
            </td>
            <td style="text-align: left">
                <asp:LinkButton ID="BtnAdjuntar" runat="server" 
                    CssClass="defaultBtn btnThemeGrey btnThemeSlick" >
                    <asp:Image ID="Image9" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/attach.png"
                        CssClass="btnIcon" />
                    <asp:Literal ID="lt_btnAdjuntar" runat="server">Adjuntar</asp:Literal>
                </asp:LinkButton>
                &nbsp;
                <asp:LinkButton ID="BtnQuitar" runat="server" 
                    CssClass="defaultBtn btnThemeGrey btnThemeSlick" >
                    <asp:Image ID="Image8" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/cancel.png"
                        CssClass="btnIcon" />
                    <asp:Literal ID="lt_btnQuitar" runat="server">Quitar</asp:Literal>
                </asp:LinkButton>
            </td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3"></td>
            <td colspan="4" style="text-align: left">
                <asp:LinkButton ID="lbAdjunto" runat="server" CssClass="LabelInfoDefault"></asp:LinkButton></td>
        </tr>
        <tr>
            <td colspan="5">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="5">&nbsp; </td>
        </tr>
        <tr>
            <td colspan="5" style="text-align: center;">
                <asp:LinkButton ID="BtnEnviar" runat="server" 
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium">
                    <asp:Image ID="Image3" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/woocons/Button White Check.png"
                        CssClass="btnIcon"
                        height="24" />
                    <asp:Literal ID="lt_btnEnviar" runat="server" Text="Enviar Mensaje" />
                </asp:LinkButton>
                &nbsp;
                <asp:LinkButton ID="BtnLimpiar" runat="server" 
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" >
                    <asp:Image ID="Image4" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/draw_eraser.png"
                        CssClass="btnIcon"
                        height="24" />
                    <asp:Literal ID="lt_btnLimpiar" runat="server" Text="Limpiar" />
                </asp:LinkButton></td></tr></table><table class="hiddenFields">
        <tr>
            <td>
                <%-- Los hidden fields de búsqueda se deben inicializar con la wild card 
                    para que funcionen las consultas sin buscar --%>
                <asp:HiddenField ID="hfBusquedaProfesores_isActive" runat="server" />

            </td>
            <td>
                <%-- Los hidden fields de búsqueda se deben inicializar con la wild card 
                    para que funcionen las consultas sin buscar --%>
                <asp:HiddenField ID="hfBusquedaCoordinadores_isActive" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfAdjuntoFisico" runat="server" />
            </td>
            <td>
                &nbsp;</td></tr><tr>
            <td>
                <asp:HiddenField ID="hfBuscaProfNombre" runat="server" Value="%" />

            </td>
            <td><asp:HiddenField ID="hfBuscaProfApellidos" runat="server" Value="%" /></td>
            <td><asp:HiddenField ID="hfBuscaProfAsignatura" runat="server" Value="%" /></td>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hfBuscaCoordNombre" runat="server" Value="%" />
            </td>
            <td>
                <asp:HiddenField ID="hfBuscaCoordApellidos" runat="server" Value="%" />
            </td>
            <td>
                <asp:HiddenField ID="hfCarpetaAdjunto" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfIsReenvio" runat="server" Value="False" />
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>
                <asp:SqlDataSource ID="SDScomunicacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Comunicacion]"></asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSciclosescolares" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [CicloEscolar] 
WHERE Estatus = 'Activo'
ORDER BY [FechaInicio]"></asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDSprofesores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select P.IdProfesor, rtrim(P.Apellidos) + ', ' + rtrim(P.Nombre) as Profesor, A.Descripcion as Asignatura, Pr.IdProgramacion
from Profesor P, Asignatura A, Programacion Pr
where Pr.IdGrupo = @Idgrupo and P.IdProfesor = Pr.IdProfesor and 
Pr.IdCicloEscolar = @IdCicloescolar and A.IdAsignatura = Pr.IdAsignatura
and P.Estatus = 'Activo' 
AND P.Apellidos LIKE @BuscaApellidos AND P.Nombre LIKE @BuscaNombre AND A.Descripcion LIKE @BuscaAsignatura 
order by A.Descripcion, P.Apellidos, P.Nombre">
                    <SelectParameters>
                        <asp:SessionParameter Name="Idgrupo" SessionField="EnviarMensajeA_IdGrupo" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloescolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter Name="BuscaNombre" ControlID="hfBuscaProfNombre" PropertyName="Value" />
                        <asp:ControlParameter Name="BuscaApellidos" ControlID="hfBuscaProfApellidos" PropertyName="Value" />
                        <asp:ControlParameter Name="BuscaAsignatura" ControlID="hfBuscaProfAsignatura" PropertyName="Value" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDScoordinadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="
SELECT C.IdCoordinador, rtrim(C.Apellidos) + ', ' + rtrim(C.Nombre) as Coordinador
 FROM Coordinador C, Grupo G, Plantel P, CoordinadorPlantel CP
 WHERE C.IdCoordinador = CP.IdCoordinador
  AND CP.IdPlantel = P.IdPlantel
  AND P.IdPlantel = G.IdPlantel
  AND G.IdGrupo = @IdGrupo
  AND G.IdCicloEscolar = @IdCicloEscolar
  AND C.Apellidos LIKE @BuscaApellidos AND C.Nombre LIKE @BuscaNombre 
order by C.Apellidos, C.Nombre">
                    <SelectParameters>
                        <asp:SessionParameter Name="Idgrupo" SessionField="EnviarMensajeA_IdGrupo" />
                        <asp:ControlParameter ControlID="DDLcicloescolar" Name="IdCicloescolar"
                            PropertyName="SelectedValue" />
                        <asp:ControlParameter Name="BuscaNombre" ControlID="hfBuscaCoordNombre" PropertyName="Value" />
                        <asp:ControlParameter Name="BuscaApellidos" ControlID="hfBuscaCoordApellidos" PropertyName="Value" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="SDScomunicacionCA" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [ComunicacionCA]"></asp:SqlDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

