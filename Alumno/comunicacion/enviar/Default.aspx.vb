﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO

Partial Class alumno_comunicacion_enviar_Default
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            UserInterface.Include.HtmlEditor(Master.ltHeader, 750, 200, 12, Session("Usuario_Idioma").ToString())

            aplicaLenguaje()

            initPageData()

            OutputCss()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = Lang.FileSystem.Alumno.Enviar.LT_TITULO.Item(Session("Usuario_Idioma"))

    PageTitle_v1.show(Lang.FileSystem.Alumno.Enviar.LT_TITULO_PAGINA.Item(Session("Usuario_Idioma")),
                      Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/email_go.png")

    lit_hintAlumno.Text = Lang.FileSystem.Alumno.Enviar.LT_HINT_ALUMNO.Item(Session("Usuario_Idioma"))
    lit_hintCiclo.Text = Lang.FileSystem.Alumno.Enviar.LT_HINT_CICLO.Item(Session("Usuario_Idioma"))
    lit_hintDestino.Text = Lang.FileSystem.Alumno.Enviar.LT_HINT_DESTINO.Item(Session("Usuario_Idioma"))
    ddlDestino.Items(0).Text = Lang.FileSystem.Alumno.Enviar.DDLDESTINO_PROFESORES.Item(Session("Usuario_Idioma"))
    ddlDestino.Items(1).Text = Lang.FileSystem.Alumno.Enviar.DDLDESTINO_COORDINADORES.Item(Session("Usuario_Idioma"))
    lit_hintDestinatario.Text = Lang.FileSystem.Alumno.Enviar.LT_HINT_DESTINATARIO.Item(Session("Usuario_Idioma"))
    lt_search_profe_nombre.Text = Lang.FileSystem.Alumno.Enviar.LT_SEARCH_PROFE_NOMBRE.Item(Session("Usuario_Idioma"))
    lt_search_profe_apellidos.Text = Lang.FileSystem.Alumno.Enviar.LT_SEARCH_PROFE_APELLIDOS.Item(Session("Usuario_Idioma"))
    lt_search_profe_asignatura.Text = Lang.FileSystem.Alumno.Enviar.LT_SEARCH_PROFE_ASIGNATURA.Item(Session("Usuario_Idioma"))
    lt_btnBuscaProfes.Text = Lang.FileSystem.Alumno.Enviar.BTN_BUSCA_PROFESOR.Item(Session("Usuario_Idioma"))
    lt_btnLimpiabusquedaProfes.Text = Lang.FileSystem.Alumno.Enviar.BT_LIMPIA_BUSQUEDA_PROFESOR.Item(Session("Usuario_Idioma"))
    lt_search_coord_nombre.Text = Lang.FileSystem.Alumno.Enviar.LT_SEARCH_COORD_NOMBRE.Item(Session("Usuario_Idioma"))
    lt_search_coord_apellidos.Text = Lang.FileSystem.Alumno.Enviar.LT_SEARCH_COORD_APELLIDOS.Item(Session("Usuario_Idioma"))
    lt_buscaCoords.Text = Lang.FileSystem.Alumno.Enviar.BTN_BUSCA_COORDINADOR.Item(Session("Usuario_Idioma"))
    lt_limpiaBusquedaCoords.Text = Lang.FileSystem.Alumno.Enviar.BTN_LIMPIA_BUSQUEDA_COORDINADOR.Item(Session("Usuario_Idioma"))
    lt_hintAsunto.Text = Lang.FileSystem.Alumno.Enviar.LT_HINT_ASUNTO.Item(Session("Usuario_Idioma"))
    lt_hintContenido.Text = Lang.FileSystem.Alumno.Enviar.LT_HINT_CONTENIDO.Item(Session("Usuario_Idioma"))
    lt_hintAdjuntar.Text = Lang.FileSystem.Alumno.Enviar.LT_HINT_ADJUNTAR.Item(Session("Usuario_Idioma"))
    lt_btnAdjuntar.Text = Lang.FileSystem.Alumno.Enviar.BTN_ADJUNTAR.Item(Session("Usuario_Idioma"))
    lt_btnQuitar.Text = Lang.FileSystem.Alumno.Enviar.BTN_QUITAR.Item(Session("Usuario_Idioma"))
    lt_btnEnviar.Text = Lang.FileSystem.Alumno.Enviar.BTN_ENVIAR.Item(Session("Usuario_Idioma"))
    lt_btnLimpiar.Text = Lang.FileSystem.Alumno.Enviar.BTN_LIMPIAR.Item(Session("Usuario_Idioma"))
  End Sub

  ' inicializo los datos que necesito para los controles de la página
  Protected Sub initPageData()
    ' necesito obtener los datos del alumno para usarse al enviar mensajes
    Try
      Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
        conn.Open()

        Dim cmd As SqlCommand = New SqlCommand("SELECT G.IdGrado, A.IdPlantel, A.IdGrupo, IdAlumno, A.Nombre, A.ApePaterno, A.ApeMaterno, A.Matricula, A.Estatus " &
                                               "FROM Usuario U, Alumno A, Grupo G " &
                                               "WHERE U.Login = @Login and A.IdUsuario = U.IdUsuario and G.IdGrupo = A.IdGrupo", conn)
        cmd.Parameters.AddWithValue("@Login", Trim(User.Identity.Name))

        Dim results As SqlDataReader = cmd.ExecuteReader()
        results.Read()
        LblNombre.Text = results.Item("Nombre") + " " + results.Item("ApePaterno") + " " + results.Item("ApeMaterno")
        Session("EnviarMensajeA_NombreAlumno") = LblNombre.Text
        Session("EnviarMensajeA_IdAlumno") = results.Item("IdAlumno").ToString()
        Session("EnviarMensajeA_IdGrupo") = results.Item("IdGrupo").ToString()
        hfCarpetaAdjunto.Value = User.Identity.Name.Trim()

        results.Close()
        cmd.Dispose()
        conn.Close()
      End Using
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try

    ' si viene de mensaje y presionó reenviar, paso la información del mensaje a reenviar
    ' esto debe estar después de initpagedata() puesto que sobreescribo la carpeta de adjuntos si es reenvio
    If Not IsNothing(Session("Reenvio")) Then
      ddlDestino.SelectedValue = Session("Reenvio") ' reenvio tiene que tener P o C
      actualizaPaneles() ' actualizo páneles de acuerdo al destino
      hfIsReenvio.Value = "True"

      Dim asunto As String
      asunto = "RENV: " + Session("Envio_Asunto") + " [" + Session("Envio_Fecha") + "]"
      TBasunto.Text = Mid(asunto, 1, 100)

      TBmensaje.Text = Session("Envio_Contenido")
      lbAdjunto.Text = Session("Envio_AdjuntoNombre")
      hfAdjuntoFisico.Value = Session("Envio_AdjuntoFisico")
      hfCarpetaAdjunto.Value = Session("Envio_Carpeta")

      BtnAdjuntar.Visible = False
      FUadjunto.Visible = False
      BtnQuitar.Visible = False

      Session.Remove("Reenvio")
      Session.Remove("Envio_Asunto")
      Session.Remove("Envio_Fecha")
      Session.Remove("Envio_Contenido")
      Session.Remove("Envio_AdjuntoNombre")
      Session.Remove("Envio_AdjuntoFisico")
      Session.Remove("Envio_Carpeta")
    End If

    ' inicializo los hidden fields de búsqueda para que las consultas funcionen bien sin búsquedas
    hfBuscaProfNombre.Value = "%"
    hfBuscaProfApellidos.Value = "%"
    hfBuscaProfAsignatura.Value = "%"
    hfBuscaCoordNombre.Value = "%"
    hfBuscaCoordApellidos.Value = "%"
  End Sub

  ' imprimo los estilos variables de ésta página
  Protected Sub OutputCss()
    Dim s As StringBuilder = New StringBuilder()
    s.Append("<style type='text/css'>")
    s.Append("  #menu .current_area_comunicacion a {") ' esta clase css debe corresponder a la clase del boton a resaltar
    s.Append("    border-bottom: 5px solid " & Config.Color.MenuBar_SelectedOption_BorderColor & ";")
    s.Append("  }")
    s.Append("  #menusCol .current_page_enviar {")
    s.Append("    background: " & Config.Color.MenuPendientes_Subtitle & ";")
    s.Append("  }")
    s.Append("</style>")
    ltEstilos.Text += s.ToString()
  End Sub

#End Region

#Region "adjuntos"

  Protected Sub BtnAdjuntar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdjuntar.Click
    Try  'Cuando se adjunta el archivo, se borra el nombre del FileUpload
      If FUadjunto.HasFile Then
        Dim ruta As String = Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value & "\" 'Esta ruta contempla un directorio como alumno nombrado como su login

        'Verifico si ya existe el directorio del alumno, si no, lo creo
        If Not (Directory.Exists(ruta)) Then
          Dim directorio As DirectoryInfo = Directory.CreateDirectory(ruta)
        End If

        'Subo archivo
        Dim now As String = Date.Now.Year.ToString & "_" & Date.Now.Month.ToString & "_" & Date.Now.Day.ToString & "_" & Date.Now.Hour.ToString & "_" & Date.Now.Minute.ToString & "_" & Date.Now.Second.ToString & "_" & Date.Now.Millisecond.ToString
        hfAdjuntoFisico.Value = now & FUadjunto.FileName
        FUadjunto.SaveAs(ruta & hfAdjuntoFisico.Value)
        lbAdjunto.Text = FUadjunto.FileName

        msgError.hide()
        msgSuccess.show(Lang.FileSystem.Alumno.Enviar.MSG_ADJUNTADO.Item(Session("Usuario_Idioma")))
      Else
        msgError.show(Lang.FileSystem.Alumno.Enviar.ERR_FALTA_ARCHIVO.Item(Session("Usuario_Idioma")))
        msgSuccess.hide()
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      msgSuccess.hide()
    End Try
  End Sub

  Protected Sub lbAdjunto_Click(sender As Object, e As EventArgs) Handles lbAdjunto.Click
    Dim file As System.IO.FileInfo =
        New System.IO.FileInfo(Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value + "\" & hfAdjuntoFisico.Value) '-- if the file exists on the server

    If file.Exists Then 'set appropriate headers
      Response.Clear()
      Response.AddHeader("Content-Disposition", "attachment; filename=""" & lbAdjunto.Text & """")
      Response.AddHeader("Content-Length", file.Length.ToString())
      Response.ContentType = "application/octet-stream"
      Response.WriteFile(file.FullName)
      'Response.End()
    Else
      msgError.show(Lang.FileSystem.Alumno.Enviar.ERR_ADJUNTO_NO_DISPONIBLE.Item(Session("Usuario_Idioma")))
    End If 'nothing in the URL as HTTP GET
  End Sub

  Protected Sub BtnQuitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnQuitar.Click
    quitarAdjunto()
  End Sub

  Protected Sub quitarAdjunto()
    Try
      If hfAdjuntoFisico.Value.Length > 0 Then
        Dim ruta As String = Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value & "\" 'Esta ruta contempla un directorio como alumno nombrado como su login

        'Si NO es reenvio, borro el archivo que estaba anteriormente porque se pondrá uno nuevo
        If hfIsReenvio.Value = "False" Then
          Dim adjunto As FileInfo = New FileInfo(ruta & hfAdjuntoFisico.Value)
          adjunto.Delete()
        End If

        lbAdjunto.Text = ""
        hfAdjuntoFisico.Value = ""
        msgError.hide()
        msgSuccess.show(Lang.FileSystem.Alumno.Enviar.MSG_DESADJUNTADO.Item(Session("Usuario_Idioma")))
      Else
        msgError.show(Lang.FileSystem.Alumno.Enviar.ERR_NO_HA_ADJUNTADO.Item(Session("Usuario_Idioma")))
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgSuccess.hide()
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

#End Region

#Region "enviar"

  Protected Sub BtnEnviar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEnviar.Click
    If (Trim(TBasunto.Text).Length > 0) And (Trim(TBmensaje.Text).Length) > 0 Then
      If ddlDestino.SelectedItem.Text = Lang.FileSystem.Alumno.Enviar.DDLDESTINO_PROFESORES.Item(Session("Usuario_Idioma")) Then
        ' _____________________________________________________________
        ' correo para profesores
        If Not GVprofesores.SelectedIndex > -1 Or
            Not GVprofesores.SelectedIndex < GVprofesores.Rows.Count Then
          msgError.show(Lang.FileSystem.Alumno.Enviar.ERR_FALTA_PROFESOR_DESTINO.Item(Session("Usuario_Idioma")))
        Else
          enviarMensajeProfesor()
        End If
      Else
        ' _____________________________________________________________
        ' correo para coordinadores
        If Not GVcoordinadores.SelectedIndex > -1 Or
            Not GVcoordinadores.SelectedIndex < GVcoordinadores.Rows.Count Then
          msgError.show(Lang.FileSystem.Alumno.Enviar.ERR_FALTA_COORDINADOR_DESTINO.Item(Session("Usuario_Idioma")))
        Else
          enviarMensajeCoordinador()
        End If
      End If
    Else
      msgError.show(Lang.FileSystem.Alumno.Enviar.ERR_FALTA_CONTENIDO.Item(Session("Usuario_Idioma")))
      msgSuccess.hide()
    End If
  End Sub

  Protected Sub enviarMensajeProfesor()
    Try
      ' busco el Email del profesor para mandarle correo
      Dim Email As String
      Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
        conn.Open()

        Dim cmd As SqlCommand = New SqlCommand("SELECT Email FROM Profesor WHERE IdProfesor = @IdProfesor", conn)
        cmd.Parameters.AddWithValue("@IdProfesor", GVprofesores.SelectedDataKey.Values(0).ToString)

        Dim results As SqlDataReader = cmd.ExecuteReader()
        results.Read()
        If IsDBNull(results.Item("Email")) Then 'Si intento asignar un valor Nulo a una variable, marca error
          Email = ""
        Else
          Email = results.Item("Email")
        End If

        results.Close()
        cmd.Dispose()
        conn.Close()
      End Using

      'Inserto el mensaje [Sentido (E=Envia, R=Recibe - desde el punto de vista del profesor)][Estatus (Leido, Nuevo, Baja)]
      SDScomunicacion.InsertCommand = "SET dateformat dmy; INSERT INTO Comunicacion(IdProgramacion,Sentido,IdAlumno,Asunto,Contenido,Fecha,ArchivoAdjunto,ArchivoAdjuntoFisico,CarpetaAdjunto,EmailEnviado,Estatus, EstatusA)" &
          " VALUES(@IdProgramacion, 'R', @IdAlumno, @Asunto, @Contenido, getdate(), @NombreAdjunto, @RutaAdjunto, @CarpetaAdjunto, @EmailEnviado, 'Nuevo', 'Nuevo')"
      SDScomunicacion.InsertParameters.Add("IdProgramacion", GVprofesores.SelectedDataKey.Values(1).ToString)
      SDScomunicacion.InsertParameters.Add("IdAlumno", Session("EnviarMensajeA_IdAlumno").ToString)
      SDScomunicacion.InsertParameters.Add("Asunto", TBasunto.Text)
      SDScomunicacion.InsertParameters.Add("Contenido", TBmensaje.Text)
      SDScomunicacion.InsertParameters.Add("NombreAdjunto", lbAdjunto.Text)
      SDScomunicacion.InsertParameters.Add("RutaAdjunto", hfAdjuntoFisico.Value)
      SDScomunicacion.InsertParameters.Add("CarpetaAdjunto", hfCarpetaAdjunto.Value)
      SDScomunicacion.InsertParameters.Add("EmailEnviado", Email)
      SDScomunicacion.Insert()

      'Si tenía un Email registrado el Coordinador, le envía una notificación
      If Trim(Email).Length > 1 Then
        If Config.Global.MAIL_ACTIVO Then
          Utils.Correo.EnviaCorreo(Email.Trim(),
                                   Config.Global.NOMBRE_FILESYSTEM & Lang.FileSystem.Alumno.Enviar.MSG_NOTIFICACION_ASUNTO.Item(Session("Usuario_Idioma")),
                                   Lang.FileSystem.Alumno.Enviar.MSG_NOTIFICACION_CUERPO.Item(Session("Usuario_Idioma")) & " " & Session("EnviarMensajeA_NombreAlumno").ToString,
                                   False)
        End If
      End If

      msgSuccess.show(Lang.FileSystem.Alumno.Enviar.MSG_ENVIADO_PROFESOR.Item(Session("Usuario_Idioma")) & " " & GVprofesores.SelectedRow.Cells(1).Text)
      msgError.hide()
      GVprofesores.SelectedIndex = -1 ' si había una fila seleccionada la quito
      TBasunto.Text = ""
      TBmensaje.Text = ""
      lbAdjunto.Text = ""
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      msgSuccess.hide()
    End Try
  End Sub

  Protected Sub enviarMensajeCoordinador()
    Try
      ' busco el Email del profesor para mandarle correo
      Dim Email As String
      Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
        conn.Open()

        Dim cmd As SqlCommand = New SqlCommand("SELECT Email FROM Coordinador WHERE IdCoordinador = @IdCoordinador", conn)
        cmd.Parameters.AddWithValue("@IdCoordinador", GVcoordinadores.SelectedDataKey.Values(0).ToString)

        Dim results As SqlDataReader = cmd.ExecuteReader()
        results.Read()
        If IsDBNull(results.Item("Email")) Then 'Si intento asignar un valor Nulo a una variable, marca error
          Email = ""
        Else
          Email = results.Item("Email")
        End If

        results.Close()
        cmd.Dispose()
        conn.Close()
      End Using

      'Inserto el mensaje [Sentido (E=Envia, R=Recibe - desde el punto de vista del profesor)][Estatus (Leido, Nuevo, Baja)]
      SDScomunicacionCA.InsertCommand = "SET dateformat dmy; INSERT INTO ComunicacionCA(IdCoordinador,IdAlumno,Sentido,Asunto,Contenido,Fecha,ArchivoAdjunto,ArchivoAdjuntoFisico,CarpetaAdjunto,EmailEnviado,Estatus, EstatusA)" &
                          " VALUES(@IdCoordinador, @IdAlumno, 'R', @Asunto, @Contenido, getdate(), @NombreAdjunto, @RutaAdjunto, @CarpetaAdjunto, @EmailEnviado, 'Nuevo', 'Nuevo')"
      SDScomunicacionCA.InsertParameters.Add("IdCoordinador", GVcoordinadores.SelectedDataKey.Values(0).ToString)
      SDScomunicacionCA.InsertParameters.Add("IdAlumno", Session("EnviarMensajeA_IdAlumno").ToString)
      SDScomunicacionCA.InsertParameters.Add("Asunto", TBasunto.Text)
      SDScomunicacionCA.InsertParameters.Add("Contenido", TBmensaje.Text)
      SDScomunicacionCA.InsertParameters.Add("NombreAdjunto", lbAdjunto.Text)
      SDScomunicacionCA.InsertParameters.Add("RutaAdjunto", hfAdjuntoFisico.Value)
      SDScomunicacionCA.InsertParameters.Add("CarpetaAdjunto", hfCarpetaAdjunto.Value)
      SDScomunicacionCA.InsertParameters.Add("EmailEnviado", Email)
      SDScomunicacionCA.Insert()

      'Si tenía un Email registrado el Profesor, le envía una notificación
      If Trim(Email).Length > 1 Then
        If Config.Global.MAIL_ACTIVO Then
          Utils.Correo.EnviaCorreo(Trim(Email),
                                   Config.Global.NOMBRE_FILESYSTEM & Lang.FileSystem.Alumno.Enviar.MSG_NOTIFICACION_ASUNTO.Item(Session("Usuario_Idioma")),
                                   Lang.FileSystem.Alumno.Enviar.MSG_NOTIFICACION_CUERPO.Item(Session("Usuario_Idioma")) & " " & Session("EnviarMensajeA_NombreAlumno").ToString,
                                   False)
        End If
      End If

      msgSuccess.show(Lang.FileSystem.Alumno.Enviar.MSG_ENVIADO_COORDINADOR.Item(Session("Usuario_Idioma")) & " " & GVcoordinadores.SelectedRow.Cells(1).Text)
      msgError.hide()
      GVcoordinadores.SelectedIndex = -1 ' si había una fila seleccionada la quito
      TBasunto.Text = ""
      TBmensaje.Text = ""
      lbAdjunto.Text = ""
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      msgSuccess.hide()
    End Try
  End Sub

#End Region

  Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
    GVprofesores.DataBind()
  End Sub

  Protected Sub BtnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLimpiar.Click
    TBasunto.Text = ""
    TBmensaje.Text = ""
    ' al limpiar se limpia el adjunto, y debe eliminarse el archivo
    quitarAdjunto()
    ' se ocultan los mensajes al final puesto que al quitar el archivo pueden mostrarse
    msgSuccess.hide()
    msgError.hide()
  End Sub

#Region "gv_profesores"

  Protected Sub GVprofesores_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVprofesores.DataBound
    If hfBusquedaProfesores_isActive.Value.Length > 0 Then
      msgInfoProfesores.show(GVprofesores.Rows.Count & " " & Lang.FileSystem.Alumno.Enviar.MSG_PROFESORES_ENCONTRADOS.Item(Session("Usuario_Idioma")))
    Else
      If (GVprofesores.Rows.Count = 0) Then
        msgInfoProfesores.show(Lang.FileSystem.Alumno.Enviar.MSG_NO_PROFESORES_ASIGNADOS.Item(Session("Usuario_Idioma")))
      End If
    End If
  End Sub

  Protected Sub GVprofesores_PreRender(sender As Object, e As EventArgs) Handles GVprofesores.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVprofesores.Rows.Count > 0 Then
      GVprofesores.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVprofesores_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVprofesores.RowDataBound
    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(1)
      LnkHeaderText.Text = Lang.FileSystem.Alumno.Enviar.GVPROFESORES_HEADER_PROFESOR.Item(Session("Usuario_Idioma"))

      LnkHeaderText = e.Row.Cells(2).Controls(1)
      LnkHeaderText.Text = Lang.FileSystem.Alumno.Enviar.GVPROFESORES_HEADER_ASIGNATURA.Item(Session("Usuario_Idioma"))

    ElseIf e.Row.RowType = DataControlRowType.DataRow Then
      Dim LnkSelectText As LinkButton = e.Row.Cells(0).Controls(0)
      LnkSelectText.Text = Lang.FileSystem.Alumno.Enviar.GV_SELECCIONAR.Item(Session("Usuario_Idioma"))
    End If

    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVprofesores, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  Protected Sub GVprofesores_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVprofesores.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVprofesores.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

#End Region

#Region "gv_coordinadores"

  Protected Sub GVcoordinadores_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVcoordinadores.DataBound
    If hfBusquedaCoordinadores_isActive.Value.Length > 0 Then
      MsgInfoCoordinadores.show(GVcoordinadores.Rows.Count & " " & Lang.FileSystem.Alumno.Enviar.MSG_COORDINADORES_ENCONTRADOS.Item(Session("Usuario_Idioma")))
    Else
      If (GVcoordinadores.Rows.Count = 0) Then
        MsgInfoCoordinadores.show(Lang.FileSystem.Alumno.Enviar.MSG_NO_COORDINADORES_ASIGNADOS.Item(Session("Usuario_Idioma")))
      End If
    End If
  End Sub

  Protected Sub GVcoordinadores_PreRender(sender As Object, e As EventArgs) Handles GVcoordinadores.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GVcoordinadores.Rows.Count > 0 Then
      GVcoordinadores.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GVcoordinadores_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVcoordinadores.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVcoordinadores.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GVcoordinadores_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVcoordinadores.RowDataBound
    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(1)
      LnkHeaderText.Text = Lang.FileSystem.Alumno.Enviar.GVCOORDINADORES_HEADER_COORDINADOR.Item(Session("Usuario_Idioma"))

    ElseIf e.Row.RowType = DataControlRowType.DataRow Then
      Dim LnkSelectText As LinkButton = e.Row.Cells(0).Controls(0)
      LnkSelectText.Text = Lang.FileSystem.Alumno.Enviar.GV_SELECCIONAR.Item(Session("Usuario_Idioma"))
    End If

    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVcoordinadores, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

#End Region

    Protected Sub ddlDestino_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDestino.SelectedIndexChanged
        actualizaPaneles()
    End Sub

    Protected Sub actualizaPaneles()
        If ddlDestino.SelectedItem.Value = "P" Then
            GVprofesores.SelectedIndex = -1 ' si había una fila seleccionada la quito
            pnlProfesores.Visible = True
            pnlCoordinadores.Visible = False
        Else
            GVcoordinadores.SelectedIndex = -1 ' si había una fila seleccionada la quito
            pnlProfesores.Visible = False
            pnlCoordinadores.Visible = True
        End If
    End Sub

#Region "busqueda"

    Protected Sub btnBuscaProfesor_Click(sender As Object, e As EventArgs) Handles btnBuscaProfesor.Click
        GVprofesores.SelectedIndex = -1 ' si había una fila seleccionada la quito
        hfBuscaProfNombre.Value = "%" & tbBuscaProfesorNombre.Text & "%"
        hfBuscaProfApellidos.Value = "%" & tbBuscaProfesorApellidos.Text & "%"
        hfBuscaProfAsignatura.Value = "%" & tbBuscaProfesorAsignatura.Text & "%"
        hfBusquedaProfesores_isActive.Value = "1"
    End Sub

    Protected Sub btnLimpiaBusquedaProfesor_Click(sender As Object, e As EventArgs) Handles btnLimpiaBusquedaProfesor.Click
        GVprofesores.SelectedIndex = -1 ' si había una fila seleccionada la quito
        tbBuscaProfesorNombre.Text = ""
        tbBuscaProfesorApellidos.Text = ""
        tbBuscaProfesorAsignatura.Text = ""
        hfBuscaProfNombre.Value = "%"
        hfBuscaProfApellidos.Value = "%"
        hfBuscaProfAsignatura.Value = "%"
        hfBusquedaProfesores_isActive.Value = ""
        msgInfoProfesores.hide()
    End Sub

    Protected Sub btnBuscaCoordinador_Click(sender As Object, e As EventArgs) Handles btnBuscaCoordinador.Click
        GVcoordinadores.SelectedIndex = -1 ' si había una fila seleccionada la quito
        hfBuscaCoordNombre.Value = "%" & tbBuscaCoordinadorNombre.Text & "%"
        hfBuscaCoordApellidos.Value = "%" & tbBuscaCoordinadorApellidos.Text & "%"
        hfBusquedaCoordinadores_isActive.Value = "1"
    End Sub

    Protected Sub btnLimpiaBusquedaCoordinador_Click(sender As Object, e As EventArgs) Handles btnLimpiaBusquedaCoordinador.Click
        GVcoordinadores.SelectedIndex = -1 ' si había una fila seleccionada la quito
        tbBuscaCoordinadorNombre.Text = ""
        tbBuscaCoordinadorApellidos.Text = ""
        hfBuscaCoordNombre.Value = "%"
        hfBuscaCoordApellidos.Value = "%"
        hfBusquedaCoordinadores_isActive.Value = ""
        MsgInfoCoordinadores.hide()
    End Sub

#End Region

    ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
    ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
    ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
    ' para que no falle la validación de integridad del control.
    ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
    Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
        Dim r As GridViewRow
        For Each r In GVprofesores.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVprofesores, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        For Each r In GVcoordinadores.Rows()
            If r.RowType = DataControlRowType.DataRow Then
                Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVcoordinadores, "Select$" + r.RowIndex.ToString()))
            End If
        Next

        MyBase.Render(writer) ' necesario por default
    End Sub

End Class
