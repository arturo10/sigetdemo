﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO

Partial Class alumno_comunicacion_responder_Default
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            UserInterface.Include.HtmlEditor(Master.ltHeader, 665, 200, 12, Session("Usuario_Idioma").ToString())

            aplicaLenguaje()

            initPageData()

            OutputCss()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = Lang.FileSystem.Alumno.Responder.LT_PAGE_TITLE.Item(Session("Usuario_Idioma"))

        'Label1.Text = Config.Etiqueta.PROFESORES
        'Label2.Text = Config.Etiqueta.COORDINADORES
        If Session("Responder") = "P" Then ' MENSAJE DE PROFESOR
            lt_hint_destinatario.Text = Lang.FileSystem.Alumno.Responder.LT_HINT_PROFESOR.Item(Session("Usuario_Idioma"))
            lt_hint_destinatario_ubicacion.Text = Lang.FileSystem.Alumno.Responder.LT_HINT_ASIGNATURA.Item(Session("Usuario_Idioma"))
        ElseIf Session("Responder") = "C" Then ' MENSAJE PARA COORDINADOR
            lt_hint_destinatario.Text = Lang.FileSystem.Alumno.Responder.LT_HINT_COORDINADOR.Item(Session("Usuario_Idioma"))
            lt_hint_destinatario_ubicacion.Text = Lang.FileSystem.Alumno.Responder.LT_HINT_PLANTEL.Item(Session("Usuario_Idioma"))
        End If

        lt_hint_asunto.Text = Lang.FileSystem.Alumno.Responder.LT_HINT_ASUNTO.Item(Session("Usuario_Idioma"))
        lt_hint_contenido.Text = Lang.FileSystem.Alumno.Responder.LT_HINT_CONTENIDO.Item(Session("Usuario_Idioma"))
        lt_hint_adjuntar.Text = Lang.FileSystem.Alumno.Responder.LT_HINT_ADJUNTAR.Item(Session("Usuario_Idioma"))
        lt_btnAdjuntar.Text = Lang.FileSystem.Alumno.Responder.LT_BTN_ADJUNTAR.Item(Session("Usuario_Idioma"))
        lt_btnQuitar.Text = Lang.FileSystem.Alumno.Responder.LT_BTN_QUITAR.Item(Session("Usuario_Idioma"))
        lt_btnEnviar.Text = Lang.FileSystem.Alumno.Responder.LT_BTN_ENVIAR.Item(Session("Usuario_Idioma"))
        lt_btnLimpiar.Text = Lang.FileSystem.Alumno.Responder.LT_BTN_LIMPIAR.Item(Session("Usuario_Idioma"))
        lt_hl_regresarMensajes.Text = Lang.FileSystem.Alumno.Responder.LT_HL_REGRESARMENSAJES.Item(Session("Usuario_Idioma"))
    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub initPageData()
        hfCarpetaAdjunto.Value = User.Identity.Name.Trim()

        If Session("Envio_Previous").Equals("LeerMensajeSalidaA") Then
            HyperLink2.NavigateUrl = "~/alumno/comunicacion/salida/"
        Else
            HyperLink2.NavigateUrl = "~/alumno/comunicacion/entrada/"
        End If

        TBasunto.Text = Mid("RESP: " + Session("Envio_Asunto") + " [" + Session("Envio_Fecha") + "]", 1, 100)
        If Session("Responder") = "P" Then ' MENSAJE DE PROFESOR
            LblNombre.Text = Session("Envio_NombreProfesor")
            LblAsignatura.Text = Session("Envio_Asignatura")
        ElseIf Session("Responder") = "C" Then ' MENSAJE DE COORDINADOR
            LblNombre.Text = Session("Envio_NombreCoordinador")
            LblAsignatura.Text = Session("Envio_Plantel")
        End If
    End Sub

    ' imprimo los estilos variables de ésta página
    Protected Sub OutputCss()
        Dim s As StringBuilder = New StringBuilder()
        s.Append("<style type='text/css'>")
        s.Append("  #menu .current_area_comunicacion a {") ' esta clase css debe corresponder a la clase del boton a resaltar
        s.Append("    border-bottom: 5px solid " & Config.Color.MenuBar_SelectedOption_BorderColor & ";")
        s.Append("  }")
        s.Append("</style>")
        ltEstilos.Text += s.ToString()
    End Sub

#End Region

#Region "Adjuntos"

    Protected Sub BtnAdjuntar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdjuntar.Click
        Try  'Cuando se adjunta el archivo, se borra el nombre del FileUpload
            If FUadjunto.HasFile Then
        Dim ruta As String = Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value & "\" 'Esta ruta contempla un directorio como alumno nombrado como su login

        'Verifico si ya existe el directorio del alumno, si no, lo creo
        If Not (Directory.Exists(ruta)) Then
          Dim directorio As DirectoryInfo = Directory.CreateDirectory(ruta)
        End If

        'Subo archivo
        Dim now As String = Date.Now.Year.ToString & "_" & Date.Now.Month.ToString & "_" & Date.Now.Day.ToString & "_" & Date.Now.Hour.ToString & "_" & Date.Now.Minute.ToString & "_" & Date.Now.Second.ToString & "_" & Date.Now.Millisecond.ToString
        hfAdjuntoFisico.Value = now & FUadjunto.FileName
        FUadjunto.SaveAs(ruta & hfAdjuntoFisico.Value)
        lbAdjunto.Text = FUadjunto.FileName
        msgError.hide()
        msgSuccess.show(Lang.FileSystem.Alumno.Responder.MSG_ADJUNTADO.Item(Session("Usuario_Idioma")))
      Else
        msgError.show(Lang.FileSystem.Alumno.Responder.ERR_FALTA_ARCHIVO.Item(Session("Usuario_Idioma")))
        msgSuccess.hide()
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      msgSuccess.hide()
    End Try
  End Sub

  Protected Sub lbAdjunto_Click(sender As Object, e As EventArgs) Handles lbAdjunto.Click
    Dim file As System.IO.FileInfo = New System.IO.FileInfo(Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value + "\" & hfAdjuntoFisico.Value) '-- if the file exists on the server
    If file.Exists Then 'set appropriate headers
      Response.Clear()
      Response.AddHeader("Content-Disposition", "attachment; filename=""" & lbAdjunto.Text & """")
      Response.AddHeader("Content-Length", file.Length.ToString())
      Response.ContentType = "application/octet-stream"
      Response.WriteFile(file.FullName)
      'Response.End()
    Else
      msgError.show(Lang.FileSystem.Alumno.Responder.ERR_ADJUNTO_NO_DISPONIBLE.Item(Session("Usuario_Idioma")))
    End If 'nothing in the URL as HTTP GET
  End Sub

  Protected Sub BtnQuitar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnQuitar.Click
    quitarAdjunto()
  End Sub

  Protected Sub quitarAdjunto()
    Try
      If hfAdjuntoFisico.Value.Length > 0 Then
        Dim ruta As String = Config.Global.rutaAdjuntos_fisica & hfCarpetaAdjunto.Value & "\" 'Esta ruta contempla un directorio como alumno nombrado como su login

        'Borro el archivo que estaba anteriormente porque se pondrá uno nuevo
        Dim adjunto As FileInfo = New FileInfo(ruta & hfAdjuntoFisico.Value)
        adjunto.Delete()

        lbAdjunto.Text = ""
        hfAdjuntoFisico.Value = ""
        msgError.hide()
        msgSuccess.show(Lang.FileSystem.Alumno.Responder.MSG_DESADJUNTADO.Item(Session("Usuario_Idioma")))
      Else
        msgError.show(Lang.FileSystem.Alumno.Responder.ERR_NO_HA_ADJUNTADO.Item(Session("Usuario_Idioma")))
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgSuccess.hide()
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

#End Region

  Protected Sub BtnLimpiar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnLimpiar.Click
    TBcontenido.Text = ""
    ' al limpiar se limpia el adjunto, y debe eliminarse el archivo
    quitarAdjunto()
    ' se ocultan los mensajes al final puesto que al quitar el archivo pueden mostrarse
    msgSuccess.hide()
    msgError.hide()
  End Sub

#Region "enviar"

  Protected Sub BtnEnviar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEnviar.Click
    If (Trim(TBasunto.Text).Length > 0) And (Trim(TBcontenido.Text).Length) > 0 Then
      If Session("Responder") = "P" Then ' MENSAJE DE PROFESOR
        enviarMensajeProfesor()
      ElseIf Session("Responder") = "C" Then ' MENSAJE PARA COORDINADOR
        enviarMensajeCoordinador()
      End If
    Else
      msgError.show(Lang.FileSystem.Alumno.Responder.ERR_FALTA_CONTENIDO.Item(Session("Usuario_Idioma")))
      msgSuccess.hide()
    End If
  End Sub

  Protected Sub enviarMensajeProfesor()
    Try
      'Inserto el mensaje [Sentido (E=Envia, R=Recibe - desde el punto de vista del profesor)][Estatus (Leido, Nuevo, Baja)]
      SDScomunicacion.InsertCommand = "SET dateformat dmy; INSERT INTO Comunicacion(IdProgramacion,Sentido,IdAlumno,Asunto,Contenido,Fecha,ArchivoAdjunto,ArchivoAdjuntoFisico,CarpetaAdjunto,EmailEnviado,Estatus, EstatusA)" &
          " VALUES(@IdProgramacion, 'R', @IdAlumno, @Asunto, @Contenido, getdate(), @NombreAdjunto, @RutaAdjunto, @CarpetaAdjunto, @EmailEnviado, 'Nuevo', 'Nuevo')"
      SDScomunicacion.InsertParameters.Add("IdProgramacion", Session("Envio_IdProgramacion").ToString)
      SDScomunicacion.InsertParameters.Add("IdAlumno", Session("Envio_IdAlumno").ToString)
      SDScomunicacion.InsertParameters.Add("Asunto", TBasunto.Text)
      SDScomunicacion.InsertParameters.Add("Contenido", TBcontenido.Text)
      SDScomunicacion.InsertParameters.Add("NombreAdjunto", lbAdjunto.Text)
      SDScomunicacion.InsertParameters.Add("RutaAdjunto", hfAdjuntoFisico.Value)
      SDScomunicacion.InsertParameters.Add("CarpetaAdjunto", hfCarpetaAdjunto.Value)
      SDScomunicacion.InsertParameters.Add("EmailEnviado", Session("Envio_Email"))
      SDScomunicacion.Insert()

      'Si tenía un Email registrado el Coordinador, le envía una notificación
      If Trim(Session("Envio_Email")).Length > 1 Then
        If Config.Global.MAIL_ACTIVO Then
          Utils.Correo.EnviaCorreo(Trim(Session("Envio_Email")),
                                   Config.Global.NOMBRE_FILESYSTEM & Lang.FileSystem.Alumno.Responder.MSG_NOTIFICACION_ASUNTO.Item(Session("Usuario_Idioma")),
                                   Lang.FileSystem.Alumno.Responder.MSG_NOTIFICACION_CUERPO.Item(Session("Usuario_Idioma")) & " " & Session("Envio_NombreAlumno").ToString,
                                   False)
        End If
      End If

      msgSuccess.show(Lang.FileSystem.Alumno.Responder.MSG_ENVIADO_PROFESOR.Item(Session("Usuario_Idioma")))
      msgError.hide()
      TBasunto.Text = ""
      TBcontenido.Text = ""
      lbAdjunto.Text = ""
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      msgSuccess.hide()
    End Try
  End Sub

  Protected Sub enviarMensajeCoordinador()
    Try
      'Inserto el mensaje [Sentido (E=Envia, R=Recibe - desde el punto de vista del profesor)][Estatus (Leido, Nuevo, Baja)]
      SDScomunicacionCA.InsertCommand = "SET dateformat dmy; INSERT INTO ComunicacionCA(IdCoordinador,IdAlumno,Sentido,Asunto,Contenido,Fecha,ArchivoAdjunto,ArchivoAdjuntoFisico,CarpetaAdjunto,EmailEnviado,Estatus, EstatusA)" &
                          " VALUES(@IdCoordinador, @IdAlumno, 'R', @Asunto, @Contenido, getdate(), @NombreAdjunto, @RutaAdjunto, @CarpetaAdjunto, @EmailEnviado, 'Nuevo', 'Nuevo')"
      SDScomunicacionCA.InsertParameters.Add("IdCoordinador", Session("Envio_IdCoordinador"))
      SDScomunicacionCA.InsertParameters.Add("IdAlumno", Session("Envio_IdAlumno").ToString)
      SDScomunicacionCA.InsertParameters.Add("Asunto", TBasunto.Text)
      SDScomunicacionCA.InsertParameters.Add("Contenido", TBcontenido.Text)
      SDScomunicacionCA.InsertParameters.Add("NombreAdjunto", lbAdjunto.Text)
      SDScomunicacionCA.InsertParameters.Add("RutaAdjunto", hfAdjuntoFisico.Value)
      SDScomunicacionCA.InsertParameters.Add("CarpetaAdjunto", hfCarpetaAdjunto.Value)
      SDScomunicacionCA.InsertParameters.Add("EmailEnviado", Session("Envio_Email"))
      SDScomunicacionCA.Insert()

      'Si tenía un Email registrado el Profesor, le envía una notificación
      If Trim(Session("Envio_Email")).Length > 1 Then
        If Config.Global.MAIL_ACTIVO Then
          Utils.Correo.EnviaCorreo(Trim(Session("Envio_Email")),
                                   Config.Global.NOMBRE_FILESYSTEM & Lang.FileSystem.Alumno.Responder.MSG_NOTIFICACION_ASUNTO.Item(Session("Usuario_Idioma")),
                                   Lang.FileSystem.Alumno.Responder.MSG_NOTIFICACION_CUERPO.Item(Session("Usuario_Idioma")) & " " & Session("Envio_NombreAlumno").ToString,
                                   False)
        End If
      End If

      msgSuccess.show(Lang.FileSystem.Alumno.Responder.MSG_ENVIADO_COORDINADOR.Item(Session("Usuario_Idioma")))
      msgError.hide()
      TBasunto.Text = ""
      TBcontenido.Text = ""
      lbAdjunto.Text = ""
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      msgSuccess.hide()
    End Try
  End Sub

#End Region

End Class
