﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/alumno/comunicacion_alumno_5-1-0.master"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="alumno_comunicacion_responder_Default"
    ValidateRequest="false" %>

<%-- Se necesita esta propiedad para acceder directamente a propiedades de la página maestra --%>
<%@ MasterType VirtualPath="~/alumno/comunicacion_alumno_5-1-0.master" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder_comunicacion" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head_comunicacion" runat="Server">
    <%-- Aquí imprimo los estilos variables de ésta página específica --%>
    <asp:Literal ID="ltEstilos" runat="server" />

    <style type="text/css">
        .style28 {
            height: 44px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style33 {
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
        }

        .style32 {
            width: 10px;
        }

        .style31 {
            font-family: Arial, Helvetica, sans-serif;
            text-align: left;
        }

        .style21 {
            width: 273px;
        }

        .style13 {
            width: 143px;
        }

        .style34 {
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
            font-weight: normal;
            font-size: small;
        }
        .style14 {
            text-align: left;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor_comunicacion" runat="Server">
    <table class="style10">
        <tr class="estandar">
            <td class="style14" colspan="7">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style33" colspan="2">
                <asp:Literal ID="lt_hint_destinatario" runat="server" Text="[PROFESOR]" />
            </td>
            <td class="style14" colspan="3">
                <asp:Label ID="LblNombre" runat="server"
                    CssClass="LabelInfoDefault"></asp:Label>
            </td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style33" colspan="2">
                <asp:Literal ID="lt_hint_destinatario_ubicacion" runat="server" Text="[ASIGNATURA]" />
            </td>
            <td class="style14" colspan="3">
                <asp:Label ID="LblAsignatura" runat="server"
                    CssClass="LabelInfoDefault"></asp:Label>
            </td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style33" colspan="2">
                <asp:Literal ID="lt_hint_asunto" runat="server">[Asunto]</asp:Literal></td>
            <td class="style14" colspan="3">
                <asp:TextBox ID="TBasunto" runat="server" MaxLength="100" Width="665px"></asp:TextBox>
            </td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style33" colspan="2">
                <asp:Literal ID="lt_hint_contenido" runat="server">[Contenido]</asp:Literal></td>
            <td class="style14" colspan="3">
                <asp:TextBox ID="TBcontenido" runat="server" 
                    ClientIDMode="Static" 
                    TextMode="MultiLine"
                    CssClass="tinymce"></asp:TextBox>
            </td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style33" colspan="2">
                <asp:Literal ID="lt_hint_adjuntar" runat="server">[Adjuntar archivo (solo uno)]</asp:Literal></td>
            <td class="style32">
                <asp:FileUpload ID="FUadjunto" runat="server" />
            </td>
            <td class="style14">
                <asp:LinkButton ID="BtnAdjuntar" runat="server" 
                    CssClass="defaultBtn btnThemeGrey btnThemeSlick" >
                    <asp:Image ID="Image9" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/attach.png"
                        CssClass="btnIcon" />
                    <asp:Literal ID="lt_btnAdjuntar" runat="server">[Adjuntar]</asp:Literal>
                </asp:LinkButton>
                &nbsp;
                <asp:LinkButton ID="BtnQuitar" runat="server" 
                    CssClass="defaultBtn btnThemeGrey btnThemeSlick" >
                    <asp:Image ID="Image8" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/cancel.png"
                        CssClass="btnIcon" />
                    <asp:Literal ID="lt_btnQuitar" runat="server">[Quitar]</asp:Literal>
                </asp:LinkButton>
            </td>
            <td class="style14" colspan="3">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="2"></td>
            <td colspan="5" style="text-align: left">
                <asp:LinkButton ID="lbAdjunto" runat="server" CssClass="LabelInfoDefault"></asp:LinkButton>
            </td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="2">&nbsp;</td>
            <td class="style14" colspan="3">&nbsp;</td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr class="estandar">
            <td colspan="7">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="style31" colspan="2">&nbsp;</td>
            <td colspan="3">
                <asp:LinkButton ID="BtnEnviar" runat="server"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" >
                    <asp:Image ID="Image1" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/woocons/Button White Check.png"
                        CssClass="btnIcon"
                        height="24" />
                    <asp:Literal ID="lt_btnEnviar" runat="server" Text="[Enviar Mensaje]" />
                </asp:LinkButton>
                &nbsp;
                <asp:LinkButton ID="BtnLimpiar" runat="server"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" >
                    <asp:Image ID="Image4" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/draw_eraser.png"
                        CssClass="btnIcon"
                        height="24" />
                    <asp:Literal ID="lt_btnLimpiar" runat="server" Text="[Limpiar]" />
                </asp:LinkButton>
            </td>
            <td class="style14" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="style22">&nbsp;</td>
            <td class="style21" colspan="3">&nbsp;</td>
            <td class="style13" colspan="2">&nbsp;</td>
            <td class="style15">&nbsp;</td>
        </tr>
        <tr>
            <td class="style21" colspan="4">
                <asp:HyperLink ID="HyperLink2" runat="server" 
                    NavigateUrl="javascript:history.go(-2);"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                    <asp:Image ID="Image3" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/bullet_back_2.png"
                        CssClass="btnIcon" />
                    <asp:Literal ID="lt_hl_regresarMensajes" runat="server">[Regresar a Mensajes]</asp:Literal>
                </asp:HyperLink>
            </td>
            <td class="style13" colspan="2">&nbsp;</td>
            <td class="style15">&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>
                <asp:SqlDataSource ID="SDScomunicacion" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Comunicacion]"></asp:SqlDataSource>
            </td>
            <td>
                <asp:SqlDataSource ID="SDScomunicacionCA" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [ComunicacionCA]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:HiddenField ID="hfAdjuntoFisico" runat="server" />
            </td>
            <td>
                <asp:HiddenField ID="hfCarpetaAdjunto" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>

