﻿Imports Siget

Partial Class comunicacion_alumno_5_1_0
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' Cuenta mensajes entrantes: si hay mensajes, muestra el globo con el número en el menú lateral
        Dim msgCount As Integer = DataAccess.ComunicacionDa.CuentaMensajesAlumno(Session("Usuario_IdAlumno"))
        If msgCount > 0 Then
            estableceMensajes(msgCount)
        End If

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            ' aplica lenguaje necesita controles inicializados de initPageData
            aplicaLenguaje()
        End If
    End Sub

    ' éste método permite establecer el número de mensajes recibidos 
    ' en el menú superior
    Public Sub estableceMensajes(ByRef numero As Integer)
        lblNotificaEntrada.Text = numero
        lblNotificaEntrada.Visible = True
    End Sub

    ' Ésta propiedad es necesaria para inyectar estilos y scripts en el encabezado de la página principal
    ' para poder referenciarse (Master.ltHeader) a ésta página maestra en las páginas hijas, 
    ' incluir el inicio de la página de diseño de cada una:
    ' <%@ MasterType VirtualPath="~/master_examplefile.master"  %>
    Public Property ltHeader() As Literal
        Get
            Return pnlHeader ' reporto el literal en el head
        End Get

        Set(value As Literal)
            ' no es necesaria
        End Set
    End Property

    Protected Sub aplicaLenguaje()
        lt_enviar.Text = Lang.FileSystem.Alumno.Master_Comunicacion.LT_ENVIAR.Item(Session("Usuario_Idioma"))
        lt_entrada.Text = Lang.FileSystem.Alumno.Master_Comunicacion.LT_ENTRADA.Item(Session("Usuario_Idioma"))
        lt_salida.Text = Lang.FileSystem.Alumno.Master_Comunicacion.LT_SALIDA.Item(Session("Usuario_Idioma"))
    End Sub

End Class

