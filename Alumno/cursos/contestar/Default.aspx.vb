﻿Imports Siget
Imports Siget.Beans
Imports Siget.DataAccess
Imports System.Data


'Imports System.Data
'Imports System.Data.OleDb
Imports System.Data.SqlClient

Partial Class alumno_cursos_contestar_Default
  Inherits System.Web.UI.Page

  Protected Shared titleIcon As String =
      Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/document_prepare.png"

#Region "init"

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    If Not Utils.Sesion.sesionAbierta() Then
      Session("Nota") = Lang.FileSystem.General.Msg_SuSesionExpiro.Item(Session("Usuario_Idioma").ToString())
      Response.Redirect("~/")
    End If

    ' protección en contra de intentos ilegales
    If IsNothing(Session("Contestar_IdEvaluacion")) Then
      Session("Nota") = Lang.FileSystem.Alumno.Cursos.msg_evaluacion_no_identificada.Item(Session("Usuario_Idioma"))
      Response.Redirect("~/")
    End If

    ' aquí inicializo la página la primera vez que se carga
    If Not IsPostBack Then
      UserInterface.Include.HtmlEditor(CType(Master.FindControl("pnlHeader"), Literal), 700, 250, 12, Session("Usuario_Idioma").ToString())

      aplicaLenguaje()

      initPageData()

      OutputCss()
    End If
  End Sub

  ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
  Protected Sub aplicaLenguaje()
    TitleLiteral.Text = Lang.FileSystem.Alumno.Contestar.LT_TITULO.Item(Session("Usuario_Idioma"))
    Aceptar.Text = Lang.FileSystem.Alumno.Contestar.BTN_ACEPTAR_MULTIPLE.Item(Session("Usuario_Idioma"))
    Cancelar.Text = Lang.FileSystem.Alumno.Contestar.BTN_SALIR_MULTIPLE.Item(Session("Usuario_Idioma"))
    Brincar.Text = Lang.FileSystem.Alumno.Contestar.BTN_BRINCAR_MULTIPLE.Item(Session("Usuario_Idioma"))
    lt_hint_redacte_respuesta.Text = Lang.FileSystem.Alumno.Contestar.LT_HINT_REDACTE_RESPUESTA.Item(Session("Usuario_Idioma"))
    AceptarA.Text = Lang.FileSystem.Alumno.Contestar.BTN_ACEPTAR_ABIERTA.Item(Session("Usuario_Idioma"))
    AceptarA.OnClientClick = " var text = tinyMCE.activeEditor.getContent();  if(text.length < 1) {$(validacionAbierta).css('display', 'inline'); return false;} else {$(validacionAbierta).css('display', 'none');}  if(!confirm('" & Lang.FileSystem.Alumno.Contestar.MSG_CONFIRMA_RESPUESTA_ABIERTA.Item(Session("Usuario_Idioma")) & "'))return false; "
    GuardarA.Text = Lang.FileSystem.Alumno.Contestar.BTN_GUARDAR_ABIERTA.Item(Session("Usuario_Idioma"))
    GuardarA.OnClientClick = "if(!confirm('" & Lang.FileSystem.Alumno.Contestar.MSG_CONFIRMA_RESPUESTA_GUARDAR.Item(Session("Usuario_Idioma")) & "'))return false;"
    CancelarA.Text = Lang.FileSystem.Alumno.Contestar.BTN_SALIR_ABIERTA.Item(Session("Usuario_Idioma"))
    AceptarAA.Text = Lang.FileSystem.Alumno.Contestar.BTN_ACEPTAR_ABIERTA.Item(Session("Usuario_Idioma"))
    CancelarAA.Text = Lang.FileSystem.Alumno.Contestar.BTN_SALIR_ABIERTA.Item(Session("Usuario_Idioma"))
    VerificarAA.Text = Lang.FileSystem.Alumno.Contestar.BtnVerificaRespuesta.Item(Session("Usuario_Idioma"))
    ltRespuestaVacia.Text = Lang.FileSystem.Alumno.Contestar.rfv_respuestaAbierta.Item(Session("Usuario_Idioma"))

    msgVerifiedGood.show(Lang.FileSystem.Alumno.Contestar.Msg_Verifica_Bien.Item(Session("Usuario_Idioma")))
    msgVerifiedBad.show(Lang.FileSystem.Alumno.Contestar.Msg_Verifica_Mal.Item(Session("Usuario_Idioma")))

        LblFaltan.Attributes.Add("style", "background: " & Config.Color.MenuPendientes_Subtitle & "; ppresadding: 5px;")
    End Sub

  ''' <summary>
  ''' Inicializo los datos que necesito para los controles de la página
  ''' </summary>
  Protected Sub initPageData()

    ' muestra u oculta el botón de guardado de respuesta abierta
    If Config.Global.GUARDAR_RESPUESTA_ABIERTA Then
      GuardarA.Visible = True
    Else
      GuardarA.Visible = False
    End If

    'En cuanto seleccione un radiobutton se habilitará el botón aceptar
    Opcion1.Attributes.Add("onclick", "document.getElementById(""" + Aceptar.ClientID + """).removeAttribute('disabled');")
    Opcion2.Attributes.Add("onclick", "document.getElementById(""" + Aceptar.ClientID + """).removeAttribute('disabled');")
    Opcion3.Attributes.Add("onclick", "document.getElementById(""" + Aceptar.ClientID + """).removeAttribute('disabled');")
    Opcion4.Attributes.Add("onclick", "document.getElementById(""" + Aceptar.ClientID + """).removeAttribute('disabled');")
    Opcion5.Attributes.Add("onclick", "document.getElementById(""" + Aceptar.ClientID + """).removeAttribute('disabled');")
    Opcion6.Attributes.Add("onclick", "document.getElementById(""" + Aceptar.ClientID + """).removeAttribute('disabled');")

    ' configura el botón de renuncia
    IBrenunciar.Attributes.Add("onclick", "return confirm('" & Lang.FileSystem.Alumno.Contestar.MSG_CONFIRMA_RENUNCIA.Item(Session("Usuario_Idioma")) & "');")
    IBrenunciar.Attributes.Add("onmouseover", "this.src='" & Config.Global.urlImagenes & "Stop1b.png'")
    IBrenunciar.Attributes.Add("onmouseout", "this.src='" & Config.Global.urlImagenes & "Stop1a.png'")
    If Config.Global.ALUMNO_CONTESTAR_RENUNCIAR Then
      IBrenunciar.Visible = True
    End If

    ' configuro el botón de aceptar de modo que se deshabilite al hacer click en él
    'Tuve que usar un objeto image VISIBLE con una IMAGEN asignada para que se ejecutara el evento ONLOAD, 
    'ya que en el ImageButton solo se ejecuta en IExplorer y no en Google Chrome. El efecto es sobre el boton ACEPTAR
    Image1.Attributes.Add("onload", "document.getElementById(""" + Aceptar.ClientID + """).setAttribute('disabled','disabled');")
    'Aceptar.Enabled = False --> Si uso esta instrucción, cuando habilito el boton aceptar ya no funciona al darle click

    ' muestro el label con el nombre del alumno
    LblAlumno.Text = Lang.FileSystem.Alumno.Contestar.LT_HINT_ALUMNO.Item(Session("Usuario_Idioma")) & " " + Session("Usuario_Nombre").ToString.Trim()

    ' Proceso si hay reactivo a presentar o calificar
    procesaActividad()
  End Sub

  ''' <summary>
  ''' Control principal de procesamiento de la evaluacíón;
  ''' obtiene los subtemas de la evaluación y los datos de la misma, 
  ''' verifica si se terminó la evaluación, y de ser así califica, 
  ''' de lo contrario obtiene el siguiente reactivo a presentar y lo despliega.
  ''' </summary>
  Protected Sub procesaActividad()
    ' utilizo una misma conexión para todo el procesamiento
    Using conn As SqlConnection = New SqlConnection(ConfigurationManager.
                                                    ConnectionStrings("sadcomeConnectionString").
                                                    ConnectionString)
      conn.Open()
      ' *************************************************************************************

      ' el contenedor de datos de esta evaluación
      Dim evaluacion As Evaluacion
      Dim ciclo As CicloEscolar
      ' el contenedor de subtemas de la evaluación
      Dim subtemas As ArrayList = New ArrayList()

      ' ***********************************
      ' primero obtengo datos de la evaluación
      evaluacion = CType(New EvaluacionDa, EvaluacionDa).ObtenDatosEvaluacion(Session("Contestar_IdEvaluacion").ToString().Trim())
      HFidEvaluacion.Value = evaluacion.IdEvaluacion

      ' y pongo el título
      PageTitle_v1.show(Lang.FileSystem.Alumno.Contestar.LT_INICIO_TITULO.Item(Session("Usuario_Idioma")) & " " + evaluacion.ClaveBateria,
                    titleIcon)

      ' aquí obtengo el ciclo de la evaluación
      ciclo = CType(New CicloEscolarDa, CicloEscolarDa).ObtenCicloDeEvaluacion(conn, Session("Contestar_IdEvaluacion").ToString().Trim())
      HFcicloescolar.Value = ciclo.IdCicloEscolar

      ' aquí obtengo una lista de subtemas de la evaluación
      subtemas = CType(New SubtemaDa, SubtemaDa).ObtenSubtemasDeEvalaucion(
          conn,
          Session("Contestar_IdEvaluacion").ToString().Trim(),
          Session("Usuario_IdGrado").ToString())

      ' *****************************************************************************************
      ' Verifico si ya termino la actividad

      Dim terminada As Boolean = True ' indicador de terminación
      Dim subtActual As Subtema = Nothing ' cuando no ha terminado, almacena el subtema actual a trabajar
      Dim respuestasEnActividad As Integer = 0 ' contador del total de respuestas de todos los subtemas
      Dim reactivosEnSubtemas As Integer = 0 ' contador del total de reactivos de todos los subtemas

      ' itero a través de cada subtema
      For Each st As Subtema In subtemas
        Dim total As Integer

        ' obtengo el número de planteamientos respondidos
        Dim respondidos As Integer = CType(New RespuestaDa, RespuestaDa).CuentaReactivosContestadosDeSubtema(
                                            conn,
                                            st.IdDetalleEvaluacion,
                                            Session("Usuario_IdAlumno").ToString())
        respuestasEnActividad += respondidos ' almaceno el número de reactivos que ha respondido el alumno

        ' obtengo el número de planteamientos a usar, 
        ' si esta definido un límite o todos del subtema
        If st.UsarReactivos.HasValue() Then
          total = st.UsarReactivos
        Else
          total = st.Reactivos
        End If

        ' si el alumno ha respondido menos reactivos de los que indica el subtema, no ha terminado
        If (Math.Abs(total - respondidos) > 0) Then
          subtActual = st ' almaceno el subtema actual
          HFdetalleEvaluacion.Value = subtActual.IdDetalleEvaluacion

          terminada = False ' y señalo
          Exit For ' interrumpo para mantener el subtema incompleto
        End If
      Next

      ' *****************************************************************************************
      ' si terminó y no hay guardadas, califico, de lo contrario muestro guardadas
      If terminada Then
        ' antes de redirigir a terminadas, ejecuto contestarGuardadas para ver si tiene respuestas guardadas para editar.
        If Not IsNothing(Session("Contestar_Borrador")) Then

          ' el método contestarGuardadas ya presenta las respuestas guardadas
          If Not contestarGuardadas(conn, evaluacion.IdEvaluacion) Then
            Response.Redirect("~/alumno/cursos/")
          End If
        Else
          calificaActividad(conn, evaluacion, ciclo)
        End If
      End If

      ' *****************************************************************************************
      ' si no terminó, hay que preparar la página para el siguiente reactivo
      If Not terminada Then

        ' obtengo el número de reactivos que faltan del total.
        ' no lo hice en el ciclo de conteo de respuestas por que 
        ' ahí se hacen consultas a la base de datos y se interrumpe cuando no se ha terminado
        For Each st As Subtema In subtemas
          ' obtengo el número de planteamientos a usar, 
          ' si esta definido un límite o todos del subtema
          If st.UsarReactivos.HasValue() Then
            reactivosEnSubtemas += st.UsarReactivos
          Else
            reactivosEnSubtemas += st.Reactivos
          End If
        Next
        ' presento los faltantes
        presentaEtiquetaFaltan(respuestasEnActividad, reactivosEnSubtemas, subtActual.datos.Descripcion)

        ' obtengo el siguiente planteamiento a mostrar
        presentaReactivo(conn, subtActual, evaluacion)
      End If

      ' *************************************************************************************
      conn.Close()
    End Using
  End Sub

  Protected Sub calificaActividad(
                                 ByRef conn As SqlConnection,
                                 ByRef eval As Evaluacion,
                                 ByRef ciclo As CicloEscolar)
    Dim cmd As SqlCommand
    Dim res As SqlDataReader

    ' *****************************************************************************************
    'SUMO EL TOTAL DE PUNTOS QUE DA LA EVALUACION Y LA CANTIDAD DE REACTIVOS QUE LA COMPONEN
    'OJO QUE AQUÍ SACO EL PORCENTAJE DE ACIERTOS SOBRE LO CONTESTADO, NO SOBRE
    'LOS PLANTEAMIENTOS TOTALES, AUNQUE SE SUPONE QUE COINCIDE PORQUE YA TERMINO LA EVALUACION
    cmd = New SqlCommand(
"select " &
"   Sum(P.Ponderacion) Puntos, " &
"   Count(Ponderacion) Cant " &
"from " &
"   Respuesta R, " &
"   Planteamiento P, " &
"   DetalleEvaluacion D " &
"where " &
"   (R.IdPlanteamiento = P.IdPlanteamiento and P.Ocultar = 0 ) " &
"   and R.IdAlumno = @IdAlumno" &
"   and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion " &
"   and D.IdEvaluacion  = @IdEvaluacion", conn)

    cmd.Parameters.AddWithValue("@IdAlumno", Session("Usuario_IdAlumno"))
    cmd.Parameters.AddWithValue("@IdEvaluacion", eval.IdEvaluacion.ToString())

    res = cmd.ExecuteReader()
    res.Read()

    Dim TotalPuntos As Decimal
    Dim TotalReactivos As Byte
    If IsDBNull(res.Item("Puntos")) Then
      TotalPuntos = 0
    Else
      TotalPuntos = res.Item("Puntos")
    End If

    If IsDBNull(res.Item("Cant")) Then
      TotalReactivos = 0
    Else
      TotalReactivos = res.Item("Cant")
    End If

    res.Close()
    cmd.Dispose()

    ' *****************************************************************************************
    'CHECO CUANTOS PUNTOS SUMO EL ALUMNO CORRECTAMENTE EN EL PRIMER INTENTO
    cmd = New SqlCommand(
"select " &
"   Sum(P.Ponderacion) Puntos, " &
"   Count(Ponderacion) Cant " &
"from " &
"   Respuesta R, " &
"   Planteamiento P, " &
"   DetalleEvaluacion D " &
"where " &
"   (R.IdPlanteamiento = P.IdPlanteamiento) " &
"   and P.Ocultar = 0 " &
"   and R.IdAlumno = @IdAlumno " &
"   and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion " &
"   and D.IdEvaluacion  = @IdEvaluacion " &
"   and R.Acertada = 'S' " &
"   and R.Intento = 1", conn)

    cmd.Parameters.AddWithValue("@IdAlumno", Session("Usuario_IdAlumno"))
    cmd.Parameters.AddWithValue("@IdEvaluacion", eval.IdEvaluacion.ToString())

    res = cmd.ExecuteReader()
    res.Read()

    Dim PuntosOk, ReactivosAcertados As Decimal
    If IsDBNull(res.Item("Puntos")) Then
      PuntosOk = 0
      ReactivosAcertados = 0
    Else
      PuntosOk = res.Item("Puntos")
      ReactivosAcertados = res.Item("Cant")
    End If

    res.Close()
    cmd.Dispose()

    ' *****************************************************************************************
    'CHECO CUANTOS PUNTOS SUMO EL ALUMNO CORRECTAMENTE EN EL SEGUNDO INTENTO
    'NO ME ACUERDO PORQUE PUSE ESTO AQUI, NO SE SI SE NECESITA
    cmd = New SqlCommand(
"select Sum(Ponderacion - (P.Ponderacion * P.PorcentajeRestarResp)/100) Puntos, Count(Ponderacion) Cant " &
"from Respuesta R, Planteamiento P, DetalleEvaluacion D " &
"where (R.IdPlanteamiento = P.IdPlanteamiento) and Ocultar = 0 " &
"and R.IdAlumno = @IdAlumno and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion " &
"and D.IdEvaluacion = @IdEvaluacion and R.Acertada = 'S' and R.Intento = 2", conn)

    cmd.Parameters.AddWithValue("@IdAlumno", Session("Usuario_IdAlumno"))
    cmd.Parameters.AddWithValue("@IdEvaluacion", eval.IdEvaluacion.ToString())

    res = cmd.ExecuteReader()
    res.Read()

    If (Not IsDBNull(res.Item("Puntos"))) Then  '(res.Item("Cant") > 0) no es necesario
      PuntosOk = PuntosOk + res.Item("Puntos")
      ReactivosAcertados = ReactivosAcertados + res.Item("Cant")
    End If

    res.Close()
    cmd.Dispose()

    ' *****************************************************************************************
    'CALCULO EL RESULTADO
    Dim Resultado As Decimal
    If TotalPuntos <> 0 Then
      Resultado = (PuntosOk / TotalPuntos) * 100
    Else
      Resultado = 0
    End If

    ' *****************************************************************************************
    'Checo que si contestó después de la fecha límite, lo que le aplica una penalización
    If Date.Now() > Session("Contestar_FinSinPenalizacion") Then
      Resultado = Resultado - (Resultado * eval.Penalizacion / 100)
    End If

    ' *****************************************************************************************
    'Inserto el registro de la evaluacion terminada
    SDSevaluacionesterminadas.InsertCommand =
"SET dateformat dmy; " &
"INSERT INTO EvaluacionTerminada( " &
"   IdAlumno, " &
"   IdGrado, " &
"   IdGrupo, " &
"   IdCicloEscolar, " &
"   IdEvaluacion, " &
"   TotalReactivos, " &
"   ReactivosContestados, " &
"   PuntosPosibles, " &
"   PuntosAcertados, " &
"   Resultado, " &
"   FechaTermino) " &
"VALUES( " &
"   @IdAlumno, " &
"   @IdGrado, " &
"   @IdGrupo, " &
"   @IdCicloEscolar, " &
"   @IdEvaluacion, " &
"   @TotalReactivos, " &
"   @ReactivosAcertados, " &
"   @TotalPuntos, " &
"   @PuntosBien, " &
"   @Resultado, " &
"   getdate())"

    SDSevaluacionesterminadas.InsertParameters.Add("IdAlumno", Session("Usuario_IdAlumno"))
    SDSevaluacionesterminadas.InsertParameters.Add("IdGrado", Session("Usuario_IdGrado"))
    SDSevaluacionesterminadas.InsertParameters.Add("IdGrupo", Session("Usuario_IdGrupo"))
    SDSevaluacionesterminadas.InsertParameters.Add("IdCicloEscolar", ciclo.IdCicloEscolar.ToString())
    SDSevaluacionesterminadas.InsertParameters.Add("IdEvaluacion", eval.IdEvaluacion.ToString())
    SDSevaluacionesterminadas.InsertParameters.Add("TotalReactivos", TotalReactivos.ToString())
    SDSevaluacionesterminadas.InsertParameters.Add("ReactivosAcertados", ReactivosAcertados.ToString())
    SDSevaluacionesterminadas.InsertParameters.Add("TotalPuntos", FormatNumber(TotalPuntos, 2))
    SDSevaluacionesterminadas.InsertParameters.Add("PuntosBien", FormatNumber(PuntosOk, 2))
        SDSevaluacionesterminadas.InsertParameters.Add("Resultado", FormatNumber(Resultado, 2))
        ' puedo usar tambien: String.Format("{0:c}", Resultado) para que salga con simbolok y todo

        SDSevaluacionesterminadas.Insert()

    Session("ListaReactivos_IdEvaluacion") = eval.IdEvaluacion

        conn.Close() 'cierro la conexión de trabajo
    Response.Redirect("~/alumno/cursos/actividad/listareactivos/")
  End Sub

    Protected Sub presentaEtiquetaFaltan(ByVal respondidos As Integer, ByVal total As Integer, ByVal descripcionSubtema As String)
        LblFaltan.Text = "<b>" & Lang.FileSystem.Alumno.Contestar.MSG_FALTAN_REACTIVOS.Item(Session("Usuario_Idioma")) & " " + (total - respondidos).ToString + "</b> - " & Lang.FileSystem.Alumno.Contestar.LT_FALTAN_SUBTEMA.Item(Session("Usuario_Idioma")) & " " + descripcionSubtema
    End Sub

    Protected Sub presentaReactivo(ByRef conn As SqlConnection, ByRef subtemaActual As Subtema, ByRef evaluacionActual As Evaluacion)
        Dim cmd As SqlCommand
        Dim res As SqlDataReader
        Dim ancho1 As String = "180"
        Dim Redaccion As String
        Dim tipoRespuesta As String
        Dim idPlanteamiento As String
        Dim da As New SqlDataAdapter
        Dim dt As New DataTable()
        Dim arrayConsecutivo As ArrayList = New ArrayList({0, 1, 2, 3, 4, 5})
        ' *****************************************************************************************
        'AQUI SACAR LOS PLANTEAMIENTOS QUE NO HAYAN SIDO CONTESTADOS. AQUI TENDRIA QUE CAMBIAR
        'LA CONSULTA CUANDO CONSIDERE EL SEGUNDO INTENTO, MIENTRAS SOLO CHECA SI YA SE CONTESTO LA OPCION
        cmd = New SqlCommand(
"SELECT P.* " &
"FROM Planteamiento P JOIN DetalleEvaluacion DE ON DE.IdDetalleEvaluacion=@IdDetalleEvaluacion  " &
"WHERE " &
"    P.Ocultar = 0 " &
"  and P.IdSubtema = @IdSubtema " &
"    and P.IdPlanteamiento not in  (" &
"       select R.IdPlanteamiento " &
"        from Respuesta R  " &
"       where IdAlumno = @IdAlumno and idDetalleEvaluacion = @IdDetalleEvaluacion " &
" UNION ALL  " &
" SELECT CASE WHEN DE.UsarReactivos<=(SELECT COUNT(*) from Respuesta where IdAlumno=@IdAlumno  and IdDetalleEvaluacion=@IdDetalleEvaluacion) " &
" THEN IdPlanteamiento ELSE 0 END FROM Planteamiento where IdSubtema=@IdSubtema )", conn)

        cmd.Parameters.AddWithValue("@IdSubtema", subtemaActual.datos.IdSubtema.ToString())
        cmd.Parameters.AddWithValue("@IdAlumno", Session("Usuario_IdAlumno"))
        cmd.Parameters.AddWithValue("@IdDetalleEvaluacion", subtemaActual.IdDetalleEvaluacion.ToString())

        'Si es aleatoria la Actividad, agrego la instrucción que genera al azar
        If (evaluacionActual.Aleatoria = 1 Or subtemaActual.UsarReactivos.HasValue()) Then
            cmd.CommandText += " order by newid()" 'Ordena aleatoriamente los registros
            If subtemaActual.UsarReactivos.HasValue() Then
                Brincar.Visible = False 'Este boton solo tiene sentido si es de reactivos aleatorios pero TODOS
            Else
                Brincar.Visible = True
            End If
        Else
            cmd.CommandText += " order by Consecutivo"
            Brincar.Visible = False
        End If

        cmd.CommandTimeout = 200
        res = cmd.ExecuteReader()
        res.Read()

        HFidPlanteamiento.Value = res.Item("IdPlanteamiento") 'Este campo es para pasarlo al script PROCESA.ASPX

        If res.Item("EsconderTexto") Then
            LblPlanteamiento.Visible = False
            LblConsecutivo.Text = CStr(res.Item("Consecutivo")) + ".-&nbsp;"
        Else
            LblPlanteamiento.Visible = True

            If Not IsDBNull(res.Item("Redaccion")) Then
                Redaccion = Replace(HttpUtility.HtmlDecode(res.Item("Redaccion")), "*salto*", "<BR>")
                Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                LblPlanteamiento.Text = Redaccion
            End If

            If res.Item("Consecutivo") > 0 Then
                LblConsecutivo.Text = CStr(res.Item("Consecutivo")) + ".-&nbsp;" + "&nbsp;" + "&nbsp;"
            Else
                'OJO: Aqui, en el despliegue para alumnos, tendria que usar un contador que enumere el planteamiento
                LblConsecutivo.Text = "#.-&nbsp;"
            End If
        End If

        If Not IsDBNull(res.Item("HabilitaRecorder")) Then
            If res.Item("HabilitaRecorder") Then
                pnlRecorder.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:430px;height:140px;")
                pnlRecorder.Controls.Add(New LiteralControl("<embed src=""../AudioRecorder.swf"" width=""430"" height=""140""></embed>"))
            End If
        End If

        'Ahora veo si es de tiempo:
        If res.Item("Tiempo") Then
            Timer1.Interval = res.Item("Minutos") * 60 * 1000
            Timer1.Enabled = True
            LblAlumno.Text = LblAlumno.Text + "<br><font color='#79CDCD'> " & Lang.FileSystem.Alumno.Contestar.MSG_MINUTOS_INICIO.Item(Session("Usuario_Idioma")) & " " &
                  res.Item("Minutos").ToString + " " & Lang.FileSystem.Alumno.Contestar.MSG_MINUTOS_FIN.Item(Session("Usuario_Idioma")) & "</font>"
        End If


        'Saco la Referencia bibliográfica del reactivo (si la hay) que consta de 3 campos
        Dim Referencia As String
        Referencia = ""
        'If (Not IsDBNull(misRegistros.Item("Libro"))) Then
        'Referencia = HttpUtility.HtmlDecode(misRegistros.Item("Libro")) + ", "
        'End If
        'If (Not IsDBNull(misRegistros.Item("Autor"))) Then
        'Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Autor")) + ", "
        'End If
        'If (Not IsDBNull(misRegistros.Item("Editorial"))) Then
        'Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Editorial"))
        'End If
        '***

        If Trim(res.Item("Libro").ToString <> "") Then
            Referencia = HttpUtility.HtmlDecode(res.Item("Libro")) + ", "
        End If
        If Trim(res.Item("Autor").ToString <> "") Then
            Referencia = Referencia + HttpUtility.HtmlDecode(res.Item("Autor")) + ", "
        End If
        If Trim(res.Item("Editorial").ToString <> "") Then
            Referencia = Referencia + HttpUtility.HtmlDecode(res.Item("Editorial"))
        End If
        '***
        If Trim(Referencia) <> "" Then
            LblReferencia.Text = "(" & Lang.FileSystem.Alumno.Contestar.HINT_REFERENCIA.Item(Session("Usuario_Idioma")) & " " & Referencia & ")"
        End If

        'Cargo el archivo 1 de apoyo si lo hay, siempre deberán estar en el directorio material
        If (Not IsDBNull(res.Item("TipoArchivoApoyo"))) And (Not IsDBNull(res.Item("ArchivoApoyo"))) Then
            If Trim(res.Item("ArchivoApoyo")).Length > 0 Then 'Esta comparacion la hago aparte del IF superior porque marca error al querer sacar tamaño a valor NULL
                If Trim(res.Item("TipoArchivoApoyo")) = "Imagen" Then
                    ImPlanteamiento.ImageUrl = Config.Global.urlMaterial & res.Item("ArchivoApoyo")
                    ImPlanteamiento.Visible = True
                    HLapoyoplanteamiento.Visible = False
                    pnlDespliegaPlanteamiento1.Visible = False
                ElseIf Trim(res.Item("TipoArchivoApoyo")) = "Audio" Then
                    pnlDespliegaPlanteamiento1.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:460px;overflow:hidden;")
                    pnlDespliegaPlanteamiento1.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & res.Item("ArchivoApoyo") + """></audio>"))
                ElseIf Trim(res.Item("TipoArchivoApoyo")) = "Video" Then
                    LblVideo.Text = "<embed src=""" & Config.Global.urlMaterial & res.Item("ArchivoApoyo") & """ width=""280"" height=""227"" autostart=""true"" controller=""true"" align=""center""></embed>"
                    ImPlanteamiento.Visible = False
                    HLapoyoplanteamiento.Visible = False
                    pnlDespliegaPlanteamiento1.Visible = False
                Else
                    'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                    '********PENDIENTE***************
                    HLapoyoplanteamiento.NavigateUrl = Config.Global.urlMaterial & res.Item("ArchivoApoyo")
                    HLapoyoplanteamiento.Text = Lang.FileSystem.Alumno.Contestar.LT_HINT_ALUMNO.Item(Session("Usuario_Idioma")) & " " + Trim(res.Item("ArchivoApoyo").ToString)
                    HLapoyoplanteamiento.Visible = True
                    ImPlanteamiento.Visible = False
                    pnlDespliegaPlanteamiento1.Visible = False
                End If
            Else
                ImPlanteamiento.Visible = False
                HLapoyoplanteamiento.Visible = False
                pnlDespliegaPlanteamiento1.Visible = False
            End If
        End If

        'Cargo el archivo 2 de apoyo si lo hay, siempre deberán estar en el directorio material
        If (Not IsDBNull(res.Item("TipoArchivoApoyo2"))) And (Not IsDBNull(res.Item("ArchivoApoyo2"))) Then
            If Trim(res.Item("ArchivoApoyo2")).Length > 0 Then 'Esta comparacion la hago aparte del IF superior porque marca error al querer sacar tamaño a valor NULL

                If Trim(res.Item("TipoArchivoApoyo2")) = "Imagen" Then
                    ImPlanteamiento2.ImageUrl = Config.Global.urlMaterial & res.Item("ArchivoApoyo2")
                    ImPlanteamiento2.Visible = True
                    HLapoyoplanteamiento2.Visible = False
                    pnlDespliegaPlanteamiento2.Visible = False
                ElseIf Trim(res.Item("TipoArchivoApoyo")) = "Audio" Then
                    pnlDespliegaPlanteamiento2.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:460px;overflow:hidden;")
                    pnlDespliegaPlanteamiento2.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & res.Item("ArchivoApoyo") + """></audio>"))
                ElseIf Trim(res.Item("TipoArchivoApoyo2")) = "Video" Then
                    LblVideo2.Text = "<embed src=""" & Config.Global.urlMaterial & res.Item("ArchivoApoyo2") + """ width=""280"" height=""227"" autostart=""true"" controller=""true"" align=""center""></embed>"
                    ImPlanteamiento2.Visible = False
                    HLapoyoplanteamiento2.Visible = False

                Else
                    HLapoyoplanteamiento2.NavigateUrl = Config.Global.urlMaterial & res.Item("ArchivoApoyo2")
                    HLapoyoplanteamiento2.Text = Lang.FileSystem.Alumno.Contestar.LT_HINT_ALUMNO.Item(Session("Usuario_Idioma")) & " " + Trim(res.Item("ArchivoApoyo2").ToString)
                    HLapoyoplanteamiento2.Visible = True
                    ImPlanteamiento2.Visible = False
                    pnlDespliegaPlanteamiento2.Visible = False
                End If
            Else
                ImPlanteamiento2.Visible = False
                HLapoyoplanteamiento2.Visible = False
                pnlDespliegaPlanteamiento2.Visible = False
            End If
        End If

        tipoRespuesta = res.Item("TipoRespuesta").trim()
        idPlanteamiento = res.Item("IdPlanteamiento")
        HfComparaMayusculas.Value = res.Item("ComparaMayusculas").ToString()
        HfComparaAcentos.Value = res.Item("ComparaAcentos").ToString()
        HfComparaSoloAlfanumerico.Value = res.Item("ComparaSoloAlfanumerico").ToString()
        HfPuntuacion.Value = res.Item("EliminaPuntuacion").ToString()
        Dim verifica As Boolean = res.Item("Verifica")
        If Not IsDBNull(res.Item("Ancho1")) Then
            ancho1 = res.Item("Ancho1")
        End If

        res.Close()
        cmd.Dispose()

        'AHORA, EN BASE AL TIPO DE REACTIVO MUESTRO LAS OPCIONES PARA RESPONDER
        If tipoRespuesta = "Multiple" Then
            'Algoritmo respuesta multiple
            PanelMultiple.Visible = True
            PanelAbierta.Visible = False

            'OPCIONES (Todos los tipos de respuestas generan Registros en la tabla "Opcion"
            cmd = New SqlCommand("SELECT * FROM Opcion where Esconder = 'False' and idPlanteamiento = @IdPlanteamiento order by Consecutiva", conn)
            cmd.Parameters.AddWithValue("@IdPlanteamiento", idPlanteamiento)
            da.SelectCommand = cmd
            da.Fill(dt)

            res = cmd.ExecuteReader() 'Creo conjunto de registros

            If Not (res.Read()) Then 'Si hay algún reactivo que no tiene opciones para contestar por el alumno, posiblemente será capturada aparte. Con un solo reactivo que esté así no podrá terminar la actividad, así que mejor no lo dejo que ingrese
                cmd.Dispose()
                res.Close()
                conn.Close()
                Session("Nota") = Lang.FileSystem.Alumno.Contestar.MSG_NO_HAY_REACTIVOS.Item(Session("Usuario_Idioma"))
                Response.Redirect("~/alumno/cursos/")
            End If


            arrayConsecutivo = CType(New Utils.RandomGenerator, Utils.RandomGenerator).getRandomCombination(arrayConsecutivo, evaluacionActual.OpcionesAleatorias, dt)

            Redaccion = Replace(HttpUtility.HtmlDecode(dt.Rows(arrayConsecutivo(0))("Redaccion")), "*salto*", "<BR>")
            Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
            TextoOpc1.Text = Redaccion
            Opcion1.Text = IIf(evaluacionActual.OpcionesAleatorias = 0, res.Item("Consecutiva"), String.Empty)
            Opcion1Aux.Text = dt.Rows(arrayConsecutivo(0))("Consecutiva")

            If Not IsDBNull(dt.Rows(arrayConsecutivo(0))("TipoArchivoApoyo")) Then

                If dt.Rows(arrayConsecutivo(0))("TipoArchivoApoyo") = "Imagen" Then
                    ImOpc1.ImageUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(0))("ArchivoApoyo")
                    HLapoyoopc1.Visible = False
                    PnlDespliegaOpc1.Visible = False
                ElseIf Trim(dt.Rows(arrayConsecutivo(0))("TipoArchivoApoyo")) = "Audio" Then
                    PnlDespliegaOpc1.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                    PnlDespliegaOpc1.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(0))("ArchivoApoyo") + """></audio>"))
                    ImOpc1.Visible = False
                    HLapoyoopc1.Visible = False
                Else
                    HLapoyoopc1.NavigateUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(0))("ArchivoApoyo")
                    HLapoyoopc1.Text = Lang.FileSystem.Alumno.Contestar.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                    ImOpc1.Visible = False
                    PnlDespliegaOpc1.Visible = False
                End If
            Else
                ImOpc1.Visible = False
                HLapoyoopc1.Visible = False
                PnlDespliegaOpc1.Visible = False
            End If

            If res.Read() Then 'OPCION 2

                Redaccion = Replace(HttpUtility.HtmlDecode(dt.Rows(arrayConsecutivo(1))("Redaccion")), "*salto*", "<BR>")
                Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                TextoOpc2.Text = Redaccion
                Opcion2.Text = IIf(evaluacionActual.OpcionesAleatorias = 0, res.Item("Consecutiva"), String.Empty)
                Opcion2Aux.Text = dt.Rows(arrayConsecutivo(1))("Consecutiva")

                If Not IsDBNull(dt.Rows(arrayConsecutivo(1))("TipoArchivoApoyo")) Then
                    If dt.Rows(arrayConsecutivo(1))("TipoArchivoApoyo") = "Imagen" Then
                        ImOpc2.ImageUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(1))("ArchivoApoyo")
                        HLapoyoopc2.Visible = False
                        PnlDespliegaOpc2.Visible = False
                    ElseIf Trim(dt.Rows(arrayConsecutivo(1))("TipoArchivoApoyo")) = "Audio" Then
                        PnlDespliegaOpc2.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                        PnlDespliegaOpc2.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(1))("ArchivoApoyo") + """></audio>"))
                        ImOpc2.Visible = False
                        HLapoyoopc2.Visible = False
                    Else
                        HLapoyoopc2.NavigateUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(1))("ArchivoApoyo")
                        HLapoyoopc2.Text = Lang.FileSystem.Alumno.Contestar.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                        ImOpc2.Visible = False
                        PnlDespliegaOpc2.Visible = False
                    End If
                Else
                    ImOpc2.Visible = False
                    HLapoyoopc2.Visible = False
                    PnlDespliegaOpc2.Visible = False
                End If
            Else
                ImOpc2.Visible = False
                HLapoyoopc2.Visible = False
                Opcion2.Visible = False
                PnlDespliegaOpc2.Visible = False
            End If

            If res.Read() Then 'OPCION 3

                Redaccion = Replace(HttpUtility.HtmlDecode(dt.Rows(arrayConsecutivo(2))("Redaccion")), "*salto*", "<BR>")
                Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                TextoOpc3.Text = Redaccion
                Opcion3.Text = IIf(evaluacionActual.OpcionesAleatorias = 0, res.Item("Consecutiva"), String.Empty)
                Opcion3Aux.Text = dt.Rows(arrayConsecutivo(2))("Consecutiva")

                If Not IsDBNull(dt.Rows(arrayConsecutivo(2))("TipoArchivoApoyo")) Then

                    If dt.Rows(arrayConsecutivo(2))("TipoArchivoApoyo") = "Imagen" Then
                        ImOpc3.ImageUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(2))("ArchivoApoyo")
                        HLapoyoopc3.Visible = False
                        PnlDespliegaOpc3.Visible = False
                    ElseIf Trim(dt.Rows(arrayConsecutivo(2))("TipoArchivoApoyo")) = "Audio" Then
                        PnlDespliegaOpc3.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                        PnlDespliegaOpc3.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(2))("ArchivoApoyo") + """></audio>"))
                        ImOpc3.Visible = False
                        HLapoyoopc3.Visible = False
                    Else
                        HLapoyoopc3.NavigateUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(2))("ArchivoApoyo")
                        HLapoyoopc3.Text = Lang.FileSystem.Alumno.Contestar.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                        ImOpc3.Visible = False
                        PnlDespliegaOpc3.Visible = False
                    End If
                Else
                    ImOpc3.Visible = False
                    HLapoyoopc3.Visible = False
                    PnlDespliegaOpc3.Visible = False
                End If
            Else
                ImOpc3.Visible = False
                HLapoyoopc3.Visible = False
                Opcion3.Visible = False
                PnlDespliegaOpc3.Visible = False
            End If

            If res.Read() Then 'OPCION 4

                Redaccion = Replace(HttpUtility.HtmlDecode(dt.Rows(arrayConsecutivo(3))(2)), "*salto*", "<BR>")
                Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                TextoOpc4.Text = Redaccion
                Opcion4.Text = IIf(evaluacionActual.OpcionesAleatorias = 0, res.Item("Consecutiva"), String.Empty)
                Opcion4Aux.Text = dt.Rows(arrayConsecutivo(3))("Consecutiva")

                If Not IsDBNull(dt.Rows(arrayConsecutivo(3))(8)) Then

                    If dt.Rows(arrayConsecutivo(3))("TipoArchivoApoyo") = "Imagen" Then
                        ImOpc4.ImageUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(3))("ArchivoApoyo")
                        HLapoyoopc4.Visible = False
                        PnlDespliegaOpc4.Visible = False
                    ElseIf Trim(dt.Rows(arrayConsecutivo(3))("TipoArchivoApoyo")) = "Audio" Then
                        PnlDespliegaOpc4.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                        PnlDespliegaOpc4.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(3))("ArchivoApoyo") + """></audio>"))
                        ImOpc4.Visible = False
                        HLapoyoopc4.Visible = False
                    Else
                        HLapoyoopc4.NavigateUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(3))("ArchivoApoyo")
                        HLapoyoopc4.Text = Lang.FileSystem.Alumno.Contestar.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                        ImOpc4.Visible = False
                        PnlDespliegaOpc4.Visible = False
                    End If
                Else
                    ImOpc4.Visible = False
                    HLapoyoopc4.Visible = False
                    PnlDespliegaOpc4.Visible = False
                End If
            Else
                ImOpc4.Visible = False
                HLapoyoopc4.Visible = False
                PnlDespliegaOpc4.Visible = False
                Opcion4.Visible = False
            End If

            If res.Read() Then 'PASO A OPCION 5

                Redaccion = Replace(HttpUtility.HtmlDecode(dt.Rows(arrayConsecutivo(4))("Redaccion")), "*salto*", "<BR>")
                Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                TextoOpc5.Text = Redaccion
                Opcion5.Text = IIf(evaluacionActual.OpcionesAleatorias = 0, res.Item("Consecutiva"), String.Empty)
                Opcion5Aux.Text = dt.Rows(arrayConsecutivo(4))("Consecutiva")

                If Not IsDBNull(dt.Rows(arrayConsecutivo(4))("TipoArchivoApoyo")) Then

                    If dt.Rows(arrayConsecutivo(4))(8) = "Imagen" Then
                        ImOpc5.ImageUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(4))("ArchivoApoyo")
                        HLapoyoopc5.Visible = False
                        PnlDespliegaOpc5.Visible = False
                    ElseIf Trim(dt.Rows(arrayConsecutivo(4))("TipoArchivoApoyo")) = "Audio" Then
                        PnlDespliegaOpc5.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                        PnlDespliegaOpc5.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(4))("ArchivoApoyo") + """></audio>"))
                        ImOpc5.Visible = False
                        HLapoyoopc5.Visible = False
                    Else
                        HLapoyoopc5.NavigateUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(4))("ArchivoApoyo")
                        HLapoyoopc5.Text = Lang.FileSystem.Alumno.Contestar.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                        ImOpc5.Visible = False
                        PnlDespliegaOpc5.Visible = False
                    End If
                Else
                    ImOpc5.Visible = False
                    HLapoyoopc5.Visible = False
                    PnlDespliegaOpc5.Visible = False
                End If
            Else
                ImOpc5.Visible = False
                HLapoyoopc5.Visible = False
                Opcion5.Visible = False
                PnlDespliegaOpc5.Visible = False
            End If

            If res.Read() Then 'PASO A OPCION 6

                Redaccion = Replace(HttpUtility.HtmlDecode(dt.Rows(arrayConsecutivo(5))("Redaccion")), "*salto*", "<BR>")
                Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                TextoOpc6.Text = Redaccion
                Opcion6.Text = IIf(evaluacionActual.OpcionesAleatorias = 0, res.Item("Consecutiva"), String.Empty)
                Opcion6Aux.Text = dt.Rows(arrayConsecutivo(5))("Consecutiva")

                If Not IsDBNull(dt.Rows(arrayConsecutivo(5))(8)) Then

                    If dt.Rows(arrayConsecutivo(5))("TipoArchivoApoyo") = "Imagen" Then
                        ImOpc6.ImageUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(5))("ArchivoApoyo")
                        HLapoyoopc6.Visible = False
                        PnlDespliegaOpc6.Visible = False
                    ElseIf Trim(dt.Rows(arrayConsecutivo(5))("TipoArchivoApoyo")) = "Audio" Then
                        PnlDespliegaOpc6.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                        PnlDespliegaOpc6.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(5))("ArchivoApoyo") + """></audio>"))
                        ImOpc6.Visible = False
                        HLapoyoopc6.Visible = False
                    Else
                        HLapoyoopc6.NavigateUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(5))("ArchivoApoyo")
                        HLapoyoopc6.Text = Lang.FileSystem.Alumno.Contestar.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                        ImOpc6.Visible = False
                        PnlDespliegaOpc6.Visible = False
                    End If
                Else
                    ImOpc6.Visible = False
                    HLapoyoopc6.Visible = False
                    PnlDespliegaOpc6.Visible = False
                End If
            Else
                ImOpc6.Visible = False
                HLapoyoopc6.Visible = False
                Opcion6.Visible = False
                PnlDespliegaOpc6.Visible = False
            End If

            '****HFidGrado.Value = IdGrado 'Solo funciona la asignacion si lo pongo aqui, al parecer es por el momento en que se crea el campo oculto

        ElseIf tipoRespuesta = "Abierta" Then 'de If Trim(res.Item("TipoRespuesta")) = "Multiple"
            'Algoritmo cuando el tipo de respuesta es diferente de "Multiple"
            HFidRespuestaAbierta.Value = ""
            GuardarA.Visible = False
            'OPCIONES (Todos los tipos de respuestas generan Registros en la tabla "Opcion")
            cmd = New SqlCommand("SELECT * FROM Opcion where Esconder = 'False' and idPlanteamiento = @IdPlanteamiento order by Consecutiva", conn)

            cmd.Parameters.AddWithValue("@IdPlanteamiento", idPlanteamiento)

            res = cmd.ExecuteReader() 'Creo conjunto de registros
            res.Read() 'Leo para poder accesarlos

            'Algoritmo respuesta abierta (solo existe un registro en Opcion asignado a la Respuesta
            PanelMultiple.Visible = False
            PanelAbierta.Visible = True
            'Como respuesta "Abierta" tiene una sola opcion, la genero por default
            OpcionL.Text = res.Item("Consecutiva") 'El texto está de color BLANCO para que no se vea
            OpcionL.Checked = True
            OpcionL.Visible = False

        ElseIf tipoRespuesta = "Abierta Calificada" Then
            tbRespuestaAbiertaAutomatica.Width = ancho1
            PanelMultiple.Visible = False
            PanelAbierta.Visible = False
            PanelAbiertaAutomatica.Visible = True
            GuardarA.Visible = False

            ' guardo las respuestas correctas de las opciones en el hfVerifica para comparar sin postback
            If verifica Then
                VerificarAA.Visible = True
                Using cmd2 As SqlCommand = New SqlCommand("SELECT Redaccion, Correcta FROM Opcion WHERE IdPlanteamiento = " & idPlanteamiento, conn)

                    Using results As SqlDataReader = cmd2.ExecuteReader()

                        While results.Read()
                            If results.Item("Correcta").ToString() = "S" Then
                                HfVerificaRespuesta.Value += results.Item("Redaccion") & "~|~"
                            End If
                        End While
                        If HfVerificaRespuesta.Value.Contains("~|~") Then
                            HfVerificaRespuesta.Value = HfVerificaRespuesta.Value.Substring(0, HfVerificaRespuesta.Value.Length - 3)
                        End If
                    End Using
                End Using
            Else
                VerificarAA.Visible = False
            End If
        End If

        res.Close()
        cmd.Dispose()
        conn.Close()
    End Sub

  ' imprimo los estilos variables de ésta página
  Protected Sub OutputCss()
    Dim s As StringBuilder = New StringBuilder()
    s.Append("<style type='text/css'>")
    s.Append("  #menu .current_area_cursos a {") ' esta clase css debe corresponder a la clase del boton a resaltar
    s.Append("    border-bottom: 5px solid " & Config.Color.MenuBar_SelectedOption_BorderColor & ";")
    s.Append("  }")
    s.Append("  .AssignmentHeaderRow {")
    s.Append("    background-color: " & Config.Color.MenuPendientes_Subtitle & ";")
    s.Append("  }")
    s.Append("</style>")
    ltEstilos.Text += s.ToString()
  End Sub

#End Region

  ' reporta true si hay respuestas abiertas guardadas por editar; prepara la página para editar la primer respuesta del siguiente intento
  Private Function contestarGuardadas(ByRef conn As SqlConnection, ByVal idEvaluacion As String) As Boolean
    Dim r As Boolean = False ' resultado de la función

    ' revizar si para la actividad y alumno actual hay respuestas abiertas guardadas, que no esten en la variable de sesion de guardadasMostradas
    ' si hay, mostrar la primera y reportar true
    ' si no hay, reportar false

    If Session("guardadasMostradas") = "" Then
      Session("guardadasMostradas") = "-1"
    End If

    Dim resultados As SqlDataReader
    Dim cmd As SqlCommand = New SqlCommand(
"SELECT " &
"   RA.IdRespuestaA, " &
"   RA.Redaccion RedaccionRespuesta, " &
"   R.IdCicloEscolar, " &
"   R.IdDetalleEvaluacion, " &
"   P.Consecutivo, " &
"   P.IdPlanteamiento, " &
"   P.Redaccion, " &
"   P.ArchivoApoyo, " &
"   P.TipoArchivoApoyo, " &
"   P.ArchivoApoyo2, " &
"   P.TipoArchivoApoyo2, " &
"   P.Libro, " &
"   P.Autor, " &
"   P.Editorial, " &
"   P.Tiempo, " &
"   P.Minutos, " &
"   P.EsconderTexto, " &
"   O.Consecutiva, " &
"   S.IdSubtema, " &
"   S.Descripcion, " &
"   E.ClaveBateria " &
"From " &
"   Respuesta R, " &
"   DetalleEvaluacion DE, " &
"   Planteamiento P, " &
"   Subtema S, " &
"   Evaluacion E, " &
"   Opcion O, " &
"   (SELECT * FROM RespuestaAbierta WHERE Estado = 'Borrador') As RA " &
"WHERE " &
"   R.IdRespuesta = RA.IdRespuesta " &
"   AND R.IdAlumno = @Idalumno " &
"   AND DE.IdEvaluacion = @IdEvaluacion " &
"   AND DE.IdDetalleEvaluacion = R.IdDetalleEvaluacion " &
"   AND P.IdPlanteamiento = R.IdPlanteamiento " &
"   AND P.IdSubtema = S.IdSubtema " &
"   AND E.IdEvaluacion = DE.IdEvaluacion " &
"   AND RA.IdRespuestaA NOT IN (" & Session("guardadasMostradas") & ") " &
"   AND O.IdPlanteamiento = P.IdPlanteamiento", conn)

    cmd.Parameters.AddWithValue("@IdAlumno", Session("Usuario_IdAlumno"))
    cmd.Parameters.AddWithValue("@IdEvaluacion", idEvaluacion)

    resultados = cmd.ExecuteReader()

    If resultados.Read() Then
      r = True
      ' la siguiente nota la preparo para modificar la redirección cuando termine la edición de las respeustas,
      ' puesto que normalmente mandaría a "termiandas.aspx" informando que ya se había terminado la actividad
      Session("Nota") = Lang.FileSystem.Alumno.Contestar.MSG_TERMINO_EDITAR_GUARDADAS.Item(Session("Usuario_Idioma"))

      ' añade la respuesta abierta guardada actual a la lista de las que ya mostré en esta ejecución de actividad
      Session("guardadasMostradas") = Session("guardadasMostradas") & "," & resultados("IdRespuestaA")

      PageTitle_v1.show(Lang.FileSystem.Alumno.Contestar.LT_INICIO_TITULO.Item(Session("Usuario_Idioma")) & " " & resultados("ClaveBateria"),
                          titleIcon)
      LblFaltan.Text = Lang.FileSystem.Alumno.Contestar.LT_FALTAN_SUBTEMA.Item(Session("Usuario_Idioma")) & " " & resultados("Descripcion")

      'Campos que pasaré a la pagina PROCESA.ASPX
      HFcicloescolar.Value = resultados("IdCicloEscolar")
      HFdetalleEvaluacion.Value = resultados("IdDetalleEvaluacion")
      HFidRespuestaAbierta.Value = resultados("IdRespuestaA")

      'Los siguientes campos ocultos solo funcionan si los pongo aqui
      HFidEvaluacion.Value = idEvaluacion 'Este campo es para pasarlo al script PROCESA.ASPX
      HFidPlanteamiento.Value = resultados.Item("IdPlanteamiento") 'Este campo es para pasarlo al script PROCESA.ASPX

      Dim Redaccion As String
      If resultados.Item("EsconderTexto") Then
        LblPlanteamiento.Visible = False
      Else
        LblPlanteamiento.Visible = True
        Redaccion = Replace(HttpUtility.HtmlDecode(resultados.Item("Redaccion")), "*salto*", "<BR>")
        Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
        LblPlanteamiento.Text = Redaccion
        If resultados.Item("Consecutivo") > 0 Then
          LblConsecutivo.Text = CStr(resultados.Item("Consecutivo")) + ".-&nbsp;"
        Else
          'OJO: Aqui, en el despliegue para alumnos, tendria que usar un contador que enumere el planteamiento
          LblConsecutivo.Text = "#.-&nbsp;"
        End If
      End If

      'Ahora veo si es de tiempo:
      If resultados.Item("Tiempo") Then
        Timer1.Interval = resultados.Item("Minutos") * 60 * 1000
        Timer1.Enabled = True
        LblAlumno.Text = LblAlumno.Text + "<br><font color='#79CDCD'> " & Lang.FileSystem.Alumno.Contestar.MSG_MINUTOS_INICIO.Item(Session("Usuario_Idioma")) & " " &
            resultados.Item("Minutos").ToString + " " & Lang.FileSystem.Alumno.Contestar.MSG_MINUTOS_FIN.Item(Session("Usuario_Idioma")) & "</font>"
      End If


      'Saco la Referencia bibliográfica del reactivo (si la hay) que consta de 3 campos
      Dim Referencia As String
      Referencia = ""
      'If (Not IsDBNull(misRegistros.Item("Libro"))) Then
      'Referencia = HttpUtility.HtmlDecode(misRegistros.Item("Libro")) + ", "
      'End If
      'If (Not IsDBNull(misRegistros.Item("Autor"))) Then
      'Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Autor")) + ", "
      'End If
      'If (Not IsDBNull(misRegistros.Item("Editorial"))) Then
      'Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Editorial"))
      'End If
      '***

      If Trim(resultados.Item("Libro").ToString <> "") Then
        Referencia = HttpUtility.HtmlDecode(resultados.Item("Libro")) + ", "
      End If
      If Trim(resultados.Item("Autor").ToString <> "") Then
        Referencia = Referencia + HttpUtility.HtmlDecode(resultados.Item("Autor")) + ", "
      End If
      If Trim(resultados.Item("Editorial").ToString <> "") Then
        Referencia = Referencia + HttpUtility.HtmlDecode(resultados.Item("Editorial"))
      End If
      '***
      If Trim(Referencia) <> "" Then
        LblReferencia.Text = "(" & Lang.FileSystem.Alumno.Contestar.HINT_REFERENCIA.Item(Session("Usuario_Idioma")) & " " + Referencia + ")"
      End If

      'Cargo el archivo 1 de apoyo si lo hay, siempre deberán estar en el directorio material
      If (Not IsDBNull(resultados.Item("TipoArchivoApoyo"))) And (Not IsDBNull(resultados.Item("ArchivoApoyo"))) Then
        If Trim(resultados.Item("ArchivoApoyo")).Length > 0 Then 'Esta comparacion la hago aparte del IF superior porque marca error al querer sacar tamaño a valor NULL
          If Trim(resultados.Item("TipoArchivoApoyo")) = "Imagen" Then
            'ImPlanteamiento.Width = 300
            'ImPlanteamiento.Height = 300
            ImPlanteamiento.ImageUrl = Config.Global.urlMaterial & resultados.Item("ArchivoApoyo")
            ImPlanteamiento.Visible = True
            HLapoyoplanteamiento.Visible = False
          ElseIf Trim(resultados.Item("TipoArchivoApoyo")) = "Video" Or Trim(resultados.Item("TipoArchivoApoyo")) = "Audio" Then
            LblVideo.Text = "<embed src=""" & Config.Global.urlMaterial & resultados.Item("ArchivoApoyo") + """ width=""280"" height=""227"" autostart=""true"" controller=""true"" align=""center""></embed>"
            ImPlanteamiento.Visible = False
            HLapoyoplanteamiento.Visible = False
          Else
            'Algoritmo cuando el archivo de apoyo no es imagen, será liga
            '********PENDIENTE***************
            HLapoyoplanteamiento.NavigateUrl = Config.Global.urlMaterial & resultados.Item("ArchivoApoyo")
            'HLapoyoplanteamiento.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
            HLapoyoplanteamiento.Text = Lang.FileSystem.Alumno.Contestar.HINT_APOYO.Item(Session("Usuario_Idioma")) & " " + Trim(resultados.Item("ArchivoApoyo").ToString)
            HLapoyoplanteamiento.Visible = True
            ImPlanteamiento.Visible = False
          End If
        Else
          ImPlanteamiento.Visible = False
          HLapoyoplanteamiento.Visible = False
        End If
      End If

      'Cargo el archivo 2 de apoyo si lo hay, siempre deberán estar en el directorio material
      If (Not IsDBNull(resultados.Item("TipoArchivoApoyo2"))) And (Not IsDBNull(resultados.Item("ArchivoApoyo2"))) Then
        If Trim(resultados.Item("ArchivoApoyo2")).Length > 0 Then 'Esta comparacion la hago aparte del IF superior porque marca error al querer sacar tamaño a valor NULL
          If Trim(resultados.Item("TipoArchivoApoyo2")) = "Imagen" Then
            'ImPlanteamiento2.Width = 300
            'ImPlanteamiento2.Height = 300
            ImPlanteamiento2.ImageUrl = Config.Global.urlMaterial & resultados.Item("ArchivoApoyo2")
            ImPlanteamiento2.Visible = True
            HLapoyoplanteamiento2.Visible = False
          ElseIf Trim(resultados.Item("TipoArchivoApoyo2")) = "Video" Or Trim(resultados.Item("TipoArchivoApoyo2")) = "Audio" Then
            LblVideo2.Text = "<embed src=""" & Config.Global.urlMaterial & resultados.Item("ArchivoApoyo2") + """ width=""280"" height=""227"" autostart=""true"" controller=""true"" align=""center""></embed>"
            ImPlanteamiento2.Visible = False
            HLapoyoplanteamiento2.Visible = False
          Else
            'Algoritmo cuando el archivo de apoyo no es imagen, será liga
            '********PENDIENTE***************
            HLapoyoplanteamiento2.NavigateUrl = Config.Global.urlMaterial & resultados.Item("ArchivoApoyo2")
            'HLapoyoplanteamiento2.Text = "HAGA CLIC AQUÍ PARA OBTENER<BR> MATERIAL DE APOYO"
            HLapoyoplanteamiento2.Text = Lang.FileSystem.Alumno.Contestar.HINT_APOYO.Item(Session("Usuario_Idioma")) & " " + Trim(resultados.Item("ArchivoApoyo2").ToString)
            HLapoyoplanteamiento2.Visible = True
            ImPlanteamiento2.Visible = False
          End If
        Else
          ImPlanteamiento2.Visible = False
          HLapoyoplanteamiento2.Visible = False
        End If
      End If

      TBrespuestaA.Text = resultados.Item("RedaccionRespuesta")
      ' ahora configuro para tipo de RESPUESTA ABIERTA:
      'OPCIONES (Todos los tipos de respuestas generan Registros en la tabla "Opcion")
      cmd.CommandText = "SELECT * FROM Opcion where Esconder = 'False' and idPlanteamiento = " + resultados.Item("IdPlanteamiento").ToString + " order by Consecutiva"
      resultados.Close() 'Cierro la tabla de Planteamiento para poder usar este objeto de nuevo
      resultados = cmd.ExecuteReader() 'Creo conjunto de registros
      resultados.Read() 'Leo para poder accesarlos

      'Algoritmo respuesta abierta (solo existe un registro en Opcion asignado a la Respuesta
      PanelMultiple.Visible = False
      PanelAbierta.Visible = True
      'Como respuesta "Abierta" tiene una sola opcion, la genero por default
      OpcionL.Text = resultados.Item("Consecutiva") 'El texto está de color BLANCO para que no se vea
      OpcionL.Checked = True
      OpcionL.Visible = False

    End If

    resultados.Close()
    cmd.Dispose()

    Return r
  End Function

  'Public ReadOnly Property EspecificaPlantel() As String
  '    Get
  '        Return HFplantel.Value
  '    End Get
  'End Property

  'Public ReadOnly Property PlanteamientoActual() As String
  '    Get
  '        Return HFidPlanteamiento.Value
  '    End Get
  'End Property

  'Public ReadOnly Property EvaluacionActual() As String
  '    Get
  '        Return HFidEvaluacion.Value
  '    End Get
  'End Property

  'Public ReadOnly Property BotonActivado() As String
  '    Get
  '        Return HFboton.Value
  '    End Get
  'End Property

  'Public ReadOnly Property CicloEscolarActual() As String
  '    Get
  '        Return HFcicloescolar.Value
  '    End Get
  'End Property

  'Public ReadOnly Property DetalleEvaluacionActual() As String
  '    Get
  '        Return HFdetalleEvaluacion.Value
  '    End Get
  'End Property

  'Public ReadOnly Property BrincarPregunta() As String
  '    Get
  '        Return HFBrincar.Value
  '    End Get
  'End Property

  'Public ReadOnly Property RespuestaSeleccionada() As String
  '    Get
  '        Return getSelected()
  '    End Get
  'End Property

  Public Function getSelected() As String
        If Opcion1.Checked Then
            Return Opcion1Aux.Text
        ElseIf Opcion2.Checked Then
            Return Opcion2Aux.Text
        ElseIf Opcion3.Checked Then
            Return Opcion3Aux.Text
        ElseIf Opcion4.Checked Then
            Return Opcion4Aux.Text
        ElseIf Opcion5.Checked Then
            Return Opcion5Aux.Text
        ElseIf Opcion6.Checked Then
            Return Opcion6Aux.Text
        ElseIf OpcionL.Checked Then
            Return OpcionL.Text
    End If

    Return "0"
  End Function

    'Public ReadOnly Property GradoActual() As String
    '    Get
    '        Return HFidGrado.Value
    '    End Get
    'End Property

    'Public ReadOnly Property RespuestaAbierta() As String
    '    Get
    '        Return TBrespuestaA.Text
    '        'Dim RespuestaA = Trim(TBrespuestaA.Text) 'Evité el: HttpUtility.HtmlEncode(
    '        'RespuestaA = Replace(RespuestaA, "'", "´") 'Quito los apóstrofes porque marcaría error al momento de construir la siguiente consulta
    '        ''NO FUNCIONA, MARCA ERROR DESDE ANTES:
    '        ''RespuestaA = Replace(RespuestaA, "<", "[") 
    '        ''RespuestaA = Replace(RespuestaA, ">", "]") 
    '        'Return RespuestaA
    '    End Get
    'End Property

    Protected Sub Aceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Aceptar.Click
        Session("arrayConsecutivo") = Nothing
        Context.Items.Add("BotonActivado", Aceptar.ID)
        Context.Items.Add("RespuestaSeleccionada", getSelected())
        Context.Items.Add("PlanteamientoActual", HFidPlanteamiento.Value.Trim())
        Context.Items.Add("CicloEscolarActual", HFcicloescolar.Value.Trim())
        Context.Items.Add("DetalleEvaluacionActual", HFdetalleEvaluacion.Value.Trim())
        Context.Items.Add("RespuestaAbierta", TBrespuestaA.Text.Trim())
        Context.Items.Add("idRespuestaGuardada", HFidRespuestaAbierta.Value.Trim())
        Server.Transfer("~/alumno/cursos/contestar/procesa/Default.aspx")
        'HFboton.Value = Aceptar.ID 'HFboton es un Campo Oculto donde guardo el Nombre del boton para saber cual se oprimió
        'En la Propiedad PostBackURL del boton ejecuto el script Procesa.aspx              
    End Sub

    Protected Sub Cancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cancelar.Click
    Context.Items.Add("BotonActivado", Cancelar.ID)
    Context.Items.Add("RespuestaSeleccionada", getSelected())
    Context.Items.Add("PlanteamientoActual", HFidPlanteamiento.Value.Trim())
    Context.Items.Add("CicloEscolarActual", HFcicloescolar.Value.Trim())
    Context.Items.Add("DetalleEvaluacionActual", HFdetalleEvaluacion.Value.Trim())
    Context.Items.Add("RespuestaAbierta", TBrespuestaA.Text.Trim())
    Context.Items.Add("idRespuestaGuardada", HFidRespuestaAbierta.Value.Trim())
    Server.Transfer("~/alumno/cursos/contestar/procesa/Default.aspx")
    'HFboton.Value = Cancelar.ID
  End Sub

  Protected Sub Brincar_Click(sender As Object, e As EventArgs) Handles Brincar.Click
    Context.Items.Add("BotonActivado", Brincar.ID)
    Context.Items.Add("RespuestaSeleccionada", getSelected())
    Context.Items.Add("PlanteamientoActual", HFidPlanteamiento.Value.Trim())
    Context.Items.Add("CicloEscolarActual", HFcicloescolar.Value.Trim())
    Context.Items.Add("DetalleEvaluacionActual", HFdetalleEvaluacion.Value.Trim())
    Context.Items.Add("RespuestaAbierta", TBrespuestaA.Text.Trim())
    Context.Items.Add("idRespuestaGuardada", HFidRespuestaAbierta.Value.Trim())
    Server.Transfer("~/alumno/cursos/contestar/procesa/Default.aspx")
    'HFboton.Value = Brincar.ID 'HFboton es un Campo Oculto donde guardo el Nombre del boton para saber cual se oprimió
  End Sub

  Protected Sub CancelarA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CancelarA.Click
    Context.Items.Add("BotonActivado", CancelarA.ID)
    Context.Items.Add("RespuestaSeleccionada", getSelected())
    Context.Items.Add("PlanteamientoActual", HFidPlanteamiento.Value.Trim())
    Context.Items.Add("CicloEscolarActual", HFcicloescolar.Value.Trim())
    Context.Items.Add("DetalleEvaluacionActual", HFdetalleEvaluacion.Value.Trim())
    Context.Items.Add("RespuestaAbierta", TBrespuestaA.Text.Trim())
    Context.Items.Add("idRespuestaGuardada", HFidRespuestaAbierta.Value.Trim())
    Server.Transfer("~/alumno/cursos/contestar/procesa/Default.aspx")
    'HFboton.Value = CancelarA.ID
  End Sub

  Protected Sub AceptarA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles AceptarA.Click
    Context.Items.Add("BotonActivado", Aceptar.ID)
    Context.Items.Add("RespuestaSeleccionada", getSelected())
    Context.Items.Add("PlanteamientoActual", HFidPlanteamiento.Value.Trim())
    Context.Items.Add("CicloEscolarActual", HFcicloescolar.Value.Trim())
    Context.Items.Add("DetalleEvaluacionActual", HFdetalleEvaluacion.Value.Trim())
    Context.Items.Add("RespuestaAbierta", TBrespuestaA.Text.Trim())
    Context.Items.Add("idRespuestaGuardada", HFidRespuestaAbierta.Value.Trim())
    Server.Transfer("~/alumno/cursos/contestar/procesa/Default.aspx")
    'HFboton.Value = Aceptar.ID 'HFboton es un Campo Oculto donde guardo el Nombre del boton para saber cual se oprimió
    'En la Propiedad PostBackURL del boton ejecuto el script Procesa.aspx
  End Sub

  Protected Sub GuardarA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles GuardarA.Click
    Context.Items.Add("BotonActivado", GuardarA.ID)
    Context.Items.Add("RespuestaSeleccionada", getSelected())
    Context.Items.Add("PlanteamientoActual", HFidPlanteamiento.Value.Trim())
    Context.Items.Add("CicloEscolarActual", HFcicloescolar.Value.Trim())
    Context.Items.Add("DetalleEvaluacionActual", HFdetalleEvaluacion.Value.Trim())
    Context.Items.Add("RespuestaAbierta", TBrespuestaA.Text.Trim())
    Context.Items.Add("idRespuestaGuardada", HFidRespuestaAbierta.Value.Trim())
    Server.Transfer("~/alumno/cursos/contestar/procesa/Default.aspx")
    'HFboton.Value = GuardarA.ID 'HFboton es un Campo Oculto donde guardo el Nombre del boton para saber cual se oprimió
    'En la Propiedad PostBackURL del boton ejecuto el script Procesa.aspx
  End Sub

  Protected Sub AceptarAA_Click(sender As Object, e As EventArgs) Handles AceptarAA.Click
    Context.Items.Add("BotonActivado", AceptarA.ID)

    ' obtengo la respuesta 
        Context.Items.Add("RespuestaSeleccionada", evaluaRespuestaAbierta())
    Context.Items.Add("PlanteamientoActual", HFidPlanteamiento.Value.Trim())
    Context.Items.Add("CicloEscolarActual", HFcicloescolar.Value.Trim())
    Context.Items.Add("DetalleEvaluacionActual", HFdetalleEvaluacion.Value.Trim())
    Context.Items.Add("RespuestaAbierta", tbRespuestaAbiertaAutomatica.Text.Trim())
    Context.Items.Add("idRespuestaGuardada", HFidRespuestaAbierta.Value.Trim())
    Server.Transfer("~/alumno/cursos/contestar/procesa/Default.aspx")
  End Sub

  Protected Sub CancelarAA_Click(sender As Object, e As EventArgs) Handles CancelarAA.Click
    Context.Items.Add("BotonActivado", CancelarA.ID)
    Context.Items.Add("RespuestaSeleccionada", evaluaRespuestaAbierta())
    Context.Items.Add("PlanteamientoActual", HFidPlanteamiento.Value.Trim())
    Context.Items.Add("CicloEscolarActual", HFcicloescolar.Value.Trim())
    Context.Items.Add("DetalleEvaluacionActual", HFdetalleEvaluacion.Value.Trim())
    Context.Items.Add("RespuestaAbierta", tbRespuestaAbiertaAutomatica.Text.Trim())
    Context.Items.Add("idRespuestaGuardada", HFidRespuestaAbierta.Value.Trim())
    Server.Transfer("~/alumno/cursos/contestar/procesa/Default.aspx")
  End Sub

  Protected Function evaluaRespuestaAbierta() As String
    Dim respuesta As String = tbRespuestaAbiertaAutomatica.Text
    Dim resultado As String = "-" ' pretende inicialmente que no coincidió con ninguna respuesta correcta
    Dim comparaMayusculas As Boolean = Boolean.Parse(HfComparaMayusculas.Value)
    Dim comparaAcentos As Boolean = Boolean.Parse(HfComparaAcentos.Value)
        Dim comparaSoloAlfanumerico As Boolean = Boolean.Parse(HfComparaSoloAlfanumerico.Value)
        Dim eliminaPuntuacion As Boolean = Boolean.Parse(HfPuntuacion.Value)

    Try
      Using conn As SqlConnection = New SqlConnection(ConfigurationManager.
                                                      ConnectionStrings("sadcomeConnectionString").
                                                      ConnectionString)
        conn.Open()
        Using cmd As SqlCommand = New SqlCommand("SELECT * FROM Opcion Where IdPlanteamiento = @IdPlanteamiento", conn)

          cmd.Parameters.AddWithValue("@IdPlanteamiento", HFidPlanteamiento.Value)

          Using results As SqlDataReader = cmd.ExecuteReader()

            While (results.Read())
                            If (Siget.Utils.StringUtils.ComparaRespuesta(Regex.Replace(HttpUtility.HtmlDecode(results.Item("Redaccion").ToString()), "<[^>]*(>|$)", String.Empty),
                                                                         respuesta,
                                                                         comparaMayusculas,
                                                                         comparaAcentos,
                                                                         comparaSoloAlfanumerico,
                                                                         eliminaPuntuacion
                                                                         ) = 0) Then
                                resultado = results.Item("Consecutiva")
                                Exit While
                            End If
                        End While
          End Using
        End Using
      End Using
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      Return False
    End Try

    Return resultado
  End Function

  Protected Sub IBrenunciar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles IBrenunciar.Click
    Try
      'ESTA FUNCIÓN SOLO OCURRE EN LA PRIMERA VUELTA
      Dim strConexion As String
      'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
      strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString

      Dim objConexion As New SqlConnection(strConexion)
      Dim miComando As SqlCommand
      Dim misRegistros As SqlDataReader
      miComando = objConexion.CreateCommand
      miComando.CommandTimeout = 200
      'SUMO EL TOTAL DE PUNTOS QUE DA LA EVALUACION Y LA CANTIDAD DE REACTIVOS QUE LA COMPONEN
      miComando.CommandText = "select ISNULL(Sum(P.Ponderacion),0.0) Puntos, ISNULL(Count(P.Ponderacion),0.0) Cant " &
          " from Planteamiento P, DetalleEvaluacion D " &
          " where(D.IdEvaluacion = " + HFidEvaluacion.Value.ToString + ")" &
          " and P.IdSubtema = D.IdSubtema and P.Ocultar = 0"

      objConexion.Open() 'Abro la conexion
      misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
      misRegistros.Read()
      Dim TotalPuntos = misRegistros.Item("Puntos")
      Dim TotalReactivos = misRegistros.Item("Cant")

      'CHECO CUANTOS PUNTOS SUMO EL ALUMNO CORRECTAMENTE EN EL PRIMER INTENTO
      misRegistros.Close()
      miComando.CommandText = "select Sum(P.Ponderacion) Puntos, Count(Ponderacion) Cant " &
          "from Respuesta R, Planteamiento P, DetalleEvaluacion D " &
          "where (R.IdPlanteamiento = P.IdPlanteamiento) and P.Ocultar = 0 " &
          "and R.IdAlumno = " + CStr(Session("Usuario_IdAlumno")) + " and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion " &
          "and D.IdEvaluacion  = " + HFidEvaluacion.Value.ToString + " and R.Acertada = 'S' and R.Intento = 1"
      misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
      misRegistros.Read()

      Dim PuntosOk, ReactivosAcertados As Decimal
      If IsDBNull(misRegistros.Item("Puntos")) Then
        PuntosOk = 0
        ReactivosAcertados = 0
      Else
        PuntosOk = misRegistros.Item("Puntos")
        ReactivosAcertados = misRegistros.Item("Cant")
      End If
      misRegistros.Close()

      'CALCULO EL RESULTADO
      Dim Resultado As Decimal
      If TotalPuntos > 0 Then
        Resultado = (PuntosOk / TotalPuntos) * 100

        'BUSCO LA PENALIZACIÓN GENERAL
        miComando.CommandText = "Select * from Evaluacion where IdEvaluacion = " + HFidEvaluacion.Value.ToString
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        misRegistros.Read()
        Dim FinSinPenalizacion = misRegistros.Item("FinSinPenalizacion")
        Dim Penalizacion = misRegistros.Item("Penalizacion")
        misRegistros.Close()

        'BUSCO LA PENALIZACION POR PLANTEL, QUE TIENE PRIORIDAD SOBRE LA GENERAL
        miComando.CommandText = "select FinSinPenalizacion from EvaluacionPlantel where IdEvaluacion = " + HFidEvaluacion.Value.ToString + " and IdPlantel in (select IdPlantel from Alumno where IdAlumno = " + CStr(Session("Usuario_IdAlumno")) + ")"
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        If misRegistros.Read() Then
          FinSinPenalizacion = misRegistros.Item("FinSinPenalizacion")
        End If
        misRegistros.Close()

        'BUSCO LA PENALIZACION POR ALUMNO, QUE TIENE PRIORIDAD SOBRE LA DE PLANTE
        miComando.CommandText = "select FinSinPenalizacion from EvaluacionAlumno where IdEvaluacion = " + HFidEvaluacion.Value.ToString + " and IdAlumno = " + CStr(Session("Usuario_IdAlumno"))
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        If misRegistros.Read() Then
          FinSinPenalizacion = misRegistros.Item("FinSinPenalizacion")
        End If
        misRegistros.Close()
        objConexion.Close()

        'Checo que si contestó después de la fecha límite, lo que le aplica una penalización
        If Date.Now() > FinSinPenalizacion Then
          Resultado = Resultado - (Resultado * Penalizacion / 100)
        End If
      Else
        Resultado = 0
      End If

      'Inserto el registro de la evaluacion terminada
      SDSevaluacionesterminadas.InsertCommand = "SET dateformat dmy; INSERT INTO EvaluacionTerminada(IdAlumno,IdGrado,IdGrupo,IdCicloEscolar,IdEvaluacion,TotalReactivos,ReactivosContestados,PuntosPosibles,PuntosAcertados,Resultado,FechaTermino) " &
      "VALUES(" + CStr(Session("Usuario_IdAlumno")) + "," + CStr(Session("Usuario_IdGrado")) + "," &
          CStr(Session("Usuario_IdGrupo")) + "," + CStr(HFcicloescolar.Value) + "," &
          CStr(HFidEvaluacion.Value) + "," + CStr(TotalReactivos) + "," &
          CStr(ReactivosAcertados) + "," + CStr(FormatNumber(TotalPuntos, 2)) + "," &
          CStr(FormatNumber(PuntosOk, 2)) + "," + CStr(FormatNumber(Resultado, 2)) + ",cast(getdate() as smalldatetime))"
      'puedo usar tambien: String.Format("{0:c}", Resultado) para que salga con simbolo y todo
      SDSevaluacionesterminadas.Insert()
      Session("Nota") = Lang.FileSystem.Alumno.Contestar.MSG_RENUNCIADO.Item(Session("Usuario_Idioma"))
      msgError.hide()
      Response.Redirect("~/alumno/cursos/")
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  Protected Sub SDSresp_Inserted(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSresp.Inserted
    'Este procedimiento se ejecuta cada que se inseta un registro en la tabla Respuesta
    If Trim(Session("TipoResp")) = "Abierta" Then
      Dim nuevoId = e.Command.Parameters("@NuevoId").Value
      SDSrespuestasabiertas.InsertCommand = "SET dateformat dmy; INSERT INTO RespuestaAbierta(IdRespuesta,Redaccion) VALUES(@IdRespuesta, @Redaccion)"

      SDSrespuestasabiertas.InsertParameters.Add("IdRespuesta", nuevoId.ToString)
      SDSrespuestasabiertas.InsertParameters.Add("Redaccion", TBrespuestaA.Text)

      SDSrespuestasabiertas.Insert()
    End If
  End Sub

  Protected Sub Timer1_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.PreRender
    'Esto se ejecuta antes de que el Timer se ejecute
    Session("IdPlant") = HFidPlanteamiento.Value
  End Sub

  Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    'ALGORITMO PARA ALMACENAR LA RESPUESTA DADA CUANDO VENCIÓ EL TIEMPO. AL PARECER SE EJECUTA YA QUE SE CARGÓ LA PÁGINA DONDE ESTÁ COLOCADO EL TIMER (DESPUES DE VENCER EL TIEMPO)
    'PASO 1) Obtengo los datos para ingresar la respuesta
    'ANTES IDENTIFICAR QUE SI NO ES ALUMNO (ES ADMINISTRADOR) NO EJECUTE ESTE CODIGO
    Dim Usuario = User.Identity.Name
    Dim strConexion As String
    'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
    strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
    Dim objConexion As New SqlConnection(strConexion)
    Dim miComando As SqlCommand
    Dim misRegistros As SqlDataReader

    miComando = objConexion.CreateCommand
    'El usuario de cada Usuario es único en el sistema
    miComando.CommandText = "SELECT A.IdAlumno, G.IdGrupo, G.IdGrado FROM Usuario U, Alumno A, Grupo G where U.Login = '" + Usuario + "' and A.IdUsuario = U.IdUsuario and G.IdGrupo = A.IdGrupo"

    Try
      'Abrir la conexión y leo los registros del Planteamiento
      objConexion.Open()
      misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
      misRegistros.Read() 'Leo para poder accesarlos
      Dim IdAlumno = Long.Parse(CStr(misRegistros.Item("IdAlumno")).Trim())
      Dim IdGrado = Integer.Parse(CStr(misRegistros.Item("IdGrado")).Trim())
      Dim IdGrupo = Integer.Parse(CStr(misRegistros.Item("IdGrupo")).Trim())
      Dim IdPlanteamiento = Integer.Parse(CStr(Session("IdPlant")).Trim()) 'Si elijo HFidPlanteamiento.Value me pone el valor del siguiente reactivo que salió
      Dim Consecutiva = "0"
      If getSelected() <> "0" Then
        Consecutiva = getSelected()
      End If

      Dim IdCicloEscolar = HFcicloescolar.Value
      Dim IdDetalleEvaluacion = HFdetalleEvaluacion.Value
      misRegistros.Close()

      'PASO 2) Obtengo el Id de la Opcion
      'ANTES: miComando.CommandText = "select IdOpcion, Consecutiva, Correcta from Opcion where idPlanteamiento = " + IdPlanteamiento + " and Consecutiva = '" + Consecutiva + "'"
      'Ahora saco tambien en TipoRespuesta (Multiple, Abierta o Completar)
      miComando.CommandText = "select P.TipoRespuesta, O.IdOpcion, O.Consecutiva, O.Correcta, O.SeCalifica from Opcion O, Planteamiento P where P.IdPlanteamiento = O.IdPlanteamiento and O.idPlanteamiento = @IdPlanteamiento and O.Consecutiva = @Consecutiva"
      miComando.Parameters.AddWithValue("@IdPlanteamiento", IdPlanteamiento.ToString.Trim())
      miComando.Parameters.AddWithValue("@Consecutiva", Consecutiva.ToString.Trim())
      msgError.show(miComando.CommandText)
      misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
      If misRegistros.Read() Then 'Leo para poder accesarlos
        Dim IdOpcion = CStr(misRegistros.Item("IdOpcion"))
        Dim Correcta = misRegistros.Item("Correcta")
        Session("TipoResp") = Trim(misRegistros.Item("TipoRespuesta").ToString)

        'PASO 3) Ingreso la respuesta, pero antes verifico si no habia ya ingresado la respuesta para que sea el segundo intento
        misRegistros.Close()
        'En el select con el puro IdDetalleEvaluacion puedo localizar la respuesta, pero lo pongo para que quede mas amarrado
        miComando.Parameters.Clear()
        miComando.CommandText = "select IdRespuesta, Intento, Acertada from Respuesta where IdDetalleEvaluacion = @IdDetalleEvaluacion and IdPlanteamiento = @IdPlanteamiento and IdAlumno = @IdAlumno"
        miComando.Parameters.AddWithValue("@IdDetalleEvaluacion", IdDetalleEvaluacion)
        miComando.Parameters.AddWithValue("@IdPlanteamiento", IdPlanteamiento)
        miComando.Parameters.AddWithValue("@IdAlumno", IdAlumno)
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros

        If misRegistros.Read() Then
          Dim Intento = misRegistros.Item("Intento") 'AUNQUE SI ESTÁ EN ESTE SCRIPT CONTESTAR EL INTENTO SOLO PUEDE SER 1
          Intento = Intento + 1
          If Intento > 2 Then 'Esto lo hago para que no intenten escribir el URL directamente en el navegador y seguir contestando. Y también es la clave para pemitir mas vueltas
            Session("Nota") = Lang.FileSystem.Alumno.Contestar.MSG_ILEGAL.Item(Session("Usuario_Idioma"))
            Response.Redirect("~/alumno/cursos/")
          End If
          SDSresp.UpdateParameters.Clear()
          SDSresp.UpdateCommand = "SET dateformat dmy; UPDATE Respuesta SET IdOpcion = @IdOpcion, Intento = @Intento" &
              ", Acertada = @Acertada, FechaContestada = getdate() WHERE IdDetalleEvaluacion = @IdDetalleEvaluacion" &
             " and IdPlanteamiento = @IdPlanteamiento and IdAlumno = @IdAlumno and IdGrupo = @IdGrupo" &
             " and IdGrado = @IdGrado and IdCicloEscolar = @IdCicloEscolar"
          SDSresp.UpdateParameters.Add("IdOpcion", IdOpcion)
          SDSresp.UpdateParameters.Add("Intento", CStr(Intento))
          SDSresp.UpdateParameters.Add("Acertada", Correcta)
          SDSresp.UpdateParameters.Add("IdDetalleEvaluacion", IdDetalleEvaluacion)
          SDSresp.UpdateParameters.Add("IdPlanteamiento", IdPlanteamiento)
          SDSresp.UpdateParameters.Add("IdAlumno", IdAlumno)
          SDSresp.UpdateParameters.Add("IdGrupo", IdGrupo)
          SDSresp.UpdateParameters.Add("IdGrado", IdGrado)
          SDSresp.UpdateParameters.Add("IdCicloEscolar", IdCicloEscolar)
          SDSresp.Update()
        Else
          SDSresp.InsertParameters.Clear()
          SDSresp.InsertCommand = "SET dateformat dmy; INSERT INTO Respuesta (IdPlanteamiento,IdOpcion,IdAlumno,IdGrado,IdGrupo,IdCicloEscolar,IdDetalleEvaluacion,Intento,Acertada,FechaContestada) VALUES (" &
                                                   "@IdPlanteamiento,@IdOpcion,@IdAlumno,@IdGrado,@IdGrupo,@IdCicloEscolar,@IdDetalleEvaluacion,1,@Acertada,getdate()); SELECT @NuevoId = @@Identity"
          SDSresp.InsertParameters.Add("IdPlanteamiento", IdPlanteamiento)
          SDSresp.InsertParameters.Add("IdOpcion", IdOpcion)
          SDSresp.InsertParameters.Add("IdAlumno", IdAlumno)
          SDSresp.InsertParameters.Add("IdGrado", IdGrado)
          SDSresp.InsertParameters.Add("IdGrupo", IdGrupo)
          SDSresp.InsertParameters.Add("IdCicloEscolar", IdCicloEscolar)
          SDSresp.InsertParameters.Add("IdDetalleEvaluacion", IdDetalleEvaluacion)
          SDSresp.InsertParameters.Add("Acertada", Correcta)
          SDSresp.InsertParameters.Add("NuevoId", Nothing)
          SDSresp.Insert()
        End If
      Else
        'AQUI VA EL CÓDIGO DE COMO CONTESTAR SI SE LE TERMINÓ EL TIEMPO Y NO TENÍA NADA SELECCIONADO (EN RESPUESTA MULTIPLE)
        SDSresp.InsertParameters.Clear()
        SDSresp.InsertCommand = "SET dateformat dmy; INSERT INTO Respuesta (IdPlanteamiento,IdOpcion,IdAlumno,IdGrado,IdGrupo,IdCicloEscolar,IdDetalleEvaluacion,Intento,Acertada,FechaContestada) VALUES (" &
                                                     "@IdPlanteamiento,NULL,@IdAlumno,@IdGrado,@IdGrupo,@IdCicloEscolar,@IdDetalleEvaluacion,1,'N',getdate()); SELECT @NuevoId = @@Identity"
        SDSresp.InsertParameters.Add("IdPlanteamiento", IdPlanteamiento)
        SDSresp.InsertParameters.Add("IdAlumno", IdAlumno)
        SDSresp.InsertParameters.Add("IdGrado", IdGrado)
        SDSresp.InsertParameters.Add("IdGrupo", IdGrupo)
        SDSresp.InsertParameters.Add("IdCicloEscolar", IdCicloEscolar)
        SDSresp.InsertParameters.Add("IdDetalleEvaluacion", IdDetalleEvaluacion)
        SDSresp.InsertParameters.Add("NuevoId", Nothing)
        SDSresp.Insert()
      End If
      misRegistros.Close()
      objConexion.Close()

      'PASO 5) Redirecciono a la página nuevamente
      Response.Redirect("~/alumno/cursos/contestar/")

    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  ' deprecated desde introducción de tinymce
  Protected Sub IBwords_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles IBwords.Click
    Dim p = System.Text.RegularExpressions.Regex.Matches(TBrespuestaA.Text, "\w+").Count().ToString
    Dim c = TBrespuestaA.Text.Count.ToString
    ' System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=""JavaScript"">alert('" + x + ")</SCRIPT>")
    ClientScript.RegisterStartupScript(IBwords.GetType(), "myalert", "alert('El texto contiene " + p + " palabras y " + c + " caracteres de 2000 máximo');", True)
  End Sub

  Protected Sub SDSevaluacionesterminadas_Deleting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles SDSevaluacionesterminadas.Deleting
    e.Command.CommandTimeout = 200
  End Sub

  Protected Sub SDSevaluacionesterminadas_Inserting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles SDSevaluacionesterminadas.Inserting
    e.Command.CommandTimeout = 200
  End Sub

  Protected Sub SDSrespuestasabiertas_Deleting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles SDSrespuestasabiertas.Deleting
    e.Command.CommandTimeout = 200
  End Sub

  Protected Sub SDSrespuestasabiertas_Inserting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles SDSrespuestasabiertas.Inserting
    e.Command.CommandTimeout = 200
  End Sub

  Protected Sub SDSresp_Inserting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles SDSresp.Inserting
    e.Command.CommandTimeout = 200
  End Sub

  Protected Sub SDSresp_Updating(sender As Object, e As SqlDataSourceCommandEventArgs) Handles SDSresp.Updating
    e.Command.CommandTimeout = 200
  End Sub

  Protected Sub SDSresp_Deleting(sender As Object, e As SqlDataSourceCommandEventArgs) Handles SDSresp.Deleting
    e.Command.CommandTimeout = 200
  End Sub

  Protected Sub SDSresp_Selecting(sender As Object, e As SqlDataSourceSelectingEventArgs) Handles SDSresp.Selecting
    e.Command.CommandTimeout = 200
  End Sub

  Protected Sub SDSrespuestasabiertas_Selecting(sender As Object, e As SqlDataSourceSelectingEventArgs) Handles SDSrespuestasabiertas.Selecting
    e.Command.CommandTimeout = 200
  End Sub

  Protected Sub SDSrespuestasabiertas_Updating(sender As Object, e As SqlDataSourceCommandEventArgs) Handles SDSrespuestasabiertas.Updating
    e.Command.CommandTimeout = 200
  End Sub

  Protected Sub SDSevaluacionesterminadas_Selecting(sender As Object, e As SqlDataSourceSelectingEventArgs) Handles SDSevaluacionesterminadas.Selecting
    e.Command.CommandTimeout = 200
  End Sub

  Protected Sub SDSevaluacionesterminadas_Updating(sender As Object, e As SqlDataSourceCommandEventArgs) Handles SDSevaluacionesterminadas.Updating
    e.Command.CommandTimeout = 200
  End Sub

End Class
