﻿Imports Siget

'Imports System.Web.Security
'Imports System.Data.OleDb
Imports System.Data
Imports System.IO
Imports System.Data.SqlClient

Partial Class alumno_cursos_contestar_procesa_Default
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Utils.Sesion.sesionAbierta() Then
            Session("Nota") = Lang.FileSystem.General.Msg_SuSesionExpiro(Session("Usuario_Idioma").ToString())
            Response.Redirect("~/")
        End If

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            aplicaLenguaje()

            procesaRespuesta()

            OutputCss()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = Lang.FileSystem.Alumno.Contestar_Procesa.LT_TITULO.Item(Session("Usuario_Idioma"))

    PageTitle_v1.show(Lang.FileSystem.Alumno.Contestar_Procesa.LT_TITULO.Item(Session("Usuario_Idioma")),
                      Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/server_error.png")

        lt_btn_regresar.Text = Lang.FileSystem.Alumno.Contestar_Procesa.LT_BTN_REGRESAR.Item(Session("Usuario_Idioma"))
    End Sub

    ' imprimo los estilos variables de ésta página
    Protected Sub OutputCss()
        Dim s As StringBuilder = New StringBuilder()
        s.Append("<style type='text/css'>")
        s.Append("  #menu .current_area_cursos a {") ' esta clase css debe corresponder a la clase del boton a resaltar
        s.Append("    border-bottom: 5px solid " & Config.Color.MenuBar_SelectedOption_BorderColor & ";")
        s.Append("  }")
        s.Append("  .AssignmentHeaderRow {")
        s.Append("    background-color: " & Config.Color.MenuPendientes_Subtitle & ";")
        s.Append("  }")
        s.Append("</style>")
        ltEstilos.Text += s.ToString()
    End Sub

#End Region

    ' procesa la respuesta de la página anterior
    Protected Sub procesaRespuesta()
        ' Objetivo de redirección: 0 - ninguno; 1 - ~/alumno/cursos/contestar/; 2 - ~/alumno/cursos/
        Dim redirect As Integer = 0

        'Filtro si el usuario anteriormente pulso Aceptar Respuesta o Contestar Luego o ésta aquí porque venció el tiempo
        If IsNothing(Context.Items.Item("BotonActivado")) Then
            Session("Nota") = Lang.FileSystem.Alumno.Cursos.msg_evaluacion_no_identificada.Item(Session("Usuario_Idioma"))
            redirect = 2 ' fallo algo
        ElseIf Context.Items.Item("BotonActivado") = "Brincar" Then
            redirect = 1 ' Redirecciono a la siguiente pregunta
        ElseIf (
                (
                    (Context.Items.Item("BotonActivado") = "Aceptar") Or
                    (Context.Items.Item("BotonActivado") = "AceptarA") Or
                    (Context.Items.Item("BotonActivado") = "GuardarA")
                ) AndAlso
                Context.Items.Item("RespuestaSeleccionada") <> ""
            ) Then

            If (CStr(Context.Items.Item("RespuestaSeleccionada")) = "0") Then
                redirect = 1 ' Redirecciono a la siguiente pregunta
            Else
                Try
                    'PROCEDIMIENTO ALMACENADO PARA INSERTAR LA RESPUESTA
                    Dim objCommand As New SqlClient.SqlCommand("Respuesta_Inserta")
                    Dim Conexion As String = ConfigurationManager.
                                            ConnectionStrings("sadcomeConnectionString").
                                            ConnectionString

                    objCommand.CommandType = CommandType.StoredProcedure
                    objCommand.Parameters.Add("@IdPlanteamiento", SqlDbType.Int)
                    objCommand.Parameters("@IdPlanteamiento").Value = CInt(Context.Items.Item("PlanteamientoActual"))

                    objCommand.Parameters.Add("@Consecutiva", SqlDbType.Char)
                    If Context.Items.Item("RespuestaSeleccionada") = "" Then
                        objCommand.Parameters("@Consecutiva").Value = ""
                    Else
                        objCommand.Parameters("@Consecutiva").Value = Context.Items.Item("RespuestaSeleccionada")
                    End If

                    objCommand.Parameters.Add("@IdCicloEscolar", SqlDbType.Int)
                    objCommand.Parameters("@IdCicloEscolar").Value = CInt(Context.Items.Item("CicloEscolarActual"))
                    objCommand.Parameters.Add("@IdDetalleEvaluacion", SqlDbType.BigInt)
                    objCommand.Parameters("@IdDetalleEvaluacion").Value = CInt(Context.Items.Item("DetalleEvaluacionActual"))

                    objCommand.Parameters.Add("@Usuario", SqlDbType.VarChar, 100)
                    objCommand.Parameters("@Usuario").Value = User.Identity.Name

                    objCommand.Parameters.Add("@RespuestaA", SqlDbType.VarChar, -1)
                    If Context.Items.Item("RespuestaAbierta") = "" Then
                        objCommand.Parameters("@RespuestaA").Value = ""
                    Else
                        objCommand.Parameters("@RespuestaA").Value = Context.Items.Item("RespuestaAbierta")
                    End If

                    ' el parámetro idRespuestaGuardada almacena el IdRespuestaAbierta de la respuesta que se guardó
                    ' cuando no es una respuesta guardada viene vacío, así que puedo seleccionar el modo
                    objCommand.Parameters.Add("@Modo", SqlDbType.Int)
                    If Context.Items.Item("BotonActivado") = "GuardarA" Then
                        If Context.Items.Item("idRespuestaGuardada") = "" Then ' cuando guarda y es la primera vez que contesta la respuesta abierta
                            objCommand.Parameters("@Modo").Value = 1
                        Else ' cuando guarda y es una respuesta ya guardada
                            objCommand.Parameters("@Modo").Value = 3
                        End If
                    Else
                        If Context.Items.Item("idRespuestaGuardada") = "" Then ' cuando envia y es la primera vez que contesta
                            objCommand.Parameters("@Modo").Value = 0
                        Else ' cuando envia y es una respuesta ya guardada
                            objCommand.Parameters("@Modo").Value = 2
                        End If
                    End If

                    objCommand.Parameters.Add("@IdRespuestaAbierta", SqlDbType.Int)
                    If Context.Items.Item("idRespuestaGuardada") = "" Then
                        ' cuando se mande éste parámetro arriba se seleccionó un modo en el que no se toma en cuenta este parámetro dentro del StoredProcedure
                        objCommand.Parameters("@IdRespuestaAbierta").Value = -1
                    Else
                        objCommand.Parameters("@IdRespuestaAbierta").Value = CInt(Context.Items.Item("idRespuestaGuardada"))
                    End If

                    objCommand.CommandTimeout = 1000
                    objCommand.Connection = New SqlClient.SqlConnection(Conexion)
                    objCommand.Connection.Open()

                    objCommand.ExecuteNonQuery() 'Como no es consulta, hay que ejecutar esto

                    objCommand.Connection.Close()

                    redirect = 1 'Redirecciono a la página nuevamente
                Catch ex As Exception
                    Utils.LogManager.ExceptionLog_InsertEntry(ex)
                    msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
                End Try
            End If
        Else
            'AQUI IRIA EL ALGORITMO PARA PASAR A LA SIGUIENTE PREGUNTA (PLANTEAMIENTO) SIN GUARDAR RESPUESTA
            'PASARLE EL SIGUIENTE IDPLANTEAMIENTO EN EL RESPONSE.REDIRECT
            If ((Context.Items.Item("BotonActivado") <> "Aceptar") And (Context.Items.Item("BotonActivado") <> "AceptarA")) Then
                redirect = 2 ' Redirecciono al menú de actividades
            ElseIf Context.Items.Item("RespuestaSeleccionada") = "" Then 'Significa que acepto pero no eligio respuesta
                redirect = 1 ' Redirecciono a la siguiente pregunta
            End If
        End If

        ' *********************************************************************
        '                              Redirección
        ' *********************************************************************
        If redirect = 1 Then
            Response.Redirect("~/alumno/cursos/contestar/")
        ElseIf redirect = 2 Then
            Response.Redirect("~/alumno/cursos/")
        End If
    End Sub

End Class
