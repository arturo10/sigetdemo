<%@ Page Title=""
  Language="VB"
  MasterPageFile="~/alumno/principal_alumno_5-1-0.master"
  AutoEventWireup="false"
  CodeFile="Default.aspx.vb"
  Inherits="alumno_cursos_contestar_Default"
  ValidateRequest="false" %>

<%-- Se necesita esta propiedad para acceder directamente a propiedades de la p�gina maestra --%>
<%@ MasterType VirtualPath="~/alumno/principal_alumno_5-1-0.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/PageTitle_v1.ascx" TagPrefix="uc1" TagName="PageTitle_v1" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
  <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

     <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts%>audiojs/audiojs/audio.min.js"></script>
    <script type="text/javascript">
        audiojs.events.ready(function () {
            var as = audiojs.createAll();
        });
    </script>
  <style type="text/css">
    .style11 {
      width: 100%;
    }

    .style21 {
    }

    .style23 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: medium;
      font-weight: bold;
      color: #0000FF;
    }

    .style24 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: x-small;
      color: #000099;
      font-weight: bold;
      background-color: #CCCCCC;
    }

    .style25 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: small;
      font-weight: bold;
      color: #FFFFFF;
    }

    .style26 {
      width: 75px;
    }

    .style27 {
      width: 116px;
    }

    .style29 {
      width: 80px;
      height: 8px;
    }

    .style30 {
      width: 7px;
      height: 8px;
    }

    .style31 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: large;
      font-weight: bold;
    }

    .style32 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: large;
      font-weight: bold;
      color: #0000FF;
      text-align: right;
    }

    .style33 {
      font-family: Arial, Helvetica, sans-serif;
    }

    .style34 {
      width: 45px;
      height: 8px;
      font-family: Arial, Helvetica, sans-serif;
    }

    .style35 {
      width: 47px;
      height: 8px;
      font-family: Arial, Helvetica, sans-serif;
    }

    .style37 {
      height: 38px;
      font-family: Arial, Helvetica, sans-serif;
      width: 80px;
    }

    .style38 {
      width: 7px;
      height: 15px;
      font-family: Arial, Helvetica, sans-serif;
      text-align: center;
    }

    .planteamiento p {
      margin-top: 0;
    }
  </style>

  <%-- Aqu� imprimo los estilos variables de �sta p�gina espec�fica --%>
  <asp:Literal ID="ltEstilos" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">

  <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1" />

  <table style="width: 100%;">
    <tr>
      <td style="text-align: center;">
        <asp:Label ID="LblFaltan" runat="server"
          Style="font-size: small; font-family: Arial, Helvetica, sans-serif;"></asp:Label>
      </td>
    </tr>
  </table>

  <table style="background-color: white; margin-left: auto; margin-right: auto; max-width: 800px;">
    <tr class="titulo">
      <td colspan="4">
        <uc1:msgError runat="server" ID="msgError" />
      </td>
    </tr>
    <tr>
      <td class="style21" colspan="2">
        <table>
          <tr>
            <td style="vertical-align: top;">
              <asp:Label ID="LblConsecutivo" runat="server"
                Style="font-family: Arial, Helvetica, sans-serif; font-size: 22px; font-weight: 700; color: #000099;"></asp:Label>
            </td>
            <td class="planteamiento">
              <asp:Label ID="LblPlanteamiento" runat="server"
                Style="font-family: Arial, Helvetica, sans-serif; font-size: 22px; color: #000099;"></asp:Label>
            </td>
            
          </tr>
        </table>
            <div class="pull-right col-lg-4">
                 <asp:LinkButton ID="btnCrearCalificaciones" 
              runat="server" data-toggle="modal" 
                data-target=".retroalimentacion-modal" 
              CssClass="btn  btn-lg btn-labeled btn-blue ">
              <i class="btn-label fa fa-info"></i>Reportar Reactivo
          </asp:LinkButton>
          </div>

      </td>
      <td colspan="2">
        <asp:Image ID="ImPlanteamiento" runat="server" Visible="False" style="display:block;margin-left:auto;margin-right:auto;"  />
        <asp:HyperLink ID="HLapoyoplanteamiento" runat="server" Target="_blank"
          CssClass="style24" Visible="False">[HLapoyoplanteamiento]</asp:HyperLink>
        <asp:Label ID="LblVideo" runat="server"
          Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
      </td>
        
    </tr>
      <tr>
          <td colspan="2">
              <asp:Panel ID="pnlDespliegaPlanteamiento1" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
          </td>
      </tr>
    <tr>
      <td class="style21" colspan="2">
        <asp:Image ID="ImPlanteamiento2" runat="server" Visible="False" />
        <asp:HyperLink ID="HLapoyoplanteamiento2" runat="server" Target="_blank"
          CssClass="style24" Visible="False">[HLapoyoplanteamiento2]</asp:HyperLink>
        <asp:Label ID="LblVideo2" runat="server"
          Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
           
      </td>
      <td colspan="2">&nbsp;</td>
    </tr>
      <tr>
          <td colspan="2">
              <asp:Panel ID="pnlDespliegaPlanteamiento2" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
          </td>
      </tr>
      <tr>
           <td colspan="2">
             <asp:Panel ID="pnlRecorder" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
         </td>
      </tr>
    <tr>
      <td class="style21" colspan="4" style="background-color: white;">
        <asp:Label ID="LblReferencia" runat="server"
          Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
      </td>
    </tr>
    <tr>
      <td colspan="4" style="background-color: #666666; text-align: right;">
        <asp:Label ID="LblAlumno" runat="server"
          Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #FFFFFF"></asp:Label>
      </td>
    </tr>
    <tr>
      <td colspan="4">
        <asp:Panel ID="PanelMultiple" runat="server">
          <table id="TblOpciones" style="background-color: white; margin-left: auto; margin-right: auto;">
            <tr>
              <td class="style29" style="background-color: white;">
                <asp:RadioButton ID="Opcion1" runat="server" GroupName="eleccion"
                  CssClass="style32" Width="50px" TextAlign="Left" />
                   <asp:RadioButton ID="Opcion1Aux" runat="server" Visible="false" 
                  CssClass="style32" Width="50px" TextAlign="Left" />
              </td>
              <td style="background-color: white;">
                <asp:Label ID="TextoOpc1" runat="server" CssClass="style31"></asp:Label>
              </td>
              <td class="style30" style="background-color: white;">
                <asp:RadioButton ID="Opcion2" runat="server" GroupName="eleccion"
                  CssClass="style32" Width="50px" TextAlign="Left" />
                   <asp:RadioButton ID="Opcion2Aux" runat="server" Visible="false" 
                  CssClass="style32" Width="50px" TextAlign="Left" />
              </td>
              <td colspan="2">
                <asp:Label ID="TextoOpc2" runat="server" CssClass="style31"></asp:Label>
              </td>
            </tr>
            <tr>
              <td class="style37">&nbsp;</td>
              <td>
                <asp:Image ID="ImOpc1" runat="server" CssClass="style24" />
                  <asp:Panel ID="PnlDespliegaOpc1" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
                <asp:HyperLink ID="HLapoyoopc1" runat="server" Target="_blank"
                  CssClass="style24">[HLapoyoopc1]</asp:HyperLink>
              </td>
              <td class="style38">&nbsp;</td>
              <td colspan="2">
                <asp:Image ID="ImOpc2" runat="server" CssClass="style24" />
                  <asp:Panel ID="PnlDespliegaOpc2" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
                <asp:HyperLink ID="HLapoyoopc2" runat="server" Target="_blank"
                  CssClass="style24">[HLapoyoopc2]</asp:HyperLink>
              </td>
            </tr>
            <tr>
              <td class="style29">
                <asp:RadioButton ID="Opcion3" runat="server" GroupName="eleccion"
                  CssClass="style32" Width="50px" TextAlign="Left" />
                   <asp:RadioButton ID="Opcion3Aux" runat="server" Visible="false" 
                  CssClass="style32" Width="50px" TextAlign="Left" />
              </td>
              <td>
                <asp:Label ID="TextoOpc3" runat="server" CssClass="style31"></asp:Label>
              </td>
              <td class="style30">
                <asp:RadioButton ID="Opcion4" runat="server" GroupName="eleccion"
                  CssClass="style32" Width="50px" TextAlign="Left" />
                  <asp:RadioButton ID="Opcion4Aux" runat="server" Visible="false" 
                  CssClass="style32" Width="50px" TextAlign="Left" />
              </td>
              <td colspan="2">
                <asp:Label ID="TextoOpc4" runat="server" CssClass="style31"></asp:Label>
              </td>
            </tr>
            <tr>
              <td class="style37">&nbsp;</td>
              <td>
                <asp:Image ID="ImOpc3" runat="server" CssClass="style24" />
                  <asp:Panel ID="PnlDespliegaOpc3" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
                <asp:HyperLink ID="HLapoyoopc3" runat="server" Target="_blank"
                  CssClass="style24">[HLapoyoopc3]</asp:HyperLink>
              </td>
              <td class="style38">&nbsp;</td>
              <td colspan="2">
                <asp:Image ID="ImOpc4" runat="server" CssClass="style24" />
                   <asp:Panel ID="PnlDespliegaOpc4" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
                <asp:HyperLink ID="HLapoyoopc4" runat="server" Target="_blank"
                  CssClass="style24">[HLapoyoopc4]</asp:HyperLink>
              </td>
            </tr>
            <tr>
              <td class="style29">
                <asp:RadioButton ID="Opcion5" runat="server" CssClass="style32"
                  GroupName="eleccion" TextAlign="Left" Width="50px" />
                   <asp:RadioButton ID="Opcion5Aux" runat="server" CssClass="style32"
                     Visible="false"  TextAlign="Left" Width="50px" />
              </td>
              <td>
                <asp:Label ID="TextoOpc5" runat="server" CssClass="style31"></asp:Label>
              </td>
              <td class="style30">
                <asp:RadioButton ID="Opcion6" runat="server" CssClass="style32"
                  GroupName="eleccion" TextAlign="Left" Width="50px" />
                   <asp:RadioButton ID="Opcion6Aux" runat="server" CssClass="style32"
                    Visible="false"  TextAlign="Left" Width="50px" />
              </td>
              <td colspan="2">
                <asp:Label ID="TextoOpc6" runat="server" CssClass="style31"></asp:Label>
              </td>
            </tr>
            <tr>
              <td class="style29">&nbsp;</td>
              <td>
                <asp:Image ID="ImOpc5" runat="server" />
                  <asp:Panel ID="PnlDespliegaOpc5" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
                <asp:HyperLink ID="HLapoyoopc5" runat="server" CssClass="style24"
                  Target="_blank">[HLapoyoopc5]</asp:HyperLink>
              </td>
              <td class="style30">&nbsp;</td>
              <td colspan="2">
                <asp:Image ID="ImOpc6" runat="server" />
                  <asp:Panel ID="PnlDespliegaOpc6" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
                <asp:HyperLink ID="HLapoyoopc6" runat="server" CssClass="style24"
                  Target="_blank">[HLapoyoopc6]</asp:HyperLink>
              </td>
            </tr>
            <tr>
              <td class="style29">&nbsp;</td>
              <td>&nbsp;</td>
              <td class="style30">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td class="style29">&nbsp;</td>
              <td>
                <asp:Button ID="Aceptar" runat="server"
                  OnClientClick="this.disabled=true"
                  Text="[Aceptar Respuesta]"
                  UseSubmitBehavior="false"
                  CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
                                <asp:Button ID="Cancelar" runat="server"
                                  CssClass="defaultBtn btnThemeGrey btnThemeMedium"
                                  Text="   [Salir]   " />
              </td>
              <td class="style30">&nbsp;</td>
              <td>
                <asp:Button ID="Brincar" runat="server"
                  OnClientClick="this.disabled=true"
                  Text="[Brincar Pregunta]"
                  UseSubmitBehavior="False"
                  CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
              </td>
              <td style="text-align: right;">
                <asp:ImageButton ID="IBrenunciar" runat="server" ImageUrl="~/Resources/imagenes/Stop1a.png"
                  Style="text-align: right" Visible="false" />
              </td>
            </tr>
          </table>
        </asp:Panel>
        <asp:Panel ID="PanelAbierta" runat="server">
          <table style="margin-right: auto; margin-left: auto;">
            <tr>
              <td class="style25" colspan="4" style="background-color: #000066;">
                <asp:Literal ID="lt_hint_redacte_respuesta" runat="server">[Redacte su respuesta:]</asp:Literal>
              </td>
            </tr>
            <tr>
              <td class="style27" colspan="2">
                <asp:RadioButton ID="OpcionL" runat="server" CssClass="style23"
                  GroupName="eleccion" TextAlign="Left" Width="50px" />
              </td>
              <td>
                <asp:TextBox ID="TBrespuestaA" runat="server"
                  ClientIDMode="Static"
                  TextMode="MultiLine"
                  CssClass="tinymce"></asp:TextBox>
                <asp:ImageButton ID="IBwords" runat="server" Height="26px"
                  ImageUrl="~/Resources/imagenes/palabras.png" Width="26px" Visible="false" />
              </td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td colspan="3" style="text-align: center;">
                <span id="validacionAbierta" style="font-size: 1.3em; font-weight: bold; display: none; color: rgb(213, 29, 29);">
                  <asp:Literal ID="ltRespuestaVacia" runat="server"></asp:Literal>
                </span>
              </td>
            </tr>
            <tr>
              <td class="style26">&nbsp;</td>
              <td colspan="2" style="text-align: center;">

                  <div class="col-lg-12" style="margin:10px; ">
                      <asp:LinkButton ID="AceptarA" runat="server"
                          ClientIDMode="AutoID"
                         
                          CssClass="btn btn-lg btn-labeled btn-palegreen">
                         <i class="btn-label fa fa-check"></i> <%=Siget.Lang.FileSystem.Alumno.Contestar.BTN_ACEPTAR_ABIERTA.Item(Session("Usuario_Idioma")) %>
                      </asp:LinkButton>


                      <asp:LinkButton ID="GuardarA" runat="server"
                          ClientIDMode="AutoID"
                          Text="[Guardar Respuesta]"
                          CssClass="defaultBtn btnThemeGrey btnThemeMedium" />

                      <asp:LinkButton ID="CancelarA" runat="server" style="margin:10px; "
                          Text="   [Salir]   "
                          CssClass="btn btn-lg btn-labeled btn-darkorange">
                           <i class="btn-label fa fa-remove"></i><%=Siget.Lang.FileSystem.Alumno.Contestar.BTN_SALIR_ABIERTA.Item(Session("Usuario_Idioma"))%>
                      </asp:LinkButton>
                  </div>

              </td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </asp:Panel>
        <asp:Panel ID="PanelAbiertaAutomatica" runat="server" Visible="false">
          <table style="margin-left: auto; margin-right: auto;">
            <tr>
              <td>
                <asp:TextBox ID="tbRespuestaAbiertaAutomatica" runat="server"
                  ClientIDMode="Static" CssClass="wideTextbox"></asp:TextBox>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>
                <asp:Button ID="VerificarAA" runat="server" Visible="false"
                  ClientIDMode="AutoID"
                  OnClientClick="verify(); return false;"
                  Text="[Verificar Respuesta]"
                  CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
                &nbsp;
                <asp:Button ID="AceptarAA" runat="server"
                  ClientIDMode="AutoID"
                  Text="[Aceptar Respuesta]"
                  CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
                <asp:Button ID="CancelarAA" runat="server"
                  Text="   [Salir]   "
                  CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                <script>
                  function verify() {
                    var str = $("#HfVerificaRespuesta").val().split("~|~");
                    var currAnswer;
                    var usrResponse = $("#tbRespuestaAbiertaAutomatica").val();

                    usrResponse = procesaString(usrResponse);

                    for (var i = 0; i < str.length; i++) {
                      currAnswer = str[i];

                      currAnswer = procesaString(currAnswer);

                      if (currAnswer == usrResponse) {
                        $("#msgVerificaBien").show();
                        $("#msgVerificaMal").hide();
                        return;
                      }
                    }

                    $("#msgVerificaBien").hide();
                    $("#msgVerificaMal").show();
                  }

                  // TODO - el comparar alfanum�rico en esta funci�n elimina tambi�n caracteres con acentos :S
                  function procesaString(input) {
                    if ($("#HfComparaMayusculas").val() != 'True') {
                      input = $("#tbRespuestaAbiertaAutomatica").val().toLowerCase();
                    }

                    if ($("#HfComparaAcentos").val() != 'True') {
                      input = accentFold(input);
                    }

                    if ($("#HfComparaSoloAlfanumerico").val() == 'True') {
                      input = input.replace(/\W/g, '');
                    }

                    return input;
                  }

                  var accentMap = {
                    '�': 'a',
                    '�': 'a',
                    '�': 'a',
                    '�': 'a',
                    '�': 'a',
                    '�': 'a',
                    '�': 'A',
                    '�': 'A',
                    '�': 'A',
                    '�': 'A',
                    '�': 'A',
                    '�': 'A',

                    '�': 'c',
                    '�': 'C',

                    '�': 'e',
                    '�': 'e',
                    '�': 'e',
                    '�': 'e',
                    '�': 'E',
                    '�': 'E',
                    '�': 'E',
                    '�': 'E',

                    '�': 'i',
                    '�': 'i',
                    '�': 'i',
                    '�': 'i',
                    '�': 'I',
                    '�': 'I',
                    '�': 'I',
                    '�': 'I',

                    '�': 'o',
                    '�': 'o',
                    '�': 'o',
                    '�': 'o',
                    '�': 'o',
                    '�': 'O',
                    '�': 'O',
                    '�': 'O',
                    '�': 'O',
                    '�': 'O',

                    '�': 'u',
                    '�': 'u',
                    '�': 'u',
                    '�': 'u',
                    '�': 'U',
                    '�': 'U',
                    '�': 'U',
                    '�': 'U'
                  };

                  function accentFold(s) {
                    if (!s) { return ''; }
                    var ret = '';
                    for (var i = 0; i < s.length; i++) {
                      ret += accentMap[s.charAt(i)] || s.charAt(i);
                    }
                    return ret;
                  };
                </script>
              </td>
            </tr>
            <tr>
              <td>
                <div id="msgVerificaBien" style="display: none;">
                  <uc1:msgSuccess runat="server" ID="msgVerifiedGood" style="display: none;" />
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <div id="msgVerificaMal" style="display: none;">
                  <uc1:msgError runat="server" ID="msgVerifiedBad" style="display: none;" />
                </div>
              </td>
            </tr>
          </table>
        </asp:Panel>
          

          <asp:Image ID="Image1" runat="server" Height="50px"
          ImageUrl="~/Resources/imagenes/faqs.jpg" Width="51px" />
        <br />
      
        

      </td>
        
    </tr>
    
  </table>

    
    <div class="modal fade retroalimentacion-modal" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <asp:UpdatePanel ID="CalificacionesModal" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                            <h4 class="modal-title" id="myLargeModalLabel"><b>Redactar las observaciones </b></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">

                                <div class="panel panel-default">

                                    <div class="panel-body">
                                        <div class="col-lg-12">
                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">

                                                <ContentTemplate>

                                                    <asp:TextBox ID="ckEditor" runat="server"
                                                        ClientIDMode="Static"
                                                        TextMode="MultiLine"></asp:TextBox>

                                                    <div class="form-group">
                                                        <div class="col-lg-8 col-lg-offset-3">
                                                            <asp:LinkButton ID="btnEnviarComentario" runat="server" 
                                                                CssClass="btn btn-lg btn-labeled btn-palegreen">
                                                                    <i class="btn-label fa fa-check"></i>
                                                                 <%=Siget.Lang.FileSystem.Alumno.Contestar.BTN_ACEPTAR_ABIERTA.Item(Session("Usuario_Idioma")) %>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton3" runat="server" Style="margin: 10px;"
                                                                Text="   [Salir]   " data-dismiss="modal"
                                                                CssClass="btn btn-lg btn-labeled btn-darkorange">
                                                         <i class="btn-label fa fa-remove"></i><%=Siget.Lang.FileSystem.Alumno.Contestar.BTN_SALIR_ABIERTA.Item(Session("Usuario_Idioma"))%>
                                                            </asp:LinkButton>
                                                        </div>

                                                    </div>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

  <table class="dataSources">
    <tr>
      <td>
        <asp:HiddenField ID="HFidPlanteamiento" runat="server" />
      </td>
      <td>
        <asp:HiddenField ID="HFidEvaluacion" runat="server" />
      </td>
      <td>
        <asp:HiddenField ID="HFboton" runat="server" />
      </td>
      <td>
        <asp:HiddenField ID="HF1" runat="server" />
      </td>
    </tr>
    <tr>
      <td>
        <asp:HiddenField ID="HFplantel" runat="server" />
      </td>
      <td>
        <asp:HiddenField ID="HFcicloescolar" runat="server" />
      </td>
      <td>
        <asp:HiddenField ID="HFdetalleEvaluacion" runat="server" />
      </td>
      <td>
        <asp:HiddenField ID="HFBrincar" runat="server" Value="" />
      </td>
    </tr>
    <tr>
      <td>
        <asp:HiddenField ID="HFidGrado" runat="server" />
      </td>
      <td>
        <asp:Timer ID="Timer1" runat="server" Interval="10000" Enabled="False">
        </asp:Timer>
      </td>
      <td>
        <asp:HiddenField ID="HfVerificaRespuesta" ClientIDMode="Static" runat="server" />
      </td>
      <td>
        <asp:HiddenField ID="HFidRespuestaAbierta" runat="server" />
      </td>
    </tr>
    <tr>
      <td>

        <asp:SqlDataSource ID="SDSresp" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT * FROM [Respuesta]">
          <InsertParameters>
            <asp:Parameter Direction="Output" Name="NuevoId" Size="4" Type="Int16" />
          </InsertParameters>
        </asp:SqlDataSource>

      </td>
      <td>

        <asp:SqlDataSource ID="SDSrespuestasabiertas" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT * FROM [RespuestaAbierta]"></asp:SqlDataSource>

      </td>
      <td>

        <asp:SqlDataSource ID="SDSevaluacionesterminadas" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT * FROM [EvaluacionTerminada]"></asp:SqlDataSource>

      </td>
      <td>
        <asp:HiddenField ID="HfComparaMayusculas" ClientIDMode="Static" runat="server" />
      </td>
    </tr>
    <tr>
      <td>
        <asp:HiddenField ID="HfComparaAcentos" ClientIDMode="Static" runat="server" />
      </td>
      <td>
        <asp:HiddenField ID="HfComparaSoloAlfanumerico" ClientIDMode="Static" runat="server" />
      </td>
        <td>
        <asp:HiddenField ID="HfPuntuacion" ClientIDMode="Static" runat="server" />
      </td>
    </tr>
  </table>

      <script type="text/javascript">

          Sys.Application.add_load(LoadHandler);

          function LoadHandler(sender, args0) {

              $(function () {

                  var editor = CKEDITOR.replace('ckEditor', {
                      language: 'es',
                      height: 250,
                  });

                  editor.on('change', function (evt) {
                      $('#ckEditor').val(evt.editor.getData());
                  });

               
              });
          }


    </script>
</asp:Content>

