﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/alumno/principal_alumno_5-1-0.master"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="alumno_cursos_contestardemo_Default" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/PageTitle_v1.ascx" TagPrefix="uc1" TagName="PageTitle_v1" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
      <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts%>audiojs/audiojs/audio.min.js"></script>
    <script type="text/javascript">
        audiojs.events.ready(function () {
            var as = audiojs.createAll();
        });
    </script>
    <style type="text/css">
        .style11 {
            width: 100%;
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
            color: #000099;
            font-weight: bold;
            background-color: #CCCCCC;
        }

        .style28 {
            height: auto;
            
        }

        .style29 {
            width: 49px;
            height: 8px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: medium;
            font-weight: bold;
            color: #0000FF;
            text-align: right;
        }

        .style18 {
            width: 345px;
        }

        .style31 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: larger;
            font-weight: bold;
        }

        .style30 {
            width: 45px;
            height: 8px;
        }

        .style35 {
            width: 55px;
            height: 8px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style33 {
            font-family: Arial, Helvetica, sans-serif;
        }

        .style34 {
            width: 45px;
            height: 8px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style25 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            color: #FFFFFF;
        }

        .style27 {
            width: 116px;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: medium;
            font-weight: bold;
            color: #0000FF;
        }

        .style26 {
            width: 75px;
        }

        .style36 {
            text-align: center;
            font-size: large;
        }

        .style37 {
            width: 306px;
        }

        .style38 {
            width: 49px;
            height: 38px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style39 {
            width: 45px;
            height: 15px;
            font-family: Arial, Helvetica, sans-serif;
            text-align: center;
        }

        .style40 {
            text-align: center;
            color: #000066;
            font-size: large;
            font-style:italic;
            font-weight:bold;
            height:35px;
        }
    </style>

    <%-- Aquí imprimo los estilos variables de ésta página específica --%>
    <asp:Literal ID="ltEstilos" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">

    <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1" />

    <table style="background-color: white; margin-left: auto; margin-right: auto; max-width: 750px;">
        <tr class="titulo" >
            <td colspan="3" style="background-color: #FFFFFF;" class="style40">
                <asp:Literal ID="lt_hint_sinvalor" runat="server">[ESTA ACTIVIDAD ES SOLO DE PRÁCTICA, NO CUENTA PARA SU CALIFICACIÓN.]</asp:Literal>
            </td>
        </tr>
    </table>

     <table style="width: 100%;">
        <tr>
           <td style="text-align: center;">
                <asp:Label ID="LblFaltan" runat="server"
                    Style="font-size: small; font-family: Arial, Helvetica, sans-serif;"></asp:Label>
            </td>
        </tr>
        
    </table>
    
    
    <table style="background-color: white; margin-left: auto; margin-right: auto; max-width: 750px;">
       
        <tr class="titulo">
            <td colspan="3" style="background-color: white;">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style21" colspan="3" style="background-color: white;">
                <asp:Label ID="LblPlanteamiento" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: 22px; font-weight: 700; color: #000099;"
                    Text="Label"></asp:Label>
            </td>
        </tr>
        <tr id="trApoyoPlanteamiento1" runat="server">
             <td class="style37" style="width:100%" >
                 <div style="margin-left:auto;margin-right:auto;text-align:center">
                <asp:Image ID="ImPlanteamiento" runat="server" />
                <asp:HyperLink ID="HLapoyoplanteamiento" runat="server" Target="_blank" Visible="false"
                    CssClass="style24">[HLapoyoplanteamiento]</asp:HyperLink>
                <asp:Label ID="LblVideo" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
                     </div>
            </td>
        </tr>
         <tr>
          <td colspan="4">
              <asp:Panel ID="pnlDespliegaPlanteamiento1" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
          </td>
        </tr>
        <tr  id="trApoyoPlanteamiento2" runat="server">
            <td class="style21" colspan="2" style="background-color: white;">
                <asp:Image ID="ImPlanteamiento2" runat="server" />
                <asp:HyperLink ID="HLapoyoplanteamiento2" runat="server" Target="_blank" Visible="false"
                    CssClass="style24">[HLapoyoplanteamiento2]</asp:HyperLink>
                <asp:Label ID="LblVideo2" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
            </td>
            <td class="style37">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="4">
              <asp:Panel ID="pnlDespliegaPlanteamiento2" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
          </td>
      </tr>
        <tr>
           <td colspan="4">
             <asp:Panel ID="pnlRecorder" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
         </td>
      </tr>
        <tr>
            <td class="style21" colspan="3" style="background-color: white;">
                <asp:Label ID="LblReferencia" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="background-color: #666666; text-align: right;">
                <asp:Label ID="LblAlumno" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #FFFFFF"></asp:Label>
            </td>
        </tr>
        <tr runat="server" id="trPanelAbiertaMultiple">
            <td class="style28" colspan="3">
                <asp:Panel ID="PanelMultiple" runat="server">
                    <table id="TblOpciones" style="margin-left:auto;margin-right:auto;">
                        <tr>
                            <td class="style29">
                                <asp:RadioButton ID="Opcion1" runat="server" GroupName="eleccion"
                                    CssClass="style32" Width="50px" TextAlign="Left" />
                            </td>
                            <td class="style18">
                                <asp:Label ID="TextoOpc1" runat="server" CssClass="style31"></asp:Label>
                            </td>
                            <td class="style30">
                                <asp:RadioButton ID="Opcion2" runat="server" GroupName="eleccion"
                                    CssClass="style32" Width="50px" TextAlign="Left" />
                            </td>
                            <td colspan="2">
                                <asp:Label ID="TextoOpc2" runat="server" CssClass="style31"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style38">&nbsp;</td>
                            <td class="style18">
                                <asp:Image ID="ImOpc1" runat="server" />
                                <asp:Panel ID="PnlDespliegaOpc1" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
                                <asp:HyperLink ID="HLapoyoopc1" runat="server" Target="_blank"
                                    CssClass="style24">[HLapoyoopc1]</asp:HyperLink>
                            </td>
                            <td class="style39">&nbsp;</td>
                            <td colspan="2">
                                <asp:Image ID="ImOpc2" runat="server" />
                                 <asp:Panel ID="PnlDespliegaOpc2" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
                                <asp:HyperLink ID="HLapoyoopc2" runat="server" Target="_blank"
                                    CssClass="style24">[HLapoyoopc2]</asp:HyperLink>
                            </td>
                        </tr>
                        <tr>
                            <td class="style29">
                                <asp:RadioButton ID="Opcion3" runat="server" GroupName="eleccion"
                                    CssClass="style32" Width="50px" TextAlign="Left" />
                            </td>
                            <td class="style18">
                                <asp:Label ID="TextoOpc3" runat="server" CssClass="style31"></asp:Label>
                            </td>
                            <td class="style30">
                                <asp:RadioButton ID="Opcion4" runat="server" GroupName="eleccion"
                                    CssClass="style32" Width="50px" TextAlign="Left" />
                            </td>
                            <td colspan="2">
                                <asp:Label ID="TextoOpc4" runat="server" CssClass="style31"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style38">&nbsp;</td>
                            <td class="style18">
                                <asp:Image ID="ImOpc3" runat="server" />
                                 <asp:Panel ID="PnlDespliegaOpc3" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
                                <asp:HyperLink ID="HLapoyoopc3" runat="server" Target="_blank"
                                    CssClass="style24">[HLapoyoopc3]</asp:HyperLink>
                            </td>
                            <td class="style39">&nbsp;</td>
                            <td colspan="2">
                                <asp:Image ID="ImOpc4" runat="server" />
                                <asp:Panel ID="PnlDespliegaOpc4" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
                                <asp:HyperLink ID="HLapoyoopc4" runat="server" Target="_blank"
                                    CssClass="style24">[HLapoyoopc4]</asp:HyperLink>
                            </td>
                        </tr>
                        <tr id="trRbtn56" runat="server">
                            <td class="style29">
                                <asp:RadioButton ID="Opcion5" runat="server" CssClass="style32"
                                    GroupName="eleccion" TextAlign="Left" Width="50px" />
                            </td>
                            <td class="style18">
                                <asp:Label ID="TextoOpc5" runat="server" CssClass="style31"></asp:Label>
                            </td>
                            <td class="style30">
                                <asp:RadioButton ID="Opcion6" runat="server" CssClass="style32"
                                    GroupName="eleccion" TextAlign="Left" Width="50px" />
                            </td>
                            <td colspan="2">
                                <asp:Label ID="TextoOpc6" runat="server" CssClass="style31"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style29">&nbsp;</td>
                            <td class="style18">
                                <asp:Image ID="ImOpc5" runat="server" />
                                  <asp:Panel ID="PnlDespliegaOpc5" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
                                <asp:HyperLink ID="HLapoyoopc5" runat="server" CssClass="style24"
                                    Target="_blank">[HLapoyoopc5]</asp:HyperLink>
                            </td>
                            <td class="style30">&nbsp;</td>
                            <td colspan="2">
                                <asp:Image ID="ImOpc6" runat="server" />
                                 <asp:Panel ID="PnlDespliegaOpc6" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
                                <asp:HyperLink ID="HLapoyoopc6" runat="server" CssClass="style24"
                                    Target="_blank">[HLapoyoopc6]</asp:HyperLink>
                            </td>
                             <td>
                                <asp:HiddenField ID="HFacertadas" runat="server" />
                            </td>
                        </tr>

                        <tr>
                            <td class="style29">&nbsp;</td>
                            <td class="style18">
                                <div style="margin-right:auto;margin-left:auto;text-align:center;">
                                <asp:Button ID="Aceptar" runat="server" Text="[Aceptar Respuesta]"
                                    UseSubmitBehavior="False" CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                &nbsp;
                            <asp:Button ID="Cancelar" runat="server"
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" Text="   [Salir]  " />
                                    </div>
                            </td>
                            <td class="style30">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>
                                <asp:HiddenField ID="HFcontador" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="PanelAbierta" runat="server">
                    <table class="style11" >
                        <tr>
                            <td class="style25" colspan="4" style="background-color: #000066;">
                                <asp:Literal ID="lt_hint_redacte" runat="server">[Redacte su respuesta:]</asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td class="style27" colspan="2">
                                <asp:RadioButton ID="OpcionL" runat="server" CssClass="style23"
                                    GroupName="eleccion" TextAlign="Left" Width="50px" />
                            </td>
                            <td>
                                <asp:TextBox ID="TBrespuestaA" runat="server" MaxLength="400" Width="620px"
                                    Rows="4" TextMode="MultiLine" onkeydown="if(this.value.length >= 400){ alert('Has superado el tamaño máximo permitido de caracteres (400)'); this.value=this.value.substr(0,400-1); return false; }"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style26">&nbsp;</td>
                            <td colspan="2">
                                <asp:Button ID="AceptarA" runat="server"
                                    Text="[Aceptar Respuesta]" UseSubmitBehavior="False"
                                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                                &nbsp;
                            <asp:Button ID="CancelarA" runat="server"
                                Text="  [Salir]  " CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
            </td>
        </tr>
         <tr>
             <asp:Panel ID="PanelAbiertaAutomatica" runat="server" Visible="false">
          <table style="margin-left: auto; margin-right: auto;">
            <tr>
              <td>
                <asp:TextBox ID="tbRespuestaAbiertaAutomatica" runat="server"
                  ClientIDMode="Static" CssClass="wideTextbox"></asp:TextBox>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>
                <asp:Button ID="AceptarAA" runat="server"
                  ClientIDMode="AutoID"
                  Text="[Aceptar Respuesta]"
                  CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
                <asp:Button ID="CancelarAA" runat="server"
                  Text="   [Salir]   "
                  CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                
              </td>
            </tr>
            <tr>
              <td>
                <div id="msgVerificaBien" style="display: none;">
                  <uc1:msgSuccess runat="server" ID="msgVerifiedGood" style="display: none;" />
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <div id="msgVerificaMal" style="display: none;">
                  <uc1:msgError runat="server" ID="msgVerifiedBad" style="display: none;" />
                </div>
              </td>
            </tr>
              
          </table>
        </asp:Panel>
         </tr>
        <tr>
            <table style="margin-left: auto; margin-right: auto;">

                <tr>
                    <td colspan="2">&nbsp</td>
                    <td>
                        <asp:Button ID="BtnSiguiente" runat="server"
                            Visible="false" Text="[Siguiente]" CssClass="defaultBtn btnThemeBlue btnThemeMedium " />
                        <asp:Button ID="BtnTerminar" Visible="false" runat="server" Text="[Terminar]" CssClass="defaultBtn btnThemeGrey btnThemeMedium " />
                    </td>
                    <td>
                        <asp:Button ID="BtnIniciar" Visible="false" runat="server" Text="[Iniciar nuevamente]"
                            CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                    </td>
                   
                </tr>
                </table>
             <table style="margin-left: auto; margin-right: auto;">
                <tr>
                    <td colspan="3">
                         <uc1:msgSuccess runat="server" ID="msgSuccessVerified"  />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <uc1:msgError runat="server" ID="msgErrorVerified"  />
                    </td>
                </tr>
                </table>
        </tr>
     
       
        <tr>
            <td class="style12">
                <asp:SqlDataSource ID="SDSevaluacionesterminadas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [EvaluacionTerminada]"></asp:SqlDataSource>
            </td>
            <td class="style12">&nbsp;</td>
            <td class="style37">&nbsp;</td>
        </tr>
        <tr>
            <td>
                 <asp:HiddenField ID="HfComparaMayusculas" ClientIDMode="Static" runat="server" />
            </td>
            <td>
                 <asp:HiddenField ID="HfComparaAcentos" ClientIDMode="Static" runat="server" />
            </td>
            <td>
                 <asp:HiddenField ID="HfComparaSoloAlfanumerico" ClientIDMode="Static" runat="server" />
            </td>
              <td>
                 <asp:HiddenField ID="HfPuntuacion" ClientIDMode="Static" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>

