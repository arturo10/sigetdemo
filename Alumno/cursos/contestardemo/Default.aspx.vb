﻿Imports Siget

Imports System.Data.SqlClient
Imports Siget.DataAccess


Partial Class alumno_cursos_contestardemo_Default
    Inherits System.Web.UI.Page

    Protected Shared titleIcon As String =
        Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/document_prepare.png"

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Utils.Sesion.sesionAbierta() Then
            Session("Nota") = Lang.FileSystem.General.Msg_SuSesionExpiro(Session("Usuario_Idioma").ToString())
            Response.Redirect("~/")
        End If

        Session("IdEvaluacion") = Trim(Request.QueryString("IdEvaluacion"))
        If Not IsPostBack Then
            Session("Acertadas") = Trim(Request.QueryString("Acertadas"))
            Session("Contador") = Trim(Request.QueryString("Contador"))
            aplicaLenguaje()
            initPageData()
            OutputCss()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = Lang.FileSystem.Alumno.ContestarDemo.LT_TITULO.Item(Session("Usuario_Idioma"))

        lt_hint_sinvalor.Text = Lang.FileSystem.Alumno.ContestarDemo.LT_HINT_SINVALOR.Item(Session("Usuario_Idioma"))
        Aceptar.Text = Lang.FileSystem.Alumno.ContestarDemo.BTN_ACEPTAR_MULTIPLE.Item(Session("Usuario_Idioma"))
        Cancelar.Text = Lang.FileSystem.Alumno.ContestarDemo.BTN_SALIR_MULTIPLE.Item(Session("Usuario_Idioma"))
        AceptarA.Text = Lang.FileSystem.Alumno.ContestarDemo.BTN_ACEPTAR_ABIERTA.Item(Session("Usuario_Idioma"))
        CancelarA.Text = Lang.FileSystem.Alumno.ContestarDemo.BTN_SALIR_ABIERTA.Item(Session("Usuario_Idioma"))
        lt_hint_redacte.Text = Lang.FileSystem.Alumno.ContestarDemo.LT_HINT_REDACTE_RESPUESTA.Item(Session("Usuario_Idioma"))
        AceptarAA.Text = Lang.FileSystem.Alumno.Contestar.BTN_ACEPTAR_ABIERTA.Item(Session("Usuario_Idioma"))
        CancelarAA.Text = Lang.FileSystem.Alumno.Contestar.BTN_SALIR_ABIERTA.Item(Session("Usuario_Idioma"))
        LblFaltan.Attributes.Add("style", "background: " & Config.Color.MenuPendientes_Subtitle & "; padding: 5px;")
        BtnSiguiente.Text = Lang.FileSystem.Alumno.ContestarDemo_Procesa.BTN_SIGUIENTE.Item(Session("Usuario_Idioma"))
        BtnIniciar.Text = Lang.FileSystem.Alumno.ContestarDemo_Procesa.BTN_REINICIAR.Item(Session("Usuario_Idioma"))
        BtnTerminar.Text = Lang.FileSystem.Alumno.ContestarDemo_Procesa.BTN_TERMINAR.Item(Session("Usuario_Idioma"))
        LblAlumno.Text = Lang.FileSystem.Alumno.ContestarDemo.LT_HINT_ALUMNO.Item(Session("Usuario_Idioma")) & " " & Session("Usuario_Nombre")
        LblFaltan.Text = Session("Contador") + " " & Lang.FileSystem.Alumno.ContestarDemo_Procesa.MSG_FALTAN_TOTALES.Item(Session("Usuario_Idioma")) + " " + Session("Acertadas")

        'Lang.FileSystem.Alumno.ContestarDemo.LT_FALTA_1.Item(Session("Usuario_Idioma")) & " " + Session("Contador").ToString + " " & Lang.FileSystem.Alumno.ContestarDemo.LT_FALTA_DE.Item(Session("Usuario_Idioma")) & " " + Session("TotalReactivos").ToString
    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub initPageData()
        'Try
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString

        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misReactivos As SqlDataReader
        miComando = objConexion.CreateCommand

        miComando.CommandText = "SELECT ED.Consecutivo,S.Descripcion,P.IdPlanteamiento,P.IdSubtema,P.Redaccion,P.TipoRespuesta,P.ArchivoApoyo,P.TipoArchivoApoyo,P.ArchivoApoyo2,P.TipoArchivoApoyo2,P.EsconderTexto,P.Libro,P.Autor,P.Editorial,P.HabilitaRecorder,P.ComparaMayusculas,P.ComparaAcentos,P.ComparaSoloAlfanumerico,P.EliminaPuntuacion" + _
                            " FROM Planteamiento P, EvaluacionDemo ED, Subtema S where ED.IdEvaluacion =" + Trim(Session("IdEvaluacion")) + _
                            " and P.IdPlanteamiento = ED.IdPlanteamiento and S.IdSubtema = P.IdSubtema order by ED.Consecutivo"
        objConexion.Open()
        misReactivos = miComando.ExecuteReader()
        Dim I = 0
        Do While I < CInt(Session("Contador").ToString)
            misReactivos.Read()
            I = I + 1
        Loop

        Dim Redaccion As String
        Dim posParagraph As Integer
        Session("IdPlanteamiento") = misReactivos.Item("IdPlanteamiento") 'Lo usaré para mandar la respuesta
        HfComparaMayusculas.Value = misReactivos.Item("ComparaMayusculas").ToString()
        HfComparaAcentos.Value = misReactivos.Item("ComparaAcentos").ToString()
        HfComparaSoloAlfanumerico.Value = misReactivos.Item("ComparaSoloAlfanumerico").ToString()
        HfPuntuacion.Value = misReactivos.Item("EliminaPuntuacion").ToString()
        PageTitle_v1.show(Lang.FileSystem.Alumno.ContestarDemo.LT_FALTAN_SUBTEMA.Item(Session("Usuario_Idioma")) & " " + misReactivos.Item("Descripcion"),
                          titleIcon)
        If misReactivos.Item("EsconderTexto") Then
            LblPlanteamiento.Visible = False
        ElseIf IsDBNull(misReactivos.Item("Redaccion")) Then
            Redaccion = String.Empty
            LblPlanteamiento.Text = Redaccion
        Else
            LblPlanteamiento.Visible = True
            Redaccion = Replace(HttpUtility.HtmlDecode(misReactivos.Item("Redaccion")), "*salto*", "<BR>")
            Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")

            If IsDBNull(misReactivos.Item("Consecutivo")) Then
                LblPlanteamiento.Text = Redaccion
            Else
                posParagraph = Redaccion.IndexOf("<p>")
                Redaccion = Redaccion.Insert(Redaccion.IndexOf(">") + 1, (CStr(misReactivos.Item("Consecutivo")) + ".- &nbsp;&nbsp"))
                LblPlanteamiento.Text = Redaccion
            End If
        End If

        If misReactivos.Item("HabilitaRecorder") Then
            pnlRecorder.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:430px;height:140px;")
            pnlRecorder.Controls.Add(New LiteralControl("<embed src=""../AudioRecorder.swf"" width=""430"" height=""140""></embed>"))
        End If

        'Saco la Referencia bibliográfica del reactivo (si la hay) que consta de 3 campos
        Dim Referencia As String
        Referencia = ""

        If Trim(misReactivos.Item("Libro").ToString <> "") Then
            Referencia = HttpUtility.HtmlDecode(misReactivos.Item("Libro")) + ", "
        End If
        If Trim(misReactivos.Item("Autor").ToString <> "") Then
            Referencia = Referencia + HttpUtility.HtmlDecode(misReactivos.Item("Autor")) + ", "
        End If
        If Trim(misReactivos.Item("Editorial").ToString <> "") Then
            Referencia = Referencia + HttpUtility.HtmlDecode(misReactivos.Item("Editorial"))
        End If
        '***
        If Trim(Referencia) <> "" Then
            LblReferencia.Text = "(" & Lang.FileSystem.Alumno.ContestarDemo.HINT_REFERENCIA.Item(Session("Usuario_Idioma")) & " " + Referencia + ")"
        End If

        'Cargo el archivo 1 de apoyo si lo hay, siempre deberán estar en el directorio material
        If (Not IsDBNull(misReactivos.Item("TipoArchivoApoyo"))) And (Not IsDBNull(misReactivos.Item("ArchivoApoyo"))) Then
            If Trim(misReactivos.Item("ArchivoApoyo")).Length > 0 Then 'Esta comparacion la hago aparte del IF superior porque marca error al querer sacar tamaño a valor NULL
                If Trim(misReactivos.Item("TipoArchivoApoyo")) = "Imagen" Then
                    ImPlanteamiento.ImageUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo")
                    ImPlanteamiento.Visible = True
                    HLapoyoplanteamiento.Visible = False
                ElseIf Trim(misReactivos.Item("TipoArchivoApoyo")) = "Audio" Then
                    pnlDespliegaPlanteamiento1.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:460px;overflow:hidden;")
                    pnlDespliegaPlanteamiento1.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo") + """></audio>"))
                ElseIf Trim(misReactivos.Item("TipoArchivoApoyo")) = "Video" Then
                    LblVideo.Text = "<embed src=""" & Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo") + """ width=""280"" height=""227"" autostart=""true"" controller=""true"" align=""center""></embed>"
                    ImPlanteamiento.Visible = False
                    HLapoyoplanteamiento.Visible = False
                Else
                    HLapoyoplanteamiento.NavigateUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo")
                    HLapoyoplanteamiento.Text = Lang.FileSystem.Alumno.ContestarDemo.HINT_APOYO.Item(Session("Usuario_Idioma")) & " " + Trim(misReactivos.Item("ArchivoApoyo").ToString)
                    HLapoyoplanteamiento.Visible = True
                    ImPlanteamiento.Visible = False
                End If
            Else
                ImPlanteamiento.Visible = False
                HLapoyoplanteamiento.Visible = False
            End If
        Else
            trApoyoPlanteamiento1.Visible = False
            trApoyoPlanteamiento2.Visible = False
        End If

        'Cargo el archivo 2 de apoyo si lo hay, siempre deberán estar en el directorio material
        If (Not IsDBNull(misReactivos.Item("TipoArchivoApoyo2"))) And (Not IsDBNull(misReactivos.Item("ArchivoApoyo2"))) Then
            If Trim(misReactivos.Item("ArchivoApoyo2")).Length > 0 Then 'Esta comparacion la hago aparte del IF superior porque marca error al querer sacar tamaño a valor NULL
                If Trim(misReactivos.Item("TipoArchivoApoyo2")) = "Imagen" Then

                    ImPlanteamiento2.ImageUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo2")
                    ImPlanteamiento2.Visible = True
                    HLapoyoplanteamiento2.Visible = False
                ElseIf Trim(misReactivos.Item("TipoArchivoApoyo2")) = "Audio" Then
                    pnlDespliegaPlanteamiento1.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:460px;overflow:hidden;")
                    pnlDespliegaPlanteamiento1.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo2") + """></audio>"))
                ElseIf Trim(misReactivos.Item("TipoArchivoApoyo2")) = "Video" Then
                    LblVideo2.Text = "<embed src=""" & Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo2") + """ width=""280"" height=""227"" autostart=""true"" controller=""true"" align=""center""></embed>"
                    ImPlanteamiento2.Visible = False
                    HLapoyoplanteamiento2.Visible = False
                Else
                    HLapoyoplanteamiento2.NavigateUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo2")
                    HLapoyoplanteamiento2.Text = Lang.FileSystem.Alumno.ContestarDemo.HINT_APOYO.Item(Session("Usuario_Idioma")) & " " + Trim(misReactivos.Item("ArchivoApoyo2").ToString)
                    HLapoyoplanteamiento2.Visible = True
                    ImPlanteamiento2.Visible = False
                End If
            Else
                ImPlanteamiento2.Visible = False
                HLapoyoplanteamiento2.Visible = False
            End If
        End If

        'AHORA, EN BASE AL TIPO DE REACTIVO MUESTRO LAS OPCIONES PARA RESPONDER
        If Trim(misReactivos.Item("TipoRespuesta")) = "Multiple" Then
            'Algoritmo respuesta multiple
            PanelMultiple.Visible = True
            PanelAbierta.Visible = False


            'OPCIONES (Todos los tipos de respuestas generan Registros en la tabla "Opcion"
            miComando.CommandText = "SELECT * FROM Opcion where Esconder = 'False' and idPlanteamiento = " + misReactivos.Item("IdPlanteamiento").ToString + " order by Consecutiva"
            misReactivos.Close() 'Cierro la tabla de Planteamiento para poder usar este objeto de nuevo
            misReactivos = miComando.ExecuteReader() 'Creo conjunto de registros
            misReactivos.Read() 'Leo para poder accesarlos

            'DEBO USAR: (Primero colocal un PlaceHolder y llamarlo "PanelOpciones"
            'Dim TextoOpc1 As New Label
            'TextoOpc1.Text = misReactivos.Item("Redaccion")
            'PanelOpciones.Controls.Add(TextoOpc1) 'ó Controls.AddAt(1, TextoOpc1)
            'Dim TblOpciones As New Table

            Redaccion = Replace(HttpUtility.HtmlDecode(misReactivos.Item("Redaccion")), "*salto*", "<BR>")
            Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
            'TextoOpc1.Text = "<font color='blue'>" + CStr(misReactivos.Item("Consecutiva")) + ") </font> " + Redaccion
            TextoOpc1.Text = Redaccion
            Opcion1.Text = misReactivos.Item("Consecutiva")
            If Not IsDBNull(misReactivos.Item("TipoArchivoApoyo")) Then
                If misReactivos.Item("TipoArchivoApoyo") = "Imagen" Then
                    ImOpc1.ImageUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo")
                    HLapoyoopc1.Visible = False
                ElseIf Trim(misReactivos.Item("TipoArchivoApoyo")) = "Audio" Then
                    PnlDespliegaOpc1.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                    PnlDespliegaOpc1.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo") + """></audio>"))
                    ImOpc1.Visible = False
                    HLapoyoopc1.Visible = False
                Else
                    'Algoritmo cuando el archivo de apoyo no es imagen, será liga
                    HLapoyoopc1.NavigateUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo")
                    HLapoyoopc1.Text = Lang.FileSystem.Alumno.ContestarDemo.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                    ImOpc1.Visible = False
                End If
            Else
                ImOpc1.Visible = False
                HLapoyoopc1.Visible = False
            End If

            If misReactivos.Read() Then 'OPCION 2
                Redaccion = Replace(HttpUtility.HtmlDecode(misReactivos.Item("Redaccion")), "*salto*", "<BR>")
                Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                'TextoOpc2.Text = "<font color='blue'>" + CStr(misReactivos.Item("Consecutiva")) + ") </font> " + Redaccion
                TextoOpc2.Text = Redaccion
                Opcion2.Text = misReactivos.Item("Consecutiva")
                If Not IsDBNull(misReactivos.Item("TipoArchivoApoyo")) Then
                    If misReactivos.Item("TipoArchivoApoyo") = "Imagen" Then
                        ImOpc2.ImageUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo")
                        HLapoyoopc2.Visible = False
                    ElseIf Trim(misReactivos.Item("TipoArchivoApoyo")) = "Audio" Then
                        PnlDespliegaOpc2.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                        PnlDespliegaOpc2.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo") + """></audio>"))
                        ImOpc2.Visible = False
                        HLapoyoopc2.Visible = False
                    Else
                        HLapoyoopc2.NavigateUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo")
                        HLapoyoopc2.Text = Lang.FileSystem.Alumno.ContestarDemo.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                        ImOpc2.Visible = False
                    End If
                Else
                    ImOpc2.Visible = False
                    HLapoyoopc2.Visible = False
                End If
            Else
                'Significa que no hay opcion
                ImOpc2.Visible = False
                HLapoyoopc2.Visible = False
                Opcion2.Visible = False
            End If


            If misReactivos.Read() Then 'OPCION 3
                Redaccion = Replace(HttpUtility.HtmlDecode(misReactivos.Item("Redaccion")), "*salto*", "<BR>")
                Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                'TextoOpc3.Text = "<font color='blue'>" + CStr(misReactivos.Item("Consecutiva")) + ") </font> " + Redaccion
                TextoOpc3.Text = Redaccion
                Opcion3.Text = misReactivos.Item("Consecutiva")
                If Not IsDBNull(misReactivos.Item("TipoArchivoApoyo")) Then
                    If misReactivos.Item("TipoArchivoApoyo") = "Imagen" Then
                        ImOpc3.ImageUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo")
                        HLapoyoopc3.Visible = False
                    ElseIf Trim(misReactivos.Item("TipoArchivoApoyo")) = "Audio" Then
                        PnlDespliegaOpc3.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                        PnlDespliegaOpc3.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo") + """></audio>"))
                        ImOpc3.Visible = False
                        HLapoyoopc3.Visible = False
                    Else
                        HLapoyoopc3.NavigateUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo")
                        HLapoyoopc3.Text = Lang.FileSystem.Alumno.ContestarDemo.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                        ImOpc3.Visible = False
                    End If
                Else
                    ImOpc3.Visible = False
                    HLapoyoopc3.Visible = False
                End If
            Else
                ImOpc3.Visible = False
                HLapoyoopc3.Visible = False
                Opcion3.Visible = False
            End If

            If misReactivos.Read() Then 'OPCION 4
                Redaccion = Replace(HttpUtility.HtmlDecode(misReactivos.Item("Redaccion")), "*salto*", "<BR>")
                Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                TextoOpc4.Text = Redaccion
                Opcion4.Text = misReactivos.Item("Consecutiva")
                If Not IsDBNull(misReactivos.Item("TipoArchivoApoyo")) Then
                    If misReactivos.Item("TipoArchivoApoyo") = "Imagen" Then
                        ImOpc4.ImageUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo")
                        HLapoyoopc4.Visible = False
                    ElseIf Trim(misReactivos.Item("TipoArchivoApoyo")) = "Audio" Then
                        PnlDespliegaOpc4.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                        PnlDespliegaOpc4.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo") + """></audio>"))
                        ImOpc4.Visible = False
                        HLapoyoopc4.Visible = False
                    Else
                        HLapoyoopc4.NavigateUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo")
                        HLapoyoopc4.Text = Lang.FileSystem.Alumno.ContestarDemo.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                        ImOpc4.Visible = False
                    End If
                Else
                    ImOpc4.Visible = False
                    HLapoyoopc4.Visible = False
                End If
            Else
                ImOpc4.Visible = False
                HLapoyoopc4.Visible = False
                Opcion4.Visible = False
            End If

            If misReactivos.Read() Then 'PASO A OPCION 5
                Redaccion = Replace(HttpUtility.HtmlDecode(misReactivos.Item("Redaccion")), "*salto*", "<BR>")
                Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                TextoOpc5.Text = Redaccion
                Opcion5.Text = CStr(misReactivos.Item("Consecutiva"))
                If Not IsDBNull(misReactivos.Item("TipoArchivoApoyo")) Then
                    If misReactivos.Item("TipoArchivoApoyo") = "Imagen" Then
                        ImOpc5.ImageUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo")
                        HLapoyoopc5.Visible = False
                    ElseIf Trim(misReactivos.Item("TipoArchivoApoyo")) = "Audio" Then
                        PnlDespliegaOpc5.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                        PnlDespliegaOpc5.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo") + """></audio>"))
                        ImOpc5.Visible = False
                        HLapoyoopc5.Visible = False
                    Else
                        HLapoyoopc5.NavigateUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo")
                        HLapoyoopc5.Text = Lang.FileSystem.Alumno.ContestarDemo.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                        ImOpc5.Visible = False
                    End If
                Else
                    ImOpc5.Visible = False
                    HLapoyoopc5.Visible = False
                End If
            Else
                ImOpc5.Visible = False
                HLapoyoopc5.Visible = False
                Opcion5.Visible = False
                trRbtn56.Visible = False
            End If

            If misReactivos.Read() Then 'PASO A OPCION 6
                Redaccion = Replace(HttpUtility.HtmlDecode(misReactivos.Item("Redaccion")), "*salto*", "<BR>")
                Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                TextoOpc6.Text = Redaccion
                Opcion6.Text = CStr(misReactivos.Item("Consecutiva"))
                If Not IsDBNull(misReactivos.Item("TipoArchivoApoyo")) Then
                    If misReactivos.Item("TipoArchivoApoyo") = "Imagen" Then
                        ImOpc6.ImageUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo")
                        HLapoyoopc6.Visible = False
                    ElseIf Trim(misReactivos.Item("TipoArchivoApoyo")) = "Audio" Then
                        PnlDespliegaOpc6.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                        PnlDespliegaOpc6.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo") + """></audio>"))
                        ImOpc6.Visible = False
                        HLapoyoopc6.Visible = False
                    Else
                        HLapoyoopc6.NavigateUrl = Config.Global.urlMaterial & misReactivos.Item("ArchivoApoyo")
                        HLapoyoopc6.Text = Lang.FileSystem.Alumno.ContestarDemo.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                        ImOpc6.Visible = False
                    End If
                Else
                    ImOpc6.Visible = False
                    HLapoyoopc6.Visible = False
                End If
            Else
                ImOpc6.Visible = False
                HLapoyoopc6.Visible = False
                Opcion6.Visible = False
            End If

        Else 'de If Trim(misReactivos.Item("TipoRespuesta")) = "Multiple"
            'Algoritmo cuando el tipo de respuesta es diferente de "Multiple"
            If Trim(misReactivos.Item("TipoRespuesta")) = "Abierta" Then
                'OPCIONES (Todos los tipos de respuestas generan Registros en la tabla "Opcion")
                miComando.CommandText = "SELECT * FROM Opcion where Esconder = 'False' and idPlanteamiento = " + misReactivos.Item("IdPlanteamiento").ToString + " order by Consecutiva"
                misReactivos.Close() 'Cierro la tabla de Planteamiento para poder usar este objeto de nuevo
                misReactivos = miComando.ExecuteReader() 'Creo conjunto de registros
                misReactivos.Read() 'Leo para poder accesarlos

                'Algoritmo respuesta abierta (solo existe un registro en Opcion asignado a la Respuesta
                PanelMultiple.Visible = False
                PanelAbierta.Visible = True
                'Como respuesta "Abierta" tiene una sola opcion, la genero por default
                OpcionL.Text = misReactivos.Item("Consecutiva") 'El texto está de color BLANCO para que no se vea (es la única respuesta)
                OpcionL.Checked = True
                OpcionL.Visible = False

            ElseIf Trim(misReactivos.Item("TipoRespuesta")) = "Abierta Calificada" Then
                PanelMultiple.Visible = False
                PanelAbierta.Visible = False
                PanelAbiertaAutomatica.Visible = True
                trPanelAbiertaMultiple.Visible = False


            End If
        End If
        misReactivos.Close()
        objConexion.Close()
        msgError.hide()
        'Catch ex As Exception
        '    Utils.LogManager.ExceptionLog_InsertEntry(ex)
        '    msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        'End Try
    End Sub
    Protected Sub AceptarAA_Click(sender As Object, e As EventArgs) Handles AceptarAA.Click
        processAnswer(evaluaRespuestaAbierta())
    End Sub

    Protected Sub CancelarAA_Click(sender As Object, e As EventArgs) Handles CancelarAA.Click
        Response.Redirect("~/alumno/cursos/actividad/" &
                         "?IdEvaluacion=" & Session("IdEvaluacion") &
                         "&Tarea=" & Session("Actividad") &
                         "&Alcance=" & Session("SNAlcance") &
                         "&Extra=" & Request.QueryString("Extra"))
    End Sub
    ' imprimo los estilos variables de ésta página
    Protected Sub OutputCss()
        Dim s As StringBuilder = New StringBuilder()
        s.Append("<style type='text/css'>")
        s.Append("  #menu .current_area_cursos a {") ' esta clase css debe corresponder a la clase del boton a resaltar
        s.Append("    border-bottom: 5px solid " & Config.Color.MenuBar_SelectedOption_BorderColor & ";")
        s.Append("  }")
        s.Append("  .AssignmentHeaderRow {")
        s.Append("    background-color: " & Config.Color.MenuPendientes_Subtitle & ";")
        s.Append("  }")
        s.Append("</style>")
        ltEstilos.Text += s.ToString()
    End Sub

#End Region

    Protected Sub Aceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Aceptar.Click
        Dim Respuesta As String


        If Opcion1.Checked Then
            Respuesta = Opcion1.Text
        ElseIf Opcion2.Checked Then
            Respuesta = Opcion2.Text
        ElseIf Opcion3.Checked Then
            Respuesta = Opcion3.Text
        ElseIf Opcion4.Checked Then
            Respuesta = Opcion4.Text
        ElseIf Opcion5.Checked Then
            Respuesta = Opcion5.Text
        ElseIf Opcion6.Checked Then
            Respuesta = Opcion6.Text
        ElseIf OpcionL.Checked Then
            Respuesta = OpcionL.Text
        Else : Respuesta = "0"
        End If
        If Respuesta = "0" Then
            msgError.show(Lang.FileSystem.Alumno.ContestarDemo.MSG_NO_ELIGIO_RESPUESTA.Item(Session("Usuario_Idioma")))
        Else
            msgError.hide()
            'Response.Redirect("~/alumno/cursos/contestardemo/procesa/" &
            '                  "?IdPlanteamiento=" & Session("IdPlanteamiento").ToString &
            '                  "&OpcRespuesta=" & Respuesta &
            '                  "&Acertadas=" & HFacertadas.Value.ToString &
            '                  "&Contador=" & HFcontador.Value.ToString &
            '                  "&Extra=" & Request.QueryString("Extra"))
            processAnswer(Respuesta)
        End If
    End Sub

    Protected Sub AceptarA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles AceptarA.Click
        Dim Respuesta As String
        If Opcion1.Checked Then
            Respuesta = Opcion1.Text
        ElseIf Opcion2.Checked Then
            Respuesta = Opcion2.Text
        ElseIf Opcion3.Checked Then
            Respuesta = Opcion3.Text
        ElseIf Opcion4.Checked Then
            Respuesta = Opcion4.Text
        ElseIf Opcion5.Checked Then
            Respuesta = Opcion5.Text
        ElseIf Opcion6.Checked Then
            Respuesta = Opcion6.Text
        ElseIf OpcionL.Checked Then
            Respuesta = OpcionL.Text
        Else : Respuesta = "0"
        End If
        If Respuesta = "0" Then
            Session("Consecutivo") = Session("valordeentrada")
            Session("Total") = Session("Total") - 1
            msgError.show(Lang.FileSystem.Alumno.ContestarDemo.MSG_NO_ELIGIO_RESPUESTA.Item(Session("Usuario_Idioma")))
        Else
            msgError.hide()
            processAnswer(Respuesta)
            'Response.Redirect("~/alumno/cursos/contestardemo/procesa/" &
            '                  "?IdPlanteamiento=" & Session("IdPlanteamiento").ToString &
            '                  "&OpcRespuesta=" & Respuesta &
            '                  "&Acertadas=" & HFacertadas.Value.ToString &
            '                  "&Contador=" + HFcontador.Value.ToString &
            '                  "&Extra=" & Request.QueryString("Extra"))
        End If
    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub processAnswer(ByVal respuesta As String)

        Try
            Dim strConexion As String
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objConexion As New SqlConnection(strConexion)
            Dim miComando As SqlCommand
            Dim misRegistros As SqlDataReader
            miComando = objConexion.CreateCommand

            miComando.CommandText = "select P.TipoRespuesta, O.IdOpcion, O.Consecutiva, O.Correcta from Opcion O, Planteamiento P where P.IdPlanteamiento = O.IdPlanteamiento and O.idPlanteamiento = " + Trim(Session("IdPlanteamiento").ToString) + " and O.Consecutiva = '" + respuesta + "'"
            objConexion.Open()
            misRegistros = miComando.ExecuteReader()
            misRegistros.Read()
            If misRegistros.Item("Correcta") = "N" Then
                msgErrorVerified.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), Lang.FileSystem.Alumno.ContestarDemo_Procesa.MSG_INCORRECTA.Item(Session("Usuario_Idioma")))
            Else
                msgSuccessVerified.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")), Lang.FileSystem.Alumno.ContestarDemo_Procesa.MSG_CORRECTA.Item(Session("Usuario_Idioma")))
                Session("Acertadas") = CStr(CInt(Session("Acertadas").ToString) + 1)
            End If

            LblFaltan.Text = Session("Contador") + " " & Lang.FileSystem.Alumno.ContestarDemo_Procesa.MSG_FALTAN_TOTALES.Item(Session("Usuario_Idioma")) + " " + Session("Acertadas")

            misRegistros.Close()
            objConexion.Close()

            If Session("Contador") = Session("TotalReactivos") Then
                BtnSiguiente.Visible = False
                Aceptar.Visible = False
                AceptarA.Visible = False
                Cancelar.Visible = False
                CancelarA.Visible = False
                AceptarAA.Visible = False
                CancelarAA.Visible = False
                InsertaActividadTerminada()
                BtnTerminar.Visible = True
            Else
                Session("Contador") = CStr(CInt(Session("Contador").ToString) + 1)
                Aceptar.Visible = False
                AceptarA.Visible = False
                Cancelar.Visible = False
                CancelarA.Visible = False
                AceptarAA.Visible = False
                CancelarAA.Visible = False
                BtnSiguiente.Visible = True
                BtnIniciar.Visible = False
                BtnTerminar.Visible = True
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub InsertaActividadTerminada()

        ' reviso si la actividad es de material; de ser así se califica con 100 por haber revisado uno, de no haber sido calificada ya
        If CType(New EvaluacionDa, EvaluacionDa).ObtenDatosEvaluacion(Session("IdEvaluacion")).Tipo = "Material" Then
            If IsNothing(CType(New EvaluacionTerminadaDa, EvaluacionTerminadaDa).Obten(Session("IdEvaluacion"),
                                                                                           Session("Usuario_IdAlumno"),
                                                                                           Session("Usuario_IdCicloEscolar"))) Then
                If Session("Contestar_FinContestar") >= Date.Now() Then
                    Try
                        Using conn As SqlConnection = New SqlConnection(ConfigurationManager.
                                                                        ConnectionStrings("sadcomeConnectionString").
                                                                        ConnectionString)
                            conn.Open()
                            Using cmd As SqlCommand = New SqlCommand(
                                " INSERT INTO EvaluacionTerminada (" &
                                "   IdAlumno, " &
                                "   IdGrado, " &
                                "   IdGrupo, " &
                                "   IdCicloEscolar, " &
                                "   IdEvaluacion, " &
                                "   Resultado, " &
                                "   FechaTermino " &
                                " ) VALUES ( " &
                                "   @IdAlumno, " &
                                "   @IdGrado, " &
                                "   @IdGrupo, " &
                                "   @IdCicloEscolar, " &
                                "   @IdEvaluacion, " &
                                "   100, " &
                                "   getdate()) ", conn)

                                cmd.Parameters.AddWithValue("@IdAlumno", Session("Usuario_IdAlumno"))
                                cmd.Parameters.AddWithValue("@IdGrado", Session("Usuario_IdGrado"))
                                cmd.Parameters.AddWithValue("@IdGrupo", Session("Usuario_IdGrupo"))
                                cmd.Parameters.AddWithValue("@IdCicloEscolar", Session("Usuario_IdCicloEscolar"))
                                cmd.Parameters.AddWithValue("@IdEvaluacion", Session("IdEvaluacion"))

                                cmd.ExecuteNonQuery()
                            End Using
                        End Using
                    Catch ex As Exception
                        Utils.LogManager.ExceptionLog_InsertEntry(ex)
                        Throw ex
                    End Try
                End If
            End If
        End If
    End Sub



    Protected Sub BtnSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSiguiente.Click


        Response.Redirect("~/alumno/cursos/contestardemo/" &
                          "?IdEvaluacion=" & Session("IdEvaluacion") &
                          "&IdAlumno=" & CStr(Session("Usuario_IdAlumno")) &
                          "&Acertadas=" & Session("Acertadas").ToString &
                          "&Contador=" & Session("Contador").ToString &
                          "&Extra=" & Request.QueryString("Extra"))
    End Sub

    Protected Sub BtnTerminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnTerminar.Click
        Response.Redirect("~/alumno/cursos/actividad/" &
                          "?IdEvaluacion=" & Session("IdEvaluacion") &
                          "&Tarea=" & Session("Actividad") &
                          "&Alcance=" & Session("SNAlcance") &
                          "&Extra=" & Request.QueryString("Extra"))
    End Sub

    Protected Sub BtnIniciar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnIniciar.Click
        Response.Redirect("~/alumno/cursos/contestardemo/" &
                          "?IdEvaluacion=" & Session("IdEvaluacion") &
                          "&IdAlumno=" & CStr(Session("Usuario_IdAlumno")) &
                          "&Acertadas=0" &
                          "&Contador=1" &
                          "&Extra=" & Request.QueryString("Extra"))
    End Sub

    Protected Sub Cancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cancelar.Click
        Response.Redirect("~/alumno/cursos/")
    End Sub

    Protected Sub CancelarA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CancelarA.Click
        Response.Redirect("~/alumno/cursos/")
    End Sub

    Protected Function evaluaRespuestaAbierta() As String
        Dim respuesta As String = tbRespuestaAbiertaAutomatica.Text
        Dim resultado As String = "-" 'pretende inicialmente que no coincidió con ninguna respuesta correcta
        Dim comparaMayusculas As Boolean = Boolean.Parse(HfComparaMayusculas.Value)
        Dim comparaAcentos As Boolean = Boolean.Parse(HfComparaAcentos.Value)
        Dim comparaSoloAlfanumerico As Boolean = Boolean.Parse(HfComparaSoloAlfanumerico.Value)
        Dim eliminaPuntuacion As Boolean = Boolean.Parse(HfPuntuacion.Value)

        Try
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.
                                                            ConnectionStrings("sadcomeConnectionString").
                                                            ConnectionString)
                conn.Open()
                Using cmd As SqlCommand = New SqlCommand("SELECT * FROM Opcion Where IdPlanteamiento = @IdPlanteamiento", conn)

                    cmd.Parameters.AddWithValue("@IdPlanteamiento", Session("IdPlanteamiento").ToString())

                    Using results As SqlDataReader = cmd.ExecuteReader()

                        While (results.Read())
                            If (Siget.Utils.StringUtils.ComparaRespuesta(results.Item("Redaccion").ToString(),
                                                                         respuesta,
                                                                         comparaMayusculas,
                                                                         comparaAcentos,
                                                                         comparaSoloAlfanumerico,
                                                                         eliminaPuntuacion) = 0) Then
                                resultado = results.Item("Consecutiva")
                                Exit While
                            End If
                        End While
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            Return False
        End Try

        Return resultado
    End Function

End Class
