﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/alumno/principal_alumno_5-1-0.master"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="alumno_cursos_contestardemo_procesa_Default" %>

<%@ PreviousPageType VirtualPath="~/alumno/cursos/contestardemo/Default.aspx" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/PageTitle_v1.ascx" TagPrefix="uc1" TagName="PageTitle_v1" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            text-align: center;
            width: 474px;
        }

        .style24 {
            width: 200px;
        }

        .style25 {
            width: 474px;
            text-align: center;
        }
    </style>

    <%-- Aquí imprimo los estilos variables de ésta página específica --%>
    <asp:Literal ID="ltEstilos" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">

    <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1" />

    <table style="margin-left: auto; margin-right: auto;">
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Label ID="LblRespuesta" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: medium; font-weight: 700"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="style25">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="style25">
                <asp:Label ID="LblContestadas" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: medium; font-weight: 700; color: #000099;"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="style25">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="style25">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="style23">
                <asp:Button ID="BtnSiguiente" runat="server" Text="[Siguiente]" CssClass="defaultBtn btnThemeBlue btnThemeMedium " />
                &nbsp;
                <asp:Button ID="BtnIniciar" runat="server" Text="[Iniciar nuevamente]"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                &nbsp;
                <asp:Button ID="BtnTerminar" runat="server" Text="[Terminar]" CssClass="defaultBtn btnThemeGrey btnThemeMedium " />
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:HiddenField ID="HFacertadas" runat="server" />

            </td>
            <td>

                <asp:HiddenField ID="HFcontador" runat="server" />

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

