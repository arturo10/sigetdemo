﻿Imports Siget

Imports System.Data.SqlClient

Partial Class alumno_cursos_contestardemo_procesa_Default
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Utils.Sesion.sesionAbierta() Then
            Session("Nota") = Lang.FileSystem.General.Msg_SuSesionExpiro(Session("Usuario_Idioma").ToString())
            Response.Redirect("~/")
        End If

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            aplicaLenguaje()

            initPageData()

            OutputCss()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = Lang.FileSystem.Alumno.ContestarDemo_Procesa.LT_TITULO.Item(Session("Usuario_Idioma"))

    PageTitle_v1.show(Lang.FileSystem.Alumno.ContestarDemo_Procesa.LT_TITULO.Item(Session("Usuario_Idioma")),
                      Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/check_box_list.png")

    BtnSiguiente.Text = Lang.FileSystem.Alumno.ContestarDemo_Procesa.BTN_SIGUIENTE.Item(Session("Usuario_Idioma"))
    BtnIniciar.Text = Lang.FileSystem.Alumno.ContestarDemo_Procesa.BTN_REINICIAR.Item(Session("Usuario_Idioma"))
    BtnTerminar.Text = Lang.FileSystem.Alumno.ContestarDemo_Procesa.BTN_TERMINAR.Item(Session("Usuario_Idioma"))
  End Sub

  ' inicializo los datos que necesito para los controles de la página
  Protected Sub initPageData()
    'SI LLEGA AQUÍ ES QUE SI ELIGIÓ ALGUNA RESPUESTA
    Try
      HFacertadas.Value = Trim(Request.QueryString("Acertadas"))
      HFcontador.Value = Trim(Request.QueryString("Contador"))
      Dim strConexion As String
      'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
      strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
      Dim objConexion As New SqlConnection(strConexion)
      Dim miComando As SqlCommand
      Dim misRegistros As SqlDataReader
      miComando = objConexion.CreateCommand

      'Verifico si la opcion es correcta
      miComando.CommandText = "select P.TipoRespuesta, O.IdOpcion, O.Consecutiva, O.Correcta from Opcion O, Planteamiento P where P.IdPlanteamiento = O.IdPlanteamiento and O.idPlanteamiento = " + Trim(Request.QueryString("IdPlanteamiento")) + " and O.Consecutiva = '" + Trim(Request.QueryString("OpcRespuesta")) + "'"
      objConexion.Open()
      misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
      misRegistros.Read() 'Leo para poder accesarlos
      If misRegistros.Item("Correcta") = "N" Then
        LblRespuesta.Text = "<div style='width: 100%; height: 100%; text-align: center; margin-top: 50px; margin-bottom: 50px; background: #FED2D3;'>" & Lang.FileSystem.Alumno.ContestarDemo_Procesa.MSG_INCORRECTA.Item(Session("Usuario_Idioma")) & " <img src='" & Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/cancel.png' style='vertical-align: middle; padding: 20px;'></div>"
        LblRespuesta.ForeColor = System.Drawing.ColorTranslator.FromHtml("#981921")
      Else
        LblRespuesta.Text = "<div style='width: 100%; height: 100%; text-align: center; margin-top: 50px; margin-bottom: 50px; background: #DAFECC;'>" & Lang.FileSystem.Alumno.ContestarDemo_Procesa.MSG_CORRECTA.Item(Session("Usuario_Idioma")) & " <img src='" & Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/accept.png' style='vertical-align: middle; padding: 20px;'></div>"
        LblRespuesta.ForeColor = System.Drawing.ColorTranslator.FromHtml("#278219")
        HFacertadas.Value = HFacertadas.Value + 1
      End If
      LblContestadas.Text = HFcontador.Value.ToString + " " & Lang.FileSystem.Alumno.ContestarDemo_Procesa.MSG_FALTAN_TOTALES.Item(Session("Usuario_Idioma")) & " " + HFacertadas.Value.ToString

      misRegistros.Close()
      objConexion.Close()

      If HFcontador.Value = Session("TotalReactivos") Then
        BtnSiguiente.Visible = False
      Else
        HFcontador.Value = HFcontador.Value + 1
        BtnIniciar.Visible = False
      End If
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

    ' imprimo los estilos variables de ésta página
    Protected Sub OutputCss()
        Dim s As StringBuilder = New StringBuilder()
        s.Append("<style type='text/css'>")
        s.Append("  #menu .current_area_cursos a {") ' esta clase css debe corresponder a la clase del boton a resaltar
        s.Append("    border-bottom: 5px solid " & Config.Color.MenuBar_SelectedOption_BorderColor & ";")
        s.Append("  }")
        s.Append("  .AssignmentHeaderRow {")
        s.Append("    background-color: " & Config.Color.MenuPendientes_Subtitle & ";")
        s.Append("  }")
        s.Append("</style>")
        ltEstilos.Text += s.ToString()
    End Sub

#End Region

    Protected Sub BtnSiguiente_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSiguiente.Click
        Response.Redirect("~/alumno/cursos/contestardemo/" &
                          "?IdEvaluacion=" & Session("IdEvaluacion") &
                          "&IdAlumno=" & CStr(Session("Usuario_IdAlumno")) &
                          "&Acertadas=" & HFacertadas.Value.ToString &
                          "&Contador=" & HFcontador.Value.ToString &
                          "&Extra=" & Request.QueryString("Extra"))
    End Sub

    Protected Sub BtnTerminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnTerminar.Click
        Response.Redirect("~/alumno/cursos/actividad/" &
                          "?IdEvaluacion=" & Session("IdEvaluacion") &
                          "&Tarea=" & Session("Actividad") &
                          "&Alcance=" & Session("SNAlcance") &
                          "&Extra=" & Request.QueryString("Extra"))
    End Sub

    Protected Sub BtnIniciar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnIniciar.Click
        Response.Redirect("~/alumno/cursos/contestardemo/" &
                          "?IdEvaluacion=" & Session("IdEvaluacion") &
                          "&IdAlumno=" & CStr(Session("Usuario_IdAlumno")) &
                          "&Acertadas=0" &
                          "&Contador=1" &
                          "&Extra=" & Request.QueryString("Extra"))
    End Sub
End Class
