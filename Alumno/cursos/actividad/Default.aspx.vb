﻿Imports Siget

Imports System.Data.SqlClient
Imports System.IO 'Para que funcione: FileInfo
Imports Siget.Config
Imports Siget.Beans
Imports Siget.DataAccess

Partial Class alumno_actividad_Default
  Inherits System.Web.UI.Page

#Region "init"

  Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Utils.Sesion.sesionAbierta()

    Session("Actividad") = Request.QueryString("Tarea").Trim() 'La usaré en scripts más adelante (ListaReactivos.aspx)
    'Estas variables de sesión las usaré para en ProcesaDemo.aspx
    Session("SNAlcance") = Request.QueryString("Alcance") 'Esta variable la necesito para guardar la sintaxis en ContestarDemo y ProcesaDemo.aspx ya que ese valor se necesita en Contestar.aspx Y Corregir.aspx
    Session("IdEvaluacion") = Trim(Request.QueryString("IdEvaluacion")) 'Para que se cargue el GridView
    Session("Login") = User.Identity.Name.Trim()

    ' aquí inicializo la página la primera vez que se carga
    If Not IsPostBack Then
      aplicaLenguaje()

      initPageData()

      OutputCss()
    End If

    If Not Session("Comenta_IdComentarioEvaluacion") = Nothing Then
      Session.Remove("Comenta_IdComentarioEvaluacion")
    End If
    If (Not IsNothing(Session("EsForo"))) Then
            CargaComentarios()
        End If

        GVmaterial.DataBind()
        GVactividaddemo.DataBind()
        GVmaterialadicional.DataBind()
        presentaInstruccionDeInicio()
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If (Not IsNothing(Session("Comenta_Deleted"))) Then
            ' al borrar un comentario, el comentario se elimina de la base de datos, pero el evento click() del boton de borrar
            ' se ejecuta despues del onLoad , donde se cargan los comentarios, por lo que el cambio no se ve reflejado.
            ' (borra de la base de datos después de presentar los comentarios; habrá uno fantasma)
            ' por lo tanto, muestro un diálogo de aviso de borrado (con postback) para que el usuario recargue la página y 
            ' se ejecute onload() cargando los comentarios ya actualizados.
            ' No se puede borrar un comentario de la lista de PnlComentarios.Controls por que no se guarda en el viewstate,
            ' y si alguien borra un comentario y edita otro inmediatamente después, al editar se referencia a un comentario erróneo
            ' que podría estar una posición adelante del comentario borrado.
            Session.Remove("Comenta_Deleted")
            lblMensajeInicio.Text = Lang.FileSystem.Alumno.Actividad.Msg_ComentarioBorrado.Item(Session("Usuario_Idioma").ToString())
            ModalPopupMessage.Show()
        End If
    End Sub


    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = Lang.FileSystem.Alumno.Actividad.Txt_Titulo.Item(Session("Usuario_Idioma").ToString())

        'GVmaterial.Caption = "<div style='width:100%;'><div style='width:350px; margin-left: auto; margin-right: auto;'><h3 style='background-color: #CC2300; color: #FFFFFF;'>" &
        '    Lang.FileSystem.Alumno.Actividad.GVMATERIAL_CAPTION & "</h3></div></div>" ' <5.2.0>

        GVmaterialadicional.Caption = "<h3>" & Lang.FileSystem.Alumno.Actividad.GvMaterialAdicional_Caption.Item(Session("Usuario_Idioma").ToString()) & "</h3>"

        'GVactividaddemo.Caption = "<h3>" & Lang.FileSystem.Alumno.Actividad.GVACTIVIDADDEMO_CAPTION & "</h3>" ' <5.2.0>

        lt_material_alternativo.Text = Lang.FileSystem.Alumno.Actividad.Msg_MaterialAlternativo.Item(Session("Usuario_Idioma").ToString())
        LBcontestar.Text = Lang.FileSystem.Alumno.Actividad.Btn_Iniciar.Item(Session("Usuario_Idioma").ToString())
        lt_btn_regresar_center.Text = Lang.FileSystem.Alumno.Actividad.Btn_Regresar.Item(Session("Usuario_Idioma").ToString())

        lt_hint_carga_archivos.Text = Lang.FileSystem.Alumno.Actividad.Txt_CargaArchivos.Item(Session("Usuario_Idioma").ToString())
        lt_carga_un_archivo.Text = Lang.FileSystem.Alumno.Actividad.Txt_CargaUnArchivo.Item(Session("Usuario_Idioma").ToString())
        lt_btn_cargar.Text = Lang.FileSystem.Alumno.Actividad.Btn_Cargar.Item(Session("Usuario_Idioma").ToString())
        lt_btn_eliminar.Text = Lang.FileSystem.Alumno.Actividad.Btn_Eliminar.Item(Session("Usuario_Idioma").ToString())
        LblPDF.Text = Lang.FileSystem.Alumno.Actividad.Btn_DescargaAcrobatReader.Item(Session("Usuario_Idioma").ToString())
        lt_btn_regresar_low.Text = Lang.FileSystem.Alumno.Actividad.Btn_Regresar.Item(Session("Usuario_Idioma").ToString())
        lt_TabMaterialApoyo.Text = Lang.FileSystem.Alumno.Actividad.Tab_Material.Item(Session("Usuario_Idioma").ToString())
        lt_Tab_Practicas.Text = Lang.FileSystem.Alumno.Actividad.Tab_Practicas.Item(Session("Usuario_Idioma").ToString())
        lblCerrarDialogo.Text = Lang.FileSystem.Alumno.Actividad.Btn_Cerrar.Item(Session("Usuario_Idioma").ToString())
        lblCancelarDialogo.Text = Lang.FileSystem.Alumno.Actividad.Btn_Cancelar.Item(Session("Usuario_Idioma").ToString())
        lblContinuarDialogo.Text = Lang.FileSystem.Alumno.Actividad.Btn_Continuar.Item(Session("Usuario_Idioma").ToString())
        Lt_No_Hay_Comentarios.Text = Lang.FileSystem.Alumno.Actividad.Lt_No_Hay_Comentarios.Item(Session("Usuario_Idioma").ToString())

    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub initPageData()
        PageTitle_v1.show(Request.QueryString("Tarea").Trim(),
                          Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/folder_brick.png")

        Dim SeCarga As Boolean
        Dim EsForo As Boolean
        Dim strConexion As String
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misReactivos As SqlDataReader
        miComando = objConexion.CreateCommand
        Try
            'NO SIRVE: miComando.CommandText = "select * from Evaluacion where IdEvaluacion = " + Request.QueryString("IdEvaluacion")
            'La variable Session("Usuario_IdPlantel") viene desde el inicio de sesión del alumno
            miComando.CommandText = "select distinct IdEvaluacion, IdCalificacion, ClaveBateria, ClaveAbreviada, Porcentaje, InicioContestar, FinSinPenalizacion, FinContestar, Tipo, Penalizacion, Estatus, Aleatoria, SeEntregaDocto, EsForo, Califica, Instruccion, PermiteSegunda " + _
                  " from Evaluacion where IdEvaluacion not in (select IdEvaluacion from EvaluacionPlantel where IdEvaluacion = @IdEvaluacion and IdPlantel = @IdPlantel) and IdEvaluacion = @IdEvaluacion " + _
                  " and IdEvaluacion not in (select IdEvaluacion from EvaluacionAlumno where IdEvaluacion = @IdEvaluacion and IdAlumno = @IdAlumno)" + _
                  " UNION " + _
                  " select distinct E.IdEvaluacion, E.IdCalificacion, E.ClaveBateria, E.ClaveAbreviada, E.Porcentaje, P.InicioContestar, P.FinSinPenalizacion, P.FinContestar, E.Tipo, E.Penalizacion, P.Estatus, E.Aleatoria, E.SeEntregaDocto, EsForo, E.Califica, E.Instruccion, E.PermiteSegunda " + _
                  " from Evaluacion E, EvaluacionPlantel P where E.IdEvaluacion = @IdEvaluacion and P.IdEvaluacion = E.IdEvaluacion and P.IdPlantel = @IdPlantel" + _
                  " and E.IdEvaluacion not in (select IdEvaluacion from EvaluacionAlumno where IdEvaluacion = @IdEvaluacion and IdAlumno = @IdAlumno)" + _
                  " UNION " + _
                  " select distinct E.IdEvaluacion, E.IdCalificacion, E.ClaveBateria, E.ClaveAbreviada, E.Porcentaje, A.InicioContestar, A.FinSinPenalizacion, A.FinContestar, E.Tipo, E.Penalizacion, A.Estatus, E.Aleatoria, E.SeEntregaDocto, EsForo, E.Califica, E.Instruccion, E.PermiteSegunda " + _
                  " from Evaluacion E, EvaluacionAlumno A where E.IdEvaluacion = @IdEvaluacion and A.IdEvaluacion = E.IdEvaluacion and A.IdAlumno = @IdAlumno"

            miComando.Parameters.AddWithValue("@IdEvaluacion", Request.QueryString("IdEvaluacion"))
            miComando.Parameters.AddWithValue("@IdAlumno", Session("Usuario_IdAlumno"))
            miComando.Parameters.AddWithValue("@IdPlantel", Session("Usuario_IdPlantel"))

            objConexion.Open() 'Abro la conexion
            misReactivos = miComando.ExecuteReader()
            misReactivos.Read()

            ' presento la instrucción de la actividad si la hay
            If Not IsDBNull(misReactivos.Item("Instruccion")) Then
                If misReactivos.Item("Instruccion").ToString() <> "" Then
                    pnlInstruccion.Visible = True
                    ltInstruccion.Text = HttpUtility.HtmlDecode(misReactivos.Item("Instruccion").ToString())
                End If
            End If

            Session("SegundaVuelta") = misReactivos.Item("PermiteSegunda")
            'Las siguientes variables las usaré para comparar más abajo
            Session("Contestar_InicioContestar") = misReactivos.Item("InicioContestar")
            Session("Contestar_FinContestar") = misReactivos.Item("FinContestar")
            Session("Contestar_FinSinPenalizacion") = misReactivos.Item("FinSinPenalizacion")

            'VERIFICO SI LA ACTIVIDAD CONTIENE ARCHIVO PARA SUBIR:
            If Not IsNothing(Session("EsForo")) Then Session.Remove("EsForo")

            If misReactivos.Item("SeEntregaDocto") = "True" Then
                'Cuando la actividad se trata de Entregar Archivos, no se permite contestar reactivos
                PanelIniciar.Visible = False
                PanelIniciar.Style("display") = "none"
                PnlCargar.Visible = True
                SeCarga = True
            ElseIf misReactivos.Item("EsForo") = "True" Then
                'Cuando la actividad se trata de Foro, no se permite contestar reactivos ni cargar archivos
                PanelIniciar.Visible = False
                PanelIniciar.Style("display") = "none"
                SeCarga = False
                PnlCargar.Visible = False
                EsForo = True
                PnlForo.Visible = True
                Session("EsForo") = "True"
            Else
                PanelIniciar.Visible = True
                PnlCargar.Visible = False
                SeCarga = False
            End If

            misReactivos.Close()

            'Checo si ya tenía cargado el archivo (es decir que ya contestó la Actividad), para ponerlo en la liga
            If SeCarga Then
                miComando.Parameters.Clear()
                miComando.CommandText = "select Documento, Retroalimentacion from DoctoEvaluacion where IdEvaluacion = @IdEvaluacion and IdAlumno = @IdAlumno"
                miComando.Parameters.AddWithValue("@IdEvaluacion", Request.QueryString("IdEvaluacion"))
                miComando.Parameters.AddWithValue("@IdAlumno", Session("Usuario_IdAlumno"))

                misReactivos = miComando.ExecuteReader()
                If misReactivos.Read() Then
                    'LblDocumento.Text = misReactivos.Item("Documento")
                    If Not IsDBNull(misReactivos.Item("Retroalimentacion")) Then
                        msgInfo.show(Lang.FileSystem.Alumno.Actividad.Msg_Retroalimentacion.Item(Session("Usuario_Idioma").ToString()), "<b>" + misReactivos.Item("Retroalimentacion") + "</b>") ' TODO 'teacher's feedback'
                    End If
                    HLdocumento.Text = misReactivos.Item("Documento")
                    HLdocumento.NavigateUrl = Config.Global.urlCargas & Trim(User.Identity.Name) + "/" + misReactivos.Item("Documento")
                Else
                    HLdocumento.Text = ""
                    HLdocumento.NavigateUrl = ""
                End If
                misReactivos.Close()
            End If

            ' reviso si la evaluación tiene reactivos; de no ser asi, lo indico y escondo el 
            If Not CType(New EvaluacionDa, EvaluacionDa).Evaluacion_TieneReactivos(Request.QueryString("IdEvaluacion"), objConexion) Then
                PanelIniciar.Visible = False
                PanelIniciar.Style("display") = "none"
                If Not SeCarga Then
                    PanelRegresarCentro.Visible = True
                    PanelRegresarLow.Visible = False
                End If
            End If

            misReactivos.Close()
            objConexion.Close()
            'msgError.hide()
            'Si no tomo aquí el nombre de la página previa, en cualquier otro lado me aparece que es MuestraMaterial.aspx
            'If Session("Previa").Length <= 0 Then
            '    Session("Previa") = Page.Request.ServerVariables("HTTP_REFERER")
            '    Session("Previa") = Mid(Session("Previa"), Session("Previa").Length - 14, Session("Previa").Length)
            'End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    ' imprimo los estilos variables de ésta página
    Protected Sub OutputCss()
        Dim s As StringBuilder = New StringBuilder()
        s.Append("<style type='text/css'>")
        s.Append("  #menu .current_area_cursos a {") ' esta clase css debe corresponder a la clase del boton a resaltar
        s.Append("    border-bottom: 5px solid " & Config.Color.MenuBar_SelectedOption_BorderColor & ";")
        s.Append("  }")
        s.Append("</style>")
        ltEstilos.Text += s.ToString()
    End Sub

#End Region

    Protected Sub CargaComentarios(ByVal Padre As Panel, ByVal IdComentarioEvaluacion As Integer)

        Try
            Using conn As New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Using cmd As New SqlCommand("Consulta_ForoComentariosRelacionados", conn)
                    cmd.CommandType = Data.CommandType.StoredProcedure

                    cmd.Parameters.AddWithValue("@IdComentarioEvaluacion", IdComentarioEvaluacion)

                    Using results As SqlDataReader = cmd.ExecuteReader()
                        Dim comentario As ASP.controls_comentarioforo_ascx

                        While results.Read()

                            comentario = New ASP.controls_comentarioforo_ascx
                            comentario.IdComentarioEvaluacion = Integer.Parse(results.Item("IdComentarioEvaluacion").ToString())
                            comentario.IdEvaluacion = Integer.Parse(Session("IdEvaluacion").ToString())


                            If Not IsDBNull(results.Item("IdProfesor")) Then
                                comentario.IdProfesor = Integer.Parse(results.Item("IdProfesor").ToString())

                            Else
                                comentario.IdAlumno = Integer.Parse(results.Item("IdAlumno").ToString())
                                If results.Item("IdAlumno").ToString() = Session("Usuario_IdAlumno") Then
                                    comentario.EsEditable = True

                                End If
                            End If


                            comentario.isSecondReply = True

                            If Not IsDBNull(results.Item("Calificacion")) Then
                                comentario.IdEvaluacionT = Long.Parse(results.Item("IdEvaluacionT").ToString())
                                comentario.Calificacion = Integer.Parse(results.Item("Calificacion").ToString())
                            End If
                            If Not IsDBNull(results.Item("ArchivoAdjunto")) Then
                                comentario.ArchivoAdjunto = results.Item("ArchivoAdjunto").ToString()
                                comentario.ArchivoAdjuntoFisico = results.Item("ArchivoAdjuntoFisico").ToString()
                                comentario.Usuario = results.Item("Login").ToString()
                            End If
                            comentario.NombreUsuario = results.Item("Nombre").ToString().Trim()
                            comentario.Fecha = results.Item("Fecha")
                            comentario.Comentario = results.Item("Comentario")

                            If Session("Contestar_FinContestar") <= Date.Now() Then
                                comentario.EsEditable = False
                            ElseIf Session("Contestar_InicioContestar") >= Date.Now() Then
                                comentario.EsEditable = False
                            ElseIf CType(New EvaluacionTerminadaDa, EvaluacionTerminadaDa).TerminoEvalaucion(Request.QueryString("IdEvaluacion"),
                                                                  Session("Usuario_IdAlumno"),
                                                                  Session("Usuario_IdCicloEscolar"),
                                                                  Session("Usuario_IdGrado"),
                                                                  Session("Usuario_IdGrupo")) Then
                                comentario.EsEditable = False
                            End If
                            comentario.phrContenedorPadre = Padre
                            Padre.Controls.Add(comentario)



                        End While
                    End Using


                End Using
            End Using
        Catch ex As Exception
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try


    End Sub




    Protected Sub CargaComentarios()
        Dim hasComments As Boolean = False

        Try
            Using conn As New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()
                Using cmd As New SqlCommand("Consulta_ForoComentarios", conn)
                    cmd.CommandType = Data.CommandType.StoredProcedure

                    cmd.Parameters.AddWithValue("@IdEvaluacion", Session("IdEvaluacion").ToString())
                    cmd.Parameters.AddWithValue("@IdGrupo", Session("Usuario_IdGrupo").ToString())

                    Using results As SqlDataReader = cmd.ExecuteReader()
                        Dim comentario As ASP.controls_comentarioforo_ascx

                        While results.Read()
                            hasComments = True

                            comentario = New ASP.controls_comentarioforo_ascx
                            comentario.IdComentarioEvaluacion = Integer.Parse(results.Item("IdComentarioEvaluacion").ToString())
                            comentario.IdEvaluacion = Integer.Parse(Session("IdEvaluacion").ToString())

                            If Not IsDBNull(results.Item("IdProfesor")) Then
                                comentario.IdProfesor = Integer.Parse(results.Item("IdProfesor").ToString())

                            Else
                                comentario.IdAlumno = Integer.Parse(results.Item("IdAlumno").ToString())
                                If results.Item("IdAlumno").ToString() = Session("Usuario_IdAlumno") Then
                                    comentario.EsEditable = True
                                Else
                                    comentario.EsReplicable = True
                                End If
                            End If

                            If Not IsDBNull(results.Item("IdComentarioRelacionado")) Then
                                comentario.hasComments = True
                            End If

                            If Not IsDBNull(results.Item("Calificacion")) Then
                                comentario.IdEvaluacionT = Long.Parse(results.Item("IdEvaluacionT").ToString())
                                comentario.Calificacion = Integer.Parse(results.Item("Calificacion").ToString())
                            End If
                            If Not IsDBNull(results.Item("ArchivoAdjunto")) Then
                                comentario.ArchivoAdjunto = results.Item("ArchivoAdjunto").ToString()
                                comentario.ArchivoAdjuntoFisico = results.Item("ArchivoAdjuntoFisico").ToString()
                                comentario.Usuario = results.Item("Login").ToString()
                            End If
                            comentario.NombreUsuario = results.Item("Nombre").ToString().Trim()
                            comentario.Fecha = results.Item("Fecha")
                            comentario.Comentario = results.Item("Comentario")

                            If Session("Contestar_FinContestar") <= Date.Now() Then
                                comentario.EsEditable = False
                            ElseIf Session("Contestar_InicioContestar") >= Date.Now() Then
                                comentario.EsEditable = False
                            ElseIf CType(New EvaluacionTerminadaDa, EvaluacionTerminadaDa).TerminoEvalaucion(Request.QueryString("IdEvaluacion"),
                                                                  Session("Usuario_IdAlumno"),
                                                                  Session("Usuario_IdCicloEscolar"),
                                                                  Session("Usuario_IdGrado"),
                                                                  Session("Usuario_IdGrupo")) Then
                                comentario.EsEditable = False
                            End If
                            comentario.phrContenedorPadre = PnlComentarios

                            PnlComentarios.Controls.Add(comentario)


                            CargaComentarios(comentario.panelComments, comentario.IdComentarioEvaluacion)
                        End While
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex)
        End Try

        If Not hasComments Then
            msgInfo.show("No hay comentarios en esta actividad")
        End If
    End Sub

    Protected Sub UserControlID_buttonClick(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub GVmaterial_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmaterial.DataBound
        If GVmaterial.Rows.Count = 0 Then
            LblMensaje.Text = Lang.FileSystem.Alumno.Actividad.Msg_NoHayMaterial.Item(Session("Usuario_Idioma").ToString())
            LblPDF.Visible = False
            HLpdf.Visible = False
            If GVactividaddemo.Rows.Count = 0 Then
                pnlMateriales.Visible = False
            End If
            TabPanelMaterial.Visible = False
        Else
            LblPDF.Visible = True
            HLpdf.Visible = True
            TabPanelMaterial.Visible = True

            ' aqui oculto las descripciones de asignaturas, temas y subtemas cuando en las filas superiores están repetidos
            Dim currentAsignatura As String = ""
            Dim currentTema As String = ""
            Dim currentSubtema As String = ""
            For Each r As GridViewRow In GVmaterial.Rows()
                If r.RowType = DataControlRowType.DataRow Then
                    If currentAsignatura = "" Then
                        currentAsignatura = r.Cells(3).Text
                        currentTema = r.Cells(5).Text
                        currentSubtema = r.Cells(7).Text
                    Else

                        If r.Cells(3).Text = currentAsignatura Then
                            r.Cells(3).Text = "&nbsp;&nbsp;&nbsp;"""

                            If r.Cells(5).Text = currentTema Then
                                r.Cells(5).Text = "&nbsp;&nbsp;&nbsp;"""

                                If r.Cells(7).Text = currentSubtema Then
                                    r.Cells(7).Text = "&nbsp;&nbsp;&nbsp;"""
                                Else
                                    currentSubtema = r.Cells(7).Text
                                End If
                            Else
                                currentTema = r.Cells(5).Text
                                currentSubtema = r.Cells(7).Text
                            End If
                        Else
                            currentAsignatura = r.Cells(3).Text
                            currentTema = r.Cells(5).Text
                            currentSubtema = r.Cells(7).Text
                        End If

                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub GVmaterialadicional_DataBound(sender As Object, e As EventArgs) Handles GVmaterialadicional.DataBound
        If GVmaterialadicional.Rows.Count = 0 Then
            pnlApoyos.Visible = False
            pnlApoyos.Style("display") = "none"
        Else
            pnlApoyos.Visible = True
        End If
    End Sub

    Protected Sub GVmaterial_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVmaterial.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(0)
            LnkHeaderText.Text = Lang.FileSystem.Alumno.Actividad.GvMaterial_Material.Item(Session("Usuario_Idioma").ToString())

            LnkHeaderText = e.Row.Cells(2).Controls(0)
            LnkHeaderText.Text = Lang.FileSystem.Alumno.Actividad.GvMaterial_Tipo.Item(Session("Usuario_Idioma").ToString())

            LnkHeaderText = e.Row.Cells(3).Controls(0)
            LnkHeaderText.Text = Lang.FileSystem.Alumno.Actividad.GvMaterial_Asignatura.Item(Session("Usuario_Idioma").ToString())

            LnkHeaderText = e.Row.Cells(4).Controls(0)
            LnkHeaderText.Text = Lang.FileSystem.Alumno.Actividad.GvMaterial_NumeroTema.Item(Session("Usuario_Idioma").ToString())

            LnkHeaderText = e.Row.Cells(5).Controls(0)
            LnkHeaderText.Text = Lang.FileSystem.Alumno.Actividad.GvMaterial_Tema.Item(Session("Usuario_Idioma").ToString())

            LnkHeaderText = e.Row.Cells(6).Controls(0)
            LnkHeaderText.Text = Lang.FileSystem.Alumno.Actividad.GvMaterial_NumeroSubtema.Item(Session("Usuario_Idioma").ToString())

            LnkHeaderText = e.Row.Cells(7).Controls(0)
            LnkHeaderText.Text = Lang.FileSystem.Alumno.Actividad.GvMaterial_Subtema.Item(Session("Usuario_Idioma").ToString())
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim Lnk As LinkButton = e.Row.Cells(0).Controls(0)
            Lnk.Text = Lang.FileSystem.Alumno.Actividad.Gv_Abrir.Item(Session("Usuario_Idioma").ToString())
        End If
    End Sub

    Protected Sub GVmaterialadicional_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVmaterialadicional.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(1).Text = Lang.FileSystem.Alumno.Actividad.GvMaterialAdicional_Consecutivo.Item(Session("Usuario_Idioma").ToString())

            e.Row.Cells(3).Text = Lang.FileSystem.Alumno.Actividad.GvMaterialAdicional_ArchivoConsulta.Item(Session("Usuario_Idioma").ToString())

            e.Row.Cells(4).Text = Lang.FileSystem.Alumno.Actividad.GvMaterialAdicional_Descripcion.Item(Session("Usuario_Idioma").ToString())
        End If
    End Sub

    Protected Sub GVactividaddemo_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVactividaddemo.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(2).Text = Lang.FileSystem.Alumno.Actividad.GvActividadDemo_Actividad.Item(Session("Usuario_Idioma").ToString())
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim Lnk As LinkButton = e.Row.Cells(0).Controls(0)
            Lnk.Text = Lang.FileSystem.Alumno.Actividad.GvActividadDemo_Iniciar.Item(Session("Usuario_Idioma").ToString())
        End If
    End Sub

    Protected Sub GVmaterial_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVmaterial.SelectedIndexChanged
        LblMensaje.Text = Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(3).Text)) + " - " + _
                          Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(5).Text)) + " - " + _
                          Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(7).Text))
        PnlLink.Visible = False
        PnlDespliega.Controls.Add(New LiteralControl("<Table align='left' border='1' bordercolor = '#CCCCCC'>")) 'Agrego el inicio de la Tabla

        If Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(2).Text)) = "Imagen" Then
            PnlDespliega.Controls.Add(New LiteralControl("<IMG SRC=""" & Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0))) + """>"))
        ElseIf (Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(2).Text)) = "Video") Then
            'Acepta videos en formato .mpg y .avi hasta ahorita probados
            PnlLink.Visible = True
            HLarchivo.Text = HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0))
            HLarchivo.NavigateUrl = Config.Global.urlMaterial & HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0))
            HLarchivo.Target = "_blank"
            If HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0)).Contains("webm") Then
                PnlDespliega.Controls.Add(New LiteralControl("<video src=""" & Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0))) + """   type='video/webm; codecs=""vp8, vorbis""' width=""588""  autoplay=""autoplay"" loop=""loop"" controls /></video>"))
            Else
                PnlDespliega.Controls.Add(New LiteralControl("<embed src=""" & Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0))) + """ width=""588"" height=""476"" autostart=""true"" controller=""true"" align=""center""></embed>")) 'Si quiero que no se vea y se escuche una vez, le puedo agregar: hidden=""true
            End If

        ElseIf (Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(2).Text)) = "Audio") Then
            PnlDespliega.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:460px;")
            PnlDespliega.Controls.Add(New LiteralControl("<audio id=""reproductor""  src=""" + Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0))) + """></audio>"))

        ElseIf Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(2).Text)) = "YouTube" Then
            Dim VideoID As String = Regex.Match(GVmaterial.SelectedDataKey(0), "youtu(?: \.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)").Groups(1).Value

            PnlDespliega.Controls.Add(New LiteralControl("<object><param name='movie' value='http://www.youtube.com/v/" & VideoID & "'></param><param name='wmode' value='transparent'></param><param name='allowFullScreen' value='true'></param><embed src='http://www.youtube.com/v/" & VideoID & "' type='application/x-shockwave-flash' allowfullscreen='true' wmode='transparent' width='640' height='390'></embed></object>"))

        ElseIf Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(2).Text)) = "FLV" Then
            'ASÍ FUNCIONABA ANTES CON EL FLVPLAYER: PnlDespliega.Controls.Add(New LiteralControl("<object type=""application/x-shockwave-flash"" width=""600"" height=""450"" wmode=""transparent"" data=""/" & Config.Global.NOMBRE_FILESYSTEM & "/flvplayer.swf?file=Resources/apoyo/prueba.flv&autoStart=true""><param name=""movie"" value=""/" & Config.Global.NOMBRE_FILESYSTEM & "/flvplayer.swf?file=apoyo/prueba.flv&autoStart=true"" /><param name=""wmode"" value=""transparent"" /></object>"))
            'PnlDespliega.Controls.Add(New LiteralControl("<object type=""application/x-shockwave-flash"" width=""700"" height=""520"" wmode=""transparent"" data=""/" & Config.Global.NOMBRE_FILESYSTEM & "/flvplayer.swf?file=material/" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + "&autoStart=true""><param name=""movie"" value=""/" & Config.Global.NOMBRE_FILESYSTEM & "/flvplayer.swf?file=material/" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + "&autoStart=true"" /><param name=""wmode"" value=""transparent"" /></object>"))
            PnlDespliega.Controls.Add(New LiteralControl("<table style='margin-left: auto; margin-right: auto;'><tr><td><script type=""text/javascript"" src=""/" & Config.Global.NOMBRE_FILESYSTEM & "/usuarios/flash/jwplayer.js""></script><div id=""container"">Cargando el video ...</div><script type=""text/javascript""> jwplayer(""container"").setup({ flashplayer: ""/" & Config.Global.NOMBRE_FILESYSTEM & "/usuarios/flash/player.swf"", file: """ & Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0))) + """, autostart: true, height: 420, width: 680 }); </script></td></tr></table>"))
        ElseIf Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(2).Text)) = "SWF" Then
            PnlDespliega.Controls.Add(New LiteralControl("<object classid=""clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"" width=""680"" height=""420""><param name=""allowScriptAccess"" value=""never""/><param name=""allowNetworking"" value=""internal"" /><param name=""movie"" value=""" & Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0))) + """/><param name=""wmode"" value=""transparent"" /><embed type=""application/x-shockwave-flash"" allowScriptAccess=""never"" allowNetworking=""internal"" src=""" & Config.Global.urlMaterial & Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0))) + """ width=""680"" height=""420"" wmode=""transparent""/></object>"))
        ElseIf Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(2).Text)) = "Link" Then
            'PnlDespliega.Controls.Add(New LiteralControl("<b><font face=""Arial"" size=3><a href=""" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + """ target=""_blank"">" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + "</a></font></b>"))
            PnlLink.Visible = True
            HLarchivo.Text = HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0))
            HLarchivo.NavigateUrl = HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0))
            HLarchivo.Target = "_blank"
            Response.Write("<script>window.open('" + HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0)) + "','_blank');</script>")
        Else
            'Si llega aquí queda como una liga a un documento:
            'PnlDespliega.Controls.Add(New LiteralControl("<b><font face=""Arial"" size=3><a href=""/" & Config.Global.NOMBRE_FILESYSTEM & "/material/" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + """ target=""_blank"">" + Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(6).Text)) + "</a></font></b>"))
            PnlLink.Visible = True
            HLarchivo.Text = HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0))
            If Trim(HttpUtility.HtmlDecode(GVmaterial.SelectedRow.Cells(2).Text)) = "PDF" And Siget.Config.Global.DOCUMENTO_PROTEGIDO = True Then
                HLarchivo.NavigateUrl = Config.Global.urlMaterialPDF & HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0))
                HLarchivo.Target = "_blank"
                Response.Write("<script>window.open('" & Config.Global.urlMaterialPDF & HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0)) + "','_blank','width=850,height=700,scrollbars=yes,resizable=yes','True');</script>")
            Else
                HLarchivo.NavigateUrl = Config.Global.urlMaterial & HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0))
                HLarchivo.Target = "_blank"
                Response.Write("<script>window.open('" & Config.Global.urlMaterial & HttpUtility.HtmlDecode(GVmaterial.SelectedDataKey(0)) + "','_blank','width=850,height=700,scrollbars=yes,resizable=yes','True');</script>")
            End If

        End If
        PnlDespliega.Controls.Add(New LiteralControl("</Table>"))

        ' reviso si la actividad es de material; de ser así se califica con 100 por haber revisado uno, de no haber sido calificada ya
        If CType(New EvaluacionDa, EvaluacionDa).ObtenDatosEvaluacion(Session("IdEvaluacion")).Tipo = "Material" Then
            If IsNothing(CType(New EvaluacionTerminadaDa, EvaluacionTerminadaDa).Obten(Session("IdEvaluacion"),
                                                                                           Session("Usuario_IdAlumno"),
                                                                                           Session("Usuario_IdCicloEscolar"))) Then
                If Session("Contestar_FinContestar") >= Date.Now() Then
                    Try
                        Using conn As SqlConnection = New SqlConnection(ConfigurationManager.
                                                                        ConnectionStrings("sadcomeConnectionString").
                                                                        ConnectionString)
                            conn.Open()
                            Using cmd As SqlCommand = New SqlCommand(
                                " INSERT INTO EvaluacionTerminada (" &
                                "   IdAlumno, " &
                                "   IdGrado, " &
                                "   IdGrupo, " &
                                "   IdCicloEscolar, " &
                                "   IdEvaluacion, " &
                                "   Resultado, " &
                                "   FechaTermino " &
                                " ) VALUES ( " &
                                "   @IdAlumno, " &
                                "   @IdGrado, " &
                                "   @IdGrupo, " &
                                "   @IdCicloEscolar, " &
                                "   @IdEvaluacion, " &
                                "   100, " &
                                "   getdate()) ", conn)

                                cmd.Parameters.AddWithValue("@IdAlumno", Session("Usuario_IdAlumno"))
                                cmd.Parameters.AddWithValue("@IdGrado", Session("Usuario_IdGrado"))
                                cmd.Parameters.AddWithValue("@IdGrupo", Session("Usuario_IdGrupo"))
                                cmd.Parameters.AddWithValue("@IdCicloEscolar", Session("Usuario_IdCicloEscolar"))
                                cmd.Parameters.AddWithValue("@IdEvaluacion", Session("IdEvaluacion"))

                                cmd.ExecuteNonQuery()
                            End Using
                        End Using
                    Catch ex As Exception
                        Utils.LogManager.ExceptionLog_InsertEntry(ex)
                        Throw ex
                    End Try
                End If
            End If
        End If
    End Sub

    Protected Sub GVactividaddemo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVactividaddemo.SelectedIndexChanged
        Dim strConexion As String
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misReactivos As SqlDataReader
        miComando = objConexion.CreateCommand
        Try
            miComando.CommandText = "SELECT Count(*) Total FROM Planteamiento P, EvaluacionDemo ED where ED.IdEvaluacion = @IdEvaluacion and P.IdPlanteamiento = ED.IdPlanteamiento"
            miComando.Parameters.AddWithValue("@IdEvaluacion", Request.QueryString("IdEvaluacion"))

            objConexion.Open() 'Abro la conexion
            misReactivos = miComando.ExecuteReader()
            misReactivos.Read()
            Session("TotalReactivos") = misReactivos.Item("Total")
            misReactivos.Close()
            objConexion.Close()
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try

        Response.Redirect("~/alumno/cursos/contestardemo/?IdEvaluacion=" + Trim(Request.QueryString("IdEvaluacion")) + "&IdAlumno=" + CStr(Session("Usuario_IdAlumno")) + "&Acertadas=0&Contador=1&Extra=" & Request.QueryString("Extra"))
    End Sub

    Protected Sub LBcontestar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBcontestar.Click
        ' bandera de control de contestado
        Dim contesta As Integer = 0 ' 0 : no contesta - 1 : primera vuelta - 2 : segunda vuelta

        ' esto debe ir antes que cualquier operación que utilice base de datos
        If Utils.Bloqueo.sistemaBloqueado() Then
            Utils.Sesion.CierraSesion()
            Response.Redirect("~/EnMantenimiento.aspx")
        End If

        ' limpio las variables de la sesión de contestado si están definidas
        'Utils.Sesion.LimpiaSesionContestado()

        'PRIMERO VERIFICO SI ESTÁ SERIADA LA ACTIVIDAD
        If Not CType(New SeriacionEvaluacionesDa, SeriacionEvaluacionesDa).VerificaActividadesPrevias(Request.QueryString("IdEvaluacion"), Session("Usuario_IdAlumno").ToString) Then
            lblMensajeInicio.Text = Session("Nota")
            Session.Remove("Nota")
            CType(New MessageError(LBcontestar, Me.GetType(), Session("Nota")), MessageError).show()
        ElseIf Session("Contestar_FinContestar") <= Date.Now() Then
            CType(New MessageError(LBcontestar, Me.GetType(), Lang.FileSystem.Alumno.Actividad.Msg_FechaLimiteExcedida.Item(Session("Usuario_Idioma").ToString())), MessageError).show()
          
        ElseIf Session("Contestar_InicioContestar") >= Date.Now() Then
            CType(New MessageError(LBcontestar, Me.GetType(), Lang.FileSystem.Alumno.Actividad.Msg_NoHaIniciado.Item(Session("Usuario_Idioma").ToString())), MessageError).show()
        ElseIf CType(New EvaluacionTerminadaDa, EvaluacionTerminadaDa).TerminoEvalaucion(Request.QueryString("IdEvaluacion"),
                                              Session("Usuario_IdAlumno"),
                                              Session("Usuario_IdCicloEscolar"),
                                              Session("Usuario_IdGrado"),
                                              Session("Usuario_IdGrupo")) Then
            If Trim(Request.QueryString("Extra")) = "B" Then
                ' reviso si no es actividad con borradores: 
                ' en ese caso si hay registro en EvaluacionTerminada, 
                ' pero se debe enviar a /Contestar/ nuevamente
                contesta = 1
                Session("Contestar_Borrador") = "true"
            Else
                Dim impide As Boolean = False
                ' si la evaluación permite segunda vuelta
                If Boolean.Parse(Session("SegundaVuelta").ToString()) Then
                    If Session("Contestar_FinSinPenalizacion") >= Date.Now() Then
                        ' revisa si hay reactivos candidatos a segunda vuelta
                        ' (cuando el reactivo permite segunda vuelta, y el alumno la sacó mal en la primera vuelta)
                        If CType(New RespuestaDa, RespuestaDa).SegundaVueltaPendiente(Request.QueryString("IdEvaluacion"),
                                                               Session("Usuario_IdAlumno")) Then
                            ' si el alumno termino la primera vuelta, hay segunda vuelta,
                            ' y no ha terminado la segunda vuelta, envía a segunda vuelta
                            contesta = 2
                        Else
                            impide = True
                        End If
                    Else
                        ' presento el mensaje 
                        lblMensajeInicio.Text = Lang.FileSystem.Alumno.Actividad.Msg_NoSegundaVueltaPorPenalizacion.Item(Session("Usuario_Idioma").ToString())
                        ModalPopupMessage.Show()
                        impide = False
                        contesta = 0
                    End If
                Else
                    impide = True
                End If

                If impide Then
                    ' si no hay borradores y ya terminó 1° y 2° vuelta
                    lblMensajeInicio.Text = Lang.FileSystem.Alumno.Actividad.Msg_NoHayMasIntentos.Item(Session("Usuario_Idioma").ToString())
                    ModalPopupMessage.Show()
                    contesta = 0
                End If
            End If
        Else
            contesta = 1
        End If

        If contesta Then
            If Not GVmaterial.Rows.Count() = 0 Then
                If Not GVmaterial.SelectedIndex <> -1 Then
                    Session("Contesta") = contesta
                    lblMensajeInicio.Text = Lang.FileSystem.Alumno.Actividad.Msg_RepasaPrimero.Item(Session("Usuario_Idioma").ToString())
                    btnCerrarDialogo.Visible = False
                    lbnCancelarDialogo.Visible = True
                    lbnContinuarDialogo.Visible = True
                    ModalPopupMessage.Show()
                Else
                    If contesta = 1 Then
                        EnviaContestar()
                    ElseIf contesta = 2 Then
                        EnviaCorregir()
                    End If
                End If
            Else
                ' si no hay materiales, inicia la evaluación
                If contesta = 1 Then
                    EnviaContestar()
                ElseIf contesta = 2 Then
                    EnviaCorregir()
                End If
            End If
        End If

    End Sub

    Protected Sub lbnContinuarDialogo_Click(sender As Object, e As EventArgs) Handles lbnContinuarDialogo.Click
        If Session("Contesta") = "1" Then
            Session.Remove("Contesta")
            EnviaContestar()
        ElseIf Session("Contesta") = "2" Then
            Session.Remove("Contesta")
            EnviaCorregir()
        End If
    End Sub

    Protected Sub EnviaContestar()
        Session("Contestar_IdEvaluacion") = Request.QueryString("IdEvaluacion").Trim()
        Session("Contestar_Alcance") = Request.QueryString("Alcance").Trim()
        Session("Contestar_Vuelta") = "Primera"
        Response.Redirect("~/alumno/cursos/contestar/")
    End Sub

    Protected Sub EnviaCorregir()
        Session("Contestar_IdEvaluacion") = Request.QueryString("IdEvaluacion").Trim()
        Session("Contestar_Actividad") = Request.QueryString("Tarea").Trim()
        Session("Contestar_Alcance") = Request.QueryString("Alcance").Trim()
        Session("Contestar_Vuelta") = "Segunda"
        Response.Redirect("~/alumno/cursos/corregir/")
    End Sub

    Protected Sub BtnCargar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCargar.Click
        'Si ya existe archivo cargado, primero lo elimino y luego lo vuelvo a subir
        'VERIFICO SI LA ACTIVIDAD CONTIENE ARCHIVO PARA SUBIR:
        If Session("Contestar_FinContestar") < Date.Now() Then
            msgError.show(Lang.FileSystem.Alumno.Actividad.Msg_CargaFechaVencida.Item(Session("Usuario_Idioma").ToString()))
        Else
            If Session("Contestar_InicioContestar") > Date.Now() Then
                msgError.show(Lang.FileSystem.Alumno.Actividad.Msg_CargaAunNoInicia.Item(Session("Usuario_Idioma").ToString()))
            Else
                If (RutaArchivo.HasFile) Then 'El atributo .HasFile compara si se indico un archivo
                    Dim strConexion As String
                    strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                    Dim objConexion As New SqlConnection(strConexion)
                    Dim miComando As SqlCommand
                    Dim misRegistros As SqlDataReader
                    miComando = objConexion.CreateCommand
                    Try
                        miComando.CommandText = "select * from DoctoEvaluacion where IdEvaluacion = @IdEvaluacion and IdAlumno = @IdAlumno"
                        miComando.Parameters.AddWithValue("@IdEvaluacion", Request.QueryString("IdEvaluacion"))
                        miComando.Parameters.AddWithValue("@IdAlumno", Session("Usuario_IdAlumno"))

                        objConexion.Open() 'Abro la conexion
                        misRegistros = miComando.ExecuteReader()
                        Dim Cargar As Boolean

                        If misRegistros.Read() Then
                            If IsDBNull(misRegistros.Item("Calificacion")) Then
                                Cargar = True
                            Else
                                Cargar = False
                                msgError.show(Lang.FileSystem.Alumno.Actividad.Msg_CargaYaCalifico.Item(Session("Usuario_Idioma").ToString()))
                            End If
                        Else
                            Cargar = True
                        End If

                        If Cargar Then
                            'ALGORITMO PARA CARGAR ARCHIVO
                            Dim ruta As String
                            ruta = Config.Global.rutaCargas

                            'Verifico si ya existe el directorio del alumno, si no, lo creo
                            ruta = ruta + Trim(User.Identity.Name) + "\" 'Esta ruta contempla un directorio como alumno nombrado como su login
                            If Not (Directory.Exists(ruta)) Then
                                Dim directorio As DirectoryInfo = Directory.CreateDirectory(ruta)
                            End If

                            'Lo siguiente es por si ya había subido archivo, lo borre antes de volver a subir uno nuevo. De esta manera,
                            'si esta subiendo un archivo con el mismo nombre pero es de él mismo, no importa. Pero en caso de que exista
                            'un archivo con el mismo nombre de otro usuario, entonces si le restringe el subirlo --> ESTO YA NO APLICARIA?
                            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                                conn.Open()

                                Using cmd As SqlCommand = New SqlCommand()
                                    cmd.Connection = conn

                                    cmd.CommandText = "select * from DoctoEvaluacion where IdEvaluacion = " + Request.QueryString("IdEvaluacion") + " and IdAlumno = " + Session("Usuario_IdAlumno").ToString

                                    Dim reader As SqlDataReader = cmd.ExecuteReader()

                                    If reader.Read() Then
                                        SDSdoctoevaluacion.DeleteCommand = "DELETE FROM DoctoEvaluacion WHERE IdEvaluacion = " + Request.QueryString("IdEvaluacion") + " and IdAlumno = " + Session("Usuario_IdAlumno").ToString
                                        SDSdoctoevaluacion.Delete()

                                        Dim MiArchivo As FileInfo = New FileInfo(ruta & Trim(reader.Item("Documento").ToString()))
                                        MiArchivo.Delete()
                                        HLdocumento.Text = ""
                                        HLdocumento.NavigateUrl = ""
                                    End If
                                    reader.Close()
                                End Using
                            End Using

                            'Ahora inserto el archivo
                            Dim MiArchivo2 As FileInfo = New FileInfo(ruta & RutaArchivo.FileName)
                            If MiArchivo2.Exists Then
                                'Avisa que ya existe, NO LO SUBE pero sí almacena el registro
                                msgError.show(Lang.FileSystem.Alumno.Actividad.Msg_CargaYaExiste.Item(Session("Usuario_Idioma").ToString()))
                            Else
                                'Ahora compruebo si quedó grabado en disco duro del servidor antes de afectar el registro de la base d datos
                                RutaArchivo.SaveAs(ruta & RutaArchivo.FileName)
                                MiArchivo2 = New FileInfo(ruta & RutaArchivo.FileName)
                                If MiArchivo2.Exists Then
                                    'LblDocumento.Text = RutaArchivo.FileName
                                    HLdocumento.Text = RutaArchivo.FileName
                                    HLdocumento.NavigateUrl = Config.Global.urlCargas & Trim(User.Identity.Name) + "/" + RutaArchivo.FileName

                                    SDSdoctoevaluacion.InsertCommand = "SET dateformat dmy; INSERT INTO DoctoEvaluacion (IdEvaluacion,IdAlumno,Documento, FechaEntregado) VALUES (" + Request.QueryString("IdEvaluacion") + "," + Session("Usuario_IdAlumno").ToString + ",'" + RutaArchivo.FileName + "', getdate())"
                                    SDSdoctoevaluacion.Insert()

                                    'envio un correo electrónico al profesor
                                    Try
                                        Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                                            conn.Open()
                                            Dim cmd As SqlCommand = New SqlCommand("SELECT P.Email, rtrim(A.Nombre) + ' ' + rtrim(A.ApePaterno) + ' ' + rtrim(A.ApeMaterno) AS NomAlumno FROM Profesor P, Alumno A, Usuario U, Grupo G, Programacion Pg, Asignatura Asi, Calificacion C, Evaluacion E WHERE U.Login = @Login AND U.IdUsuario = A.IdUsuario AND A.IdGrupo = G.IdGrupo AND Pg.IdGrupo = G.IdGrupo AND Pg.IdProfesor = P.IdProfesor AND Pg.IdAsignatura = Asi.IdAsignatura AND Asi.IdAsignatura = C.IdAsignatura AND C.IdCalificacion = E.IdCalificacion AND E.IdEvaluacion = @IdEvaluacion", conn)

                                            cmd.Parameters.AddWithValue("@Login", User.Identity.Name)
                                            cmd.Parameters.AddWithValue("@IdEvaluacion", Request.QueryString("IdEvaluacion"))

                                            Dim result As SqlDataReader = cmd.ExecuteReader()

                                            While result.Read()
                                                If Trim(result.Item("Email")).Length > 1 Then
                                                    If Config.Global.MAIL_ACTIVO Then
                                                        Utils.Correo.EnviaCorreo(Trim(result.Item("Email")),
                                                                                 Config.Global.NOMBRE_FILESYSTEM & Lang.FileSystem.Alumno.Actividad.Mail_Asunto.Item(Session("Usuario_Idioma").ToString()),
                                                                                 Lang.FileSystem.Alumno.Actividad.Mail_InicioContenido.Item(Session("Usuario_Idioma").ToString()) &
                                                        " " & result.Item("NomAlumno") & " " &
                                                        Lang.FileSystem.Alumno.Actividad.Mail_FinContenido.Item(Session("Usuario_Idioma").ToString()) &
                                                        " " & Session("Actividad") & ".",
                                                                                 False)
                                                    End If
                                                End If
                                            End While

                                            result.Close()
                                            cmd.Dispose()
                                            conn.Close()
                                        End Using

                                        LblMensaje.Text = Lang.FileSystem.Alumno.Actividad.Msg_CargaExitosaConCorreo.Item(Session("Usuario_Idioma").ToString())
                                        msgError.hide()
                                    Catch ex As Exception
                                        Utils.LogManager.ExceptionLog_InsertEntry(ex)
                                        LblMensaje.Text = Lang.FileSystem.Alumno.Actividad.Msg_CargaExitosa.Item(Session("Usuario_Idioma").ToString())
                                        msgError.hide()
                                    End Try
                                Else 'de If MiArchivo2.Exists Then
                                    msgError.show(Lang.FileSystem.Alumno.Actividad.Msg_CargaError.Item(Session("Usuario_Idioma").ToString()))
                                End If
                            End If
                        End If
                        misRegistros.Close()
                        objConexion.Close()
                    Catch ex As Exception
                        Utils.LogManager.ExceptionLog_InsertEntry(ex)
                        msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
                    End Try
                Else
                    msgError.show(Lang.FileSystem.Alumno.Actividad.Msg_CargaNoSeleccionoArchivo.Item(Session("Usuario_Idioma").ToString()))
                End If
            End If 'Del Session("Contestar_InicioContestar") < Date.Now() Then
        End If 'Del Session("Contestar_FinContestar") < Date.Now() Then
    End Sub

    Protected Sub BtnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnEliminar.Click
        'VERIFICO SI LA ACTIVIDAD CONTIENE ARCHIVO PARA SUBIR:
        Try
            If Trim(HLdocumento.Text).Length > 0 Then 'Significa que ya hay un archivo cargado
                Dim strConexion As String
                strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                Dim objConexion As New SqlConnection(strConexion)
                Dim miComando As SqlCommand
                Dim misRegistros As SqlDataReader
                miComando = objConexion.CreateCommand
                miComando.CommandText = "select * from DoctoEvaluacion where IdEvaluacion = " + Request.QueryString("IdEvaluacion") + " and IdAlumno = " + Session("Usuario_IdAlumno").ToString
                objConexion.Open() 'Abro la conexion
                misRegistros = miComando.ExecuteReader()
                Dim Borrar As Boolean

                If misRegistros.Read() Then
                    If IsDBNull(misRegistros.Item("Calificacion")) Then
                        Borrar = True
                    Else
                        Borrar = False
                        msgError.show(Lang.FileSystem.Alumno.Actividad.Msg_CargaYaCalifico.Item(Session("Usuario_Idioma").ToString()))
                    End If
                Else
                    Borrar = True
                End If
                misRegistros.Close()
                objConexion.Close()

                If Borrar Then
                    SDSdoctoevaluacion.DeleteCommand = "DELETE FROM DoctoEvaluacion WHERE IdEvaluacion = " + Request.QueryString("IdEvaluacion") + " and IdAlumno = " + Session("Usuario_IdAlumno").ToString
                    SDSdoctoevaluacion.Delete()

                    Dim ruta As String
                    ruta = Config.Global.rutaCargas
                    ruta = ruta + Trim(User.Identity.Name) + "\"
                    'Borro el archivo:
                    Dim MiArchivo As FileInfo = New FileInfo(ruta & Trim(HLdocumento.Text))
                    MiArchivo.Delete()
                    HLdocumento.Text = ""
                    HLdocumento.NavigateUrl = ""
                    LblMensaje.Text = Lang.FileSystem.Alumno.Actividad.Msg_CargaEliminado.Item(Session("Usuario_Idioma").ToString())
                    msgError.hide()
                End If
            Else
                msgError.show(Lang.FileSystem.Alumno.Actividad.Msg_CargaNoHayArchivoParaEliminar.Item(Session("Usuario_Idioma").ToString()))
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    ' ATENCIÓN, CONTROLA LBregresar.Click y lblRegresarCentro.Click
    Protected Sub LBregresar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LBregresar.Click, lblRegresarCentro.Click
        Response.Redirect("~/alumno/cursos/")
        'Dim unset As Boolean = False

        'If Session("Previa") Is Nothing Then
        '    unset = True
        'ElseIf Session("Previa").Equals("") Then
        '    unset = True
        'End If

        'If unset Then
        '    Response.Redirect("~/alumno/")
        'Else
        '    Response.Redirect("~/alumno/" + Session("Previa"))
        'End If
    End Sub

    Protected Sub GVactividaddemo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVactividaddemo.DataBound
        If GVactividaddemo.Rows.Count > 0 Then
            TabPanelPracticas.Visible = True
        Else
            If GVmaterial.Rows.Count = 0 Then
                pnlMateriales.Visible = False
            End If
            TabPanelPracticas.Visible = False
        End If
    End Sub

    Protected Sub presentaInstruccionDeInicio()
        Dim material As Boolean = False
        Dim apoyo As Boolean = False
        Dim practica As Boolean = False

        If GVmaterial.Rows.Count > 0 Then
            material = True
        End If

        If GVmaterialadicional.Rows.Count > 0 Then
            apoyo = True
        End If

        If GVactividaddemo.Rows.Count > 0 Then
            practica = True
        End If

        If material Or apoyo Then
            If practica Then
                LblLiga.Text = Lang.FileSystem.Alumno.Actividad.Txt_LigaConMaterialYPractica.Item(Session("Usuario_Idioma").ToString())
            Else
                LblLiga.Text = Lang.FileSystem.Alumno.Actividad.Txt_LigaConMaterial.Item(Session("Usuario_Idioma").ToString())
            End If
        Else
            If practica Then
                LblLiga.Text = Lang.FileSystem.Alumno.Actividad.Txt_LigaConPractica.Item(Session("Usuario_Idioma").ToString())
            Else
                LblLiga.Text = Lang.FileSystem.Alumno.Actividad.Txt_Liga.Item(Session("Usuario_Idioma").ToString())
            End If
        End If

    End Sub

    Protected Sub BtnNuevoComentario_Click(sender As Object, e As EventArgs) Handles BtnNuevoComentario.Click
        ' bandera de control de contestado
        Dim contesta As Integer = 0 ' 0 : no contesta - 1 : primera vuelta - 2 : segunda vuelta


        ' esto debe ir antes que cualquier operación que utilice base de datos
        If Utils.Bloqueo.sistemaBloqueado() Then
            Utils.Sesion.CierraSesion()
            Response.Redirect("~/EnMantenimiento.aspx")
        End If

        ' limpio las variables de la sesión de contestado si están definidas
        Utils.Sesion.LimpiaSesionContestado()

        If Session("Contestar_FinContestar") <= Date.Now() Then
            lblMensajeInicio.Text = Lang.FileSystem.Alumno.Actividad.Msg_FechaLimiteExcedida.Item(Session("Usuario_Idioma").ToString())
            ModalPopupMessage.Show()
        ElseIf Session("Contestar_InicioContestar") >= Date.Now() Then
            lblMensajeInicio.Text = Lang.FileSystem.Alumno.Actividad.Msg_NoHaIniciado.Item(Session("Usuario_Idioma").ToString())
            ModalPopupMessage.Show()
        ElseIf CType(New EvaluacionTerminadaDa, EvaluacionTerminadaDa).TerminoEvalaucion(Request.QueryString("IdEvaluacion"),
                                              Session("Usuario_IdAlumno"),
                                              Session("Usuario_IdCicloEscolar"),
                                              Session("Usuario_IdGrado"),
                                              Session("Usuario_IdGrupo")) Then
            lblMensajeInicio.Text = Lang.FileSystem.Alumno.Actividad.Msg_NoHayMasIntentos.Item(Session("Usuario_Idioma").ToString())
            ModalPopupMessage.Show()
        Else
            contesta = 1
        End If

        If contesta Then
            Session("Comenta_IdEvaluacion") = Session("IdEvaluacion")
            Session("Comenta_IdComentarioRelacionado") = Nothing
            Session("Comenta_Origen") = Context.Request.Url.AbsoluteUri
            Response.Redirect("~/Alumno/cursos/actividad/comenta/")
        End If
    End Sub
End Class
