﻿<%@ Page
  Title=""
  Language="C#"
  MasterPageFile="~/Alumno/principal_alumno_5-1-0.master"
  AutoEventWireup="true"
  CodeFile="Default.aspx.cs"
  Inherits="Alumno_cursos_contestar_comenta_Default"
  ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/PageTitle_v1.ascx" TagPrefix="uc1" TagName="PageTitle_v1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
  <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
  <style type="text/css">
    .style10 {
      width: 100%;
    }

    .style11 {
      text-align: center;
      height: 22px;
    }

    .style12 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: medium;
      font-weight: bold;
    }

    .style13 {
      height: 66px;
      text-align: center;
    }

    .style14 {
      text-align: center;
    }

    .style23 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: small;
      font-weight: bold;
    }

    .style24 {
      font-size: small;
      font-weight: bold;
    }

    .style25 {
      font-family: Arial, Helvetica, sans-serif;
    }

    .style26 {
      text-align: center;
      font-family: Arial, Helvetica, sans-serif;
      font-size: medium;
      font-weight: bold;
      color: #000099;
    }

    .style27 {
      width: 100%;
    }

    .style28 {
      width: 243px;
    }

    .style29 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: small;
      font-weight: bold;
      width: 392px;
      text-align: right;
      height: 16px;
    }

    .style30 {
      width: 392px;
      font-family: Arial, Helvetica, sans-serif;
      font-size: x-small;
      text-align: center;
    }

    .style32 {
      width: 244px;
    }

    .style33 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: x-small;
      text-align: center;
      color: #000066;
    }

    .style34 {
      width: 243px;
      height: 16px;
    }

    .style35 {
      height: 16px;
    }

    .style36 {
      width: 376px;
      font-family: Arial, Helvetica, sans-serif;
      font-size: small;
      font-weight: bold;
      text-align: right;
    }

    .style37 {
      height: 85px;
    }

    .style38 {
      width: 387px;
      font-family: Arial, Helvetica, sans-serif;
      font-size: small;
      font-weight: bold;
      text-align: center;
      height: 38px;
    }
  </style>

  <style type="text/css">
    .minTabStyle {
      margin-left: auto;
      margin-right: auto;
    }

      .minTabStyle .ajax__tab_header {
      }

        .minTabStyle .ajax__tab_header a {
          color: black;
        }

        .minTabStyle .ajax__tab_header .ajax__tab_outer {
          background: #e5e5e5;
          margin-right: 1px;
        }

        .minTabStyle .ajax__tab_header .ajax__tab_inner {
        }

      .minTabStyle .ajax__tab_tab {
        padding: 10px;
      }

      .minTabStyle .ajax__tab_hover .ajax__tab_outer {
      }

      .minTabStyle .ajax__tab_hover .ajax__tab_inner {
      }

      .minTabStyle .ajax__tab_active .ajax__tab_outer {
        background: #205C93;
      }

      .minTabStyle .ajax__tab_active .ajax__tab_inner {
      }

      .minTabStyle .ajax__tab_active a {
        color: mintcream;
      }

      .minTabStyle .ajax__tab_body {
        border: none;
        border-top: 6px solid #205C93;
        border-bottom: 4px solid #777;
        background: #f5f5f5;
        padding-top: 15px;
        padding-bottom: 15px;
      }

    .leftSpacer {
      padding-left: 20px;
    }

    .rightSpacer {
      padding-right: 20px;
    }
  </style>

  <%-- Aquí imprimo los estilos variables de ésta página específica --%>
  <asp:Literal ID="ltEstilos" runat="server" />

  <script type="text/javascript"
    src="/<%=Siget.Config.Global.NOMBRE_FILESYSTEM%>/Resources/scripts/tinymce/tinymce.min.js"></script>

  <script type="text/javascript" src="Default.js"></script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Contenedor" runat="Server">

  <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1" />

  <asp:Panel ID="pnlInstruccion" runat="server" Visible="false">
    <table style="margin-left: auto; margin-right: auto;">
      <tr>
        <td>
          <div style="text-align: left; max-width: 900px; /*border: 1px solid #444; padding: 10px; */">
            <asp:Literal ID="ltInstruccion" runat="server"></asp:Literal>
          </div>
        </td>
      </tr>
    </table>
  </asp:Panel>

  <table style="margin-left: auto; margin-right: auto;">
    <tr>
      <td>
        <asp:TextBox ID="TxtComentario" runat="server"
          ClientIDMode="Static"
          TextMode="MultiLine"
          CssClass="tinymce"></asp:TextBox>
        <br />
      </td>
    </tr>
    <tr>
      <td>
        <asp:LinkButton ID="LbAdjunto" runat="server"
          OnClick="LbAdjunto_Click"
          CssClass="LabelInfoDefault"></asp:LinkButton>
      </td>
    </tr>
    <tr>
      <td>
        <asp:FileUpload ID="FuAdjunto" runat="server" />
        &nbsp;
        <asp:Button ID="BtnQuitarAdjunto" runat="server" Visible="false"
          CssClass="defaultBtn btnThemeGrey btnThemeSlick"
          Text="Quitar Adjunto" OnClick="BtnQuitarAdjunto_Click" />
      </td>
    </tr>
    <tr>
      <td style="text-align: center;">
        <asp:Button ID="BtnGuardarCambios" runat="server" Visible="false"
          CssClass="defaultBtn btnThemeBlue btnThemeWide"
          Text="Guardar Cambios" OnClick="BtnGuardarCambios_Click" />
        &nbsp;
        <asp:Button ID="BtnNuevoComentario" runat="server"
          CssClass="defaultBtn btnThemeBlue btnThemeWide"
          Text="Aceptar" OnClick="BtnNuevoComentario_Click" />
        &nbsp;
        <asp:Button ID="BtnCancelar" runat="server"
          CssClass="defaultBtn btnThemeGrey btnThemeWide"
          Text="Cancelar" OnClick="BtnCancelar_Click" />
      </td>
    </tr>
  </table>
  <asp:HiddenField ID="HfAdjuntoFisico" runat="server" />
</asp:Content>
