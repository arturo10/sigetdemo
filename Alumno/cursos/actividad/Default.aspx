﻿<%@ Page Title=""
  Language="VB"
  MasterPageFile="~/alumno/principal_alumno_5-1-0.master"
  AutoEventWireup="false" 
  CodeFile="Default.aspx.vb"
  Inherits="alumno_actividad_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/PageTitle_v1.ascx" TagPrefix="uc1" TagName="PageTitle_v1" %>

<%@ Register Src="~/Controls/ComentarioForo.ascx" TagPrefix="uc1" TagName="ComentarioForo" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
  <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

<script type="text/javascript" src="<%=Siget.Config.Global.urlScripts%>audiojs/audiojs/audio.min.js"></script>

    <script type="text/javascript">
        audiojs.events.ready(function () {
            var as = audiojs.createAll({
                autoplay: true,
                autoload: "none"
            });

        });
    </script>
    
  <style type="text/css">
    .style10 {
      width: 100%;
    }

    .style11 {
      text-align: center;
      height: 22px;
    }

    .style12 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: medium;
      font-weight: bold;
    }

    .style13 {
      height: 66px;
      text-align: center;
    }

    .style14 {
      text-align: center;
    }

    .style23 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: small;
      font-weight: bold;
    }

    .style24 {
      font-size: small;
      font-weight: bold;
    }

    .style25 {
      font-family: Arial, Helvetica, sans-serif;
    }

    .style26 {
      text-align: center;
      font-family: Arial, Helvetica, sans-serif;
      font-size: medium;
      font-weight: bold;
      color: #000099;
    }

    .style27 {
      width: 100%;
    }

    .style28 {
      width: 243px;
    }

    .style29 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: small;
      font-weight: bold;
      width: 392px;
      text-align: right;
      height: 16px;
    }

    .style30 {
      width: 392px;
      font-family: Arial, Helvetica, sans-serif;
      font-size: x-small;
      text-align: center;
    }

    .style32 {
      width: 244px;
    }

    .style33 {
      font-family: Arial, Helvetica, sans-serif;
      font-size: x-small;
      text-align: center;
      color: #000066;
    }

    .style34 {
      width: 243px;
      height: 16px;
    }

    .style35 {
      height: 16px;
    }

    .style36 {
      width: 376px;
      font-family: Arial, Helvetica, sans-serif;
      font-size: small;
      font-weight: bold;
      text-align: right;
    }

    .style37 {
      height: 85px;
    }

    .style38 {
      width: 387px;
      font-family: Arial, Helvetica, sans-serif;
      font-size: small;
      font-weight: bold;
      text-align: center;
      height: 38px;
    }
  </style>

  <style type="text/css">
    .minTabStyle {
      margin-left: auto;
      margin-right: auto;
    }

      .minTabStyle .ajax__tab_header {
      }

        .minTabStyle .ajax__tab_header a {
          color: black;
        }

        .minTabStyle .ajax__tab_header .ajax__tab_outer {
          background: #e5e5e5;
          margin-right: 1px;
        }

        .minTabStyle .ajax__tab_header .ajax__tab_inner {
        }

      .minTabStyle .ajax__tab_tab {
        padding: 10px;
      }

      .minTabStyle .ajax__tab_hover .ajax__tab_outer {
      }

      .minTabStyle .ajax__tab_hover .ajax__tab_inner {
      }

      .minTabStyle .ajax__tab_active .ajax__tab_outer {
        background: #205C93;
      }

      .minTabStyle .ajax__tab_active .ajax__tab_inner {
      }

      .minTabStyle .ajax__tab_active a {
        color: mintcream;
      }

      .minTabStyle .ajax__tab_body {
        border: none;
        border-top: 6px solid #205C93;
        border-bottom: 4px solid #777;
        background: #f5f5f5;
        padding-top: 15px;
        padding-bottom: 15px;
      }

    .leftSpacer {
      padding-left: 20px;
    }

    .rightSpacer {
      padding-right: 20px;
    }
  </style>

  <%-- Aquí imprimo los estilos variables de ésta página específica --%>
  <asp:Literal ID="ltEstilos" runat="server" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">

  <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1" />

  <asp:Panel ID="pnlInstruccion" runat="server" Visible="false">
    <table style="margin-left: auto; margin-right: auto;">
      <tr>
        <td>
          <div style="text-align: left; max-width: 900px; /*border: 1px solid #444; padding: 10px; */">
            <asp:Literal ID="ltInstruccion" runat="server"></asp:Literal>
          </div>
        </td>
      </tr>
    </table>
  </asp:Panel>

  <asp:Panel ID="pnlMateriales" runat="server" Visible="true">
    <table class="style10">
      <tr>
        <td colspan="4">
          <asp:TabContainer ID="TabContainer1" runat="server"
            Width="920px"
            CssClass="minTabStyle">
            <asp:TabPanel ID="TabPanelMaterial" runat="server">
              <HeaderTemplate>
                <asp:Literal ID="lt_TabMaterialApoyo" runat="server">Material de Apoyo</asp:Literal>
              </HeaderTemplate>
              <ContentTemplate>
                <asp:GridView ID="GVmaterial" runat="server"
                  AllowSorting="True"
                  AutoGenerateColumns="False"
                  DataSourceID="SDSmaterial"
                  DataKeyNames="archivoMaterial"
                  Width="100%"
                  GridLines="None"
                  CellPadding="5"
                  Font-Bold="False"
                  Style="font-family: Arial, Helvetica, sans-serif; font-size: small; margin-left: auto; margin-right: auto;"
                  ForeColor="Black">
                  <Columns>
                    <asp:CommandField ShowSelectButton="True" SelectText="[Abrir]">
                      <ControlStyle ForeColor="White" Font-Bold="True"
                        CssClass="btn btn-primary" />
                      <ItemStyle CssClass="leftSpacer" />
                    </asp:CommandField>
                    <asp:BoundField DataField="Material" HeaderText="[Material]"
                      SortExpression="Material">
                      <ItemStyle Font-Bold="True" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Tipo"
                      HeaderText="[Tipo]"
                      SortExpression="Tipo">
                      <HeaderStyle CssClass="dataCell" />
                      <ItemStyle CssClass="dataCell" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Asignatura"
                      HeaderText="[ASIGNATURA]"
                      SortExpression="Asignatura">
                      <HeaderStyle CssClass="dataCell" />
                      <ItemStyle CssClass="dataCell" />
                    </asp:BoundField>
                    <asp:BoundField DataField="NumTema" HeaderText="[No. Tema]"
                      SortExpression="NumTema" Visible="False" />
                    <asp:BoundField DataField="Tema"
                      HeaderText="[Tema]"
                      SortExpression="Tema">
                      <HeaderStyle CssClass="dataCell" />
                      <ItemStyle CssClass="dataCell" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Numsubtema" HeaderText="[No. Subtema]"
                      SortExpression="Numsubtema" Visible="False" />
                    <asp:BoundField DataField="Subtema"
                      HeaderText="[Subtema]"
                      SortExpression="Subtema">
                      <HeaderStyle CssClass="dataCell" />
                      <ItemStyle CssClass="dataCell rightSpacer" />
                    </asp:BoundField>
                  </Columns>
                  <AlternatingRowStyle BackColor="#E5E5E5" />
                </asp:GridView>
              </ContentTemplate>
            </asp:TabPanel>
            <asp:TabPanel ID="TabPanelPracticas" runat="server">
              <HeaderTemplate>
                <asp:Literal ID="lt_Tab_Practicas" runat="server">Prácticas</asp:Literal>
              </HeaderTemplate>
              <ContentTemplate>
                <asp:GridView ID="GVactividaddemo" runat="server"
                  AutoGenerateColumns="False"
                  DataSourceID="SDSevaluacionDemo"
                  DataKeyNames="IdEvaluacion"
                  CellPadding="5"
                  GridLines="None"
                  Style="font-family: Arial, Helvetica, sans-serif; font-size: small; text-align: center; margin-left: auto; margin-right: auto;"
                  ForeColor="Black">
                  <Columns>
                    <asp:CommandField ShowSelectButton="True" SelectText="[Iniciar Práctica]">
                      <ControlStyle Font-Bold="True" ForeColor="White" Font-Size="small"
                        CssClass="btn btn-primary" />
                    </asp:CommandField>
                    <asp:BoundField DataField="IdEvaluacion" HeaderText="IdEvaluacion"
                      InsertVisible="False" ReadOnly="True" SortExpression="IdEvaluacion"
                      Visible="False" />
                    <asp:BoundField DataField="Actividad" HeaderText="[Ejercicio de Comprensión (No cuenta para calificación)]"
                      ReadOnly="True" SortExpression="Actividad">
                      <ItemStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                  </Columns>
                  <AlternatingRowStyle BackColor="#e5e5e5" />
                </asp:GridView>
              </ContentTemplate>
            </asp:TabPanel>
          </asp:TabContainer>
        </td>
      </tr>
      <tr>
        <td colspan="4">
          <asp:Panel ID="PnlLink" runat="server" Visible="False">
            <table class="style27">
              <tr>
                <td class="style38" style="background-color: #ffff66;">
                  <asp:Literal ID="lt_material_alternativo" runat="server">[Si el archivo no se abre automáticamente, da clic aquí:]</asp:Literal>
                </td>
                <td style="background-color: #ffff66;">
                  <asp:HyperLink ID="HLarchivo" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: medium; color: #000099">{HyperLink}</asp:HyperLink>
                </td>
              </tr>
            </table>
          </asp:Panel>
        </td>
      </tr>
      <tr>
        <td colspan="4">
          <asp:Panel ID="PnlDespliega" runat="server"
            BackColor="#E3EAEB" Style="text-align: center">
          </asp:Panel>
        </td>
      </tr>
    </table>
  </asp:Panel>
  <asp:Panel ID="pnlApoyos" runat="server" Visible="true">
    <table style="width: 100%;">
      <tr>
        <%--1 <asp:BoundField DataField="IdProgramacion" HeaderText="IdProgramacion"
                        SortExpression="IdProgramacion" Visible="False" />--%>
        <td colspan="4" style="padding-top: 15px;">
          <asp:GridView ID="GVmaterialadicional" runat="server"
            AutoGenerateColumns="False"
            BackColor="White"
            BorderColor="White"
            BorderStyle="Solid"
            GridLines="None"
            BorderWidth="1px"
            Caption="<h3>[Información sobre la evaluación:]</h3>"
            CellPadding="3"
            DataSourceID="SDSapoyoevaluacion"
            Font-Bold="False"
            Style="font-family: Arial, Helvetica, sans-serif; font-size: small; margin-left: auto; margin-right: auto;"
            ForeColor="Black">
            <Columns>
              <asp:BoundField DataField="IdEvaluacion" HeaderText="IdEvaluacion"
                SortExpression="IdEvaluacion" Visible="False" />
              <asp:BoundField DataField="Consecutivo" HeaderText="[Consecutivo]"
                SortExpression="Consecutivo">
                <HeaderStyle Width="70px" />
              </asp:BoundField>
              <asp:BoundField DataField="ArchivoApoyo" HeaderText="ArchivoApoyo"
                SortExpression="ArchivoApoyo" Visible="False" />
              <asp:HyperLinkField DataNavigateUrlFields="ArchivoApoyo"
                DataNavigateUrlFormatString="~/Resources/apoyo/{0}" DataTextField="ArchivoApoyo"
                HeaderText="[Archivo de Consulta]" Target="_blank"
                ItemStyle-CssClass="dataCell" />
              <asp:BoundField DataField="Descripcion" HeaderText="[Descripción]"
                SortExpression="Descripcion"
                ItemStyle-CssClass="dataCell" />
            </Columns>
            <FooterStyle CssClass="minimalist_1" />
            <HeaderStyle BackColor="#FFFF99" Font-Bold="True" ForeColor="Black" />
          </asp:GridView>
        </td>
      </tr>
    </table>
  </asp:Panel>
  <table style="width: 100%;">
    <tr>
      <td style="text-align: center">
        <asp:Label ID="LblMensaje" runat="server"
          CssClass="LabelInfoDefault"></asp:Label>
      </td>
    </tr>
    <tr>
      <td style="text-align: center">
        <uc1:msgError runat="server" ID="msgError" />
      </td>
    </tr>
  </table>
  <asp:Panel ID="PanelIniciar" runat="server">
    <table class="style10">
      <tr>
        <td class="style26" colspan="4">
          <asp:Label ID="LblLiga" runat="server"
            Text="[Una vez que haya repasado el material y realizado el ejercicio de comprensión, si es que hay, continúe con la Actividad:]"></asp:Label>
        </td>
      </tr>
      <tr>
        <td class="style14" colspan="4">
          <asp:LinkButton ID="LBcontestar" runat="server"
             CssClass="btn btn-primary" 
            Style="font-size: 1.5em; font-weight: 700;">[Iniciar la Evaluación]</asp:LinkButton>
        </td>
      </tr>
    </table>
  </asp:Panel>
  <asp:Panel ID="PanelRegresarCentro" runat="server" Visible="false">
    <table class="style10">
     
      <tr>
        <td class="style14" colspan="4">
          <asp:LinkButton ID="lblRegresarCentro" runat="server"
            CssClass="defaultBtn btnThemeGrey btnThemeWide"
            Style="font-size: 1.3em; font-weight: 700;">
            <asp:Image ID="Image2" runat="server"
              ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/bullet_back_2.png"
              CssClass="btnIcon" />
            <asp:Literal ID="lt_btn_regresar_center" runat="server">[Regresar]</asp:Literal>
          </asp:LinkButton>
        </td>
      </tr>
    </table>
  </asp:Panel>
  <table class="style10">
    <tr>
      <td colspan="4">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="4">
        <uc1:msgInfo runat="server" ID="msgInfo" />
      </td>
    </tr>
    <tr>
      <td colspan="4">
        <asp:Panel ID="PnlForo" runat="server" Visible="false">
          <table style="max-width: 1000px; margin-left: auto; margin-right: auto;">
            <tr>
              <td style="text-align: center;">
                <asp:Button ID="BtnNuevoComentario" runat="server"
                  CssClass="defaultBtn btnThemeBlue btnThemeWide"
                  Style="font-size: 1.3em; font-weight: 700;"
                  Text="Comentar" />
              </td>
            </tr>
            <tr>
              <td>
                <asp:Panel ID="PnlComentarios" runat="server"></asp:Panel>
              </td>
            </tr>
            <asp:PlaceHolder ID="PhrNoHayComentarios" runat="server" Visible="false">
              <tr>
                <td>
                  <asp:Label ID="Lt_No_Hay_Comentarios" runat="server" CssClass="LabelInfoDefault">No hay comentarios aún.</asp:Label>
                </td>
              </tr>
            </asp:PlaceHolder>
          </table>
        </asp:Panel>
      </td>
    </tr>
    <tr>
      <td colspan="4">
        <asp:Panel ID="PnlCargar" runat="server" BackColor="#E3EAEB">
          <table style="margin-left: auto; margin-right: auto;">
            <tr>
              <td class="style29">
                <asp:Literal ID="lt_hint_carga_archivos" runat="server">[Una vez terminada la Actividad, cargue el archivo de trabajo]</asp:Literal>
              </td>
              <td class="style34">
                <asp:FileUpload ID="RutaArchivo" runat="server" />
              </td>
              <td class="style35">
                <asp:HyperLink ID="HLdocumento" runat="server"
                  Style="font-family: Arial, Helvetica, sans-serif; font-size: small"
                  Target="_blank">HLdocumento</asp:HyperLink>
              </td>
            </tr>
            <tr>
              <td class="style30">
                <asp:Literal ID="lt_carga_un_archivo" runat="server">[(Solo se puede cargar un archivo, si requiere cargar varios archivos, 
                                comprímalos en un solo ZIP)]</asp:Literal>
              </td>
              <td class="style28">
                <asp:LinkButton ID="BtnCargar" runat="server"
                  CssClass="defaultBtn btnThemeBlue btnThemeMedium">
                  <asp:Image ID="Image3" runat="server"
                    ImageUrl="~/Resources/imagenes/btnIcons/woocons/Arrow Up.png"
                    CssClass="btnIcon"
                    Height="24" />
                  <asp:Literal ID="lt_btn_cargar" runat="server">[Cargar]</asp:Literal>
                </asp:LinkButton>
                &nbsp;
                                <asp:LinkButton ID="BtnEliminar" runat="server"
                                  CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                                  <asp:Image ID="Image1" runat="server"
                                    ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/cancel.png"
                                    CssClass="btnIcon"
                                    Height="24" />
                                  <asp:Literal ID="lt_btn_eliminar" runat="server">[Eliminar]</asp:Literal>
                                </asp:LinkButton>
              </td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </asp:Panel>
      </td>
    </tr>
    <tr>
      <td class="style14">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="style14">&nbsp;</td>
      <td>&nbsp;</td>
      <td>
        <table class="style32">
          <tr>
            <td class="style33">
              <asp:Label ID="LblPDF" runat="server"
                Text="[Descargar Acrobat Reader si no se pueden visualizar documentos PDF:]"></asp:Label>
            </td>
          </tr>
          <tr>
            <td style="text-align: center">
              <asp:HyperLink ID="HLpdf" runat="server" ImageUrl="~/Resources/imagenes/Acrobat.jpg"
                NavigateUrl="//get.adobe.com/es/reader/" Target="_blank">Acrobat Reader</asp:HyperLink>
            </td>
          </tr>
        </table>
      </td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <asp:Panel ID="PanelRegresarLow" runat="server">
    <table style="width: 100%;">
      <tr>
        <td colspan="4" style="text-align: center;">
          <asp:LinkButton ID="LBregresar" runat="server"
            CssClass="btn btn-default">
            <asp:Image ID="Image7" runat="server"
              ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/bullet_back_2.png"
              CssClass="btnIcon" />
            <asp:Literal ID="lt_btn_regresar_low" runat="server">[Regresar]</asp:Literal>
          </asp:LinkButton>
        </td>
      </tr>
    </table>
  </asp:Panel>

  <asp:Panel ID="pnlMessage" runat="server" CssClass="dialogOverwrite dialogRespuestaAbierta">
    <table>
      <tr>
        <td style="text-align: center;">
          <asp:Label ID="lblMensajeInicio" runat="server" CssClass="LabelInfoDefault"></asp:Label>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <asp:LinkButton ID="btnCerrarDialogo" runat="server"
            CssClass="defaultBtn btnThemeGrey btnThemeMedium">
            <asp:Image ID="imgAceptarCrear" runat="server"
              ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/accept.png"
              CssClass="btnIcon"
              Height="24" />
            <asp:Label ID="lblCerrarDialogo" runat="server">
                            Cerrar
            </asp:Label>
          </asp:LinkButton>
          &nbsp;
          <asp:LinkButton ID="lbnCancelarDialogo" runat="server" Visible="false"
            CssClass="defaultBtn btnThemeGrey btnThemeMedium">
            <asp:Image ID="Image4" runat="server"
              ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/cancel.png"
              CssClass="btnIcon"
              Height="24" />
            <asp:Label ID="lblCancelarDialogo" runat="server">
                  Cancelar
            </asp:Label>
          </asp:LinkButton>
          &nbsp;
          <asp:LinkButton ID="lbnContinuarDialogo" runat="server" Visible="false"
            CssClass="defaultBtn btnThemeGrey btnThemeMedium">
            <asp:Image ID="Image5" runat="server"
              ImageUrl="~/Resources/imagenes/btnIcons/woocons/Arrow Right.png"
              CssClass="btnIcon"
              Height="24" />
            <asp:Label ID="lblContinuarDialogo" runat="server">
                  Continuar
            </asp:Label>
          </asp:LinkButton>
        </td>
      </tr>
    </table>
  </asp:Panel>

  <table class="dataSources">
    <tr>
      <td>

        <asp:HiddenField ID="HFidalumno" runat="server" />

      </td>
      <td>

        <asp:SqlDataSource ID="SDSevaluacionDemo" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="select IdEvaluacion, 'PRÁCTICA PARA: ' + Clavebateria as Actividad
from Evaluacion E
where IdEvaluacion = @IdEvaluacion and IdEvaluacion in
(select IdEvaluacion from EvaluacionDemo)">
          <SelectParameters>
            <asp:SessionParameter Name="IdEvaluacion" SessionField="IdEvaluacion" />
          </SelectParameters>
        </asp:SqlDataSource>

      </td>
      <td>

        <asp:SqlDataSource ID="SDSapoyoevaluacion" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="
                        select distinct IdEvaluacion, Consecutivo, ArchivoApoyo, Descripcion from ApoyoEvaluacion
                        where IdEvaluacion = @IdEvaluacion
                        and IdProgramacion in 
                            (
                            select IdProgramacion from Programacion
                            where IdAsignatura = (select IdAsignatura from Calificacion C, Evaluacion E where E.IdEvaluacion = @IdEvaluacion and C.IdCalificacion = E.IdCalificacion)
                            and IdCicloEscolar = (select IdCicloEscolar from Calificacion C, Evaluacion E where E.IdEvaluacion = @IdEvaluacion and C.IdCalificacion = E.IdCalificacion)
                            and IdGrupo = (select IdGrupo from Alumno A, Usuario U where U.Login = @Login and A.IdUsuario = U.IdUsuario )
                            ) ORDER BY Consecutivo">
          <SelectParameters>
            <asp:SessionParameter Name="IdEvaluacion" SessionField="IdEvaluacion" />
            <asp:SessionParameter Name="Login" SessionField="Login" />
          </SelectParameters>
        </asp:SqlDataSource>

      </td>
      <td>

        <asp:SqlDataSource ID="SDSmaterial" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="SELECT     A.Descripcion AS Asignatura, T.Numero AS NumTema, T.Descripcion AS Tema, S.Numero AS Numsubtema, S.Descripcion AS Subtema, M.Descripcion AS Material, M.ArchivoApoyo AS archivoMaterial, M.TipoArchivoApoyo AS Tipo
FROM Evaluacion AS E 
INNER JOIN DetalleEvaluacion AS D ON E.IdEvaluacion = D.IdEvaluacion 
INNER JOIN Subtema AS S ON D.IdSubtema = S.IdSubtema 
INNER JOIN ApoyoSubtema AS M ON M.IdSubtema = S.IdSubtema
INNER JOIN Tema AS T ON S.IdTema = T.IdTema 
INNER JOIN Asignatura AS A ON T.IdAsignatura = A.IdAsignatura
WHERE (E.IdEvaluacion = @IdEvaluacion)
ORDER BY A.Descripcion, T.Descripcion, S.Descripcion, M.Consecutivo">
          <SelectParameters>
            <asp:SessionParameter Name="IdEvaluacion" SessionField="IdEvaluacion" />
          </SelectParameters>
        </asp:SqlDataSource>

      </td>
    </tr>
    <tr>
      <td>

        <asp:SqlDataSource ID="SDSdoctoevaluacion" runat="server"
          ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
          SelectCommand="SELECT * FROM [DoctoEvaluacion]"></asp:SqlDataSource>

      </td>
      <td>
        <asp:HiddenField ID="hfDestino" runat="server" />
      </td>
      <td>
        <asp:Button ID="HF1" runat="server" Text="HF1" />
        <asp:ModalPopupExtender ID="ModalPopupMessage" runat="server"
          PopupControlID="pnlMessage" TargetControlID="HF1" BackgroundCssClass="overlay">
        </asp:ModalPopupExtender>
      </td>
      <td>
        &nbsp;</td>
    </tr>
  </table>
</asp:Content>

