﻿Imports Siget
Imports Siget.Beans
Imports Siget.TransferObjects
Imports Siget.DataAccess
Imports Siget.BusinessLogic
Imports System.Data

Imports System.Data.SqlClient

Partial Class alumno_cursos_actividad_listareactivos_Default
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        If Not Utils.Sesion.sesionAbierta() Then
            Session("Nota") = Lang.FileSystem.General.Msg_SuSesionExpiro(Session("Usuario_Idioma").ToString())
            Response.Redirect("~/")
        End If

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            aplicaLenguaje()

            initPageData()

            OutputCss()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = Lang.FileSystem.Alumno.ListaReactivos.LT_TITULO.Item(Session("Usuario_Idioma"))

        hl_continuar.Text = Lang.FileSystem.Alumno.ListaReactivos.BTN_CONTINUAR.Item(Session("Usuario_Idioma"))
        LblMult.Text = Lang.FileSystem.Alumno.ListaReactivos.LB_REACTIVOS_MULTIPLES.Item(Session("Usuario_Idioma"))
        LblAb.Text = Lang.FileSystem.Alumno.ListaReactivos.LB_REACTIVOS_ABIERTA.Item(Session("Usuario_Idioma"))
        lt_resultado1.Text = Lang.FileSystem.Alumno.ListaReactivos.lt_resultado1.Item(Session("Usuario_Idioma"))
        lt_resultado2.Text = Lang.FileSystem.Alumno.ListaReactivos.lt_resultado2.Item(Session("Usuario_Idioma"))
        lt_segundavuelta.Text = Lang.FileSystem.Alumno.ListaReactivos.lt_segundavuelta.Item(Session("Usuario_Idioma"))
        lblAceptar.Text = Lang.FileSystem.Alumno.ListaReactivos.btnAceptarDialogo.Item(Session("Usuario_Idioma"))

    PageTitle_v1.show(Lang.FileSystem.Alumno.ListaReactivos.LT_TITULO_PAGINA.Item(Session("Usuario_Idioma")),
                      Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/check_box_list.png")
    LblActividad.Text = Session("Actividad")
  End Sub

  ' inicializo los datos que necesito para los controles de la página
  Protected Sub initPageData()
    If IsNothing(Session("IdEvaluacion")) Then
      Response.Redirect("~/")
    End If

    Dim idEvaluacion As String = Session("IdEvaluacion").ToString()
    Dim Actividad As String = Session("Actividad").ToString()
    Dim Contestar_Vuelta As String = Session("Contestar_Vuelta").ToString()


        presentaReactivosGeneral(Session("IdEvaluacion").ToString)
        presentaResultadoEvaluacion(Session("IdEvaluacion").ToString)

    ' limpio las variables de la sesión de contestado si están definidas
    Utils.Sesion.LimpiaSesionContestado()
    Session("IdEvaluacion") = idEvaluacion
    Session("Actividad") = Actividad
    Session("Contestar_Vuelta") = Contestar_Vuelta
  End Sub

  ' imprimo los estilos variables de ésta página
  Protected Sub OutputCss()
    'Dim s As StringBuilder = New StringBuilder()
    's.Append("<style type='text/css'>")
    's.Append("</style>")
    'ltEstilos.Text += s.ToString()
  End Sub

#End Region


    Protected Sub presentaReactivosGeneral(ByVal idEvaluacion As String)

        Using conn As SqlConnection = New SqlConnection(ConfigurationManager.
                                                     ConnectionStrings("sadcomeConnectionString").
                                                     ConnectionString)
            conn.Open()
            Dim cmd As SqlCommand = New SqlCommand("ObtenRespuestasListaReactivos", conn)
            cmd.CommandType = Data.CommandType.StoredProcedure

            cmd.Parameters.Add("@IdEvaluacion", SqlDbType.VarChar).Value = idEvaluacion
            cmd.Parameters.Add("@IdAlumno", SqlDbType.VarChar).Value = Session("Usuario_IdAlumno").ToString
            Dim planteamientos As SqlDataReader = cmd.ExecuteReader()

            If planteamientos.HasRows Then

                LblAb.Visible = False
                LblMult.Visible = False

                LblAb.Text = LblAb.Text
                'Ciclo para ir avazando reactivo por reactivo y sacando sus opciones o textbox para respuesta abierta y agregándolos al panel
                ltAbiertos.Text += "<Table align='center' width='80%' border='1' bordercolor = '#777777'>" 'Agrego el inicio de la Tabla
                Dim Cont As Byte 'Esta variable la usaré para pintar los renglones
                Cont = 0
                Dim color As String

                'AGREGO LOS ENCABEZADOS DE LAS TABLAS
                ltAbiertos.Text += "<tr style='background-color: #424242;'><td width='2%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_CONSECUTIVO.Item(Session("Usuario_Idioma")) &
                                   "</font></td><td width='15%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_PLANTEAMIENTO.Item(Session("Usuario_Idioma")) &
                                   "</font></td><td width='15%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_RESPUESTA.Item(Session("Usuario_Idioma")) &
                                    "</font></td><td width='5%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_OPORTUNIDAD.Item(Session("Usuario_Idioma"))


                If Config.Global.ALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION Then
                    ltAbiertos.Text += "</font></td><td width='5%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_PENALIZACION.Item(Session("Usuario_Idioma"))
                End If

                ltAbiertos.Text += "</font></td><td width='5%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_OK.Item(Session("Usuario_Idioma")) & "</font></td></tr>"

                Do While planteamientos.Read()
                    Cont += 1
                    If Cont Mod 2 = 0 Then
                        color = "#E3EAEB"
                    Else
                        color = "#CCCCCC"
                    End If

                    'AGREGO EL PLANTEAMIENTO
                    Dim Redaccion As String = String.Empty

                    If (Not IsDBNull(planteamientos.Item("resp"))) Then
                        Redaccion = Replace(HttpUtility.HtmlDecode(planteamientos.Item("resp")), "*salto*", "<BR>")
                        Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                    End If

                    ltAbiertos.Text += "<tr  bgcolor = '" + color + "'><td width='2%'>"
                    ltAbiertos.Text += "<font face = 'Arial' size='1'>" + planteamientos.Item("Consecutivo").ToString + ".-" + "</font>"
                    ltAbiertos.Text += "</td>"

                    ltAbiertos.Text += "<td width='15%'"
                    ltAbiertos.Text += "<font face = 'Arial' size='1'>" + Redaccion + "</font>"
                    ltAbiertos.Text += "</td>"

                    'AGREGO LA RESPUESTA
                    ltAbiertos.Text += "<td width='15%'"
                    ltAbiertos.Text += "<font face = 'Arial' size='1'>" + planteamientos.Item("RespuestaAlumno").ToString + "</font>"
                    ltAbiertos.Text += "</td>"

                    'AGREGO EL INTENTO DE RESPUESTA
                    ltAbiertos.Text += "<td width='5%'"
                    ltAbiertos.Text += "<font face = 'Arial' size='1'>" + planteamientos.Item("Intento").ToString + "</font>"
                    ltAbiertos.Text += "</td>"

                    'AGREGO EL PORCENTAJE DE PENALIZACIÓN
                    If Config.Global.ALUMNO_PENDIENTES_COLUMNA_FECHAPENALIZACION Then
                        ltAbiertos.Text += "<td width='5%'"
                        ltAbiertos.Text += "<font face = 'Arial' size='1'>" + planteamientos.Item("PorcentajeRestarResp").ToString + "%" + "</font>"
                        ltAbiertos.Text += "</td>"
                    End If

                    'AGREGO IMAGEN PARA CALIFICAR
                    ltAbiertos.Text += "<td width='5%' bgcolor = 'white'>"
                    If planteamientos.Item("Acertada") = "S" Then
                        ltAbiertos.Text += "<img src='" & Config.Global.urlImagenes & "caritafeliz.png' />"
                    ElseIf planteamientos.Item("Acertada") = "N" Then
                        ltAbiertos.Text += "<img src='" & Config.Global.urlImagenes & "caritatriste.png' />"
                    Else
                        ltAbiertos.Text += "<img src='" & Config.Global.urlImagenes & "caritanose.png' />"
                    End If
                    ltAbiertos.Text += "</td></tr>"

                Loop 'del While misPlanteamientos.Read()
                ltAbiertos.Text += "</Table>"
                msgInfo.show(Lang.FileSystem.General.MSG_NOTE.Item(Session("Usuario_Idioma")), Lang.FileSystem.Alumno.ListaReactivos.MSG_REVISA_POSTERIORMENTE.Item(Session("Usuario_Idioma")))
            Else
                LblAb.Visible = False
                msgInfo.show(msgInfo.getText() & " " & Lang.FileSystem.Alumno.ListaReactivos.MSG_NO_HAY_REACTIVOS_ABIERTOS.Item(Session("Usuario_Idioma")))
            End If

            planteamientos.Close()
            cmd.Dispose()
            conn.Close()




        End Using



    End Sub

  ''' <summary>
  ''' Obtiene la lista de respuestas de opción múltiple de la actividad indicada, 
  ''' y las despliega como una tabla indicando si cada respuesta fue correcta o 
  ''' incorrecta, y el intento en el que se contestó.
  ''' </summary>
  Protected Sub presentaReactivosMultiple(ByVal idevaluacion As String)
    Try
      Using conn As SqlConnection = New SqlConnection(ConfigurationManager.
                                                      ConnectionStrings("sadcomeConnectionString").
                                                      ConnectionString)
                conn.Open()
                



        'Consulta para sacar todos los reactivos de respuesta múltiple que tiene la actividad que se acaba de contestar
        Dim cmd As SqlCommand = New SqlCommand(
            "select " &
            "   P.Consecutivo, " &
            "   P.Redaccion, " &
            "   P.ArchivoApoyo, " &
            "   P.TipoArchivoApoyo, " &
            "   P.ArchivoApoyo2, " &
            "   P.TipoArchivoApoyo2, " &
            "   P.Ponderacion, " &
            "   R.Intento, " &
            "   P.PorcentajeRestarResp, " &
            "   R.Acertada " &
            "from " &
            "   Planteamiento P, " &
            "   DetalleEvaluacion DE, " &
            "   Respuesta R " &
            "   LEFT JOIN Opcion O " &
            "       ON O.IdOpcion = R.IdOpcion " &
            "where " &
            "   DE.IdEvaluacion = @IdEvaluacion " &
            "   and R.IdDetalleEvaluacion = DE.IdDetalleEvaluacion " &
            "   and P.IdPlanteamiento = R.IdPlanteamiento " &
            "   and R.IdAlumno = @IdAlumno " &
            "   and P.TipoRespuesta = 'Multiple' " &
            "order by " &
            "   P.Consecutivo ", conn)

        cmd.Parameters.AddWithValue("@IdEvaluacion", idevaluacion)
        cmd.Parameters.AddWithValue("@IdAlumno", Session("Usuario_IdAlumno").ToString)

        Dim planteamientos As SqlDataReader = cmd.ExecuteReader()

        If planteamientos.HasRows Then
          LblMult.Visible = True
          'Ciclo para ir avazando reactivo por reactivo y sacando sus opciones o textbox para respuesta abierta y agregándolos al panel
          ltReactivos.Text += "<Table align='left' border='1' bordercolor = '#CCCCCC'>" 'Agrego el inicio de la Tabla
          Dim Cont As Byte 'Esta variable la usaré para pintar los renglones
          Cont = 0
          Dim color As String

          'AGREGO LOS ENCABEZADOS DE LAS TABLAS
          ltReactivos.Text += "<tr style='background-color: #424242;'><td width='35%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_PLANTEAMIENTO.Item(Session("Usuario_Idioma")) &
                                                       "</font></td><td width='25%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_APOYO_1.Item(Session("Usuario_Idioma")) &
                                                       "</font></td><td width='25%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_APOYO_2.Item(Session("Usuario_Idioma")) &
                                                       "</font></td><td width='5%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_OPORTUNIDAD.Item(Session("Usuario_Idioma")) &
                                                       "</font></td><td width='5%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_PENALIZACION.Item(Session("Usuario_Idioma")) &
                                                       "</font></td><td width='5%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_OK.Item(Session("Usuario_Idioma")) & "</font></td></tr>"
          Do While planteamientos.Read()
            Cont += 1
            If Cont Mod 2 = 0 Then
              color = "#E3EAEB"
            Else
              color = "#CCCCCC"
            End If

            'AGREGO EL PLANTEAMIENTO
                        Dim Redaccion As String = String.Empty
                        If (Not IsDBNull(planteamientos.Item("Redaccion"))) Then

                            Redaccion = Replace(HttpUtility.HtmlDecode(planteamientos.Item("Redaccion")), "*salto*", "<BR>")
                            Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                        End If


            ltReactivos.Text += "<tr  bgcolor = '" + color + "'><td width='35%'>"
            ltReactivos.Text += "<font face = 'Arial' size='1'>" + planteamientos.Item("Consecutivo").ToString + ".-" + Redaccion + "</font>"
            ltReactivos.Text += "</td>"

            'AGREGO EL ARCHIVO DE APOYO 1 (SI HAY) --> El valor del campo no puede ser "Ninguno", en ese caso queda como NULL
            If IsDBNull(planteamientos.Item("TipoArchivoApoyo")) Then
              ltReactivos.Text += "<td width='20%' align='center'></td>"
            Else
              ltReactivos.Text += "<td width='20%' align='center'>"
              If planteamientos.Item("TipoArchivoApoyo") = "Imagen" Then
                ltReactivos.Text += "<img src='" & Config.Global.urlMaterial & planteamientos.Item("ArchivoApoyo") & "' />"
              Else
                ltReactivos.Text += "<a href='" & Config.Global.urlMaterial & planteamientos.Item("ArchivoApoyo") & "' target='_blank'>" &
                    "<font face = 'Arial' size='1'>" + planteamientos.Item("ArchivoApoyo") + "</font>" &
                    "</a>"
              End If
              ltReactivos.Text += "</td>"
            End If

            'AGREGO EL ARCHIVO DE APOYO 2 (SI HAY) --> El valor del campo no puede ser "Ninguno", en ese caso queda como NULL
            If IsDBNull(planteamientos.Item("TipoArchivoApoyo2")) Then
              ltReactivos.Text += "<td width='20%'></td>"
            Else
              ltReactivos.Text += "<td width='20%' align='center'>"
              If planteamientos.Item("TipoArchivoApoyo2") = "Imagen" Then
                ltReactivos.Text += "<img src='" & Config.Global.urlMaterial & planteamientos.Item("ArchivoApoyo2") & "' />"
              Else
                ltReactivos.Text += "<a href='" & Config.Global.urlMaterial & planteamientos.Item("ArchivoApoyo2") & "' target='_blank'>" &
                    "<font face = 'Arial' size='1'>" + planteamientos.Item("ArchivoApoyo2") + "</font>" &
                    "</a>"
              End If
              ltReactivos.Text += "</td>"
            End If

            'AGREGO EL NÚMERO DE INTENTOS EN QUE SE CONTESTÓ
            ltReactivos.Text += "<td width='5%' align='center'>"
            ltReactivos.Text += "<font face = 'Arial' size='1'>" + planteamientos.Item("Intento").ToString + "</font>"
            ltReactivos.Text += "</td>"

            'AGREGO EL PORCENTAJE DE CASTIGO
            ltReactivos.Text += "<td width='5%' align='center'>"
            ltReactivos.Text += "<font face = 'Arial' size='1'>" + planteamientos.Item("PorcentajeRestarResp").ToString + " %" + "</font>"
            ltReactivos.Text += "</td>"

            'AGREGO IMAGEN PARA CALIFICAR
            ltReactivos.Text += "<td width='5%' bgcolor = 'white'>"
            If planteamientos.Item("Acertada") = "S" Then
              ltReactivos.Text += "<img src='" & Config.Global.urlImagenes & "caritafeliz.png' />"
            Else
              ltReactivos.Text += "<img src='" & Config.Global.urlImagenes & "caritatriste.png' />"
            End If
            ltReactivos.Text += "</td></tr>"

          Loop 'del While misPlanteamientos.Read()
          ltReactivos.Text += "</Table>"
          msgInfo.hide()
        Else
          LblMult.Visible = False
          msgInfo.show(Lang.FileSystem.Alumno.ListaReactivos.MSG_NO_HAY_REACTIVOS_MULTIPLES.Item(Session("Usuario_Idioma")))
        End If

        planteamientos.Close()
        cmd.Dispose()
        conn.Close()
      End Using
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

  ''' <summary>
  ''' Obtiene la lista de respuestas abiertas de la actividad indicada, 
  ''' y las despliega como una tabla indicando si cada respuesta fue correcta o 
  ''' incorrecta.
  ''' </summary>
  Protected Sub presentaReactivosAbiertas(ByVal idEvaluacion As String)
    Try
      Using conn As SqlConnection = New SqlConnection(ConfigurationManager.
                                                      ConnectionStrings("sadcomeConnectionString").
                                                      ConnectionString)
        conn.Open()

        'Consulta para sacar todos los reactivos de respuesta múltiple que tiene la actividad que se acaba de contestar
        Dim cmd As SqlCommand = New SqlCommand(
            "select " &
            "   P.Consecutivo, " &
            "   P.Redaccion, " &
            "   P.ArchivoApoyo, " &
            "   P.TipoArchivoApoyo, " &
            "   P.ArchivoApoyo2, " &
            "   P.TipoArchivoApoyo2, " &
            "   P.Ponderacion, " &
            "   P.PorcentajeRestarResp, " &
            "   A.Redaccion as Resp, " &
            "   R.Acertada " + _
            "from " &
            "   Planteamiento P, " &
            "   DetalleEvaluacion DE, " &
            "   RespuestaAbierta A, " &
            "   Respuesta R " &
            "   LEFT JOIN Opcion O " &
            "       ON O.IdOpcion = R.IdOpcion " &
            "where " &
            "   DE.IdEvaluacion = @IdEvaluacion " &
            "   and R.IdDetalleEvaluacion = DE.IdDetalleEvaluacion " &
            "   and P.IdPlanteamiento = R.IdPlanteamiento " &
            "   and R.IdAlumno = @IdAlumno " &
            "   and (P.TipoRespuesta = 'Abierta' OR P.TipoRespuesta = 'Abierta Calificada') " &
            "   and A.IdRespuesta = R.IdRespuesta " &
            "order by " &
            "   P.Consecutivo", conn)

        cmd.Parameters.AddWithValue("@IdEvaluacion", idEvaluacion)
        cmd.Parameters.AddWithValue("@IdAlumno", Session("Usuario_IdAlumno").ToString)

        Dim planteamientos As SqlDataReader = cmd.ExecuteReader()

        If planteamientos.HasRows Then
          LblAb.Visible = True
          LblAb.Text = LblAb.Text
          'Ciclo para ir avazando reactivo por reactivo y sacando sus opciones o textbox para respuesta abierta y agregándolos al panel
          ltAbiertos.Text += "<Table align='left' border='1' bordercolor = '#CCCCCC'>" 'Agrego el inicio de la Tabla
          Dim Cont As Byte 'Esta variable la usaré para pintar los renglones
          Cont = 0
          Dim color As String

          'AGREGO LOS ENCABEZADOS DE LAS TABLAS
          ltAbiertos.Text += "<tr style='background-color: #424242;'><td width='30%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_PLANTEAMIENTO.Item(Session("Usuario_Idioma")) &
                             "</font></td><td width='20%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_APOYO_1.Item(Session("Usuario_Idioma")) &
                             "</font></td><td width='20%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_APOYO_2.Item(Session("Usuario_Idioma")) &
                             "</font></td><td width='25%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_RESPUESTA.Item(Session("Usuario_Idioma")) &
                             "</font></td><td width='5%' align='center'><font face='Arial' color='white' size='1'>" & Lang.FileSystem.Alumno.ListaReactivos.TABLA_OK.Item(Session("Usuario_Idioma")) & "</font></td></tr>"
          Do While planteamientos.Read()
            Cont += 1
            If Cont Mod 2 = 0 Then
              color = "#E3EAEB"
            Else
              color = "#CCCCCC"
            End If

            'AGREGO EL PLANTEAMIENTO
            Dim Redaccion As String
            Redaccion = Replace(HttpUtility.HtmlDecode(planteamientos.Item("Redaccion")), "*salto*", "<BR>")
            Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")

            ltAbiertos.Text += "<tr  bgcolor = '" + color + "'><td width='30%'>"
            ltAbiertos.Text += "<font face = 'Arial' size='1'>" + planteamientos.Item("Consecutivo").ToString + ".-" + Redaccion + "</font>"
            ltAbiertos.Text += "</td>"

            'AGREGO EL ARCHIVO DE APOYO 1 (SI HAY) --> El valor del campo no puede ser "Ninguno", en ese caso queda como NULL
            If IsDBNull(planteamientos.Item("TipoArchivoApoyo")) Then
              ltAbiertos.Text += "<td width='20%' align='center'></td>"
            Else
              ltAbiertos.Text += "<td width='20%' align='center'>"
              If planteamientos.Item("TipoArchivoApoyo") = "Imagen" Then
                ltAbiertos.Text += "<img src='" & Config.Global.urlMaterial & planteamientos.Item("ArchivoApoyo") & "' />"
              Else
                ltAbiertos.Text += "<a href='" & Config.Global.urlMaterial & planteamientos.Item("ArchivoApoyo") & "' target='_blank'>" &
                    "<font face = 'Arial' size='1'>" + planteamientos.Item("ArchivoApoyo") + "</font>" &
                    "</a>"
              End If
              ltAbiertos.Text += "</td>"
            End If

            'AGREGO EL ARCHIVO DE APOYO 2 (SI HAY) --> El valor del campo no puede ser "Ninguno", en ese caso queda como NULL
            If IsDBNull(planteamientos.Item("TipoArchivoApoyo2")) Then
              ltAbiertos.Text += "<td width='20%'></td>"
            Else
              ltAbiertos.Text += "<td width='20%' align='center'>"
              If planteamientos.Item("TipoArchivoApoyo2") = "Imagen" Then
                ltAbiertos.Text += "<img src='" & Config.Global.urlMaterial & planteamientos.Item("ArchivoApoyo2") & "' />"
              Else
                ltAbiertos.Text += "<a href='" & Config.Global.urlMaterial & planteamientos.Item("ArchivoApoyo2") & "' target='_blank'>" &
                    "<font face = 'Arial' size='1'>" + planteamientos.Item("ArchivoApoyo2") + "</font>" &
                    "</a>"
              End If
              ltAbiertos.Text += "</td>"
            End If

            'AGREGO LA RESPUESTA
            ltAbiertos.Text += "<td width='25%'"
            ltAbiertos.Text += "<font face = 'Arial' size='1'>" + planteamientos.Item("resp").ToString + "</font>"
            ltAbiertos.Text += "</td>"

            'AGREGO IMAGEN PARA CALIFICAR
            ltAbiertos.Text += "<td width='5%' bgcolor = 'white'>"
            If planteamientos.Item("Acertada") = "S" Then
              ltAbiertos.Text += "<img src='" & Config.Global.urlImagenes & "caritafeliz.png' />"
            ElseIf planteamientos.Item("Acertada") = "N" Then
              ltAbiertos.Text += "<img src='" & Config.Global.urlImagenes & "caritatriste.png' />"
            Else
              ltAbiertos.Text += "<img src='" & Config.Global.urlImagenes & "caritanose.png' />"
            End If
            ltAbiertos.Text += "</td></tr>"

          Loop 'del While misPlanteamientos.Read()
          ltAbiertos.Text += "</Table>"
          msgInfo.show(Lang.FileSystem.General.MSG_NOTE.Item(Session("Usuario_Idioma")), Lang.FileSystem.Alumno.ListaReactivos.MSG_REVISA_POSTERIORMENTE.Item(Session("Usuario_Idioma")))
        Else
          LblAb.Visible = False
          msgInfo.show(msgInfo.getText() & " " & Lang.FileSystem.Alumno.ListaReactivos.MSG_NO_HAY_REACTIVOS_ABIERTOS.Item(Session("Usuario_Idioma")))
        End If

        planteamientos.Close()
        cmd.Dispose()
        conn.Close()
      End Using
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
    End Try
  End Sub

    Protected Sub presentaResultadoEvaluacion(ByVal idEvaluacion As String)
        ' el contenedor de datos de esta evaluación
        Dim evaluacion As Evaluacion = CType(New EvaluacionDa, EvaluacionDa).ObtenDatosEvaluacion(idEvaluacion)

        ' obtengo datos de la evaluación terminada
        Dim evaluacionTerminada As EvaluacionTerminadaTo = CType(New EvaluacionTerminadaDa, EvaluacionTerminadaDa).Obten(evaluacion.IdEvaluacion,
                                                                                                                         Session("Usuario_IdAlumno").ToString(),
                                                                                                                         Session("Usuario_IdCicloEscolar").ToString())

        ' presento el resultado
        ModalPopupResultado.Show()

        lblResultado.Text = evaluacionTerminada.Resultado.Value.ToString()

        ' muestro si hay segunda vuelta en esta actividad y si viene de la primera
        If evaluacion.PermiteSegunda AndAlso Session("Contestar_Vuelta") = "Primera" AndAlso Session("Contestar_FinSinPenalizacion") >= Date.Now() Then
            lt_segundavuelta.Visible = True
        End If

        ' si está activada la repetición automática para esta evaluación, 
        If evaluacion.RepiteAutomatica Then
            Dim daoEvRepetida As DataAccess.EvaluacionRepetidaDa = New EvaluacionRepetidaDa()

            ' indica si el alumno repetirá esta evaluación
            Dim repite As Boolean = False

            ' obtengo el indicador indicado para la repetición
            Dim indicador As IndicadorTo = (New IndicadorDa()).obten_IdIndicador(evaluacion.RepiteIdIndicador)

            ' obtengo el número de repeticiones que ha hecho el usuario 
            Dim intentos As Integer = daoEvRepetida.CuentaRepeticiones(
                evaluacion.IdEvaluacion,
            Session("Usuario_IdAlumno").ToString(),
                evaluacionTerminada.IdCicloEscolar)


            ' obtengo el resultado mínimo requerido
            Dim resultadoMinimoRequierido As Decimal = 0
            If evaluacion.RepiteRango = 0 Then
                resultadoMinimoRequierido = indicador.MinRojo
            ElseIf evaluacion.RepiteRango = 1 Then
                resultadoMinimoRequierido = indicador.MinAmarillo
            ElseIf evaluacion.RepiteRango = 2 Then
                resultadoMinimoRequierido = indicador.MinVerde
            ElseIf evaluacion.RepiteRango = 3 Then
                resultadoMinimoRequierido = indicador.MinAzul
            End If

            ' reviso si nó alcanzó el resultado mínimo requerido
            If (evaluacionTerminada.Resultado < resultadoMinimoRequierido) Then
                repite = True
            End If

            ' ahora, si repite, reviso si los intentos que lleva son menos que el límite
            If repite Then
                ' informo al alumno su repetición
                pnlRepite.Visible = True
                lt_repite1.Text = Lang.FileSystem.Alumno.ListaReactivos.lt_repite1.Item(Session("Usuario_Idioma"))
                ltResultadoValor.Text = resultadoMinimoRequierido

                If intentos < evaluacion.RepiteMaximo Then
                    ' realizo la repetición automática
                    CType(New EvaluacionRepetidaBl, EvaluacionRepetidaBl).RepiteEvaluacion(evaluacionTerminada, "automatica")

                    lt_repite2.Text = Lang.FileSystem.Alumno.ListaReactivos.lt_repite2_repite.Item(Session("Usuario_Idioma"))
                    ' y escondo el mensaje de la segunda vuelta si tocaba
                    lt_segundavuelta.Visible = False

                    If (evaluacion.RepiteMaximo - 1 - intentos) = 0 Then
                        lt_repite_intentos1.Text = Lang.FileSystem.Alumno.ListaReactivos.lt_repite_intentos1_uno.Item(Session("Usuario_Idioma"))
                        lt_repite_intentosVALOR.Visible = False
                        lt_repite_intentos2.Visible = False
                    Else
                        lt_repite_intentos1.Text = Lang.FileSystem.Alumno.ListaReactivos.lt_repite_intentos1_parte1.Item(Session("Usuario_Idioma"))
                        lt_repite_intentosVALOR.Text = (evaluacion.RepiteMaximo - intentos)
                        lt_repite_intentos2.Text = Lang.FileSystem.Alumno.ListaReactivos.lt_repite_intentos1_parte2.Item(Session("Usuario_Idioma"))
                    End If
                Else
                    lt_repite2.Text = Lang.FileSystem.Alumno.ListaReactivos.lt_repite2_norepite.Item(Session("Usuario_Idioma"))
                    lt_repite_intentos1.Visible = False
                    lt_repite_intentosVALOR.Visible = False
                    lt_repite_intentos2.Visible = False
                End If
            End If
        End If

    End Sub

End Class
