﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/alumno/principal_alumno_5-1-0.master"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="alumno_cursos_actividad_listareactivos_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/PageTitle_v1.ascx" TagPrefix="uc1" TagName="PageTitle_v1" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style23 {
            text-align: center;
        }

        .style24 {
            font-family: Arial, Helvetica, sans-serif;
        }

        .style25 {
            font-family: Arial, Helvetica, sans-serif;
            height: 24px;
        }

        .style26 {
            text-align: center;
            color: #000000;
            height: 28px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">

    <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1" />

    <table style="margin-left: auto; margin-right: auto;">
        <tr>
            <td colspan="3">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style26" colspan="3">
                <asp:Label ID="LblActividad" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: medium; font-weight: 700; color: #000066"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style24" colspan="3">
                <asp:Label ID="LblMult" runat="server"
                    Text="[Reactivos de opción múltiple contestados:]"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style23" colspan="3">
                    <asp:Literal ID="ltReactivos" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="style24" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td class="style24" colspan="3">
                <asp:Label ID="LblAb" runat="server"
                    Text="[Reactivos de respuesta abierta contestados:]"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style23" colspan="3">
                    <asp:Literal ID="ltAbiertos" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="style23" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td class="style23">&nbsp;</td>
            <td style="text-align: center">
                <asp:HyperLink ID="hl_continuar" runat="server"
                    NavigateUrl="~/alumno/cursos/"
                    CssClass="defaultBtn btnThemeLightBlue btnThemeWide">[Ver calificaciones y otras actividades]</asp:HyperLink>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style23">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td class="style23">&nbsp;</td>
            <td>&nbsp;</td>
            <td style="text-align: right">&nbsp;</td>
        </tr>
        <tr>
            <td class="style23">&nbsp;</td>
            <td>&nbsp;</td>
            <td style="text-align: right">&nbsp;</td>
        </tr>
    </table>
    
    <asp:Panel ID="pnlResultado" runat="server" CssClass="dialogOverwrite dialogRespuestaAbierta">
        <table>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <span style="margin: 0; font-weight: bold; font-size: 1.1em">
                        <asp:Literal ID="lt_resultado1" runat="server">
                            ¡Bien hecho! Has terminado la actividad. Tu resultado es:
                        </asp:Literal>
                    </span>
                    <br />
                    <asp:Label ID="lblResultado" runat="server" style="padding: 10px; background: #e5e5e5; display: inline-block; margin: 10px; font-weight: bold; font-size: 2em;">
                        100.00%
                    </asp:Label>
                    <br />
                    <asp:Literal ID="lt_resultado2" runat="server">
                        Si respondiste respuestas abiertas que serán calificadas por el profesor, 
                        tu calificación puede cambiar en cuanto se califiquen.
                    </asp:Literal>
                    <br />
                    <asp:Label ID="lt_segundavuelta" runat="server" Visible="false" style="background-color: #DAFECC;">
                        Puedes realizar una segunda vuelta para corregir tus respuestas erróneas.
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="pnlRepite" runat="server" Visible="false">
                        <table style="width: 100%; margin-top: 15px; background: #FFF7A8;">
                            <tr>
                                <td style="vertical-align: top;">
                                    <asp:Image ID="Image1" runat="server"
                                        style="margin: 10px;"
                                        ImageUrl="~/Resources/imagenes/btnicons/woocons/Sign Warning.png" />
                                </td>
                                <td>
                                    <asp:Literal ID="lt_repite1" runat="server">
                                        Lamentablemente no obtuviste la calificación mínima necesaria en esta actividad (
                                    </asp:Literal>
                                    <asp:Literal ID="ltResultadoValor" runat="server">90</asp:Literal>
                                    <asp:Literal ID="lt_repite2" runat="server">
                                        ), por lo que debes realizarla nuevamente.
                                    </asp:Literal>
                                    <br />
                                    <br />
                                    <asp:Literal ID="lt_repite_intentos1" runat="server">
                                        aún tienes (
                                    </asp:Literal>
                                    <asp:Literal ID="lt_repite_intentosVALOR" runat="server">3</asp:Literal>
                                    <asp:Literal ID="lt_repite_intentos2" runat="server">
                                        ), por lo que debes realizarla nuevamente.
                                    </asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom: 1px solid #333; min-height: 1em;">
                    <uc1:msgError runat="server" ID="msgErrorDialog" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-top: 15px;">
                    <asp:LinkButton ID="btnAceptar" runat="server"
                        CssClass="defaultBtn btnThemeGrey btnThemeMedium">
                        <asp:Image ID="imgAceptar" runat="server"
                            ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/accept.png"
                            CssClass="btnIcon"
                            Height="24" />
                        <asp:Label ID="lblAceptar" runat="server">
                        Aceptar
                        </asp:Label>
                    </asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>

    <table>
        <tr>
            <td>
                <asp:HiddenField ID="HF1" runat="server" />
                <asp:ModalPopupExtender ID="ModalPopupResultado" runat="server"
                    PopupControlID="pnlResultado" TargetControlID="HF1" BackgroundCssClass="overlay"></asp:ModalPopupExtender>
            </td>
            <td>

            </td>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
</asp:Content>

