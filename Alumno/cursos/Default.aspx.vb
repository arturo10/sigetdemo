﻿Imports Siget
Imports Siget.DataAccess
Imports Siget.Beans
Imports Siget.Utils

Imports System.Data.SqlClient

' *****************************************************************************
'                                    Cursos
' *****************************************************************************

''' <summary>
''' 
''' /alumno/cursos/
''' 
''' Presenta actividades pendientes y terminadas para cursos inscritos del alumno, y permite entrar a ellas.
''' 
''' </summary>
''' <remarks></remarks>
Partial Class alumno_cursos_Default
    Inherits System.Web.UI.Page

    ' Aquí construllo la lista de asignaturas
    Protected asignaturas As ArrayList = Nothing

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        If (Siget.Utils.Sesion.verificaSiEsPrimeraVez(User.Identity.Name) And Siget.Config.Global.CONTRA_OBLIGATORIA) Then
            Response.Redirect("../misdatos/cambiarpassword")
        End If

        ' esto debe ir antes que cualquier operación que utilice base de datos
        If Utils.Bloqueo.sistemaBloqueado() Then
            Utils.Sesion.CierraSesion()
            Response.Redirect("~/EnMantenimiento.aspx")
        End If

        If Not Session("Nota") = "" Then
            msgInfo.show(Trim(Session("Nota")))
            Session("Nota") = "" 'La uso y la borro para que no se quede el mensaje
        End If

        If Not IsNothing(Session("guardadasMostradas")) Then
            Session.Remove("guardadasMostradas")
        End If

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            ' limpio las variables de la sesión de contestado si están definidas
            Utils.Sesion.LimpiaSesionContestado()

            aplicaLenguaje()

            initPageData()

            OutputCss()



        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = Lang.FileSystem.Alumno.Cursos.LT_PAGE_TITLE.Item(Session("Usuario_Idioma"))

        PageTitle_v1.show(Lang.FileSystem.Alumno.Cursos.HINT_AVISOS.Item(Session("Usuario_Idioma")),
                          Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/comments.png")

        lt_bienvenida.Text = Lang.FileSystem.Alumno.Cursos.LT_BIENVENIDA.Item(Session("Usuario_Idioma"))
        lbl_nombre_usuario.Text = HttpContext.Current.User.Identity.Name.Trim()
        'Lt_MisDatos.Text = Lang.FileSystem.Alumno.Cursos.Txt_MisDatos.Item(Session("Usuario_Idioma"))
    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub initPageData()
        ' *********************************************************************
        ' Despliego los Avisos si los hay
        ' *********************************************************************
        If Not Siget.Config.PaginaInicio.AvisoPublico = "" Then
            LblMensaje1.Text = Siget.Config.PaginaInicio.AvisoPublico
        Else
            LblMensaje1.Text = Lang.FileSystem.Alumno.Cursos.MSG_NO_AVISOS.Item(Session("Usuario_Idioma"))
        End If

        ' *********************************************************************
        ' imprimo las actividades
        ' *********************************************************************
        ' el alumno debe estar activo
        If Session("Usuario_Estatus") = "Activo" Then
            ' obtengo la lista de cursos y actividades
            Try
                asignaturas = CType(New EvaluacionDa, EvaluacionDa).ObtenEvaluacionesConJerarquia(Session("Usuario_IdAlumno").ToString(),
                                                                                                  Session("Usuario_IdPlantel").ToString(),
                                                                                                  Session("Usuario_IdGrado").ToString(),
                                                                                                  Session("Usuario_IdCicloEscolar").ToString())
            Catch ex As Exception
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try

            If Not IsNothing(asignaturas) Then
                For Each a As Asignatura In asignaturas
                    For Each c As Calificacion In a.getCalificaciones()
                        c.CuentaActividadesTerminadas()
                    Next
                Next

                ' imprimo la lista obtenida
                printHierarchy()
            End If
        Else
            msgWarning.show(Lang.FileSystem.General.MSG_WARNING.Item(Session("Usuario_Idioma")), Lang.FileSystem.Alumno.Cursos.MSG_NO_ACTIVO.Item(Session("Usuario_Idioma")))
        End If
    End Sub

    ' imprimo los estilos variables de ésta página
    Protected Sub OutputCss()
        Dim s As StringBuilder = New StringBuilder()
        s.Append("<style type='text/css'>")
        s.Append("  #menu .current_area_cursos a {") ' esta clase css debe corresponder a la clase del boton a resaltar
        s.Append("    border-bottom: 5px solid #666;")
        s.Append("  }")
        s.Append("  .AssignmentHeaderRow {")
        s.Append("    background-color: " & Config.Color.MenuPendientes_Subtitle & ";")
        s.Append("  }")
        s.Append("</style>")
        ltEstilos.Text += s.ToString()
    End Sub

#End Region

#Region "PendingMenu_v2"

    ''' <summary>
    ''' Se apoya en los métodos de presentación de cada Bean para imprimir la herarquía 
    ''' de cursos y actividades en los controles asp:Literal correspondientes.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub printHierarchy()
        Dim generadorDeMenu As UserInterface.CursosPendientes = New UserInterface.CursosPendientes()

        If Not asignaturas.Count() = 0 Then
            ' para cada asignatura en la jerarquía
            For Each a As Asignatura In asignaturas
                lt_courseList.Text += generadorDeMenu.printCourseButton(a) ' genera el botón de la asignatura en la lista de asignaturas/calificaciones
                lt_courseList.Text += generadorDeMenu.printOpenPeriodTags(a) ' genera la apertura de la tabla de calificaciones

                If Not a.getCalificaciones().Count() = 0 Then
                    ' para cada calificación de la asignatura
                    For Each c As Calificacion In a.getCalificaciones()
                        lt_courseList.Text += generadorDeMenu.ImprimeBotonCalificacion(c) ' imprime el botón de la calificación
                        lt_activityList.Text += generadorDeMenu.ImprimeInicioListaActividades(c)

                        If Not c.Evaluaciones.Count() = 0 Then
                            For Each e As Evaluacion In c.Evaluaciones
                                lt_activityList.Text += generadorDeMenu.printActivityRow(e)
                            Next
                        End If

                        lt_activityList.Text += generadorDeMenu.ImprimeFinListaActividades()
                    Next
                End If

                lt_courseList.Text += generadorDeMenu.printClosePeriodTags() ' genera el cierre de la tabla de calificaciones
            Next
        Else
            msgInfo.show(Lang.FileSystem.Alumno.Cursos.MSG_NO_CURSOS.Item(Session("Usuario_Idioma")))
        End If
    End Sub

#End Region

End Class
