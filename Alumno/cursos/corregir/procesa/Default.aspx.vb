﻿Imports Siget

'Imports System.Web.Security
'Imports System.Data
'Imports System.Data.OleDb
Imports System.Data.SqlClient

Partial Class alumno_cursos_corregir_procesa
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Utils.Sesion.sesionAbierta() Then
            Session("Nota") = Lang.FileSystem.General.Msg_SuSesionExpiro.Item(Session("Usuario_Idioma").ToString())
            Response.Redirect("~/")
        End If

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            aplicaLenguaje()

            initPageData()

            OutputCss()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = Lang.FileSystem.Alumno.Corregir_Procesa.LT_TITULO.Item(Session("Usuario_Idioma"))

    PageTitle_v1.show(Lang.FileSystem.Alumno.Corregir_Procesa.LT_TITULO.Item(Session("Usuario_Idioma")),
                      Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/server_error.png")

        lt_btn_regresar.Text = Lang.FileSystem.Alumno.Corregir_Procesa.LT_BTN_REGRESAR.Item(Session("Usuario_Idioma"))
    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub initPageData()
        ' Objetivo de redirección: 0 - ninguno; 1 - ~/alumno/cursos/corregir/; 2 - ~/alumno/cursos/
        Dim redirect As Integer = 0

        Dim IdAlumno As String = Session("Usuario_IdAlumno").ToString()
        Dim IdGrado As String = Session("Usuario_IdGrado").ToString()
        Dim IdGrupo As String = Session("Usuario_IdGrupo").ToString()
        Dim IdPlanteamiento As String = Context.Items.Item("PlanteamientoActual")
        Dim Consecutiva As String = Context.Items.Item("RespuestaSeleccionada")
        Dim IdCicloEscolar As String = Context.Items.Item("CicloEscolarActual")
        Dim IdDetalleEvaluacion As String = Context.Items.Item("DetalleEvaluacionActual")

        'Filtro si el usuario anteriormente pulso Aceptar Respuesta o Contestar Luego
        If IsNothing(Context.Items.Item("BotonActivado")) Then
            Session("Nota") = Lang.FileSystem.Alumno.Cursos.msg_evaluacion_no_identificada.Item(Session("Usuario_Idioma"))
            redirect = 2 ' fallo algo
        ElseIf (
            Context.Items.Item("BotonActivado") = "Aceptar" Or
                    (Context.Items.Item("BotonActivado") = "AceptarA") Or
                    (Context.Items.Item("BotonActivado") = "GuardarA")
            ) And Trim(CStr(Context.Items.Item("RespuestaSeleccionada"))) <> "" Then
            If (CStr(Context.Items.Item("RespuestaSeleccionada")) = "0") Then
                'significa que no selecciono respuesta, por tanto no puede continuar y la regreso a donde mismo   
                redirect = 1
            Else
                'ALGORITMO PARA ALMACENAR LA RESPUESTA DADA
                'PASO 1) Obtengo los datos para ingresar la respuesta
                'ANTES IDENTIFICAR QUE SI NO ES ALUMNO (ES ADMINISTRADOR) NO EJECUTE ESTE CODIGO
                Dim strConexion As String
                'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
                Dim objConexion As New SqlConnection(strConexion)
                Dim miComando As SqlCommand
                Dim misRegistros As SqlDataReader

                miComando = objConexion.CreateCommand

                Try
                    'Abrir la conexión y leo los registros del Planteamiento
                    objConexion.Open()

                    'PASO 2) Obtengo el Id de la Opcion
                    miComando.CommandText = "select IdOpcion, Consecutiva, Correcta from Opcion where idPlanteamiento = " + IdPlanteamiento + " and Consecutiva = '" + Consecutiva + "'"
                    misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                    misRegistros.Read() 'Leo para poder accesarlos
                    Dim IdOpcion = CStr(misRegistros.Item("IdOpcion"))
                    Dim Correcta = misRegistros.Item("Correcta")

                    'PASO 3) Ingreso la respuesta, pero antes verifico si no habia ya ingresado la respuesta para que sea el segundo intento
                    misRegistros.Close()
                    'En el select con el puro IdDetalleEvaluacion puedo localizar la respuesta, pero lo pongo para que quede mas amarrado
                    miComando.CommandText = "select IdRespuesta, Intento, Acertada from Respuesta where IdDetalleEvaluacion = " + IdDetalleEvaluacion + " and IdPlanteamiento = " + IdPlanteamiento + " and IdAlumno = " + IdAlumno
                    misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros

                    If misRegistros.Read() Then
                        Dim Intento = misRegistros.Item("Intento")
                        Intento = Intento + 1
                        If Intento > 2 Then 'Esto lo hago para que no intenten escribir el URL directamente en el navegador y seguir contestando
                            Session("Nota") = "Está intentado realizar una actividad ilegalmente"
                            redirect = 2
                        Else
                            SDSrespuestas.UpdateCommand = "SET dateformat dmy; UPDATE Respuesta SET IdOpcion = " + IdOpcion + ", Intento = " + CStr(Intento) + _
                                ", Acertada = '" + Correcta + "', FechaContestada = getdate() WHERE IdDetalleEvaluacion = " + IdDetalleEvaluacion + _
                               " and IdPlanteamiento = " + IdPlanteamiento + " and IdAlumno = " + IdAlumno + " and IdGrupo = " + IdGrupo + _
                               " and IdGrado = " + IdGrado + " and IdCicloEscolar = " + IdCicloEscolar
                            SDSrespuestas.Update()
                        End If
                    Else
                        SDSrespuestas.InsertCommand = "SET dateformat dmy; INSERT INTO Respuesta (IdPlanteamiento,IdOpcion,IdAlumno,IdGrado,IdGrupo,IdCicloEscolar,IdDetalleEvaluacion,Intento,Acertada,FechaContestada) VALUES (" + _
                                                     IdPlanteamiento + "," + IdOpcion + "," + IdAlumno + "," + IdGrado + "," + IdGrupo + "," + IdCicloEscolar + "," + IdDetalleEvaluacion + ",1,'" + Correcta + "',getdate())"
                        SDSrespuestas.Insert()
                    End If

                    misRegistros.Close()
                    objConexion.Close()

                    'PASO 5) Redirecciono a la página nuevamente si no fue ilegal
                    If Not redirect = 2 Then redirect = 1

                Catch ex As Exception
                    Utils.LogManager.ExceptionLog_InsertEntry(ex)
                    msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
                End Try
            End If
        Else
            'AQUI IRIA EL ALGORITMO PARA PASAR A LA SIGUIENTE PREGUNTA (PLANTEAMIENTO) SIN GUARDAR RESPUESTA
            'PASARLE EL SIGUIENTE IDPLANTEAMIENTO EN EL RESPONSE.REDIRECT
            If (Context.Items.Item("BotonActivado") <> "Aceptar") Then
                redirect = 2
            ElseIf Context.Items.Item("RespuestaSeleccionada") = "" Then
                redirect = 1
            End If
        End If

        ' *********************************************************************
        '                              Redirección
        ' *********************************************************************
        If redirect = 1 Then
            Response.Redirect("~/alumno/cursos/corregir/")
        ElseIf redirect = 2 Then
            Response.Redirect("~/alumno/cursos/")
        End If
    End Sub

    ' imprimo los estilos variables de ésta página
    Protected Sub OutputCss()
        Dim s As StringBuilder = New StringBuilder()
        s.Append("<style type='text/css'>")
        s.Append("  #menu .current_area_cursos a {") ' esta clase css debe corresponder a la clase del boton a resaltar
        s.Append("    border-bottom: 5px solid " & Config.Color.MenuBar_SelectedOption_BorderColor & ";")
        s.Append("  }")
        s.Append("  .AssignmentHeaderRow {")
        s.Append("    background-color: " & Config.Color.MenuPendientes_Subtitle & ";")
        s.Append("  }")
        s.Append("</style>")
        ltEstilos.Text += s.ToString()
    End Sub

#End Region

End Class
