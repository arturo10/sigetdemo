﻿Imports Siget

'Imports System.Data
'Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Data

Partial Class alumno_cursos_corregir_Default
    Inherits System.Web.UI.Page

    Protected Shared titleIcon As String =
      Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/document_prepare.png"

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Utils.Sesion.sesionAbierta() Then
            Session("Nota") = Lang.FileSystem.General.Msg_SuSesionExpiro(Session("Usuario_Idioma").ToString())
            Response.Redirect("~/")
        End If

        ' protección en contra de intentos ilegales
        If IsNothing(Session("Contestar_IdEvaluacion")) Then
            Session("Nota") = Lang.FileSystem.Alumno.Cursos.msg_evaluacion_no_identificada.Item(Session("Usuario_Idioma"))
            Response.Redirect("~/")
        End If

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            aplicaLenguaje()

            Dim redireccion As Integer = initPageData()

            If redireccion = 1 Then
                Response.Redirect("~/alumno/cursos/")
            ElseIf redireccion = 2 Then
                Response.Redirect("~/alumno/cursos/actividad/listareactivos/")
            End If

            OutputCss()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = Lang.FileSystem.Alumno.Corregir.LT_TITULO.Item(Session("Usuario_Idioma"))

        LblFaltan.Attributes.Add("style", "background: " & Config.Color.MenuPendientes_Subtitle & "; padding: 5px;")

        lt_segunda_oportunidad.Text = Lang.FileSystem.Alumno.Corregir.LT_SEGUNDA_OPORTUNIDAD.Item(Session("Usuario_Idioma"))
        Aceptar.Text = Lang.FileSystem.Alumno.Corregir.BTN_ACEPTAR.Item(Session("Usuario_Idioma"))
        Cancelar.Text = Lang.FileSystem.Alumno.Corregir.BTN_SALIR.Item(Session("Usuario_Idioma"))
        AceptarAA.Text = Lang.FileSystem.Alumno.Contestar.BTN_ACEPTAR_ABIERTA.Item(Session("Usuario_Idioma"))
        CancelarAA.Text = Lang.FileSystem.Alumno.Contestar.BTN_SALIR_ABIERTA.Item(Session("Usuario_Idioma"))
        VerificarAA.Text = Lang.FileSystem.Alumno.Contestar.BtnVerificaRespuesta.Item(Session("Usuario_Idioma"))
    End Sub

    ''' <summary>
    ''' inicializo los datos que necesito para los controles de la página
    ''' </summary>
    ''' <returns>
    ''' 0 - no redirecciona
    ''' 1 - Response.Redirect("~/alumno/cursos/")
    ''' 2 - Response.Redirect("~/alumno/cursos/actividad/listareactivos/")
    ''' </returns>
    Protected Function initPageData() As Integer
        'PRIMERO OBTENGO LOS DETALLESEVALUACION DE LA EVALUACION
        Dim IdEvaluacion As String = Session("Contestar_IdEvaluacion")
        Dim IdGrado As String = Session("Usuario_IdGrado")
        Dim Plantel As String = Session("SNAlcance")
        Dim da As New SqlDataAdapter
        Dim dt As New DataTable
        Dim ancho1 As String = "180"
        Dim arrayConsecutivo As ArrayList = New ArrayList({0, 1, 2, 3, 4, 5})

        'Esta la utilizaré cuando mando a MuestraMaterial.aspx
        'Session("Tarea") = Trim(Request.QueryString("Tarea"))

        LblAlumno.Text = Lang.FileSystem.Alumno.Corregir.LT_HINT_ALUMNO.Item(Session("Usuario_Idioma")) & " " & Session("Usuario_Nombre")

        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString

        Dim objConexionEv As New SqlConnection(strConexion)
        Dim miComandoEv As SqlCommand
        Dim misEvaluaciones As SqlDataReader
        miComandoEv = objConexionEv.CreateCommand

        'Calificacion --> Evaluacion --> DetalleEvaluacion --> Subtema --> Planteamiento
        'DE AQUI SACARE EL NUMERO DE SUBTEMAS PARA DESPUES OBTENER LOS REACTIVOS DE CADA UNO

        '¿Esta consulta se compara contra la Fecha FinSinPenalizacion y tuve que poner en la condicion: <= E.FinSinPenalizacion + 1?
        '¿porque no tomaba el dia correctamente cuando la fecha era la misma que esa fecha (al parecer por la hora)?

        'La siguiente consulta, si los subtemas de la Actividad no tienen reactivos (como podría ser en una actividad de entrega de documento),
        'No devolvería ningún registro y se va al ELSE. Como se supone que este script se ejecuta viniendo de las actividades terminadas, pues
        'es parte de la lógica que ya no pueda cargar ningún archivo (no le permite ir a MuestraMaterial.aspx)
        If Plantel = "0" Then 'Si Plantel="N" significa que escogio la tarea del Grid de tareas generales para todos los planteles de Pendientes.aspx
            miComandoEv.CommandText = "select distinct D.IdDetalleEvaluacion, C.IdCicloEscolar, P.IdSubtema, P.Consecutivo, ST.Numero, ST.Descripcion, E.FinSinPenalizacion, E.Penalizacion, E.Aleatoria, E.Estatus, E.SeEntregaDocto, E.ClaveBateria,E.OpcionesAleatorias " +
      " from Evaluacion E, Calificacion C, DetalleEvaluacion D, Planteamiento P, Subtema ST, Tema T, Asignatura A " +
      "where E.IdEvaluacion = " + Trim(IdEvaluacion) + " and D.IdEvaluacion = E.IdEvaluacion " +
      "and ST.IdSubtema = D.IdSubtema and E.IdCalificacion = C.IdCalificacion " +
      "and T.IdTema = ST.IdTema and A.IdAsignatura = T.IdAsignatura and A.IdGrado = " + Trim(IdGrado) + " " +
      "and P.Ocultar = 0 and P.IdSubtema = ST.IdSubtema and (E.InicioContestar <= cast(getdate() as smalldatetime)) and (cast(getdate() as smalldatetime) <= E.FinContestar) " +
      "order by ST.Numero, P.Consecutivo, ST.Descripcion, P.IdSubtema, D.IdDetalleEvaluacion, C.IdCicloEscolar"
        ElseIf Plantel = "1" Then 'Si Plantel="S" significa que escogio la tarea del Grid de tareas específicas por plantel de Pendientes.aspx
            miComandoEv.CommandText = "select distinct D.IdDetalleEvaluacion, C.IdCicloEscolar, P.IdSubtema, P.Consecutivo, ST.Numero, ST.Descripcion, EP.FinSinPenalizacion, E.Penalizacion, E.Aleatoria, EP.Estatus, E.SeEntregaDocto, E.ClaveBateria,E.OpcionesAleatorias " +
      " from Evaluacion E, Calificacion C, DetalleEvaluacion D, Planteamiento P, Subtema ST, Tema T, Asignatura A, EvaluacionPlantel EP " +
      "where E.IdEvaluacion = " + Trim(IdEvaluacion) + " and D.IdEvaluacion = E.IdEvaluacion and EP.IdEvaluacion = E.IdEvaluacion and EP.IdPlantel = " + Session("Usuario_IdPlantel") +
      " and ST.IdSubtema = D.IdSubtema and E.IdCalificacion = C.IdCalificacion " +
      "and T.IdTema = ST.IdTema and A.IdAsignatura = T.IdAsignatura and A.IdGrado = " + Trim(IdGrado) + " " +
      "and P.Ocultar = 0 and P.IdSubtema = ST.IdSubtema and (EP.InicioContestar <= cast(getdate() as smalldatetime)) and (cast(getdate() as smalldatetime) <= EP.FinContestar) " +
      "order by ST.Numero, P.Consecutivo, ST.Descripcion, P.IdSubtema, D.IdDetalleEvaluacion, C.IdCicloEscolar"
        Else 'Significaría que Plantel="A" es que escogio la tarea del Grid de tareas específicas por alumno de Pendientes.aspx
            miComandoEv.CommandText = "select distinct D.IdDetalleEvaluacion, C.IdCicloEscolar, P.IdSubtema, P.Consecutivo, ST.Numero, ST.Descripcion, EA.FinSinPenalizacion, E.Penalizacion, E.Aleatoria, EA.Estatus, E.SeEntregaDocto, E.ClaveBateria,E.OpcionesAleatorias " +
      " from Evaluacion E, Calificacion C, DetalleEvaluacion D, Planteamiento P, Subtema ST, Tema T, Asignatura A, EvaluacionAlumno EA " +
      "where E.IdEvaluacion = " + Trim(IdEvaluacion) + " and D.IdEvaluacion = E.IdEvaluacion and EA.IdEvaluacion = E.IdEvaluacion and EA.IdAlumno = " + Session("Usuario_IdAlumno") +
      " and ST.IdSubtema = D.IdSubtema and E.IdCalificacion = C.IdCalificacion " +
      "and T.IdTema = ST.IdTema and A.IdAsignatura = T.IdAsignatura and A.IdGrado = " + Trim(IdGrado) + " " +
      "and P.Ocultar = 0 and P.IdSubtema = ST.IdSubtema and (EA.InicioContestar <= cast(getdate() as smalldatetime)) and (cast(getdate() as smalldatetime) <= EA.FinContestar) " +
      "order by ST.Numero, P.Consecutivo, ST.Descripcion, P.IdSubtema, D.IdDetalleEvaluacion, C.IdCicloEscolar"
        End If

        Try
            objConexionEv.Open() 'Abro la conexion
            misEvaluaciones = miComandoEv.ExecuteReader() 'Creo conjunto de registros
            'Objetos para ir sacando los PLANTEAMIENTOS por cada SUBTEMA
            Dim Redaccion As String
            Dim objConexion As New SqlConnection(strConexion)
            Dim miComando As SqlCommand
            Dim misRegistros As SqlDataReader
            Dim opcionesAleatorias As Integer
            miComando = objConexion.CreateCommand

            If misEvaluaciones.Read() Then 'misEvaluaciones trae los SUBTEMAS
                opcionesAleatorias = misEvaluaciones("OpcionesAleatorias")
                Session("Actividad") = misEvaluaciones.Item("ClaveBateria")  'La usaré en ListaReactivos.aspx
                'Checo que no vaya a quererse hacer un segundo intento si ya venció la Fecha FinsSinPenalizacion
                If Date.Now() > misEvaluaciones("FinSinPenalizacion") Then
                    misEvaluaciones.Close()
                    objConexionEv.Close()
                    Session("Nota") = Lang.FileSystem.Alumno.Corregir.MSG_NO_DISPONIBLE_FECHA_PENALIZACION.Item(Session("Usuario_Idioma"))
                    Return 1
                End If

                If misEvaluaciones("Estatus") = "Terminada" Then
                    misEvaluaciones.Close()
                    objConexionEv.Close()
                    Session("Nota") = Lang.FileSystem.Alumno.Corregir.MSG_NO_DISPONIBLE_FECHAS.Item(Session("Usuario_Idioma"))
                    Return 1
                End If

                'Llega a esta parte cuando el Subtema ligado a la Evaluacion, aunque esta es de Entregar Documento, tiene reactivos creados (planteamientos y opciones),
                'si el o los subtemas no tuvieran reactivos, no hubiera entrado al THEN, se hubiera ido directo al ELSE principal
                If misEvaluaciones("SeEntregaDocto") Then
                    'Una actividad de Entregar Documento solo aparece en Terminadas cuando ya la revisó el Profesor, por tanto no se permite cambiar archivo
                    misEvaluaciones.Close()
                    objConexionEv.Close()
                    Session("Nota") = Lang.FileSystem.Alumno.Corregir.MSG_NO_DISPONIBLE_DOCTO_ENTREGADO.Item(Session("Usuario_Idioma"))
                    Return 1
                End If
                Dim Actividad = misEvaluaciones.Item("ClaveBateria")
                'Campos que pasaré a la pagina PROCESA2.ASPX
                HFcicloescolar.Value = misEvaluaciones("IdCicloEscolar")
                HFdetalleEvaluacion.Value = misEvaluaciones("IdDetalleEvaluacion")
                HFidEvaluacion.Value = IdEvaluacion
                HFidGrado.Value = IdGrado
                HFplantel.Value = Plantel

                'El siguiente ciclo lo uso para avanzar en cada subtema que componga la
                'evaluacion, sacando los reactivos de cada uno
                Dim compara As Boolean
                compara = True
                objConexion.Open()
                Do While compara
                    'Cuento el total de registros (reactivos que contesto mal de la evaluacion)
                    miComando.CommandText = "select Count(*) Total" +
                      " from Planteamiento P,Respuesta R where IdSubtema = " + misEvaluaciones.Item("IdSubtema").ToString +
                      " and R.IdAlumno = " + CStr(Session("Usuario_IdAlumno")) +
                      " and R.idDetalleEvaluacion = " + CStr(misEvaluaciones("IdDetalleEvaluacion")) +
                      " and R.Intento = 1 and R.Acertada = 'N' and R.IdPlanteamiento = P.IdPlanteamiento and P.Ocultar = 0 and P.TipoRespuesta <> 'Abierta'"
                    misRegistros = miComando.ExecuteReader()
                    misRegistros.Read()

                    If (misRegistros.Item("Total")) = 0 Then
                        'Esta condición me permite romper el ciclo cuando se terminen los registros

                        'NOTA IMPORTANTE: No es necesario considerar la penalización a la calificación
                        'del primer intento para sumarle los puntos del segundo intento ya que nunca
                        'sucederá que tengan que acumularse los puntos de ambas vueltas cuando hubo
                        'penalización en la primera (El sistema no permite contestar segunda vuelta si
                        'ya paso la fecha de contestar SIN PENALIZACION). De hecho, la segunda vuelta ya
                        'tiene penalizacion por reactivo
                        If Not misEvaluaciones.Read() Then
                            misRegistros.Close()
                            misEvaluaciones.Close()
                            objConexionEv.Close()
                            Session("Nota") = Lang.FileSystem.Alumno.Corregir.MSG_NO_DISPONIBLE_FECHAS.Item(Session("Usuario_Idioma"))

                            'ALGORITMO CUANDO TERMINO DE CONTESTAR LA EVALUACION (TODOS LOS REACTIVOS QUE ESTABAN MAL)
                            'SUMO EL TOTAL DE PUNTOS QUE DA LA EVALUACION Y LA CANTIDAD DE REACTIVOS QUE LA COMPONEN
                            'OJO QUE AQUÍ SACO EL PORCENTAJE DE ACIERTOS SOBRE LO CONTESTADO, NO SOBRE
                            'LOS PLANTEAMIENTOS TOTALES, AUNQUE SE SUPONE QUE COINCIDE PORQUE YA TERMINO LA EVALUACION
                            misRegistros.Close()
                            miComando.CommandText = "select Sum(P.Ponderacion) Puntos, Count(Ponderacion) Cant " +
                  "from Respuesta R, Planteamiento P, DetalleEvaluacion D " +
                  "where (R.IdPlanteamiento = P.IdPlanteamiento) and P.Ocultar = 0 " +
                  "and R.IdAlumno = " + CStr(Session("Usuario_IdAlumno")) + " and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion " +
                  "and D.IdEvaluacion  = " + CStr(IdEvaluacion)
                            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                            misRegistros.Read()

                            Dim TotalPuntos As Decimal
                            Dim TotalReactivos As Byte
                            If IsDBNull(misRegistros.Item("Puntos")) Then
                                TotalPuntos = 0
                            Else
                                TotalPuntos = misRegistros.Item("Puntos")
                            End If

                            If IsDBNull(misRegistros.Item("Cant")) Then
                                TotalReactivos = 0
                            Else
                                TotalReactivos = misRegistros.Item("Cant")
                            End If

                            'CHECO CUANTOS PUNTOS SUMO EL ALUMNO CORRECTAMENTE EN EL PRIMER INTENTO
                            misRegistros.Close()
                            miComando.CommandText = "select Sum(P.Ponderacion) Puntos, Count(P.Ponderacion) Cant " +
                  "from Respuesta R, Planteamiento P, DetalleEvaluacion D " +
                  "where (R.IdPlanteamiento = P.IdPlanteamiento) and P.Ocultar = 0 " +
                  "and R.IdAlumno = " + CStr(Session("Usuario_IdAlumno")) + " and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion " +
                  "and D.IdEvaluacion  = " + CStr(IdEvaluacion) + " and R.Acertada = 'S' and R.Intento = 1"
                            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                            misRegistros.Read()
                            Dim PuntosOk, ReactivosAcertados As Decimal
                            If IsDBNull(misRegistros.Item("Puntos")) Then
                                PuntosOk = 0
                                ReactivosAcertados = 0
                            Else
                                PuntosOk = misRegistros.Item("Puntos")
                                ReactivosAcertados = misRegistros.Item("Cant")
                            End If

                            'CHECO CUANTOS PUNTOS SUMO EL ALUMNO CORRECTAMENTE EN EL SEGUNDO INTENTO
                            misRegistros.Close()
                            miComando.CommandText = "select Sum(Ponderacion - (P.Ponderacion * P.PorcentajeRestarResp)/100) Puntos, Count(Ponderacion) Cant " +
                  "from Respuesta R, Planteamiento P, DetalleEvaluacion D " +
                  "where (R.IdPlanteamiento = P.IdPlanteamiento) and P.Ocultar = 0 " +
                  "and R.IdAlumno = " + CStr(Session("Usuario_IdAlumno")) + " and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion " +
                  "and D.IdEvaluacion  = " + CStr(IdEvaluacion) + " and R.Acertada = 'S' and R.Intento = 2"
                            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                            misRegistros.Read()

                            If (Not IsDBNull(misRegistros.Item("Puntos"))) Then  '(misRegistros.Item("Cant") > 0) no es necesario
                                PuntosOk = PuntosOk + misRegistros.Item("Puntos")
                                ReactivosAcertados = ReactivosAcertados + misRegistros.Item("Cant")
                            End If

                            'CALCULO EL RESULTADO
                            Dim Resultado As Decimal
                            If TotalPuntos <> 0 Then
                                Resultado = (PuntosOk / TotalPuntos) * 100
                            Else
                                Resultado = 0
                            End If

                            'Actualizo el registro de la evaluacion terminada
                            SDSevaluacionesterminadas.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionTerminada SET TotalReactivos = " + CStr(TotalReactivos) +
                  ", ReactivosContestados = " + CStr(ReactivosAcertados) +
                  ", PuntosPosibles = " + CStr(FormatNumber(TotalPuntos, 2)) + ", PuntosAcertados = " + CStr(FormatNumber(PuntosOk, 2)) +
                  ", Resultado = " + CStr(FormatNumber(Resultado, 2)) + ", FechaTermino = CAST(getdate() as smalldatetime)" +
                  " where IdAlumno = " + CStr(Session("Usuario_IdAlumno")) + " and IdGrupo = " + CStr(Session("Usuario_IdGrupo")) +
                  " and IdGrado = " + CStr(Session("Usuario_IdGrado")) +
                  " and IdCicloEscolar = " + CStr(HFcicloescolar.Value) + " and IdEvaluacion = " + CStr(HFidEvaluacion.Value)
                            SDSevaluacionesterminadas.Update()
                            Session("Nota") = Lang.FileSystem.Alumno.Corregir.MSG_TERMINO.Item(Session("Usuario_Idioma"))

                            misRegistros.Close()
                            objConexion.Close()
                            'ANTES 1: Response.Redirect("~/alumno/cursos/")
                            'ANTES 2: Response.Redirect("~/alumno/cursos/actividad/?IdEvaluacion=" + IdEvaluacion.ToString + "&IdGrado=" + IdGrado.ToString + "&Tarea=" + Session("Tarea") + "&Plantel=" + Plantel)
                            Session("IdEvaluacion") = IdEvaluacion  'Session("Usuario_IdAlumno") ya está generada
                            Return 2
                        Else
                            'Campos que pasaré a la pagina PROCESA2.ASPX (los tengo que tomar de nuevo de aqui)
                            HFcicloescolar.Value = misEvaluaciones("IdCicloEscolar")
                            HFdetalleEvaluacion.Value = misEvaluaciones("IdDetalleEvaluacion")
                            HFidEvaluacion.Value = IdEvaluacion
                            HFplantel.Value = Plantel
                        End If
                    Else
                        compara = False
                    End If
                    misRegistros.Close()
                Loop

                'Cuento el total de registros (reactivos que contesto mal de la evaluacion)
                miComando.CommandText = "select Count(*) Total from Respuesta R, DetalleEvaluacion D " +
                "where(R.IdAlumno = " + CStr(Session("Usuario_IdAlumno")) + " And D.IdEvaluacion = " + IdEvaluacion + ") " +
                "and R.idDetalleEvaluacion = D.idDetalleEvaluacion and R.Intento = 1 and R.Acertada = 'N'"
                'Ahora uso otra consulta para sacar el total de reactivos de la evaluación (con todos los subtemas incluidos)
                'miComando.CommandText = "SELECT Count(*) Total FROM Planteamiento P, DetalleEvaluacion D " + _
                '       "WHERE (D.IdEvaluacion = " + Trim(IdEvaluacion) + ") " + _
                '      "and P.IdSubtema = D.IdSubtema and P.IdPlanteamiento not in " + _
                '     "(select R.IdPlanteamiento from Respuesta R,DetalleEvaluacion D " + _
                '    "where(R.IdAlumno = " + CStr(Session("Usuario_IdAlumno")) + ") and D.idEvaluacion = " + Trim(IdEvaluacion) + _
                '   " and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion)"
                misRegistros = miComando.ExecuteReader()
                misRegistros.Read()
                LblFaltan.Text = "<b>" & Lang.FileSystem.Alumno.Corregir.MSG_FALTAN_REACTIVOS.Item(Session("Usuario_Idioma")) & " " + misRegistros.Item("Total").ToString + "</b> - " & Lang.FileSystem.Alumno.Corregir.LT_FALTAN_SUBTEMA.Item(Session("Usuario_Idioma")) & " " + misEvaluaciones.Item("Descripcion")
                misRegistros.Close()

                PageTitle_v1.show(Lang.FileSystem.Alumno.Corregir.LT_INICIO_TITULO.Item(Session("Usuario_Idioma")) & " " + Actividad,
                          titleIcon)

                'AQUI SACAR LOS PLANTEAMIENTOS QUE NO HAYAN SIDO CONTESTADOS CORRECTAMENTE. AQUI CAMBIA
                'LA CONSULTA CUANDO ES EL SEGUNDO INTENTO
                miComando.CommandText = "select P.IdPlanteamiento,P.IdSubtema,P.Consecutivo,P.Redaccion,P.TipoRespuesta,P.ArchivoApoyo,P.TipoArchivoApoyo,P.ArchivoApoyo2,P.TipoArchivoApoyo2,R.IdOpcion,P.Libro,P.Autor,P.Editorial,P.EsconderTexto,P.Tiempo,P.Minutos,P.Ancho1,P.Verifica,P.ComparaMayusculas,P.ComparaAcentos,ComparaSoloAlfanumerico,EliminaPuntuacion " +
                        " from Planteamiento P,Respuesta R,DetalleEvaluacion D where P.IdSubtema = " + misEvaluaciones.Item("IdSubtema").ToString + " and R.IdAlumno = " + CStr(Session("Usuario_IdAlumno")) +
                        " and D.IdEvaluacion = " + IdEvaluacion + " and R.idDetalleEvaluacion = D.IdDetalleEvaluacion" +
                        " and R.Intento = 1 and R.Acertada = 'N' and R.IdPlanteamiento = P.IdPlanteamiento and P.Ocultar = 0 and P.TipoRespuesta <> 'Abierta' and P.Permite2 = 1 "
                If misEvaluaciones("Aleatoria") Then
                    miComando.CommandText = miComando.CommandText + " order by newid()" 'Ordena aleatoriamente los registros
                Else
                    miComando.CommandText = miComando.CommandText + " order by Consecutivo"
                End If

                'Abrir la conexión y leo los registros del Planteamiento
                'objConexion.Open()
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                If misRegistros.Read() Then 'Leo para poder accesar el Planteamiento siguiente
                    'Los siguientes campos ocultos solo funcionan si los pongo aqui
                    HFidEvaluacion.Value = IdEvaluacion 'Este campo es para pasarlo al script PROCESA2.ASPX
                    HFidPlanteamiento.Value = misRegistros.Item("IdPlanteamiento") 'Este campo es para pasarlo al script PROCESA2.ASPX
                    If Not IsDBNull(misRegistros.Item("IdOpcion")) Then
                        HFidOpcion.Value = misRegistros.Item("IdOpcion") 'Este campo es para pasarlo al script PROCESA2.ASPX
                    Else
                        HFidOpcion.Value = 0 'Al darle valor de cero, significa que no se contestó nada (el los reactivos por tiempo esto puede pasar)
                    End If
                    HFplantel.Value = Plantel

                    If misRegistros.Item("EsconderTexto") Then
                        LblPlanteamiento.Visible = False
                    Else
                        LblPlanteamiento.Visible = True
                        Redaccion = Replace(HttpUtility.HtmlDecode(misRegistros.Item("Redaccion")), "*salto*", "<BR>")
                        Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                        If misRegistros.Item("Consecutivo") > 0 Then
                            LblPlanteamiento.Text = CStr(misRegistros.Item("Consecutivo")) + ".- " + Redaccion
                        Else
                            'OJO: Aqui, en el despliegue para alumnos, tendria que usar un contador que enumere el planteamiento
                            LblPlanteamiento.Text = "#.- " + Redaccion
                        End If
                    End If

                    'Ahora veo si es de tiempo:
                    If misRegistros.Item("Tiempo") Then
                        Timer1.Interval = misRegistros.Item("Minutos") * 60 * 1000
                        Timer1.Enabled = True
                        LblAlumno.Text = LblAlumno.Text + "<br><font color='#79CDCD'> " & Lang.FileSystem.Alumno.Corregir.MSG_MINUTOS_INICIO.Item(Session("Usuario_Idioma")) & " " + misRegistros.Item("Minutos").ToString + " " & Lang.FileSystem.Alumno.Corregir.MSG_MINUTOS_FIN.Item(Session("Usuario_Idioma")) & "</font>"
                    End If


                    HfComparaMayusculas.Value = misRegistros.Item("ComparaMayusculas").ToString()
                    HfComparaAcentos.Value = misRegistros.Item("ComparaAcentos").ToString()
                    HfComparaSoloAlfanumerico.Value = misRegistros.Item("ComparaSoloAlfanumerico").ToString()
                    HfPuntuacion.Value = misRegistros.Item("EliminaPuntuacion").ToString()
                    Dim verifica As Boolean = misRegistros.Item("Verifica")
                    If Not IsDBNull(misRegistros.Item("Ancho1")) Then
                        ancho1 = misRegistros.Item("Ancho1")
                    End If

                    'Saco la Referencia bibliográfica del reactivo (si la hay) que consta de 3 campos
                    Dim Referencia As String
                    Referencia = ""

                    'If (Not IsDBNull(misRegistros.Item("Libro"))) Then
                    'Referencia = HttpUtility.HtmlDecode(misRegistros.Item("Libro")) + ", "
                    'End If
                    'If (Not IsDBNull(misRegistros.Item("Autor"))) Then
                    'Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Autor")) + ", "
                    'End If
                    'If (Not IsDBNull(misRegistros.Item("Editorial"))) Then
                    'Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Editorial"))
                    'End If
                    '***

                    If Trim(misRegistros.Item("Libro").ToString <> "") Then
                        Referencia = HttpUtility.HtmlDecode(misRegistros.Item("Libro")) + ", "
                    End If
                    If Trim(misRegistros.Item("Autor").ToString <> "") Then
                        Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Autor")) + ", "
                    End If
                    If Trim(misRegistros.Item("Editorial").ToString <> "") Then
                        Referencia = Referencia + HttpUtility.HtmlDecode(misRegistros.Item("Editorial"))
                    End If
                    '***
                    If Trim(Referencia) <> "" Then
                        LblReferencia.Text = "(" & Lang.FileSystem.Alumno.Corregir.HINT_REFERENCIA.Item(Session("Usuario_Idioma")) & " " + Referencia + ")"
                    End If

                    'Cargo el archivo de apoyo 1 si lo hay, siempre deberán estar en el directorio material
                    If (Not IsDBNull(misRegistros.Item("TipoArchivoApoyo"))) And (Not IsDBNull(misRegistros.Item("ArchivoApoyo"))) Then
                        If Trim(misRegistros.Item("ArchivoApoyo")).Length > 0 Then 'Esta comparacion la hago aparte del IF superior porque marca error al querer sacar tamaño a valor NULL
                            If Trim(misRegistros.Item("TipoArchivoApoyo")) = "Imagen" Then
                                ImPlanteamiento.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                                ImPlanteamiento.Visible = True
                                HLapoyoplanteamiento.Visible = False
                                pnlDespliegaPlanteamiento1.Visible = False
                            ElseIf Trim(misRegistros.Item("TipoArchivoApoyo")) = "Audio" Then
                                pnlDespliegaPlanteamiento1.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:460px;overflow:hidden;")
                                pnlDespliegaPlanteamiento1.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto""  src=""" + Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo") + """></audio>"))
                            ElseIf Trim(misRegistros.Item("TipoArchivoApoyo")) = "Video" Then
                                LblVideo.Text = "<embed src=""" & Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo") + """ width=""280"" height=""227"" autostart=""true"" controller=""true"" align=""center""></embed>"
                                ImPlanteamiento.Visible = False
                                HLapoyoplanteamiento.Visible = False
                                pnlDespliegaPlanteamiento1.Visible = False
                            Else
                                HLapoyoplanteamiento.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo")
                                HLapoyoplanteamiento.Text = Lang.FileSystem.Alumno.Corregir.HINT_APOYO.Item(Session("Usuario_Idioma")) & " " + Trim(misRegistros.Item("ArchivoApoyo").ToString)
                                HLapoyoplanteamiento.Visible = True
                                ImPlanteamiento.Visible = False
                                pnlDespliegaPlanteamiento1.Visible = False
                            End If
                        Else
                            ImPlanteamiento.Visible = False
                            HLapoyoplanteamiento.Visible = False
                            pnlDespliegaPlanteamiento1.Visible = False
                        End If
                    End If

                    'Cargo el archivo de apoyo 2 si lo hay, siempre deberán estar en el directorio material
                    If (Not IsDBNull(misRegistros.Item("TipoArchivoApoyo2"))) And (Not IsDBNull(misRegistros.Item("ArchivoApoyo2"))) Then
                        If Trim(misRegistros.Item("ArchivoApoyo2")).Length > 0 Then 'Esta comparacion la hago aparte del IF superior porque marca error al querer sacar tamaño a valor NULL
                            If Trim(misRegistros.Item("TipoArchivoApoyo2")) = "Imagen" Then

                                ImPlanteamiento2.ImageUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo2")
                                ImPlanteamiento2.Visible = True
                                HLapoyoplanteamiento2.Visible = False
                                pnlDespliegaPlanteamiento2.Visible = False
                            ElseIf Trim(misRegistros.Item("TipoArchivoApoyo")) = "Audio" Then
                                pnlDespliegaPlanteamiento2.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:460px;overflow:hidden;")
                                pnlDespliegaPlanteamiento2.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo") + """></audio>"))
                            ElseIf Trim(misRegistros.Item("TipoArchivoApoyo2")) = "Video" Then
                                LblVideo2.Text = "<embed src=""" & Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo2") + """ width=""280"" height=""227"" autostart=""true"" controller=""true"" align=""center""></embed>"
                                ImPlanteamiento2.Visible = False
                                HLapoyoplanteamiento2.Visible = False
                                pnlDespliegaPlanteamiento2.Visible = False
                            Else
                                HLapoyoplanteamiento2.NavigateUrl = Config.Global.urlMaterial & misRegistros.Item("ArchivoApoyo2")
                                HLapoyoplanteamiento2.Text = Lang.FileSystem.Alumno.Corregir.HINT_APOYO.Item(Session("Usuario_Idioma")) & " " + Trim(misRegistros.Item("ArchivoApoyo2").ToString)
                                HLapoyoplanteamiento2.Visible = True
                                ImPlanteamiento2.Visible = False
                                pnlDespliegaPlanteamiento2.Visible = False
                            End If
                        Else
                            ImPlanteamiento2.Visible = False
                            HLapoyoplanteamiento2.Visible = False
                            pnlDespliegaPlanteamiento2.Visible = False
                        End If
                    End If

                    'OPCIONES
                    If Trim(misRegistros.Item("TipoRespuesta")) = "Multiple" Then
                        'Algoritmo respuesta multiple
                        PanelAbierta.Visible = False
                        PanelAbiertaAutomatica.Visible = False

                        miComando.CommandText = "SELECT * FROM Opcion where Esconder = 'False' and idPlanteamiento =@IdPlanteamiento ORDER BY Consecutiva"
                        miComando.Parameters.AddWithValue("@IdPlanteamiento", misRegistros.Item("IdPlanteamiento").ToString)
                        misRegistros.Close() 'Cierro la tabla de Planteamiento para poder usar este objeto de nuevo
                        da.SelectCommand = miComando
                        da.Fill(dt)

                        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros

                        If Not (misRegistros.Read()) Then 'Si hay algún reactivo que no tiene opciones para contestar por el alumno, posiblemente será capturada aparte. Con un solo reactivo que esté así no podrá terminar la actividad, así que mejor no lo dejo que ingrese
                            misRegistros.Close()
                            objConexion.Close()
                            Session("Nota") = Lang.FileSystem.Alumno.Corregir.MSG_NO_HAY_REACTIVOS.Item(Session("Usuario_Idioma"))
                            Return 1
                        End If


                        arrayConsecutivo = CType(New Utils.RandomGenerator, Utils.RandomGenerator).getRandomCombination(arrayConsecutivo, opcionesAleatorias, dt)

                        Redaccion = Replace(HttpUtility.HtmlDecode(dt.Rows(arrayConsecutivo(0))("Redaccion")), "*salto*", "<BR>")
                        Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                        TextoOpc1.Text = Redaccion
                        Opcion1.Text = IIf(opcionesAleatorias = 0, misRegistros.Item("Consecutiva"), String.Empty)
                        Opcion1Aux.Text = dt.Rows(arrayConsecutivo(0))("Consecutiva")


                        If dt.Rows(arrayConsecutivo(0))("idOpcion") = HFidOpcion.Value Then
                            Opcion1.Checked = True
                        End If

                        If Not IsDBNull(dt.Rows(arrayConsecutivo(0))("TipoArchivoApoyo")) Then
                            If dt.Rows(arrayConsecutivo(0))("TipoArchivoApoyo") = "Imagen" Then
                                ImOpc1.ImageUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(0))("ArchivoApoyo")
                                HLapoyoopc1.Visible = False
                                PnlDespliegaOpc1.Visible = False
                            ElseIf Trim(dt.Rows(arrayConsecutivo(0))("TipoArchivoApoyo")) = "Audio" Then
                                PnlDespliegaOpc1.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                                PnlDespliegaOpc1.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(0))("ArchivoApoyo") + """></audio>"))
                                ImOpc1.Visible = False
                                HLapoyoopc1.Visible = False
                            Else
                                HLapoyoopc1.NavigateUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(0))("ArchivoApoyo")
                                HLapoyoopc1.Text = Lang.FileSystem.Alumno.Corregir.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                                ImOpc1.Visible = False
                                PnlDespliegaOpc1.Visible = False
                            End If
                        Else
                            ImOpc1.Visible = False
                            HLapoyoopc1.Visible = False
                            PnlDespliegaOpc1.Visible = False
                        End If

                        If misRegistros.Read() Then 'OPCION 2

                            Redaccion = Replace(HttpUtility.HtmlDecode(dt.Rows(arrayConsecutivo(1))("Redaccion")), "*salto*", "<BR>")
                            Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                            TextoOpc2.Text = Redaccion
                            Opcion2.Text = IIf(opcionesAleatorias = 0, misRegistros.Item("Consecutiva"), String.Empty)
                            Opcion2Aux.Text = dt.Rows(arrayConsecutivo(1))("Consecutiva")

                            If dt.Rows(arrayConsecutivo(1))("idOpcion") = HFidOpcion.Value Then
                                Opcion2.Checked = True
                            End If

                            If Not IsDBNull(dt.Rows(arrayConsecutivo(1))("TipoArchivoApoyo")) Then

                                If dt.Rows(arrayConsecutivo(1))("TipoArchivoApoyo") = "Imagen" Then
                                    ImOpc2.ImageUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(1))("ArchivoApoyo")
                                    HLapoyoopc2.Visible = False
                                    PnlDespliegaOpc2.Visible = False
                                ElseIf Trim(dt.Rows(arrayConsecutivo(1))("TipoArchivoApoyo")) = "Audio" Then
                                    PnlDespliegaOpc2.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                                    PnlDespliegaOpc2.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(1))("ArchivoApoyo") + """></audio>"))
                                    ImOpc2.Visible = False
                                    HLapoyoopc2.Visible = False
                                Else
                                    HLapoyoopc2.NavigateUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(1))("ArchivoApoyo")
                                    HLapoyoopc2.Text = Lang.FileSystem.Alumno.Corregir.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                                    ImOpc2.Visible = False
                                    PnlDespliegaOpc2.Visible = False
                                End If
                            Else
                                ImOpc2.Visible = False
                                HLapoyoopc2.Visible = False
                                PnlDespliegaOpc2.Visible = False
                            End If
                        Else
                            ImOpc2.Visible = False
                            HLapoyoopc2.Visible = False
                            Opcion2.Visible = False
                            PnlDespliegaOpc2.Visible = False
                        End If

                        If misRegistros.Read() Then 'OPCION 3
                            Redaccion = Replace(HttpUtility.HtmlDecode(dt.Rows(arrayConsecutivo(2))("Redaccion")), "*salto*", "<BR>")
                            Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                            TextoOpc3.Text = Redaccion
                            If dt.Rows(arrayConsecutivo(2))("idOpcion") = HFidOpcion.Value Then
                                Opcion3.Checked = True
                            End If
                            Opcion3.Text = IIf(opcionesAleatorias = 0, misRegistros.Item("Consecutiva"), String.Empty)
                            Opcion3Aux.Text = dt.Rows(arrayConsecutivo(2))("Consecutiva")
                            If Not IsDBNull(dt.Rows(arrayConsecutivo(2))("TipoArchivoApoyo")) Then

                                If dt.Rows(arrayConsecutivo(2))("TipoArchivoApoyo") = "Imagen" Then
                                    ImOpc3.ImageUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(2))("ArchivoApoyo")
                                    HLapoyoopc3.Visible = False
                                    PnlDespliegaOpc3.Visible = False
                                ElseIf Trim(dt.Rows(arrayConsecutivo(2))("TipoArchivoApoyo")) = "Audio" Then
                                    PnlDespliegaOpc3.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                                    PnlDespliegaOpc3.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(2))("ArchivoApoyo") + """></audio>"))
                                    ImOpc3.Visible = False
                                    HLapoyoopc3.Visible = False
                                Else
                                    HLapoyoopc3.NavigateUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(2))("ArchivoApoyo")
                                    HLapoyoopc3.Text = Lang.FileSystem.Alumno.Corregir.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                                    ImOpc3.Visible = False
                                    PnlDespliegaOpc3.Visible = False
                                End If
                            Else
                                ImOpc3.Visible = False
                                HLapoyoopc3.Visible = False
                                PnlDespliegaOpc3.Visible = False
                            End If
                        Else
                            ImOpc3.Visible = False
                            HLapoyoopc3.Visible = False
                            Opcion3.Visible = False
                            PnlDespliegaOpc3.Visible = False
                        End If

                        If misRegistros.Read() Then 'OPCION 4

                            Redaccion = Replace(HttpUtility.HtmlDecode(dt.Rows(arrayConsecutivo(3))("Redaccion")), "*salto*", "<BR>")
                            Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                            TextoOpc4.Text = Redaccion
                            If dt.Rows(arrayConsecutivo(3))("idOpcion") = HFidOpcion.Value Then
                                Opcion4.Checked = True
                            End If
                            Opcion4.Text = IIf(opcionesAleatorias = 0, misRegistros.Item("Consecutiva"), String.Empty)
                            Opcion4Aux.Text = dt.Rows(arrayConsecutivo(3))("Consecutiva")

                            If Not IsDBNull(dt.Rows(arrayConsecutivo(3))("TipoArchivoApoyo")) Then
                                If dt.Rows(arrayConsecutivo(3))("TipoArchivoApoyo") = "Imagen" Then
                                    ImOpc4.ImageUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(3))("ArchivoApoyo")
                                    HLapoyoopc4.Visible = False
                                    PnlDespliegaOpc4.Visible = False
                                ElseIf Trim(dt.Rows(arrayConsecutivo(3))("TipoArchivoApoyo")) = "Audio" Then
                                    PnlDespliegaOpc4.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                                    PnlDespliegaOpc4.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(3))("ArchivoApoyo") + """></audio>"))
                                    ImOpc4.Visible = False
                                    HLapoyoopc4.Visible = False
                                Else
                                    HLapoyoopc4.NavigateUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(3))("ArchivoApoyo")
                                    HLapoyoopc4.Text = Lang.FileSystem.Alumno.Corregir.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                                    ImOpc4.Visible = False
                                    PnlDespliegaOpc4.Visible = False
                                End If
                            Else
                                ImOpc4.Visible = False
                                HLapoyoopc4.Visible = False
                                PnlDespliegaOpc4.Visible = False
                            End If
                        Else
                            ImOpc4.Visible = False
                            HLapoyoopc4.Visible = False
                            Opcion4.Visible = False
                            PnlDespliegaOpc4.Visible = False
                        End If

                        If misRegistros.Read() Then 'PASO A OPCION 5

                            Redaccion = Replace(HttpUtility.HtmlDecode(dt.Rows(arrayConsecutivo(4))("Redaccion")), "*salto*", "<BR>")
                            Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                            TextoOpc5.Text = Redaccion
                            If dt.Rows(arrayConsecutivo(4))("idOpcion") = HFidOpcion.Value Then
                                Opcion5.Checked = True
                            End If
                            Opcion5.Text = IIf(opcionesAleatorias = 0, misRegistros.Item("Consecutiva"), String.Empty)
                            Opcion5Aux.Text = dt.Rows(arrayConsecutivo(4))("Consecutiva")
                            If Not IsDBNull(dt.Rows(arrayConsecutivo(4))("TipoArchivoApoyo")) Then
                                If dt.Rows(arrayConsecutivo(4))("TipoArchivoApoyo") = "Imagen" Then

                                    ImOpc5.ImageUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(4))("ArchivoApoyo")
                                    HLapoyoopc5.Visible = False
                                    PnlDespliegaOpc5.Visible = False
                                ElseIf Trim(dt.Rows(arrayConsecutivo(4))("TipoArchivoApoyo")) = "Audio" Then
                                    PnlDespliegaOpc5.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                                    PnlDespliegaOpc5.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(4))("ArchivoApoyo") + """></audio>"))
                                    ImOpc5.Visible = False
                                    HLapoyoopc5.Visible = False
                                Else
                                    HLapoyoopc5.NavigateUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(4))("ArchivoApoyo")
                                    HLapoyoopc5.Text = Lang.FileSystem.Alumno.Corregir.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                                    ImOpc5.Visible = False
                                    PnlDespliegaOpc5.Visible = False
                                End If
                            Else
                                ImOpc5.Visible = False
                                HLapoyoopc5.Visible = False
                                PnlDespliegaOpc5.Visible = False
                            End If
                        Else
                            ImOpc5.Visible = False
                            HLapoyoopc5.Visible = False
                            Opcion5.Visible = False
                            PnlDespliegaOpc5.Visible = False
                        End If

                        If misRegistros.Read() Then 'PASO A OPCION 6

                            Redaccion = Replace(HttpUtility.HtmlDecode(dt.Rows(arrayConsecutivo(5))("Redaccion")), "*salto*", "<BR>")
                            Redaccion = Replace(HttpUtility.HtmlDecode(Redaccion), "*_*", "&nbsp;")
                            TextoOpc6.Text = Redaccion

                            If dt.Rows(arrayConsecutivo(5))("idOpcion") = HFidOpcion.Value Then
                                Opcion6.Checked = True
                            End If

                            Opcion6.Text = IIf(opcionesAleatorias = 0, misRegistros.Item("Consecutiva"), String.Empty)
                            Opcion6Aux.Text = dt.Rows(arrayConsecutivo(5))("Consecutiva")

                            If Not IsDBNull(dt.Rows(arrayConsecutivo(5))("TipoArchivoApoyo")) Then
                                If dt.Rows(arrayConsecutivo(5))("TipoArchivoApoyo") = "Imagen" Then
                                    ImOpc6.ImageUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(5))("ArchivoApoyo")
                                    HLapoyoopc6.Visible = False
                                    PnlDespliegaOpc6.Visible = False
                                ElseIf Trim(dt.Rows(arrayConsecutivo(5))("TipoArchivoApoyo")) = "Audio" Then
                                    PnlDespliegaOpc6.Attributes.Add("style", "margin-right:auto;margin-left:auto;width:340px;overflow:hidden;")
                                    PnlDespliegaOpc6.Controls.Add(New LiteralControl("<audio id=""reproductor"" preload=""auto"" src=""" + Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(5))("ArchivoApoyo") + """></audio>"))
                                    ImOpc6.Visible = False
                                    HLapoyoopc6.Visible = False
                                Else
                                    HLapoyoopc6.NavigateUrl = Config.Global.urlMaterial & dt.Rows(arrayConsecutivo(5))("ArchivoApoyo")
                                    HLapoyoopc6.Text = Lang.FileSystem.Alumno.Corregir.HL_MATERIAL_APOYO.Item(Session("Usuario_Idioma"))
                                    ImOpc6.Visible = False
                                    PnlDespliegaOpc6.Visible = False
                                End If
                            Else
                                ImOpc6.Visible = False
                                HLapoyoopc6.Visible = False
                                PnlDespliegaOpc6.Visible = False
                            End If
                        Else
                            ImOpc6.Visible = False
                            HLapoyoopc6.Visible = False
                            Opcion6.Visible = False
                            PnlDespliegaOpc6.Visible = False
                        End If

                    ElseIf Trim(misRegistros.Item("TipoRespuesta")) = "Abierta" Then 'de If Trim(res.Item("TipoRespuesta")) = "Multiple"
                        'Algoritmo cuando el tipo de respuesta es diferente de "Multiple"
                        HFidRespuestaAbierta.Value = ""
                        GuardarA.Visible = False
                        'OPCIONES (Todos los tipos de respuestas generan Registros en la tabla "Opcion")
                        miComando = New SqlCommand("SELECT * FROM Opcion where Esconder = 'False' and idPlanteamiento = @IdPlanteamiento order by Consecutiva", objConexion)

                        miComando.Parameters.AddWithValue("@IdPlanteamiento", HFidEvaluacion.Value)

                        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                        misRegistros.Read() 'Leo para poder accesarlos

                        'Algoritmo respuesta abierta (solo existe un registro en Opcion asignado a la Respuesta
                        TblOpciones.Visible = False
                        PanelAbierta.Visible = True
                        'Como respuesta "Abierta" tiene una sola opcion, la genero por default
                        OpcionL.Text = misRegistros.Item("Consecutiva") 'El texto está de color BLANCO para que no se vea
                        OpcionL.Checked = True
                        OpcionL.Visible = False

                    ElseIf Trim(misRegistros.Item("TipoRespuesta")) = "Abierta Calificada" Then
                        tbRespuestaAbiertaAutomatica.Width = ancho1
                        TblOpciones.Visible = False
                        PanelAbierta.Visible = False
                        PanelAbiertaAutomatica.Visible = True
                        GuardarA.Visible = False

                        ' guardo las respuestas correctas de las opciones en el hfVerifica para comparar sin postback
                        If verifica Then
                            VerificarAA.Visible = True
                            Using cmd2 As SqlCommand = New SqlCommand("SELECT Redaccion, Correcta FROM Opcion WHERE IdPlanteamiento = " & HFidEvaluacion.Value, objConexion)

                                Using results As SqlDataReader = cmd2.ExecuteReader()

                                    While results.Read()
                                        If results.Item("Correcta").ToString() = "S" Then
                                            HfVerificaRespuesta.Value += results.Item("Redaccion") & "~|~"
                                        End If
                                    End While
                                    If HfVerificaRespuesta.Value.Contains("~|~") Then
                                        HfVerificaRespuesta.Value = HfVerificaRespuesta.Value.Substring(0, HfVerificaRespuesta.Value.Length - 3)
                                    End If
                                End Using
                            End Using
                        Else
                            VerificarAA.Visible = False
                        End If
                    End If


                Else 'Si se viene aqui es que ya no tiene Planteamientos pendientes por contestar
                    'TAL VEZ YA NO SE NECESITE EL SIGUIENTE CODIGO PORQUE LO SUBÍ DEBIDO A LOS CAMBIOS DE ESTRUCTURAS
                    '(AL PARECER YA NO SE ENTRA A ESTE ELSE)

                    'SUMO EL TOTAL DE PUNTOS QUE DA LA EVALUACION Y LA CANTIDAD DE REACTIVOS QUE LA COMPONEN
                    'OJO QUE AQUÍ SACO EL PORCENTAJE DE ACIERTOS SOBRE LO CONTESTADO, NO SOBRE
                    'LOS PLANTEAMIENTOS TOTALES, AUNQUE SE SUPONE QUE COINCIDE PORQUE YA TERMINO LA EVALUACION
                    misRegistros.Close()
                    miComando.CommandText = "select Sum(P.Ponderacion) Puntos, Count(Ponderacion) Cant " +
              "from Respuesta R, Planteamiento P, DetalleEvaluacion D " +
              "where (R.IdPlanteamiento = P.IdPlanteamiento) and P.Ocultar = 0 " +
              "and R.IdAlumno = " + CStr(Session("Usuario_IdAlumno")) + " and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion " +
              "and D.IdEvaluacion  = " + CStr(IdEvaluacion)
                    misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                    misRegistros.Read()
                    Dim TotalPuntos = misRegistros.Item("Puntos")
                    Dim TotalReactivos = misRegistros.Item("Cant")

                    'CHECO CUANTOS PUNTOS SUMO EL ALUMNO CORRECTAMENTE EN EL PRIMER INTENTO
                    misRegistros.Close()
                    miComando.CommandText = "select Sum(P.Ponderacion) Puntos, Count(Ponderacion) Cant " +
              "from Respuesta R, Planteamiento P, DetalleEvaluacion D " +
              "where (R.IdPlanteamiento = P.IdPlanteamiento) and P.Ocultar = 0 " +
              "and R.IdAlumno = " + CStr(Session("Usuario_IdAlumno")) + " and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion " +
              "and D.IdEvaluacion  = " + CStr(IdEvaluacion) + " and R.Acertada = 'S' and R.Intento = 1"
                    misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                    misRegistros.Read()
                    Dim PuntosOk As Decimal
                    If (Not IsDBNull(misRegistros.Item("Puntos"))) Then
                        PuntosOk = misRegistros.Item("Puntos")
                    Else
                        PuntosOk = 0 'Puede darse el caso de que en el primer intento no tenga ningún reactivo correcto
                    End If
                    Dim ReactivosAcertados = misRegistros.Item("Cant")

                    'CHECO CUANTOS PUNTOS SUMO EL ALUMNO CORRECTAMENTE EN EL SEGUNDO INTENTO
                    misRegistros.Close()
                    miComando.CommandText = "select Sum(Ponderacion - (P.Ponderacion * P.PorcentajeRestarResp)/100) Puntos, Count(Ponderacion) Cant " +
              "from Respuesta R, Planteamiento P, DetalleEvaluacion D " +
              "where (R.IdPlanteamiento = P.IdPlanteamiento) and P.Ocultar = 0 " +
              "and R.IdAlumno = " + CStr(Session("Usuario_IdAlumno")) + " and R.IdDetalleEvaluacion = D.IdDetalleEvaluacion " +
              "and D.IdEvaluacion  = " + CStr(IdEvaluacion) + " and R.Acertada = 'S' and R.Intento = 2"
                    misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                    misRegistros.Read()

                    If (Not IsDBNull(misRegistros.Item("Puntos"))) Then  '(misRegistros.Item("Cant") > 0) no es necesario
                        PuntosOk = PuntosOk + misRegistros.Item("Puntos")
                        ReactivosAcertados = ReactivosAcertados + misRegistros.Item("Cant")
                    End If

                    'CALCULO EL RESULTADO
                    Dim Resultado = (PuntosOk / TotalPuntos) * 100

                    'Actualizo el registro de la evaluacion terminada
                    SDSevaluacionesterminadas.UpdateCommand = "SET dateformat dmy; UPDATE EvaluacionTerminada SET TotalReactivos = " + CStr(TotalReactivos) +
              ", ReactivosContestados = " + CStr(ReactivosAcertados) +
              ", PuntosPosibles = " + CStr(FormatNumber(TotalPuntos, 2)) + ", PuntosAcertados = " + CStr(FormatNumber(PuntosOk, 2)) +
              ", Resultado = " + CStr(FormatNumber(Resultado, 2)) + ", FechaTermino = CAST(getdate() as smalldatetime)" +
              " where IdAlumno = " + CStr(Session("Usuario_IdAlumno")) + " and IdGrupo = " + CStr(Session("Usuario_IdGrupo")) +
              " and IdGrado = " + CStr(Session("Usuario_IdGrado")) +
              " and IdCicloEscolar = " + CStr(HFcicloescolar.Value) + " and IdEvaluacion = " + CStr(HFidEvaluacion.Value)
                    SDSevaluacionesterminadas.Update()
                    Session("Nota") = Lang.FileSystem.Alumno.Corregir.MSG_TERMINO.Item(Session("Usuario_Idioma"))

                    misRegistros.Close()
                    objConexion.Close()

                    'ANTES 1: Response.Redirect("~/alumno/cursos/")
                    'ANTES 2: Response.Redirect("~/alumno/cursos/actividad/?IdEvaluacion=" + IdEvaluacion.ToString + "&IdGrado=" + IdGrado.ToString + "&Tarea=" + Session("Tarea") + "&Plantel=" + Plantel)
                    Session("IdEvaluacion") = IdEvaluacion  'Session("Usuario_IdAlumno") ya está generada
                    Return 2
                End If ' del misRegistros.Read() de Planteamiento (ESTE ELSE NO SE EJECUTA NUNCA, CONSIDERAR QUE AQUI SE ENTRA PARA CORREGIR)
                misRegistros.Close()
                objConexion.Close()
            Else 'del misEvaluaciones.Read() 'Si llega aqui es porque no hay evaluaciones disponibles    
                'También llega aquí si el subtema ligado a la Evaluacion NO tiene Reactivos, debido a que se diseñó como actividad de entrega de Documento
                misEvaluaciones.Close()
                objConexionEv.Close()
        Session("Nota") = Lang.FileSystem.Alumno.Corregir.MSG_NO_DISPONIBLE.Item(Session("Usuario_Idioma")) 'En realidad este mensaje ya no aparecería porque ahora se va directo a muestramaterial.aspx
        'ANTES:Response.Redirect("~/alumno/cursos/")
        'AHORA: Para poder ver el material de apoyo aunque la actividad ya haya caducado:
        'Response.Redirect("~/alumno/cursos/actividad/?IdEvaluacion=" + IdEvaluacion + "&IdGrado=" + IdGrado + "&Tarea=" + Session("Actividad") + "&Plantel=" + Plantel)

        ' 22/04/2014 al intentar corregir una evaluación con fechas vencidas no indica ningún error, de modo que se revirtió:
        Return 1
      End If ' del misEvaluaciones.Read()
      misEvaluaciones.Close()
      objConexionEv.Close()
    Catch ex As Exception
      Utils.LogManager.ExceptionLog_InsertEntry(ex)
      msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
      objConexionEv.Close()
    End Try

    Return 0
  End Function

  ' imprimo los estilos variables de ésta página
  Protected Sub OutputCss()
    Dim s As StringBuilder = New StringBuilder()
    s.Append("<style type='text/css'>")
    s.Append("  #menu .current_area_cursos a {") ' esta clase css debe corresponder a la clase del boton a resaltar
    s.Append("    border-bottom: 5px solid " & Config.Color.MenuBar_SelectedOption_BorderColor & ";")
    s.Append("  }")
    s.Append("  .AssignmentHeaderRow {")
    s.Append("    background-color: " & Config.Color.MenuPendientes_Subtitle & ";")
    s.Append("  }")
    s.Append("</style>")
    ltEstilos.Text += s.ToString()
  End Sub

#End Region

    'Public ReadOnly Property EspecificaPlantel() As String
    '    Get
    '        Return HFplantel.Value
    '    End Get
    'End Property

    'Public ReadOnly Property PlanteamientoActual() As String
    '    Get
    '        Return HFidPlanteamiento.Value
    '    End Get
    'End Property

    'Public ReadOnly Property EvaluacionActual() As String
    '    Get
    '        Return HFidEvaluacion.Value
    '    End Get
    'End Property

    'Public ReadOnly Property BotonActivado() As String
    '    Get
    '        Return HFboton.Value
    '    End Get
    'End Property

    'Public ReadOnly Property CicloEscolarActual() As String
    '    Get
    '        Return HFcicloescolar.Value
    '    End Get
    'End Property

    'Public ReadOnly Property DetalleEvaluacionActual() As String
    '    Get
    '        Return HFdetalleEvaluacion.Value
    '    End Get
    'End Property

    'Public ReadOnly Property OpcionElegida() As String
    '    Get
    '        Return HFidOpcion.Value
    '    End Get
    'End Property

    Public ReadOnly Property RespuestaSeleccionada() As String
        Get
            Dim Respuesta As String
            If Opcion1.Checked Then
                Respuesta = Opcion1Aux.Text
            ElseIf Opcion2.Checked Then
                Respuesta = Opcion2Aux.Text
            ElseIf Opcion3.Checked Then
                Respuesta = Opcion3Aux.Text
            ElseIf Opcion4.Checked Then
                Respuesta = Opcion4Aux.Text
            ElseIf Opcion5.Checked Then
                Respuesta = Opcion5Aux.Text
            ElseIf Opcion6.Checked Then
                Respuesta = Opcion6Aux.Text
            Else : Respuesta = "0"
            End If
            Return Respuesta
        End Get
    End Property

    'Public ReadOnly Property GradoActual() As String
    '    Get
    '        Return HFidGrado.Value
    '    End Get
    'End Property
    Protected Sub AceptarAA_Click(sender As Object, e As EventArgs) Handles AceptarAA.Click
        Context.Items.Add("BotonActivado", AceptarA.ID)

        ' obtengo la respuesta 
        Context.Items.Add("RespuestaSeleccionada", evaluaRespuestaAbierta())
        Context.Items.Add("PlanteamientoActual", HFidPlanteamiento.Value.Trim())
        Context.Items.Add("CicloEscolarActual", HFcicloescolar.Value.Trim())
        Context.Items.Add("DetalleEvaluacionActual", HFdetalleEvaluacion.Value.Trim())
        Context.Items.Add("RespuestaAbierta", tbRespuestaAbiertaAutomatica.Text.Trim())
        Context.Items.Add("idRespuestaGuardada", HFidRespuestaAbierta.Value.Trim())
        Server.Transfer("~/alumno/cursos/corregir/procesa/Default.aspx")
    End Sub

    Protected Sub CancelarAA_Click(sender As Object, e As EventArgs) Handles CancelarAA.Click
        Context.Items.Add("BotonActivado", CancelarAA.ID)
        Context.Items.Add("RespuestaSeleccionada", evaluaRespuestaAbierta())
        Context.Items.Add("PlanteamientoActual", HFidPlanteamiento.Value.Trim())
        Context.Items.Add("CicloEscolarActual", HFcicloescolar.Value.Trim())
        Context.Items.Add("DetalleEvaluacionActual", HFdetalleEvaluacion.Value.Trim())
        Context.Items.Add("RespuestaAbierta", tbRespuestaAbiertaAutomatica.Text.Trim())
        Context.Items.Add("idRespuestaGuardada", HFidRespuestaAbierta.Value.Trim())
        Server.Transfer("~/alumno/cursos/corregir/procesa/Default.aspx")
    End Sub

    Protected Sub Aceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Aceptar.Click
        Context.Items.Add("BotonActivado", Aceptar.ID)
        Context.Items.Add("RespuestaSeleccionada", RespuestaSeleccionada())
        Context.Items.Add("PlanteamientoActual", HFidPlanteamiento.Value.Trim())
        Context.Items.Add("CicloEscolarActual", HFcicloescolar.Value.Trim())
        Context.Items.Add("DetalleEvaluacionActual", HFdetalleEvaluacion.Value.Trim())
        Server.Transfer("~/alumno/cursos/corregir/procesa/Default.aspx")
        'HFboton.Value = Aceptar.ID
    End Sub

    Protected Sub Omitir_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cancelar.Click
        Context.Items.Add("BotonActivado", Cancelar.ID)
        Context.Items.Add("RespuestaSeleccionada", RespuestaSeleccionada())
        Context.Items.Add("PlanteamientoActual", HFidPlanteamiento.Value.Trim())
        Context.Items.Add("CicloEscolarActual", HFcicloescolar.Value.Trim())
        Context.Items.Add("DetalleEvaluacionActual", HFdetalleEvaluacion.Value.Trim())
        Server.Transfer("~/alumno/cursos/corregir/procesa/Default.aspx")
        'HFboton.Value = Cancelar.ID
    End Sub

    Protected Sub Timer1_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.PreRender
        'Esto se ejecuta antes de que el Timer se ejecute.
        Session("IdPlant") = HFidPlanteamiento.Value
    End Sub

    Protected Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'ALGORITMO PARA ALMACENAR LA RESPUESTA DADA CUANDO VENCIÓ EL TIEMPO. AL PARECER SE EJECUTA YA QUE SE CARGÓ LA PÁGINA DONDE ESTÁ COLOCADO EL TIMER (DESPUES DE VENCER EL TIEMPO)
        'PASO 1) Obtengo los datos para ingresar la respuesta
        'ANTES IDENTIFICAR QUE SI NO ES ALUMNO (ES ADMINISTRADOR) NO EJECUTE ESTE CODIGO
        Dim Usuario = User.Identity.Name
        Dim strConexion As String
        'TAMBIEN FUNCIONA: strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader

        miComando = objConexion.CreateCommand
        'El Login de Usuario es único en el sistema
        miComando.CommandText = "SELECT A.IdAlumno, G.IdGrupo, G.IdGrado FROM Usuario U, Alumno A, Grupo G where U.Login = '" + Usuario + "' and A.IdUsuario = U.IdUsuario and G.IdGrupo = A.IdGrupo"

        'Try
        'Abrir la conexión y leo los registros del Planteamiento
        objConexion.Open()
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        misRegistros.Read() 'Leo para poder accesarlos
        Dim IdAlumno = CStr(misRegistros.Item("IdAlumno"))
        Dim IdGrado = CStr(misRegistros.Item("IdGrado"))
        Dim IdGrupo = CStr(misRegistros.Item("IdGrupo"))
        Dim IdPlanteamiento = Session("IdPlant") 'Si elijo HFidPlanteamiento.Value me pone el valor del siguiente reactivo que salió
        Dim Consecutiva = "0"
        If RespuestaSeleccionada <> "0" Then
            Consecutiva = RespuestaSeleccionada
        End If
        Dim IdCicloEscolar = HFcicloescolar.Value
        Dim IdDetalleEvaluacion = HFdetalleEvaluacion.Value
        misRegistros.Close()

        'PASO 2) Obtengo el Id de la Opcion
        miComando.CommandText = "select IdOpcion, Consecutiva, Correcta from Opcion where idPlanteamiento = " + IdPlanteamiento + " and Consecutiva = '" + Consecutiva + "'"
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        If misRegistros.Read() Then 'Leo para poder accesarlos
            Dim IdOpcion = CStr(misRegistros.Item("IdOpcion"))
            Dim Correcta = misRegistros.Item("Correcta")
            'No es necesario ver que tipo de Respuesta es porque las Abiertas no tienen segunda vuelta

            'PASO 3) Ingreso la respuesta, pero antes verifico si no habia ya ingresado la respuesta para que sea el segundo intento
            misRegistros.Close()
            'En el select con el puro IdDetalleEvaluacion puedo localizar la respuesta, pero lo pongo para que quede mas amarrado
            miComando.CommandText = "select IdRespuesta, Intento, Acertada from Respuesta where IdDetalleEvaluacion = " + IdDetalleEvaluacion + " and IdPlanteamiento = " + IdPlanteamiento + " and IdAlumno = " + IdAlumno
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros

            If misRegistros.Read() Then
                Dim Intento = misRegistros.Item("Intento")
                Intento = Intento + 1
                If Intento > 2 Then 'Esto lo hago para que no intenten escribir el URL directamente en el navegador y seguir contestando. Y también es la clave para pemitir mas vueltas
                    Session("Nota") = "Está intentado contestar una actividad ilegalmente"
                    Response.Redirect("~/alumno/cursos/")
                End If
                SDSresp.UpdateCommand = "SET dateformat dmy; UPDATE Respuesta SET IdOpcion = " + IdOpcion + ", Intento = " + CStr(Intento) + _
                        ", Acertada = '" + Correcta + "', FechaContestada = getdate() WHERE IdDetalleEvaluacion = " + IdDetalleEvaluacion + _
                       " and IdPlanteamiento = " + IdPlanteamiento + " and IdAlumno = " + IdAlumno + " and IdGrupo = " + IdGrupo + _
                       " and IdGrado = " + IdGrado + " and IdCicloEscolar = " + IdCicloEscolar
                SDSresp.Update()
            Else 'En realidad aquí no entraría porque ya es segunda vuelta, pero por si las dudas lo dejo
                SDSresp.InsertCommand = "SET dateformat dmy; INSERT INTO Respuesta (IdPlanteamiento,IdOpcion,IdAlumno,IdGrado,IdGrupo,IdCicloEscolar,IdDetalleEvaluacion,Intento,Acertada,FechaContestada) VALUES (" + _
                                                             IdPlanteamiento + "," + IdOpcion + "," + IdAlumno + "," + IdGrado + "," + IdGrupo + "," + IdCicloEscolar + "," + IdDetalleEvaluacion + ",1,'" + Correcta + "',getdate()); SELECT @NuevoId = @@Identity"
                SDSresp.Insert()
            End If
        Else
            'AQUI VA EL CÓDIGO DE COMO CONTESTAR SI SE LE TERMINÓ EL TIEMPO Y NO TENÍA NADA SELECCIONADO (EN RESPUESTA MULTIPLE)
            'Y AL TRATARSE DE SEGUNDA VUELTA SIGNIFICA EN LA PRIMERA TAMPOCO SELECCIONO NADA (EN CASO DE MAS VUELTAS, CONTROLAR AQUI EL NUMERO DE INTENTOS)
            SDSresp.UpdateCommand = "SET dateformat dmy; UPDATE Respuesta SET IdOpcion = NULL, Intento = 2" + _
                        ", Acertada = 'N', FechaContestada = getdate() WHERE IdDetalleEvaluacion = " + IdDetalleEvaluacion + _
                       " and IdPlanteamiento = " + IdPlanteamiento + " and IdAlumno = " + IdAlumno + " and IdGrupo = " + IdGrupo + _
                       " and IdGrado = " + IdGrado + " and IdCicloEscolar = " + IdCicloEscolar
            SDSresp.Update()
        End If
        misRegistros.Close()
        objConexion.Close()

        'PASO 5) Redirecciono a la página nuevamente
        Response.Redirect("~/alumno/cursos/corregir/")

        '  Catch ex As Exception
        'msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        ' End Try
    End Sub
    Protected Function evaluaRespuestaAbierta() As String
        Dim respuesta As String = tbRespuestaAbiertaAutomatica.Text
        Dim resultado As String = "-" ' pretende inicialmente que no coincidió con ninguna respuesta correcta
        Dim comparaMayusculas As Boolean = Boolean.Parse(HfComparaMayusculas.Value)
        Dim comparaAcentos As Boolean = Boolean.Parse(HfComparaAcentos.Value)
        Dim comparaSoloAlfanumerico As Boolean = Boolean.Parse(HfComparaSoloAlfanumerico.Value)
        Dim eliminaPuntuacion As Boolean = Boolean.Parse(HfPuntuacion.Value)

        Try
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.
                                                            ConnectionStrings("sadcomeConnectionString").
                                                            ConnectionString)
                conn.Open()
                Using cmd As SqlCommand = New SqlCommand("SELECT * FROM Opcion Where IdPlanteamiento = @IdPlanteamiento", conn)

                    cmd.Parameters.AddWithValue("@IdPlanteamiento", HFidPlanteamiento.Value)

                    Using results As SqlDataReader = cmd.ExecuteReader()

                        While (results.Read())
                            If (Siget.Utils.StringUtils.ComparaRespuesta(results.Item("Redaccion").ToString(),
                                                                         respuesta,
                                                                         comparaMayusculas,
                                                                         comparaAcentos,
                                                                         comparaSoloAlfanumerico,
                                                                         eliminaPuntuacion
                                                                         ) = 0) Then
                                resultado = results.Item("Consecutiva")
                                Exit While
                            End If
                        End While
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            Return False
        End Try

        Return resultado
    End Function
End Class
