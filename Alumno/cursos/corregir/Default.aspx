﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/alumno/principal_alumno_5-1-0.master"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="alumno_cursos_corregir_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/PageTitle_v1.ascx" TagPrefix="uc1" TagName="PageTitle_v1" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
     <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts%>audiojs/audiojs/audio.min.js"></script>
    <script type="text/javascript">
        audiojs.events.ready(function () {
            var as = audiojs.createAll();
        });
    </script>
    <style type="text/css">
        .style11 {
            width: 100%;
        }

        .style21 {
        }

        .style24 {
            color: #FFFFFF;
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
            font-weight: bold;
            background-color: #CCCCCC;
        }

        .style23 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: large;
            font-weight: bold;
            color: #0000FF;
            text-align: right;
        }

        .style18 {
            width: 345px;
        }

        .style25 {
            width: 107px;
        }

        .style26 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
            color: #000099;
            font-weight: bold;
            background-color: #CCCCCC;
        }

        .style27 {
            font-family: Arial, Helvetica, sans-serif;
        }

        .style28 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-large;
            font-weight: bold;
        }

        .style29 {
            width: 36px;
            height: 8px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style30 {
            height: 8px;
        }

        .style31 {
            width: 50px;
            height: 8px;
            font-family: Arial, Helvetica, sans-serif;
        }

        .style32 {
            width: 36px;
            height: 8px;
        }

        .style37 {
            color: #FFFFFF;
            font-size: small;
        }
        .auto-style1 {
            height: 21px;
        }
    </style>

    <%-- Aquí imprimo los estilos variables de ésta página específica --%>
    <asp:Literal ID="ltEstilos" runat="server" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">

    <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1" />

    <table style="width: 100%;">
        <tr>
            <td style="text-align: center;">
                <asp:Label ID="LblFaltan" runat="server"
                    Style="font-size: small; font-family: Arial, Helvetica, sans-serif;"></asp:Label>
            </td>
        </tr>
    </table>
    
    <table style="background-color: white; margin-left: auto; margin-right: auto; max-width: 800px;">
        <tr class="titulo">
            <td colspan="5">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style21" colspan="3">
                <asp:Label ID="LblPlanteamiento" runat="server"
                    Style="font-size: 22px; font-weight: 700; color: #000099;"
                    Text="Label" CssClass="style27"></asp:Label>
            </td>
            <td colspan="2">
                <asp:Image ID="ImPlanteamiento" runat="server" Visible="False" />
                <asp:HyperLink ID="HLapoyoplanteamiento" runat="server" Target="_blank"
                    CssClass="style26" Visible="False">[HLapoyoplanteamiento]</asp:HyperLink>
                <asp:Label ID="LblVideo" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
                <asp:Panel ID="pnlDespliegaPlanteamiento1" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style21" colspan="3">
                <asp:Image ID="ImPlanteamiento2" runat="server" Visible="False" />
                <asp:HyperLink ID="HLapoyoplanteamiento2" runat="server" Target="_blank"
                    CssClass="style26" Visible="False">[HLapoyoplanteamiento2]</asp:HyperLink>
                <asp:Label ID="LblVideo2" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
                           <asp:Panel ID="pnlDespliegaPlanteamiento2" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
            </td>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style21" colspan="5" style="background-color: white;">
                <asp:Label ID="LblReferencia" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: x-small"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="5" style="background-color: #666666; text-align: right;" class="style24">
                <asp:Label ID="LblAlumno" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700; color: #FFFFFF"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="style12" colspan="5">
                <table id="TblOpciones" runat="server" class="style11">
                    <tr>
                        <td class="style30">
                            <asp:RadioButton ID="Opcion1" runat="server" GroupName="eleccion"
                                CssClass="style23" Width="50px" TextAlign="Left" />
                            <asp:RadioButton ID="Opcion1Aux" runat="server" Visible="false"
                                CssClass="style23" Width="50px" TextAlign="Left" />
                        </td>
                        <td class="style18">
                            <asp:Label ID="TextoOpc1" runat="server" CssClass="style28"></asp:Label>
                        </td>
                        <td class="style32">
                            <asp:RadioButton ID="Opcion2" runat="server" GroupName="eleccion"
                                CssClass="style23" Width="50px" TextAlign="Left" />
                             <asp:RadioButton ID="Opcion2Aux" runat="server" Visible="false"
                                CssClass="style23" Width="50px" TextAlign="Left" />
                        </td>
                        <td colspan="2">
                            <asp:Label ID="TextoOpc2" runat="server" CssClass="style28"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style31">&nbsp;</td>
                        <td class="style18">
                            <asp:Image ID="ImOpc1" runat="server" CssClass="style27" />
                            <asp:Panel ID="PnlDespliegaOpc1" runat="server"
                                 BackColor="#E3EAEB" Style="text-align: center">
                             </asp:Panel>
                            <asp:HyperLink ID="HLapoyoopc1" runat="server" Target="_blank"
                                CssClass="style26">[HLapoyoopc1]</asp:HyperLink>
                        </td>
                        <td class="style29">&nbsp;</td>
                        <td colspan="2">
                            <asp:Image ID="ImOpc2" runat="server" CssClass="style27" />
                             <asp:Panel ID="PnlDespliegaOpc2" runat="server"
                                 BackColor="#E3EAEB" Style="text-align: center">
                              </asp:Panel>
                            <asp:HyperLink ID="HLapoyoopc2" runat="server" Target="_blank"
                                CssClass="style26">[HLapoyoopc2]</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="style30">
                            <asp:RadioButton ID="Opcion3" runat="server" GroupName="eleccion"
                                CssClass="style23" Width="50px" TextAlign="Left" />
                             <asp:RadioButton ID="Opcion3Aux" runat="server" Visible="false"
                                CssClass="style23" Width="50px" TextAlign="Left" />
                        </td>
                        <td class="style18">
                            <asp:Label ID="TextoOpc3" runat="server" CssClass="style28"></asp:Label>
                        </td>
                        <td class="style32">
                            <asp:RadioButton ID="Opcion4" runat="server" GroupName="eleccion"
                                CssClass="style23" Width="50px" TextAlign="Left" />
                             <asp:RadioButton ID="Opcion4Aux" runat="server" Visible="false"
                                CssClass="style23" Width="50px" TextAlign="Left" />
                        </td>
                        <td colspan="2">
                            <asp:Label ID="TextoOpc4" runat="server" CssClass="style28"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style31">&nbsp;</td>
                        <td class="style18">
                            <asp:Image ID="ImOpc3" runat="server" CssClass="style27" />
                              <asp:Panel ID="PnlDespliegaOpc3" runat="server"
                                BackColor="#E3EAEB" Style="text-align: center">
                             </asp:Panel>
                            <asp:HyperLink ID="HLapoyoopc3" runat="server" Target="_blank"
                                CssClass="style26">[HLapoyoopc3]</asp:HyperLink>
                        </td>
                        <td class="style29">&nbsp;</td>
                        <td colspan="2">
                            <asp:Image ID="ImOpc4" runat="server" CssClass="style27" />
                            <asp:Panel ID="PnlDespliegaOpc4" runat="server"
                             BackColor="#E3EAEB" Style="text-align: center">
                            </asp:Panel>
                            <asp:HyperLink ID="HLapoyoopc4" runat="server" Target="_blank"
                                CssClass="style26">[HLapoyoopc4]</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="style30">
                            <asp:RadioButton ID="Opcion5" runat="server" GroupName="eleccion"
                                CssClass="style23" Width="50px" TextAlign="Left" />
                             <asp:RadioButton ID="Opcion5Aux" runat="server" Visible="false"
                                CssClass="style23" Width="50px" TextAlign="Left" />

                        </td>
                        <td class="style18">
                            <asp:Label ID="TextoOpc5" runat="server" CssClass="style28"></asp:Label>
                        </td>
                        <td class="style32">
                            <asp:RadioButton ID="Opcion6" runat="server" GroupName="eleccion"
                                CssClass="style23" Width="50px" TextAlign="Left" />
                            <asp:RadioButton ID="Opcion6Aux" runat="server" Visible="false"
                                CssClass="style23" Width="50px" TextAlign="Left" />
                        </td>
                        <td colspan="2">
                            <asp:Label ID="TextoOpc6" runat="server" CssClass="style28"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="style31">&nbsp;</td>
                        <td class="style18">
                            <asp:Image ID="ImOpc5" runat="server" CssClass="style27" />
                             <asp:Panel ID="PnlDespliegaOpc5" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
                            <asp:HyperLink ID="HLapoyoopc5" runat="server" Target="_blank"
                                CssClass="style26">[HLapoyoopc5]</asp:HyperLink>
                        </td>
                        <td class="style29">&nbsp;</td>
                        <td colspan="2">
                            <asp:Image ID="ImOpc6" runat="server" CssClass="style27" />
                            <asp:Panel ID="PnlDespliegaOpc6" runat="server"
                        BackColor="#E3EAEB" Style="text-align: center">
                   </asp:Panel>
                            <asp:HyperLink ID="HLapoyoopc6" runat="server" Target="_blank"
                                CssClass="style26">[HLapoyoopc6]</asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td class="style30">&nbsp;</td>
                        <td class="style18">
                            &nbsp;</td>
                        <td class="style32">&nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="style30">
                            &nbsp;</td>
                        <td class="style18">
                            <asp:Button ID="Aceptar" runat="server" 
                                Text="[Aceptar Respuesta]" 
                                OnClientClick="this.disabled=true" 
                                UseSubmitBehavior="False"
                                CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                            &nbsp;
                            <asp:Button ID="Cancelar" runat="server" 
                                Text="  [Salir]  " 
                                CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                        </td>
                        <td class="style32">&nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>

                <asp:Panel ID="PanelAbierta" runat="server">
          <table style="margin-right: auto; margin-left: auto;">
            <tr>
              <td class="style25" colspan="4" style="background-color: #000066;">
                <asp:Literal ID="lt_hint_redacte_respuesta" runat="server">[Redacte su respuesta:]</asp:Literal>
              </td>
            </tr>
            <tr>
              <td class="style27" colspan="2">
                <asp:RadioButton ID="OpcionL" runat="server" CssClass="style23"
                  GroupName="eleccion" TextAlign="Left" Width="50px" />
              </td>
              <td>
                <asp:TextBox ID="TBrespuestaA" runat="server"
                  ClientIDMode="Static"
                  TextMode="MultiLine"
                  CssClass="tinymce"></asp:TextBox>
                <asp:ImageButton ID="IBwords" runat="server" Height="26px"
                  ImageUrl="~/Resources/imagenes/palabras.png" Width="26px" Visible="false" />
              </td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td colspan="3" style="text-align: center;">
                <span id="validacionAbierta" style="font-size: 1.3em; font-weight: bold; display: none; color: rgb(213, 29, 29);">
                  <asp:Literal ID="ltRespuestaVacia" runat="server"></asp:Literal>
                </span>
              </td>
            </tr>
            <tr>
              <td class="style26">&nbsp;</td>
              <td colspan="2" style="text-align: center;">
                <asp:Button ID="AceptarA" runat="server"
                  ClientIDMode="AutoID"
                  Text="[Aceptar Respuesta]"
                  CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
                                <asp:Button ID="GuardarA" runat="server"
                                  ClientIDMode="AutoID"
                                  Text="[Guardar Respuesta]"
                                  CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                &nbsp;
                                <asp:Button ID="CancelarA" runat="server"
                                  Text="   [Salir]   "
                                  CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
              </td>
              <td>&nbsp;</td>
            </tr>
          </table>
        </asp:Panel>
        <asp:Panel ID="PanelAbiertaAutomatica" runat="server" Visible="false">
          <table style="margin-left: auto; margin-right: auto;">
            <tr>
              <td>
                <asp:TextBox ID="tbRespuestaAbiertaAutomatica" runat="server"
                  ClientIDMode="Static" CssClass="wideTextbox"></asp:TextBox>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>
                <asp:Button ID="VerificarAA" runat="server" Visible="false"
                  ClientIDMode="AutoID"
                  OnClientClick="verify(); return false;"
                  Text="[Verificar Respuesta]"
                  CssClass="defaultBtn btnThemeGreen btnThemeMedium" />
                &nbsp;
                <asp:Button ID="AceptarAA" runat="server"
                  ClientIDMode="AutoID"
                  Text="[Aceptar Respuesta]"
                  CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
                <asp:Button ID="CancelarAA" runat="server"
                  Text="   [Salir]   "
                  CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
                <script>
                    function verify() {
                        var str = $("#HfVerificaRespuesta").val().split("~|~");
                        var currAnswer;
                        var usrResponse = $("#tbRespuestaAbiertaAutomatica").val();

                        usrResponse = procesaString(usrResponse);

                        for (var i = 0; i < str.length; i++) {
                            currAnswer = str[i];

                            currAnswer = procesaString(currAnswer);

                            if (currAnswer == usrResponse) {
                                $("#msgVerificaBien").show();
                                $("#msgVerificaMal").hide();
                                return;
                            }
                        }

                        $("#msgVerificaBien").hide();
                        $("#msgVerificaMal").show();
                    }

                    // TODO - el comparar alfanumérico en esta función elimina también caracteres con acentos :S
                    function procesaString(input) {
                        if ($("#HfComparaMayusculas").val() != 'True') {
                            input = $("#tbRespuestaAbiertaAutomatica").val().toLowerCase();
                        }

                        if ($("#HfComparaAcentos").val() != 'True') {
                            input = accentFold(input);
                        }

                        if ($("#HfComparaSoloAlfanumerico").val() == 'True') {
                            input = input.replace(/\W/g, '');
                        }

                        return input;
                    }

                    var accentMap = {
                        'á': 'a',
                        'à': 'a',
                        'â': 'a',
                        'ã': 'a',
                        'ä': 'a',
                        'å': 'a',
                        'Á': 'A',
                        'À': 'A',
                        'Â': 'A',
                        'Ã': 'A',
                        'Ä': 'A',
                        'Å': 'A',

                        'ç': 'c',
                        'Ç': 'C',

                        'é': 'e',
                        'è': 'e',
                        'ê': 'e',
                        'ë': 'e',
                        'É': 'E',
                        'È': 'E',
                        'Ê': 'E',
                        'Ë': 'E',

                        'ì': 'i',
                        'í': 'i',
                        'î': 'i',
                        'ï': 'i',
                        'Ì': 'I',
                        'Í': 'I',
                        'Î': 'I',
                        'Ï': 'I',

                        'ó': 'o',
                        'ò': 'o',
                        'ô': 'o',
                        'õ': 'o',
                        'ö': 'o',
                        'Ó': 'O',
                        'Ò': 'O',
                        'Ô': 'O',
                        'Õ': 'O',
                        'Ö': 'O',

                        'ù': 'u',
                        'ú': 'u',
                        'û': 'u',
                        'ü': 'u',
                        'Ù': 'U',
                        'Ú': 'U',
                        'Û': 'U',
                        'Ü': 'U'
                    };

                    function accentFold(s) {
                        if (!s) { return ''; }
                        var ret = '';
                        for (var i = 0; i < s.length; i++) {
                            ret += accentMap[s.charAt(i)] || s.charAt(i);
                        }
                        return ret;
                    };
                </script>
              </td>
            </tr>
            <tr>
              <td>
                <div id="msgVerificaBien" style="display: none;">
                  <uc1:msgSuccess runat="server" ID="msgVerifiedGood" style="display: none;" />
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <div id="msgVerificaMal" style="display: none;">
                  <uc1:msgError runat="server" ID="msgVerifiedBad" style="display: none;" />
                </div>
              </td>
            </tr>
          </table>
        </asp:Panel>
            </td>
        </tr>
        <tr>
            <td class="style37" colspan="5"
                style="background-color: #666666; text-align: right;">
                <asp:Literal ID="lt_segunda_oportunidad" runat="server">[SEGUNDA OPORTUNIDAD]</asp:Literal>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSresp" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Respuesta]">
                    <InsertParameters>
                        <asp:Parameter Direction="Output" Name="NuevoId" Size="4" Type="Int16" />
                    </InsertParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                            <asp:HiddenField ID="HFidPlanteamiento" runat="server" />

            </td>
            <td>

                            <asp:HiddenField ID="HFidEvaluacion" runat="server" />

            </td>
            <td>

                            <asp:HiddenField ID="HFboton" runat="server" />

            </td>
        </tr>
        <tr>
            <td>

                            <asp:Timer ID="Timer1" runat="server" Enabled="False">
                            </asp:Timer>

            </td>
            <td>

                            <asp:HiddenField ID="HFplantel" runat="server" />

            </td>
            <td>

                <asp:HiddenField ID="HFidOpcion" runat="server" />

            </td>
            <td>

                <asp:HiddenField ID="HFcicloescolar" runat="server" />

            </td>
        </tr>
        <tr>
            <td>

                <asp:HiddenField ID="HFdetalleEvaluacion" runat="server" />

            </td>
            <td>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluacionesterminadas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [EvaluacionTerminada]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:HiddenField ID="HFidGrado" runat="server" />

            </td>
            <td>
                 <asp:HiddenField ID="HfVerificaRespuesta" ClientIDMode="Static" runat="server" />
            </td>
            <td>
                    <asp:HiddenField ID="HFidRespuestaAbierta" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
        <asp:HiddenField ID="HfComparaMayusculas" ClientIDMode="Static" runat="server" />
      </td>
        </tr>
    <tr>
      <td>
        <asp:HiddenField ID="HfComparaAcentos" ClientIDMode="Static" runat="server" />
      </td>
      <td>
        <asp:HiddenField ID="HfComparaSoloAlfanumerico" ClientIDMode="Static" runat="server" />
      </td>
         <td>
        <asp:HiddenField ID="HfPuntuacion" ClientIDMode="Static" runat="server" />
      </td>
    </tr>

    </table>
</asp:Content>

