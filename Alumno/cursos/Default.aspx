﻿    <%@ Page Title=""
    Language="VB"
    MasterPageFile="~/alumno/principal_alumno_5-1-0.master"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="alumno_cursos_Default" %>

<%--
    ***************************************************************************
                                      Cursos
    ***************************************************************************
--%>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/PageTitle_v1.ascx" TagPrefix="uc1" TagName="PageTitle_v1" %>
<%@ Register Src="~/Controls/msgWarning.ascx" TagPrefix="uc1" TagName="msgWarning" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .progressBar {
            display: inline-block; 
            
            width: 80px; 
            height: 14px; 
            margin-left: 80px;

            border: 1px solid #333;
	        background-color: #f3f3f3;  

            -moz-border-radius: 2px;
            -webkit-border-radius: 2px;
            border-radius: 2px;
        }

        .progressBarValue {
            height: 100%;

            background-color: #7E7E7E;
        }

        .resultadoAsignatura 
        {
            display: inline-block;
            margin-left: 90px;
            padding-top: 5px;
            padding-bottom: 5px;
            padding-left: 7px;
            padding-right: 7px;

            font-weight: bold;
            background: #c8c8c8;

            border-radius: 4px;
        }

        .pendingHeader {
        }

        .pendingTable {
            width: 100%;
        }

        .pendingActivitiesTable {
            font-size: small;
            width: 100%;
            border-collapse: collapse;
        }

        .AssignmentHeaderRow {
            font-size: small;
            white-space: nowrap;
            background-color: #ddeecc; /*ccddee*/
        }

        .AssignmentHeaderCell {
            border-right: 2px solid #333;
            /* TODO que las celdas no tengan separadores */
        }

        .AssignmentRow {
            /*white-space: nowrap;*/
            /*TODO resaltar, tentativo sobre un renglon nuevo*/
        }

        .AssignmentRowRed td a,
        .AssignmentRowRed td a:link,
        .AssignmentRowRed td a:visited,
        .AssignmentRowRed td a:active {
            color: #c41e00;
            font-weight: 700;
        }

        .AssignmentRowGreen td a,
        .AssignmentRowGreen td a:link,
        .AssignmentRowGreen td a:visited,
        .AssignmentRowGreen td a:active {
            color: #338033;
            font-weight: 700;
        }

        .centerText {
            text-align: center;
        }




        /* blue - 12759E */
        /*  */

        /*background-color: #EDF3F3;
            color: #63686B;

            border: 2px solid #ACB5B7;*/

        /*.pendingTableRow {
            max-width: 400px;
        }*/

        .CourseRow {
            cursor: pointer;
            font-size: small;
            padding: 7px 7px 7px 7px;
            color: #000;
            /*background: #E3E3E3;*/
            background-color: #eee;
            border: 1px solid #808080;
            border-bottom-width: 2px;
            border-right-width: 2px;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
            margin-bottom: 2px;
        }

        .CourseName {
            float: left;
            margin-left: 30px;
            width: 300px;
            max-width: 300px;
        }

        .CourseCalif {
            float: right;
            margin-right: 30px;
            width: 300px;
            max-width: 300px;
        }

        .AssignmentsContainer {
            /*width: 650px;*/
            padding-left: 50px;
        }

        .AssignmentRow td a,
        .AssignmentRow td a:link,
        .AssignmentRow td a:visited,
        .AssignmentRow td a:active {
            color: #12659E;
            font-weight: 700;
        }

        .assignmentTitle {
            max-width: 250px;
        }

        .padded {
            padding: 5px 5px 5px 5px;
        }

        .courseTitle, .califTitle {
            text-align: center;
            width: 300px;
            font-weight: bold;
            font-size: 0.9em;
        }

        .tituloTipoFecha {
            padding-bottom: 8px;
            border-bottom: 2px solid #008000; /* TODO */
            font-size: 0.8em;
            text-align: center;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            margin-bottom: 5px;
            margin-top: 15px;
        }
    </style>

    

    <style type="text/css">
        .periodList {
        }

        .sideMenu {
            display: block;
            list-style: none;
            padding: 0;
            margin: 0;
        }

        .sideMenu_asignatura {
  
            margin: 0px;
        }

        .sideMenu_calificacion {
            padding-left: 30px;
            margin: 0px;
        }

        .sideMenu_button {
            padding: 10px 10px 10px 10px;
            background: #ddddd9;
            border-bottom: 2px solid #c0c0bd;
            cursor: pointer;
        }

            .sideMenu_button:hover {
                background: #ddddd9;
            }
        /* ---------------------------------------------------- */
        .messageDiv {
            /*margin-top: 25px;*/
            display: inline-block;
            min-width: 280px;
            max-width: 400px;
        }

        .message {
            font-size: 1.1em;
            display: inline-block;
            float: left;
            position: relative;
            clear: both;
            margin-top: 15px;
            font-weight: 500;
            color: /*1*/ #000000;
            font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
        }

        .msgTitle {
            width: 100%;
            border-bottom: solid;
            border-bottom-width: 2px;
            border-bottom-color: #888;
            font-size: 1.1em;
            font-weight: 800;
        }

        .nombreUsuario {
            font-weight: bold;
        }
    </style>

    <%-- Aquí imprimo los estilos variables de ésta página específica --%>
    <asp:Literal ID="ltEstilos" runat="server" />

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Contenedor" runat="Server">


    <div class="col-lg-4 col-md-5 col-xs-12 col-sm-6" style="padding-left: 0px;">
    <div class="collapse navbar-collapse" id="menuAlumno"  style="padding-left: 0px;">

            <div style="padding-bottom: 30px; padding-right: 6px; border-right: 3px solid #888;">
                <ul class="sideMenu" style="">
                    <asp:Literal ID="lt_courseList" runat="server" />
                </ul>
            </div>
    </div>

        </div>
    <div class="col-lg-8 col-md-7 col-xs-12 col-sm-6">
     
        <div class="row">
            <div class="table-responsive">
            <table class="table">
                <tr>
                    <td style="text-align:center;">
                            <asp:Image ID="Image1" runat="server"
                                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/to_do_list_cheked_all.png"
                                        CssClass="menuBarIcon" Width="24" />

                                    <asp:Label ID="lt_bienvenida" style="font-size:1.2em;" runat="server">[Listado de actividades para ]</asp:Label>
                                    
                        <label>
                                <asp:Label ID="lbl_nombre_usuario" style="font-weight:bold;font-size:1.3em;" runat="server" Text="lbl_nombre_usuario"  />
                        </label>
                       
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Literal ID="lt_activityList" runat="server" />
                    </td>
                </tr>
            </table>
                </div>
            </div>
    </div>



    <div class="col-lg-8 col-md-6 col-xs-6" style="padding-left: 0px">


        <uc1:msgError runat="server" ID="msgError" />

        <uc1:msgWarning runat="server" ID="msgWarning" />

        <uc1:msgInfo runat="server" ID="msgInfo" />

        <uc1:msgSuccess runat="server" ID="msgSuccess" />


        <div class="col-lg-8 col-lg-offset-4">


        
        <table id="messageLine">
            <tr>
                <td style="text-align: center;">
                    <table style="width: 100%;">
                        <tr>
                            <td style="padding-left: 100px; text-align: left;"></td>
                        </tr>
                        <tr>
                            <td>
                                <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="messageDiv">
                                    <asp:Label ID="LblMensaje1" runat="server" CssClass="message"></asp:Label>
                                    <asp:Label ID="LblMensaje2" runat="server" CssClass="message"></asp:Label>
                                    <asp:Label ID="LblMensaje3" runat="server" CssClass="message"></asp:Label>
                                    <asp:Label ID="LblMensaje4" runat="server" CssClass="message"></asp:Label>
                                    <asp:Label ID="LblMensaje5" runat="server" CssClass="message"></asp:Label>
                                    <asp:Label ID="LblMensaje6" runat="server" CssClass="message"></asp:Label>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
            </div>


    </div>
 
    <table class="dataSources">
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSmensajesCoordinadores" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" SelectCommand="
select CA.IdComunicacionCA, CA.Fecha, CA.Asunto, C.Nombre + ' ' + C.Apellidos as 'Coordinador que Envía', CA.EstatusA, U.Login, P.Descripcion As Plantel
from ComunicacionCA CA, Coordinador C, Usuario U, Plantel P, CoordinadorPlantel CP, Alumno A
where CA.IdAlumno = @IdAlumno and CA.EstatusA &lt;&gt; 'Baja' and CA.Sentido = 'E'
and U.IdUsuario = C.IdUsuario and CA.IdCoordinador = C.IdCoordinador 
and CP.IdPlantel = P.IdPlantel and C.IdCoordinador = CP.IdCoordinador 
and A.IdAlumno = CA.IdAlumno AND A.IdPlantel = P.IdPlantel 
order by CA.Fecha DESC">
                    <SelectParameters>
                        <asp:SessionParameter Name="IdAlumno" SessionField="LeerMensajeEntradaA_IdAlumno" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>
                <asp:SqlDataSource ID="Query_Courses" runat="server"
                    SelectCommand="
SELECT * 
FROM (
SELECT DISTINCT
    A.Consecutivo AS 'ConsecutivoAsignatura',
	A.IdAsignatura,
	A.Descripcion AS 'Asignatura',
	Cal.IdCalificacion,
	Cal.Consecutivo,
	Cal.Descripcion AS 'Calificacion',

	E.IdEvaluacion, 
	E.ClaveBateria, 
	E.Tipo, 
	E.Penalizacion,
	E.SeEntregaDocto,

	CASE WHEN EvAl.InicioContestar IS NOT NULL
		THEN EvAl.InicioContestar
		ELSE 
			CASE WHEN EvPl.InicioContestar IS NOT NULL
				THEN EvPl.InicioContestar
				ELSE E.InicioContestar
			END
	END AS 'InicioContestar',
	
	CASE WHEN EvAl.FinSinPenalizacion IS NOT NULL
		THEN EvAl.FinSinPenalizacion
		ELSE 
			CASE WHEN EvPl.FinSinPenalizacion IS NOT NULL
				THEN EvPl.FinSinPenalizacion
				ELSE E.FinSinPenalizacion
			END
	END AS 'FinSinPenalizacion',
	
	CASE WHEN EvAl.FinContestar IS NOT NULL
		THEN EvAl.FinContestar
		ELSE 
			CASE WHEN EvPl.FinContestar IS NOT NULL
				THEN EvPl.FinContestar
				ELSE E.FinContestar
			END
	END AS 'FinContestar',
	
	CASE WHEN EvAl.FinContestar IS NOT NULL
		THEN '2' --alumno
		ELSE 
			CASE WHEN EvPl.FinContestar IS NOT NULL
				THEN '1' -- plantel
				ELSE '0' -- institucion
			END
	END AS 'Alcance',
	
	CASE WHEN EvAl.Estatus IS NOT NULL
		THEN EvAl.Estatus
		ELSE 
			CASE WHEN EvPl.Estatus IS NOT NULL
				THEN EvPl.Estatus
				ELSE E.Estatus
			END
	END AS 'Estatus',

	CASE WHEN Borradores.IdEvaluacion IS NULL
		THEN 0
		ELSE 1
	END AS 'Borrador',

    ET.FechaTermino,

	ET.Resultado,
	Docto.Documento AS 'Entrega'
FROM
	Calificacion Cal,
	Evaluacion E
	LEFT JOIN EvaluacionTerminada ET
		ON ET.IdEvaluacion = E.IdEvaluacion
		AND ET.IdAlumno = @IdAlumno
	LEFT JOIN EvaluacionAlumno EvAl
		ON EvAl.IdEvaluacion = E.IdEvaluacion 
		AND EvAl.IdAlumno = @IdAlumno
	LEFT JOIN EvaluacionPlantel EvPl
		ON EvPl.IdEvaluacion = E.IdEvaluacion 
		And EvPl.IdPlantel = @IdPlantel
	LEFT JOIN DoctoEvaluacion Docto
		ON Docto.IdEvaluacion = E.IdEvaluacion 
		AND Docto.IdAlumno = @IdAlumno
	LEFT JOIN 
		(SELECT 
			DetalleEvaluacion.IdEvaluacion 
		FROM 
			DetalleEvaluacion, 
			(SELECT 
				IdDetalleEvaluacion, 
				IdAlumno 
			FROM 
				(SELECT 
					* 
				FROM 
					Respuesta 
				WHERE 
					IdAlumno = @IdAlumno)
				As Respuesta, 
				(SELECT 
					* 
				FROM 
					RespuestaAbierta 
				WHERE 
					Estado = 'Borrador') 
				As Abiertas 
			WHERE 
				Respuesta.IdRespuesta = Abiertas.IdRespuesta 
				AND Respuesta.IdAlumno = @IdAlumno) 
			As Respuestas 
		WHERE 
			Respuestas.IdDetalleEvaluacion = DetalleEvaluacion.IdDetalleEvaluacion) Borradores
		ON Borradores.IdEvaluacion = E.IdEvaluacion,
	DetalleEvaluacion D,
	Subtema ST,
	Tema T,
	Asignatura A,
	Cicloescolar CE
WHERE
	A.IdGrado = @IdGrado  
	and T.IdAsignatura = A.IdAsignatura 
	and ST.IdTema = T.IdTema 
	and D.IdSubtema = ST.IdSubtema 
	and E.IdEvaluacion = D.IdEvaluacion 
	and Cal.IdCalificacion = E.IdCalificacion 
	and CE.IdCicloEscolar = Cal.IdCicloEscolar 
	and CE.Estatus = 'Activo' 
	and CE.IdCicloEscolar = @IdCicloEscolar 
) A
WHERE
    Estatus &lt;&gt; 'Terminada' 
	and Estatus &lt;&gt; 'Cancelada'
ORDER BY
    CASE WHEN ConsecutivoAsignatura IS NULL THEN 1 ELSE 0 END,
    ConsecutivoAsignatura, 
    Asignatura, 
    CASE WHEN Consecutivo IS NULL THEN 1 ELSE 0 END,
    Consecutivo, 
    Calificacion, 
    InicioContestar,
    ClaveBateria
                    "></asp:SqlDataSource>
            </td>
        </tr>
    </table>



    <script>
        $(function () {

            if (localStorage.getItem("Cal")) {

                $('#messageLine').hide();
                $(localStorage.getItem("Cal")).show();
                $(localStorage.getItem("ColBtnCal")).find(
                    '.sideMenu_button').css('background-color','#ddddd9');
                $(localStorage.getItem("Act")).show();
                $(localStorage.getItem("ColBtnAct")).find('.menuBarIcon').css('display', 'inline-block');
                $('html, body').animate({ scrollTop: 0 }, 'fast');
            }
        });
    </script>

    <script>

        function showOptions(target, sender) {
            if ($(target).css('display') != 'none') {
                $(target).hide(300);
                $(sender).css('background-color', '');
                $(sender).css('color', 'black');
                $(sender).css('font-weight', '');
            }
            else {
                $(target).show(300);
                $(sender).css('background-color', '#338033'); /* TODO 3CB371*/
                $(sender).css('color', '#fff');
                $(sender).css('font-weight', 'bold');
            }
        }
    </script>
    <script type='text/javascript'>
        function showCalificaciones(target, sender) {



            if (typeof (Storage) != "undefined") {
                localStorage.setItem("Cal", target);
                localStorage.setItem("ColBtnCal", sender);

            } else {
                alert('Sorry! no web storage support');
            }

            // si el objetivo ya está mostrado, significa que se hizo click en el mismo botón
            if ($(target).css('display') != 'none') {
                // primero muestro los mensajes si están ocultos, luego presento las actividades
                if ($('#messageLine').css('display') == 'none')
                    // oculta cualquier lista de actividades  y luego muestra los avisos
                    $.when(
                        $('.AssignmentsContainer').hide(250)
                    ).done(function () {
                        $('#messageLine').show(250);
                    });


                // oculta las calificaciones de la asignatura seleccionada
                $(target).hide(250);
                // y deselecciona cualquier calificacion que peuda estar maximizada
                $('.sideMenu_calificacion').css('border-right', '');
                $('.sideMenu_calificacion').find('.menuBarIcon').css('display', 'none'); /* TODO 3CB371*/

                // regresa el color de los botones de asignaturas a gris claro
                $('.sideMenu_asignatura').find('.sideMenu_button').css('background-color', '#ddddd9');
            }
                // si el objetivo no está mostrado, se hizo click en otro botón de asignatura nuevo
            else {
                // oculta cualquier otra lista de calificaciones de otra asignatura
                $('.periodList').not(target).hide(250);

                if ($('#messageLine').css('display') == 'none')
                    // oculta cualquier lista de actividades  y luego muestra los avisos
                    $.when(
                        $('.AssignmentsContainer').hide(250)
                    ).done(function () {
                        $('#messageLine').show(250);
                    });


                // regresa el color de los botones de asignaturas (por si hay uno seleccionado) a gris claro
                $('.sideMenu_asignatura').not(sender).find('.sideMenu_button').css('background-color', '#ddddd9');


                // pinta el color de fondo de la asignatura seleccionada gris más oscuro
                $(sender).find('.sideMenu_button').css('background', '#edede9'); /* TODO 3CB371*/

                // muestro el contenedor de calificaciones de la asignatura seleccionada
                $(target).show(250, function () { // Y cuando termine la animación:
                    if ($(window).scrollTop() != 0) // si no se está en el inicio de la página
                        $('html,body').animate({ // desplazarse al boton de la asignatura seleccionada
                            scrollTop: $(sender).offset().top
                        });
                });
            }
        }

        function showActividades(target, sender) {

            if (typeof (Storage) != "undefined") {
                localStorage.setItem("Act", target);
                localStorage.setItem("ColBtnAct", sender);
            } else {
                alert('Sorry! no web storage support');
            }

            // primero oculto los mensajes si están desplegados, luego presento las actividades
            if ($('#messageLine').css('display') != 'none')
                $('#messageLine').hide(250, function () { complete_showActividades(target, sender) });
            else
                complete_showActividades(target, sender);

        }

        function complete_showActividades(target, sender) {
            $(sender).find('.menuBarIcon').css("padding-right","5px")
            $(sender).find('.menuBarIcon').css('display', 'inline-block'); /* TODO 3CB371*/

            $(sender).find('.sideMenu_button').css("background", "white")
            $(sender).not(sender).find('.sideMenu_button').css("background", "#ddddd9");

            $('.sideMenu_calificacion').not(sender).find('.sideMenu_button').css("background", "#ddddd9");

            $('.sideMenu_calificacion').not(sender).find('.menuBarIcon').css('display', 'none'); /* TODO 3CB371*/

            $(sender).css('border-right', '4px solid <%= Siget.Config.Color.MenuBar_SelectedOption_BorderColor%>'); /* TODO 3CB371*/
            $('.sideMenu_calificacion').not(sender).css('border-right', '');

            // oculta cualquier otra lista de actividades de otra calificación
            $('.AssignmentsContainer').not(target).hide(250);

            // muestra la lsita de actividades de la calificación seleccionada
            $(target).show(250);

            // mueve el scroll de la página hasta la cima para ver la tabla de actividades
            $('html, body').animate({ scrollTop: 0 }, 'fast');
            //$('.sideMenu_calificacion').not(sender).css('font-weight', '');
            //$(sender).css('font-weight', 'bold');
        }
    </script>
</asp:Content>

