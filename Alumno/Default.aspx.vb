﻿Imports Siget


Partial Class alumno_Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load


        If Session("Usuario_Estatus") <> "Registrado" Then

            If (Siget.Utils.Sesion.verificaSiEsPrimeraVez(User.Identity.Name) And Siget.Config.Global.CONTRA_OBLIGATORIA) Then
                Response.Redirect("misdatos/cambiarpassword")
            Else
                Response.Redirect("cursos/")
            End If
        Else
            Siget.Utils.Sesion.CierraSesion()
        End If
    End Sub

End Class
