﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/alumno/principal_alumno_5-1-0.master"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="alumno_misdatos_Defalt" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/PageTitle_v1.ascx" TagPrefix="uc1" TagName="PageTitle_v1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <%-- Aquí imprimo los estilos variables de ésta página específica --%>
    <asp:Literal ID="ltEstilos" runat="server" />

    <style type="text/css">
        .rightCol {
            text-align: left;
            padding-bottom: 5px;
        }
        .leftCol {
            text-align: right;
        }
    </style>
     <link type="text/css" rel="stylesheet" href="<%=Siget.Config.Global.urlCss%>Badge.css" />
    <script type="text/javascript">
        $(function () {

        
            $('#temporalImageProfile').css('background-image','url("../../Resources/profileImages/<%=Session("urlProfileImage")%>")');
          
            $('#profileImage').on('change', function (event) {
                $('#temporalImageProfile').css('background-image', "url("+URL.createObjectURL(event.target.files[0])+")");
            });
        });

     
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table style="margin-left: auto; margin-right: auto;">
        
        <tr>
            <td colspan="2">
                <uc1:PageTitle_v1 runat="server" ID="PageTitle_v1" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                <span style="font-size: 0.8em;">
                    <asp:Literal ID="lt_passChangeText" runat="server">[Si desea cambiar su contraseña, de click]</asp:Literal>
					<asp:HyperLink ID="HyperLink1" runat="server"
                        NavigateUrl="~/alumno/misdatos/cambiarpassword/"
                        Style="color: #045B6E; font-weight: 700"> <asp:Literal ID="lt_hl_passChange" runat="server">[Aquí]</asp:Literal></asp:HyperLink>
                </span>
            </td>
        </tr>
        <tr class="titulo">
            <td class="style23" colspan="2"></td>
        </tr>
        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="txtIdioma" runat="server" Text="Idioma" />
            </td>
            <td class="rightCol">
                <asp:DropDownList ID="Idioma" runat="server" 
                    Width="145px"
                    AutoPostBack="true"
                    CssClass="wideTextbox">
                    <asp:ListItem Value="es-mx">Español (MX)</asp:ListItem>
                    <asp:ListItem Value="en-us">English (US)</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_grupo" runat="server" Text="[GRUPO]" />
            </td>
            <td class="rightCol" style="font-weight: bold;">
                <asp:Label ID="LblGrupo" runat="server"></asp:Label>
            </td>
        </tr>
        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_matricula" runat="server" Text="[Matricula]" />
            </td>
            <td class="rightCol" style="font-weight: bold;">
                <asp:Label ID="lblMatricula" runat="server"></asp:Label>
            </td>
              <td>
                  <div id="temporalImageProfile"  style="border: 5px solid black; position: absolute; width: 150px;height:150px;text-align:center;background-size:cover;background-repeat:no-repeat;">                        
                  </div>
                
              </td>
        </tr>
        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_nombre" runat="server" 
                    Text="[Nombre]" />
            </td>
            <td class="rightCol">
                <asp:TextBox ID="TBnombre" runat="server" 
                    width="300"
                    MaxLength="40"
                    CssClass="wideTextbox"></asp:TextBox>
            </td>
           
        </tr>
        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_appat" runat="server" 
                    Text="[Apellido paterno]" />
            </td>
            <td class="rightCol">
                <asp:TextBox ID="TBApePaterno" runat="server" 
                    Width="200"
                    MaxLength="30"
                    CssClass="wideTextbox"></asp:TextBox>
            </td>
        </tr>
        <tr class="estandar">
             <td class="leftCol">
                <asp:Literal ID="Literal1" runat="server" 
                    Text="Imagen de perfil" />
            </td>
            <td class="rightCol">

               <asp:AsyncFileUpload ClientIDMode="Static" runat="server"
                    ID="profileImage" />
            </td>
            
        </tr>
   
        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_apmat" runat="server" Text="[Apellido materno]" />
            </td>
            <td class="rightCol">
                <asp:TextBox ID="TBApeMaterno" runat="server" 
                    Width="200"
                    MaxLength="30"
                    CssClass="wideTextbox"></asp:TextBox>
            </td>
        </tr>
        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_equipo" runat="server" 
                    Text="[Equipo]" />
            </td>
            <td class="rightCol">
                <asp:DropDownList ID="DDLequipo" runat="server" 
                    Width="145px"
                    CssClass="wideTextbox">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_subequipo" runat="server" 
                    Text="[Subequipo]" />
            </td>
            <td class="rightCol">
                <asp:DropDownList ID="DDLsubequipo" runat="server" 
                    Width="145px"
                    CssClass="wideTextbox">
                </asp:DropDownList>
            </td>
        </tr>
        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_email_principal" runat="server" 
                    Text="[Correo Electrónico Principal]" />
            </td>
            <td class="rightCol">
                <asp:TextBox ID="TBemail_1" runat="server" 
                    MaxLength="100"
                    Width="200px"
                    CssClass="wideTextbox"></asp:TextBox>
                <asp:Image ID="imgInfoEmail1" runat="server"
                    Title="El e-mail que más utilices."
                    ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                    CssClass="infoPopper"
                    style="padding-left: 5px;" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_email_secundario" runat="server" 
                    Text="[Correo Electrónico Secundario]" />
            </td>
            <td class="rightCol">
                <asp:TextBox ID="TBemail_2" runat="server" 
                    MaxLength="100"
                    Width="200px"
                    CssClass="wideTextbox"></asp:TextBox>
                <asp:Image ID="imgInfoEmail2" runat="server"
                    Title="Un e-mail de respaldo."
                    ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons16x16/information.png"
                    CssClass="infoPopper"
                    style="padding-left: 5px;" />
            </td>
        </tr>
        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_usuario_facebook" runat="server" 
                    Text="[Usuario de Facebook]" />
                <asp:Image ID="Image1" runat="server"
                    ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/facebook.png"
                    CssClass="menuBarIcon" />
            </td>
            <td class="rightCol">
                <asp:TextBox ID="tbFacebook" runat="server" 
                    MaxLength="100"
                    Width="200px"
                    CssClass="wideTextbox"></asp:TextBox>
            </td>
        </tr>
        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_usuario_twitter" runat="server" 
                    Text="[Usuario de Twitter]" />
                <asp:Image ID="Image2" runat="server"
                    ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/twitter_1.png"
                    CssClass="menuBarIcon" />
            </td>
            <td class="rightCol">
                <asp:TextBox ID="tbTwitter" runat="server" 
                    MaxLength="100"
                    Width="200px"
                    CssClass="wideTextbox"></asp:TextBox>
            </td>
        </tr>

        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_sexo" runat="server" Text="Sexo" />
            </td>
            <td class="rightCol">
                <asp:RequiredFieldValidator ID="RFVSexo" runat="server" Display="Dynamic" ControlToValidate="DDLsexo" InitialValue="Null">*</asp:RequiredFieldValidator>
                <asp:DropDownList ID="DDLsexo" runat="server" 
                    Width="170px"
                    AutoPostBack="true"
                    CssClass="wideTextbox">
                    <asp:ListItem Value="Null" ID="lt_seleccioneSuSexo">[Seleccione su sexo]</asp:ListItem>
                    <asp:ListItem Value="F" ID="lt_femenino">[Femenino]</asp:ListItem>
                    <asp:ListItem Value="M" ID="lt_masculino">[Masculino]</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>

        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_curp" runat="server" Text="[CURP]" />
            </td>
            <td class="rightCol">
                <asp:RequiredFieldValidator ID="RFVcurp" runat="server" Display="Dynamic" ControlToValidate="TBcurp">*</asp:RequiredFieldValidator>
                <asp:TextBox ID="TBcurp" runat="server" 
                    width="200"
                    MaxLength="40"
                    CssClass="wideTextbox"></asp:TextBox>
            </td>
        </tr>
        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_puesto" runat="server" Text="[Puesto]" />
            </td>
            <td class="rightCol">
                <asp:RequiredFieldValidator ID="RFVpuesto" runat="server" Display="Dynamic" ControlToValidate="TBpuesto">*</asp:RequiredFieldValidator>
                <asp:TextBox ID="TBpuesto" runat="server" 
                    width="200"
                    MaxLength="40"
                    CssClass="wideTextbox"></asp:TextBox>
               
            </td>
        </tr>
        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_discapacidad" runat="server" Text="[Discapacidad]" />
                 <asp:RequiredFieldValidator ID="RFVdiscapacidad" runat="server" Display="Dynamic" ControlToValidate="RBLdiscapacidad">*</asp:RequiredFieldValidator>
            </td>
            <td class="rightCol">
                
                <asp:RadioButtonList ID="RBLdiscapacidad" runat="server" OnSelectedIndexChanged="OnSelectedIndexChanged_RBLdiscapacidad" AutoPostBack="true">
                    <asp:ListItem Value="Si" ID="lt_si">[Sí]</asp:ListItem>
                    <asp:ListItem Value="No" ID="lt_no">[NO]</asp:ListItem>
                </asp:RadioButtonList>
                
            </td>
        </tr>

        <tr class="estandar">
            <td class="leftCol">
                
            </td>
            <td class="rightCol">
                        <asp:RequiredFieldValidator ID="RFVdiscapacidad_especifico" runat="server" Display="Dynamic" ControlToValidate="TBdiscapacidad" >*</asp:RequiredFieldValidator>
                        <asp:TextBox ID="TBdiscapacidad" runat="server" 
                    Width="200px" Visible="false"
                    CssClass="wideTextbox"></asp:TextBox>
            </td>
        </tr>
        <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_estado_civil" runat="server" Text="[Estado Civil]" />
            </td>
            <td class="rightCol">
                <asp:RequiredFieldValidator ID="RFVestado_civil" runat="server" Display="Dynamic" ControlToValidate="TBestado_civil">*</asp:RequiredFieldValidator>
                <asp:TextBox ID="TBestado_civil" runat="server" 
                    width="200"
                    MaxLength="40"
                    CssClass="wideTextbox"></asp:TextBox>
            </td>
        </tr>
                <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_nivel_estudios" runat="server" Text="[Nivel de Estudios]" />
            </td>
            <td class="rightCol">
                <asp:RequiredFieldValidator ID="RFVnivel_estudios" runat="server" Display="Dynamic" ControlToValidate="TBnivel_estudios">*</asp:RequiredFieldValidator>
                <asp:TextBox ID="TBnivel_estudios" runat="server" 
                    width="200"
                    MaxLength="40"
                    CssClass="wideTextbox"></asp:TextBox>
            </td>
        </tr>

                <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_titulo_estudios" runat="server" Text="[Titulo Obtenido]" />
            </td>
            <td class="rightCol">
                <asp:RequiredFieldValidator ID="RFVtitulo_estudios" runat="server" Display="Dynamic" ControlToValidate="TBtitulo_estudios">*</asp:RequiredFieldValidator>
                <asp:TextBox ID="TBtitulo_estudios" runat="server" 
                    width="200"
                    MaxLength="40"
                    CssClass="wideTextbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center; padding-top: 8px; padding-bottom: 40px;">
                <asp:LinkButton ID="BtnActualizar" runat="server" 
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" > <asp:Image ID="Image3" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/update_contact_info.png"
                        CssClass="btnIcon"
                        height="24" />
                    <asp:Literal ID="lt_btn_Actualizar" runat="server">[Actualizar]</asp:Literal></asp:LinkButton>
                 
            </td>
            
        </tr>
        <tr>
            <td colspan="2">
                <uc1:PageTitle_v1 runat="server" ID="PageTitle_v2" />
            </td>
        </tr>
         <tr class="estandar">
            <td class="leftCol">
                <asp:Literal ID="lt_btn_Imprimir_Certificado" runat="server" Text="[Selecciona Certificado]" />
            </td>
            <td class="rightCol">
                <asp:DropDownList ID="DDL_Certificado" runat="server" 
                    Width="210px"
                    AutoPostBack="true" AppendDataBoundItems="true"
                    CssClass="wideTextbox">  
                    <asp:ListItem Value="0">--SELECCIONA UNA ASIGNATURA--</asp:ListItem>
                </asp:DropDownList>
                 <asp:LinkButton ID="BtnCertificado" runat="server" 
                    CssClass="defaultBtn  btnThemeSmall defaultBtn-wb-wback" > <asp:Image ID="Image4" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/printer.png"
                        CssClass="btnIcon"
                        height="24" />
                 </asp:LinkButton>
                
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:msgSuccess runat="server" ID="msgSuccess" />
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSalumnos" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Alumno]"></asp:SqlDataSource>
            </td>
        </tr>
    </table>

        <table class="dataSources">
        <tr>
            <td>
                <asp:SqlDataSource ID="SDSalumnosDatosAdicionales" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [AlumnoDatosAdicionales]"></asp:SqlDataSource>
            </td>
        </tr>
    </table>

    <asp:Panel ID="Wrapper_Certificado" runat="server" >
        <div id="certificado" style="display:none;">
            <div id="badge-main-container" >

                <div class="badge-header">
                    <div class="badge-name">
                        <img class="badge-medal" src="<%=Siget.Config.Global.urlImagenes %>medal2.png" />
                        <div class="badge-center container-lg">
                            <label class="badge-label-lg">Otorgado a:</label>
                        </div>
                        <label id="Nombre_Alumno" runat="server"  class="badge-name-classmate"></label>
                    </div>

                </div>
                <div class="badge-body">
                    <div class="badge-info">
                        <label class="badge-header1">por haber superado todos los módulos del curso </label>
                        <p id="Nombre_Asignatura" class="badge-course" runat="server"></p>
                        <label class="badge-header1">con un equivalente a <label id="Horas" runat="server"></label> Horas </label>
                        <div>
                            <img class="vector-flow" src="<%=Siget.Config.Global.urlImagenes %>vector.png" />
                        </div>

                        <div class="logos">
                            <div class="container-space-control">
                                <div class="half-size-logo loleft">
                                    <label class="badge-label-lg fix-black">Con la colaboración de :</label>
                                    <img class="badge-img-common " src="<%=Siget.Config.Global.rutaCustomization %>clientLogo.png" />
                                </div>
                            </div>
                            <div class="container-space-control">
                                <div class="half-size-logo loright fix-footer-width ">
                                    <img class="badge-img-common-platform" src="<%=Siget.Config.Global.rutaCustomization %>integrant.png" />
                                    <label class="badge-label-lg-1x fix-black">
                                        Plataforma de aprendizaje en línea</label>
                                    <a href="http://www.capacitacionweb.mx" class="badge-label-lg-1x fix-black" style="clear: both;">www.capacitacionweb.mx</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="badge-footer">
                        <label class="badge-label-center">
                            Este diploma acredita que su poseedor ha superado con éxito todos 
                            los módulos de los que consta el curso 
                            <label id="Nombre_Curso_footer" runat="server"></label> , el cual completó el día: <label id="Last_Date" runat="server"></label>.
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts %>jspdf/jspdf.source.js"></script>
    <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts %>jspdf/jspdf.plugin.addimage.js"></script>
    <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts %>jspdf/jspdf.plugin.addimage.js"></script>
    <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts %>jspdf/jspdf.plugin.cell.js"></script>
    <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts %>jspdf/jspdf.plugin.from_html.js"></script>
    <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts %>jspdf/jspdf.plugin.ie_below_9_shim.js"></script>
    <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts %>jspdf/jspdf.plugin.javascript.js"></script>
    <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts %>jspdf/jspdf.plugin.sillysvgrenderer.js"></script>
    <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts %>jspdf/jspdf.plugin.split_text_to_size.js"></script>
    <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts %>jspdf/jspdf.plugin.standard_fonts_metrics.js"></script>
    <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts %>jspdf/jspdf.PLUGINTEMPLATE.js"></script>
    <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts %>html2canvas.js"></script>
    <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts %>html2canvas.svg.js"></script>
    <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts %>jquery-1.11.1.min.js"></script>



</asp:Content>

