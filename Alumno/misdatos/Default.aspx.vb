﻿Imports Siget
Imports Siget.DataAccess

Imports System.Data.SqlClient
Imports System.Data
Imports System.IO

Partial Class alumno_misdatos_Defalt
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        ' esto debe ir antes que cualquier operación que utilice base de datos
        If Utils.Bloqueo.sistemaBloqueado() Then
            Utils.Sesion.CierraSesion()
            Response.Redirect("~/EnMantenimiento.aspx")
        End If

        If Not IsNothing(Session("SuccessMessage")) Then
            msgSuccess.show(Session("SuccessMessage"))
            Session.Remove("SuccessMessage")
        End If

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then

            'Oculto el botón de certificado si esta en modelo de Sistema=1
            If Config.Global.Modelo_Sistema = 0 Then
                BtnCertificado.Visible = False
                lt_btn_Imprimir_Certificado.Visible = False
                DDL_Certificado.Visible = False
                PageTitle_v2.Visible = False
            End If


            Session("urlProfileImage") = Utils.FileUtils.getUrlProfileImage(MapPath("../../Resources/profileImages/"), User.Identity.Name)

            ' limpio las variables de la sesión de contestado si están definidas
            Utils.Sesion.LimpiaSesionContestado()

            aplicaLenguaje()

            initPageData()

            OutputCss()

            Get_Asignatura_Certificadas()
        End If
    End Sub


    Protected Sub Get_Asignatura_Certificadas()

        Dim asignaturaRepository As IRepository(Of Siget.Entity.Asignatura) = New AsignaturaRepository()

        Try

            Using conn As New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)

                conn.Open()

                Dim reader As SqlDataReader
                Dim cmd As SqlCommand = New SqlCommand("VerificaCertificado", conn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@IdAlumno", Session("Usuario_IdAlumno"))
                cmd.Parameters.AddWithValue("@IdPlantel", Session("Usuario_IdPlantel"))

                reader = cmd.ExecuteReader()

                While (reader.Read() = True)
                    Dim asignatura As Siget.Entity.Asignatura = asignaturaRepository.FindById(reader("IdAsignatura"))
                    Dim lt As ListItem = New ListItem()
                    lt.Value = asignatura.IdAsignatura
                    lt.Text = asignatura.Descripcion
                    DDL_Certificado.Items.Add(lt)

                    If Not reader("Enviado") Then

                        If (Session("Usuario_Email") <> Nothing) And Config.Global.Modelo_Sistema = 1 And Not asignatura.Enviado Then
                            Siget.Utils.Correo.EnviaCorreo(Session("Usuario_Email"),
                                                                 Siget.Config.Global.NOMBRE_FILESYSTEM + " - Mensaje en " + Siget.Config.Etiqueta.SISTEMA_CORTO,
                                                                 " Ya puede imprimir el certificado de la <b>ASIGNATURA<b>:<h1> " + asignatura.Descripcion.ToString().ToUpper() + "</h1>.&nbsp" +
                                                                 " Encontrará el área para imprimirlo en la parte de abajo de ""MIS DATOS"".",
                                                                 True)

                            asignatura.Enviado = True
                            asignaturaRepository.Update(asignatura)
                        End If

                    End If


                End While
                DDL_Certificado.DataBind()

            End Using

        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex) ' registra el error
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try



    End Sub
    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = Lang.FileSystem.Alumno.MisDatos.LT_TITULO.Item(Session("Usuario_Idioma"))
        PageTitle_v1.show(Lang.FileSystem.Alumno.MisDatos.LT_TITULO_PAGINA.Item(Session("Usuario_Idioma")),
                      Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/vcard_edit.png")
        PageTitle_v2.show(Lang.FileSystem.Alumno.MisDatos.LT_TITULO_CERTIFICADO.Item(Session("Usuario_Idioma")),
                      Config.Global.urlImagenes & "btnIcons/fatcow-hosting-icons-3800/FatCow_Icons32x32/medal_award_gold.png")

        lt_passChangeText.Text = Lang.FileSystem.Alumno.MisDatos.LT_PASSWORD_CHANGE_TEXT.Item(Session("Usuario_Idioma"))
        lt_hl_passChange.Text = Lang.FileSystem.Alumno.MisDatos.LT_HERE_PASSWORD_CHANGE.Item(Session("Usuario_Idioma"))

        txtIdioma.Text = Lang.FileSystem.Alumno.MisDatos.TxtIdioma.Item(Session("Usuario_Idioma"))
        lt_grupo.Text = Lang.FileSystem.Alumno.MisDatos.LT_GRUPO.Item(Session("Usuario_Idioma"))
        lt_matricula.Text = Lang.FileSystem.Alumno.MisDatos.LT_MATRICULA.Item(Session("Usuario_Idioma"))
        lt_nombre.Text = Lang.FileSystem.Alumno.MisDatos.LT_NOMBRE.Item(Session("Usuario_Idioma"))
        TBnombre.Attributes.Add("placeholder", Lang.FileSystem.Alumno.MisDatos.LT_NOMBRE.Item(Session("Usuario_Idioma")))
        lt_appat.Text = Lang.FileSystem.Alumno.MisDatos.LT_APPAT.Item(Session("Usuario_Idioma"))
        TBApePaterno.Attributes.Add("placeholder", Lang.FileSystem.Alumno.MisDatos.LT_APPAT.Item(Session("Usuario_Idioma")))
        lt_apmat.Text = Lang.FileSystem.Alumno.MisDatos.LT_APMAT.Item(Session("Usuario_Idioma"))
        TBApeMaterno.Attributes.Add("placeholder", Lang.FileSystem.Alumno.MisDatos.LT_APMAT.Item(Session("Usuario_Idioma")))
        lt_equipo.Text = Lang.FileSystem.Alumno.MisDatos.LT_EQUIPO.Item(Session("Usuario_Idioma"))
        lt_subequipo.Text = Lang.FileSystem.Alumno.MisDatos.LT_SUBEQUIPO.Item(Session("Usuario_Idioma"))
        lt_email_principal.Text = Lang.FileSystem.Alumno.MisDatos.LT_EMAIL.Item(Session("Usuario_Idioma"))
        TBemail_1.Attributes.Add("placeholder", Lang.FileSystem.Alumno.MisDatos.LT_EMAIL.Item(Session("Usuario_Idioma")))
        imgInfoEmail1.Attributes.Add("title", Lang.FileSystem.Alumno.MisDatos.hint_infoEmail_1.Item(Session("Usuario_Idioma")))
        lt_email_secundario.Text = Lang.FileSystem.Alumno.MisDatos.lt_email_2.Item(Session("Usuario_Idioma"))
        TBemail_2.Attributes.Add("placeholder", Lang.FileSystem.Alumno.MisDatos.lt_email_2.Item(Session("Usuario_Idioma")))
        imgInfoEmail2.Attributes.Add("title", Lang.FileSystem.Alumno.MisDatos.hint_infoEmail_2.Item(Session("Usuario_Idioma")))
        lt_usuario_facebook.Text = Lang.FileSystem.Alumno.MisDatos.facebook.Item(Session("Usuario_Idioma"))
        tbFacebook.Attributes.Add("placeholder", Lang.FileSystem.Alumno.MisDatos.facebook.Item(Session("Usuario_Idioma")))
        lt_usuario_twitter.Text = Lang.FileSystem.Alumno.MisDatos.twitter.Item(Session("Usuario_Idioma"))
        tbTwitter.Attributes.Add("placeholder", Lang.FileSystem.Alumno.MisDatos.twitter.Item(Session("Usuario_Idioma")))
        lt_sexo.Text = Lang.FileSystem.Alumno.MisDatos.LT_SEXO.Item(Session("Usuario_Idioma"))
        lt_seleccioneSuSexo.Text = Lang.FileSystem.Alumno.MisDatos.LT_SELECCIONE_SU_SEXO.Item(Session("Usuario_Idioma"))
        lt_femenino.Text = Lang.FileSystem.Alumno.MisDatos.LT_FEMENINO.Item(Session("Usuario_Idioma"))
        lt_masculino.Text = Lang.FileSystem.Alumno.MisDatos.LT_MASCULINO.Item(Session("Usuario_Idioma"))
        lt_curp.Text = Lang.FileSystem.Alumno.MisDatos.LT_CURP.Item(Session("Usuario_Idioma"))
        lt_puesto.Text = Lang.FileSystem.Alumno.MisDatos.LT_PUESTO.Item(Session("Usuario_Idioma"))
        lt_discapacidad.Text = Lang.FileSystem.Alumno.MisDatos.LT_DISCAPACIDAD.Item(Session("Usuario_Idioma"))
        lt_si.Text = Lang.FileSystem.Alumno.MisDatos.LT_SI.Item(Session("Usuario_Idioma"))
        lt_no.Text = Lang.FileSystem.Alumno.MisDatos.LT_NO.Item(Session("Usuario_Idioma"))
        TBdiscapacidad.Attributes.Add("placeholder", Lang.FileSystem.Alumno.MisDatos.LT_DISCAPACIDAD_ESPECIFICA.Item(Session("Usuario_Idioma")))
        lt_estado_civil.Text = Lang.FileSystem.Alumno.MisDatos.LT_ESTADO_CIVIL.Item(Session("Usuario_Idioma"))
        lt_nivel_estudios.Text = Lang.FileSystem.Alumno.MisDatos.LT_NIVEL_ESTUDIOS.Item(Session("Usuario_Idioma"))
        lt_titulo_estudios.Text = Lang.FileSystem.Alumno.MisDatos.LT_TITULO_ESTUDIOS.Item(Session("Usuario_Idioma"))
        lt_btn_Actualizar.Text = Lang.FileSystem.Alumno.MisDatos.BTN_ACTUALIZAR.Item(Session("Usuario_Idioma"))
        lt_btn_Imprimir_Certificado.Text = Lang.FileSystem.Alumno.MisDatos.BTN_CERTIFICADO.Item(Session("Usuario_Idioma"))
    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub initPageData()
        'AQUI HAGO CICLO PRIMERO PARA LIMPIAR DDL Y LUEGO PARA LLENARLOS CON LOS VALORES DE EQUIPO Y SUBEQUIPO
        Try
            Dim strConexion As String
            strConexion = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
            Dim objConexion As New SqlConnection(strConexion)
            Dim miComando As SqlCommand
            Dim misRegistros As SqlDataReader
            miComando = objConexion.CreateCommand
            If (DDLequipo.Items.Count <= 0) And (DDLsubequipo.Items.Count <= 0) Then
                'Limpio por si las dudas
                DDLequipo.Items.Clear()
                DDLsubequipo.Items.Clear()
                'El Login de Usuario es único en el sistema. Obtengo todos sus planteles asignados
                miComando.CommandText = "select * from Equipo where IdInstitucion = (select P.IdInstitucion from Alumno A, Usuario U, Plantel P where U.Login = '" + User.Identity.Name + "' and A.IdUsuario = U.IdUsuario and P.IdPlantel = A.IdPlantel) order by Descripcion desc"

                objConexion.Open()
                'CARGO LOS EQUIPOS:
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                Do While misRegistros.Read()
                    DDLequipo.Items.Insert(0, misRegistros.Item("Descripcion"))
                Loop
                DDLequipo.Items.Insert(0, "")
                'AHORA CARGO LOS SUBEQUIPOS:
                misRegistros.Close()
                miComando.CommandText = "select * from Subequipo where IdInstitucion = (select P.IdInstitucion from Alumno A, Usuario U, Plantel P where U.Login = '" + User.Identity.Name + "' and A.IdUsuario = U.IdUsuario and P.IdPlantel = A.IdPlantel) order by Descripcion desc"
                misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
                Do While misRegistros.Read()
                    DDLsubequipo.Items.Insert(0, misRegistros.Item("Descripcion"))
                Loop
                DDLsubequipo.Items.Insert(0, "")
                misRegistros.Close()
            End If

            miComando.CommandText = "select A.*,G.Descripcion from Alumno A, Usuario U, Grupo G where U.Login = '" + User.Identity.Name + "' and A.IdUsuario = U.IdUsuario and G.IdGrupo = A.IdGrupo"
            misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
            If misRegistros.Read() Then 'Leo para poder accesarlos
                LblGrupo.Text = HttpUtility.HtmlDecode(misRegistros.Item("Descripcion"))
                lblMatricula.Text = HttpUtility.HtmlDecode(misRegistros.Item("Matricula"))
                TBnombre.Text = HttpUtility.HtmlDecode(misRegistros.Item("Nombre"))
                TBApePaterno.Text = HttpUtility.HtmlDecode(misRegistros.Item("ApePaterno"))
                TBApeMaterno.Text = HttpUtility.HtmlDecode(misRegistros.Item("ApeMaterno"))
                If Not IsDBNull(misRegistros.Item("Equipo")) Then
                    DDLequipo.SelectedValue = misRegistros.Item("Equipo")
                End If
                If Not IsDBNull(misRegistros.Item("SubEquipo")) Then
                    DDLsubequipo.SelectedValue = misRegistros.Item("SubEquipo")
                End If
                If Not IsDBNull(misRegistros.Item("Email")) Then
                    TBemail_1.Text = HttpUtility.HtmlDecode(misRegistros.Item("Email"))
                End If
                If Not IsDBNull(misRegistros.Item("Email2")) Then
                    TBemail_2.Text = HttpUtility.HtmlDecode(misRegistros.Item("Email2"))
                End If
                If Not IsDBNull(misRegistros.Item("Facebook")) Then
                    tbFacebook.Text = HttpUtility.HtmlDecode(misRegistros.Item("Facebook"))
                End If
                If Not IsDBNull(misRegistros.Item("Twitter")) Then
                    tbTwitter.Text = HttpUtility.HtmlDecode(misRegistros.Item("Twitter"))
                End If
                Idioma.SelectedValue = Session("Usuario_Idioma")

            End If
            misRegistros.Close()
            objConexion.Close()


            Using conn As New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                Dim cmd As New SqlCommand
                Dim reader As SqlDataReader

                cmd.CommandText = "SELECT * FROM AlumnoDatosAdicionales WHERE IdAlumno = @IdAlumno"
                cmd.Parameters.AddWithValue("@IdAlumno", Session("Usuario_IdAlumno"))

                cmd.CommandType = Data.CommandType.Text
                cmd.Connection = conn

                conn.Open()

                reader = cmd.ExecuteReader()


                If (reader.Read() = True) Then

                    DDLsexo.SelectedValue = reader.Item("Sexo")
                    TBcurp.Text = reader.Item("Curp")
                    TBpuesto.Text = reader.Item("Puesto")
                    If reader.Item("Discapacidad") = "Ninguna" Then
                        RBLdiscapacidad.SelectedValue = "No"
                    Else
                        RBLdiscapacidad.SelectedValue = "Si"
                        TBdiscapacidad.Text = reader.Item("Discapacidad")
                        TBdiscapacidad.Visible = True
                    End If
                    TBestado_civil.Text = reader.Item("EstadoCivil")
                    TBnivel_estudios.Text = reader.Item("NivelEstudios")
                    TBtitulo_estudios.Text = reader.Item("TituloRecibido")
                End If
            End Using




        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    ' imprimo los estilos variables de ésta página
    Protected Sub OutputCss()
        Dim s As StringBuilder = New StringBuilder()
        s.Append("<style type='text/css'>")
        s.Append("    #login .username a {")
        s.Append("        background: " & Config.Color.MenuBar_SelectedOption_BorderColor & ";")
        s.Append("        color: #FFF;")
        s.Append("    }")
        s.Append("</style>")
        ltEstilos.Text += s.ToString()
    End Sub

#End Region

    Protected Sub profileImage_OnUploadCompleted(ByVal sendser As Object, ByVal e As System.EventArgs) Handles profileImage.UploadedComplete
        Dim regex As Regex = New Regex("[A-Ba-b]*.")
        Dim name As String
        name = regex.Replace(profileImage.FileName, "")
        Session("urlProfileImage") = User.Identity.Name + System.IO.Path.GetExtension(profileImage.FileName)
        Utils.FileUtils.clearAllProfileImages(MapPath("../../Resources/profileImages/"), User.Identity.Name)
        profileImage.SaveAs(Config.Global.rutaPerfil + User.Identity.Name + System.IO.Path.GetExtension(profileImage.FileName))
    End Sub

    Protected Sub BtnActualizar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnActualizar.Click
        msgError.hide()
        msgSuccess.hide()
        Try
            SDSalumnos.UpdateCommand = "SET dateformat dmy; UPDATE Alumno SET Nombre = @Nombre, ApePaterno = @ApePaterno, ApeMaterno = @ApeMaterno, " &
                "Equipo = @Equipo, Subequipo = @Subequipo, Email = @Email, Email2 = @Email2, Facebook = @Facebook, Twitter = @Twitter, FechaModif = getdate() " &
                " where IdAlumno = (select A.IdAlumno from Alumno A, Usuario U where U.Login = @Login and A.IdUsuario = U.IdUsuario)"
            SDSalumnos.UpdateParameters.Add("Nombre", TBnombre.Text.Trim())
            SDSalumnos.UpdateParameters.Add("ApePaterno", TBApePaterno.Text.Trim())
            SDSalumnos.UpdateParameters.Add("ApeMaterno", TBApeMaterno.Text.Trim())
            SDSalumnos.UpdateParameters.Add("Equipo", DDLequipo.SelectedItem.ToString())
            SDSalumnos.UpdateParameters.Add("Subequipo", DDLsubequipo.SelectedItem.ToString())
            SDSalumnos.UpdateParameters.Add("Email", TBemail_1.Text.Trim())
            SDSalumnos.UpdateParameters.Add("Email2", TBemail_2.Text.Trim())
            SDSalumnos.UpdateParameters.Add("Facebook", tbFacebook.Text.Trim())
            SDSalumnos.UpdateParameters.Add("Twitter", tbTwitter.Text.Trim())
            SDSalumnos.UpdateParameters.Add("Login", User.Identity.Name.Trim())

            For Each p As Parameter In SDSalumnos.UpdateParameters
                p.ConvertEmptyStringToNull = False
            Next

            SDSalumnos.Update()
            SDSalumnos.UpdateParameters.Clear()

            Using conn As New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)


                Dim cmd As New SqlCommand
                Dim reader As SqlDataReader

                cmd.CommandText = "SELECT IdAlumno FROM AlumnoDatosAdicionales WHERE IdAlumno = @IdAlumno"
                cmd.Parameters.AddWithValue("@IdAlumno", Session("Usuario_IdAlumno"))

                cmd.CommandType = Data.CommandType.Text
                cmd.Connection = conn

                conn.Open()

                reader = cmd.ExecuteReader()


                If (reader.Read() = True) Then

                    SDSalumnosDatosAdicionales.UpdateCommand = "UPDATE AlumnoDatosAdicionales SET IdAlumno =@IdAlumno, Sexo = @Sexo, Curp = @Curp, Puesto=@Puesto, Discapacidad=@Discapacidad, EstadoCivil=@EstadoCivil, NivelEstudios=@NivelEstudios, TituloRecibido=@TituloRecibido " &
                    " where IdAlumno = @IdAlumno"
                    SDSalumnosDatosAdicionales.UpdateParameters.Add("IdAlumno", Session("Usuario_IdAlumno"))
                    SDSalumnosDatosAdicionales.UpdateParameters.Add("Sexo", DDLsexo.SelectedValue)
                    SDSalumnosDatosAdicionales.UpdateParameters.Add("Curp", TBcurp.Text.Trim())
                    If RBLdiscapacidad.SelectedValue = "Si" Then
                        SDSalumnosDatosAdicionales.UpdateParameters.Add("Discapacidad", TBdiscapacidad.Text)
                    Else
                        SDSalumnosDatosAdicionales.UpdateParameters.Add("Discapacidad", "Ninguna")
                    End If
                    SDSalumnosDatosAdicionales.UpdateParameters.Add("Puesto", TBpuesto.Text)
                    SDSalumnosDatosAdicionales.UpdateParameters.Add("EstadoCivil", TBestado_civil.Text)
                    SDSalumnosDatosAdicionales.UpdateParameters.Add("NivelEstudios", TBnivel_estudios.Text)
                    SDSalumnosDatosAdicionales.UpdateParameters.Add("TituloRecibido", TBtitulo_estudios.Text)
                    '  Dim IdAlumno As Integer = reader.Item("IdAlumno")
                    'SDSalumnosDatosAdicionales.UpdateParameters.Add("IdAlumno", IdAlumno.ToString)

                    For Each p As Parameter In SDSalumnosDatosAdicionales.UpdateParameters
                        p.ConvertEmptyStringToNull = False
                    Next

                    SDSalumnosDatosAdicionales.Update()
                    SDSalumnosDatosAdicionales.UpdateParameters.Clear()
                Else
                    SDSalumnosDatosAdicionales.InsertCommand = "INSERT INTO AlumnoDatosAdicionales VALUES (@IdAlumno, @Sexo, @Curp, @Puesto, @Discapacidad, @EstadoCivil,@NivelEstudios,@TituloRecibido)"
                    SDSalumnosDatosAdicionales.InsertParameters.Add("IdAlumno", Session("Usuario_IdAlumno"))
                    SDSalumnosDatosAdicionales.InsertParameters.Add("Sexo", DDLsexo.SelectedValue)
                    SDSalumnosDatosAdicionales.InsertParameters.Add("Curp", TBcurp.Text.Trim())
                    If RBLdiscapacidad.SelectedValue = "Si" Then
                        SDSalumnosDatosAdicionales.InsertParameters.Add("Discapacidad", DDLsexo.SelectedValue)
                    Else
                        SDSalumnosDatosAdicionales.InsertParameters.Add("Discapacidad", "Ninguna")
                    End If
                    SDSalumnosDatosAdicionales.InsertParameters.Add("Puesto", TBpuesto.Text)
                    SDSalumnosDatosAdicionales.InsertParameters.Add("EstadoCivil", TBestado_civil.Text)
                    SDSalumnosDatosAdicionales.InsertParameters.Add("NivelEstudios", TBnivel_estudios.Text)
                    SDSalumnosDatosAdicionales.InsertParameters.Add("TituloRecibido", TBtitulo_estudios.Text)

                    For Each q As Parameter In SDSalumnosDatosAdicionales.InsertParameters
                        q.ConvertEmptyStringToNull = False
                    Next

                    SDSalumnosDatosAdicionales.Insert()
                    SDSalumnosDatosAdicionales.InsertParameters.Clear()

                End If
            End Using
            Response.Redirect(Request.RawUrl)
            'NOTA: Si el Alumno ya se había registrado en el BLOG, su email no queda actualizado
            msgSuccess.show(Lang.FileSystem.General.MSG_SUCCESS.Item(Session("Usuario_Idioma")), Lang.FileSystem.Alumno.MisDatos.MSG_UPDATED.Item(Session("Usuario_Idioma")))
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex) ' registra el error
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub Idioma_SelectedIndexChanged(sender As Object, e As EventArgs) Handles Idioma.SelectedIndexChanged
        Dim actualizo As Boolean = False

        msgError.hide()
        msgSuccess.hide()
        Try
            SDSalumnos.UpdateCommand = "SET dateformat dmy; " &
                "UPDATE Usuario SET Idioma = @Idioma WHERE Login = @Login "
            SDSalumnos.UpdateParameters.Add("Login", User.Identity.Name.Trim())
            SDSalumnos.UpdateParameters.Add("Idioma", Idioma.SelectedValue)

            For Each p As Parameter In SDSalumnos.UpdateParameters
                p.ConvertEmptyStringToNull = False
            Next

            SDSalumnos.Update()
            SDSalumnos.UpdateParameters.Clear()

            'NOTA: Si el Alumno ya se había registrado en el BLOG, su email no queda actualizado

            Session("Usuario_Idioma") = Idioma.SelectedValue
            actualizo = True
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try

        If actualizo Then Response.Redirect("~/alumno/misdatos/")
    End Sub

    Protected Sub OnSelectedIndexChanged_RBLdiscapacidad(sender As Object, e As EventArgs) Handles RBLdiscapacidad.SelectedIndexChanged
        If RBLdiscapacidad.SelectedValue = "Si" Then
            TBdiscapacidad.Visible = True
        Else
            TBdiscapacidad.Visible = False
        End If
    End Sub

    Protected Sub BtnCertificado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCertificado.Click
        If Not DDL_Certificado.SelectedValue = "0" Then
            Last_Date.InnerHtml = CDate(CType(New EvaluacionDa, EvaluacionDa).Get_Last_DateFrom_Subject(DDL_Certificado.SelectedValue, Session("Usuario_IdAlumno"))).ToString("dd/MM/yyyy")
            Horas.InnerHtml = CType(New EvaluacionDa, EvaluacionDa).Get_Hour_From_Subject(DDL_Certificado.SelectedValue)
            Nombre_Asignatura.InnerHtml = DDL_Certificado.Items.FindByValue(DDL_Certificado.SelectedValue).Text
            Nombre_Curso_footer.InnerHtml = DDL_Certificado.Items.FindByValue(DDL_Certificado.SelectedValue).Text
            Nombre_Alumno.InnerHtml = Session("Usuario_Nombre")
            Page.ClientScript.RegisterClientScriptInclude("Certificado", ResolveUrl(Siget.Config.Global.urlScripts + "/Generador_Certificado.js"))
        End If
    End Sub


End Class
