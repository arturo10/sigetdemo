﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/alumno/principal_alumno_5-1-0.master"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="alumno_misdatos_cambiarpassword_Default" %>

<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>


<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Contenedor" runat="Server">
     <script type="text/javascript" src="<%=Siget.Config.Global.urlScripts %>sweetalert/sweetalert.min.js"></script>
    <table style="margin-left: auto; margin-right: auto;">
        <tr>
            <td colspan="2" style="text-align: center;">
                <h3>
                    <asp:Literal ID="lt_titulo" runat="server" Text="[Cambio de Contraseña]" /></h3>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="lt_currentPass" runat="server" Text="[Contraseña Actual]" />
            </td>
            <td>
                <asp:TextBox ID="tbOldPass" runat="server" Width="200" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="lt_newPass" runat="server" Text="[Nueva Contraseña]" />
            </td>
            <td>
                <asp:TextBox ID="tbNewPass" runat="server" Width="200" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="lt_newPassConfirm" runat="server" Text="[Confirmar Nueva Contraseña]" />
            </td>
            <td>
                <asp:TextBox ID="tbNewPassConfirm" runat="server" Width="200" TextMode="Password"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                <asp:LinkButton ID="btnCambiar" runat="server"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium">
                    <asp:Image ID="Image3" runat="server"
                        ImageUrl="~/Resources/imagenes/btnIcons/woocons/Button White Check.png"
                        CssClass="btnIcon"
                        Height="24" />
                    <asp:Literal ID="lt_btnCambiar" runat="server" Text="[Cambiar]" />
                </asp:LinkButton></td></tr></table></asp:Content>