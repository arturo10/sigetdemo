﻿Imports Siget

Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Data

Partial Class alumno_misdatos_cambiarpassword_Default
    Inherits System.Web.UI.Page

#Region "init"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Membership.GetUser(User.Identity.Name).CreationDate = Membership.GetUser(User.Identity.Name).LastPasswordChangedDate) Then
            ClientScript.RegisterStartupScript(tbOldPass.GetType(), "myalert", "swal(""!Cambia tu contraseña !"", ""!Por seguridad cambia tu contraseña !"", ""warning"");", True)
        End If

        Utils.Sesion.sesionAbierta()

        tbOldPass.Attributes("value") = tbOldPass.Text
        tbNewPass.Attributes("value") = tbNewPass.Text
        tbNewPassConfirm.Attributes("value") = tbNewPassConfirm.Text

        ' aquí inicializo la página la primera vez que se carga
        If Not IsPostBack Then
            aplicaLenguaje()

            initPageData()

            OutputCss()
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        TitleLiteral.Text = Lang.FileSystem.Alumno.CambiarPassword.LT_TITULO_PESTANIA.Item(Session("Usuario_Idioma"))
        lt_titulo.Text = Lang.FileSystem.Alumno.CambiarPassword.LT_TITULO.Item(Session("Usuario_Idioma"))
        lt_currentPass.Text = Lang.FileSystem.Alumno.CambiarPassword.LT_CONTRASENIA_ACTUAL.Item(Session("Usuario_Idioma"))
        lt_newPass.Text = Lang.FileSystem.Alumno.CambiarPassword.LT_CONTRASENIA_NUEVA.Item(Session("Usuario_Idioma"))
        lt_newPassConfirm.Text = Lang.FileSystem.Alumno.CambiarPassword.LT_CONTRASENIA_CONFIRMAR.Item(Session("Usuario_Idioma"))
        lt_btnCambiar.Text = Lang.FileSystem.Alumno.CambiarPassword.BTN_CAMBIAR.Item(Session("Usuario_Idioma"))

    End Sub

    ' inicializo los datos que necesito para los controles de la página
    Protected Sub initPageData()
    End Sub

    ' imprimo los estilos variables de ésta página
    Protected Sub OutputCss()
    End Sub

#End Region

    Protected Sub btnCambiar_Click(sender As Object, e As EventArgs) Handles btnCambiar.Click
        Dim validation As String = String.Empty
        Dim changed As Boolean = False

        If Not passwordChangeRequest_validate(tbOldPass, tbNewPass, tbNewPassConfirm, validation) Then
            msgError.show(validation)
        Else
            Try
                changePasswords(tbNewPass.Text.Trim())
                changed = True
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
                msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
            End Try
        End If

        If changed Then
            Siget.Utils.Sesion.SeConectaPrimeraVez(User.Identity.Name)
            Session("SuccessMessage") = Lang.FileSystem.Alumno.CambiarPassword.MSG_SUCCESS.Item(Session("Usuario_Idioma"))
            Response.Redirect("~/alumno/misdatos/")
        End If
    End Sub



#Region "passwordChangeRequest_Validation"

    ''' <summary>
    ''' Gestor principal de validación de la petición de cambio de contraseña. 
    ''' 
    ''' Debe recibir (en tres textboxes) la contraseña actual, la nueva contraseña, 
    ''' y una confirmación de la nueva contraseña,
    ''' además de un string donde se almacenará el mensaje de error si la validación falla.
    ''' </summary>
    ''' <param name="tbOldPass">Vieja contraseña.</param>
    ''' <param name="tbNewPass">Nueva contraseña.</param>
    ''' <param name="tbNewPassConfirm">Confirmación de la nueva contraseña.</param>
    ''' <param name="msgError">String referenciado donde se guarda el mensaje de error de fallar la validación.</param>
    ''' <returns>True si la validación pasa, False de lo contrario.</returns>
    ''' <remarks></remarks>
    Public Function passwordChangeRequest_validate(ByRef tbOldPass As TextBox,
                                                          ByRef tbNewPass As TextBox,
                                                          ByRef tbNewPassConfirm As TextBox,
                                                          ByRef msgError As String) As Boolean
        msgError = String.Empty
        tbOldPass.Attributes.Remove("style")
        tbNewPass.Attributes.Remove("style")
        tbNewPassConfirm.Attributes.Remove("style")

        If Not passwordChangeRequest_FieldsComplete(tbOldPass, tbNewPass, tbNewPassConfirm, msgError) Then
            Return False

        ElseIf Not passwordChangeRequest_isPasswordCurrent(tbOldPass, msgError) Then
            Return False

        ElseIf Not passwordChangeRequest_isNewValid(tbNewPass, msgError) Then
            Return False

        ElseIf Not passwordChangeRequest_isConfirmed(tbNewPass, tbNewPassConfirm, msgError) Then
            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' Verifica que todos los campos necesarios estén completos.
    ''' </summary>
    ''' <param name="tbOldPass">Vieja contraseña.</param>
    ''' <param name="tbNewPass">Nueva contraseña.</param>
    ''' <param name="tbNewPassConfirm">Confirmación de la nueva contraseña.</param>
    ''' <param name="msgError">String referenciado donde se guarda el mensaje de error de fallar la validación.</param>
    ''' <returns>True si la validación pasa, False de lo contrario.</returns>
    ''' <remarks></remarks>
    Protected Function passwordChangeRequest_FieldsComplete(ByRef tbOldPass As TextBox,
                                                                   ByRef tbNewPass As TextBox,
                                                                   ByRef tbNewPassConfirm As TextBox,
                                                                   ByRef msgError As String) As Boolean
        Dim v As Boolean = True

        If Not tbOldPass.Text.Length > 0 Then
            tbOldPass.Attributes.Add("style", "border: 2px solid #ee0000; background: #ffeeee;")
            msgError = Lang.FileSystem.Alumno.CambiarPassword.MSG_COMPLETAR.Item(Session("Usuario_Idioma"))
            v = False
        End If

        If Not tbNewPass.Text.Length > 0 Then
            tbNewPass.Attributes.Add("style", "border: 2px solid #ee0000; background: #ffeeee;")
            msgError = Lang.FileSystem.Alumno.CambiarPassword.MSG_COMPLETAR.Item(Session("Usuario_Idioma"))
            v = False
        End If

        If Not tbNewPassConfirm.Text.Length > 0 Then
            tbNewPassConfirm.Attributes.Add("style", "border: 2px solid #ee0000; background: #ffeeee;")
            msgError = Lang.FileSystem.Alumno.CambiarPassword.MSG_COMPLETAR.Item(Session("Usuario_Idioma"))
            v = False
        End If

        Return v
    End Function

    ''' <summary>
    ''' Verifica que la contraseña indicada es la actual del usuario.
    ''' </summary>
    ''' <param name="tbOldPass">Vieja contraseña.</param>
    ''' <param name="msgError">String referenciado donde se guarda el mensaje de error de fallar la validación.</param>
    ''' <returns>True si la validación pasa, False de lo contrario.</returns>
    ''' <remarks></remarks>
    Protected Function passwordChangeRequest_isPasswordCurrent(ByRef tbOldPass As TextBox,
                                                                      ByRef msgError As String) As Boolean
        Dim actual As String = String.Empty ' variable de almacenamiento de la contraseña real

        Try
            ' obtengo la contraseña del alumno almacenada en la base de datos
            Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
                conn.Open()

                Dim cmd As SqlCommand = New SqlCommand("SELECT U.Password FROM Usuario U WHERE U.Login = @Login", conn)
                cmd.Parameters.AddWithValue("@Login", User.Identity.Name.Trim())

                Dim results As SqlDataReader = cmd.ExecuteReader()
                results.Read()

                actual = results.Item("Password").ToString()

                results.Close()
                cmd.Dispose()
                conn.Close()
            End Using
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError = ex.Message.ToString()
        End Try

        ' hago la comparación del obtenido con el proporcionado
        If Not actual = tbOldPass.Text Then
            tbOldPass.Attributes.Add("style", "border: 2px solid #ee0000; background: #ffeeee;")
            msgError = Lang.FileSystem.Alumno.CambiarPassword.MSG_CURRENT_INCORRECT.Item(Session("Usuario_Idioma"))
            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' Verifica que la nueva contraseña y su confirmación sean iguales
    ''' </summary>
    ''' <param name="tbNewPass">Nueva contraseña.</param>
    ''' <param name="tbNewPassConfirm">Confirmación de la nueva contraseña.</param>
    ''' <param name="msgError">String referenciado donde se guarda el mensaje de error de fallar la validación.</param>
    ''' <returns>True si la validación pasa, False de lo contrario.</returns>
    ''' <remarks></remarks>
    Protected Function passwordChangeRequest_isConfirmed(ByRef tbNewPass As TextBox,
                                                                ByRef tbNewPassConfirm As TextBox,
                                                                ByRef msgError As String) As Boolean
        If Not tbNewPass.Text = tbNewPassConfirm.Text Then
            tbNewPassConfirm.Attributes.Add("style", "border: 2px solid #ee0000; background: #ffeeee;")
            msgError = Lang.FileSystem.Alumno.CambiarPassword.MSG_CONFIRMATION_INCORRECT.Item(Session("Usuario_Idioma"))
            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' Verifica que la nueva contraseña tenga un formato adecuado de acuerdo a los lineamientos de seguridad definidos.
    ''' </summary>
    ''' <param name="tbNewPass">Nueva contraseña.</param>
    ''' <param name="msgError">String referenciado donde se guarda el mensaje de error de fallar la validación.</param>
    ''' <returns>True si la validación pasa, False de lo contrario.</returns>
    ''' <remarks></remarks>
    Protected Function passwordChangeRequest_isNewValid(ByRef tbNewPass As TextBox,
                                                               ByRef msgError As String) As Boolean
        If Not tbNewPass.Text.Length >= 4 Then
            tbNewPass.Attributes.Add("style", "border: 2px solid #ee0000; background: #ffeeee;")
            msgError = Lang.FileSystem.Alumno.CambiarPassword.MSG_NEW_PASS_RESTRICTION_1.Item(Session("Usuario_Idioma"))
            Return False
        End If

        Return True
    End Function

#End Region

#Region "passwordChangeRequest_Update"

    ''' <summary>
    ''' Realiza el cambio real de las contraseñas en las bases de datos
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub changePasswords(ByRef newPass As String)
        ' primero cambio la contraseña de ASPNET
        changePasswordASPNET(newPass)

        ' luego cambio la contraseña en sadcome
        changePasswordSADCOME(newPass)
    End Sub

    Protected Sub changePasswordASPNET(ByRef NewPass As String)
        Dim u As MembershipUser
        u = Membership.GetUser(User.Identity.Name.Trim()) 'Obtiene datos del usuario con el LOGIN
        u.ChangePassword(u.ResetPassword(), NewPass) 'cambia PASSWORD
    End Sub

    Protected Sub changePasswordSADCOME(ByRef NewPass As String)
        Using conn As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString)
            conn.Open()

            Dim cmd As SqlCommand
            'USUARIO: alumno, profesor, coordinador  ADMIN: superadministrador, capturista
            cmd = New SqlCommand("SET dateformat dmy; UPDATE Usuario set Password = @Pass WHERE Login = @Login", conn)
            cmd.Parameters.AddWithValue("@Pass", NewPass)
            cmd.Parameters.AddWithValue("@Login", User.Identity.Name.Trim())

            cmd.ExecuteNonQuery()
            cmd.Dispose()
            conn.Close()
        End Using
    End Sub

#End Region

End Class
