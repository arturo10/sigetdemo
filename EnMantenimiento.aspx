﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Bloqueo.master" 
    AutoEventWireup="false" 
    CodeFile="EnMantenimiento.aspx.vb" 
    Inherits="EnMantenimiento"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts_ContentPlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Title_ContentPlaceHolder" Runat="Server">
    <h1>
        <asp:Label ID="Label1" runat="server" Text="[SISTEMA]" /> está en mantenimiento
    </h1>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="Contenedor" Runat="Server">
    <p>
        Disculpe las molestias, para brindarle un mejor servicio 
        el sistema está en mantenimiento en éste momento.
        <br />Esto no suele durar más de una hora, por favor regrese más tarde.
    </p>
</asp:Content>

