﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Siget.Entity;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Siget;
using System.Data.OleDb;
using System.Data.SqlClient;
using Siget.Entity;
using System.Web.Security;

public partial class Register_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void DDLInstitucion_DataBound(object sender, EventArgs e)
    {
        if (DDLInstitucion.Items.Count > 1)
            DDLInstitucion.Items.Insert(0, new ListItem("--Elije una institución", "0"));

    }
    protected void DDLPlantel_DataBound(object sender, EventArgs e)
    {
        if (DDLPlantel.Items.Count > 1)
            DDLPlantel.Items.Insert(0, new ListItem("--Elije un plantel", "0"));
    }
    protected void DDLGrupo_DataBound(object sender, EventArgs e)
    {
        
            DDLGrupo.Items.Insert(0, new ListItem("--Elije un grupo", "0"));
    }
    protected void btnRegistrarAlumno_Click(object sender, EventArgs e)
    {

        SqlCommand command;
        List<string> listCursos = new List<String>();
        string cursos="";
        Boolean atLeastOne = false;
        int success = 0;
        MembershipCreateStatus status;
        MembershipUser newUser;
        SqlParameter output = new SqlParameter("@isSuccesful", SqlDbType.Int);
        output.Direction = ParameterDirection.Output;
        IRepository<Alumno> alumnoRepository=new AlumnoRepository();

        
        foreach (ListItem item in listBoxAlumnos.Items)
        {
            if (item.Selected)
            {
                listCursos.Add(item.Value.ToString());
                atLeastOne = true;
            }
        }
        if(atLeastOne) cursos = String.Join(",", listCursos.ToArray());   
        try
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sadcomeConnectionString"].ConnectionString))
            {
                conn.Open();
                command = new SqlCommand("RegistraAlumno", conn);
                command.Parameters.Add("@Login", SqlDbType.VarChar).Value = TBUsername.Text.Trim();
                command.Parameters.Add("@Password", SqlDbType.VarChar).Value = TBPassword.Text.Trim();
                command.Parameters.Add("@Email", SqlDbType.VarChar).Value = TBEmail.Text.Trim();
                command.Parameters.Add("@Modifico", SqlDbType.VarChar).Value = User.Identity.Name;
                command.Parameters.Add("@Matricula", SqlDbType.VarChar).Value = TBMatricula.Text.Trim();
                command.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = TBNombres.Text.Trim();
                command.Parameters.Add("@ApePaterno", SqlDbType.VarChar).Value = TBApellidoPaterno.Text.Trim();
                command.Parameters.Add("@ApeMaterno", SqlDbType.VarChar).Value = TBApellidoPaterno.Text.Trim();
                command.Parameters.Add("@IdGrupo", SqlDbType.Int).Value = DDLGrupo.SelectedValue;
                command.Parameters.Add("@IdPlantel", SqlDbType.VarChar).Value = DDLPlantel.SelectedValue;
                command.Parameters.Add("@IdEvaluaciones", SqlDbType.VarChar).Value = cursos;
                command.Parameters.Add(output);
                command.CommandType = CommandType.StoredProcedure;
                command.ExecuteNonQuery();

                if (output.Value.ToString() == "1")
                {
                    newUser = Membership.CreateUser(TBUsername.Text.Trim(), TBPassword.Text.Trim(),
                                                                      TBEmail.Text.Trim(), null,
                                                                      null, true, out status);
                    if (newUser == null)
                        MsgError.show(Siget.Lang.FileSystem.General.MSG_ERROR[Session["Usuario_Idioma"].ToString()].ToString(), GetErrorMessage(status));
                    else
                    {
                        Roles.AddUserToRole(TBUsername.Text.ToString(), "Alumno");
                        MsgSuccess.show("ÉXITO", "El registro fue exitoso. Ahora solo espera a que sea autorizado por el administrador.");
                    }
                      
                }
                else
                    MsgError.show("ERROR", "Hubo algún error, contacte al administrador del sistema");
           }
 
            if(Siget.Config.Global.MAIL_ACTIVO){
                int counter=0;
                List<Alumno> alumnoList=alumnoRepository.List.ToList();
                    
                foreach(Alumno alumno in alumnoList){
                    if(alumno.Estatus.Trim()=="Registrado"){
                        counter++;
                    }
                }
                if(counter%20==0)
                    Siget.Utils.Correo.EnviaCorreo(Session["Usuario_Email"].ToString()==null?Session["Usuario_Email"].ToString():"soporte@integrant.com.mx",
                                       Siget.Config.Global.NOMBRE_FILESYSTEM + " - Mensaje en " + Siget.Config.Etiqueta.SISTEMA_CORTO,
                                         "Tiene acumulado" + counter.ToString()+" solicitudes por atender de alumnos nuevos",
                                       false);
            }

        }
        catch (Exception ex)
        {
            MsgError.show("ERROR", ex.Message.ToString());
            Siget.Utils.LogManager.ExceptionLog_InsertEntry(ex);
        } 

    }

    public string GetErrorMessage(MembershipCreateStatus status){
        switch(status){
            case MembershipCreateStatus.DuplicateUserName:
                return "El usuario ya existe, elija otro usuario.";
            case MembershipCreateStatus.DuplicateEmail:
                return "Ya existe un usuario para ese email, por favor escriba un email diferente.";
            case MembershipCreateStatus.InvalidPassword:
                return "El password es inválido, pruebe con otro.";
            case MembershipCreateStatus.InvalidEmail:
                return "El formato del email es inválido, por favor revíselo.";
            case MembershipCreateStatus.InvalidAnswer:
                return "La respuesta secreta es inválida, pruebe con otra.";
            case MembershipCreateStatus.InvalidQuestion:
                return "La pregunta de recuperación de password es inválida, pruebe con otra.";
            case MembershipCreateStatus.InvalidUserName:
                return "El nombre de usuario es inválido, corríjalo.";
            case MembershipCreateStatus.ProviderError:
                return "La autentificación falló, contacte al administrador del sistema.";
            case MembershipCreateStatus.UserRejected:
                return "La creación del usuario ha sido cancelada, consulte al administrador.";
            default:
                return "Ha ocurrido un error desconocido, verifique o contacte al administrador.";
        }
    }

  
}