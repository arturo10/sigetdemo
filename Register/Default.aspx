﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Register_Default" %>



<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>
<%@ Register Src="~/Controls/msgSuccess.ascx" TagPrefix="uc1" TagName="msgSuccess" %>
<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript" src="../Resources/scripts/jquery.min.js"></script>
    <link rel="stylesheet" href="register.css" runat="server" />
    <link rel="stylesheet" href="../Resources/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../Resources/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../Resources/css/select2.min.css" />
    <link rel="stylesheet" href="../Resources/css/MessageBoxes.css" />
   


</head>
<body class="register-body">
    <form id="form1" runat="server">
        <div>
            <asp:ToolkitScriptManager ID="MasterToolkitScriptManager" runat="server"></asp:ToolkitScriptManager>

            <section id="container-register">
                <asp:UpdatePanel runat="server" ID="UpdatePanelRegister">
                    <ContentTemplate>
                        <div id="register_Form" class="form-horizontal">
                            <div class="col-lg-6 col-lg-offset-3">
                                <div class="panel panel-default register-box">
                                    <div class="panel-heading header-panel-register">
                                        <b>Formulario de Registro</b>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Nombres</label>
                                            <div class="col-lg-9">
                                                <asp:TextBox ID="TBNombres" runat="server" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>

                                           
                                        </div>
                                        <div class="form-group">
                                             <label class="col-lg-2 control-label">Apellido Paterno</label>
                                            <div class="col-lg-4">
                                                <asp:TextBox ID="TBApellidoPaterno" runat="server" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                                <label class="col-lg-2 control-label">Apellido Materno</label>
                                            <div class="col-lg-4">
                                                <asp:TextBox ID="TBApellidoMaterno" runat="server" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Username</label>
                                            <div class="col-lg-9">
                                                <asp:TextBox ID="TBUsername" runat="server" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Contraseña</label>
                                            <div class="col-lg-3">
                                                <asp:TextBox ID="TBPassword" TextMode="Password" runat="server" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">Verifica Contraseña</label>
                                            <div class="col-lg-3">
                                                <asp:TextBox ID="TBPasswordVerifica" TextMode="Password" runat="server" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Matrícula</label>
                                            <div class="col-lg-3">
                                                <asp:TextBox ID="TBMatricula" runat="server" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-lg-2 control-label">Email</label>
                                            <div class="col-lg-4">
                                                <asp:TextBox ID="TBEmail" runat="server" CssClass="form-control">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Institución</label>
                                            <div class="col-lg-3">
                                                <asp:DropDownList runat="server" OnDataBound="DDLInstitucion_DataBound" 
                                                    DataSourceID="SDSInstitucion" ID="DDLInstitucion" AutoPostBack="true"
                                                    DataTextField="Descripcion" DataValueField="IdInstitucion" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>

                                            <label class="col-lg-2 control-label">Plantel</label>
                                            <div class="col-lg-4">
                                                <asp:DropDownList runat="server" OnDataBound="DDLPlantel_DataBound" ID="DDLPlantel"
                                                     DataSourceID="SDSPlanteles"
                                                    AutoPostBack="true" DataTextField="Descripcion"
                                                    DataValueField="IdPlantel" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Grupo</label>
                                            <div class="col-lg-9">
                                                <asp:DropDownList runat="server" OnDataBound="DDLGrupo_DataBound" ID="DDLGrupo"
                                                     DataSourceID="SDSGrupos" DataTextField="Descripcion" DataValueField="IdGrupo" AutoPostBack="true" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label">Actividades</label>
                                            <div class="col-lg-9">
                                                <asp:ListBox runat="server" CssClass="listBoxAlumnos"
                                                    ID="listBoxAlumnos" SelectionMode="Multiple" Style="width: 100%;"
                                                    DataSourceID="SDSActividades"
                                                    DataTextField="ClaveBateria" DataValueField="IdEvaluacion"></asp:ListBox>
                                            </div>
                                        </div>

                                        <div class="form-group" style="text-align: center;">
                                            <asp:LinkButton ID="btnRegistrarAlumno" OnClick="btnRegistrarAlumno_Click" runat="server"
                                                CssClass="btn btn-lg btn-default btnRegistrarAlumno">
                                        <i class="btn-label fa fa-refresh" style="padding:8px;"></i>Registrar
                                            </asp:LinkButton>

                                        </div>
                                        <div class="form-group">
                                            <uc1:msgError ID="MsgError" runat="server"></uc1:msgError>
                                            <uc1:msgSuccess ID="MsgSuccess" runat="server"></uc1:msgSuccess>
                                        </div>
                                            <div class="form-group" style="text-align: center;">
                                            <asp:LinkButton ID="LinkButton1" onClientclick="javascript: window.history.back();" runat="server"
                                                CssClass="btn btn-lg btn-success ">
                                        <i class="btn-label fa fa-arrow-left" style="padding:8px;"></i>Volver
                                            </asp:LinkButton>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </section>



            <asp:SqlDataSource ID="SDSActividades" runat="server"
                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                SelectCommand="SELECT IdEvaluacion,ClaveBateria FROM Evaluacion WHERE IdCalificacion in
(SELECT IdCalificacion FROM Calificacion
 WHERE IdAsignatura in 
	(SELECT IdAsignatura FROM Asignatura 
		WHERE Idgrado in (SELECT IdGrado FROM Grupo WHERE IdGrupo=@IdGrupo)))">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DDLGrupo" Name="IdGrupo" PropertyName="SelectedValue" />
                </SelectParameters>
            </asp:SqlDataSource>

            <asp:SqlDataSource ID="SDSInstitucion" runat="server"
                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                SelectCommand="SELECT IdInstitucion,Descripcion from Institucion"></asp:SqlDataSource>

            <asp:SqlDataSource ID="SDSPlanteles" runat="server"
                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                SelectCommand="SELECT IdPlantel,Descripcion from Plantel where IdInstitucion=@IdInstitucion">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DDLInstitucion" Name="IdInstitucion" PropertyName="SelectedValue" />

                </SelectParameters>
            </asp:SqlDataSource>

            <asp:SqlDataSource ID="SDSGrupos" runat="server"
                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                SelectCommand="SELECT IdGrupo,Descripcion from Grupo where IdPlantel=@IdPlantel">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DDLPlantel" Name="IdPlantel" PropertyName="SelectedValue" />
                </SelectParameters>
            </asp:SqlDataSource>

            <asp:SqlDataSource ID="SDSGrados" runat="server"
                ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                SelectCommand="SELECT IdGrado,Descripcion from Grado where IdNivel=(SELECT IdNivel from Plantel where IdPlantel=@IdPlantel)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DDLPlantel" Name="IdPlantel" PropertyName="SelectedValue" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>

        <script type="text/javascript" src="../Resources/scripts/bootstrap.min.js"></script>
        <script type="text/javascript" src="../Resources/scripts/select2.min.js"></script>
         <script type="text/javascript" src="../Resources/scripts/validation/bootstrapValidator.min.js"></script>
        <script>

            Sys.Application.add_load(LoadHandler);

            function LoadHandler() {

                $(function(){

              
                    $('#btnRegistrarAlumno').on('click', function (event) {

                        $("#register_Form").data('bootstrapValidator').validate();
                        if (!($("#register_Form").data('bootstrapValidator').isValid())) {
                            $("#register_Form").data('bootstrapValidator').validate();
                            event.preventDefault();
                        }
                    })
                
                    $(".listBoxAlumnos").select2({
                        placeholder: "--Selecciona un elemento--",
                        allowClear: true
                    });


                    $("#register_Form").bootstrapValidator({
                        excluded: [':disabled', ':hidden', ':not(:visible)'],
                        feedbackIcons: {
                            valid: 'glyphicon glyphicon-ok',
                            invalid: 'glyphicon glyphicon-remove',
                            validating: 'glyphicon glyphicon-refresh'
                        },
                        live: 'enabled',
                        message: 'This value is not valid',
                        submitHandler: null,
                        trigger: null,
                        fields: {
                            TBUsername: {
                                validators: {
                                    notEmpty: {
                                        message: 'El campo no puede estar vacio'
                                    }
                                }
                            },
                            TBNombres: {
                                validators: {
                                    notEmpty: {
                                        message: 'El campo no puede estar vacio'
                                    }
                                }
                            },
                            TBApellidoMaterno: {
                                validators: {
                                    notEmpty: {
                                        message: 'El campo no puede estar vacio'
                                    }
                                }
                            },
                            TBApellidoPaterno: {
                                validators: {
                                    notEmpty: {
                                        message: 'El campo no puede estar vacio'
                                    }
                                }
                            },
                            TBPassword: {
                                validators: {

                                    identical: {
                                        field: 'TBPasswordVerifica',
                                        message: 'El password y su confirmación no son los mismos'
                                    }
                                }
                            },
                            TBPasswordVerifica: {
                                validators: {

                                    identical: {
                                        field: 'TBPassword',
                                        message: 'El password y su confirmación no son los mismos'
                                    }
                                }
                            },

                            TBEmail: {
                                validators: {
                                    emailAddress: {
                                        message: 'No es una dirección de correo válida'
                                    }
                                }
                            },
                            listBoxAlumnos: {
                                validators: {
                                    notEmpty: {
                                        message: 'El campo no puede estar vacio'
                                    }
                                }
                            }

                        }
                    });

                });
            }
        </script>


    </form>
</body>
</html>
