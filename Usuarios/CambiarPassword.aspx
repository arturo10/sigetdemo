﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="CambiarPassword.aspx.vb" 
    Inherits="usuarios_CambiarPassword" 
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">

        .style11
    {
        width: 100%;
        height: 401px;
    }
    .style13
    {
        font-family: Arial, Helvetica, sans-serif;
        font-size: small;
        height: 31px;
            font-weight: bold;
        }
        .style14
    {
        width: 194px;
        height: 97px;
    }
    .style15
    {
            height: 97px;
        }
        .style20
        {
            width: 194px;
            height: 22px;
        }
        .style21
        {
            height: 22px;
        }
        .style37
        {
            height: 88px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
		<h1>
			Cambiar Contraseña de Usuario
		</h1>
		
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style11">
        <tr>
            <td class="style14">
            </td>
            <td class="style15">
                <asp:ChangePassword ID="ChangePassword1" runat="server" BackColor="#F7F6F3" 
                    BorderColor="#E6E2D8" BorderPadding="4" BorderStyle="Solid" BorderWidth="1px" 
                    ContinueDestinationPageUrl="~/Default.aspx" Font-Names="Verdana" 
                    Font-Size="0.8em" 
                    style="font-family: Arial, Helvetica, sans-serif; font-size: small">
                    <CancelButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" 
                        BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" 
                        ForeColor="#284775" />
                    <PasswordHintStyle Font-Italic="True" ForeColor="#888888" />
                    <ContinueButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" 
                        BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" 
                        ForeColor="#284775" />
                    <ChangePasswordButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" 
                        BorderStyle="Solid" BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" 
                        ForeColor="#284775" />
                    <TitleTextStyle BackColor="#5D7B9D" Font-Bold="True" Font-Size="0.9em" 
                        ForeColor="White" />
                    <ChangePasswordTemplate>
                        <table border="0" cellpadding="4" cellspacing="0" 
                            style="border-collapse:collapse;">
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0">
                                        <tr>
                                            <td align="center" colspan="2" 
                                                style="color:White;background-color:#5D7B9D;font-size:0.9em;font-weight:bold;">
                                                Cambiar la contraseña</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;">
                                                <asp:Label ID="CurrentPasswordLabel" runat="server" 
                                                    AssociatedControlID="CurrentPassword">Contraseña actual:</asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="CurrentPassword" runat="server" Font-Size="0.8em" 
                                                    TextMode="Password"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" 
                                                    ControlToValidate="CurrentPassword" 
                                                    ErrorMessage="La contraseña es obligatoria." 
                                                    ToolTip="La contraseña es obligatoria." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;">
                                                <asp:Label ID="NewPasswordLabel" runat="server" 
                                                    AssociatedControlID="NewPassword">Nueva contraseña:</asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="NewPassword" runat="server" Font-Size="0.8em" 
                                                    TextMode="Password"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" 
                                                    ControlToValidate="NewPassword" 
                                                    ErrorMessage="La nueva contraseña es obligatoria." 
                                                    ToolTip="La nueva contraseña es obligatoria." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;">
                                                <asp:Label ID="ConfirmNewPasswordLabel" runat="server" 
                                                    AssociatedControlID="ConfirmNewPassword">Confirmar la nueva contraseña:</asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="ConfirmNewPassword" runat="server" Font-Size="0.8em" 
                                                    TextMode="Password"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" 
                                                    ControlToValidate="ConfirmNewPassword" 
                                                    ErrorMessage="Confirmar la nueva contraseña es obligatorio." 
                                                    ToolTip="Confirmar la nueva contraseña es obligatorio." 
                                                    ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <asp:CompareValidator ID="NewPasswordCompare" runat="server" 
                                                    ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" 
                                                    Display="Dynamic" 
                                                    ErrorMessage="Confirmar la nueva contraseña debe coincidir con la entrada Nueva contraseña." 
                                                    ValidationGroup="ChangePassword1"></asp:CompareValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="color:Red;">
                                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;">
                                                <asp:Button ID="ChangePasswordPushButton" runat="server"
                                                    CommandName="ChangePassword"
                                                    CssClass="defaultBtn btnThemeBlue btnThemeMedium"
                                                    Text="Cambiar contraseña" ValidationGroup="ChangePassword1" />
                                            </td>
                                            <td>
                                                <asp:Button ID="CancelPushButton" runat="server"
                                                    CausesValidation="False" CommandName="Cancel"
																										CssClass="defaultBtn btnThemeGrey btnThemeMedium" Text="Cancelar" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </ChangePasswordTemplate>
                    <TextBoxStyle Font-Size="0.8em" />
                    <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
                </asp:ChangePassword>
            </td>
            <td class="style15">
            </td>
        </tr>
        <tr>
            <td class="style37">
            </td>
            <td class="style37">
                <asp:SqlDataSource ID="SDSusuario" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [Usuario]"></asp:SqlDataSource>
            </td>
            <td class="style37">
                <asp:SqlDataSource ID="SDSadmin" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>" 
                    SelectCommand="SELECT * FROM [Admin]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style20">
                <asp:HyperLink ID="HyperLink2" runat="server"
					NavigateUrl="~/" 
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Menú
                </asp:HyperLink>
            </td>
            <td class="style21">
                &nbsp;</td>
            <td class="style21">
            </td>
        </tr>
    </table>
</asp:Content>

