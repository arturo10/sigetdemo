﻿Imports Siget

Imports System.Data.OleDb 'Este es para MS Access
Imports System.Data

Partial Class usuarios_CambiarPassword
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

    End Sub

    Protected Sub ChangePassword1_ChangedPassword(ByVal sender As Object, ByVal e As System.EventArgs) Handles ChangePassword1.ChangedPassword
        'Primero leo el nuevo password
        Dim NuevoPasswd = ChangePassword1.NewPassword
        'Áhora actualizo las tablas de datos (las de seguridad las actualiza el componente ChangePassword)
        'USUARIO: alumno, profesor, coordinador  ADMIN: superadministrador, capturista
        SDSusuario.UpdateCommand = "SET dateformat dmy; UPDATE Usuario set Password = '" + NuevoPasswd + "' WHERE Login = '" + User.Identity.Name + "'"
        SDSusuario.Update()
    End Sub
End Class
