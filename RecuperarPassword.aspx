﻿<%@ Page Title="" 
    Language="VB" 
    MasterPageFile="~/Principal.master" 
    AutoEventWireup="false" 
    CodeFile="RecuperarPassword.aspx.vb" 
    Inherits="RecuperarPassword"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" Runat="Server">
    <asp:Literal ID="Literal1" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style11
        {
            width: 100%;
            height: 450px;
        }
        .style12
        {
            width: 221px;
        }
        .style13
        {
            height: 29px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" Runat="Server">
    <table class="style11">
        <tr>
            <td class="style13" colspan="3">
                    <span class="titulo">Recuperar Contraseña</span></td>
        </tr>
        <tr>
            <td class="style12">
                &nbsp;</td>
            <td>
                <asp:PasswordRecovery ID="PasswordRecovery1" runat="server" 
                    AnswerLabelText="Respuesta :" 
                    AnswerRequiredErrorMessage="La respuesta es obligatoria" BackColor="#E3EAEB" 
                    BorderColor="#E6E2D8" BorderPadding="4" BorderStyle="Solid" BorderWidth="1px" 
                    Font-Names="Verdana" Font-Size="0.8em" 
                    GeneralFailureText="Error al intentar recuperar la contraseña. Inténtelo de nuevo" 
                    QuestionFailureText="La respuesta no se pudo comprobar. Inténtelo de nuevo" 
                    QuestionInstructionText="Responda a la siguiente pregunta para recibir la contraseña" 
                    QuestionLabelText="Pregunta :" QuestionTitleText="Confirmación de la Identidad" 
                    SubmitButtonText="Enviar." 
                    SuccessText="Se ha enviado la contraseña al email con el cual se registró" 
                    UserNameFailureText="No fue posible tener acceso a su información. Inténtelo de nuevo" 
                    UserNameInstructionText="Escriba su Nombre de usuario para recibir su contraseña" 
                    UserNameLabelText="Nombre de Usuario:" 
                    UserNameRequiredErrorMessage="El nombre de usuario es obligatorio" 
                    UserNameTitleText="¿Olvidó su Contraseña?">
                    <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
                    <SuccessTextStyle Font-Bold="True" ForeColor="#1C5E55" />
                    <TextBoxStyle Font-Size="0.8em" />
                    <TitleTextStyle BackColor="#1C5E55" Font-Bold="True" Font-Size="0.9em" 
                        ForeColor="White" />
                    <SubmitButtonStyle BackColor="White" BorderColor="#C5BBAF" BorderStyle="Solid" 
                        BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#1C5E55" />
                </asp:PasswordRecovery>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Default.aspx" 
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">Regresar</asp:HyperLink>
                </td>
        </tr>
    </table>
</asp:Content>

