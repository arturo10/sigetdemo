﻿Imports Siget

Imports System.Data.SqlClient

Partial Class superadministrador_LlenarEvaluaciones
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utils.Sesion.sesionAbierta()

        TitleLiteral.Text = "Capturar Respuestas"

        Label1.Text = Config.Etiqueta.ALUMNO
        Label2.Text = Config.Etiqueta.INSTITUCION
        Label3.Text = Config.Etiqueta.PLANTEL
        Label4.Text = Config.Etiqueta.NIVEL
        Label5.Text = Config.Etiqueta.GRADO
        Label6.Text = Config.Etiqueta.CICLO
        Label7.Text = Config.Etiqueta.ASIGNATURA
        Label8.Text = Config.Etiqueta.ALUMNO
        Label9.Text = Config.Etiqueta.GRUPO
        GValumnos.Caption = Config.Etiqueta.ALUMNOS & " con Actividad Pendiente de Realizar"
        GValumnosterminados.Caption = Config.Etiqueta.ALUMNOS & " con Actividad Completada"

        If (Request.QueryString.Count > 0) Then
            DDLinstitucion.SelectedValue = Session("IdInstitucion")
            DDLcicloescolar.SelectedValue = Session("IdCicloEscolar")
            Dim Veces = CInt(Request.QueryString("Otro").ToString) 'Esta variable la uso para que solo se acomoden automáticamente
            Session("Entra") = Session("Entra") + Veces                      'los combobox cuando regreso de la página FormatoCaptura y no
        End If                                                                                                       'cada que se regresque la página porque marcan error los SDS

    End Sub

    Protected Sub DDLplantel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLplantel.DataBound
        DDLplantel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_PLANTEL & " " & Config.Etiqueta.PLANTEL, 0))
    End Sub

    Protected Sub DDLnivel_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLnivel.DataBound
        DDLnivel.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_NIVEL & " " & Config.Etiqueta.NIVEL, 0))
    End Sub

    Protected Sub DDLgrado_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrado.DataBound
        DDLgrado.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRADO & " " & Config.Etiqueta.GRADO, 0))
    End Sub

    Protected Sub DDLcicloescolar_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcicloescolar.DataBound
        DDLcicloescolar.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_CICLO & " " & Config.Etiqueta.CICLO, 0))
    End Sub

    Protected Sub DDLasignatura_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLasignatura.DataBound
        DDLasignatura.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_ASIGNATURA & " " & Config.Etiqueta.ASIGNATURA, 0))
    End Sub

    Protected Sub DDLcalificacion_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLcalificacion.DataBound
        DDLcalificacion.Items.Insert(0, New ListItem("---Elija una Calificación", 0))
    End Sub

    Protected Sub DDLgrupo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.DataBound
        DDLgrupo.Items.Insert(0, New ListItem("---Elija " & Config.Etiqueta.ARTIND_GRUPO & " " & Config.Etiqueta.GRUPO, 0))
    End Sub

    Protected Sub GVevaluaciones_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevaluaciones.SelectedIndexChanged
        'Las siguientes variables las necesitaré cuando regrese a esta página a capturar otro Alumno para mover los combobox
        Session("IdInstitucion") = DDLinstitucion.SelectedValue
        Session("IdPlantel") = DDLplantel.SelectedValue
        Session("IdNivel") = DDLnivel.SelectedValue
        Session("IdGrado") = DDLgrado.SelectedValue
        Session("IdAsignatura") = DDLasignatura.SelectedValue
        Session("IdCicloEscolar") = DDLcicloescolar.SelectedValue
        Session("IdCalificacion") = DDLcalificacion.SelectedValue
        Session("IndiceEvaluacion") = GVevaluaciones.SelectedIndex
    End Sub

    Protected Sub GValumnos_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GValumnos.SelectedIndexChanged
        'EL CAPTURISTA PUEDE CAPTURAR LO QUE SEA SIN RESTRICCIONES DE SERIACION DE ACTIVIDADES
        Session("IdEvaluacion") = GVevaluaciones.SelectedRow.Cells(1).Text
        Session("IdAlumno") = GValumnos.SelectedRow.Cells(1).Text
        Session("Titulo") = "Captura de respuestas para <font color=BLUE>" + GValumnos.SelectedRow.Cells(7).Text + _
                            " " + GValumnos.SelectedRow.Cells(5).Text + " " + GValumnos.SelectedRow.Cells(6).Text + _
                            " </font> en la Actividad <font color=BLUE>" + GVevaluaciones.SelectedRow.Cells(3).Text + "</font>"
        Session("Entra") = 0 'Esta variable la uso para que funcionen los cambios de valores en los ComboBox cuando regrese a capturar otro
        Response.Redirect("FormatoCaptura.aspx")
    End Sub


    'Los siguientes componentes deben estar seguidos en la misma columna si no, no agarra el cambio automatico

    Protected Sub DDLgrupo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DDLgrupo.SelectedIndexChanged
        Session("IdGrupo") = DDLgrupo.SelectedValue 'Esta variable la necesitare para cuando regrese a capturar otro
    End Sub

    Protected Sub SDSplanteles_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSplanteles.Selected
        'Request.QueryString.Count > 0 Es para saber que la pagina viene llamada de otra anterior que le está pasando un valor
        If Request.QueryString.Count > 0 And Session("Entra") = 1 Then
            DDLplantel.SelectedValue = Session("IdPlantel")
        End If
    End Sub

    Protected Sub SDSniveles_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSniveles.Selected
        If Request.QueryString.Count > 0 And Session("Entra") = 1 Then
            DDLnivel.SelectedValue = Session("IdNivel")
        End If
    End Sub

    Protected Sub SDSgrados_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSgrados.Selected
        If Request.QueryString.Count > 0 And Session("Entra") = 1 Then
            DDLgrado.SelectedValue = Session("IdGrado")
        End If
    End Sub

    Protected Sub SDSasignaturas_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSasignaturas.Selected
        If Request.QueryString.Count > 0 And Session("Entra") = 1 Then
            DDLasignatura.SelectedValue = Session("IdAsignatura")
        End If
    End Sub

    Protected Sub SDScalificaciones_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDScalificaciones.Selected
        Try
            If (Request.QueryString.Count > 0) And Session("Entra") = 1 Then
                DDLcalificacion.SelectedValue = Session("IdCalificacion")
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub SDSgrupos_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSgrupos.Selected
        If Request.QueryString.Count > 0 And Session("Entra") = 1 Then
            DDLgrupo.SelectedValue = Session("IdGrupo")
        End If
    End Sub
    Protected Sub SDSevaluaciones_Selected(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceStatusEventArgs) Handles SDSevaluaciones.Selected
        Try
            If (Request.QueryString.Count > 0) And Session("Entra") = 1 Then
                GVevaluaciones.SelectedIndex = Session("IndiceEvaluacion")
            End If
        Catch ex As Exception
            Utils.LogManager.ExceptionLog_InsertEntry(ex)
            msgError.show(Lang.FileSystem.General.MSG_ERROR.Item(Session("Usuario_Idioma")), ex.Message.ToString())
        End Try
    End Sub

    Protected Sub GVevaluaciones_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVevaluaciones.DataBound
        If GVevaluaciones.Rows.Count <= 0 And DDLcalificacion.SelectedIndex > 0 Then
            msgError.show("No existen Actividades asignadas para capturar")
            GValumnosterminados.DataBind()
            GValumnos.DataBind()
        Else
            msgError.hide()
        End If
    End Sub

    ' wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww

    Protected Sub GVevaluaciones_PreRender(sender As Object, e As EventArgs) Handles GVevaluaciones.PreRender
        ' la siguiente línea se necesita para que al crear el markup final de la tabla,
        ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
        ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
        If GVevaluaciones.Rows.Count > 0 Then
            GVevaluaciones.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub

    Protected Sub GVevaluaciones_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowCreated
        ' aqui agrego los botones de ordenamiento para los títulos de columnas
        If e.Row.RowType = DataControlRowType.Header Then
            For Each tc As TableCell In e.Row.Cells
                If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
                    Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
                    If Not IsNothing(lb) Then
                        ' quito el link button para reañadirlo después del link
                        tc.Controls.RemoveAt(0)
                        ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
                        Dim div As HtmlGenericControl = New HtmlGenericControl("div")
                        ' creo el ícono de sorting
                        Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GVevaluaciones.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GVevaluaciones_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GVevaluaciones.RowDataBound
    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GVevaluaciones, "Select$" & e.Row.RowIndex).ToString())
    End If
  End Sub

  ' para seleccionar una fila de (un) datagrid(s) al hacer click sobre cualquier parte de la fila
  ' necesito sobreescribir éste método de la página, iterando a través de las filas del(os) gridview(s),
  ' y para cada fila encontrada añadir el evento Select$ + INDEX al registro de event validation,
  ' para que no falle la validación de integridad del control.
  ' http://msdn.microsoft.com/en-us/library/system.web.ui.page.enableeventvalidation.aspx
  Protected Overrides Sub Render(ByVal writer As HtmlTextWriter)
    Dim r As GridViewRow
    For Each r In GVevaluaciones.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GVevaluaciones, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    For Each r In GValumnos.Rows()
      If r.RowType = DataControlRowType.DataRow Then
        Page.ClientScript.RegisterForEventValidation(New System.Web.UI.PostBackOptions(GValumnos, "Select$" + r.RowIndex.ToString()))
      End If
    Next

    MyBase.Render(writer) ' necesario por default
  End Sub

  Protected Sub GValumnos_PreRender(sender As Object, e As EventArgs) Handles GValumnos.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GValumnos.Rows.Count > 0 Then
      GValumnos.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GValumnos_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GValumnos.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GValumnos.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

  Protected Sub GValumnos_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GValumnos.RowDataBound
    ' a cada fila le añado el postback event para seleccionarla si se hace click sobre ella
    If e.Row.RowType = DataControlRowType.DataRow Then
      e.Row.Cells(0).Attributes.Add("onclick", Page.ClientScript.GetPostBackClientHyperlink(GValumnos, "Select$" & e.Row.RowIndex).ToString())
    End If

    ' Etiquetas de GridView
    If e.Row.RowType = DataControlRowType.Header Then
      Dim LnkHeaderText As LinkButton = e.Row.Cells(1).Controls(1)
      LnkHeaderText.Text = "Id_" & Config.Etiqueta.ALUMNO

      LnkHeaderText = e.Row.Cells(8).Controls(1)
      LnkHeaderText.Text = Config.Etiqueta.MATRICULA
    End If
  End Sub

  Protected Sub GValumnosterminados_PreRender(sender As Object, e As EventArgs) Handles GValumnosterminados.PreRender
    ' la siguiente línea se necesita para que al crear el markup final de la tabla,
    ' la fila de encabezado se incluya en la sección separada <thead>; de lo contrario
    ' el se incluye en <tbody> como la primera fila, y se mezclan los estilos css para tablas con filas seleccionables
    If GValumnosterminados.Rows.Count > 0 Then
      GValumnosterminados.HeaderRow.TableSection = TableRowSection.TableHeader
    End If
  End Sub

  Protected Sub GValumnosterminados_RowCreated(sender As Object, e As GridViewRowEventArgs) Handles GValumnosterminados.RowCreated
    ' aqui agrego los botones de ordenamiento para los títulos de columnas
    If e.Row.RowType = DataControlRowType.Header Then
      For Each tc As TableCell In e.Row.Cells
        If tc.HasControls() Then ' evita añadir íconos en celdas que no tengan link
          Dim lb As LinkButton = CType(tc.Controls(0), LinkButton)
          If Not IsNothing(lb) Then
            ' quito el link button para reañadirlo después del link
            tc.Controls.RemoveAt(0)
            ' creo el div que contendrá al ícono de sorting (necesario por problemas de render en Firefox)
            Dim div As HtmlGenericControl = New HtmlGenericControl("div")
            ' creo el ícono de sorting
            Dim icon As Image = New Image()
            icon.ImageUrl = Config.Global.urlImagenes & "btnIcons/sortable.png"
            ' ésta propiedad es importante para que se cargue a la izquierda
            div.Attributes.Add("style", "float: left; position: absolute;")
            ' añado el ícono al div
            div.Controls.Add(icon)
            ' añado el div al header
            tc.Controls.Add(div)
            ' reañado el link
            tc.Controls.Add(lb)
            ' el márgen a la izquierda del control debe ser al menos igual al ancho del ícono
            lb.Attributes.Add("style", "float: left; margin-left: 15px;")

            ' reviso si la expresión de ordenado del gridview es igual al comando del link,
            ' para asignarle un resaltado con css
            If GValumnosterminados.SortExpression = lb.CommandArgument Then
              lb.Attributes.Add("style", "float: left; margin-left: 15px; text-decoration: underline;")
            End If
          End If
        End If
      Next
    End If
  End Sub

    Protected Sub GValumnosterminados_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles GValumnosterminados.RowDataBound
        ' Etiquetas de GridView
        If e.Row.RowType = DataControlRowType.Header Then
            Dim LnkHeaderText As LinkButton = e.Row.Cells(0).Controls(1)
            LnkHeaderText.Text = "Id_" & Config.Etiqueta.ALUMNO

            LnkHeaderText = e.Row.Cells(4).Controls(1)
            LnkHeaderText.Text = Config.Etiqueta.MATRICULA
        End If
    End Sub

End Class
