﻿<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="Default.aspx.vb"
    Inherits="capturista_Default"
    MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type='text/css'>#toolBarContainer{display: none;}</style> <!-- la toolbar no es necesaria en páginas principales -->

    <!-- Estilos para los menús de inicio con esquema de lista -->
    <link id="MainMenu_List" rel="stylesheet" media="screen" href="../Resources/css/MainMenu_List.css" />

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Menú de Capturistas
    </h1>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <div class="centeringContainer">
        <div class="centered">
            <table class="mainMenuTable">
                <tbody>
                    <tr>
                        <td class="lineSeparator">&nbsp;
                        </td>
                    </tr>
                    <tr class="MenuLineA">
                        <!-- 
                            MENU SECTION
                             - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                            -->
                        <td class="MenuColumn VerticalIndented">
                            <div class="MenuTitleDiv">
                                <asp:Image ID="Image2" runat="server"
                                    ImageUrl="~/Resources/imagenes/btnIcons/iconsweets/invoice_document_file.png" />
                                <br />
                                Reactivos
                            </div>
                        </td>
                        <td class="OptionsColumn VerticalIndented">
                            <table>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="HyperLink2" runat="server"
                                            NavigateUrl="~/capturista/LlenarEvaluaciones ">
											<div class="mnItem mnIndent1 mnItemLink">
                                                <asp:Image ID="Image25" runat="server"
                                                    ImageUrl="~/Resources/imagenes/dotsmall.png"
                                                    Width="10" Height="10" />
													Capturar Respuestas
											</div>
                                        </asp:HyperLink>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>

