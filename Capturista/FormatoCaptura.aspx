<%@ Page Title=""
    Language="VB"
    MasterPageFile="~/Principal.master"
    AutoEventWireup="false"
    CodeFile="FormatoCaptura.aspx.vb"
    Inherits="capturista_FormatoCaptura"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/Controls/msgInfo.ascx" TagPrefix="uc1" TagName="msgInfo" %>
<%@ Register Src="~/Controls/msgError.ascx" TagPrefix="uc1" TagName="msgError" %>



<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle_ContentPlaceHolder" runat="Server">
    <asp:Literal ID="TitleLiteral" runat="server" Text="Integrant" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style10 {
            width: 100%;
        }

        .style11 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: bold;
            text-align: center;
        }

        .style39 {
            text-align: center;
        }

        .style40 {
            text-align: right;
        }

        .style41 {
            height: 24px;
        }

        .style32 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: small;
            font-weight: normal;
            text-align: right;
            height: 25px;
            width: 419px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Scripts_ContentPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Title_ContentPlaceHolder" runat="Server">
    <h1>Reactivos - Capturar respuestas
    </h1>
    Permite capturar las respuestas que hizo un 
	<asp:Label ID="Label1" runat="server" Text="[ALUMNO]" />
    &nbsp;en una actividad de opci�n m�ltiple o respuesta abierta.
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Contenedor" runat="Server">
    <table class="style10">
        <tr>
            <td colspan="3" class="style41">
                <asp:Label ID="LblTitulo" runat="server"
                    Style="font-family: Arial, Helvetica, sans-serif; font-size: small; font-weight: 700"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <uc1:msgError runat="server" ID="msgError" />
            </td>
        </tr>
        <tr>
            <td class="style11" colspan="3">CAPTURAR LAS RESPUESTAS</td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Panel ID="PnlCaptura" runat="server" BackColor="#E3EAEB"
                    HorizontalAlign="Left">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="text-align: center" colspan="3">
                <asp:Button ID="BtnAceptar" runat="server" OnClientClick="this.disabled=true"
                    Text="Aceptar" UseSubmitBehavior="False" CssClass="defaultBtn btnThemeBlue btnThemeMedium" />
                &nbsp;
				<asp:Button ID="BtnCancelar" runat="server"
                    PostBackUrl="~/capturista/LlenarEvaluaciones.aspx" Text="Cancelar"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium" />
            </td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td class="style39" colspan="3">
                <uc1:msgInfo runat="server" ID="msgInfo" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Panel ID="PnlRespuestas" runat="server" BackColor="#E3EAEB"
                    HorizontalAlign="Left">
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="text-align: center" colspan="3">
                <asp:Button ID="BtnAceptarResp" runat="server" OnClientClick="this.disabled=true"
                    Text="  Aceptar Respuestas  " UseSubmitBehavior="False"
                    CssClass="defaultBtn btnThemeBlue btnThemeMedium" Visible="False" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="style40">&nbsp;</td>
        </tr>
        <tr>
            <td class="style40">
                <asp:HyperLink ID="HyperLink1" runat="server"
                    NavigateUrl="~/capturista/Default.aspx"
                    CssClass="defaultBtn btnThemeGrey btnThemeWide">
					Regresar&nbsp;al&nbsp;Men�
                </asp:HyperLink>
            </td>
            <td class="style40">&nbsp;</td>
            <td class="style40">
                <asp:LinkButton ID="LinkButton1" runat="server"
                    PostBackUrl="LlenarEvaluaciones.aspx?Otro=1"
                    CssClass="defaultBtn btnThemeGrey btnThemeMedium">Capturar otro</asp:LinkButton>
            </td>
        </tr>
    </table>

    <table class="dataSources">
        <tr>
            <td>

                <asp:SqlDataSource ID="SDSrespuestas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [Respuesta]">
                    <InsertParameters>
                        <asp:Parameter Direction="Output" Name="NuevoIdresp" Size="8" Type="Int32" />
                    </InsertParameters>
                </asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSrespuestasabiertas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [RespuestaAbierta]"></asp:SqlDataSource>

            </td>
            <td>

                <asp:SqlDataSource ID="SDSevaluacionesterminadas" runat="server"
                    ConnectionString="<%$ ConnectionStrings:sadcomeConnectionString %>"
                    SelectCommand="SELECT * FROM [EvaluacionTerminada]"></asp:SqlDataSource>

            </td>
            <td></td>
        </tr>
    </table>
</asp:Content>

