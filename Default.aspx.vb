﻿Imports Integrant.Siget

Imports System.Data.SqlClient

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LiteralVersion.Text = "5.2.8"

        If Utils.Bloqueo.sistemaBloqueado() Then
            FormsAuthentication.SignOut()
            Response.Redirect("~/EnMantenimiento.aspx")
        End If

        ' evita la redirección de forms authentication
        deshabilitaReturnUrl()

        ' antes de cargar, redirecciona a su menu si el usuario ya inició sesión
        redirectIfLoggedIn()

        ' revisa si es tiempo de desbloquear usuarios y si es, desbloquea
        ' esto significa que alguien bloqueado puede desbloquearse a si mismo esperando un tiempo
        Utils.DesbloquearUsuarios.Revisa_Desbloquea()

        Integrant.Siget.UserInterface.Include.JQuery(Literal2)

        '______________________________________________________________________
        'Despliego los Avisos Públicos si los hay
        Dim strConexion As String = ConfigurationManager.ConnectionStrings("sadcomeConnectionString").ConnectionString
        Dim objConexion As New SqlConnection(strConexion)
        Dim miComando As SqlCommand
        Dim misRegistros As SqlDataReader
        miComando = objConexion.CreateCommand
        objConexion.Open()
        miComando.CommandText = "select * from Mensaje where FechaInicia <= getdate() and FechaTermina >= getdate() order by Posicion"
        misRegistros = miComando.ExecuteReader() 'Creo conjunto de registros
        If misRegistros.HasRows Then
            While misRegistros.Read()
                Select Case misRegistros.Item("Posicion")
                    Case 1
                        LblMensaje1.Text = misRegistros.Item("Contenido")
                    Case 2
                        LblMensaje2.Text = misRegistros.Item("Contenido")
                    Case 3
                        LblMensaje3.Text = misRegistros.Item("Contenido")
                    Case 4
                        LblMensaje4.Text = misRegistros.Item("Contenido")
                    Case 5
                        LblMensaje5.Text = misRegistros.Item("Contenido")
                    Case 6
                        LblMensaje6.Text = misRegistros.Item("Contenido")
                End Select
            End While
        End If
        misRegistros.Close()
        objConexion.Close()

        '______________________________________________________________________
        If Not IsPostBack Then
            ' si existe una cookie con nombre de usuario, lo presento
            If Not Request.Cookies("RememberMe") Is Nothing Then
                Try
                    CType(LoginPpal.FindControl("UserName"), TextBox).Text = Request.Cookies("RememberMe").Value
                    CType(LoginPpal.FindControl("chkRememberMe"), CheckBox).Checked = True

                    If CType(LoginPpal.FindControl("CBmostrar"), CheckBox).Checked Then
                        LoginPpal.Controls(0).Controls(11).Focus()
                    Else
                        LoginPpal.Controls(0).Controls(9).Focus()
                    End If
                Catch ex As Exception
                    Utils.LogManager.ExceptionLog_InsertEntry(ex)
                End Try
            End If

            ' si llegó a este inicio, no ha iniciado sesión, y no es un postback, va a iniciar sesión: destruye sesiones viejas si las hay
            Session.Contents.RemoveAll()

            ' personaliza la página
            OutputCSS()

            aplicaLenguaje()
        End If

        ' oculto la firma si está configurada
        If Not Config.Siget_Config.FIRMA_PIE_INTEGRANT Then
            imgLogoIntegrantFooter.Visible = False
            hlIntegrant.Visible = False
            ltCompany.Visible = False
        End If
    End Sub

    ' aplica las etiquetas del lenguaje sobre los controles estáticos de la página
    Protected Sub aplicaLenguaje()
        LblFecha.Text = DateAndTime.Now

        LabelA.Text = Lang.Etiqueta.Sistema_Corto.Item(Config.Siget_Config.Idioma_General)
        TitleLiteral.Text = Lang.Etiqueta.Sistema_Corto.Item(Config.Siget_Config.Idioma_General)

        LoginPpal.FailureText = Lang.FileSystem.Home.LOGIN_FAILURE.Item(Config.Siget_Config.Idioma_General)
        CType(LoginPpal.FindControl("UserNameLabel"), Label).Text = Lang.FileSystem.Home.LOGIN_USERNAME.Item(Config.Siget_Config.Idioma_General)
        CType(LoginPpal.FindControl("PasswordLabel"), Label).Text = Lang.FileSystem.Home.LOGIN_PASSWORD.Item(Config.Siget_Config.Idioma_General)
        CType(LoginPpal.FindControl("UserNameRequired"), RequiredFieldValidator).ErrorMessage = Lang.FileSystem.Home.LOGIN_USER_REQUIRED.Item(Config.Siget_Config.Idioma_General)
        CType(LoginPpal.FindControl("UserNameRequired"), RequiredFieldValidator).ToolTip = Lang.FileSystem.Home.LOGIN_USER_REQUIRED.Item(Config.Siget_Config.Idioma_General)
        CType(LoginPpal.FindControl("PasswordRequired"), RequiredFieldValidator).ErrorMessage = Lang.FileSystem.Home.LOGIN_PASSWORD_REQUIRED.Item(Config.Siget_Config.Idioma_General)
        CType(LoginPpal.FindControl("PasswordRequired"), RequiredFieldValidator).ToolTip = Lang.FileSystem.Home.LOGIN_PASSWORD_REQUIRED.Item(Config.Siget_Config.Idioma_General)
        CType(LoginPpal.FindControl("LoginButton"), Button).Text = Lang.FileSystem.Home.LOGIN_BUTTON.Item(Config.Siget_Config.Idioma_General)
        CType(LoginPpal.FindControl("chkRememberMe"), CheckBox).Text = Lang.FileSystem.Home.LOGIN_REMEMBER_ME.Item(Config.Siget_Config.Idioma_General)
        CType(LoginPpal.FindControl("CBmostrar"), CheckBox).Text = Lang.FileSystem.Home.LOGIN_SHOW_PASSWORD.Item(Config.Siget_Config.Idioma_General)
        hl_privacyNotice.Text = Lang.FileSystem.Home.HL_PRIVACY.Item(Config.Siget_Config.Idioma_General)
        lt_rightsReserved.Text = Lang.FileSystem.Home.LT_RIGHTS_RESERVED.Item(Config.Siget_Config.Idioma_General)
    End Sub

    ''' <summary>
    ''' Manejo de diseño inicial en la página
    ''' </summary>
    Protected Sub OutputCSS()
        Literal1.Text = "" &
                "<style type='text/css'>" &
                        ".loginTextBox {" &
                                "border: 1px;" &
                                "border-style: solid;" &
                                "/* Color del borde de los textboxes de login */" &
                                "border-color: " & Config.Color.Login_TextBox_Border & ";" &
                        "}" &
                        "body {" &
                            "background-image: url(/" & Config.Siget_Config.NOMBRE_FILESYSTEM & "/Resources/Customization/imagenes/watermark.png);" &
                        "}" &
                        "#content {" &
                            "background-image: url(/" & Config.Siget_Config.NOMBRE_FILESYSTEM & "/Resources/Customization/imagenes/fondo_900x722.png);" &
                        "}" &
                "</style>"

        LoginPpal.Controls(0).Controls(5).Focus()

        'Cargo las imagenes de resalte para los botones
        Image3.Attributes.Add("onmouseover", "this.src='/" & Config.Siget_Config.NOMBRE_FILESYSTEM & "/Resources/Customization/imagenes/boton1up.png'")
        Image3.Attributes.Add("onmouseout", "this.src='/" & Config.Siget_Config.NOMBRE_FILESYSTEM & "/Resources/Customization/imagenes/boton1.png'")

        Image4.Attributes.Add("onmouseover", "this.src='/" & Config.Siget_Config.NOMBRE_FILESYSTEM & "/Resources/Customization/imagenes/boton2up.png'")
        Image4.Attributes.Add("onmouseout", "this.src='/" & Config.Siget_Config.NOMBRE_FILESYSTEM & "/Resources/Customization/imagenes/boton2.png'")

        Image5.Attributes.Add("onmouseover", "this.src='/" & Config.Siget_Config.NOMBRE_FILESYSTEM & "/Resources/Customization/imagenes/boton3up.png'")
        Image5.Attributes.Add("onmouseout", "this.src='/" & Config.Siget_Config.NOMBRE_FILESYSTEM & "/Resources/Customization/imagenes/boton3.png'")

        Image6.Attributes.Add("onmouseover", "this.src='/" & Config.Siget_Config.NOMBRE_FILESYSTEM & "/Resources/Customization/imagenes/boton4up.png'")
        Image6.Attributes.Add("onmouseout", "this.src='/" & Config.Siget_Config.NOMBRE_FILESYSTEM & "/Resources/Customization/imagenes/boton4.png'")

        ' cargo enlaces de controles configurables
        If Config.Enlace.needs_validation Then
            Config.Enlace.loadFromDatabase()
        End If
        hlClientLogo.NavigateUrl = Config.Enlace.client_logo
        hlButton1.NavigateUrl = Config.Enlace.home_button1
        hlButton2.NavigateUrl = Config.Enlace.home_button2
        hlButton3.NavigateUrl = Config.Enlace.home_button3
        hlButton4.NavigateUrl = Config.Enlace.home_button4

    End Sub

    Protected Sub redirectIfLoggedIn()
        'Envía al menú correspondiente si ya inició sesión
        If (Roles.IsUserInRole(User.Identity.Name, Config.Siget_Config.Perfil_Alumno)) Then
            Response.Redirect("~/alumno/cursos/")
        ElseIf (Roles.IsUserInRole(User.Identity.Name, Config.Siget_Config.Perfil_Administrador) Or
                Roles.IsUserInRole(User.Identity.Name, Config.Siget_Config.Perfil_SysAdmin)) Then
            Response.Redirect("~/superadministrador/")
        ElseIf (Roles.IsUserInRole(User.Identity.Name, Config.Siget_Config.Perfil_Capturista)) Then
            Response.Redirect("~/capturista/")
        ElseIf (Roles.IsUserInRole(User.Identity.Name, Config.Siget_Config.Perfil_Coordinador)) Then
            Response.Redirect("~/coordinador/")
        ElseIf (Roles.IsUserInRole(User.Identity.Name, Config.Siget_Config.Perfil_Operador)) Then
            Response.Redirect("~/operador/")
        ElseIf (Roles.IsUserInRole(User.Identity.Name, Config.Siget_Config.Perfil_Profesor)) Then
            Response.Redirect("~/profesor/")
        End If
    End Sub

    ''' <summary>
    ''' Deshabilita la funcionalidad de ReturnUrl al expirar la sesión de un usuario.
    ''' </summary>
    ''' <remarks>
    ''' Ocasionaba errores al originarse desde una página que necesita variables de sesión o estado 
    ''' para desplegar información, como reportes.
    ''' 
    ''' Cuando un usuario hace una petición a una página mientras su sesión expiró, forms 
    ''' authentication envía a la página de login (definida en web.config) estableciendo la página 
    ''' originaria como ReturnUrl en el URL; al reinciar sesión se envía a la página originaria (1). 
    ''' 
    ''' (1) http://msdn.microsoft.com/en-us/library/ka5ffkce%28v=vs.110%29.aspx
    ''' </remarks>
    Protected Sub deshabilitaReturnUrl()
        If Not IsNothing(Request.QueryString("ReturnUrl")) Then
            Response.Redirect("~/")
        End If
    End Sub

    Protected Sub CBmostrar_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Los objetos del componente "Login" los accedo mediante un array ...Controls(#)
        LoginPpal.Controls(0).Controls(9).Visible = Not (LoginPpal.Controls(0).Controls(9).Visible) 'Password
        LoginPpal.Controls(0).Controls(11).Visible = Not (LoginPpal.Controls(0).Controls(11).Visible)   'Password2

        'El objeto "Login" toma la contraseña del edit que se llame "Password"
        If LoginPpal.Controls(0).Controls(9).Visible Then
            LoginPpal.Controls(0).Controls(11).ID = "SinUsar"
            LoginPpal.Controls(0).Controls(9).ID = "Password"
            LoginPpal.Controls(0).Controls(9).Focus()   'El Controls(9) es el PasswordEdit
        Else 'signfica que esta activado el ver el password
            LoginPpal.Controls(0).Controls(9).ID = "SinUsar2"
            LoginPpal.Controls(0).Controls(11).ID = "Password"
            LoginPpal.Controls(0).Controls(11).Focus() 'El Controls(11) es el PasswordEdit
        End If
    End Sub

    Protected Sub LoginPpal_LoggedIn(ByVal sender As Object, ByVal e As System.EventArgs) Handles LoginPpal.LoggedIn
        ' Desde aqui aún no se puede tomar User.Identity.Name

        ' una vez iniciada la sesión, controlo funcionalidad de remember me
        If CType(LoginPpal.FindControl("chkRememberMe"), CheckBox).Checked Then
            Try
                Response.Cookies("RememberMe").Value = CType(LoginPpal.FindControl("UserName"), TextBox).Text.Trim()
                Response.Cookies("RememberMe").Expires = DateTime.Now.AddDays(30)
            Catch ex As Exception
                Utils.LogManager.ExceptionLog_InsertEntry(ex)
            End Try
        Else
            If Not Request.Cookies("RememberMe") Is Nothing Then
                Response.Cookies("RememberMe").Expires = DateTime.Now.AddDays(-1D)
            End If
        End If

        '______________________________________________________________________
        ' Establece las variables de sesion del usuario
        Utils.Sesion.sesionAbierta(LoginPpal.UserName.Trim())

        '______________________________________________________________________
        ' detecta la información del navegador del usuario
        Integrant.Siget.Utils.Sesion.RegistraInicioSesion(CType(LoginPpal.FindControl("UserName"), TextBox).Text.Trim(),
                                                                 Request.Browser,
                                                                 HfWidth.Value,
                                                                 HfHeight.Value)

    End Sub

    Protected Sub chkRememberMe_CheckedChanged(sender As Object, e As EventArgs)
        If Not CType(LoginPpal.FindControl("chkRememberMe"), CheckBox).Checked Then
            If Not Request.Cookies("RememberMe") Is Nothing Then
                Response.Cookies("RememberMe").Expires = DateTime.Now.AddDays(-1D)
            End If
        End If
    End Sub

End Class
