﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Specialized;

namespace BeyondAdmin.WebForms
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;

        protected void Page_Init(object sender, EventArgs e)
        {

            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;

            SiteMapDataSource sitemap = new SiteMapDataSource();
            sitemap.ID = "MenuDataSource";
            PlaceHolder pg = (PlaceHolder)this.FindControl("phMenuDataSource");
            BeyondAdmin.WebForms.Helpers.SideBarMenu menu = new Helpers.SideBarMenu();
            menu.ID = "SideBarMenu";
            menu.MenuItemDataBound += SideBarMenu_MenuItemDataBound;
            menu.DataSource = sitemap;

            XmlSiteMapProvider testXmlProvider = new XmlSiteMapProvider();
            NameValueCollection providerAttributes = new NameValueCollection(1);


            if (Roles.IsUserInRole("superadministrador"))
            {
                if(Siget.Config.Global.Modelo_Sistema==0)
                    providerAttributes.Add("siteMapFile", "~/Web_ALE.sitemap");
                else
                    providerAttributes.Add("siteMapFile", "~/Web_ALEMP.sitemap");

               
                testXmlProvider.Initialize("AdministradorLight", providerAttributes);

            }
            else if (Roles.IsUserInRole("sysadmin"))
            {

                if (Siget.Config.Global.Modelo_Sistema == 0)
                    providerAttributes.Add("siteMapFile", "~/WebSAE.sitemap");
                else
                    providerAttributes.Add("siteMapFile", "~/WebSAEMP.sitemap");
              
                testXmlProvider.Initialize("administrador", providerAttributes);
               
            }
            testXmlProvider.BuildSiteMap();
            sitemap.Provider = testXmlProvider;
            pg.Controls.Add(menu);
            pg.Controls.Add(sitemap);
 
            menu.DataBind();

        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

           


        }

        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut();
        }

        protected void SideBarMenu_MenuItemDataBound(object sender, MenuEventArgs e)
        {
            var node = e.Item.DataItem as SiteMapNode;

            if (node != null && !string.IsNullOrEmpty(node["imageUrl"]))
                e.Item.ImageUrl = node["imageUrl"];

            if (!string.IsNullOrEmpty(e.Item.NavigateUrl) && Request.Url.AbsolutePath.ToLower().Contains(Page.ResolveUrl(e.Item.NavigateUrl.ToLower().Replace(".aspx", ""))))
            {
                e.Item.Selected = true;
            }

        }
    }

}